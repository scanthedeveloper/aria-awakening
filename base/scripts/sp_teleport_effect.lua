

function ValidateSpellTeleport(targetLoc)
	--DebugMessage("Debuggery Deh Yah")
	if( not(IsPassable(targetLoc)) ) then
		this:SystemMessage("[FA0C0C]You cannot teleport to that location.[-]","info")
		return false
	end

	if not(this:HasLineOfSightToLoc(targetLoc,ServerSettings.Combat.LOSEyeLevel)) then
		this:SystemMessage("[$2630]","info")
		return false
	end

	-- Make sure the region can be teleported to
	local regions = GetRegionsAtLoc(targetLoc)
	for i = 1, #regions do
		if ( regions[i] == "NoTeleportSpell" ) then
			this:SystemMessage("[FA0C0C]You cannot teleport to that location.[-]","info")
			return false
		end
	end

	return true
end

RegisterEventHandler(EventType.Message,"TeleportSpellTargetResult",
	function (targetLoc)
		-- validate teleport
		if not(ValidateSpellTeleport(targetLoc)) then
			this:DelModule("sp_teleport_effect")
			return
		end

		PlayEffectAtLoc("TeleportFromEffect",this:GetLoc())
		PlayEffectAtLoc("TeleportToEffect",targetLoc)
		ignoreView = false
		this:SetWorldPosition(targetLoc)
		this:SendMessage("BreakInvisEffect", "Action", "Teleport")
		this:DelModule("sp_teleport_effect")
	end)