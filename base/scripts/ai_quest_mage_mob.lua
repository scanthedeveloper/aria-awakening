require 'base_ai_mob'
require 'base_ai_intelligent'
require 'base_ai_casting'

AI.Settings.CanUseCombatAbilities = false
AI.Settings.CanCast = true

RegisterEventHandler(EventType.Message,"CopyOtherMobile",function ( ... )
	AI.Settings.CanUseCombatAbilities = true
	AI.Settings.CanConverse = true
	this:SendMessage("UpdateName")
end)

RegisterEventHandler(EventType.Message, "HasDiedMessage",
    function(killer)

        if Success(0.03) then
            MoveEquipmentToGround(this, true)
        end

    	this:ScheduleTimerDelay(TimeSpan.FromSeconds(0.5),"DestroyWraith")
    	PlayEffectAtLoc("VoidTeleportToEffect",this:GetLoc(),2)
    end)


RegisterEventHandler(EventType.Timer,"DestroyWraith",function ( ... )
    this:SetScale(0*this:GetScale())
    CallFunctionDelayed(TimeSpan.FromSeconds(10),function()this:Destroy()end)
end)

RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()  
    local spawnRegion
    local regions = GetRegionsAtLoc(this:GetLoc())
    for i=1, #regions do
        local region = regions[i]
        if region == "GrandmasterMobSpawnA" or region == "GrandmasterMobSpawnB" then
            spawnRegion = region
        end
    end
    if not spawnRegion then
        --DebugMessage("HARD-SPAWN:",this:GetName())
        return
    end
    --DebugMessage("O: Created",this:GetName(),"at",this:GetLoc(),"in", spawnRegion)

    local gmMobs = FindObjects(SearchMobileInRegion(spawnRegion))
    for i=1, #gmMobs do
        local template = gmMobs[i]:GetCreationTemplateId()
        if template == "quest_fighter_mob" or template == "quest_mage_mob" then
            --DebugMessage("X! Found",gmMobs[i]:GetName(),"at", gmMobs[i]:GetLoc(),"in", spawnRegion,"- Deleting", this:GetName())
            this:Destroy()
            return
        end
    end
end)