require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false
AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

local currencyName = QuestsLeague.LeagueCurrencyName.Dungeoneers
local hasRewards = this:HasObjVar("HasRewards")

function Dialog.OpenGreetingDialog(user)
        local text = "The fiends are at our gates and we are all there is to hold back the tides of death and slaughter. Be quick about your business, I have matters to attend to."
        
        
        local response = {}
        local key = 1

        if( hasRewards ) then
            text = "Hello there, is there something you'd like to purchase from the League of Dungeoneers? The table over there showcases our stocks."
            
            response[key] = {}
            response[key].text = "What can I purchase?"
            response[key].handle = "PurchaseRewards"
            key = key + 1

        end

        --[[
        response[key] = {}
        response[key].text = "Who are you?"
        response[key].handle = "Who"
        key = key + 1
        ]]


        response[key] = {}
        response[key].text = "Goodbye."
        response[key].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
end

function MakeGetRewardsTable(user)
    local Rewards = {
        HairDye = {
            Name = "Dungeoneer Hair Dye",
            Cost = 8000,
            Template = "hair_dye_dungeoneer",
        },
        ClothDye = {
            Name = "Dungeoneer Cloth Dye",
            Cost = 8000,
            Template = "dungeoneer_clothing_dye",
        },
        LeatherDye = {
            Name = "Dungeoneer Leather Dye",
            Cost = 8000,
            Template = "dungeoneer_leather_dye",
        },
        Mace = {
            Name = "Frostbite",
            Cost = 36000,
            Template = "pr10_league_weapon",
        },
        MaceCursed = {
            Name = "Frostbite (Cursed)",
            Cost = 48000,
            Template = "pr10_league_weapon_cursed",
        },
        Cloak = {
            Name = "Dungeoneer League Cloak",
            Cost = 18000,
            Template = "dungeoneer_cloak",
        },
        Ring = {
            Name = "Dungeoneer's Necklace",
            Cost = 36000,
            Template = "pr9_league_melee_necklace",
        },
        LotteryBox = {
            Name = "Title Scroll: The Cold-Hearted",
            Cost = 24000,
            Template = "pr10_league_dungeoneer_titlescroll",
        },
    }

    return Rewards
end

-- Callback to alter items if needed
function AfterPurchase(item)
    return
end

function Purchase(user, itemKey, Rewards)
    
    -- See if the item is still for sale
    if ( Rewards[itemKey] == nil or Rewards[itemKey].Cost == nil or Rewards[itemKey].Template == nil ) then
        QuickDialogMessage(this,user,"That item is not available right now, unfortunately.")
        return
    end
    
    -- Check to see if the player has enough currency
    if( QuestsLeague.AdjustLeagueCurrency( user, "Dungeoneers", -Rewards[itemKey].Cost ) ) then
        Create.InBackpack(Rewards[itemKey].Template, user, nil, function(item)
            if ( item ) then
                AfterPurchase(item)
                Dialog.OpenPurchaseRewardsDialog(user)
            else
                QuickDialogMessage(this,user,"I'm sorry something went wrong, please try that again.")
            end
        end)
    else
        QuickDialogMessage(this,user,"You do not have enough "..currencyName.." to purchase that.")
    end

end

function SetupPurchaseDialogHandles(user, Rewards)
    -- setup the dialog handles for each item purchase
    for key,val in pairs(Rewards) do
        Dialog[string.format("Open%sConfirmDialog", key)] = function(user)
            Purchase(user, key, Rewards)
        end
        Dialog[string.format("Open%sDialog", key)] = function(user)
            local text = string.format("I can sell you %s for %s "..currencyName..".", val.Name, val.Cost)
            local response = {
                {
                    text = "I accept your offer.",
                    handle = key.."Confirm",
                },
                {
                    text = "What else do you have to sell?",
                    handle = "PurchaseRewards"
                },
                {
                    text = "Goodbye.",
                    handle = ""
                },
            }
            NPCInteractionLongButton(text,this,user,"Responses",response)
        end
    end
end

function Dialog.OpenPurchaseRewardsDialog(user)
    local text = "Which item would you like to purchase?\n\nYou currently have [483D8B]"..QuestsLeague.GetLeagueCurrecy(user,"Dungeoneers").."[-] favor with the League of Dungeoneers."
    local response = {}
    local Rewards = MakeGetRewardsTable(user)

    SetupPurchaseDialogHandles(user, Rewards)

    for x, y in sortpairs(Rewards, function(Rewards, a, b) return Rewards[a]["Name"] < Rewards[b]["Name"] end) do
        table.insert(response, {text = y.Name..": "..y.Cost.." "..currencyName, handle = x})
    end

    table.insert( response, { text = "Goodbye.", handle = "" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenLotteryBoxInfoDialog(user)
    local text = "The supply box will contain one of the following items: a demon executioner scroll, a 'Demonslayer' title scroll, or a potion of protection versus demons."
    local response = {}

    table.insert( response, { text = "Thank you.", handle = "Greeting" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end


if( not hasRewards ) then

    -- [[ QUEST STUFF ]]
    function AI.QuestStepsInvolvedInFunc()
        return 
        { 
            {"DungoneerLeaguePR10", math.random(1, #AllQuests["DungoneerLeaguePR10"])} ,
            {"DungoneerLeaguePR10", math.random(1, #AllQuests["DungoneerLeaguePR10"])} ,
            {"DungoneerLeaguePR10", math.random(1, #AllQuests["DungoneerLeaguePR10"])} 
        }
    end

    AI.QuestsWelcomeText ="Tethy's balls it cold up here! But the light must be brough to this darkened land. Be quick about your tasks, this land is unforgiving to idlers."

    AI.QuestAvailableMessages = 
    {
        "Come, help end the plauges upon our lands!",
    }

    function Dialog.OpenWhoDialog(user)
        QuickDialogMessage(this,user,"I am a purveyor of prophecy and sadly a herald of doom. Help me in these dark times to shine light in the dark places of the world. If we cannot succeed, the cold grip of death awaits us all.")
    end

end


