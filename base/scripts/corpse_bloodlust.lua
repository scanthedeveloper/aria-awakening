local pulseDelay = TimeSpan.FromSeconds(1)
local killer = nil

function OnLoad()
	killer = GetKiller()
	SetRecipient(killer)
    this:PlayEffect("WarriorCorpseBloodlusted")
	this:ScheduleTimerDelay(pulseDelay, "Pulse")
	Pulse()
end

function Pulse()
	if ( not IsDead(this) ) then
		Remove()
		return
	end

	if ( TryTransfer(killer) ) then
		Remove()
		return
	end

	this:ScheduleTimerDelay(pulseDelay, "Pulse")
end

function Remove()
    this:StopEffect("WarriorCorpseBloodlusted", 3.0)
	this:DelModule(GetCurrentModule())
end

function TryTransfer(killer)
	if ( killer and killer:IsValid() and killer:DistanceFrom(this) <= 5 ) then
		if ( HasMobileEffect(killer, "Bloodlust") ) then
			local yield = GetMobYield(this)
			if ( yield and yield > 0 ) then
				-- remove bloodlust from corpse
				this:DelObjVar("BloodlustRecipient")

				local overrideRate = 16
				local projectileDist = this:GetLoc():Distance(killer:GetLoc())
				local projectileTimer = (projectileDist - .5) / overrideRate  
				this:PlayProjectileEffectToWithSound("WarriorSiphon", killer, "event:/magic/bard/riddle_of_indulgence", overrideRate, math.max(4,projectileTimer), "Bone=Ground,Target=Head")

				-- add bloodlust to killer
				killer:NpcSpeech("+ "..yield.." Bloodlust", "combat")
				AdjustCurBloodlust(killer, yield)
				UpdateBloodlust(killer)
				return true
			end
		end
	end
    return false
end

function GetMobYield()
    return 10
end

function GetKiller()
	local tagKillers = this:GetObjVar("TagKillers")
	if ( not tagKillers or not tagKillers[1] ) then 
		Remove()
		return
	end
	return tagKillers[1]
end

function SetRecipient(killer)
	local recipient = this:GetObjVar("BloodlustRecipient")
	if ( not recipient ) then
		if ( HasMobileEffect(killer, "Bloodlust") ) then
			this:SetObjVar("BloodlustRecipient", killer)
		else
			Remove()
			return
		end
	end
end

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), OnLoad)
RegisterEventHandler(EventType.LoadedFromBackup, nil, OnLoad)
RegisterEventHandler(EventType.Timer, "Pulse", Pulse)