require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

AI.QuestStepsInvolvedIn = {
    {"TailorProfessionTierFour", 2, true},
    {"TailorProfessionTierFour", 3, true},
    {"TailorProfessionTierFour", 4, true},
    {"TailorProfessionTierFour", 5, true},
    {"TailorProfessionTierFour", 6, true},
}

AI.GreetingMessages = 
{
    "Hmm?",
}

function Dialog.OpenTalkDialog(user)
    --QuickDialogMessage(this,user,"Will we be ready?")
end

function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,
    	"Just a humble clothworker."
    	)
end

function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)] or AI.GreetingMessages[1]

    response = {}

    response[1] = {}
    response[1].text = "Who are you?"
    response[1].handle = "Who" 
    
    response[2] = {}
    response[2].text = "Goodbye."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)

