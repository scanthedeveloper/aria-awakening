require 'base_ai_npc'

RUNE_PURCHASE_FEE = 100

AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

gatekeeperTeleporterObject = nil

nukeTargets = {}

function GetDestinationPrice(destInfo)
    return destInfo.Price * (1-ServerSettings.Merchants.GatekeeperDiscount)
end

function GetTeleporterObject()
    if not(gatekeeperTeleporterObject) then
        gatekeeperTeleporterObject = FindObject(SearchHasObjVar("GatekeeperTeleporter",20))
    end
    return gatekeeperTeleporterObject
end

local universes = nil
local universeSelection = {}

function Dialog.OpenGreetingDialog(user)
    FaceObject(this,user)

    if(universes == nil) then
        universes = GetUniversesWithMap("NewCelador")
    end
    
    if(universes ~= nil and #universes > 1 and user:HasObjVar("IsAttuned")) then
        local text = "Greetings. Which universe are we traveling to?"
        local responses = {}

        for i,universeName in pairs(universes) do
            table.insert(responses,{
                text = UniverseDisplayNames[universeName],
                handle = "Universe|"..tostring(i)
            })
        end

        table.insert(responses,{
            text = "Nevermind.",
            handle = "Close",
        })

        NPCInteractionLongButton(text,this,user,"Responses",responses)
    else
        ShowDestinationDialog(user)
    end
end

function ShowDestinationDialog(user)
    -- Get our destinations
    local destinations = this:GetObjVar("Destinations") or {}
        
    local text = "Where would you like to go?"
    local responses = {}
    
    for i,destinationInfo in pairs(destinations) do
        local text = destinationInfo.DisplayName
        if(GetDestinationPrice(destinationInfo) > 0) then
            text = text .. " (" ..ValueToAmountStr(GetDestinationPrice(destinationInfo),false,true)..")"
        end

        table.insert(responses,{
            text = text,
            handle = "Destination|"..tostring(i)
        })
    end

    if(universeSelection[user]) then        
        local curUniverse = GetUniverseName()
        if(universes[universeSelection[user]] ~= curUniverse) then
            local location = this:GetObjVar("Location")
            if(location) then
                local text = location.DisplayName
                if(GetDestinationPrice(location) > 0) then
                    text = text .. " (" ..ValueToAmountStr(GetDestinationPrice(location),false,true)..")"
                end

                table.insert(responses,{
                    text = text,
                    handle = "Location"
                })
            end
        end
    end

    table.insert(responses,{
        text = "Nevermind.",
        handle = "Close",
    })

    NPCInteractionLongButton(text,this,user,"Responses",responses)
end

RegisterEventHandler(EventType.ModuleAttached,"ai_npc_gatekeeper",
    function ( ... )
        if(initializer and initializer.Destinations) then
            this:SetObjVar("Destinations",initializer.Destinations)
        end

        if( initializer and initializer.RedDestinations ) then
            this:SetObjVar("RedDestinations",initializer.RedDestinations)
        end

        if( initializer and initializer.Location) then
            this:SetObjVar("Location",initializer.Location)
        end

        this:SetObjVar("IsGuard",true)
    end)

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",
    function ( user,buttonId )    
        if(buttonId == nil or buttonId == "") then return end

        if(buttonId == "Close") then
            user:CloseDynamicWindow("Responses")            
        elseif(buttonId == "BuyRune") then
            local text = "Sure. I can sell you one for "..RUNE_PURCHASE_FEE.." coins."
            local responses = {
                {
                    text = "Great!",
                    handle = "BuyRune2",
                },
                {
                    text = "Nevermind.",
                    handle = "Close",
                },
            }
            NPCInteractionLongButton(text,this,user,"Responses",responses)
        elseif(buttonId == "BuyRune2") then
            user:CloseDynamicWindow("Responses")     
            BuyRune(user)
        elseif(buttonId == "Location") then
            local location = this:GetObjVar("Location")
            StartAttune(user,location)
        else            
            local travelType, index = string.match(buttonId, "(%a+)|(%d+)")
            if(travelType == "Universe") then
                universeSelection[user] = tonumber(index)
                ShowDestinationDialog(user)
            elseif(travelType == "Destination") then
                local destinations = this:GetObjVar("Destinations") or {}
                local responseIndex = tonumber(index)
                if(destinations and responseIndex and responseIndex <= #destinations) then            
                    local destinationInfo = destinations[responseIndex]

                    StartAttune(user,destinationInfo)
                end
            end
        end
    end)

function StartAttune(user,destinationInfo)
    if not(GetTeleporterObject()) then
        this:NpcSpeech("This tower is not active.")
        return
    end

    local price = GetDestinationPrice(destinationInfo)

    if (price > 0 and CountCoins(user) < price) then
        this:NpcSpeech("I beg your pardon, but you can't afford that.")
        return
    end

    if(price > 0) then
        local transactionId = "Teleport|"..index                        
        RequestConsumeResource(user,"coins",price,transactionId,this)                   
    else
        CompleteTransaction(user,destinationInfo)
    end
end

OverrideEventHandler("base_ai_mob",EventType.Message,"AddThreat",
    function (targetObj,amount)
        if(targetObj:DistanceFrom(this) <= ServerSettings.PlayerInteractions.GatekeeperProtectionRange) then
            table.insert(nukeTargets,targetObj)
            local faceTarget = nukeTargets[1]
            if not(this:HasTimer("NukeComplete")) then
                FaceObject(this,faceTarget)

                this:PlayAnimation("cast")
                this:PlayEffect("CastWater2")
                this:PlayObjectSound("event:/magic/air/magic_air_cast_air",false,1.0)

                CallFunctionDelayed(TimeSpan.FromSeconds(1),
                    function ( ... )
                        for i,nukeTarget in pairs(nukeTargets) do
                            nukeTarget:SendMessage("ProcessTrueDamage", this, 5000, true)
                            nukeTarget:PlayEffect("LightningCloudEffect")
                            nukeTarget:SystemMessage("[$1820]","info")
                            if not(IsPlayerCharacter(nukeTarget)) then
                                nukeTarget:SetObjVar("guardKilled",true)
                            end
                        end
                    end,"NukeComplete")
            end
        end
    end)

function CompleteTransaction(user,destinationInfo)
    if not(GetTeleporterObject()) then return end

    FaceObject(this,user)

    this:NpcSpeech("One moment while I attune your body to that destination.")

    -- this is on a timer so it can handle multiple people
    CallFunctionDelayed(TimeSpan.FromSeconds(1),function ()
        if not(this:HasTimer("CastComplete")) then
            this:PlayAnimation("cast")
            this:PlayEffect("CastWater2")
            this:PlayObjectSound("event:/magic/air/magic_air_cast_air",false,2.0)
        end
        this:ScheduleTimerDelay(TimeSpan.FromSeconds(2),"CastComplete")
    end)                        

    CallFunctionDelayed(TimeSpan.FromSeconds(3.2),function()
            PlayEffectAtLoc("TeleportToEffect",user:GetLoc())
            user:SystemMessage("You have been attuned for travel to the "..destinationInfo.DisplayName,"info")            
            local destTable = { DestinationInfo = destinationInfo }
            if(universeSelection[user]) then
                DebugMessage("HMM "..tostring(universeSelection[user]))
                destTable.Universe = universes[universeSelection[user]]
            end
            DebugMessage("UNIVERSE "..tostring(destTable.Universe))
            user:SendMessage("StartMobileEffect", "GatekeeperEffect", GetTeleporterObject(), destTable)
        end)
end

function BuyRune(user)
    RequestConsumeResource(user,"coins",RUNE_PURCHASE_FEE,"Rune|",this)
end

function CreateRune(user)
    local runeTemplate = this:GetObjVar("RuneTemplate")
    CreateObjInBackpackOrAtLocation(user, runeTemplate, "RuneCreated")
    this:NpcSpeech("It's been a pleasure doing business with you.")
    user:SystemMessage("The gatekeeper hands you a teleportation rune.","info")
    RegisterSingleEventHandler(EventType.CreatedObject,"RuneCreated",
        function(success,objRef)
            if(success) then
                local regionAddress = ServerSettings.RegionAddress
                if(regionAddress) then
                    objRef:SetObjVar("RegionAddress", regionAddress)
                end
                SetItemTooltip(objRef, true)
            end
        end)
end

RegisterEventHandler(EventType.Message, "ConsumeResourceResponse", 
    function (success,transactionId,user)   
        if(success) then            
            local args = StringSplit(transactionId,"|")

            if(args[1] == "Teleport") then
                local responseIndex = tonumber(args[2])
                if not(responseIndex) then return end

                local destinations = this:GetObjVar("Destinations") or {}
                if not(destinations) or not(destinations[responseIndex]) then return end

                local destinationInfo = destinations[responseIndex]

                CompleteTransaction(user,destinationInfo)
            elseif(args[1] == "Rune") then
                CreateRune(user)
            end
        else
            this:NpcSpeech("I beg your pardon, but you can't afford that.")
            return
        end
    end)

RegisterEventHandler(EventType.Timer, "CastComplete",
    function ( ... )
        this:PlayAnimation("cast_heal")
        this:StopEffect("CastWater2",0.5)
    end)