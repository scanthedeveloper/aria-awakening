require 'base_ai_npc'

-- Leif (Liz)
-- Torynx (Brandon)
-- Ewok (Dusty)
-- Kai (Adrian)
-- Supreem (Derek)
-- Grim (James)
-- Miphon (Jeffery) 
-- Derrick

AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true
AI.Settings.CanConverse = false
AI.Settings.StationedLeash = true

local Name = this:GetObjVar("Name")

-- PRIMARY DIALOG
function Dialog.OpenGreetingDialog(user)
    local text = StaffTextStrings[Name].Greeting
    response = {}
    table.insert(response, { text = "Goodbye.", handle = "" } )
    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)

StaffTextStrings = 
{
    Leif = {
        Greeting = "Hello there, my name is Leif! Don't you look wonderful. My job here at the lounge is to make others realize how wonderful things can be, or are... or will be."
    },
    Torynx = {
        Greeting = "Greetings, I am Torynx.  My job here at the lounge is to shape the various worlds the lounge services.  You mortal live in such a world as I have shaped."
    },
    Ewok = {
        Greeting = "Salutations and good day.  I am Ewok and my job at the lounge is to catalog the histories of each and every world. I am currently researching the lands of Celeador."
    },
    Supreem = {
        Greeting = "I see you. I am Supreem and I created the lounge you stand in. It is I that gathered the worlds and set your story in motion. Use it wisely."
    },
    Miphon = {
        Greeting = "Hello there friend, stay awh... No wait, that's already been used. Forgive me, my name is Miphon and I design the new worlds that the lounge creates for motrals such as you."
    },
    Kai = {
        Greeting = "Be mindful that I am watching you. Kai's job is to see all and know all. From world to world I travel and ensure serenity reigns."
    },
    Grim = {
        Greeting = "Traveler! I am happy to see you. Introductions are in order, I am Grim. My job here at the lounge is to ensure that everything is in proper working order. Remember, a place for everything and everything in its place."
    },
    Derrick = {
        Greeting = "Tick toc... toc tick... Time flows forwards and backwards through this universe and I stay the same. One thing is finished as another starts, but both are governed by time."
    },
    Kade = {
        Greeting = "01000010 01110101 01101001 01101100 01100100 00100000 01101001 01110100 00100000 01100011 01101111 01110010 01110010 01100101 01100011 01110100 01101100 01111001 00101100 00100000 01100001 01101110 01100100 00100000 01100010 01110101 01101001 01101100 01100100 00100000 01101001 01110100 00100000 01101111 01101110 01100011 01100101 00101110"
    },
    Ameka = {
        Greeting = "I'm so glad you came. I breathe life into worlds, creating that which has never been fathomed by mortals before. These worlds are full of joys. Tomorrow, even more perhaps. Remember, life always goes on, no matter what."
    },
}