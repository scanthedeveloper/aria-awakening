function OnInteract(user, usedType)
    if (usedType ~= "Interact") then return end

    if this:GetLoc():Distance2(user:GetLoc()) > 3 then
    	user:SystemMessage("Too far to reach that.", "info")
    	return
    end

    local tomeId = this:GetObjVar("TomeId")
    local entryIndices = Dialogue.ProcessNumberRange(this:GetObjVar("EntryIndicesNumberRange"))

    if not (tomeId and entryIndices and next(entryIndices)) then user:SystemMessage("There is nothing important to learn from this.") return end

    if Lore.KnowsLore(user, tomeId, entryIndices) then
        user:SystemMessage("You've already recorded this information.","info")
        return
    end
    
    ClientDialog.Show{
	    TargetUser = user,
	    TitleStr = "Interesting Find",
	    DescStr = "This looks interesting. Want to record this in your Catalog?",
	    Button1Str = "Confirm",
	    Button2Str = "Cancel",
	    ResponseObj = this,
	    ResponseFunc = function (user,buttonId)
			if (buttonId == 0) then
				Lore.EarnLoreIfNew(user, tomeId, entryIndices)
			end
		end,
	}
end

RegisterEventHandler(EventType.Message, "UseObject", OnInteract)

RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(), function()
    AddUseCase(this,"Interact",true)
    --AddUseCase(this,"Investigate",true)
    --this:SetObjVar("LockedDown",true)
    --this:SetSharedObjectProperty("DenyPickup", true)
end)