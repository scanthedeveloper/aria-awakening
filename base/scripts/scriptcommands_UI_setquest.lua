RegisterEventHandler(EventType.DynamicWindowResponse,"SetQuestWindow",
	function (user,returnId,fieldData)
		if(returnId ~= nil) then

			local args = StringSplit(returnId, "|") or {}
			local id = args[1] or ""
			local action = tostring(args[2]) or ""
			local professionName = tostring(args[3]) or ""
			local tierIndex = tonumber(args[4]) or -99
			local tierName = tostring(args[5]) or ""
			local stepIndex = tonumber(args[6]) or -99
			local tierDotStep = tostring(tierIndex - 1).."."..tostring(stepIndex)

			--DebugMessage("Target:",id,"Caller:",user)

			if( action == "OpenTiers") then
				OpenSetTierWindow(id, professionName)
			elseif( action == "OpenSteps") then
				OpenSetStepWindow(id, professionName, tierIndex, tierName)
			elseif( action == "ChooseStep") then
				Quests.SetQuest(GameObj(tonumber(id)), user, professionName, tierDotStep)
				OpenSetStepWindow(id, professionName, tierIndex, tierName)
			elseif( action == "BackTiers") then
				OpenSetTierWindow(id, professionName)
			elseif( action == "BackBase") then
				OpenSetQuestWindow(GameObj(tonumber(id)))
			end
		end
	end)

--[[RegisterEventHandler(EventType.Message,"UpdateMissionUI",
	function()
		DebugMessage("I saw mission UI update!")
	end)]]

--SetQuest Command
OpenSetQuestWindow = function(target)
	if target then
		local name = target:GetName()
		local id = tostring(target.Id)
		local userId = tostring(target:GetAttachedUserId())
		local title = name.."'s Quests"
		local setQuestWindow = DynamicWindow("SetQuestWindow",title,250,700)
		local topMargin = 40
		local backButtonTopMargin = 0
		--local windowName = professionName or "???"
		local windowHeight = 0

		local quests = target:GetObjVar("Quests")
		
		--setQuestWindow = DynamicWindow("SetQuestWindow",tostring(target).."'s Quests",250,700)

		local professionNames = {}
		local hash = {}
		for k,v in pairs(quests) do
			local index = string.find(k, "Profession")
			if index then
				local prof = string.sub(k, 1, (index - 1))
				if not hash[prof] then
					table.insert(professionNames, prof)
					hash[prof] = true
				end
			else
				table.insert(professionNames, k)
				hash[k] = true
			end
		end
		table.sort(professionNames, function(a, b) return a < b end)

		local longList = #professionNames > 7

		if longList then
			windowHeight = 360 + topMargin * 2
			setQuestWindow = DynamicWindow("SetQuestWindow",title,250,windowHeight)
			setQuestWindow:AddLabel(40,0, "[F3F781]Obj[-]\n"..id, 230, 60, 20)
			setQuestWindow:AddLabel(120,0, "[F3F781]User[-]\n"..userId, 230, 60, 20)
			local scrollWindow = ScrollWindow(20, topMargin, 195, 280, 40)

			for i=1, #professionNames do
				local scrollElement = ScrollElement()
				--scrollElement:AddButton(0,0, "OpenTiers|"..professionNames[i], tostring(i), 40, 40, "", "", false, "List")
				scrollElement:AddButton(0,0, tostring(id).."|OpenTiers|"..professionNames[i], professionNames[i], 180, 40, "", "", false, "List")
				scrollWindow:Add(scrollElement)
			end
			setQuestWindow:AddScrollWindow(scrollWindow)
		else
			windowHeight = (#professionNames + 1) * 40 + topMargin * 2
			setQuestWindow = DynamicWindow("SetQuestWindow",title,250,windowHeight)
			setQuestWindow:AddLabel(40,0, "[F3F781]Obj[-]\n"..id, 230, 60, 20)
			setQuestWindow:AddLabel(120,0, "[F3F781]User[-]\n"..userId, 230, 60, 20)
			for i=1, #professionNames do
				setQuestWindow:AddButton(20,topMargin + 40 * (i - 1), tostring(id).."|OpenTiers|"..professionNames[i], tostring(i), 40, 40, "", "", false, "List")
				setQuestWindow:AddButton(60,topMargin + 40 * (i - 1), tostring(id).."|OpenTiers|"..professionNames[i], professionNames[i], 150, 40, "", "", false, "List")
			end
		end

		this:OpenDynamicWindow(setQuestWindow)
	end
end

OpenSetTierWindow = function(targetId, professionName, tierIndex, tierName, stepIndex)
	local targetObj = GameObj(tonumber(targetId))
	if targetObj then
		local setQuestWindow = DynamicWindow("SetQuestWindow","Edit Quest",250,700)
		local topMargin = 40
		local backButtonTopMargin = 0
		local windowName = professionName or "???"
		local windowHeight = 0
		local tierNames = ProfessionsHelpers.GetProfessionQuests(professionName)

		windowHeight = (#tierNames + 3) * 40 + topMargin * 2
		setQuestWindow = DynamicWindow("SetQuestWindow",windowName,250,windowHeight)

		local activeTier = Quests.GetActiveTierAndStep(targetObj, professionName).Tier

		for i=1, #tierNames do
			local tierTip = "Intro"
			if i > 1 then tierTip = TaskIndexTitles[i - 1] or "" end
			local ID = tostring(targetId).."|OpenSteps|"..tostring(professionName).."|"..tostring(i).."|"..tostring(tierNames[i])
			local buttonState = (i == activeTier + 1) and "disabled" or "List"
			setQuestWindow:AddButton(20,topMargin + 40 * (i - 1), ID,tostring(i - 1)..".0", 40, 40, tierTip, "", false, buttonState)
			setQuestWindow:AddButton(60,topMargin + 40 * (i - 1), ID,tierTip, 150, 40, tierTip, "", false, buttonState)
		end
		backButtonTopMargin = topMargin + 40 * (#tierNames + 1)
		local backId = tostring(targetId).."|BackBase|"..tostring(professionName)
		setQuestWindow:AddButton(20,backButtonTopMargin, backId,"Back", 125, 40, "", "", false, "List")
		
		this:OpenDynamicWindow(setQuestWindow)
	end
end

OpenSetStepWindow = function(targetId, professionName, tierIndex, tierName, stepIndex)
	local targetObj = GameObj(tonumber(targetId))
	if targetObj then
		local setQuestWindow = DynamicWindow("SetQuestWindow","Edit Quest",250,700)
		local topMargin = 40
		local backButtonTopMargin = 0
		local windowName = professionName or "???"
		local windowHeight = 0
		local windowWidth = 500
		local stringCap = 50
		local steps = Quests.GetStepList(targetObj, tierName)
		local tierTitle = ProfessionsHelpers.GetProfessionTierTitle(tierIndex)
		windowName = professionName.." "..tierTitle

		local activeTierStep = Quests.GetActiveTierAndStep(targetObj, professionName)

		local isLongList = #steps > 7
		if isLongList then 
			windowHeight = 400 + topMargin * 2
			setQuestWindow = DynamicWindow("SetQuestWindow",windowName,windowWidth,windowHeight)
			local scrollWindow = ScrollWindow(20, topMargin, windowWidth - 65, 280, 40)
			for i=1, #steps do
				local stepTip = steps[i].Instructions or ""
				local ID = tostring(targetId).."|ChooseStep|"..tostring(professionName).."|"..tostring(tierIndex).."|"..tostring(tierName).."|"..tostring(i)
				local scrollElement = ScrollElement()
				local subStepTip = string.sub(stepTip, 1, stringCap - 15)
				if string.len(stepTip) > (stringCap - 15) then subStepTip = subStepTip.."..." end
				local buttonState = (i == activeTierStep.Step and tierIndex - 1 == activeTierStep.Tier) and "disabled" or "List"
				scrollElement:AddButton(0,0, ID,tostring(tierIndex - 1).."."..tostring(i), 40, 40, stepTip, "", false, buttonState)
				scrollElement:AddButton(40,0, ID,subStepTip, windowWidth - 140, 40, stepTip, "", false, buttonState)
				scrollWindow:Add(scrollElement)
			end
			setQuestWindow:AddScrollWindow(scrollWindow)
			
			backButtonTopMargin = 360
		else
			windowHeight = (#steps + 3) * 40 + topMargin * 2
			setQuestWindow = DynamicWindow("SetQuestWindow",windowName,windowWidth,windowHeight)
			for i=1, #steps do
				local stepTip = steps[i].Instructions or ""
				local subStepTip = string.sub(stepTip, 1, stringCap)
				if string.len(stepTip) > stringCap then subStepTip = subStepTip.."..." end
				local ID = tostring(targetId).."|ChooseStep|"..tostring(professionName).."|"..tostring(tierIndex).."|"..tostring(tierName).."|"..tostring(i)
				local buttonState = (i == activeTierStep.Step and tierIndex - 1 == activeTierStep.Tier) and "disabled" or "List"
				setQuestWindow:AddButton(20,topMargin + 40 * (i - 1), ID, tostring(tierIndex - 1).."."..tostring(i), 40, 40, stepTip, "", false, buttonState)
				setQuestWindow:AddButton(60,topMargin + 40 * (i - 1), ID, subStepTip, windowWidth - 100, 40, stepTip, "", false, buttonState)
			end
			backButtonTopMargin = topMargin + 40 * (#steps + 1)
		end

		local backId = tostring(targetId).."|BackTiers|"..tostring(professionName).."|"..tostring(tierIndex).."|"..tostring(tierName)
		setQuestWindow:AddButton(20,backButtonTopMargin, backId,"Back", windowWidth * 0.5, 40, "", "", false, "List")

		this:OpenDynamicWindow(setQuestWindow)
	end
end