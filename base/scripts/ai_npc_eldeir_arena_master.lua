require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

AI.IntroMessages =
{
	"Hello!",
} 

function Dialog.OpenGreetingDialog(user)
    local text = nil    
    
    text = "Hello! What brings you to the Arena?"

    local response = {
        {
            text = "How does the Arena work?",
            handle = "How"
        }
    }
    
    NPCInteractionLongButton(text,this,user,"Responses",response)
end


function Dialog.OpenHowDialog(user)
	user:SystemMessage("Use the [F2F5A9]/party[-] command to create a party, then press [F2F5A9]Consent[-] to allow dueling with other Party Members.", "info")
	DialogEndMessage(this,user,"Form a party with your opponent and then consent to fighting them. You should then be ready to enter the ring.","OK.")
end


OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)