local equipper = nil

function EnterCostume()
	if(equipper:HasObjVar("CostumeBaseAppearance")) then 
		ExitCostume()
	end

	if not(equipper) or not(equipper:IsValid()) then return end

	local myAppearance = equipper:GetIconId()
	local myHue = equipper:GetHue()
	--local myColor = equipper:GetColor()
	local myScale = equipper:GetScale()
	equipper:SetObjVar("CostumeBaseAppearance",{Appearance=myAppearance,Hue=myHue,Scale=myScale})--Color=myColor,

	local appearanceTemplate = this:GetObjVar("AppearanceTemplate") or "goblin"
	local appearanceScale = GetTemplateObjectScale(appearanceTemplate)

	equipper:SetAppearanceFromTemplate(appearanceTemplate,true)
	equipper:SetScale(appearanceScale)

	PlayEffectAtLoc("CloakEffect", equipper:GetLoc())
end

function ExitCostume()
	if not(equipper) or not(equipper:IsValid()) then return end

	local myAppearance = equipper:GetObjVar("CostumeBaseAppearance")

	if(myAppearance) then
		equipper:SetAppearanceFromIconId(myAppearance.Appearance)
		equipper:SetHue(myAppearance.Hue)
		--equipper:SetColor(myAppearance.Color)
		equipper:SetScale(myAppearance.Scale)

		equipper:DelObjVar("CostumeBaseAppearance")

		PlayEffectAtLoc("CloakEffect", equipper:GetLoc())
	end
end

function UpdateViews(isEquipped)
	if(isEquipped) then
		equipper = this:TopmostContainer()
		if(equipper) then
			RegisterEventHandler(EventType.EnterView,"costume_view",
				function ()
					EnterCostume()
				end)

			RegisterEventHandler(EventType.LeaveView,"costume_view",
				function ()
					ExitCostume()
				end)

			AddView("costume_view",SearchSingleObject(equipper,SearchRegion("GuardZone")),1.0)
		end
	else
		ExitCostume()
		DelView("costume_view")
	end
end



RegisterSingleEventHandler(EventType.ModuleAttached,"costume_mask",
	function ( ... )
		this:SetObjVar("HandlesEquip",true)
		UpdateViews(this:IsEquipped())
	end)

RegisterSingleEventHandler(EventType.LoadedFromBackup,"",
	function ( ... )
		UpdateViews(this:IsEquipped())
	end)

RegisterEventHandler(EventType.Message, "EquipmentChanged",
	function (isEquipped)
		UpdateViews(isEquipped)
	end)

RegisterEventHandler(EventType.Message, "EquipmentChanged",
	function (isEquipped)
		UpdateViews(isEquipped)
	end)

