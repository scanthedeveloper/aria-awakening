require 'base_ai_mob'
require 'base_ai_intelligent'

-- set charge speed and attack range in combat ai
AI.Settings.Debug = false
AI.Settings.AggroRange = 15.0
AI.Settings.ChaseRange = 15.0
AI.Settings.LeashDistance = 25
AI.Settings.CanConverse = false
AI.Settings.ScaleToAge = false
AI.Settings.CanWander = true
AI.Settings.CanUseCombatAbilities = true
AI.Settings.CanCast = true
AI.Settings.ChanceToNotAttackOnAlert = 0

function FindAITarget()
    if(AI.MainTarget) then
        if(AI.MainTarget == this) then AI.MainTarget = nil end    
    end
    local mobiles = FindObjects(SearchMulti({SearchRange(this:GetObjVar("SpawnLocation"),AI.Settings.AggroRange), SearchMobile()}), GameObj(0))
    for key,mobile in pairs(mobiles) do
        -- just grab the first target
        if not(mobile:IsPlayer() or mobile:HasObjVar("Summoned") or mobile:HasModule("pet_controller") or IsDead(mobile)) then
            SetAITarget(mobile)
            return mobile
        end
    end
    return nil
end



  