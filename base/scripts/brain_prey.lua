
require 'combat'
require 'brain_inc' -- must come after combat, before mobile
require 'base_mobile'

local fsm = FSM(this, {
    States.Death,
    States.Flee,
    States.Leash,
    States.Aggro,
    States.AttackAggroList,
    States.Fight,
    States.Wander,
}, 3)

if(this:HasObjVar("AI-NoFight")) then
	fsm.RemoveState(States.Fight)
end

fsm.Start()