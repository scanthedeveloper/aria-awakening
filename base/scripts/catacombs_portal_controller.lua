require 'base_ai_settings'
require 'base_ai_state_machine'

AI.Settings.ShouldSleep = false

PropPermObjects = {
	PortalAnimation = nil,
	BonePile = nil
}

portalObj = nil

AI.StateMachine.AllStates = {	
	Inactive = {		
		OnEnterState = function(self)
			this:SetObjVar("CurrentState","Inactive")
			if(portalObj ~= nil and portalObj:IsValid()) then
				portalObj:Destroy()
				portalObj = nil
			end
			--DebugMessage("HIDE "..tostring(PropPermObjects.BonePile).." "..tostring(PropPermObjects.PortalAnimation))
			PropPermObjects.PortalAnimation:SetVisualState("Hidden")

			this:DelObjVar("CatacombsPortalActive")
			AI.StateMachine.ChangeState("WaitingForTokens")

		end,

		OnExitState = function(self)
		end,
	},

	WaitingForTokens = {
		GetPulseFrequencyMS = function() return 2500 end,

		OnEnterState = function(self)
			this:SetObjVar("CurrentState","WaitingForTokens")

			PropPermObjects.PortalAnimation:SetVisualState("Default")			

			self:AiPulse()
		end,

		AiPulse = function(self)	
			--DebugMessage("Feed me!")
			local dragonObj = nil
			local cultistObj = nil
			local treeObj = nil

			local objsOnAltar = FindObjects(SearchRange(this:GetLoc(),10))
			for i,obj in pairs(objsOnAltar) do 
				if(obj:GetCreationTemplateId() == "dragon_skull") then
					dragonObj = obj
				elseif(obj:GetCreationTemplateId() == "cultist_skull") then
					cultistObj = obj
				elseif(obj:GetCreationTemplateId() == "tree_skull") then
					treeObj = obj
				end
			end

			if(dragonObj and cultistObj and treeObj) then
				AI.StateMachine.ChangeState("PortalActive")
				dragonObj:Destroy()					
				cultistObj:Destroy()				
				treeObj:Destroy()
			end
		end,

		OnExitState = function(self,newState)			
			this:StopEffect("RadiationAuraEffect")
		end
	},

	PortalActive = {
		--GetPulseFrequencyMS = function() return 4 * 60 * 60 * 1000 end,

		OnEnterState = function(self)
			this:SetObjVar("CurrentState","PortalActive")

		    local sendto = GetRegionAddressesForName("Catacombs")
		    if(#sendto == 0) then
		    	DebugMessage("Error unable to find Catacombs")
				return
			end

			MessageRemoteClusterController(sendto[1], "Reset")

			CallFunctionDelayed(TimeSpan.FromSeconds(8),function ( ... )
				this:PlayEffect("RedCoreImpactWaveEffect")		
				this:SetObjVar("CatacombsPortalActive",true)
				RegisterSingleEventHandler(EventType.CreatedObject,"portal",
					function(success,objRef)
						portalObj = objRef
						local catacombsHubLoc = MapLocations.Catacombs.Hub
					
						objRef:SetObjVar("Destination",catacombsHubLoc)
						objRef:SetObjVar("RegionAddress","Catacombs")
						objRef:SetName("Mysterious Portal")
						objRef:SetColor("FF0000")
						MessageRemoteClusterController("Catacombs", "portal_state", "Open")

						AddView("ActivePortal",SearchSingleObject(objRef),1000)
						RegisterEventHandler(EventType.LeaveView,"ActivePortal",
							function()
								AI.StateMachine.ChangeState("Inactive")
							end)
					end)

				CreateTempObj("portal_red",this:GetLoc(),"portal")
			end)
		end,

		--AiPulse = function(self)
		--	AI.StateMachine.ChangeState("Inactive")
		--end
	}
}

function OnLoad()
	local searchResults = FindPermanentObjects(PermanentObjSearchHasObjectTag("Catacombs Portal Animation"))
	if(#searchResults > 0) then
		PropPermObjects.PortalAnimation = searchResults[1]
	end

	AI.StateMachine.Init("Inactive")
end


function OnClose()
	this:PlayEffect("DeathwaveEffect")		
	this:PlayEffect("RedCoreImpactWaveEffect")		
	AI.StateMachine.ChangeState("Inactive")
	MessageRemoteClusterController("Catacombs", "portal_state", "Closed")
end


RegisterSingleEventHandler(EventType.ModuleAttached,"catacombs_portal_controller",OnLoad)
RegisterSingleEventHandler(EventType.LoadedFromBackup,"",OnLoad)
RegisterEventHandler(EventType.Message,"close_catacombs_portal",OnClose)



