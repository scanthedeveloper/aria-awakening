_EffectType = "GroundCircleRed1"

-- On Load
RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), 
    function()
        if( this:GetCreationTemplateId() ~= "zone_radius_red" ) then
            _EffectType = "GroundCircleGreen1"
        end

        this:SetObjVar("ZoneEffect", _EffectType)

        FireEngines()
    end
)

function FireEngines()
    local radius = this:GetObjVar("Radius") or 3
    this:PlayEffectWithArgs(_EffectType, 1.0,"Bone=Ground,Radius="..radius)
    
    -- This repeats the effect and causes it to look like it's is pulsating.
    CallFunctionDelayed(TimeSpan.FromMilliseconds(750),function()
        FireEngines()
    end)
end