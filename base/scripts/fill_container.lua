
RegisterSingleEventHandler(EventType.ModuleAttached, "fill_container", 
	function()
		for i,itemInfo in pairs(initializer.Items) do
			local location = itemInfo.Location or GetRandomDropPosition(this)
			if not(itemInfo.Packed) then				
				CreateObjInContainer(itemInfo.Template,this,location,"item_created",itemInfo.StackCount)
			else
				Create.PackedObject.InContainer(itemInfo.Template, this, location)
			end
		end

		CallFunctionDelayed(TimeSpan.FromSeconds(5),function() this:DelModule("fill_container") end)
	end)

RegisterEventHandler(EventType.CreatedObject,"item_created",
	function (success,objRef,stackCount)
		if(success) then
			if(stackCount) then
				RequestSetStack(objRef,stackCount)
			end
			SetItemTooltip(objRef)
		end
	end)