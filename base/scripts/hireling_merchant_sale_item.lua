
MERCHANT_CONSIGNMENT_PERCENT = 10

function OnLoad()
	if (this:GetSharedObjectProperty("DenyPickup") == false) then
		this:SetSharedObjectProperty("DenyPickup", true)
		local itemTemplate = this:GetCreationTemplateId()
		local originalWeight = this:GetObjVar("OriginalWeight")
		if (originalWeight ~= nil) then
			this:SetSharedObjectProperty("Weight",originalWeight)
		end
	end
end

local _merchantOwner
function GetMerchantOwner()
	if ( _merchantOwner == nil ) then
		_merchantOwner = this:GetObjVar("merchantOwner")
	end
	return _merchantOwner
end

local _merchantPlot
function GetMerchantPlot(merchantOwner)
	if ( _merchantPlot == nil and merchantOwner ~= nil and merchantOwner:IsValid() ) then
		_merchantPlot = merchantOwner:GetObjVar("PlotController")
	end
	return _merchantPlot
end

function IsItemOwner(user)
	local merchant = GetMerchantOwner()
	if ( merchant ) then
		local plot = GetMerchantPlot(merchant)
		if ( plot ) then
			return Plot.IsOwner(user, plot)
		end
	end
	return false
end

function RemoveFromSale(buyer, toBank)
	
	this:DelObjVar("OriginalWeight")
	
	this:DelObjVar("itemOwner")
	this:DelObjVar("itemPrice")
	this:SetSharedObjectProperty("DenyPickup", false)
	this:DelObjVar("LockedDown")
	--SCAN MOVED THESE HERE
	this:DelObjVar("merchantOwner")
	this:DelModule("hireling_merchant_sale_item")

	local merchant = GetMerchantOwner()
	if( merchant ) then
		local merchantSaleItem = merchant:GetObjVar("merchantSaleItem")
		if (merchant:GetObjVar("merchantSaleItem") ~= nil) then
			_merchantPlot = GetMerchantPlot(_merchantOwner)
			local saleCount = 1

			for key,value in pairs(merchantSaleItem) do
				if (value == this) then
					table.remove(merchantSaleItem, key)
				else
					if( value:TopmostContainer() == value or value:TopmostContainer() == nil ) then
						saleCount = saleCount + 1
					end
				end
			end
			merchant:SetObjVar("merchantSaleItem",merchantSaleItem)
			_merchantPlot:SetObjVar( "SaleCount", saleCount )
		end
	else
		LuaDebugCallStack("Tried to remove item ["..tostring(this.Id).."] from sale. But merchant is NIL!")
	end
	

	RemoveTooltipEntry(this,"item_price")

	RemoveUseCase(this,"Buy")
	ClearOverrideDefaultInteraction(this)
	--DebugMessage("Yes, exactly what you think is happening is happening.")
	RemoveUseCase(this,"Remove from Sale")	
	this:DelObjVar("NoReset")

	local stackCount = this:GetObjVar("StackCount") or 1
	if ( stackCount > 1 ) then
		AddUseCase(this,"Split Stack", false)
	end

	if(buyer ~= nil) then
		local backpackObj = buyer:GetEquippedObject("Backpack")
		local bankObj = buyer:GetEquippedObject("Bank")
		
		if( toBank and bankObj ) then

			local dropPos = GetRandomDropPosition(bankObj)
	        local success,reason = TryPutObjectInContainer(this, bankObj, dropPos, true)

		elseif(backpackObj) then
			-- See if adding this item will put the player overweight, if so warn them!
			local canAdd,weightCont,maxWeight = CanAddWeightToContainer(backpackObj,this:GetSharedObjectProperty("Weight") or -1)
			if not (canAdd) then
				buyer:SystemMessage("You purchase "..this:GetName().." and are [FF0000]overweight[-]!", 'info')
				buyer:SystemMessage("You purchase "..this:GetName().." and are [FF0000]overweight[-]!")
			end

			-- Place the item in their container, regardless of the weight limit of the backpack.
	        local dropPos = GetRandomDropPosition(backpackObj)
	        local success,reason = TryPutObjectInContainer(this, backpackObj, dropPos, true)

			-- The below won't be triggered as TryPutObjectInContainer() is being told to ignore weight
			--[[
			if not(success) then
	            buyer:SystemMessage(reason.." The item has fell to the ground.")
	            this:SetWorldPosition(buyer:GetLoc())
			end
			]]

	    else
	        this:SetWorldPosition(buyer:GetLoc())
	    end
	end

	--SCAN REMOVED CALL TO CLUSTER CONTROLLER FOR BAZAARS
	--local clusterController = GetClusterController()
	--clusterController:SendMessage("DoHttpRequest", ServerSettings.Bazaar.DelURL.."?data="..json.encode( { objectId = this.Id } ), uuid())

	--this:DelObjVar("merchantOwner")
	--this:DelModule("hireling_merchant_sale_item")
end

RegisterEventHandler(EventType.Timer, "check_valid_hireling", 
	function ()
		local merchant = GetMerchantOwner()
		if ( merchant and merchant:IsValid() and merchant:HasObjVar("ShopLocation") ) then
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(math.random(100,200)),"check_valid_hireling")
		else
			RemoveFromSale()
		end
	end)

RegisterEventHandler(EventType.ModuleAttached,"hireling_merchant_sale_item", 
	function()
		if( IsLockedDown(this) ) then ReleaseObject(this) end

		if ( this:DecayScheduled() ) then this:RemoveDecay() end

        this:SetObjVar("NoReset",true)
		this:SetObjVar("LockedDown",true)
		this:SetSharedObjectProperty("DenyPickup", true)

		if ( HasUseCase(this,"Split Stack") ) then
			RemoveUseCase(this,"Split Stack")
		end

		if(initializer.Price ~= nil and initializer.Price > 0 ) then
			this:SetObjVar("itemPrice",initializer.Price)

			local priceStr = ValueToAmountStr(initializer.Price)
			SetTooltipEntry(this,"item_price","Price: "..priceStr.."\n",100)
		end

		_merchantOwner = initializer.Merchant
		this:SetObjVar("merchantOwner", _merchantOwner)
		local merchantSaleItem = _merchantOwner:GetObjVar("merchantSaleItem") or {}
		merchantSaleItem[#merchantSaleItem+1] = this
		_merchantOwner:SetObjVar("merchantSaleItem", merchantSaleItem)

		-- Adjust the SaleCount of the plot up; only if the item is not in a container
		if( this:TopmostContainer() == this or this:TopmostContainer() == nil ) then
			_merchantPlot = GetMerchantPlot(_merchantOwner)
			local saleCount = ( _merchantPlot:GetObjVar("SaleCount") or 0 ) + 1
			_merchantPlot:SetObjVar( "SaleCount", saleCount )
		end
		

		AddUseCase(this,"Buy")
		SetOverrideDefaultInteraction(this,"Buy")
		AddUseCase(this,"Remove from Sale",false,"HasPlotControl") --TODO this should be HasMerchantControl
		BazaarHelper.PutItemInBazaar( this )
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(5), "check_valid_hireling")
	end)

RegisterEventHandler(EventType.Message, "UseObject", 
	function(user,usedType)
		if( user == nil or not(user:IsValid()) or not(user:IsPlayer()) ) then
			return
		end
		--DebugMessage(usedType)
		if ( usedType == "Buy" or usedType == "Use" ) then
			local merchant = GetMerchantOwner()
			if ( merchant ~= nil ) then			
				merchant:SendMessage("SellItem",user,this)
			end
		elseif( usedType == "Remove from Sale" ) then
			if ( IsItemOwner(user) ) then
				RemoveFromSale()
				user:SystemMessage("The item is no longer for sale.","info")
			end
		end
	end)

RegisterEventHandler(EventType.LoadedFromBackup,"", function ()
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(math.random(5,20)),"check_valid_hireling")
	OnLoad()
end)

RegisterEventHandler(EventType.Message, "RemoveFromSale", RemoveFromSale)

RegisterEventHandler(EventType.Message, "BazaarPurchaseItem", 
function ( buyer, purchaseItem, useBackpack )
	--DebugMessage( "PRICE: " .. purchaseItem.objectPrice )

	RegisterSingleEventHandler(EventType.Message, "ConsumeResourceResponse", 
	function(success,transactionId,user)
		if( success ) then
			--DebugMessage("["..purchaseItem.objectId.."] : ConsumeCallback")
			local itemObj = GameObj(tonumber(purchaseItem.objectId))
			local merchant = itemObj:GetObjVar("merchantOwner")

			local bankObj = merchant:GetEquippedObject("Bank")
			if( bankObj ~= nil ) then
				--DebugMessage( "PRICE: " .. purchaseItem.objectPrice )
				local itemEarnings = math.floor(purchaseItem.objectPrice * ((100 - MERCHANT_CONSIGNMENT_PERCENT) / 100))
				--DebugMessage( "Item Earnings: " .. itemEarnings )
				local coinsObj = FindItemInContainerByTemplate(bankObj,"coin_purse")
				if(coinsObj ~= nil) then
					--DebugMessage("Adding to stack")
					RequestAddToStack(coinsObj,itemEarnings)
				else
					--DebugMessage("Creating new stack")
					CreateObjInContainer("coin_purse", bankObj, Loc(0,0,0), "sale_coins",itemEarnings)    
				end
			end

			merchant:SendMessage("ReserveItem", buyer, itemObj)
			purchaseItem.merchantId = merchant.Id
			user:SendMessageGlobal("BazaarPurchaseItem", true, purchaseItem)

		else
			user:SendMessageGlobal("BazaarPurchaseItem", false, purchaseItem)
		end
	end)

	if( useBackpack ) then
		--DebugMessage("["..purchaseItem.objectId.."] : Consume Coins Backpack")
		buyer:SendMessageGlobal( "ConsumeResource", "coins", purchaseItem.objectPrice, "PurchaseBazaarItem", this )
	else
		--DebugMessage("["..purchaseItem.objectId.."] : Consume Coins Bank")
		buyer:SendMessageGlobal( "ConsumeResourceFromBank", "coins", purchaseItem.objectPrice, "PurchaseBazaarItem", this )
	end

end)

RegisterEventHandler(EventType.CreatedObject,"sale_coins",
function(success,objRef,amount)
	if(success and amount > 1) then
		RequestSetStack(objRef,amount)
	end
end)