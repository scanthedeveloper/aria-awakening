require 'base_player_guild'

PossesseeCommandFuncs = {
    -- Possessee Commands

	GuildMessage = function(...)
		Guild.SendMessage(...)
	end,

	GroupMessage = function(...)
		local player = ChangeToPossessor(this)
		GroupSendChat(player,...)
	end,

	MilitiaMessage = function(...)
		local militiaId = Militia.GetId(this)

		if (militiaId == nil) then
			this:SystemMessage("Must be in a Militia to Militia chat.")
			return
		end

		local rankNumber = Militia.GetRankNumber(this)
		if ( rankNumber == nil or type(rankNumber) ~= "number" or rankNumber < ServerSettings.Militia.ChatRankRequired ) then
			this:SystemMessage("Don't meet the rank requirements to use Militia chat.")
			return
		end

		local chatCooldown = this:GetObjVar("MilitiaChatCooldown")
		if ( chatCooldown ~= nil and DateTime.UtcNow < chatCooldown ) then
			local timeLeft = chatCooldown:Subtract(DateTime.UtcNow)
			this:SystemMessage("Must wait "..TimeSpanToWords(timeLeft).." to Militia chat again.")
			return
		end

		local line = CombineArgs(...)

		local encoded = json.encode(line)
		local msgtype = 'militia","militiaid":"' .. militiaId
		this:LogChat(msgtype, encoded)

		Militia.Chat(this, militiaId, line);
	end,

		-- SCAN ADDED WORLD CHAT
		WorldMessage = function(...)  
			if ( this:HasTimer("AntiWorldChatSpam") ) then      
				return            
			end        
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(2), "AntiWorldChatSpam")        
			local onlineUsers = GlobalVarRead("User.Online")        
			local message = CombineArgs(...)        
			if(onlineUsers) then        
				for user,y in pairs(onlineUsers) do                        
					user:SendMessageGlobal("World.Chat", this:GetCharacterName(), tostring(this:GetSharedObjectProperty("Title")), message)                          
				end            
			end
		end,

	Tell = function(userNameOrId,...)
		if( userNameOrId == nil ) then 
			replied = false
			Usage("tell", this) 
			return 
		end

		local line = CombineArgs(...)
		if ( line ~= nil) then

			local player = nil

			if not (IsImmortal(this)) then
				local friendList = this:GetObjVar("FriendList")

				local foundFriend = false

				for i=1, #friendList, 1 do
					if (friendList[i].Friend:GetCharacterName() == userNameOrId) then
						if (foundFriend == false) then
							player = friendList[i].Friend
							foundFriend = true
						else
							this:SystemMessage("You have multiple friends with that name.")
							replied = false
                    		return
                    	end
					end
				end

				--This should only happen when a mortal player replies using /r
				if (player == nil and replied == true) then
					player = GetPlayerByNameOrIdGlobal(userNameOrId)
				end
			else
				player = GetPlayerByNameOrIdGlobal(userNameOrId)
			end

			replied = false

			if( player ~= nil ) then

				local name = player:GetCharacterName() or "Unknown"
				local encoded = json.encode(line)
				local msgtype = 'tell","tellto":"' .. name
        		this:LogChat(msgtype, encoded)
				player:SendMessageGlobal("PrivateMessage",this:GetName(),line,this.Id)
				this:SystemMessage("[E352EA]To "..name..":[-] "..line,"custom")
			end
		end
	end,

	ReplyTell = function(...)
		replied = true
		PossesseeCommandFuncs.Tell(mLastTeller,...)
	end,
	Wiki = function (search)
		if (search) then
			this:SendClientMessage("OpenURL", "https://malevolentmultiverse.atlassian.net/wiki/spaces/MG/overview"..search)
		else
			this:SendClientMessage("OpenURL", "https://malevolentmultiverse.atlassian.net/wiki/spaces/MG/overview")
		end
	end,
	YouTube = function ()
		this:SendClientMessage("OpenURL", "https://www.youtube.com/channel/UCHG96k0l5KWHh_Limn0AOwA")
	end,
	Discord = function ()
		this:SendClientMessage("OpenURL", "https://discord.gg/QveBmF2T8G")
	end,
	SCAN = function ()
		this:SendClientMessage("OpenURL", "https://www.youtube.com/channel/UCHG96k0l5KWHh_Limn0AOwA")
	end,

	AddToChat = function()
        local this = this:GetObjVar("controller") or this
		local player = GetPlayerByNameOrIdGlobal(mLastTeller)

		if (player ~= nil and not FriendInChatChannel(this, player) and FriendValidForChat(this, player)) then
			AddFriendToChatChannel(this, player)
		end
	end,

	EndPossess = function ()
		--if ( HasMobileEffect(this, "Charmed") ) then
			--this:SendMessage(string.format("End%sEffect", "Charmed"))
		--else
			EndPossess(this)
		--end
	end,
}

RegisterCommand{ Command="g", AccessLevel = AccessLevel.Mortal, Func=PossesseeCommandFuncs.GuildMessage, Desc="Sends a guild message" }
RegisterCommand{ Command="group", AccessLevel = AccessLevel.Mortal, Func=PossesseeCommandFuncs.GroupMessage, Desc="Sends a group message", Aliases={"gr", "party", "p"} }
RegisterCommand{ Command="militia", AccessLevel = AccessLevel.Mortal, Func=PossesseeCommandFuncs.MilitiaMessage, Desc="Sends a Militia message", Aliases={"m"} }
RegisterCommand{ Command="tell", AccessLevel = AccessLevel.Mortal, Func=PossesseeCommandFuncs.Tell, Usage="<name|id>", Desc="Send a private message to another player." }
RegisterCommand{ Command="r", AccessLevel = AccessLevel.Mortal, Func=PossesseeCommandFuncs.ReplyTell, Desc="Reply to a direct message." }
RegisterCommand{ Command="addtochat", AccessLevel = AccessLevel.Mortal, Func=PossesseeCommandFuncs.AddToChat, Desc="Add user from recent direct message"}
RegisterCommand{ Command="endpossess", AccessLevel = AccessLevel.Mortal, Func=PossesseeCommandFuncs.EndPossess, Desc="End possession on current mob." }
--SCAN, ADDED
RegisterCommand{ Command="world", AccessLevel = AccessLevel.Mortal, Func=PossesseeCommandFuncs.WorldMessage, Desc="Sends a world message", Aliases={"wd", "yell", "wc",} }