require 'base_player_guild'

mCurrentTab = "Guild"
mSortOrder = {"LastOnline","Rank","Name"}
mSortDirection = {LastOnline=true,Rank=true,Name=true}
mItemsPerPage = 30
mShowOffline = true
mCurPage = 1
mNumPages = 0
mSelectedMemberId = nil
mGuildWindowOpen = false

function GetWarDurationStr(warDuration)
	local durationStr = warDuration.TotalDays .. " days"
	if(warDuration.TotalDays <= 1) then
		if(warDuration.TotalHours <= 1) then
			durationStr = warDuration.TotalMinutes .. " minutes"
		else
			durationStr = warDuration.TotalHours .. " hours"
		end
	end
	return durationStr
end

function ShowMessageEditWindow(messageType)
	TextFieldDialog.Show{
        TargetUser = this,
        DialogId = "Edit Guild " ..messageType,
        Title = "Edit Guild "..messageType,
        Description = "",
        ResponseFunc = function(user,newName)
        	if(newName ~= nil and newName ~= "") then
        		-- dont allow colors in guild names
        		newName = StripColorFromString(newName)
        		
        		-- DAB TODO: VALIDATE GUILD NAME!
        		if (string.len(newName) > 250) then
			 		this:SystemMessage("The message must be less than 250 characters.","info")
			 		return
			 	end

				Guild.SetGuildMessage(this,nil,messageType,newName)
				
				CallFunctionDelayed(TimeSpan.FromSeconds(1),GuildInfo)
	        end
        end
    }
end

function ShowLeaveGuildConfirm()
	local g = GuildHelpers.Get(this)

	if (g == nil) then
		user:SystemMessage("You are not in a guild","info");
		return
	end

	if (this == g.Leader) then
		ClientDialog.Show{
			TargetUser = this,
			DialogId = "Disband Guild Confirm",
		    TitleStr = "Disband Guild",
		    DescStr = "Are you sure you wish to disband your guild?",
		    Button1Str = "Confirm",
		    Button2Str = "Cancel",
			ResponseFunc=function(user,buttonId)
				--DebugMessage("Guild: Handle Guild Invite Response");
				if (user == nil) then return end

				if (buttonId == nil) then return end

				-- Handles the invite command of the dynamic window
				if (buttonId == 0) then
					Guild.Disband(this)
					this:CloseDynamicWindow("GuildWindow")
					mGuildWindowOpen = false
					return
				end
			end,
		}
	else
		ClientDialog.Show{
			TargetUser = this,
			DialogId = "Leave Guild Confirm",
		    TitleStr = "Leave Guild",
		    DescStr = "Are you sure you wish to leave your guild?",
		    Button1Str = "Confirm",
		    Button2Str = "Cancel",
			ResponseFunc=function(user,buttonId)
				--DebugMessage("Guild: Handle Guild Invite Response");
				if (user == nil) then return end

				if (buttonId == nil) then return end

				-- Handles the invite command of the dynamic window
				if (buttonId == 0) then
					GuildHelpers.Remove(user, g)
					this:CloseDynamicWindow("GuildWindow")
					mGuildWindowOpen = false
					return
				end
			end,
		}			
	end		
end

function GetRosterList(g)		
	local resultTable = {}
	for mobileId,entry in pairs(g.Members) do
		if(mShowOffline or entry.IsOnline) then
			local arrayEntry = { Id=mobileId, Entry=entry}
			table.insert(resultTable,arrayEntry)
		end
	end

	local totalItems = #resultTable

	table.sort(resultTable,
		function(a,b)
			for i,sortType in pairs(mSortOrder) do
				if(sortType == "Name") then
					local nameA = StripColorFromString(a.Entry.Name:lower())
					local nameB = StripColorFromString(b.Entry.Name:lower())
					if(nameA < nameB) then
						return mSortDirection.Name
					elseif(nameB < nameA) then
						return not(mSortDirection.Name)
					end
				elseif(sortType == "Rank") then
					local rankA = GuildHelpers.GetAccessLevelIndex(a.Entry.AccessLevel)
					local rankB = GuildHelpers.GetAccessLevelIndex(b.Entry.AccessLevel)
					if( rankA < rankB ) then
						return mSortDirection.Rank
					elseif(rankB < rankA ) then
						return not(mSortDirection.Rank)
					end					
				elseif(sortType == "LastOnline") then
					if(a.Entry.IsOnline and not b.Entry.IsOnline) then
						return mSortDirection.LastOnline
					elseif(b.Entry.IsOnline and not a.Entry.IsOnline) then
						return not(mSortDirection.LastOnline)
					end

					if(a.Entry.LastOnline > b.Entry.LastOnline) then
						return mSortDirection.LastOnline
					elseif(b.Entry.LastOnline > a.Entry.LastOnline) then
						return not(mSortDirection.LastOnline)
					end
				end
			end
			-- all things equal
			return true
		end)

	mNumPages = math.ceil(totalItems / mItemsPerPage)
	if(mCurPage > mNumPages) then
		mCurPage = mNumPages
	end

	local startIndex = ((mCurPage - 1) * mItemsPerPage) + 1
	local endIndex = startIndex + mItemsPerPage

	local trimmedTable = {}
	for i=startIndex,endIndex do
		table.insert(trimmedTable,resultTable[i])
	end

	return trimmedTable
end

function GuildInfo()
	local array = {}

	local g = GuildHelpers.Get(this)

	local resultTable = {}

	if (g == nil) then		
		this:SystemMessage("You are not in a guild","info");
		return
	end

	mGuildWindowOpen = true

	-- populate the table with is online
	for id,memberData in pairs(g.Members) do
		if ( GlobalVarReadKey("User.Online", GameObj(id)) ) then
	    	g.Members[id].IsOnline = true
	    end
	end

	-- convert legacy time to datetime
	for id,memberData in pairs(g.Members) do 
		if(type(memberData.LastOnline) == "number") then
			memberData.LastOnline = UnixTimeToDateTime(memberData.LastOnline)
		end
	end

	local newWindow = DynamicWindow("GuildWindow",g.Name,590,480)

	AddTabMenu(newWindow,
	{
        ActiveTab = mCurrentTab, 
        Buttons = {
			{ Text = "Guild" },
			{ Text = "Roster" },
			{ Text = "War" },
        }
    })	

	if(mCurrentTab == "Roster") then		
		newWindow:AddImage(8,52,"BasicWindow_Panel",554,340,"Sliced")
		newWindow:AddImage(10,35,"HeaderRow_Bar",550,21,"Sliced")

		newWindow:AddButton(10,35,"Sort|Name","Name",74,21,"","",false,"Text")
		newWindow:AddButton(272,35,"Sort|LastOnline","Last Online",132,21,"","",false,"Text")
		newWindow:AddButton(402,35,"Sort|Rank","Rank",144,21,"","",false,"Text")		

		local scrollWindow = ScrollWindow(10,60,535,312,24)		

		for i,memberData in pairs(GetRosterList(g)) do
			local isSelected = mSelectedMemberId == memberData.Id

			local labelColor = (memberData.Entry.IsOnline and "") or "[918369]"

			local scrollElement = ScrollElement()

			local buttonState = isSelected and "pressed" or ""
			scrollElement:AddButton(4,0,"Select|"..tostring(memberData.Id),"",522,24,"","",false,"ThinFrameHover",buttonState)

			scrollElement:AddLabel(18,6,labelColor..StripColorFromString(memberData.Entry.Name).."[-]",300,20,18,"left")	

			if(memberData.Entry.IsOnline) then
				scrollElement:AddLabel(330,6,labelColor.."Now[-]",120,20,18,"center")	
			else
				scrollElement:AddLabel(330,6,labelColor..memberData.Entry.LastOnline:ToString("MM/dd/yy").."[-]",120,20,18,"center")					
			end

			scrollElement:AddLabel(466,6,labelColor..memberData.Entry.AccessLevel.."[-]",120,20,18,"center")	

			if(isSelected and this.Id ~= mSelectedMemberId) then
				local buttons = {}

				if(memberData.Entry.IsOnline) then
					table.insert(buttons,"Message")
					table.insert(buttons,"Invite to Group")
				end

				if(Guild.CanKickMembers(this, GameObj(memberData.Id))) then
					table.insert(buttons,"Kick")
				end

				if(Guild.CanBePromotedBy(this,GameObj(memberData.Id))) then
					table.insert(buttons,"Promote")
					table.insert(buttons,"Demote")
				end

				if (GuildHelpers.GetAccessLevel(this, GuildHelpers.Get(this)) == "Guildmaster") then
					table.insert(buttons,"Transfer Guild")
				end

				if(#buttons > 0) then
					table.insert(buttons,"Cancel")

					local frameHeight = #buttons * 30 + 4
					local yVal = 12 - frameHeight/2
					scrollElement:AddImage(530,yVal,"BasicWindow_Panel",130,frameHeight,"Sliced")
					yVal = yVal + 2
					for i,button in pairs(buttons) do
						scrollElement:AddButton(532,yVal,"Action|"..button,button,126,28,"","",false,"")
						yVal = yVal + 30
					end
				end
			end

			scrollWindow:Add(scrollElement)
		end

		newWindow:AddScrollWindow(scrollWindow)

		if(mNumPages > 1 and mCurPage > 1) then
			newWindow:AddButton(204,398,"PrevEnd|","",0,0,"","",false,"PreviousEnd")
			newWindow:AddButton(217,398,"Prev|","",0,0,"","",false,"Previous")
		end

		newWindow:AddLabel(275,395,"Page "..mCurPage.." of "..mNumPages,250,24,20,"center")	

		if(mNumPages > 1 and mCurPage < mNumPages) then
			newWindow:AddButton(327,398,"Next|","",0,0,"","",false,"Next")
			newWindow:AddButton(340,398,"NextEnd|","",0,0,"","",false,"NextEnd")
		end

		local checkedState = mShowOffline and "pressed" or ""

		newWindow:AddButton(10,404,"ShowOffline|","Show Offline Members",110,30,"","",false,"Selection2",checkedState)
		
	elseif (mCurrentTab == "Guild") then
		newWindow:AddImage(8,33,"BasicWindow_Panel",550,174,"Sliced")
		newWindow:AddLabel(20,37+8,"Guild Information",250,40,24,"",false,false,"SpectralSC-SemiBold")		

		local guildInfo = GuildHelpers.GetGuildMessage(this,g,"Info")
		newWindow:AddLabel(25,37+40,guildInfo,500,200,18)		

		newWindow:AddImage(8,33+180,"BasicWindow_Panel",550,174,"Sliced")
		newWindow:AddLabel(20,37+180+8,"Message of the Day",250,40,24,"",false,false,"SpectralSC-SemiBold")		

		local guildMessage = GuildHelpers.GetGuildMessage(this,g,"MOTD")
		newWindow:AddLabel(25,37+180+40,guildMessage,500,200,18)

		if(Guild.HasAccessLevel(this,"Officer",g)) then
			newWindow:AddButton(440,180,"Edit|Info","",120,20,"","",false,"Invisible")
			newWindow:AddLabel(500,184,"[9393b3][Edit Message][9393b3]",120,20,18,"center")	

			newWindow:AddButton(440,180+180,"Edit|MOTD","",120,20,"","",false,"Invisible")
			newWindow:AddLabel(500,184+180,"[9393b3][Edit Message][9393b3]",120,20,18,"center")	
		end
		newWindow:AddButton(20,400,"Leave|","Leave",110,25,"","",false,"")
	elseif (mCurrentTab == "War") then
		local guildWars = GuildHelpers.GetWars(g.Id)
		local activeWars = {}
		local outgoingDecs = {}
		local incomingDecs = {}
		for i,warRecord in pairs(guildWars) do
			if(warRecord.StartDate) then
				table.insert(activeWars,warRecord)
			elseif(warRecord.SourceGuild == g.Id) then
				table.insert(outgoingDecs,warRecord)
			elseif(warRecord.TargetGuild == g.Id and not(warRecord.Declined)) then
				table.insert(incomingDecs,warRecord)
			end
		end

		newWindow:AddImage(8,52,"BasicWindow_Panel",554,100,"Sliced")
		newWindow:AddImage(10,35,"HeaderRow_Bar",550,21,"Sliced")

		newWindow:AddLabel(18,40,"Active Wars",300,20,18,"left")	

		if(#activeWars > 0) then
			local activeWindow = ScrollWindow(10,60,530,86,21)
			for i,activeWar in pairs(activeWars) do
				local warElement = ScrollElement()
				local targetGuild = activeWar.TargetGuild
				if(targetGuild == g.Id) then targetGuild = activeWar.SourceGuild end
				local targetName = GuildHelpers.GetNameByGuildId(targetGuild)

				if(activeWar.SurrenderedBy or DateTime.UtcNow > GuildHelpers.GetWarEnd(activeWar)) then
					targetName = targetName .. " [ENDED]"
				end

				local warElement = ScrollElement()
				warElement:AddButton(4,0,"Active|"..activeWar.SourceGuild.."|"..activeWar.TargetGuild,"",510,20,"","",false,"ThinFrameHover",GetButtonState(curSelection,i))
				warElement:AddLabel(12,4,targetName,270,20,18,"left")	


				if(activeWar.SurrenderedBy) then					
					warElement:AddLabel(290,4,"Surrendered: "..activeWar.SurrenderedOn:ToString().." UTC",300,20,18,"left")
				elseif(DateTime.Compare(DateTime.UtcNow,GuildHelpers.GetWarEnd(activeWar)) > 0) then
					warElement:AddLabel(290,4,"Ended: "..GuildHelpers.GetWarEnd(activeWar):ToString().." UTC",300,20,18,"left")
				else
					warElement:AddLabel(290,4,"Ends: "..GuildHelpers.GetWarEnd(activeWar):ToString().." UTC",270,20,18,"left")	
				end

				activeWindow:Add(warElement)
			end
			newWindow:AddScrollWindow(activeWindow)
		end

		newWindow:AddImage(8,172,"BasicWindow_Panel",554,100,"Sliced")
		newWindow:AddImage(10,155,"HeaderRow_Bar",550,21,"Sliced")

		newWindow:AddLabel(18,160,"Outgoing Declarations",300,20,18,"left")	

        if(#outgoingDecs > 0) then
			local outgoingWindow = ScrollWindow(10,180,530,86,21)
			for i,outgoingDec in pairs(outgoingDecs) do
				local decElement = ScrollElement()
				local targetGuild = outgoingDec.TargetGuild
				local targetName = GuildHelpers.GetNameByGuildId(targetGuild)
				if(outgoingDec.Declined) then
					targetName = targetName .. " [DECLINED]"
				end
			
				local decElement = ScrollElement()
				decElement:AddButton(4,0,"Outgoing|"..outgoingDec.SourceGuild.."|"..outgoingDec.TargetGuild,"",510,20,"","",false,"ThinFrameHover",GetButtonState(curSelection,i))
				decElement:AddLabel(12,4,targetName,270,20,18,"left")	
				decElement:AddLabel(290,4,"Expires: "..outgoingDec.ExpireDate:ToString().." UTC",270,20,18,"left")	

				outgoingWindow:Add(decElement)
			end
			newWindow:AddScrollWindow(outgoingWindow)
		end

		newWindow:AddImage(8,292,"BasicWindow_Panel",554,100,"Sliced")
		newWindow:AddImage(10,275,"HeaderRow_Bar",550,21,"Sliced")

		newWindow:AddLabel(18,280,"Incoming Declarations",300,20,18,"left")	

		if(#incomingDecs > 0) then
			local incomingWindow = ScrollWindow(10,300,530,86,21)
			for i,incomingDec in pairs(incomingDecs) do
				local decElement = ScrollElement()
				local targetGuild = incomingDec.SourceGuild
				local targetName = GuildHelpers.GetNameByGuildId(targetGuild)
			
				local decElement = ScrollElement()
				decElement:AddButton(4,0,"Incoming|"..incomingDec.SourceGuild.."|"..incomingDec.TargetGuild,"",510,20,"","",false,"ThinFrameHover",GetButtonState(curSelection,i))
				decElement:AddLabel(12,4,targetName,270,20,18,"left")	
				decElement:AddLabel(290,4,"Expires: "..incomingDec.ExpireDate:ToString().." UTC",270,20,18,"left")	

				incomingWindow:Add(decElement)
			end
			newWindow:AddScrollWindow(incomingWindow)
		end

		if(Guild.HasAccessLevel(this,"Officer",g)) then
			newWindow:AddButton(20,400,"Declare|","Declare War",140,25,"","",false,"")
		end
	end

	local membersOnline = GuildHelpers.GetOnlineMemberCount(this,g)
	local membersTotal = CountTable(g.Members)
	local membersOnlineStr = tostring(membersOnline) .. "/" .. tostring(membersTotal)
	newWindow:AddLabel(550,408,"Members Online: "..membersOnlineStr,250,40,18,"right")

	this:OpenDynamicWindow(newWindow)
end
RegisterEventHandler(EventType.Message,"UpdateGuildInfo",
	function()
		if(mGuildWindowOpen) then
			GuildInfo()
		end
	end)
RegisterEventHandler(EventType.Message,"OpenGuildInfo",GuildInfo)

RegisterEventHandler(EventType.DynamicWindowResponse,"GuildWindow",
	function ( user, buttonId )
		local result = StringSplit(buttonId,"|")
		local action = result[1]
		local arg = result[2]
		local arg2 = nil
		if(#result > 2) then
			arg2 = result[3]
		end
		--DebugMessage("DynamicWindowResponse",action,arg)

		local newTab = HandleTabMenuResponse(buttonId)
		if(newTab) then
			mCurrentTab = newTab
			GuildInfo()
		elseif(action == "Edit") then
			ShowMessageEditWindow(arg)
		elseif(action == "Leave") then
			ShowLeaveGuildConfirm()
		elseif(action == "ShowOffline") then
			mShowOffline = not(mShowOffline)
			GuildInfo()
		elseif(action == "Next" and mCurPage ~= mNumPages) then
			mCurPage = mCurPage + 1
			GuildInfo()
		elseif(action == "NextEnd" and mCurPage ~= mNumPages) then
			mCurPage = mNumPages
			GuildInfo()
		elseif(action == "Prev" and mCurPage > 1) then
			mCurPage = mCurPage - 1
			GuildInfo()
		elseif(action == "PrevEnd" and mCurPage > 1) then
			mCurPage = 1
			GuildInfo()
		elseif(action == "Sort" ) then
			if(mSortOrder[1] ~= arg) then
				RemoveFromArray(mSortOrder,arg)
				table.insert(mSortOrder,1,arg)
				--DebugMessage(DumpTable(mSortOrder))				
			else
				mSortDirection[arg] = not(mSortDirection[arg])
				--DebugMessage("SortDir",arg,mSortDirection[arg])
			end
			GuildInfo()
		elseif(action == "Select") then
			mSelectedMemberId = tonumber(arg)
			GuildInfo()
		elseif(action == "Declare") then
			ShowCreateWar()
		elseif(action == "Active") then
			ShowActiveWar(arg,arg2)
		elseif(action == "Outgoing") then
			ShowOutgoingDeclaration(arg,arg2)
		elseif(action == "Incoming") then
			ShowIncomingDeclaration(arg,arg2)	
		elseif(action == "Action") then
			if(arg == "Message") then
				this:SendClientMessage("EnterChat","/tell " .. mSelectedMemberId .." ")
			elseif(arg == "Invite to Group") then
				local memberObj = GameObj(mSelectedMemberId)
				local groupId = GetGroupId(this)
				if( groupId ~= nil and GetGroupVar(groupId, "Leader") ~= this ) then
					this:SystemMessage("You are not the leader of your group.", "info")
				else
					GroupInvite(this, memberObj)
				end
			elseif(arg == "Kick") then
				local g = GuildHelpers.Get(this)
				if(g == nil or g.Members[mSelectedMemberId] == nil) then return end

				local kickMember = GameObj(mSelectedMemberId)
				local kickName = g.Members[mSelectedMemberId].Name or "him"
				
				if (Guild.CanKickMembers(this,kickMember)) then
					ClientDialog.Show{
						TargetUser = this,
						DialogId = "Kick Guild Member:"..mSelectedMemberId,
					    TitleStr = "Kick Guild Member",
					    DescStr = "Are you sure you wish to remove "..kickName.." from your guild?",
					    Button1Str = "Confirm",
					    Button2Str = "Cancel",
						ResponseFunc=function(user,buttonId)
							--DebugMessage("Guild: Handle Guild Invite Response");
							if (user == nil) then return end

							if (buttonId == nil) then return end

							-- Handles the invite command of the dynamic window
							if (buttonId == 0) then
								GuildHelpers.Remove(kickMember, g)
								CallFunctionDelayed(TimeSpan.FromSeconds(1),GuildInfo)
							end
						end,
					}
				end
			elseif(arg == "Promote") then
				local memberObj = GameObj(mSelectedMemberId)
				if(Guild.CanBePromotedBy(this,memberObj)) then
					Guild.PromoteMember(memberObj)
					CallFunctionDelayed(TimeSpan.FromSeconds(1),GuildInfo)
				end
			elseif(arg == "Demote") then
				local memberObj = GameObj(mSelectedMemberId)
				if(Guild.CanBePromotedBy(this,memberObj)) then
					Guild.DemoteMember(memberObj)
					CallFunctionDelayed(TimeSpan.FromSeconds(1),GuildInfo)
				end
			elseif(arg == "Transfer Guild") then
				local g = GuildHelpers.Get(this)
				local memberObj = GameObj(mSelectedMemberId)
				if (Guild.CanChangeGuildMaster(memberObj, g)) then
					ClientDialog.Show{
						TargetUser = this,
						DialogId = "Transfer Guild:"..mSelectedMemberId,
					    TitleStr = "Transfer Guild",
					    DescStr = "Are you sure you wish to transfer "..g.Name.." to " ..memberObj:GetName().. "?",
					    Button1Str = "Confirm",
					    Button2Str = "Cancel",
						ResponseFunc=function(user,buttonId)
							if (user == nil) then return end
							if (buttonId == nil) then return end
							if (buttonId == 0) then
								Guild.ChangeGuildMaster(memberObj)
								CallFunctionDelayed(TimeSpan.FromSeconds(1),GuildInfo)
							end
						end,
					}
				end
			elseif(arg == "Cancel") then
				mSelectedMemberId = nil
				GuildInfo()
			end
		elseif(buttonId == "" or buttonId == nil) then
			mGuildWindowOpen = false
		end
	end)

function ToggleGuildWindow()
	if(mGuildWindowOpen) then
		mGuildWindowOpen = false
		this:CloseDynamicWindow("GuildWindow")
	else
		GuildInfo()
	end
end

--------------------

local guildSearch = ""
local warDuration = ServerSettings.GuildWars.Durations[1]
local guildMatches = nil
local curSelection = nil

function ShowCreateWar()
	local newWindow = DynamicWindow("WarCreate","Declare War",340,370)

	newWindow:AddLabel(8,8,"Enemy Guild:[-]",300,20,18,"left")
	newWindow:AddTextField(8, 30, 276,23, "enemy", guildSearch)	
	newWindow:AddButton(286,28,"Search","",0,0,"","",false,"Search")

	newWindow:AddImage(6,60,"BasicWindow_Panel",306,136,"Sliced")

	if (not(guildMatches) or #guildMatches == 0) then
		newWindow:AddLabel(18,70,"No Matches",300,20,18,"left")
	else
		local searchWindow = ScrollWindow(6,64,290,130,26)
		for i,guildMatch in pairs(guildMatches) do
			local matchElement = ScrollElement()
			matchElement:AddButton(4,0,"match|"..tostring(i),"",276,24,"","",false,"ThinFrameHover",GetButtonState(curSelection,i))
			matchElement:AddLabel(12,6,guildMatch.Name,270,20,18,"left")	

			searchWindow:Add(matchElement)
		end
		newWindow:AddScrollWindow(searchWindow)
	end

	newWindow:AddLabel(8,210,"Duration:[-]",300,20,18,"left")
	newWindow:AddButton(20, 230, "duration|1", GetWarDurationStr(ServerSettings.GuildWars.Durations[1]), 100, 23, "", "", false,"Selection",GetButtonState(warDuration,ServerSettings.GuildWars.Durations[1]))
	newWindow:AddButton(120, 230, "duration|2", GetWarDurationStr(ServerSettings.GuildWars.Durations[2]), 100, 23, "", "", false,"Selection",GetButtonState(warDuration,ServerSettings.GuildWars.Durations[2]))
	newWindow:AddButton(220, 230, "duration|3", GetWarDurationStr(ServerSettings.GuildWars.Durations[3]), 100, 23, "", "", false,"Selection",GetButtonState(warDuration,ServerSettings.GuildWars.Durations[3]))

	local buttonState = (curSelection ~= nil and warDuration ~= nil) and "" or "disabled"
	newWindow:AddButton(8,286,"Declare","Issue War Declaration",304,25,"","",true,"",buttonState)
	this:OpenDynamicWindow(newWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"WarCreate",
	function (user,buttonId,fieldData)
		if(buttonId == "Search") then
			if(string.len(fieldData.enemy) < 3) then
				user:SystemMessage("Please enter atleast 3 characters to perform a guild name search.","info")
				return
			end

			guildSearch = fieldData.enemy
			guildMatches = GuildSearchByName(guildSearch) 
			curSelection = nil
			ShowCreateWar()
		elseif(buttonId == "Declare") then
			if(curSelection and guildMatches and curSelection <= #guildMatches) then
				local durationStr = GetWarDurationStr(warDuration)
				local warGuildId = guildMatches[curSelection].Id

				ClientDialog.Show{
					TargetUser = this,
					DialogId = "WarConfirm",
				    TitleStr = "Confirm War Declaration",
				    DescStr = "Are you sure you wish to declare war on ".. guildMatches[curSelection].Name.."? The war will last "..durationStr..".",
				    Button1Str = "Confirm",
				    Button2Str = "Cancel",
					ResponseFunc=function(user,buttonId)
						--DebugMessage("Guild: Handle Guild Invite Response");
						if (user == nil) then return end

						if (buttonId == nil) then return end

						-- Handles the invite command of the dynamic window
						if (buttonId == 0) then
							-- double check user access level (client hack)
							if not(Guild.HasAccessLevel(this,"Officer")) then return end

							local myGuildId = this:GetObjVar("Guild")
							GuildHelpers.CreateWarDeclaration(this,myGuildId,warGuildId,warDuration)
							return
						end
					end,
				}
			end
		else
			local result = StringSplit(buttonId,"|")
			local action = result[1]
			local arg = result[2]
			if(action == "match" and guildMatches) then
				curSelection = tonumber(arg)
				if(curSelection > #guildMatches) then
					curSelection = nil
				else
					guildSearch = guildMatches[curSelection].Name
				end
				ShowCreateWar()
			elseif(action == "duration") then
				local durationIndex = tonumber(arg)
				warDuration = ServerSettings.GuildWars.Durations[durationIndex]
				
				ShowCreateWar()
			end			
		end
	end)

function GuildSearchByName(partialName)
	-- this functino only does this once every 5 minutes
	RefreshGuildList()

	local matches = {}
	for i,record in pairs(AllGuilds) do
		if(record.Name:lower():match(partialName:lower())) then
			table.insert(matches,record)
		end
	end

	return matches
end

function ShowActiveWar(sourceGuild,targetGuild)
	local warRecord = GlobalVarReadKey("GuildWars."..sourceGuild,targetGuild)
	if not(warRecord) then return end

	local myGuildId = this:GetObjVar("Guild")
	local enemyGuildId = warRecord.TargetGuild
	local killCount = warRecord.SourceKills or 0
	local deathCount = warRecord.TargetKills or 0
	if(myGuildId == enemyGuildId) then
		enemyGuildId = warRecord.SourceGuild
		killCount = warRecord.TargetKills or 0
		deathCount = warRecord.SourceKills or 0
	end
	local enemyGuildName = GuildHelpers.GetNameByGuildId(enemyGuildId)
	local warActive = GuildHelpers.IsWarActive(warRecord)

	local newWindow = DynamicWindow("WarDetails","War Details",340,232)

	newWindow:AddLabel(10,8,"Enemy Guild: "..enemyGuildName,300,20,24,"left",false,false,"bold")	
	newWindow:AddImage(8,35,"BasicWindow_Panel",302,110,"Sliced")
	
	local durationStr = GetWarDurationStr(warRecord.Duration)
	newWindow:AddLabel(18,46,"Duration: "..durationStr,300,20,18,"left")	
	newWindow:AddLabel(18,70,"Kills: "..killCount,300,20,18,"left")
	newWindow:AddLabel(18,94,"Deaths: "..deathCount,300,20,18,"left")
	if(warRecord.SurrenderedBy) then
		local surrenderedName = enemyGuildName
		if(warRecord.SurrenderedBy ~= enemyGuildId) then
			surrenderedName = GuildHelpers.GetNameByGuildId(myGuildId)
		end
		newWindow:AddLabel(18,118,"War Surrendered By: "..surrenderedName,300,20,18,"left")
	elseif not(warActive) then
		newWindow:AddLabel(18,118,"War Ended: "..GuildHelpers.GetWarEnd(warRecord):ToString().." UTC",300,20,18,"left")
	else
		newWindow:AddLabel(18,118,"War Ends: "..GuildHelpers.GetWarEnd(warRecord):ToString().." UTC",300,20,18,"left")
	end

	local buttonState = warActive and "" or "disabled"	
	newWindow:AddButton(8,148,"Surrender|"..sourceGuild.."|"..targetGuild,"Surrender",304,25,"","",true,"",buttonState)
	this:OpenDynamicWindow(newWindow)	
end

function ShowOutgoingDeclaration(sourceGuild,targetGuild)
	local warRecord = GlobalVarReadKey("GuildWars."..sourceGuild,targetGuild)
	if not(warRecord) then return end

	local enemyGuildId = warRecord.TargetGuild

	local newWindow = DynamicWindow("WarDetails","Outgoing War Declaration",340,190)

	local enemyGuildName = GuildHelpers.GetNameByGuildId(enemyGuildId)
	newWindow:AddLabel(10,8,"Enemy Guild: "..enemyGuildName,300,20,24,"left",false,false,"bold")	
	newWindow:AddImage(8,35,"BasicWindow_Panel",302,58,"Sliced")

	local durationStr = GetWarDurationStr(warRecord.Duration)
	newWindow:AddLabel(18,46,"Duration: "..durationStr,300,20,18,"left")
	newWindow:AddLabel(18,70,"Declaration Expires: "..warRecord.ExpireDate:ToString(),300,20,18,"left")
	
	newWindow:AddButton(8,106,"Revoke|"..sourceGuild.."|"..targetGuild,"Revoke War Declaration",304,25,"","",true,"")
	this:OpenDynamicWindow(newWindow)	
end

function ShowIncomingDeclaration(sourceGuild,targetGuild)
	local warRecord = GlobalVarReadKey("GuildWars."..sourceGuild,targetGuild)
	if not(warRecord) then return end

	local enemyGuildId = warRecord.SourceGuild

	local newWindow = DynamicWindow("WarDetails","Incoming War Declaration",340,190)

	local enemyGuildName = GuildHelpers.GetNameByGuildId(enemyGuildId)
	newWindow:AddLabel(10,8,"Enemy Guild: "..enemyGuildName,300,20,24,"left",false,false,"bold")	
	newWindow:AddImage(8,35,"BasicWindow_Panel",302,58,"Sliced")
	
	local durationStr = GetWarDurationStr(warRecord.Duration)
	newWindow:AddLabel(18,46,"Duration: "..durationStr,300,20,18,"left")
	newWindow:AddLabel(18,70,"Declaration Expires: "..warRecord.ExpireDate:ToString(),300,20,18,"left")
	
	newWindow:AddButton(8,106,"Accept|"..sourceGuild.."|"..targetGuild,"Accept",152,25,"","",true,"")
	newWindow:AddButton(160,106,"Decline|"..sourceGuild.."|"..targetGuild,"Decline",152,25,"","",true,"")
	this:OpenDynamicWindow(newWindow)		
end

RegisterEventHandler(EventType.DynamicWindowResponse,"WarDetails",function(user,buttonId)
		if(buttonId == nil or buttonId == "") then return end

		local result = StringSplit(buttonId,"|")
		local action = result[1]
		local sourceGuild = result[2]		
		local targetGuild = result[3]

		local warRecord = GlobalVarReadKey("GuildWars."..sourceGuild,targetGuild)
		local g = GuildHelpers.Get(user)

		if(action == "Revoke") then
			-- protect from players being sneaky
			if(warRecord.StartDate) then return end
			if(g.Id ~= warRecord.SourceGuild) then return end

			if not(Guild.HasAccessLevel(this,"Officer",g)) then
				this:SystemMessage("Only guild officers can revoke war declarations.","info")
				return
			end

			ClientDialog.Show{
				TargetUser = this,
				DialogId = "RevokeWar",
			    TitleStr = "Revoke War Declaration",
			    DescStr = "Are you sure you wish to revoke this guild war declaration?",
			    Button1Str = "Confirm",
			    Button2Str = "Cancel",
				ResponseFunc=function(user,buttonId)
					--DebugMessage("Guild: Handle Guild Invite Response");
					if (user == nil) then return end

					if (buttonId == nil) then return end

					-- Handles the invite command of the dynamic window
					if (buttonId == 0) then
						GuildHelpers.ScrubGuildWar(sourceGuild,targetGuild,nil,function ( ... )
							DebugMessage("[WarDetails] Declaration revoked!")

							local enemyGuild = GetGuildRecord(warRecord.TargetGuild)
							if(enemyGuild) then
								GuildHelpers.SendToAll(nil,g,"Your guild has revoked it's war declaration with "..enemyGuild.Name..".")
								GuildHelpers.SendToAll(nil,enemyGuild,g.Name.." has revoked their war declaration.")
							end

							this:SendMessage("UpdateGuildInfo")
						end)						
					end
				end,
			}	
		elseif(action == "Accept") then
			-- protect from players being sneaky
			if(warRecord.StartDate or warRecord.Declined) then return end
			if(g.Id ~= warRecord.TargetGuild) then return end

			if not(Guild.HasAccessLevel(this,"Officer",g)) then
				this:SystemMessage("Only guild officers can accept war declarations.","info")
				return
			end

			ClientDialog.Show{
				TargetUser = this,
				DialogId = "AcceptWar",
			    TitleStr = "Start War",
			    DescStr = "Are you sure you wish to start this guild war?",
			    Button1Str = "Confirm",
			    Button2Str = "Cancel",
				ResponseFunc=function(user,buttonId)
					--DebugMessage("Guild: Handle Guild Invite Response");
					if (user == nil) then return end

					if (buttonId == nil) then return end

					-- Handles the invite command of the dynamic window
					if (buttonId == 0) then
						GuildHelpers.StartGuildWar(warRecord,g,this)
						return
					end
				end,
			}			

		elseif(action == "Decline") then
			-- protect from players being sneaky
			if(warRecord.StartDate) then return end
			if(g.Id ~= warRecord.TargetGuild) then return end

			if not(Guild.HasAccessLevel(this,"Officer",g)) then
				this:SystemMessage("Only guild officers can decline war declarations.","info")
				return
			end

			ClientDialog.Show{
				TargetUser = this,
				DialogId = "DeclineWar",
			    TitleStr = "Decline War Declaration",
			    DescStr = "Are you sure you wish to decline this guild war declaration?",
			    Button1Str = "Confirm",
			    Button2Str = "Cancel",
				ResponseFunc=function(user,buttonId)
					--DebugMessage("Guild: Handle Guild Invite Response");
					if (user == nil) then return end

					if (buttonId == nil) then return end

					-- Handles the invite command of the dynamic window
					if (buttonId == 0) then
						SetGlobalVar("GuildWars."..sourceGuild,function (warGuildRecord)
							if(warGuildRecord[targetGuild]) then
								warGuildRecord[targetGuild].Declined = true
								return true
							end
						end,function(success)
							if(success) then
								DebugMessage("[WarDetails] Declaration declined!")

								local enemyGuild = GetGuildRecord(sourceGuild)
								if(enemyGuild) then
									GuildHelpers.SendToAll(nil,g,"Your guild has declined to war with "..enemyGuild.Name..".")
									GuildHelpers.SendToAll(nil,enemyGuild,g.Name.." has declined your war declaration.")
								end

								this:SendMessage("UpdateGuildInfo")
							end
						end)
					end
				end,
			}			
		elseif(action == "Surrender") then
			-- protect from players being sneaky
			if not(warRecord.StartDate) then return end
			if(g.Id ~= warRecord.TargetGuild and g.Id ~= warRecord.SourceGuild) then return end

			if not(Guild.HasAccessLevel(this,"Officer",g)) then
				this:SystemMessage("Only guild officers can surrender from wars.","info")
				return
			end

			if not(GuildHelpers.IsWarActive(warRecord)) then 
				this:SystemMessage("This war has already ended.","info")
				return
			end

			local enemyGuildId = warRecord.TargetGuild
			if(g.Id == warRecord.TargetGuild) then
				enemyGuildId = warRecord.SourceGuild
			end

			ClientDialog.Show{
				TargetUser = this,
				DialogId = "Surrender",
			    TitleStr = "Surrender",
			    DescStr = "Are you sure you wish to surrender to ".. GuildHelpers.GetNameByGuildId(enemyGuildId) .."?",
			    Button1Str = "Confirm",
			    Button2Str = "Cancel",
				ResponseFunc=function(user,buttonId)
					--DebugMessage("Guild: Handle Guild Invite Response");
					if (user == nil) then return end

					if (buttonId == nil) then return end

					-- Handles the invite command of the dynamic window
					if (buttonId == 0) then
						-- double check user access level (client hack)
						if not(Guild.HasAccessLevel(this,"Officer",g)) then return end

						SetGlobalVar("GuildWars."..sourceGuild,function (warGuildRecord)
							if(warGuildRecord[targetGuild]) then
								warGuildRecord[targetGuild].SurrenderedBy = g.Id
								warGuildRecord[targetGuild].SurrenderedOn = DateTime.UtcNow
								return true
							end
						end,function(success)
							if(success) then
								GuildHelpers.WarEnded(warRecord)

								this:SendMessage("UpdateGuildInfo") 
							end
						end)
					end
				end,
			}			
		end
	end)
