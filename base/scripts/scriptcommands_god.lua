require 'scriptcommands_UI_createcustom'
require 'scriptcommands_demigod'
require 'scriptcommands_immortal'

function DoShutdown(reason,shouldRestart)	
	local reasonMessage = reason or "Unspecified"
	if(shouldRestart == nil) then shouldRestart = false end	

	ServerBroadcast(reasonMessage,true)
	ShutdownServer(shouldRestart)
end

-- DAB Make sure the shutdown timer is not stuck on a god character
this:RemoveTimer("shutdown")
RegisterEventHandler(EventType.Timer, "shutdown",
	function(reason,shouldRestart)
		DoShutdown(reason,shouldRestart)
	end)

RegisterEventHandler(EventType.Timer,"FrameTimeTimer",
	function()
		local frameTimeWindow = DynamicWindow("FrameTimeWindow","Frame Time",100,100)
		frameTimeWindow:AddLabel(100,30,tostring(DebugGetAvgFrameTime()))
		
		this:OpenDynamicWindow(frameTimeWindow)

		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"FrameTimeTimer")
	end)

RegisterEventHandler(EventType.DynamicWindowResponse,"FrameTimeWindow",
	function()
		this:RemoveTimer("FrameTimeTimer")
	end)

RegisterEventHandler(EventType.DestroyAllComplete,"WorldReset",
	function ()
		DebugMessage("--- (WorldReset) OBJECTS DESTROYED --- LOADING SEEDS ---")
		LoadSeeds()
		ResetPermanentObjectStates()

		local plots = FindObjects(SearchTemplate("plot_controller"))
		for i=1,#plots do
				plots[i]:AddModule("plot_control_world_reset")
			end
	end)
RegisterEventHandler(EventType.ClientTargetGameObjResponse,"TestMobLoot",
	function (target,user)
		if( target ) then
			local lootContainer = user:GetEquippedObject("Backpack")
			if (lootContainer ~= nil) then
				local lootTables = target:GetObjVar("LootTables")
				if(lootTables) then
					target:SetObjVar("lootable", true)
					for i = 1, mobLootTests do
						LootTables.SpawnLoot(lootTables,lootContainer,nil,false,GetGuardProtection(target))
					end
				end
			end
		end
	end)
frameTimes = {}
mobLootTests = 1
startRange = nil
endRange = nil

RegisterEventHandler(EventType.Timer,"RecordFrameTime",
	function ( ... )
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"RecordFrameTime")
		local avgTime = DebugGetAvgFrameTime()
		this:SystemMessage(tostring(#frameTimes)..": "..tostring(string.format("%.8f",avgTime)))
		table.insert(frameTimes,avgTime)
	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "TestHueRange", 
	function(target,user)
		if( target == nil or not target:IsValid() ) then
			return
		end

		local template = target:GetCreationTemplateId()
		--DebugMessage("Template: ".. tostring(template))
		local targetLoc = target:GetLoc()

		startRange = tonumber(startRange)
		endRange = tonumber(endRange)

		if( startRange and startRange > 0 ) then
			-- Create first
			targetLoc.Z = targetLoc.Z+ 0.5
			Create.AtLocWithHue( template, startRange, targetLoc )

			if( endRange and endRange > startRange  ) then
				for i=startRange+1, endRange do 
					targetLoc.Z = targetLoc.Z+ 0.5
					Create.AtLocWithHue( template, i, targetLoc )
				end
			end
		end


	end
)

GodCommandFuncs = {
	
	FrameTime = function ()
		this:FireTimer("FrameTimeTimer")
	end,

	Shutdown = function(timeSecs,...)			
		if( timeSecs ~= nil ) then
			local arg = table.pack(...)
			local reason = "Server is shutting down in "..timeSecs.." seconds."
			if(#arg > 0) then
				reason = ""
				for i = 1,#arg do reason = reason .. tostring(arg[i]) .. " " end
			end
			
			ServerBroadcast(reason,true)
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(tonumber(timeSecs)),"shutdown",reason)
		else
			Usage("shutdown")
		end		
	end,

	Restart = function(timeSecs,...)			
		if( timeSecs ~= nil ) then
			local arg = table.pack(...)
			local reason = "Server is restarting in "..timeSecs.." seconds."
			if(#arg > 0) then
				reason = ""
				for i = 1,#arg do reason = reason .. tostring(arg[i]) .. " " end
			end

			ServerBroadcast(reason,true)
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(tonumber(timeSecs)),"shutdown",reason,true)
		else
			Usage("shutdown")
		end		
	end,

	-- Testing / Debug Commands
	Log = function(...)
		local line = CombineArgs(...)
		if( line == nil ) then Usage("log") return end

		DebugMessage("Player Log:",this,":",line)
	end,

	Backup = function()
		ForceBackup()
	end,

	ResetWorld = function(arg)
		if(arg and arg:match("force")) then		
			DestroyAllObjects(false,"WorldReset")			
		else
			ClientDialog.Show{
			    TargetUser = this,
			    DialogId = "ResetWorldDialog",
			    TitleStr = "Are you sure?",
			    DescStr = "[$2465]",
			    Button1Str = "Yes",
			    Button2Str = "No",
			    ResponseFunc = function ( user, buttonId )
					buttonId = tonumber(buttonId)
					if( buttonId == 0) then				
						DestroyAllObjects(false,"WorldReset")	
					else
						this:SystemMessage("World reset cancelled")
					end
				end
			}
		end
	end,

	DestroyAll = function(arg)
		if(arg and arg:match("force")) then
			DestroyAllObjects(true)
		elseif(arg and arg:match("ignorenoreset")) then
			DestroyAllObjects(false)
		else
			ClientDialog.Show{
			    TargetUser = this,
			    DialogId = "DestroyAllDialog",
			    TitleStr = "WARNING",
			    DescStr = "[$2466]",
			    Button1Str = "Yes",
			    Button2Str = "No",
			    ResponseFunc = function ( user, buttonId )
					buttonId = tonumber(buttonId)
					if( buttonId == 0) then				
						DestroyAllObjects(true)
					else
						this:SystemMessage("Destroy All cancelled")
					end
				end
			}
		end
	end,

	DebugFrameTime = function (arg)
		if( arg == nil ) then Usage("debugframetime") return end

		if(arg == "begin") then
			frameTimeStart = DateTime.Now
			frameTimes = {}
			this:FireTimer("RecordFrameTime")
			this:SystemMessage("Recording average frame time stats.")
		else
			local frameTimeFile = io.open("frametimes.csv","a")
			io.output(frameTimeFile)
			io.write("---------------------------------\n")
			io.write("Begin "..frameTimeStart:ToString().." : End "..DateTime.Now:ToString().."\n")
			io.output(frameTimeFile)
			local totalCumulative = 0
			for i,frameTime in pairs(frameTimes) do
				totalCumulative = totalCumulative + frameTime
				io.write(tostring(i).."\t"..tostring(string.format("%.8f",frameTime)).."\n")
			end			
			io.write("---------------------------------\n")
			io.write("Total Average: "..tostring(string.format("%.8f",totalCumulative/(#frameTimes))).."\n")
			io.write("---------------------------------\n")
			io.close(frameTimeFile)
			this:RemoveTimer("RecordFrameTime")
			this:SystemMessage("Frame time stats written to frametimes.csv.")
		end
	end,

	ToggleLuaProfile = function()
		isEnabled = not GetLuaProfilingEnabled()
		SetLuaProfilingEnabled(isEnabled)
		this:SystemMessage("Lua Profiling "..tostring(isEnabled))
		DebugMessage("Lua Profiling "..tostring(isEnabled))
	end,

	LuaGC = function(arg,arg2,arg3)
		if(arg == nil) then
			Usage("luagc")			
		elseif(arg == 'collect') then
			LuaGCAll("collect")
		elseif(arg == 'count') then			
			LuaGCAll("collect")
			LuaGCAll("collect")
			local vmUsage = tostring(LuaGCAll("memusage"))
			DebugMessage("VM USAGE: "..vmUsage)
			this:SystemMessage("VM USAGE: "..vmUsage)
		elseif(arg == 'stop') then			
			LuaGCAll("stop")
			DebugMessage("WARNING: LUA GC HALTED!")
			this:SystemMessage("WARNING: LUA GC HALTED!")
		elseif(arg == 'restart') then			
			LuaGCAll("restart")
		elseif(arg == 'memusage') then
			local vmUsage = tostring(LuaGCAll("memusage"))
			DebugMessage("VM USAGE W/O GC STEP: "..vmUsage)
			this:SystemMessage("VM USAGE W/O GC STEP: "..vmUsage)
		elseif(arg == 'mb') then
            MemBegin()
        elseif(arg == 'me') then
        	MemEnd()
        elseif(arg == 'write') then
        	if(arg2=='reg') then
        		DebugMessage("DUMPING REGISTRY")
        		dump_registry_to_xml()
        	elseif(arg2=='globals') then
        		DebugMessage("DUMPING GLOBALS")
        		dump_globals_to_xml()
        	elseif(arg2=='object') then
        		DebugMessage("DUMPING OBJECT "..arg3)
        		dump_object_to_xml(GameObj(tonumber(arg3)))
			elseif(arg2=='allobjects') then
        		DebugMessage("DUMPING ALL OBJECTS")
        		dump_all_objects_to_xml()        		
        	end        	
        	DebugMessage("DONE")
		end
	end,

	DoString = function (...)
		local line = CombineArgs(...)
		f = load(line)
		f()
	end,

	CompleteQuest = function ( questName)
		Quests.ForceComplete(this, questName, true)
	end,

	StartQuest = function ( questName, stepIndex)
		Quests.TryStart(this, questName, tonumber(stepIndex), true)
	end,

	ClearQuests = function ()
		if ( this:HasObjVar("Quests") ) then
			local activeQuests = Quests.GetActive(this)
			for questName, quest in pairs(activeQuests) do
				Quests.Fail(this, questName)
			end
			this:DelObjVar("Quests")
			this:SystemMessage("Quest progress cleared.", "info")
		end
	end,

	EmptyBag = function ()
		local backpackObj = this:GetEquippedObject("Backpack")
		if ( not backpackObj ) then return end
		ForEachItemInContainerRecursive(backpackObj,
		function (contObj)
			if ( not contObj:HasObjVar("Blessed") ) then
				contObj:Destroy()
			end
			return true
		end, 10)
	end,

	TestMobLoot = function (tests)
		mobLootTests = tests or 1
		this:RequestClientTargetGameObj(this,"TestMobLoot")
	end,

	TestHueRange = function( rangeStart, rangeEnd ) 
		startRange = rangeStart
		endRange = rangeEnd
		this:RequestClientTargetGameObj(this,"TestHueRange")
	end,

	AccountProps = function (targetId)
		local target = this

		if(targetId) then
			target = GameObj(tonumber(targetId))
		end

		if(target and target:IsValid()) then
			local props = target:GetAllAccountProps()
			if(props) then
				for propName, propValue in pairs(props) do
					this:SystemMessage(propName .. ": " .. propValue)
				end
			end
		end
	end,

	SendToStorage = function (targetId)
		local targetObj = GameObj(tonumber(targetId))
		if(targetObj:IsValid()) then
			local id = uuid()

			local storageItems = this:GetObjVar("GlobalStorageItems") or {}
			table.insert(storageItems,{Id=targetObj, Name = targetObj:GetName()})
			this:SetObjVar("GlobalStorageItems",storageItems)

			SendItemToStorage(id, targetObj)
			
			RegisterSingleEventHandler(EventType.SendItemToStorageResult, id, function (success)
				if(success) then
					this:SystemMessage("Item sent to storage. Use loadfromstorage to retrieve.")
				else
					this:SystemMessage("Item storage failed.")
				end
			end)
		end
	end,

	LoadFromStorage = function (targetId,destContainerId)
		if not(targetId) then
			local storageItems = this:GetObjVar("GlobalStorageItems") or {}
			if(#storageItems == 0) then
				this:SystemMessage("No items in storage!")
			else
				for i,objInfo in pairs(storageItems) do
					this:SystemMessage("Name: "..objInfo.Name..", Id: "..tostring(objInfo.Id))
				end	
			end
		else
			local storageItems = this:GetObjVar("GlobalStorageItems") or {}

			local targetObj = GameObj(tonumber(targetId))
			local destCont = GameObj(tonumber(destContainerId))

			local hasObj = IndexOf(storageItems,targetObj,function(a,b)
				return a.Id == targetObj
			end)

			if(hasObj and destCont:IsValid()) then

				local id = uuid()
				local randomLoc = GetRandomDropPosition(destCont)
				RequestItemFromStorage(id,targetObj,destCont,randomLoc)

				RegisterSingleEventHandler(EventType.RequestItemFromStorageResult, id, function (objRef)
					if(objRef) then
						this:SystemMessage("Item loaded. "..tostring(objRef))		

						local storageItems = this:GetObjVar("GlobalStorageItems") or {}		
						local itemIndex = IndexOf(storageItems,targetObj,function(a,b)
							return a.Id == targetObj
						end)
						if(itemIndex) then
							table.remove(storageItems,itemIndex)
							this:SetObjVar("GlobalStorageItems",storageItems)
						end
					end
				end)
			end
		end
	end,

	ManageStorage = function (userId)
		if not(this:HasModule("stored_item_manager")) then
			this:AddModule("stored_item_manager",{ UserId = userId })
		end
	end,

	BatchBazaar = function( userId )

		local ClusterController = GetClusterController()
		ClusterController:SendMessage( "BatchBazaar" )

	end,

}

RegisterCommand{ Command="shutdown", 				Category = "Dev Power", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.Shutdown, Usage="<delay_seconds> [<reason>]", Desc="[$2504]" }	
RegisterCommand{ Command="restart", 				Category = "Dev Power", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.Restart, Usage="<delay_seconds> [<reason>]", Desc="[$2505]" }	
RegisterCommand{ Command="reloadglobals", 			Category = "Dev Power", AccessLevel = AccessLevel.God, Func=function()ChangeWorld(true) end, Desc="Reload current server instance, only works on Standalone servers." }
RegisterCommand{ Command="batchbazaar", 			Category = "Dev Power", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.BatchBazaar, Desc="Forces all regions to batch their player merchant items to the bazaar service." }



RegisterCommand{ Command="backup", 					Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.Backup, Desc="[DEBUG COMMAND] Force a backup to take place." } 
RegisterCommand{ Command="resetworld", 				Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.ResetWorld, Desc="[$2508]" } 
RegisterCommand{ Command="destroyall", 				Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.DestroyAll, Desc="[$2509]" } 
RegisterCommand{ Command="emptybag", 				Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.EmptyBag, Desc="Destroy all items in your backpack except blessed items." } 
RegisterCommand{ Command="log", 					Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.Log, Usage="<log string>", Desc="[DEBUG COMMAND] Write a line to the lua log" }
RegisterCommand{ Command="frametime", 				Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.FrameTime, Desc="Opens a window that shows the average frame time" }
RegisterCommand{ Command="debugframetime", 			Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.DebugFrameTime, Usage="<begin,end>", Desc="Record average frame time statistics. end writes report to a file" }
RegisterCommand{ Command="toggleluaprofile", 		Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.ToggleLuaProfile, Desc="[DEBUG COMMAND] Toggles lua profiling on and off" }
RegisterCommand{ Command="luagc", 					Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.LuaGC, Usage="<collect|count|memusage|mb|me|write>", Desc="Various lua memory debugging commands." }
RegisterCommand{ Command="dostring", 				Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.DoString, Usage="<lua code>", Desc="[$2514]"}
RegisterCommand{ Command="completequest",			Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.CompleteQuest, Usage="<quest name>", Desc="Completes a quest with the specified name"}
RegisterCommand{ Command="startquest",				Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.StartQuest, Usage="<quest name> [<quest step>]", Desc="Force starts a quest or quest step with the specified name and/or Id."}
RegisterCommand{ Command="clearquests",				Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.ClearQuests, Desc="Clears all quest progress by deleting the Quests objvar."}
RegisterCommand{ Command="testmobloot",				Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.TestMobLoot, Usage="[<number of times>]", Desc="Generates loot in your backpack for targeted mob."}
RegisterCommand{ Command="testhuerange",			Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.TestHueRange, Usage="[<start_hue> <end_hue>]", Desc="Given a range of hues, or a single hue will generate items of the targeted type in the hue ranges."}
RegisterCommand{ Command="accountprops",			Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.AccountProps, Usage="[<target>]", Desc="Gives you all the account properties for a given user by object id (defaults to yourself)."}

RegisterCommand{ Command="sendtostorage",			Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.SendToStorage, Usage="[<target>]", Desc="Sends item to global storage."}
RegisterCommand{ Command="loadfromstorage",			Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.LoadFromStorage, Usage="[<target>]", Desc="Loads an item from global storage."}

RegisterCommand{ Command="managestorage",			Category = "Debug", AccessLevel = AccessLevel.God, Func=GodCommandFuncs.ManageStorage, Usage="<userid>", Desc="View the item storage manager for given user id"}