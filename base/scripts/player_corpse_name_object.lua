
local _corpseId

if ( initializer and initializer.CorpseId ) then
    _corpseId = initializer.CorpseId
    this:SetObjVar("Corpse", _corpseId)
end

if ( _corpseId == nil ) then
    _corpseId = this:GetObjVar("Corpse")
end

local delay = TimeSpan.FromSeconds(1)
function CheckCorpse()
    if ( _corpseId == nil or not GameObj(_corpseId):IsValid() ) then
        this:Destroy()
        return
    end
    this:ScheduleTimerDelay(delay, "CheckCorpse")
end
RegisterEventHandler(EventType.Timer, "CheckCorpse", CheckCorpse)
this:ScheduleTimerDelay(delay, "CheckCorpse")