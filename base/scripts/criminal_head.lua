

if not( this:HasTimer("CriminalHeadExpire") ) then
    this:ScheduleTimerDelay(ServerSettings.Criminal.HeadExpire, "CriminalHeadExpire")
end

RegisterEventHandler(EventType.Timer, "CriminalHeadExpire", function()
    this:SetHue(970)
    this:SetObjVar("ResourceType", "RottenCriminalHead")
    RemoveUseCase(this, "Use")
    SetItemTooltip(this)
    OnNextFrame(function()
        this:DelModule("criminal_head")
    end)
end)