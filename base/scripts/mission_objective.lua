--Module attached by the mission controller to an object
--It's sole purpose is to send ObjectiveCompleted events to it's mission controller
function RegisterMissionHandler()
	local MissionType = GetMissionTable(this:GetObjVar("MissionKey")).MissionType
	if (MissionType == "Boss") then
		--Register mob death event
		RegisterEventHandler(EventType.Message, "HasDiedMessage",
		function(killer)
			local missionSource = this:GetObjVar("MissionSource")
			missionSource:SendMessage("ObjectiveCompleted")
			return
		end)
		return

	elseif (MissionType == "Lair") then
		--Register destroyable_object destruction event
		RegisterEventHandler(EventType.Message, "ObjectDestroyed", 
		function()
			local missionSource = this:GetObjVar("MissionSource")
			missionSource:SendMessage("ObjectiveCompleted")
			return
		end)
		return

	elseif (MissionType == "Tame") then

		--If tamed, add to objective counter
		RegisterEventHandler(EventType.Message, "SetPetOwner", 
			function(newOwner)
				local missionSource = this:GetObjVar("MissionSource")
				local missionOwner = missionSource:GetObjVar("MissionOwner")

				missionSource:PlayObjectSound("event:/magic/air/magic_air_cloack")
				this:SendMessage("ReleasePet")
				this:DelModule("mission_objective")
				PlayEffectAtLoc("BuffEffect_E",this:GetLoc())
				this:SetSharedObjectProperty("Title", "")
				this:Destroy()

				if (newOwner == missionOwner) then
					missionSource:SendMessage("ObjectiveCompleted")
				end
			end)

		--If killed, check if mission can still be completed
		RegisterEventHandler(EventType.Message, "HasDiedMessage",
		function(killer)
			local missionSource = this:GetObjVar("MissionSource")
			missionSource:SendMessage("CheckMissionStatus")
			this:SetSharedObjectProperty("Title", "")
			this:DelModule("mission_objective")
		end)

		return

	elseif (MissionType == "Harvest") then

		--Once harvested, mission is complete
		RegisterEventHandler(EventType.Message, "NodeDepleted", 
			function()
				local missionSource = this:GetObjVar("MissionSource")
				this:DelModule("mission_objective")
				missionSource:SendMessage("ObjectiveCompleted")

			end)
		return

	elseif (MissionType == "Fishing") then
		--Once fished, mission is complete
		RegisterEventHandler(EventType.Message, "NodeDepleted", 
			function()
				local missionSource = this:GetObjVar("MissionSource")
				this:DelModule("mission_objective")
				missionSource:SendMessage("ObjectiveCompleted")

			end)
	return

	end


	DebugMessage("FAILED TO REGISTER MISSON OBJECTIVE OF TYPE: "..MissionType)
end

RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(), RegisterMissionHandler)
RegisterEventHandler(EventType.LoadedFromBackup, "", RegisterMissionHandler)