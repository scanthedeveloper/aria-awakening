MAX_RUNES = 16

RegisterEventHandler(EventType.Message,"HandleDrop", function(user,droppedObj)
	if( droppedObj:GetObjVar("ResourceType") ~= "Rune" ) then
		return
	end

	local destRegionAddress, destination = GetRuneDestination(droppedObj)
	
	if not(destination) then		
		user:SystemMessage("Rune is not marked.","info")
		return
	end

	AddRuneLocationToRunebook(this,droppedObj,destination,destRegionAddress)

	user:SystemMessage("Location added to runebook.","info")

	droppedObj:Destroy()
	UpdateWindow()
end)

function UpdateWindow()
	local owner = this:TopmostContainer()
	if(owner and owner:IsPlayer()) then
		owner:SendMessage("UpdateRunebookWindow")
	end
end