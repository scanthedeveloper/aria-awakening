TemplateDefines = {
	-- DAB COMBAT CHANGES: UPDATE OR REMOVE THESE DEFINES

	LootTable = 
	{
		----------------------------------------------------------------------------------
		-- MONOLITH START
		----------------------------------------------------------------------------------

		MonolithChestReagents = {
			NumItemsMin = 2,
			NumItemsMax = 4,
			LootItems = 
			{									
				{ Weight = 25, Template = "ingredient_moss", Unique = true, StackCountMin = 90, StackCountMax = 150},
				{ Weight = 25, Template = "ingredient_lemongrass", Unique = true, StackCountMin = 90, StackCountMax = 150},
				{ Weight = 25, Template = "ingredient_mushroom", Unique = true, StackCountMin = 90, StackCountMax = 150},
				{ Weight = 25, Template = "ingredient_ginsengroot", Unique = true, StackCountMin = 90, StackCountMax = 150},
				{ Weight = 25, Template = "monolith_schematic_resource", Unique = true, StackCountMin = 1, StackCountMax = 3},
			}
		},

		MonolithChestDecor = {
			NumItemsMin = 0,
			NumItemsMax = 1,
			LootItems = 
			{									
				{ Chance = 0.05, Template = "packed_crystal_floor_lamp_tall", Unique = true },
				{ Chance = 0.05, Template = "packed_crystal_stand_lamp", Unique = true },
				{ Chance = 0.05, Template = "packed_crystal_wall_lantern", Unique = true },
			}
		},

		MonolithChestArtifacts = {
			NumItemsMin = 0,
			NumItemsMax = 1,
			LootItems = 
			{									
				{ Chance = 0.025, Template = "monolith_schematic_leather_chest", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_leather_helm", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_leather_legs", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_plate_chest", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_plate_helm", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_plate_legs", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_robes", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_mask", Unique = true },
			}
		},

		MonolithCultistTrainee = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 2.5, Template = "monolith_orb_trainee", Unique = true },
				{ Chance = 2.5, Template = "monolith_weapon_mace", Unique = true },
			}
		},
		MonolithCultistAdept = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 5, Template = "monolith_orb_adept", Unique = true },
			}
		},
		MonolithSparkmaster = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 3, Template = "monolith_orb_sparkmaster", Unique = true },
			}
		},
		
		MonolithConstruct = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 5, Template = "monolith_schematic_resource", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_leather_chest", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_leather_helm", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_leather_legs", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_plate_chest", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_plate_helm", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_plate_legs", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_robes", Unique = true },
				{ Chance = 0.025, Template = "monolith_schematic_mask", Unique = true },
			}
		 },
		MonolithUnstableConstruct = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 10, Template = "monolith_schematic_resource", Unique = true },
				{ Chance = 0.05, Template = "monolith_schematic_leather_chest", Unique = true },
				{ Chance = 0.05, Template = "monolith_schematic_leather_helm", Unique = true },
				{ Chance = 0.05, Template = "monolith_schematic_leather_legs", Unique = true },
				{ Chance = 0.05, Template = "monolith_schematic_plate_chest", Unique = true },
				{ Chance = 0.05, Template = "monolith_schematic_plate_helm", Unique = true },
				{ Chance = 0.05, Template = "monolith_schematic_plate_legs", Unique = true },
				{ Chance = 0.05, Template = "monolith_schematic_robes", Unique = true },
				{ Chance = 0.05, Template = "monolith_schematic_mask", Unique = true },
			}
		 },
		
		MonolithArcaneSpark = { 
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 3, Template = "monolith_schematic_resource", Unique = true },
			}
		},

		MonolithTankLevel1 = { 
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 2.5, Template = "monolith_weapon_longsword", Unique = true },
				{ Chance = 1, Template = "monolith_mask_taskmaster", Unique = true },
				
			}
		},
		MonolithRangeLevel1 = { 
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 2.5, Template = "monolith_weapon_bonebow", Unique = true },
			}
			
		},
		MonolithMeleeLevel1 = { 
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 2.5, Template = "monolith_weapon_kyrss", Unique = true },
				{ Chance = 2.5, Template = "monolith_weapon_largeaxe", Unique = true },
				{ Chance = 2.5, Template = "monolith_weapon_longsword", Unique = true },
				{ Chance = 2.5, Template = "monolith_weapon_spear", Unique = true },
				{ Chance = 2.5, Template = "monolith_weapon_warhammer", Unique = true },
			}
			
		},
		MonolithHealerLevel1 = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 2.5, Template = "monolith_weapon_staff", Unique = true },
			}
		 },
		MonolithCasterLevel1 = { 
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 1, Template = "monolith_mask_binder", Unique = true },	
				{ Chance = 2.5, Template = "monolith_weapon_staff", Unique = true },
			}
			
		},

		MonolithTankLevel2 = { 
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 5, Template = "monolith_weapon_longsword", Unique = true },
				{ Chance = 1, Template = "monolith_mask_warbringer", Unique = true },	
			}
		},
		MonolithRangeLevel2 = { 
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 5, Template = "monolith_weapon_bonebow", Unique = true },	
			}
			
		},
		MonolithMeleeLevel2 = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 5, Template = "monolith_weapon_kyrss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_largeaxe", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_longsword", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_spear", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_warhammer", Unique = true },
			}			
		 },
		MonolithHealerLevel2 = { 
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 5, Template = "monolith_weapon_staff", Unique = true },
			}
		},
		MonolithCasterLevel2 = { 
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 1, Template = "monolith_mask_echomage", Unique = true },	
				{ Chance = 5, Template = "monolith_weapon_staff", Unique = true },
			}
		},

		-- Bosses
		MonolithBossConductor = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 10, Template = "monolith_mask_conductor", Unique = true },
				{ Chance = 10, Template = "monolith_tool_drum_conductor", Unique = true },
				-- All Bosses
				{ Chance = 5, Template = "monolith_orb_summoner", Unique = true },
				{ Chance = 5, Template = "monolith_orb_occultist", Unique = true },	
				{ Chance = 5, Template = "monolith_weapon_bonebow_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_kryss_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_largeaxe_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_longsword_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_mace_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_spear_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_warhammer_boss", Unique = true },
			}
		},
		MonolithBossDrainedConstruct = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 10, Template = "monolith_weapon_staff_boss", Unique = true },
				{ Chance = 10, Template = "monolith_weapon_wand_boss", Unique = true },
				-- All Bosses
				{ Chance = 5, Template = "monolith_orb_summoner", Unique = true },
				{ Chance = 5, Template = "monolith_orb_occultist", Unique = true },	
				{ Chance = 5, Template = "monolith_weapon_bonebow_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_kryss_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_largeaxe_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_longsword_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_mace_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_spear_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_warhammer_boss", Unique = true },
			}
		},
		MonolithBossSkullcarver = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 10, Template = "monolith_orb_skullcarver", Unique = true },
				{ Chance = 10, Template = "monolith_mask_skullcarver", Unique = true },
				-- All Bosses
				{ Chance = 5, Template = "monolith_orb_summoner", Unique = true },
				{ Chance = 5, Template = "monolith_orb_occultist", Unique = true },	
				{ Chance = 5, Template = "monolith_weapon_bonebow_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_kryss_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_largeaxe_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_longsword_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_mace_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_spear_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_warhammer_boss", Unique = true },
			}
			
		},
		MonolithBossFinkle = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 10, Template = "monolith_schematic_leather_chest", Unique = true },
				{ Chance = 10, Template = "monolith_schematic_leather_helm", Unique = true },
				{ Chance = 10, Template = "monolith_schematic_leather_legs", Unique = true },
				{ Chance = 10, Template = "monolith_schematic_plate_chest", Unique = true },
				{ Chance = 10, Template = "monolith_schematic_plate_helm", Unique = true },
				{ Chance = 10, Template = "monolith_schematic_plate_legs", Unique = true },
				{ Chance = 10, Template = "monolith_schematic_robes", Unique = true },
				{ Chance = 5, Template = "monolith_schematic_mask", Unique = true },
				{ Chance = 10, Template = "monolith_schematic_resource", StackCountMin = 2, StackCountMax = 4, Unique = true },
				-- All Bosses
				{ Chance = 5, Template = "monolith_orb_summoner", Unique = true },
				{ Chance = 5, Template = "monolith_orb_occultist", Unique = true },	
				{ Chance = 5, Template = "monolith_weapon_bonebow_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_kryss_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_largeaxe_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_longsword_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_mace_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_spear_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_warhammer_boss", Unique = true },
			}
		},
		MonolithBossRenmer = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				-- Decor
				{ Chance = 10, Template = "packed_crystal_floor_lamp_tall", Unique = true },
				{ Chance = 10, Template = "packed_crystal_stand_lamp", Unique = true },
				{ Chance = 10, Template = "packed_crystal_wall_lantern", Unique = true },
				{ Chance = 10, Template = "packed_monolith_alter", Unique = true },
				{ Chance = 10, Template = "packed_monolith_brazier", Unique = true },
				{ Chance = 10, Template = "packed_monolith_orrery", Unique = true },
				-- All Bosses
				{ Chance = 5, Template = "monolith_orb_summoner", Unique = true },
				{ Chance = 5, Template = "monolith_orb_occultist", Unique = true },	
				{ Chance = 5, Template = "monolith_weapon_bonebow_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_kryss_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_largeaxe_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_longsword_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_mace_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_spear_boss", Unique = true },
				{ Chance = 5, Template = "monolith_weapon_warhammer_boss", Unique = true },
			}
		},
		MonolithBossWatcher = {
			NumItemsMin = 1,
			NumItemsMax = 1,
	    	LootItems = {
				-- Advanced weapons
				{ Chance = 10, Template = "monolith_weapon_bonebow_watcher", Unique = true },
				{ Chance = 10, Template = "monolith_weapon_kryss_watcher", Unique = true },
				{ Chance = 10, Template = "monolith_weapon_largeaxe_watcher", Unique = true },
				{ Chance = 10, Template = "monolith_weapon_longsword_watcher", Unique = true },
				{ Chance = 10, Template = "monolith_weapon_mace_watcher", Unique = true },
				{ Chance = 10, Template = "monolith_weapon_spear_watcher", Unique = true },
				{ Chance = 10, Template = "monolith_weapon_warhammer_watcher", Unique = true },
				{ Chance = 10, Template = "monolith_weapon_staff_watcher", Unique = true },
				{ Chance = 10, Template = "monolith_weapon_wand_watcher", Unique = true },
				{ Chance = 10, Template = "monolith_orb_watcher", Unique = true },
				-- Decor
				{ Chance = 10, Template = "packed_monolith_alter", Unique = true },
			}
		},

		-- Multiple Mob tables
		MonolithLevelOneAllMobs = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 0.025, Template = "monolith_orb_summoner", Unique = true },	
				{ Chance = 0.025, Template = "monolith_orb_occultist", Unique = true },	
				{ Chance = 0.75, Template = "monolith_schematic_resource", Unique = true },
				{ Chance = 0.05, Template = "packed_crystal_stand_lamp", Unique = true },
			}
		},
		MonolithLevelTwoAllMobs = {
			NumItemsMin = 0,
			NumItemsMax = 1,
	    	LootItems = {
				{ Chance = 0.05, Template = "monolith_orb_summoner", Unique = true },	
				{ Chance = 0.05, Template = "monolith_orb_occultist", Unique = true },	
				{ Chance = 1.5, Template = "monolith_schematic_resource", Unique = true },
				{ Chance = 0.05, Template = "packed_crystal_floor_lamp_tall", Unique = true },
				{ Chance = 0.05, Template = "packed_crystal_wall_lantern", Unique = true },
			}
		},

		----------------------------------------------------------------------------------
		-- MONOLITH END
		----------------------------------------------------------------------------------

		RelicCultist = {
			NumItems = 1,
	    	LootItems = {
				{ Chance = 30, Template = "relic_cultist", Unique = true },
			},
		},
		RelicPirate = {
			NumItems = 1,
	    	LootItems = {
				{ Chance = 30, Template = "relic_pirate", Unique = true },
			},
		},
		RelicOrc = {
			NumItems = 1,
	    	LootItems = {
				{ Chance = 30, Template = "relic_orc", Unique = true },
			},
		},
		RelicUndead = {
			NumItems = 1,
	    	LootItems = {
				{ Chance = 30, Template = "relic_undead", Unique = true },
			},
		},
		RelicGiant = {
			NumItems = 1,
	    	LootItems = {
				{ Chance = 30, Template = "relic_giant", Unique = true },
			},
		},
		RelicOutlaw = {
			NumItems = 1,
	    	LootItems = {
				{ Chance = 30, Template = "relic_outlaw", Unique = true },
			},
		},

		Humanoid = 
		{
			NumItemsMin = 0,
			NumItemsMax = 2,
	    	LootItems = {
    			{ Chance = 1, Template = "torch", Unique = true },
    			{ Chance = 1, Template = "tool_hunting_knife", Unique = true },
    			{ Chance = 1, Template = "item_apple", Unique = true },
    			{ Chance = 1, Template = "item_bread", Unique = true },
    			{ Chance = 1, Template = "item_ale", Unique = true },
    			{ Chance = 1, Template = "potion_lmana", Unique = true },
    			{ Chance = 1, Template = "potion_lstamina", Unique = true },
    			{ Chance = 1, Template = "potion_lheal", Unique = true },
				{ Chance = 1, Template = "potion_lexplosion", Unique = true },
    			{ Chance = 1, Template = "potion_cure", Unique = true },
    			{ Chance = 1, Template = "tool_cookingpot", Unique = true },
    			{ Chance = 1, Template = "tool_hatchet", Unique = true },
    			{ Chance = 1, Template = "tool_mining_pick", Unique = true },
				{ Chance = 1, Template = "tool_fishing_rod", Unique = true },
				{ Chance = 1, Template = "animalparts_blood", Unique = true },
    			{ Chance = 1, Template = "animalparts_blood_beast", Unique = true },
    			{ Chance = 1, Template = "animalparts_blood_vile", Unique = true },
    			{ Chance = 1, Template = "ingredient_giant_mushrooms", Unique = true },
				{ Chance = 1, Template = "ingredient_frayed_scroll", Unique = true },
				{ Chance = 1, Template = "ingredient_fine_scroll", Unique = true },
				{ Chance = 1, Template = "ingredient_ancient_scroll", Unique = true },
				{ Chance = 1, Template = "animalparts_eye_decrepid", Unique = true },
				{ Chance = 1, Template = "animalparts_eye", Unique = true },
				{ Chance = 1, Template = "animalparts_eye_sickly", Unique = true },
				{ Chance = 1, Template = "resource_essence", Unique = true },
				{ Chance = 1, Template = "rune_blank", Unique = true },
    		},
		},

		PotionsPoor = 
		{
			NumItems = 1,
	    	LootItems = {
    			{ Chance = 2, Template = "potion_lmana", Unique = true },
    			{ Chance = 2, Template = "potion_lstamina", Unique = true },
    			{ Chance = 2, Template = "potion_lheal", Unique = true },
    			{ Chance = 2, Template = "potion_cure", Unique = true },
    		},
		},

		Potions = 
		{
			NumItems = 1,
	    	LootItems = {
    			{ Chance = 2, Template = "potion_mana", Unique = true },
    			{ Chance = 2, Template = "potion_stamina", Unique = true },
    			{ Chance = 2, Template = "potion_heal", Unique = true },
    			{ Chance = 2, Template = "potion_cure", Unique = true },
    		},
		},

		PotionsRich = 
		{
			NumItems = 1,
	    	LootItems = {
    			{ Chance = 3, Template = "potion_mana", Unique = true },
    			{ Chance = 3, Template = "potion_stamina", Unique = true },
    			{ Chance = 3, Template = "potion_heal", Unique = true },
    			{ Chance = 3, Template = "potion_cure", Unique = true },
    		},
		},

		Containers = {
			NumItems = 1,
	    	LootItems = {
    			{ Chance = 3, Template = "animalparts_blood", Unique = true },
    			{ Chance = 3, Template = "animalparts_blood_beast", Unique = true },
    			{ Chance = 1, Template = "animalparts_blood_vile", Unique = true },
    			{ Chance = 3, Template = "ingredient_giant_mushrooms", Unique = true },
				{ Chance = 3, Template = "ingredient_frayed_scroll", Unique = true },
				{ Chance = 3, Template = "ingredient_fine_scroll", Unique = true },
				{ Chance = 1, Template = "ingredient_ancient_scroll", Unique = true },
				{ Chance = 1, Template = "animalparts_eye_decrepid", Unique = true },
				{ Chance = 3, Template = "animalparts_eye", Unique = true },
				{ Chance = 3, Template = "animalparts_eye_sickly", Unique = true },
				{ Chance = 3, Template = "resource_essence", Unique = true },
				{ Chance = 3, Template = "rune_blank", Unique = true },
			},
    	},

		Average = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.5, Template = "necklace_ruby_flawed", Unique = true },
    			{ Chance = 0.5, Template = "necklace_sapphire_flawed", Unique = true },
    			{ Chance = 0.5, Template = "necklace_topaz_flawed", Unique = true },
    			{ Chance = 0.5, Template = "ring_sapphire_flawed", Unique = true },
    			{ Chance = 0.5, Template = "ring_ruby_flawed", Unique = true },
    			{ Chance = 0.5, Template = "ring_topaz_flawed", Unique = true },

    			{ Chance = 0.25, Template = "necklace_ruby_imperfect", Unique = true },
    			{ Chance = 0.25, Template = "necklace_sapphire_imperfect", Unique = true },
    			{ Chance = 0.25, Template = "necklace_topaz_imperfect", Unique = true },
    			{ Chance = 0.25, Template = "ring_sapphire_imperfect", Unique = true },
    			{ Chance = 0.25, Template = "ring_ruby_imperfect", Unique = true },
    			{ Chance = 0.25, Template = "ring_topaz_imperfect", Unique = true },
			},
		},

		Rich = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 0.25, Template = "necklace_ruby_flawed", Unique = true },
    			{ Chance = 0.25, Template = "necklace_sapphire_flawed", Unique = true },
    			{ Chance = 0.25, Template = "necklace_topaz_flawed", Unique = true },
    			{ Chance = 0.25, Template = "ring_sapphire_flawed", Unique = true },
    			{ Chance = 0.25, Template = "ring_ruby_flawed", Unique = true },
    			{ Chance = 0.25, Template = "ring_topaz_flawed", Unique = true },

    			{ Chance = 0.5, Template = "necklace_ruby_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "necklace_sapphire_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "necklace_topaz_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "ring_sapphire_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "ring_ruby_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "ring_topaz_imperfect", Unique = true },
			},
		},

		FilthyRich = 
		{
			NumItems = 1,

			LootItems = 
			{ 
				{ Chance = 0.25, Template = "necklace_ruby_flawed", Unique = true },
    			{ Chance = 0.25, Template = "necklace_sapphire_flawed", Unique = true },
    			{ Chance = 0.25, Template = "necklace_topaz_flawed", Unique = true },
    			{ Chance = 0.25, Template = "ring_sapphire_flawed", Unique = true },
    			{ Chance = 0.25, Template = "ring_ruby_flawed", Unique = true },
    			{ Chance = 0.25, Template = "ring_topaz_flawed", Unique = true },

    			{ Chance = 0.5, Template = "necklace_ruby_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "necklace_sapphire_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "necklace_topaz_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "ring_sapphire_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "ring_ruby_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "ring_topaz_imperfect", Unique = true },

    			{ Chance = 0.15, Template = "necklace_ruby_perfect", Unique = true },
    			{ Chance = 0.15, Template = "necklace_sapphire_perfect", Unique = true },
    			{ Chance = 0.15, Template = "necklace_topaz_perfect", Unique = true },
    			{ Chance = 0.15, Template = "ring_sapphire_perfect", Unique = true },
    			{ Chance = 0.15, Template = "ring_ruby_perfect", Unique = true },
    			{ Chance = 0.15, Template = "ring_topaz_perfect", Unique = true },
			},
		},

		StupidRich = 
		{
			NumItems = 1,

			LootItems = 
			{ 
				{ Chance = 0.5, Template = "necklace_ruby_flawed", Unique = true },
    			{ Chance = 0.5, Template = "necklace_sapphire_flawed", Unique = true },
    			{ Chance = 0.5, Template = "necklace_topaz_flawed", Unique = true },
    			{ Chance = 0.5, Template = "ring_sapphire_flawed", Unique = true },
    			{ Chance = 0.5, Template = "ring_ruby_flawed", Unique = true },
    			{ Chance = 0.5, Template = "ring_topaz_flawed", Unique = true },

    			{ Chance = 0.5, Template = "necklace_ruby_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "necklace_sapphire_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "necklace_topaz_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "ring_sapphire_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "ring_ruby_imperfect", Unique = true },
    			{ Chance = 0.5, Template = "ring_topaz_imperfect", Unique = true },

    			{ Chance = 0.3, Template = "necklace_ruby_perfect", Unique = true },
    			{ Chance = 0.3, Template = "necklace_sapphire_perfect", Unique = true },
    			{ Chance = 0.3, Template = "necklace_topaz_perfect", Unique = true },
    			{ Chance = 0.3, Template = "ring_sapphire_perfect", Unique = true },
    			{ Chance = 0.3, Template = "ring_ruby_perfect", Unique = true },
    			{ Chance = 0.3, Template = "ring_topaz_perfect", Unique = true },
			},
		},

		LittleBoss = 
		{
			NumItems = 3,
					
			LootItems = 
			{ 
				{ Chance = 25, Template = "necklace_ruby_flawed", Unique = true },
    			{ Chance = 25, Template = "necklace_sapphire_flawed", Unique = true },
    			{ Chance = 25, Template = "necklace_topaz_flawed", Unique = true },
    			{ Chance = 25, Template = "ring_sapphire_flawed", Unique = true },
    			{ Chance = 25, Template = "ring_ruby_flawed", Unique = true },
    			{ Chance = 25, Template = "ring_topaz_flawed", Unique = true },

    			{ Chance = 25, Template = "necklace_ruby_imperfect", Unique = true },
    			{ Chance = 25, Template = "necklace_sapphire_imperfect", Unique = true },
    			{ Chance = 25, Template = "necklace_topaz_imperfect", Unique = true },
    			{ Chance = 25, Template = "ring_sapphire_imperfect", Unique = true },
    			{ Chance = 25, Template = "ring_ruby_imperfect", Unique = true },
    			{ Chance = 25, Template = "ring_topaz_imperfect", Unique = true },

    			{ Chance = 25, Template = "necklace_ruby_perfect", Unique = true },
    			{ Chance = 25, Template = "necklace_sapphire_perfect", Unique = true },
    			{ Chance = 25, Template = "necklace_topaz_perfect", Unique = true },
    			{ Chance = 25, Template = "ring_sapphire_perfect", Unique = true },
    			{ Chance = 25, Template = "ring_ruby_perfect", Unique = true },
    			{ Chance = 25, Template = "ring_topaz_perfect", Unique = true },
			},
		},

		Boss = 
		{
			NumItems = 5,
					
			LootItems = 
			{ 
				{ Chance = 25, Template = "necklace_ruby_flawed", Unique = true },
    			{ Chance = 25, Template = "necklace_sapphire_flawed", Unique = true },
    			{ Chance = 25, Template = "necklace_topaz_flawed", Unique = true },
    			{ Chance = 25, Template = "ring_sapphire_flawed", Unique = true },
    			{ Chance = 25, Template = "ring_ruby_flawed", Unique = true },
    			{ Chance = 25, Template = "ring_topaz_flawed", Unique = true },

    			{ Chance = 25, Template = "necklace_ruby_imperfect", Unique = true },
    			{ Chance = 25, Template = "necklace_sapphire_imperfect", Unique = true },
    			{ Chance = 25, Template = "necklace_topaz_imperfect", Unique = true },
    			{ Chance = 25, Template = "ring_sapphire_imperfect", Unique = true },
    			{ Chance = 25, Template = "ring_ruby_imperfect", Unique = true },
    			{ Chance = 25, Template = "ring_topaz_imperfect", Unique = true },

    			{ Chance = 25, Template = "necklace_ruby_perfect", Unique = true },
    			{ Chance = 25, Template = "necklace_sapphire_perfect", Unique = true },
    			{ Chance = 25, Template = "necklace_topaz_perfect", Unique = true },
    			{ Chance = 25, Template = "ring_sapphire_perfect", Unique = true },
    			{ Chance = 25, Template = "ring_ruby_perfect", Unique = true },
    			{ Chance = 25, Template = "ring_topaz_perfect", Unique = true },
			},
		},

		DeathBoss = 
		{
			NumItems = 5,
					
			LootItems = 
			{ 
				{ Chance = 25, Template = "necklace_ruby_flawed", Unique = true },
    			{ Chance = 25, Template = "necklace_sapphire_flawed", Unique = true },
    			{ Chance = 25, Template = "necklace_topaz_flawed", Unique = true },
    			{ Chance = 25, Template = "ring_sapphire_flawed", Unique = true },
    			{ Chance = 25, Template = "ring_ruby_flawed", Unique = true },
    			{ Chance = 25, Template = "ring_topaz_flawed", Unique = true },

    			{ Chance = 25, Template = "necklace_ruby_imperfect", Unique = true },
    			{ Chance = 25, Template = "necklace_sapphire_imperfect", Unique = true },
    			{ Chance = 25, Template = "necklace_topaz_imperfect", Unique = true },
    			{ Chance = 25, Template = "ring_sapphire_imperfect", Unique = true },
    			{ Chance = 25, Template = "ring_ruby_imperfect", Unique = true },
    			{ Chance = 25, Template = "ring_topaz_imperfect", Unique = true },

    			{ Chance = 25, Template = "necklace_ruby_perfect", Unique = true },
    			{ Chance = 25, Template = "necklace_sapphire_perfect", Unique = true },
    			{ Chance = 25, Template = "necklace_topaz_perfect", Unique = true },
    			{ Chance = 25, Template = "ring_sapphire_perfect", Unique = true },
    			{ Chance = 25, Template = "ring_ruby_perfect", Unique = true },
    			{ Chance = 25, Template = "ring_topaz_perfect", Unique = true },
			},
		},

		DynamicSpawnerEasyBossGear = 
		{
			NumItems = 1,
			LootItems = 
			{
				{ Chance = 10, Template = "mini_artifact_cleaver", Unique = true},
				{ Chance = 10, Template = "mini_artifact_rebel", Unique = true},
				{ Chance = 10, Template = "mini_artifact_barbed_bow", Unique = true},
				{ Chance = 10, Template = "mini_artifact_giants_bone", Unique = true},
				{ Chance = 10, Template = "mini_artifact_attuned_wand", Unique = true},
				{ Chance = 10, Template = "awakening_plants_lottery_box", Unique = true},
			}
		},

		DynamicSpawnerHardBossGear = 
		{
			NumItems = 1,
			LootItems = 
			{
				{ Chance = 10, Template = "lesser_artifact_lightning", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_padded_helm", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_hardened_helm", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_plate_helm", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_attuned_staff", Unique = true},
				{ Chance = 10, Template = "lesser_artifiact_melee_shield", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_short_bow", Unique = true},
				{ Chance = 10, Template = "awakening_plants_lottery_box", Unique = true},

			}
		},

		DynamicSpawnerTundraBoss = 
		{
			NumItems = 1,
			LootItems = 
			{
				{ Chance = 10, Template = "lesser_artifact_ice_battleaxe", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_ice_bow", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_ice_mace", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_ice_spear", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_ice_sword", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_ice_warhammer", Unique = true},
				{ Chance = 10, Template = "lesser_artifact_ice_wand", Unique = true},
				{ Chance = 10, Template = "awakening_plants_lottery_box", Unique = true},
			}
		},

		WorldBossSpider = 
		{
			NumItems = 1,
			LootItems = 
			{

				{ Chance = 10, Template = "artifact_moss_shield_bone", Unique = true},
				{ Chance = 10, Template = "artifact_moss_bow", Unique = true},
				{ Chance = 10, Template = "artifact_moss_magehat", Unique = true},
				{ Chance = 10, Template = "artifact_moss_warhammer", Unique = true},
				{ Chance = 10, Template = "artifact_moss_shield_tear", Unique = true},
				{ Chance = 10, Template = "awakening_plants_lottery_box", Unique = true},
			}
		},

		WorldBossScorpion = 
		{
			NumItems = 1,
			LootItems = 
			{
				{ Chance = 10, Template = "artifact_molten_mace", Unique = true},
				{ Chance = 10, Template = "artifact_molten_helm", Unique = true},
				{ Chance = 10, Template = "artifact_molten_necklace", Unique = true},
				{ Chance = 10, Template = "artifact_molten_ring", Unique = true},
				{ Chance = 10, Template = "artifact_molten_staff", Unique = true},
				{ Chance = 10, Template = "awakening_plants_lottery_box", Unique = true},
			}
		},

		WorldBossWerewolf = 
		{
			NumItems = 1,
			LootItems = 
			{
				{ Chance = 10, Template = "artifact_royal_crook", Unique = true},
				{ Chance = 10, Template = "artifact_royal_drum", Unique = true},
				{ Chance = 10, Template = "artifact_royal_hunting_knife", Unique = true},
				{ Chance = 10, Template = "artifact_royal_quarterstaff", Unique = true},
				{ Chance = 10, Template = "artifact_royal_dagger", Unique = true},
				{ Chance = 10, Template = "awakening_plants_lottery_box", Unique = true},
			}
		},

		DeathBossGear =
		{
			NumItems = 1,
					
			LootItems = 
			{ 
			--{ Chance = 1, Template = "robe_necromancer_tunic", Unique = true},
			--{ Chance = 1, Template = "robe_necromancer_leggings", Unique = true},
			--{ Chance = 1, Template = "robe_necromancer_helm", Unique = true},


			{ Chance = 10, Template = "artifact_angelic", Unique = true},
			{ Chance = 10, Template = "artifact_crescent", Unique = true},
			{ Chance = 10, Template = "artifact_bone_bow", Unique = true},
			{ Chance = 10, Template = "artifact_necromancer", Unique = true},
			{ Chance = 10, Template = "artifact_death", Unique = true},
			{ Chance = 10, Template = "artifact_raiment", Unique = true},
			{ Chance = 10, Template = "artifact_berserker", Unique = true},
			{ Chance = 10, Template = "artifact_savage", Unique = true},

			{ Chance = 0.25, Template = "furniture_throne_reaper", Packed = true, Unique = true},
			{ Chance = 0.25, Template = "furniture_teleporter_catacombs", Packed = true,  Unique = true},
			{ Chance = 0.1, Template = "item_statue_death", Unique = true},
			},
		},

		CerberusBossGear = 
		{
			NumItems = 1,
					
			LootItems = 
			{ 
			{ Chance = 10, Template = "artifact_pyros", Unique = true},
			{ Chance = 10, Template = "artifact_dragon_guard", Unique = true},
			{ Chance = 10, Template = "artifact_silence", Unique = true},
			{ Chance = 10, Template = "artifact_cleric", Unique = true},
			{ Chance = 10, Template = "artifact_ghoul", Unique = true},
			{ Chance = 10, Template = "artifact_horror", Unique = true},
			{ Chance = 10, Template = "artifact_hunter", Unique = true},
			{ Chance = 10, Template = "item_statue_cerberus", Unique = true},
			},
		},

		SpiderPoor = 
		{
			NumItems = 1,
	    	LootItems = {
    			{ Chance = 1, Template = "animalparts_spider_silk", Unique = true },
    		},
		},

		Spider = 
		{
			NumItems = 1,
	    	LootItems = {
    			{ Chance = 10, Template = "animalparts_spider_silk", Unique = true, StackCountMin = 1, StackCountMax = 3 },
    		},
		},

		SpiderRich = 
		{
			NumItems = 1,
	    	LootItems = {
    			{ Chance = 25, Template = "animalparts_spider_silk", Unique = true, StackCountMin = 3, StackCountMax = 6 },
    		},
		},

		JeweleryPoor = 
		{
			NumItems = 1,
	    	LootItems = {
	    		{ Chance = 2, Template = "necklace_ruby_flawed", Unique = true },
    			{ Chance = 2, Template = "necklace_sapphire_flawed", Unique = true },
    			{ Chance = 2, Template = "necklace_topaz_flawed", Unique = true },
    			{ Chance = 2, Template = "ring_sapphire_flawed", Unique = true },
    			{ Chance = 2, Template = "ring_ruby_flawed", Unique = true },
    			{ Chance = 2, Template = "ring_topaz_flawed", Unique = true },
    		},
		},

		Jewelery = 
		{
			NumItems = 1,
	    	LootItems = {
	    		{ Chance = 2, Template = "necklace_ruby_flawed", Unique = true },
    			{ Chance = 2, Template = "necklace_sapphire_flawed", Unique = true },
    			{ Chance = 2, Template = "necklace_topaz_flawed", Unique = true },
    			{ Chance = 2, Template = "ring_sapphire_flawed", Unique = true },
    			{ Chance = 2, Template = "ring_ruby_flawed", Unique = true },
    			{ Chance = 2, Template = "ring_topaz_flawed", Unique = true },

    			{ Chance = 1, Template = "necklace_ruby_imperfect", Unique = true },
    			{ Chance = 1, Template = "necklace_sapphire_imperfect", Unique = true },
    			{ Chance = 1, Template = "necklace_topaz_imperfect", Unique = true },
    			{ Chance = 1, Template = "ring_sapphire_imperfect", Unique = true },
    			{ Chance = 1, Template = "ring_ruby_imperfect", Unique = true },
    			{ Chance = 1, Template = "ring_topaz_imperfect", Unique = true },
    		},
		},

		JeweleryRich = 
		{
			NumItems = 1,
	    	LootItems = {
	    		{ Chance = 5, Template = "necklace_ruby_flawed", Unique = true },
    			{ Chance = 5, Template = "necklace_sapphire_flawed", Unique = true },
    			{ Chance = 5, Template = "necklace_topaz_flawed", Unique = true },
    			{ Chance = 5, Template = "ring_sapphire_flawed", Unique = true },
    			{ Chance = 5, Template = "ring_ruby_flawed", Unique = true },
    			{ Chance = 5, Template = "ring_topaz_flawed", Unique = true },

    			{ Chance = 2, Template = "necklace_ruby_imperfect", Unique = true },
    			{ Chance = 2, Template = "necklace_sapphire_imperfect", Unique = true },
    			{ Chance = 2, Template = "necklace_topaz_imperfect", Unique = true },
    			{ Chance = 2, Template = "ring_sapphire_imperfect", Unique = true },
    			{ Chance = 2, Template = "ring_ruby_imperfect", Unique = true },
    			{ Chance = 2, Template = "ring_topaz_imperfect", Unique = true },

    			{ Chance = 1, Template = "necklace_ruby_perfect", Unique = true },
    			{ Chance = 1, Template = "necklace_sapphire_perfect", Unique = true },
    			{ Chance = 1, Template = "necklace_topaz_perfect", Unique = true },
    			{ Chance = 1, Template = "ring_sapphire_perfect", Unique = true },
    			{ Chance = 1, Template = "ring_ruby_perfect", Unique = true },
    			{ Chance = 1, Template = "ring_topaz_perfect", Unique = true },
    		},
		},

		Bones = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "animalparts_bone", Unique = true, StackCountMin = 1, StackCountMax = 3  },
			},
		},

		BonesCursed = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "animalparts_bone_cursed", Unique = true, StackCountMin = 1, StackCountMax = 3  },
			},
		},

		BonesEthereal = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "animalparts_bone_ethereal", Unique = true, StackCountMin = 1, StackCountMax = 3  },
			},
		},

		EssenceHigh = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "resource_essence", Unique = true, StackCountMin = 3, StackCountMax = 5  },
			},
		},

		Essence = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "resource_essence", Unique = true, StackCountMin = 1, StackCountMax = 3  },
			},
		},

		EssenceLow = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "resource_essence", Unique = true, StackCountMin = 1, StackCountMax = 2  },
			},
		},

		FrayedScrolls = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "ingredient_frayed_scroll", Unique = true, StackCountMin = 1, StackCountMax = 3  },
			},
		},	

		FineScrolls = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "ingredient_fine_scroll", Unique = true, StackCountMin = 1, StackCountMax = 3  },
			},
		},	

		AncientScrolls = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "ingredient_ancient_scroll", Unique = true, StackCountMin = 1, StackCountMax = 3  },
			},
		},	

		DecrepidEye = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "animalparts_eye_decrepid", StackCountMin = 1, StackCountMax = 3  },
			},
		},	

		SicklyEye = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "animalparts_eye_sickly", StackCountMin = 1, StackCountMax = 3  },
			},
		},	

		Eye = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "animalparts_eye", StackCountMin = 1, StackCountMax = 3  },
			},
		},	

		Blood = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "animalparts_blood", StackCountMin = 1, StackCountMax = 3  },
			},
		},	

		BeastBlood = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "animalparts_blood_beast", StackCountMin = 1, StackCountMax = 3  },
			},
		},	

		VileBlood = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 80, Template = "animalparts_blood_vile", StackCountMin = 1, StackCountMax = 3  },
			},
		},	

		ScrollsLow = 
		{
			NumItems = 1,		

			LootItems = 
			{ 
				{ Chance = 0.5, Template = "lscroll_heal", Unique = true },
				{ Chance = 0.5, Template = "lscroll_refresh", Unique = true },
				{ Chance = 0.5, Template = "lscroll_infuse", Unique = true },
				{ Chance = 0.5, Template = "lscroll_cure", Unique = true },
				{ Chance = 0.5, Template = "lscroll_poison", Unique = true },
				{ Chance = 0.5, Template = "lscroll_ruin", Unique = true },
				{ Chance = 0.5, Template = "lscroll_mana_missile", Unique = true },
			},
		},		
		ScrollsMed = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.15, Template = "lscroll_heal", Unique = true },
				{ Chance = 0.15, Template = "lscroll_refresh", Unique = true },
				{ Chance = 0.15, Template = "lscroll_infuse", Unique = true },
				{ Chance = 0.15, Template = "lscroll_cure", Unique = true },
				{ Chance = 0.15, Template = "lscroll_poison", Unique = true },
				{ Chance = 0.15, Template = "lscroll_ruin", Unique = true },
				{ Chance = 0.15, Template = "lscroll_greater_heal", Unique = true },
				{ Chance = 0.15, Template = "lscroll_lightning", Unique = true },
				{ Chance = 0.15, Template = "lscroll_bombardment", Unique = true },
				{ Chance = 0.15, Template = "lscroll_electricbolt", Unique = true },
				{ Chance = 0.15, Template = "lscroll_mark", Unique = true },
				{ Chance = 0.15, Template = "lscroll_frost", Unique = true },
			},
		},
		ScrollsHigh = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 0.1, Template = "lscroll_greater_heal", Unique = true },
				{ Chance = 0.1, Template = "lscroll_lightning", Unique = true },
				{ Chance = 0.1, Template = "lscroll_bombardment", Unique = true },
				{ Chance = 0.1, Template = "lscroll_electricbolt", Unique = true },
				{ Chance = 0.1, Template = "lscroll_mark", Unique = true },
				{ Chance = 0.1, Template = "lscroll_chargerunebook", Unique = true },
				{ Chance = 0.1, Template = "lscroll_etherealize", Unique = true },
				{ Chance = 0.1, Template = "lscroll_frost", Unique = true },
				{ Chance = 0.1, Template = "lscroll_resurrect", Unique = true },
				{ Chance = 0.1, Template = "lscroll_earthquake", Unique = true },
				{ Chance = 0.1, Template = "lscroll_meteor", Unique = true },
				{ Chance = 0.1, Template = "lscroll_portal", Unique = true },
				{ Chance = 0.1, Template = "lscroll_ward_evil", Unique = true },

			},
		},
		IronLow = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 20, Template = "resource_iron", Unique = true, StackCountMin = 1, StackCountMax = 2, },
			},
		},
		IronHigh = 
		{
			NumItems = 1,	

			LootItems = 
			{ 
				{ Chance = 20, Template = "resource_iron", Unique = true, StackCountMin = 2, StackCountMax = 4, },
			},
		},

		MapsLow = 
		{
			NumItems = 1,	

			LootItems = 
			{
				{ Chance = 0.75, Template = "treasure_map"},
				{ Chance = 0.25, Template = "treasure_map_1"},
			},
		},

		Maps = 
		{
			NumItems = 1,	

			LootItems = 
			{
				{ Chance = 1, Template = "treasure_map"},
				{ Chance = 0.5, Template = "treasure_map_1"},
				{ Chance = 0.35, Template = "treasure_map_2"},
				{ Chance = 0.15, Template = "treasure_map_3"},
			},
		},

		MapsHigh = 
		{
			NumItems = 1,	

			LootItems = 
			{
				{ Chance = 2, Template = "treasure_map"},
				{ Chance = 1, Template = "treasure_map_1"},
				{ Chance = 0.5, Template = "treasure_map_2"},
				{ Chance = 0.35, Template = "treasure_map_3"},
				{ Chance = 0.15, Template = "treasure_map_4"},
			},
		},

		MapsBoss = 
		{
			NumItems = 5,	

			LootItems = 
			{
				{ Chance = 10, Template = "treasure_map"},
				{ Chance = 10, Template = "treasure_map_1"},
				{ Chance = 20, Template = "treasure_map_2"},
				{ Chance = 20, Template = "treasure_map_3"},
				{ Chance = 40, Template = "treasure_map_4"},
			},
		},

		ColossalBoss = 
		{
			NumItems = 2,	

			LootItems = 
			{
				{ Weight = 10, Template = "executioner_scroll_animal"},
				{ Weight = 10, Template = "executioner_scroll_arachnid"},
				{ Weight = 10, Template = "executioner_scroll_demon"},
				{ Weight = 10, Template = "executioner_scroll_dragon"},
				{ Weight = 10, Template = "executioner_scroll_ent"},
				{ Weight = 10, Template = "executioner_scroll_giant"},
				{ Weight = 10, Template = "executioner_scroll_humanoid"},
				{ Weight = 10, Template = "executioner_scroll_ork"},
				{ Weight = 10, Template = "executioner_scroll_reptile"},
				{ Weight = 10, Template = "executioner_scroll_undead"},
				
			},
		},

		ScrollsBoss = 
		{
			NumItems = 1,	

			LootItems = 
			{
				{ Chance = 2, Template = "lscroll_electricbolt", Unique = true },
				{ Chance = 2, Template = "lscroll_walloffire", Unique = true },
				{ Chance = 2, Template = "lscroll_teleport", Unique = true },
				{ Chance = 2, Template = "lscroll_resurrect", Unique = true },
				{ Chance = 2, Template = "lscroll_cloak", Unique = true },
				{ Chance = 1, Template = "lscroll_bombardment", Unique = true },
				{ Chance = 1, Template = "lscroll_meteor", Unique = true },
				{ Chance = 1, Template = "lscroll_earthquake", Unique = true },
				{ Chance = 1, Template = "lscroll_ward_evil", Unique = true },
				{ Chance = 1, Template = "lscroll_portal", Unique = true },
				{ Chance = 2, Template = "lscroll_mark", Unique = true },
			},
		},
		MagePoor = 
		{
			NumItemsMin = 1,
			NumItemsMax = 2,	

			LootItems = 
			{ 
				{ Weight = 25, Template = "ingredient_moss", Unique = true, StackCountMin = 1, StackCountMax = 3,},
				{ Weight = 25, Template = "ingredient_lemongrass", Unique = true, StackCountMin = 1, StackCountMax = 3,},
				{ Weight = 25, Template = "ingredient_mushroom", Unique = true, StackCountMin = 1, StackCountMax = 3,},
				{ Weight = 25, Template = "ingredient_ginsengroot", Unique = true, StackCountMin = 1, StackCountMax = 3,},
			},
		},
		MageRich = 
		{
			NumItemsMin = 2,
			NumItemsMax = 3,	

			LootItems = 
			{ 
				{ Weight = 25, Template = "ingredient_moss", Unique = true, StackCountMin = 2, StackCountMax = 5,},
				{ Weight = 25, Template = "ingredient_lemongrass", Unique = true, StackCountMin = 2, StackCountMax = 5,},
				{ Weight = 25, Template = "ingredient_mushroom", Unique = true, StackCountMin = 2, StackCountMax = 5,},
				{ Weight = 25, Template = "ingredient_ginsengroot", Unique = true, StackCountMin = 2, StackCountMax = 5,},
			},
		},
		ArcherPoor = 
		{
			NumItems = 2,	

			LootItems = 
			{ 
				{ Chance = 50, Template = "arrow", Unique = true, StackCountMin = 1, StackCountMax = 10,},
				{ Chance = 2, Template = "weapon_shortbow", Unique = true },
				{ Chance = 2, Template = "weapon_longbow", Unique = true },
			},
		},
		ArcherRich = 
		{
			NumItems = 2,	

			LootItems = 
			{ 
				{ Weight = 50, Template = "arrow", Unique = true, StackCountMin = 10, StackCountMax = 30,},
				{ Chance = 2, Template = "weapon_shortbow", Unique = true },
				{ Chance = 2, Template = "weapon_longbow", Unique = true },
			},
		},
		MageBoss = 
		{
			NumItems = 3,	

			LootItems = 
			{ 
				{ Weight = 25, Template = "ingredient_moss", Unique = true, StackCountMin = 50, StackCountMax = 100,},
				{ Weight = 25, Template = "ingredient_lemongrass", Unique = true, StackCountMin = 50, StackCountMax = 100,},
				{ Weight = 25, Template = "ingredient_mushroom", Unique = true, StackCountMin = 50, StackCountMax = 100,},
				{ Weight = 25, Template = "ingredient_ginsengroot", Unique = true, StackCountMin = 50, StackCountMax = 100,},
			},
		},
		WarriorPoor = 
		{
			NumItems = 2,	

			LootItems = 
			{ 
				{ Chance = 50, Template = "bandage", Unique = true, StackCountMin = 1, StackCountMax = 2, },
				{ Chance = 2, Template = "weapon_dagger", Unique = true },
				{ Chance = 2, Template = "weapon_longsword", Unique = true },
				{ Chance = 2, Template = "weapon_mace", Unique = true },
				{ Chance = 2, Template = "shield_buckler", Unique = true },
				{ Chance = 2, Template = "armor_chain_helm", Unique = true },
				{ Chance = 2, Template = "armor_chain_tunic", Unique = true },
				{ Chance = 2, Template = "armor_chain_leggings", Unique = true },
				{ Chance = 1, Template = "armor_scale_helm", Unique = true },
				{ Chance = 1, Template = "armor_scale_tunic", Unique = true },
				{ Chance = 1, Template = "armor_scale_leggings", Unique = true },
				{ Chance = 0.1, Template = "armor_fullplate_helm", Unique = true },
				{ Chance = 0.1, Template = "armor_fullplate_tunic", Unique = true },
				{ Chance = 0.1, Template = "armor_fullplate_leggings", Unique = true },
			},
		},
		WarriorRich = 
		{
			NumItems = 2,	

			LootItems = 
			{ 
				{ Chance = 50, Template = "bandage", Unique = true, StackCountMin = 1, StackCountMax = 5,},
				{ Chance = 0.5, Template = "weapon_dagger", Unique = true },
				{ Chance = 0.5, Template = "weapon_longsword", Unique = true },
				{ Chance = 0.5, Template = "weapon_mace", Unique = true },
				{ Chance = 0.5, Template = "weapon_shortbow", Unique = true },
				{ Chance = 0.5, Template = "shield_buckler", Unique = true },
				{ Chance = 0.5, Template = "armor_chain_helm", Unique = true },
				{ Chance = 0.5, Template = "armor_chain_tunic", Unique = true },
				{ Chance = 0.5, Template = "armor_chain_leggings", Unique = true },
				{ Chance = 0.3, Template = "armor_scale_helm", Unique = true },
				{ Chance = 0.3, Template = "armor_scale_tunic", Unique = true },
				{ Chance = 0.3, Template = "armor_scale_leggings", Unique = true },
				{ Chance = 0.1, Template = "armor_fullplate_helm", Unique = true },
				{ Chance = 0.1, Template = "armor_fullplate_tunic", Unique = true },
				{ Chance = 0.1, Template = "armor_fullplate_leggings", Unique = true },
			},
		},
		Poor = 
		{
			NumCoinsMin = 1,
			NumCoinsMax = 25,
		},

		AwakeningDragon =
		{
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 2, Template = "prestige_grandmaster_fighter", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_mage", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_rogue", Unique = true },
				{ Chance = 3, Template = "prestige_master_fighter", Unique = true },
				{ Chance = 3, Template = "prestige_master_mage", Unique = true },
				{ Chance = 3, Template = "prestige_master_rogue", Unique = true },

                { Chance = 5, Template = "artifact_frostfang", Unique = true },
                { Chance = 5, Template = "artifact_ignition", Unique = true },
                { Chance = 5, Template = "artifact_lightning", Unique = true },

                { Chance = 10, Template = "dragon_skull", Unique = true },
			}
		},

		AwakeningEnt =
		{
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 2, Template = "prestige_grandmaster_fighter", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_mage", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_rogue", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_bard", Unique = true },
				{ Chance = 3, Template = "prestige_master_fighter", Unique = true },
				{ Chance = 3, Template = "prestige_master_mage", Unique = true },
				{ Chance = 3, Template = "prestige_master_rogue", Unique = true },
				{ Chance = 3, Template = "prestige_master_bard", Unique = true },

                { Chance = 10, Template = "artifact_forest_bow", Unique = true },
                { Chance = 10, Template = "armor_leather_chest_slime", Unique = true },
                { Chance = 10, Template = "armor_leather_leggings_slime", Unique = true },
                { Chance = 10, Template = "armor_leather_helm_slime", Unique = true },

                { Chance = 10, Template = "tree_skull", Unique = true },
			}
		},

		AwakeningAzura =
		{
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 2, Template = "prestige_grandmaster_fighter", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_mage", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_rogue", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_bard", Unique = true },
				{ Chance = 3, Template = "prestige_master_fighter", Unique = true },
				{ Chance = 3, Template = "prestige_master_mage", Unique = true },
				{ Chance = 3, Template = "prestige_master_rogue", Unique = true },
				{ Chance = 3, Template = "prestige_master_bard", Unique = true },

				-- Needs to be all the ice weapons
				{ Chance = 20, Template = "tundra_artifact_battleaxe", Unique = true },
				{ Chance = 20, Template = "tundra_artifact_bow", Unique = true },
				{ Chance = 20, Template = "tundra_artifact_mace", Unique = true },
				{ Chance = 20, Template = "tundra_artifact_spear", Unique = true },
				{ Chance = 20, Template = "tundra_artifact_sword", Unique = true },
				{ Chance = 20, Template = "tundra_artifact_wand", Unique = true },
				{ Chance = 20, Template = "tundra_artifact_warhammer", Unique = true },
			}
		},

		AwakeningKing =
		{
			NumItems = 1,	
			LootItems = 
			{ 
				{ Chance = 2, Template = "prestige_grandmaster_fighter", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_mage", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_rogue", Unique = true },
				{ Chance = 2, Template = "prestige_grandmaster_bard", Unique = true },
				{ Chance = 3, Template = "prestige_master_fighter", Unique = true },
				{ Chance = 3, Template = "prestige_master_mage", Unique = true },
				{ Chance = 3, Template = "prestige_master_rogue", Unique = true },
				{ Chance = 3, Template = "prestige_master_bard", Unique = true },
				

                { Chance = 10, Template = "armor_cultist_king_helm", Unique = true },
                { Chance = 10, Template = "armor_cultist_king_chest", Unique = true },
                { Chance = 10, Template = "armor_cultist_king_leggings", Unique = true },

                { Chance = 10, Template = "cultist_skull", Unique = true },
			}
		},

		TundraWarmaster =
		{
			NumItems = 1,	
			LootItems = 
			{ 
				-- Ice weapons
				{ Chance = 10, Template = "tundra_artifact_battleaxe", Unique = true },
				{ Chance = 30, Template = "tundra_artifact_bow", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_mace", Unique = true },
				{ Chance = 30, Template = "tundra_artifact_spear", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_sword", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_wand", Unique = true },
				{ Chance = 30, Template = "tundra_artifact_warhammer", Unique = true },
			}
		},

		TundraWendigo =
		{
			NumItems = 1,	
			LootItems = 
			{ 
				-- Ice weapons
				{ Chance = 10, Template = "tundra_artifact_battleaxe", Unique = true },
				{ Chance = 30, Template = "tundra_artifact_bow", Unique = true },
				{ Chance = 30, Template = "tundra_artifact_mace", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_spear", Unique = true },
				{ Chance = 30, Template = "tundra_artifact_sword", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_wand", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_warhammer", Unique = true },
			}
		},

		TundraHighPriest =
		{
			NumItems = 1,	
			LootItems = 
			{ 
				-- Ice weapons
				{ Chance = 10, Template = "tundra_artifact_battleaxe", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_bow", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_mace", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_spear", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_sword", Unique = true },
				{ Chance = 30, Template = "tundra_artifact_wand", Unique = true },
				{ Chance = 10, Template = "tundra_artifact_warhammer", Unique = true },
			}
		},

		--SCAN ADDED
		EasterMiniBoss = 
		{
			NumCoinsMin = 1000,
			NumCoinsMax = 3000,
			NumItems = 1,

			LootItems =
			{ 
				{ Chance = 100, Template = "event_currency", Unique = true, StackCountMin = 5, StackCountMax = 10,},
			},
		},

		--SCAN ADDED
		EasterMajorBoss = 
		{
			NumCoinsMin = 10000,
			NumCoinsMax = 30000,
			NumItems = 1,

			LootItems =
			{ 
				{ Chance = 100, Template = "event_currency", Unique = true, StackCountMin = 10, StackCountMax = 20,},
			},
		},

		MetalsmithSkillTier1 = {
			NumItemsMin = 1,
			NumItemsMax = 1,
			LootItems = {
				--nada
			},
		},
		MetalsmithSkillTier2 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 16.67, Template = "essence_sturdythreads_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 16.67, Template = "essence_unyieldingthreads_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 16.67, Template = "essence_resilientthreads_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 16.67, Template = "essence_accuracy_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 16.67, Template = "essence_speed_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 16.67, Template = "essence_vanquishing_lesser", StackCountMin = 3, StackCountMax = 6},
			},
		},
		MetalsmithSkillTier3 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 16.67, Template = "essence_sturdythreads_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 16.67, Template = "essence_unyieldingthreads_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 16.67, Template = "essence_resilientthreads_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 16.67, Template = "essence_accuracy_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 16.67, Template = "essence_speed_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 16.67, Template = "essence_vanquishing_lesser", StackCountMin = 4, StackCountMax = 8},
			},
		},
		MetalsmithSkillTier4 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 16.67, Template = "essence_sturdythreads_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 16.67, Template = "essence_unyieldingthreads_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 16.67, Template = "essence_resilientthreads_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 16.67, Template = "essence_accuracy_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 16.67, Template = "essence_speed_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 16.67, Template = "essence_vanquishing_lesser", StackCountMin = 5, StackCountMax = 10},
			},
		},
		MetalsmithSkillTier5 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 8.34, Template = "essence_sturdythreads_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 8.34, Template = "essence_unyieldingthreads_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 8.34, Template = "essence_resilientthreads_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 8.34, Template = "essence_accuracy_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 8.34, Template = "essence_speed_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 8.34, Template = "essence_vanquishing_lesser", StackCountMin = 6, StackCountMax = 12},

				{ Weight = 8.34, Template = "essence_sturdythreads", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 8.34, Template = "essence_unyieldingthreads", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 8.34, Template = "essence_resilientthreads", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 8.34, Template = "essence_accuracy", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 8.34, Template = "essence_speed", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 8.34, Template = "essence_vanquishing", StackCountMin = 3, StackCountMax = 6},
			},
		},
		MetalsmithSkillTier6 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 16.67, Template = "essence_sturdythreads", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 16.67, Template = "essence_unyieldingthreads", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 16.67, Template = "essence_resilientthreads", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 16.67, Template = "essence_accuracy", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 16.67, Template = "essence_speed", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 16.67, Template = "essence_vanquishing", StackCountMin = 4, StackCountMax = 8},
			},
		},
		MetalsmithSkillTier7 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 10, Template = "furniture_table_blacksmith_gold", Packed = true, Unique = true},--, Packed = true
				{ Weight = 10, Template = "furniture_table_blacksmith_copper", Packed = true, Unique = true},--, Packed = true
				{ Weight = 165, Template = "essence_sturdythreads", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 165, Template = "essence_unyieldingthreads", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 165, Template = "essence_resilientthreads", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 165, Template = "essence_accuracy", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 165, Template = "essence_speed", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 165, Template = "essence_vanquishing", StackCountMin = 5, StackCountMax = 10},

				{ Weight = 8.5, Template = "scroll_artificer_lesser" },
				{ Weight = 2.5, Template = "scroll_artificer" },
				{ Weight = 1.25, Template = "scroll_artificer_greater" },
			},
		},
		MetalsmithSkillTier8 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 10, Template = "furniture_table_blacksmith_cobalt", Packed = true, Unique = true},
				{ Weight = 10, Template = "furniture_table_blacksmith_obsidian", Packed = true, Unique = true},--, Packed = true
				{ Weight = 82.5, Template = "essence_sturdythreads", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 82.5, Template = "essence_unyieldingthreads", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 82.5, Template = "essence_resilientthreads", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 82.5, Template = "essence_accuracy", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 82.5, Template = "essence_speed", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 82.5, Template = "essence_vanquishing", StackCountMin = 6, StackCountMax = 12},
				
				{ Weight = 82.5, Template = "essence_sturdythreads_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 82.5, Template = "essence_unyieldingthreads_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 82.5, Template = "essence_resilientthreads_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 82.5, Template = "essence_accuracy_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 82.5, Template = "essence_speed_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 82.5, Template = "essence_vanquishing_greater", StackCountMin = 3, StackCountMax = 6},
				
				{ Weight = 7.5, Template = "scroll_artificer_lesser" },
				{ Weight = 5, Template = "scroll_artificer" },
				{ Weight = 2.5, Template = "scroll_artificer_greater" },
			},
		},
		MetalsmithSkillTier9 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 10, Template = "tool_anvil_copper", Packed = true, Unique = true},
				{ Weight = 10, Template = "tool_anvil_gold", Packed = true, Unique = true},
				{ Weight = 165, Template = "essence_sturdythreads_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 165, Template = "essence_unyieldingthreads_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 165, Template = "essence_resilientthreads_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 165, Template = "essence_accuracy_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 165, Template = "essence_vanquishing_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 165, Template = "essence_speed_greater", StackCountMin = 4, StackCountMax = 8},
				
				{ Weight = 15, Template = "scroll_artificer_lesser" },
				{ Weight = 10, Template = "scroll_artificer" },
				{ Weight = 5, Template = "scroll_artificer_greater" },
			},
		},
		MetalsmithSkillTier10 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 10, Template = "tool_anvil_cobalt", Packed = true, Unique = true},
				{ Weight = 10, Template = "tool_anvil_obsidian", Packed = true, Unique = true},
				{ Weight = 10, Template = "artisan_crafting_commission_blacksmith", Unique = true},
				{ Weight = 165, Template = "essence_sturdythreads_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 165, Template = "essence_unyieldingthreads_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 165, Template = "essence_resilientthreads_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 165, Template = "essence_accuracy_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 165, Template = "essence_speed_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 165, Template = "essence_vanquishing_greater", StackCountMin = 5, StackCountMax = 10},
				
				{ Weight = 30, Template = "scroll_artificer_lesser" },
				{ Weight = 20, Template = "scroll_artificer" },
				{ Weight = 10, Template = "scroll_artificer_greater" },
			},
		},

		WoodsmithSkillTier1 = {
			NumItemsMin = 1,
			NumItemsMax = 1,
			LootItems = {
				--nada
			},
		},
		WoodsmithSkillTier2 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 20, Template = "essence_ishi_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 20, Template = "essence_roving_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 20, Template = "essence_patience_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 20, Template = "essence_shinytrinket_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 20, Template = "essence_expandingtrinket_lesser", StackCountMin = 3, StackCountMax = 6},
			},
		},
		WoodsmithSkillTier3 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 20, Template = "essence_ishi_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_roving_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_patience_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_shinytrinket_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_expandingtrinket_lesser", StackCountMin = 4, StackCountMax = 8},
			},
		},
		WoodsmithSkillTier4 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 20, Template = "essence_ishi_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 20, Template = "essence_roving_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 20, Template = "essence_patience_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 20, Template = "essence_shinytrinket_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 20, Template = "essence_expandingtrinket_lesser", StackCountMin = 5, StackCountMax = 10},
			},
		},
		WoodsmithSkillTier5 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 10, Template = "essence_ishi_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 10, Template = "essence_roving_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 10, Template = "essence_patience_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 10, Template = "essence_shinytrinket_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 10, Template = "essence_expandingtrinket_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 10, Template = "essence_ishi", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 10, Template = "essence_roving", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 10, Template = "essence_patience", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 10, Template = "essence_shinytrinket", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 10, Template = "essence_expandingtrinket", StackCountMin = 3, StackCountMax = 6},
			},
		},
		WoodsmithSkillTier6 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 20, Template = "essence_ishi", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_roving", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_patience", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_shinytrinket", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_expandingtrinket", StackCountMin = 4, StackCountMax = 8},
			},
		},
		WoodsmithSkillTier7 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 120, Template = "furniture_fabric_rack", Packed = true},
				{ Weight = 120, Template = "furniture_weapon_rack", Packed = true},
				{ Weight = 151, Template = "essence_ishi", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 151, Template = "essence_roving", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 151, Template = "essence_patience", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 151, Template = "essence_shinytrinket", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 151, Template = "essence_expandingtrinket", StackCountMin = 5, StackCountMax = 10},

				{ Weight = 8.5, Template = "scroll_artificer_lesser" },
				{ Weight = 2.5, Template = "scroll_artificer" },
				{ Weight = 1.25, Template = "scroll_artificer_greater" },
			},
		},
		WoodsmithSkillTier8 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 120, Template = "furniture_table_cloth", Packed = true},
				{ Weight = 120, Template = "furniture_ornate_drawer", Packed = true},
				{ Weight = 75, Template = "essence_ishi", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 75, Template = "essence_roving", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 75, Template = "essence_patience", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 75, Template = "essence_shinytrinket", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 75, Template = "essence_expandingtrinket", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 75, Template = "essence_ishi_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 75, Template = "essence_roving_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 75, Template = "essence_patience_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 75, Template = "essence_shinytrinket_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 75, Template = "essence_expandingtrinket_greater", StackCountMin = 3, StackCountMax = 6},

				{ Weight = 7.5, Template = "scroll_artificer_lesser" },
				{ Weight = 5, Template = "scroll_artificer" },
				{ Weight = 2.5, Template = "scroll_artificer_greater" },
			},
		},
		WoodsmithSkillTier9 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 60, Template = "arena_dummy", Packed = true},
				{ Weight = 60, Template = "furniture_tent_market_stall", Packed = true},
				{ Weight = 60, Template = "furniture_market_stall", Packed = true},
				{ Weight = 60, Template = "furniture_square_market_stall", Packed = true},
				{ Weight = 151, Template = "essence_ishi_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 151, Template = "essence_roving_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 151, Template = "essence_patience_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 151, Template = "essence_shinytrinket_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 151, Template = "essence_expandingtrinket_greater", StackCountMin = 4, StackCountMax = 8},

				{ Weight = 15, Template = "scroll_artificer_lesser" },
				{ Weight = 10, Template = "scroll_artificer" },
				{ Weight = 5, Template = "scroll_artificer_greater" },
			},
		},
		WoodsmithSkillTier10 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 24, Template = "furniture_table_library", Packed = true},
				{ Weight = 24, Template = "furniture_stone_table", Packed = true},
				{ Weight = 24, Template = "cellar_chest_tall", Packed = true},
				{ Weight = 24, Template = "furniture_globe", Packed = true},
				{ Weight = 12, Template = "blueprint_magegrandtower"},
				{ Weight = 12, Template = "blueprint_tudorhouseestate"},
				{ Weight = 12, Template = "blueprint_tundra5"},
				{ Weight = 10, Template = "artisan_crafting_commission_carpentry", Unique = true},
				{ Weight = 176, Template = "essence_ishi_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 176, Template = "essence_roving_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 176, Template = "essence_patience_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 176, Template = "essence_shinytrinket_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 176, Template = "essence_expandingtrinket_greater", StackCountMin = 5, StackCountMax = 10},

				{ Weight = 30, Template = "scroll_artificer_lesser" },
				{ Weight = 20, Template = "scroll_artificer" },
				{ Weight = 10, Template = "scroll_artificer_greater" },
			},
		},

		FabricationSkillTier1 = {
			NumItemsMin = 1,
			NumItemsMax = 1,
			LootItems = {
				--nada
			},
		},
		FabricationSkillTier2 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 20, Template = "essence_evasion_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 20, Template = "essence_theurgythreads_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 20, Template = "essence_flowingthreads_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 20, Template = "essence_water_lesser", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 20, Template = "essence_mending_lesser", StackCountMin = 3, StackCountMax = 6},
			},
		},
		FabricationSkillTier3 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 20, Template = "essence_evasion_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_theurgythreads_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_flowingthreads_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_water_lesser", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_mending_lesser", StackCountMin = 4, StackCountMax = 8},
			},
		},
		FabricationSkillTier4 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 20, Template = "essence_evasion_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 20, Template = "essence_theurgythreads_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 20, Template = "essence_flowingthreads_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 20, Template = "essence_water_lesser", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 20, Template = "essence_mending_lesser", StackCountMin = 5, StackCountMax = 10},
			},
		},
		FabricationSkillTier5 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 10, Template = "essence_evasion_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 10, Template = "essence_theurgythreads_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 10, Template = "essence_flowingthreads_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 10, Template = "essence_water_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 10, Template = "essence_mending_lesser", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 10, Template = "essence_evasion", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 10, Template = "essence_theurgythreads", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 10, Template = "essence_flowingthreads", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 10, Template = "essence_water", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 10, Template = "essence_mending", StackCountMin = 3, StackCountMax = 6},
			},
		},
		FabricationSkillTier6 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 20, Template = "essence_evasion", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_theurgythreads", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_flowingthreads", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_water", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 20, Template = "essence_mending", StackCountMin = 4, StackCountMax = 8},
			},
		},
		FabricationSkillTier7 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 120, Template = "clothing_dye_shade"},
				{ Weight = 175, Template = "essence_evasion", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 175, Template = "essence_theurgythreads", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 175, Template = "essence_flowingthreads", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 175, Template = "essence_water", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 175, Template = "essence_mending", StackCountMin = 5, StackCountMax = 10},

				{ Weight = 8.5, Template = "scroll_artificer_lesser" },
				{ Weight = 2.5, Template = "scroll_artificer" },
				{ Weight = 1.25, Template = "scroll_artificer_greater" },
			},
		},
		FabricationSkillTier8 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 120, Template = "clothing_dye_crimson"},
				{ Weight = 88, Template = "essence_evasion", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 88, Template = "essence_theurgythreads", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 88, Template = "essence_flowingthreads", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 88, Template = "essence_water", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 88, Template = "essence_mending", StackCountMin = 6, StackCountMax = 12},
				{ Weight = 88, Template = "essence_evasion_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 88, Template = "essence_theurgythreads_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 88, Template = "essence_flowingthreads_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 88, Template = "essence_water_greater", StackCountMin = 3, StackCountMax = 6},
				{ Weight = 88, Template = "essence_mending_greater", StackCountMin = 3, StackCountMax = 6},

				{ Weight = 7.5, Template = "scroll_artificer_lesser" },
				{ Weight = 5, Template = "scroll_artificer" },
				{ Weight = 2.5, Template = "scroll_artificer_greater" },
			},
		},
		FabricationSkillTier9 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 60, Template = "clothing_dye_vile"},
				{ Weight = 60, Template = "clothing_dye_blaze"},
				{ Weight = 60, Template = "clothing_dye_ice"},
				{ Weight = 163, Template = "essence_evasion_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 163, Template = "essence_theurgythreads_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 163, Template = "essence_flowingthreads_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 163, Template = "essence_water_greater", StackCountMin = 4, StackCountMax = 8},
				{ Weight = 163, Template = "essence_mending_greater", StackCountMin = 4, StackCountMax = 8},

				{ Weight = 15, Template = "scroll_artificer_lesser" },
				{ Weight = 10, Template = "scroll_artificer" },
				{ Weight = 5, Template = "scroll_artificer_greater" },
			},
		},
		FabricationSkillTier10 = {
			NumItemsMin = 1,
			NumItemsMax = 3,
			LootItems = {
				{ Weight = 10, Template = "artisan_crafting_commission_fabrication", Unique = true},
				{ Weight = 197, Template = "essence_evasion_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 197, Template = "essence_theurgythreads_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 197, Template = "essence_flowingthreads_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 197, Template = "essence_water_greater", StackCountMin = 5, StackCountMax = 10},
				{ Weight = 197, Template = "essence_mending_greater", StackCountMin = 5, StackCountMax = 10},

				{ Weight = 30, Template = "scroll_artificer_lesser" },
				{ Weight = 20, Template = "scroll_artificer" },
				{ Weight = 10, Template = "scroll_artificer_greater" },
			},
		},
	},

	GridSpawns = 
	{
		UpperPlains = {
			Easy = { 
				{ Count = 5, TemplateId =  "wolf" }, 
				{ Count = 5, TemplateId =  "black_bear" }, 
				{ Count = 30, TemplateId =  "hind" }, 
				{ Count = 20, TemplateId = "chicken" }, 
				{ Count = 10, TemplateId = "bay_horse" }, 

				{ Count = 20, TemplateId = "plant_moss"},				
				{ Count = 20, TemplateId = "plant_ginseng"},
				{ Count = 20, TemplateId = "plant_lemon_grass" },				
				{ Count = 20, TemplateId =  "plant_mushrooms"},
				{ Count = 20, TemplateId =  "plant_cotton"},
				{ Count = 10,  TemplateId =  "plant_giant_mushrooms"},
				{ Count = 20, TemplateId = "plant_kindling"},

				{ Count = 20,  TemplateId =  "rat_giant"},
			},
			Medium = {
				{ Count = 5, TemplateId =  "wolf" }, 
				{ Count = 7, TemplateId =  "black_bear" }, 
				{ Count = 20, TemplateId =  "hind" }, 
				{ Count = 15, TemplateId = "fox" },
				{ Count = 10, TemplateId = "bay_horse" }, 
				{ Count = 10, TemplateId = "brown_bear" }, 
				{ Count = 5, TemplateId = "wolf_grey" }, 				

				{ Count = 20, TemplateId = "plant_moss"},				
				{ Count = 20, TemplateId = "plant_ginseng"},
				{ Count = 20, TemplateId = "plant_lemon_grass" },				
				{ Count = 20, TemplateId =  "plant_mushrooms"},
				{ Count = 30, TemplateId =  "plant_cotton"},
				{ Count = 10,  TemplateId =  "plant_giant_mushrooms"},		
				{ Count = 20, TemplateId = "plant_kindling"},

				{ Count = 2,  TemplateId =  "bandit"},
				{ Count = 2,  TemplateId =  "bandit_female"},
			},
			Hard = {
				{ Count = 1, TemplateId =  "wolf" }, 
				{ Count = 1, TemplateId =  "black_bear" }, 
				{ Count = 5, TemplateId =  "hind" }, 
				{ Count = 10, TemplateId = "chicken" }, 
				{ Count = 10, TemplateId = "bay_horse" }, 
				{ Count = 5, TemplateId = "brown_bear" }, 
				{ Count = 5, TemplateId = "wolf_grey" },
				{ Count = 5, TemplateId = "grizzly_bear" },
				{ Count = 20, TemplateId = "great_hart" },	

				{ Count = 2,  TemplateId =  "troll"},
				{ Count = 2,  TemplateId =  "spider_large"},

				{ Count = 20, TemplateId = "plant_moss"},				
				{ Count = 20, TemplateId = "plant_ginseng"},
				{ Count = 20, TemplateId = "plant_lemon_grass" },				
				{ Count = 20, TemplateId =  "plant_mushrooms"},
				{ Count = 30, TemplateId =  "plant_cotton"},
				{ Count = 10,  TemplateId =  "plant_giant_mushrooms"},	
				{ Count = 20, TemplateId = "plant_kindling"},					
			},
		},
		SouthernHills = {
			Easy = { 
				{ Count = 25, TemplateId =  "coyote" }, 
				{ Count = 12, TemplateId =  "black_bear" }, 
				{ Count = 30, TemplateId =  "hind" }, 
				{ Count = 20, TemplateId = "turkey" }, 
				{ Count = 10, TemplateId = "chestnut_horse" }, 

				{ Count = 20, TemplateId = "plant_moss"},				
				{ Count = 20, TemplateId = "plant_ginseng"},
				{ Count = 20, TemplateId = "plant_lemon_grass" },				
				{ Count = 20, TemplateId =  "plant_mushrooms"},
				{ Count = 30, TemplateId =  "plant_cotton"},
				{ Count = 10,  TemplateId =  "plant_giant_mushrooms"},
				{ Count = 20, TemplateId = "plant_kindling"},
				
				--[[ --SCAN ADDED FOR EASTER EVENT
				{ Count = 10, TemplateId = "easter_egg_green"},
				{ Count = 10, TemplateId = "easter_egg_blue"},	
				{ Count = 10, TemplateId = "easter_egg_purple"},	
				{ Count = 10, TemplateId = "easter_egg_pink"},	
				{ Count = 10, TemplateId = "easter_egg_red"},	
				{ Count = 10, TemplateId = "easter_egg_yellow"},
				]]

				{ Count = 10,  TemplateId =  "rat_giant"},

				{ Count = 1,  TemplateId =  "bandit"},
				{ Count = 1,  TemplateId =  "bandit_female"},

			},
			Medium = {
				{ Count = 20, TemplateId =  "coyote" }, 
				{ Count = 7, TemplateId =  "black_bear" }, 
				{ Count = 20, TemplateId =  "hind" }, 
				{ Count = 10, TemplateId = "turkey" }, 
				{ Count = 10, TemplateId = "chestnut_horse" }, 
				{ Count = 10, TemplateId = "brown_bear" }, 
				{ Count = 7, TemplateId = "wolf_black" }, 

				{ Count = 20, TemplateId = "plant_moss"},				
				{ Count = 20, TemplateId = "plant_ginseng"},
				{ Count = 20, TemplateId = "plant_lemon_grass" },				
				{ Count = 20, TemplateId =  "plant_mushrooms"},
				{ Count = 30, TemplateId =  "plant_cotton"},
				{ Count = 10,  TemplateId =  "plant_giant_mushrooms"},
				{ Count = 20, TemplateId = "plant_kindling"},

				--[[ --SCAN ADDED FOR EASTER EVENT
				{ Count = 10, TemplateId = "easter_egg_green"},
				{ Count = 10, TemplateId = "easter_egg_blue"},	
				{ Count = 10, TemplateId = "easter_egg_purple"},	
				{ Count = 10, TemplateId = "easter_egg_pink"},	
				{ Count = 10, TemplateId = "easter_egg_red"},	
				{ Count = 10, TemplateId = "easter_egg_yellow"},
				]]

				{ Count = 5,  TemplateId =  "bandit"},
				{ Count = 5,  TemplateId =  "bandit_female"},
			},
			Hard = {
				{ Count = 5, TemplateId =  "coyote" }, 
				{ Count = 2, TemplateId =  "black_bear" }, 
				{ Count = 5, TemplateId =  "hind" }, 
				{ Count = 10, TemplateId = "turkey" }, 
				{ Count = 10, TemplateId = "chestnut_horse" }, 
				{ Count = 5, TemplateId = "brown_bear" }, 
				{ Count = 12, TemplateId = "wolf_black" },
				{ Count = 10, TemplateId = "grizzly_bear" },
				{ Count = 20, TemplateId = "great_hart" },

				{ Count = 20, TemplateId = "plant_moss"},				
				{ Count = 20, TemplateId = "plant_ginseng"},
				{ Count = 20, TemplateId = "plant_lemon_grass" },				
				{ Count = 20, TemplateId =  "plant_mushrooms"},
				{ Count = 30, TemplateId =  "plant_cotton"},
				{ Count = 10,  TemplateId =  "plant_giant_mushrooms"},
				{ Count = 20, TemplateId = "plant_kindling"},
				
				--[[ --SCAN ADDED FOR EASTER EVENT
				{ Count = 10, TemplateId = "easter_egg_green"},
				{ Count = 10, TemplateId = "easter_egg_blue"},	
				{ Count = 10, TemplateId = "easter_egg_purple"},	
				{ Count = 10, TemplateId = "easter_egg_pink"},	
				{ Count = 10, TemplateId = "easter_egg_red"},	
				{ Count = 10, TemplateId = "easter_egg_yellow"},
				]]

				{ Count = 2,  TemplateId =  "troll"},
				{ Count = 2,  TemplateId =  "spider_large"},
				{ Count = 5,  TemplateId =  "bandit"},
				{ Count = 5,  TemplateId =  "bandit_female"},
			},

			Seasonal = {
				{ Count = 5, TemplateId =  "coyote" }, 
				{ Count = 2, TemplateId =  "black_bear" }, 
				{ Count = 5, TemplateId =  "hind" }, 
				{ Count = 10, TemplateId = "turkey" }, 
				{ Count = 10, TemplateId = "chestnut_horse" }, 
				{ Count = 5, TemplateId = "brown_bear" }, 
				{ Count = 12, TemplateId = "wolf_black" },
				{ Count = 10, TemplateId = "grizzly_bear" },
				{ Count = 20, TemplateId = "great_hart" },

				{ Count = 20, TemplateId = "plant_moss"},				
				{ Count = 20, TemplateId = "plant_ginseng"},
				{ Count = 20, TemplateId = "plant_lemon_grass" },				
				{ Count = 20, TemplateId =  "plant_mushrooms"},
				{ Count = 30, TemplateId =  "plant_cotton"},
				{ Count = 10,  TemplateId =  "plant_giant_mushrooms"},
				{ Count = 20, TemplateId = "plant_kindling"},

				{ Count = 2,  TemplateId =  "troll"},
				{ Count = 2,  TemplateId =  "spider_large"},
				{ Count = 5,  TemplateId =  "bandit"},
				{ Count = 5,  TemplateId =  "bandit_female"},

				{ Count = 75,  TemplateId =  "cauldron_spooky"},
			},
		},
		BarrenLands = {
			Easy = { 
				{ Count = 5, TemplateId =  "snake_sand" }, 
				{ Count = 5, TemplateId =  "beetle" }, 
				{ Count = 5, TemplateId =  "wolf_desert" },

				{ Count = 30, TemplateId = "plant_cactus"},		
				{ Count = 30,  TemplateId =  "plant_giant_mushrooms"},
				{ Count = 20, TemplateId = "plant_kindling"},
				{ Count = 15, TemplateId =  "plant_barrens_cotton"},

				{ Count = 2,  TemplateId =  "scorpion"},
				{ Count = 2,  TemplateId =  "beetle_giant"},
			},
		},
		BlackForest = {
			Easy = { 
				{ Count = 15, TemplateId =  "bat" }, 
				{ Count = 15, TemplateId =  "spider" }, 
				{ Count = 5, TemplateId =  "spider_large" },

				{ Count = 30, TemplateId =  "plant_mushrooms"},
				{ Count = 30, TemplateId = "plant_giant_mushrooms"},		
				{ Count = 20, TemplateId = "plant_kindling"},
				{ Count = 30, TemplateId =  "plant_cotton"},

				{ Count = 5,  TemplateId =  "spider_large"},
				{ Count = 5,  TemplateId =  "ent"},
			},
		},
		FrozenTundra = {
			Easy = { 
				{ Count = 30, TemplateId =  "whitetail" },
				{ Count = 20, TemplateId = "arctic_hare" },
				{ Count = 5, TemplateId =  "arctic_wolf" },
				{ Count = 5, TemplateId =  "polar_bear" },
				--{ Count = 10, TemplateId = "bay_horse" },

				--{ Count = 20, TemplateId = "plant_moss"},
				{ Count = 20, TemplateId = "plant_ginseng"},
				{ Count = 20, TemplateId = "plant_lemon_grass" },			
				{ Count = 20, TemplateId =  "plant_mushrooms"},
				--{ Count = 20, TemplateId =  "plant_cotton"},
				{ Count = 10,  TemplateId =  "plant_giant_mushrooms"},
				{ Count = 20, TemplateId = "plant_kindling"},

				{ Count = 20,  TemplateId =  "rat_giant"},
			},
			Medium = {
				{ Count = 30, TemplateId =  "whitetail" },
				{ Count = 20, TemplateId =  "arctic_hare" },
				{ Count = 10, TemplateId =  "arctic_wolf" },
				{ Count = 2, TemplateId =  "arctic_wolf_howler" },
				{ Count = 10, TemplateId = "polar_bear" },
				--{ Count = 15, TemplateId = "fox" },
				--{ Count = 10, TemplateId = "bay_horse" },
				{ Count = 5, TemplateId = "snow_leopard" },
				{ Count = 2, TemplateId = "snow_leopard_prowler" },

				--{ Count = 20, TemplateId = "plant_moss"},
				{ Count = 20, TemplateId = "plant_ginseng"},
				{ Count = 20, TemplateId = "plant_lemon_grass" },
				{ Count = 20, TemplateId =  "plant_mushrooms"},
				--{ Count = 30, TemplateId =  "plant_cotton"},
				{ Count = 10,  TemplateId =  "plant_giant_mushrooms"},
				{ Count = 20, TemplateId = "plant_kindling"},

				{ Count = 2,  TemplateId =  "bandit"},
				{ Count = 2,  TemplateId =  "bandit_female"},
			},
			Hard = {
				{ Count = 30, TemplateId =  "whitetail" },
				{ Count = 30, TemplateId =  "arctic_hare" },
				{ Count = 30, TemplateId =  "arctic_wolf" },
				{ Count = 6, TemplateId =  "arctic_wolf_howler" },
				{ Count = 30, TemplateId = "polar_bear" },
				{ Count = 15, TemplateId = "snow_leopard" },
				{ Count = 4, TemplateId =  "snow_leopard_prowler" },

				{ Count = 2,  TemplateId =  "troll"},
				--{ Count = 2,  TemplateId =  "spider_large"},

				--{ Count = 20, TemplateId = "plant_moss"},			
				{ Count = 20, TemplateId = "plant_ginseng"},
				{ Count = 20, TemplateId = "plant_lemon_grass" },			
				{ Count = 20, TemplateId =  "plant_mushrooms"},
				--{ Count = 30, TemplateId =  "plant_cotton"},
				{ Count = 10,  TemplateId =  "plant_giant_mushrooms"},
				{ Count = 20, TemplateId = "plant_kindling"},
			},
		},
	},

	MaleHeads =
	{
	{"head_male02",182},
	{"head_male02",176},
	{"head_male02",807},
	{"head_male02",172},
	{"head_male02",809},
	{"head_male02",810},
	{"head_male02",811},
	{"head_male02",817},

	{"head_male03",182},
	{"head_male03",176},
	{"head_male03",807},
	{"head_male03",172},
	{"head_male03",809},
	{"head_male03",810},
	{"head_male03",811},
	{"head_male03",817},

	{"head_male04",182},
	{"head_male04",176},
	{"head_male04",807},
	{"head_male04",172},
	{"head_male04",809},
	{"head_male04",810},
	{"head_male04",811},
	{"head_male04",817},

	{"head_male05",182},
	{"head_male05",176},
	{"head_male05",807},
	{"head_male05",172},
	{"head_male05",809},
	{"head_male05",810},
	{"head_male05",811},
	{"head_male05",817},
	},

	FemaleHeads =
	{
	{"head_female01",182},
	{"head_female01",176},
	{"head_female01",807},
	{"head_female01",172},
	{"head_female01",809},
	{"head_female01",810},
	{"head_female01",811},
	{"head_female01",817},

	{"head_female02",182},
	{"head_female02",176},
	{"head_female02",807},
	{"head_female02",172},
	{"head_female02",809},
	{"head_female02",810},
	{"head_female02",811},
	{"head_female02",817},

	{"head_female03",182},
	{"head_female03",176},
	{"head_female03",807},
	{"head_female03",172},
	{"head_female03",809},
	{"head_female03",810},
	{"head_female03",811},
	{"head_female03",817},

	{"head_female04",182},
	{"head_female04",176},
	{"head_female04",807},
	{"head_female04",172},
	{"head_female04",809},
	{"head_female04",810},
	{"head_female04",811},
	{"head_female04",817},

	{"head_female05",182},
	{"head_female05",176},
	{"head_female05",807},
	{"head_female05",172},
	{"head_female05",809},
	{"head_female05",810},
	{"head_female05",811},
	{"head_female05",817},
	},

	MaleFacialHair = {
		{"",4},
		{"facial_hair_beard_thin",8},
		{"facial_hair_beard",182},
		{"facial_hair_beard_long",176},
		{"facial_hair_beard_longer",807},
		{"facial_hair_beard_chops",172},
		{"facial_hair_beard_chops2",809},
		{"facial_hair_beard_goatee3",810},
		{"facial_hair_beard_goatee2",811},
		{"facial_hair_beard_goatee",817},
		{"facial_hair_beard_mustache",182},
		{"facial_hair_beard_mustache_long",176},
		{"facial_hair_beard_mustache2",807},
	},

	WarPaints = 
	{
		{"warpaint_1"},
		{"warpaint_2"},
		{"warpaint_3"},
	},


	MaleWaywardHeads = {{"head_male04","FF9933"} },
	FemaleWaywardHeads = { {"head_female04","FF9933"}, },

	FoundersDancersMaleHeads = { "head_male02","head_male03","head_male04","head_male05","head_male02","head_male03","head_male04","head_male05","head_male02","head_male03","head_male04","head_male05",
	{"head_male02"},{"head_male03"},{"head_male04"},{"head_male05",},},

	FoundersDancersFemaleHeads = { "head_female01","head_female02","head_female03","head_female04","head_female05","head_female06", "head_female01","head_female02","head_female03","head_female04","head_female05","head_female06",
	{"head_female01"},{"head_female02"},{"head_female03",},{"head_female04"},{"head_female05"},{"head_female01",},},

	MaleWaywardHair = {
		{"hair_male_messy","0xFF191919"},
		{"hair_male_buzzcut","0xFF191919"},
		{"","0xFF191919"},
		{"","0xFF191919"},
	},
	FemaleWaywardHair = {
		{"hair_female_shaggy","0xFF191919"},
	},

	MaleHairSlave = 
	{
	{"hair_male_buzzcut",4},
	{"hair_male_buzzcut",8},
	{"hair_male_buzzcut",768},
	{"hair_male_buzzcut",770},
	{"hair_male_buzzcut",772},
	{"hair_male_buzzcut",781},
	{"hair_male_buzzcut",793},
	{"hair_male_buzzcut",788},
	{"hair_male_buzzcut",789},
	{"hair_male_buzzcut",792},

	{"hair_male",4},
	{"hair_male",8},
	{"hair_male",768},
	{"hair_male",770},
	{"hair_male",772},
	{"hair_male",781},
	{"hair_male",793},
	{"hair_male",788},
	{"hair_male",789},
	{"hair_male",792},

	{"",4},
	{"",8},
	{"",768},
	{"",770},
	{"",772},
	{"",781},
	{"",793},
	{"",788},
	{"",789},
	{"",792},
	},

	MaleHairBeggar = 
	{
	{"hair_male_messy",4},
	{"hair_male_messy",8},
	{"hair_male_messy",768},
	{"hair_male_messy",770},
	{"hair_male_messy",772},
	{"hair_male_messy",781},
	{"hair_male_messy",793},
	{"hair_male_messy",788},
	{"hair_male_messy",789},
	{"hair_male_messy",792},

	{"hair_male",4},
	{"hair_male",8},
	{"hair_male",768},
	{"hair_male",770},
	{"hair_male",772},
	{"hair_male",781},
	{"hair_male",793},
	{"hair_male",788},
	{"hair_male",789},
	{"hair_male",792},

	{"hair_male_windswept",4},
	{"hair_male_windswept",8},
	{"hair_male_windswept",768},
	{"hair_male_windswept",770},
	{"hair_male_windswept",772},
	{"hair_male_windswept",781},
	{"hair_male_windswept",793},
	{"hair_male_windswept",788},
	{"hair_male_windswept",789},
	{"hair_male_windswept",792},
	
	{"",4},
	{"",8},
	{"",768},
	{"",770},
	{"",772},
	{"",781},
	{"",793},
	{"",788},
	{"",789},
	{"",792},
	},

	MaleHairVillage = 
	{
	{"hair_male",4},
	{"hair_male",8},
	{"hair_male",768},
	{"hair_male",770},
	{"hair_male",772},
	{"hair_male",781},
	{"hair_male",793},
	{"hair_male",788},
	{"hair_male",789},
	{"hair_male",792},

	{"hair_male_bangs",4},
	{"hair_male_bangs",8},
	{"hair_male_bangs",768},
	{"hair_male_bangs",770},
	{"hair_male_bangs",772},
	{"hair_male_bangs",781},
	{"hair_male_bangs",793},
	{"hair_male_bangs",788},
	{"hair_male_bangs",789},
	{"hair_male_bangs",792},

	{"hair_male_buzzcut",4},
	{"hair_male_buzzcut",8},
	{"hair_male_buzzcut",768},
	{"hair_male_buzzcut",770},
	{"hair_male_buzzcut",772},
	{"hair_male_buzzcut",781},
	{"hair_male_buzzcut",793},
	{"hair_male_buzzcut",788},
	{"hair_male_buzzcut",789},
	{"hair_male_buzzcut",792},

	{"hair_male_messy",4},
	{"hair_male_messy",8},
	{"hair_male_messy",768},
	{"hair_male_messy",770},
	{"hair_male_messy",772},
	{"hair_male_messy",781},
	{"hair_male_messy",793},
	{"hair_male_messy",788},
	{"hair_male_messy",789},
	{"hair_male_messy",792},

	{"hair_male_nobleman",4},
	{"hair_male_nobleman",8},
	{"hair_male_nobleman",768},
	{"hair_male_nobleman",770},
	{"hair_male_nobleman",772},
	{"hair_male_nobleman",781},
	{"hair_male_nobleman",793},
	{"hair_male_nobleman",788},
	{"hair_male_nobleman",789},
	{"hair_male_nobleman",792},

	{"hair_male_rougish",4},
	{"hair_male_rougish",8},
	{"hair_male_rougish",768},
	{"hair_male_rougish",770},
	{"hair_male_rougish",772},
	{"hair_male_rougish",781},
	{"hair_male_rougish",793},
	{"hair_male_rougish",788},
	{"hair_male_rougish",789},
	{"hair_male_rougish",792},

	{"hair_male_sideundercut",4},
	{"hair_male_sideundercut",8},
	{"hair_male_sideundercut",768},
	{"hair_male_sideundercut",770},
	{"hair_male_sideundercut",772},
	{"hair_male_sideundercut",781},
	{"hair_male_sideundercut",793},
	{"hair_male_sideundercut",788},
	{"hair_male_sideundercut",789},
	{"hair_male_sideundercut",792},

	{"hair_male_undercut",4},
	{"hair_male_undercut",8},
	{"hair_male_undercut",768},
	{"hair_male_undercut",770},
	{"hair_male_undercut",772},
	{"hair_male_undercut",781},
	{"hair_male_undercut",793},
	{"hair_male_undercut",788},
	{"hair_male_undercut",789},
	{"hair_male_undercut",792},

	{"hair_male_windswept",4},
	{"hair_male_windswept",8},
	{"hair_male_windswept",768},
	{"hair_male_windswept",770},
	{"hair_male_windswept",772},
	{"hair_male_windswept",781},
	{"hair_male_windswept",793},
	{"hair_male_windswept",788},
	{"hair_male_windswept",789},
	{"hair_male_windswept",792},

	{"",4},
	{"",8},
	{"",768},
	{"",770},
	{"",772},
	{"",781},
	{"",793},
	{"",788},
	{"",789},
	{"",792},
	
	},	

	FemaleHairBarbarian = 
	{
		{"hair_female_shaggy",4},
		{"hair_female_shaggy",8},
		{"hair_female_shaggy",768},
		{"hair_female_shaggy",770},
		{"hair_female_shaggy",772},
		{"hair_female_shaggy",781},
		{"hair_female_shaggy",793},
		{"hair_female_shaggy",788},
		{"hair_female_shaggy",789},
		{"hair_female_shaggy",792},
	},

	FemaleHairSlave = 
	{
	{"hair_female_shaggy",4},
	{"hair_female_shaggy",8},
	{"hair_female_shaggy",768},
	{"hair_female_shaggy",770},
	{"hair_female_shaggy",772},
	{"hair_female_shaggy",781},
	{"hair_female_shaggy",793},
	{"hair_female_shaggy",788},
	{"hair_female_shaggy",789},
	{"hair_female_shaggy",792},

	{"hair_female_buzzcut",4},
	{"hair_female_buzzcut",8},
	{"hair_female_buzzcut",768},
	{"hair_female_buzzcut",770},
	{"hair_female_buzzcut",772},
	{"hair_female_buzzcut",781},
	{"hair_female_buzzcut",793},
	{"hair_female_buzzcut",788},
	{"hair_female_buzzcut",789},
	{"hair_female_buzzcut",792},

	{"hair_cultist_female",4},
	{"hair_cultist_female",8},
	{"hair_cultist_female",768},
	{"hair_cultist_female",770},
	{"hair_cultist_female",772},
	{"hair_cultist_female",781},
	{"hair_cultist_female",793},
	{"hair_cultist_female",788},
	{"hair_cultist_female",789},
	{"hair_cultist_female",792},
	},

	FemaleHairBeggar = 
	{
	{"hair_female_shaggy",4},
	{"hair_female_shaggy",8},
	{"hair_female_shaggy",768},
	{"hair_female_shaggy",770},
	{"hair_female_shaggy",772},
	{"hair_female_shaggy",781},
	{"hair_female_shaggy",793},
	{"hair_female_shaggy",788},
	{"hair_female_shaggy",789},
	{"hair_female_shaggy",792},

	{"hair_female",4},
	{"hair_female",8},
	{"hair_female",768},
	{"hair_female",770},
	{"hair_female",772},
	{"hair_female",781},
	{"hair_female",793},
	{"hair_female",788},
	{"hair_female",789},
	{"hair_female",792},

	{"hair_female_pigtails",4},
	{"hair_female_pigtails",8},
	{"hair_female_pigtails",768},
	{"hair_female_pigtails",770},
	{"hair_female_pigtails",772},
	{"hair_female_pigtails",781},
	{"hair_female_pigtails",793},
	{"hair_female_pigtails",788},
	{"hair_female_pigtails",789},
	{"hair_female_pigtails",792},
	
	{"hair_female_bun",4},
	{"hair_female_bun",8},
	{"hair_female_bun",768},
	{"hair_female_bun",770},
	{"hair_female_bun",772},
	{"hair_female_bun",781},
	{"hair_female_bun",793},
	{"hair_female_bun",788},
	{"hair_female_bun",789},
	{"hair_female_bun",792},
	},

	FemaleHairVillage = 
	{
	{"hair_female",4},
	{"hair_female",8},
	{"hair_female",768},
	{"hair_female",770},
	{"hair_female",772},
	{"hair_female",781},
	{"hair_female",793},
	{"hair_female",788},
	{"hair_female",789},
	{"hair_female",792},

	{"hair_female_pigtails",4},
	{"hair_female_pigtails",8},
	{"hair_female_pigtails",768},
	{"hair_female_pigtails",770},
	{"hair_female_pigtails",772},
	{"hair_female_pigtails",781},
	{"hair_female_pigtails",793},
	{"hair_female_pigtails",788},
	{"hair_female_pigtails",789},
	{"hair_female_pigtails",792},

	{"hair_female_bob",4},
	{"hair_female_bob",8},
	{"hair_female_bob",768},
	{"hair_female_bob",770},
	{"hair_female_bob",772},
	{"hair_female_bob",781},
	{"hair_female_bob",793},
	{"hair_female_bob",788},
	{"hair_female_bob",789},
	{"hair_female_bob",792},

	{"hair_female_bun",4},
	{"hair_female_bun",8},
	{"hair_female_bun",768},
	{"hair_female_bun",770},
	{"hair_female_bun",772},
	{"hair_female_bun",781},
	{"hair_female_bun",793},
	{"hair_female_bun",788},
	{"hair_female_bun",789},
	{"hair_female_bun",792},

	{"hair_female_ponytail",4},
	{"hair_female_ponytail",8},
	{"hair_female_ponytail",768},
	{"hair_female_ponytail",770},
	{"hair_female_ponytail",772},
	{"hair_female_ponytail",781},
	{"hair_female_ponytail",793},
	{"hair_female_ponytail",788},
	{"hair_female_ponytail",789},
	{"hair_female_ponytail",792},

	{"hair_female_shaggy",4},
	{"hair_female_shaggy",8},
	{"hair_female_shaggy",768},
	{"hair_female_shaggy",770},
	{"hair_female_shaggy",772},
	{"hair_female_shaggy",781},
	{"hair_female_shaggy",793},
	{"hair_female_shaggy",788},
	{"hair_female_shaggy",789},
	{"hair_female_shaggy",792},
	},	

	FemaleHairFounders = 
	{
	{"hair_female",4},
	{"hair_female",8},
	{"hair_female",768},
	{"hair_female",770},
	{"hair_female",772},
	{"hair_female",781},
	{"hair_female",793},
	{"hair_female",788},
	{"hair_female",789},
	{"hair_female",792},

	{"hair_female_pigtails",4},
	{"hair_female_pigtails",8},
	{"hair_female_pigtails",768},
	{"hair_female_pigtails",770},
	{"hair_female_pigtails",772},
	{"hair_female_pigtails",781},
	{"hair_female_pigtails",793},
	{"hair_female_pigtails",788},
	{"hair_female_pigtails",789},
	{"hair_female_pigtails",792},

	{"hair_female_bob",4},
	{"hair_female_bob",8},
	{"hair_female_bob",768},
	{"hair_female_bob",770},
	{"hair_female_bob",772},
	{"hair_female_bob",781},
	{"hair_female_bob",793},
	{"hair_female_bob",788},
	{"hair_female_bob",789},
	{"hair_female_bob",792},

	{"hair_female_bun",4},
	{"hair_female_bun",8},
	{"hair_female_bun",768},
	{"hair_female_bun",770},
	{"hair_female_bun",772},
	{"hair_female_bun",781},
	{"hair_female_bun",793},
	{"hair_female_bun",788},
	{"hair_female_bun",789},
	{"hair_female_bun",792},

	{"hair_female_ponytail",4},
	{"hair_female_ponytail",8},
	{"hair_female_ponytail",768},
	{"hair_female_ponytail",770},
	{"hair_female_ponytail",772},
	{"hair_female_ponytail",781},
	{"hair_female_ponytail",793},
	{"hair_female_ponytail",788},
	{"hair_female_ponytail",789},
	{"hair_female_ponytail",792},

	{"hair_female_shaggy",4},
	{"hair_female_shaggy",8},
	{"hair_female_shaggy",768},
	{"hair_female_shaggy",770},
	{"hair_female_shaggy",772},
	{"hair_female_shaggy",781},
	{"hair_female_shaggy",793},
	{"hair_female_shaggy",788},
	{"hair_female_shaggy",789},
	{"hair_female_shaggy",792},
	},	

	MaleHairFounders = 
	{
	{"hair_male",4},
	{"hair_male",8},
	{"hair_male",768},
	{"hair_male",770},
	{"hair_male",772},
	{"hair_male",781},
	{"hair_male",793},
	{"hair_male",788},
	{"hair_male",789},
	{"hair_male",792},

	{"hair_male_bangs",4},
	{"hair_male_bangs",8},
	{"hair_male_bangs",768},
	{"hair_male_bangs",770},
	{"hair_male_bangs",772},
	{"hair_male_bangs",781},
	{"hair_male_bangs",793},
	{"hair_male_bangs",788},
	{"hair_male_bangs",789},
	{"hair_male_bangs",792},

	{"hair_male_buzzcut",4},
	{"hair_male_buzzcut",8},
	{"hair_male_buzzcut",768},
	{"hair_male_buzzcut",770},
	{"hair_male_buzzcut",772},
	{"hair_male_buzzcut",781},
	{"hair_male_buzzcut",793},
	{"hair_male_buzzcut",788},
	{"hair_male_buzzcut",789},
	{"hair_male_buzzcut",792},

	{"hair_male_messy",4},
	{"hair_male_messy",8},
	{"hair_male_messy",768},
	{"hair_male_messy",770},
	{"hair_male_messy",772},
	{"hair_male_messy",781},
	{"hair_male_messy",793},
	{"hair_male_messy",788},
	{"hair_male_messy",789},
	{"hair_male_messy",792},

	{"hair_male_nobleman",4},
	{"hair_male_nobleman",8},
	{"hair_male_nobleman",768},
	{"hair_male_nobleman",770},
	{"hair_male_nobleman",772},
	{"hair_male_nobleman",781},
	{"hair_male_nobleman",793},
	{"hair_male_nobleman",788},
	{"hair_male_nobleman",789},
	{"hair_male_nobleman",792},

	{"hair_male_rougish",4},
	{"hair_male_rougish",8},
	{"hair_male_rougish",768},
	{"hair_male_rougish",770},
	{"hair_male_rougish",772},
	{"hair_male_rougish",781},
	{"hair_male_rougish",793},
	{"hair_male_rougish",788},
	{"hair_male_rougish",789},
	{"hair_male_rougish",792},

	{"hair_male_sideundercut",4},
	{"hair_male_sideundercut",8},
	{"hair_male_sideundercut",768},
	{"hair_male_sideundercut",770},
	{"hair_male_sideundercut",772},
	{"hair_male_sideundercut",781},
	{"hair_male_sideundercut",793},
	{"hair_male_sideundercut",788},
	{"hair_male_sideundercut",789},
	{"hair_male_sideundercut",792},

	{"hair_male_undercut",4},
	{"hair_male_undercut",8},
	{"hair_male_undercut",768},
	{"hair_male_undercut",770},
	{"hair_male_undercut",772},
	{"hair_male_undercut",781},
	{"hair_male_undercut",793},
	{"hair_male_undercut",788},
	{"hair_male_undercut",789},
	{"hair_male_undercut",792},

	{"hair_male_windswept",4},
	{"hair_male_windswept",8},
	{"hair_male_windswept",768},
	{"hair_male_windswept",770},
	{"hair_male_windswept",772},
	{"hair_male_windswept",781},
	{"hair_male_windswept",793},
	{"hair_male_windswept",788},
	{"hair_male_windswept",789},
	{"hair_male_windswept",792},

	{"",4},
	{"",8},
	{"",768},
	{"",770},
	{"",772},
	{"",781},
	{"",793},
	{"",788},
	{"",789},
	{"",792},
	
	},	

	MonolithMaleHeads =
	{
		{"head_male02",868},
		{"head_male03",868},
		{"head_male04",868},
		{"head_male05",868},
	},

	FoundersChests =  { {"founders_chest_base","0xFFFFFF00"},
						{"founders_chest_base","0xFFFF00FF"},
						{"founders_chest_base","0xFF00FFFF"},
						{"founders_chest_base","0xFFFF0000"},
						{"founders_chest_base","0xFF00FF00"},
						{"founders_chest_base","0xFF0000FF"}, 
						{"founders_chest2_base","0xFFFFFF00"},
						{"founders_chest2_base","0xFFFF00FF"},
						{"founders_chest2_base","0xFF00FFFF"},
						{"founders_chest2_base","0xFFFF0000"},
						{"founders_chest2_base","0xFF00FF00"},
						{"founders_chest2_base","0xFF0000FF"},
					    {"founders_chest3_base","0xFFFFFF00"},
					    {"founders_chest3_base","0xFFFF00FF"},
					    {"founders_chest3_base","0xFF00FFFF"},
					    {"founders_chest3_base","0xFFFF0000"},
					    {"founders_chest3_base","0xFF00FF00"},
					    {"founders_chest3_base","0xFF0000FF"},},

	FoundersLegs =  { 	{"founders_legs_base","0xFFFFFF00"},
						{"founders_legs_base","0xFFFF00FF"},
						{"founders_legs_base","0xFF00FFFF"},
						{"founders_legs_base","0xFFFF0000"},
						{"founders_legs_base","0xFF00FF00"},
						{"founders_legs_base","0xFF0000FF"},
						{"founders_legs2_base","0xFFFFFF00"},
						{"founders_legs2_base","0xFFFF00FF"},
						{"founders_legs2_base","0xFF00FFFF"},
						{"founders_legs2_base","0xFFFF0000"},
						{"founders_legs2_base","0xFF00FF00"},
						{"founders_legs2_base","0xFF0000FF"},
					    {"founders_legs3_base","0xFFFFFF00"},
					    {"founders_legs3_base","0xFFFF00FF"},
					    {"founders_legs3_base","0xFF00FFFF"},
					    {"founders_legs3_base","0xFFFF0000"},
					    {"founders_legs3_base","0xFF00FF00"},
					    {"founders_legs3_base","0xFF0000FF"}, },

	ShortNames = {"Uadi","Thraz","Inall","Veat","Honst","Noom","Otasa",
					"Enysa","Urody","Irv","Blard","Eelde","Torn",
					"Amora","Hourd","Kalrr","Etr","Kas","Oldk","Igary",
					"Cric","Schir","Morgh","Eato","Garlt","Yero","Itb",
					"Essr","Oesta","Ieta","Eono","Irl","Troum","Drack",
					"Elmrr","Kals","Drald","Onyu","Dral","Urake","Ymose",
					"Ghaf","Enc","Oeme","Yalei","Oaleo","Utia","Roip",
					"Ingsh","Oesty","Nos","Chroh","Endd","Emss","Yacky",
					"Dret","Lerd","Vof","Itai","Aentha","Morz","Irany",
					"Tiash","Abele","Epolo","Kon","Skelnd","Onale","Lak",
					"Neyl","Iusta","Than","Uomu","Neiy","Athnn","Enll",
					"Risck","Yskeli","Ashp","Utury","Tinq","Yquay","Deys",
					"Orada","Yessi","Eaugho","Taih","Voel","Mort","Veur",
					"Yhata","Ihiny","Alee","Ieno","Itr","Therr","Ytaio",
					"Queck","Nysz","Denn","Belf","Alend","Overy","Rich",
					"Ryng","Swaeck","Abelo","Iemi","Yomi","Etaie","Quel",
					"Soirt","Yuski","Chriat","Oosa","Cerph","Orilo"},

	MinerNames = {"Uadi the Miner","Thraz the Miner","Inall the Miner","Veat the Miner","Honst the Miner","Noom the Miner","Otasa the Miner",
					"Enysa the Miner","Urody the Miner","Irv the Miner","Blard the Miner","Eelde the Miner","Torn the Miner",
					"Amora the Miner","Hourd the Miner","Kalrr the Miner","Etr the Miner","Kas the Miner","Oldk the Miner","Igary the Miner",
					"Cric the Miner","Schir the Miner","Morgh the Miner","Eato the Miner","Garlt the Miner","Yero the Miner","Itb the Miner",
					"Essr the Miner","Oesta the Miner","Ieta the Miner","Eono the Miner","Irl the Miner","Troum the Miner","Drack the Miner",
					"Elmrr the Miner","Kals the Miner","Drald the Miner","Onyu the Miner","Dral the Miner","Urake the Miner","Ymose the Miner",
					"Ghaf the Miner","Enc the Miner","Oeme the Miner","Yalei the Miner","Oaleo the Miner","Utia the Miner","Roip the Miner",
					"Ingsh the Miner","Oesty the Miner","Nos the Miner","Chroh the Miner","Endd the Miner","Emss the Miner","Yacky the Miner",
					"Dret the Miner","Lerd the Miner","Vof the Miner","Itai the Miner","Aentha the Miner","Morz the Miner","Irany the Miner",
					"Tiash the Miner","Abele the Miner","Epolo the Miner","Kon the Miner","Skelnd the Miner","Onale the Miner","Lak the Miner",
					"Neyl the Miner","Iusta the Miner","Than the Miner","Uomu the Miner","Neiy the Miner","Athnn the Miner","Enll the Miner",
					"Risck the Miner","Yskeli the Miner","Ashp the Miner","Utury the Miner","Tinq the Miner","Yquay the Miner","Deys the Miner",
					"Orada the Miner","Yessi the Miner","Eaugho the Miner","Taih the Miner","Voel the Miner","Mort the Miner","Veur the Miner",
					"Yhata the Miner","Ihiny the Miner","Alee the Miner","Ieno the Miner","Itr the Miner","Therr the Miner","Ytaio the Miner",
					"Queck the Miner","Nysz the Miner","Denn the Miner","Belf the Miner","Alend the Miner","Overy the Miner","Rich the Miner",
					"Ryng the Miner","Swaeck the Miner","Abelo the Miner","Iemi the Miner","Yomi the Miner","Etaie the Miner","Quel the Miner",
					"Soirt the Miner","Yuski the Miner","Chriat the Miner","Oosa the Miner","Cerph the Miner","Orilo the Miner"},

	MaleNpcNames = {"Abaet","Abarden","Acamen","Achard","Adeen","Aerden","Aghon","Agnar","Ahburn","Ahdun","Airen","Airis","Aldaren","Alderman","Alkirk","Allso","Amitel","Anfar","Anumil","Asden","Asen","Aslan","Atgur","Atlin","Auden","Ault","Aysen","Bacohl","Badeek","Balati","Baradeer","Basden","Bayde","Bedic","Beeron","Beson","Besur","Bewul","Biedgar","Biston","Bithon","Boaldelr","Bolrock","Breanon","Bredere","Bredock","Breen","Bristan","Buchmeid","Busma","Buthomar","Caelholdt","Cainon","Camchak","Camilde","Casden","Cayold","Celorn","Celthric","Cerdern","Cespar","Cevelt","Chamon","Chidak","Cibrock","Ciroc","Codern","Connell","Cordale","Cosdeer","Cuparun","Cydare","Cylmar","Cyton","Daburn","Daermod","Dakamon","Dakkone","Dalmarn","Dapvhir","Darkkon","Darko","Darmor","Darpick","Dask","Deathmar","Derik","Derrin","Dessfar","Dinfar","Doceon","Dochrohan","Dorn","Dosoman","Drakone","Drandon","Dritz","Drophar","Dryn","Duba","Duran","Durmark","Dyfar","Dyten","Eard","Eckard","Efar","Egmardern","Ekgamut","Eli","Elson","Elthin","Endor","Enidin","Enro","Erikarn","Eritai","Escariet","Etar","Etburn","Ethen","Etmere","Eythil","Faoturk","Faowind","Fenrirr","Fetmar","Ficadon","Fickfylo","Firedorn","Firiro","Folmard","Fraderk","Fydar","Fyn","Gafolern","Gai","Galiron","Gametris","Gemardt","Gemedern","Gerirr","Geth","Gibolock","Gibolt","Gom","Gosford","Gothikar","Gresforn","Gryn","Gundir","Guthale","Gybol","Gyin","Halmar","Harrenhal","Hectar","Hecton","Hermenze","Hermuck","Hildale","Hildar","Hydale","Hyten","Iarmod","Idon","Ieserk","Ikar","Illilorn","Illium","Ipedorn","Irefist","Isen","Isil","Jackson","Jalil","Janus","Jayco","Jesco","Jespar","Jex","Jib","Jin","Juktar","Jun","Justal","Kafar","Kaldar","Keran","Kesad","Kethren","Kib","Kiden","Kilbas","Kildarien","Kimdar","Kip","Kirder","Kolmorn","Kyrad","Lackus","Lacspor","Lafornon","Lahorn","Ledale","Leit","Lephidiles","Lerin","Letor","Lidorn","Liphanes","Loban","Ludokrin","Luphildern","Lurd","Macon","Madarlon","Marderdeen","Mardin","Markdoon","Marklin","Mathar","Medarin","Mellamo","Meowol","Meridan","Merkesh","Mes'ard","Mesophan","Mezo","Michael","Mickal","Migorn","Miphates","Mi'talrythin","Modric","Modum","Mufar","Mujarin","Mythik","Mythil","Nadeer","Nalfar","Naphates","Neowyld","Nikpal","Nikrolin","Niro","Noford","Nuthor","Nuwolf","Nythil","O’tho","Ocarin","Occhi","Odaren","Ohethlic","Okar","Omarn","Orin","Othelen","Oxbaren","Padan","Palid","Peitar","Pelphides","Pendus","Perder","Phairdon","Phemedes","Phoenix","Picon","Picumar","Pildoor","Ponith","Poran","Prothalon","Puthor","Qeisan","Qidan","Quid","Quiss","Qysan","Radag'mal","Randar","Rayth","Reaper","Reth","Rethik","Rhithin","Rhysling","Rikar","Rismak","Ritic","Rogeir","Rogoth","Rophan","Rydan","Ryfar","Ryodan","Rysdan","Rythern","Sabal","Sadareen","Samon","Samot","Scoth","Scythe","Sed","Sedar","Senthyril","Serin","Seryth","Sesmidat","Setlo","Shade","Shane","Shard","Shillen","Silco","Sil'forrin","Silpal","Soderman","Sothale","Stenwulf","Steven","Suth","Sutlin","Syth","Sythril","Talberon","Telpur","Temilfist","Tempist","Tespar","Tessino","Thiltran","Tholan","Tibolt","Ticharol","Tithan","Tobale","Tol’","Tolle","Tolsar","Tothale","Tousba","Tuk","Tuscanar","Tyden","Uerthe","Ugmar","Undin","Updar","Vaccon","Vacone","Valynard","Vectomon","Vespar","Vethelot","Vider","Vigoth","Vildar","Vinald","Virde","Voltain","Voudim","Vythethi","Wak’dern","Walkar","Wekmar","Werymn","William","Willican","Wiltmar","Wishane","Wrathran","Wraythe","Wyder","Wyeth","Xander","Xavier","Xex","Xithyl","Y’reth","Yabaro","Yesirn","Yssik","Zak","Zakarn","Zeke","Zerin","Zidar","Zigmal","Zilocke","Zio","Zotar","Zutar","Zyten"},
	FemaleNpcNames = {"Acele","Acholate","Ada","Adiannon","Adorra","Ahanna","Akara","Akassa","Akia","Amaerilde","Amara","Amarisa","Amarizi","Ana","Andonna","Ani","Annalyn","Archane","Ariannona","Arina","Arryn","Asada","Awnia","Ayne","Basete","Bathelie","Bethe","Brana","Brianan","Bridonna","Brynhilde","Calene","Calina","Celestine","Celoa","Cephenrene","Chani","Chivahle","Chrystyne","Corda","Cyelena","Dalavesta","Desini","Dylena","Ebatryne","Ecematare","Efari","Enaldie","Enoka","Enoona","Errinaya","Fayne","Frederika","Frida","Gene","Gessane","Gronalyn","Gvene","Gwethana","Halete","Helenia","Hildandi","Hyza","Idona","Ikini","Ilene","Illia","Iona","Jessika","Jezzine","Justalyne","Kassina","Kilayox","Kilia","Kilyne","Kressara","Laela","Laenaya","Lelani","Lenala","Linovahle","Linyah","Lloyanda","Lolinda","Lyna","Lynessa","Mehande","Melisande","Midiga","Mirayam","Mylene","Nachaloa","Naria","Narisa","Nelenna","Niraya","Nymira","Ochala","Olivia","Onathe","Ondola","Orwyne","Parthinia","Pascheine","Pela","Peri’el","Pharysene","Philadona","Prisane","Prysala","Pythe","Q’ara","Q’pala","Quasee","Rhyanon","Rivatha","Ryiah","Sanala","Sathe","Senira","Sennetta","Sepherene","Serane","Sevestra","Sidara","Sidathe","Sina","Sunete","Synestra","Sythini","Szene","Tabika","Tabithi","Tajule","Tamare","Teresse","Tolida","Tonica","Treka","Tressa","Trinsa","Tryane","Tybressa","Tycane","Tysinni","Undaria","Uneste","Urda","Usara","Useli","Ussesa","Venessa","Veseere","Voladea","Vysarane","Vythica","Wanera","Welisarne","Wellisa","Wesolyne","Wyeta","Yilvoxe","Ysane","Yve","Yviene","Yvonnette","Yysara","Zana","Zathe","Zecele","Zenobia","Zephale","Zephere","Zerma","Zestia","Zilka","Zoura","Zrye","Zyneste","Zynoa"},
	MalePlayerNames = {"Austin","Dan","Supreem","DukeGorilla","TonyTheHutt","Drayak","Zarcul","Troop LoD","dbltnk LoD","Ezekiel","Jairone","Obs LoD","BannerKC LOD","veeego","BlackLung LoD","Harry LoD","Dung Fist","Shibby","Rumble","Darknight","Foghladha","AlexSage","Tombles","Netnet","bensku","Gundehar","ColdMarrow","Auren Stark","Balkazarno","Rainman LoD","Lavos","Grimskab","Nexid","Mark Trail","IceCrow","Austin","Toad","Ursoc","Sarl","Freerk","TheFreshness","SkullLoD","SteelReign","Wayist","Freq","Bhais","sklurb","Jitsu","Laufer LoD","Tabarnak LoD","Starfire3D","Oliver","Theos","Nitroman","Kal","robl","Gunga Din","Conbro","Dargron","Mystikal","FLIPSKU","Zeph","Min","Sir Bob","Helis","Deathreign","TonyTheHutt","Fror","Harald Blackfist","Noobzilla","Corvyn","Crucible","Howler","BaGz","RaVaGe","tryerino","splixx","Dargron2","Zerdoza","mindseye","Sekkerhund","Soth","Gone","Schwarze Schaf","Mengbilar","Alpha","Lavender","Zerdozer","Slaughterjoe","Codex","Ravenclaw","Papi","Zaxion","Liberty","Raith","Dax","Chargul","Amero","Grim","Macros","Deimos","Majister","Vanak","Alien","Rada Lorkaaz","Tony","Yorlik","Brage","adbos","Zerojr","Behanglul","Lagardere","Poukah","Otto Sandrock","TEEM","SatelliteMind","Idontknow","Jazard","Myrkrid Ashen","Ursoc","Ugliness","Amishphenom","Ragestar","Ainz","Brave SirRobin","hierba","Lore","Ranghar","Indeed","Rinnsu","Hawkfire","Dago","Shadowpain","Shadowhunt","Hugo","Rorick","Gurney","Odin","Knuddenstore","Runesabre","Tabulazero","Hawkfyre II","Cassyr Dubois","Lufari","Photosozo","Finvega","Caerbanog","Fletch","Asmorex","Coins","Schattig","Tofu","JooGar","Caerbanog VanRurik","Zabalus","Abimor","Cookie","CookieTM","Kaid","Raja","Dunkelzwerg","Aokis","Verdorben","Xerxes ","Hilip","Chester","Kerbox","Hexplate","Ness199X","SquashSash","squid","Newbatonium","Slik","Magnus","deatrider","Enarial","Betarays","Yoofaloof","blasteron","Turk Shakespeare","Tintagil","Node","Jtoriko","Fortisq","aada","Lordy","HKPackee","Sunkara","Eden","Mayushi","Phearnun","Drexy","Solaris","Cammoo","Supercub013","Hessian","Rooster Castille","Misneach","Samuel","ReigekiKai","Kush Monstar","Certhas","Matt","Drydenn","Arconious","Nazrudin","Tharalik","Gnerix","Asmodai","Velit","Belador","Maganis","NiTe","aumua","Kourgath","Calheb","Deege","Raldor Santoro","Taelyn","Tokee","Exiled","Huart","Apronks","Icky","Lenart","Bidimus","Ekul","Auxilium","Dwyane Wade","Stucky","Sparda","Merif","Highfather","Kysper","migdr","Lester","Alipheese","TheChester","Nazrudin","Padag","Jasc Onine","Rapolda","Girion Telchar","Dirlettia","Parallaxe","Ecksesive","Sunroc","Trok","Cear Dragon","Gand","vidocq","Siodacain","Genoce","Koen","Arkii","Auzze","DocSteal","Eidenuul","Kintaro","Rylok","Splinter","Gnar","Tynian","Lauro","Drake","Sheltem","Trabbie","Ajax","Brauhm","Voldemort","Abaddon","Tarvok","Sebi Vayah","Whiskeypops","Graycloud","Nalahir","Raven","Kaelem","Colge","Tetravus","Miaobao","Gaston","Gondalf" },
	FemalePlayerNames = {"Kerrigan","Adida","Karma LoD","Danue Sunrider","verve","Achira","Adaline","Amarlia","Amoren","Meldor","Elinari","Selrina","Isha","Prana","Emy","Dixen","squid friend","Jebra","Anzha","Humblemisty","Cyliena","Hermannie","Sea","Ceres","viv","Sanja","Kanyin","Kitty","Eiahn","Miroquin","Tala","Swizzle","Culaan","Marean Lumia","Heather","Celinda Sheridan","JolaughnLLTS","Avanah","Netherwynn","Sylanavan","Aurelia","catsparkle","Felina Covenant","NickyB","Ahina","SilverSong","Sumsare","Hussy","Ali","Seylara Vigdisdottir","Brigitte"},
}