require 'base_ai_npc'

-- Leif (Liz)
-- Torynx (Brandon)
-- Ewok (Dusty)
-- Kai (Adrian)
-- Supreem (Derek)
-- Grim (James)
-- Miphon (Jeffery) 
-- Kade

AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true
AI.Settings.EnableBank = true
AI.Settings.CanConverse = false
AI.Settings.StationedLeash = true

AI.GreetingMessages = 
{
	"Hey I remember you. You have a membership?",
}

-- PRIMARY DIALOG
function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)]

    response = {}
    table.insert(response, { text = "...Yes?", handle = "YesMembership" } )
    table.insert(response, { text = "Don't think so.", handle = "NoMembership" } )
    table.insert(response, { text = "Goodbye.", handle = "" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

-- SECONDARY DIALOG
function Dialog.OpenInformationDialog(user)
    text = "Do you have any questions about the services provided at the lounge?"

    response = {}
    table.insert(response, { text = "Yes, what services are offered?", handle = "Service" } )
    table.insert(response, { text = "Goodbye.", handle = "" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

-- YES SERVICES
function Dialog.OpenServiceDialog(user)
    text = "We offer a variety of services at the lounge. Which interest you?"

    response = {}
    table.insert(response, { text = "Tonics.", handle = "Tonics" } )
    table.insert(response, { text = "I WIN buttons?!?", handle = "Iwinbuttons" } )
    table.insert(response, { text = "I'm good...", handle = "Information" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

-- TONICS
function Dialog.OpenTonicsDialog(user)
    text = "Our tonics are some of the best in the galaxy! I would recommend sampling them all. Be careful though, the effects can be quite -- altering."
    response = {}
    table.insert(response, { text = "Great, thanks.", handle = "Service" } )
    NPCInteractionLongButton(text,this,user,"Responses",response)
end

-- I WIN BUTTONS
function Dialog.OpenIwinbuttonsDialog(user)
    text = "I WIN BUTTONS?!? We run a reputable establishment here! I would suggest getting a tonic and thinking about your choices. May I suggest the Bloody Tiger Fizzy-Pop."
    response = {}
    table.insert(response, { text = "I'm sorry.", handle = "Service" } )
    NPCInteractionLongButton(text,this,user,"Responses",response)
end

-- MEMBERSHIP 
function Dialog.OpenYesMembershipDialog(user)    
    local text = MyTextStrings.IsNotMember
    if (IsCollector(user)) then
        text = MyTextStrings.IsCollector
    elseif (IsFounder(user) or HasPremiumSubscription(user)) then
        text = MyTextStrings.IsFounder
    end

    response = {}
    table.insert(response, { text = "Great, thanks.", handle = "Information" } )
    NPCInteractionLongButton(text,this,user,"Responses",response)
end

-- UNSURE MEMBERSHIP
function Dialog.OpenNoMembershipDialog(user)
    if (IsCollector(user) ) then
        text = MyTextStrings.IsCollector
    elseif (IsFounder(user) or HasPremiumSubscription(user)) then
        text = MyTextStrings.IsFounder
    else
        text = MyTextStrings.IsNotMember
    end
    
    response = {}
    table.insert(response, { text = "Great, thanks.", handle = "Information" } )
    NPCInteractionLongButton(text,this,user,"Responses",response)

end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)

MyTextStrings = 
{
    IsFounder = "You are a member and are most welcome in the lounge."
    , IsCollector = "You are one of the Collectors! I have heard about your kind, but to meet you in person -- amazing! Please... the lounge is at your disposal."
    , IsNotMember = "It does not appear you are a member. I will have to ask you to leave."
}