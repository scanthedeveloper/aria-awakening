local _HttpRequest = HttpRequest
function HttpRequest(url, cb)
    if not( cb ) then cb = function(response) end end
    local requestId = uuid()
    RegisterSingleEventHandler(EventType.HttpResponse, requestId, cb)
    _HttpRequest(url, requestId)
end


HttpRequest("https://www.legendsofaria.com/news/", function(response)
    DebugMessage(response)
end)