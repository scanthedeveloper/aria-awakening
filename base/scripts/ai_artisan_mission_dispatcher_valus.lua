require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false
AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

local currencyName = QuestsLeague.LeagueCurrencyName.Artisans
local hasRewards = this:HasObjVar("HasRewards")

function Dialog.OpenGreetingDialog(user)
        local text = "A good needle and a steady hand will get you far in this world. But a sack full of coins will get you even farther."
        local response = {}
        local key = 1

        if( hasRewards ) then
            text = "Hello there, is there something you'd like to purchase from the League of Artisans? The table here showcases our stocks."
            
            response[key] = {}
            response[key].text = "What can I purchase?"
            response[key].handle = "PurchaseRewards"
            key = key + 1
        end

        --[[
        response[key] = {}
        response[key].text = "Who are you?"
        response[key].handle = "Who"
        key = key + 1
        ]]


        response[key] = {}
        response[key].text = "Goodbye."
        response[key].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
end

function MakeGetRewardsTable(user)
    local Rewards = {
        HairDye = {
            Name = "Artisan Hair Dye",
            Cost = 8000,
            Template = "hair_dye_artisan", -- 958
        },
        ClothDye = {
            Name = "Artisan Cloth Dye",
            Cost = 8000,
            Template = "artisan_clothing_dye",
        },
        LeatherDye = {
            Name = "Artisan Leather Dye",
            Cost = 8000,
            Template = "artisan_leather_dye",
        },
        Cloak = {
            Name = "Artisan League Cloak",
            Cost = 18000,
            Template = "artisan_cloak",
        },
        Necklace = {
            Name = "Artisan's Ring",
            Cost = 36000,
            Template = "pr10_league_artisan_ring",
        },
        Helm = {
            Name = "Artisan's Apron",
            Cost = 36000,
            Template = "pr10_league_artisan_apron",
        },
        TitleScroll = {
            Name = "Title Scroll: 'The Mason'",
            Cost = 24000,
            Template = "pr10_league_artisan_titlescroll",
        },
    }

    return Rewards
end

-- Callback to alter items if needed
function AfterPurchase(item)
    return
end

function Purchase(user, itemKey, Rewards)
    
    -- See if the item is still for sale
    if ( Rewards[itemKey] == nil or Rewards[itemKey].Cost == nil or Rewards[itemKey].Template == nil ) then
        QuickDialogMessage(this,user,"That item is not available right now, unfortunately.")
        return
    end
    
    -- Check to see if the player has enough currency
    if( QuestsLeague.AdjustLeagueCurrency( user, "Artisans", -Rewards[itemKey].Cost ) ) then
        Create.InBackpack(Rewards[itemKey].Template, user, nil, function(item)
            if ( item ) then
                AfterPurchase(item)
                Dialog.OpenPurchaseRewardsDialog(user)
            else
                QuickDialogMessage(this,user,"I'm sorry something went wrong, please try that again.")
            end
        end)
    else
        QuickDialogMessage(this,user,"You do not have enough "..currencyName.." to purchase that.")
    end

end

function SetupPurchaseDialogHandles(user, Rewards)
    -- setup the dialog handles for each item purchase
    for key,val in pairs(Rewards) do
        Dialog[string.format("Open%sConfirmDialog", key)] = function(user)
            Purchase(user, key, Rewards)
        end
        Dialog[string.format("Open%sDialog", key)] = function(user)
            local text = string.format("I can sell you %s for %s "..currencyName..".", val.Name, val.Cost)
            local response = {
                {
                    text = "I accept your offer.",
                    handle = key.."Confirm",
                },
                {
                    text = "What else do you have to sell?",
                    handle = "PurchaseRewards"
                },
                {
                    text = "Goodbye.",
                    handle = ""
                },
            }
            NPCInteractionLongButton(text,this,user,"Responses",response)
        end
    end
end

function Dialog.OpenPurchaseRewardsDialog(user)
    local text = "Which item would you like to purchase?\n\nYou currently have [483D8B]"..QuestsLeague.GetLeagueCurrecy(user,"Artisans").."[-] favor with the League of Artisans."
    local response = {}
    local Rewards = MakeGetRewardsTable(user)

    SetupPurchaseDialogHandles(user, Rewards)

    for x, y in sortpairs(Rewards, function(Rewards, a, b) return Rewards[a]["Name"] < Rewards[b]["Name"] end) do
        table.insert(response, {text = y.Name..": "..y.Cost.." "..currencyName, handle = x})
    end

    table.insert( response, { text = "Goodbye.", handle = "" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenLotteryBoxInfoDialog(user)
    local text = "Each supply box can contain a light, nimble, deft, or mercurial tool for the listed profession or a 'Outfitter' title scroll."
    local response = {}

    table.insert( response, { text = "Thank you.", handle = "Greeting" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

if( not hasRewards ) then

    -- [[ QUEST STUFF ]]
    function AI.QuestStepsInvolvedInFunc()
        local quests = { 1, 3, 5, 7, 9, 11 }
        return 
        {
            {"ArtisansLeaguePR10", quests[math.random(#quests)]} ,
            {"ArtisansLeaguePR10", quests[math.random(#quests)]} ,
            {"ArtisansLeaguePR10", quests[math.random(#quests)]} ,
            {"ArtisansLeagueValus", 2 },
            {"ArtisansLeagueValus", 4 },
            {"ArtisansLeagueValus", 6 },
            {"ArtisansLeagueValus", 8 },
            {"ArtisansLeagueValus", 10 },
            {"ArtisansLeaguePR9", 2 },
            {"ArtisansLeaguePR9", 4 },
            {"ArtisansLeaguePR9", 6 },
            {"ArtisansLeaguePR9", 8 },
            {"ArtisansLeaguePR9", 10 },
            {"ArtisansLeaguePR9", 12 },
            {"ArtisansLeaguePR10", 2 },
            {"ArtisansLeaguePR10", 4 },
            {"ArtisansLeaguePR10", 6 },
            {"ArtisansLeaguePR10", 8 },
            {"ArtisansLeaguePR10", 10 },
            {"ArtisansLeaguePR10", 12 },

        }
    end

    AI.QuestsWelcomeText ="I am in need of a skilled fabricator to help me. Decent pay, long hours, and honest work. You interested?"

    AI.QuestAvailableMessages = 
    {
        "I've a job for you to do.",
    }

    function Dialog.OpenWhoDialog(user)
        QuickDialogMessage(this,user,"I am a simple craftsman who loves the smell of curing hides and freshly picked cotton. Oh, and sacks of coins... sacks and sacks of coins.")
    end

end