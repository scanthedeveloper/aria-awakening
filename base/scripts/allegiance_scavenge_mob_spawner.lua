require 'incl_gametime'

local isDungeon = IsDungeonMap()

-- the delay between spawn attempts, can be overridden by adding "spawnDelay" objvar
local DEFAULT_DELAY_SECS = 180
-- the delay between spawn amounts, can be overridden by adding "spawnCount" objvar
local DEFAULT_SPAWN_COUNT = 6
local DEFAULT_SPAWN_CHANCE = 100
local DEFAULT_PULSE_FREQ_SECS = 10
local SPAWN_RADIUS = this:GetObjVar("spawnRadius") or 150

function OnMobKill(mobileObj)
    --validate the dying mob and its spawner
    local Spawner = mobileObj:GetObjVar("Spawner")
    if ( Spawner == nil or not Spawner:IsValid() ) then return false end
    local damagersTable = mobileObj:GetObjVar("Damagers")
    if ( damagersTable == nil ) then return false end

    for damager, v in pairs(damagersTable) do
        --damager is valid
        if ( damager:IsValid() ) then
            --damager is nonpossessed player
            if ( damager:IsPlayer() and not IsPossessed(damager) ) then
                --damager is in valid militia
                local militiaId = damager:GetObjVar("Militia")
                if ( militiaId ~= nil and militiaId ~= 0 ) then
                    --damager has a valid amount
                    if ( v.Amount ~= nil and v.Amount > 0 ) then
                        local amount = math.round(v.Amount/10)
                        Spawner:SendMessage("AddEventPoints", damager, amount, militiaId)
                        --damager:NpcSpeechToUser("+"..tostring(v.Amount).." Event Points", damager, "combat")
                    end
                end
            end
        end
    end
end

function SetMaxSpawnCount()
    local MilitiaPlayersInRadius = FindObjects(SearchMulti(
	{
		SearchMobileInRange(SPAWN_RADIUS),
        SearchHasObjVar("Favor"),
        SearchHasObjVar("Militia"),
    }))
    local newCount = DEFAULT_SPAWN_COUNT --+ #MilitiaPlayersInRadius
    this:SetObjVar("spawnCount", newCount)
end

function GetSpawnPulse()
    local delaySecs = this:GetObjVar("spawnPulseSecs") or DEFAULT_PULSE_FREQ_SECS
    return TimeSpan.FromSeconds(delaySecs + math.random())
end

function GetSpawnDelay()
    local delaySecs = this:GetObjVar("spawnDelay") or DEFAULT_DELAY_SECS
    return TimeSpan.FromSeconds(delaySecs + math.random())
end

function GetSpawnLoc()
    local spawnRegion = nil
    if(this:HasObjVar("spawnRegions")) then
        -- DAB TODO: Weight based on size of region
        local spawnRegions = this:GetObjVar("spawnRegions")
        spawnRegion = spawnRegions[math.random(1,#spawnRegions)]
    else
        spawnRegion = this:GetObjVar("spawnRegion")
    end

    if(spawnRegion) then
        if(isDungeon) then
            return GetRandomDungeonSpawnLocation(spawnRegion)
        else
            return GetRandomPassableLocation(spawnRegion,true)
        end
    end

    local spawnRadius = this:GetObjVar("spawnRadius")
    if(spawnRadius) then
        return GetRandomPassableLocationInRadius(this:GetLoc(), spawnRadius, true)
    end    

    return this:GetLoc()
end

function GetCurrentWinningMilitia()
    local t = this:GetObjVar("Score") or {}
    for k, v in sortpairs(t, function(t,a,b) return t[b] < t[a] end) do
        if ( v > 0 ) then
            return k
        else
            return 0
        end
        return
    end
end

function ShouldSpawn(spawnData, spawnIndex)
    local isValid = spawnData[spawnIndex].ObjRef and spawnData[spawnIndex].ObjRef:IsValid()
    local isDead = isValid and spawnData[spawnIndex].ObjRef and spawnData[spawnIndex].ObjRef:IsMobile() and IsDead(spawnData[spawnIndex].ObjRef)

    -- the mob is still on the map and hes not a pet, no spawn
    if( isValid and not(isDead) and not(IsPet(spawnData[spawnIndex].ObjRef)) ) then
        return false
    end
    
    local spawnDelay = GetSpawnDelay(spawnInfo)
    --DebugMessageB(this,"Checking spawnDelay "..tostring(spawnDelay.TotalSeconds))
    if(spawnDelay.TotalSeconds ~= 0) then
        -- this mob just disappeared or died
        local goneTime = spawnData[spawnIndex].GoneTime
        if( spawnData[spawnIndex].ObjRef and not(goneTime) ) then  
            DebugMessageB(this,"MOB DEAD OR DESPAWNED")   
            spawnData[spawnIndex].GoneTime = DateTime.UtcNow
            return false
        end

        -- are we waiting on the spawn timer
        if(goneTime and DateTime.UtcNow < (goneTime + spawnDelay)) then
            --DebugMessageB(this,"Waiting on spawn timer")   
            return false
        end
    end

    -- roll to spawn
    local spawnChance = this:GetObjVar("spawnChance") or DEFAULT_SPAWN_CHANCE
    if(spawnChance < 1 and math.random() > spawnChance) then   
        spawnData[spawnIndex].GoneTime = DateTime.UtcNow
        return false
    end

    return true
end

function CheckSpawn()
    -- add some randomness so there arent all spawning at the same time
    this:ScheduleTimerDelay(GetSpawnPulse(), "spawnTimer")

    local active = this:GetObjVar("Active")
    if ( active == nil or active ~= 1 ) then return end

    local spawnData = this:GetObjVar("spawnData")        
    local spawnCount = this:GetObjVar("spawnCount") or DEFAULT_SPAWN_COUNT

    local templateId = nil
    local spawnTable = this:GetObjVar("spawnTable")
    if(spawnTable) then
        local totalWeight = 0
        for i,spawnEntry in pairs(spawnTable) do
            totalWeight = totalWeight + (spawnEntry.Weight or 1)
        end

        local weightRoll = math.random() * totalWeight
        totalWeight = 0
        for i,spawnEntry in pairs(spawnTable) do
            if( weightRoll <= totalWeight + spawnEntry.Weight ) then
                templateId = spawnEntry.Template
                break
            end
            totalWeight = totalWeight + (spawnEntry.Weight or 1)
        end        
    end

    if not(templateId) then
        templateId = this:GetObjVar("spawnTemplate")
    end

    if( templateId == nil ) then
        DebugMessage("ERROR: militia scavenge mob spawner picked invalid template: "..tostring(this.Id).. " SpawnerTemplate: "..tostring(this:GetCreationTemplateId()))
        return
    end
    
    if (spawnData ~= nil) then
        if (this:HasObjVar("NightSpawn")) then
            if (IsDayTime()) then
                local playerAround = not (#FindObjects(SearchPlayerInRange(20)) == 0)
                if not playerAround then
                    for i=1,#spawnData do
                        if (spawnData[i].ObjRef ~= nil and spawnData[i].ObjRef:IsValid()) then
                            spawnData[i].ObjRef:Destroy()
                            spawnData[i].GoneTime = DateTime.UtcNow
                        end
                    end
                end
                return
            end
        elseif (this:HasObjVar("DaySpawn")) then
            if (IsNightTime()) then 
                local playerAround = not (#FindObjects(SearchPlayerInRange(20)) == 0)
                if not playerAround then
                    for i=1,#spawnData do
                        if (spawnData[i].ObjRef ~= nil and spawnData[i].ObjRef:IsValid()) then
                            spawnData[i].ObjRef:Destroy()
                            spawnData[i].GoneTime = DateTime.UtcNow
                        end
                    end
                end
                return
            end
        end
    end
    -- NOTE: If spawn count is made smaller, we are not destroying the extra mobs 
    if spawnData == nil then
        spawnData = {}
    --elseif #spawnData > spawnCount then
    	--for i=spawnCount+1,#spawnData do
           -- table.remove(spawnData,i)
       -- end
    end        

    --DebugMessage("---CheckSpawn SpawnCount: "..spawnCount.." spawnChance: "..tostring(spawnChance))     

        --DebugMessage("SPAWNING")
        for i=1, spawnCount do
            --DebugMessage("---Checking i="..i)     
        if(spawnData[i] == nil) then spawnData[i] = {} end
        if( ShouldSpawn(spawnData, i) ) then   
                --DebugMessage("---Create "..templateId)     
                local spawnLoc = GetSpawnLoc()
                if(spawnLoc) then
                	CreateObj(templateId, spawnLoc, "mobSpawned", i)
                    -- DAB NOTE: SHOULD WE ONLY SPAWN ONE PER PULSE? AND SHOULD WE ONLY ROLL ONCE?
                	break
                end
            end
        end

    this:SetObjVar("spawnData", spawnData)
    end

RegisterEventHandler(EventType.Message,"Activate",function ( ... )
    this:SetObjVar("Active", 1)
end)

RegisterEventHandler(EventType.Message,"Deactivate",function ( ... )
    this:SetObjVar("Active", 0)
end)

RegisterEventHandler(EventType.Message,"DestroyAllSpawns",function()
    local spawnData = this:GetObjVar("spawnData")
    if ( spawnData == nil ) then return false end

    for i,spawnInfo in pairs(spawnData) do
        if ( spawnInfo.ObjRef ) then
            spawnInfo.ObjRef:Destroy()
            spawnInfo.ObjRef = nil
            spawnInfo.GoneTime = DateTime.UtcNow
        end
    end
    this:SetObjVar("spawnData",spawnData)
end)

RegisterSingleEventHandler(EventType.ModuleAttached, "militia_scavenge_mob_spawner", 
	function()
        if(initializer and initializer.SpawnTable) then
            this:SetObjVar("spawnTable",initializer.SpawnTable)
        end
        if(initializer and initializer.SpawnObjVars) then
            this:SetObjVar("spawnObjVars",initializer.SpawnObjVars)
        end
		CheckSpawn()
	end)

function MobSpawned(success, objref, index)
    if( success) then
        local spawnData = this:GetObjVar("spawnData") 
        if(spawnData[index]) then
            spawnData[index].ObjRef = objref
            spawnData[index].GoneTime = nil
        else
            spawnData[index] = { ObjRef = objref }
        end            
        this:SetObjVar("spawnData",spawnData)
        objref:SetObjVar("Spawner",this)
        if(objref:IsMobile()) then
            if(this:HasObjVar("spawnRadius")) then
                objref:SetFacing(math.random() * 360)
            else
                objref:SetFacing(this:GetFacing())
            end
            local winning = GetCurrentWinningMilitia()
            if ( winning == nil ) then
                --nada
            elseif ( winning ) == 1 then
                objref:SetName("Spirit of Pyros")
            elseif ( winning ) == 2 then
                objref:SetName("Spirit of Tethys")
            elseif ( winning ) == 3 then
                objref:SetName("Spirit of Ebris")
            end
        end

        local spawnObjVars = this:GetObjVar("spawnObjVars")
        if(spawnObjVars) then
            for varName,varData in pairs(spawnObjVars) do
                objref:SetObjVar(varName,varData)
            end
        end
    end
end

-- this allows the function to be overridden in a mod
RegisterEventHandler(EventType.CreatedObject, "mobSpawned", function(...) MobSpawned(...) end)

RegisterEventHandler(EventType.Timer, "spawnTimer", 
    function()
        SetMaxSpawnCount()
		CheckSpawn()
	end)

this:ScheduleTimerDelay(GetSpawnPulse(), "spawnTimer")

RegisterEventHandler(EventType.Message, "MobHasDied",	
    function (objRef)
        if ( IsDead(objRef) ) then
            OnMobKill(objRef)
			if ( objRef == this:GetObjVar("spawnRef") ) then
                this:DelObjVar("spawnRef")
            end
		end
    end)