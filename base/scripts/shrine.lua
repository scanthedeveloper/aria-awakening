
function ValidPlayer(player)
    --Overriding karma check and return true if this shrine has objvar ReviveAll
    if ( this:HasObjVar("ReviveAll") ) then return true end
    return not IsMurderer(player)
end

function ValidateUse(user)
	local topmostObj = this:TopmostContainer() or this
	if ( topmostObj:GetLoc():Distance(user:GetLoc()) > OBJECT_INTERACTION_RANGE ) then    
        user:SystemMessage("Cannot reach that.", "info")  
        return false
    end

    if not( ValidPlayer(user) ) then 
        user:SystemMessage("Murderers cannot use this resurrect stone.", "info")
        return false
    end

    if not( IsDead(user) ) then
        user:SystemMessage("[$2523]","info")
        return false
    end

    return true
end

RegisterEventHandler(EventType.Message, "UseObject", function(user,usedType)
    if ( usedType ~= "Resurrect" ) then return end

    if ( ValidateUse(user) ) then
        user:SendMessage("Resurrect", 0.65)
    end
end)

RegisterSingleEventHandler(EventType.ModuleAttached, "shrine", function()
    SetTooltipEntry(this,"shrine","[$2524]")
    AddUseCase(this,"Resurrect",true)
end)

this:SetObjVar("UseableWhileDead",true)

-- when a player forces the client closed, they will not complete the region transfer.
-- This ensures a player must be next to a resurrection shrine to clear the force transfer
RegisterEventHandler(EventType.EnterView, "ClearTransferForce", function(player)
    -- player is now next to a shrine, delete this objvar
    player:DelObjVar("DeathShrineClear")
    local livingScale = player:GetObjVar("LivingScale")
    if ( livingScale ~= nil ) then
        player:SetScale(livingScale)
        player:DelObjVar("LivingScale")
    end
end)
AddView("ClearTransferForce", SearchHasObjVar("DeathShrineClear", 20), 0.2)