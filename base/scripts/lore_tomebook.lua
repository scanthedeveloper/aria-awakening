function OpenTomebook(user, tomebook)
	user:SendMessage("StartMobileEffect", "Tomebook", nil, {TomeId = tomebook:GetObjVar("TomeId"), Entries = tomebook:GetObjVar("Entries")})
end

function UpdateNameAndTooltip(entries, tomeData)
	entries = entries or this:GetObjVar("Entries") or {}
	tomeData = tomeData or AllTomes[this:GetObjVar("TomeId")]
	local collection = AllTomeCollections[this:GetObjVar("CollectionIndex")]
	RemoveTooltipEntry(this,"summary")
	local isValidTomebook = next(entries) and tomeData and collection
	if isValidTomebook then
		local isComplete = true
		for i=1, #tomeData.Entries do
			if not entries[i] then entries[i] = 0 end
			if entries[i] == 0 then isComplete = false end
		end

		this:SetObjVar("Blessed", true)
		SetTooltipEntry(this, "Blessed", "Blessed", 999993)
		SetTooltipEntry(this,"collection", "'"..collection[1].."'' Collection, Tome #"..tostring(this:GetObjVar("TomeIndex") or "X"), 50)
		if isComplete then
			local collectionColor = collection.TitleColor or "FFFFFF"
			this:SetName("["..collectionColor.."]"..tomeData.Name.."[-]")
			this:SetObjVar("IsComplete",true)
			--SetTooltipEntry(this,"summary", tomeData.CompleteTooltip)
		else
			this:SetName(tomeData.Name)
			this:DelObjVar("IsComplete")
		end
		if tomeData.Tooltip and tomeData.Tooltip ~= "" then
			SetTooltipEntry(this,"summary", tomeData.Tooltip)
		end
	else
		this:DelObjVar("Blessed")
		RemoveTooltipEntry(this,"Blessed")
		this:DelObjVar("IsComplete")
		this:SetName("Empty Tomebook")
	end
end

function AddEntries(user, id, entryIndices)
	local tomeId = this:GetObjVar("TomeId")
	if not this:HasObjVar("Entries") then InitEntries() end

	if tomeId == id then
		local tomeData = AllTomes[tomeId]
		local entries = this:GetObjVar("Entries")
		for i=1, #entryIndices do
			local entryIndex = entryIndices[i]
			if tomeData.Entries[entryIndex] and entries[entryIndex] == 0 then
				entries[entryIndex] = 1
			end
		end
		this:SetObjVar("Entries", entries)
		UpdateNameAndTooltip(entries, tomeData)
	end
	if HasMobileEffect(user, "Tomebook") then
		user:SendMessage("StartMobileEffect", "Tomebook", nil, {TomeId = id, Entries = entries})
	end
end

function InitEntries()
	local tomeData = AllTomes[this:GetObjVar("TomeId")] or {}
	local entryCount = tomeData.Entries and #tomeData.Entries or 0

	local emptyEntries = {}
	for i=1, entryCount do
		emptyEntries[i] = 0
	end

	this:SetObjVar("Entries", emptyEntries)
end

RegisterEventHandler(EventType.LoadedFromBackup,"", function()
	
	--this:SetSharedObjectProperty("Text", this:GetObjVar("TomeIndex") or "")
	
	UpdateNameAndTooltip()
end)

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()
	AddUseCase(this,"Open", true)

	this:SetSharedObjectProperty("Text", this:GetObjVar("TomeIndex") or "")
	
	UpdateNameAndTooltip()
end)

RegisterEventHandler(EventType.Message, "UseObject", function(user,usedType)
	if ( usedType == "Open" or usedType == "Use" ) then
		this:PlayObjectSound("Use", true)
		OpenTomebook(user, this)
	elseif usedType == "UpdateName" then
		UpdateNameAndTooltip()
	end
end)

RegisterEventHandler(EventType.Message, "UpdateHue", function(user)
	local collectionData = AllTomeCollections[this:GetObjVar("CollectionIndex")]
	if collectionData then
		this:SetHue(collectionData.BookHue)
	else
		DebugMessage("CollectionData failed")
	end
end)

RegisterEventHandler(EventType.Message, "AddEntries", function(user, tomeId, entryIndices)
	if this:HasObjVar("TomeId") then
		if this:GetObjVar("TomeId") ~= tomeId then DebugMessage("[tomebook] AddEntries() failed! TomeIds do not match:",this:GetObjVar("TomeId"),tomeId) return end	
		AddEntries(user, tomeId, entryIndices)
	else
		this:SetObjVar("TomeId", tomeId)
		InitEntries()
		AddEntries(user, tomeId, entryIndices)
	end
end)

--[[ If we add new entries to previous tomes, add functionality to let you drag one tomebook onto another to merge them, return empty tomebook 
RegisterEventHandler(EventType.Message,"HandleDrop", function (user,droppedObject)
	
end)]]