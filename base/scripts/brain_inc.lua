
mSkipAddCombatModule = true -- prevent base_mobile from attaching combat module

function SetCurrentTarget(newTarget)
    if ( mCurrentTarget ~= newTarget ) then
        mCurrentTarget = newTarget
        this:SetObjVar("CurrentTarget", mCurrentTarget)
		this:SendMessage("CurrentTargetUpdate", mCurrentTarget)
    end
    if ( not mCurrentTarget and mInCombatState ) then
        this:DelObjVar("CurrentTarget")
        EndCombat()
    end
end

SetInCombat(false)
SetCurrentTarget(nil)

OverrideEventHandler("combat", EventType.Message, "SetCurrentTarget", SetCurrentTarget)