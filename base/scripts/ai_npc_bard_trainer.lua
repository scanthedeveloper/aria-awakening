require 'base_ai_npc'

AI.Settings.StationedLeash = true

AI.QuestStepsInvolvedIn = {
    {"BardProfessionIntro", 1},
    {"BardProfessionIntro", 2},
    {"BardProfessionIntro", 3},
    {"BardProfessionIntro", 6},
    {"BardProfessionIntro", 8},
    {"BardProfessionIntro", 10},

    {"BardProfessionTierOne", 1},
    {"BardProfessionTierOne", 2},
    {"BardProfessionTierOne", 3},

    {"BardProfessionTierTwo", 1},
    {"BardProfessionTierTwo", 3},
    {"BardProfessionTierTwo", 6},
    {"BardProfessionTierTwo", 8},

    {"BardProfessionTierThree", 1},
    {"BardProfessionTierThree", 3},
    {"BardProfessionTierThree", 6},

    {"BardProfessionTierFour", 1},
    {"BardProfessionTierFour", 3},
    {"BardProfessionTierFour", 6},
}

AI.GreetingMessages = 
{
    "Hi there, what can I do for you?",
}

function Dialog.OpenTalkDialog(user)
    --QuickDialogMessage(this,user,"Will we be ready?")
end

function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,
        "I am a performer! If you are interested in learning an instrument or some songs, I can give you some pointers."
        )
    --QuickDialogMessage(this,user,"REPLACED -> I am a quest guy!")
end

function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)] or AI.GreetingMessages[1]

    response = {}

    response[1] = {}
    response[1].text = "Who are you?"
    response[1].handle = "Who" 
    
    response[2] = {}
    response[2].text = "Goodbye."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)