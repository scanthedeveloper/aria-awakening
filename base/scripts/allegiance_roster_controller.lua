require 'globals.lua.extensions'

--- Saves this controller's gameObj ID to global
function SaveControllerIDtoGlobal()
  local militiaId = this:GetObjVar("Militia")
  if ( militiaId == nil ) then return false end

  SetGlobalVar(string.format("Militia.%s.%s", "RosterController", militiaId),
  function(record)
    record["ID"] = this
    return true
  end,
  function(success)
    if ( success ) then
      --DebugMessage("Militia roster controller ID "..tostring(this).." saved to globals")
    end
  end)
end

RegisterEventHandler(EventType.Message,"AdjustFavorTable",
--- Updates FavorTable on this controller with for a player and creates a time(from current+server setting) when the player would be considered inactive, optionally removes their entry instead
-- @param playerObj
-- @param favor, favor value to set to
-- @param (optional) remove, set true to remove player from the favor table instead
function (playerObj, favor, remove)
  if ( playerObj == nil or favor == nil ) then return false end
  local favorTable = this:GetObjVar("FavorTable") or {}--could this clear the roster if it exists?
  if ( remove ) then
    favorTable[playerObj] = nil
  else
    favorTable[playerObj] = {
      favor,
      DateTime.UtcNow:Add(ServerSettings.Militia.InactiveTime),
    }
  end

  this:SetObjVar("FavorTable", favorTable)
end)

--- Gets and sorts favor data from controller's FavorTable ObjVar, rewrites global Standings var with current rank and percentile info
function CalculateAndUpdateRanks()
  local standingTable = SortRosterByFavor()
  if ( standingTable == nil ) then return false end

  local militiaId = this:GetObjVar("Militia")
  if ( militiaId == nil ) then return false end

  local update = {}
  SetGlobalVar(string.format("Militia.%s.%s", "Standings", militiaId),
  function(record)
    record["SeasonStartDate"] = nil
    record["CalculationDate"] = nil
    for playerObj, rankData in pairs(record) do
      record[playerObj] = nil
      update[playerObj] = 1
    end
    for playerObj, rankData in pairs(standingTable) do
      record[playerObj] = rankData
      update[playerObj] = 1
    end

    local seasonStartDate = GlobalVarReadKey("Militia.CurrentSeason", "StartDate")
    record["SeasonStartDate"] = seasonStartDate
    local calculationDate = DateTime.UtcNow
    record["CalculationDate"] = calculationDate

    return true
  end,
  function(success)
    if ( success ) then
      if ( update ~= nil ) then
        for k, v in pairs(update) do
          k:SendMessageGlobal("UpdateMilitiaObjVars")
          
        end
      end
      DebugMessage("Militia #"..militiaId.." standings calculation successful and saved to globals")
    end
  end)
end
RegisterEventHandler(EventType.Message,"CalculateAndUpdateRanks",function()CalculateAndUpdateRanks()end)

RegisterEventHandler(EventType.Message,"WipeFavorTable",
function()
    local favorTable = this:GetObjVar("FavorTable")
    if not( favorTable ) then
      DebugMessage("Couldn't get FavorTable ObjVar on "..tostring(this))
      return false
    end
    local deleteResult = this:DelObjVar("FavorTable")
    if ( deleteResult ) then
      DebugMessage("Wiping of FavorTable ObjVar successful on "..tostring(this))
      CalculateAndUpdateRanks()
      return true
    else
      DebugMessage("Wiping of FavorTable ObjVar failed on "..tostring(this))
      return false
    end
end)

function SortRosterByFavor()
  local t = this:GetObjVar("FavorTable") or {}
  --remove inactive players
  local now = DateTime.UtcNow
  for playerObj, rankData in pairs(t) do
    if ( rankData[2] == nil or now > rankData[2] ) then
      t[playerObj] = nil
    end
  end
  local length = CountTable(t)
  local standing = length
  local count = 1
  for playerObj, rankData in sortpairs(t, function(t, a, b) return t[a][1] < t[b][1] end) do
    t[playerObj] = {math.round(count/length, 2), standing, rankData[1]}
    standing = standing - 1
    count = count + 1
  end
  return t
end

function OnLoad()
  SaveControllerIDtoGlobal()
  CalculateAndUpdateRanks()
end
RegisterEventHandler(EventType.LoadedFromBackup,"",function()OnLoad()end)