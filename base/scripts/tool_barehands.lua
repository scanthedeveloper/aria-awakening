require 'base_tool_resourceharvester'

QUALITY_IMPROVEMENT_SKILL_THRESHOLD = 40
FAIL_CHANCE_SKILL_THRESHOLD = 30
BASE_UPGRADE_CHANCE = 10
ResourceHarvester.ToolType = "BareHands"
ResourceHarvester.HarvestAnimation = "forage"
ResourceHarvester.HarvestStopAnimation = "kneel_standup"
-- 0 means the animation does not loop
ResourceHarvester.HarvestAnimationDurationSecs = 0
ResourceHarvester.DefaultHarvestDelaySecs = 2

RegisterSingleEventHandler(EventType.ModuleAttached,"tool_barehands",
	function ()
		ResourceHarvester.Initialize()
	end)

mUser = nil

ResourceHarvester.CollectResource = function(user,resourceType)
	local backpackObj = user:GetEquippedObject("Backpack")  
	if( backpackObj ~= nil ) then		
		
		-- see if the user gets an upgraded version
		resourceType = GetHarvestResourceType(user,resourceType)
		if (resourceType == nil) then return end
			--DebugMessage("ResType:" .. resourceType)
    		--DebugTable(ResourceData.ResourceInfo[resourceType])
		-- try to add to the stack in the players pack

		local resourceData = nil
		--SCAN ADDED FORAGING SKILL
		----------------------------------------------------------------------------------------
		local foraging = GetSkillLevel(user,"ForagingSkill")	
				
		--NOVICE
		local stackAmount = 1
		local displayName = GetResourceDisplayName(resourceType)
		local foragingBonus = 0

		if ( ResourceData.ResourceInfo[resourceType] ) then
			--NOVICE
			if(foraging > -1 ) and (foraging < 30.0 ) then 
				stackAmount = 1
				--user:SystemMessage("[FF9900]Apprentice Bonus - [-] "..stackAmount.."", "info")
				user:NpcSpeech("[FFFFFF]+"..stackAmount.." "..displayName.."[-]","combat")
				CheckSkillChance(user,"ForagingSkill",GetSkillLevel(user,"ForagingSkill"),0.5)
			end
			--APPRENTICE
			if(foraging > 29.99 ) and (foraging < 50.0 ) then 
				foragingBonus = math.random(1,4)
				stackAmount = stackAmount + foragingBonus
				--user:SystemMessage("[FF9900]Apprentice Bonus - [-] "..stackAmount.."", "info")
				user:NpcSpeech("[FFFFFF]+"..stackAmount.."[-] "..displayName.."[-]","combat")
				user:NpcSpeech("[00FF00] +"..foragingBonus.." Apprentice Bonus[-]","combat")
				CheckSkillChance(user,"ForagingSkill",GetSkillLevel(user,"ForagingSkill"),0.5)
			end
			--JOURNEYMAN
			if(foraging > 49.9 ) and (foraging < 80.0 ) then 
				foragingBonus = math.random(2,5)
				stackAmount = stackAmount + foragingBonus
				--user:SystemMessage("[FF9900]Journeyman Bonus - [-] "..stackAmount.."", "info")
				user:NpcSpeech("[FFFFFF]+"..stackAmount.."[-] "..displayName.."[-]","combat")
				user:NpcSpeech("[00FF00] +"..foragingBonus.." Journeyman Bonus[-]","combat")
				CheckSkillChance(user,"ForagingSkill",GetSkillLevel(user,"ForagingSkill"),0.5)
			end
			--MASTER
			if(foraging > 79.9) and (foraging < 100.0 ) then 
				foragingBonus = math.random(3,6)
				stackAmount = stackAmount + foragingBonus
				--user:SystemMessage("[FF9900]Master Bonus - [-] "..stackAmount.."", "info")
				user:NpcSpeech("[FFFFFF]+"..stackAmount.."[-] "..displayName.."[-]","combat")
				user:NpcSpeech("[00FF00] +"..foragingBonus.." Master Bonus[-]","combat")
				CheckSkillChance(user,"ForagingSkill",GetSkillLevel(user,"ForagingSkill"),0.5)
			end
			--GRANDMASTER
			if(foraging == 100.00) then 
				foragingBonus = math.random(4,10)
				stackAmount = stackAmount + foragingBonus 
				--user:SystemMessage("[FF9900]Grandmaster Bonus - [-] "..stackAmount.."", "info")
				user:NpcSpeech("[FFFFFF]+"..stackAmount.."[-] "..displayName.."[-]","combat")
				user:NpcSpeech("[00FF00] +"..foragingBonus.." Grandmaster Bonus[-]","combat")
			end
			--GOD 100 HARVESTING
			if(foraging > 100.00) then 
				foragingBonus = 100
				stackAmount = stackAmount + foragingBonus 
				--user:SystemMessage("[FF9900]Grandmaster Bonus - [-] "..stackAmount.."", "info")
				user:NpcSpeech("[FFFFFF]+"..stackAmount.."[-] "..displayName.."[-]","combat")
				user:NpcSpeech("[00FF00] +"..foragingBonus.." GOD Bonus[-]","combat")
			end
		else
			stackAmount = math.random(1,2)
			user:NpcSpeech("[FFFFFF]+"..stackAmount.." "..displayName.."[-]","combat")
		end

		--BONUS SKILL CHECK
		CheckSkillChance(user,"ForagingSkill",GetSkillLevel(user,"ForagingSkill"),0.5)

		--CREATE BONUS
		Create.Stack.InBackpack( ResourceData.ResourceInfo[resourceType].Template, user, stackAmount )
		----------------------------------------------------------------------------------------
	    --local displayName = GetResourceDisplayName(resourceType)
	    user:SystemMessage("You harvest "..stackAmount.." "..displayName..".","info")
		mUser = user
		--user:NpcSpeech("[F4FA58]+"..stackAmount.." "..displayName.."[-]","combat")
		Quests.SendQuestEventMessage( user, "Harvesting", { "BareHands", resourceType }, stackAmount )
	end	
end

base_CompleteHarvest = ResourceHarvester.CompleteHarvest
ResourceHarvester.CompleteHarvest = function(objRef,user)
	-- make sure the user is still valid
	if( user == nil or not(user:IsValid()) ) then return end
	
	--DebugMessage("CompleteHarvest "..tostring(objRef) .. ", "..tostring(harvestQueued))
	
	local resInfoTab = GetResourceSourceInfo(objRef)

	if(resInfoTab == nil) then
		local sourceId = GetResourceSourceId(objRef)
		local name = ((objRef ~= nil and objRef:IsValid() and not(objRef:IsPermanent())) and objRef:GetName()) or ""
		local IsPermanent = (objRef ~= nil and objRef:IsValid() and (objRef:IsPermanent()))
		LuaDebugCallStack("ERROR: CompleteHarvest called with no source info Object: "..tostring(objRef).." SourceId: "..tostring(sourceId).. " Name: "..name.." IsPermanent: "..tostring(IsPermanent))
		user:DelObjVar("IsHarvesting")
		return
	end

	base_CompleteHarvest(objRef,user,1)
end

RegisterEventHandler(EventType.CreatedObject, "create_foraging_harvest", 
	function(success, objRef, amount)
		--DebugMessage("Created Object")
		if(success == true) then
			SetItemTooltip(objRef)
			local resName = objRef:GetObjVar("ResourceType")
			if(resName == nil) then return end
			if(amount < 2) then return end
		--local backpackObj = this:GetEquippedObject("Backpack")
			RequestSetStack(objRef,amount)
		end
	end)