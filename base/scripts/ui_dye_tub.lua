squareLength = 30
squareGap = 6

function GetTubType()
	return dyeTub:GetObjVar("TubType") or "ClothingBasic"
end

function GetTubHue(hueRow, hueColumn)
	local colorTable = "DyeColorTypesClothingBasic"
	if ( tubType == "ClothingBasic" ) then
		colorTable = "DyeColorTypesClothingBasic"
	end
	return CharacterCustomization[colorTable][hueRowSelected][hueColumnSelected] or 0
end

function OpenDyeTubWindow()
	local hueTable = nil
	if (tubType == "ClothingBasic") then
		hueTable = CharacterCustomization.DyeColorTypesClothingBasic
	end

	local rowLength = 0
	local columnLength = #hueTable
	for row = 1, #hueTable do
		if ( #hueTable[row] > rowLength ) then
			rowLength = #hueTable[row]
		end
	end

	local buttonWidth = 122
	local buttonHeight = 32
	local curX = 30
	local curY = 30
	local windowWidth = math.max( 
		(squareLength*rowLength)--squares
		+ (squareGap*(rowLength-1))--gaps
		+ (curX*2)--x offset
		+ 22--window border
	,--OR (whichever is wider)
		(buttonWidth)--button
		+ (curX*2)--x offset
		+ 22--window border
	)
	local windowHeight = (squareLength*columnLength)--squares
	+ (squareGap*(columnLength-1))--gaps
	+ (curY*2)--y offset
	+ (buttonHeight*2)--space for two buttons
	+ 54--window border
	local buttonX = (windowWidth/2)-(buttonWidth/2)-10
	local buttonY = windowHeight-buttonHeight-54
	
	local dynWindow = DynamicWindow("DyeTubWindow","Dye Tub",windowWidth,windowHeight,0,0,"Default","Center")
	
	dynWindow:AddButton(buttonX, buttonY-buttonHeight, "Confirm", "Confirm", buttonWidth, buttonHeight, "", "", true,"")
	dynWindow:AddButton(buttonX, buttonY, "Cancel", "Cancel", buttonWidth, buttonHeight, "", "", true,"")

	for rowIndex, hueRow in pairs(hueTable) do
		for hueIndex = 1, #hueRow do
			local state = ""
			if ( hueRowSelected == rowIndex
			and hueColumnSelected == hueIndex ) then
				state = "pressed"
			end
			dynWindow:AddButton(curX + (hueIndex - 1) * (squareLength + squareGap) - 4, curY + (rowIndex - 1) * (squareLength + squareGap) - 4,tostring(rowIndex).."|"..tostring(hueIndex),"",squareLength+8,squareLength+8,"","",false,"OutlineButton",state)
			dynWindow:AddImage(curX + (hueIndex - 1) * (squareLength + squareGap), curY + (rowIndex - 1) * (squareLength + squareGap), "HueSwatch",squareLength,squareLength,"",nil,hueRow[hueIndex])
		end
	end

	this:OpenDynamicWindow(dynWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse, "DyeTubWindow",
	function (user,buttonId)
		if ( buttonId == "Confirm" ) then
			if ( dyeTub == nil or not dyeTub:IsValid() ) then
				user:SystemMessage("Can't find the dye tub!","info")
			end
			if ( dyeHue ) then
				dyeTub:SetObjVar("DyeHue", dyeHue)
				dyeTub:SetHue(dyeHue)
			end
			user:DelModule("ui_dye_tub")
		elseif ( buttonId == "Cancel" or buttonId == "" ) then
			user:DelModule("ui_dye_tub")
		else
			local split = StringSplit(buttonId,"|")
			local row = tonumber(split[1])
			local column = tonumber(split[2])
			if ( row and column ) then
				hueRowSelected = row
				hueColumnSelected = column
				dyeHue = GetTubHue()
				OpenDyeTubWindow()
			end
		end
	end)
RegisterEventHandler(EventType.Message, "ChangeTub",
	function(user,tub)
		if ( user == nil or not user:IsValid() or tub == nil or not tub:IsValid() ) then
			return
		end

		dyeTub = tub
		tubType = GetTubType()
		hueRowSelected = 0
		hueColumnSelected = 0

		OpenDyeTubWindow()
	end)