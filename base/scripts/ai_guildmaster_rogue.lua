require 'base_ai_npc'

AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = true
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

function GetPrestigeClass()
    return this:GetObjVar("PrestigeClass") or ""
end
local prestigeClass = GetPrestigeClass()
if ( prestigeClass ) then
        AI.QuestStepsInvolvedIn = {
            {"RogueProfessionTierTwo", 5},
            {"RogueProfessionTierThree", 9},
            {"RogueProfessionTierFour", 7},
        }
end

function Dialog.OpenGreetingDialog(user)
    local text = nil    
    
    text = "Greetings traveler. Are you here to train in the ways of the Rogue..?"

    local response = {
        {
            text = "Yes, I'd like to learn some basics.",
            handle = "SkillTrain"
        },
        {
            text = "Goodbye.",
            handle = ""
        }
    }
    
    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenSkillTrainDialog(user)
        SkillTrainer.ShowTrainContextMenu(user)
end