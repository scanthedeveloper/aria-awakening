require 'incl_celador_locations'
require 'incl_regions'
require 'base_ai_mob'
require 'base_ai_intelligent' 
require 'base_ai_conversation' 
require 'base_merchant_event'
require 'base_skill_trainer'
require 'incl_faction'
require 'incl_crafting_orders'

Dialog = {}
QuestDialog = {}

AI.Settings.SkipIntroDialog = false
AI.Settings.SetIntroObjVar = true
AI.Settings.KnowName = true
AI.Settings.CanCast = true
AI.Settings.DoNotBoast = true
AI.Settings.EnableRepair = false
AI.Settings.SpeechTable = "Villager"

require 'incl_npc_tasks'

AI.TradeMessages = 
{
    "Well I'm here to trade!"
}

AI.GreetingMessages = 
{
    "Hello, anything I can help you with?",
}

--start chunk - overwrite these in npc ai modules
AI.QuestCompleteMessages = 
{
    "I have been expecting you.",
    "Finally you have arrived!",
    "What took so long?",
    "Are you ready for me? I am ready for you.",
    "I thought you had died!",
    "That was faster than usual, actually.",
    "Come here already.",
    "I have been standing here waiting this whole time.",
    "It's good to see you.",
}
AI.QuestAvailableMessages = 
{
    "We must speak!",
    "There is a matter which might interest you.",
    "We may be able to help each other.",
    "Can you help me?",
    "I require assistance!",
    "I may be able to teach you a thing or two.",
    "Come here! I want to show you something.",
    "I heard you like adventure!",
    "I need capable hands.",
}
AI.QuestsWelcomeText ="Greetings. I wonder if you could help me with something..."
AI.QuestStepsInvolvedIn = {}
function AI.QuestStepsInvolvedInFunc()return nil end
--end chunk

--old
AI.QuestMessages = 
{
    ", I have something for you!",
    ", come talk to me!",
    ", come over here!",
}

AI.NevermindMessages = 
{
"Anything else?",
}

AI.Settings.AlertRange = 10.0

function IsFriend(target)
    --My only enemy is the enemy
    if (AI.InAggroList(target)) then
        --DebugMessage(1)
        return false
    else
        --DebugMessage(2)
        return true
    end
end

function OpenQuestDialog(user,questName,taskName)
    if (Dialog["Open"..questName..taskName.."Dialog"] ~= nil) then
        Dialog["Open"..questName..taskName.."Dialog"](user)
    else
        DebugMessage("[base_ai_npc|OpenQuestDialog] ERROR: No quest dialog found for "..this:GetName()..", quest is "..tostring(questName)..", taskName is "..tostring(taskName))
    end
end

AI.StateMachine.AllStates.Idle = { 
        GetPulseFrequencyMS = function() return math.random(3000,4000) end,
        AiPulse = function() 
            local homeFacing = this:GetObjVar("SpawnFacing")
            if( homeFacing ~= nil ) then
                --DebugMessage("Setting facing to "..tostring(homeFacing))
                --this:SetFacing(homeFacing)
            end
            --aiRoll = math.random(4)
            --if(aiRoll == 1) then
            --    AI.StateMachine.ChangeState("GoLocation")            
            --else
            --    AI.StateMachine.ChangeState("Wander")
            --end         
            
            if ((this:GetObjVar("ImportantNPC")) and math.random(1,3) == 1) then
                local spawnLoc = this:GetObjVar("SpawnLocation")
                local distance = this:GetLoc():Distance(spawnLoc)        
                if(distance > 2) then
                    AI.StateMachine.ChangeState("GoHome")
                end
            end

            DecideIdleState()
        end,        
    }

AI.StateMachine.AllStates.GoLocation = {
        OnEnterState = function()
            destination = CeladorLocations[math.random(#CeladorLocations)]
            this:PathTo(destination.Loc,1.0,"GoLocation")
        end,

        OnArrived = function (success)
            if(success) then
                if (AI.StateMachine.CurState ~= "GoLocation") then
                    return 
                end
                if( destination ~= nil ) then 
                    this:SetFacing(destination.Facing)
                    if( destination.Name == "VillageWell" ) then
                        this:PlayAnimation("cast")
                    end
                end
            end
            DecideIdleState()
        end,
    }

function CanUseNPC(user)
    if (IsDead(this)) then return false end
    --DebugMessage("AI.MainTarget is "..tostring(AI.MainTarget),"user is "..tostring(user)," and is in combat is "..tostring(IsInCombat(this)))
    if (AI.MainTarget == user and IsInCombat(this)) then 
        if (InsultTarget ~= nil and AI.GetSetting("CanConverse")) then
            --DebugMessage("Yarrgh why are you doing this")
            InsultTarget(user)
        end
        user:CloseDynamicWindow("Responses")
        return false
    end
    return true
end

function HandleInteract(user,usedType)
    if(usedType ~= "Interact") then return end
    if (not CanUseNPC(user)) then return end
    
    --DebugMessage(tostring(user))
    if (not AI.IsValidTarget(user) and not user:HasObjVar("Invulnerable")) then
        return 
    end

    if (IsAsleep(this)) then
        return 
    end
    AI.IdleTarget = user
    --if (AI.GetSetting("CanConverse")) then
        AI.StateMachine.ChangeState("Converse")
    --end
    user:SendMessage("NPCInteraction",this)
    --NEW QUEST SYSTEM 6/27/19
    if ( Dialog.OpenQuestsDialog(user) ) then return end
    --old quest system
    local userQuestTable = user:GetObjVar("QuestTable") or {}
    local activeQuest = user:GetObjVar("LastActiveQuest")
    if (activeQuest ~= nil) then
         for n,k in pairs(userQuestTable) do
            if k.QuestName == activeQuest  then
                --DebugMessage("k.questName is "..tostring(k.QuestName).." k.CurrentTask is "..tostring(k.CurrentTask))
                if (k.CurrentTask ~= nil and (not (k.QuestFinished == true))) then
                    if (Dialog["Open"..k.QuestName..k.CurrentTask.."Dialog"] ~= nil) then
                        OpenQuestDialog(user,k.QuestName,k.CurrentTask)
                        return
                    end
                end
            end
        end
    end
    --go through all the elements again, so that if there's a quest that's not active then choose that dialog
     for n,k in pairs(userQuestTable) do
         if (k.CurrentTask ~= nil and (not (k.QuestFinished == true))) then
            --DebugMessage("CHECKING FOR","Open"..k.QuestName..k.CurrentTask.."Dialog")
            if (Dialog["Open"..k.QuestName..k.CurrentTask.."Dialog"] ~= nil) then
                OpenQuestDialog(user,k.QuestName,k.CurrentTask)
                return
            end
         end
    end
  --DebugMessage(5)
    local strippedName = StripColorFromString(this:GetName())
    if (not(AI.GetSetting("SkipIntroDialog")) and not user:HasObjVar("Intro|"..strippedName) and IntroDialog ~= nil) then
        if (AI.GetSetting("SetIntroObjVar") == true) then
            user:SetObjVar("Intro|"..strippedName,true)
        end
        IntroDialog(user)
        return
    end
   --DebugMessage(6)
    local urgentTask = GetUrgentTask(user)
    if (urgentTask ~= nil) then 
        Dialog.OpenNPCTasksInquiryDialog(user,urgentTask.TaskName)
        return
    end
        
    Dialog.OpenGreetingDialog(user)
end

function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)]

    response = {}

    response[1] = {}
    response[1].text = "I want to know something..."
    response[1].handle = "Talk" 

    if (AI.GetSetting("MerchantEnabled") ~= nil and AI.GetSetting("MerchantEnabled") == true) then
        response[2] = {}
        response[2].text = "I want to trade..."
        response[2].handle = "Trade" 
    else
        response[2] = {}
        response[2].text = "Who are you?"
        response[2].handle = "Who" 
    end

    if (CountTable(NPCTasks) > 0) then 
        response[3] = {}
        response[3].text = "It's regarding work..."
        response[3].handle = "Work"
    end

    if (AI.GetSetting("EnableTrain") ~= nil and AI.GetSetting("EnableTrain") == true and CanTrain()) then
        response[4] = {}
        response[4].text = "Train me in a skill..."
        response[4].handle = "Train" 
    end

    if (this:GetObjVar("CraftOrderSkill")~= nil) then
        response[5] = {}
        response[5].text = "About crafting orders..."
        response[5].handle = "Work" 
    end

    if ( AI.GetSetting("AcceptCriminalHeads") == true ) then
        response[6] = {}
        response[6].text = "I have a criminal head to turn in."
        response[6].handle = "TurnInCriminalHead" 
    end

    response[7] = {}
    response[7].text = "Goodbye."
    response[7].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

--dummy function that deletes the intro objvar allowing the intros to show more than once.
function Dialog.OpenResetIntroDialog(user)
    user:DelObjVar("Intro|"..this:GetName())
end

function IntroDialog(user)

    if AI.IntroMessages then
        text = AI.IntroMessages[math.random(1,#AI.IntroMessages)]
    end
    response = {}

    response[1] = {}
    response[1].text = "I have a question..."
    response[1].handle = "Talk" 

    response[2] = {}
    response[2].text = "Who are you?"
    response[2].handle = "Who"

    response[3] = {}
    response[3].text = "I need something."
    response[3].handle = "Nevermind"

    if (this:GetObjVar("CraftOrderSkill")~= nil) then
        response[4] = {}
        response[4].text = "About crafting orders..."
        response[4].handle = "Work" 
    end

    if ( AI.GetSetting("AcceptCriminalHeads") == true ) then
        response[5] = {}
        response[5].text = "I have a criminal head to turn in."
        response[5].handle = "TurnInCriminalHead" 
    end

    response[6] = {}
    response[6].text = "Goodbye."
    response[6].handle = "" 

    NPCInteraction(text,this,user,"Responses",response)

    GetAttention(user)
end

function Dialog.OpenTurnInCriminalHeadDialog(user)
    user:RequestClientTargetGameObj(this, "TargetHumanHead")

    local text = "Please select the head."

    NPCInteractionLongButton(text,this,user,"Responses", {
        { text = "Nevermind.", handle = "Greeting" },
    })
end

function Dialog.OpenWorkDialog(user)
    text = "Folks come to me when they need something made, but I don't always have the time to handle all of these orders. That's why I sometimes look to other craftsmen for help. If you're willing to take a crafting order off of my hands, I'll cut you in on the reward."

    response = {}

    response[1] = {}
    response[1].text = "What orders do you need filled?"
    response[1].handle = "CraftingOrder"

    response[2] = {}
    response[2].text = "I have a completed order."
    response[2].handle = "OrderSubmit"

--[[
    response[1] = {}
    response[1].text = "I need work."
    response[1].handle = "NPCTasks" 

    response[2] = {}
    response[2].text = "I have a task I need to complete..."
    response[2].handle = "NPCCurrentTasksList" 
]]--
    response[3] = {}
    response[3].text = "Nevermind."
    response[3].handle = "Nevermind" 

    NPCInteractionLongButton(text,this,user,"Responses",response)

end

function Dialog.OpenTradeDialog(user)
    text = AI.TradeMessages[math.random(1,#AI.TradeMessages)]

    if (AI.GetSetting("MerchantEnabled") == nil) then
        return
    end

    response = {}

    if (AI.GetSetting("EnableBuy") ~= nil and AI.GetSetting("EnableBuy") == true) then
        response[1] = {}
        response[1].text = "I want to buy something."
        response[1].handle = "Buy" 
        
        response[2] = {}
        response[2].text = "How much would you buy this for?"
        response[2].handle = "Appraise" 

        response[3] = {}
        response[3].text = "I wish to sell something..."
        response[3].handle = "Sell" 
    end

    if (AI.GetSetting("EnableBank") ~= nil and AI.GetSetting("EnableBank") == true) then
        response[4] = {}
        response[4].text = "I want to bank items."
        response[4].handle = "Bank" 
    end
    if (AI.GetSetting("EnableTax") ~= nil and AI.GetSetting("EnableTax") == true) then
        response[5] = {}
        response[5].text = "I want to pay taxes."
        response[5].handle = "Tax" 
    end
    if (AI.GetSetting("EnableRepair"))  then
        response[6] = {}
        response[6].text = "I need to repair something."
        response[6].handle = "RepairItem"
    end

    --To enable crafting orders, add "CraftOrderSkill" objVar to required skill. Ex, Blacksmiths should hand out Metalsmith orders
    if (this:GetObjVar("CraftOrderSkill")~= nil and this:HasModule("base_merchant")) then
        response[7] = {}
        response[7].text = "About crafting orders..."
        response[7].handle = "Work" 
    end

    response[8] = {}
    response[8].text = "Nevermind"
    response[8].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)

end

--THESE FUNCTIONS SHOULD BE OVERRIDDEN
function Dialog.OpenHelpDialog(user)
    QuickDialogMessage(this,user,"I have no brains to help you!")
end
function Dialog.OpenTalkDialog(user)
    QuickDialogMessage(this,user,"I have nothing to say at all!")
end
function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,"I don't know who I am!")
end
function Dialog.OpenRepairItemDialog(user) 
    DebugMessage("ERROR: Repair not implemented!")
    --this:AddModule("repair_controller")   
    --this:SendMessage("RESTART_REPAIR_PROCESS", user,this,this:GetObjVar("RepairSkill"))
end

function Dialog.OpenCraftingOrderDialog(user)
    
    local orderInfo = {}

    local commission = GetCommission(user)
    --If the commission has already been offered, use it. Otherwise, pick a new one.
    if ( not commission or not next(commission) ) then
        for i=1,3 do
            local order = PickCraftingOrder(user, this:GetObjVar("CraftOrderSkill"))
            if ( order and next(order) ) then
                table.insert(orderInfo, order)
            end
        end
    else
        orderInfo = commission[1]
    end

    response = {}

    --If the player doesn't meet minimum skill for any order, shoo them away
    
    if ( orderInfo and next(orderInfo) ) then
        AddCommission(user, orderInfo)

        text = "Here's what I have."
        if not (GetCommission(user)[2]) then
            for i, j in pairs(orderInfo) do
                response[i] = {}

                if (orderInfo[i].Material ~= nil) then
                    response[i].text = orderInfo[i].Amount.." "..ResourceData.ResourceInfo[orderInfo[i].Material].CraftedItemPrefix.." "..GetItemNameFromRecipe(orderInfo[i].Recipe)
                else
                    response[i].text = orderInfo[i].Amount.." "..GetItemNameFromRecipe(orderInfo[i].Recipe)
                end

                local orderSkill = this:GetObjVar("CraftOrderSkill")
                local tier = OrderScoreToTier(orderSkill, GetCraftOrderScore(orderSkill, orderInfo[i]))

                response[i].text = response[i].text .. " (Tier "..tier..")"
                
                response[i].handle = "CraftOrder|"..i
            end
        else 
            text = "I already gave you a crafting order. Why don't you try completing that first. If you really want more work, come back later and I'll see what I can get for you."
        end
    else
        text = "Sorry pal, but I'm not paying amateurs. Try getting some experience under your belt, and maybe I'll be able to pull up something for you."
    end

    response[#orderInfo+1] = {}
    response[#orderInfo+1].text = "Nevermind."
    response[#orderInfo+1].handle = "Nevermind" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenOrderAcceptDialog(user)
    text = ("Here you go then. Don't be thinking you just bring me a pile of scattered items though. Use the crafting order to package them up, and return it to me. Other craftsmen like myself will also accept a completed order. It'll get back to me one way or another.")

    response = {}

    response[1] = {}
    response[1].text = "Thank you."
    response[1].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenOrderSubmitDialog(user)
    text = "Alright then, show me what you got!"
    user:RequestClientTargetGameObj(this, "pickOrder")
    response = {}

    response[1] = {}
    response[1].text = "Nevermind"
    response[1].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenQuestsDialog(user)
    local questStartDialogue = Quests.GetDialogue(user, AI.QuestStepsInvolvedInFunc() or AI.QuestStepsInvolvedIn, "Start") or {}
    local questEndDialogue = Quests.GetDialogue(user, AI.QuestStepsInvolvedInFunc() or AI.QuestStepsInvolvedIn, "End") or {}
    local questIneligibleDialogue = Quests.GetDialogue(user, AI.QuestStepsInvolvedIn, "Ineligible") or {}
    local wasQuestFailed = Quests.HasFailed(user, AI.QuestStepsInvolvedIn)
    if ( not wasQuestFailed and (not questStartDialogue or not next(questStartDialogue)) and (not questEndDialogue or not next(questEndDialogue)) and (not questIneligibleDialogue or not next(questIneligibleDialogue)) ) then
        return false
    end
    
    text = AI.QuestsWelcomeText
        
    response = {}

    local nResponse = 1
    for quest, steps in pairs(questEndDialogue) do
        for step, v in pairs(steps) do
            if ( nResponse < 6 ) then
                response[nResponse] = {}
                response[nResponse].text = v[1]
                response[nResponse].handle = "#qId#"..quest.."#active#"..step
                nResponse = nResponse + 1
            end
        end
    end
    for quest, steps in pairs(questStartDialogue) do
        for step, v in pairs(steps) do
            if ( nResponse < 6 ) then
                response[nResponse] = {}
                response[nResponse].text = v[1]
                response[nResponse].handle = "#qId#"..quest.."#offer#"..step
                nResponse = nResponse + 1
            end
        end
    end
    for quest, steps in pairs(questIneligibleDialogue) do
        for step, v in pairs(steps) do
            if ( nResponse < 6 ) then
                response[nResponse] = {}
                response[nResponse].text = v[1]
                response[nResponse].handle = "#qId#"..quest.."#deny#"..step
                nResponse = nResponse + 1
            end
        end
    end
    if wasQuestFailed then
        response[1] = {}
        response[1].text = "[ Profession ] Remind me what I need to do, again?"
        response[1].handle = "#qId#"..tostring(wasQuestFailed).."#fail#"
    end

    response[6] = {}
    response[6].text = "I have another matter to discuss..."
    response[6].handle = "Greeting"

    NPCInteraction(text,this,user,"Responses",response)
    return true
end

function QuestResponse(user, qId)
    local qId = string.gsub(tostring(qId), "#qId#", "")
    if ( string.match(qId, "#offer#") ) then
        local quest, step = qId:match("(.+)#offer#(.+)")
        step = tonumber(step)
        local questDialogue = AllQuests[quest][step].StartDialogue
        text = questDialogue[2]
        response = {}
        response[1] = {}
        response[1].text = "I accept."
        response[1].handle = "#qId#"..quest.."#accept#"..step

        response[6] = {}
        response[6].text = "Perhaps later."
        response[6].handle = "Greeting"
        NPCInteraction(text,this,user,"Responses",response)
    elseif ( string.match(qId, "#accept#") ) then
        local quest, step = qId:match("(.+)#accept#(.+)")
        step = tonumber(step)
        user:CloseDynamicWindow("Responses")
        if ( not Quests.TryStart(user, quest, step) ) then
            QuickDialogMessage(this, user, "You are not prepared.", 10)
        end
    elseif ( string.match(qId, "#active#") ) then
        local quest, step = qId:match("(.+)#active#(.+)")
        step = tonumber(step)
        local questDialogue = AllQuests[quest][step].EndDialogue
        text = questDialogue[2]
        response = {}
        response[1] = {}
        response[1].text = "Continue."
        response[1].handle = "#qId#"..quest.."#turnin#"..step
        NPCInteraction(text,this,user,"Responses",response)
    elseif ( string.match(qId, "#turnin#") ) then
        local quest, step = qId:match("(.+)#turnin#(.+)")
        step = tonumber(step)
        local reqs = AllQuests[quest][step].Requirements
        if reqs then 
            for i=1, #reqs do
                if reqs[i][1] == "HasCompletedCraftingOrder" then
                    local commissions = this:GetObjVar("Commissions") or {}
                    if commissions then 
                        commissions[user] = nil
                        this:SetObjVar("Commissions", commissions)
                    end
                end
            end
        end
        user:CloseDynamicWindow("Responses")
        if ( not Quests.TryEnd(user, quest, step, 1) ) then
            QuickDialogMessage(this, user, "Come back when you have completed the task.", 10)
        end
    elseif ( string.match(qId, "#deny#") ) then
        local quest, step = qId:match("(.+)#deny#(.+)")
        step = tonumber(step)
        local questDialogue = AllQuests[quest][step].IneligibleDialogue
        text = questDialogue[2]
        response = {}
        response[1] = {}
        response[1].text = "Okay."
        response[1].handle = "Close"
        NPCInteraction(text,this,user,"Responses",response)
    elseif ( string.match(qId, "#fail#") ) then
        local quest, step = qId:match("(.+)#fail#(.+)")
        step = tonumber(step)
        text = "Here is what I needed..."
        response = {}
        response[1] = {}
        response[1].text = "Thank you."
        response[1].handle = "#qId#"..text.."#resume#"
        NPCInteraction(text,this,user,"Responses",response)
    elseif ( string.match(qId, "#resume#") ) then
        local quest, step = qId:match("(.+)#resume#(.+)")
        step = tonumber(step)
        local failedQuests = Quests.GetFailedQuests(user, AI.QuestStepsInvolvedIn)
        user:CloseDynamicWindow("Responses")
        for i=1, #failedQuests do 
            Quests.Resume(user, failedQuests[i])    -- possible flaw: NPC will resume all quests with Cancelled set to 1. Maybe add them as dialogue options?
        end
    end
end

function NevermindDialog(user)
    if (user == nil or not user:IsValid()) then return end
    text = AI.NevermindMessages[math.random(1,#AI.NevermindMessages)]

    response = {}

    response[1] = {}
    response[1].text = "I want to know something..."
    response[1].handle = "Talk" 

    if (AI.GetSetting("MerchantEnabled") ~= nil and AI.GetSetting("MerchantEnabled") == true) then
        response[2] = {}
        response[2].text = "I want to trade..."
        response[2].handle = "Trade" 
    else
        response[2] = {}
        response[2].text = "Who are you?"
        response[2].handle = "Who" 
    end
    if (CountTable(NPCTasks) > 0) then 
        response[3] = {}
        response[3].text = "It's regarding work..."
        response[3].handle = "Work" 
    end

    if (this:GetObjVar("CraftOrderSkill")~= nil) then
        response[4] = {}
        response[4].text = "About crafting orders..."
        response[4].handle = "Work" 
    end

    if (AI.GetSetting("EnableTrain") ~= nil and AI.GetSetting("EnableTrain") == true and CanTrain()) then
        response[5] = {}
        response[5].text = "Train me in a skill..."
        response[5].handle = "Train" 
    end

    response[6] = {}
    response[6].text = "Goodbye."
    response[6].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)

    this:StopMoving()
    --DebugMessage(tostring(user))
    this:SetFacing(this:GetLoc():YAngleTo(user:GetLoc()))
    AI.StateMachine.ChangeState("Idle")
end
AI.NevermindDialog = NevermindDialog

function GetAttention(user)
    if (IsAsleep(this)) then
        return 
    end
    this:StopMoving()
    this:SetFacing(this:GetLoc():YAngleTo(user:GetLoc()))
    AI.StateMachine.ChangeState("Converse")
end

function ResponsesDialog (user,buttonID)
    if (not CanUseNPC(user)) then return end
    --if (IsInCombat(this)) then return end
    --DebugMessage("ButtonID is "..tostring(buttonID))
    if (not AI.IsValidTarget(user) and not user:HasObjVar("Invulnerable")) then return end
    GetAttention(user)
    --DebugMessage("user is "..tostring(user))
    if (buttonID == "Trade" and AI.GetSetting("MerchantEnabled") ~= nil) then
        Dialog.OpenTradeDialog(user)
    elseif ( string.match(tostring(buttonID), "#qId#") ) then
        QuestResponse(user, buttonID)
    elseif (buttonID == "Quest") then
        Dialog.OpenQuestDialog(user)
	elseif (buttonID == "Nevermind") then
        NevermindDialog(user)
    elseif (buttonID == "Help") then
        Dialog.OpenHelpDialog(user)
    elseif (buttonID == "Bank") then
        OpenBank(user,this)
    elseif (buttonID == "Talk") then
        Dialog.OpenTalkDialog(user)
    elseif (buttonID == "Who") then
        Dialog.OpenWhoDialog(user)
    elseif (buttonID == "Sell") then
        Merchant.DoSell(user)
    elseif (buttonID == "Appraise") then
        Merchant.DoAppraise(user)
    elseif (buttonID == "Train" and AI.GetSetting("EnableTrain") ~= nil and CanTrain()) then
        SkillTrainer.ShowTrainContextMenu(user)
    elseif (buttonID == "Buy") then
        QuickDialogMessage(this,user,AI.HowToPurchaseMessages[math.random(1,#AI.HowToPurchaseMessages)])
    elseif ( string.match(tostring(buttonID), "Quest") and Dialog["OpenQuestsDialog"] ~= nil) then
        Dialog["OpenQuestsDialog"](user, buttonID)
    elseif ( Dialog["Open"..buttonID.."Dialog"] ~= nil) then
        Dialog["Open"..buttonID.."Dialog"](user)
    elseif(buttonID == "Close") then
        user:CloseDynamicWindow("Responses")
    elseif (this:GetObjVar("CraftOrderSkill")) then
        local items = StringSplit(buttonID, "|")
        if (items[1] == "CraftOrder") then
            local commission = GetCommission(user)

            local orderIndex = tonumber(items[2])
            local order =  commission[1][orderIndex]
            if not (commission[2]) then
                CommissionAccepted(user)
                CreateCraftingOrder(user, order)
                Dialog.OpenOrderAcceptDialog(user)
            end
        end
    elseif (buttonID ~= nil and buttonID ~= "" and buttonID ~= "Ok") then
        DebugMessage("[base_ai_npc|ResponsesDialog] ERROR: Invalid NPC dialog received! buttonID is "..tostring(buttonID))
    end
end

------- New NPC Interaction System (April 2020) -------
--

NPCDialogue = {}

if initializer and initializer.NPCDialogue then
    this:SetObjVar("NPCDialogue", initializer.NPCDialogue)
    Dialogue.ClearInteractionRecord(this)
end

function GetNPCDialogue()
    NPCDialogue = next(NPCDialogue) and NPCDialogue or this:GetObjVar("NPCDialogue")
    return NPCDialogue
end

-- Checks that player "user" is valid to interact "usedType" with and then draws its root dialogue.
function InitializeNPCInteraction(user, usedType)
    if(usedType ~= "Interact") then return end
    if (not CanUseNPC(user)) then return end
    if (not AI.IsValidTarget(user) and not user:HasObjVar("Invulnerable")) then return end
    if (IsAsleep(this)) then return end
    AI.IdleTarget = user
    AI.StateMachine.ChangeState("Converse")
    GetAttention(user)
    
    --DebugMessage("---- FRESH INTERACT ----")
    Dialogue.GoTo(this, user, GetNPCDialogue(), {}, 1, nil)
end

-- Processes string "clickId" sent from DynamicWindowResponse to find new instruction for traversal along Dialogue Trees.
function ProcessInstruction(user, clickId)
    local npc = this
    local player = user

    local commandInfo = Dialogue.ProcessCommandString(npc, player, NPCDialogue, clickId)
    local success = Dialogue.GlobalProcessInstruction(npc, player, NPCDialogue, commandInfo)
    if not success then
        local depth = commandInfo.Depth
        local indices = commandInfo.Indices
        local compoundSplits = commandInfo.CompoundSplits
        local instruction = commandInfo.Instruction

        -- LEGACY "base_ai_npc" logic only. New Instruction logic should be handled using Global Helpers in the static_data/npc_dialogue.lua file. 
        if instruction == "SKILL" then
            local skillName = compoundSplits[2]
            local displayName = compoundSplits[3]
            local newInstruction = DialogueTrain(player, skillName)
            Dialogue.SetTempVariables(npc, player, {SpeechVars = {displayName, ValueToAmountStr(DEFAULT_TRAIN_PRICE,false,true)}, SkillName = skillName })
            --DebugMessage("SPEECHVARS:",displayName, skillName)
            Dialogue.GoTo(npc, player, NPCDialogue, indices, depth + 1, newInstruction)
            return
        elseif instruction == "SKILLSUCCESS" then
            local skillName = Dialogue.GetTempVariables(npc, player, "SkillName")
            Dialogue.SetTempVariables(npc, player, {SkillName = nil })
            if skillName then
                local transactionId = "Train"..skillName
                RequestConsumeResource(player,"coins",DEFAULT_TRAIN_PRICE,transactionId,npc)
                return
            else
                Dialogue.GoTo(npc, player, NPCDialogue, indices, depth, "SKILLFAIL")
                return
            end
        elseif instruction == "REQ" then
            local responses = DialogueGetRandomCraftingOrderResponses(player)
            if not responses then return end
            Dialogue.SetTempVariables(npc, player, {ResponseVars = responses })
        elseif instruction == "CO" then
            local orderIndex = tonumber(compoundSplits[2])
            local commission = GetCommission(player)
            if orderIndex and commission then
                local order = commission[1][orderIndex]
                if not commission[2] then
                    CommissionAccepted(user)
                    CreateCraftingOrder(user, order)
                    Dialogue.GoTo(npc, player, NPCDialogue, indices, depth + 1, "REQSUCCESS")
                    return
                end
            end
        end

        -- No special behavior needed, continue on to next Depth and call logic from static_data/npc_dialogue.lua.
        Dialogue.GoTo(npc, player, NPCDialogue, indices, depth + 1, instruction)
    end
end

function HandleObjectSelection(target, user)
    --DebugMessage("HandleObjectSelection()", target, user)
    local temp = Dialogue.GetTempInteractionRecord(this, user)
    local isValid = temp.Indices and temp.Depth and temp.Instruction
    if target == nil then return end
    if not temp.NeedContext then DebugMessage("Context was lost") return end
    if not isValid then DebugMessage("TempRecord not valid", temp.Indices, temp.Depth, temp.Instruction) return end

    local selectionAccepted = true
    local newInstruction = ""

    if temp.Instruction == "SELL" then
        selectionAccepted = HandleSellTargeted(target, user)
        newInstruction = selectionAccepted and "SELLSUCCESS" or "SELLFAIL"
    elseif temp.Instruction == "BUY" then
        selectionAccepted = target:HasObjVar("merchantOwner") and target:GetObjVar("merchantOwner") == this
        if selectionAccepted then target:SendMessage("UseObject", user, "Buy") end
        newInstruction = selectionAccepted and "BUYSUCCESS" or "BUYFAIL"
    elseif temp.Instruction == "TURNIN" then
        newInstruction = DialogueOrderSubmission(target, user) or "TURNINFAIL"
    elseif temp.Instruction == "HEAD" then
        newInstruction = DialogueCriminalHead(target, user) or "HEADFAIL"
    elseif temp.Instruction == "SHOWOBJ" then
        newInstruction = DialogueShowObj(target, user) or "SHOWOBJFAIL"
        -- these are too specific, maybe keep obj as TempVariable and then on the upcoming GoTo, set up some logic to run on it that can be written in npc static data
    elseif temp.Instruction == "ACCREDIT" then
        newInstruction = DialogueAccreditCatalog(target, user) or "ACCREDITFAIL"
    else
        selectionAccepted = false
        DebugMessage("No valid SelectionId")
    end

    Dialogue.GoTo(this, user, NPCDialogue, temp.Indices, temp.Depth + 1, newInstruction)
end

function DialogueAccreditCatalog(target, user)
    if target:GetCreationTemplateId() ~= "lore_catalog" then return "NOTCATALOG" end
    if target:GetObjVar("Owner") ~= user then return "NOTOWNER" end
    local accreditee = target:GetObjVar("Accreditee")
    if accreditee and (accreditee ~= user) then return "NOTACCREDITEE" end
    local accreditedCatalog = user:GetObjVar("LoreAccreditedCatalog")
    if accreditedCatalog and accreditedCatalog:IsValid() and accreditedCatalog ~= target then return "DIFFCATALOG" end

    return Lore.AccreditCatalog(user, target) and "ACCREDITSUCCESS" or "ACCREDITFAIL"
end

function DialogueShowObj(target, user)
    local desiredObjVars = this:GetObjVar("DesiredObjVars")
    local varSplits = StringSplit(desiredObjVars, ",")
    for i=1, #varSplits do
        local pairSplits = StringSplit(varSplits[i], "=")

        if pairSplits[1] == "Template" then
            if target:GetCreationTemplateId() ~= pairSplits[2] then
                --DebugMessage("DialogueShowObj: Template ~=",pairSplits[2],"!")
                return "SHOWOBJFAIL"
            end
        else
            local targetObjVar = target:GetObjVar(pairSplits[1])
            if not (targetObjVar and tostring(targetObjVar) == pairSplits[2]) then
                --DebugMessage("DialogueShowObj:",tostring(targetObjVar),"~=",pairSplits[2],"!")
                return "SHOWOBJFAIL"
            end
        end
    end
    return "SHOWOBJSUCCESS"
end

-- returns selectionAccepted (bool), failureTag (string)
function DialogueOrderSubmission(target, user)
    if (target == nil) then return end
    local isGod = IsGod(user) and not TestMortal(user)

    local orderInfo = target:GetObjVar("OrderInfo")
    local orderOwner = GetOrderOwner(user,target)
    local craftOrderSkill = this:GetObjVar("CraftOrderSkill")

    if not orderInfo and not orderOwner then return "NOT" end
    
    --If there's no owner, continue anyways (useful for orders created using /create).
    if (orderOwner ~= nil) then
        --If the order was not issued to the current player. 
        if (orderOwner ~= user and not isGod ) then return "OWN" end

        --If the order CraftOrderSkill doesn't match up with the skill of the recipe
        if (GetRecipeTableFromSkill(this:GetObjVar("CraftOrderSkill"))[orderInfo.Recipe] == nil) then return "QUALIFY" end
    end

    --If the order is complete, issue reward and destroy the order.
    if (target:GetObjVar("OrderComplete") or isGod ) then

        user:SystemMessage("You attempt to turn in a craft order...", "info")
        --SCAN MODIFIED EFFECT
        user:PlayEffect("HelmFlagEffect",5)
        user:PlayObjectSound("event:/ui/quest_complete",false)

        --Reset commission
        local commissions = this:GetObjVar("Commissions") or {}
        commissions[user] = nil
        this:SetObjVar("Commissions", commissions)
        
        CraftingOrderReward(user, target, craftOrderSkill, orderInfo)

        return "TURNINSUCCESS"
    else
        return "DONE"
    end
end

function DialogueGetRandomCraftingOrderResponses(user)
    local orderInfo = {}
    local response = {}

    local commission = GetCommission(user)
    --If the commission has already been offered, use it. Otherwise, pick a new one.
    if ( not commission or not next(commission) ) then
        for i=1,3 do
            local order = PickCraftingOrder(user, this:GetObjVar("CraftOrderSkill"))
            if ( order and next(order) ) then
                table.insert(orderInfo, order)
            end
        end
    else
        orderInfo = commission[1]
    end

    --If the player doesn't meet minimum skill for any order, shoo them away
    if ( orderInfo and next(orderInfo) ) then
        AddCommission(user, orderInfo)

        if not (GetCommission(user)[2]) then
            for i, j in pairs(orderInfo) do
                response[i] = {}

                if (orderInfo[i].Material ~= nil) then
                    response[i].text = orderInfo[i].Amount.." "..ResourceData.ResourceInfo[orderInfo[i].Material].CraftedItemPrefix.." "..GetItemNameFromRecipe(orderInfo[i].Recipe)
                else
                    response[i].text = orderInfo[i].Amount.." "..GetItemNameFromRecipe(orderInfo[i].Recipe)
                end

                local orderSkill = this:GetObjVar("CraftOrderSkill")
                local tier = OrderScoreToTier(orderSkill, GetCraftOrderScore(orderSkill, orderInfo[i]))

                response[i].text = response[i].text .. " (Tier "..tier..")"
                
                response[i].handle = "CraftingOrder:CO|"..i
            end
        else
            local indices = Dialogue.GetTempVariables(this, user, "Indices")
            Dialogue.GoTo(this, user, NPCDialogue, indices, 4, "GAVE")
            return nil
        end
    else
        local indices = Dialogue.GetTempVariables(this, user, "Indices")
        Dialogue.GoTo(this, user, NPCDialogue, indices, 4, "INEXP")
        return nil
    end

    local newIndex = #response + 1
    response[newIndex] = {}
    response[newIndex].text = "Nevermind."
    response[newIndex].handle = "H"

    return response
end

function DialogueCriminalHead(target, user)
    if ( user == nil ) then
        LuaDebugCallStack("[SkillScroll.Create] playerObj not provided.")
        return "HEADFAIL"
    end
    if ( target == nil or not target:IsValid() ) then
        LuaDebugCallStack("[SkillScroll.Create] criminalHead not provided (or invalid).")
        return "HEADFAIL"
    end

    if target:GetCreationTemplateId() == "human_head" then return "RAND" end

    local resourceType = target:GetObjVar("ResourceType")
    -- give a more detailed response to rotten heads
    if ( resourceType == "RottenCriminalHead" ) then return "ROTTEN" end
    if ( resourceType ~= "CriminalHead" ) then return "NOT" end

    local criminalSkills = target:GetObjVar("CriminalSkillData")
    if ( criminalSkills == nil ) then return "EMPTY" end

    local skillScrollInfo = SkillScroll.BuildSkillScrollInfo(criminalSkills)
    if ( skillScrollInfo == nil ) then return "EMPTY" end

    -- destroy in same frame as to prevent exploit, sorry if it fails.
    target:Destroy()
    Create.InBackpack("skill_scroll", user, nil, function(scrollObj)
        if ( scrollObj ) then
            local name, color = StripColorFromString(scrollObj:GetName() or "")
            scrollObj:SetName(string.format("%s%s %s[-]", color, skillScrollInfo[1], name))
            scrollObj:SetObjVar("SkillScrollInfo", skillScrollInfo)
            SetItemTooltip(scrollObj)
            user:SystemMessage("You have been rewarded one "..StripColorFromString(scrollObj:GetName()), "info")
        else
            return "SCROLLFAIL"
        end
    end, true)

    return "HEADSUCCESS"
end

function DialogueTrain(user, skillName)
    if( user == nil or not(user:IsValid())) then return end
    if( user:DistanceFrom(this) > OBJECT_INTERACTION_RANGE) then return end
    local skillData = SkillData.AllSkills[skillName]
    if (skillData == nil ) then 
        DebugMessage("[base_skill_trainer|TrainDialog] ERROR: skillData is nil for: "..this:GetName().." skillName: "..skillName)
        return ""
    end

    if GetSkillLevel(user,skillName) >= STARTING_SKILL_VALUE then
        return "OVER"
    end

    if GetSkillTotal(user) >= (ServerSettings.Skills.PlayerSkillCap.Total - STARTING_SKILL_VALUE) then
        return "MAX"
    end

    if GetSkillCap(user, skillName, GetSkillDictionary(user)) < STARTING_SKILL_VALUE then
        return "CAP"
    end

    if CountCoins(user) < 30 then
        return "POOR"
    end         
    
    return "TEACH"
end

function OnConsumeResource(success,transactionId,user)
    if transactionId:match("Train") then
        local indices = Dialogue.GetTempVariables(this, user, "Indices")
        if success then
            local skillName = StripFromString(transactionId,"Train")
            SetSkillLevel(user, skillName, 30, true)
            Dialogue.GoTo(this, user, NPCDialogue, indices, 4, "SKILLSUCCESS")
        else
            DebugMessage("Failed Consume")
            Dialogue.GoTo(this, user, NPCDialogue, indices, 4, "POOR")
        end
    end
end
--
------- End NPC Interaction block -------

RegisterEventHandler(EventType.EnterView,"NearbyPlayer",
    function(player)
        --new quest system
        local questStartDialogue = Quests.GetDialogue(player, AI.QuestStepsInvolvedIn, "Start") or {}
        local questEndDialogue = Quests.GetDialogue(player, AI.QuestStepsInvolvedIn, "End") or {}
        if ( not this:HasTimer("NoShout") and GetSkillTotal(player) < 600 ) then
            if ( questEndDialogue and next(questEndDialogue) ) then
                this:NpcSpeech("[F2F5A9]"..player:GetName().."![-] "..AI.QuestCompleteMessages[math.random(1,#AI.QuestCompleteMessages)])
                this:ScheduleTimerDelay(TimeSpan.FromSeconds(10),"NoShout")
            elseif ( questStartDialogue and next(questStartDialogue) ) then
                this:NpcSpeech("[F2F5A9]"..player:GetName().."![-] "..AI.QuestAvailableMessages[math.random(1,#AI.QuestAvailableMessages)])
                this:ScheduleTimerDelay(TimeSpan.FromSeconds(10),"NoShout")
            end
        end
        --old quest system
        if (player ~= nil and player:IsValid()) then
            if (AI.GetSetting("HasQuest")) then
                if (AI.QuestList ~= nil) then
                    for i,j in pairs(AI.QuestList) do --DFB TODO: Make quest lists open up a dialog on interact.
                        if (not HasFinishedQuest(player,j)) then
                            FaceObject(this,player)
                            if (AI.GetSetting("KnowName") ~= false) then
                                this:NpcSpeech("Hey "..player:GetName()..AI.QuestMessages[math.random(1,#AI.QuestMessages)])
                            else
                                this:NpcSpeech(AI.QuestMessages[math.random(1,#AI.QuestMessages)])
                            end
                            return
                        end
                    end
                end
            end
        end
    end)

AddView("NearbyPlayer", SearchPlayerInRange(10))
RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(),
    function ()
        --        --DFB TODO: Right now guard protect is specific to villagers. When it's generic remove this check and always add it.
        if (this:GetObjVar("MobileTeamType") == "Villagers") then
            if (not this:HasModule("guard_protect")) then
                this:AddModule("guard_protect")
            end
        end
        
        AddUseCase(this,"Interact",true)
    
    end)

if ( this:GetObjVar("ImportantNPC") ) then
    SetMobileMod(this, "AttackTimes", "ImportantNPC", 3.5)
end

--OverrideEventHandler("base_ai_conversation",EventType.Message, "UseObject", HandleInteract)
RegisterEventHandler(EventType.DynamicWindowResponse, "Responses",ResponsesDialog)
RegisterEventHandler(EventType.Message,"DamageInflicted",function (damager,damageAmount)
    if (damager:IsPlayer()) then
        damager:CloseDynamicWindow("Responses")
    end
end)
RegisterEventHandler(EventType.Arrived, "GoLocation",AI.StateMachine.AllStates.GoLocation.OnArrived)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "pickOrder", HandleOrderSubmission)

function HandleHeadTargeted(target, user)
    SkillScroll.Create(user, target, function(skillScroll, errorMessage)
        if ( skillScroll ~= nil ) then
            local text = string.format("You have been given one %s.", StripColorFromString(skillScroll:GetName()))
        
            NPCInteractionLongButton(text,this,user,"Responses", {
                { text = "Thank You!", handle = "Greeting" },
            })
        else
            NPCInteractionLongButton(errorMessage or "Unknown error occured.",this,user,"Responses", {
                { text = "Ok", handle = "Greeting" },
            })
        end
    end)
end
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "TargetHumanHead", HandleHeadTargeted)

-- New Interaction System EventHandlers
if this:HasObjVar("NPCDialogue") then
   -- OverrideEventHandler("base_ai_conversation",EventType.Message, "UseObject", InitializeNPCInteraction)
   -- OverrideEventHandler("base_skill_trainer",EventType.Message, "ConsumeResourceResponse", OnConsumeResource)
    RegisterEventHandler(EventType.DynamicWindowResponse, "NPCInteraction", ProcessInstruction)
    RegisterEventHandler(EventType.ClientTargetGameObjResponse, "DialogueObjectSelection", HandleObjectSelection)
end