require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

AI.QuestStepsInvolvedIn = {
    {"FighterProfessionTierThree", 3},		-- Bladius Dart, otuside Ruin
    {"FighterProfessionTierThree", 10},
}

AI.QuestAvailableMessages = 
{
    "What a day to die!",
}

AI.QuestsWelcomeText ="Well met, fancy meeting you here? Are you here for the death? You'll find plenty in there..."

AI.GreetingMessages = 
{
    "Why have you come here?",
}

function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,
    	"I serve to warn Adventurers of the dangers within these ruins. We call upon experienced Fighters to clear it out as a test of their strength and skill."
    	)
    --QuickDialogMessage(this,user,"REPLACED -> I am a quest guy!")
end

function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)] or AI.GreetingMessages[1]

    response = {}

    response[1] = {}
    response[1].text = "Who are you?"
    response[1].handle = "Who" 
    
    response[2] = {}
    response[2].text = "Goodbye."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)