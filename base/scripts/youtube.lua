-- Load the YouTube API
local youtube = require("youtube")

-- Set the video ID to the provided video
youtube.video_id = "gLsGW5uhFSs"

-- Set the width and height of the dynamic window
youtube.width = 1024
youtube.height = 768

-- Open the video in a dynamic window
youtube.open_video()