MobileEffectLibrary.SummonMobiles = 
{
    OnEnterState = function(self,root,target,args)
        
        local amount = args.amount or 0

        for i=1, amount do
            local spawnLoc = GetNearbyPassableLocFromLoc(self.ParentObj:GetLoc(), 2, args.range or 20)
            Create.AtLoc(
                args.template,
                spawnLoc,
                function(spawnedMobile)
                    -- This is set to help despawn when the leader dies 
                    if( args.leader ) then
                        spawnedMobile:SetObjVar("MobileLeader", args.leader)
                    end
                end
            )
        end

        EndMobileEffect(root)
        return false
	end,
}