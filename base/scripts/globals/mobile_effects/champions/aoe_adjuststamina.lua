MobileEffectLibrary.AoeAdjustStamina = 
{

	OnEnterState = function(self,root,target,args)
        self.Amount = args.Amount or self.Amount
        self.TargetEnemies = args.TargetEnemies or self.TargetEnemies
        self.Range = args.Range or self.Range


        local targets = {}
        if( self.TargetEnemies == false ) then
            targets = FindObjects(SearchMulti(
                {
                    SearchMobileInRange(self.Range),
                    SearchObjVar("MobileTeamType",self.ParentObj:GetObjVar("MobileTeamType")),
                }))
        else
            local enemyObjects = FindObjects(SearchMobileInRange(self.Range))
            if (enemyObjects ~= nil) then
                for i,enemyObj in pairs(enemyObjects) do
                    if(enemyObj:GetObjVar("MobileTeamType") ~= self.ParentObj:GetObjVar("MobileTeamType") and self.ParentObj:HasLineOfSightToObj(enemyObj, ServerSettings.Combat.LOSEyeLevel)) then
                        table.insert(targets,enemyObj)
                    end
                end
            end
        end

        for i,target in pairs(targets) do
            target:SendMessage("StartMobileEffect", "AdjustStamina", target, { Amount = self.Amount })    
        end

        EndMobileEffect(root)
	end,

	GetPulseFrequency = nil,

    Amount = 10,
    TargetEnemies = true,
    Range = 10
}