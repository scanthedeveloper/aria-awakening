MobileEffectLibrary.MassResurrectMobiles = 
{
    OnEnterState = function(self,root,target,args)
        
        local amount = args.amount or 0
        local loc = self.ParentObj:GetLoc()

        matchingObjs = FindObjects(SearchMulti{SearchTemplate(args.template),SearchRange(loc, args.range)})


        for i,matchingObj in pairs(matchingObjs) do 
            
            if( IsDead(matchingObj) ) then
                local spawnLoc = matchingObj:GetLoc()
                

                Create.AtLoc(
                    args.template,
                    spawnLoc,
                    function(obj)
                        obj:PlayEffect("ResurrectEffect") 
                        matchingObj:Destroy()
                    end
                )
            end
        end

        EndMobileEffect(root)
        return false
	end,
}