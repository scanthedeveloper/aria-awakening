--SCAN ADDED
MobileEffectLibrary.Tangledroots = 
{

	OnEnterState = function(self,root,target,args)
		self.Range = args.Range or self.Range
		self.Target = target
		
		--self.ParentObj:PlayEffect("DustwaveEffect")
		--self.ParentObj:PlayEffect("BardAbsorbThewEffect",10)
        --self.ParentObj:PlayEffect("BardAbsorbEarthEffect",10)
        --self.ParentObj:PlayEffect("CastEarth2",10)
        
		if (IsMale(self.ParentObj)) then
			self.ParentObj:PlayObjectSound("event:/magic/air/magic_air_lightning_impact")
		else
			self.ParentObj:PlayObjectSound("event:/magic/air/magic_air_lightning_impact")
		end

		self.Weapon = GetPrimaryWeapon(self.ParentObj)

		if not( self.Weapon ) then
			return EndMobileEffect(root)
		end

		--self._Loc = self.ParentObj:GetLoc()
		--self._Facing = self.ParentObj:GetFacing()

		--local nearbyMobiles = FindObjects(SearchMobileInRange(self.Range, true))
		--for i=1,#nearbyMobiles do
		--	local mobile = nearbyMobiles[i]
        	if ( 
				--self.IsInFront(self, mobile)
				--and
				ValidCombatTarget(self.ParentObj, target, true)
				and
				self.ParentObj:HasLineOfSightToObj(target, ServerSettings.Combat.LOSEyeLevel)
			) then
				--self.ParentObj:PlayAnimation("followthrough")
                
                    target:SendMessage("ProcessWeaponDamage", self.ParentObj, false, self.Weapon)
                    target:PlayEffect("RootedSpellEffect",3)
                    target:PlayEffect("CastEarth2",3)
                    target:SendMessage("StartMobileEffect", "VenomousAffliction", self.ParentObj, {
                        PulseFrequency = TimeSpan.FromSeconds(1),
                        PulseMax = 8,
                        DamageMin = 5,
                        DamageMax = 15,
                    })
                    target:SendMessage("StartMobileEffect", "Demoralized", self.ParentObj)
        	end
        --end

		EndMobileEffect(root)
	end,

	GetPulseFrequency = nil,
	Range = 1,
	_Facing,
	_Loc
}