--SCAN ADDED
MobileEffectLibrary.MarkedShot = 
{

	OnEnterState = function(self,root,target,args)
		-- set args
		self.AttackModifier = args.AttackModifier or self.AttackModifier

		self.ParentObj:PlayAnimation("block")
		target:PlayEffect("ArenaFlagBlueEffect", 5)
		if (IsMale(self.ParentObj)) then
			self.ParentObj:PlayObjectSound("event:/character/human_male/human_male_attack")
		else
			self.ParentObj:PlayObjectSound("event:/character/human_female/human_female_attack")
		end

		SetCombatMod(self.ParentObj, "AttackTimes", "MarkedShot", self.AttackModifier)
		self.ParentObj:SendMessage("ExecuteHitAction", target, "RightHand", false)
		SetCombatMod(self.ParentObj, "AttackTimes", "MarkedShot", nil)

		EndMobileEffect(root)
	end,

	GetPulseFrequency = nil,
	AttackModifier = 0.30,
	Range = 1,
}