MobileEffectLibrary.WandLightning = 
{

	OnEnterState = function(self,root,target,args)

		self.Range = args.Range or self.Range
		
		self.ParentObj:PlayAnimation("holdcrook")
		self.ParentObj:PlayEffect("ElectricShield",4)
		if (IsMale(self.ParentObj)) then
			self.ParentObj:PlayObjectSound("event:/character/human_male/human_male_attack")
		else
			self.ParentObj:PlayObjectSound("event:/character/human_female/human_female_attack")
		end

		self.Weapon = GetPrimaryWeapon(self.ParentObj)

		if not( self.Weapon ) then
			return EndMobileEffect(root)
		end

		self._Loc = self.ParentObj:GetLoc()
		self._Facing = self.ParentObj:GetFacing()
		
		--BONUS SKILL CHECK
		--self.ParentObj:CheckSkillChance(user,"MagicalAttunementSkill",GetSkillLevel(user,"MagicalAttunementSkill"),0.5)

		local nearbyMobiles = FindObjects(SearchMobileInRange(self.Range, true))
		for i=1,#nearbyMobiles do
			local mobile = nearbyMobiles[i]
        	if ( 
				self.IsInFront(self, mobile)
				and
				ValidCombatTarget(self.ParentObj, mobile, true)
				and
				self.ParentObj:HasLineOfSightToObj(mobile, ServerSettings.Combat.LOSEyeLevel)
			) then
        		mobile:SendMessage("RequestMagicalAttack", "Lightning", mobile, false, self.Weapon)
                mobile:PlayEffect("LightningExplosionEffect")
                mobile:PlayEffect("LightningBallEffect",2)
				self.ParentObj:PlayAnimation("cast_lightning")
        	end
        end

		EndMobileEffect(root)
	end,

	-- determine if mobile is within our 180 degree swing area
	IsInFront = function(self, mobile)
		return ( math.abs( self._Facing - self._Loc:YAngleTo(mobile:GetLoc()) ) <= 90 )
	end,

	GetPulseFrequency = nil,

	Range = 10,

	_Facing,
	_Loc
}