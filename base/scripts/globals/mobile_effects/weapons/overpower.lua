MobileEffectLibrary.Overpower = 
{

	OnEnterState = function(self,root,target,args)
		-- set args

		self.AttackModifier = args.AttackModifier or self.AttackModifier
		self.Target = args.Target or self.Target
		self.Target = target

		
		if (IsMale(self.ParentObj)) then
			self.ParentObj:PlayObjectSound("event:/character/human_male/human_male_attack")
		else
			self.ParentObj:PlayObjectSound("event:/character/human_female/human_female_attack")
		end

		self.ParentObj:PlayAnimation("jump_attack")
		target:PlayEffect("BuffEffect_A")
		SetCombatMod(self.ParentObj, "AttackTimes", "Overpower", self.AttackModifier)
		self.ParentObj:SendMessage("ExecuteHitAction", target, "RightHand", false)
		SetCombatMod(self.ParentObj, "AttackTimes", "Overpower", nil)
		--SCAN ADDED
		self.ParentObj:NpcSpeech("[FFFF00]-Overpower-[-]","combat")	
		local WeaponDamage = math.random(50,100)
		self.Target:SendMessage("ProcessTrueDamage", self.ParentObj, WeaponDamage)
		if ( WeaponDamage > 89 ) then
			self.Target:NpcSpeech("[FF7777]Critical Strike [-]"..WeaponDamage)
		end
		

		EndMobileEffect(root)
	end,

	GetPulseFrequency = nil,
	AttackModifier = 0.0,
	Range = 1,
}