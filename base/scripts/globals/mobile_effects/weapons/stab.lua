MobileEffectLibrary.Stab = 
{
	OnEnterState = function(self,root,target,args)
		-- set args
		self.AttackModifier = args.AttackModifier or self.AttackModifier
		self.StealthAttackModifier = args.StealthAttackModifier or self.StealthAttackModifier

		self.ParentObj:PlayObjectSound("event:/character/combat_abilities/puncture")
		if ( self.ParentObj:HasObjVar("WasHidden") ) then
			SetCombatMod(self.ParentObj, "AttackTimes", "Stab", self.StealthAttackModifier)
		else
			SetCombatMod(self.ParentObj, "AttackTimes", "Stab", self.AttackModifier)
		end
		self.ParentObj:PlayEffect("BuffEffect_A")
		self.ParentObj:SendMessage("ExecuteHitAction", target, "RightHand", false)
		SetCombatMod(self.ParentObj, "AttackTimes", "Stab", nil)
		--SCAN ADDED
		self.ParentObj:NpcSpeech("[FFFF00]-Stabbed-[-]","combat")
			local WeaponDamage = math.random(20,100)
			self.ParentObj:SendMessage("ProcessTrueDamage", self.ParentObj, WeaponDamage)
			if ( WeaponDamage > 89 ) then
				self.ParentObj:NpcSpeech("[FF7777]CriticalStrike [-]"..WeaponDamage)
			end

		EndMobileEffect(root)
	end,

	GetPulseFrequency = nil,
	Range = 1,
	AttackModifier = 0.02,
	StealthAttackModifier = 0.03,
}