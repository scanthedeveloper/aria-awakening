MobileEffectLibrary.Example = 
{
	-- Options flag for this effect to not be removed on death
	--PersistDeath = true,
	
	-- Prevents this effect from being applied to Immune targets and when people go Immune, this effect is dropped.
	--Debuff = true,

	-- Prevents this effect from being applied if any of the effects in the table are currently active
	--PreventEffects = { "ResoundingEchoes" },

	-- Can this be resisted by Willpower?
	--Resistable = true,

	-- Optional flag for the effect to end on movement; sets self._MovementFiredExit(bool) if triggered
	--EndOnMovement = true,

	-- Optional flag for the effect to end on action; sets self._ActionFiredExit(bool) and [self._EndOnActionEventType,_EndOnActiveEventDetails  <--- what trigged breakInvis] if triggered
	--EndOnAction = true,

	-- Optional string; when EndOnAction is called from a weapon ability you can choose to have specific abilities ignored
	-- IgnoreEndOnActionTypes = {"WeaponAbilityName"},

	-- Option flag to force the player to dismount
	--ForceDismount = true,

	OnEnterState = function(self,root,target,args)
		self.PulseFrequency = args.PulseFrequency or self.PulseFrequency
		self.PulseMax = args.PulseMax or self.PulseMax
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "ExamplePulse", "Example", "Force Push 02", "This is a description.", true)
		end
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "ExamplePulse")
		end
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

	-- Optional function to handle when the effect is applied while already active, set nil or keep commented out when not needed.
--[[
	OnStack = function(self,root,target,args)

	end,
]]

	-- Optional function when the effect receives the end effect message, This is NOT called when the effect ends internally, only when an external source trys to end the effect.
--[[
	OnEndEffect = function(self,root)
		self.ParentObj:NpcSpeech("EndEffect!")
		-- must call EndMobileEffect in here (unless you have a reason not to) if OnEndEffect is provided.
		EndMobileEffect(root)
	end,
]]


	AiPulse = function(self,root)
		self.CurrentPulse = self.CurrentPulse + 1
		if ( self.CurrentPulse > self.PulseMax ) then
			EndMobileEffect(root)
		else

		end
	end,

	PulseFrequency = TimeSpan.FromSeconds(1),
	PulseMax = 1,
	CurrentPulse = 0,
}