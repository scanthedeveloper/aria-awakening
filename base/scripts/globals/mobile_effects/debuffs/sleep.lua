MobileEffectLibrary.Sleep = 
{

	Debuff = true,
	-- Can this be resisted by Willpower?
	Resistable = true,

	OnEnterState = function(self,root,target,args)
		args = args or {}

        SetMobileMod(self.ParentObj, "Disable", "Sleep", true)
        SetMobileMod(self.ParentObj, "Freeze", "Sleep", true)

		self.ParentObj:SendMessage("CancelSpellCast")
        self.ParentObj:NpcSpeech("[FF0000]*slept*[-]", "combat")
        self.ParentObj:PlayEffect("StunnedEffectObject")


        self.IsPlayer = self.ParentObj:IsPlayer()
		if ( self.IsPlayer ) then
			AddBuffIcon(self.ParentObj, "Sleep", "Sleeping", "Night", "You are sleeping and cannot move or act.", true)
		end

		RegisterEventHandler(EventType.Message, "BreakInvisEffect", function( what )
            if( what == "Damage" ) then
                EndMobileEffect(root)
            end
		end)
		
	end,

	OnExitState = function(self,root)
		SetMobileMod(self.ParentObj, "Disable", "Sleep", nil)
        SetMobileMod(self.ParentObj, "Freeze", "Sleep", nil)
        self.ParentObj:SendMessage("ResetSwingTimer", 0)
        self.ParentObj:StopEffect("StunnedEffectObject")

		if ( self.IsPlayer ) then
			RemoveBuffIcon(self.ParentObj, "Sleep")
		end

		UnregisterEventHandler("", EventType.Message, "OnResurrect")
	end,

	--OnStack = OnStackRefreshDuration,

	GetPulseFrequency = function(self,root) 
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromSeconds(10),
}