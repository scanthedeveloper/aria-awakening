MobileEffectLibrary.CapitalPunishment = 
{
	--- Optional flag for this effect to apply (if there's enough duration left) on login.
	PersistSession = true,

	-- Options flag for this effect to not be removed on death
	PersistDeath = true,

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "CapitalPunishment", "Capital Punishment", "shadowrun", "All stats lowered by 60%", true, self.Duration)
        end
        
		SetMobileMod(self.ParentObj, "MaxHealthTimes", "CapitalPunishment", -0.6)
        SetMobileMod(self.ParentObj, "MaxManaTimes", "CapitalPunishment", -0.6)
        -- whatever else
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "CapitalPunishment")
		end
		SetMobileMod(self.ParentObj, "MaxHealthTimes", "CapitalPunishment", nil)
        SetMobileMod(self.ParentObj, "MaxManaTimes", "CapitalPunishment", nil)
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
    end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromMinutes(20),
}