MobileEffectLibrary.ArmsmasterSpreadFormation = 
{

    Debuff = true,
	
    OnEnterState = function(self,root, target, args)
        self._Args = args or {}
        self.BossObj = target

        self.ParentObj:PlayEffect("AvoidArrowEffect")

        -- We end if we are told to!
        RegisterSingleEventHandler(EventType.Message, "RemoveSpreadFormation", function() EndMobileEffect(root) end) 
    end,

    OnExitState = function(self,root)
        self.ParentObj:PlayEffect("FireballExplosionEffect")
        self.ParentObj:PlayObjectSound("event:/magic/air/magic_air_lightning_impact")

        local players = FindObjects(SearchMulti({SearchUser(),SearchRange(self.ParentObj:GetLoc(),5)}))
        for i,player in pairs (players) do 
            if( player ~= self.ParentObj ) then
                ForeachMobileAndPet( player, function(mobile) 
                    local health = GetMaxHealth(mobile)
                    mobile:SendMessage("ProcessTrueDamage", self.BossObj, ( health * 0.5 ), true)
                end)
            end
        end
        self.ParentObj:StopEffect("AvoidArrowEffect")
	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(2)
	end,

	AiPulse = function(self,root)
        if( self.BossObj and self.BossObj:IsValid() and not IsDead(self.BossObj) ) then
            self.ParentObj:PlayEffect("AvoidArrowEffect")
        else
            EndMobileEffect(root)
        end
    end,

}