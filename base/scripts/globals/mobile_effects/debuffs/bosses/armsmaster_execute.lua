MobileEffectLibrary.ArmsmasterExecutionMarked = 
{

    Debuff = true,
	
    OnEnterState = function(self,root, target, args)
        self._Args = args or {}
        self.BossObj = target

		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "ExecuteMarked", "Impending Doom", "Seismic Slam 01", "You are marked for execution." , true)
        end

        RegisterEventHandler(EventType.Message, "DoArmsmasterExecute", function()
            DebugMessage("GotMessage : DoArmsmasterExecute")
            self.DoExecute(self, root)
        end)

        -- We end if we are told to!
        RegisterSingleEventHandler(EventType.Message, "RemoveArmsmasterExecute", function() EndMobileEffect(root) end) 
    end,

    OnExitState = function(self,root)
        self.ParentObj:StopEffect("AvoidArrowEffect")
        UnregisterEventHandler("", EventType.Message, "DoArmsmasterExecute")
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "ExecuteMarked")
        end
	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(2)
	end,

	AiPulse = function(self,root)
        if( self.BossObj and self.BossObj:IsValid() and not IsDead(self.BossObj) ) then
            self.ParentObj:PlayEffect("AvoidArrowEffect")
        else
            EndMobileEffect(root)
        end
    end,

    DoExecute = function(self,root)
        -- The conductor is valid?
        if( self.BossObj and self.BossObj:IsValid() ) then
            PlayEffectAtLoc("WispSummonEffect", self.ParentObj:GetLoc(), 3)
            self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact")
		    self.ParentObj:SendMessage("ProcessTrueDamage", self.ParentObj, 10000, true)
        end
        EndMobileEffect(root)
    end
}