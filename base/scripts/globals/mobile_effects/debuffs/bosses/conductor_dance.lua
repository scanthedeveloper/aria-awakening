MobileEffectLibrary.ConductorDance = 
{

    Debuff = true,
    Stacked = false,
	
    OnEnterState = function(self,root, target, args)
        self._Args = args or {}
        self.DebuffIndicatorColor = self._Args.DebuffIndicatorColor
        self.ConductorObj = target
        self._Stacks = self._Args.Stacks or self._Stacks or 0
        self._DamagePercentPerStack = self._Args.DamagePercentPerStack or self._DamagePercentPerStack or self._DamagePercentPerStack or 10
        self.DamagePercentIncrease = (self._DamagePercentPerStack * self._Stacks)

        if( self.DebuffIndicatorColor == nil ) then
            local random = math.random( 1,100 )
            if( random > 50 ) then
                self.DebuffIndicatorColor = "MagicPrimeTrailRedEffect"
            else
                self.DebuffIndicatorColor = "MagicPrimeTrailGreenEffect"
            end
        end

		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "ConductorDance", "Overcharge", "shadowrun", "Echo damaged taken: " .. self.DamagePercentIncrease .. "%" , true)
        end

        RegisterEventHandler(EventType.Message, "ConductorDanceAOE", function()
            self.TakeDamageFromAOE(self, root)
        end)

        -- We end if we are told to!
        RegisterSingleEventHandler(EventType.Message, "ConductorDanceEnd", function() EndMobileEffect(root) end) 
    end,

    OnExitState = function(self,root)
        self.ParentObj:StopEffect(self.DebuffIndicatorColor)
        UnregisterEventHandler("", EventType.Message, "ConductorDanceAOE")
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "ConductorDance")
        end
	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
	end,

	AiPulse = function(self,root)
        if( self.ConductorObj and self.ConductorObj:IsValid() ) then
            
            self.ParentObj:PlayEffectWithArgs(self.DebuffIndicatorColor,2.0,"Bone=Ground")

            local safeZones = FindObjects(SearchMulti{SearchRegion("1-F-arena"),SearchHasObjVar("SafeEffect")})
            for i,zone in pairs(safeZones) do 
                if( zone:GetObjVar("SafeEffect") == self.DebuffIndicatorColor ) then

                    if( self.ParentObj:DistanceFrom(zone) <= (zone:GetObjVar("Radius") or 3.5) ) then
                        if( self._Stacks > 0 ) then
                            --self.ParentObj:SystemMessage("Echo damaged taken decreased.")
                        end
                        self._Stacks = self._Stacks - 1
                    else
                        --self.ParentObj:SystemMessage("Echo damaged taken increased.")
                        self._Stacks = self._Stacks + 1
                    end
                -- If they stand in the wrong zone they get 3x stacks!
                else
                    if( self.ParentObj:DistanceFrom(zone) <= (zone:GetObjVar("Radius") or 3.5) ) then
                        self._Stacks = self._Stacks + 3
                    end
                end
            end

            -- Make sure our stacks don't go negative
            self._Stacks = math.max(0, self._Stacks)
            self.DamagePercentIncrease = (self._DamagePercentPerStack * self._Stacks)

            -- Update the buff with the new values
            if ( self.ParentObj:IsPlayer() ) then
                --DebugMessage("Buff : " .. self._Stacks, self.DamagePercentIncrease)
                AddBuffIcon(self.ParentObj, "ConductorDance", "Overcharge", "shadowrun", "Echo damaged taken: +" .. self.DamagePercentIncrease .. "%" , true)
            end

        else
            EndMobileEffect(root)
        end
    end,

    TakeDamageFromAOE = function(self,root)
        -- The conductor is valid?
        if( self.ConductorObj and self.ConductorObj:IsValid() ) then
            self.DamageTaken = 100 * ( 1 + ( self.DamagePercentIncrease / 100 ) )
            --self.ParentObj:SendMessage("ProcessTypeDamage", self.Target, self.DamageTaken, "TrueDamage", false, false)
            self.ParentObj:SendMessage("ProcessTrueDamage", self.Target, self.DamageTaken, true)
        else 
            EndMobileEffect(root)
        end
    end
}