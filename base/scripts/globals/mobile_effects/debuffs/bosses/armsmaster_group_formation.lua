MobileEffectLibrary.ArmsmasterGroupFormation = 
{

    Debuff = true,
	
    OnEnterState = function(self,root, target, args)
        self._Args = args or {}
        self.BossObj = target

		if ( self.ParentObj:IsPlayer() ) then
            if( self._Args.IsLeader ) then
                self.ParentObj:SystemMessage("["..COLORS.Green.."]You are formation leader.[-]", 'info')
                AddBuffIcon(self.ParentObj, "ArmsmasterGroupFormation", "Commander", "Berserker Rage", "You are formation leader." , true)
                self.ParentObj:PlayEffect("TargetArrowEffect")
            else
                AddBuffIcon(self.ParentObj, "ArmsmasterGroupFormation", "Commander", "Blink Strike", "You are in formation." , true)
            end
        end

        -- We end if we are told to!
        RegisterSingleEventHandler(EventType.Message, "RemoveGroupFormation", function() EndMobileEffect(root) end) 
    end,

    OnExitState = function(self,root)
        self.ParentObj:StopEffect("TargetArrowEffect")
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "ArmsmasterGroupFormation")
        end
	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(0.5)
	end,

	AiPulse = function(self,root)
        if( self.BossObj and self.BossObj:IsValid() and not IsDead(self.BossObj) ) then
            if( self._Args.IsLeader ) then
                self.ParentObj:PlayEffect("TargetArrowEffect")
                local players = FindObjects(SearchMulti({SearchUser(),SearchRange(self.ParentObj:GetLoc(),5)}))
                for i,player in pairs (players) do 
                    if( player ~= self.ParentObj ) then
                        players[i]:SendMessage("StartMobileEffect", "ArmsmasterGroupFormation", self.ParentObj)
                    end
                end
            end
        else
            EndMobileEffect(root)
        end
    end,

}