MobileEffectLibrary.MortalStruck = 
{
	Debuff = true,
	PreventEffects = { "ResoundingDrums" },

	-- Can this be resisted by Willpower?
	Resistable = true,

	OnEnterState = function(self,root,target,args)
		
		-- Determine if a pet is mortal strike immuned
		if( IsPet( self.ParentObj ) ) then
			local owner = GetPetOwner(self.ParentObj)
			if( owner:IsValid() ) then
				local ability, class = GetPrestigeAbilityNameClass(owner, 1)
				if( ability == "NaturesGrace" ) then
					DoMobileImmune(self.ParentObj, "mortal strike")
					EndMobileEffect(root)
					return false
				end
			end
		end
		
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "MortalStruck", "Mortal Strike", "Deep Cuts", "Cannot heal with bandages or magic.", true)
			ShowStatusElement(self.ParentObj,{IsSelf=true, HealthHue="FF9E00"})
		end
		self.ParentObj:PlayEffect("LaughingSkullEffect")
		--SCAN ADDED
		self.ParentObj:NpcSpeech("[FFFF00]-Mortally Struck-[-]","combat")
		local WeaponDamage = math.random(50,100)
		self.ParentObj:SendMessage("ProcessTrueDamage", self.ParentObj, WeaponDamage)
		if ( WeaponDamage > 89 ) then
			self.ParentObj:NpcSpeech("[FF7777]CriticalStrike [-]"..WeaponDamage)
		end
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "MortalStruck")
			ShowStatusElement(self.ParentObj,{IsSelf=true, HealthHue="FF0000"})
		end
		StartMobileEffect(self.ParentObj, "NoMortalStrike")
		self.ParentObj:StopEffect("LaughingSkullEffect")
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

	AiPulse = function(self,root)
		self.CurrentPulse = self.CurrentPulse + 1
		if ( self.CurrentPulse > self.PulseMax ) then
			EndMobileEffect(root)
		else
			self.ParentObj:PlayEffect("LaughingSkullEffect")
		end
	end,

	PulseFrequency = TimeSpan.FromSeconds(2),
	PulseMax = 4,
	CurrentPulse = 0,
}