MobileEffectLibrary.Concus = 
{
	Debuff = true,

	OnEnterState = function(self,root,opponent,args)				
		self.Opponent = opponent
		self.Duration = args.Duration or self.Duration
		self.Modifier = args.Modifier or self.Modifier

		SetMobileMod(self.ParentObj, "IntelligenceTimes", "Concused", self.Modifier)
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "DebuffConcused", "Concused", "Terrify", "Intelligence Reduced by "..-(self.Modifier*100).."%", true)
		end

		self.ParentObj:PlayEffect("BuffEffect_D")
		self.ParentObj:PlayObjectSound("event:/character/combat_abilities/male_shout")
		--SCAN ADDED
		self.ParentObj:NpcSpeech("[FFFF00]-Concused-[-]","combat")
			local WeaponDamage = math.random(50,100)
			self.ParentObj:SendMessage("ProcessTrueDamage", self.ParentObj, WeaponDamage)
			if ( WeaponDamage > 89 ) then
				self.ParentObj:NpcSpeech("[FF7777]CriticalStrike [-]"..WeaponDamage)
			end
	end,

	OnExitState = function(self,root)
		SetMobileMod(self.ParentObj, "IntelligenceTimes", "Concused", nil)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "DebuffConcused")
		end
		self.ParentObj:StopEffect("BuffEffect_D")
	end,

	GetPulseFrequency = function(self,root) 
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Opponent,
	Duration = TimeSpan.FromSeconds(1),
	Modifier = -0.2
}