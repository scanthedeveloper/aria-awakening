MobileEffectLibrary.Hamstring = 
{
	Debuff = true,
	PreventEffects = { "ResoundingEchoes" },

	-- Can this be resisted by Willpower?
	Resistable = true,

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Modifier = args.Modifier or self.Modifier
		if ( self.Modifier > 0 or self.Modifier < -1 ) then
			self.Modifier = 0
		end

		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "Hamstring", "Hamstring", "Weapon Throw", "Movement slowed by "..((-self.Modifier)*100).."%.", true)
		else
			self.ParentObj:SendMessage("AddThreat", target, 1)
		end
		SetMobileMod(self.ParentObj, "MoveSpeedTimes", "Hamstring", self.Modifier)
		self.ParentObj:NpcSpeech("[FFFF00]-Hamstrung-[-]","combat")

		self.ParentObj:PlayObjectSound("event:/character/combat_abilities/impale")
		
		-- Determine the weapon type of the attacker (target) and play the correct animations
		if( Weapon.IsRanged( Weapon.GetType( Weapon.GetPrimary( target ) ) ) ) then
			PerformClientArrowShot(target, self.ParentObj, Weapon.GetPrimary( target ))
		else
			PlayAttackAnimation(target)
		end
		
		
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "Hamstring")
		end
		SetMobileMod(self.ParentObj, "MoveSpeedTimes", "Hamstring", nil)
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromSeconds(1),
	Modifier = -0.1,
}