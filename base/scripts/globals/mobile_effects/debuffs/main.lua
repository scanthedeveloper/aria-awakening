require 'globals.mobile_effects.debuffs.stun'
require 'globals.mobile_effects.debuffs.daze'
require 'globals.mobile_effects.debuffs.concus'
require 'globals.mobile_effects.debuffs.sunder'
require 'globals.mobile_effects.debuffs.hamstring'
require 'globals.mobile_effects.debuffs.eviscerate'
require 'globals.mobile_effects.debuffs.bleed'
require 'globals.mobile_effects.debuffs.dismount'
require 'globals.mobile_effects.debuffs.poison'
require 'globals.mobile_effects.debuffs.scavengable'
require 'globals.mobile_effects.debuffs.mortalstruck'
require 'globals.mobile_effects.debuffs.demoralized'
require 'globals.mobile_effects.debuffs.sleep'

--SCAN ADDED
require 'globals.mobile_effects.debuffs.jawbreaker'

-- player
require 'globals.mobile_effects.debuffs.player.hungry'
require 'globals.mobile_effects.debuffs.player.lowvitality'
require 'globals.mobile_effects.debuffs.player.capitalpunishment'
require 'globals.mobile_effects.debuffs.player.increaseskillgain'

--skills
require 'globals.mobile_effects.debuffs.skills.heavyarmordebuff'

-- rogue
require 'globals.mobile_effects.debuffs.rogue.vanish'
require 'globals.mobile_effects.debuffs.rogue.huntersmark'

-- mage
require 'globals.mobile_effects.debuffs.mage.silence'
require 'globals.mobile_effects.debuffs.mage.destructionaoe'

-- mounts
require 'globals.mobile_effects.debuffs.dazed'
require 'globals.mobile_effects.debuffs.nomount'

-- spells
require 'globals.mobile_effects.debuffs.chilled'

-- bosses
require 'globals.mobile_effects.debuffs.bosses.conductor_dance'
require 'globals.mobile_effects.debuffs.bosses.armsmaster_execute'
require 'globals.mobile_effects.debuffs.bosses.armsmaster_group_formation'
require 'globals.mobile_effects.debuffs.bosses.armsmaster_spread_formation'