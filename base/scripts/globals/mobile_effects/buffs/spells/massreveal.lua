MobileEffectLibrary.MassReveal = 
{
    OnEnterState = function(self,root,target,args)
        self.ParentObj:PlayAnimation("cast_heal")
        
        local players = FindObjects(SearchPlayerInRange(10, true), self.ParentObj)
        self.Revealed = false

        for k,player in pairs(players) do
            if( self.ParentObj:HasLineOfSightToObj(player,ServerSettings.Combat.LOSEyeLevel) ) then

                if ( HasMobileEffect(player, "Hide") or player:HasModule("sp_cloak_effect") ) then
                    -- If the players are in an active conflict we always let them reveal
                    if( GetConflictRelation( self.ParentObj, player ) or IsCriminal( player ) or not IsCriminalAction(self.ParentObj, player) ) then
                        player:SendMessage("BreakInvisEffect", "Debuff", "Reveal")
                        self.Revealed = true
                    else
                        ExecuteCriminalAction(self.ParentObj)
                        player:SendMessage("BreakInvisEffect", "Debuff", "Reveal")
                        self.Revealed = true
                    end
                end
            end
        end

		EndMobileEffect(root)
	end,

    OnExitState = function(self,root)
        if( self.Revealed ) then
            self.ParentObj:SystemMessage("You have revealed someone.", "info")
        else
            self.ParentObj:SystemMessage("Nothing was revealed.", "info")
        end
	end,
}