
MobileEffectLibrary.ChargeRunebook = 
{
	OnEnterState = function(self,root,target,args)
		if(target:GetObjVar("ResourceType") == "Runebook") then
			local charges, maxCharges = GetRunebookCharges(target)
			if (charges >= maxCharges) then
				self.ParentObj:SystemMessage("Runebook is already full charged.","info")
				EndMobileEffect(root)
				return false
			end			

			charges = math.min(maxCharges,(charges + self.RechargeCount))
			target:SetObjVar("RuneCount",charges)

			self.ParentObj:SystemMessage("You transfer some magical energy into the runebook.","info")

			local topmost = target:TopmostContainer() or target
			topmost:PlayEffect("TeleportToEffect")
		else
			self.ParentObj:SystemMessage("That is not a runebook.","info")
		end

		EndMobileEffect(root)
		return false
	end,

	RechargeCount = 5,
}