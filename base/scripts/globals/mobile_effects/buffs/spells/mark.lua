NoMarkSubregions = 
{
	--"Subregion-BarrenLands",
	"NoMark",
	"Arena",
	--"BlackForest",
}

MobileEffectLibrary.Mark = 
{
	ValidateLoc = function (self,destLoc)

		if (IsLocInRegion(destLoc, "NoTeleport")) then return false end

		if (IsDungeonMap()) then return false end

		-- prevent marking inside plots
		local plotController = Plot.GetAtLoc(destLoc)
		if ( plotController ~= nil ) then return false end
		

		local regions = GetRegionsAtLoc(destLoc)
		for i=1, #NoMarkSubregions do

			for j,regionName in pairs(regions) do
				--DebugMessage(regionName.." "..NoMarkSubregions[i])
				if (regionName == NoMarkSubregions[i]) then
					--DebugMessage(regionName.." "..NoMarkSubregions[i])
					return false
				end
			end

		end
		return true
	end,	

	OnEnterState = function(self,root,target,args)
		if(not target:HasObjVar("Destination") and target:GetObjVar("ResourceType") == "Rune") then

			if ( not IsInBackpack(target,self.ParentObj) ) then
				self.ParentObj:SystemMessage("Rune must be placed in your backpack.","info")
				EndMobileEffect(root)
				return false
			end

			if (target:HasObjVar("StaticDestination")) then
				self.ParentObj:SystemMessage("Rune is already marked.","info")
				EndMobileEffect(root)
				return false
			end

			local destRegionAddress = ServerSettings.RegionAddress			
			local destLoc = self.ParentObj:GetLoc()
			local destFacing = self.ParentObj:GetFacing()

			if(self:ValidateLoc(destLoc)) then
				MarkRune(target,destLoc,destRegionAddress,destFacing)

				target:SetName("Teleportation Rune")
				target:SendMessage("Mark")

				self.ParentObj:PlayEffect("TeleportToEffect")
				self.ParentObj:PlayObjectSound("event:/magic/air/magic_air_cast_air",false)
				self.ParentObj:SystemMessage("The rune is now marked for travel to this location.","info")
			else
				self.ParentObj:SystemMessage("This location lacks the magical energy for teleportation.","info")
			end
			--end
		else
			self.ParentObj:SystemMessage("That is not a blank rune.","info")
		end

		EndMobileEffect(root)
		return false

	end,
}