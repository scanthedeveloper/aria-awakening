MobileEffectLibrary.RelentlessHate = 
{

	OnEnterState = function(self,root,target,args)
        
        -- If we don't have a target; exit
        if ( target == nil ) then
			EndMobileEffect(root)
			return false
        end

        -- If we don't have line of sight; exit
        if( not self.ParentObj:HasLineOfSightToObj(target,ServerSettings.Combat.LOSEyeLevel) ) then
            EndMobileEffect(root)
			return false
        end

        -- We need to play an effect where we currently are
        self.ParentObj:PlayAnimation("roar")
        PlayEffectAtLoc( "BuffEffect_B", self.ParentObj:GetLoc(), 1 )
        local targetLoc = target:GetLoc()
        target:SendMessage("StartMobileEffect", "Stun", self.ParentObj, {Duration=TimeSpan.FromSeconds(3)})
        local destLoc = target:GetLoc()
        if IsValidLoc(destLoc, false, {"NoTeleport"}) then
            self.ParentObj:SetWorldPosition(destLoc)
        else
            local bufferedLoc = GetClosestLocationBuffered(self.ParentObj:GetLoc(), destLoc, 1, false, {"NoTeleport"})
            if bufferedLoc then
                self.ParentObj:SetWorldPosition(bufferedLoc)
            else
                self.ParentObj:SystemMessage("That foe blocked the teleportation!", "info")
            end
        end
        --self.ParentObj:SetFacing(target)
        PlayEffectAtLoc( "BuffEffect_B", destLoc, 1 )
        EndMobileEffect(root)

    end,
    
}