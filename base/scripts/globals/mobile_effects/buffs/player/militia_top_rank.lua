-- Top rank effect disabled
MobileEffectLibrary.MilitiaTopRank =
{
	OnEnterState = function(self,root,target,args)
		EndMobileEffect(root)
    end,
}

--[[MobileEffectLibrary.MilitiaTopRank = 
{
    --PersistDeath = true,

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.AiPulse(self, root)
    end,

	OnExitState = function(self,root,target,args)
		self.ParentObj:StopEffect("FireHeadEffect")
		self.ParentObj:StopEffect("LightningBodyEffect")
		self.ParentObj:StopEffect("RegenEffect")
		RemoveBuffIcon(self.ParentObj, "MilitiaTopRank")
    end,

  	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(60)
	end,

	TrackMe = function(playerObj, militiaId)
		if ( not playerObj:IsCloaked() ) then
			Militia.SetVar("TopRank"..militiaId, function(record)
				record[1] = playerObj
				record[2] = playerObj:GetLoc()
				record[3] = DateTime.UtcNow
				return true
			end)
		end
	end,

	AiPulse = function(self, root)
		self.ParentObj:StopEffect("FireHeadEffect")
		self.ParentObj:StopEffect("LightningBodyEffect")
		self.ParentObj:StopEffect("RegenEffect")
		local rank = Militia.GetRankNumber(self.ParentObj)
		local militiaId = self.ParentObj:GetObjVar("Militia")
		if ( rank == nil or rank < 7 or militiaId == nil or IsDead(self.ParentObj) ) then
			EndMobileEffect(root)
			return false
		else
			if ( militiaId == 1 ) then
				self.ParentObj:PlayEffect("FireHeadEffect")
				AddBuffIcon(self.ParentObj, "MilitiaTopRank",ServerSettings.Militia.Militias[militiaId].TextColor..ServerSettings.Militia.Militias[militiaId].Ranks[7].Name.."[-]","Fire Nova","You are the highest ranked member of the "..ServerSettings.Militia.Militias[militiaId].TextColor..ServerSettings.Militia.Militias[militiaId].Name.."[-] Militia.", false)
				self.TrackMe(self.ParentObj, 1)
			elseif ( militiaId == 2 ) then
				self.ParentObj:PlayEffect("LightningBodyEffect")
				AddBuffIcon(self.ParentObj, "MilitiaTopRank",ServerSettings.Militia.Militias[militiaId].TextColor..ServerSettings.Militia.Militias[militiaId].Ranks[7].Name.."[-]","Lightning Nova","You are the highest ranked member of the "..ServerSettings.Militia.Militias[militiaId].TextColor..ServerSettings.Militia.Militias[militiaId].Name.."[-] Militia.", false)
				self.TrackMe(self.ParentObj, 2)
			elseif ( militiaId == 3 ) then
				self.ParentObj:PlayEffectWithArgs("RegenEffect",0.0,"Color=004400")
				AddBuffIcon(self.ParentObj, "MilitiaTopRank",ServerSettings.Militia.Militias[militiaId].TextColor..ServerSettings.Militia.Militias[militiaId].Ranks[7].Name.."[-]","Regrowth","You are the highest ranked member of the "..ServerSettings.Militia.Militias[militiaId].TextColor..ServerSettings.Militia.Militias[militiaId].Name.."[-] Militia.", false)
				self.TrackMe(self.ParentObj, 3)
			end
		end
	end,
	
    Duration = TimeSpan.FromSeconds(60),
}]]