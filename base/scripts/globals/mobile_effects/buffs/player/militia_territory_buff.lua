MobileEffectLibrary.MilitiaTerritoryBuff = 
{
	OnEnterState = function(self,root,target,args)
		self.AiPulse(self, root)
		return self.SetBuffs(self,root)
	end,
	
	OnStack = function(self,root,target,args)
		self._CleanUp(self, root)
        self.OnEnterState(self,root,target,args)
	end,

  	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(3)
	end,

	AiPulse = function(self, root)
		local militiaId = self.ParentObj:GetObjVar("Militia") or TownshipsHelper.GetMilitaId(nil, self.ParentObj)
		if ( not militiaId or HasMobileEffect(self.ParentObj, "RankedArenaMatch") ) then
			EndMobileEffect(root)
		end
	end,

	SetBuffs = function(self,root)
		local militiaId = self.ParentObj:GetObjVar("Militia") or TownshipsHelper.GetMilitaId(nil, self.ParentObj)
		local events = GlobalVarRead("Militia.Events")

		local flagsControlled = 0

		if ( events and next(events) ) then
			local buffDescription = ""
			for controller, data in pairs(events) do
				if ( militiaId and
				data[2] and
				data[4] and
				tonumber(data[2]) == tonumber(militiaId) ) then					
					if ( tonumber(data[4]) == 1 ) then
						flagsControlled = flagsControlled + 1
						buffDescription = buffDescription..ServerSettings.Militia.Militias[1].TextColor.."Control of "..ServerSettings.Militia.Militias[1].Town.."[-]\n" --pyros
						--SetMobileMod(self.ParentObj, "SkillGainIncreaseTimes", "MilitiaTerritorySkillGainIncreaseTimes", 5)
					elseif ( tonumber(data[4]) == 2 ) then
						flagsControlled = flagsControlled + 1
						buffDescription = buffDescription..ServerSettings.Militia.Militias[2].TextColor.."Control of "..ServerSettings.Militia.Militias[2].Town.."[-]\n" --helm
						
					elseif ( tonumber(data[4]) == 3 ) then
						flagsControlled = flagsControlled + 1
						buffDescription = buffDescription..ServerSettings.Militia.Militias[3].TextColor.."Control of "..ServerSettings.Militia.Militias[3].Town.."[-]\n" --eldeir	
					end
				end
			end

			if(flagsControlled >= 1) then
				buffDescription = buffDescription.."Conquer other towns to gain buffs\n"
			end

			if(flagsControlled >= 2) then

				SetMobileMod(self.ParentObj, "ArachnidDamageReductionTimes", "MilitiaTerritoryArachnidDamageReductionTimes", 15)
				buffDescription = buffDescription.."+15% Arachnid Resistance\n"
				SetMobileMod(self.ParentObj, "DemonDamageReductionTimes", "MilitiaTerritoryDemonDamageReductionTimes", 10)
				buffDescription = buffDescription.."+10% Demon Resistance\n"
				SetMobileMod(self.ParentObj, "AnimalDamageReductionTimes", "MilitiaTerritoryAnimalDamageReductionTimes", 10)
				buffDescription = buffDescription.."+10% Animal Resistance\n"
				SetMobileMod(self.ParentObj, "UndeadDamageReductionTimes", "MilitiaTerritoryUndeadDamageReductionTimes", 15)
				buffDescription = buffDescription.."+15% Undead Resistance\n"				

				SetMobileMod(self.ParentObj, "LumberjackingDelayReductionTimes", "MilitiaTerritoryLumberjackingDelayReductionTimes", 20)
				buffDescription = buffDescription.."+20% Lumberjacking Rate\n"
				SetMobileMod(self.ParentObj, "FishingDelayReductionTimes", "MilitiaTerritoryFishingDelayReductionTimes", 20)
				buffDescription = buffDescription.."+20% Fishing Rate\n"
				SetMobileMod(self.ParentObj, "MiningDelayReductionTimes", "MilitiaTerritoryMiningDelayReductionTimes", 20)
				buffDescription = buffDescription.."+20% Mining Rate\n"
			end
			if(flagsControlled >= 3) then
				self.ParentObj:SendMessage("StartMobileEffect", "IncreaseSkillGain", nil )
				buffDescription = buffDescription.."+5% Skill Gain Increase\n"

				SetMobileMod(self.ParentObj, "GoldIncreaseTimes", "MilitiaTerritoryGoldIncreaseTimes", 5)
				buffDescription = buffDescription.."+10% Gold Drop Rate\n"
			end

			if ( buffDescription and buffDescription ~= "" ) then
				AddBuffIcon(self.ParentObj, "MilitiaTerritoryBuff", "Warlord's Blessing", "Holy Mastery", buffDescription, false)
			else
				EndMobileEffect(root)
				return false
			end
		end
	end,

	_CleanUp = function(self,root)
		--pyros
		SetMobileMod(self.ParentObj, "PowerPlus", "MilitiaTerritoryPowerPlus", nil)
		SetMobileMod(self.ParentObj, "WillPlus", "MilitiaTerritoryWillPlus", nil)
		SetMobileMod(self.ParentObj, "ManaRegenPlus", "MilitiaTerritoryManaRegenPlus", nil)
		--helm
		SetMobileMod(self.ParentObj, "AttackPlus", "MilitiaTerritoryAttackPlus", nil)
		SetMobileMod(self.ParentObj, "WisdomPlus", "MilitiaTerritoryWisdomPlus", nil)
		SetMobileMod(self.ParentObj, "HealthRegenPlus", "MilitiaTerritoryHealthRegenPlus", nil)
		--eldeir
		SetMobileMod(self.ParentObj, "HealingReceivedPlus", "MilitiaTerritoryHealingReceivedPlus", nil)
		SetMobileMod(self.ParentObj, "StaminaRegenPlus", "MilitiaTerritoryStaminaRegenPlus", nil)
		SetMobileMod(self.ParentObj, "DefensePlus", "MilitiaTerritoryDefensePlus", nil)
		--fremen quarry
		SetMobileMod(self.ParentObj, "DemonDamageReductionTimes", "MilitiaTerritoryDemonDamageReductionTimes", nil)
		SetMobileMod(self.ParentObj, "AnimalDamageReductionTimes", "MilitiaTerritoryAnimalDamageReductionTimes", nil)
		SetMobileMod(self.ParentObj, "MiningDelayReductionTimes", "MilitiaTerritoryMiningDelayReductionTimes", nil)
		--valus cemetery
		SetMobileMod(self.ParentObj, "UndeadDamageReductionTimes", "MilitiaTerritoryUndeadDamageReductionTimes", nil)
		SetMobileMod(self.ParentObj, "HumanoidDamageReductionTimes", "MilitiaTerritoryHumanoidDamageReductionTimes", nil)
		SetMobileMod(self.ParentObj, "FishingDelayReductionTimes", "MilitiaTerritoryFishingDelayReductionTimes", nil)
		--spiritwood lumber
		SetMobileMod(self.ParentObj, "OrkDamageReductionTimes", "MilitiaTerritoryOrkDamageReductionTimes", nil)
		SetMobileMod(self.ParentObj, "ArachnidDamageReductionTimes", "MilitiaTerritoryArachnidDamageReductionTimes", nil)
		SetMobileMod(self.ParentObj, "LumberjackingDelayReductionTimes", "MilitiaTerritoryLumberjackingDelayReductionTimes", nil)
		
		self.ParentObj:SendMessage("EndIncreaseSkillGain" )

		RemoveBuffIcon(self.ParentObj, "MilitiaTerritoryBuff")
	end,

	OnExitState = function(self,root,target,args)
		self._CleanUp(self,root)
    end,
}