
MobileEffectLibrary.Hearth = 
{

	OnEnterState = function(self,root,target,args)
		if ( not self.ParentObj:IsPlayer() ) then
			EndMobileEffect(root)
			return false
		end

		self.Target = target

		AddBuffIcon(self.ParentObj, "HearthEffect", "Hearth", "Ignite", "You are warmed by the hearth fire. Listening to a bard song will restore vitality.", false)
	end,

	OnExitState = function(self,root)
		RemoveBuffIcon(self.ParentObj, "HearthEffect")
	end,

	

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

	AiPulse = function(self,root)
		if( not self.Target:IsValid() or self.ParentObj:DistanceFrom(self.Target) > ServerSettings.Instrument.MaximumHearthRange ) then
			EndMobileEffect(root)	
		end
	end,

	PulseFrequency = TimeSpan.FromSeconds(2), --used as a way to verify the effect is still valid
}