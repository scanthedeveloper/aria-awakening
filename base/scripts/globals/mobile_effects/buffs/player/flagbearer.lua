MobileEffectLibrary.Flagbearer = 
{
	PersistSession = true,

	OnEnterState = function(self,root,target,args)
		self.ParentObj:SendMessage("EndInvulnerableCloakEffect")
		self.Duration = args.Duration or ServerSettings.Militia.Events[3].FlagbearerDuration
		self.ParentObj:ScheduleTimerDelay(self.Duration,"FlagbearerDuration")
		RegisterSingleEventHandler(EventType.Timer,"FlagbearerDuration",function()
			EndMobileEffect(root)
			return
		end)
		RegisterSingleEventHandler(EventType.UserLogout,"",function (...)self.Logout(self,root,...)end)
		RegisterSingleEventHandler(EventType.Message, "UseObject", 
		function(user,usedType)
			if(usedType == "Drop Flag" ) then
				EndMobileEffect(root)
				return
			end
		end)
		RegisterEventHandler(EventType.Timer,"LocPulse",function()self.LocPulse(self, root)end)
		self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"LocPulse")
		args.PulseDuration = args.PulseDuration or 2
		args.Captured = args.Captured or false
		args.FlagMilitiaId = args.FlagMilitiaId or args.Flag:GetObjVar("FlagMilitia")
		args.StandMilitiaId = args.StandMilitiaId or args.Flag:GetObjVar("StandMilitia")
		args.FlagbearerMilitiaId = args.FlagbearerMilitiaId or self.ParentObj:GetObjVar("Militia")
		if ( args.Flag:IsValid() ) then
			local flagTemplate = args.Flag:GetCreationTemplateId()
			if ( flagTemplate and flagTemplate == "ctf_flag_on_ground" ) then
				args.Flag:Destroy()
			end
		end
		self._Args = args

		self.LastValidLoc = self.ParentObj:GetLoc()
		AddBuffIcon(self.ParentObj, "Flagbearer", "Flagbearer", "Quick Step", "You are carrying a Militia flag!", true)
		self.ParentObj:NpcSpeech("[FFFF00]*Flag Taken*[-]", "combat")
		AddUseCase(self.ParentObj, "Drop Flag", false, "IsSelf")

		self.ApplyAnimation(self, root)
	end,

	ApplyAnimation = function(self, root)
		if ( self._Args.FlagMilitiaId == 1 ) then
			self.ParentObj:PlayEffectWithArgs("PyrosFlagEffect",0.0,"Bone=Head")
		elseif ( self._Args.FlagMilitiaId == 2 ) then
			self.ParentObj:PlayEffectWithArgs("HelmFlagEffect",0.0,"Bone=Head")
		elseif ( self._Args.FlagMilitiaId == 3 ) then
			self.ParentObj:PlayEffectWithArgs("EldeirFlagEffect",0.0,"Bone=Head")
		end
	end,

	RemoveAnimation = function(self, root)
		if ( self._Args.FlagMilitiaId == 1 ) then
			self.ParentObj:StopEffect("PyrosFlagEffect")
		elseif ( self._Args.FlagMilitiaId == 2 ) then
			self.ParentObj:StopEffect("HelmFlagEffect")
		elseif ( self._Args.FlagMilitiaId == 3 ) then
			self.ParentObj:StopEffect("EldeirFlagEffect")
		end
	end,

	Logout = function(self, root, arg)
		if ( arg and arg == "Disconnect" ) then
			EndMobileEffect(root)
			return
		end
	end,

	OnExitState = function(self,root)
		self.ParentObj:RemoveTimer("LocPulse")
		UnregisterEventHandler("",EventType.Timer,"LocPulse")
		self.RemoveAnimation(self, root)
		if ( self._Args.Captured == false and HasUseCase(self.ParentObj,"Drop Flag") ) then
			self.DropFlag(self, root)
		end
		RemoveBuffIcon(self.ParentObj, "Flagbearer")
		RemoveUseCase(self.ParentObj, "Drop Flag")
	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(self._Args.PulseDuration)
	end,

	LocPulse = function(self,root)
		local loc = self.ParentObj:GetLoc()
		if ( not loc ) then return end
		if ( loc and not Plot.GetAtLoc(loc) ) then
			self.LastValidLoc = loc
		else
			self.ParentObj:SystemMessage("[ff0000]Cannot enter private property while carrying a flag![-]", "info")
			if ( loc:Distance(self.LastValidLoc) < 10 and CanPathTo(loc, self.LastValidLoc) and not Plot.GetAtLoc(self.LastValidLoc) ) then
				SetMobileModExpire(self.ParentObj, "Busy", "GetOffMyLawn", true, TimeSpan.FromSeconds(2))
				SetMobileModExpire(self.ParentObj, "Freeze", "GetOffMyLawn", true, TimeSpan.FromSeconds(2))
				self.ParentObj:PlayEffectWithArgs("StunnedEffectObject", 2.0)
				self.ParentObj:SetWorldPosition(self.LastValidLoc)
				if ( not Plot.GetAtLoc(loc) ) then
					self.LastValidLoc = loc
				end
			else
				EndMobileEffect(root)
            	return
			end
		end

		self.LocUpdate = self.LocUpdate - 1
		if ( self.LocUpdate <= 0 ) then
			self.LocUpdate = self.LocUpdateFrequency
			Militia.SetVar("MissingFlag"..self._Args.FlagMilitiaId, function(record)
				record[1] = self.LastValidLoc
				record[2] = self.ParentObj
				record[3] = self._Args.FlagbearerMilitiaId
				return true
			end)
		end

		self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"LocPulse")
	end,

	AiPulse = function(self,root)
		if( self.Pulses % self._Args.PulseDuration == 0 ) then

            if( self.Pulses >= self.MaxPulses or not MilitiaEvent.EligibleFlagbearer(self.ParentObj, nil, false) ) then
				EndMobileEffect(root)
                return
			end
			
		end
		
		if ( self.TryReturn(self, root) or self.TryCapture(self, root) ) then
			EndMobileEffect(root)
			return
		end

		self.ApplyAnimation(self, root)

		Militia.FlagForConflict(self.ParentObj, ServerSettings.Militia.VulnerabilityRadius)

		self.Pulses = self.Pulses + 1
	end,

	DropFlag = function(self,root)
		RemoveUseCase(self.ParentObj, "Drop Flag")
		local loc = self.ParentObj:GetLoc()
		if ( not Plot.GetAtLoc(loc) ) then
			self.LastValidLoc = loc
		end
		self.ParentObj:StopEffect("FireFlagEffect")
		self.ParentObj:StopEffect("WaterFlagEffect")
		self.ParentObj:StopEffect("EarthFlagEffect")
		Create.AtLoc("ctf_flag_on_ground", self.LastValidLoc, function(flag)--update
			if ( flag ) then
				flag:SetScale(Loc(0,0,0))
				flag:RemoveDecay()
				flag:SetObjVar("FlagMilitia", self._Args.FlagMilitiaId)
			end
			--if ( cb ) then cb(flag) end
		end)
		self.ParentObj:NpcSpeech("[FFFF00]*Flag Dropped*[-]", "combat")
		Militia.SetVar("MissingFlag"..self._Args.FlagMilitiaId, function(record)
			record[1] = self.LastValidLoc
			record[2] = nil
			record[3] = nil
			return true
		end)
	end,

	TryCapture = function(self,root)
		local baseFlag = FindObject(SearchMulti(
			{
				SearchTemplate("ctf_flag_at_base"),
				SearchRange(self.LastValidLoc, 10),
				SearchObjVar("FlagMilitia", self._Args.FlagMilitiaId),
				SearchObjVar("StandMilitia", self._Args.FlagbearerMilitiaId),
			}))

		if ( baseFlag ) then
			local winner = baseFlag:GetObjVar("Winner")
			--if stand winner is already the same as the flag you are carrying, it's a return
			if ( winner and winner == self._Args.FlagbearerMilitiaId ) then
				return false
			end

			-- check if you have your own flag
			local myFlag = FindObject(SearchMulti(
			{
				SearchTemplate("ctf_flag_at_base"),
				SearchRange(self.LastValidLoc, 10),
				SearchObjVar("FlagMilitia", self._Args.FlagbearerMilitiaId),				
			}))
			if ( not(myFlag) or myFlag:GetObjVar("Active") == 1 or myFlag:GetObjVar("Winner") ~= self._Args.FlagbearerMilitiaId ) then
				if( not(self.ParentObj:HasTimer("CaptureWarn")) ) then
                    Militia.EventMessageMilitia("Your militia must return your flag before you can capture an enemy flag.",self._Args.FlagbearerMilitiaId)
                    self.ParentObj:ScheduleTimerDelay(TimeSpan.FromMinutes(15), "CaptureWarn")
                end
				return false
			end

			baseFlag:SendMessage("CaptureFlag", self.ParentObj)
			self._Args.Captured = true
			self.ParentObj:NpcSpeech("[FFFF00]*Flag Captured*[-]", "combat")
			Militia.SetVar("MissingFlag"..self._Args.FlagMilitiaId, function(record)
				record[1] = nil
				record[2] = nil
				record[3] = nil
				return true
			end)

			return true
		end
		return false
	end,

	TryReturn = function(self, root)
		local events = GlobalVarRead("Militia.Events")
		if ( events and next(events) ) then
			for controller, data in pairs(events) do
				if ( data[5] and data[5] == 3--event type is ctf
				and data[3] == 1--flag is gone
				and tonumber(data[4]) == tonumber(self._Args.FlagMilitiaId)--flag stand holds this flag
				and tonumber(data[2]) == tonumber(self._Args.FlagbearerMilitiaId) ) then--flag stand controlled
					local scaleToDistance = 600
					local distanceFromStand = scaleToDistance
					if ( controller:IsValid() ) then
						controller:SendMessage("ReturnFlag", self.ParentObj)
						distanceFromStand = controller:GetLoc():Distance(self.LastValidLoc)
					else
						controller:SendMessageGlobal("ReturnFlag", self.ParentObj)
					end
					self._Args.Captured = true
					self.ParentObj:NpcSpeech("[FFFF00]*Flag Returned*[-]", "combat")
					Militia.SetVar("MissingFlag"..tostring(self._Args.FlagMilitiaId), function(record)
						record[1] = nil
						record[2] = nil
						record[3] = nil
						return true
					end)
					
					return true
				end
			end
		end
		return false
	end,

	Pulses = 0,
	MaxPulses = 1800,
	LocUpdate = 1,
	LocUpdateFrequency = 6,
	_Args = {},
}