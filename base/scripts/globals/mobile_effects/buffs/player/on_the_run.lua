MobileEffectLibrary.OnTheRun = 
{
    PersistDeath = true,

    OnEnterState = function(self,root,target,args)
        self.UpdateIcon(self,root,target,args)
        self.CheckOnPlot(self, root)
    end,

    UpdateIcon = function(self,root,target,args)
        AddBuffIcon(self.ParentObj, "OnTheRunDebuff", "On the Run", "Arcane Shackles", "Unable to fast travel due to recently committing a criminal action.", true, args.Duration or self.Duration)
    end,

    OnStack = function(self,root,target,args)
        self.UpdateIcon(self,root,target,args)
    end,

    GetPulseFrequency = function(self,root)
        return self.Duration
    end,

    AiPulse = function(self,root)
        EndMobileEffect(root)
    end,
    
    OnEndEffect = function(self,root)
        EndMobileEffect(root)
    end,

    OnExitState = function(self,root)
        self.BannedPlots = nil
        RemoveBuffIcon(self.ParentObj, "OnTheRunDebuff")
    end,
    
    CheckOnPlot = function( self, root )
        local loc = self.ParentObj:GetLoc()
        local plot = Plot.GetAtLoc(loc)
        
        if( plot ~= nil ) then
            if( not HasMobileEffect(self.ParentObj, "OnTheRunPlotCooldown")) then
                if( self.BannedPlots[plot] ) then
                    --DebugMessage("Auto Kick Player")
                    Plot.KickMobile(plot, self.ParentObj)
                else
                    self.BannedPlots[plot] = true
                    self.ParentObj:SystemMessage("Criminal cannot enter plots.", "info")
                    self.ParentObj:SendMessage("StartMobileEffect", "OnTheRunPlotCooldown", plot)
                end
            end
        end

        if( self.BannedPlots ~= nil ) then
            CallFunctionDelayed(TimeSpan.FromSeconds(1), function()
                self.CheckOnPlot( self, root )
            end)
        end
        

    end,

    Duration = ServerSettings.Criminal.TemporaryDuration,
    BannedPlots = {},
}



-- local house = Plot.GetHouseAt(controller, loc, false, true) -- checking roofbounds
-- Plot.GetAtLoc(loc)