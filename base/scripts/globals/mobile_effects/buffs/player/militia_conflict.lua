MobileEffectLibrary.MilitiaConflict = 
{
	PersistDeath = true,
	PersistSession = true,

	OnEnterState = function(self,root,target,args)
		local militiaId = self.ParentObj:GetObjVar("Militia")
		if ( not militiaId ) then
			EndMobileEffect(root)
			return false
		end
		self.HasKeepHUD = false
		self.StartTime = DateTime.UtcNow
		self.Duration = ServerSettings.Militia.VulnerabilityDuration
		self.ParentObj:SetSharedObjectProperty("Faction", ServerSettings.Militia.Militias[militiaId].Town)
		-- Add the faction flag from each pet
		ForeachActivePet(self.ParentObj, function(pet) pet:SetSharedObjectProperty("Faction", ServerSettings.Militia.Militias[militiaId].Town) end)
		AddBuffIcon(self.ParentObj, "MilitiaConflictBuff", "Militia Conflict", "riposte", "You and enemy Militia members with this buff may engage without penalty.", false, self.Duration)
		if ( not self._Stacked or self._Stacked ~= true ) then
			self.ParentObj:SystemMessage("You are now flagged for Militia conflict!", "info")
			self.ParentObj:NpcSpeech("+ Militia conflict","combat")
		end
		
		self.AiPulse(self, root)
	end,

	OnStack = function(self,root,target,args)
		self._Stacked = true
		self.OnEnterState(self,root,target,args)
	end,

  	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(31)
	end,

	AiPulse = function(self,root)
		if ( DateTime.Compare(DateTime.UtcNow, self.StartTime:Add(self.Duration)) > 0 ) then
			EndMobileEffect(root)
		end
		local militiaId = self.ParentObj:GetObjVar("Militia")
		if ( not militiaId ) then
			EndMobileEffect(root)
		end

		self.ParentObj:SendMessage( "StartMobileEffect", "KeepSiege", nil )
	end,

	OnExitState = function(self,root,target,args)
		self.ParentObj:SystemMessage("You are no longer flagged for Militia conflict.", "info")
		self.ParentObj:NpcSpeech("- Militia conflict","combat")

		-- Remove the faction flag from each pet
		ForeachActivePet(self.ParentObj, function(pet) pet:SetSharedObjectProperty("Faction", "") end)

		self.ParentObj:SetSharedObjectProperty("Faction", "")
		RemoveBuffIcon(self.ParentObj, "MilitiaConflictBuff")
    end,
}