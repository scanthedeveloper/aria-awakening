--[[
    Used as an effect to regenerate a player's vitality
]]

MobileEffectLibrary.VitalityHearth = 
{

	OnEnterState = function(self,root,target,args)
		if ( target == nil or not self.ParentObj:IsPlayer() ) then
			EndMobileEffect(root)
			return false
		end

		-- target refers to the hearth obj that is causing the effect
		self.Target = target

		if( self.Target:HasModule("hearth") ) then
			self._MaxRange = ServerSettings.Instrument.MaximumListenRange
		end

		-- are we listening to a band?
		if( self.ParentObj:HasObjVar("ListeningToBandID")  ) then
			self._RegenRate = ServerSettings.Vitality.AdjustOnBardListeningPulse
		end

		AddBuffIcon(self.ParentObj, "VitalityRegen", "Vitality", "Fire Spark", "Your vitality is regenerating.", false)
		self.ParentObj:PlayEffect("ManaInfuseReceive")
	end,

	OnExitState = function(self,root)
		RemoveBuffIcon(self.ParentObj, "VitalityRegen")
		self.ParentObj:StopEffect("ManaInfuseReceive")
	end,

	GetPulseFrequency = function(self,root)
		return self._PulseFrequency
	end,

	AiPulse = function(self,root)
		if not( self.ValidateEffect(self) ) then
			--self.ParentObj:SystemMessage("Your rest is interrupted.","info")
			EndMobileEffect(root)
		elseif self.VitalityMax == false and (GetCurVitality(self.ParentObj) >= GetMaxVitality(self.ParentObj)) then
			self.VitalityMax = true
			self.ParentObj:SystemMessage("You are well rested.","info")
		elseif (GetCurVitality(self.ParentObj) < GetMaxVitality(self.ParentObj)) then
			SafeAdjustCurVitality(self._RegenRate, self.ParentObj)
		end
		
	end,

	ValidateEffect = function(self)
		return 
		( 
			(
				ServerSettings.Vitality.Hearth.EnableHearthVitalityRegen 
				or (
					self.ParentObj:HasObjVar("ListeningToBandID") 
					or self.Target:GetObjVar("BardPlaying") == false)
				) 
			and 
			self.VitalityMax == false 
			and self.ParentObj:DistanceFrom(self.Target) <= self._MaxRange
		)
	end,

	VitalityMax = false,
	Target = nil,
	_MaxRange = ServerSettings.Campfire.MaxRange,
	_PulseFrequency = ServerSettings.Vitality.Hearth.PulseFrequency, --used as a way to verify the effect is still valid
	_RegenRate = ServerSettings.Vitality.AdjustOnHearthNearPulse
}