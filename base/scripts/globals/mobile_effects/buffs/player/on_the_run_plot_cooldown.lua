MobileEffectLibrary.OnTheRunPlotCooldown = 
{

	OnEnterState = function(self,root,target,args)
        self.Plot = target
    end,
    
    OnExitState = function(self,root)
        if( self.CurrentPulse == 5 ) then
            Plot.KickMobile(self.Plot, self.ParentObj)
            self.ParentObj:SystemMessage("You have been kicked from the plot.", "info")
        end
    end,

    AiPulse = function(self,root)
        local loc = self.ParentObj:GetLoc()
        local plot = Plot.GetAtLoc(loc)
		self.CurrentPulse = self.CurrentPulse + 1
		if ( self.CurrentPulse > self.PulseMax or plot ~= self.Plot ) then
			EndMobileEffect(root)
		else
            self.ParentObj:SystemMessage("Kicking in " .. tostring( 5 - self.CurrentPulse ) .. " seconds.")
		end
	end,

    GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

    Plot = nil,
    PulseFrequency = TimeSpan.FromSeconds(1),
    PulseMax = 5,
	CurrentPulse = 0,
}