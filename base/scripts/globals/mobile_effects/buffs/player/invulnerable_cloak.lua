--[[
    Used to make the player cloaked and invulnerable for a short time
    or until they move or perform an action.
]]

MobileEffectLibrary.InvulnerableCloak = 
{
    EndOnAction = true,
    EndOnMovement = true,

	OnEnterState = function(self,root,target,args)
        -- If this is not a player or a pet we don't need to apply this
        if ( not IsPlayerCharacter(self.ParentObj) and not IsPet(self.ParentObj)) then
            EndMobileEffect(root)
            --DebugMessage("Ended here")
			return false
        end

        -- flagbearers are not affected
        if ( HasMobileEffect(self.ParentObj, "Flagbearer") ) then
            return false
        end

        -- We don't want gods affected by this
        if( IsImmortal(self.ParentObj) ) then
            EndMobileEffect(root)
            return false
        end
        
        -- Duration is 30 seconds unless otherwise passed
        self.Duration = args.Duration or 30

        -- If it's a player we need to show the buff icon
        if( IsPlayerCharacter(self.ParentObj) ) then
            AddBuffIcon(self.ParentObj, "InvulnerableCloak", "Cloaked", "cloak", "Invisible to all!", true)
        end

        -- Make the mobile invul
        self.ParentObj:SetObjVar("Invulnerable", true)

        -- Make the mobile invis
        self.ParentObj:SendMessage("AddInvisEffect", "InvulnerableCloak")
	end,

    OnExitState = function(self,root)
        --DebugMessage("Started Moving")

        RemoveBuffIcon(self.ParentObj,"InvulnerableCloak")
        if not( self.ParentObj:HasAccessLevel(AccessLevel.Immortal) ) then 
            self.ParentObj:DelObjVar("Invulnerable")
        end
        self.ParentObj:SendMessage("RemoveInvisEffect", "InvulnerableCloak")
        self.ParentObj:SystemMessage("The cloak effect has ended.", "info")
	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(self.Duration)
	end,

    AiPulse = function(self,root)
        --DebugMessage("Ended there")
        EndMobileEffect(root)
    end,
    
    Duration = nil,
}