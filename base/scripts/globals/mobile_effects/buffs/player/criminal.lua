MobileEffectLibrary.Criminal = 
{
	PersistDeath = true,

    OnEnterState = function(self,root,target,args)
        self.UpdateIcon(self,root,target,args)
    end,

    UpdateIcon = function(self,root,target,args)
        AddBuffIcon(self.ParentObj, "CriminalDebuff", "[A7A7A7]Criminal[-]", "Fire Shackles", "May experience some skill loss on death. You may no longer enter town, cannot fast travel, and are freely attackable by all players.", true, args.Duration or self.Duration)
    end,

    OnStack = function(self,root,target,args)
        self.UpdateIcon(self,root,target,args)
    end,
    
    OnEndEffect = function(self,root)
        RemoveBuffIcon(self.ParentObj, "CriminalDebuff")
        EndMobileEffect(root)
    end,

	Duration = ServerSettings.Criminal.TemporaryDuration,
}