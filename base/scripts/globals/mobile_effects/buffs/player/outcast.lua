MobileEffectLibrary.Outcast = 
{
	PersistDeath = true,

	OnEnterState = function(self,root,target,args)
        AddBuffIcon(self.ParentObj, "OutcastDebuff", "[FF0000]Outcast[-]", "Fire Shackles2", "May experience severe skill loss on death. You may no longer enter town and are freely attackable by all players.", true)
    end,
    
    OnEndEffect = function(self,root)
        RemoveBuffIcon(self.ParentObj, "OutcastDebuff")
        EndMobileEffect(root)
    end,
}