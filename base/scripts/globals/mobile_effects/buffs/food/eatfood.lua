MobileEffectLibrary.EatFood = 
{
    EndOnMovement = true,
    EndOnAction = true,
    ForceDismount = true,
	OnEnterState = function(self,root,target,args)
        --DebugMessage("Start Food Buff")
        -- Clear out any food buffs

        self._Args = args

        self.isPlayer = IsPlayerCharacter(self.ParentObj)
        SetMobileMod(self.ParentObj, "Busy", "Eating", true)

        -- Stop moving
		self.ParentObj:StopMoving()

		-- Stop combat
		if ( IsInCombat(self.ParentObj) ) then
			StopCombat(self.ParentObj)
        end
        
        -- Set regen rates
		self._stamRegen = 6 * 0.5
		self._healthRegen = 6 * 0.5

        -- Start regening
		SetMobileMod(self.ParentObj, "StaminaRegenPlus", "FoodStaRegen", self._stamRegen)
        SetMobileMod(self.ParentObj, "HealthRegenPlus", "FoodHPRegen", self._healthRegen)
        
        -- Animation // Visual Effects
		self.ParentObj:PlayAnimation("eat")
		self.PlayEffect(self,root)

		-- If this is a player then lets show their buff and progress bar
		if ( self.isPlayer ) then
			AddBuffIcon(self.ParentObj, "EatFood", "Eating", "food", self._stamRegen>0 and "Stamina and Health regeneration increased." or "Consuming food.", false)

            ProgressBar.Show{
				Label="Eating",
				Duration=self.PulseFrequency.TotalSeconds,
                TargetUser=self.ParentObj
			}
        end
        
    end,

    -- If we have health or stamina regen lets show the player they are regening
	PlayEffect = function(self,root)
		if ( self._stamRegen > 0 or self._healthRegen > 0 ) then
			self.ParentObj:PlayEffectWithArgs("PotionHealEffect",0.0,"Color=00FF00")
		end
	end,

    OnExitState = function(self,root)
        self._CleanUp(self, root)
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

    AiPulse = function(self,root)
        self.ApplyFoodBuffs(self,root)
        EndMobileEffect(root)
    end,

    _CleanUp = function(self, root)
        self._DoneEating = false
        ProgressBar.Cancel("Eating", self.ParentObj)
        self.ParentObj:PlayAnimation("idle")
        RemoveBuffIcon(self.ParentObj, "EatFood")
        SetMobileMod(self.ParentObj, "Busy", "Eating", nil)
        SetMobileMod(self.ParentObj, "StaminaRegenPlus", "FoodStaRegen", nil)
        SetMobileMod(self.ParentObj, "HealthRegenPlus", "FoodHPRegen", nil)
    end,

    ApplyFoodBuffs = function(self,root)
        StartMobileEffect(self.ParentObj, "FoodBuff", nil, self._Args)
    end,
    
    _Args = {},
    PulseFrequency = TimeSpan.FromSeconds(FoodStats.FoodEatTimeInSecounds),

}
