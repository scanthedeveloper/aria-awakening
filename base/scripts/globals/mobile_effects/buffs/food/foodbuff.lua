MobileEffectLibrary.FoodBuff = 
{
    PersistSession = true,
	OnEnterState = function(self,root,target,args)

        self._Args = args

        self.isPlayer = IsPlayerCharacter(self.ParentObj)

        self.ApplyFoodBuffs(self, root)
        
    end,

    
    OnStack = function(self,root,target,args)
        self.ClearFoodBuffs(self, root)
        self.OnEnterState(self,root,target,args)
    end,


    OnExitState = function(self,root)
        self.ClearFoodBuffs(self, root)
        --SCAN ADDED VISUAL FOR FOOD EXPIRE
        self.ParentObj:PlayEffectWithArgs("EarthFlagEffect")
        self.ParentObj:PlayEffectWithArgs("PotionHealEffect",2,"Color=FF0000")
        self.ParentObj:PlayAnimation("tired")
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
        EndMobileEffect(root)
    end,
    
    ClearFoodBuffs = function( self, root )
        RemoveBuffIcon(self.ParentObj, "FoodBuff")
        -- We need to clear all the food buffs they have if they eat more food!
        for stat,mobilemod in pairs(FoodStats.FoodStatToMobileMod) do
            SetMobileMod(self.ParentObj, mobilemod, stat, nil)
        end
    end,

    ApplyFoodBuffs = function(self,root)
        self.ClearFoodBuffs(self, root)

        RequestMobileMod( self.ParentObj, self.ParentObj, {"MealEffectTimes"},
        function(MobileMod) 
            local effectMod = GetMobileMod(MobileMod.MealEffectTimes, 1)

            -- Start buff icon tooltip text
            local buffIconText = "Gained "
            local args = self._Args
            -- We need to go through our args and apply the food buffs and build the buff icon text.
            for i=1, #args do
                buffAmount = math.floor(args[i].buffAmount * effectMod)
                --DebugTable(args[i])
                if( i > 1) then buffIconText = buffIconText .. " and " end
                buffIconText = buffIconText .. "[ffb523]+" .. tostring(buffAmount) .. " " .. args[i].buffStat .. "[-]"
                SetMobileMod(self.ParentObj, FoodStats.FoodStatToMobileMod[args[i].buffStat], args[i].buffStat, buffAmount)    
            end

            buffIconText = buffIconText .. " from eating."

            -- Give the player a buff icon
            if ( self.ParentObj:IsPlayer() ) then
                --DebugMessage("Add Buff Icon")
                AddBuffIcon(
                    self.ParentObj, 
                    "FoodBuff", 
                    "Food Buff", 
                    "Night", 
                    buffIconText,
                    false
                )
            end

        end)
    end,
    
    _Args = {},
    Duration = TimeSpan.FromMinutes(FoodStats.BuffDurationInMinutes or 1)
}
