-- general buffs
require 'globals.mobile_effects.buffs.immune'
require 'globals.mobile_effects.buffs.heal_over_time'
require 'globals.mobile_effects.buffs.nomortalstrike'
require 'globals.mobile_effects.buffs.nobleed'
require 'globals.mobile_effects.buffs.nostun'
require 'globals.mobile_effects.buffs.nosilence'
require 'globals.mobile_effects.buffs.nodaze'
require 'globals.mobile_effects.buffs.nosunder'
require 'globals.mobile_effects.buffs.rage'
require 'globals.mobile_effects.buffs.selfdestruct'

-- player buffs
require 'globals.mobile_effects.buffs.player.vitality_hearth'
require 'globals.mobile_effects.buffs.player.campfire'
require 'globals.mobile_effects.buffs.player.hearth'
require 'globals.mobile_effects.buffs.player.tinderbox'
require 'globals.mobile_effects.buffs.player.militia_top_rank'
require 'globals.mobile_effects.buffs.player.militia_territory_buff'
require 'globals.mobile_effects.buffs.player.militia_conflict'
require 'globals.mobile_effects.buffs.player.invulnerable_cloak'
require 'globals.mobile_effects.buffs.player.flagbearer'
require 'globals.mobile_effects.buffs.player.criminal'
require 'globals.mobile_effects.buffs.player.outcast'
require 'globals.mobile_effects.buffs.player.on_the_run'
require 'globals.mobile_effects.buffs.player.on_the_run_plot_cooldown'
require 'globals.mobile_effects.buffs.player.passive'

-- fighter
require 'globals.mobile_effects.buffs.fighter.shieldbash' -- works
require 'globals.mobile_effects.buffs.fighter.heroism' -- works
require 'globals.mobile_effects.buffs.fighter.righteousweapon' -- works
require 'globals.mobile_effects.buffs.fighter.vanguard' -- works
require 'globals.mobile_effects.buffs.fighter.charge' -- buggy
require 'globals.mobile_effects.buffs.fighter.stunstrike' -- works
require 'globals.mobile_effects.buffs.fighter.adrenaline_rush' -- works
require 'globals.mobile_effects.buffs.fighter.relentlesshate' -- works
--SCAN ADDED
require 'globals.mobile_effects.buffs.fighter.martial_enraged'

-- rogue
require 'globals.mobile_effects.buffs.rogue.dart' -- works
require 'globals.mobile_effects.buffs.rogue.evasion' -- works
require 'globals.mobile_effects.buffs.rogue.backstab'

-- monk
--require 'globals.mobile_effects.buffs.monk.flurryofblows' --works

-- mage
require 'globals.mobile_effects.buffs.mage.spellchamber' --works
require 'globals.mobile_effects.buffs.mage.magearmor'
require 'globals.mobile_effects.buffs.mage.energyvortex' -- works
require 'globals.mobile_effects.buffs.mage.destruction' -- needs testing
require 'globals.mobile_effects.buffs.mage.meditation'
require 'globals.mobile_effects.buffs.mage.stasis'
require 'globals.mobile_effects.buffs.mage.empower'
require 'globals.mobile_effects.buffs.mage.empoweraoe'
--require 'globals.mobile_effects.buffs.mage.charming'
--require 'globals.mobile_effects.buffs.mage.charmed'

-- spells
require 'globals.mobile_effects.buffs.spells.recall'
require 'globals.mobile_effects.buffs.spells.mark'
require 'globals.mobile_effects.buffs.spells.chargerunebook'
require 'globals.mobile_effects.buffs.spells.summonportal'
require 'globals.mobile_effects.buffs.spells.intellect'
require 'globals.mobile_effects.buffs.spells.agility'
require 'globals.mobile_effects.buffs.spells.energywall'
require 'globals.mobile_effects.buffs.spells.massreveal'

-- food
require 'globals.mobile_effects.buffs.food.foodbuff'
require 'globals.mobile_effects.buffs.food.eatfood'