MobileEffectLibrary.Enrage = 
{
    PersistSession = true,
    
    OnEnterState = function(self,root,target,args)
        self._Args = args
        self._MaxPulses = args.MaxPulseCount or 1
        self._Args.BuffDescription = args.BuffDescription or ""
        self._Args.PulseDuration = args.PulseDuration or 1
        self.IsPlayer = IsPlayerCharacter(self.ParentObj)
        
        self.ParentObj:PlayAnimation("roar")

        if ( not MartialSuccessCheck(self.ParentObj, args.SkillRequirementMin, args.SkillRequirementMax, "Martial", self.EffectName) ) then
            EndMobileEffect(root)
            return
        end
        
        self.ParentObj:PlayEffectWithArgs(self._Args.BuffVFX,0.0,"Bone=Head")
        --SetMobileMod(self.ParentObj, "AttackTimes", self.EffectName, 0.50)
        --SCAN ADDED NEW MOBILE EFFECT
        SetMobileMod(self.ParentObj, "HealthRegenPlus", self.EffectName, 10)
        self.ParentObj:NpcSpeech("[00FF00]Regenerating Health[-]","combat")
        self.ParentObj:PlayEffect("FireballEffect",28)
        AddBuffIcon(self.ParentObj, self.EffectName, self._Args.DisplayName, self._Args.BuffIcon, self._Args.BuffDescription, false)
        self.ParentObj:PlayObjectSound(self._Args.ActSFX)
    end,
    
    -- OnStack we want to set the pulse count back to 0, but clean-up
    -- so we don't duplicate the buff icon.
    OnStack = function(self,root,target,args)
        self._CleanUp(self,root)
        self._Pulses = 0
        self._Stacked = true
        self.OnEnterState(self,root,target,args)
    end,

    OnExitState = function(self,root)
        self._CleanUp(self, root)
    end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(self._Args.PulseDuration)
	end,

    AiPulse = function(self,root) 

        if( self._Pulses % self._Args.PulseDuration == 0 ) then

            if( self._CompletedPulses >= self._MaxPulses ) then
                EndMobileEffect(root)
                return
            end

            self._CompletedPulses =  self._CompletedPulses + 1
        end

        self._Pulses = self._Pulses + 1        
    end,

    _CleanUp = function(self, root)
        if( self.EffectName and self.IsPlayer ) then
            self.ParentObj:StopEffect(self._Args.BuffVFX, 3.0)
            SetMobileMod(self.ParentObj, "HealthRegenPlus", self.EffectName, nil)
            self.ParentObj:NpcSpeech("[00FF00]Regeneration Ended[-]","combat")
            RemoveBuffIcon(self.ParentObj, self.EffectName)
        end
    end,

    _Pulses = 0,
    _MaxPulses = 1,
    _CompletedPulses = 0,
    _Stacked = false,
}
