MobileEffectLibrary.Demoralize = 
{
    OnEnterState = function(self,root,target,args)
        self._Args = args

        self.ParentObj:PlayAnimation("roar")

        if ( not MartialSuccessCheck(self.ParentObj, args.SkillRequirementMin, args.SkillRequirementMax, "Martial", self.EffectName) ) then
            EndMobileEffect(root)
            return
        end

        local targets = FindObjects(SearchMulti(
            {
                SearchMobileInRange(args.AreaOfEffect),
            }))
        for i = 1, #targets do
            if ( ValidCombatTarget(self.ParentObj, targets[i]) ) then
                if ( not IsProtected(targets[i], self.ParentObj) ) then
                    targets[i]:SendMessage("StartMobileEffect", "Demoralized", targets[i])
                end
            end
        end

        self.ParentObj:PlayObjectSound(self._Args.ActSFX)
        self.ParentObj:PlayEffectWithArgs(self._Args.ActivationVFX, 0.0, "Bone=Ground")
        EndMobileEffect(root)
    end,

    OnExitState = function(self,root)
    end,
}
