--SCAN ADDED - Improved Taunt
MobileEffectLibrary.Taunt = 
{
    OnEnterState = function(self,root,target,args)
        self._Args = args

        if ( ValidCombatTarget(self.ParentObj, target) ) then
            if( self.ParentObj:DistanceFrom(target) > self._MaxRange ) then
                if( self.ParentObj:IsPlayer() ) then
                    self.ParentObj:SystemMessage("Target out of range.", "info")
                end
                EndMobileEffect(root)
                return
            end
            
            if ( not MartialSuccessCheck(self.ParentObj, args.SkillRequirementMin, args.SkillRequirementMax, "Martial", self.EffectName) ) then
                self._TriggerCooldown = true
                EndMobileEffect(root)
                return
            end
            --SCAN ADDED
            self.ParentObj:PlayAnimation("dance_chickendance")
            target:PlayEffect("ArenaFlagRedEffect", 5)
            target:SendMessage("AddThreat", self.ParentObj, 1000)
            target:SendMessage("AddAggro", self.ParentObj, 1000)
            --target:SendMessage("AddThreat", self.ParentObj, 200)
            --target:SendMessage("AddAggro", self.ParentObj, 200)
            target:SendMessage("StartMobileEffect", "TauntDamage", target)
            --
            local overrideRate = 32
            local projectileDist = target:GetLoc():Distance(self.ParentObj:GetLoc())
            local projectileTimer = (projectileDist - .5) / overrideRate  
            target:PlayProjectileEffectToWithSound(self._Args.ProjVFX, self.ParentObj, self._Args.ProjSFX, overrideRate, math.max(0.8,projectileTimer), "Orient=2Loc")
            self._TriggerCooldown = true
        end

        EndMobileEffect(root)
    end,

    OnExitState = function(self,root)
        if( self._TriggerCooldown ) then
            StartPrestigePositionCooldown(self.ParentObj, "Martial", "Taunt", PrestigeData.Martial.Abilities.Taunt.Cooldown )
        end
    end,

    _MaxRange = 8,
    _TriggerCooldown = false,
}
