MobileEffectLibrary.Bloodlust = 
{
    PersistSession = true,
    
    OnEnterState = function(self,root,target,args)
        self._Args = args
        self._MaxPulses = args.MaxPulseCount or 1
        self._Args.BuffDescription = args.BuffDescription or ""
        self._Args.PulseDuration = args.PulseDuration or 1
        self.IsPlayer = IsPlayerCharacter(self.ParentObj)

        self.ParentObj:PlayAnimation("roar")

        if ( not MartialSuccessCheck(self.ParentObj, args.SkillRequirementMin, args.SkillRequirementMax, "Martial", self.EffectName) ) then
            EndMobileEffect(root)
            return
        end

        UpdateBloodlust(self.ParentObj)
        self.ParentObj:PlayObjectSound(self._Args.ActSFX)
        self.ParentObj:PlayEffectWithArgs(self._Args.ActivationVFX, 0.0, "Bone=Head")
        self.ParentObj:PlayEffectWithArgs(self._Args.BuffVFX, 0.0, "Bone=Spine2")

        AddBuffIcon(self.ParentObj, self.EffectName, self._Args.DisplayName, self._Args.BuffIcon, self._Args.BuffDescription, false)
    end,
    
    -- OnStack we want to set the pulse count back to 0, but clean-up
    -- so we don't duplicate the buff icon.
    OnStack = function(self,root,target,args)
        EndMobileEffect(root)
    end,

    OnExitState = function(self,root)
        self._CleanUp(self, root)
        SetMobileMod(self.ParentObj, "BloodlustRegenPlus","Bloodlust", -0.5)
    end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(self._Args.PulseDuration)
    end,

    AiPulse = function(self,root)
        if ( HasConflictShorterThan(self.ParentObj, 10) ) then
            SetMobileMod(self.ParentObj, "BloodlustRegenPlus","Bloodlust", nil)
        else
            SetMobileMod(self.ParentObj, "BloodlustRegenPlus","Bloodlust", -0.5)
            UpdateBloodlust(self.ParentObj)
        end
    end,

    _CleanUp = function(self, root)
        if( self.EffectName and self.IsPlayer ) then
            SetCurBloodlust(self.ParentObj, 0)
            UpdateBloodlust(self.ParentObj)
            self.ParentObj:StopEffect(self._Args.BuffVFX, 3.0)
            RemoveBuffIcon(self.ParentObj, self.EffectName)
        end
    end,

    _Stacked = false,
}
