MobileEffectLibrary.Nemesis = 
{
    PersistSession = true,
    
    OnEnterState = function(self,root,target,args)
        self._Args = args
        self._MaxPulses = args.MaxPulseCount or 1
        self._Args.BuffDescription = args.BuffDescription or ""
        self._Args.PulseDuration = args.PulseDuration or 1
        self.IsPlayer = IsPlayerCharacter(self.ParentObj)

        local mobileKind = target:GetObjVar("MobileKind")
        if ( not mobileKind ) then
            EndMobileEffect(root)
            self.ParentObj:SystemMessage("That can't be a nemesis.", "info")
            return false
        end

        if ( not MartialSuccessCheck(self.ParentObj, args.SkillRequirementMin, args.SkillRequirementMax, "Martial", self.EffectName) ) then
            EndMobileEffect(root)
            return
        end

        local overrideRate = 16
        local projectileDist = target:GetLoc():Distance(self.ParentObj:GetLoc())
        local projectileTimer = (projectileDist - .5) / overrideRate  
        self.ParentObj:PlayProjectileEffectToWithSound(self._Args.ProjVFX, target, self._Args.ProjSFX, overrideRate, math.max(4,projectileTimer), "Bone=Head,Target=Head")


        self.ParentObj:SetObjVar("Nemesis", mobileKind)

        AddBuffIcon(self.ParentObj, self.EffectName, self._Args.DisplayName, self._Args.BuffIcon, self._Args.BuffDescription, false)
    end,
    
    -- OnStack we want to set the pulse count back to 0, but clean-up
    -- so we don't duplicate the buff icon.
    OnStack = function(self,root,target,args)
        self._CleanUp(self,root)
        self._Pulses = 0
        self._Stacked = true
        self.OnEnterState(self,root,target,args)
    end,

    OnExitState = function(self,root)
        self._CleanUp(self, root)
    end,

	GetPulseFrequency = function(self,root)
        local nemeses = FindObjects(SearchMulti(
            {
                SearchMobileInRange(6),
                SearchObjVar("MobileKind", self.ParentObj:GetObjVar("Nemesis")),
            }))
            for i = 1, #nemeses do
                local overrideRate = 32
                local projectileDist = nemeses[i]:GetLoc():Distance(self.ParentObj:GetLoc())
                local projectileTimer = (projectileDist - .5) / overrideRate  
                nemeses[i]:PlayProjectileEffectToWithSound(self._Args.BeaconVFX, self.ParentObj, self._Args.BeaconSFX, overrideRate, math.max(2,projectileTimer), "Bone=Head,Target=Head")
            end

        return TimeSpan.FromSeconds(self._Args.PulseDuration)
	end,

    AiPulse = function(self,root) 

        if( self._Pulses % self._Args.PulseDuration == 0 ) then

            if( self._CompletedPulses >= self._MaxPulses ) then
                EndMobileEffect(root)
                return
            end

            self._CompletedPulses =  self._CompletedPulses + 1
        end

        self._Pulses = self._Pulses + 1        
    end,

    _CleanUp = function(self, root)
        if( self.EffectName and self.IsPlayer ) then
            self.ParentObj:DelObjVar("Nemesis")
            RemoveBuffIcon(self.ParentObj, self.EffectName)
        end
    end,

    _Pulses = 0,
    _MaxPulses = 1,
    _CompletedPulses = 0,
    _Stacked = false,
}