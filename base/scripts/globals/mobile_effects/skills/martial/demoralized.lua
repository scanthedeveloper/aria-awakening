MobileEffectLibrary.Demoralized = 
{
    Debuff = true,
    Resistable = true,

    OnEnterState = function(self,root,target,args)
        self._Args = args
        self._MaxPulses = args.MaxPulseCount or 30
        self._Args.PulseDuration = args.PulseDuration or 1
        self._Args.BuffIcon = "Demoralizing_Shout"
        self._Args.BuffVFX = "WarriorDemoStatus"
        self._Args.BuffDescription = "Intimidated. Attack speed reduced by 20%."
        self._Args.DisplayName = "Intimidation"

        if not ( self._Stacked ) then
            self.ParentObj:PlayEffectWithArgs(self._Args.BuffVFX, 0.0, "Bone=Ground")
        end
        SetMobileMod(self.ParentObj, "AttackSpeedTimes", self.EffectName, -0.2)
        AddBuffIcon(self.ParentObj, self.EffectName, self._Args.DisplayName, self._Args.BuffIcon, self._Args.BuffDescription, false)
    end,
    
    -- OnStack we want to set the pulse count back to 0, but clean-up
    -- so we don't duplicate the buff icon.
    OnStack = function(self,root,target,args)
        self._CleanUp(self,root)
        self._Pulses = 0
        self._Stacked = true
        self.OnEnterState(self,root,target,args)
    end,

    OnExitState = function(self,root)
        self._CleanUp(self, root)
    end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(self._Args.PulseDuration)
	end,

    AiPulse = function(self,root) 
        if( self._Pulses % self._Args.PulseDuration == 0 ) then

            if( self._CompletedPulses >= self._MaxPulses ) then
                EndMobileEffect(root)
                return
            end

            self._CompletedPulses =  self._CompletedPulses + 1
        end

        self._Pulses = self._Pulses + 1        
    end,

    _CleanUp = function(self, root)
        if( self.EffectName ) then
            SetMobileMod(self.ParentObj, "AttackSpeedTimes", self.EffectName)
            self.ParentObj:StopEffect(self._Args.BuffVFX, 3.0)
            RemoveBuffIcon(self.ParentObj, self.EffectName)
        end
    end,

    _Pulses = 0,
    _MaxPulses = 1,
    _CompletedPulses = 0,
    _Stacked = false,
}
