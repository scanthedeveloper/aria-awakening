MobileEffectLibrary.ChallengingShout = 
{
    OnEnterState = function(self,root,target,args)
        self._Args = args

        self.ParentObj:PlayAnimation("roar")


        if ( not MartialSuccessCheck(self.ParentObj, args.SkillRequirementMin, args.SkillRequirementMax, "Martial", self.EffectName) ) then
            EndMobileEffect(root)
            return
        end

        local targets = FindObjects(SearchMulti(
            {
                SearchMobileInRange(args.AreaOfEffect),
            }))
        --SCAN ADDED (deal 10% of weapon damage as AOE taunt)
        

        for i = 1, #targets do
            if ( ValidCombatTarget(self.ParentObj, targets[i]) ) then
                --SCAN Increased from 200 to 1000
                targets[i]:SendMessage("AddThreat", self.ParentObj, 1000)
                targets[i]:SendMessage("AddAggro", self.ParentObj, 1000)
                --SCAN ADDED
                local tauntDamage = math.random(10,35)
                targets[i]:SendMessage("ProcessTrueDamage", self.ParentObj, tauntDamage)
                targets[i]:PlayEffect("ArenaFlagRedEffect", 5)
                targets[i]:PlayEffectWithArgs(self._Args.TargetVFX, 0.0, "Bone=Head")
            end
        end

        self.ParentObj:PlayEffectWithArgs(self._Args.ActivationVFX, 0.0, "Bone=Ground")
        self.ParentObj:PlayObjectSound(self._Args.ActSFX)
        EndMobileEffect(root)
    end,

    OnExitState = function(self,root)
    end,
}
