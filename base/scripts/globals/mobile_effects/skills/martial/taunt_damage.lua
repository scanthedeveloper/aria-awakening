-- SCAN ADDED 
-- Taunt Damage
-- Causes direct damage (DD) or damage over time (DOT) to an enemy target.
-- Uses a modified version of the Poison spell.
MobileEffectLibrary.TauntDamage = 
{
	Debuff = true,
	Resistable = true,

	OnEnterState = function(self,root,target,args)
		if(target) then SpellCaster = target end
		self.Target = target
		self.PulseFrequency = args.PulseFrequency or self.PulseFrequency
		self.PulseMax = args.PulseMax or self.PulseMax
		self.MinDamage = args.MinDamage or self.MinDamage
		self.MaxDamage = args.MaxDamage or self.MaxDamage
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "PoisonDebuff", "Taunted", "Poison", self.MinDamage.."-"..self.MaxDamage.." damage every "..self.PulseFrequency.Seconds.." seconds." .. "\nYour feelings have been hurt.", true)
		end

		self.ParentObj:PlayEffect("FireFliesEffect")
		self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_cast_fire",false)
		AdvanceConflictRelation(target, self.ParentObj)
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "PoisonDebuff")
			ShowStatusElement(self.ParentObj,{IsSelf=true, HealthHue="FF0000"})
		end
        self.ParentObj:StopEffect("FireFliesEffect")
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

	AiPulse = function(self,root)
		self.CurrentPulse = self.CurrentPulse + 1
		if ( IsDead(self.ParentObj) or self.CurrentPulse > self.PulseMax ) then
			EndMobileEffect(root)
		else
			self.ParentObj:PlayEffect("ArenaFlagRedEffect", 3)
			self.ParentObj:PlayEffect("WarriorDemoStatus",2.5)
			self.ParentObj:PlayEffect("WarriorEnrage",2.5)
			self.ParentObj:SendMessage("ProcessTypeDamage", self.Target, math.random(self.MinDamage, self.MaxDamage), "Poison")
		end
	end,

	PulseFrequency = TimeSpan.FromSeconds(1),
	PulseMax = 3,
	CurrentPulse = 0,
	MinDamage = 10,
	MaxDamage = 15,
	SpellCaster = nil,
}