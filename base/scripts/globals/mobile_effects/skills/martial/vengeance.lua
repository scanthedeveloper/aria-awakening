MobileEffectLibrary.Vengeance = 
{
	OnEnterState = function(self,root,target,args)
		self._Args = args
		self._Args._BloodlustReward = args.BloodlustReward or 1

		local curHealth = GetCurHealth(self.ParentObj)
		if(curHealth < 25) then
			self.ParentObj:SystemMessage("Not enough health", "info")
			CallFunctionDelayed(TimeSpan.FromMilliseconds(1),function()self.ResetCooldown(self,root)end)
			EndMobileEffect(root)
			return
		end

		if ( not HasMobileEffect(self.ParentObj, "Bloodlust") ) then
			self.ParentObj:SystemMessage("You must be under the effect of Bloodlust", "info")
			CallFunctionDelayed(TimeSpan.FromMilliseconds(1),function()self.ResetCooldown(self,root)end)
			EndMobileEffect(root)
			return
		end

		if ( not MartialSuccessCheck(self.ParentObj, args.SkillRequirementMin, args.SkillRequirementMax, "Martial", self.EffectName) ) then
            EndMobileEffect(root)
            return
		end
		
		bloodlustGain = self._Args._BloodlustReward
		AdjustCurBloodlust(self.ParentObj, bloodlustGain)
		UpdateBloodlust(self.ParentObj)

		self.ParentObj:NpcSpeech("+ "..bloodlustGain.." Bloodlust", "combat")
		self.ParentObj:PlayAnimation("roar")
        self.ParentObj:PlayObjectSound(self._Args.ActSFX)
		self.ParentObj:PlayEffect(self._Args.ActivationVFX)
		self.ParentObj:PlayObjectSound("Pain", true)
		EndMobileEffect(root)
	end,

	ResetCooldown = function(self,root)
		ResetPrestigeCooldown(self.ParentObj, "Martial", "Vengeance")
	end,

	OnExitState = function(self,root)
	end,
}