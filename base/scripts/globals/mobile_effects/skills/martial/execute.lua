MobileEffectLibrary.Execute = 
{
    OnEnterState = function(self,root,target,args)
        self._Args = args

        if not ( self.ParentObj:HasLineOfSightToObj(target,ServerSettings.Combat.LOSEyeLevel) ) then
            self.ParentObj:SystemMessage("Cannot see target.", "info")
            EndMobileEffect(root)
            return false
        end

        -- Do we have a weapon?
        local weaponObj = Weapon.GetPrimary(self.ParentObj)
        if( weaponObj ) then
            self._Args.Range = GetCombatRange(self.ParentObj, target, Weapon.GetRange( Weapon.GetType( weaponObj ) ) )
        end

        if( tonumber( self.ParentObj:DistanceFrom( target ) ) > (self._Args.Range or 5) ) then
            self.ParentObj:SystemMessage("Enemy not in range.", "info")
            EndMobileEffect(root)
            return false
        end

        local bloodlust = GetCurBloodlust(self.ParentObj)
        if ( not bloodlust or bloodlust == 0 ) then
            EndMobileEffect(root)
            self.ParentObj:SystemMessage("No Bloodlust resource available.", "info")
            return false
        end

        local enemyHP = GetCurHealth(target)
        local enemyMaxHP = GetMaxHealth(target)
        local enemyPercentageHP = enemyHP / enemyMaxHP
        if ( enemyPercentageHP > 0.3 ) then
            EndMobileEffect(root)
            self.ParentObj:SystemMessage("Target is not low enough health.", "info")
            return false
        end

        self.ParentObj:PlayAnimation("attack")
        SetCurBloodlust(self.ParentObj, 0)

        local weaponSkill = Weapon.GetSkill(Weapon.GetType(Weapon.GetPrimary(self.ParentObj)))
        local hitChance = (math.max( 25, GetSkillLevel(self.ParentObj, weaponSkill )  ) / 100)

        

        if ( not MartialSuccessCheck(self.ParentObj, args.SkillRequirementMin, args.SkillRequirementMax, "Martial", self.EffectName) or  Success(hitChance) == false ) then
            target:NpcSpeech("[FF8C00]*execute miss*[-]","combat")
            EndMobileEffect(root)
            return
        end

        self.ParentObj:PlayEffectWithArgs(self._Args.ActivationVFX, 3.0, "Bone=Ground,Lifetime="..tostring(0.8+bloodlust*0.008))
        self.ParentObj:SendMessage("ClearQueuedWeaponAbility")
        self.ParentObj:PlayObjectSound(self._Args.ActSFX)
        local vigorBonus = (GetSkillLevel(self.ParentObj, "MeleeSkill") or 0) / 100
        
        -- If we are fighting a player we deal one damage per bloodlust, if PvE we deal 2 damage per bloodlust
        if( IsPlayerCharacter( target ) ) then
            target:SendMessage("ProcessTypeDamage", self.ParentObj, bloodlust * vigorBonus, "TrueDamage", false, false)
        else
            target:SendMessage("ProcessTypeDamage", self.ParentObj, bloodlust * 2 * vigorBonus, "TrueDamage", false, false)
        end

        if target~=nil then
            target:PlayEffectWithArgs(self._Args.ImpactVFX, 3.0, "Lifetime="..tostring(0.8+bloodlust*0.008))
        end

        EndMobileEffect(root)
    end,

    OnExitState = function(self,root)
    end,
}
