MobileEffectLibrary.Bandage = 
{

	OnEnterState = function(self,root,target,args)
		self.Target = target or self.ParentObj

		if ( HasMobileEffect(self.ParentObj, "NoBandage") ) then
			if ( self.ParentObj:IsPlayer() ) then
				self.ParentObj:NpcSpeechToUser("You cannot bandage yet.",self.ParentObj)
			end
			EndMobileEffect(root)
			return false
		end

		if not( ValidateRangeWithError(SkillData.AllSkills.HealingSkill.Options.BandageRange, self.ParentObj, self.Target, "Too far away.") ) then
			EndMobileEffect(root)
			return false
		end

		-- cache if target is player
		self.IsPlayer = IsPlayerCharacter(target)
		self.IsPet = false
		if not ( self.IsPlayer ) then
			self.IsPet = IsPet(target)
		end

		if ( not self.IsPlayer and not self.IsPet and not target:HasObjVar("CanBandage") ) then
			self.ParentObj:NpcSpeechToUser("Cannot bandage that.", self.ParentObj)
			EndMobileEffect(root)
			return false
		end

		if( not self.ParentObj:HasLineOfSightToObj(self.Target) ) then
			if( self.ParentObj:IsPlayer() ) then
				self.ParentObj:SystemMessage("You cannot see that.", "info")
			end
			EndMobileEffect(root)
			return false
		end

		if ( HasMobileEffect(target, "MortalStruck") ) then
			if ( self.IsPlayer ) then
				if ( self.ParentObj == target ) then
					self.ParentObj:NpcSpeechToUser("Cannot bandage right now.", self.ParentObj)
				else
					self.ParentObj:NpcSpeechToUser("They cannot be bandaged right now.", self.ParentObj)
				end
			end
			EndMobileEffect(root)
			return false
		end

		--SCAN DISABLED
		--[[
		if ( HasMobileEffect(target, "Enrage") ) then
			if ( self.IsPlayer ) then
				if ( self.ParentObj == target ) then
					self.ParentObj:NpcSpeechToUser("You cannot bandage whilst enraged.", self.ParentObj)
				else
					self.ParentObj:NpcSpeechToUser("They are enraged.", self.ParentObj)
				end
			end
			EndMobileEffect(root)
			return false
		end
		]]

		self.SupplimentalSkill = "MeleeSkill" -- default to vigor

		if ( self.IsPet ) then
			self.SupplimentalSkill = "AnimalLoreSkill"
		end

		local skillDictionary = GetSkillDictionary(self.ParentObj)
		self.Healing = GetSkillLevel(self.ParentObj, "HealingSkill", skillDictionary)
		self.Supplimental = GetSkillLevel(self.ParentObj, self.SupplimentalSkill, skillDictionary)
		self._BaseHeal = 50

		if ( IsDead(self.Target) ) then
			-- attempt to resurrect.
			if ( self.Target:HasObjVar("IsGhost") ) then
				-- you can't bandage a ghost back to life, only bodies.
				self.ParentObj:NpcSpeechToUser("Cannot bandage a ghost.", self.ParentObj)
				EndMobileEffect(root)
				return false
			end
			if (
				self.Healing >= SkillData.AllSkills.HealingSkill.Options.SkillRequiredToResurrect
				and
				self.Supplimental >= SkillData.AllSkills.HealingSkill.Options.SkillRequiredToResurrect
			) then
				LookAt(self.ParentObj, self.Target)
				if ( IsPlayerCharacter(self.Target) ) then
					self.Target:SystemMessage("" .. self.ParentObj:GetName() .. " is attempting to revive you.", "info")
				end
			else
				self.ParentObj:NpcSpeechToUser("Not skilled enough to revive this corpse.", self.ParentObj)
				EndMobileEffect(root)
				return false
			end

			LookAt(self.ParentObj, self.Target)

			-- handle breaking effect from actions/moving
			self._WasDead = true
			SetMobileMod(self.ParentObj, "Busy", "BandagingCorpse", true)
			RegisterEventHandler(EventType.Message, "BreakInvisEffect", function(what)
				if ( what ~= "Pickup" ) then
					self.Interrupted(self, root)
				end
			end)
			RegisterEventHandler(EventType.StartMoving, "", function()
				self.Interrupted(self, root)
			end)
			if ( IsInCombat(self.ParentObj) ) then
				self.ParentObj:SendMessage("EndCombatMessage")
			end
			self.ParentObj:StopMoving()
			self.ParentObj:PlayAnimation("forage")

			return -- stop the rest of OnEnterState execution, but wait for the pulse still.
		end

		if (not self.ParentObj:IsMoving()) then
			self.ParentObj:PlayAnimation("bandage")
			self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(2),"ReturnToIdle", true, self.ParentObj:GetLoc())
		end

		-- clear theses since we aren't resurrecting.
		self.GetPulseFrequency = nil
		self.AiPulse = nil

		self._MaxHealth = GetMaxHealth(self.Target)

		if ( GetCurHealth(self.Target) >= self._MaxHealth ) then
			if ( self.ParentObj:IsPlayer() ) then
				self.ParentObj:NpcSpeechToUser("That patient seems fine.",self.ParentObj)
			end
			EndMobileEffect(root)
			return false
		end

		self._HealMultiplier = 0.1 + ( (self.Healing) / 150 )
		self._HealAmount = 10 + self._BaseHeal + ((self.Supplimental) * 2.75)
		--DebugMessage("self._HealMultiplier" .. self._HealMultiplier)
		--DebugMessage("self._HealAmount" .. self._HealAmount)

		if ( self.ParentObj ~= self.Target ) then
			LookAt(self.ParentObj, self.Target)
			-- cut the heal in half for healing other players
			if not( self.IsPet ) then
				self._HealAmount = self._HealAmount * 0.5
			end
		end

		self.Target:SendMessage("HealRequest", self._HealAmount * self._HealMultiplier , self.ParentObj)

		--SCAN LOWERED REQUIREMENT TO CURE POISON FROM 60 to 50.
		if ( IsPoisoned(self.Target) ) then
			if ( self.Healing >= 50 ) then
				self.Target:SendMessage("EndPoisonEffect")
			else
				self.ParentObj:NpcSpeechToUser("Not skilled enough to cure poison.", self.ParentObj)
			end
		end

		--SCAN ADDED
		if ( IsBleeding(self.Target) ) then
			if ( self.Healing >= 70 ) then
				self.Target:SendMessage("EndBleedEffect")
			else
				self.ParentObj:NpcSpeechToUser("Not skilled enough to resolve bleeding.", self.ParentObj)
			end
		end

		-- apply no bandage to person that did the bandaging
		StartMobileEffect(self.ParentObj, "NoBandage")

		self.Target:PlayEffect("PotionHealEffect")
		
		-- gain check
		CheckSkillChance(self.ParentObj, "HealingSkill", self.Healing)
		CheckSkillChance(self.ParentObj, self.SupplimentalSkill, self.Supplimental)

		EndMobileEffect(root)
	end,

	-- for resurrecting.
	GetPulseFrequency = function(self,root) 
		return TimeSpan.FromSeconds(4)
	end,

	Interrupted = function(self,root)
		self.ParentObj:SystemMessage("Bandaging interrupted.", "info")
		self._Interrupted = true
		EndMobileEffect(root)
	end,

	OnExitState = function(self,root)
		if ( self._WasDead ) then
			SetMobileMod(self.ParentObj, "Busy", "BandagingCorpse", nil)
			UnregisterEventHandler("", EventType.Message, "BreakInvisEffect")
			UnregisterEventHandler("", EventType.StartMoving, "")
			self.ParentObj:PlayAnimation("idle")
		end
	end,

	AiPulse = function(self,root)
		if ( 
			not self._Interrupted
			and
			ValidateRangeWithError(SkillData.AllSkills.HealingSkill.Options.BandageRange, self.ParentObj, self.Target, "Too far away.")
		) then
			if ( Success((self.Healing-SkillData.AllSkills.HealingSkill.Options.SkillRequiredToResurrect)*0.02+0.1) and IsDead(self.Target) ) then
				-- resurrect the corpse
				self.Target:SetObjVar("sp_resurrect_effectSource", self.ParentObj)
				self.Target:AddModule("sp_resurrect_effect")
				-- skill gain check
				CheckSkill(self.ParentObj, "HealingSkill", self.Healing, SkillData.AllSkills.HealingSkill.Options.SkillRequiredToResurrect)
				CheckSkill(self.ParentObj, self.SupplimentalSkill, self.Supplimental, SkillData.AllSkills.HealingSkill.Options.SkillRequiredToResurrect)
			else
				if ( IsDead(self.Target) ) then
					self.ParentObj:NpcSpeechToUser("Failed to stir the corpse.", self.ParentObj)
				end
			end
		end
		EndMobileEffect(root)
	end,
	
	ParentObj = nil,
	_HealAmount = 0,
	_MaxHealth = 0,
	_WasDead = false,
	_Interrupted = false,
}