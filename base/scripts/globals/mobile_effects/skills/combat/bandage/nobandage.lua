MobileEffectLibrary.NoBandage = 
{

	-- Options flag for this effect to not be removed on death
	PersistDeath = true,

	PersistSession = true,

	OnEnterState = function(self,root,target,args)
		self.DurationTotalSeconds = args.Duration or 13
		--DebugMessage( "DurationSeconds: " .. self.DurationTotalSeconds )
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "NoBandage", "Recently Bandaged", "skills", "Cannot be bandaged.", true)
			local agility = GetAgi(self.ParentObj) or 10
			local durationModifier =  math.clamp(40/agility, 0.8, 1.5)
			--DebugMessage( "durationModifier: " .. durationModifier )
			if( durationModifier ~= 0 ) then
				self.DurationTotalSeconds = math.ceil( self.DurationTotalSeconds * durationModifier )
			end

		end
		--DebugMessage( "DurationSeconds 2: " .. self.DurationTotalSeconds )

		--SCAN ADDED HEALING TIME REDUCTION BONUS
		local healingSkill = GetSkillLevel(self.ParentObj,"HealingSkill")	
		local healingBonus = 0

		if (healingSkill > 0) and (healingSkill < 30) then 
			healingBonus = 1
		end

		if (healingSkill > 29.99) and (healingSkill < 50) then 
			healingBonus = 2
		end

		if (healingSkill > 49.99) and (healingSkill < 80) then 
			healingBonus = 3
		end

		if (healingSkill > 79.9) and (healingSkill < 100) then 
			healingBonus = 4
		end

		if (healingSkill > 99.9) and (healingSkill < 200) then 
			healingBonus = 5
		end

		--SET DEFAULT BONUS
		self.Duration = TimeSpan.FromSeconds(self.DurationTotalSeconds)
		
		--ADDED Time bonus based on skill
		local timeBonus = self.DurationTotalSeconds-healingBonus
		self.Duration = TimeSpan.FromSeconds(timeBonus)
		self.ParentObj:NpcSpeech("[00FF00]+"..healingBonus.." Healing Time Reduction[-]" ,"combat")
		
		ProgressBar.Show(
		{
			TargetUser = self.ParentObj,
			Label = "Cannot Bandage",
			Duration = timeBonus,
			PresetLocation = "UnderPlayer",
			DialogId = "Heal",
		})
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "NoBandage")
		end
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromSeconds(13),

}