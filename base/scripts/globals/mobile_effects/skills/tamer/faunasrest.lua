MobileEffectLibrary.FaunasRest = 
{
    OnEnterState = function(self,root,target,args)
        
        ForeachActivePet(self.ParentObj, function(pet)
            pet:PlayEffect("HealEffect")
            pet:PlayObjectSound("event:/magic/misc/magic_water_greater_heal")
            SetCurHealth(pet,math.floor(GetMaxHealth(pet)))
            pet:SendMessage("StartMobileEffect", "Immune", nil, { Duration = TimeSpan.FromSeconds(5) })
        end)
        
        EndMobileEffect(root)
	end,
}