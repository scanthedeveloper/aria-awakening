MobileEffectLibrary.PrimalRage = 
{
    OnEnterState = function(self,root,target,args)
        
        ForeachActivePet(self.ParentObj, function(pet)
            pet:SendMessage("StartMobileEffect", "PrimalRageApplied", self.ParentObj, { DurationInSeconds = 10, TargetEffect = "WarriorBloodlustDefensive" })
        end)
        
        EndMobileEffect(root)
	end,
}