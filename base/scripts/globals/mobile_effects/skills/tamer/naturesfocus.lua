MobileEffectLibrary.NaturesFocus = 
{
    OnEnterState = function(self,root,target,args)
        
        ForeachActivePet(self.ParentObj, function(pet)
            pet:SendMessage("StartMobileEffect", "NaturesFocusApplied", self.ParentObj, { DurationInSeconds = 10, TargetEffect = "AfterburnEffect" })
        end)

        EndMobileEffect(root)
	end,
}