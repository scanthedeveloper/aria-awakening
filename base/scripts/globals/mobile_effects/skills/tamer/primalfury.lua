MobileEffectLibrary.PrimalFury = 
{
    OnEnterState = function(self,root,target,args)
        
        ForeachActivePet(self.ParentObj, function(pet)
            pet:SendMessage("StartMobileEffect", "PrimalFuryApplied", self.ParentObj, { DurationInSeconds = 10, TargetEffect = "EnergyShieldEffect" })
        end)
        
        EndMobileEffect(root)
	end,
}