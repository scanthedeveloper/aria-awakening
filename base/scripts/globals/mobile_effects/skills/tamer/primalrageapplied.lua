MobileEffectLibrary.PrimalRageApplied = 
{

    OnEnterState = function(self,root,target,args)
        self._ID = args.MobileModID or "mod_"..uuid()
        self._Args = args

        -- If our target is not our owner we need to stop!
        if(  GetPetOwner(self.ParentObj) ~= target ) then EndMobileEffect(root) return false end

        SetMobileMod(self.ParentObj, "AttackTimes", self._ID, 0.5)

        -- Play effect if able
        if( self._Args.TargetEffect ) then
            if (self._Args.EffectBone~=nil) then
                self.ParentObj:PlayEffectWithArgs(self._Args.TargetEffect,0.0,self._Args.EffectBone)
            else
                self.ParentObj:PlayEffect(self._Args.TargetEffect)
            end
        end
        
    end,

    OnExitState = function(self,root)
        self._CleanUp(self, root)
    end,
     
    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
    end,
    
    AiPulse = function(self,root) 

        if( self.PulsesCount >= self._DurationInSeconds ) then
            EndMobileEffect(root)
        end
        self.PulsesCount = self.PulsesCount + 1
    end,

    _CleanUp = function(self, root)

        -- Stop effect if able
        if( self._Args.TargetEffect ) then
            self.ParentObj:StopEffect(self._Args.TargetEffect, 4)
        end

        SetMobileMod(self.ParentObj, "AttackTimes", self._ID, nil)
    end,

    PulsesCount = 0,
    _Args = nil,
    _ID = nil,
    _DurationInSeconds = 3,
}