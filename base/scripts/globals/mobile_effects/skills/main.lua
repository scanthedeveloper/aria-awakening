
--combat
require 'globals.mobile_effects.skills.combat.focus'
require 'globals.mobile_effects.skills.combat.hide'

--martial
require 'globals.mobile_effects.skills.martial.enrage'
require 'globals.mobile_effects.skills.martial.bloodlust'
require 'globals.mobile_effects.skills.martial.demoralize'
require 'globals.mobile_effects.skills.martial.demoralized'
require 'globals.mobile_effects.skills.martial.execute'
require 'globals.mobile_effects.skills.martial.nemesis'
require 'globals.mobile_effects.skills.martial.shieldwall'
require 'globals.mobile_effects.skills.martial.taunt'
require 'globals.mobile_effects.skills.martial.challengingshout'
require 'globals.mobile_effects.skills.martial.vengeance'

--SCAN ADDED
require 'globals.mobile_effects.skills.martial.taunt_damage'

--combat.bandage
require 'globals.mobile_effects.skills.combat.bandage.bandage'
require 'globals.mobile_effects.skills.combat.bandage.nobandage'
require 'globals.mobile_effects.skills.combat.bandage.salve'

--pets
require 'globals.mobile_effects.skills.pets.tame'
require 'globals.mobile_effects.skills.pets.beingtamed'
require 'globals.mobile_effects.skills.pets.command'

--bard
require 'globals.mobile_effects.skills.bard.play'
require 'globals.mobile_effects.skills.bard.solo'
require 'globals.mobile_effects.skills.bard.listen_to_band'
require 'globals.mobile_effects.skills.bard.song_aoe'
require 'globals.mobile_effects.skills.bard.riddle_aoe'
require 'globals.mobile_effects.skills.bard.riddle_targeted'
require 'globals.mobile_effects.skills.bard.resoundingechoes'
require 'globals.mobile_effects.skills.bard.resoundingdrums'
require 'globals.mobile_effects.skills.bard.azurasblessing'
require 'globals.mobile_effects.skills.bard.yurinslullaby'




--rogue
require 'globals.mobile_effects.skills.rogue.snoop'

--resources
require 'globals.mobile_effects.skills.resources.harvest'
require 'globals.mobile_effects.skills.resources.mill'
require 'globals.mobile_effects.skills.resources.smelt'
require 'globals.mobile_effects.skills.resources.fish'
require 'globals.mobile_effects.skills.resources.dig'
require 'globals.mobile_effects.skills.resources.steal'
require 'globals.mobile_effects.skills.resources.enchant'

--gardening
require 'globals.mobile_effects.skills.gardening.plantseed'
require 'globals.mobile_effects.skills.gardening.waterplant'
require 'globals.mobile_effects.skills.gardening.gatherplant'
require 'globals.mobile_effects.skills.gardening.emptyplant'

-- sorcerer
require 'globals.mobile_effects.skills.sorcerer.summonwisp'
require 'globals.mobile_effects.skills.sorcerer.consumewisp'
require 'globals.mobile_effects.skills.sorcerer.sorcery'
require 'globals.mobile_effects.skills.sorcerer.summonfox'
require 'globals.mobile_effects.skills.sorcerer.summonconstruct'
require 'globals.mobile_effects.skills.sorcerer.consumesummon'
require 'globals.mobile_effects.skills.sorcerer.magicmissile'
require 'globals.mobile_effects.skills.sorcerer.summonbeam'
require 'globals.mobile_effects.skills.sorcerer.summonbeam_damage'
require 'globals.mobile_effects.skills.sorcerer.wisparmor'
require 'globals.mobile_effects.skills.sorcerer.summonstarfall'
require 'globals.mobile_effects.skills.sorcerer.sorcery_cooldown'
require 'globals.mobile_effects.skills.sorcerer.wisp_affliction'

-- tamer
require 'globals.mobile_effects.skills.tamer.naturesfocus'
require 'globals.mobile_effects.skills.tamer.naturesfocusapplied'
require 'globals.mobile_effects.skills.tamer.primalrage'
require 'globals.mobile_effects.skills.tamer.primalrageapplied'
require 'globals.mobile_effects.skills.tamer.primalfury'
require 'globals.mobile_effects.skills.tamer.primalfuryapplied'
require 'globals.mobile_effects.skills.tamer.faunasrest'