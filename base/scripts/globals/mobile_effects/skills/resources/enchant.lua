MobileEffectLibrary.Enchant = 
{
    OnEnterState = function(self,root,target,args)

        self.GenerateEnchantWindow(self, root)

        -- Get enchantment window responses
        RegisterEventHandler(EventType.DynamicWindowResponse, "EnchantmentWindow", 
            function( user, id )
                self.HandleEnchantResponse( user, id, self, root )
            end
        )
	end,

    OnExitState = function(self,root)
        UnregisterEventHandler("",EventType.DynamicWindowResponse,"EnchantmentWindow")
        DelView("CheckEnchantPouchAdded")
        self._enchantPouch:Destroy()
    end,

    GenerateEnchantWindow = function(self, root)
        self._mainWindow = DynamicWindow("EnchantmentWindow","Enchant Item",800,480,-410,-280,"Parchment","Center")
        
        --Left side section
        self._mainWindow:AddLabel(130,20,"[412A08]Place Item to Enchant[-]",220,20,22,"center",false,false,"PermianSlabSerif_Dynamic_Bold","")
        self._mainWindow:AddImage(60,60,"DropHeaderBackground",144,144,"Sliced")
        self._mainWindow:AddLabel(130,245,"[412A08]Place Essences Below[-]",220,20,22,"center",false,false,"PermianSlabSerif_Dynamic_Bold","")

        -- Create the enchanting pouch as temp container on player
        if( self._enchantPouch == nil ) then
            Create.Equipped("pouch_enchant", self.ParentObj, 
                function(objRef)
                    objRef:SetObjVar("EnchantingPouch",true)
                    self._enchantPouch = objRef
                end
            )        
        end
        
        -- Add the section for weapons/armor to be dropped on.
        self._mainWindow:AddButton(80,94,"EnchantCarriedObject","",100,100,self._enchantItemLabel,"",false,"Invisible")

        -- If we have an item we are enchanting, lets show the icon; this overlays on the button above
        if( self._enchantItem ~= nil ) then
            local itemTemplate = self._enchantItem:GetObjVar("BaseItemObjVar") or self._enchantItem:GetObjVar("UnpackedTemplate") or self._enchantItem:GetCreationTemplateId()
            self._mainWindow:AddImage(85,90,tostring(GetTemplateIconId(itemTemplate)),90,90,"Object")
        end

        -- Check to see what all is in the enchant pouch
        local enchantPouchArgs = EnchantingHelper.CalculateEssencesInPouch( self._enchantPouch, self._enchantItem )                

        local essenceStr = "[927c4a]Add Essence[-]"
        local essenceTooltip = "Drop essence here."
        if(enchantPouchArgs and #enchantPouchArgs.Items > 0) then
            local essenceTemplate = enchantPouchArgs.Items[1]:GetCreationTemplateId()
            self._mainWindow:AddImage(22,292,tostring(GetTemplateIconId(essenceTemplate)),60,60,"Object")
            essenceStr = "[412A08]"..StripColorFromString(enchantPouchArgs.Items[1]:GetName()).."[-]"
            essenceTooltip = "Click to remove item."
        end
        self._mainWindow:AddButton(20,290,"Essence|1","",64,64,essenceTooltip,"",false,"Invisible")
        self._mainWindow:AddImage(20,290,"empty_circle",64,64,"Sliced")
        self._mainWindow:AddLabel(48,364,essenceStr,80,64,18,"center",false,false,"PermianSlabSerif_Dynamic_Bold","")        

        essenceStr = "[927c4a]Add Essence[-]"
        essenceTooltip = "Drop essence here."
        if(enchantPouchArgs and #enchantPouchArgs.Items > 1) then
            local essenceTemplate = enchantPouchArgs.Items[2]:GetCreationTemplateId()
            self._mainWindow:AddImage(104,292,tostring(GetTemplateIconId(essenceTemplate)),60,60,"Object")
            essenceStr = "[412A08]"..StripColorFromString(enchantPouchArgs.Items[2]:GetName()).."[-]"
            essenceTooltip = "Click to remove item."
        end
        self._mainWindow:AddButton(102,290,"Essence|2","",64,64,essenceTooltip,"",false,"Invisible")
        self._mainWindow:AddImage(102,290,"empty_circle",64,64,"Sliced")
        self._mainWindow:AddLabel(132,364,essenceStr,80,64,18,"center",false,false,"PermianSlabSerif_Dynamic_Bold","")        

        essenceStr = "[927c4a]Add Essence[-]"
        essenceTooltip = "Drop essence here."
        if(enchantPouchArgs and #enchantPouchArgs.Items > 2) then
            local essenceTemplate = enchantPouchArgs.Items[3]:GetCreationTemplateId()
            self._mainWindow:AddImage(186,292,tostring(GetTemplateIconId(essenceTemplate)),60,60,"Object")
            essenceStr = "[412A08]"..StripColorFromString(enchantPouchArgs.Items[3]:GetName()).."[-]"
            essenceTooltip = "Click to remove item."
        end
        self._mainWindow:AddButton(184,290,"Essence|3","",64,64,essenceTooltip,"",false,"Invisible")
        self._mainWindow:AddImage(184,290,"empty_circle",64,64,"Sliced")
        self._mainWindow:AddLabel(214,364,essenceStr,80,64,18,"center",false,false,"PermianSlabSerif_Dynamic_Bold","")

        self._mainWindow:AddImage(276,20,"Blank",1,400,"Sliced","A49062")        
        
        -- self._ItemName is set when a weapon/armor piece is dropped on the EnchantCarriedObject button
        if( self._ItemName ~= nil and enchantPouchArgs ~= false ) then
            --DebugTable( enchantPouchArgs )

            -- Combine the prefix, name, and suffix
            local finalItemName = EnchantingHelper.GetItemNameFromArgs( self._enchantItem, enchantPouchArgs )

            -- Generate the label of the new object that will be created on enchanting
            self._mainWindow:AddLabel(533,20,"[412A08]"..finalItemName.."[-]",476,120,46,"center",false,false,"Kingthings_Dynamic")

            local modListY = 110
            -- Do enchant stats
            for i=1, #enchantPouchArgs.Enchants do 

                local string = EnchantingHelper.GetEnchantModifierLabel(enchantPouchArgs, i)
                self._mainWindow:AddImage(306,modListY,"Blank",450,38,"Sliced","A49062")  
                self._mainWindow:AddLabel(340,modListY+9,string,510,30,26,"left")
                modListY = modListY + 42

            end            
        end

        local cost = (enchantPouchArgs and enchantPouchArgs.Cost) or 0
        local backpackCanBuy = ( CountCoins(self.ParentObj) >= cost )
        local bankCanBuy = ( CountCoinsInBank(self.ParentObj) >= cost )
        local canAfford = ( backpackCanBuy or bankCanBuy )
        local costLabel = (canAfford and "008000") or "FF0000"
        self._mainWindow:AddLabel(533,260,"[412A08]GOLD COST:[-]",0,0,25,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
           
        self._mainWindow:AddLabel(533,290,"["..costLabel.."]"..cost.."[-]",0,0,40,"center",false,false,"PermianSlabSerif_Dynamic_Bold")

        -- Generate enchant button
        local buttonState = "disabled"
        
        if( enchantPouchArgs and enchantPouchArgs.Enchants and #enchantPouchArgs.Enchants > 0 and canAfford ) then
            buttonState = ""                    
        end
        self._mainWindow:AddButton(392,340 ,"EnchantButton","Enchant",0,0,"Enchant this item.","",true,"GoldRedButton",buttonState)
                
        -- Generate window
        self.ParentObj:OpenDynamicWindow(self._mainWindow)
    end,
    
    HandleEnchantResponse = function( user, id, self, root )
        -- Make sure we are the right user and we didn't just close the window!
        if(id == nil or id == "" or self.ParentObj ~= user) then 
            TransferContainerContents( self._enchantPouch, self.ParentObj:GetEquippedObject("Backpack") )
            EndMobileEffect(root)
            return
        end

        local args = StringSplit(id, "|")
        local id = args[1]

        -- Something was dropped on the weapon/armor dropbox, or they clicked it to remove the current item
        if (id == "EnchantCarriedObject") then
            local carriedItem = self.ParentObj:CarriedObject()

            -- We are dropping an item on the echantment box.
            if( carriedItem ~= nil ) then

                -- Is this item enchantable?
                if not( EnchantingHelper.IsEnchantable(carriedItem)  ) then
                    self.ParentObj:SystemMessage("That item cannot be enchanted.", "info")
                    return
                end

                -- We set this so we can use CanEssenceBeAppliedToObject() in the container.lua
                self._enchantPouch:SetObjVar("enchantItem", carriedItem)
                self._enchantItem = carriedItem
                self._ItemName = self._enchantItem:GetName()
                self._enchantItemLabel = "Click to remove item."
                -- Place item back in players pack; from the cursor
                TryPutObjectInContainer(self._enchantItem, self.ParentObj:GetEquippedObject("Backpack"))
            
            -- We are removing the item
            else
                self._enchantItem = nil
                self._ItemName = ""
                self._enchantPouch:DelObjVar("enchantItem")
                self._enchantItemLabel = "Drop item to enchant here."
                TransferContainerContents( self._enchantPouch, self.ParentObj:GetEquippedObject("Backpack") )
            end
            
            -- Regenerate the window
            self.GenerateEnchantWindow(self, root)
            return
        elseif( id == "Essence" ) then
            if not(self._enchantPouch) then return end
            
            -- We are dropping an item on the echantment box.
            local carriedItem = self.ParentObj:CarriedObject()
            if( carriedItem ~= nil ) then
                local success, reason = TryPutObjectInContainer(carriedItem, self._enchantPouch, Loc(0,0,0), false, true, false, self.ParentObj, self.ParentObj)
                if(success) then
                    -- Regenerate the window
                    self.GenerateEnchantWindow(self, root)
                else
                    self.ParentObj:SystemMessage(reason,"info")
                end
            else
                local enchantIndex = tonumber(args[2])
                local enchantPouchArgs = EnchantingHelper.CalculateEssencesInPouch( self._enchantPouch, self._enchantItem )
                if(enchantPouchArgs and #enchantPouchArgs.Items >= enchantIndex) then
                    enchantPouchArgs.Items[enchantIndex]:MoveToContainer(self.ParentObj,Loc(0,0,0))

                    -- Regenerate the window
                    self.GenerateEnchantWindow(self, root)
                end
            end
        elseif( id == "EnchantButton" ) then
            --DebugMessage( "Enchant button pressed" )
            local enchantPouchArgs = EnchantingHelper.CalculateEssencesInPouch(self._enchantPouch, self._enchantItem)
            local name = EnchantingHelper.GetItemNameFromArgs( self._enchantItem, enchantPouchArgs )
            local cost = (enchantPouchArgs and enchantPouchArgs.Cost) or 0
            local backpackCanBuy = ( CountCoins(self.ParentObj) >= cost )
            local bankCanBuy = ( CountCoinsInBank(self.ParentObj) >= cost )
            local canAfford = ( backpackCanBuy or bankCanBuy )

            if not( canAfford ) then
                self.ParentObj:SystemMessage("You cannot afford to enchant that.","info")
                TransferContainerContents( self._enchantPouch, self.ParentObj:GetEquippedObject("Backpack") )
                EndMobileEffect(root)
                return
            end

            if not( enchantPouchArgs.CanEnchant ) then
                self.ParentObj:SystemMessage("Those essences cannot be applied to that item.","info")
                TransferContainerContents( self._enchantPouch, self.ParentObj:GetEquippedObject("Backpack") )
                EndMobileEffect(root)
                return
            end

            RegisterSingleEventHandler(EventType.Message, "ConsumeResourceResponse", function( success, transactionId, consumer ) 
                if( name and enchantPouchArgs ~= nil and success and consumer == self.ParentObj and transactionId == "EnchantItem" ) then
                    self._enchantItem:SetName( name )
    
                    local enchants = self._enchantItem:GetObjVar("Enchants") or {}
                    local numEnchants = #enchants


                    for i=1, #enchantPouchArgs.Enchants do 
                        if not ( numEnchants >= EssenceData.MaximumEnchantsPerItem ) then
                            table.insert(enchants, { 
                                essenceName = enchantPouchArgs.Enchants[i].essenceName,
                                essenceModifier = math.random( enchantPouchArgs.Enchants[i].minMax[1], enchantPouchArgs.Enchants[i].minMax[2] )
                            })
                            numEnchants = numEnchants + 1
                        end
                    end
    
                    self._enchantItem:SetObjVar("Enchants", enchants)
    
                    EnchantingHelper.UpdateItemTooltip( self._enchantItem )
    
                    -- Consume all the essences in our pouch
                    EnchantingHelper.EssencePouchConsumeContents(self._enchantPouch)
    
                    self.ParentObj:SystemMessage("You successfully enchant the item.","info")
                    self.ParentObj:PlayEffect("IceExplosionEffect")
                    self.ParentObj:PlayObjectSound("event:/environment/misc/death_canon",false)
                    EndMobileEffect(root)
                end
            end)

            local backpackCanBuy = ( CountCoins(self.ParentObj) >= cost )
            local bankCanBuy = ( CountCoinsInBank(self.ParentObj) >= cost )
            
            if( backpackCanBuy ) then
                RequestConsumeResource(self.ParentObj,"coins",cost,"EnchantItem",self.ParentObj)
            else
                RequestConsumeResourceFromBank(self.ParentObj,"coins",cost,"EnchantItem",self.ParentObj)
            end
            
        end
    end,
    
    -- Variables
    _enchantPouch = nil,
    _mainWindow = nil,
    _enchantItem = nil,
    _enchantItemLabel = "Drop item to enchant here.",
    _ItemName = nil,

}