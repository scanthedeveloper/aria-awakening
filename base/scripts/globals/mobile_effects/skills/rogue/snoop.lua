MobileEffectLibrary.Snoop = 
{
    QTarget = true,

    OnEnterState = function(self,root,target)
    	self.RequestInitialTarget(self, root, target)
    end,

    OnStack = function(self,root,target,args)
        self.OnEnterState(self,root,target)
        return
    end,

    RequestInitialTarget = function(self,root,target)
		-- handle a new target
		RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse, "SnoopTarget",
			function (snoopTarget)
				if(snoopTarget ~= nil) then
					if not( self.VerifyTarget(self,root,snoopTarget) ) then EndMobileEffect(root) return false end
					self.Target = snoopTarget
					self.StartSnooping(self, root, snoopTarget)
					return true
				end
				self.ParentObj:SystemMessage("Snoop cancelled.","info")
				EndMobileEffect(root)
				return false
			end)

		self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "SnoopTarget")
	end,

	StartSnooping = function (self, root, target)
        RegisterEventHandler(EventType.StartMoving, "", function()
            EndMobileEffect(root)
        end)

        RegisterEventHandler(EventType.Message, "BreakInvisEffect", function (what)
            EndMobileEffect(root)
        end)

        local skillDict = GetSkillDictionary(self.ParentObj)        
        local snoopSkill = GetSkillLevel(self.ParentObj, "SnoopingSkill", skillDictionary)

        -- do a pure skill check for success
        if not(CheckSkillChance( self.ParentObj, "SnoopingSkill", snoopSkill, nil, false, skillDict ) ) then
            target:SystemMessage("You feel someone trying to peek into your pack.", "info")
            self.ParentObj:SystemMessage("You fail to open their pack.", "info")
            EndMobileEffect(root)
            return false
        end

        -- turn the fucker grey
        ExecuteCriminalAction(self.ParentObj, target)
        
        -- do a tough skill check to see if the person notices, no skill gain        
        if not(Success(SkillValueMinMax(snoopSkill,50,110))) then
            target:SystemMessage("You feel someone rummaging through your pack.", "info")
        end

        self.BackpackObj = target:GetEquippedObject("Backpack")
        if(self.BackpackObj) then
            self.BackpackObj:SendOpenContainer(self.ParentObj)
        end

        target:SetObjVar("LastSnooped",DateTime.UtcNow)

        self.ParentObj:SetObjVar("SnoopTarget",target)
	end,

    OnExitState = function(self,root)
        if ( self.Target ) then
            UnregisterEventHandler("", EventType.Message, "BreakInvisEffect")
            UnregisterEventHandler("", EventType.StartMoving, "")            
			UnregisterEventHandler("", EventType.ClientTargetGameObjResponse, "SnoopTarget")

            self.ParentObj:DelObjVar("SnoopTarget")

            if(self.BackpackObj) then
                self.BackpackObj:SendCloseContainer(self.ParentObj)
            end
        end
	end,

    VerifyTarget = function(self,root,target)
        if ( not target or self.ParentObj == target or not target:IsMobile() ) then
            self.ParentObj:SystemMessage("Invalid Target.", "info")
            EndMobileEffect(root)
            return false
        end

        if( not IsPet( target ) and not IsPlayerCharacter(target) ) then
            self.ParentObj:SystemMessage("You cannot snoop this target.", "info")
            EndMobileEffect(root)
            return false
        end

        if( IsGod( target ) and not IsGod(self.ParentObj) ) then
            self.ParentObj:SystemMessage("You cannot snoop the gods!", "info")
            EndMobileEffect(root)
            return false
        end

        if( self.ParentObj:DistanceFrom(target) > OBJECT_INTERACTION_RANGE) then
            self.ParentObj:SystemMessage("Too far away.", "info")
            EndMobileEffect(root)
            return false
        end

        if not( self.ParentObj:HasLineOfSightToObj(target,ServerSettings.Combat.LOSEyeLevel) ) then
            self.ParentObj:SystemMessage("Cannot see that.", "info")
            EndMobileEffect(root)
            return false
        end

        if not( HasMobileEffect(self.ParentObj, "Hide") ) then
            self.ParentObj:SystemMessage("You can not do that in plain sight!", "info")
            EndMobileEffect(root)
            return false 
        end

        if( IsProtected(target, self.ParentObj, nil, true) ) then
            self.ParentObj:SystemMessage("The gods will not allow that.", "info")
            EndMobileEffect(root)
            return false 
        end

        if( self.ParentObj:IsInRegion("ForestOutpost") ) then
            self.ParentObj:SystemMessage("The guards are too aware of your presence.", "info")
            EndMobileEffect(root)
            return false 
        end

        return true
    end,

    GetPulseFrequency = function(self,root)
		return self.PulseFrequency
    end,

    AiPulse = function(self,root)
        -- resource effects will verify in backpack the first use, but not on continued use.
        if ( not self.VerifyTarget(self,root,self.Target) ) then return end
        
        -- do a tough skill check to see if the person notices, no skill gain
        local snoopSkill = GetSkillLevel(self.ParentObj, "SnoopingSkill")
        if not(Success(SkillValueMinMax(snoopSkill,50,110))) then
            self.Target:SystemMessage("You feel someone rummaging through your pack.", "info")
        end
    end,

    PulseFrequency = TimeSpan.FromSeconds(5),
    DistanceSquared = 9,
    BackpackObj = nil,
}