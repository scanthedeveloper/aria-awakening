MobileEffectLibrary.GatherPlant = 
{

        -- @param: self.ParentObj should be plantObj
        -- @param: target should be a player
	OnEnterState = function(self,root,target,args)

        -- Is the target (person watering) a player?
        if not( target:IsPlayer() ) then 
                EndMobileEffect(root)
                return false 
        end
        
        -- Make sure we have a plant
        if not( Gardening.IsPlant(self.ParentObj) ) then
                target:SystemMessage("Invalid target.", "info")
                EndMobileEffect(root)
                return false
        end

        Gardening.GatherPlant(target, self.ParentObj)
        EndMobileEffect(root)
	end,

}