MobileEffectLibrary.PlantSeed = 
{

	OnEnterState = function(self,root,target,args)

        -- Make sure we're trying to plant in a valid container
		if ( not target or not target:IsValid() or not target:HasObjVar("MaximumPlantSize") ) then
			self.ParentObj:SystemMessage("Invalid planting container.", "info")
			EndMobileEffect(root)
			return false
        end
        
        -- Ensure the plant container can be accessed
		if not( Gardening.CanPlantContainerBeUsed(self.ParentObj, target) ) then
			self.ParentObj:SystemMessage("Cannot reach that.", "info")
			EndMobileEffect(root)
			return false
        end

        -- Check to see if there is already a plant?
        if ( target:GetObjVar("Planted") == true ) then
            self.ParentObj:SystemMessage("There is already a plant there.", "info")
			EndMobileEffect(root)
			return false
        end

        -- Check to make sure the seed has all the information we need.
        if( args.PlantType == nil or args.DaysToMature == nil or args.MaximumYield == nil or args.PlantSize == nil) then
            self.ParentObj:SystemMessage("You cannot plant that seed.", "info")
			EndMobileEffect(root)
			return false
        end

        -- Start your engines, we're going gardening!
        local success = Gardening.PlantSeed( target, args )
        local message = "That seed cannot be planted."
        if( success ) then
            message = "The seed has been planted."
        end
        
        self.ParentObj:SystemMessage(message, "info")
        EndMobileEffect(root)
        
        -- This is required; it determines if the seed is consumed (true) or not (false)!
        return success
	end,
}