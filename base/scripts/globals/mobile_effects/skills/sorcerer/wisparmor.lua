MobileEffectLibrary.WispArmor = 
{
    OnEnterState = function(self,root,target,args)       
        -- If they don't have the DLC they cannot do this!
        if not( SorceryHelper.CanUseSorceryAbilities(self.ParentObj, "WispArmor") )then EndMobileEffect(root) return false end
        
        self._IsPlayer = IsPlayerCharacter( self.ParentObj )

        if not(self._ArmorValue) then
            self._ArmorValue = 0
        else
            self._ArmorValue = self.ParentObj:GetObjVar("WispArmor") or 0
        end

        if not(self._ArmorEffects) then
            self._ArmorEffects = {}
        end

        --[[Clean up any previous shield effects
        for i=1,4 do
            self.ParentObj:StopEffect("ShieldEffect"..tostring(i))
        end
        ]]

        if( args.Damage ) then
            self.UpdateArmorValue( self, root, -args.Damage )
            return
        else

            if( self._ArmorValue >= ( SorceryHelper.WispArmorIncrease * 4 ) ) then
                self.ParentObj:NpcSpeechToUser("Wisp armor at maximum.",self.ParentObj)
                return
            end

            if( self._Loaded ~= true ) then
                -- We need to end this effect if the Sorcery MobileEffect ends.
                RegisterEventHandler(EventType.Message, "SorceryMessage", function( message, args )
                    args = args or {}

                    if( message == "EndSorceryEffects" ) then
                        EndMobileEffect(root)
                    elseif( message == "ActiveWispCount" and args.MessageTag == "WispArmorActivate" ) then
                        local wispCount = args.WispCount or 1
                        local totalShields = math.ceil((self._ArmorValue/SorceryHelper.WispArmorMaxValue) * 4)
                        wispCount = math.min( wispCount, ( 4 - totalShields ) )
                        SorceryHelper.TryConsumeWisp( self.ParentObj, wispCount, function(success, wispsConsumed)  
                            if( success ) then
                                if( #wispsConsumed ~= 0 ) then
                                    StartPrestigePositionCooldown(self.ParentObj, "Sorcery", "WispArmor", PrestigeData.Sorcery.Abilities.WispArmor.Cooldown )
                                    local armorValue = 0
                                    self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_force")
                                    self.ParentObj:PlayObjectSound("event:/character/combat_abilities/pommel")
                                    self.ParentObj:PlayAnimation("cast_heal")
                                    for i=1,#wispsConsumed do
                                        armorValue = armorValue + SorceryHelper.WispArmorIncrease
                                    end
                                    self.UpdateArmorValue( self, root, armorValue )
                                end
                            end
                        end)

                    elseif( message == "ConsumeWispShield" ) then
                        self.UpdateArmorValue( self, root, -SorceryHelper.WispArmorIncrease )
                        return
                    end
                end)
            end
            
            SorceryHelper.SorceryMessageSender( self.ParentObj, "GetActiveWispCount", { MessageTag = "WispArmorActivate" })
            
            
        end

        self._Loaded = true
        

        
    end,

    OnStack = function(self,root,target,args)
        self.OnEnterState( self, root, target, args )
	end,
    
    OnExitState = function(self,root)
        --UnregisterEventHandler("", EventType.Message, "SorceryMessage")
        if( self._IsPlayer ) then
            RemoveBuffIcon(self.ParentObj, "WispArmor")
            self.ParentObj:SystemMessage("Your wisp armor fades.", "info")
        end

        --Clean up shield effects
        if( self._ArmorEffects ) then
            for i= 1,#self._ArmorEffects do
                self.ParentObj:StopEffect("WispShield"..tostring(i))
                self._ArmorEffects[i] = nil
            end
        end
        
        self.ParentObj:DelObjVar("WispArmor")
    end,

    UpdateArmorValue = function( self, root, delta )
        self._ArmorValue = math.min(self._ArmorValue + delta, SorceryHelper.WispArmorMaxValue)
        
        if ( self.ParentObj:IsPlayer() ) then
            AddBuffIcon(self.ParentObj, "WispArmor", "Wisp Armor", "Arcane Shield2", "Absorbs the next " .. self._ArmorValue .. " damage." , true)
        end
        
        --DebugMessage(self._ArmorValue)
        if( self._ArmorValue > 0 ) then
            self.ParentObj:SetObjVar("WispArmor", self._ArmorValue)

            --Find number of shields there are supposed to be
            local totalShields = math.ceil((self._ArmorValue/SorceryHelper.WispArmorMaxValue) * 4)

            for i=1, 4 do 

                -- If our wisp is empty and we have a shield in that spot, create it
                if( totalShields >= i and self._ArmorEffects[i] == nil ) then
                    local effectId = "WispShield"..tostring(i)
                    self.ParentObj:PlayEffectWithArgs("WispShieldEffect", 0.0, "WispIndex="..tostring(i)..",EffectId="..effectId)
                    self._ArmorEffects[i] = effectId
                end
                
                if( totalShields < i and self._ArmorEffects[i] ~= nil ) then
                    self.ParentObj:StopEffect("WispShield"..tostring(i))
                    self.ParentObj:PlayEffectWithArgs("WispExplosionEffect", 2.0, "WispIndex="..tostring(i))
                    self._ArmorEffects[i] = nil
                end

            end

        else
            EndMobileEffect(root)
        end

    end,

    AddShieldEffect = function (self, root, wispsConsumed)
        
    end,

    _ArmorValue = 0,
    _Loaded = false,
    _EffectActive = false,
    _BubbleEffect = "BubbleEnergyEffect",

}