MobileEffectLibrary.MagicMissile = 
{
    OnEnterState = function(self,root,target,args)
        -- If they don't have the DLC they cannot do this!
        if not( SorceryHelper.CanUseSorceryAbilities(self.ParentObj, "MagicMissile") )then EndMobileEffect(root) return false end
        StartPrestigePositionCooldown(self.ParentObj, "Sorcery", "MagicMissile", PrestigeData.Sorcery.Abilities.MagicMissile.PostCooldown )
        
        self.QueuedTarget = nil
        self._Message = nil
        self._AutoTarget = self.ParentObj:HasObjVar("AutotargetEnabled")
        self._CurrentTarget = self.ParentObj:GetObjVar("CurrentTarget")
        if(  
            self._AutoTarget 
            and self._CurrentTarget
            and not self.TargetInvalid(self, self._CurrentTarget)
        ) then
            self.UseAbility( self, root, self._CurrentTarget )
        else

            -- We need to register getting our target back
            RegisterEventHandler(EventType.ClientTargetGameObjResponse, "SorcerySpellTarget", 
            function(target)
                self.UseAbility( self, root, target )
            end)

            RegisterEventHandler(EventType.Message, "CancelSpellCast", function() EndMobileEffect(root) end)
            self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "SorcerySpellTarget")

        end
    end,

    TargetInvalid = function( self, target )
        return (
            target == nil 
            or not target:IsValid() 
            or not ValidCombatTarget(self.ParentObj, target)
            or ShouldCriminalProtect(self.ParentObj, target)
        )
    end,

    UseAbility = function( self, root, target )
        if( target == nil ) then EndMobileEffect(root) return end
        
        -- If the target isn't valid get out of here!
        if(
            self.TargetInvalid(self, target)
        ) then 
            self._Message = "Invalid target." 
            EndMobileEffect(root) 
            return false 
        end
        
        -- Are we in range for a harmful ability?
        if( self.ParentObj:DistanceFrom(target) > SorceryHelper.HarmfulAbilityRange ) then
            self._Message = "Out of range." 
            EndMobileEffect(root) 
            return
        end

        -- Queue up our target
        self.QueuedTarget = target

        -- Can we send a wisp to this target?
        if( SorceryHelper.CanSendWispToTargetMobile(self.ParentObj, self.QueuedTarget) ) then

            -- Try and consume a wisp from our player
            SorceryHelper.TryConsumeWisp( self.ParentObj, 1, function(success, wispsConsumed, doNotTriggerCooldown)  

                -- Wisp consumed
                if( success ) then
                    -- Try and send a wisp to the target
                    self.ParentObj:PlayAnimation("spell_fire")
                    self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_force")

                    -- This delay is to make the animation sync with the travel speed of the wisp.
                    CallFunctionDelayed(TimeSpan.FromMilliseconds(200), function()
                        SorceryHelper.SendWispToTargetMobile( wispsConsumed, self.ParentObj, self.QueuedTarget, 300, function(success)
                            -- THIS IS WHERE THE MAGIC HAPPENS!
                            if( success ) then
                                self.ParentObj:PlayObjectSound("event:/magic/air/magic_air_lightning_impact")
                                local damageAmount = math.floor( math.min(math.max( ((GetSkillLevel( self.ParentObj, "SorcerySkill" ) / 100) * SorceryHelper.MagicMissleMinMaxDamage[2]) , SorceryHelper.MagicMissleMinMaxDamage[1]), SorceryHelper.MagicMissleMinMaxDamage[2]) )
                                SorceryHelper.DealSorceryDamageToTarget( self.ParentObj, self.QueuedTarget, damageAmount )
                                self.QueuedTarget:SendMessage( "StartMobileEffect", "WispAffliction" )
                            else
                                self.ParentObj:NpcSpeechToUser("Wisp cannot perform that action.",self.ParentObj)
                            end
                        end)

                        if( not doNotTriggerCooldown ) then
                            StartPrestigePositionCooldown(self.ParentObj, "Sorcery", "MagicMissile", PrestigeData.Sorcery.Abilities.MagicMissile.PostCooldown )
                        end

                        EndMobileEffect(root)
                    end)

                else -- wisp wasn't consumed
                    --self.ParentObj:NpcSpeechToUser("Not enough wisps.",self.ParentObj)
                    EndMobileEffect(root)
                    return
                end
                
            end)

        else -- wisp cannot get to target
            self._Message = "Cannot see target."
            EndMobileEffect(root)
            return false
        end
    end,

    OnStack = function(self,root,target,args)
        --DebugMessage("STACKING!!!")
        self.ParentObj:SendClientMessage("CancelSpellCast")
        EndMobileEffect(root)
	end,
    
    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "SorcerySpellTarget")
        UnregisterEventHandler("", EventType.Message, "CancelSpellCast")
        if( self._Message ~= nil ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
    end,
}