MobileEffectLibrary.WispAffliction = 
{
    OnEnterState = function(self,root,target,args)
        self._Stacks = self._Stacks + 1
        self._Stacks = math.min( self._Stacks, SorceryHelper.MaxAfflictionStacks)
        local damageIncrease = (self._Stacks * SorceryHelper.AfflictionIncreasePerStacks)
        SetMobileMod(self.ParentObj, "SorceryDamageTakenTimes", "WispAffliction", damageIncrease)

        if( self._Stacks == 1 ) then
            self.ParentObj:PlayEffect("PrimedVoid")
        elseif( self._Stacks == 3 and not self._MaxApplied ) then
            self._MaxApplied = true
            self.ParentObj:PlayEffect("SlowEffect")
        end

        if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "WispAffliction", "Affliction", "Summon Raven 01", "Echo damaged taken increased by " .. tostring( damageIncrease * 100 ) .. "%" , true)
        end

        self._EndTime = os.time(os.date("!*t")) + 15
    end,

    OnExitState = function(self, root)
        SetMobileMod(self.ParentObj, "SorceryDamageTakenTimes", "WispAffliction", nil)
        self.ParentObj:StopEffect("PrimedVoid")
        self.ParentObj:StopEffect("SlowEffect")
        if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "WispAffliction")
        end
    end,

    OnStack = function(self,root,target,args)
        self.OnEnterState( self, root, target, args )
	end,

    AiPulse = function(self,root)
        local unixTS = os.time(os.date("!*t"))
        if( unixTS >= self._EndTime ) then
            EndMobileEffect(root)
            return
        end
    end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
    end,

    _Stacks = 0,
    _MaxApplied = false
}