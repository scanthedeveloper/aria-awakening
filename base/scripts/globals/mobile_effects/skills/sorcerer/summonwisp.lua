MobileEffectLibrary.SummonWisp = 
{
    OnEnterState = function(self,root,target,args)
        -- If they don't have the DLC they cannot do this!
        if not( SorceryHelper.CanUseSorceryAbilities(self.ParentObj) )then EndMobileEffect(root) return false end
        
        -- If we don't have the sorcery effect we cannot use this!
        if( not HasMobileEffect(self.ParentObj, "Sorcery") ) then
            EndMobileEffect(root)
            return false
        end

        self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_docility")
        self.ParentObj:PlayAnimation("cast_heal")
        CheckSkillChance(self.ParentObj, "SummoningSkill")
        skillLevel = GetSkillLevel(self.ParentObj, "SummoningSkill")
        wispCount = 0

        -- Determine how many wisps should be summoned based on SummoningSkill
        if( skillLevel <= 30 ) then
            wispCount = 1
        elseif( skillLevel <= 60 ) then
            wispCount = 2
        elseif( skillLevel <= 90 ) then
            wispCount = 3
        elseif( skillLevel == 100 ) then
            wispCount = 4
        end

        SorceryHelper.SorceryMessageSender( self.ParentObj, "SummonWisp", { Count = wispCount } )
        EndMobileEffect(root)
	end,
}