MobileEffectLibrary.SummonBeam = 
{
    OnEnterState = function(self,root,target,args)
        -- If they don't have the DLC they cannot do this!
        if not( SorceryHelper.CanUseSorceryAbilities(self.ParentObj, "SummonBeam") )then EndMobileEffect(root) return false end
        
        SorceryHelper.TryConsumeWisp( self.ParentObj, 1, function(success, wispsConsumed, doNotTriggerCooldown, increaseSummonPower, beamCooldownReduction)  
            
            if( success ) then
                self._CanPulse = true
                SetMobileMod(self.ParentObj, "Disable", "EchoBeam", true)
                self.ParentObj:PlayAnimation("cast_beam")
                self.ParentObj:PlayEffectWithArgs("WispBeamEffect", 3, "BeamLength=15")
                self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_flame_wave")
                self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_wall_of_fire", false, 0.0)
                self._BeamCooldownMultiplier = beamCooldownReduction or 1

            else -- wisp wasn't consumed
                --self.ParentObj:NpcSpeechToUser("Not enough wisps.",self.ParentObj)
                EndMobileEffect(root)
                return false
            end
        end)

        -- We need to end this effect if the Sorcery MobileEffect ends.
        RegisterEventHandler(EventType.Message, "SorceryMessage", function( message, args )
            if( message == "EndSorceryEffects" ) then
                EndMobileEffect(root)
            end
        end)

        RegisterEventHandler(EventType.Message, "CancelSpellCast", function()
            EndMobileEffect(root)
        end)

    end,
    
    OnExitState = function(self,root)
        --DebugMessage("BEAM ENDING")
        UnregisterEventHandler("", EventType.Message, "CancelSpellCast")
        self.ParentObj:PlayAnimation("idle")
        if( self._PulseCount < 6 ) then
            self.ParentObj:StopEffect("WispBeamEffect")
            self.ParentObj:StopObjectSound("event:/magic/fire/magic_fire_wall_of_fire")
        end
        CallFunctionDelayed(TimeSpan.FromMilliseconds(100), function()
            SetMobileMod(self.ParentObj, "Disable", "EchoBeam", nil)
        end)
    end,

    AiPulse = function(self,root)
        if( not self._CanPulse ) then EndMobileEffect(root) return end
        --DebugMessage("PulseCount : " .. self._PulseCount)
        self._PulseCount = self._PulseCount + 1
        if( self._PulseCount >= 6 ) then
            self.ParentObj:StopObjectSound("event:/magic/fire/magic_fire_wall_of_fire")
            
            --[[
            DebugMessage( 
                "Beam Cooldown : ",
                tostring( TimeSpan.FromSeconds( PrestigeData.Sorcery.Abilities.SummonBeam.Cooldown.TotalSeconds * self._BeamCooldownMultiplier) ),
                tostring( self._BeamCooldownMultiplier )
            )
            ]]
            
            StartPrestigePositionCooldown(self.ParentObj, "Sorcery", "SummonBeam", TimeSpan.FromSeconds( PrestigeData.Sorcery.Abilities.SummonBeam.Cooldown.TotalSeconds * self._BeamCooldownMultiplier) )
            EndMobileEffect(root)
        else
            local bounds = Box(-2, 0, 2, 4,1,100):Rotate(self.ParentObj:GetFacing()):Add(self.ParentObj:GetLoc()):Flatten()
            local mobiles = FindObjects(SearchMulti({SearchRect(bounds),SearchMobileInRange(20,true)}))
            
            for i=1, #mobiles do 

                if(  
                    SorceryHelper.CanSendWispToTargetLocation( self.ParentObj, mobiles[i]:GetLoc() ) -- has LOS?
                    and not IsDead(mobiles[i])
                    and ValidCombatTarget(self.ParentObj, mobiles[i])
                    and not ShouldCriminalProtect(self.ParentObj, mobiles[i])
                    and mobiles[i]:GetObjVar("controller") ~= self.ParentObj -- We don't want to hurt our pets!
                ) then
                    mobiles[i]:SendMessage("StartMobileEffect", "SummonBeamDamage", self.ParentObj)
                end

            end

        end

    end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(0.25)
    end,

    _PulseCount = 0,
    _BeamCooldownMultiplier = 1,
    _CanPulse = false,
}