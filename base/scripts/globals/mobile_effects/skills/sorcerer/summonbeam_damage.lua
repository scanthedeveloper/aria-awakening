MobileEffectLibrary.SummonBeamDamage = 
{
    OnEnterState = function(self,root,target,args)

        if( DateTime.Compare(DateTime.UtcNow, self._LastStacked) > 0 ) then
            self._Stacks = self._Stacks + 0.5
            self._LastStacked = DateTime.UtcNow:Add(TimeSpan.FromMilliseconds(200))
            local damageAmount = math.floor( math.min(math.max( ((GetSkillLevel( target, "SorcerySkill" ) / 100) * SorceryHelper.BeamMinMaxDamage[2]) , SorceryHelper.BeamMinMaxDamage[1]), SorceryHelper.BeamMinMaxDamage[2]) * self._Stacks )
            SorceryHelper.DealSorceryDamageToTarget( target, self.ParentObj, damageAmount )
        end
        
    end,

    OnStack = function(self,root,target,args)
        self.OnEnterState( self, root, target, args )
	end,

    AiPulse = function(self,root)
        EndMobileEffect(root)
    end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(5)
    end,

    _Stacks = 0.5,
    _LastStacked = DateTime.UtcNow:Subtract(TimeSpan.FromMilliseconds(20000)),
}