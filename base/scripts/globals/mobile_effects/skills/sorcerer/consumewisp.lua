MobileEffectLibrary.ConsumeWisp = 
{
    OnEnterState = function(self,root,target,args)
        self._IsPlayer = IsPlayerCharacter( self.ParentObj )
        if not( self._IsPlayer ) then EndMobileEffect(root) return false end

        if( args.ForceApply ) then 
            self.DoEffect(self, 50)
        else

            -- If they don't have the DLC they cannot do this!
            if not( SorceryHelper.CanUseSorceryAbilities(self.ParentObj) )then EndMobileEffect(root) return false end

            RegisterEventHandler(EventType.Message, "SorceryMessage", function( message, args )
                args = args or {}
                if( message == "ActiveWispCount" and args.MessageTag == "ConsumeWispActivate" ) then
                    local wispCount = args.WispCount or 1
                    SorceryHelper.TryConsumeWisp( self.ParentObj, wispCount, function(success, wispsConsumed)  
                        if( success ) then
                            self.DoEffect(self, nil, wispCount)
                        else
                            if( IsPlayerCharacter(self.ParentObj) ) then
                                --self.ParentObj:NpcSpeechToUser("Not enough wisps.",self.ParentObj)
                            end
                        end
                    end)
                end
            end)

            SorceryHelper.SorceryMessageSender( self.ParentObj, "GetActiveWispCount", { MessageTag = "ConsumeWispActivate" })
        end

    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "SorceryMessage")
    end,

    DoEffect = function( self, overrideHealAmount, wispCount )
        self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_docility")
        self.ParentObj:PlayAnimation("cast_heal")
        local healAmount = overrideHealAmount or ( SorceryHelper.ConsumeWispHealAmount * wispCount )
        self.ParentObj:PlayEffect("GreaterHealEffect")
        self.ParentObj:SendMessage("HealRequest", healAmount , self.ParentObj)
    end,
    
    AiPulse = function(self,root)
		EndMobileEffect(root)
	end,
    
    GetPulseFrequency = function(self,root)
		return self.PulseFrequency
    end,

    PulseFrequency = TimeSpan.FromSeconds(3),
}