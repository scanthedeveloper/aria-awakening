MobileEffectLibrary.SummonStarfall = 
{
    OnEnterState = function(self,root,target,args)
        -- If they don't have the DLC they cannot do this!
        if not( SorceryHelper.CanUseSorceryAbilities(self.ParentObj, "Starfall") )then EndMobileEffect(root) return false end
        
        self.QueueLocation = nil
        self._Message = nil

        -- We need to register getting our target back
        RegisterEventHandler(EventType.ClientTargetLocResponse, "SorcerySpellTarget", 
        function(success, location)
            if( success ~= true ) then EndMobileEffect(root) return end
            
            -- If the target isn't valid get out of here!
            if(location == nil ) then 
                self._Message = "Invalid location." 
                EndMobileEffect(root) 
                return false 
            end
            
            if( self.ParentObj:GetLoc():Distance(location) > SorceryHelper.HarmfulAbilityRange ) then
                self._Message = "Out of range." 
                EndMobileEffect(root) 
                return false 
            end

            -- Queue up our target
            self.QueueLocation = location

            -- Can we send a wisp to this target?
            if( SorceryHelper.CanSendWispToTargetLocation(self.ParentObj, self.QueueLocation) ) then

                -- Try and consume a wisp from our player
                SorceryHelper.TryConsumeWisp( self.ParentObj, 1, function(success, wispsConsumed, doNotTriggerCooldown)  
                    
                    --DebugMessage( "TryConsumeWisp : ", tostring(success), tostring(wispsConsumed), tostring(doNotTriggerCooldown)  )

                    -- Wisp consumed
                    if( success ) then
                        self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_force")
                        --Launch wisps into the air
                        for i=1,#wispsConsumed do
                            self.ParentObj:PlayAnimation("cast_heal")
                            self.ParentObj:PlayEffectWithArgs("WispLaunchEffect", 0.0, "WispIndex="..tostring(wispsConsumed[i]))
                        end

                        -- Try and send a wisp to the target
                        SorceryHelper.SendWispToTargetLocation( wispsConsumed, self.ParentObj, self.QueueLocation, 200, function(success)

                            -- THIS IS WHERE THE MAGIC HAPPENS!
                            if( success ) then
                                SorceryHelper.TriggerEchofall( self )
                            end
                            
                        end)

                        if( doNotTriggerCooldown ~= true ) then
                            StartPrestigePositionCooldown(self.ParentObj, "Sorcery", "Starfall", PrestigeData.Sorcery.Abilities.Starfall.PostCooldown )
                        end
                        
                        EndMobileEffect(root)
                    else -- wisp wasn't consumed
                        --self.ParentObj:NpcSpeechToUser("Not enough wisps.",self.ParentObj)
                        EndMobileEffect(root)
                        return false
                    end
                    
                end)

            else -- wisp cannot get to target
                self._Message = "Cannot see target."
                EndMobileEffect(root)
                return false
            end

        end)

        RegisterEventHandler(EventType.Message, "CancelSpellCast", function() EndMobileEffect(root) end)
        self.ParentObj:RequestClientTargetLoc(self.ParentObj, "SorcerySpellTarget")

    end,

    OnStack = function(self,root,target,args)
        self.ParentObj:SendClientMessage("CancelSpellCast")
        EndMobileEffect(root)
	end,
    
    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "SorcerySpellTarget")
        UnregisterEventHandler("", EventType.Message, "CancelSpellCast")
        if( self._Message ~= nil ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
    end,

    
}