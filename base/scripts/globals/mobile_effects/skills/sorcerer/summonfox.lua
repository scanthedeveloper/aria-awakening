MobileEffectLibrary.SummonFox = 
{
    OnEnterState = function(self,root,target,args)
        
        
        -- Check if we are at our maximum summons?
        local summonCount = SorceryHelper.GetSummonsCount( self.ParentObj, "monolith_fox_summon" )
        if( summonCount >= SorceryHelper.MaximumSummons["monolith_fox_summon"] ) then
            self.ParentObj:NpcSpeechToUser("Cannot summon that right now.",self.ParentObj)
            EndMobileEffect(root)
            return false
        end

        if( args.ForceApply ) then
            self.DoEffect(self, {1})
            EndMobileEffect(root)
            return false
        end



        -- If they don't have the DLC they cannot do this!
        if not( SorceryHelper.CanUseSorceryAbilities(self.ParentObj, "SummonFox") )then EndMobileEffect(root) return false end

        SorceryHelper.TryConsumeWisp( self.ParentObj, 1, function(success, wispsConsumed, doNotTriggerCooldown, increaseSummonPower)  
            
            if( success ) then
                
                self.DoEffect(self, wispsConsumed, doNotTriggerCooldown, increaseSummonPower)
                EndMobileEffect(root)

            else -- wisp wasn't consumed
                --self.ParentObj:NpcSpeechToUser("Not enough wisps.",self.ParentObj)
                EndMobileEffect(root)
                return false
            end
            
        end)

    end,
    
    OnStack = function(self,root,target,args)
        self.OnEnterState( self, root, target, args )
    end,

    DoEffect = function(self, wispsConsumed, doNotTriggerCooldown, increaseSummonPower)
        RegisterSingleEventHandler(EventType.Timer, "SummonTimer", function(spawnLoc)
            local summonCount = SorceryHelper.GetSummonsCount( self.ParentObj, "monolith_fox_summon" )
            if( summonCount < SorceryHelper.MaximumSummons["monolith_fox_summon"] ) then
                Create.AtLoc( "monolith_fox_summon", spawnLoc,function( summonObj )
                    summonObj:SetObjVar("controller", self.ParentObj)
                    summonObj:SetObjVar("IsSummonedPet", true)
                    summonObj:DelObjVar("RecentlySummoned")
                    local duration = summonObj:GetObjVar("SummonDuration") or SorceryHelper.SummonDuration
                    summonObj:SetObjVar("SummonDuration", SorceryHelper.GetSummonDuration(self.ParentObj, duration) )

                    -- We want to quickly set a target and attack it!
                    CallFunctionDelayed(TimeSpan.FromMilliseconds(40), function() 
                        local target = GetNearestCombatTarget( self.ParentObj, SorceryHelper.SummonTargetRange, self.ParentObj:GetLoc() )
                        if( target and (ShareGroup( self.ParentObj, target ) == false or ShareGroupConsent(self.ParentObj, target)) ) then
                            CallFunctionDelayed(TimeSpan.FromMilliseconds(200), function() 
                                summonObj:SendMessage("SetCurrentTarget", target)
                                CallFunctionDelayed(TimeSpan.FromMilliseconds(200), function() 
                                    summonObj:FireTimer("FSMPulse")
                                end)
                            end)
                        end
                    end)

                    -- Should the summons be increased in power?
                    if( increaseSummonPower ) then
                        summonObj:SetName( self.ParentObj:GetName() .. "'s Empowered " .. summonObj:GetName() )
                        
                        CallFunctionDelayed(TimeSpan.FromMilliseconds(200), function() 
                            SorceryHelper.SorceryMessageSender( summonObj, "EmpowerSummon", { MaxHealthTimes = 0.5, MaxAttackTimes = 0.25 })
                        end)
                        
                    else
                        summonObj:SetName( self.ParentObj:GetName() .. "'s " .. summonObj:GetName() )
                    end
                end)
            end
        end)

        self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact")
        local spawnLocation = SorceryHelper.GetSummonSpawnLocation( self.ParentObj )
        
        if( spawnLocation ) then
            self.ParentObj:PlayAnimation("cast_heal")
            self.ParentObj:PlayEffectWithArgs("WispLaunchEffect", 0.0, "WispIndex="..tostring(wispsConsumed[i]))
            PlayEffectAtLoc("WispSummonEffect", spawnLocation, 3)
            self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(0.5), "SummonTimer", spawnLocation)
        end

    end,
    
    -- We want to force the mobile effect to end
    -- even if we don't hear back from the player.
    AiPulse = function(self,root)
		EndMobileEffect(root)
    end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
    end,
    
}