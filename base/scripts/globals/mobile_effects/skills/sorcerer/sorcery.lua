MobileEffectLibrary.Sorcery = 
{
    --PersistSession = true,

    OnEnterState = function(self,root,target,args)
        --DebugMessage("SORCERY START")
        self._IsPlayer = IsPlayerCharacter( self.ParentObj )
        if not( self._IsPlayer ) then EndMobileEffect(root) return false end
        
        -- If they don't have the DLC they cannot do this!
        if not( SorceryHelper.CanUseSorceryAbilities(self.ParentObj) )then EndMobileEffect(root) return false end

        self._Wisps = self.ParentObj:GetObjVar("_Wisps") or self._Wisps

        --StartWeaponAbilityCooldown(self.ParentObj, false, WeaponAbilitiesData.SummonWisp.Cooldown)

        -- If they equipped an Orb we need to set a 10 second cooldown on "Consume Wisp"
        if( Weapon.GetType( Weapon.GetPrimary( self.ParentObj ) ) == "SorcererWand" ) then
            StartWeaponAbilityCooldown( self.ParentObj, true, TimeSpan.FromSeconds(10) )
        end
        
        AddBuffIcon(self.ParentObj, "SorceryEffect", "Sorcery", "Lightning Surge", "Can use sorcerer abilities.", true)
        
        RegisterEventHandler(EventType.Message, "SorceryMessage", function( message, args ) self.ProcessSorceryMessage( self, root, message, args ) end)
    end,

    OnExitState = function(self,root)
        --DebugMessage("SORCERY END")
        self.ParentObj:DelObjVar("Summons")
        self.ParentObj:DelObjVar("WispArmor")
        UnregisterEventHandler("", EventType.Message, "SorceryMessage")
        if( self._IsPlayer ) then
            RemoveBuffIcon(self.ParentObj, "SorceryEffect")
            self.ParentObj:SendMessage("StartMobileEffect", "SorceryCooldown")
        end

        -- Remove wisp effects
        self.RemoveAllWispEffects(self)

        -- Destroy Summons
        local summons = SorceryHelper.GetAllSummons(self.ParentObj)
        for i=1, #summons do 
            SorceryHelper.SorceryMessageSender( summons[i], "EndSorceryEffects" )
        end

        -- End all sorcery effects for player
        SorceryHelper.SorceryMessageSender( self.ParentObj, "EndSorceryEffects" )
    end,

    -- Called when the number of wisps the player has is changed
    UpdateWispEffects = function(self)

        for i=1, #self._Wisps do 
            local wisp = self._Wisps[i]
            if( wisp.Active ) then
                if( not wisp.EffectOn ) then
                    self._Wisps[i].EffectOn = true
                    local wispArg = "WispIndex="..tostring(i)
                    self.ParentObj:PlayEffectWithArgs(wisp.Effect, 0.0, wispArg)
                end
            else
                if( wisp.EffectOn ) then
                    self._Wisps[i].EffectOn = false
                    self.ParentObj:StopEffect("WispEffect"..tostring(i))
                end
            end
        end

        self.ParentObj:SetObjVar("_Wisps", self._Wisps)

    end,
    
    ProcessSorceryMessage = function( self, root, message, args )
        -- End mobile effect
        if( message == "EndMobileEffect" ) then
            EndMobileEffect(root)
            
        -- Clears the wisp effects when you change weapons
        elseif( message == "RemoveAllWisps" ) then
            self.RemoveAllWispEffects(self)

        -- Return how many active wisps we have
        elseif( message == "GetActiveWispCount" ) then
            args = args or {}
            args.MessageTag = args.MessageTag or nil
            SorceryHelper.SorceryMessageSender( self.ParentObj, "ActiveWispCount", { WispCount = self.GetActiveWisps(self), MessageTag = args.MessageTag } )
        -- Add wisp(s) to the player
        elseif( message == "SummonWisp" ) then
            args = args or {}
            self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_docility")
            self.ParentObj:PlayAnimation("cast_heal")
            CheckSkillChance(self.ParentObj, "SummoningSkill")
            local maximumWisps = SorceryHelper.GetMaximumWispsAllowed( self.ParentObj )
            local activeWisps = self.GetActiveWisps(self)
            args.Count = args.Count or 4
                
            for i=1, args.Count  do 
                    if(  activeWisps < maximumWisps ) then
                    local added = false
                    for j=1, #self._Wisps do 
                        if( not added and not self._Wisps[j].Active ) then
                            self._Wisps[j].Active = true
                            added = true
                            activeWisps = activeWisps + 1
                        end
                    end
                end
            end

            self.UpdateWispEffects(self)

        -- Use ability
        elseif( message == "ConsumeWisp" ) then
            local doNotComsumeWisp, doNotTriggerCooldown, increaseSummonPower = false

            RequestMobileMod( self.ParentObj, self.ParentObj, { "SorceryBeamCooldownReduction", "SorceryChanceToNotConsumeWisp", "SorceryChanceToNotTriggerCooldown", "SorceryChanceToSummonMorePowerful" },
                function(MobileMod)
                    local success = false
                    local wispsConsumed = {}

                    local doNotComsumeWisp = Success(GetMobileMod(MobileMod.SorceryChanceToNotConsumeWisp) / 100)
                    local doNotTriggerCooldown = Success(GetMobileMod(MobileMod.SorceryChanceToNotTriggerCooldown) / 100)
                    local increaseSummonPower = Success(GetMobileMod(MobileMod.SorceryChanceToSummonMorePowerful) / 100)
                    local beamCooldownReduction = ( 1 - (GetMobileMod(MobileMod.SorceryBeamCooldownReduction)/100))

                    if( self.GetActiveWisps(self) >= args.Count ) then
                        for i=1, args.Count do 
                            local deleted = false
                            for j=1, #self._Wisps do 
                                if( not deleted and self._Wisps[j].Active ) then
                                    if( not doNotComsumeWisp ) then
                                        self._Wisps[j].Active = false
                                    end
                                    table.insert( wispsConsumed, j )
                                    deleted = true
                                end
                            end
                        end
                        success = true
                        self.UpdateWispEffects(self)
        
                        -- Damage the orb
                        local orbObj = Weapon.GetPrimary(self.ParentObj)
                        if(Success(ServerSettings.Durability.Chance.AttunedWeapon) ) then
                            AdjustDurability(orbObj, -1)
                        end
                    end
                    
                    --DebugMessage( "SuccessChanges : ", tostring(doNotComsumeWisp), tostring(doNotTriggerCooldown), tostring(increaseSummonPower)  )
                    args.Player:SendMessage( args.MessageTag, success, wispsConsumed, doNotTriggerCooldown, increaseSummonPower, beamCooldownReduction)

                end
            )

        end
        
    end,

    -- How many wisps are active
    GetActiveWisps = function(self)
        local count = 0

        for i=1, #self._Wisps do 
            if( self._Wisps[i].Active ) then
                count = count + 1
            end
        end

        return count
    end,

    RemoveAllWispEffects = function(self)
        --Clean up any previous wisp and shield effects
        for i=1,4 do
            self.ParentObj:StopEffect("WispEffect"..tostring(i))
            self.ParentObj:StopEffect("WispShield"..tostring(i))
            self._Wisps[i].Active = false
            self._Wisps[i].EffectOn = false
        end
        self.ParentObj:DelObjVar("_Wisps")
    end,

    GetPulseFrequency = function(self,root)
		return self.PulseFrequency
    end,
    
    PulseFrequency = TimeSpan.FromSeconds(1),

    AiPulse = function(self,root)
        self._PulseCount = self._PulseCount + 1
        if( self._PulseCount % 6 == 0 ) then
            local activeWisps = self.GetActiveWisps(self)
            local maximumWisps = SorceryHelper.GetMaximumWispsAllowed( self.ParentObj )

            if( IsInCombat(self.ParentObj) and ( activeWisps < maximumWisps ) ) then
                self.ProcessSorceryMessage( self, root, "SummonWisp" )
            end
        else
            local weaponObj = Weapon.GetPrimary(self.ParentObj)
            if( weaponObj == nil or not weaponObj:HasObjVar("WispCount") ) then
                EndMobileEffect(root)
            end
        end
	end,

    _Wisps = 
    {
        { Active = false, EffectOn = false, Effect = "WispEffect" },
        { Active = false, EffectOn = false, Effect = "WispEffect" },
        { Active = false, EffectOn = false, Effect = "WispEffect" },
        { Active = false, EffectOn = false, Effect = "WispEffect" },
    },

    _PulseCount = 0

}