MobileEffectLibrary.SorceryCooldown = 
{
    OnEnterState = function(self,root,target,args)
        AddBuffIcon(self.ParentObj, "ResonatingEnergy", "Resonating Energy", "Lightning Surge", "Cannot be healed by others.", true)
    end,

    OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "ResonatingEnergy")
		end
	end,
    
    GetPulseFrequency = function(self,root)
		return self.PulseFrequency
    end,
    
    AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	PulseFrequency = TimeSpan.FromSeconds(6),
}