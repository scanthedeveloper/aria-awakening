MobileEffectLibrary.RiddleAOE = 
{
    ForceDismount = true,
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self._AreaOfEffect = args.AreaOfEffect or 5
        self._MaxPulses = args.MaxPulseCount or 1
        self._FriendOnly = args.FriendOnly or false
        self._BardEffect = args.BardEffect or nil
        self._SkillRequirement = args.SkillRequirement or 75
        self._BaseMobileModAmount = args.MobileModAmount or 0
        self.BaseChanceToLand = args.ChanceToLand or nil
        
        -- Is this a player?
        self.IsPlayer = IsPlayerCharacter(self.ParentObj)

        -- We need a songbook
        if not( HasSongBook(self.ParentObj) ) then
            self.ParentObj:SystemMessage("Songbook required.", "info")
            EndMobileEffect(root)
            return false
        end

        --SCAN ADDED (Require Insturment)
        if( self._AbilityName ~= "TricksterPsalm" ) then
            -- Does the player have a instrument equipped?; if not throw an error. 
            if( Instrument.GetEquippedInstrument(self.ParentObj) == nil ) then
                local instrument = Instrument.GetInstrumentFromBackpack(self.ParentObj, true)
                -- If we don't have an instrument to equip we end mobile effect
                if( instrument == nil ) then
                    self.ParentObj:SystemMessage("Instrument not equipped.", "info")
                    EndMobileEffect(root)
                    return false
                end
            end
        end

        -- Check our skill gain
        if(not CheckSkill(self.ParentObj, "EntertainmentSkill", self._SkillRequirement) or not CheckSkill(self.ParentObj, "MusicianshipSkill", self._SkillRequirement, nil) ) then
            self.ParentObj:SystemMessage(Instrument.GetRandomRiddleInterruptedMessage(), "info")
            EndMobileEffect(root)
            return false
        end

        Instrument.DoInstrumentAnimationAndEffect( self.ParentObj, nil, true )
        self.ParentObj:PlayEffectWithArgs(self._BardEffect,5.0,"Bone=Head")
        self.ParentObj:PlayObjectSound(self._Args.AoESFX, false)

        if( self._Args.MobileModAmount ~= nil ) then
            self._Args.MobileModAmount = self._BaseMobileModAmount * (GetSkillLevel(self.ParentObj, "EntertainmentSkill") / 100)
            if( self._Args.MobileModAmountMin ~= nil ) then
                self._Args.MobileModAmount = math.max(self._Args.MobileModAmount, self._Args.MobileModAmountMin)
            end
            self._Args.EffectBone = "Bone=Head"
        end
        
        
        if( self._Args.ChanceToLand ~= nil ) then
            self._Args.ChanceToLand = self.BaseChanceToLand * (GetSkillLevel(self.ParentObj, "EntertainmentSkill") / 100)
            --DebugMessage("Riddle AOE BASE Chance: " .. self.BaseChanceToLand)
            --DebugMessage("Riddle AOE Chance: " .. self._Args.ChanceToLand)
        end


        self._ApplyRiddle(self, root)
        self._Args.MobileModAmount = self._BaseMobileModAmount
        self._Args.ChanceToLand = self.BaseChanceToLand
        EndMobileEffect(root)
        return false

    end,

    OnExitState = function(self,root)
        self._CleanUp(self, root)
    end,

    _CleanUp = function(self, root)
        Instrument.DoStopInstrumentAnimationAndEffect(self.ParentObj, nil)
    end,

    _ApplyRiddle = function(self, root)
        if( self.IsPlayer ) then
            local targets = {}

            if( self._Args.CreatureOnly ) then
                targets = GetNearbyCreatures( self.ParentObj, self._AreaOfEffect )
            else
                -- Gets nearby players (friendly/enemny) based on self._FriendOnly flag
                targets = GetNearbyPlayers( self.ParentObj, self._AreaOfEffect, self._FriendOnly, true )
            end
            
            -- For each target we need to start the mobile effect
            for i=1,#targets do
                local applyToTarget = true
    
                -- Does this have a %chance to land; if so see if it landed.
                if( self._Args.ChanceToLand ) then
                    applyToTarget = Success((self._Args.ChanceToLand/100))
                end

                if( applyToTarget 
                    and self.ParentObj:HasLineOfSightToObj(targets[i],ServerSettings.Combat.LOSEyeLevel) -- can we see it ?
                    and not ShouldCriminalProtect(self.ParentObj, targets[i], false, true) -- will karma let us attack it?
                    and (
                        not AllowFriendlyActions( self.ParentObj, targets[i] ) -- not friendly
                    or
                        GetConflictRelation(self.ParentObj,targets[i] )
                    )
                 ) then
                    targets[i]:SendMessage("StartMobileEffect", self._Args.MobileEffect, targets[i], self._Args)
                end
                
            end
        end
    end,

    _MobileModAmount = nil,
    _AreaOfEffect = nil,
    _FriendOnly = false,
}
