MobileEffectLibrary.ResoundingEchoes = 
{
    -- PreventEffects = { "ResoundingEchoes" },

    OnEnterState = function(self,root,target,args)
        
        if( self.ParentObj:IsPlayer() ) then
            AddBuffIcon(
                self.ParentObj, 
                "ResoundingEchoes", 
                "Resounding Echos", 
                "Natures Grace", 
                "The echoes stir your feet. Your movement cannot be impaired.", 
                true
            )
        end

        self.ParentObj:SystemMessage("Resounding echoes stir your feet.","info")
        ClearMovementImpairmentEffects( self.ParentObj )
        self.ParentObj:PlayEffectWithArgs("AfterburnEffect",10.0,"Bone=Ground")
    end,

    OnExitState = function(self,root)
        self.ParentObj:SystemMessage("The echoes fade.","info")
        self.ParentObj:StopEffect("AfterburnEffect")
        RemoveBuffIcon(self.ParentObj, "ResoundingEchoes")
    end,
    
    GetPulseFrequency = function(self,root)
		return self.Duration
    end,
    
    AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromSeconds(10),
    
}