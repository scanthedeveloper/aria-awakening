MobileEffectLibrary.ResoundingDrums = 
{
    -- PreventEffects = { "ResoundingDrums" },

    OnEnterState = function(self,root,target,args)
        
        if( self.ParentObj:IsPlayer() ) then
            AddBuffIcon(
                self.ParentObj, 
                "ResoundingDrums", 
                "Resounding Drums", 
                "Natures Grace", 
                "The drums vitalize you. You cannot be bleed, mortal struck, or poisoned.", 
                true
            )
        end

        self.ParentObj:SystemMessage("Resounding drums vitalize you.","info")
        ClearBleedEffects( self.ParentObj )
        ClearMortalEffects( self.ParentObj )
        ClearPoisonEffects( self.ParentObj )
        self.ParentObj:PlayEffectWithArgs("AfterburnEffect",10.0,"Bone=Ground")
    end,

    OnExitState = function(self,root)
        self.ParentObj:SystemMessage("The drums fade.","info")
        self.ParentObj:StopEffect("AfterburnEffect")
        RemoveBuffIcon(self.ParentObj, "ResoundingDrums")
    end,
    
    GetPulseFrequency = function(self,root)
		return self.Duration
    end,
    
    AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromSeconds(10),
    
}