MobileEffectLibrary.Play = 
{
    EndOnMovement = true,
    EndOnAction = true,
    IgnoreEndOnActionTypes = {"Solo"},
    ForceDismount = true,

	OnEnterState = function(self,root,target,args)

        self._InstrumentEquipped = Instrument.GetInstrumentParameterName(self.ParentObj)

        -- Is this the player that initiated the "Play" ability
        self._Player = IsPlayerCharacter(self.ParentObj)

        -- If this is not a player; full stop!
        if( self._Player == false ) then
            EndMobileEffect(root)
			return false
        end

        -- Does the player have a instrument equipped?; if not throw an error. 
        local equipInstrument = self.ParentObj:GetEquippedObject("RightHand")
        local instrumentToolType = Weapon.GetToolType(equipInstrument)
        local instrumentType = Weapon.GetType(equipInstrument)
        if( instrumentToolType ~= "Instrument" ) then
            self._Message = "Instrument not equipped."
            EndMobileEffect(root)
            return false
        end

        -- End Combat
        self.ParentObj:SendMessage("EndCombatMessage")

        RegisterEventHandler(EventType.UserLogout,"", 
            function ( ... )
                EndMobileEffect(root)
            end)

        RegisterEventHandler(EventType.LoadedFromBackup,"", 
            function ( ... )
                EndMobileEffect(root)
            end)

        RegisterEventHandler(EventType.UserLogin,"", 
            function ( ... )
                EndMobileEffect(root)
            end)

        -- Register event to set the self._BandState table
        RegisterEventHandler(EventType.Message, "SetBandState", function( bandState )
            self._BandState = bandState
            self.ParentObj:SetObjVar("BandID", self._BandState.bandId)
            Instrument.SyncWithBand(self.ParentObj, self._BandState)
        end)

        RegisterEventHandler(EventType.Message, "BandLeaderResponse", function( bandState, leader )
            self._BandState = bandState
            self.ParentObj:SetObjVar("BandID", self._BandState.bandId)
            self._Leader = leader
            Instrument.SyncWithBand(self.ParentObj, self._BandState)
        end)

        -- Register event to send the self._BandState
        RegisterEventHandler(EventType.Message, "SendListenerRequest", function( requester )
            if self._Leader~=nil then
                if self._Leader==self.ParentObj then
                    requester:SendMessage("ReplyToListener", self._BandState, self._Orchestra.InstrumentTable)
                else
                    self._Leader:SendMessage("SendListenerRequest", requester)
                end
            end
        end)

        -- Register event to send the self._BandState
        RegisterEventHandler(EventType.Message, "RequestListenerUpdate", function( requester )
            if self._Leader~=nil then
                if self._Leader==self.ParentObj then
                    requester:SendMessage("UpdateListener", self._Orchestra.InstrumentTable)
                else
                    self._Leader:SendMessage("RequestListenerUpdate", requester)
                end
            end
        end)

        -- Register event to send the self._BandState
        RegisterEventHandler(EventType.Message, "DetachListener", function( requester )
            if self._Leader~=nil then
                if self._Leader==self.ParentObj then
                    requester:StopObjectSound(self._BandState.song.EventName, false, 0.5)
                else
                    self._Leader:SendMessage("DetachListener", requester)
                end
            end
        end)

        -- Register event to send the self._BandState
        RegisterEventHandler(EventType.Message, "GetBandState", function( requester )
            requester:SendMessage("SetBandState", self._BandState)
        end)


        RegisterEventHandler(EventType.Message, "BandJoinRequest", function( instrument, requester )
            if instrument~=nil then
                if self._Orchestra.Instruments[instrument]==nil then
                    self._Orchestra.Instruments[instrument] = 0
                end
                self._Orchestra.Instruments[instrument] = self._Orchestra.Instruments[instrument] + 1
                if requester ~= nil then
                    table.insert(self._Orchestra.Players, requester)
                end
                if self._Orchestra.Instruments[instrument] == 1 then
                    local _Parameters = {}
                    _Parameters[instrument] = 1
                    self._Orchestra.InstrumentTable[instrument] = 1
                end
                for i,j in pairs(self._Orchestra.Players) do
                    j:UpdateObjectSound(self._BandState.song.EventName, self._Orchestra.InstrumentTable, false, 0.0, false, false)
                end
                self.ParentObj:UpdateObjectSound(self._BandState.song.EventName, self._Orchestra.InstrumentTable, false, 0.0, false, false)
            end
            requester:SendMessage("BandLeaderResponse", self._BandState, self.ParentObj)
        end)

        RegisterEventHandler(EventType.Message, "DrumMe", function()
            self._Orchestra.InstrumentTable["drum"] = 1
        end)

        RegisterEventHandler(EventType.Message, "BandLeft", function( instrument, requester  )
            if instrument~=nil then
                if self._Orchestra.Instruments[instrument]==nil then
                    self._Orchestra.Instruments[instrument] = 0
                end
                self._Orchestra.Instruments[instrument] = self._Orchestra.Instruments[instrument] - 1
                if requester ~= nil then
                    requester:StopObjectSound(self._BandState.song.EventName, false, 0.5)
                    for i,j in pairs(self._Orchestra.Players) do
                        if j == requester then
                            table.remove(self._Orchestra.Players,i)
                        end
                    end
                end
                if self._Orchestra.Instruments[instrument] == 0 then
                    local _Parameters = {}
                    _Parameters[instrument] = 0
                    self._Orchestra.InstrumentTable[instrument] = 0
                end
                for i,j in pairs(self._Orchestra.Players) do
                    j:UpdateObjectSound(self._BandState.song.EventName, self._Orchestra.InstrumentTable, false, 0.0, false, false)
                end
                self.ParentObj:UpdateObjectSound(self._BandState.song.EventName, self._Orchestra.InstrumentTable, false, 0.0, false, false)
            end
        end)

        RegisterEventHandler(EventType.Message, "BandDisbanded", function()
            EndMobileEffect(root)
        end)

        -- Join a band or play solo
        local bandLeader = Instrument.IsBandLeaderPlaying(self.ParentObj)
        if( bandLeader ) then
            -- Request the band state from band member
            bandLeader:SendMessage( "BandJoinRequest", self._InstrumentEquipped, self.ParentObj )
        else
            -- Set our own band state
            self._BandState = Instrument.PlayInstrument(self.ParentObj, args)
            self._BandState.hearth = Instrument.GetNearbyHearth(self.ParentObj) or Instrument.GetNearbyCampfire(self.ParentObj)
            
            -- Set playing
            self.ParentObj:SetObjVar("BandID", self._BandState.bandId)
            self.ParentObj:SetObjVar("BandLeader", true)
            self._Orchestra.Instruments = self._BandState.song.Instruments
            if self._InstrumentEquipped~=nil then
                self._Orchestra.InstrumentTable[self._InstrumentEquipped] = 1
            end
            self._Orchestra.Instruments = self._Orchestra.InstrumentTable
            self._Leader = self.ParentObj
            self.ParentObj:StopObjectSound(self._BandState.song.EventName, false, 0.5)
            self.ParentObj:UpdateObjectSound(self._BandState.song.EventName, self._Orchestra.InstrumentTable, false, 0.0, false, false)
        end

        -- Add buff
        if( self.ParentObj:IsPlayer() ) then
            AddBuffIcon(
                self.ParentObj, 
                "PlayingSong", 
                "Playing Song", 
                "Natures Grace", 
                "You are playing a beautiful melody", 
                true
            )
        end
    end,
    
    CleanUp = function( parentObj )
        parentObj:DelObjVar("BandID")
        parentObj:DelObjVar("BandLeader")
    end,

    OnExitState = function(self,root)

    if self._Leader~=nil then
        if self._Leader==self.ParentObj then
            for i,j in pairs(self._Orchestra.Players) do
                j:StopObjectSound(self._BandState.song.EventName, false, 0.5)
                j:SendMessage("BandDisbanded")
            end
            self.ParentObj:StopObjectSound(self._BandState.song.EventName, false, 0.5)
            self._Leader:DelObjVar("BandLeader")
        else
            self._Leader:SendMessage("BandLeft", self._InstrumentEquipped, self.ParentObj )
        end
    end

        -- Remove objVar
        self.ParentObj:DelObjVar("BandID")

        -- This lets MobileEffectLibrary.Solo to occur in the same frame
        self.ParentObj:ScheduleTimerDelay(TimeSpan.FromMilliseconds(20), "StoppedPlayingInstrument", self._BandState)

        -- Unregister from events
        UnregisterEventHandler("", EventType.Message, "SetBandState")
        UnregisterEventHandler("", EventType.Message, "GetBandState")
        UnregisterEventHandler("", EventType.Message, "SendListenerRequest")
        UnregisterEventHandler("", EventType.Message, "RequestListenerUpdate")
        UnregisterEventHandler("", EventType.Message, "DetachListener")
        UnregisterEventHandler("", EventType.Message, "BandJoinRequest")
        UnregisterEventHandler("", EventType.Message, "BandLeaderResponse")
        UnregisterEventHandler("", EventType.Message, "BandLeft")
        UnregisterEventHandler("", EventType.Message, "BandDisbanded")


        -- Trigger the stop playing instrument events
        Instrument.StopPlayInstrument(self.ParentObj)

        -- Remove our playing buff icon
        RemoveBuffIcon(self.ParentObj, "PlayingSong")
        
        -- We moved or tried to complete an action
        if( self._MovementFiredExit or self._ActionFiredExit ) then
            self._Message = Instrument.GetRandomSongInterruptedMessage()
            --self.ParentObj:SystemMessage(self._Message, "info")
        end
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

    AiPulse = function(self,root)
        -- We only want to process skill gains when a pulse fires which can be 8,7,6 seconds
        if( self._PulseTracker % self.Pulse == 0 or self._PulseTracker == 0 ) then
            
            -- Get the count of listeners to drive skill gain
            local listenerCount = Instrument.PlayerHasListeners(self.ParentObj)

            Instrument.DoInstrumentAnimationAndEffect( self.ParentObj, nil )

            -- If we have listeners we can gain skill
            if( listenerCount ) then
                --DebugMessage("Listening")
                self._Succeeded = CheckSkillChance(self.ParentObj, "MusicianshipSkill")
            end
            
        end

        -- We need to make sure our pulse rate is up to date; we subtract one for current player instrument
        self.Pulse = (self.StartPulse - (Instrument.GetBandUniqueInstrumentCount(self.ParentObj) - 1))

        -- iterate our pulse tracker
        self._PulseTracker = self._PulseTracker + 1
        --DebugMessage("Current: " .. self._PulseTracker)

        -- Gain vitality
        if self.VitalityMax == false and (GetCurVitality(self.ParentObj) >= GetMaxVitality(self.ParentObj)) then
			self.VitalityMax = true
            self.ParentObj:SystemMessage("You are well rested.","info")
		elseif (GetCurVitality(self.ParentObj) < GetMaxVitality(self.ParentObj)) then
            local selfVitRate = math.floor( GetSkillLevel(self.ParentObj, "MusicianshipSkill") / 10) * 0.1
            
            SafeAdjustCurVitality(selfVitRate, self.ParentObj)
		end

    end,

    PulseFrequency = TimeSpan.FromSeconds(1),
    Pulse = 6,
    StartPulse = 6,
    
    _Orchestra = {
        Players = {},
        Instruments = {},
        InstrumentTable = {}
    },
    _InstrumentEquipped = nil,
    _PulseTracker = 0,
    _Message = "",
    _SkillCheck = false,
    _Leader = nil,
    _BandState = {
        bandId =  nil, -- uuid
        song = nil, -- LuaTable
        startedPlaying = nil, -- ServerTimeSecs()
        hearth = nil, -- nearby hearth
    },
    _RegenRate = ServerSettings.Vitality.AdjustOnHearthNearPulse,
    VitalityMax = false,
}