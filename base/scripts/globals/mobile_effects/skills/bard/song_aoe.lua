MobileEffectLibrary.SongAOE = 
{
    ForceDismount = true,
    EndOnAction = true,
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self._AreaOfEffect = args.AreaOfEffect or 5
        self._MaxPulses = args.MaxPulseCount or 1
        self._FriendOnly = args.FriendOnly or false
        self._BardEffect = args.BardEffect or nil
        self._SkillRequirement = args.SkillRequirement or 75
        self._Args._BuffDescription = args._BuffDescription or ""
        self._BaseMobileModAmount = args.MobileModAmount or 0
        self._AbilityName, class = GetPrestigeAbilityNameClass(self.ParentObj, 1)


        if( self._AbilityName ~= "TricksterPsalm" ) then
            -- Does the player have a instrument equipped?; if not throw an error. 
            if( Instrument.GetEquippedInstrument(self.ParentObj) == nil ) then
                local instrument = Instrument.GetInstrumentFromBackpack(self.ParentObj, true)
                -- If we don't have an instrument to equip we end mobile effect
                if( instrument == nil ) then
                    self.ParentObj:SystemMessage("Instrument not equipped.", "info")
                    EndMobileEffect(root)
                    return false
                end
            end
        end

        

        -- We need a songbook
        if not( HasSongBook(self.ParentObj) ) then
            self.ParentObj:SystemMessage("Songbook required.", "info")
            EndMobileEffect(root)
            return false
        end

        -- Check our skill gain
        if (not CheckSkill(self.ParentObj, "EntertainmentSkill", self._SkillRequirement) or not CheckSkill(self.ParentObj, "MusicianshipSkill", self._SkillRequirement, nil) ) then
            self.ParentObj:SystemMessage(Instrument.GetRandomSongInterruptedMessage(), "info")
            EndMobileEffect(root)
            return false
        end

        if( self._AbilityName ~= "TricksterFooting" ) then
            -- Slow the player
            SetMobileMod(self.ParentObj, "MoveSpeedTimes", "BardSongPlaying", PrestigeData.Barding.SongMovementSpeed) -- 70% reduction
        end
        
        -- Is this a player?
        self.IsPlayer = IsPlayerCharacter(self.ParentObj)

        -- Clear conflict
        self.ParentObj:SendMessage("EndCombatMessage")

        -- Do animation
        self.ParentObj:PlayEffect(self._BardEffect)
        Instrument.DoInstrumentAnimationAndEffect( self.ParentObj, nil, self._Stacked )
        self.ParentObj:PlayObjectSound(self._Args.SongSFX, false)
        self._ApplySong(self, root)

    end,
    
    -- OnStack we want to set the pulse count back to 0, but clean-up
    -- so we don't duplicate the buff icon.
    OnStack = function(self,root,target,args)
        self._CleanUp(self,root)
        self._Pulses = 0
        -- Used to make sure we don't replay the animation in OnEnterState()
        self._Stacked = true
        self.OnEnterState(self,root,target,args)
    end,

    OnExitState = function(self,root)
        SetMobileMod(self.ParentObj, "MoveSpeedTimes", "BardSongPlaying", nil)
        -- Just stop the instrument playing animation, _CleanUp will get the effect
        Instrument.DoStopInstrumentAnimationAndEffect(self.ParentObj, nil)
        self.ParentObj:StopObjectSound(self._Args.SongSFX, false, 1.0)
        self._CleanUp(self, root)
    end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
	end,

    AiPulse = function(self,root) 

        -- All of this needs to be moved into INSTRUMENT HELPER
        -- Does the player have a instrument equipped?; if not throw an error. 
        if( self._AbilityName ~= "TricksterPsalm" and  Instrument.GetEquippedInstrument(self.ParentObj) == nil ) then
            self.ParentObj:SystemMessage("Instrument not equipped.", "info")
            EndMobileEffect(root)
            return
        end

        if( self._Pulses % self._Args.PulseDuration == 0 ) then

            if( self._CompletedPulses >= self._MaxPulses ) then
                EndMobileEffect(root)
                return
            end

            -- We only need to show the animation since the effect was triggered already
            --Instrument.DoInstrumentAnimationAndEffect( self.ParentObj, nil )
            -- We are adjusting the amount based on the bard entertainment skill
            self._Args.MobileModAmount = self._Args.MobileModAmount * (GetSkillLevel(self.ParentObj, "EntertainmentSkill") / 100)

            -- If we have the passive "Trickster's Psalm" and we don't have an instrument equipped we need to reduce the song effect by 50%
            if( self._AbilityName == "TricksterPsalm" and Instrument.GetEquippedInstrument(self.ParentObj) == nil ) then
                self._Args.MobileModAmount = math.floor( self._Args.MobileModAmount / 2 )
            end

            self._ApplySong(self, root)
            self._Args.MobileModAmount = self._BaseMobileModAmount
            self._CompletedPulses =  self._CompletedPulses + 1
        end

        self._Pulses = self._Pulses + 1        
    end,

    _CleanUp = function(self, root)
        self.ParentObj:StopEffect(self._BardEffect, 4)
        Instrument.DoStopInstrumentAnimationAndEffect(self.ParentObj, nil, true)
        if( self._Args.BuffIcon ~= nil and self.IsPlayer ) then
            --RemoveBuffIcon(self.ParentObj, self._Args.MobileModID)
        end
    end,

    _ApplySong = function(self, root)
        if( self.IsPlayer and self.ParentObj:HasObjVar("Group") ) then
            -- Get group members
            local members = GetGroupVar(GetGroupId(self.ParentObj), "Members")
            
            -- For each player we need to start the mobile effect on them
            for i=1,#members do
                ForeachMobileAndPet( members[i], function( mobile ) 
                    -- AllowFriendlyActions() is broke atm, this is hackery -bphelps
                    if(
                        AllowFriendlyActions( self.ParentObj, mobile) -- Is the mobile friendly?
                        and (self._Args.MobileModID ~= "SongOfThew" or SorceryHelper.CanTargetBeHealed( self.ParentObj, mobile ) ) -- It is not the healing song, or the target can be healed.
                    )then
                        self._ApplyMobileEffect(mobile,self)
                    end
                end)
            end
        else
            self.ParentObj:SendMessage("StartMobileEffect", "ApplyMobileMod", self.ParentObj, self._Args)
            ForeachActivePet(self.ParentObj, function(pet)
                self._ApplyMobileEffect(pet,self)
            end)    
        end
    end,

    _ApplyMobileEffect = function(mobile,self)
        if( self.ParentObj:DistanceFrom(mobile) <= self._AreaOfEffect and self.ParentObj:HasLineOfSightToObj(mobile,ServerSettings.Combat.LOSEyeLevel) ) then
            mobile:SendMessage("StartMobileEffect", "ApplyMobileMod", mobile, self._Args)
            ForeachConflict( mobile, function( conflictObj )  
                if( conflictObj:IsValid() and not IsDead(conflictObj) and conflictObj:HasObjVar("MobileTeamType") ) then
                    conflictObj:SendMessage("AddAggro", self.ParentObj, math.floor( self._BaseMobileModAmount * 2 ))
                end
            end)
        end
    end,

    _MobileModAmount = nil,
    _AreaOfEffect = nil,
    _FriendOnly = false,
    _Pulses = 0,
    _MaxPulses = 1,
    _CompletedPulses = 0,
    _Stacked = false,
    _AbilityName = nil,
}
