-- Accessed from "Listen to Music" context menu option; 
-- on players who are actively playing an instrument. (engine_callbacks.lua)

MobileEffectLibrary.ListenToBand = 
{
    OnEnterState = function(self,root,target,args)

    RegisterEventHandler(EventType.UserLogout,"", 
            function ( ... )
                EndMobileEffect(root)
            end)

        RegisterEventHandler(EventType.UserLogin,"", 
            function ( ... )
                EndMobileEffect(root)
            end)

        RegisterEventHandler(EventType.LoadedFromBackup,"", 
            function ( ... )
                EndMobileEffect(root)
            end)

        --DebugMessage("Listening")

        -- Is this the player that initiated the "Play" ability
        self._Player = self.ParentObj:IsPlayer()

        -- If this is not a player; full stop!
        if( self._Player == false ) then
            EndMobileEffect(root)
			return false
        end

        -- Set target
        self._Target = target

        -- If we are not listening to a player; or the player is more than 15 (MaxRange) yards away; full stop!
        if( target:IsPlayer() == false or self.ParentObj:DistanceFrom(target) > ServerSettings.Instrument.MaximumListenRange ) then
            self._Message = "Invalid target."
            EndMobileEffect(root)
			return false
        end

        -- Register event to set the self._BandState table
        RegisterEventHandler(EventType.Message, "ReplyToListener", function( bandState, InstrumentTable )
            
            self._BandState = bandState
            
            -- Initate the listen state; if it fails return an error.
            if not(Instrument.StartListening( self._BandState )) then
                self._Message = "You must be closer to listen."
                EndMobileEffect(root)
                return false
            else
                self.ParentObj:SystemMessage("Listening to song " .. self._BandState.song.Name, "info")
            end

            -- Set flag for band to drive skill from
            self.ParentObj:SetObjVar("ListeningToBandID", self._BandState.bandId)

            self._SongListenedTo = self._BandState.song.EventName
                self.ParentObj:StopObjectSound(self._SongListenedTo, false, 0.5)
            self.ParentObj:PlayObjectSoundWithParameter(self._SongListenedTo, InstrumentTable, false, 0.0, false, false)

            -- Buff icon
            if ( self._Player ) then
                AddBuffIcon(
                    self.ParentObj, 
                    "ListeningSong", 
                    "Listening to Song", 
                    "Natures Grace", 
                    "You are listening to a beautiful melody", 
                    true
                )
            end

        end)
        
        -- Look at band
       	LookAt(self.ParentObj, self._Target)

        RegisterEventHandler(EventType.Message, "UpdateListener", function( InstrumentTable )
            self.ParentObj:UpdateObjectSound(self._SongListenedTo, InstrumentTable, false, 0.0, false, false)
        end)

        -- Request the bandState table from our target
        target:SendMessage("SendListenerRequest", self.ParentObj)

        if( self._BandState.hearth ~= nil and self._BandState.hearth:IsValid() ) then
            StartMobileEffect(self.ParentObj, "VitalityHearth", target)
        end

    end,
    
    CleanUp = function( parentObj )
        parentObj:DelObjVar("ListeningToBandID")
    end,

    OnExitState = function(self,root)

        -- Remove the objVar
        self.ParentObj:DelObjVar("ListeningToBandID")

        -- Unregister events
        UnregisterEventHandler("", EventType.Message, "ReplyToListener")
        UnregisterEventHandler("", EventType.Message, "UpdateListener")

        -- Remove buff
        RemoveBuffIcon(self.ParentObj, "ListeningSong")

		-- Set idle
        self.ParentObj:PlayAnimation("idle")
        -- Stop listening
        Instrument.StopListening( self._BandState ) 

        -- Display message
        self.ParentObj:SystemMessage(self._Message, "info")

        if self._SongListenedTo ~= nil then
            self.ParentObj:StopObjectSound(self._SongListenedTo, false, 0.5)
        end

    end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

    AiPulse = function(self,root)
        self._PulseCount = self._PulseCount + 1
        
        -- The hearth or campfire disappeared
        if( self._BandState.hearth ~= nil and not self._BandState.hearth:IsValid() ) then
            EndMobileEffect(root)
            return false
        end

        -- End if playing
        if ( HasMobileEffect(self.ParentObj, "Play") ) then
            EndMobileEffect(root)
            return false
        end

        -- Every 6 second we want to change the dance
        if( self._PulseCount % 6 == 0 and self._IsDancing ) then
            self._IsDancing = false
        end

        -- We continuely update our listening state or end the mobile effect.
        if (not self.ParentObj:HasObjVar("ListeningToBandID") ) or (not Instrument.StartListening(self._BandState) ) then
            self._Message = "The music fades."
            EndMobileEffect(root)
			return false
        elseif not ( self._IsDancing ) then
            self.ParentObj:PlayAnimation(Instrument.GetRandomDanceAnimation())
            self._IsDancing = true
        end
        if (self._Target ~= nil) then
            self._Target:SendMessage("RequestListenerUpdate", self.ParentObj)
        end

	end,

    PulseFrequency = TimeSpan.FromSeconds(2),
	_PulseCount = 0,
    _UpdateBand = true,
    _Target = nil,
    _BandState = {},
    _IsDancing = false,
    _Message = "The music fades.",
    _SongListenedTo = nil,
    _InstrumentTable = {},
}