MobileEffectLibrary.Solo = 
{
    ForceDismount = true,
	OnEnterState = function(self,root,target,args)
        
        -- Is this the player that initiated the "Play" ability
        self._Player = self.ParentObj:IsPlayer()

        -- If this is not a player; full stop!
        if( self._Player == false ) then
            EndMobileEffect(root)
			return false
        end

        if not(HasMobileEffect(self.ParentObj, "Play")) then
            self.ParentObj:SendMessage("StartMobileEffect", "Play")
        end

        StartWeaponAbilityCooldown( self.ParentObj, true, WeaponAbilitiesData.Play.Cooldown )

        -- Get our flurry skill check
        self._SkillCheck =  CheckSkillChance(self.ParentObj, "MusicianshipSkill")
        
        local myListeners = Instrument.PlayerHasListeners(self.ParentObj, true)
        local hearth = Instrument.GetNearbyHearth(self.ParentObj)

        if(  self._SkillCheck ) then
            --##TODO: Play success animation
            self._Message = Instrument.GetRandomSongFlurrySuccessMessage()
            self.ParentObj:PlayEffect("MusicNoteIntensity3Effect")
            -- We have listeners and are close to a hearth
            if( myListeners ~= false and hearth ~= nil ) then
                for i=1, #myListeners do 
                    Instrument.GiveSoloVitalityBonus(myListeners[i])
                end
            end

        else
            --##TODO: Play failure animation
            self._Message = Instrument.GetRandomSongFlurryFailureMessage()
            self.ParentObj:PlayEffect("MusicNoteIntensity2Effect")
        end

        self.ParentObj:SystemMessage(self._Message, "info")
        
	end,

    OnExitState = function(self,root)  
        -- Send message
        self.ParentObj:StopEffect("MusicNoteIntensity3Effect")
        self.ParentObj:StopEffect("MusicNoteIntensity2Effect")
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

    AiPulse = function(self,root)
        EndMobileEffect(root)
	end,

	PulseFrequency = TimeSpan.FromSeconds(3),

    _Message = "",
    _SkillCheck = false,
    _BandState = {},
}