MobileEffectLibrary.RiddleCurrentTarget = 
{
    ForceDismount = true,
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self._AreaOfEffect = args.AreaOfEffect or 5
        self._MaxPulses = args.DurationInSeconds or 1
        self._BardEffect = args.BardEffect or nil
        self._SkillRequirement = args.SkillRequirement or 75
        self._BaseMobileModAmount = args.MobileModAmount or 0
        target = self.ParentObj:GetObjVar("CurrentTarget") or nil

        -- We need a CurrentTarget in range
        if ( target == nil ) then
            self._Message = "Need to be fighting."
            EndMobileEffect(root)
            return false
        else
            if( tonumber( self.ParentObj:DistanceFrom( target ) ) > (self._Args.Range or 5) ) then
                self._Message = "Your enemy is not in range."
                EndMobileEffect(root)
                return false
            end
        end

        -- We need a songbook
        if not( HasSongBook(self.ParentObj) ) then
            self.ParentObj:SystemMessage("Songbook required.", "info")
            EndMobileEffect(root)
            return false
        end

        -- Need line of sight
        if not ( self.ParentObj:HasLineOfSightToObj(target,ServerSettings.Combat.LOSEyeLevel) ) then
            self._Message ="Cannot See Target."
            EndMobileEffect(root)
            return false
        end

        -- Check our skill gain
        if(not CheckSkill(self.ParentObj, "EntertainmentSkill", self._SkillRequirement) or not CheckSkill(self.ParentObj, "MusicianshipSkill", self._SkillRequirement, nil) ) then
            self.ParentObj:SystemMessage(Instrument.GetRandomRiddleInterruptedMessage(), "info")
            EndMobileEffect(root)
            return false
        end

        -- Is this a player?
        self.IsPlayer = IsPlayerCharacter(self.ParentObj)

        if( self._Args.BuffIcon ~= nil and self.IsPlayer ) then
            --DebugMessage("Add Buff Icon")
            AddBuffIcon(self.ParentObj, self._Args.MobileModID, "Combat Riddle", self._Args.BuffIcon, self._Args.BuffDescription, false)
        end

        self._Args.EffectBone = "Bone=Head,Target=Head"

        local overrideRate = 8
        local projectileDist = target:GetLoc():Distance(self.ParentObj:GetLoc())
        local projectileTimer = (projectileDist - .5) / overrideRate  
        self.ParentObj:PlayProjectileEffectToWithSound(self._BardEffect, target, self._Args.ProjSFX, overrideRate, math.max(3,projectileTimer), self._Args.EffectBone)

        Instrument.DoInstrumentAnimationAndEffect( self.ParentObj, nil, true )
    end,

    OnStack = function(self,root,target,args)
        self._LastTarget = nil
        self._RemoveRiddle(self,root,true)
        self._CleanUp(self,root)
        self._Pulses = 0
        self.OnEnterState(self,root,target,args)
    end,

    OnExitState = function(self,root)
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end

        --DebugMessage( "End Mobile Effect" )

        -- Remove riddle effects from targets
        self._RemoveRiddle(self,root,true)
        -- Remove mobile effects from bard
        self._CleanUp(self, root)
    end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
	end,

    AiPulse = function(self,root)

        -- Has our effect ran out of time?
        if( self._MaxPulses <= self._Pulses ) then
            --self.ParentObj:SystemMessage(Instrument.GetRandomSongEndedMessage(), "info")
            EndMobileEffect(root)
            return
        end


        -- We do skill checks every AI pulse
        
        if( 
            CheckSkill(self.ParentObj, "EntertainmentSkill", self._SkillRequirement) 
           and CheckSkill(self.ParentObj, "MusicianshipSkill", self._SkillRequirement, nil)
         ) then
            
            --DebugMessage( self._BaseMobileModAmount .." * ".. GetSkillLevel(self.ParentObj, "EntertainmentSkill") .." / ".. 100 )
            self._Args.MobileModAmount = self._BaseMobileModAmount * (GetSkillLevel(self.ParentObj, "EntertainmentSkill") / 100)
            if( self._Args.ForceWholeNumber ) then
                self._Args.MobileModAmount = math.floor(self._Args.MobileModAmount)
            end

            self._CurrentTarget = self.ParentObj:GetObjVar("CurrentTarget")
            self._RemoveRiddle(self,root)
            self._ApplyRiddle(self, root)
            self._Args.MobileModAmount = self._BaseMobileModAmount
         end

        self._Pulses = self._Pulses + 1
    end,

    _CleanUp = function(self, root)
        Instrument.DoStopInstrumentAnimationAndEffect(self.ParentObj, nil)
        if( self._Args.BuffIcon ~= nil and self.IsPlayer ) then
            RemoveBuffIcon(self.ParentObj, self._Args.MobileModID)
        end
    end,

    _ApplyRiddle = function(self, root)
        if( self.IsPlayer and self._CurrentTarget and (self._CurrentTarget ~= self._LastTarget) and self.ParentObj:HasLineOfSightToObj(self._CurrentTarget,ServerSettings.Combat.LOSEyeLevel) ) then

            if( 
                self.ParentObj:HasLineOfSightToObj(self._CurrentTarget,ServerSettings.Combat.LOSEyeLevel) -- can we see it ?
                and not ShouldCriminalProtect(self.ParentObj, self._CurrentTarget) -- will karma let us attack it?
                --and not AllowFriendlyActions( self.ParentObj, self._CurrentTarget ) -- not friendly
             ) then

                if( self._Args.TargetEffect ) then
                    self._CurrentTarget:PlayEffectWithArgs(self._Args.TargetEffect,0.0,"Bone=Head")
                end
                SetMobileMod(self._CurrentTarget, self._Args.MobileMod, self._Args.MobileModID, self._Args.MobileModAmount)
                self._LastTarget = self._CurrentTarget
                table.insert(self._Targets, self._CurrentTarget)

            end

        end
    end,

    _RemoveRiddle = function(self, root, endEffect)        
        for i=1, #self._Targets do
            if( self._CurrentTarget ~= self._Targets[i] or IsDead(self._Targets[i]) or endEffect ) then

                if( self._Args.TargetEffect ) then
                    self._Targets[i]:StopEffect(self._Args.TargetEffect, 4)
                end

                SetMobileMod(self._Targets[i], self._Args.MobileMod, self._Args.MobileModID, nil)

            end
        end
    end,

    _MobileModAmount = nil,
    _AreaOfEffect = nil,
    _FriendOnly = false,
    _Pulses = 0,
    _CurrentTarget = nil,
    _LastTarget = nil,
    _Targets = {}
}
