MobileEffectLibrary.AzurasBlessing = 
{

    OnEnterState = function(self,root,target,args)

        self.QueuedTarget = nil
        self._Message = nil

        -- We need to register getting our target back
        RegisterEventHandler(EventType.ClientTargetGameObjResponse, "AzurasBlessingTarget", 
        function(target)
            self.UseAbility( self, root, target )
        end)

        RegisterEventHandler(EventType.Message, "CancelSpellCast", function() EndMobileEffect(root) end)
        self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "AzurasBlessingTarget")

    end,

    TargetInvalid = function( self, target )
        return (
            target == nil 
            or not target:IsValid() 
            or not target:HasObjVar("controller")
            or not IsDead(target:GetObjVar("controller"))
            or not IsPlayerObject(target:GetObjVar("controller"))
        )
    end,

    UseAbility = function( self, root, target )
        if( target == nil ) then EndMobileEffect(root) return end
        
        -- If the target isn't valid get out of here!
        if(
            self.TargetInvalid(self, target)
        ) then 
            self._Message = "Invalid target." 
            EndMobileEffect(root) 
            return false 
        end
        
        -- Are we in range for a harmful ability?
        if( self.ParentObj:DistanceFrom(target) > SorceryHelper.HarmfulAbilityRange ) then
            self._Message = "Out of range." 
            EndMobileEffect(root) 
            return
        end

        -- Queue up our target
        self.QueuedTarget = target

        -- Can we send a wisp to this target?
        if( SorceryHelper.CanSendWispToTargetMobile(self.ParentObj, self.QueuedTarget) ) then

            self.ParentObj:PlayAnimation("roar")
            self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_force")

            self.QueuedTarget:SendMessage("Resurrect", 1.0, self.ParentObj)
            
            StartPrestigePositionCooldown(self.ParentObj, "Bard", "AzurasBlessing", PrestigeData.Bard.Abilities.AzurasBlessing.PostCooldown )
            EndMobileEffect(root)
    
        else -- wisp cannot get to target
            self._Message = "Cannot see target."
            EndMobileEffect(root)
            return false
        end
    end,
    
    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "AzurasBlessingTarget")
        UnregisterEventHandler("", EventType.Message, "CancelSpellCast")
        if( self._Message ~= nil ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
    end,
    
}