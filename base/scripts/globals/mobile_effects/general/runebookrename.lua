MobileEffectLibrary.RunebookRename = 
{
    OnEnterState = function(self,root,target,args)
        self._RunebookObj = self.ParentObj
        self._PlayerObj = target

        if( Gardening.CanPlantContainerBeUsed( self._PlayerObj, self._RunebookObj ) ) then
            TextFieldDialog.Show{
                TargetUser = self._PlayerObj,
                ResponseObj = self._PlayerObj,
                Title = "Rename Runebook",
                Description = "Maximum 20 characters",
                ResponseFunc = function(user,newName)
                    if(newName == nil) then
                        self.ParentObj:SystemMessage("Runebook rename cancelled.", "info")
                        return
                    end
    
                    if ( user ~= self._PlayerObj or newName == "" or newName == nil ) then
                        self.ParentObj:SystemMessage("Invalid runebook name. Try again.", "info")
                        StartMobileEffect(self._RunebookObj, "RunebookRename", self._PlayerObj)
                        return
                    end
                    
                    newName = StripColorFromString(newName)
    
                    local valid, error = ValidatePlayerInput(newName, 3, 20)
                    if not( valid ) then
                        self.ParentObj:SystemMessage("Runebook name "..error, "info")
                        StartMobileEffect(self._RunebookObj, "RunebookRename", self._PlayerObj)
                        return
                    end
        
                    newName = "[FF9500]"..newName.."[-]"
                    self._RunebookObj:SetName(newName)
                    self._PlayerObj:SystemMessage("Runebook renamed to " .. newName .. ".", "info")

                end
            }

        else
            self._Message = "You cannot rename this runebook."
        end

        EndMobileEffect(root)

	end,

	OnExitState = function(self,root)
        if( self._Message ) then
            self._PlayerObj:SystemMessage(self._Message, "info")
        end

	end,

    _RunebookObj = nil,
    _PlayerObj = nil,
    _Message = nil
}