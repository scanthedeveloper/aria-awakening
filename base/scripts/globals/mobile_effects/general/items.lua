



MobileEffectLibrary.FillWaterContainer = 
{
	EndOnMovement = true,
    EndOnAction = true,
	OnEnterState = function(self,root,target,args)
		local backpack = self.ParentObj:GetEquippedObject("Backpack")

		if (target:GetObjVar("State") == "Full") then
			-- We need to look for another empty waterskin if one exists.
			local object = FindObjectInContainer( backpack, "waterskin", {ObjVar = "State", Value = "Empty"} )

			-- Do we have another waterskin that can be filled?
			if( object ) then
				target = object
			else
				-- no need for system message as this should not have a fill use case when already full
				EndMobileEffect(root)
				return false
			end
		end

		local nearWater = false
		if (self.ParentObj:IsInRegion("Water")) then 
			nearWater = true
		else
			local waterSource = FindObject(SearchHasObjVar("WaterSource",OBJECT_INTERACTION_RANGE))
			nearWater = (waterSource ~= nil)
		end

		if not(nearWater) then
			self.ParentObj:SystemMessage("You don't see anywhere nearby to fill it.","info")
			EndMobileEffect(root)
			return false
		end

		if(self.DamageInterrupts) then
			RegisterEventHandler(EventType.Message,"DamageInflicted",
			function (damager,damageAmt)
				if(damageAmt > 0) then
					EndMobileEffect(root)
				end
			end)
		end

		RegisterSingleEventHandler(EventType.Timer, "FillingTimer", function()
			self.ParentObj:PlayAnimation("kneel_standup")
			UnregisterEventHandler("", EventType.Message, "DamageInflicted")
			SetMobileMod(self.ParentObj, "Disable", "FillWaterMod", nil)
			self.CompleteFill(self, root, target)
		end)

		self.ParentObj:PlayAnimation("forage")	
		self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(self.FillingTime),"FillingTimer")
		SetMobileMod(self.ParentObj, "Disable", "FillWaterMod", true)
		ProgressBar.Show
		{
			Label="Filling",
			Duration=self.FillingTime,
			TargetUser = self.ParentObj,
			PresetLocation="AboveHotbar",
		}
	end,

	CompleteFill = function(self, root, target)
		self.ParentObj:SystemMessage("You fill the "..(StripColorFromString(target:GetObjVar("OriginalName")) or "container")..".","info")
		UpdateWaterContainerState(target,"Full")

		EndMobileEffect(root)
	end,

	OnExitState = function(self,root)
		self.ParentObj:PlayAnimation("idle")
		UnregisterEventHandler("", EventType.Message, "DamageInflicted")
		self.ParentObj:RemoveTimer("FillingTimer")
		ProgressBar.Cancel("Filling", self.ParentObj)
		SetMobileMod(self.ParentObj, "Disable", "FillWaterMod", nil)
	end,

	DamageInterrupts = true,
	FillingTime = 4,
}

MobileEffectLibrary.DrinkContainer = 
{
	OnEnterState = function(self,root,target,args)
		if (target:GetObjVar("State") ~= "Full") then
			-- no need for system message as this should not have a fill use case when already full
			EndMobileEffect(root)
			return false
		end

		CompleteEatFood(self.ParentObj)

		UpdateWaterContainerState(target,"Empty")

		EndMobileEffect(root)
	end,

	FoodClass = "Refreshment"
}

MobileEffectLibrary.CriminalHead = 
{
    
    OnEnterState = function(self,root,target,args)

        if ( target == nil or target:GetObjVar("PlayerId") ~= self.ParentObj.Id ) then
            self.ParentObj:SystemMessage("Cannot think of a way to use that.", "info")
            EndMobileEffect(root)
            return false
        end

        -- just to not deal with the weirdness of players doing something between confirm window showing up and accepted
        SetMobileMod(self.ParentObj, "Disable", "ContemplatingEatingHead", true)
        SetMobileMod(self.ParentObj, "Freeze", "ContemplatingEatingHead", true)

        local criminalSkills = target:GetObjVar("CriminalSkillData")
        if ( criminalSkills ~= nil ) then
            local skillChanges, skillDictionary = SanitizeCriminalStats(self.ParentObj, criminalSkills)

            local message = ""

            local skillName, value
            for i=1,#skillChanges do
                skillName, value = skillChanges[i][1], skillChanges[i][2]
                message = message .. string.format("+%s to %s (%s)\n", value, GetSkillDisplayName(skillName), (skillDictionary[skillName].SkillLevel or 0) + value)
            end

            if ( #skillChanges > 0 ) then
                message = "You will gain the following:\n\n" .. message
            else
                message = "Cannot gain anything from this, lower some skills to receive a benefit."
            end

            -- Are you sure?
            ClientDialog.Show{
                TargetUser = self.ParentObj,
                DialogId = "EatTheHead",
                TitleStr = "",
                DescStr = message,
                Button1Str = "Ok",
                Button2Str = "Cancel",
                ResponseObj = self.ParentObj,
                ResponseFunc = function( user, buttonId )
                    buttonId = tonumber(buttonId)
                    if ( user == self.ParentObj and buttonId == 0 and target:IsValid() ) then
                        skillChanges, skillDictionary = SanitizeCriminalStats(self.ParentObj, criminalSkills)
                        if ( ApplySanitizedCriminalStats(self.ParentObj, criminalSkills, skillChanges, skillDictionary) ) then
                            target:Destroy()
                        end
                    end
                    EndMobileEffect(root)
                end,
            }
        end
    end,

    OnExitState = function(self,root)
        SetMobileMod(self.ParentObj, "Disable", "ContemplatingEatingHead", nil)
        SetMobileMod(self.ParentObj, "Freeze", "ContemplatingEatingHead", nil)
    end,
}


MobileEffectLibrary.SkillScroll = 
{
    
    OnEnterState = function(self,root,target,args)

        if ( target == nil ) then
            self.ParentObj:SystemMessage("Cannot think of a way to use that.", "info")
            EndMobileEffect(root)
            return false
        end

        if(SkillScroll.Apply(target, self.ParentObj)) then
        	target:Destroy()
        end

        EndMobileEffect(root)
        return false

    end
}
