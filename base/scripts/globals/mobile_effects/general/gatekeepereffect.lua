-- This mobile effect handles teleporting someone who has purchased travel through a gatekeeper
MobileEffectLibrary.GatekeeperEffect = {

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration			

		if not(args.DestinationInfo) or not(target) then
			EndMobileEffect(root)
		end

		local destinationInfo = args.DestinationInfo
		local universe = args.Universe or GetUniverseName()

		local regionAddress = destinationInfo.World
		if(destinationInfo.Subregion) then
			regionAddress = regionAddress .. "." .. destinationInfo.Subregion
		end

		if(universe) then
			regionAddress = universe .. "." .. regionAddress
		end
		DebugMessage("RA : "..regionAddress)

		self.TeleporterRange = target:GetObjVar("TeleportRange") or self.TeleporterRange

		local displayName = destinationInfo.DisplayName or "Unknown"
		AddBuffIcon(self.ParentObj, "GatekeeperBuff", "Attuned", "Cold Mastery", "You are attuned for travel to the "..displayName..".", false, self.Duration.TotalSeconds)
		AddView("GatekeeperTeleporterView",SearchSingleObject(target,SearchObjectInRange(self.TeleporterRange)))
		RegisterEventHandler(EventType.EnterView,"GatekeeperTeleporterView",
			function ( ... )
				CallFunctionDelayed(TimeSpan.FromSeconds(0.8),function ( ... )
					TeleportUser(target,self.ParentObj,destinationInfo.Destination,regionAddress,destinationInfo.DestFacing)
					EndMobileEffect(root)
				end)

				local gatekeeperTower = FindObject(SearchTemplate("mage_tower",10))
				-- DAB TODO: Handle multiple people going through
				if(gatekeeperTower) then
					gatekeeperTower:SendMessage("Activate")
				end
			end)
	end,

	OnExitState = function (self)
		RemoveBuffIcon(self.ParentObj,"GatekeeperBuff")
	end,

	TeleporterRange = 1,
	Duration = TimeSpan.FromMinutes(5),
}