MobileEffectLibrary.CutOffHead = 
{
	
	-- Optional flag for the effect to end on movement; sets self._MovementFiredExit(bool) if triggered
	EndOnMovement = true,

	-- Optional flag for the effect to end on action; sets self._ActionFiredExit(bool) and [self._EndOnActionEventType,_EndOnActiveEventDetails  <--- what trigged breakInvis] if triggered
	EndOnAction = true,

	-- Optional string; when EndOnAction is called from a weapon ability you can choose to have specific abilities ignored
	-- IgnoreEndOnActionTypes = {"WeaponAbilityName"},


	-- Option flag to force the player to dismount
	ForceDismount = true,

	OnEnterState = function(self,root,target,args)
        self.Duration = args.Duration or self.Duration
        self._Target = target

        self.Verify(self,root)
        
        if ( self._Interrupted ) then
            EndMobileEffect(root)
            return false
        end

        self._Target:ScheduleTimerDelay(self.Duration, "HeadIsBeingCut")

		self.ParentObj:SendMessage("EndCombatMessage")
        self.ParentObj:PlayObjectSound("event:/character/skills/gathering_skills/hunting/hunting_knife")
        
        FaceObject(self.ParentObj, self._Target)
        
		ProgressBar.Show({
			TargetUser=self.ParentObj,
			Label="Cutting Head",
			Duration=self.Duration,
			PresetLocation="AboveHotbar",
            DialogId = "CutOffHead"
        })

        self.ParentObj:PlayAnimation("carve")
        
    end,

    Verify = function(self,root)
        if ( self._Target == nil or not self._Target:IsValid() or not IsPlayerCorpse(self._Target) ) then
            return self.Interrupt(self, "Invalid target.")
        end
        
        if ( self._Target:HasObjVar("IsHeadless") ) then
            return self.Interrupt(self, "Their head has already been cut off.")
        end

        if ( not IsCriminal(self._Target) ) then
            return self.Interrupt(self, "Can only cut off the heads of criminals.")
        end

        if ( self.ParentObj:DistanceFrom(self._Target) > 2.5) then
            return self.Interrupt(self, "Too far away.")
        end

        return true
    end,
    
    Interrupt = function(self,msg)
        self._Interrupted = true
        self.ParentObj:SystemMessage(msg, "info")
        return false
    end,

    OnExitState = function(self,root)
        self.ParentObj:PlayAnimation("idle")

        if ( self._Interrupted ) then
			if ( self.ParentObj:HasTimer("CutOffHeadClose") ) then
				self.ParentObj:FireTimer("CutOffHeadClose") -- close progress bar
            end
        end
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

    AiPulse = function(self,root)
        if ( self.Verify(self,root) ) then
            -- mark corpse as without head
            self._Target:SetObjVar("IsHeadless", true)
            
            -- force them to release if they are online and not released yet
            local player = GameObj(self._Target:GetObjVar("PlayerId"))
            if ( player:IsValid() and player:GetSharedObjectProperty("IsDead") == true ) then
                player:FireTimer("AutoCorpseRelease")
            end

            -- update the corpses auto release time so they will release if offline
			self._Target:SetObjVar("AutoReleaseAt", DateTime.UtcNow)

            self.ParentObj:PlayObjectSound("event:/objects/pickups/bounty_head/bounty_head_pickup",false)

            local templateData = GetTemplateData("criminal_head")
            templateData.Name = EnglishPossessive(player:GetCharacterName()) .. " Head"

            if not( templateData.ObjectVariables ) then templateData.ObjectVariables = {} end
            templateData.ObjectVariables.PlayerId = self._Target:GetObjVar("PlayerId")

            -- this data represents skills lost on death, pass it onto the corpse
            local criminalSkillData = self._Target:GetObjVar("CriminalSkillData")
            if ( criminalSkillData ~= nil ) then
                self._Target:DelObjVar("CriminalSkillData") -- clear it so that it will not be reapplied on a resurrect
            end

            Create.Custom.InBackpack("criminal_head", templateData, self.ParentObj, nil, function(head)
                if ( criminalSkillData ~= nil ) then -- cannot set this from custom template data
                    head:SetObjVar("CriminalSkillData", criminalSkillData)
                    SetItemTooltip(head)
                end
                EndMobileEffect(root)
            end, true, true)
        else
            EndMobileEffect(root)
        end
	end,

    Duration = TimeSpan.FromSeconds(1.5),
    _Interrupted = false,
}