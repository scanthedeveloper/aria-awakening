MobileEffectLibrary.Regen = 
{

    OnEnterState = function(self,root,target,args)
        self._ID = args.MobileModID or "mod_"..uuid()
        self._MobileMod = args.MobileMod or nil
        self._RegenAmount = args.RegenAmount or nil
        self._DurationInSeconds = args.PulseDuration or 3
        self._BuffIcon = args.BuffIcon or nil
        self._BuffDescription = args.BuffDescription or ""

        -- If we don't have a mobile mod we need to get out of here!
        if( self._MobileMod == nil ) then EndMobileEffect(root) return false end

        -- Are we a player?
        self.IsPlayer = IsPlayerCharacter(self.ParentObj)

        -- Lets regen!
        SetMobileMod(self.ParentObj, self._MobileMod, self._ID, self._RegenAmount)

        -- Do we need to show the buff?
        if( self._BuffIcon ~= nil and self.IsPlayer ) then
            --DebugMessage("Add Buff Icon")
            AddBuffIcon(self.ParentObj, self._ID, "Regenerating", self._BuffIcon, self._BuffDescription, false)
        end

        --DebugMessage("Regen Started!")
        --DebugTable( args )

    end,

    OnExitState = function(self,root)
        --DebugMessage("Regen Stopped!")
        self._CleanUp(self, root)
    end,
    
    OnStack = function(self,root,target,args)
        --DebugMessage("Regen Stacked!")
        self.PulsesCount = 0
        self._CleanUp(self,root)
        self.OnEnterState(self,root,target,args)
    end,
    
    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
    end,
    
    AiPulse = function(self,root) 
        --DebugMessage( "Regen Pulsed: " .. self.PulsesCount )
        --DebugMessage( "MaxCount: " .. self._DurationInSeconds )
        
        if( self.PulsesCount >= self._DurationInSeconds ) then
            EndMobileEffect(root)
        end
        self.PulsesCount = self.PulsesCount + 1
    end,

    _CleanUp = function(self, root)
        --DebugMessage("Regen Cleaned!")
        SetMobileMod(self.ParentObj, self._MobileMod, self._ID, nil)
        if( self._BuffIcon ~= nil and self.IsPlayer ) then
            RemoveBuffIcon(self.ParentObj, self._ID)
        end
    end,

    PulsesCount = 0,
    _ID = nil,
    _RegenAmount = nil,
    _DurationInSeconds = 3,
    _BuffIcon = nil,
    _BuffDescription = "",
    _MobileMod = nil,
    IsPlayer = false
}