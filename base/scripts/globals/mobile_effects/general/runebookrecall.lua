MobileEffectLibrary.RunebookRecall = 
{
	EndOnMovement = true,
	EndOnAction = true,
	CancelSpellCast = true,
	ForceStandUp = true,
	IgnoreEndOnActionTypes = {"SwungAt", "Pickup"},
	
	OnEnterState = function(self,root,target,args)
		self.RuneEntry = args.RuneEntry
		self.RunebookObj = args.RunebookObj

		-- If we don't have a rune entry something is wrong!
		if not(self.RuneEntry) then
			--DebugMessage("End Recall: 1")
			EndMobileEffect(root)
			return false
		end

		-- Does the player have the security to use this runebook?
		if( not(Plot.CheckUseObjectSecurity(self.ParentObj,self.RunebookObj)) ) then
			--DebugMessage("End Recall: 2")
			EndMobileEffect(root)
			return false
		end
		
		-- We need to verify the location we are teleporting to
		RegisterEventHandler(EventType.Message, "PortalLocValidated", function (InvalidMessage, NewDestLoc, Protection, RegionalName)
			if (InvalidMessage == "") then

				if (NewDestLoc ~= nil) then
					self.RuneEntry.Destination = NewDestLoc
				end

				-----------------------------------------------------------------
				self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(10), "SpellPrimeTimer", "RunebookRecall", self.ParentObj)--hack because this effect doesn't use the spell system
				self.ParentObj:PlayAnimation("cast")
				self.ParentObj:PlayEffectWithArgs("ConjurePrimeBlueEffect",self.RecallTime,"Bone=Ground")
				self.ParentObj:PlayEffectWithArgs("MagicPrimeHandBlueEffect",self.RecallTime,"Bone=L_Hand")
				self.ParentObj:PlayObjectSound("event:/magic/water/magic_water_cast_water",false,self.RecallTime)

				--DebugMessage(" self.RecallTime : " .. self.RecallTime)

				ProgressBar.Show
				{
					Label="Runebook Recall",
					Duration=TimeSpan.FromSeconds(self.RecallTime),
					TargetUser = self.ParentObj,
					PresetLocation="AboveHotbar",
					DialogId="RunebookRecall",
					Callback = function()
						--DebugMessage("Callback fired!")
						self.CleanupInterruption(self,root)
						self:CompleteRecall(self,root)
						EndMobileEffect(root)
					end
				}
				-----------------------------------------------------------------
			
			else
				self.ParentObj:SystemMessage(InvalidMessage, "info")
				EndMobileEffect(root)
			end
			
		end)

		-- if the desination subregion is offline, don't create portal
		if (self.RuneEntry.RegionAddress ~= ServerSettings.RegionAddress and IsClusterRegionOnline(self.RuneEntry.RegionAddress) == false) then
			self.ParentObj:SystemMessage("Cannot create a portal right now.", "info")
			--DebugMessage("End Recall: 4")
			EndMobileEffect(root)
			return
		end

		-- if in arena match, you cannot recall
		if( HasMobileEffect(self.ParentObj, "RankedArenaMatch") ) then
			self.ParentObj:SystemMessage("Cannot recall or portal out of arena.", "info")
			EndMobileEffect(root)
			return
		end

		--handle user made runes
		if (self.RuneEntry.Destination ~= nil) then
			--Skip cluster controller validation if player is in same subregion as destination
			if(not(self.RuneEntry.RegionAddress) or self.RuneEntry.RegionAddress == ServerSettings.RegionAddress) then
				local invalidMessage, newDestLoc = ValidatePortalSpawnLoc(self.ParentObj, self.RuneEntry.Destination, self.RuneEntry.RegionAddress)
				local regionalName = GetRegionalName(newDestLoc)
				local protection = GetGuardProtectionForLoc(newDestLoc)
				self.ParentObj:SendMessage("PortalLocValidated", invalidMessage, newDestLoc, protection, regionalName)
			else
				MessageRemoteClusterController(self.RuneEntry.RegionAddress,"ValidatePortalLoc",self.ParentObj, self.RuneEntry.Destination)
			end
		end
		
	end,

	CompleteRecall = function(self, root, target)
		if(self.RunebookObj) then
			local charges = self.RunebookObj:GetObjVar("RuneCount")
			charges = math.max(0,(charges - 1))
			self.RunebookObj:SetObjVar("RuneCount",charges)
		end

		self.ParentObj:PlayAnimation("idle")
		PlayEffectAtLoc("TeleportFromEffect",self.ParentObj:GetLoc())

		-- We don't want this player to get the InvulCloak effect
		self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(5),"EnterPlayerPortal")
		
		-- Beam me up, Scotty!
		TeleportUser(
			self.ParentObj,
			self.ParentObj,
			self.RuneEntry.Destination,
			self.RuneEntry.RegionAddress,
			self.RuneEntry.Facing
		)
	end,

	CleanupInterruption = function(self, root)
		self.ParentObj:RemoveTimer("SpellPrimeTimer")
    end,

	OnExitState = function(self,root)
		--DebugMessage( "Exit Called!" )
		ProgressBar.Cancel("RunebookRecall", self.ParentObj)
		UnregisterEventHandler("", EventType.Message, "PortalLocValidated")
		self.ParentObj:PlayAnimation("idle")
		self.CleanupInterruption(self,root)
		self.ParentObj:RemoveTimer("RecallTimer")
		
	end,

	RecallTime = 6,
	RunebookObj = nil,
	RuneEntry = nil,
}