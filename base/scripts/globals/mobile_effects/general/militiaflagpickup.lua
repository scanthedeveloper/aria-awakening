MobileEffectLibrary.MilitiaFlagPickup = 
{
	OnEnterState = function(self,root,target,args)
		if ( self.ParentObj:IsMoving() ) then
			self.ParentObj:SystemMessage("Must be still to do that.", "info")
			EndMobileEffect(root)
			return false
		end

		self.Flag = args.Flag
		self.FlagMilitiaId = args.Flag:GetObjVar("FlagMilitia")

		self.BeginCast(self,root)
	end,

	ApplyAnimation = function(self, root)
		if ( self.FlagMilitiaId == 1 ) then
			self.ParentObj:PlayEffectWithArgs("PyrosFlagEffect",0.0,"Bone=Head")
		elseif ( self.FlagMilitiaId == 2 ) then
			self.ParentObj:PlayEffectWithArgs("HelmFlagEffect",0.0,"Bone=Head")
		elseif ( self.FlagMilitiaId == 3 ) then
			self.ParentObj:PlayEffectWithArgs("EldeirFlagEffect",0.0,"Bone=Head")
		end
	end,

	RemoveAnimation = function(self, root)
		if ( self.FlagMilitiaId == 1 ) then
			self.ParentObj:StopEffect("PyrosFlagEffect")
		elseif ( self.FlagMilitiaId == 2 ) then
			self.ParentObj:StopEffect("HelmFlagEffect")
		elseif ( self.FlagMilitiaId == 3 ) then
			self.ParentObj:StopEffect("EldeirFlagEffect")
		end
	end,

	BeginCast = function(self,root)
		self._Applied = true
        
		SetMobileMod(self.ParentObj, "Busy", "FlagPickup", true)
        
		RegisterEventHandler(EventType.Message, "BreakInvisEffect", function(what)
			if ( what ~= "Pickup" ) then
				self.Interrupt(self,root)
            end
        end)

        RegisterEventHandler(EventType.StartMoving, "", function() self.Interrupt(self,root) end)
        
        if ( IsInCombat(self.ParentObj) ) then
            self.ParentObj:SendMessage("EndCombatMessage")
        end
			
        ProgressBar.Show{
            Label="Securing Flag",
            Duration=self.FlagPickupTime,
            TargetUser=self.ParentObj,
            PresetLocation="AboveHotbar",
        }
		self.ParentObj:PlayAnimation("cast")
		self:ApplyAnimation(root)
	end,

	GetPulseFrequency = function(self,root)
		return self.FlagPickupTime
	end,

	AiPulse = function(self,root)
		self.Flag:SendMessage("CompletePickup",self.ParentObj)

		EndMobileEffect(root)
	end,

	Interrupt = function(self,root)
		if( self._IgnoreInterrupted ~= true ) then
			self._Interrupted = true
			EndMobileEffect(root)
		end
	end,

	OnExitState = function(self,root)
		if ( self._Applied ) then
			if ( self._Interrupted ) then
				ProgressBar.Cancel("Securing Flag", self.ParentObj)
				self.ParentObj:SystemMessage("Securing flag interruped.", "info")
			end
			SetMobileMod(self.ParentObj, "Busy", "FlagPickup", nil)
			UnregisterEventHandler("", EventType.Message, "BreakInvisEffect")
			UnregisterEventHandler("", EventType.StartMoving, "")
			self.ParentObj:PlayAnimation("idle")

			self:RemoveAnimation(root)
		end
	end,

	Flag = nil,
	FlagMilitiaId = nil,
	FlagPickupTime = ServerSettings.Militia.Events[3].FlagPickupTime,
}

