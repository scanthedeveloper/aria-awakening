MobileEffectLibrary.EquipMilitiaCloak = 
{
	GetCloakTemplate = function(self)
		local militia = Militia.GetId(self.ParentObj)
		local rank = Militia.GetRankNumber(self.ParentObj)		
        local cloakTemplate = nil
		if(militia and rank and rank > 0 and ServerSettings.Militia.Militias[militia] and ServerSettings.Militia.Militias[militia].RankRewardCloaks) then
			for rankReq,template in pairs(ServerSettings.Militia.Militias[militia].RankRewardCloaks) do
				if(rank >= rankReq) then
					cloakTemplate = template
				end
			end
		end

		return cloakTemplate
	end,

    OnEnterState = function(self,root,target,args)
        if not( self:Validate() ) then
            EndMobileEffect(root)
            return false
        end

        local cloakTemplate = self:GetCloakTemplate()
        if not(cloakTemplate) then
            self.ParentObj:SystemMessage("Not high enough rank to equip militia cloak.","info")
            EndMobileEffect(root)
            return false
        end

        local equippedCloak = self.ParentObj:GetEquippedObject("Cloak")
        if(equippedCloak and equippedCloak:GetCreationTemplateId() == cloakTemplate) then
            self.ParentObj:SystemMessage("You already have your militia cloak equipped.","info")
            EndMobileEffect(root)
            return false
        end
        
        local tempLoc = self.ParentObj:GetLoc()
        Create.Temp.AtLoc(cloakTemplate, tempLoc, function(cloak)
            if ( cloak ) then
                self.ParentObj:SystemMessage("You equip your militia cloak.","info")
                DoEquip(cloak,self.ParentObj)
                cloak:SetObjVar("Summoned",true)
            end

            EndMobileEffect(root)
        end, true)
    end,
    
    -- this is used for validating the spell cast as well (to avoid resource consumption)
    Validate = function(self)
        if ( IsDead(self.ParentObj) ) then
            self.ParentObj:SystemMessage("You are dead!", "info")
            return false
        end
        
        return true
    end,
}