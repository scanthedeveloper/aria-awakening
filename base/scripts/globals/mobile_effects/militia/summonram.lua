MobileEffectLibrary.SummonRam = 
{
    QTarget = true,

	OnEnterState = function(self,root,target,args)
		local myMilitia = Militia.GetId(self.ParentObj)

		if not(self:ValidateUse(myMilitia)) then
			ResetPrestigeCooldown(self.ParentObj, "Militia", "SummonRam")
			EndMobileEffect(root)
			return
		end		

		self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "select_keep_door")
		RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse,"select_keep_door",function (targetObj)
			if not(self:ValidateTarget(targetObj,myMilitia)) then
				ResetPrestigeCooldown(self.ParentObj, "Militia", "SummonRam")
				EndMobileEffect(root)
				return
			end		

			local ramLoc = targetObj:GetLoc():Project(90,6.5)
			CreateObjExtended("battering_ram",nil,ramLoc,Loc(0,-90,0),Loc(1,1,1),"ram")

			Militia.ConsumeGlobalResource(myMilitia,"Spiritwood",ServerSettings.Militia.GlobalResources.RamResourceCost)

			RegisterSingleEventHandler(EventType.CreatedObject,"ram",
				function (success,objRef)
					if(success) then
						objRef:SetObjVar("MilitiaId",myMilitia)

						EndMobileEffect(root)

						MoveMobilesOutOfObject(objRef)
					end
				end)
		end)		
	end,

	ValidateUse = function(self, myMilitia)		
		-- do we have enough spiritwood
		local spiritwood = Militia.GetGlobalResource(myMilitia,"Spiritwood")
		if(spiritwood < ServerSettings.Militia.GlobalResources.RamResourceCost) then
			self.ParentObj:SystemMessage("Your militia does not have enough spiritwood. Required: "..ServerSettings.Militia.GlobalResources.RamResourceCost,"info")
			return false
		end

		return true
	end,

	ValidateTarget = function (self,keepDoor, myMilitia)
		if( not(keepDoor) or keepDoor:GetCreationTemplateId() ~= "keep_wood_door") then
			self.ParentObj:SystemMessage("You must target an enemy keep door.","info")
			return false
		end

		if (self.ParentObj:DistanceFrom(keepDoor) > OBJECT_INTERACTION_RANGE) then
   	    	self.ParentObj:SystemMessage("Too far away.", "info")
   	    	return false
   	    end

		if not(MilitiaEvent.IsKeepVulnerable(keepDoor)) then
			self.ParentObj:SystemMessage("This keep is not vulnerable to attack.","info")
			return false
		end
		
		local doorTownship = keepDoor:GetObjVar("Township")
		local doorMilitia = TownshipsHelper.GetMilitaId( doorTownship ) 
		if(doorMilitia == myMilitia) then
			self.ParentObj:SystemMessage("You can only construct a ram near an enemy keep door.","info")
			return false
		end
		
		-- is the door intact
		if(keepDoor:GetSharedObjectProperty("IsDestroyed")) then
			self.ParentObj:SystemMessage("You can only construct a ram near an enemy keep door.","info")
			return false
		end		

        local ramObj = FindObject(SearchTemplate("battering_ram",20))
        if(ramObj) then
			self.ParentObj:SystemMessage("There is already a battering ram at this keep door.","info")
			return false
		end	        	

		return true
	end
}