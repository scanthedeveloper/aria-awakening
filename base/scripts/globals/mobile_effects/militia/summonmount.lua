MobileEffectLibrary.SummonMilitiaMount = 
{
	GetMountTemplate = function(self)
		local militia = Militia.GetId(self.ParentObj)
		local rank = Militia.GetRankNumber(self.ParentObj)		
        local mountTemplate = nil
		if(militia and rank and rank > 0 and ServerSettings.Militia.Militias[militia] and ServerSettings.Militia.Militias[militia].RankRewardMounts) then
			for rankReq,template in pairs(ServerSettings.Militia.Militias[militia].RankRewardMounts) do
				if(rank >= rankReq) then
					mountTemplate = template
				end
			end
		end

		return mountTemplate
	end,

    OnEnterState = function(self,root,target,args)
        if not( self:Validate() ) then
            EndMobileEffect(root)
            return false
        end

        local mountTemplate = self:GetMountTemplate()
        if not(mountTemplate) then
            self.ParentObj:SystemMessage("Not high enough rank to summon militia mount.","info")
            EndMobileEffect(root)
            return false
        end

        self.BeginCast(self,root)        
    end,

    BeginCast = function (self,root)
        if ( self._Blocked ) then return end

        self._Applied = true
        
        SetMobileMod(self.ParentObj, "Busy", "MilitiaMount", true)
        RegisterEventHandler(EventType.Message, "BreakInvisEffect", function(what)
            EndMobileEffect(root)
        end)
        RegisterEventHandler(EventType.StartMoving, "", function() EndMobileEffect(root) end)
        self.ParentObj:StopMoving()
        if ( IsInCombat(self.ParentObj) ) then
            self.ParentObj:SendMessage("EndCombatMessage")
        end
            
        ProgressBar.Show{
            Label="Summoning",
            Duration=self.Duration,
            TargetUser=self.ParentObj,
            PresetLocation="AboveHotbar",
        }

        self.ParentObj:PlayAnimation("cast")
    end,

    OnDone = function(self,root)
        local mountTemplate = self:GetMountTemplate()
        if not(mountTemplate) then
            self.ParentObj:SystemMessage("Not high enough rank to summon militia mount.","info")
            EndMobileEffect(root)
            return false
        end

        local mountLoc = self.ParentObj:GetLoc()
        Create.AtLoc(mountTemplate, mountLoc, function(mobile)
            if ( mobile ) then
                self.ParentObj:SystemMessage("You summon your militia mount.","info")
                PlayEffectAtLoc("CloakEffect", mountLoc, 0.5)
                MountMobile(self.ParentObj, mobile)    
                mobile:SetObjVar("Summoned",true)
            end

            EndMobileEffect(root)
        end, true)
    end,

    OnExitState = function(self,root)
        if ( self._Applied ) then
            SetMobileMod(self.ParentObj, "Busy", "MilitiaMount", nil)
            UnregisterEventHandler("", EventType.StartMoving, "")
            UnregisterEventHandler("", EventType.Message, "BreakInvisEffect")
            ProgressBar.Cancel("Summoning", self.ParentObj)
            self.ParentObj:PlayAnimation("idle")
        end
    end,

    AiPulse = function(self,root)
        self.OnDone(self,root)
    end,

    GetPulseFrequency = function(self,root)
        return self.Duration
    end,
    
    -- this is used for validating the spell cast as well (to avoid resource consumption)
    Validate = function(self)
        if ( IsInCombat(self.ParentObj) ) then
            self.ParentObj:SystemMessage("Can not summon mount while in combat.", "info")
            return false
        end
        if ( IsDead(self.ParentObj) ) then
            self.ParentObj:SystemMessage("You are dead!", "info")
            return false
        end
        if ( IsMounted(self.ParentObj) ) then
            self.ParentObj:SystemMessage("You are already mounted.", "info")
            return false
        end
        if ( self.ParentObj:IsInRegion("NoMount") ) then
            self.ParentObj:SystemMessage("Cannot mount here.", "info")
            return false
        end
        
        return true
    end,

    Duration = TimeSpan.FromSeconds(3),
}