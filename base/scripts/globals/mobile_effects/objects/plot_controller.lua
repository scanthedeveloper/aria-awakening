MobileEffectLibrary.PlotController = 
{

    OnEnterState = function(self,root,target,args)
        if ( target:HasModule("plot_bid_window") ) then
            target:SendMessage("ShowBidWindow", self.ParentObj)
        elseif ( target:HasModule("plot_bid_controller") ) then
            local window = target:GetObjVar("BidWindow")
            if ( window ) then window:SendMessage("ShowBidWindow", self.ParentObj) end
        elseif ( Plot.HasControl(self.ParentObj,target) ) then
            Plot.ShowControlWindow(self.ParentObj,target)
        elseif ( Plot.HasHouse(target, self.ParentObj) ) then
            -- co owners of houses can pay taxes
            self.ParentObj:AddModule("plot_tax_window")
            self.ParentObj:SendMessage("InitTaxWindow", target)
        else
            -- TODO: Show a window with a description maybe?
        end
        
        EndMobileEffect(root)
    end,

}

MobileEffectLibrary.PlotDestroy = 
{

    OnEnterState = function(self,root,target,args)
        if ( args.GodDestroy == true and IsGod(self.ParentObj) ) then
            -- Are you sure?
            ClientDialog.Show{
                TargetUser = self.ParentObj,
                DialogId = "GodDestroyPlot",
                TitleStr = "Destroy Plot",
                DescStr = "As a God of this Realm, you have the power to NUKE entire plots. WARNING!!!! This will destroy ALL ITEMS AND HOUSES on the plot. Continue?",
                Button1Str = "DESTROY PLOT AND EVERYTHING ON IT",
                Button2Str = "Cancel",
                ResponseObj = self.ParentObj,
                ResponseFunc = function( user, buttonId )
                    buttonId = tonumber(buttonId)
                    if ( user == self.ParentObj and buttonId == 0 ) then
                        Plot.Destroy(target)
                        Plot.CloseControlWindow(self.ParentObj)
                    end
                end,
            }
        else
            local plotBounds = target:GetObjVar("PlotBounds")
            local bounds = Plot.Bound2Box(plotBounds[1])
            if(FindObject(SearchMulti({
                    -- will need to loop each bound when L shape are supported..
                    SearchRect(bounds),
                    SearchHasObjVar("PlotStorage")
                    }))) then
                self.ParentObj:SystemMessage("You can not relinquish this plot with a moving crate on it.", "info")
                EndMobileEffect(root)
                return
            end

            local any = false
            Plot.ForeachLockdown(target, function() any = true return true end)
            if ( any or Plot.HasHouse(target) ) then
                self.ParentObj:SystemMessage("Plot must be cleared of all houses and locked down items to relinquish.", "info")
            else
                -- Are you sure?
                ClientDialog.Show{
                    TargetUser = self.ParentObj,
                    DialogId = "DestroyPlot",
                    TitleStr = "Relinquish Plot",
                    DescStr = "Relinquish control of the land and remove this plot. A land deed will be refunded. Continue?",
                    Button1Str = "Relinquish",
                    Button2Str = "Cancel",
                    ResponseObj = self.ParentObj,
                    ResponseFunc = function( user, buttonId )
                        buttonId = tonumber(buttonId)
                        if ( user == self.ParentObj and buttonId == 0 ) then
                            local freeResize = target:GetObjVar("FreeResize")

                            Plot.Destroy(target, function(success)
                                if ( success ) then
                                    Create.InBackpack("land_deed", self.ParentObj, nil, function(deed)
                                        Plot.CloseControlWindow(self.ParentObj)

                                        if(freeResize) then
                                            deed:SetObjVar("FreeResize",freeResize)
                                            SetTooltipEntry(deed,"pack_house","\n\nFree Resize: "..freeResize.X.." x "..freeResize.Z, -200)
                                        end
                                    end)
                                end
                            end)
                        end
                    end,
                }
            end
        end
        EndMobileEffect(root)
	end,

}

MobileEffectLibrary.PlotAdjust = 
{

    OnEnterState = function(self,root,target,args)
        Plot.Adjust(self.ParentObj, target, args)
        EndMobileEffect(root)
	end,

}

-- Used when spliting stack of items that are locked down on a plot
MobileEffectLibrary.PlotSplitStack = 
{
    OnEnterState = function(self,root,target,args)
        self._Target = target
        self._WindowID = "StackSplitLockedDown"
        self._ActiveSplit = false

        -- Make sure we have a target we are trying to split
        if( target ~= nil ) then

            local lockedDown = IsLockedDown(target)
            local plotController = target:GetObjVar("PlotController")
            local isPlotOwner = Plot.IsOwner( self.ParentObj, plotController )
            local isPlotCoOwner = Plot.IsCoOwner( self.ParentObj, plotController )

            if( lockedDown and (isPlotOwner or isPlotCoOwner) ) then
                RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)
                RegisterEventHandler(EventType.CreatedObject,"lockdown_stack_created",
                    function(success,objRef,args)   
                        self._ActiveSplit = false

                        if (not success) then
                            self._Target:SendMessage("AdjustStack",args.Amount)			
                            return
                        end
                        local vars = self._Target:GetAllObjVars()
                        for i,j in pairs(vars) do 
                            if(i ~= "DecayTime") then
                                objRef:SetObjVar(i,j)
                            end
                        end
                        local scripts = self._Target:GetAllModules()
                        for i,j in pairs(scripts) do
                            if(j ~= "decay" and not(objRef:HasModule(j))) then
                                objRef:AddModule(j)
                            end
                        end
                        local properties = self._Target:GetAllSharedObjectProperties()
                        for i,j in pairs(properties) do
                            if (i ~= "Weight") then
                                objRef:SetSharedObjectProperty(i,j)
                            end
                        end
                        --DebugMessage("Amount is "..args.Amount)
                        local stackCount = self._Target:GetObjVar("StackCount")
                        if (args.Amount < 1) then
                            args.Amount = 1
                        elseif args.Amount > stackCount then
                            args.Amount = stackCount
                        end
                        --DebugMessage("stackCount is "..stackCount)
                        RequestSetStackCount(objRef,args.Amount)
                        self._Target:SendMessage("AdjustStack",-args.Amount)
                        Plot.Release(self.ParentObj, self._Target:GetObjVar("PlotController"), objRef, true)
                        CallFunctionDelayed(TimeSpan.FromMilliseconds(30), 
                        function() 
                            args.User:SendMessage("PickupObject", objRef, true)    
                        end)
                        
                    end)
                self.CreateSplitWindow( self, root )
                return
            end
        end

        EndMobileEffect(root)
        return false
    end,

    OnExitState = function(self,root)
        -- Unregister
        UnregisterEventHandler("", EventType.DynamicWindowResponse, self._WindowID )
        -- Close the window
    end,
    
    CreateSplitWindow = function( self, root, fieldAmount )

        local newWindow = DynamicWindow(self._WindowID,"Stack Split",238,96,-50,-50,"","Center")
        newWindow:AddButton(20,15,"MinusOneStack","",0,0,"","",false,"Previous")
        newWindow:AddTextField(37,10,56,20,"StackAmount",fieldAmount or "1")
        newWindow:AddButton(100,15,"PlusOneStack","",0,0,"","",false,"Next")
        newWindow:AddButton(120,5,"CreateStack","Accept",0,0,"","",true)
        self.ParentObj:OpenDynamicWindow(newWindow,self.ParentObj)
    end,

    WindowResponse = function( user, buttonId, fieldData, self, root )
        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]
        local stackCount = self._Target:GetObjVar("StackCount") or 1
        newAmount = math.floor(tonumber(fieldData.StackAmount))

        -- Window responses
        if( action == "MinusOneStack" ) then 
            self.CreateSplitWindow( self, root, tostring(math.max(1,newAmount - 1)) )
        elseif( action == "PlusOneStack" ) then 
            self.CreateSplitWindow( self, root, tostring(math.min(stackCount -1,newAmount + 1)) )
        elseif( action == "CreateStack" ) then 
            if(self._ActiveSplit) then return end
            self._ActiveSplit = true
            CreateObjInContainer(self._Target:GetCreationTemplateId(), self.ParentObj, Loc(0,0,0), "lockdown_stack_created",{Amount = newAmount,User = self.ParentObj})
        else 
            EndMobileEffect(root) 
        end
    end,
}



MobileEffectLibrary.PlotLockdown = 
{
    QTarget = true,

    OnEnterState = function(self,root,target,args)
        self.ParentObj:SystemMessage("Select an object on your plot to lock down.","info")
        RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse, "LockdownTarget", function(gameObj)
            if ( gameObj ) then
                Plot.Lockdown(self.ParentObj, target, gameObj)
            end

            Plot.UpdateUI(self.ParentObj)

            EndMobileEffect(root)
        end)
        self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "LockdownTarget")
	end,

}

MobileEffectLibrary.PlotRelease = 
{
    QTarget = true,

    OnEnterState = function(self,root,target,args)
        self.ParentObj:SystemMessage("Select a locked down object to release.","info")
        RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse, "ReleaseTarget", function(gameObj)
            if ( gameObj ) then
                Plot.Release(self.ParentObj, target, gameObj)
            end

            Plot.UpdateUI(self.ParentObj)

            EndMobileEffect(root)
        end)
        self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "ReleaseTarget")
	end,

    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.ClientTargetGameObjResponse, "ReleaseTarget")
    end,

}

MobileEffectLibrary.PlotMoveObject =
{
    QTarget = true,

    OnEnterState = function(self,root,target,args)
        self.ParentObj:SystemMessage("Select a locked down object.","info")
        RegisterEventHandler(EventType.ClientTargetGameObjResponse, "PlotMoveObject", function(targetObj)
            if ( targetObj and targetObj:IsValid() ) then
                if not( self.ParentObj:HasModule("plot_decorate_window") ) then
                    self.ParentObj:AddModule("plot_decorate_window")
                end
                self.ParentObj:SendMessage("InitDecorate", targetObj)
            end
            EndMobileEffect(root)
        end)

        self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "PlotMoveObject")
    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.ClientTargetGameObjResponse, "PlotMoveObject")
    end,
}

--- uses the 'push' command
MobileEffectLibrary.PlotMoveObjectPush =
{
    QTarget = true,

    OnEnterState = function(self,root,target,args)

        RegisterEventHandler(EventType.ClientTargetGameObjResponse, "PlotMoveObject", function(targetObj)
            if ( targetObj and targetObj:IsValid() and Plot.TryMove(self.ParentObj, targetObj) ) then
                self._RegisteredTransform = true

                RegisterEventHandler(EventType.ClientObjectCommand, "transform", function(user,targetId,identifier,command,...)
                    if ( user == self.ParentObj and identifier == "plot_move_object" and command == "confirm" ) then
                        local targetObj = GameObj(tonumber(targetId))
                        if ( targetObj:IsValid() ) then
                            local commandArgs = table.pack(...)
                            local newPos = Loc(tonumber(commandArgs[1]),math.min(5,tonumber(commandArgs[2])),tonumber(commandArgs[3]))
                            if ( Plot.TryMoveTo(self.ParentObj, targetObj, newPos) ) then
                                local rotation = targetObj:GetRotation()
                                local newRot = Loc(rotation.X,tonumber(commandArgs[5]),rotation.Z)
                                targetObj:SetWorldPosition(newPos)
                                targetObj:SetRotation(newRot)
                                targetObj:SetScale(targetObj:GetScale()) -- hack to force client not to keep a scale user set.
                            end
                        end
                    else
                        self.ParentObj:SystemMessage("Cancelled.", "info")
                    end
                    EndMobileEffect(root)
                end)

                self.ParentObj:SendClientMessage("EditObjectTransform",{targetObj,self.ParentObj,"plot_move_object"})
            else
                EndMobileEffect(root)
            end
        end)

        self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "PlotMoveObject")
    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.ClientTargetGameObjResponse, "PlotMoveObject")
        if ( self._RegisteredTransform ) then
            UnregisterEventHandler("", EventType.ClientObjectCommand, "transform")
        end
    end,

    _RegisteredTransform = false,
}

MobileEffectLibrary.PlotTransferOwnership =
{
    QTarget = true,
    
    OnEnterState = function(self,root,target,args)
        if ( not target or not Plot.IsOwner(self.ParentObj, target) ) then
            EndMobileEffect(root)
            return false
        end

        self.ParentObj:SystemMessage("Who do you want to trade this plot to?", "info")
        RegisterEventHandler(EventType.ClientTargetGameObjResponse, "PlotTransfer", function(targetObj)
            
            -- Free accounts cannot own land.
            if( IsFreeAccount(targetObj) ) then
                self.ParentObj:SystemMessage("Trial accounts cannot own land, transfer failed.", "info")
                targetObj:SystemMessage("Trial accounts cannot own land, transfer failed.", "info")
                EndMobileEffect(root)
                return
            end

            if ( targetObj:DistanceFrom(target) > 5 ) then
                self.ParentObj:SystemMessage("Transfer target must be standing near mailbox.", "info")
                return EndMobileEffect(root)
            end
            if ( IsPlayerCharacter(targetObj) ) then
                -- gods can transfer houses to themselves
                if ( targetObj == self.ParentObj and IsGod(self.ParentObj) ) then
                    Plot.TransferOwnership(self.ParentObj,target,self.ParentObj, function()
                        EndMobileEffect(root)
                    end)
                    return
                elseif(self.ParentObj:HasModule("trading_controller")) then
                    self.ParentObj:SystemMessage("You already have an active trade in progress.","info")
                else
                    self.ParentObj:AddModule("trading_controller",{TradeTarget=targetObj,PlotTarget=target})
                end
            end
            EndMobileEffect(root)
        end)
        self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "PlotTransfer")
    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.ClientTargetGameObjResponse, "PlotTransfer")
    end,
}

MobileEffectLibrary.PlotRename = 
{

    OnEnterState = function(self,root,target,args)
        -- target parameter is the plot controller

        if ( target == nil or not target:IsValid() ) then
            return EndMobileEffect(root)
        end

        TextFieldDialog.Show{
            TargetUser = self.ParentObj,
            ResponseObj = self.ParentObj,
            Title = "Rename Plot",
            Description = "Maximum 20 characters.",
            ResponseFunc = function(user,newName)
                if ( newName == nil or newName == "" ) then
                    self.ParentObj:SystemMessage("Plot rename cancelled.", "info")
                else
                    if not( Plot.Rename(self.ParentObj, target, newName) ) then
                        StartMobileEffect(self.ParentObj, "PlotRename", target)
                    end
                end
            end
        }

        return EndMobileEffect(root)
	end,

}


MobileEffectLibrary.PlotKick = 
{
    QTarget = true,

    OnEnterState = function(self,root,target,args)
        -- target parametere is the plot controller, args is the house (if any)
        if not( Plot.HasHouseControl(self.ParentObj, target, args) ) then
            EndMobileEffect(root)
            return false
        end

        self.ParentObj:SystemMessage("Select target.", "info")
        RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse, "PlotKick", function(targetObj)
            if ( targetObj ) then
                if ( not targetObj:IsPermanent() and targetObj:IsMobile() ) then
                    if ( targetObj ~= self.ParentObj ) then
                        if not( Plot.IsOwner(targetObj, target) ) then
                            if ( Plot.GetAtLoc(targetObj:GetLoc()) == target ) then
                                Plot.KickMobile(target, targetObj)
                            else
                                self.ParentObj:SystemMessage("They are not in a place you can kick them from.", "info")
                            end
                        else
                            self.ParentObj:SystemMessage("Cannot kick plot owner from their own plot.", "info")
                        end
                    else
                        self.ParentObj:SystemMessage("Cannot kick yourself.", "info")
                    end
                else
                    self.ParentObj:SystemMessage("Invalid target.", "info")
                end
            end
            EndMobileEffect(root)
        end)
        self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "PlotKick")
	end,

    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.ClientTargetGameObjResponse, "PlotKick")
    end,

}

MobileEffectLibrary.PlotKickStrangers = 
{
    --QTarget = true,

    OnEnterState = function(self,root,target,args)
        -- target parametere is the plot controller, args is the house (if any)
        if not( Plot.HasHouseControl(self.ParentObj, target, args) ) then
            EndMobileEffect(root)
            return false
        end

        Plot.KickAllStrangers(target, self.ParentObj)
        --self.ParentObj:SystemMessage("All strangers have been kicked from the plot.", "info")
        EndMobileEffect(root)
    end,

    OnExitState = function(self,root)

    end,

}