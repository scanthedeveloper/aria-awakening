function OnEnterArtifactSchematic( self, root )
    self._IsPlayer = IsPlayerCharacter( self.ParentObj )
        if not( self._IsPlayer ) then EndMobileEffect(root) return false end

        if( HasMobileEffect(self.ParentObj, "ArtifactSchematic") ) then
            self.ParentObj:SystemMessage("You already have Artifact Chance activated.","info")
            return false
        end

        AddBuffIcon(self.ParentObj, "ArtifactSchematic", "Artificer's Brilliance", "Bash", "Allows for the chance to craft artifact items.", true, self.Duration.TotalSeconds)
        return true
end

MobileEffectLibrary.ArtifactSchematicLesser = 
{
    PersistDeath = true,
    PersistSession = true,

	OnEnterState = function(self,root,target,args)
        self.Duration = args.Duration or self.Duration
        if( OnEnterArtifactSchematic(self,root) == false ) then
            return false
        end

        self.ParentObj:PlayEffect("TeleportToEffect")
        self.ParentObj:SystemMessage("The Spirit of the Artificer enters you.", "info")
        self.ParentObj:PlayObjectSound("event:/ui/skill_gain",false)

        --SCAN ADDED COOLDOWN BAR
        ProgressBar.Show(
            {
                TargetUser = self.ParentObj,
                Label = "Artifact Crafting Timer",
                Duration = self.Duration,
                PresetLocation = "AboveHotbar",
                DialogId = "ArtifactCrafting",
            })

    end,

    OnExitState = function(self,root)
        if( self._IsPlayer ) then
            RemoveBuffIcon(self.ParentObj, "ArtifactSchematic")
        end
    end,

    GetPulseFrequency = function(self,root)
		return self.Duration
	end,

    AiPulse = function(self,root) 
        EndMobileEffect(root)
    end,

    -- SCAN CHANGED TIMER FROM 2 MINUTES TO 1
    Duration = TimeSpan.FromMinutes(1),
}

MobileEffectLibrary.ArtifactSchematic = 
{
    PersistDeath = true,
    PersistSession = true,

	OnEnterState = function(self,root,target,args)
        self.Duration = args.Duration or self.Duration
        if( OnEnterArtifactSchematic(self,root) == false ) then
            return false
        end

        self.ParentObj:PlayEffect("TeleportToEffect")
        self.ParentObj:SystemMessage("The Spirit of the Artificer enters you.", "info")
        self.ParentObj:PlayObjectSound("event:/ui/skill_gain",false)

        --SCAN ADDED COOLDOWN BAR
        ProgressBar.Show(
            {
                TargetUser = self.ParentObj,
                Label = "Artifact Crafting Timer",
                Duration = self.Duration,
                PresetLocation = "AboveHotbar",
                DialogId = "ArtifactCrafting",
            })
    end,

    OnExitState = function(self,root)
        if( self._IsPlayer ) then
            RemoveBuffIcon(self.ParentObj, "ArtifactSchematic")
        end
    end,

    GetPulseFrequency = function(self,root)
		return self.Duration
	end,

    AiPulse = function(self,root) 
        EndMobileEffect(root)
    end,

    -- SCAN CHANGED TIMER FROM 2 MINUTES TO 1
    Duration = TimeSpan.FromMinutes(1),
}

MobileEffectLibrary.ArtifactSchematicGreater = 
{
    PersistDeath = true,
    PersistSession = true,

	OnEnterState = function(self,root,target,args)
        self.Duration = args.Duration or self.Duration
        if( OnEnterArtifactSchematic(self,root) == false ) then
            return false
        end

        self.ParentObj:PlayEffect("TeleportToEffect")
        self.ParentObj:SystemMessage("The Spirit of the Artificer enters you.", "info")
        self.ParentObj:PlayObjectSound("event:/ui/skill_gain",false)

        --SCAN ADDED COOLDOWN BAR
        ProgressBar.Show(
            {
                TargetUser = self.ParentObj,
                Label = "Artifact Crafting Timer",
                Duration = self.Duration,
                PresetLocation = "AboveHotbar",
                DialogId = "ArtifactCrafting",
            })
    end,

    OnExitState = function(self,root)
        if( self._IsPlayer ) then
            RemoveBuffIcon(self.ParentObj, "ArtifactSchematic")
        end
    end,

    GetPulseFrequency = function(self,root)
		return self.Duration
	end,

    AiPulse = function(self,root) 
        EndMobileEffect(root)
    end,

    -- SCAN CHANGED TIMER FROM 2 MINUTES TO 1
    Duration = TimeSpan.FromMinutes(1),
}


