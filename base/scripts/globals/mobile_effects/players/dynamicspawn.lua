MobileEffectLibrary.DynamicSpawn = 
{
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self._Spawner = target or nil
        self._SpawnData = {}
        self._HUDShown = false

        if( not IsPlayerCharacter(self.ParentObj) or not self._Spawner:IsValid() ) then
            EndMobileEffect(root)
            return false 
        end

        RegisterEventHandler(EventType.Message, "GetSpawnData", function( spawnData )
            self._SpawnData = spawnData
            if( self._SpawnData.IsSpawning ) then
                self._HUDShown = true
                UpdateDynamicEventHUD( self.ParentObj,  self._SpawnData.DynamicHUD)
            elseif( self._HUDShown == true ) then
                self._HUDShown = false
                CloseDynamicEventHUD( self.ParentObj )
            end
            
        end)

        self._Spawner:SendMessage("GetSpawnData", self.ParentObj)
        AddBuffIcon(self.ParentObj, "DynamicSpawnArea", "Dynamic Spawn Area", "Lightning Nova", "Killings monsters in this area can trigger a dynamic spawn event.", true)
    end,

    OnExitState = function(self,root)
        CloseDynamicEventHUD( self.ParentObj )
        self._Spawner:SendMessageGlobal("LeaveSpawnArea", self.ParentObj)
        UnregisterEventHandler("", EventType.Message, "GetSpawnData")
        RemoveBuffIcon(self.ParentObj, "DynamicSpawnArea")

        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
	end,

    AiPulse = function(self,root) 
        if( 
            self._Spawner:IsValid()  -- Is the spawner valid? (ie. in same region)
            and self.ParentObj:DistanceFrom(self._Spawner) <= self._SpawnData.SpawnRadius -- are we still within range of it?
        ) then
            self._Spawner:SendMessage("GetSpawnData", self.ParentObj)
        else
            EndMobileEffect(root)
        end   
    end,

}
