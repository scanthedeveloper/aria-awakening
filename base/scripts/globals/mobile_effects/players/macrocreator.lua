MobileEffectLibrary.MacroCreator = 
{
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self._WindowID = "CreateMacro"..uuid()
        self.isPlayer = IsPlayerCharacter(self.ParentObj)
        self._Message = nil
        self._HotbarActions = {}
        self._MacroID = nil

        -- Make sure we're a player
        if( self.isPlayer ) then
            RemoveActionsFromSlotRange(70, 90, self.ParentObj)

            if not( self.ShowWindow(self) ) then
                self.ParentObj:SystemMessage("Unable to open macro window.", "info")
                EndMobileEffect(root)
                return false
            else
                -- Register window reponse listener
                RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, 
                function( user, id, fieldData )
                    self.WindowResponse( user, id, fieldData, self, root )
                end)

                RegisterEventHandler(EventType.Message,"MacroActionDropped", function()
                    self.ShowWindow(self, self._MacroID)
                end)
            end

        -- This is not a player, so we need to end mobile effect.
        else
            EndMobileEffect(root)
            return false
        end
    end,


    OnExitState = function(self,root)
        UnregisterEventHandler("",EventType.DynamicWindowResponse,self._WindowID)
        UnregisterEventHandler("",EventType.Message,"MacroActionPickedUp")
        UnregisterEventHandler("",EventType.Message,"MacroActionDropped")
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,
    
    ShowWindow = function(self, macroId)
        self._HotbarActions = self.ParentObj:GetObjVar("HotbarActions") or {}
        local LoadedMacro = HelperMacros.GetMacroForPlayer( self.ParentObj, macroId ) or {}

        --DebugTable( LoadedMacro )

        local dynWindow = DynamicWindow(self._WindowID,"Create Macro",800,388,200,200,"Default","TopLeft")
        self._MacroID = macroId or ""
        local x = 10
        local y = 10
        
        

        dynWindow:AddImage(0,0,"BasicWindow_Panel",200,295,"Sliced")
        dynWindow:AddImage(200,0,"BasicWindow_Panel",430,295,"Sliced")
        dynWindow:AddImage(630,0,"BasicWindow_Panel",150,295,"Sliced")

        dynWindow:AddLabel(x,y,"Saved Macros",0,0,24)
        -- Generate macro conditions
	    dynWindow:AddScrollWindow( self.GenerateSavedMacrosScrollWindow( self.ParentObj ) )


        x = 210

        -- Add labels
        dynWindow:AddLabel(x,y,"Macro Sequence",0,0,24)
        dynWindow:AddLabel(x+435,y,"Conditions",0,0,24)

        y = 40
        dynWindow:AddLabel(x,y+3,"Macro Name:",0,0,16)
        dynWindow:AddTextField(x+80,y,200,20,"macroName", LoadedMacro.MacroName or "")

        -- Adjust for labels
        y = 80

        -- Generate macro action rows and columns
        self.GenerateMacroActions( dynWindow, y, self )

        -- Generate macro conditions
	    dynWindow:AddScrollWindow( self.GenerateMacroConditionsScrollWindow( self.ParentObj ) )
    

        -- Add buttons
        x = 210
        y = 300
        --dynWindow:AddButton(x+130, y, "Test|"..tostring(macroId), "Test", 100, 0, "", "", false)
        dynWindow:AddLabel(5,y+8,"Macros can be clicked or moved to a hotbar to activate!",0,0,18)
        dynWindow:AddButton(x+130, y, "New|"..tostring(self._MacroID), "New", 100, 0, "", "", false)
        dynWindow:AddButton(x+240, y, "Save|"..tostring(self._MacroID), "Save", 100, 0, "", "", false)
        dynWindow:AddButton(x+350, y, "Clear", "Clear", 100, 0, "", "", false)
	    dynWindow:AddButton(x+460, y, "Close", "Close", 100, 0, "", "", true)

        self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)

        return true

    end,

    GenerateMacroActions = function( dynWindow, y, self )

        --DebugTable(self._HotbarActions)
        local hotbar_slot_count = 70

        for row=0, 2 do 
            x = 210
            for column=0, 6 do
                local mAction = self._HotbarActions[hotbar_slot_count] or {}
                mAction.Slot = hotbar_slot_count
                mAction.ID = mAction.ID or "macro_slot_"..tostring(hotbar_slot_count)
                mAction.ActionType = mAction.ActionType or "MacroSlot"
                mAction.Tooltip = mAction.Tooltip or "Drag action, item, or condition here."
                --mAction.Icon = mAction.Icon or "settings button"

                --DebugMessage("Adding to slot: " .. hotbar_slot_count .. " action: " .. mAction.ActionType)
                AddUserActionToSlot( mAction, self.ParentObj )
                dynWindow:AddHotbarAction(x,y + row*60,hotbar_slot_count,50,50,"",true)

                -- Needs to go up regardless
                x = x+60
                hotbar_slot_count = hotbar_slot_count + 1
                
            end
        end

        return 

    end,

    GenerateMacroConditionsScrollWindow = function( playerObj )
        local scrollWindow = ScrollWindow(640,40,120,250,50)
        local x = 0
        local y = 0
        local scrollElement = nil

        -- For each entry in MacroConditions
        for i=1, #MacroConditions do 
            x = 0

            if( i % 2 == 0 ) then 
                x = 50 
            else 
                scrollElement = ScrollElement()
            end
            --DebugMessage("Placing Macro Condition: " .. i .. " x:"..x.." y:"..y )
            
            scrollElement:AddUserAction(x,y,MacroConditions[i],50)
            
            if( i % 2 == 0 or  i == #MacroConditions ) then
                scrollWindow:Add(scrollElement)
            end

        end

        return scrollWindow
    end,

    GenerateSavedMacrosScrollWindow = function( playerObj )
        local scrollWindow = ScrollWindow(5,40,175,250,50)
        local x = 0
        local y = 0
        local scrollElement = nil
        local savedMacros = playerObj:GetObjVar("SavedMacros") or {}

        -- For each entry in MacroConditions
        for i, v in pairs(savedMacros) do 
            -- DebugMessage("Loading SavedMacro " .. tostring(i))
            scrollElement = ScrollElement()
            scrollElement:AddUserAction(x,y,savedMacros[i].MacroAction,50)
            scrollElement:AddButton(50, y, "Load|"..tostring(i), "Load", 50, 50, "", "", false)
            scrollElement:AddButton(100, y, "Remove|"..tostring(i), "Remove", 60, 50, "", "", false)
            scrollWindow:Add(scrollElement)
        end

        return scrollWindow
    end,

    

    DropActionOnSlot = function( slotnum )
        --DebugMessage("Something was dropped on slot# " .. slotnum)
    end,

    WindowResponse = function( user, buttonId, fieldData, self, root )
        --DebugMessage( "User: " .. tostring(user) .. " ButtonID: " .. buttonId )
        --DebugTable( fieldData )
        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        -- Accept button
        if( action == "New" ) then 
            if( HelperMacros.SaveMacroForPlayer( self.ParentObj, fieldData.macroName ) ) then
                self.ParentObj:SystemMessage("Macro [ " .. fieldData.macroName .. " ] successfully saved.")
                RemoveActionsFromSlotRange(70, 90, self.ParentObj)
                self.ShowWindow(self)
            end
        elseif( action == "Save" ) then 
            if( HelperMacros.SaveMacroForPlayer( self.ParentObj, fieldData.macroName, true ) ) then
                self.ParentObj:SystemMessage("Macro [ " .. fieldData.macroName .. " ] successfully saved.")
                RemoveActionsFromSlotRange(70, 90, self.ParentObj)
                self.ShowWindow(self)
            end
        elseif( action == "Load" ) then
            HelperMacros.LoadMacroForPlayer( self.ParentObj, tonumber(arg) )
            self.ShowWindow(self, tonumber(arg))
        elseif( action == "Remove" ) then
            HelperMacros.DeleteMacroForPlayer( self.ParentObj, tonumber(arg) )
            self.ShowWindow(self)
        elseif( action == "Clear" ) then
            RemoveActionsFromSlotRange(70, 90, self.ParentObj)
            self.ShowWindow(self)
        elseif( action == "Close" ) then 
            EndMobileEffect(root) 
        elseif( action == "Slot" ) then 
            self.DropActionOnSlot( arg )
        else  
            EndMobileEffect(root) 
        end

    end,
    
    _Args = {},
    _HotbarActions = {},

}
