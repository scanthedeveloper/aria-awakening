MobileEffectLibrary.PlayMacro = 
{
    OnEnterState = function(self,root,target,args)
        self._Args = args
        self._WindowID = "PlayMacro"
        self.isPlayer = IsPlayerCharacter(self.ParentObj)
        self._Args.MacroID = tonumber( self._Args.MacroID ) or nil
        self._Macro = HelperMacros.GetMacroForPlayer( self.ParentObj, self._Args.MacroID )
        self._Message = nil

        --DebugMessage(" Playing Macro " .. self._Args.MacroID)

        -- Make sure we are a player
        if( self.isPlayer ) then

            --DebugMessage( tostring( self._Macro ) )

            -- Do we have a macro?
            if( self._Macro == nil or self._Args.MacroID == nil ) then
                self._Message = "Invalid macro."
                EndMobileEffect(root)
                return false
            end

            if (self.ShowMacroWindow( self, root, nil, nil ) ) then

                -- Register window reponse listener
                RegisterSingleEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId ) self.WindowResponse( user, buttonId, self, root ) end)
                
                -- Register macro update event
                RegisterEventHandler(EventType.Message, "UpdateMacroWindow", function( stepNum, timesRan, endEffect ) self.UpdateMacroWindow(stepNum, timesRan, endEffect, self, root)  end)

                -- Smoke'em if you got'em!
                CallFunctionDelayed(TimeSpan.FromMilliseconds(50), 
                function()
                    HelperMacros.StartMacro( self.ParentObj, self._Args.MacroID )
                end)

            end
            
        end
    end,

    ShowMacroWindow = function(self, root, stepNum, timesRan, endEffect)
        local macroText = "[FF7F50]"..self._Macro.MacroName.."[-]"
        
        -- If we need to stop lets do that.
        if( endEffect ) then
            self._Message = "Macro "..macroText.." complete."
            EndMobileEffect(root)
            return
        end
        
        stepNum = stepNum or 0
        timesRan = timesRan or 0

        local dynWindow = DynamicWindow(self._WindowID, "Playing Macro",256,200,0,0,"")
        dynWindow:AddLabel(10,10,macroText,0,0,24)
        dynWindow:AddLabel(10,40,"On step " .. tostring(stepNum)..".",0,0,24)
        dynWindow:AddLabel(10,70,"Repeated " .. tostring(timesRan) .. " times.",0,0,24)
        dynWindow:AddButton(64-25,120,"Stop","Stop",157,27,"","",false,"")
        self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)

        return true

    end,

    WindowResponse = function( user, buttonId, self, root )
        
        --DebugMessage( "User: " .. tostring(user) .. " ButtonID: " .. buttonId )
        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        -- Stop Button
        if( action == "Stop" ) then 
            EndMobileEffect(root) 
        else 
            EndMobileEffect(root) 
        end

    end,

    UpdateMacroWindow = function( stepNum, timesRan, endEffect, self, root )  

        self.ShowMacroWindow( self, root, stepNum, timesRan, endEffect )

    end,


    OnExitState = function(self,root)
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
            self.ParentObj:SystemMessage(self._Message)
        end
        UnregisterEventHandler("", EventType.Message, "UpdateMacroWindow")
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,
    
    _Args = {},
    _WindowID = nil,

}
