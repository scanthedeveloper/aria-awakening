MobileEffectLibrary.ChallegeSystem = 
{
    OnEnterState = function(self,root,target,args)
        self._WindowID = ServerSettings.ChallengeSystem.WindowID
        ChallengeSystemHelper.DoUpkeep( self.ParentObj )
        
        -- Open the window
        self.OpenWindow(self, "HuntingChallenges")

        RegisterEventHandler(EventType.Message, "UpdateChallengeUI", function() self.OpenWindow(self, self._Mode) end)
        -- Register window reponse listener
        RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)
    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "UpdateChallengeUI")
        UnregisterEventHandler("",EventType.DynamicWindowResponse, self._WindowID)
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,

    -- Control when the window opens
    OpenWindow = function(self, mode)
        self._Mode = mode or self._Mode
        local dynWindow = DynamicWindow(self._WindowID, ServerSettings.ChallengeSystem.Title,1000,750,0,0,"TransparentDraggable")
        --SCAN CHANGED SEASON BACKGROUND
        dynWindow:AddImage(0,0,"Parchment_BG", 1000, 750)
        --dynWindow:AddImage(0,0,"season7_1", 1000, 750)
        local y = 0;

        y = 215;
        dynWindow:AddLabel(420,y+20,"[000000]Tokens (PVE)[-]",0,0,26,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddButton(550,y+20,"Nothing","CW",810,20,tostring(ChallengeSystemHelper.GetTokenAmount(self.ParentObj)) .. " / " .. tostring(ServerSettings.ChallengeSystem.PrizeThreeTokensNeeded),"",false,"Invisible")
        dynWindow:AddImage(550,y+20,"ProgressBarBG", 137, 19, "Sliced")
        dynWindow:AddImage(685,y+20,"ProgressBarBG", 137, 19, "Sliced")
        dynWindow:AddImage(820,y+20,"ProgressBarBG", 137, 19, "Sliced")
        
        local tokenPercent = (ChallengeSystemHelper.GetTokenAmount(self.ParentObj) / ServerSettings.ChallengeSystem.PrizeThreeTokensNeeded) * 100;
        local pixelSize = (408 / 100) * tokenPercent;
        dynWindow:AddImage(551,y+23,"SkillLevelProgressBar",pixelSize,12,"Sliced")
        
        dynWindow:AddImage(535,y+15,"seasonsMenu_BarNotch", 30, 30, "Sliced")
        dynWindow:AddImage(671,y+15,"seasonsMenu_BarNotch", 30, 30, "Sliced")
        dynWindow:AddImage(807,y+15,"seasonsMenu_BarNotch", 30, 30, "Sliced")
        dynWindow:AddImage(942,y+15,"seasonsMenu_BarNotch", 30, 30, "Sliced")

        -- Rewards
        dynWindow:AddImage(588,y-60,"DropHeaderBackground",60,60,"Sliced")
        dynWindow:AddImage(589,y-59,"SeasonsChest",58,58)
        if( not ChallengeSystemHelper.HasClaimedReward( self.ParentObj, ServerSettings.ChallengeSystem.PVE1Reward ) ) then
            local tooltipText = "Claim Reward\nRequires: 2,000 Tokens\n\nContents includes a Tier 1 reward crate.\n\nAll items will be placed in your backpack."
            dynWindow:AddButton(588,y-60,"Claim|"..ServerSettings.ChallengeSystem.PVE1Reward,"CW",60,60,tooltipText,"",false,"Invisible")
        else
            dynWindow:AddImage(588,y-60,"Checkmark",16,16,"Sliced")		
        end
        
        

        dynWindow:AddImage(714,y-80,"DropHeaderBackground",80,80,"Sliced")
        dynWindow:AddImage(715,y-79,"SeasonsChest",80,80)
        if( not ChallengeSystemHelper.HasClaimedReward( self.ParentObj, ServerSettings.ChallengeSystem.PVE2Reward ) ) then
            local tooltipText = "Claim Reward\nRequires: 3,000 Tokens\n\nContents includes a Tier 2 reward crate.\n\nAll items will be placed in your backpack."
            dynWindow:AddButton(714,y-80,"Claim|"..ServerSettings.ChallengeSystem.PVE2Reward,"CW",80,80,tooltipText,"",false,"Invisible")
        else
            dynWindow:AddImage(714,y-80,"Checkmark",16,16,"Sliced")		
        end

        

        dynWindow:AddImage(840,y-100,"DropHeaderBackground",100,100,"Sliced")
        dynWindow:AddImage(843,y-92,"SeasonsChest",96,96)
        if( not ChallengeSystemHelper.HasClaimedReward( self.ParentObj, ServerSettings.ChallengeSystem.PVE3Reward ) ) then
            local tooltipText = "Claim Reward\nRequires: 4,000 Tokens\n\nContents includes a Tier 3 reward crate.\n\nAll items will be placed in your backpack."
            dynWindow:AddButton(840,y-100,"Claim|"..ServerSettings.ChallengeSystem.PVE3Reward,"CW",100,100,tooltipText,"",false,"Invisible")
        else
            dynWindow:AddImage(840,y-100,"Checkmark",16,16,"Sliced")
        end
        

        y = 360;
        dynWindow:AddLabel(430,y+20,"[000000]Skulls (PVP)[-]",0,0,26,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddButton(550,y+20,"Nothing","CW",810,20,tostring(ChallengeSystemHelper.GetTokenAmount(self.ParentObj, true)) .. " / " .. tostring(ServerSettings.ChallengeSystem.PrizeThreeTokensNeeded),"",false,"Invisible")
        dynWindow:AddImage(550,y+20,"ProgressBarBG", 137, 19, "Sliced")
        dynWindow:AddImage(685,y+20,"ProgressBarBG", 137, 19, "Sliced")
        dynWindow:AddImage(820,y+20,"ProgressBarBG", 137, 19, "Sliced")

        local tokenPercent = (ChallengeSystemHelper.GetTokenAmount(self.ParentObj, true) / ServerSettings.ChallengeSystem.PrizeThreeTokensNeeded) * 100;
        local pixelSize = (408 / 100) * tokenPercent;
        dynWindow:AddImage(551,y+23,"SkillLevelProgressBar",pixelSize,12,"Sliced")
        
        dynWindow:AddImage(535,y+15,"seasonsMenu_BarNotch", 30, 30, "Sliced")
        dynWindow:AddImage(671,y+15,"seasonsMenu_BarNotch", 30, 30, "Sliced")
        dynWindow:AddImage(807,y+15,"seasonsMenu_BarNotch", 30, 30, "Sliced")
        dynWindow:AddImage(942,y+15,"seasonsMenu_BarNotch", 30, 30, "Sliced")


        -- Rewards
        dynWindow:AddImage(588,y-60,"DropHeaderBackground",60,60,"Sliced")
        dynWindow:AddImage(589,y-59,"SeasonsChest",58,58)
        if( not ChallengeSystemHelper.HasClaimedReward( self.ParentObj, ServerSettings.ChallengeSystem.PVP1Reward ) ) then
            local tooltipText = "Claim Reward\nRequires: 2,000 Skulls\n\nContents includes a Tier 1 reward crate.\n\nAll items will be placed in your backpack."
            dynWindow:AddButton(588,y-60,"Claim|"..ServerSettings.ChallengeSystem.PVP1Reward,"CW",60,60,tooltipText,"",false,"Invisible")
        else
            dynWindow:AddImage(588,y-60,"Checkmark",16,16,"Sliced")		
        end
        

        dynWindow:AddImage(714,y-80,"DropHeaderBackground",80,80,"Sliced")
        dynWindow:AddImage(715,y-79,"SeasonsChest",80,80)
        if( not ChallengeSystemHelper.HasClaimedReward( self.ParentObj, ServerSettings.ChallengeSystem.PVP2Reward ) ) then
            local tooltipText = "Claim Reward\nRequires: 3,000 Skulls\n\nContents includes a Tier 2 reward crate.\n\nAll items will be placed in your backpack."
            dynWindow:AddButton(714,y-80,"Claim|"..ServerSettings.ChallengeSystem.PVP2Reward,"CW",80,80,tooltipText,"",false,"Invisible")
        else
            dynWindow:AddImage(714,y-80,"Checkmark",16,16,"Sliced")		
        end
        

        dynWindow:AddImage(840,y-100,"DropHeaderBackground",100,100,"Sliced")
        dynWindow:AddImage(843,y-92,"SeasonsChest",96,96)
        if( not ChallengeSystemHelper.HasClaimedReward( self.ParentObj, ServerSettings.ChallengeSystem.PVP3Reward ) ) then
            local tooltipText = "Claim Reward\nRequires: 4,000 Tokens\n\nContents includes a Tier 3 reward crate. \n\nAll items will be placed in your backpack."
            dynWindow:AddButton(840,y-100,"Claim|"..ServerSettings.ChallengeSystem.PVP3Reward,"CW",100,100,tooltipText,"",false,"Invisible")
        else
            dynWindow:AddImage(840,y-100,"Checkmark",16,16,"Sliced")
        end

        y = 444;

        --dynWindow:AddImage(0,400,"DropHeaderBackground",1000,340,"Sliced")

        -- Left Scrollable
        local scrollWindowLeft = ScrollWindow(12,y,460,291,97)

        AddTabMenu(dynWindow,
        {
            ActiveTab = mode,
            OffsetX = 15,
            OffsetY = y-23,
            Buttons = {
                { Text = "Hunting", TabId = "HuntingChallenges" },
                { Text = "Crafting", TabId = "CraftingChallenges" },
                { Text = "Gathering", TabId = "GatheringChallenges" },
            }
        })

        if( AllQuests[mode] ) then
            for i=1, #AllQuests[mode] do 
                local questData = AllQuests[mode][i]
                local playerQuestData = Quests.GetQuestData( self.ParentObj, questData.QuestData.Key )
                if( playerQuestData.Count == nil ) then
                    Quests.InitEventTracker( self.ParentObj, mode, i )
                    playerQuestData = Quests.GetQuestData( self.ParentObj, questData.QuestData.Key )
                end

                playerQuestData.Needed = questData.QuestData.Count or playerQuestData.Needed
                local progress = "( Complete )"
                if( playerQuestData.Count ~= nil and playerQuestData.Needed ~= nil ) then
                    progress = "( "..playerQuestData.Count.." / "..playerQuestData.Needed.." )"
                end

                ChallengeSystemHelper.DisplayUIElement( scrollWindowLeft, {
                    Title = questData.Name,
                    Progress = progress,
                    Description = questData.Instructions,
                    TokensAwarded = questData.QuestData.Tokens or 0,
                    IsComplete = not( Quests.IsActive(self.ParentObj, mode, i ) ),
                    RepeatHours = ( questData.Cooldown and questData.Cooldown.TotalHours ) or nil
                } )
                
            end
        end
        
        dynWindow:AddScrollWindow(scrollWindowLeft)

        -- Right Scoll
        local scrollWindowRight = ScrollWindow(513,y,460,291,97)
        AddTabMenu(dynWindow,
        {
            ActiveTab = "PVP",
            OffsetX = 525,
            OffsetY = y-23,
            Buttons = {
                { Text = "PVP", TabId = self._Mode },
            }
        })

        mode = "PVPChallenges"
        if( AllQuests[mode] ) then
            for i=1, #AllQuests[mode] do 
                local questData = AllQuests[mode][i]
                local playerQuestData = Quests.GetQuestData( self.ParentObj, questData.QuestData.Key )
                playerQuestData.Needed = questData.QuestData.Count or playerQuestData.Needed
                local progress = "( Complete )"
                if( playerQuestData.Count and playerQuestData.Needed ) then
                    progress = "( "..playerQuestData.Count.." / "..playerQuestData.Needed.." )"
                end

                ChallengeSystemHelper.DisplayUIElement( scrollWindowRight, {
                    Title = questData.Name,
                    Progress = progress,
                    Description = questData.Instructions,
                    TokensAwarded = questData.QuestData.PVPTokens or questData.QuestData.Tokens or 0 ,
                    IsPVP = ( questData.QuestData.PVPTokens ~= nil ),
                    IsComplete = not( Quests.IsActive(self.ParentObj, mode, i ) ),
                    RepeatHours = ( questData.Cooldown and questData.Cooldown.TotalHours ) or nil
                } )
                
            end
        end


        dynWindow:AddScrollWindow(scrollWindowRight)

        dynWindow:AddImage(0,0,"seasonsMenu_Frame",1000,750,"Sliced")

        dynWindow:AddButton(
                975, --(number) x position in pixels on the window
                3, --(number) y position in pixels on the window
                "Close", --(string) return id used in the DynamicWindowResponse event
                "", --(string) text in the button (defaults to empty string)
                0, --(number) width of the button (defaults to width of text)
                0,--(number) height of the button (default decided by type of button)
                "", --(string) mouseover tooltip for the button (default blank)
                "", --(string) server command to send on button click (default to none)
                false, --(boolean) should the window close when this button is clicked? (default true)
                "CloseSquare", --(string) button type (default "Default")
                buttonState --(string) button state (optional, valid options are default,pressed,disabled)
                --customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
            )


        --dynWindow:AddImage(140,-25,"ChallengeSystemBanner_Orange",720,60,"Sliced")
        dynWindow:AddLabel(550,20,"                "..ServerSettings.ChallengeSystem.SeasonTitle,0,0,44,"", false, true, "SpectralSC-SemiBold")

        self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)
    end,


    -- Register window reponse listener
    WindowResponse = function( user, buttonId, fieldData, self, root )
        
        self._FieldData = fieldData

        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        -- Picked a new tab
        if( action == "SelectTab" ) then 
            self.OpenWindow(self, arg)
        elseif( action == "Claim" and self._ClaimingReward == false ) then
            self._ClaimingReward = true
            ChallengeSystemHelper.TryClaimReward( self.ParentObj, arg )
            CallFunctionDelayed(TimeSpan.FromMilliseconds(100), function()
                self.OpenWindow(self, "HuntingChallenges")
                self._ClaimingReward = false
            end)
        else
            EndMobileEffect(root)
        end
    end,

    _Args = {},
    _WindowID = nil,
    _FieldData = nil,
    _Mode = nil,
    _ClaimingReward = false,
}
