MobileEffectLibrary.Vitality = 
{
    PersistDeath = true,
    OnEnterState = function(self,root,target,args)
        if( not IsPlayerCharacter(self.ParentObj) ) then
            EndMobileEffect(root)
            return false 
        end

        self._VitalityAmount = args.Vitality or GetCurVitality( self.ParentObj )
        self.SetEffectVariables(self)
        self.SetMobileMods(self)

    end,

    OnStack = function(self,root,target,args)
        self.OnEnterState( self, root, target, args )
	end,

    OnExitState = function(self,root)
        RemoveBuffIcon(self.ParentObj, "VitalityBuffIcon")
        self._StatIncreaseAmount = nil
        SetMobileMod(self.ParentObj,"StrengthTimes", "VitalityStatBuff", self._StatIncreaseAmount)
        SetMobileMod(self.ParentObj,"AgilityTimes", "VitalityStatBuff", self._StatIncreaseAmount)
        SetMobileMod(self.ParentObj,"IntelligenceTimes", "VitalityStatBuff", self._StatIncreaseAmount)
        SetMobileMod(self.ParentObj,"ConstitutionTimes", "VitalityStatBuff", self._StatIncreaseAmount)
        SetMobileMod(self.ParentObj,"WisdomTimes", "VitalityStatBuff", self._StatIncreaseAmount)
        SetMobileMod(self.ParentObj,"WillTimes", "VitalityStatBuff", self._StatIncreaseAmount)
    end,
    
    SetEffectVariables = function( self )
        local VitalityInt = 0
        local VitalityRanges = { 25, 50, 75, 100 }
        local VitalityBonus = { nil, 0.02, 0.05, 0.10 }
        -- SCAN Modified Vitality Icon
        local VitalityIcons = { "Fire Mastery", "Fire Mastery", "Fire Mastery", "Fire Mastery"}
        local VitalityBuffName = { "Vitality", "Vitality I", "Vitality II", "Vitality III" }
        local VitalityBuffDescription = {
            "Your vitality is too low to gain any bonuses.", 
            "You are gaining a 2% bonus on your base statistics.", 
            "You are gaining a 5% bonus on your base statistics.", 
            "You are gaining a 10% bonus on your base statistics.", 
        }

        -- 25
        if( self._VitalityAmount <= VitalityRanges[1] ) then
            VitalityInt = 1
        -- 26 - 50
        elseif( self._VitalityAmount > VitalityRanges[1] and self._VitalityAmount <= VitalityRanges[2] ) then
            VitalityInt = 2
        -- 51 - 75
        elseif( self._VitalityAmount > VitalityRanges[2] and self._VitalityAmount <= VitalityRanges[3] ) then
            VitalityInt = 3
        -- 76 - 100
        elseif( self._VitalityAmount > VitalityRanges[3] and self._VitalityAmount <= VitalityRanges[4] ) then
            VitalityInt = 4
        end

        self._StatIncreaseAmount = VitalityBonus[VitalityInt]
        self._BuffIcon = VitalityIcons[VitalityInt]
        self._BuffName = VitalityBuffName[VitalityInt]
        self._BuffTooltip = VitalityBuffDescription[VitalityInt]

    end,

    SetMobileMods = function(self)
        AddBuffIcon(self.ParentObj, "VitalityBuffIcon", self._BuffName, self._BuffIcon, self._BuffTooltip)
        SetMobileMod(self.ParentObj,"StrengthTimes", "VitalityStatBuff", self._StatIncreaseAmount)
        SetMobileMod(self.ParentObj,"AgilityTimes", "VitalityStatBuff", self._StatIncreaseAmount)
        SetMobileMod(self.ParentObj,"IntelligenceTimes", "VitalityStatBuff", self._StatIncreaseAmount)
        SetMobileMod(self.ParentObj,"ConstitutionTimes", "VitalityStatBuff", self._StatIncreaseAmount)
        SetMobileMod(self.ParentObj,"WisdomTimes", "VitalityStatBuff", self._StatIncreaseAmount)
        SetMobileMod(self.ParentObj,"WillTimes", "VitalityStatBuff", self._StatIncreaseAmount)
    end,

    _BuffIcon = nil,
    _BuffName = nil,
    _BuffTooltip = nil,
    _StatIncreaseAmount = 0,
    _VitalityAmount = 0,
}
