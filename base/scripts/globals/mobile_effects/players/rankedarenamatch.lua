MobileEffectLibrary.RankedArenaMatch = 
{
    ForceDismount = true,
    OnEnterState = function(self,root,target,args)
        self._ArenaMaster = target
        self._MatchData = args
        self._HUDArgs = self._MatchData.HUDArgs
        self._Team = nil

        -- Make sure we found the township board near us.
        if( self._ArenaMaster == nil ) then
            self._Message = "You are too far away to do that."
            EndMobileEffect(root)
            return false
        end

        SetCurHealth(self.ParentObj,GetMaxHealth(self.ParentObj))
        SetCurMana(self.ParentObj,GetMaxMana(self.ParentObj))
        SetCurStamina(self.ParentObj,GetMaxStamina(self.ParentObj))

        -- Reset arena potion limits
        self.ParentObj:SetObjVar("ArenaPotionsUsed", 0)

        self.ParentObj:SendMessage("ClearTarget")
        
        UpdateDynamicEventHUD(self.ParentObj, self._HUDArgs)

        if( RankedArena.IsPlayerOnTeam( self._MatchData.TeamA, self.ParentObj ) ) then
            self.ParentObj:SetWorldPosition( RankedArena.StartingPositions.TeamA )
            self._Team = "TeamA"
            self.ParentObj:SetObjVar("ArenaTeam", self._Team)
            self.ParentObj:PlayEffectWithArgs("ArenaFlagBlueEffect",0.0,"Bone=Head")
        elseif( RankedArena.IsPlayerOnTeam( self._MatchData.TeamB, self.ParentObj ) ) then
            self.ParentObj:SetWorldPosition( RankedArena.StartingPositions.TeamB )
            self._Team = "TeamB"
            self.ParentObj:SetObjVar("ArenaTeam", self._Team)
            self.ParentObj:PlayEffectWithArgs("ArenaFlagRedEffect",0.0,"Bone=Head")
        end

        -- Resgister logout listener
        RegisterSingleEventHandler(EventType.UserLogout,"",function (...)EndMobileEffect(root)end)
        -- Register arena message listener
        RegisterEventHandler(EventType.Message, "RankedArenaMessage",  function ( args ) self.ProcessArenaMessage(self, root, args) end)
        self.ParentObj:PlayObjectSound("event:/ui/gong", false, 0, false, false)
    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "RankedArenaMessage")
        self.ParentObj:SendMessage("ApplyMilitiaBuffs")

        if( self._Team == "TeamA" ) then
            self.ParentObj:StopEffect("ArenaFlagBlueEffect")
        elseif( self._Team == "TeamB" ) then
            self.ParentObj:StopEffect("ArenaFlagRedEffect")
        end

        self.ParentObj:DelObjVar("ArenaTeam")
        self.ParentObj:DelObjVar("ArenaPotionsUsed")
        self._ArenaMaster:SendMessage("RankedArenaMessage", { Action = "PlayerDied", Player = self.ParentObj });
        
        if( IsDead(self.ParentObj) ) then
            CallFunctionDelayed(TimeSpan.FromMilliseconds(100),function ( ... )
                local corpse = self.ParentObj:GetObjVar("CorpseObject")
                if ( corpse and corpse:IsValid() ) then
                    corpse:DelObjVar("lootable")
                end
            end)
            
            CallFunctionDelayed(TimeSpan.FromSeconds(10),function ( ... )
                PlayerResurrect( self.ParentObj, self._ArenaMaster, self.ParentObj:GetObjVar("CorpseObject"), nil )
            end)
        else
            if (IsInCombat(self.ParentObj)) then
                self.ParentObj:SendMessage("EndCombatMessage")
            end
            self.ParentObj:SetWorldPosition( RankedArena.StartingPositions.End )
        end

        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end

        CloseDynamicEventHUD( self.ParentObj )
    end,

    ProcessArenaMessage = function( self, root, args )
        --DebugMessage("ProcessArenaMessage: " .. args.Action)
        if( args.Action == "MatchEnded" ) then
            self._Message = "The match has ended."
            EndMobileEffect(root)
        elseif( args.Action == "CancelMatch" ) then
            self._Message = "The match has been cancelled."
            EndMobileEffect(root)
        end
    end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
    end,
    
    AiPulse = function(self,root)
        UpdateDynamicEventHUD(self.ParentObj, self._HUDArgs)
        if( self._Team == "TeamA" ) then
            self.ParentObj:PlayEffectWithArgs("ArenaFlagBlueEffect",0.0,"Bone=Head")
        elseif( self._Team == "TeamB" ) then
            self.ParentObj:PlayEffectWithArgs("ArenaFlagRedEffect",0.0,"Bone=Head")
        end
    end,

}
