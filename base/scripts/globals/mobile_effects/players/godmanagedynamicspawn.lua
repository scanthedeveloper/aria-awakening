MobileEffectLibrary.GodManageDynamicSpawn = 
{
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self._WindowID = "ManageDynamicSpawner"
        self._Spawner = target or nil
        self._SpawnData = {}

        -- Make sure we're a player
        if( IsGod( self.ParentObj ) ) then
            -- Register window reponse listener
            RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)

            RegisterEventHandler(EventType.Message, "GetSpawnDataAdmin", function( spawnData )
                self._SpawnData = spawnData
                self.OpenWindow( self )    
            end)

            self._Spawner:SendMessage("GetSpawnData", self.ParentObj, true)
        else
            EndMobileEffect(root)
            return false
        end

    end,


    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "GetSpawnDataAdmin")
        UnregisterEventHandler("",EventType.DynamicWindowResponse, self._WindowID)
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,
    
    OpenWindow = function(self)
        local dynWindow = DynamicWindow(self._WindowID, "Manage Dynamic Spawn",400,600,0,0,"")
        local yPos = 10
        dynWindow:AddLabel(10,yPos,"Title: "..self._SpawnData.Title,0,0,24)
        yPos = yPos + 35
        dynWindow:AddLabel(10,yPos,"Stage/Max: "..self._SpawnData.Stage.." / "..self._SpawnData.MaxStage,0,0,24)
        yPos = yPos + 35
        dynWindow:AddLabel(10,yPos,"PlayersInRange: "..self._SpawnData.PlayersInRange,0,0,24)
        yPos = yPos + 35
        dynWindow:AddLabel(10,yPos,"TotalMonstersSpawned: "..self._SpawnData.TotalMonstersSpawned,0,0,24)
        yPos = yPos + 35
        dynWindow:AddLabel(10,yPos,"TotalPlayers: "..self._SpawnData.UniquePlayerCount,0,0,24)
        yPos = yPos + 35
        dynWindow:AddLabel(10,yPos,"IsSpawning: "..tostring(self._SpawnData.IsSpawning),0,0,24)
        yPos = yPos + 35
        dynWindow:AddLabel(10,yPos,"IsPaused: "..tostring(self._SpawnData.IsPaused),0,0,24)
        yPos = yPos + 35
        yPos = yPos + 35
        dynWindow:AddLabel(10,yPos,"SpawnPower: "..tostring(self._SpawnData.SpawnPower),0,0,24)
        yPos = yPos + 35
        dynWindow:AddLabel(10,yPos,"KillScore: "..tostring(self._SpawnData.KillScore),0,0,24)
        yPos = yPos + 35
        dynWindow:AddLabel(10,yPos,"PlayerPower: "..tostring(self._SpawnData.PlayerPower),0,0,24)
        yPos = yPos + 35
        dynWindow:AddLabel(10,yPos,"AdjustedPlayerPower: "..tostring(self._SpawnData.AdjustPlayerPower),0,0,24)

        dynWindow:AddButton(10,510,"Start","Start",157,27,"","",false,"")
        dynWindow:AddButton(210,510,"Stop","Stop",157,27,"","",false,"")
        self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)

    end,

    WindowResponse = function( user, buttonId, fieldData, self, root )
        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        -- Accept button
        if( action == "Stop" ) then 
            self._Spawner:SendMessage("Stop")
        elseif( action == "Start" ) then 
            self._Spawner:SendMessage("Start")
        else 
            EndMobileEffect(root) 
        end

    end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
	end,

    AiPulse = function(self,root) 
        self._Spawner:SendMessage("GetSpawnData", self.ParentObj, true)
    end,

    _WindowID = nil,
}
