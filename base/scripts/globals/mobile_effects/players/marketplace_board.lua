MobileEffectLibrary.MarketplaceBoard = 
{
    OnEnterState = function(self,root,target,args)
        self._WindowID = "TownshipBoard" --..uuid()
        self._MarketplaceBoardObj = target

        -- Make sure we found the township board near us.
        if( self._MarketplaceBoardObj == nil ) then
            self._Message = "You are not close enough to the township board."
            EndMobileEffect(root)
            return false
        end
        
        -- Determine if the board is faction based
        local isFaction, factionName = MarketplaceBoard.GetFactionData( self._MarketplaceBoardObj )
        self._IsFactionBased = isFaction
        self._Faction = factionName

        -- If the board is faction based we need to set the name accordingly
        if( self._IsFactionBased ) then

            for i=1, #ServerSettings.Militia.Militias do
                if( ServerSettings.Militia.Militias[i].Town == self._Faction ) then
                    self._FactionVars = ServerSettings.Militia.Militias[i]
                end
            end

            -- Remove as anyone can use any township board now.
            --[[
            if not( TownshipsHelper.IsPlayerInTownship( self.ParentObj, self._FactionVars.Town ) ) then
                self._Message = "You must be a member of the " .. self._FactionVars.Town .. " township to use this."
                EndMobileEffect(root)
                return false
            end
            ]]

            self._BoardName = self._FactionVars.Town .. " Township Board"
        end

        -- Open the window
        self.OpenWindow(self)

        -- Register window reponse listener
        RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)

        -- Register donate item listener
        RegisterEventHandler(EventType.ClientTargetGameObjResponse, "select_donate_item", function( target, user ) self.TargetDonateItem(target, user, self, root) end)

        -- Purchase listener
        RegisterEventHandler(EventType.Message, "ConsumeResourceResponse", 
        function (success,transactionId,template)
            if (transactionId == "PurchaseMarketplaceItem") then
                if not(success) then
                    self.ParentObj:SystemMessage("Purchase failed.","info")
                else
                    local count = MarketplaceBoard.StockStackCountsToDonate[template] or 1

                    -- See if we need to give them a different item based on their faction
                    if( self._IsFactionBased and MarketplaceBoard.FactionPurchaseConversionList[template] ) then
                        template = MarketplaceBoard.FactionPurchaseConversionList[template][self._Faction] or template
                    end

                    if( count > 1 ) then
                        Create.Stack.InBackpack( template, self.ParentObj, count)
                    else
                        Create.InBackpack( template, self.ParentObj )
                    end
                    
                    self.ParentObj:SystemMessage("You sucessfully purchase the item.","info")
                    MarketplaceBoard.AdjustStockpileCountForTemplate( template, self._MarketplaceBoardObj, nil, -1 )
                    self.OpenWindow(self, "purchase")
                end
            end
        end)

    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("",EventType.DynamicWindowResponse, self._WindowID)
        UnregisterEventHandler("",EventType.Message, "ConsumeResourceResponse")
        UnregisterEventHandler("", EventType.ClientTargetGameObjResponse, "select_donate_item")
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,

    CheckBoardRange = function( self, root )
        if( not self._MarketplaceBoardObj or self.ParentObj:DistanceFrom(self._MarketplaceBoardObj) > 10 ) then
            self._Message = "Too far from marketboard."
            EndMobileEffect(root)
            return false
        else
            return true
        end
    end,

    -- Control when the window opens
    OpenWindow = function(self, mode, category)
        -- Get stockpile data
        self.GetStockpileData(self)

        -- Get the player's faction currency
        self._FactionCurrency =  MarketplaceBoard.GetFactionCurrency(self.ParentObj,self._Faction) or 0

        -- Load top bar
        local dynWindow = DynamicWindow(self._WindowID, self._BoardName,540,600,0,0,"Parchment")
        dynWindow:AddButton(25,30,"Donate","Donated Items",173,29,"Shows a list of items, along with their values, which can donated to this trade board.","",false,"ParchmentButton")
        dynWindow:AddButton(315,30,"PurchaseItems","Purchasable Items",173,29,"Shows a list of items, along with their cost, which can be purchased from this trade board.","",false,"ParchmentButton")
        --dynWindow:AddButton(480,-21,"Help","?",20,20,"","",false,"")

        dynWindow:AddImage(5,90,"Prestige_Divider",510,0,"Sliced")
        dynWindow:AddImage(5,500,"Prestige_Divider",510,0,"Sliced")

        if( self._IsFactionBased ) then
            dynWindow:AddImage(220,5,self._FactionVars.IconTown,75,75,"")
        end

        dynWindow:AddButton(295,503,"DonateItem","Donate Item",227,55,"Click to start donating items.","",false,"GoldRedButton")
        
        if( self._IsFactionBased ) then
            self._CurrencyType = QuestsLeague.LeagueCurrencyName[self._Faction]
            dynWindow:AddLabel(15,520,self._CurrencyType..": ",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            local labelXOffset = (string.len(self._CurrencyType) * 12) + 15
            dynWindow:AddLabel(labelXOffset,520,"[483D8B]"..self._FactionCurrency.."[-]",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        end

        -- Load help
        if( mode == "help" ) then
            dynWindow:AddLabel(10,100,"You can [F0E68C]donate[-] items by dragging and dropping them \non the window with the donation tab opened.",0,0,24)
        
        -- Load purchases
        elseif( mode == "purchase" ) then
            -- category buttons
            
            local catBtnY = 130
            local catBtnX = 0

            

            --dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategorySelectionActive",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"empty_circle",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategory_Torso",50,50,"Sliced")
            dynWindow:AddButton(catBtnX,catBtnY,"Purchase|CraftedArmor","CA",50,50,"Crafted Armor","",false,"Invisible")
            
            catBtnY = catBtnY + 60
            --dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategorySelectionActive",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"empty_circle",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategory_Bashing",50,50,"Sliced")
            dynWindow:AddButton(catBtnX,catBtnY,"Purchase|CraftedWeapons","CW",50,50,"Crafted Weapons","",false,"Invisible")
            
            catBtnY = catBtnY + 60
            --dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategorySelectionActive",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"empty_circle",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategory_Iron",50,50,"Sliced")
            dynWindow:AddButton(catBtnX,catBtnY,"Purchase|CraftMaterials","CM",50,50,"Crafted Materials","",false,"Invisible")
            
            catBtnY = catBtnY + 60
            --dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategorySelectionActive",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"empty_circle",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategory_Crystals",50,50,"Sliced")
            dynWindow:AddButton(catBtnX,catBtnY,"Purchase|RawMaterials","RM",50,50,"Raw Materials","",false,"Invisible")
            
            catBtnY = catBtnY + 60
            --dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategorySelectionActive",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"empty_circle",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategory_BuffsPotions",50,50,"Sliced")
            dynWindow:AddButton(catBtnX,catBtnY,"Purchase|WorldDrops","WD",50,50,"World Drops","",false,"Invisible")

            local scrollWindow = ScrollWindow(50,100,455,400,24)

            -- Fabrication armor
            if( MarketplaceBoard.AllowAllFabricationArmor and (category == nil or category == "CraftedArmor") ) then
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "Armor", "FabricationSkill" )
            end

            -- Metalsmith armor
            if( MarketplaceBoard.AllowAllMetalsmithArmor and (category == nil or category == "CraftedArmor") ) then
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "Armor", "MetalsmithSkill" )
            end

            -- Woodsmith weapons
            if( MarketplaceBoard.AllowAllWoodsmithWeapons and (category == nil or category == "CraftedWeapons") ) then
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "Weapons", "WoodsmithSkill" )
            end

            -- MetalsmithSkill weapons
            if( MarketplaceBoard.AllowAllWoodsmithWeapons and (category == nil or category == "CraftedWeapons") ) then
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "Weapons", "MetalsmithSkill" )
            end

            -- Fabrication crafted materials
            if( MarketplaceBoard.AllowAllFabricationResources and (category == nil or category == "CraftMaterials") ) then
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "Resources", "FabricationSkill" )
            end

            -- Metalsmith crafted materials
            if( MarketplaceBoard.AllowAllMetalsmithResources and (category == nil or category == "CraftMaterials") ) then
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "Resources", "MetalsmithSkill" )
            end

            -- Woodsmith crafted materials
            if( MarketplaceBoard.AllowAllWoodsmithResources and (category == nil or category == "CraftMaterials") ) then
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "Resources", "WoodsmithSkill" )
            end

            -- Raw materials
            if( (category == nil or category == "RawMaterials") ) then
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "Materials", "WoodsmithSkill" )
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "Materials", "FabricationSkill" )
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "Materials", "MetalsmithSkill" )
            end

            -- World Drops
            if( (category == nil or category == "WorldDrops") ) then
                self.GeneratePurchaseListFromStockpile( self, scrollWindow, "All", "WorldDrops" )
            end

            dynWindow:AddScrollWindow(scrollWindow)

        -- Load donations
        else

            -- category buttons
            local catBtnY = 130
            local catBtnX = 0
            
            --dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategorySelectionActive",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"empty_circle",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategory_Torso",50,50,"Sliced")
            dynWindow:AddButton(catBtnX,catBtnY,"Donate|CraftedArmor","CA",50,50,"Crafted Armor","",false,"Invisible")
            
            catBtnY = catBtnY + 60
            --dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategorySelectionActive",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"empty_circle",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategory_Bashing",50,50,"Sliced")
            dynWindow:AddButton(catBtnX,catBtnY,"Donate|CraftedWeapons","CW",50,50,"Crafted Weapons","",false,"Invisible")
            
            catBtnY = catBtnY + 60
            --dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategorySelectionActive",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"empty_circle",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategory_Iron",50,50,"Sliced")
            dynWindow:AddButton(catBtnX,catBtnY,"Donate|CraftMaterials","CM",50,50,"Crafted Materials","",false,"Invisible")
            
            catBtnY = catBtnY + 60
            --dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategorySelectionActive",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"empty_circle",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategory_Crystals",50,50,"Sliced")
            dynWindow:AddButton(catBtnX,catBtnY,"Donate|RawMaterials","RM",50,50,"Raw Materials","",false,"Invisible")
            
            catBtnY = catBtnY + 60
            --dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategorySelectionActive",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"empty_circle",50,50,"Sliced")
            dynWindow:AddImage(catBtnX,catBtnY,"CraftingCategory_BuffsPotions",50,50,"Sliced")
            dynWindow:AddButton(catBtnX,catBtnY,"Donate|WorldDrops","WD",50,50,"World Drops","",false,"Invisible")

            local scrollWindow = ScrollWindow(50,100,455,400,24)

            -- Fabrication armor
            if( MarketplaceBoard.AllowAllFabricationArmor and (category == nil or category == "CraftedArmor") ) then
                self.GenerateDonateListFromStockpile( self, scrollWindow, "Armor", "FabricationSkill" )
            end

            -- Metalsmith armor
            if( MarketplaceBoard.AllowAllMetalsmithArmor and (category == nil or category == "CraftedArmor") ) then
                self.GenerateDonateListFromStockpile( self, scrollWindow, "Armor", "MetalsmithSkill" )
            end

            -- Woodsmith weapons
            if( MarketplaceBoard.AllowAllWoodsmithWeapons and (category == nil or category == "CraftedWeapons") ) then
                self.GenerateDonateListFromStockpile( self, scrollWindow, "Weapons", "WoodsmithSkill" )
            end

            -- MetalsmithSkill weapons
            if( MarketplaceBoard.AllowAllWoodsmithWeapons and (category == nil or category == "CraftedWeapons") ) then
                self.GenerateDonateListFromStockpile( self, scrollWindow, "Weapons", "MetalsmithSkill" )
            end

            -- Fabrication crafted materials
            if( MarketplaceBoard.AllowAllFabricationResources and (category == nil or category == "CraftMaterials") ) then
                self.GenerateDonateListFromStockpile( self, scrollWindow, "Resources", "FabricationSkill" )
            end

            -- Metalsmith crafted materials
            if( MarketplaceBoard.AllowAllMetalsmithResources and (category == nil or category == "CraftMaterials") ) then
                self.GenerateDonateListFromStockpile( self, scrollWindow, "Resources", "MetalsmithSkill" )
            end

            -- Woodsmith crafted materials
            if( MarketplaceBoard.AllowAllWoodsmithResources and (category == nil or category == "CraftMaterials") ) then
                self.GenerateDonateListFromStockpile( self, scrollWindow, "Resources", "WoodsmithSkill" )
            end

            -- Raw materials
            if( (category == nil or category == "RawMaterials") ) then
                self.GenerateDonateListFromStockpile( self, scrollWindow, "Materials", "WoodsmithSkill" )
                self.GenerateDonateListFromStockpile( self, scrollWindow, "Materials", "FabricationSkill" )
                self.GenerateDonateListFromStockpile( self, scrollWindow, "Materials", "MetalsmithSkill" )
            end

            -- World Drops
            if( (category == nil or category == "WorldDrops") ) then
                self.GenerateDonateListFromStockpile( self, scrollWindow, "All", "WorldDrops" )
            end

            dynWindow:AddScrollWindow(scrollWindow)
            dynWindow:AddButton(60,100,"DonateCarriedObject","",435,390,"You can drop items or containers of items here to donate them.","",false,"Invisible")
        end

        
        dynWindow:AddLabel(5,106,"[412A08]Filters[-]",200,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
        self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)
    end,

    GeneratePurchaseListFromStockpile = function( self, scrollWindow, category, skill )
        local stocks = self._StockpileData[skill][category]
        local skillDisplayName = nil
        local newStocks = {}
        
        for template,data in pairs(stocks) do 
            data.Template = template
            newStocks[#newStocks+1] = data
        end
        
        -- Sort alphabetically  
        if( skill == "WorldDrops" ) then
            table.sort( newStocks, function(a,b) return a.DisplayName < b.DisplayName end )
            skillDisplayName = "World Drops"
        elseif( category == "Materials" ) then
            table.sort( newStocks, function(a,b) return ResourceData.ResourceInfo[a.ResourceType].DisplayName < ResourceData.ResourceInfo[b.ResourceType].DisplayName end )
            skillDisplayName = SkillData.AllSkills[skill].DisplayName
        else
            table.sort( newStocks, function(a,b) return AllRecipes[skill][a.ResourceType].DisplayName < AllRecipes[skill][b.ResourceType].DisplayName end )
            skillDisplayName = SkillData.AllSkills[skill].DisplayName
        end

        local scrollElement = ScrollElement()
        scrollElement:AddLabel(18,6,"[412A08]"..skillDisplayName.." "..category.."[-]",200,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
        scrollElement:AddLabel(200,6,"[412A08]"..self._CurrencyType.." Cost[-]",120,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
        --scrollElement:AddLabel(330,6,"[FFFF00]"..self._CurrencyType.." Bonus[-]",120,20,18,"left")
        scrollWindow:Add(scrollElement)




        for i=1, #newStocks do 
            local scrollElement = ScrollElement()
            
            --DebugMessage(newStocks[i].Template)

            local text = nil
            if( skill == "WorldDrops" ) then
                text = newStocks[i].DisplayName
            elseif( category == "Materials" ) then
                text = ResourceData.ResourceInfo[newStocks[i].ResourceType].DisplayName
            else
                text = AllRecipes[skill][newStocks[i].ResourceType].DisplayName
            end

            if( MarketplaceBoard.StockStackCountsToDonate[newStocks[i].Template] ~=  nil ) then
                text = text .. " (x"..MarketplaceBoard.StockStackCountsToDonate[newStocks[i].Template]..")"
            end

            local hasStock = true
            if( newStocks[i].Count <= 1 ) then
                hasStock = false
            end
            local adjust,percent = MarketplaceBoard.PurchaseAdjust(self._AverageStockCount,newStocks[i].Count)
            local btnState = "disabled"
            local btnText = "Out of Stock"
            if( hasStock ) then
                btnText = "Purchase"
                btnState = ""
            end
            scrollElement:AddButton(325,3,"PurchaseTemplate|"..newStocks[i].Template,btnText,100,28,"Purchase: "..text,"",false,"GoldRedButton", btnState)
            scrollElement:AddLabel(18,8,text,200,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
            scrollElement:AddLabel(200,8,"[483D8B]"..adjust.."[-]",120,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
            
            
            scrollWindow:Add(scrollElement)
        end

    end,

    GenerateDonateListFromStockpile = function( self, scrollWindow, category, skill )

        local stocks = self._StockpileData[skill][category]
        local skillDisplayName = nil
        local newStocks = {}
        
        for template,data in pairs(stocks) do 
            data.Template = template
            newStocks[#newStocks+1] = data
        end
        
        -- Sort alphabetically  
        if( skill == "WorldDrops" ) then
            table.sort( newStocks, function(a,b) return a.DisplayName < b.DisplayName end )
            skillDisplayName = "World Drops"
        elseif( category == "Materials" ) then
            table.sort( newStocks, function(a,b) return ResourceData.ResourceInfo[a.ResourceType].DisplayName < ResourceData.ResourceInfo[b.ResourceType].DisplayName end )
            skillDisplayName = SkillData.AllSkills[skill].DisplayName
        else
            table.sort( newStocks, function(a,b) return AllRecipes[skill][a.ResourceType].DisplayName < AllRecipes[skill][b.ResourceType].DisplayName end )
            skillDisplayName = SkillData.AllSkills[skill].DisplayName
        end

        local scrollElement = ScrollElement()
        scrollElement:AddLabel(18,6,"[412A08]"..skillDisplayName.." "..category.."[-]",200,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
        scrollElement:AddLabel(200,6,"[412A08]"..self._CurrencyType.." Awarded[-]",120,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
        scrollElement:AddLabel(330,6,"[412A08]"..self._CurrencyType.." Bonus[-]",120,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
        scrollWindow:Add(scrollElement)




        for i=1, #newStocks do 
            local scrollElement = ScrollElement()
            

            local text = nil
            if( skill == "WorldDrops" ) then
                text = newStocks[i].DisplayName
            elseif( category == "Materials" ) then
                text = ResourceData.ResourceInfo[newStocks[i].ResourceType].DisplayName
            else
                text = AllRecipes[skill][newStocks[i].ResourceType].DisplayName
            end

            if( MarketplaceBoard.StockStackCountsToDonate[newStocks[i].Template] ~=  nil ) then
                text = text .. " (x"..MarketplaceBoard.StockStackCountsToDonate[newStocks[i].Template]..")"
            end
            local adjust,percent = MarketplaceBoard.DonateAdjust(self._AverageStockCount,newStocks[i].Count)
            local percentColor = "412A08"

            if( percent < 100 ) then
                percent = "-"..tostring((100 - percent))
                percentColor = "FF0000"
            else
                percent = percent - 100
                percent = "+"..tostring((percent))
            end

            scrollElement:AddLabel(18,6,text,200,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
            scrollElement:AddLabel(200,6,"[483D8B]"..adjust.."[-]",120,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
            scrollElement:AddLabel(330,6,"["..percentColor.."]"..percent.."%[-]",120,20,18,"left", false, false, "PermianSlabSerif_Dynamic_Bold")
            scrollWindow:Add(scrollElement)
        end

    end,

    -- Register window reponse listener
    WindowResponse = function( user, buttonId, fieldData, self, root )

        -- Check to see if we are in range of the board.
        if not( self.CheckBoardRange(self, root) ) then return false end

        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        -- Donate
        if( action == "Donate" ) then 
            self.OpenWindow(self, "donate", arg)
            self._LastCategory = arg
        elseif( action == "Purchase" ) then 
                self.OpenWindow(self, "purchase", arg)
                self._LastCategory = arg
        elseif( action == "PurchaseItems" ) then 
            self.OpenWindow(self, "purchase")
        elseif( action == "PurchaseTemplate" ) then
            --DebugMessage("Template: " .. arg)
            MarketplaceBoard.TryPurchaseMarketItem( arg, self._MarketplaceBoardObj, self.ParentObj )
            self.OpenWindow(self, "purchase", self._LastCategory)
        elseif( action == "DonateCarriedObject" ) then 
            local carriedItem = self.ParentObj:CarriedObject()
            self.TryDonateObject( carriedItem, self )
        elseif( action == "DonateItem" ) then 
            self.ChooseDonateItem(self)
        elseif( action == "Help" ) then 
            self.OpenWindow(self, "help")
        else
            --DebugMessage("Closing Window!")
            EndMobileEffect(root)
        end
    end,

    ChooseDonateItem = function( self )
        self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "select_donate_item")
    end,

    TargetDonateItem = function( target, user, self, root )

        if not( self.CheckBoardRange(self, root) ) then return false end

        if( target ~= nil ) then
            if ( target:TopmostContainer() ~= self.ParentObj ) then
                user:SystemMessage("That must be in your backpack to use it.","info")
                return false
            else
                self.TryDonateObject( target, self )
                self.ChooseDonateItem( self )
            end
            
        end
    end,

    TryDonateObject = function( gameObj, self )
        if( gameObj ~= nil ) then
                
            -- Is it a container?
            if( gameObj:IsContainer() ) then
            
                --DebugMessage("Is container!")

                -- If so we try to donate each item in it!
                local contObjects = gameObj:GetContainedObjects()
                for index, contObj in pairs(contObjects) do
                    if not (MarketplaceBoard.TryDonateObjectToStockpile( contObj, self._MarketplaceBoardObj, self.ParentObj ) ) then
                        return 
                    end
                end

                -- Once done we put the container in the users backpack!
                backpackObj = self.ParentObj:GetEquippedObject("Backpack")
                TryPutObjectInContainer( gameObj, backpackObj )

            -- Or a single item?
            else
                MarketplaceBoard.TryDonateObjectToStockpile( gameObj, self._MarketplaceBoardObj, self.ParentObj )
            end

            self.OpenWindow(self, "donate", self._LastCategory)

        end
    end,

    GetStockpileData = function(self)
        self._StockpileData = self._MarketplaceBoardObj:GetObjVar("stockpile")
        self._AverageStockCount = MarketplaceBoard.GetStockpileAverageStockCount( self._MarketplaceBoardObj )
        --DebugMessage("Average Stock Count : " .. self._AverageStockCount )
    end,

    _Args = {},
    _WindowID = nil,
    _Message = nil,
    _MarketplaceBoardObj = nil,
    _StockpileData = {},
    _AverageStockCount = 0,
    _IsFactionBased = false,
    _Faction = nil,
    _FactionVars = nil,
    _FactionCurrency = nil,
    _CurrencyType = "Gold",
    _BoardName = "Marketplace Board",
    _LastCategory = nil,
}
