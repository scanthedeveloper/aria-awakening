MobileEffectLibrary.GodManageRankedArena = 
{
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self._WindowID = "ManageRankedArenaSpawner"
        self._ArenaMaster = target or nil
        self._ArenaData = {}

        -- Make sure we're a player
        if( IsGod( self.ParentObj ) ) then
            -- Register window reponse listener
            RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)

            RegisterEventHandler(EventType.Message, "GetArenaDataAdmin", function( arenaData )
                self._ArenaData = arenaData
                --DebugTable( self._ArenaData )
                --DebugTable( self._ArenaData.MatchData )
                self.OpenWindow( self )    
            end)

            self._ArenaMaster:SendMessage("RankedArenaMessage", { Action = "GetData", Player = self.ParentObj })
        else
            EndMobileEffect(root)
            return false
        end

    end,


    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "GetArenaDataAdmin")
        UnregisterEventHandler("",EventType.DynamicWindowResponse, self._WindowID)
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,
    
    OpenWindow = function(self)
        local dynWindow = DynamicWindow(self._WindowID, "Manage Ranked Arena",400,600,0,0,"")
        local yPos = 10
        --[[
        dynWindow:AddLabel(10,yPos,"MatchInProgress: "..tostring( self._ArenaData.MatchInProgress ),0,0,24)
        yPos = yPos + 35
        ]]
        dynWindow:AddLabel(10,yPos,"Queued Matches: "..tostring( #self._ArenaData.RankedQueue ),0,0,24)
        yPos = yPos + 35

        local count1v1 = 0
        local count2v2 = 0
        local count3v3 = 0
        for i=1, #self._ArenaData.RankedQueue do 
            if( self._ArenaData.RankedQueue[i].GroupSize == 1 ) then count1v1 = count1v1 + 1 
            elseif( self._ArenaData.RankedQueue[i].GroupSize == 2 ) then count2v2 = count2v2 + 1 
            elseif( self._ArenaData.RankedQueue[i].GroupSize == 3 ) then count1v1 = count3v3 + 1 end
        end

        dynWindow:AddLabel(10,yPos,"1v1: "..tostring( count1v1 ),0,0,24)
        dynWindow:AddLabel(150,yPos,"2v2: "..tostring( count2v2 ),0,0,24)
        dynWindow:AddLabel(300,yPos,"3v3: "..tostring( count3v3 ),0,0,24)
        yPos = yPos + 35
        yPos = yPos + 35
        if( self._ArenaData.MatchInProgress and self._ArenaData.MatchData ~= nil ) then
            local currentUnixTS = os.time(os.date("!*t"))
            
            dynWindow:AddLabel(10,yPos,"Current Match: "..tostring( self._ArenaData.MatchData.Title ),0,0,24)
            yPos = yPos + 35

            dynWindow:AddLabel(10,yPos,"Ranked: "..tostring( self._ArenaData.MatchData.Ranked ),0,0,24)
            yPos = yPos + 35

            -- Coutdown
            if( self._ArenaData.MatchData.Countdown ) then
                dynWindow:AddLabel(10,yPos,"Countdown: "..tostring( self._ArenaData.MatchData.Countdown ),0,0,24)
                yPos = yPos + 35
            end
            
            -- EndTime
            if( self._ArenaData.MatchData.EndTime ) then
                dynWindow:AddLabel(10,yPos,"Match Ends: "..tostring( math.max(math.abs(currentUnixTS - self._ArenaData.MatchData.EndTime), 0) ).." seconds",0,0,24)
                yPos = yPos + 35
            end
            
            
            dynWindow:AddLabel(10,yPos,"BlueTeamDead: "..tostring( #self._ArenaData.MatchData.TeamADead ),0,0,24)
            yPos = yPos + 35
            dynWindow:AddLabel(10,yPos,"RedTeamDead: "..tostring( #self._ArenaData.MatchData.TeamBDead ),0,0,24)
            yPos = yPos + 35

        end

        -- Display the ranked match toggle
        if( self._ArenaMaster:GetObjVar("RankedMatches") == true ) then
            dynWindow:AddButton(10,480,"DisableMatches","Disable Ranked",157,27,"","",false,"")
        else 
            dynWindow:AddButton(10,480,"EnableMatches","Enable Ranked",157,27,"","",false,"")
        end
        
        dynWindow:AddButton(10,510,"End","End Match: Draw",157,27,"","",false,"")
        dynWindow:AddButton(210,510,"Clear","Clear Queue",157,27,"","",false,"")

        dynWindow:AddButton(210,480,"EndSeason","End Season",157,27,"","",false,"")

        self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)

    end,

    WindowResponse = function( user, buttonId, fieldData, self, root )
        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        -- Accept button
        if( action == "End" ) then 
            self._ArenaMaster:SendMessage("EndMatch")
        elseif( action == "Clear" ) then 
            self._ArenaMaster:SendMessage("ClearQueue")
        elseif( action == "EnableMatches" ) then 
            self._ArenaMaster:SetObjVar("RankedMatches", true)
        elseif( action == "DisableMatches" ) then 
            self._ArenaMaster:SetObjVar("RankedMatches", false)
        elseif( action == "EndSeason" ) then
            ClientDialog.Show
            {
                TargetUser = self.ParentObj,
                DialogId = "EndSeason",
                TitleStr = "End Season",
                DescStr = "Are you sure you wish to end this arena season?",
                Button1Str = "Yes",
                Button2Str = "No",
                ResponseObj = self.ParentObj,
                ResponseFunc = function( user, buttonId )
                    buttonId = tonumber(buttonId)
                    if ( buttonId == 0 ) then
                        RankedArena.EndArenaSeason( self._ArenaMaster )
                    end
                end,
            }
        else 
            EndMobileEffect(root) 
        end

    end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(2)
	end,

    AiPulse = function(self,root) 
        self._ArenaMaster:SendMessage("RankedArenaMessage", { Action = "GetData", Player = self.ParentObj })
    end,

    _WindowID = nil,
}
