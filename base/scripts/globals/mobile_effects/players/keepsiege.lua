MobileEffectLibrary.KeepSiege = 
{
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self._KeepDoor = target or FindObject(SearchTemplate("keep_wood_door",self._MaximumDistance))
        self._HUDShown = false

        if( 
            not IsPlayerCharacter(self.ParentObj)  -- Are we a player?
            or not self._KeepDoor -- Do we have a keep door?
            or not self._KeepDoor:IsValid() -- Is the keep door valid?
        ) then
            EndMobileEffect(root)
            return false 
        end
    end,

    OnExitState = function(root)
        --DebugMessage("Keep Siege Buff Exited")
    end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
	end,

    AiPulse = function(self,root) 
        if( 
            not self._KeepDoor -- Do we have keep door?
            or not self._KeepDoor:IsValid() -- Is the keep door valid?
            or (self.ParentObj:DistanceFrom(self._KeepDoor) > self._MaximumDistance)  -- Are we outside the maximum distance of the keep door?
        ) then
            self._KeepDoor = nil
        end

        if( Militia.UpdateKeepHUD(self.ParentObj, self._KeepDoor) == false ) then
            EndMobileEffect(root)
        end   
    end,

    _KeepDoor = nil,
    _MaximumDistance = 30

}
