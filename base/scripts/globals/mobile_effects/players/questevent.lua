MobileEffectLibrary.QuestEvent = 
{
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self.isPlayer = IsPlayerCharacter(self.ParentObj)

        if( self.isPlayer ) then
            if( self._Args.EventType and self._Args.EventVars and self._Args.EventCount ) then
                Quests.SendQuestEventMessage( self.ParentObj, self._Args.EventType, self._Args.EventVars, self._Args.EventCount )
                self.ParentObj:SystemMessage("[87CEEB]You feel a slight chill as you touch the note.[-]")
            end
        end

        EndMobileEffect(root)
    end,

    _Args = {},
    isPlayer = false,

}
