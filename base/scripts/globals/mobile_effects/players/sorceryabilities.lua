MobileEffectLibrary.SorceryAbilities = 
{
    OnEnterState = function(self,root,target,args)
        self._WindowID = "SorceryAbilityWindow" --..uuid()

        -- Open the window
        self.OpenWindow(self)

        -- Register window reponse listener
        RegisterSingleEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)

        RegisterEventHandler(EventType.ClientObjectCommand, "dropAction",
        function (user,sourceId,actionType,actionId,slot)
            --DebugMessage(1)
            DebugMessage(user,sourceId,actionType,actionId,slot)
            if((sourceId == self._WindowID) and slot ~= nil) then
                local hotbarAction = nil
                local prestigeClass, prestigeName = actionId:match("pa_(%a+)_(%a+)")
                hotbarAction = GetPrestigeAbilityUserAction(self.ParentObj, nil,prestigeClass,prestigeName)
                
                hotbarAction.Slot = tonumber(slot)
                RequestAddUserActionToSlot(user,hotbarAction)
            end
        end)

    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("",EventType.DynamicWindowResponse, self._WindowID)
        UnregisterEventHandler("", EventType.ClientObjectCommand, "dropAction")
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,

    -- Control when the window opens
    OpenWindow = function(self)
        local dynWindow = DynamicWindow(self._WindowID, "Sorcerer Abilities",450,450,0,0,"TransparentDraggable")
        dynWindow:AddImage(0,0,"SorcerySkillWindow", 450, 450)

        dynWindow:AddButton(
        326, --(number) x position in pixels on the window
        40, --(number) y position in pixels on the window
        "", --(string) return id used in the DynamicWindowResponse event
        "", --(string) text in the button (defaults to empty string)
        0, --(number) width of the button (defaults to width of text)
        0,--(number) height of the button (default decided by type of button)
        "", --(string) mouseover tooltip for the button (default blank)
        "", --(string) server command to send on button click (default to none)
        true, --(boolean) should the window close when this button is clicked? (default true)
        "CloseSquare", --(string) button type (default "Default")
        buttonState --(string) button state (optional, valid options are default,pressed,disabled)
        --customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
    )
        
        local action1 = GetPrestigeAbilityUserAction(self.ParentObj, nil, "Sorcery", "SummonFox")
        dynWindow:AddUserAction(121,83,action1,52)
        
        local action2 = GetPrestigeAbilityUserAction(self.ParentObj, nil, "Sorcery", "MagicMissile")
        dynWindow:AddUserAction(50,200,action2,52)

        local action3 = GetPrestigeAbilityUserAction(self.ParentObj, nil, "Sorcery", "Starfall")
        dynWindow:AddUserAction(121,317,action3,52)

        local action4 = GetPrestigeAbilityUserAction(self.ParentObj, nil, "Sorcery", "SummonBeam")
        dynWindow:AddUserAction(277,83,action4,52)

        local action5 = GetPrestigeAbilityUserAction(self.ParentObj, nil, "Sorcery", "WispArmor")
        dynWindow:AddUserAction(341,200,action5,52)

        local action5 = GetPrestigeAbilityUserAction(self.ParentObj, nil, "Sorcery", "SummonConstruct")
        dynWindow:AddUserAction(277,317,action5,52)


        self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)
    end,

    -- Register window reponse listener
    WindowResponse = function( user, buttonId, fieldData, self, root )
        EndMobileEffect(root)
    end,


}
