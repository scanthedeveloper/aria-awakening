MobileEffectLibrary.RankedArenaQueued = 
{
    OnEnterState = function(self,root,target,args)
        self._WindowID = "RankedArenaQueue" --..uuid()
        self._ArenaMaster = args.arenaMaster
        self._MatchType = args.matchType
        self._TeamA = args.TeamA
        self._TeamB = args.TeamB
        self._Rank = self.ParentObj:GetObjVar("ArenaRank") or RankedArena.DefaultScore
        self._MatchLeader = target
        self._MatchEndedByLeader = false
        self._Countdown = nil
        self._MatchStarted = false
        self._Ready = false
        self._Queued = false

        -- Open the window
        self.OpenWindow(self)

        -- Register window reponse listener
        RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)
        RegisterEventHandler(EventType.Message, "RankedArenaMessage",  function ( args ) self.ProcessArenaMessage(self, root, args) end)
        RegisterSingleEventHandler(EventType.UserLogout,"",function (...)EndMobileEffect(root)end)

    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("",EventType.DynamicWindowResponse, self._WindowID)
        UnregisterEventHandler("", EventType.Message, "RankedArenaMessage")
        
        -- We need to inform the leader that we want to leave the match
        if( self._MatchEndedByLeader == false and not self._MatchStarted ) then
            self._MatchLeader:SendMessage("RankedArenaMessage", {Action = "LeaveMatch", Player = self.ParentObj})
        end

        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        self.ParentObj:CloseDynamicWindow(self._WindowID)
        CloseDynamicEventHUD( self.ParentObj )
    end,

    -- Control when the window opens
    OpenWindow = function(self)
        --local dynWindow = RankedArena.ShowArenaWindow( self._WindowID, self._ArenaMaster, self._MatchType, self._TeamA, self._TeamB, self._Rank, true, self._Countdown, self._Ready, self._Queued )
        --self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)
    end,

    -- Register window reponse listener
    WindowResponse = function( user, buttonId, fieldData, self, root )
        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        if( action == "LeaveMatch" ) then
            EndMobileEffect(root)
        elseif( action == "Ready" ) then
            self._Ready = true
            self._ArenaMaster:SendMessage("RankedArenaMessage", {Action = "Ready", Player = self.ParentObj})
            self.OpenWindow( self )
        else
            EndMobileEffect(root)
        end
    end,

    ProcessArenaMessage = function( self, root, args )
        --DebugMessage("ProcessArenaMessageQueued: " .. args.Action)
        if( args.Action == "CancelMatch" ) then
            self._MatchEndedByLeader = true
            self._Message = args.Message or "The match has been cancelled."
            EndMobileEffect(root)
        elseif( args.Action == "UpdateTeams" ) then
            self._TeamA = args.TeamA
            self._TeamB = args.TeamB
            self._Queued = args.Queue
            self.OpenWindow(self)
        elseif( args.Action == "MatchCountdown" ) then
            self._Countdown = args.Countdown
            self.OpenWindow(self)
            self.ParentObj:PlayObjectSound("event:/ui/drum1", false, 0, false, false)
        elseif( args.Action == "MatchQueued" ) then
            self.ParentObj:SystemMessage(args.Message, "info")
            self._Queued = true
            self.OpenWindow( self )
        elseif( args.Action == "MatchStarted" ) then
            self._MatchStarted = true
            self.ParentObj:SendMessage("StartMobileEffect", "RankedArenaMatch", self._ArenaMaster, args.MatchData)
            EndMobileEffect(root)
            return
        end
    end,
}
