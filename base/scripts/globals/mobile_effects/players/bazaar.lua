MobileEffectLibrary.BazaarBoard = 
{
    OnEnterState = function(self,root,target,args)
        self._WindowID = "BazaarBoard" --..uuid()
        self._BazaarBoardObject = target
        self._Location = self.ParentObj:GetLoc()
        self._SearchRequestId = uuid()
        self._ClusterController = GetClusterController()
        self._PurchaseObjId = nil
        self._Results = nil

        -- Make sure we found the township board near us.
        if( self._BazaarBoardObject == nil ) then
            self._Message = "You are too far away to do that."
            EndMobileEffect(root)
            return false
        end

        -- Build the ESSENCE filters
        for k,v in pairs( EssenceData ) do 
            if( type(v) == "table" and v.Name ~= nil and v.Hidden ~= true ) then
                self._FilterStates[ k ] = { State = nil, Name = v.EffectName, Type = "Essence" }
            end
        end

        -- Build the CATEGORY filters
        for skill,recipes in pairs( AllRecipes ) do 
            for item,data in pairs(recipes) do 
                if( self._FilterStates[ data.Category ] == nil ) then
                    self._FilterStates[ data.Category ] = { State = nil, Name = data.Category, Type = "Category" }
                end
            end
        end

        -- Manual ones
        self._FilterStates["Essences"] = { State = nil, Name = "Essences", Type = "Category" }
        --self._FilterStates["Artifacts"] = { State = nil, Name = "Artifacts", Type = "Category" }
        self._FilterStates["Recipes"] = { State = nil, Name = "Recipes", Type = "Category" }
        
        self._FilterStates["Runebooks"] = { State = nil, Name = "Runebooks", Type = "BookType" }
        self._FilterStates["Spellbooks"] = { State = nil, Name = "Spellbooks", Type = "BookType" }
        self._FilterStates["TomeBooks"] = { State = nil, Name = "Ability Tomes", Type = "BookType" }

        self._FilterStates["Defense3"] = { State = nil, Name = "+5 Defense", Type = "Armor" }
        self._FilterStates["Defense2"] = { State = nil, Name = "+3 Defense", Type = "Armor" }
        self._FilterStates["Defense1"] = { State = nil, Name = "+2 Defense", Type = "Armor" }

        self._FilterStates["ArmorType1"] = { State = nil, Name = "Cloth", Type = "ArmorType" }
        self._FilterStates["ArmorType2"] = { State = nil, Name = "Light", Type = "ArmorType" }
        self._FilterStates["ArmorType3"] = { State = nil, Name = "Heavy", Type = "ArmorType" }

        self._FilterStates["WeaponDamage4"] = { State = nil, Name = "Perfect Damage (50%)", Type = "Weapons" }
        self._FilterStates["WeaponDamage3"] = { State = nil, Name = "High Damage (45%-49%)", Type = "Weapons" }
        self._FilterStates["WeaponDamage2"] = { State = nil, Name = "Medium Damage (40%-44%)", Type = "Weapons" }
        self._FilterStates["WeaponDamage1"] = { State = nil, Name = "Low Damage (35%-39%)", Type = "Weapons" }

        self._FilterStates["WandAttunement1"] = { State = nil, Name = "Lesser Attunement", Type = "WandAttunement" }
        self._FilterStates["WandAttunement2"] = { State = nil, Name = "Some Attunement", Type = "WandAttunement" }
        self._FilterStates["WandAttunement3"] = { State = nil, Name = "Greater Attunement", Type = "WandAttunement" }
        self._FilterStates["WandAttunement4"] = { State = nil, Name = "Powerful Attunement", Type = "WandAttunement" }

        self._FilterStates["StaffAttunement1"] = { State = nil, Name = "Lesser Attunement", Type = "StaffAttunement" }
        self._FilterStates["StaffAttunement2"] = { State = nil, Name = "Some Attunement", Type = "StaffAttunement" }
        self._FilterStates["StaffAttunement3"] = { State = nil, Name = "Greater Attunement", Type = "StaffAttunement" }
        self._FilterStates["StaffAttunement4"] = { State = nil, Name = "Powerful Attunement", Type = "StaffAttunement" }

        self._FilterStates["Slashing"] = { State = nil, Name = "Slashing", Type = "WeaponTypes" }
        self._FilterStates["Bashing"] = { State = nil, Name = "Bashing", Type = "WeaponTypes" }
        self._FilterStates["Piercing"] = { State = nil, Name = "Piercing", Type = "WeaponTypes" }
        self._FilterStates["Lancing"] = { State = nil, Name = "Lancing", Type = "WeaponTypes" }
        self._FilterStates["Instrument"] = { State = nil, Name = "Instrument", Type = "WeaponTypes" }
        self._FilterStates["Ranged"] = { State = nil, Name = "Ranged", Type = "WeaponTypes" }
        self._FilterStates["Magic Staff"] = { State = nil, Name = "Magic Staff", Type = "WeaponTypes" }
        self._FilterStates["Magic Wand"] = { State = nil, Name = "Magic Wand", Type = "WeaponTypes" }
        self._FilterStates["Sorcery Orb"] = { State = nil, Name = "Sorcery Orb", Type = "WeaponTypes" }

        self._FilterStates["Fabrication"] = { State = nil, Name = "Fabrication", Type = "CrafterProfession" }
        self._FilterStates["Blacksmithing"] = { State = nil, Name = "Blacksmithing", Type = "CrafterProfession" }
        self._FilterStates["Carpentry"] = { State = nil, Name = "Carpentry", Type = "CrafterProfession" }

        -- Crafting Resources [manual]
        self._FilterStates["ResourcesFineScroll"] = { State = nil, ResourceType = "FineScroll", Name = "Fine Scroll", Type = "CraftingResources" }
        self._FilterStates["ResourcesFrayedScroll"] = { State = nil, ResourceType = "FrayedScroll", Name = "Frayed Scroll", Type = "CraftingResources" }
        self._FilterStates["ResourcesBlankScroll"] = { State = nil, ResourceType = "BlankScroll", Name = "Blank Scroll", Type = "CraftingResources" }
        self._FilterStates["ResourcesAncientScroll"] = { State = nil, ResourceType = "AncientScroll", Name = "Ancient Scroll", Type = "CraftingResources" }
        self._FilterStates["Resources".."EtherealBones"] = { State = nil, ResourceType = "EtherealBones", Name = "Ethereal Bones", Type = "CraftingResources" }
        self._FilterStates["Resources".."CursedBones"] = { State = nil, ResourceType = "CursedBones", Name = "Cursed Bones", Type = "CraftingResources" }
        self._FilterStates["Resources".."Bones"] = { State = nil, ResourceType = "Bones", Name = "Bones", Type = "CraftingResources" }
        self._FilterStates["Resources".."Essence"] = { State = nil, ResourceType = "Essence", Name = "Magical Essence", Type = "CraftingResources" }
        self._FilterStates["Resources".."Eye"] = { State = nil, ResourceType = "Eye", Name = "Eye", Type = "CraftingResources" }
        self._FilterStates["Resources".."SicklyEye"] = { State = nil, ResourceType = "SicklyEye", Name = "Sickly Eye", Type = "CraftingResources" }
        self._FilterStates["Resources".."DecrepidEye"] = { State = nil, ResourceType = "DecrepidEye", Name = "Decrepid Eye", Type = "CraftingResources" }
        self._FilterStates["Resources".."VileBlood"] = { State = nil, ResourceType = "VileBlood", Name = "Vile Blood", Type = "CraftingResources" }
        self._FilterStates["Resources".."BeastBlood"] = { State = nil, ResourceType = "BeastBlood", Name = "Beast Blood", Type = "CraftingResources" }
        self._FilterStates["Resources".."Blood"] = { State = nil, ResourceType = "Blood", Name = "Blood", Type = "CraftingResources" }
        self._FilterStates["Resources".."WildFeather"] = { State = nil, ResourceType = "WildFeather", Name = "Wild Feather", Type = "CraftingResources" }
        self._FilterStates["Resources".."Feather"] = { State = nil, ResourceType = "Feather", Name = "Feather", Type = "CraftingResources" }


        for k,v in pairs( AllRecipes.FabricationSkill ) do
            if( v.Category == "Resources" ) then
                self._FilterStates[v.Category..k] = { State = nil, Name = v.DisplayName, Type = "CraftingResources" }
            end
            if( v.NeedRecipe == true ) then
                self._FilterStates["RecipesFabrication"..k] = { State = nil, Recipe = k, Name = v.DisplayName, Type = "RecipesFabrication" }
            end
        end
        for k,v in pairs( AllRecipes.WoodsmithSkill ) do
            if( v.Category == "Resources" ) then
                self._FilterStates[v.Category..k] = { State = nil, Name = v.DisplayName, Type = "CraftingResources" }
            end
            if( v.Category == "Furnishings" ) then
                self._FilterStates[v.Category..v.Subcategory] = { State = nil, Name = v.Subcategory, Type = "FurnishingsType" }
            end
            if( v.NeedRecipe == true ) then
                self._FilterStates["RecipesCarpentry"..k] = { State = nil, Recipe = k, Name = v.DisplayName, Type = "RecipesCarpentry" }
            end
        end
        for k,v in pairs( AllRecipes.MetalsmithSkill ) do
            if( v.Category == "Resources" ) then
                self._FilterStates[v.Category..k] = { State = nil, Name = v.DisplayName, Type = "CraftingResources" }
            end
            if( v.NeedRecipe == true ) then
                self._FilterStates["RecipesBlacksmithing"..k] = { State = nil, Recipe = k, Name = v.DisplayName, Type = "RecipesBlacksmithing" }
            end
        end

        -- Scrolls
        self._FilterStates["ScrollCircle1"] = { State = nil, Name = "I", Type = "ScrollTypes" }
        self._FilterStates["ScrollCircle2"] = { State = nil, Name = "II", Type = "ScrollTypes" }
        self._FilterStates["ScrollCircle3"] = { State = nil, Name = "III", Type = "ScrollTypes" }
        self._FilterStates["ScrollCircle4"] = { State = nil, Name = "IV", Type = "ScrollTypes" }
        self._FilterStates["ScrollCircle5"] = { State = nil, Name = "V", Type = "ScrollTypes" }
        self._FilterStates["ScrollCircle6"] = { State = nil, Name = "VI", Type = "ScrollTypes" }
        self._FilterStates["ScrollCircle7"] = { State = nil, Name = "VII", Type = "ScrollTypes" }
        self._FilterStates["ScrollCircle8"] = { State = nil, Name = "VIII", Type = "ScrollTypes" }


        for k,v in pairs( self._FilterStates ) do 
            table.insert (self._FilterSorted, k)
        end
        table.sort (self._FilterSorted) -- sort keys

        -- Open the window
        self.OpenWindow(self, "Search")

        -- Register window reponse listener
        RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)

        RegisterEventHandler(EventType.Message, "BazaarSearchResults", function( response, requestId )
            
            --DebugMessage( "Response : " .. response )
            if( string.len( response ) > 0 ) then
                self._Results = json.decode(json.decode(response)) or {}
            else
                self._Results = {}
            end

            self.OpenWindow(self, "Results", nil, self._FilterStates, self._Results)
        end)

        RegisterEventHandler(EventType.Message, "BazaarVerifyPurchaseable", function( response, requestId )
            
            local response = json.decode(response) or {}

            if( response.Success == true ) then
                local purchaseItem = {}
                for i=1,#self._Results do 
                    if( tonumber(self._Results[i].objectId) == self._PurchaseObjId) then purchaseItem = self._Results[i] end
                end

                local descStr = "Are you sure you wish to purchase [FFFF00]"..purchaseItem.objectName.."[-] for [FFFF00]".. ValueToAmountStrShort(purchaseItem.objectPrice) .."[-]? \n\n You will still have to travel to the vendor location to collect your purchase. A map marker will be provided."
                local btn2Str = "Cancel."

                local backpackCanBuy = ( CountCoins(self.ParentObj) >= tonumber(purchaseItem.objectPrice) )
                local bankCanBuy = ( CountCoinsInBank(self.ParentObj) >= tonumber(purchaseItem.objectPrice) )
                local canBuy = ( backpackCanBuy or bankCanBuy )

                if( not canBuy ) then
                    descStr = "You cannot afford that item."
                    btn2Str = nil
                end

                ClientDialog.Show{
                    TargetUser = self.ParentObj,
                    ResponseObj = self.ParentObj,
                    DialogId = "ConfirmBazaarPurchase",
                    TitleStr = "Confirm Purchase",
                    DescStr = descStr,
                    Button1Str = "Ok.",
                    Button2Str = btn2Str,
                    ResponseFunc = function( user, buttonId )
                        if( buttonId == 0 and canBuy ) then
                            self.DoBazaarPurchase( self, root, purchaseItem, backpackCanBuy )
                        else
                            self._PurchaseObjId = nil
                        end
                    end
                }
            
            else
                self.ParentObj:SystemMessage("That item is no longer for sale.","info")
            end

        end)


        

        RegisterEventHandler(EventType.Message, "BazaarPurchaseItem", function( success, purchaseItem )
            if( success == true ) then
                --DebugMessage("["..purchaseItem.objectId.."] : Finalize Purchase")
                self.WindowResponse( self.ParentObj, "Search", self._FieldData, self, root )
                local locArgs = StringSplit( purchaseItem.objectLoc, ",")
                local objectLoc = Loc( tonumber(locArgs[1]), tonumber(locArgs[2]), tonumber(locArgs[3]) )
                AddDynamicMapMarker(self.ParentObj,{Icon="marker_pickup", Location=objectLoc, Tooltip="Bazaar Purchase: "..purchaseItem.objectName},"Bazaar|"..purchaseItem.merchantId)
                self.ParentObj:SystemMessage("You successfully purchased ("..purchaseItem.objectName..") from the bazaar. Check your map for a marker indicating where you can collect your purchase.")
            else
                self.ParentObj:SystemMessage("Purchase failed. Please try again.","info")
            end
        end)

    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("",EventType.DynamicWindowResponse, self._WindowID)
        UnregisterEventHandler("", EventType.Message, "BazaarSearchResults")
        UnregisterEventHandler("", EventType.Message, "BazaarPurchaseItem")
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,

    CheckBoardRange = function( self, root )
        if( not self._BazaarBoardObject or self.ParentObj:DistanceFrom(self._BazaarBoardObject) > 10 ) then
            self._Message = "Too far from bazaar board."
            EndMobileEffect(root)
            return false
        else
            return true
        end
    end,

    -- Control when the window opens
    OpenWindow = function(self, mode, category, fieldData, searchResults)
        local searchBtnState = nil
        fieldData = self._FieldData or { itemSearch = nil, costGold = nil, costPlatnium = nil }

        -- We disable the search button if we cannot actively use it
        if( mode ~= 'Search' and mode ~= 'Results' ) then searchBtnState = "disabled" end

        -- Load top bar
        local dynWindow = DynamicWindow(self._WindowID, "Grand Bazaar",1000,800,0,0,"Parchment")
        local y = 5
        -- Divider
        dynWindow:AddImage(5,y,"Prestige_Divider",965,0,"Sliced")
        dynWindow:AddImage(5,100,"TextFieldChatUnsliced",960,645,"Sliced")
        
        local y = y+10

        -- Item Name
        dynWindow:AddLabel(5,y,"Item Name",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddImage(5,y+20,"TextFieldChatUnsliced",250,30,"Sliced")
        dynWindow:AddTextField(5,y+25,250,20,"itemSearch", fieldData.itemSearch)

        -- Cost
        dynWindow:AddLabel(300,y,"Maximum Cost",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddImage(300,y+20,"TextFieldChatUnsliced",50,30,"Sliced")
        dynWindow:AddTextField(300,y+25,50,20,"costPlatnium", fieldData.costPlatnium)
        dynWindow:AddLabel(300+52,y+40,"p",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddImage(370,y+20,"TextFieldChatUnsliced",50,30,"Sliced")
        dynWindow:AddTextField(370,y+25,50,20,"costGold", fieldData.costGold)
        dynWindow:AddLabel(370+52,y+40,"g",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")

        -- Filters Button
        dynWindow:AddButton(765,695,"Filters","Filters",200,48,"Allows you to adjust the filters used in your search.","",false,"GoldRedButton")

        -- Search
        dynWindow:AddButton(750,20,"Search","Search",227,55,"Click to search for items.","",false,"GoldRedButton", searchBtnState)
        
        -- Divider
        dynWindow:AddImage(5,80,"Prestige_Divider",965,0,"Sliced")
        
        -- SHOW FILTERS
        if( mode == "Filters" ) then

            local showEnchants = false

            --dynWindow:AddImage(5,100,"TextFieldChatUnsliced",960,590,"Sliced")

            -- CATEGORIES
            local xPos = 10
            local yPos = 130
            local iCount = 0
            dynWindow:AddLabel(10,yPos-20,"Category",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            for t,k in pairs( self._FilterSorted ) do 
                local v = self._FilterStates[k]
                if( v.Type == "Category" ) then
                    -- Determine if we need to toggle [ENCHANTS]
                    if( (v.Name == "Armor" or v.Name == "Weapons" or v.Name == "Artifacts") and v.State == "pressed" ) then showEnchants = true end
                    dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                    xPos = xPos + 250
                    iCount = iCount+1
                    if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                end
            end
            if( iCount % 4 == 0 ) then yPos = yPos - 20 end

            -- RECIPES
            if( self._FilterStates.Recipes and self._FilterStates.Recipes.State == "pressed" ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Profession",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "CrafterProfession" ) then
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- RECIPES FABRICATION
            if( self._FilterStates.Recipes and self._FilterStates.Recipes.State == "pressed" and ( self._FilterStates.Fabrication and self._FilterStates.Fabrication.State == "pressed" ) ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Recipes",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "RecipesFabrication" ) then
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- RECIPES Carpentry
            if( self._FilterStates.Recipes and self._FilterStates.Recipes.State == "pressed" and ( self._FilterStates.Carpentry and self._FilterStates.Carpentry.State == "pressed" ) ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Recipes",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "RecipesCarpentry" ) then
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- RECIPES Blacksmithing
            if( self._FilterStates.Recipes and self._FilterStates.Recipes.State == "pressed" and ( self._FilterStates.Blacksmithing and self._FilterStates.Blacksmithing.State == "pressed" ) ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Recipes",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "RecipesBlacksmithing" ) then
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            


            -- FURNISHING TYPES
            if( self._FilterStates.Furnishings and self._FilterStates.Furnishings.State == "pressed" ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Furniture Type",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "FurnishingsType" ) then
                        -- Determine if we need to toggle [ENCHANTS]
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- SCROLL CIRCLES
            if( self._FilterStates.Scrolls and self._FilterStates.Scrolls.State == "pressed" ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Difficulty",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "ScrollTypes" ) then
                        -- Determine if we need to toggle [ENCHANTS]
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- CRAFTING RESOURCES
            if( self._FilterStates.Resources and self._FilterStates.Resources.State == "pressed" ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Resource Type",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "CraftingResources" ) then
                        -- Determine if we need to toggle [ENCHANTS]
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- BOOK TYPES
            if( self._FilterStates.Books and self._FilterStates.Books.State == "pressed" ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Book Type",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "BookType" ) then
                        -- Determine if we need to toggle [ENCHANTS]
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- ARMOR TYPES
            if( self._FilterStates.Armor and self._FilterStates.Armor.State == "pressed" ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Armor Type",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "ArmorType" ) then
                        -- Determine if we need to toggle [ENCHANTS]
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- ARMOR PROPERTIES
            if( self._FilterStates.Armor and self._FilterStates.Armor.State == "pressed" ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Defense",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "Armor" ) then
                        -- Determine if we need to toggle [ENCHANTS]
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- WEAPONS TYPES
            if( self._FilterStates.Weapons and self._FilterStates.Weapons.State == "pressed" ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Weapon Type",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "WeaponTypes" ) then
                        -- Determine if we need to toggle [ENCHANTS]
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end


            -- WEAPONS PROPERTIES
            if( self._FilterStates.Weapons and self._FilterStates.Weapons.State == "pressed" and ( self._FilterStates.Bashing.State == "pressed" or self._FilterStates.Piercing.State == "pressed" or self._FilterStates.Lancing.State == "pressed" or self._FilterStates.Ranged.State == "pressed" or self._FilterStates.Slashing.State == "pressed" ) ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Weapon Damage",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "Weapons" ) then
                        -- Determine if we need to toggle [ENCHANTS]
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- MAGIC STAFF ATTUNEMENT
            if( self._FilterStates.Weapons and self._FilterStates.Weapons.State == "pressed" and self._FilterStates["Magic Staff"].State == "pressed" ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Attunement Level",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "StaffAttunement" ) then
                        -- Determine if we need to toggle [ENCHANTS]
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- MAGIC WAND ATTUNEMENT
            if( self._FilterStates.Weapons and self._FilterStates.Weapons.State == "pressed" and self._FilterStates["Magic Wand"].State == "pressed" ) then
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Attunement Level",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "WandAttunement" ) then
                        -- Determine if we need to toggle [ENCHANTS]
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 250
                        iCount = iCount+1
                        if( iCount % 4 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 4 == 0 ) then yPos = yPos - 20 end
            end

            -- ENCHANTS
            if( showEnchants ) then    
                xPos = 10
                iCount = 0
                yPos = yPos + 50
                dynWindow:AddLabel(10,yPos-20,"Enchants",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                for t,k in pairs( self._FilterSorted ) do 
                    local v = self._FilterStates[k]
                    if( v.Type == "Essence" ) then
                        dynWindow:AddButton(xPos, yPos,"ToggleFilter|"..k,v.Name,110,30,"","",false,"Selection2",v.State)
                        xPos = xPos + 330
                        iCount = iCount+1
                        if( iCount % 3 == 0 ) then yPos = yPos + 20 xPos = 10 end
                    end
                end
                if( iCount % 3 == 0 ) then yPos = yPos - 20 end
            end
            
            dynWindow:AddButton(765,695,"SaveFilters","Apply",200,48,"Save the filter settings.","",false,"GoldRedButton")

        elseif( mode == "Results" ) then
            
            -- Light the fires!
            if( type(searchResults) == 'table' and #searchResults > 0 ) then
                local scrollWindow = ScrollWindow(10,130,945,500,24)
                dynWindow:AddLabel(25,105,"Item Name",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                dynWindow:AddLabel(327,105,"Price",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                dynWindow:AddLabel(457,105,"Region",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                dynWindow:AddLabel(587,105,"Distance",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
                dynWindow:AddLabel(697,105,"Shop Name",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")

                for i=1,#searchResults do 
                    searchResults[i].objectHue = 0
                    
                    if( 
                        not BazaarHelper.BlacklistedTemplates[searchResults[i].objectTemplate] 
                        or ( 
                            -- Filtered directly to runebooks
                            ( searchResults[i].objectTemplate == "runebook" and self._FilterStates.Runebooks.State == "pressed" ) or 
                            
                            -- Filtered directly to story books
                            ( searchResults[i].objectTemplate == "book_blue" and self._FilterStates.StoryBooks.State == "pressed" ) or
                            ( searchResults[i].objectTemplate == "book_brown" and self._FilterStates.StoryBooks.State == "pressed" ) or
                            ( searchResults[i].objectTemplate == "book_green" and self._FilterStates.StoryBooks.State == "pressed" ) or
                            ( searchResults[i].objectTemplate == "book_grey" and self._FilterStates.StoryBooks.State == "pressed" ) or
                            ( searchResults[i].objectTemplate == "book_red" and self._FilterStates.StoryBooks.State == "pressed" ) or
                            ( searchResults[i].objectTemplate == "book_yellow" and self._FilterStates.StoryBooks.State == "pressed" )
                        ) 
                    ) then
                        -- Get Location
                        local locArgs = StringSplit( searchResults[i].objectLoc, ",")
                        local objectLoc = Loc( tonumber(locArgs[1]), tonumber(locArgs[2]), tonumber(locArgs[3]) )
                        local buttonState = isSelected and "pressed" or ""
                        local distance = math.floor(math.abs(self._Location:Distance(objectLoc)))

                        local scrollElement = ScrollElement()
                        scrollElement:AddButton(4,0,"Purchase|"..tostring(searchResults[i].objectId),"",922,24,tostring(searchResults[i].objectTooltip) .. "\n\n ["..COLORS.GreenYellow.."]CLICK TO PURCHASE[-]","",false,"ThinFrameHover",buttonState)
                        scrollElement:AddLabel(18,6,tostring(searchResults[i].objectName),300,20,18,"left")
                        scrollElement:AddLabel(320,6,ValueToAmountStrShort(searchResults[i].objectPrice),120,20,18,"left")
                        scrollElement:AddLabel(450,6,GetRegionalName( objectLoc ),120,20,18,"left")
                        scrollElement:AddLabel(597,6,tostring(distance).."m",120,20,18,"left")
                        scrollElement:AddLabel(687,6,tostring(searchResults[i].shopName),200,20,18,"left")
                        scrollWindow:Add(scrollElement)
                    end
                
                end

                dynWindow:AddScrollWindow(scrollWindow)

            -- No results returned; display a simple message to players.
            else
                dynWindow:AddLabel(400,350,"No Items Found",0,0,32,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            end

        end

        self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)
        self._FieldData = nil
    end,


    -- Register window reponse listener
    WindowResponse = function( user, buttonId, fieldData, self, root )
        self._FieldData = fieldData
        -- Check to see if we are in range of the board.
        if not( self.CheckBoardRange(self, root) ) then return false end

        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        --DebugMessage("Debug: {Action : " .. tostring(action) .. ", Arg: "..tostring(arg).."}")

        -- Show filter window
        if( action == "Filters" ) then 
            self.OpenWindow( self, "Filters", nil, fieldData )
            -- Toggle a filter off and on
        elseif( action == "ToggleFilter" ) then
            if( self._FilterStates[arg].Type ~= "Essence") then self.ClearFilters( self._FilterStates[arg].Type, arg, self ) end
            if( self._FilterStates[arg].State == "pressed" ) then self._FilterStates[arg].State = nil else self._FilterStates[arg].State = "pressed" end
            self.OpenWindow( self, "Filters", nil, fieldData )
        elseif( action == "SaveFilters" or action == "Search" ) then 
            
            -- Restricted Searched
            if( 
                (self._FilterStates.Recipes and self._FilterStates.Recipes.State == "pressed") or
                (self._FilterStates.Fabrication and self._FilterStates.Fabrication.State == "pressed") or 
                (self._FilterStates.Blacksmithing and self._FilterStates.Blacksmithing.State == "pressed") or 
                (self._FilterStates.Carpentry and self._FilterStates.Carpentry.State == "pressed") ) then
                    local found = false
                    for k,v in pairs( self._FilterStates ) do 
                        if(v.State == "pressed" and v.Recipe ~= nil) then
                            found = true
                        end
                    end

                    if( found == false ) then
                        self.ParentObj:SystemMessage("Please choose a recipe.")    
                        return 
                    end
            end
            
            local jsonStr = BazaarHelper.BuildSearchPacket( fieldData, self._FilterStates )
            --DebugMessage( jsonStr )
            self._ClusterController:SendMessage("DoHttpRequest", ServerSettings.Bazaar.GetURL.."?data="..jsonStr, self._SearchRequestId, self.ParentObj, "BazaarSearchResults")
        elseif( action == "Purchase" ) then
            self._PurchaseObjId = tonumber(arg)
            self._ClusterController:SendMessage("DoHttpRequest", ServerSettings.Bazaar.VerifyURL.."?data="..json.encode( { objectId = self._PurchaseObjId } ), self._SearchRequestId, self.ParentObj, "BazaarVerifyPurchaseable")
        else
            --DebugMessage("Closing Window!")
            EndMobileEffect(root)
        end
    end,

    ClearFilters = function( mType, key, self )
        for k,v in pairs( self._FilterStates ) do 
            if( mType == "WeaponTypes" and v.State == "pressed" and k ~= key ) then
                if( v.Type == "Weapons" or v.Type == "WandAttunement" or v.Type == "StaffAttunement" ) then
                    self._FilterStates[k].State = nil
                end
            end

            if( mType == "CrafterProfession" and v.State == "pressed" and k ~= key ) then
                if( v.Type == "RecipesFabrication" or v.Type == "RecipesBlacksmithing" or v.Type == "RecipesCarpentry" ) then
                    self._FilterStates[k].State = nil
                end
            end

            if( (mType == "Category" or v.Type == mType) and v.State == "pressed" and k ~= key) then
                self._FilterStates[k].State = nil
            end
        end
    end,

    DoBazaarPurchase = function( self, root, purchaseItem, backpackCanBuy )
        local purchaseObj = GameObj( tonumber( purchaseItem.objectId ) )
        --DebugTable(purchaseItem)
        --DebugMessage("["..purchaseObj.Id.."] : Bazaar Purchase")
        purchaseObj:SendMessageGlobal("BazaarPurchaseItem", self.ParentObj, purchaseItem, backpackCanBuy)
    end,

    _Args = {},
    _WindowID = nil,
    _Message = nil,
    _BazaarBoardObject = nil,
    _BoardName = "Bazaar Board",
    _LastCategory = nil,
    _FilterStates = {},
    _FilterSorted = {},
    _Location = nil,
    _SearchRequestId = nil,
    _FieldData = nil,
    _ClusterController = nil,
    _PurchaseObjId = nil,
    _Results = nil,
}
