MobileEffectLibrary.RankedArenaSignup = 
{
    OnEnterState = function(self,root,target,args)
        self._WindowID = "RankedArenaSignup"..uuid()
        self._ArenaMaster = target
        self._GroupMembers = {}
        self._GroupSize = 0
        self._GroupLeader = nil
        self._IsQueued = false

        -- Make sure we found the township board near us.
        if( self._ArenaMaster == nil ) then
            self._Message = "You are too far away to do that."
            EndMobileEffect(root)
            return false
        end

        self._GroupID = GetGroupId( self.ParentObj )

        -- They are queuing as a group!
        if( self._GroupID ) then
            self._GroupLeader = GetGroupVar(self._GroupID, "Leader")
            if( self._GroupLeader ~= self.ParentObj ) then
                self._Message = "You must be the group leader to queue for arena matches."
                EndMobileEffect(root)
                return false
            end

            self._GroupMembers = GetGroupVar(self._GroupID, "Members")
            self._GroupSize = CountTable( self._GroupMembers )
            
            -- Make sure they don't have more than 3 members in their group
            if( self._GroupSize > 3 ) then
                self._Message = "Your group has too many members to sign up for an areana match. Maximum group size is 3."
                EndMobileEffect(root)
                return false
            end

        -- They are queuing solo!
        else
            self._GroupSize = 1
            self._GroupMembers = { self.ParentObj }
        end

        -- Register window reponse listener
        RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)
        
        -- Resgister logout listener
        RegisterSingleEventHandler(EventType.UserLogout,"",function (...)EndMobileEffect(root)end)

        -- Register arena message listener
        RegisterEventHandler(EventType.Message, "RankedArenaMessage",  function ( args ) self.ProcessArenaMessage(self, root, args) end)

        -- Open the window
        self.OpenWindow(self)

        -- Tell the arena master we are ready!
        self._ArenaMaster:SendMessage("RankedArenaMessage", {Action = "EnterQueue", Player = self.ParentObj, GroupSize = self._GroupSize, GroupMembers = self._GroupMembers })
    end,

    OnExitState = function(self,root)
        self.ParentObj:CloseDynamicWindow(self._WindowID)

        UnregisterEventHandler("", EventType.Message, "RankedArenaMessage")
        UnregisterEventHandler("",EventType.DynamicWindowResponse, self._WindowID)
                
        -- Let each group member know we left the queue!
        if( self._IsQueued ) then
            self._ArenaMaster:SendMessage("RankedArenaMessage", {Action = "LeaveQueue", Player = self.ParentObj})
            self.ForEachGroupMember( self, function( member ) 
                member:SystemMessage("Your group has left the arena queue.")
            end)
        end
        

        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        
        CloseDynamicEventHUD( self.ParentObj )
    end,

    -- Control when the window opens
    OpenWindow = function(self)
        --local dynWindow = RankedArena.ShowSignupWindow( self._WindowID, self._ArenaMaster, self._GroupMembers )
        --self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)
    end,

    -- Register window reponse listener
    WindowResponse = function( user, buttonId, fieldData, self, root )
        self._FieldData = fieldData
        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]
        local argValue = result[3]

        -- Show filter window
        if( action == "Leave" ) then 
            EndMobileEffect(root)
        else
            EndMobileEffect(root)
        end
    end,

    ForEachGroupMember = function( self, cb )
        cb = cb or function()end
        for key,member in pairs( self._GroupMembers ) do 
            if( member:IsValid() ) then
                cb( member )
            end
        end
    end,

    ProcessArenaMessage = function( self, root, args )
        if( args.Action == "OutOfRange" ) then
            self._Message = args.Message or "Arena queue cancelled, a group member is out of range."
            EndMobileEffect(root)
        elseif( args.Action == "NoPetsAllowed" ) then
            self._Message = args.Message or "Pets are not allowed in arena matches. Please stable any pets on your team."
            EndMobileEffect(root)
        elseif( args.Action == "QueueSuccessful" ) then
            self._IsQueued = true
            self.ForEachGroupMember( self, function( member ) 
                member:SystemMessage("You are queued for an arena match. Please stay close to the arena area.", "info")
                member:SystemMessage("You are queued for an arena match. Please stay close to the arena area.")
            end)

        elseif( args.Action == "AlreadyQueued" ) then
            self.ParentObj:CloseDynamicWindow(self._WindowID)
            self._Message = args.Message or "Yourself or a party member are already queued for an arena match."
            EndMobileEffect(root)

        elseif( args.Action == "CountdownPulse" ) then

            self.ParentObj:CloseDynamicWindow(self._WindowID)

            -- Let each group member know we left the queue!
            self.ForEachGroupMember( self, function( member ) 
                member:PlayObjectSound("event:/ui/drum1", false, 0, false, false)
                member:SystemMessage("Arena Match starts in [FFFF00]" .. args.Countdown .. "[-] seconds.", "info")
                member:SystemMessage("Arena Match starts in [FFFF00]" .. args.Countdown .. "[-] seconds.")
            end)

        elseif( args.Action == "MatchStarted" ) then
            self.ForEachGroupMember( self, function( member ) 
                --DebugMessage(member:GetName() .. " should be entering arena!")
                member:SendMessage("StartMobileEffect", "RankedArenaMatch", args.ArenaMaster, args.MatchData)
            end)
            EndMobileEffect(root)
        end
    end,

}
