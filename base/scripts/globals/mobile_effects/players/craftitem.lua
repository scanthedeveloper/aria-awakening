MobileEffectLibrary.CraftItem = 
{
    ForceDismount = true,
    EndOnMovement = true,

    OnEnterState = function(self,root,target,args)

       --DebugMessage("CraftItem : MOBILE EFFECT STARTED")
       --DebugMessage( DumpTable( args ) )

        if( not IsPlayerCharacter(self.ParentObj) ) then
            --DebugMessage("Not Player!")
            EndMobileEffect(root)
            return false 
        end

        self._Args = args or {}
        if( self._Args.Recipe == nil or self._Args.Skill == nil ) then
           -- DebugMessage("Missing Args!", DumpTable( self._Args ))
            EndMobileEffect(root)
            return false 
        end

        if( not Materials[self._Args.Quality] ) then
            self._Args.Quality = nil
        end

        RegisterEventHandler(EventType.CreatedObject,"crafted_item", function( success, objRef, wasStacked, stackCount )
            self.HandleItemCreation(self, root, success, objRef, wasStacked, stackCount)
        end)

        RegisterEventHandler(EventType.Timer,"AutoCraft",function ( ... )
            self.PreCraftSteps( self, root )
        end)

        self.PreCraftSteps( self, root )
        
    end,

    OnExitState = function(self,root)
        
        if( self._MovementFiredExit ) then
            self.Message = "Crafting interrupted."
        end

        self.CleanUp(self, root)
        
        
    end,

    PreCraftSteps = function( self, root )

        --DebugMessage("CraftItem : PreCraftSteps")
        self.ParentObj:CloseDynamicWindow("CraftingWindow")
        self._SkillName = self._Args.Skill
        self._SkillNameFriendly = StripFromString(self._Args.Skill,"Skill")

        -- We need to find a tool near us of one isn't there, leave
        -- Get table of recipes for the crafting skill we're using
        --DebugMessage("CraftItem : PreCraftSteps 1")
        if( not self._Args.Tool ) then 
            self.Message = "[FA0C0C]You are too far to use that![-]"
            EndMobileEffect(root)
            return
        end

        -- Can we see the tool?
        --DebugMessage("CraftItem : PreCraftSteps 2")
        if not(self.ParentObj:HasLineOfSightToObj(self._Args.Tool,ServerSettings.Combat.LOSEyeLevel)) and not(self._Args.Tool:HasObjVar("IgnoreLOS")) then 
            self.Message = "[FA0C0C]You cannot see that![-]"
            EndMobileEffect(root)
            return
        end

        -- Are we close to the tool?
        --DebugMessage("CraftItem : PreCraftSteps 3")
        local toolLoc = self._Args.Tool:GetLoc()
        if(self.ParentObj:GetLoc():Distance(toolLoc) > self._UsageDistance ) then
            self.Message = "[FA0C0C]You are too far to use that![-]"
            EndMobileEffect(root)
            return
        end

        -- Get player skill level
        --DebugMessage("CraftItem : PreCraftSteps 4")
        self._SkillLevel = GetSkillLevel(self.ParentObj,self._Args.Skill)

        -- Get table of recipes for the crafting skill we're using
        --DebugMessage("CraftItem : PreCraftSteps 5")
        self._RecipeSkillTable = GetRecipeTableFromSkill(self._Args.Skill)[self._Args.Recipe]
        self._ConsumeTable = self._RecipeSkillTable.Resources
        --DebugMessage( DumpTable( self._RecipeSkillTable ) )
        if( not self._RecipeSkillTable ) then 
            self.Message = "[FA0C0C]Something went wrong, try again![-]"
            EndMobileEffect(root)
            return
        end

        -- Get the minimum skill to craft the item
        self._MinimumSkillLevel, self._MaximumSkillLevel = GetRecipeSkillRequired(self._Args.Recipe,self._Args.Quality)

        -- If the user doesn't have enough skill to craft this lets exit out.
        --DebugMessage("CraftItem : PreCraftSteps 6")
        if( self._SkillLevel < self._MinimumSkillLevel ) then
            self.Message = "You lack the "..self._SkillNameFriendly.." skill to craft: " .. self._RecipeSkillTable.DisplayName .. "" .."\r\n[FA0C0C]Requires: "..self._MinimumSkillLevel.."[-]"
            EndMobileEffect(root)
            return
        end

        -- Does the player have the recipe?
        --DebugMessage("CraftItem : PreCraftSteps 7")
        if (not HasRecipe(self.ParentObj,self._Args.Recipe,self._Args.Skill)) then

            self.Message = "[FA0C0C]You are missing the recipe for "..self._Args.Recipe.."[-]"
            EndMobileEffect(root)
            return
        end

        -- Does the player have the resources?
        --DebugMessage("CraftItem : PreCraftSteps 8")
        if not(HasResources(self._ConsumeTable, self.ParentObj, self._Args.Quality)) then
			self.Message = "[FA0C0C]You are missing the required resources.[-]"
            EndMobileEffect(root)
            return
        end

        self.TryCraftItem(self, root)


    end,

    TryCraftItem = function(self, root)
        --DebugMessage("CraftItem : TryCraftItem")

        -- If no skill name we need to leave!
        if(self._SkillName == nil) then 
            self.Message = "[FA0C0C]Something went wrong, try again![-]"
            EndMobileEffect(root)
            return
        end

        --DebugMessage("CRAFTITEM : 2")
        -- Get our chance to successfully craft the item
        local chance = SkillValueMinMax(self._SkillLevel, self._MinimumSkillLevel, self._MaximumSkillLevel)	
        --DebugMessage( tostring(self.ParentObj), tostring(self._SkillName), tostring(self._SkillLevel), tostring(chance) )
        self._Success = CheckSkillChance(self.ParentObj, self._SkillName, self._SkillLevel, chance, true)

        --DebugMessage("Success : " .. tostring(self._Success))

        --DebugMessage("CRAFTITEM : 3")
        -- If we failed we need to consume the right resources
        if( not(self._Success) and self._RecipeSkillTable.ConsumeOnFail ) then
            self._ConsumeTable = {}
		    for resourceName,resourceAmount in pairs(self._RecipeSkillTable.Resources) do 
			    self._ConsumeTable[resourceName] = self._RecipeSkillTable.ConsumeOnFail[resourceName]
		    end
        end

        --DebugMessage("CRAFTITEM : 4")
        -- Lets eat the resources!

        -- End Combat
        self.ParentObj:SendMessage("EndCombatMessage")
        self._CraftingDuration = self.GetCraftingDuration(self, root)

        --DebugMessage( "CraftingDuration" , self._CraftingDuration, self._SkillName )
        
        -- Face the tool
        if( self._Args.Tool ) then 
            FaceObject(self.ParentObj, self._Args.Tool)
        else
            self.Message = "[FA0C0C]You are not near the required tool to make that.[-]"
            EndMobileEffect(root)
            return
        end

        -- Play tool sound and animations
        if( self._Args.Tool ) then
            local toolAnim = self._Args.Tool:GetObjVar("ToolAnimation")
            if(toolAnim ~= nil) then
                --DebugMessage("PLAY ANIMATION")
                self.ParentObj:PlayAnimation(toolAnim)
            end

            local toolSound = self._Args.Tool:GetObjVar("ToolSound")
            if(toolSound ~= nil) then
                --DebugMessage("PLAY SOUND")
                self.ParentObj:PlayObjectSound(toolSound,false,self._CraftingDuration)
            end
        end


        CallFunctionDelayed(TimeSpan.FromSeconds(self._CraftingDuration),function()
            if( self._MovementFiredExit ) then return end
            local success = ConsumeResources(self._ConsumeTable, self.ParentObj, "crafting_controller", self._Args.Quality)
            self:HandleConsumeResourceResponse(root,success,"crafting_controller")
        end)
    end,

    GetCraftingDuration = function( self, root )
        if(self._SkillLevel >= self._MaximumSkillLevel) then
            if( 
                ( self._SkillNameFriendly == "Metalsmith" and ProfessionsHelpers.IsTaskCompleted(self.ParentObj,"Blacksmith",4) )
                or ( self._SkillNameFriendly == "Woodsmith" and ProfessionsHelpers.IsTaskCompleted(self.ParentObj,"Carpenter",4) )
                or ( self._SkillNameFriendly == "Fabrication" and ProfessionsHelpers.IsTaskCompleted(self.ParentObj,"Tailor",4) )
            ) then
                return GRANDMASTER_CRAFTING_DURATION
            elseif( 
                ( self._SkillNameFriendly == "Metalsmith" and ProfessionsHelpers.IsTaskCompleted(self.ParentObj,"Blacksmith",3) )
                or ( self._SkillNameFriendly == "Woodsmith" and ProfessionsHelpers.IsTaskCompleted(self.ParentObj,"Carpenter",3) )
                or ( self._SkillNameFriendly == "Fabrication" and ProfessionsHelpers.IsTaskCompleted(self.ParentObj,"Tailor",3) )
            ) then
                return MASTER_CRAFTING_DURATION
            elseif( 
                ( self._SkillNameFriendly == "Metalsmith" and ProfessionsHelpers.IsTaskCompleted(self.ParentObj,"Blacksmith",2) )
                or ( self._SkillNameFriendly == "Woodsmith" and ProfessionsHelpers.IsTaskCompleted(self.ParentObj,"Carpenter",2) )
                or ( self._SkillNameFriendly == "Fabrication" and ProfessionsHelpers.IsTaskCompleted(self.ParentObj,"Tailor",2) )
            ) then
                return JOURNEYMAN_CRAFTING_DURATION
            else
                return BASE_CRAFTING_DURATION
            end
        -- The item is not trivial!
        else
            return BASE_CRAFTING_DURATION
        end
    end,

    HandleConsumeResourceResponse = function( self, root, _success, _transationId )
        
        --DebugMessage("CONSUMED RESOURCES", tostring( _success ), tostring(_transationId) )

        -- Transaction id was is bad...
        if(_transationId ~= "crafting_controller") then 
            self.Message = "[FA0C0C]Something went wrong, try again![-]"
            EndMobileEffect(root)
            return
        end

        if( not _success ) then
            self.Message = "[FA0C0C]You are missing the required resources.[-]"
            EndMobileEffect(root)
            return
        end

        if( self._ResourcesAlreadyConsumed ) then return end
        self._ResourcesAlreadyConsumed = true

        local chance = SkillValueMinMax(self._SkillLevel, self._MinimumSkillLevel, self._MaximumSkillLevel)	
            CheckSkillChance(self.ParentObj, self._SkillName, self._SkillLevel, chance)

            self._Args.AutoCraftCompleted = self._Args.AutoCraftCompleted + 1

            -- If we failed we want to get out
            if( self._Success ~= true ) then
                self.Message = "[FA0C0C]You fail to create the item.[-]"
                self.ParentObj:SystemMessage(self.Message, "info")
                self.Message = nil

                if (self._Args.AutoCraft and self._Args.AutoCraftCompleted <= self._Args.AutoCraftQuantity ) then
                    --DebugMessage(" -- AUTO CRAFT NEEDED -- ")
                    self._ResourcesAlreadyConsumed = false
                    self.ParentObj:FireTimerAsync("AutoCraft")
                else
                    EndMobileEffect(root)
                    return
                end
            else
                self.CreateCraftedItem( self, root, item )
            end
    end,

    CreateCraftedItem = function(self, root, item)

        self._ItemTemplate = self._RecipeSkillTable.CraftingTemplateFile
        
        if(self._ItemTemplate == nil)then
            self.Message = "[FA0C0C]Something went wrong, try again![-]"
            EndMobileEffect(root)
            return
        end

        local backpackObj = self.ParentObj:GetEquippedObject("Backpack")
        local randomLoc = GetRandomDropPosition(self.ParentObj)

        local creationTemplateData=GetTemplateData(self._ItemTemplate)
        local resourceType=nil

        -- Get resource type for items that are resources
        if (creationTemplateData ~= nil) then
            if creationTemplateData.ObjectVariables~=nil then
                resourceType=creationTemplateData.ObjectVariables.ResourceType
            end
        end

        -- Fire Quest Event
        --DebugMessage( "QUEST EVENT : ", tostring(self.ParentObj), "Crafting", DumpTable( { self._RecipeSkillTable.Category, self._ItemTemplate, self._Args.Quality } ), self._RecipeSkillTable.StackCount or 1 )
        Quests.SendQuestEventMessage( self.ParentObj, "Crafting", { self._RecipeSkillTable.Category, self._ItemTemplate, self._Args.Quality }, self._RecipeSkillTable.StackCount or 1 )

        -- Check if the item is a stackable resource
        if (resourceType ~= nil) and (creationTemplateData.LuaModules ~= nil) and (creationTemplateData.LuaModules.stackable ~= nil) then
            -- try to add to the stack in the players pack		
            local stackCount = self._RecipeSkillTable.StackCount or 1
            local stackSuccess, stackObj = TryAddToStack(resourceType,backpackObj,stackCount)
            if not( stackSuccess ) then
                -- no stack in players pack so create an object in the pack
                --DebugMessage("Created Stackable : No Stack")
                CreateObjInBackpackOrAtLocation(self.ParentObj,self._ItemTemplate,"crafted_item",false,stackCount)
                
            else
                --DebugMessage("Created Stackable : Found Stack")
                self.HandleItemCreation(self, root, true, stackObj, true)
            end
            
            return -- Leave - we're done here
        end	

        weight = GetTemplateObjectProperty(self._ItemTemplate,"Weight")
            
        if(weight == -1) then
            --DebugMessage("Created Packed")
            CreatePackedObjectInBackpack(self.ParentObj,self._ItemTemplate,false,"crafted_item")    
        else
            --DebugMessage("Created Non-Packed")
            CreateObjInBackpackOrAtLocation(self.ParentObj, self._ItemTemplate,"crafted_item")     
        end
    end,

    HandleItemCreation = function(self, root, success,objRef,wasStacked,stackCount)	
        --DebugMessage(" HandleItemCreation ")
        if not(success == true) then
            self.Message = "[FA0C0C]Something went wrong, try again![-]"
            EndMobileEffect(root)
            return
        end

        objRef:SetObjVar("Crafter", self.ParentObj)
        objRef:SetObjVar("CraftedBy", self.ParentObj:GetName())

        if ( self._Args.Quality ) then
            ApplyCraftedMaterialProperties(objRef, self._Args.Quality, self._SkillLevel)
        end
        
        local name = objRef:GetName()

        if (objRef:GetObjVar("ResourceType") == "PackedObject") then
            name = StripColorFromString(GetTemplateObjectName(self._ItemTemplate)).." (Packed)"
        end

        if not(wasStacked) then
            objRef:SetName(name)
        end

        if(stackCount) then
            RequestAddToStack(objRef,stackCount - 1)
        end

        SetItemTooltip(objRef)

        -- Is this item craftable?
        if( EnchantingHelper.IsEnchantable( objRef ) and 
            (
                (
                HasMobileEffect(self.ParentObj, "ArtifactSchematic") or
                HasMobileEffect(self.ParentObj, "ArtifactSchematicLesser") or
                HasMobileEffect(self.ParentObj, "ArtifactSchematicGreater")
                ) and 
                Success(ServerSettings.Crafting.ArtifactChance)
            ) 
        ) then
            objRef:SetObjVar("ArtifactCrafted", self.ParentObj)
            EnchantingHelper.ApplyRandomEnchantsToItem( objRef, self.ParentObj, self._Args.Quality )
            self.ParentObj:SystemMessage("Crafted an artifact!", "info")
            self.ParentObj:PlayObjectSound("event:/ui/quest_complete", false, 0, false, false)
            self.ParentObj:PlayEffect("HeadFlareEffect",3)
            self.ParentObj:NpcSpeech("[00FF00]Magic Artifact Created![-]","combat")
        else
                --SCAN ADDED MAGIC CRAFTING
                -- This prevents resources from receiving magic bonuses.
                local resourceType = objRef:GetObjVar("ResourceType")
                if (resourceType == nil) then
                    --Creates a 13% chance per craft that the user might make a magic item
                    local randomNumber = math.random(1,100)
                    if (randomNumber > 87 and randomNumber < 101) then
                        objRef:SetObjVar("MagicCrafted", self.ParentObj)
                        --Added a durabiility bonus of 50-100 from magic crafting.
                        local durability = objRef:GetObjVar("MaxDurability")
                        local durabilityBonus = math.random(50,200)
                        local totalDurabilityBonus = durability + durabilityBonus
                        objRef:SetObjVar("DuraBonus", durabilityBonus)
                        objRef:SetObjVar("Durability", totalDurabilityBonus)
                        objRef:SetObjVar("MaxDurability", totalDurabilityBonus)
                        self.ParentObj:PlayEffect("Teleport1Effect")
                        self.ParentObj:NpcSpeech("[00FF00]Magic Bonus Added[-]","combat")
                        self.ParentObj:PlayObjectSound("event:/ui/quest_complete", false, 0, false, false)
                        --Determines the quality of the enchant
                        local enchantingRng = (math.random(2,3))
                        EnchantingHelper.ApplyRandomEnchantsToItem( objRef, self.ParentObj, self._Args.Quality, true, enchantingRng, 1 )
                        --self.ParentObj:SystemMessage("You created a Magic Item!", "info")
                    end
                else
                self.ParentObj:SystemMessage("Crafted " .. name .. ".", "info")
            end
        end

        if (self._Args.AutoCraft and self._Args.AutoCraftCompleted < self._Args.AutoCraftQuantity ) then
            --DebugMessage(" -- AUTO CRAFT NEEDED -- ")
             self._ResourcesAlreadyConsumed = false
             -- This must be done in the next frame so the object database can destroy the resources
             self.ParentObj:FireTimerAsync("AutoCraft")            
        else
            --DebugMessage(" -- AUTO CRAFT NOT NEEDED -- ")
            if( self._Args.OpenWindow ) then
                CallFunctionDelayed(TimeSpan.FromSeconds(0.5),function()
                    self.ParentObj:SendMessage("ShowCraftingMenu", objRef, false, false )
                end)
            end

            self.Message = nil
            EndMobileEffect(root)
        end
    end,

    CleanUp = function(self, root)
        if( self.Message ) then
            self.ParentObj:SystemMessage(self.Message, "info")
        end

        --DebugMessage( "Clean Up : ", tostring( self.Message ) )
        --self.ParentObj:DelModule("base_crafting_controller")
        self.ParentObj:PlayAnimation("idle")
        UnregisterEventHandler("",EventType.Message, "ConsumeResourceResponse")
        UnregisterEventHandler("",EventType.CreatedObject, "crafted_item")
        UnregisterEventHandler("",EventType.Timer,"AutoCraft")
    end,


    _Args = {},
        -- Quality
        -- Skill
        -- Recipe
        -- Tool
    _Material = nil,
    _SkillLevel = nil,
    _SkillName = nil,
    _SkillNameFriendly = nil,
    _MinimumSkillLevel = nil,
    _RecipeSkillTable = {},
    _UsageDistance = 5,
    _Success = false,
    _ConsumeTable = {},
    _CraftingDuration = nil,
    _ItemTemplate = nil,
    _ResourcesAlreadyConsumed = false

}
