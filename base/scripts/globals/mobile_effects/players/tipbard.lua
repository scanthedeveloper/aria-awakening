MobileEffectLibrary.TipBard = 
{
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self._WindowID = "GiveTipWindow"..uuid()
        self.isPlayer = IsPlayerCharacter(self.ParentObj)
        self.Target = target or nil
        self._Message = nil

        -- Make sure we're a player
        if( self.isPlayer ) then


            -- They cannot tip this bard
            if not( self.CanGiveTip(self) ) then
                EndMobileEffect(root)
                return false
            -- If the ledger didn't open lets exit out of the mobile effect
            elseif not( self.ShowTipWindow(self) ) then
                self.ParentObj:SystemMessage("Unable to open tip window.", "info")
                EndMobileEffect(root)
                return false
            end

            -- Register window reponse listener
            RegisterSingleEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)
                
            RegisterSingleEventHandler(EventType.Message, "ConsumeResourceResponse", 
            function (success,transactionId,user) 
                if (success) then

                    local tipString = "You tipped the bard "..self._TipAmount.." coins."
                    self.ParentObj:SystemMessage(tipString)
                    self.ParentObj:SystemMessage(tipString,"info")
                    self.ParentObj:PlayObjectSound("event:/objects/pickups/coins/coins_drop",false)

                    local userToTipString  = self.ParentObj:GetName().." gave a "..tostring(self._TipAmount).." coin tip. The coins have been sent to your bank.","info"
                    self.Target:SystemMessage(userToTipString)
                    self.Target:SystemMessage(userToTipString, "info")
                    self.Target:PlayObjectSound("event:/objects/pickups/coins/coins_drop",false)
                    
                    local targetBankObj = self.Target:GetEquippedObject("Bank")
                    Create.Coins.InContainer(targetBankObj, self._TipAmount)
                    
                else
                    self.ParentObj:SystemMessage("Failed to tip bard.","info")
                end
                EndMobileEffect(root)
            end)

        -- This is not a player, so we need to end mobile effect.
        else
            EndMobileEffect(root)
            return false
        end

    end,


    OnExitState = function(self,root)
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,
    
    ShowTipWindow = function(self)

        if not ( self.CanGiveTip(self) ) then
            EndMobileEffect(root)
            return
        end

        local dynWindow = DynamicWindow(self._WindowID, "Give Tip",256,128,0,0,"")
        dynWindow:AddLabel(64-15,10,Denominations[1].Color..Denominations[1].DisplayName,0,0,24)
        dynWindow:AddTextField(64+35,10,50,20,"Gold","0")
        dynWindow:AddButton(64-25,40,"Accept","Tip",157,27,"","",false,"")
        self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)

        return true

    end,

    CanGiveTip = function(self)

        -- Is the target valid?
        if not (self.Target or self.Target:IsValid()) then
            self._Message = "Invlaid target."
            return false
        end

        -- Are the players close enough to each other?
        if not ( self.ParentObj:DistanceFrom(self.Target) <= 10 ) then
            self._Message = "Too far away to tip."
            return false
        end

        -- Does the bard have a hearth near them?
        if ( Instrument.GetNearbyHearth(self.Target) == nil ) then
            self._Message = "Can only tip bards which are near a hearth."
            return false
        end

        local userListenToBandId = self.ParentObj:GetObjVar("ListeningToBandID")
        local targetBandId = self.Target:GetObjVar("BandID")
        if not(userListenToBandId == targetBandId) then
            self._Message = "You must listen to the bard before tipping."
            return false
        end

        return true

    end,

    WindowResponse = function( user, buttonId, fieldData, self, root )
        --DebugMessage( "User: " .. tostring(user) .. " ButtonID: " .. buttonId )
        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        -- Accept button
        if( action == "Accept" ) then 
            self.AcceptTip( self, root, fieldData )
        else 
            EndMobileEffect(root) 
        end

    end,

    AcceptTip = function( self, root, fieldData )
        if not(self.CanGiveTip(self)) then
            EndMobileEffect(root)
            return
        end

        --Consume resources, add to userToTip's bank
        self._TipAmount = tonumber(fieldData.Gold) or 0
        local backpackObj = self.ParentObj:GetEquippedObject("Backpack")
        RequestConsumeResource(self.ParentObj,"coins",self._TipAmount,"ConsumeResourceResponse",self.ParentObj)

    end,
    
    _Args = {},
    _WindowID = nil,
    _TipAmount = 0,
    isPlayer = false,

}
