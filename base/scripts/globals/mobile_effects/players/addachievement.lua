MobileEffectLibrary.AddAchievement = 
{
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self.isPlayer = IsPlayerCharacter(self.ParentObj)

        if( self._Args.Category == nil or self._Args.Achievement == nil  or self._Args.Type == nil ) then
            self.ParentObj:SystemMessage("Unable to perform that action.", "info")
            EndMobileEffect(root)
            return false
        end

        if( self.isPlayer ) then
        
            if( HasAchievement( self.ParentObj, self._Args.Achievement ) ) then
                self.ParentObj:SystemMessage("You have already earned this achievement.", "info")
                EndMobileEffect(root)
                return false
            end

            CheckAchievementStatus(self.ParentObj, self._Args.Category, self._Args.Type, 1)
        end

        EndMobileEffect(root)
    end,

    _Args = {},
    isPlayer = false,

}
