MobileEffectLibrary.RankedArenaSetup = 
{
    OnEnterState = function(self,root,target,args)
        self._WindowID = "RankedArenaSetup" --..uuid()
        self._ArenaMaster = target
        self._MatchType = nil
        self._TeamA = { self.ParentObj }
        self._TeamB = {}
        self._Rank = self.ParentObj:GetObjVar("ArenaRank") or RankedArena.DefaultScore
        self._Countdown = nil
        self._MatchStarted = false
        self._Ready = false
        self._Queued = false

        -- Make sure we found the township board near us.
        if( self._ArenaMaster == nil ) then
            self._Message = "You are too far away to do that."
            EndMobileEffect(root)
            return false
        end

        -- Open the window
        self.OpenWindow(self)

        -- Register window reponse listener
        RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, fieldData, self, root ) end)
        
        -- Resgister logout listener
        RegisterSingleEventHandler(EventType.UserLogout,"",function (...)EndMobileEffect(root)end)

        -- Register arena message listener
        RegisterEventHandler(EventType.Message, "RankedArenaMessage",  function ( args ) self.ProcessArenaMessage(self, root, args) end)
    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "RankedArenaMessage")
        UnregisterEventHandler("",EventType.DynamicWindowResponse, self._WindowID)
        
        -- If the match has not started yet, and the organizer cancels we end it for all players.
        if( not self._MatchStarted ) then
            RankedArena.SendMessageToPlayers( self._TeamA, self._TeamB, {Action = "CancelMatch", Message = self._Message } )
        end

        if( self._Queued ) then
            self._ArenaMaster:SendMessage("RankedArenaMessage", {Action = "LeaveQueue", Player = self.ParentObj})
        end
        
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        self.ParentObj:CloseDynamicWindow(self._WindowID)
        CloseDynamicEventHUD( self.ParentObj )
    end,

    -- Control when the window opens
    OpenWindow = function(self)
        --local dynWindow = RankedArena.ShowArenaWindow( self._WindowID, self._ArenaMaster, self._MatchType, self._TeamA, self._TeamB, self._Rank, nil, self._Countdown, self._Ready, self._Queued )
        --self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)
    end,

    -- Register window reponse listener
    WindowResponse = function( user, buttonId, fieldData, self, root )
        self._FieldData = fieldData
        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]
        local argValue = result[3]

        -- Show filter window
        if( action == "OneVersusOne" ) then 
            self._MatchType = RankedArena.MatchTypes[action]
            self.OpenWindow( self )
        elseif( action == "TwoVersusTwo" ) then 
            self._MatchType = RankedArena.MatchTypes[action]
            self.OpenWindow( self )
        elseif( action == "ThreeVersusThree" ) then 
            self._MatchType = RankedArena.MatchTypes[action]
            self.OpenWindow( self )
        elseif( action == "AddPlayer" and arg == "Opponent" ) then 
            RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse,"AddOpponent",function(target,user) 
                if( self.ValidateAddPlayer( self, target ) ) then
                    table.insert( self._TeamB, target )
                    self.AddPlayerToMatchQueue(self, target)
                    self.OpenWindow( self )
                else
                    self.ParentObj:SystemMessage("Target cannot be added to this match.")
                end

            end)
            self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "AddOpponent")
        elseif( action == "AddPlayer" and arg == "Friendly" ) then 
            RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse,"AddFriendly",function(target,user) 
                if( self.ValidateAddPlayer( self, target ) ) then
                    table.insert( self._TeamA, target )
                    self.AddPlayerToMatchQueue(self, target)
                    self.OpenWindow( self )
                else
                    self.ParentObj:SystemMessage("Target cannot be added to this match.")
                end

            end)
            self.ParentObj:RequestClientTargetGameObj(self.ParentObj, "AddFriendly")
        
        elseif( action == "StartMatch" ) then
            self._ArenaMaster:SendMessage("RankedArenaMessage",
                {
                    Action = "StartMatch",
                    matchType = self._MatchType,
                    TeamA = self._TeamA,
                    TeamB = self._TeamB,
                }
            )
        elseif( action == "CancelMatch" ) then
            self._Message = args.Message or "The match has been cancelled."
            EndMobileEffect(root)
        elseif( action == "Ready" ) then
            self._Ready = true
            self._ArenaMaster:SendMessage("RankedArenaMessage", {Action = "Ready", Player = self.ParentObj})
            self.OpenWindow( self )
        else
            EndMobileEffect(root)
        end
    end,

    ValidateAddPlayer = function( self, target )
        if( not IsPlayerCharacter( target )  ) then return false end
        
        for i=1, #self._TeamA do 
            if( self._TeamA[i] == target ) then return false end
        end

        for i=1, #self._TeamB do 
            if( self._TeamB[i] == target ) then return false end
        end

        if( self.ParentObj:DistanceFrom(target) > RankedArena.MaximumDistance ) then
            return false
        end

        local pets, slots = GetActivePets( target )
        if( #pets > 0 ) then
            self.ParentObj:SystemMessage(target:GetName() .. " has active pets and cannot participate in arena matches.")
            self.ParentObj:SystemMessage(target:GetName() .. " has active pets and cannot participate in arena matches.", "info")
            return false
        end

        if( 
            HasMobileEffect( target, "RankedArenaSetup" ) or 
            HasMobileEffect(target, "RankedArenaQueued") or 
            HasMobileEffect(target, "RankedArenaMatch")
        ) then
            self.ParentObj:SystemMessage(target:GetName() .. " cannot participate in a match right now.")
            return false
        end

        return true
    end,

    AddPlayerToMatchQueue = function( self, target )
        target:SendMessage(
            "StartMobileEffect",
            "RankedArenaQueued", 
            self.ParentObj,
            {
                arenaMaster = self._ArenaMaster,
                matchType = self._MatchType,
                TeamA = self._TeamA,
                TeamB = self._TeamB,
            } 
        )
    end,

    ProcessArenaMessage = function( self, root, args )
        --DebugMessage("ProcessArenaMessage: " .. args.Action)
        if( args.Action == "LeaveMatch" ) then
            if( self._Queued == true ) then
                self._Queued = false
                self._ArenaMaster:SendMessage("RankedArenaMessage", {Action = "LeaveQueue", Player = self.ParentObj})
            end

            self._TeamA, self._TeamB = RankedArena.RemovePlayer( self._TeamA, self._TeamB, args.Player )
            RankedArena.SendMessageToPlayers( self._TeamA, self._TeamB, 
                {
                    Action = "UpdateTeams",
                    TeamA = self._TeamA,
                    TeamB = self._TeamB,
                    Queue = self._Queued,
                }
            )
            self.ParentObj:SystemMessage("[ "..args.Player:GetName().." ] has left the match.")
            self.OpenWindow( self )
        
        elseif( args.Action == "CancelMatch" ) then
            self._Message = args.Message or "The match has been cancelled."
            EndMobileEffect(root)
        elseif( args.Action == "MatchQueued" ) then
            self.ParentObj:SystemMessage(args.Message, "info")
            self._Queued = true
            self.OpenWindow( self )
        elseif( args.Action == "MatchCountdown" ) then
            self._Countdown = args.Countdown
            self.OpenWindow(self)
            self.ParentObj:PlayObjectSound("event:/ui/drum1", false, 0, false, false)
        elseif( args.Action == "MatchStarted" ) then
            self._MatchStarted = true
            self.ParentObj:SendMessage("StartMobileEffect", "RankedArenaMatch", args.ArenaMaster, args.MatchData)
            EndMobileEffect(root)
        end
    end,

}
