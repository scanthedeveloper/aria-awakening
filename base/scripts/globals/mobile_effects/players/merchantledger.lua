MobileEffectLibrary.MerchantLedger = 
{
	OnEnterState = function(self,root,target,args)
        self._Args = args
        self._WindowID = "ShowSaleList_"..uuid()
        self.isPlayer = IsPlayerCharacter(self.ParentObj)
        self.Target = target
        self._SelectedTab = args.SelectedTab or "For Sale"
        self._IsOwner = args.IsOwner or false

        -- Make sure we're a player
        if( self.isPlayer ) then

            -- If the ledger didn't open lets exit out of the mobile effect
            if not( self.ShowLedger(self) ) then
                self.ParentObj:SystemMessage("Unable to open ledger.", "info")
                EndMobileEffect(root)
                return false
            -- This is a player and the ledger opened correctly, lets do this!
            else
                -- Register window reponse listener
                RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId ) self.WindowResponse( user, buttonId, self, root ) end)


            end
        -- This is not a player, so we need to end mobile effect.
        else
            EndMobileEffect(root)
            return false
        end
	
    end,


    OnExitState = function(self,root)
        UnregisterEventHandler("",EventType.DynamicWindowResponse,self._WindowID)
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,
    
    ShowLedger = function(self)
        return HelperHiredMerchants.ShowSaleList(self.ParentObj, self.Target, self._WindowID, self._SelectedTab, self._SortBy, self._SortDirection, self._IsOwner, self._CurrentPage)
    end,

    WindowResponse = function( user, buttonId, self, root )
        --DebugMessage( "User: " .. tostring(user) .. " ButtonID: " .. buttonId )
        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        if( action == "SelectTab" ) then self.ChangeTab( arg, self )
        elseif( action == "Prev" ) then self.ChangePage(false, self)
        elseif( action == "Next" ) then self.ChangePage(true, self)
        elseif( action == "Select" ) then self.BuyItem( arg, self, root )
        elseif( action == "Action" and arg == "Sell" ) then self.SellItem(arg, self, root)
        elseif( action == "Sort" ) then self.SortResults( arg, self )
        elseif( action == "ClaimReservedItems" ) then 
            self.Target:SendMessage("ClaimReservedItems", self.ParentObj)
            EndMobileEffect(root)
        elseif(buttonId == "" or buttonId == nil) then EndMobileEffect(root) end

    end,

    ChangeTab = function( tabID, self )
        if( tabID ~= self._SelectedTab ) then
            self._SelectedTab = tabID
            self.ShowLedger(self)
        end
    end,

    ChangePage = function( forward, self )
        if( forward == true ) then
            self._CurrentPage = self._CurrentPage + 1
        else
            self._CurrentPage = self._CurrentPage - 1
        end
        self.ShowLedger(self)
    end,

    BuyItem = function( itemID, self, root )
        local ItemsForSale = self.Target:GetObjVar("merchantSaleItem") or {}
        itemID = tonumber(itemID)

        --DebugMessage( "ItemID: " .. itemID )

        if not( ItemsForSale[itemID] ) then return false end

        self.Target:SendMessage("SellItem", self.ParentObj, ItemsForSale[itemID])
        EndMobileEffect(root)
        return true
    end,

    SortResults = function( sortOrder, self )
        
        -- Do we need to reverse the sorting order?
        if( self._SortBy == sortOrder ) then
            self._SortDirection = not self._SortDirection
        end

        --DebugMessage( "SelfSort: " .. self._SortBy .. " and SortDirection: " .. tostring(self._SortDirection) )
        self._SortBy = sortOrder
        self.ShowLedger(self)
    end,
    
    _Args = {},
    _WindowID = nil,
    isPlayer = false,
    _SelectedTab = "For Sale",
    _SortBy = "Name",
    _SortDirection = true,
    _IsOwner = false,
    _CurrentPage = 1

}
