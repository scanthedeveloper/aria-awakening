MobileEffectLibrary.Chaos = 
{
    PersistDeath = true,
    OnEnterState = function(self,root,target,args)
        if( not IsPlayerCharacter(self.ParentObj) ) then
            EndMobileEffect(root)
            return false 
        end

        self.ParentObj:SetObjVar("NameColorOverride", COLORS.Orange)
        self.ParentObj:SendMessage("UpdateName")
        
        self.ParentObj:SystemMessage("You have entered a chaos zone!", "info")
        self.ParentObj:SystemMessage("You have entered a chaos zone! All criminal actions performed by you or others are not punishable in this area.")
        self.ParentObj:PlayObjectSound("event:/ui/gong", false, 0, false, false)
    
        AddBuffIcon(self.ParentObj, "ChaosZone", "Chaotic", "crosscut", "You are in a Chaos Zone and will not be punished for criminal actions.", true)

        RegisterEventHandler(EventType.Message, "BreakInvisEffect", function(what, whattype) 
            if( what == "Action" and whattype == "Logout" ) then
                EndMobileEffect(root)
            end
        end)
    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "BreakInvisEffect")

        ClearConflictTable(self.ParentObj)
        ForeachActivePet(self.ParentObj, function(pet)
            ClearConflictTable(pet)
        end, true)

        self.ParentObj:SystemMessage("You have left a chaos zone!", "info")
        self.ParentObj:SystemMessage("You have left a chaos zone! All criminal actions performed by you or others are now punishable.")
        self.ParentObj:DelObjVar("NameColorOverride")
        self.ParentObj:SendMessage("UpdateName")
        RemoveBuffIcon(self.ParentObj, "ChaosZone")
    end,

}
