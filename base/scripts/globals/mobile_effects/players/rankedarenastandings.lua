MobileEffectLibrary.RankedArenaStandings = 
{
    OnEnterState = function(self,root,target,args)
        self._WindowID = "ArenaRankings" --..uuid()
        self._ArenaMaster = target
        self._Rankings = target:GetObjVar("PlayerRatings") or {}
        self._MatchLog = target:GetObjVar("ArenaMatchLog") or {}
        self._MatchType = "1v1"


        self.OpenWindow(self, "RankedSorted1v1")
        RegisterEventHandler(EventType.DynamicWindowResponse, self._WindowID, function ( user, buttonId, fieldData ) self.WindowResponse( user, buttonId, self, root ) end)

    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("",EventType.DynamicWindowResponse, self._WindowID)
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        self.ParentObj:CloseDynamicWindow(self._WindowID)
    end,

    OpenWindow = function(self, mode)
        -- Load top bar
        local dynWindow = DynamicWindow(self._WindowID, "Arena Rankings",1000,850,0,0,"Parchment")
        local y = 5
        local iconImg = "ArenaLeaderboardIcon_Color"
        local badgeImg = "ParchmentButton_Default"

        -- Divider
        dynWindow:AddImage(5,5,"Prestige_Divider",965,0,"Sliced")

        -- Rankings
        -- ( rank >= 1000 ) [ y = y - 45 ]
        -- ( rank < 1000 ) [ y = y - 30 ]

        -- Divider
        dynWindow:AddImage(5,185,"Prestige_Divider",965,0,"Sliced")

        -- Content Background
        dynWindow:AddImage(5,220,"TextFieldChatUnsliced",960,520,"Sliced")
        
        -- SHOW FILTERS
        if( mode == "RankedSorted1v1" ) then
            self._MatchType = "1v1"
            local rankings = RankedArena.GetSortedRanks( self._ArenaMaster, self._Rankings, self._MatchType )
            local scrollWindow = ScrollWindow(5,225,945,510,24)
            dynWindow:AddLabel(11,195,"[000000]#[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(47,195,"[000000]Player Name[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(362,195,"[000000]ELO Ratings[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")

            -- Light the fires!
            if( #rankings > 0 ) then
                for i=1, #rankings do 
                    local scrollElement = ScrollElement()
                    scrollElement:AddLabel(10,6,tostring(i),300,20,24,"left")
                    scrollElement:AddLabel(45,6,tostring(rankings[i].PlayerName),300,20,24,"left")
                    scrollElement:AddLabel(360,6,tostring(rankings[i].ELOScore),120,20,24,"left")
                    scrollWindow:Add(scrollElement)
                end
            else
                dynWindow:AddLabel(400,440,"No Rankings Found",0,0,32,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            end

            dynWindow:AddScrollWindow(scrollWindow)

        elseif( mode == "RankedSorted2v2" ) then
            self._MatchType = "2v2"
            local rankings = RankedArena.GetSortedRanks( self._ArenaMaster, self._Rankings, self._MatchType )
            local scrollWindow = ScrollWindow(5,225,945,510,24)
            dynWindow:AddLabel(11,195,"[000000]#[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(47,195,"[000000]Player Name[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(362,195,"[000000]ELO Ratings[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")

            -- Light the fires!
            if( #rankings > 0 ) then
                for i=1, #rankings do 
                    local scrollElement = ScrollElement()
                    scrollElement:AddLabel(10,6,tostring(i),300,20,24,"left")
                    scrollElement:AddLabel(45,6,tostring(rankings[i].PlayerName),300,20,24,"left")
                    scrollElement:AddLabel(360,6,tostring(rankings[i].ELOScore),120,20,24,"left")
                    scrollWindow:Add(scrollElement)
                end
            else
                dynWindow:AddLabel(400,440,"No Rankings Found",0,0,32,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            end

            dynWindow:AddScrollWindow(scrollWindow)

        elseif( mode == "RankedSorted3v3" ) then
            self._MatchType = "3v3"
            local rankings = RankedArena.GetSortedRanks( self._ArenaMaster, self._Rankings, self._MatchType )
            local scrollWindow = ScrollWindow(5,225,945,510,24)
            dynWindow:AddLabel(11,195,"[000000]#[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(47,195,"[000000]Player Name[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(362,195,"[000000]ELO Ratings[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")

            -- Light the fires!
            if( #rankings > 0 ) then
                for i=1, #rankings do 
                    local scrollElement = ScrollElement()
                    scrollElement:AddLabel(10,6,tostring(i),300,20,24,"left")
                    scrollElement:AddLabel(45,6,tostring(rankings[i].PlayerName),300,20,24,"left")
                    scrollElement:AddLabel(360,6,tostring(rankings[i].ELOScore),120,20,24,"left")
                    scrollWindow:Add(scrollElement)
                end
            else
                dynWindow:AddLabel(400,440,"No Rankings Found",0,0,32,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            end

            dynWindow:AddScrollWindow(scrollWindow)

        elseif( mode == "MatchHistory" ) then
            local matches = RankedArena.GetMatchHistory( self._ArenaMaster, self._MatchLog, self._Rankings, self.ParentObj, self._MatchType )
            local winCount = 0
            local lossCount = 0

            local scrollWindow = ScrollWindow(5,225,945,510,24)
            dynWindow:AddLabel(11,195,"[000000]#[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(50,195,"[000000]Allies[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(355,195,"[000000]Enemies[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(620,195,"[000000]Ranked[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(735,195,"[000000]Outcome[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            dynWindow:AddLabel(860,195,"[000000]Date[-]",0,0,28,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            
            if( #matches > 0 ) then
                for i=1, #matches do 
                    local match = matches[i]
                    local scrollElement = ScrollElement()
                    local ranked = match.Ranked and "Yes" or "No"
                    local outcome = match.Won and "[32CD32]Win[-]" or "[FFB6C1]Loss[-]"

                    if( match.Won ) then
                        winCount = winCount + 1
                    else
                        lossCount = lossCount + 1
                    end

                    scrollElement:AddLabel(10,6,tostring(i),300,20,24,"left")
                    scrollElement:AddLabel(45,6,tostring(table.concat( match.Allies, ", ")),300,20,24,"left")
                    scrollElement:AddLabel(350,6,tostring(table.concat( match.Enemies, ", ")),300,20,24,"left")
                    scrollElement:AddLabel(640,6,tostring(ranked),300,20,24,"left")
                    scrollElement:AddLabel(760,6,tostring(outcome),300,20,24,"left")
                    scrollElement:AddLabel(840,6,tostring(match.Date),300,20,24,"left")
                    scrollWindow:Add(scrollElement)
                end
            else
                dynWindow:AddLabel(400,440,"No Matches Found",0,0,32,"", false, false, "PermianSlabSerif_Dynamic_Bold")
            end

            local scrollElement = ScrollElement()
            scrollElement:AddLabel(610,6,tostring("Wins/Losses"),300,20,24,"left")
            scrollElement:AddLabel(750,6,"[32CD32]"..tostring(winCount).."[-] / " .. "[FFB6C1]"..tostring(lossCount).."[-]",300,20,24,"left")
            scrollWindow:Add(scrollElement)

            dynWindow:AddScrollWindow(scrollWindow)

        end

        local rank = RankedArena.GetArenaRatingForPlayer( self.ParentObj, self._ArenaMaster, "1v1" )
        if( rank < 1000 ) then y = 160 - 30 else y = 160 - 45 end
        iconImg = self._MatchType == "1v1" and "ArenaLeaderboardIcon_Color" or "ArenaLeaderboardIcon_Grayscale" 
        badgeImg = self._MatchType == "1v1" and "ParchmentButton_Pressed" or "ParchmentButton_Default" 
        dynWindow:AddImage(95,5,iconImg,160,160,"Sliced")
        dynWindow:AddLabel(y,62,"[000000]"..tostring(rank).."[-]",0,0,72,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddImage(90,155,badgeImg)
        dynWindow:AddLabel(160,160,"[000000]1v1[-]",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddButton(95,65,"ViewRanking|1v1","",160,115,"View 1v1 rankings.","",false,"Invisible")

        rank = RankedArena.GetArenaRatingForPlayer( self.ParentObj, self._ArenaMaster, "2v2" )
        if( rank < 1000 ) then y = 480 - 30 else y = 480 - 45 end
        iconImg = self._MatchType == "2v2" and "ArenaLeaderboardIcon_Color" or "ArenaLeaderboardIcon_Grayscale" 
        badgeImg = self._MatchType == "2v2" and "ParchmentButton_Pressed" or "ParchmentButton_Default" 
        dynWindow:AddImage(410,5,iconImg,160,160,"Sliced")
        dynWindow:AddLabel(y,62,"[000000]"..tostring(rank).."[-]",0,0,72,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddImage(405,155,badgeImg)
        dynWindow:AddLabel(475,160,"[000000]2v2[-]",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddButton(410,65,"ViewRanking|2v2","",160,115,"View 2v2 rankings.","",false,"Invisible")

        rank = RankedArena.GetArenaRatingForPlayer( self.ParentObj, self._ArenaMaster, "3v3" )
        if( rank < 1000 ) then y = 800 - 30 else y = 800 - 45 end
        iconImg = self._MatchType == "3v3" and "ArenaLeaderboardIcon_Color" or "ArenaLeaderboardIcon_Grayscale" 
        badgeImg = self._MatchType == "3v3" and "ParchmentButton_Pressed" or "ParchmentButton_Default" 
        dynWindow:AddImage(730,5,iconImg,160,160,"Sliced")
        dynWindow:AddLabel(y,62,"[000000]"..tostring(rank).."[-]",0,0,72,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddImage(725,155,badgeImg)
        dynWindow:AddLabel(795,160,"[000000]3v3[-]",0,0,24,"", false, false, "PermianSlabSerif_Dynamic_Bold")
        dynWindow:AddButton(730,65,"ViewRanking|3v3","",160,115,"View 3v3 rankings.","",false,"Invisible")

        -- Match History BTN
        dynWindow:AddButton(730,750,"ViewMatchHistory","Match History",200,48,"View my matches for this bracket.","",false,"GoldRedButton")


        self.ParentObj:OpenDynamicWindow(dynWindow, self.ParentObj)
        self._FieldData = nil

        
    end,

    WindowResponse = function( user, buttonId, self, root )

        local result = StringSplit(buttonId,"|")
        local action = result[1]
        local arg = result[2]

        --DebugMessage("Debug: {Action : " .. tostring(action) .. ", Arg: "..tostring(arg).."}")

        -- Show filter window
        if( action == "ViewRanking" ) then 
            if( arg == "1v1" ) then
                self.OpenWindow(self, "RankedSorted1v1")
            elseif( arg == "2v2" ) then
                self.OpenWindow(self, "RankedSorted2v2")
            elseif( arg == "3v3" ) then
                self.OpenWindow(self, "RankedSorted3v3")
            end
        elseif( action == "ViewMatchHistory" ) then
            self.OpenWindow(self, "MatchHistory")
        else
            --DebugMessage("Closing Window!")
            EndMobileEffect(root)
        end
    end,
}
