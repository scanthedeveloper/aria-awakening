MobileEffectLibrary.MagicAmplification = 
{
    OnEnterState = function(self,root,target,args)
        local currentTarget = self.ParentObj:GetObjVar("CurrentTarget")

        if( currentTarget ) then
            currentTarget:SendMessage(
                "StartMobileEffect", 
                "ApplyMobileMod", 
                self.ParentObj, 
                {
                    MobileMod = "MagicDamageTakenTimes",
                    MobileModAmount = "0.1",
                    DurationInSeconds = 8,
                    BuffIcon = "Ball Lightning 01",
                    BuffDescription = "Increases magic damaged received for 8 seconds.",
                    TargetEffect = "PurplePortalEffect",
                }
            )

        end

        EndMobileEffect(root)
    end,
    
    _Args = {}
}
