MobileEffectLibrary.Clearcast = 
{
	OnEnterState = function(self,root,target,args)
        self._Args = args

        self.isPlayer = IsPlayerCharacter(self.ParentObj)

		-- If this is a player then lets show their buff and progress bar
		if ( self.isPlayer ) then
			AddBuffIcon(
                self.ParentObj, 
                "ClearcastBuff", 
                "Clearcast", 
                "Unholy Mastery", 
                "Reduces cost of next spell by 100%.",
                 false
            )

            self.ParentObj:SystemMessage("Gained clearcasting!", "info")
        end

        RegisterSingleEventHandler(EventType.Message, "ClearcastUsed",
        function()
            EndMobileEffect(root)
        end)
    end,
    
    OnStack = function(self,root,target,args)
        self._CleanUp(self, root)
        self.OnEnterState(self,root,target,args)
    end,


    OnExitState = function(self,root)
        self._CleanUp(self, root)
	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(self._Args.Duration or 8)
	end,

	AiPulse = function(self,root) 
        EndMobileEffect(root)
    end,

    _CleanUp = function(self, root)    
        RemoveBuffIcon(self.ParentObj, "ClearcastBuff")
    end,
    
    _Args = {}
}
