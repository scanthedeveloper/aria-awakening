-- This mobile effect is triggered by combat.lua (CheckOnDamageSpellHitProcs)

MobileEffectLibrary.OnDamageSpellHit = 
{
    OnEnterState = function(self,root,target,args)
        --DebugMessage("OnDamageSpellHit", args.HitTag)

        -- If we don't have our target or our any args we need to exit!
        if( not target or not args ) then EndMobileEffect(root) return false end

        -- Set our variables
        self.Target = target
        self.Args = args
        self.Message = nil

        -- If we don't know what type of hit we are preforming then exit
        if( not self.Args.HitTag ) then EndMobileEffect(root) return false end


        if( self.Args.HitTag == "TriggerSummonConstruct" ) then
            self.Message = "Your spell summons a Construct."
            local args = { ForceApply = true }
            self.ParentObj:SendMessage("StartMobileEffect", "SummonConstruct", nil, args)
        end

        if( self.Args.HitTag == "TriggerIgniteTarget" ) then
            self.Message = "Your spell ignites the target."
            self.Target:PlayEffectWithArgs("FirePillarEffect", 0.0,"Bone=Ground")
            local args = { ForceApply = true }
            self.Target:SendMessage("ProcessTypeDamage", self.ParentObj, 3, "MAGIC", false, false)
            CallFunctionDelayed(TimeSpan.FromMilliseconds(1000), function() 
                self.Target:SendMessage("ProcessTypeDamage", self.ParentObj, 5, "MAGIC", false, false)
            end)
            CallFunctionDelayed(TimeSpan.FromMilliseconds(2000), function() 
                self.Target:SendMessage("ProcessTypeDamage", self.ParentObj, 7, "MAGIC", false, false)
                self.Target:StopEffect("FirePillarEffect")
            end)
        end

        if( self.Args.HitTag == "TriggerEchoHeal" ) then
            self.Message = "Your spell regenerates you."
            local args = { ForceApply = true }
            self.ParentObj:SendMessage("StartMobileEffect", "ConsumeWisp", nil, args)
        end

        if( self.Message ) then
            self.ParentObj:SystemMessage( "[FFA500]" .. self.Message .. "[-]" )
        end

        EndMobileEffect(root)
    end,
}