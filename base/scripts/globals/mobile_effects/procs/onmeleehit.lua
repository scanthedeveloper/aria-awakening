-- This mobile effect is called by combat.lua (TryExecuteOnMeleeHit)

MobileEffectLibrary.OnMeleeHit = 
{
    OnEnterState = function(self,root,target,args)
        -- If we don't have our target or our any args we need to exit!
        if( not target or not args ) then EndMobileEffect(root) return false end

        -- Set our variables
        self.Target = target
        self.Args = args
        self.Message = nil

        -- If we don't know what type of hit we are preforming then exit
        if( not self.Args.HitTag ) then EndMobileEffect(root) return false end


        if( self.Args.HitTag == "TriggerSummonWispChaser" ) then
            self.Message = "Your attack summons a Wisp Chaser."
            local args = { ForceApply = true }
            self.ParentObj:SendMessage("StartMobileEffect", "SummonFox", nil, args)
        end

        if( self.Args.HitTag == "TriggerAfflictTarget" ) then
            self.Message = "Your attack afflicts your target."
            self.Target:SendMessage("StartMobileEffect", "WispAffliction", self.ParentObj, args)
        end

        if( self.Args.HitTag == "TriggerChillTarget" ) then
            self.Message = "Your attack chills your target."
            self.Target:SendMessage("StartMobileEffect", "Chilled", self.ParentObj, args)
        end

        if( self.Args.HitTag == "TriggerEchoHeal" ) then
            self.Message = "Your attack regenerates you."
            local args = { ForceApply = true }
            self.ParentObj:SendMessage("StartMobileEffect", "ConsumeWisp", nil, args)
        end

        if( self.Args.HitTag == "TriggerGainWisp" ) then
            self.Message = "Your attack summons a wisp."
            SorceryHelper.SorceryMessageSender( self.ParentObj, "SummonWisp", { Count = 1 } )
        end

        if( self.Message ) then
            self.ParentObj:SystemMessage( "[FFA500]" .. self.Message .. "[-]" )
        end

        EndMobileEffect(root)
    end,
}