MobileEffectLibrary.WeaponProc = 
{

    OnEnterState = function(self,root,target,args)
        self._Args = {}
        self._Args.Damage = args.Damage
        self._Args.DamageType = args.DamageType or "TrueDamage"
        self._Args.AOERange = args.AOERange or false
        self._Args.ProcEffect = args.ProcEffect or "FireballExplosionEffect"
        self._Args.ProcEffectDelay = args.ProcEffectDelay or "FireballExplosionEffect"
        self._Args.ProcSound  = args.ProcSound or "event:/magic/fire/magic_fire_fireball_impact"
        self._Args.ProcDelay = args.ProcDelay or nil --TimeSpan.FromSeconds()
        self._Args.Location = self.ParentObj:GetLoc() or nil
        self._Args.EffectType = args.EffectType or "target"
        self._Target = target or nil
        self._SkipPulseEndEffect = false

        -- If we don't have a target we need to stop
        if (target == nil) then
            EndMobileEffect(root)
            return false
        end

        -- Play effects
        self.PlayInitialEffects(self)
        
        -- Should the proc be delayed, ie waiting for meteor to land?
        if( self._Args.ProcDelay ) then
            self._SkipPulseEndEffect = true
            local delayedEvent = "WeaponProc_"..uuid()

            RegisterSingleEventHandler(EventType.Timer, delayedEvent,
            function()
                self.PlayDelayedEffects(self)
                self.ProcessDamage(self, root)
                EndMobileEffect(root)
            end)

            self.ParentObj:ScheduleTimerDelay(self._Args.ProcDelay,delayedEvent)

        else
            self.ProcessDamage(self, root)
        end

    end,

    PlayInitialEffects = function(self)
        
        if( self._Args.EffectType == "target" ) then
            self.ParentObj:PlayEffect(self._Args.ProcEffect)
        else
            PlayEffectAtLoc(self._Args.ProcEffect, self._Args.Location, 5)
        end

        self.ParentObj:PlayObjectSound(self._Args.ProcSound,false)
    end,

    PlayDelayedEffects = function(self)
        PlayEffectAtLoc(self._Args.ProcEffectDelay, self._Args.Location, 5)
    end,

    ProcessDamage = function(self,root)
        if not( self._Args.AOERange ) then
            if( ValidCombatTarget(self._Target, self.ParentObj, true) ) then
                
                local damage = math.floor(self._Args.Damage *  math.max(self.ParentObj:GetStatValue("AttackSpeed"), ServerSettings.Combat.MinimumSwingSpeed) )
                self.ParentObj:SendMessage("ProcessTypeDamage", self._Target, damage, self._Args.DamageType, false, false)
            end
        else

            local mobiles = FindObjects(SearchMulti({
                SearchRange(self._Args.Location,self._Args.AOERange),
                SearchMobile()}), GameObj(0))
            for i=1,#mobiles do
                if ( mobiles[i]:HasLineOfSightToLoc(self._Args.Location,ServerSettings.Combat.LOSEyeLevel) and ValidCombatTarget(self._Target, mobiles[i], true) ) then
                    mobiles[i]:SendMessage("ProcessTypeDamage", self._Target, self._Args.Damage, self._Args.DamageType, false, false)
                end
            end
            
        end
    end,
    
    GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

    AiPulse = function(self,root)
        self.ParentObj:StopEffect(self._Args.ProcEffect)
        if not( self._SkipPulseEndEffect ) then
            EndMobileEffect(root)
        end
    end,

    PulseFrequency = TimeSpan.FromMilliseconds(600),
}