-- This mobile effect is triggered from the base_mobile.lua script.

MobileEffectLibrary.OnMeleeStruck = 
{
    OnEnterState = function(self,root,target,args)
        -- If we don't have our target or our any args we need to exit!
        if( not target or not args ) then EndMobileEffect(root) return false end

        -- Set our variables
        self.Target = target
        self.Args = args
        self.Message = nil

        -- If we don't know what type of hit we are preforming then exit
        if( not self.Args.StruckTag ) then EndMobileEffect(root) return false end


        if( self.Args.StruckTag == "TriggerEchofall" ) then
            self.Message = "Enemies attack triggered Echofall."
            local args = { QueueLocation = self.ParentObj:GetLoc(), ParentObj = self.ParentObj, }
            SorceryHelper.TriggerEchofall(args)
        end

        if( self.Args.StruckTag == "TriggerEchoHeal" ) then
            self.Message = "Enemies attack triggered regeneration."
            local args = { ForceApply = true }
            self.ParentObj:SendMessage("StartMobileEffect", "ConsumeWisp", nil, args)
        end

        if( self.Args.StruckTag == "TriggerGainWisp" ) then
            self.Message = "Enemies attack triggered wisp summon."
            SorceryHelper.SorceryMessageSender( self.ParentObj, "SummonWisp", { Count = 1 } )
        end

        if( self.Message ) then
            self.ParentObj:SystemMessage( "[ADFF2F]" .. self.Message .. "[-]" )
        end

        EndMobileEffect(root)
    end,
}