MobileEffectLibrary.UseSpookyCauldron = 
{
    EndOnMovement = true,
    EndOnAction = true, 
    ForceDismount = true,
    
    OnEnterState = function(self,root,target,args)
        DebugMessage("UseSpookyCauldron")
        self._CauldronObj = target
        self.ParentObj:SendMessage("EndCombatMessage")

        -- If this isn't a player or we don't have a sapling targeted we need to leave
        if( not IsPlayerCharacter(self.ParentObj) or not self._CauldronObj ) then
            EndMobileEffect(root)
            return false 
        end

        self._CauldronObjLoc = self._CauldronObj:GetLoc()
        self._SpawnLoc = GetRandomPassableLocationInRadius(self._CauldronObjLoc,5,true)

        -- Play our animations
        self.ParentObj:PlayAnimation("fabrication")
        

        CallFunctionDelayed(TimeSpan.FromSeconds(2.0),function ( ... )
            if( not self._Ended ) then
                self._Complete = true
                EndMobileEffect(root)
            end
        end)
        
    end,

    OnExitState = function(self,root)
        self._Ended = true

        if( self._Complete ) then
            -- Used cauldron event
            Quests.SendQuestEventMessage( self.ParentObj, "Seasonal", { "UseSpookyCauldron" }, 1 )
            
            -- Do animation and ground effect
            self.ParentObj:PlayAnimation("idle")

            -- Destroy cauldron object
            self._CauldronObj:Destroy()
            PlayEffectAtLoc("RedCoreImpactWaveEffect",self._CauldronObjLoc,0.5)

            local rngGo = math.random( 1, 100 )

            -- Determine what should happend
            if( rngGo <= 10 ) then
                Create.AtLoc( "coveness", self._SpawnLoc)
                self._Message = "A conveness appears."
            elseif( rngGo <= 70 ) then
                Create.AtLoc( "covener", self._SpawnLoc)
                self._Message = "A conveness appears."
            else
                Quests.SendQuestEventMessage( self.ParentObj, "Seasonal", { "GotPotionFromCauldron" }, 1 )
                local potionRNG = math.random( 1, 100 )
                self._Message = "You recieve a potion."

                if( potionRNG <= 50 ) then
                    Create.InBackpack("potion_unstable_concoction", self.ParentObj)
                else
                    Create.InBackpack("potion_coven_treats", self.ParentObj)
                end
            end

            if( self._Message ) then
                self.ParentObj:SystemMessage(self._Message,"info")
            end
        end
    end,

    _CauldronObj = nil,
    _CauldronObjLoc = nil,
    _Message = nil,
    _Complete = false,
    _Ended = false,


}
