MobileEffectLibrary.BurnEntSapling = 
{
    EndOnMovement = true,
    EndOnAction = true, 
    ForceDismount = true,
    
    OnEnterState = function(self,root,target,args)
        DebugMessage("BurnEntSapling")
        self._SaplingObj = target
        self.ParentObj:SendMessage("EndCombatMessage")
        
        self._HasTorch = ( self.ParentObj:GetEquippedObject("Torch") ) or false

        -- If this isn't a player or we don't have a sapling targeted we need to leave
        if( not IsPlayerCharacter(self.ParentObj) or not self._SaplingObj ) then
            EndMobileEffect(root)
            return false 
        end

        self._SaplingObjLoc = self._SaplingObj:GetLoc()
        self._SpawnLoc = GetRandomPassableLocationInRadius(self._SaplingObjLoc,5,true)

        -- You need a torch to do this!
        if( not self._HasTorch ) then
            self._Message = "You need a torch equipped to burn that."
            EndMobileEffect(root)
            return false 
        end

        -- Play our animations
        self.ParentObj:PlayAnimation("carve")
        

        CallFunctionDelayed(TimeSpan.FromSeconds(2.0),function ( ... )
            if( not self._Ended ) then
                self._Complete = true
                EndMobileEffect(root)
            end
        end)
        
    end,

    OnExitState = function(self,root)
        self._Ended = true
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message,"info")
        end

        if( self._Complete ) then
            -- Do animation and ground effect
            self.ParentObj:PlayAnimation("idle")
            PlayEffectAtLoc("FirePillarEffect",self._SaplingObjLoc,4)

            -- Destroy sapling object
            self._SaplingObj:Destroy()

            -- Spawn the entling
            PlayEffectAtLoc("DigDirtEffect",self._SpawnLoc,3)
            CallFunctionDelayed(TimeSpan.FromSeconds(1.0),function ( ... )
                Create.AtLoc( "entling", self._SpawnLoc,function( summonObj )
                
                end)
                -- Fire off quest event trigger
                Quests.SendQuestEventMessage( self.ParentObj, "Seasonal", { "BurnEntSapling" }, 1 )


            end)
            
        end
    end,

    _SaplingObj = nil,
    _SaplingObjLoc = nil,
    _Message = nil,
    _Complete = false,
    _Ended = false,


}
