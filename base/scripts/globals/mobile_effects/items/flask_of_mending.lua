MobileEffectLibrary.FlaskOfMending = 
{

    OnEnterState = function(self,root,target,args)
        local charges = target:GetObjVar("Charges")
        if ( charges == nil or charges < 1 ) then
            self.ParentObj:SystemMessage("Not even a drop left!", "info")
            EndMobileEffect(root)
            return false
        end
        if ( self.ParentObj:HasTimer("RecentPotion") ) then
			self.ParentObj:SystemMessage("Cannot use again yet.", "info")
			EndMobileEffect(root)
			return false
		end
		if not( HasAnyMobileEffect(self.ParentObj, {"MortalStruck", "Bleed"}) ) then
			self.ParentObj:SystemMessage("You are not wounded.", "info")
			EndMobileEffect(root)
			return false
		end
        --SCAN REDUCED FROM 1 MINUTE TO 15 SECONDS
        self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(15), "RecentPotion")

        self.ParentObj:PlayObjectSound("event:/magic/misc/magic_water_restoration")
		self.ParentObj:PlayEffect("HealEffect")
		self.ParentObj:SendMessage("EndMortalStruckEffect")
		self.ParentObj:SendMessage("EndBleedEffect")
        self.ParentObj:SystemMessage("Your wounds have been mended.", "info")
        
        target:SetObjVar("Charges", charges - 1)
        SetItemTooltip(target)

        local recharges = target:GetObjVar("Recharges")
        if ( recharges and recharges < 1 and charges < 2 ) then
            target:Destroy()
            self.ParentObj:SystemMessage("The flask shatters in your hand!", "event")
        end

		EndMobileEffect(root)
	end,

}

MobileEffectLibrary.RefillFlaskOfMending = 
{

	OnEnterState = function(self,root,target,args)
    
    local recharges = target:GetObjVar("Recharges")
    if ( recharges < 1 ) then
        self.ParentObj:SystemMessage("The flask seems to have lost its magic.", "info")
        EndMobileEffect(root)
        return false
    end
    local charges = target:GetObjVar("Charges")
    if ( charges > 28 ) then
        self.ParentObj:SystemMessage("Flask won't hold another potion!", "info")
        EndMobileEffect(root)
        return false
    end
    local backpack = self.ParentObj:GetEquippedObject("Backpack")
    if ( backpack ) then
        if ( ConsumeResourceContainer(backpack, "PotionMend", 1) ) then
            target:SetObjVar("Charges", math.min(charges + 2, 30))
            target:SetObjVar("Recharges", recharges - 1)
            SetItemTooltip(target)
            self.ParentObj:PlayObjectSound("event:/magic/water/magic_water_cast_water",false,0.7)
            self.ParentObj:SystemMessage("Added one mending potion to the flask.", "info")
        else
            self.ParentObj:SystemMessage("Out of mending potions.", "info")
            EndMobileEffect(root)
            return false
        end
    end

		EndMobileEffect(root)
	end,
}