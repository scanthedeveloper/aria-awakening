MobileEffectLibrary.ApplyExecutionerScroll = 
{

	OnEnterState = function(self,root,target,args)

		if ( not target or not target:IsValid() or not target:HasObjVar("WeaponType") or target:HasObjVar("Artifact") ) then
			self.ParentObj:SystemMessage("Cannot apply executioner to that!", "info")
			EndMobileEffect(root)
			return false
		end

		if ( target:TopmostContainer() ~= self.ParentObj ) then
			self.ParentObj:SystemMessage("Cannot reach that.", "info")
			EndMobileEffect(root)
			return false
		end

		if ( target:HasObjVar("Executioner") ) then
            self.ParentObj:SystemMessage("That item is already an executioner!", "info")
			EndMobileEffect(root)
			return false
		end

        target:SetObjVar("Executioner", args.ExecutionerType)
        target:SetObjVar("ExecutionerLevel", args.ExecutionerPowerRange[math.random(1,#args.ExecutionerPowerRange)])
		SetItemTooltip(target)

		self.ParentObj:SystemMessage("You have applied executioner effect to "..StripColorFromString(target:GetName()), "info")

		EndMobileEffect(root)
	end,

}