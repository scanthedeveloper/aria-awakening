MobileEffectLibrary.LandDeed = 
{
    QTarget = true,
    
    -- Args 
    -- PlotX, PlotZ size of plot to place
    -- Source identifier for LandDeedPlaced message (used in packed plots)
    OnEnterState = function(self,root,target,args)
        
        -- Free accounts cannot use land deeds
        if( IsFreeAccount(self.ParentObj) ) then
            self.ParentObj:SystemMessage("Free accounts cannot use land deeds.", "info")
            EndMobileEffect(root)
            return
        end

        --[[
        -- DAB TEMPORARY: DISABLE LAND CLAIM IN TUNDRA
        if( ServerSettings.SubregionName == "FrozenTundra" ) then
            local enableTundra = nil
            if( ServerSettings.ClusterId == ServerSettings.Clusters["Crimson Sea"].Id ) then
                enableTundra = DateTime.SpecifyKind(DateTime(2020, 8, 01, 19, 0, 0), DateTimeKind.Utc)
            elseif( ServerSettings.ClusterId == ServerSettings.Clusters.Internal.Id ) then
                enableTundra = DateTime.SpecifyKind(DateTime(2020, 7, 29, 19, 0, 0), DateTimeKind.Utc)
            else
                enableTundra = DateTime.SpecifyKind(DateTime(2020, 8, 02, 00, 0, 0), DateTimeKind.Utc)
            end
            if( enableTundra == nil or DateTime.Compare( DateTime.UtcNow, enableTundra ) < 1  ) then
                self.ParentObj:SystemMessage("You can not yet claim land in the Frozen Tundra.", "info")
                EndMobileEffect(root)
                return
            end
        end
        ]]
        
        if ( self.ParentObj:HasTimer("LandDeedTimer") ) then
            self.ParentObj:SystemMessage("Please wait to use this again.", "info")
            EndMobileEffect(root)
            return
        end
        self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(2.5), "LandDeedTimer")

        local plotSize = Loc(args.PlotX or ServerSettings.Plot.MinimumSize,0,args.PlotZ or ServerSettings.Plot.MinimumSize)

        RegisterSingleEventHandler(EventType.ClientTargetLocResponse, "ClaimLand", function(success, loc, gameobj)
            if ( not success or not loc or not target or not target:IsValid() ) then
                self.ErrorInvalidLocation(self,root)
                return
            end
            if ( self.ParentObj:GetLoc():Distance(loc) > 15 ) then
                self.ParentObj:SystemMessage("Too far away.", "info")
                EndMobileEffect(root)
                return
            end
            if not( self.ParentObj:HasLineOfSightToLoc(loc) ) then
                self.ParentObj:SystemMessage("Cannot see that.", "info")
                EndMobileEffect(root)
                return
            end
            Plot.New(self.ParentObj, loc, plotSize, function(controller)
                if ( controller ) then
                    local freeResize = target:GetObjVar("FreeResize")
                    if(freeResize) then
                        controller:SetObjVar("FreeResize",freeResize)
                    end

                    target:Destroy()
                    -- achievement for creating a plot
                    CheckAchievementStatus(self.ParentObj, "Activity", "LandOwner", 1)

                    Plot.ShowControlWindow(self.ParentObj,controller,"Manage")                    
                end
                EndMobileEffect(root)
            end)
        end)

        local clientPreviewSize = 15

        self.ParentObj:RequestClientTargetLocPreview(self.ParentObj, "ClaimLand","plot_preview",Loc(0,0,0),Loc(plotSize.X/clientPreviewSize,1,plotSize.Z/clientPreviewSize))
	end,

    OnExitState = function(self,root)
        
    end,
    
    ErrorInvalidLocation = function(self,root)
        self.ParentObj:SystemMessage("That is not a valid location.", "info")
        EndMobileEffect(root)
    end,

}