MobileEffectLibrary.MovingLandDeed = 
{
    QTarget = true,

    UpdatePackedPlots = function(self,root,args)
        SetGlobalVar("PackedPlots."..args.UserId,function (record)
            local plotIndex = -1
            for i,plotInfo in pairs(record) do
                if(plotInfo.StorageKey == args.StorageKey) then
                    plotIndex = i
                    break
                end
            end
            if(plotIndex ~= -1) then
                table.remove(record,plotIndex)
                return true
            else
                return false
            end
        end,
        function (success)
            if not(success) then
                DebugMessage("ERROR: Failed to removed packed plot from global var "..args.StorageKey)
            end

            self.ParentObj:SendMessage("RefreshStoredItemManager")

            EndMobileEffect(root)
        end)
    end,
    
    -- Args 
    -- PlotX, PlotZ, FreeResize, StorageKey, MovingType
    OnEnterState = function(self,root,target,args)
        if ( self.ParentObj:HasTimer("LandDeedTimer") ) then
            self.ParentObj:SystemMessage("Please wait to use this again.", "info")
            EndMobileEffect(root)
            return
        end
        self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(2.5), "LandDeedTimer")

        local plotSize = Loc(args.PlotX or ServerSettings.Plot.MinimumSize,0,args.PlotZ or ServerSettings.Plot.MinimumSize)

        if(args.StorageKey) then
            RegisterSingleEventHandler(EventType.RequestItemFromStorageResult,"MovingCrate",
                function (objHeader,objRef)
                    if(objRef) then
                        if(args.MovingType == "Manual") then
                            objRef:SetObjVar("StorageKey",args.StorageKey)
                            objRef:SetObjVar("DecayDate",DateTime.UtcNow:Add(TimeSpan.FromDays(7)))
                            objRef:SetObjVar("PlotController",self.PlotController)
                            objRef:SetObjVar("IsPlotObject",true)
                            objRef:SetObjVar("NoReset",true)
                            objRef:SetObjVar("SecureContainer",true)
                            objRef:SetObjVar("LockedDown",true)
                            objRef:SetObjVar("locked",true)
                            
                            objRef:DelModule("container")
                            objRef:AddModule("moving_crate")                            
                        else
                            local plotMarkers = self.PlotController:GetObjVar("PlotMarkers") 

                            local plotMarkers = Plot.GetMarkers(self.PlotController)            
                            local offsetLocX, offsetLocZ
                            for i,marker in pairs(plotMarkers) do
                                local markerLoc = marker:GetLoc()
                                if not(offsetLocX) or markerLoc.X < offsetLocX then
                                    offsetLocX = markerLoc.X
                                end

                                if not(offsetLocZ) or markerLoc.Z < offsetLocZ then
                                    offsetLocZ = markerLoc.Z
                                end
                            end

                            local allItems = objRef:GetContainedObjects()
                            for i,storedItem in pairs(allItems) do
                                local storedLoc = storedItem:GetLoc()
                                local newLoc = Loc(offsetLocX + storedLoc.X, storedLoc.Y, offsetLocZ + storedLoc.Z)

                                if(storedItem:HasObjVar("IsHouse")) then
                                    storedItem:SetObjVar("PlotController",self.PlotController)

                                    local houses = self.PlotController:GetObjVar("PlotHouses") or {}
                                    houses[#houses+1] = storedItem
                                    self.PlotController:SetObjVar("PlotHouses",houses)
                                elseif(storedItem:HasObjVar("PlotController")) then
                                    storedItem:SetObjVar("PlotController",self.PlotController)
                                    if(storedItem:HasModule("ai_hireling_merchant")) then
                                        storedItem:SetObjVar("ShopLocation",newLoc)
                                        storedItem:SetObjVar("SpawnPosition",newLoc)
                                        storedItem:SetObjVar("SpawnLocation",newLoc)
                                    end
                                end

                                if(storedItem:HasObjVar("RentRefund")) then
                                    self.ParentObj:SystemMessage("Rent refund has been placed in your backpack.","info")
                                    backpackObj = self.ParentObj:GetEquippedObject("Backpack")
                                    local randomLoc = GetRandomDropPosition(backpackObj)
                                    storedItem:MoveToContainer(backpackObj,randomLoc)
                                else
                                    storedItem:SetWorldPosition(newLoc)
                                end
                            end

                            -- fix plot/house lockdown/container counts
                            Plot.CalculateTrueLockCount(self.PlotController)
                            -- Fix plot item sale counts
                            Plot.CalculateItemSaleCount(self.PlotController)

                            objRef:Destroy()
                        end

                        self:UpdatePackedPlots(root,args)
                    else
                        DebugMessage("ERROR: Failed to load moving crate "..args.StorageKey)
                        self:UpdatePackedPlots(root,args)
                        EndMobileEffect(root)
                    end                
                end)            
        end

        RegisterSingleEventHandler(EventType.ClientTargetLocResponse, "ClaimLand", function(success, loc, gameobj)
            if ( not success or not loc or not target or not target:IsValid() ) then
                self.ErrorInvalidLocation(self,root)
                return
            end
            if ( self.ParentObj:GetLoc():Distance(loc) > 15 ) then
                self.ParentObj:SystemMessage("Too far away.", "info")
                EndMobileEffect(root)
                return
            end
            if not( self.ParentObj:HasLineOfSightToLoc(loc) ) then
                self.ParentObj:SystemMessage("Cannot see that.", "info")
                EndMobileEffect(root)
                return
            end
            local text = "\n[FF0000]THIS CANNOT BE UNDONE[-]"
            if ( args.MovingType == "Manual" ) then
                text = "Do you wish to claim a 12 x 12 plot freely resizable up to your old plot size? All of your items including blueprint, merchant and sale items can be reclaimed from a Moving Crate on the plot. You have one week to claim the items before the Moving Crate is destroyed." .. text
            else
                text = "Do you wish to restore your old plot, including orientation, exactly as it was?" .. text
            end
            -- Are you sure?
            ClientDialog.Show{
                TargetUser = self.ParentObj,
                DialogId = "ConfirmPlaceHouse",
                TitleStr = "Confirm Place House",
                DescStr = text,
                Button1Str = "Acknowledge",
                Button2Str = "Cancel",
                ResponseObj = self.ParentObj,
                ResponseFunc = function( user, buttonId )
                    buttonId = tonumber(buttonId)
                    if ( user == self.ParentObj and buttonId == 0 ) then
                        Plot.New(self.ParentObj, loc, plotSize, function(controller)
                            if ( controller ) then         
                                self.PlotController = controller           

                                -- achievement for creating a plot
                                CheckAchievementStatus(self.ParentObj, "Activity", "LandOwner", 1)

                                Plot.ShowControlWindow(self.ParentObj,controller,"Manage")

                                local freeResize = args.FreeResize
                                if(freeResize) then
                                    controller:SetObjVar("FreeResize",freeResize)
                                end

                                if(args.StorageKey) then
                                    local plotLoc = controller:GetLoc()
                                    local crateLoc = Loc(plotLoc.X-1,0,plotLoc.Z+1)
                                    RequestItemFromStorage("MovingCrate", args.StorageKey, nil, crateLoc)
                                else
                                    self:UpdatePackedPlots(root,args)
                                end
                            else
                                EndMobileEffect(root)
                            end
                        end)
                    else
                        EndMobileEffect(root)
                    end
                end,
            }
        end)

        local clientPreviewSize = 15

        self.ParentObj:RequestClientTargetLocPreview(self.ParentObj, "ClaimLand","plot_preview",Loc(0,0,0),Loc(plotSize.X/clientPreviewSize,1,plotSize.Z/clientPreviewSize))
	end,

    OnExitState = function(self,root)
        
    end,
    
    ErrorInvalidLocation = function(self,root)
        self.ParentObj:SystemMessage("That is not a valid location.", "info")
        EndMobileEffect(root)
    end,

    PlotController = nil,
}