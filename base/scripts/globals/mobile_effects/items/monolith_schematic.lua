MobileEffectLibrary.MonolithSchematic = 
{
    OnEnterState = function(self,root,target,args)        
        local skillUsed = target:GetObjVar("SkillUsed")
        local skillLevelRequired = target:GetObjVar("SkillLevelRequired")
        local shardsRequired = target:GetObjVar("EchoShardsRequired")
        local recipeUsed = target:GetObjVar("RecipeUsed")
        local materialUsed = target:GetObjVar("MaterialUsed")
        local craftTemplate = target:GetObjVar("CraftTemplate")
        local backpack = self.ParentObj:GetEquippedObject("Backpack")

        -- Make sure all our variables are good
        if(
            not skillUsed 
            or not skillLevelRequired  
            or not shardsRequired  
            or not recipeUsed  
            or not materialUsed  
            or not craftTemplate
            or not backpack
        ) then
            self.ParentObj:SystemMessage("Unable to use this schematic.")
            EndMobileEffect(root)
            return false
        end

        -- Do we have enough skill?
        if( GetSkillLevel(self.ParentObj, skillUsed) < skillLevelRequired ) then
            self.ParentObj:SystemMessage("You lack the skill to use this.")
            EndMobileEffect(root)
            return false
        end

        -- Do we have the required materials?
        if( CountItemsInContainer( backpack, "monolith_schematic_resource" ) < shardsRequired ) then
            self.ParentObj:SystemMessage("You need more echo shards.")
            EndMobileEffect(root)
            return false
        end

        local materialData = GetDataFromRecipe(materialUsed)
        local materialCountNeeded = GetResourceAmountFromRecipe(skillUsed, recipeUsed, materialUsed)
        if( CountItemsInContainer( backpack, materialData.CraftingTemplateFile ) < materialCountNeeded ) then
            self.ParentObj:SystemMessage("You need more "..materialData.DisplayName..".")
            EndMobileEffect(root)
            return false
        end

        -- Try to remove the shards and materials from the backpack
        if(
            RemoveItemsInBackpack( self.ParentObj, "monolith_schematic_resource", shardsRequired )
            and RemoveItemsInBackpack( self.ParentObj, materialData.CraftingTemplateFile, materialCountNeeded )
        ) then

            -- EUREKA! Light the fires
            local recipeData = GetDataFromRecipe(recipeUsed)
            local templateData = GetTemplateData(recipeData.CraftingTemplateFile)
            --DebugTable( recipeData )
            --DebugTable( templateData.ObjectVariables )
            Create.InContainer( craftTemplate, backpack, nil, function(createdObj) 
                --target:Destroy()
                self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact")
                self.ParentObj:PlayAnimation("cast_heal")
                PlayEffectAtLoc("WispSummonEffect", self.ParentObj:GetLoc(), 2)
                createdObj:SetObjVar("ArmorBonus", 5)
                createdObj:SetObjVar("ArmorType", templateData.ObjectVariables.ArmorType)
                createdObj:SetObjVar("Material", materialUsed)
                createdObj:SetObjVar("MaxDurability", 240)
                createdObj:SetObjVar("CraftedBy", self.ParentObj:GetName())
                createdObj:SetObjVar("Crafter", self.ParentObj)
                EnchantingHelper.ApplyRandomEnchantsToItem(createdObj, nil, nil, true)
                EndMobileEffect(root)
                return true
            end)

        else

            self.ParentObj:SystemMessage("Failed to create item, something went wrong.", "info")
            EndMobileEffect(root)
            return false

        end

	end,
}