MobileEffectLibrary.Tomebook = 
{
    OnEnterState = function(self,root,target,args)
        --DebugMessage("StartMobileEffect 'Tomebook'")

        self._pageTurns = 0
        self._entries = args.Entries
        --if not self._entries then DebugMessage("Tomebook FAILED - No entries") EndMobileEffect(root) end
        self._tomeId = args.TomeId
        if self._tomeId then
            self._completeTomeData = AllTomes[self._tomeId]
            --if not self._completeTomeData then DebugMessage("Tomebook FAILED - Invalid TomeId") EndMobileEffect(root) end
            if not self._completeTomeData then EndMobileEffect(root) end

            local pageCount = 2
            for i=1, #self._completeTomeData.Entries do
                self._pageIndices[i] = pageCount + 1
                pageCount = pageCount + #self._completeTomeData.Entries[i].Pages
                --DebugMessage(i,"is at page index",self._pageIndices[i],"(",pageCount,"total pages)")
            end
            self._maxPageTurns = math.floor((pageCount - 1) / 2)
            --DebugMessage("Max",self._maxPageTurns,"=",pageCount-1,"->",(pageCount-1)/2,"->",math.floor((pageCount - 1) / 2))
        end
        self.GenerateTomebookWindow(self, root)

        RegisterEventHandler(EventType.DynamicWindowResponse, "TomebookWindow", 
            function( user, id )
                self.HandleTomebookResponse(user, id, self, root)
            end
        )
    end,

    OnStack = function(self,root,target,args)
        self.OnEnterState(self, root, target, args)
    end,

    OnExitState = function(self,root)
        --DebugMessage("ExitMobile Effect 'Tomebook'")
        UnregisterEventHandler("",EventType.DynamicWindowResponse,"TomebookWindow")
    end,

    GenerateTomebookWindow = function(self, root)
        local dynamicWindow = DynamicWindow("TomebookWindow", "", self._bookWidth, self._bookHeight, -self._bookWidth/2, -self._bookHeight/2, "TransparentDraggable", "Center")

        dynamicWindow:AddImage(0, 0, "Spellbook", self._bookWidth, self._bookHeight)

        dynamicWindow:AddButton(726, 21, "", "", 0, 0, "", "", true, "CloseSquare")
        
        if not (self._entries and self._completeTomeData) then
            self.ParentObj:OpenDynamicWindow(dynamicWindow)
            --DebugMessage("Draw Tomebook_Window failed.",self._entries,self._completeTomeData) 
            return 
        end

        local hasPrevPage = self._pageTurns >= 1
        if (hasPrevPage) then
            local pageStr = tostring(self._pageTurns * 2 - 1)
            dynamicWindow:AddButton(62, 25, "Page|"..pageStr, "", 154, 93, "", "", false, "BookPageDown")
        end

        local runeInfo = {}
        local hasNextPage = self._pageTurns < self._maxPageTurns
        if (hasNextPage) then
            local pageStr = tostring(self._pageTurns * 2 + 3)
            dynamicWindow:AddButton(570, 25, "Page|"..pageStr, "", 154, 93, "", "", false, "BookPageUp")
        end

        local buttonState = (self._pageTurns == 0 and "pressed") or ""
        dynamicWindow:AddButton(6, 70, "Page|1", "", 78, 58, "", "", false, "RuneTab", buttonState)

        if self._pageTurns == 0 then
            self.AddTomebookIndexPage(self, dynamicWindow)
        else
            self.AddTomebookDetailPage(self, dynamicWindow)
        end

        self.ParentObj:OpenDynamicWindow(dynamicWindow)
    end,

    HandleTomebookResponse = function(user, id, self, root)
        -- Make sure we are the right user and we didn't just close the window!
        if(id == nil or id == "" or self.ParentObj ~= user) then 
            --TransferContainerContents( self._enchantPouch, self.ParentObj:GetEquippedObject("Backpack") )
            EndMobileEffect(root)
            return
        end

        local page, arg = id:match("(%a+)|(.+)")
        if page == "Page" then
            self._pageTurns = math.ceil(tonumber(arg) / 2 - 1)
            self.GenerateTomebookWindow(self, root)
        else
            EndMobileEffect(root)
        end
    end,

    AddTomebookIndexPage = function(self, dynamicWindow)
        dynamicWindow:AddLabel(236, 44, "[43240f]"..StripColorFromString(self._completeTomeData.Name).."[-]", 0, 0, 46, "center", false, false, "Kingthings_Calligraphica_Dynamic")

        dynamicWindow:AddImage(110, 80, "SpellIndexInfo_Divider", 250, 0, "Sliced")

        local entries = self._completeTomeData.Entries or {}

        local xOffset = 120
        local yOffset = 90
        for entryIndex = 1, #entries do
            local isFilled = self._entries[entryIndex] ~= 0

            if isFilled then
                local page = self._pageIndices[entryIndex] or 1
                dynamicWindow:AddButton(xOffset, yOffset, "Page|"..tostring(page), tostring(entryIndex)..". "..(entries[entryIndex].Title or ""), 250, 34, "", "", false, "BookListSingle")     
            else    
                dynamicWindow:AddButton(xOffset, yOffset, "", tostring(entryIndex)..". ", 250, 34, "", "", false, "BookListSingle", "faded")
            end
            yOffset = yOffset + 36
            if(entryIndex == 8) then
                yOffset = 48
                xOffset = xOffset + 320
            end
        end
    end,

    AddTomebookDetailPage = function(self, dynamicWindow)
        local entryData
        local entryIndex = 0
        local isRightPageANewEntry = false
        local rightPageIndex
        local leftPageIndex = self._pageTurns * 2 + 1
        local leftPageEntryIndex = 1

        --DebugMessage("Pre-Loop.", "page turns:", self._pageTurns, "left page index:",leftPageIndex)

        for i=1, #self._entries do
            local s = "Possible EntryIndex: "..tostring(i)
            local currentEntryTitlePageIndex = self._pageIndices[i]
            local nextEntryTitlePageIndex = 0

            if leftPageIndex >= currentEntryTitlePageIndex and currentEntryTitlePageIndex ~= 0 then
                for j=i+1, #self._entries do
                    if self._entries[j] ~= 0 and nextEntryTitlePageIndex == 0 then
                        nextEntryTitlePageIndex = self._pageIndices[j]
                        rightPageIndex = j
                        s = s.."\nFound Next Entry:"..tostring(nextEntryTitlePageIndex)
                    end
                end
                nextEntryTitlePageIndex = (nextEntryTitlePageIndex == 0) and 999 or nextEntryTitlePageIndex
                if leftPageIndex < nextEntryTitlePageIndex then
                    leftPageEntryIndex = leftPageIndex - currentEntryTitlePageIndex + 1
                    entryIndex = i
                    entryData = self._completeTomeData.Entries[i]
                    isRightPageANewEntry = not (leftPageIndex + 1 > currentEntryTitlePageIndex and leftPageIndex + 1 < nextEntryTitlePageIndex)
                    s = s.." was chosen!"
                    s = s.."\n"..string.format("Left Page Entry Index: %s"..
                        "\nTitle Page Index: %s"..
                        "\nNext Entry Title Page Index: %s",
                        tostring(leftPageEntryIndex), tostring(currentEntryTitlePageIndex), tostring(nextEntryTitlePageIndex)
                        )
                else
                    s = s.." was skipped."
                end
            else
                s = s.." was skipped."
            end
            --DebugMessage(s)
        end

        if not entryData then return end

        local title = entryData.Title
        local pages = entryData.Pages

        -- left page ----
        if pages[leftPageEntryIndex] then
            if leftPageEntryIndex == 1 then
                --dynamicWindow:AddImage(110, 75, "ThinFrameBackgroundExpand", 260, 40, "Sliced")
                dynamicWindow:AddLabel(230, 70,"[43240f]"..tostring(entryIndex)..". "..tostring(title).."[-]",150,0,40,"center",false,false,"Kingthings_Dynamic")
                --dynamicWindow:AddImage(110, 110, "ThinFrameBackgroundExpand", 260, 270, "Sliced")
                dynamicWindow:AddLabel(110, 110,"[43240f]"..pages[leftPageEntryIndex].."[-]",260,270,19,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
            else
                --dynamicWindow:AddImage(110, 75, "ThinFrameBackgroundExpand", 260, 305, "Sliced")
                dynamicWindow:AddLabel(110, 75,"[43240f]"..pages[leftPageEntryIndex].."[-]",260,305,19,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
                dynamicWindow:AddLabel(230,50,"[43240f]"..title.."[-]",260,305,15,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
            end
        end
        dynamicWindow:AddLabel(230,365,"[43240f]"..tostring(leftPageIndex).."[-]",260,340,15,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
        -- end left page ----

        -- right page ----
        if isRightPageANewEntry then
            if not rightPageIndex then rightPageIndex = entryIndex + 1 end
            entryData = self._completeTomeData.Entries[rightPageIndex]
            title = entryData.Title
            pages = entryData.Pages
            --dynamicWindow:AddImage(420, 75, "ThinFrameBackgroundExpand", 260, 40, "Sliced")
            dynamicWindow:AddLabel(555, 70,"[43240f]"..tostring(rightPageIndex)..". "..tostring(title).."[-]",150,0,40,"center",false,false,"Kingthings_Dynamic")
            --dynamicWindow:AddImage(420, 110, "ThinFrameBackgroundExpand", 260, 270, "Sliced")
            dynamicWindow:AddLabel(420, 110,"[43240f]"..pages[1].."[-]",260,280,19,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
            --DebugMessage("Right Page is new Entry")
        elseif pages[leftPageEntryIndex + 1] then
            --dynamicWindow:AddImage(420, 75, "ThinFrameBackgroundExpand", 260, 305, "Sliced")
            dynamicWindow:AddLabel(420, 75,"[43240f]"..pages[leftPageEntryIndex + 1].."[-]",260,305,19,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
            dynamicWindow:AddLabel(555,50,"[43240f]"..title.."[-]",260,305,15,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
            --DebugMessage("Right Page continues current Entry")
        end
        dynamicWindow:AddLabel(555,365,"[43240f]"..tostring(leftPageIndex + 1).."[-]",260,340,15,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
        -- end right page ----
    end,

    _entries = nil,
    _tomeId = nil,
    _completeTomeData = nil,
    _maxPageTurns = 0,
    _pageTurns = 0,
    --_completePageIndices = {},
    _pageIndices = {},

    _bookWidth = 789,
    _bookHeight = 455,
}