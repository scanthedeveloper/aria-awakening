MobileEffectLibrary.CookingPot = 
{
    EndOnMovement = true,
    EndOnAction = true,
    IgnoreEndOnActionTypes = {"Pickup"},
    OnEnterState = function(self,root,target,args)
        self.Target = target
        if (self.Target == nil) then
            EndMobileEffect(root)
            return
        end

        if (self.CanCook(self,root)) then
            self._CookOne = args.CookOne

            RegisterEventHandler(EventType.Message, "CreateCookedItems", function(user, resourceType, amount)
                if( amount ~= nil and amount > 0 and resourceType ~= nil ) then
                    local backpack = self.ParentObj:GetEquippedObject("Backpack")
                    if ( backpack == nil ) then return false end

                    Create.Stack.InBackpack(FoodStats.BaseFoodStats[resourceType].Template, self.ParentObj, amount, nil, function(success)
                        if ( success ) then
                                local stackCount = success:GetObjVar("StackCount")
                                if ( stackCount ) then
                                    CallFunctionDelayed(TimeSpan.FromMilliseconds(1),function()UpdateStackName(stackCount, success)end)
                                end
                                self.ParentObj:SystemMessage(amount.." ".. resourceType .. " placed into your backpack.", "event")
                        else
                                DebugMessage("Food creation to backpack failed for playerId: "..self.ParentObj.Id)
                        end
                    end)
                end

                -- If they only wanted to cook one and we've already cooked lets stop
                if( self._CookOne == true and self._AlreadyCooked ) then
                    EndMobileEffect(root)
                else
                    -- This is used to check if the bot is empty after cooking.
                    CallFunctionDelayed(TimeSpan.FromMilliseconds(1),function()
                        if( self.CanCook(self,root) ) then
                            self.DoCook(self, root)
                        end
                    end) 
                end
            end)
            
            self.DoCook(self, root)
            
        else
            EndMobileEffect(root)
            return false
        end
    end,

    DoCook = function(self, root)
        self.ParentObj:StopMoving()
        SetMobileMod(self.ParentObj, "Busy", "CookingPot", true)
        self.ParentObj:PlayAnimation("carve")
        self.StartProgressBar(self, root)
    end,


    OnExitState = function(self,root)
        self.Target = nil
        self.ParentObj:PlayAnimation("idle")
        UnregisterEventHandler("", EventType.Message, "CreateCookedItems")
        SetMobileMod(self.ParentObj, "Busy", "CookingPot", nil)

        ProgressBar.Cancel("CookingPot", self.ParentObj)
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
    end,

    StartProgressBar = function(self,root)
        ProgressBar.Show(
        {
            TargetUser = self.ParentObj,
            Label = "Cooking",
            Duration = self.PulseFrequency,
            PresetLocation = "AboveHotbar",
            DialogId = "CookingPot",
            CanCancel = true,
            CancelFunc = function()
                self.Interrupted(self, root)
            end,
        })
    end,

    CanCook = function(self, root)

        if (self.Target == nil) then EndMobileEffect(root) end

        local heatSource = FindObject(SearchHasObjVar("HeatSource",OBJECT_INTERACTION_RANGE),self.ParentObj)
        
        if (heatSource == nil) then 
            self.ParentObj:SystemMessage("[$1779]")
            EndMobileEffect(root)
            return false
        end

        local conts = self.Target:GetContainedObjects()
        if(conts == nil or (#conts < 1)) then 
            self.ParentObj:SystemMessage("There is nothing in the pot.","info")
            EndMobileEffect(root)
            return false
        end
        return true

    end,

    VerifyInBackpack = function(self,root)
        -- item was destroyed or something?
        if ( not self.Target or not self.Target:IsValid() or self.Target:TopmostContainer() ~= self.ParentObj ) then
            self.ParentObj:SystemMessage("Cooking pot must be in backpack to cook.", "info")
            EndMobileEffect(root)
            return false
        end
        return true
    end,

    Interrupted = function(self, root)
        self.ParentObj:SystemMessage("Cooking interrupted.", "info")
        EndMobileEffect(root)
    end,

    AiPulse = function(self,root)
        
        -- resource effects will verify in backpack the first use, but not on continued use.
        --if ( not self.VerifyTarget(self,root,self.Target) or not self.VerifyInBackpack(self,root) ) then return end
        if not(self.VerifyInBackpack(self, root)) then return end
        if not(self.CanCook(self, root)) then
            EndMobileEffect(root)
            return false
        end

        -- did cook pot break?
        if ( not self.Target or not self.Target:IsValid() ) then
            EndMobileEffect(root)
            return false
        end

        self._AlreadyCooked = true
        if( self.CanCook(self,root) ) then
            CookFood(self.ParentObj, self.Target)
        end

    end,

    PulseFrequency = TimeSpan.FromSeconds(5),

    _Difficulty = 100,
    _CookOne = true,
    _AlreadyCooked = false,
} 