MobileEffectLibrary.HairDye = 
{
	OnEnterState = function(self,root,target,args)
			if ( not self.ValidateDying(self,root,target,args) ) then
				EndMobileEffect(root)
				return
			end

			ClientDialog.Show{
				TargetUser = self.ParentObj,
				DialogId = "DyeHair",
				TitleStr = "Hair Dye",
				DescStr = "Are you sure you would like to dye your hair and beard this color? The dye will be consumed.",
				Button1Str = "Confirm",
				Button2Str = "Cancel",
				ResponseObj = self.ParentObj,
				ResponseFunc = function( user, buttonId )
					buttonId = tonumber(buttonId)
					if ( buttonId == 0 ) then
						self.DyeHair(self,root,target,args)
						EndMobileEffect(root)
						return
					else
						EndMobileEffect(root)
						return
					end
				end,
			}
	end,

	ValidateDying = function(self,root,target,args)
		if ( not target:IsValid() ) then
			self.ParentObj:SystemMessage("Missing hair dye.", "info")
			return false
		end

		if ( not IsInBackpack(target, self.ParentObj) ) then
			self.ParentObj:SystemMessage("That must be in your backpack to use.", "info")
			return false
		end

		local hair = self.ParentObj:GetEquippedObject("BodyPartHair")
		local facialHair = self.ParentObj:GetEquippedObject("BodyPartFacialHair")
		if ( not hair and not facialHair ) then
			self.ParentObj:SystemMessage("You decide it would be wasteful to use this on your head.", "info")
			return false
		end
		
		return true
	end,

	DyeHair = function(self,root,target,args)
		if ( self.ValidateDying(self,root,target,args) ) then
			target:Destroy()
			self.ParentObj:PlayObjectSound("event:/character/skills/crafting_skills/alchemy/alchemy",false)
			self.ParentObj:SystemMessage("You massage the hair dye into your fabulous locks.", "info")
			local hair = self.ParentObj:GetEquippedObject("BodyPartHair")
			local facialHair = self.ParentObj:GetEquippedObject("BodyPartFacialHair")
			local hue = target:GetHue() or 0
			if ( hair ) then hair:SetHue(hue) end
			if ( facialHair ) then facialHair:SetHue(hue) end
		end
	end,

	OnExitState = function(self,root)
	end,
}