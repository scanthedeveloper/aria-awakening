MobileEffectLibrary.MobileModConsumable = 
{
    PersistDeath = true,
    PersistSession = true,

    OnEnterState = function(self,root,target,args)

        -- If we don't have the right values set we need to get out of here.
        if ( not args.MobileMods or #args.MobileMods < 1 or not args.Name ) then
            EndMobileEffect(root)
            return false
        end

        -- Set variables
        self._MobileMods = args.MobileMods or {}
        self._Identifier = "MobileModConsume"..uuid()
        self._Duration = args.Duration or self._Duration
        self._BuffIcon = args.BuffIcon or self._BuffIcon
        self._BuffText = args.BuffText or self._BuffText
        self._FadeMessage = args.FadeMessage or self._FadeMessage
        self._PlayEffect = args.PlayEffect or self._PlayEffect
        self._Name = args.Name or self._Name

        -- Play effect for consuming
        self.ParentObj:PlayEffectWithArgs(self._PlayEffect, 5.0)

        -- Add buff icon
        AddBuffIcon(self.ParentObj, self._Identifier, self._Name, self._BuffIcon, self._BuffText, false)

        -- Set mobile mod values
        for i=1, #self._MobileMods do 
            SetMobileMod(self.ParentObj, self._MobileMods[i].MobileMod, self._Identifier, self._MobileMods[i].Amount)
        end

    end,

    OnExitState = function(self,root,target,args)
        
        -- Play
        self.ParentObj:PlayEffectWithArgs(self._PlayEffect, 5.0)
        
        -- Alert player
        self.ParentObj:SystemMessage(self._FadeMessage, "info")

        -- Remove buff icon
        RemoveBuffIcon(self.ParentObj, self._Identifier)
        
        -- Clear mobile mods
        for i=1, #self._MobileMods do 
            SetMobileMod(self.ParentObj, self._MobileMods[i].MobileMod, self._Identifier, nil)

        end

    end,

    GetPulseFrequency = function(self,root)
		return self._Duration
	end,

    AiPulse = function(self,root)
        EndMobileEffect(root)
	end,

    _Duration = TimeSpan.FromMinutes(1),
    _BuffIcon = "Dispel",
    _BuffText = "",
    _PlayEffect = "ManaInfuseGive",
    Amount = 1,
    _Identifier = nil,
    _Name = "Missing Name",
    _MobileMods = {},
    _FadeMessage = "You feel the magic fade."
}