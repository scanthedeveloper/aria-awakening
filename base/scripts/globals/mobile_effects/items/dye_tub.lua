MobileEffectLibrary.DyeTub = 
{
	OnEnterState = function(self,root,target,args)
		RegisterEventHandler(EventType.ClientTargetLocResponse, "DyeTarget",
			function (success,targetLoc,targetObj,user)
				if ( success ) then
					if ( not targetObj ) then
						--self.ParentObj:SystemMessage("No target selected", "info")
						EndMobileEffect(root)
						return
					end

					if ( not target:IsValid() ) then
						self.ParentObj:SystemMessage("Missing dye tub.", "info")
						EndMobileEffect(root)
						return
					end
					
					if not(Plot.CheckUseObjectSecurity(self.ParentObj,target)) then
						EndMobileEffect(root)
						return
					end
				
					--[[ SCAN DISABLED
					if ( not targetObj:HasObjVar("CraftedBy") ) then
						self.ParentObj:SystemMessage("May only dye crafted items.", "info")
						EndMobileEffect(root)
						return
					end
					]]

					local charges = target:GetObjVar("Charges") or 0
					if ( not charges or charges < 1 ) then
						self.ParentObj:SystemMessage("That tub has no dye in it!", "info")
						EndMobileEffect(root)
						return false
					end

					if not ( targetLoc ) then
						self.ParentObj:SystemMessage("Invalid target.", "info")
						EndMobileEffect(root)
						return
					end

					local tubType = target:GetObjVar("TubType") or "No dyes added yet"
					local armorType = targetObj:GetObjVar("ArmorType")
					if ( tubType == "No dyes added yet" ) then
						self.ParentObj:SystemMessage("Cannot dye that.", "info")
						EndMobileEffect(root)
						return
					elseif ( tubType == "Clothing" ) then
						local canDye = targetObj:GetObjVar("CanDye")
						if ( not canDye or canDye ~= true ) then
							self.ParentObj:SystemMessage("Cannot dye that.", "info")
							EndMobileEffect(root)
							return
						end
					elseif ( not armorType ) then
						self.ParentObj:SystemMessage("Cannot dye that.", "info")
						EndMobileEffect(root)
						return
					elseif ( tubType == "Light" and
					( armorType ~= "Padded" and armorType ~= "Linen" and armorType ~= "MageRobe" ) ) then
						self.ParentObj:SystemMessage("Cannot dye that.", "info")
						EndMobileEffect(root)
						return
					elseif ( tubType == "Leather" and
					( armorType ~= "Leather" and armorType ~= "Hardened" ) ) then
						self.ParentObj:SystemMessage("Cannot dye that.", "info")
						EndMobileEffect(root)
						return
					elseif ( tubType == "Metal" and
					( armorType ~= "Scale" and armorType ~= "Plate" and armorType ~= "Chain" and armorType ~= "FullPlate" ) ) then
						self.ParentObj:SystemMessage("Cannot dye that.", "info")
						EndMobileEffect(root)
						return
					end

					local hue = target:GetHue() or 0
					targetObj:SetHue(hue)
					self.ParentObj:PlayObjectSound("event:/environment/water/footbridge",false,2.0)
					local charges = (target:GetObjVar("Charges") or 0) - 1
					target:SetObjVar("Charges", charges)
					SetItemTooltip(target)
					if ( charges < 1 ) then
						target:SetHue(10)
						target:SetObjVar("TubType", "No dyes added yet")
						target:SetObjVar("DyeColorName", "None")
						SetItemTooltip(target)
						self.ParentObj:SystemMessage("The dye tub is out of dye!", "info")
					end
					EndMobileEffect(root)
				else
					EndMobileEffect(root)
				end
			end)
		-- ask for a target
		self.PresentTarget(self.ParentObj)
	end,

	PresentTarget = function(user)
		user:RequestClientTargetLoc(user, "DyeTarget")
	end,

	OnExitState = function(self,root)
		UnregisterEventHandler("", EventType.ClientTargetLocResponse, "DyeTarget")
	end,
}

MobileEffectLibrary.DyeTubDyes = 
{
	OnEnterState = function(self,root,target,args)
		local hue = args:GetHue() or 0
		local colorName = args:GetObjVar("DyeColorName") or "Unknown"
		local tubType = args:GetObjVar("TubType")
		local chargesDye = args:GetObjVar("Charges")
		local charges = target:GetObjVar("Charges") or 0
		
		if ( charges > 0 ) then
			local targetTubType = target:GetObjVar("TubType")
			local targetHue = target:GetHue()
			if ( targetTubType
			and targetHue
			and tubType == targetTubType
			and hue == targetHue ) then
				self.ChangeDye(self,root,target,args)
				EndMobileEffect(root)
			else
				ClientDialog.Show{
					TargetUser = self.ParentObj,
					DialogId = "RemoveDye",
					TitleStr = "Remove Old Dye?",
					DescStr = "This tub already contains dye that is different from what you are trying to apply. If you continue, the existing dye will be lost.",
					Button1Str = "Confirm",
					Button2Str = "Cancel",
					ResponseObj = self.ParentObj,
					ResponseFunc = function( user, buttonId )
						buttonId = tonumber(buttonId)
						if ( buttonId == 0 ) then
							self.ChangeDye(self,root,target,args,true)
							EndMobileEffect(root)
						else
							EndMobileEffect(root)
							return false
						end
					end,
				}
				
			end
		else
			self.ChangeDye(self,root,target,args,true)
			EndMobileEffect(root)
		end
	end,

	ChangeDye = function(self,root,target,args,resetCharges)
		local targetTemplate = target:GetCreationTemplateId()
		if ( not target or not target:IsValid() or not targetTemplate or targetTemplate ~= "dye_tub" ) then
			self.ParentObj:SystemMessage("That's not a dye tub!", "info")
			EndMobileEffect(root)
			return false
		end

		if not(Plot.CheckUseObjectSecurity(self.ParentObj,target)) then
			EndMobileEffect(root)
			return
		end

		local hue = args:GetHue() or 0
		local colorName = args:GetObjVar("DyeColorName") or "Unknown"
		local tubType = args:GetObjVar("TubType")
		local chargesDye = args:GetObjVar("Charges")
		local charges = target:GetObjVar("Charges") or 0

		target:SetObjVar("DyeColorName", colorName)
		target:SetObjVar("TubType", tubType)
		if ( resetCharges and resetCharges ~= false ) then
			target:SetObjVar("Charges", chargesDye)
		else
			target:SetObjVar("Charges", charges + chargesDye)
		end
		target:SetHue(hue)
		RequestRemoveFromStack(args,1)
		SetItemTooltip(target)
		self.ParentObj:PlayObjectSound("event:/environment/water/footbridge",false,2.0)
		self.ParentObj:SystemMessage("You apply the dyes to the "..StripColorFromString(target:GetName()), "info")
	end,

	OnExitState = function(self,root)
	end,
}