MobileEffectLibrary.MovingCrate = 
{
    QTarget = true,

    RetrieveFromStorageHook = function(crateObj)
        -- meant for mods to override, this is called right after a moving crate is pulled from storage. crateObj possible to be nil
        -- do not write logic in this as this function is intended to be replaced, not extended
    end,
    
    -- Args 
    -- StorageKey
    OnEnterState = function(self,root,target,args)
        if ( self.ParentObj:HasTimer("MovingCrateTimer") ) then
            self.ParentObj:SystemMessage("Please wait to use this again.", "info")
            EndMobileEffect(root)
            return
        end
        self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(2.5), "MovingCrateTimer")
        
        RegisterSingleEventHandler(EventType.RequestItemFromStorageResult,"MovingCrate",
            function (objHeader,objRef)
                MobileEffectLibrary.MovingCrate.RetrieveFromStorageHook(objRef)
                if(objRef) then
                    objRef:SetObjVar("StorageKey",args.StorageKey)
                    objRef:SetObjVar("DecayDate",DateTime.UtcNow:Add(TimeSpan.FromDays(7)))
                    objRef:SetObjVar("PlotController",self.PlotController)
                    objRef:SetObjVar("IsPlotObject",true)
                    objRef:SetObjVar("NoReset",true)
                    objRef:SetObjVar("SecureContainer",true)
                    objRef:SetObjVar("LockedDown",true)
                    objRef:SetObjVar("locked",true)

                    objRef:DelModule("container")
                    objRef:AddModule("moving_crate")

                    Create.InContainer("land_deed",objRef,nil,function (landDeedObj)
                        if(args.FreeResize) then
                            landDeedObj:SetObjVar("FreeResize",args.FreeResize)
                            SetTooltipEntry(landDeedObj,"pack_house","\n\nFree Resize: "..args.FreeResize.X.." x "..args.FreeResize.Z, -200)
                        end
                    end)

                    SetGlobalVar("PackedPlots."..args.UserId,function (record)
                        local plotIndex = -1
                        for i,plotInfo in pairs(record) do
                            if(plotInfo.StorageKey == args.StorageKey) then
                                plotIndex = i
                                break
                            end
                        end
                        if(plotIndex ~= -1) then
                            table.remove(record,plotIndex)
                            return true
                        else
                            return false
                        end
                    end,
                    function (success)
                        if not(success) then
                            DebugMessage("ERROR: Failed to removed packed plot from global var "..args.StorageKey)
                        end

                        self.ParentObj:SendMessage("RefreshStoredItemManager")
                        EndMobileEffect(root)
                    end)
                else
                    DebugMessage("ERROR: Failed to load moving crate "..args.StorageKey)
                    EndMobileEffect(root)
                end                
            end)

        RegisterSingleEventHandler(EventType.ClientTargetLocResponse, "MovingCrate", function(success, loc, gameobj)
            if ( not success or not loc or not target or not target:IsValid() ) then
                self.ErrorInvalidLocation(self,root)
                return
            end
            if ( self.ParentObj:GetLoc():Distance(loc) > 15 ) then
                self.ParentObj:SystemMessage("Too far away.", "info")
                EndMobileEffect(root)
                return
            end
            if not( self.ParentObj:HasLineOfSightToLoc(loc) ) then
                self.ParentObj:SystemMessage("Cannot see that.", "info")
                EndMobileEffect(root)
                return
            end

            self.PlotController = Plot.GetAtLoc(loc)
            if not(self.PlotController) or not(Plot.HasControl(self.ParentObj,self.PlotController)) then
                self.ParentObj:SystemMessage("You can only place this on a plot that you own.", "info")
                EndMobileEffect(root)
                return
            end

            RequestItemFromStorage("MovingCrate", args.StorageKey, nil, loc)
        end)

        self.ParentObj:RequestClientTargetLocPreview(self.ParentObj, "MovingCrate","crate_empty",Loc(0,0,0),Loc(1,1,1))
	end,

    OnExitState = function(self,root)
        
    end,
    
    ErrorInvalidLocation = function(self,root)
        self.ParentObj:SystemMessage("That is not a valid location.", "info")
        EndMobileEffect(root)
    end,

    PlotController = nil,

}