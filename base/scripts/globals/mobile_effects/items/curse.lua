MobileEffectLibrary.Curse = 
{

	OnEnterState = function(self,root,target,args)
		if ( not target or not target:IsValid() or not(GetEquipmentClass(target)) ) then
			self.ParentObj:SystemMessage("Cannot curse that!", "info")
			EndMobileEffect(root)
			return false
		end

		if ( target:TopmostContainer() ~= self.ParentObj ) then
			self.ParentObj:SystemMessage("Cannot reach that.", "info")
			EndMobileEffect(root)
			return false
		end

		--[[
		if( not (target:HasObjVar("ArmorType") or target:HasObjVar("WeaponType")) or target:HasObjVar("ShieldType")) then
			EndMobileEffect(root)
			self.ParentObj:SystemMessage("This item cannot be Cursed.", "info")
			return false
        end

		
		if not ( target:HasObjVar("WeaponType") ) then
				self.ParentObj:SystemMessage("You can only curse a Weapon item.", "info")
			end
			EndMobileEffect(root)
			return false
		end
		]]

		if ( target:HasObjVar("Blessed") ) then
			if ( target:HasObjVar("Cursed") ) then
				self.ParentObj:SystemMessage("That item is already cursed!", "info")
			else
				self.ParentObj:SystemMessage("That item is already blessed!", "info")
			end
			EndMobileEffect(root)
			return false
		end

		--SCAN ADDED (Validate qualified item type)
		local armorType = false
		local weaponType = false
		local shieldType = false
		if(target:HasObjVar("ArmorType")) then
			armorType = true
		end
		if(target:HasObjVar("WeaponType")) then
			weaponType = true
		end
		if(target:HasObjVar("ShieldType")) then
			shieldType = true
		end

		--SCAN ADDED (Validate qualified item type)
		if(armorType == true) then
			target:SetObjVar("Blessed", true)
			target:SetObjVar("Cursed", true)
			SetItemTooltip(target)
			self.ParentObj:SystemMessage("You have cursed "..StripColorFromString(target:GetName()), "info")
		elseif (weaponType == true) then
			target:SetObjVar("Blessed", true)
			target:SetObjVar("Cursed", true)
			SetItemTooltip(target)
			self.ParentObj:SystemMessage("You have cursed "..StripColorFromString(target:GetName()), "info")
		elseif (shieldType == true) then
			target:SetObjVar("Blessed", true)
			target:SetObjVar("Cursed", true)
			SetItemTooltip(target)
			self.ParentObj:SystemMessage("You have cursed "..StripColorFromString(target:GetName()), "info")
		elseif (armorType == false) and (weaponType == false) and (shieldType == false) then
			self.ParentObj:SystemMessage("You cannot enchant that item.", "info")
			EndMobileEffect(root)
			return false
		end
		EndMobileEffect(root)
	end
}