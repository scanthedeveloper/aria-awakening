MobileEffectLibrary.ScrollOfPenitence = 
{
    OnEnterState = function(self,root,target,args)
        if ( self.ParentObj:HasTimer("ScrollOfPenitenceTimer") ) then
            self.ParentObj:SystemMessage("Please wait to use this again.", "info")
            EndMobileEffect(root)
            return false
        end
        self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(2.5), "ScrollOfPenitenceTimer")

        -- Must be a murderer
        if not( IsMurderer(self.ParentObj) ) then
            self.ParentObj:SystemMessage("Only Murderers can utilize this scroll.", "info")
            EndMobileEffect(root)
            return false
        end

        -- Cannot be in an active conflict
        if(HasAnyActiveConflictRecords(self.ParentObj) or HasMobileEffect(self.ParentObj, "OnTheRun") ) then
            self.ParentObj:SystemMessage("This cannot be used while in combat or 'On the Run'.", "info")
            EndMobileEffect(root)
            return false
        end

        -- Must be in an inn or your house
        if not( self.ParentObj:IsInRegion("WorldInns") or Plot.IsInHouse(self.ParentObj,true) ) then
            self.ParentObj:SystemMessage("Must be in your home or an inn to use this scroll.","info")
            EndMobileEffect(root)
            return false
        end

        self.ParentObj:SetObjVar("IsPenitent", true)
        self.ParentObj:SetObjVar("PenitentExpires",  DateTime.UtcNow:Add(TimeSpan.FromDays(30)) )

        -- update the name
        self.ParentObj:SendMessage("UpdateName")
        -- update all pets
        ForeachActivePet(self.ParentObj, function(pet)
            pet:SetObjVar("IsPenitent", true)
            pet:SendMessage("UpdateName")
        end, true)

        self.ParentObj:PlayEffect("HealEffect")
        self.ParentObj:PlayObjectSound("event:/ui/quest_complete", false)

        self.ParentObj:SystemMessage("You have repented.","info")
        self.ParentObj:SystemMessage("Should you commit any criminal acts in the next 30 days your penitent status will be removed and any murder counts you had will be restored.")

        EndMobileEffect(root)
        return true
	end,

}