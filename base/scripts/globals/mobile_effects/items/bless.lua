MobileEffectLibrary.Bless = 
{

	OnEnterState = function(self,root,target,args)

		if ( 
			not target or -- Check target nil
			not target:IsValid() or -- Check target in region
			(
				(
					not target:HasObjVar("CanDye") or
					target:HasObjVar("ArmorType") or -- Check target is not armor
					target:HasObjVar("WeaponType") -- Check target is not weapon
				)
				and
				(
					not self.IsBlessableTemplate(self, target)
				)
			)
		) then
			self.ParentObj:SystemMessage("Cannot bless that!", "info")
			EndMobileEffect(root)
			return false
		end

		if ( target:TopmostContainer() ~= self.ParentObj ) then
			self.ParentObj:SystemMessage("Cannot reach that.", "info")
			EndMobileEffect(root)
			return false
		end

		if ( target:HasObjVar("Blessed") ) then
			if ( target:HasObjVar("Cursed") ) then
				self.ParentObj:SystemMessage("That item is already cursed!", "info")
			else
				self.ParentObj:SystemMessage("That item is already blessed!", "info")
			end
			EndMobileEffect(root)
			return false
		end

		target:SetObjVar("Blessed", true)
		SetItemTooltip(target)

		self.ParentObj:SystemMessage("You have blessed "..StripColorFromString(target:GetName()), "info")

		EndMobileEffect(root)
	end,

	IsBlessableTemplate = function(self, item)
		local templateId = item:GetCreationTemplateId()
		for i=1, #self.BlessableTemplates do 
			if( templateId == self.BlessableTemplates[i] ) then return true end
		end
		return false
	end,

	BlessableTemplates = 
	{
		"explorer_cloak",
		"dungeoneer_cloak",
		"artisan_cloak",
		"procurement_cloak",
	}

}