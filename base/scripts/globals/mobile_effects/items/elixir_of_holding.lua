--SCAN UPDATED
-- Added visual effects
-- Added animations
-- Updated tooltips

MobileEffectLibrary.ElixirOfHolding = 
{
    PersistDeath = true,
    PersistSession = true,

    OnEnterState = function(self,root,target,args)
        self.Duration = args.Duration or self.Duration
        self.Amount = args.Amount or self.Amount
        self.Name = args.Name or "Elixir of Holding"
        --SCAN UDPATED VISUAL EFFECT
        self.ParentObj:PlayEffectWithArgs("BardDistortionOrange", 5.0)
        self.ParentObj:PlayEffectWithArgs("BardDistortionRed", 5.0)
        self.ParentObj:PlayEffectWithArgs("BardDistortionViolet", 5.0)
        self.ParentObj:PlayAnimation("showingOff")
        --SCAN UDPATED ToolTip
        self.ParentObj:SystemMessage("Your bag weight has temporarily increased.", "info")
        AddBuffIcon(self.ParentObj, "ElixirOfHolding",self.Name,"backpack","+"..self.Amount.." Weight", false)
        SetMobileMod(self.ParentObj, "MaxWeightPlus", "ElixirOfHolding", self.Amount)
    end,

    OnExitState = function(self,root,target,args)
        --SCAN UDPATED VISUAL EFFECT
        self.ParentObj:PlayEffectWithArgs("BardDistortionOrange", 5.0)
        self.ParentObj:PlayEffectWithArgs("BardDistortionRed", 5.0)
        self.ParentObj:PlayEffectWithArgs("BardDistortionViolet", 5.0)
        self.ParentObj:PlayAnimation("bashful")
        --SCAN UDPATED ToolTip
        self.ParentObj:SystemMessage("Your bag weight has returned to normal.", "info")
        RemoveBuffIcon(self.ParentObj, "ElixirOfHolding")
        SetMobileMod(this, "MaxWeightPlus", "ElixirOfHolding", nil)
    end,

    GetPulseFrequency = function(self,root)
		return self.Duration
	end,

    AiPulse = function(self,root)
        EndMobileEffect(root)
	end,

    Duration = TimeSpan.FromMinutes(1),
    Amount = 1,
}