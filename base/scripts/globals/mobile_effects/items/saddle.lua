local saddleable = {
    horse = "item_statue_mount_horse",
    bay_horse = "item_statue_mount_horse",
    void_horse = "item_statue_mount_horse",
    desert_horse = "item_statue_mount_horse",
    black_horse = "item_statue_mount_horse",
    chestnut_horse = "item_statue_mount_horse",
    cultist_horse = "item_statue_mount_horse",
    great_bear = "item_statue_mount_bear",
    llama = "item_statue_mount_llama",
    savage_bear = "item_statue_mount_savage_bear",
    savage_horse = "item_statue_mount_savage_horse",
    basilisk = "item_statue_mount_cockatrice",
}

MobileEffectLibrary.Saddle = 
{

    OnEnterState = function(self,root,target,args)
        local saddleObj = args
        
        if not( self.ValidTarget(self.ParentObj,target) ) then
            EndMobileEffect(root)
            return false
        end
        
        local targetTemplate = target:GetCreationTemplateId()
        Create.InBackpack(saddleable[targetTemplate], self.ParentObj, nil, function(statue)
            if ( statue ) then
                PlayEffectAtLoc("CloakEffect", target:GetLoc())
                target:PlayObjectSound("event:/magic/air/magic_air_cloack",false)
                self.ParentObj:SystemMessage("You have recieved a "..GetTemplateObjectName(saddleable[targetTemplate])..".","info")
                local equipmentTemplate = saddleObj:GetObjVar("EquipmentTemplate") or "saddle"
                statue:SetObjVar("EquipmentTemplate",equipmentTemplate)
                statue:SetObjVar("StatueMountTemplate",target:GetCreationTemplateId())
                target:Destroy()
                saddleObj:Destroy()
            else
                self.ParentObj:SystemMessage("Failed to create statue.", "info")
            end
            EndMobileEffect(root)
        end)
    end,
    
    -- this is used for validating the spell cast as well (to avoid resource consumption)
    ValidTarget = function(mobile,target)
        if ( not target or not target:IsValid() ) then
            mobile:SystemMessage("Invalid target.", "info")
            return false
        end
        if not( saddleable[target:GetCreationTemplateId()] ) then
            mobile:SystemMessage("You can not apply a saddle to that.", "info")
            return false
        end
        if ( target:GetObjVar("controller") ~= mobile ) then
            mobile:SystemMessage("Can only be applied to pets you own.", "info")
            return false
        end
        if ( IsDead(target) ) then
            mobile:SystemMessage("Cannot apply to dead pets.", "info")
            return false
        end
        if not( IsMount(target) ) then
            mobile:SystemMessage("Can only be applied to mounts.", "info")
            return false
        end
        if ( target:HasObjVar("HasPetPack") ) then
            mobile:SystemMessage("Can not apply a saddle to pack horses.", "info")
            return false
        end
        return true
    end,
}