MobileEffectLibrary.GodlyWeaponScroll = 
{

	OnEnterState = function(self,root,target,args)
        do
            self.ParentObj:SystemMessage("This item has lost it's magical properties","info")
            EndMobileEffect(root)
            return
        end
    
        if ( not target or not target:IsValid() or
        ( not target:HasObjVar("WeaponType") and not target:HasObjVar("ShieldType") ) ) then
			self.ParentObj:SystemMessage("Can only use on weapons and shields.", "info")
			EndMobileEffect(root)
			return false
		end

		if ( target:TopmostContainer() ~= self.ParentObj ) then
			self.ParentObj:SystemMessage("Cannot reach that.", "info")
			EndMobileEffect(root)
			return false
        end
        
        if ( target:HasObjVar("Godly") ) then
			self.ParentObj:SystemMessage("That is already Godly!", "info")
			EndMobileEffect(root)
			return false
		end

        --imbue the item
        target:SetObjVar("Cursed", true)
        target:SetObjVar("Blessed", true)
        target:SetObjVar("Godly", true)
        target:SetObjVar("MilitiaRankRequired", 7)
        target:SetHue(1022)
        target:SetName(self.ParentObj:GetName().."'s "..target:GetName())
        local maxDurability = target:GetObjVar("MaxDurability")
        if ( maxDurability ) then
            target:SetObjVar("MaxDurability", maxDurability * 2)
            if ( target:HasObjVar("Durability") ) then target:DelObjVar("Durability") end
        end
        target:SetObjVar("RequiresUser", self.ParentObj)
        SetItemTooltip(target)

        self.ParentObj:PlayEffect("LightningCloudEffect")
        self.ParentObj:PlayObjectSound("event:/magic/air/magic_air_lightning_impact",false)
		self.ParentObj:SystemMessage("The gods deem you worthy, "..self.ParentObj:GetName().."!", "event")

		EndMobileEffect(root)
	end,

}