MobileEffectLibrary.Bookshelf = 
{
    OnEnterState = function(self,root,target,args)
        --DebugMessage("StartMobileEffect 'Bookshelf'")

        self._Message = nil
        self._shelfIndex = nil
        self._isTakingTomebook = false
        self._shelfData = target:GetObjVar("ShelfData") or {}
        self._bookshelf = target
        

        if not self.IsWithinRange(self, root) then return false end
        self.GenerateBookshelfWindow(self, root)

        RegisterEventHandler(EventType.DynamicWindowResponse, "BookshelfWindow", function( user, id )
            self.HandleBookshelfResponse(self, root, user, id)
        end)

        RegisterEventHandler(EventType.ClientTargetGameObjResponse, "pickTomebook", function( target, user )
            self.AddToBookshelf(self, root, target, user)
        end)
    end,

    OnStack = function(self,root,target,args)
        self.OnEnterState(self, root, target, args)
    end,

    OnExitState = function(self,root)
        --DebugMessage("ExitMobile Effect 'Bookshelf'")
        if( self._Message ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
        UnregisterEventHandler("",EventType.DynamicWindowResponse,"BookshelfWindow")
        UnregisterEventHandler("",EventType.ClientTargetGameObjResponse, "pickTomebook")
        self.ParentObj:CloseDynamicWindow("BookshelfWindow")
    end,

    IsWithinRange = function( self, root )
        if( not self._bookshelf or self.ParentObj:DistanceFrom(self._bookshelf) > 5 ) then
            self._Message = "Too far from bookshelf."
            EndMobileEffect(root)
            return false
        else
            return true
        end
    end,

    GenerateBookshelfWindow = function(self, root)
        local booksPerShelf = self._bookshelf:GetObjVar("BooksPerShelf") or 0
        local shelfCount = self._bookshelf:GetObjVar("ShelfCount") or 0

        local sizeX = 50
        local sizeY = 80
        local rowY = 40

        local xOffset = sizeX * 0.5
        local yOffset = 20

        local plotController = Plot.GetAtLoc(self._bookshelf:GetLoc())
        --local isStranger = Plot.IsStranger(plotController, self.ParentObj) or false
        local isOwner = Plot.IsOwner(self.ParentObj, plotController) or false

        local windowWidth = xOffset + (sizeX * booksPerShelf) + xOffset * 2 + 10
        local windowHeight = yOffset + (sizeY + rowY) * shelfCount + yOffset * 4

        self._mainWindow = DynamicWindow("BookshelfWindow","Bookshelf",windowWidth,windowHeight,-410,-280,"Default","Center")

        for i=1, shelfCount do
            local shelfY = yOffset + (sizeY + rowY) * (i - 1)
            self._mainWindow:AddImage(xOffset, shelfY,"BasicWindow_Panel",sizeX * booksPerShelf + 20, sizeY + 30,"Sliced", nil)
            local shelfIndexOffset = (i-1) * booksPerShelf

            for j=1, booksPerShelf do
                local index = shelfIndexOffset + j

                local isFilled = self._shelfData[index].TomeId ~= nil
                local bookX = xOffset + (j - 1) * sizeX + 10
                local bookY = shelfY + 10

                if isFilled then
                    local collectionIndex = self._shelfData[index].CollectionIndex
                    local tomebookData = AllTomes[self._shelfData[index].TomeId] or {}
                    local collectionData = AllTomeCollections[collectionIndex] or {}
                    local buttonText = (collectionData.WindowSpine or "").." "..tostring(self._shelfData[index].TomeIndex)
                    local tomebookName = tostring(tomebookData.Name)
                    local tomebookTooltip = "Shelf Position: "..tostring(index).."\nRead "..(collectionIndex and tostring(collectionData[1]).." #"..tostring(self._shelfData[index].TomeIndex)..":\n" or "")..tomebookName
                    self._mainWindow:AddImage(bookX, bookY,"DropHeaderBackground",sizeX,sizeY,"Sliced")
                    self._mainWindow:AddImage(bookX, bookY + 15,tostring(GetTemplateIconId("lore_tomebook")),sizeX,sizeX,"Object", nil, self._shelfData[index].Hue or 0)
                    self._mainWindow:AddButton(bookX, bookY, "TomebookClick|"..tostring(index), "", sizeX, sizeY, tomebookTooltip, "", false, "Invisible")
                    --self._mainWindow:AddLabel(bookX + 30, bookY + 60, "[FAEEA4]"..buttonText.."[-]",sizeX,sizeX,20,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
                    self._mainWindow:AddLabel(bookX + 23, bookY + 33, "[FAEEA4]"..(self._shelfData[index].TomeIndex and tostring(self._shelfData[index].TomeIndex) or "").."[-]",sizeX,sizeY,20,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
                    
                    if isOwner then
                        self._mainWindow:AddLabel(bookX + 25, bookY + 78, "[FAEEA4]x[-]",sizeX,sizeY * 0.5,20,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
                        self._mainWindow:AddButton(bookX, bookY + 80, "TomebookRemove|"..tostring(index), "", sizeX, 30, "Remove", "", false, "Invisible")
                    end
                else
                    self._mainWindow:AddImage(bookX, bookY,"DropHeaderBackground",sizeX,sizeY,"Sliced")
                    self._mainWindow:AddButton(bookX, bookY, "DropTomebook|"..tostring(index), "", sizeX, sizeY, "Shelf Position: "..tostring(index).."\nAdd Tomebooks to be read here.", "", false, "Invisible")
                end
            end
        end
        if isOwner then
            -- causes frame stall on full bookshelf - disabled for now
            --self._mainWindow:AddButton(windowWidth - 155, windowHeight - 85, "RemoveAll", "", 115, 30, "Returns all Tomebooks to your backpack.", "", false, "")
            --self._mainWindow:AddLabel(windowWidth - 56, windowHeight - 79, "Remove All",150,sizeY * 0.5,20,"right",false,false,"PermianSlabSerif_Dynamic_Bold")
        end

        self.ParentObj:OpenDynamicWindow(self._mainWindow)
    end,

    HandleBookshelfResponse = function(self, root, user, id)
        -- Make sure we are the right user and we didn't just close the window!
        if(id == nil or id == "" or self.ParentObj ~= user) then 
            --TransferContainerContents( self._enchantPouch, self.ParentObj:GetEquippedObject("Backpack") )
            EndMobileEffect(root)
            return
        end

        if not self.IsWithinRange(self, root) then return false end

        local args = StringSplit(id, "|")
        local id = args[1]

        if id == "DropTomebook" then
            --DebugMessage("Dropped Tomebook")

            local carriedObject = user:CarriedObject()
            self._shelfIndex = tonumber(args[2])

            if not carriedObject then
                user:SystemMessage("Select a Tomebook to display.", "info")
                user:RequestClientTargetGameObj(this, "pickTomebook")
                return
            end

            self.AddToBookshelf(self, root, carriedObject, user)
            
        elseif id == "TomebookClick" then
            local shelfIndex = tonumber(args[2])
            user:SendMessage("StartMobileEffect", "Tomebook", nil, {TomeId = self._shelfData[shelfIndex].TomeId, Entries = self._shelfData[shelfIndex].Entries})
        elseif id == "TomebookRemove" then
            local shelfIndex = tonumber(args[2])
            --DebugMessage("Clicked Tomebook "..tostring(self._isTakingTomebook))
            if not user:HasTimer("TakingTomebook") then
                user:ScheduleTimerDelay(TimeSpan.FromSeconds(0.8),"TakingTomebook")

                Lore.TakeTomebookFromBookshelf(self.ParentObj, self._shelfData[shelfIndex], function()
                    self._shelfData[shelfIndex] = {ShelfIndex = shelfIndex, Hue = 0}
                    self._bookshelf:SetObjVar("ShelfData", self._shelfData)
                    self.GenerateBookshelfWindow(self, root)
                    self._bookshelf:SendMessage("RebuildShelf", self._shelfData)
                end)
            else
                user:SystemMessage("You must wait before you can do that.","info")
            end
        elseif id == "RemoveAll" then
            if not user:HasTimer("TakingTomebook") then
                user:ScheduleTimerDelay(TimeSpan.FromSeconds(0.8),"TakingTomebook")

                local division = math.round(#self._shelfData * 0.25)
                local count = 0

                for i=1, #self._shelfData do
                    if self._shelfData[i].TomeId then
                        --DebugMessage("WOULD REMOVE:",i,self._shelfData[i].TomeIndex, type(self._shelfData[i].TomeIndex))

                        local s = tostring(count).."/"..tostring(division)

                        -- TO DO: this is not working as intended, I want to process these books over multiple frames
                        if count > division then
                            OnNextFrame(function()
                                Lore.TakeTomebookFromBookshelf(self.ParentObj, self._shelfData[i], function()
                                    self._shelfData[i] = {ShelfIndex = i, Hue = 0}
                                    self._bookshelf:SetObjVar("ShelfData", self._shelfData)
                                    self.GenerateBookshelfWindow(self, root)
                                    self._bookshelf:SendMessage("RebuildShelf", self._shelfData)
                                    DebugMessage("removed book at",i,s)
                                end)
                            end)
                            count = count + 1
                            DebugMessage("WAIT A FRAME at",i,s)
                            count = 0
                        else
                            Lore.TakeTomebookFromBookshelf(self.ParentObj, self._shelfData[i], function()
                                self._shelfData[i] = {ShelfIndex = i, Hue = 0}
                                self._bookshelf:SetObjVar("ShelfData", self._shelfData)
                                self.GenerateBookshelfWindow(self, root)
                                self._bookshelf:SendMessage("RebuildShelf", self._shelfData)
                                DebugMessage("removed book at",i,s)
                            end)
                            count = count + 1
                        end

                        
                    else
                        --DebugMessage("skip:",i,self._shelfData[i].TomeIndex, type(self._shelfData[i].TomeIndex))
                    end
                end
                DebugMessage("removed all books")
            else
                user:SystemMessage("You must wait before you can do that.","info")
            end
        end
    end,

    AddToBookshelf = function(self, root, targetObj, user)
        if self.ParentObj ~= user then 
            EndMobileEffect(root)
            return
        end
        if not targetObj then self._shelfIndex = nil return end
        if targetObj:GetCreationTemplateId() ~= "lore_tomebook" then self.ParentObj:SystemMessage("That item is not a Tomebook.", "info") return end
        local shelfIndex = self._shelfIndex
        self._shelfIndex = nil
        --DebugMessage("AddToBookshelf",targetObj,user,shelfIndex)

        local tomeId = targetObj:GetObjVar("TomeId")
        local collectionIndex = targetObj:GetObjVar("CollectionIndex")
        local tomeIndex = targetObj:GetObjVar("TomeIndex")
        local entries = targetObj:GetObjVar("Entries")
        local transcribedBy = targetObj:GetObjVar("TranscribedBy")

        if (tomeId and collectionIndex and tomeIndex and shelfIndex) then
            --self._shelfData[shelfIndex].TomebookObj = targetObj
            self._shelfData[shelfIndex].TomeId = tomeId
            self._shelfData[shelfIndex].CollectionIndex = collectionIndex
            self._shelfData[shelfIndex].TomeIndex = tomeIndex
            self._shelfData[shelfIndex].Entries = entries
            self._shelfData[shelfIndex].Hue = AllTomeCollections[collectionIndex].BookHue
            self._shelfData[shelfIndex].ShelfIndex = shelfIndex
            self._shelfData[shelfIndex].TranscribedBy = transcribedBy
        else
            self._shelfData[shelfIndex].TomeId = "EmptyTomebook"
            self._shelfData[shelfIndex].TomeIndex = 0
            self._shelfData[shelfIndex].Hue = 0
        end

        self._bookshelf:SetObjVar("ShelfData", self._shelfData)
        self.GenerateBookshelfWindow(self, root)
        targetObj:Destroy()
        self._bookshelf:SendMessage("RebuildShelf", self._shelfData)
        --DebugMessage("shelf slot",shelfIndex," is now ",targetObj,"Hue:",self._shelfData[shelfIndex].Hue)
    end,

    _mainWindow = nil,
    _shelfData = nil,
    _bookshelf = nil,
}