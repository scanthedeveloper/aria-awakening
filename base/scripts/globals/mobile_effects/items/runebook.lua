MobileEffectLibrary.Runebook = 
{
    OnEnterState = function(self,root,target,args)       
        if not( self.ParentObj:HasModule("runebook_window") ) then
            self.ParentObj:AddModule("runebook_window",{BookTarget=target})
        else
            self.ParentObj:SendMessage("Init",target)
        end

        EndMobileEffect(root)
    end
}