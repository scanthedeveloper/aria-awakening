--SCAN ADDED
MobileEffectLibrary.PowerHourBuff = 
{
    PersistDeath = true,
    PersistSession = true,

    OnEnterState = function(self,root,target,args)
        self.Duration = args.Duration or self.Duration
        self.Amount = args.Amount or self.Amount
        self.ParentObj:PlayEffectWithArgs("CastFire", 5.0)
        self.ParentObj:PlayEffectWithArgs("FireballEffect", 5.0)
        self.ParentObj:PlayAnimation("dance_wave")
        self.ParentObj:SystemMessage("You suddenly become more skillful..", "info")
        AddBuffIcon(self.ParentObj, "PowerHourBuff","Power Hour","backpack","+"..self.Amount.."00% Skill Gain Chance", false)
        SetMobileMod(self.ParentObj, "PowerHourBuffPlus", "PowerHourBuff", self.Amount)

        ProgressBar.Show(
            {
                TargetUser = self.ParentObj,
                Label = "Skill Increase Activated",
                Duration = self.Duration,
                PresetLocation = "AboveHotbar",
                DialogId = "PowerHour",
            })

    end,

    OnExitState = function(self,root,target,args)
        self.ParentObj:PlayEffectWithArgs("BardDistortionOrange", 5.0)
        self.ParentObj:PlayEffectWithArgs("BardDistortionRed", 5.0)
        self.ParentObj:PlayEffectWithArgs("BardDistortionViolet", 5.0)
        self.ParentObj:PlayAnimation("bashful")
        self.ParentObj:SystemMessage("Your skill potion has worn off...", "info")
        RemoveBuffIcon(self.ParentObj, "PowerHourBuff")
        SetMobileMod(this, "PowerHourBuffPlus", "PowerHourBuff", nil)
    end,

    GetPulseFrequency = function(self,root)
		return self.Duration
	end,

    AiPulse = function(self,root)
        EndMobileEffect(root)
	end,

    Duration = TimeSpan.FromMinutes(1),
    Amount = 1,
}