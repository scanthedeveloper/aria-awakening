
require 'globals.mobile_effects.spells.spell_poison'
require 'globals.mobile_effects.spells.spell_ward_evil'
require 'globals.mobile_effects.spells.spell_ward_hidden'
require 'globals.mobile_effects.spells.summon_mount'

--SCAN ADDED
-- Nature Magic
require 'globals.mobile_effects.spells.nature_magic.spirit_of_the_wolf'
require 'globals.mobile_effects.spells.nature_magic.spirit_of_the_bear'
require 'globals.mobile_effects.spells.nature_magic.rejuvenation'
require 'globals.mobile_effects.spells.nature_magic.natures_touch'
require 'globals.mobile_effects.spells.nature_magic.barkskin'
require 'globals.mobile_effects.spells.nature_magic.venomous_affliction'
require 'globals.mobile_effects.spells.nature_magic.arrow_vines'

-- Pyromancy
require 'globals.mobile_effects.spells.pyromancy.innerfire'
require 'globals.mobile_effects.spells.pyromancy.ignite'
require 'globals.mobile_effects.spells.pyromancy.immolate_ignite'
require 'globals.mobile_effects.spells.pyromancy.imbuefire'
require 'globals.mobile_effects.spells.pyromancy.summon_pyro_mount'
require 'globals.mobile_effects.spells.pyromancy.summon_mount_bear'

-- Necromancy
require 'globals.mobile_effects.spells.necromancy.spectres_embrace'
require 'globals.mobile_effects.spells.necromancy.vampires_embrace'
require 'globals.mobile_effects.spells.necromancy.necrocurse'
require 'globals.mobile_effects.spells.necromancy.amplify_damage'
require 'globals.mobile_effects.spells.necromancy.exhaustion'
require 'globals.mobile_effects.spells.necromancy.agony'
require 'globals.mobile_effects.spells.necromancy.mass_plague'