-- this is a modified version of the Poison Spell from citadel
MobileEffectLibrary.ImmolationIgnite = 
{
	Debuff = true,
	Resistable = true,

	OnEnterState = function(self,root,target,args)
		-- TARGET REPRESENTS THE PERSON THAT APPLIED THE POISON
		if(target) then SpellCaster = target end
		self.Target = target
		self.PulseFrequency = args.PulseFrequency or self.PulseFrequency
		self.PulseMax = args.PulseMax or self.PulseMax
		self.MinDamage = args.MinDamage or self.MinDamage
		self.MaxDamage = args.MaxDamage or self.MaxDamage
		SetMobileMod(self.ParentObj, "HealingReceivedTimes", "Poison", -0.50)
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "PoisonDebuff", "Poisoned", "Ignite", self.MinDamage.."-"..self.MaxDamage.." damage every "..self.PulseFrequency.Seconds.." seconds." .. "\nReduced healing received.", true)
			ShowStatusElement(self.ParentObj,{IsSelf=true, HealthHue="42C92D"})
		end

		self.ParentObj:PlayEffect("FireFliesEffect")
		--self.ParentObj:PlayEffect("StatusEffectBlood")
		self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_cast_fire",false)

		if ( HasHumanAnimations(self.ParentObj) ) then
			self.ParentObj:PlayAnimation("sunder")
		else
			-- impale is the "heavy attack for mobs"
			self.ParentObj:PlayAnimation("impale")
		end

		-- manually advance conflict since the damageType Poison doesn't call guards.
		AdvanceConflictRelation(target, self.ParentObj)
	end,

	OnExitState = function(self,root)
		SetMobileMod(self.ParentObj, "HealingReceivedTimes", "Poison", nil)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "PoisonDebuff")
			ShowStatusElement(self.ParentObj,{IsSelf=true, HealthHue="FF0000"})
		end
        self.ParentObj:StopEffect("FireFliesEffect")
		--self.ParentObj:StopEffect("StatusEffectBlood")
		if(SpellCaster) then 
			--SpellCaster:SystemMessage("your pyro-ignition has faded...") 
		end
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

	AiPulse = function(self,root)
		self.CurrentPulse = self.CurrentPulse + 1
		if ( IsDead(self.ParentObj) or self.CurrentPulse > self.PulseMax ) then
			EndMobileEffect(root)
		else
			self.ParentObj:PlayEffect("FireballEffect",2.5)
			self.ParentObj:PlayEffect("BuffEffect_B",2)
			self.ParentObj:SendMessage("ProcessTypeDamage", self.Target, math.random(self.MinDamage, self.MaxDamage), "Poison")
		end
	end,

	PulseFrequency = TimeSpan.FromSeconds(1),
	PulseMax = 5,
	CurrentPulse = 0,
	MinDamage = 1,
	MaxDamage = 1,
	SpellCaster = nil,
}