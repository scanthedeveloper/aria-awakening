MobileEffectLibrary.Innerfire = 
{
	PersistSession = true,

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Amount = args.Amount or self.Amount
		--target is caster of spell
		local bonus = math.floor(GetSkillLevel(target,"PyromancyMagicSkill")/1) or 1
		if(bonus < 10) then bonus = 30 end
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "Innerfire", "Inner Fire", "Ignite", "Total Mana increased by "..bonus)
		end
        SetMobileMod(self.ParentObj, "MaxManaPlus", "Innerfire", bonus)
        self.ParentObj:PlayEffect("FireHeadEffect", 1800.0)
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "Innerfire")
		end
        SetMobileMod(self.ParentObj, "MaxManaPlus", "Innerfire", nil)
        this:StopEffect("FireHeadEffect")
	end,

	OnStack = function(self,root,target,args)
		-- refresh duration
		root.ParentObj:ScheduleTimerDelay(self.Duration, root.PulseId.."-"..root.CurStateName)
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromMinutes(30),
	Amount = .2,
}