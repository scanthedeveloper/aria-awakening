-- this is a modified version of the Poison Spell from citadel
MobileEffectLibrary.Ignite = 
{
	Debuff = true,
	Resistable = true,

	OnEnterState = function(self,root,target,args)
		-- TARGET REPRESENTS THE PERSON THAT APPLIED THE POISON
		if(target) then SpellCaster = target end
		self.Target = target
		self.PulseFrequency = args.PulseFrequency or self.PulseFrequency
		self.PulseMax = args.PulseMax or self.PulseMax
		self.MinDamage = args.MinDamage or self.MinDamage
		self.MaxDamage = args.MaxDamage or self.MaxDamage
		SetMobileMod(self.ParentObj, "HealingReceivedTimes", "Poison", -0.20)
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "PoisonDebuff", "You are on fire!", "Ignite", self.MinDamage.."-"..self.MaxDamage.." damage every "..self.PulseFrequency.Seconds.." seconds." .. "\nReduced healing received by 20%." .. "\n[FF5555]Cannot be cured[-]", true)
			ShowStatusElement(self.ParentObj,{IsSelf=true, HealthHue="FF9999"})
		end

		self.Target:NpcSpeech("[00FF00]Flaming Arrow[-]","combat")
		self.ParentObj:NpcSpeech("[FF7777]Flaming Arrow Hit[-]","combat")
		self.ParentObj:PlayEffect("IgnitedEffect")
		self.ParentObj:PlayEffect("FlameAuraEffect")
		self.ParentObj:PlayEffect("FireballExplosionEffect")
		--self.ParentObj:PlayEffect("StatusEffectBlood")
		self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_cast_fire",false)

		-- manually advance conflict since the damageType Poison doesn't call guards.
		AdvanceConflictRelation(target, self.ParentObj)
	end,

	OnExitState = function(self,root)
		SetMobileMod(self.ParentObj, "HealingReceivedTimes", "Poison", nil)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "PoisonDebuff")
			ShowStatusElement(self.ParentObj,{IsSelf=true, HealthHue="FF0000"})
		end
        self.ParentObj:StopEffect("IgnitedEffect")
		self.ParentObj:StopEffect("FlameAuraEffect")
		--self.ParentObj:StopEffect("StatusEffectBlood")
		if(SpellCaster) then 
			--SpellCaster:SystemMessage("your pyro-ignition has faded...") 
		end
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

	AiPulse = function(self,root)
		self.CurrentPulse = self.CurrentPulse + 1
		if ( IsDead(self.ParentObj) or self.CurrentPulse > self.PulseMax ) then
			EndMobileEffect(root)
		else
			self.ParentObj:PlayEffect("FireballExplosionEffect")
			self.ParentObj:SendMessage("ProcessTypeDamage", self.Target, math.random(self.MinDamage, self.MaxDamage), "Poison")
		end
	end,

	PulseFrequency = TimeSpan.FromSeconds(4),
	PulseMax = 15,
	CurrentPulse = 0,
	MinDamage = 1,
	MaxDamage = 5,
	SpellCaster = nil,
}