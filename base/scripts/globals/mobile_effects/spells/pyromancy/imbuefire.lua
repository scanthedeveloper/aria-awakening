MobileEffectLibrary.Imbuefire = 
{
	PersistSession = true,

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Amount = args.Amount or self.Amount
		--target is caster of spell
		local bonus = math.floor(GetSkillLevel(target,"PyromancyMagicSkill")/10) or 1
		if(bonus < 5) then bonus = 5 end
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "Imbuefire", "Imbue Fire", "Fire Shield", "Spell Block % Increased by "..bonus)
		end
        SetMobileMod(self.ParentObj, "BlockSpellPlus", "Imbuefire", bonus)
        self.ParentObj:PlayEffect("FireShield", 10.0)
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "Imbuefire")
		end
		local equippedWeapon = this:GetEquippedObject("RightHand")
        SetMobileMod(self.ParentObj, "BlockSpellPlus", "Imbuefire", nil)
        this:StopEffect("FireShield")
	end,

	OnStack = function(self,root,target,args)
		-- refresh duration
		root.ParentObj:ScheduleTimerDelay(self.Duration, root.PulseId.."-"..root.CurStateName)
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromMinutes(30),
	Amount = .2,
}