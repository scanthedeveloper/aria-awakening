MobileEffectLibrary.Barkskin = 
{
	PersistSession = true,

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Amount = args.Amount or self.Amount
		--target is caster of spell
		local bonus = math.floor(GetSkillLevel(target,"NatureMagicSkill")/10) or 1
		if(bonus < 1) then bonus = 5 end
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "Barkskin", "Barkskin", "Elemental Shield", "Defense increased by "..bonus)
		end
		SetMobileMod(self.ParentObj, "DefensePlus", "Barkskin", bonus)
		self.ParentObj:PlayEffect("VanguardEffect", 15.0)
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "Barkskin")
		end
		SetMobileMod(self.ParentObj, "DefensePlus", "Barkskin", nil)
		self.ParentObj:EndEffect("VanguardEffect")
	end,

	OnStack = function(self,root,target,args)
		-- refresh duration
		root.ParentObj:ScheduleTimerDelay(self.Duration, root.PulseId.."-"..root.CurStateName)
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromMinutes(30),
	Amount = .2,
}