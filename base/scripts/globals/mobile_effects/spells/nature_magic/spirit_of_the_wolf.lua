MobileEffectLibrary.SpiritOfTheWolf =  --Healing Winds
{
	PersistSession = true,

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Amount = args.Amount or self.Amount
		-- target is caster
		local bonus = math.floor(GetSkillLevel(target,"NatureMagicSkill")/5) or 1
		if(bonus < 1) then bonus = 10 end
		if ( self.ParentObj:IsPlayer() ) then
			target:SystemMessage("[FFFF00]you feel the healing winds of nature embrace you...")
			AddBuffIcon(self.ParentObj, "SpiritOfTheWolf", "Healing Winds", "Gust of Wind", "Healing increased by "..bonus)
		end
		SetMobileMod(self.ParentObj, "HealingReceivedPlus", "SpiritOfTheWolf", bonus)
		self.ParentObj:PlayEffect("YellowPortalEffect", 5.0)
		self.ParentObj:PlayEffect("WindEffect", 5.0)
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "SpiritOfTheWolf")
		end
		SetMobileMod(self.ParentObj, "HealingReceivedPlus", "SpiritOfTheWolf", nil)
		this:StopEffect("YellowPortalEffect")
		this:StopEffect("WindEffect")
	end,

	OnStack = function(self,root,target,args)
		-- refresh duration
		root.ParentObj:ScheduleTimerDelay(self.Duration, root.PulseId.."-"..root.CurStateName)
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromMinutes(30),
	Amount = .2,
}