MobileEffectLibrary.NaturesTouch = 
{
	PersistSession = true,

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Amount = args.Amount or self.Amount
		local bonus = math.floor(GetSkillLevel(target,"NatureMagicSkill")/200) or 1

		if(bonus < 1) then 
			bonus = 1 
		end

		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "NaturesTouch", "Nature's Touch", "Natures Grace", "Increases chances to forage plants. ")
			--SetMobileMod(self.ParentObj, "BonusPlantHarvesting", "NaturesTouch", bonus)
			
			self.ParentObj:SetObjVar("SpellBuffData.NaturesTouch",bonus)
			self.ParentObj:PlayEffect("BardAbsorbThewEffect", 15.0)
			--self.ParentObj:SystemMessage("Your Nature's Touch bonus is: " ..bonus)
			--self.ParentObj:SystemMessage("Your Nature's Touch bonus is: " ..bonus, "info")

		else
			target:SystemMessage("your spell didn't take...")
			EndMobileEffect(root)
		end
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "NaturesTouch")
			--SetMobileMod(self.ParentObj, "BonusPlantHarvesting", "NaturesTouch", nil)
			self.ParentObj:DelObjVar("SpellBuffData.NaturesTouch")
		end
	end,

	OnStack = function(self,root,target,args)
		-- refresh duration
		root.ParentObj:ScheduleTimerDelay(self.Duration, root.PulseId.."-"..root.CurStateName)
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromMinutes(30),
	Amount = .2,
}