MobileEffectLibrary.SpiritOfTheBear = 
{
	PersistSession = true,

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Amount = args.Amount or self.Amount
		--target is caster of spell
		local bonus = math.floor(GetSkillLevel(target,"NatureMagicSkill")/10) or 1
		if(bonus < 1) then bonus = 5 end
		if ( self.ParentObj:IsPlayer() ) then
			target:SystemMessage("[FFFF00]you feel the spirit of the bear embrace you...")
			AddBuffIcon(self.ParentObj, "SpiritOfTheBear", "Spirit of the Bear", "Summon Spirit Bear", "Strength increased by "..bonus)
		end
		SetMobileMod(self.ParentObj, "StrengthPlus", "SpiritOfTheBear", bonus)
		self.ParentObj:PlayEffect("WarriorBloodlustDefensive", 5.0)
		self.ParentObj:PlayEffect("WarriorEnrage", 10.0)
		self.ParentObj:PlayAnimation("showingOff")
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "SpiritOfTheBear")
		end
		SetMobileMod(self.ParentObj, "StrengthPlus", "SpiritOfTheBear", nil)
	end,

	OnStack = function(self,root,target,args)
		-- refresh duration
		root.ParentObj:ScheduleTimerDelay(self.Duration, root.PulseId.."-"..root.CurStateName)
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromMinutes(30),
	Amount = .2,
}