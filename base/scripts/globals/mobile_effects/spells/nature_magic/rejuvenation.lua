MobileEffectLibrary.Rejuvenation = 
{
	OnEnterState = function(self,root,target,args)
		self.Healer = target
		self.PulseFrequency = args.PulseFrequency or self.PulseFrequency
		self.PulseMax = args.PulseMax or self.PulseMax
		self.SkillBonus = GetSkillLevel(target,"NatureMagicSkill")/200 or 0
		local healAmount = math.ceil(GetMaxHealth(self.ParentObj) * 0.04)
		self.HealAmount = healAmount
		if(healAmount < 1) then healAmount = 1 end
		if (target:IsPlayer()) then
			AddBuffIcon(self.ParentObj, "Rejuvenation", "Rejuvenation", "Regrowth", "heal " .. math.floor(healAmount * (1 + self.SkillBonus)) .. " every 1 seconds", true,TimeSpan.FromSeconds(self.BuffDuration))
		end
	end,

	OnExitState = function(self,root)

	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,


	AiPulse = function(self,root)	
		self.CurrentPulse = self.CurrentPulse + 1
		if ( self.CurrentPulse > self.PulseMax ) then
			EndMobileEffect(root)
		else
			if(self.ParentObj:IsPlayer()) then
				self.ParentObj:SendMessage("HealRequest", math.floor(self.HealAmount * (self.PlayerBonus + self.SkillBonus)) , self.Healer)
				self.ParentObj:PlayEffect("PrimedWater2",1.5)
				--self.ParentObj:PlayEffect("MagicRingExplosionEffect",1.0)
			else
			if(self.ParentObj:GetMobileType() == "Friendly") then
					self.ParentObj:SendMessage("HealRequest", math.floor(self.HealAmount * (self.FriendlyBonus + self.SkillBonus)) , self.Healer)
					self.ParentObj:PlayEffect("PrimedWater2",1.5)
					--self.ParentObj:PlayEffect("MagicRingExplosionEffect",1.0)
				end
			end
		end
	end,

	BuffDuration = 10, -- seconds
	PulseFrequency = TimeSpan.FromSeconds(1),
	PulseMax = 10,
	CurrentPulse = 0,
	HealAmount = 0,
	Healer = nil,
	PlayerBonus = 0,
	FriendlyBonus = 0,
	SkillBonus = 0,
}