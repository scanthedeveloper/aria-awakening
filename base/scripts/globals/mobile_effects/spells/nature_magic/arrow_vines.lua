-- this is a modified version of the Poison Spell from citadel
MobileEffectLibrary.ArrowVines = 
{
	Debuff = true,
	Resistable = true,

	OnEnterState = function(self,root,target,args)
		-- TARGET REPRESENTS THE PERSON THAT APPLIED THE POISON
		if(target) then SpellCaster = target end
		self.Target = target
		self.PulseFrequency = args.PulseFrequency or self.PulseFrequency
		self.PulseMax = args.PulseMax or self.PulseMax
		self.MinDamage = args.MinDamage or self.MinDamage
		self.MaxDamage = args.MaxDamage or self.MaxDamage
		SetMobileMod(self.ParentObj, "MoveSpeedPlus", "Rooted", -0.70)
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "ArrowVines", "You are rooted!", "Arrow Vines", self.MinDamage.."-"..self.MaxDamage.." damage every "..self.PulseFrequency.Seconds.." seconds." .. "\nReduced movement speed by 70%.", true)
			ShowStatusElement(self.ParentObj,{IsSelf=true, HealthHue="B99438"})
		end

		mobile:PlayEffect("RootedSpellEffect",3)
        mobile:PlayEffect("CastEarth2",3)
		--self.ParentObj:PlayEffect("StatusEffectPoison")
		self.ParentObj:PlayObjectSound("event:/magic/air/magic_air_lightning_impact",false)

		-- manually advance conflict since the damageType Poison doesn't call guards.
		AdvanceConflictRelation(target, self.ParentObj)
	end,

	OnExitState = function(self,root)
		SetMobileMod(self.ParentObj, "MoveSpeedPlus", "Rooted", nil)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "ArrowVines")
			ShowStatusElement(self.ParentObj,{IsSelf=true, HealthHue="FF0000"})
		end
		--self.ParentObj:StopEffect("DestructionEffect")
		--self.ParentObj:StopEffect("StatusEffectPoison")
		--if(SpellCaster) then 
			--SpellCaster:SystemMessage("your insect swarm has dissipated...") 
		--end
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

	AiPulse = function(self,root)
		self.CurrentPulse = self.CurrentPulse + 1
		if ( IsDead(self.ParentObj) or self.CurrentPulse > self.PulseMax ) then
			EndMobileEffect(root)
		else
			self.ParentObj:SendMessage("ProcessTypeDamage", self.Target, math.random(self.MinDamage, self.MaxDamage), "Poison")
		end
	end,

	PulseFrequency = TimeSpan.FromSeconds(1),
	PulseMax = 5,
	CurrentPulse = 0,
	MinDamage = 3,
	MaxDamage = 5,
	SpellCaster = nil,
}