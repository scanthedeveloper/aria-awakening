MobileEffectLibrary.VampiresEmbrace = 
{
	-- this makes the mobile effect last through logging out then back in
	PersistSession = true,

	-- when the self.ParentObj gets this mobile effect applied we want
	-- to set up the initial buffs or mobile mods or what ever effect
	-- happens when we first enter this state
	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Amount = args.Amount or self.Amount
		--target is caster of spell
		local bonus = math.floor(GetSkillLevel(target,"DemonologySkill")/80) or 1
		if(bonus < 0.1) then bonus = 0.3 end
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "VampiresEmbrace", "Unholy Regeneration", "Raise Undead", "Health regeneration increased by "..bonus)
		end
		SetMobileMod(self.ParentObj, "HealthRegenPlus", "VampiresEmbrace", bonus)
		self.ParentObj:PlayEffect("PrimedVoid", 1800.0)
	end,

	-- when the self.ParentObj goes to exit this mobile effect state we want
	-- to remove any buffs or mobile mods attached to the self.ParentObj
	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "VampiresEmbrace")
		end
		SetMobileMod(self.ParentObj, "HealthRegenPlus", "VampiresEmbrace", nil)
		self.ParentObj:StopEffect("PrimedVoid")
	end,

	-- when the self.ParentObj gets called and if you want to add any refreshments
	-- to durations or things of that sort you can do so when this mobile effect
	-- gets applied a 2nd time
	OnStack = function(self,root,target,args)
		-- refresh duration
		root.ParentObj:ScheduleTimerDelay(self.Duration, root.PulseId.."-"..root.CurStateName)
	end,

	-- when the self.parentOjb calls the self.OnEnterState the internal state machine
	-- will set the pulse frequency in which to end this mobile effect. this function
	-- will return the TimeSpan duration in which to call the self.OnExitState
	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	-- when the self.ParentObj calls the self.AiPulse, this happens when the timer
	-- pulses so here is a good place to call our EndMobileEffect function and will
	-- fire off our self.OnExitState 
	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromMinutes(30),
	Amount = .2,
}