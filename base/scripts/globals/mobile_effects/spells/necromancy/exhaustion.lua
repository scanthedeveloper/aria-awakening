MobileEffectLibrary.Exhaustion = 
{
	-- this makes the mobile effect last through logging out then back in
	PersistSession = true,

	-- when the self.ParentObj gets this mobile effect applied we want
	-- to set up the initial buffs or mobile mods or what ever effect
	-- happens when we first enter this state
	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Amount = args.Amount or self.Amount
		--target is caster of spell
		local bonus = math.ceil(GetSkillLevel(target,"DemonologySkill")/10)
		if(bonus < 1) then bonus = 10 end
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "Exhaustion", "Exhaustion", "stealth", "You feel weak and tired")
		end
		SetMobileMod(self.ParentObj, "MoveSpeedTimes", "Exhaustion", -0.50)
		SetMobileMod(self.ParentObj, "StaminaRegenPlus", "Exhaustion",-math.ceil(bonus/2))
		self.ParentObj:SendMessage("ProcessTrueDamage", self.ParentObj, math.random(10,25), true) 
		self.ParentObj:PlayEffect("CastEarth", 30.0)
	end,

	-- when the self.ParentObj goes to exit this mobile effect state we want
	-- to remove any buffs or mobile mods attached to the self.ParentObj
	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "Exhaustion")
		end
		SetMobileMod(self.ParentObj, "MoveSpeedTimes", "Exhaustion",nil)
		SetMobileMod(self.ParentObj, "StaminaRegenPlus", "Exhaustion",nil)
		self.ParentObj:StopEffect("CastEarth")
	end,

	-- when the self.ParentObj gets called and if you want to add any refreshments
	-- to durations or things of that sort you can do so when this mobile effect
	-- gets applied a 2nd time
	OnStack = function(self,root,target,args)
		-- refresh duration
		root.ParentObj:ScheduleTimerDelay(self.Duration, root.PulseId.."-"..root.CurStateName)
		self.ParentObj:PlayEffect("CastEarth", 30.0)
	end,

	-- when the self.parentOjb calls the self.OnEnterState the internal state machine
	-- will set the pulse frequency in which to end this mobile effect. this function
	-- will return the TimeSpan duration in which to call the self.OnExitState
	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromSeconds(30),
	Amount = .2,
}