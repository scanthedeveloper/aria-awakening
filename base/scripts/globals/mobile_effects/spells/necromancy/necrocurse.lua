MobileEffectLibrary.NecroCurse = 
{
	-- this makes the mobile effect last through logging out then back in
	PersistSession = true,

	-- when the self.ParentObj gets this mobile effect applied we want
	-- to set up the initial buffs or mobile mods or what ever effect
	-- happens when we first enter this state
	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Amount = args.Amount or self.Amount
		--target is caster of spell
		--local bonus = math.ceil(GetSkillLevel(target,"NecromancySkill")/10)
		local bonus = 10
		if(bonus < 1) then bonus = 1 end
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "Curse", "Curse", "cloak", "you are cursed, -".. bonus .. " to strength, agility, wisdom, will and intelligence")
		end
		SetMobileMod(self.ParentObj, "StrengthPlus", "Curse", -bonus)
		SetMobileMod(self.ParentObj, "AgilityPlus", "Curse", -bonus)
		SetMobileMod(self.ParentObj, "IntelligencePlus", "Curse", -bonus)
		SetMobileMod(self.ParentObj, "WillPlus", "Curse", -bonus)
		SetMobileMod(self.ParentObj, "WisdomPlus", "Curse", -bonus)
		self.ParentObj:SendMessage("ProcessTrueDamage", self.ParentObj, math.random(100,250), true) 
		self.ParentObj:PlayEffect("GrimAuraDebuffEffect", 120.0)
	end,

	-- when the self.ParentObj goes to exit this mobile effect state we want
	-- to remove any buffs or mobile mods attached to the self.ParentObj
	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "Curse")
		end
		SetMobileMod(self.ParentObj, "StrengthPlus", "Curse", nil)
		SetMobileMod(self.ParentObj, "AgilityPlus", "Curse", nil)
		SetMobileMod(self.ParentObj, "IntelligencePlus", "Curse", nil)
		SetMobileMod(self.ParentObj, "WillPlus", "Curse", nil)
		SetMobileMod(self.ParentObj, "WisdomPlus", "Curse", nil)
	end,

	-- when the self.ParentObj gets called and if you want to add any refreshments
	-- to durations or things of that sort you can do so when this mobile effect
	-- gets applied a 2nd time
	OnStack = function(self,root,target,args)
		-- refresh duration
		root.ParentObj:ScheduleTimerDelay(self.Duration, root.PulseId.."-"..root.CurStateName)
		self.ParentObj:PlayEffect("GrimAuraDebuffEffect", 120.0)
	end,

	-- when the self.parentOjb calls the self.OnEnterState the internal state machine
	-- will set the pulse frequency in which to end this mobile effect. this function
	-- will return the TimeSpan duration in which to call the self.OnExitState
	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromMinutes(1),
	Amount = .2,
}