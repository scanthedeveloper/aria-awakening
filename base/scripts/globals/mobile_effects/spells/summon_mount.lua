ethereal_template = "ethereal_horse"

MobileEffectLibrary.SummonMount = 
{
    OnEnterState = function(self,root,target,args)
        if not( self.ValidTarget(self.ParentObj,target) ) then
            EndMobileEffect(root)
            return false
        end
        
        local mountLoc = target:GetLoc()
        Create.AtLoc(ethereal_template, mountLoc, function(mobile)
            if ( mobile ) then
                PlayEffectAtLoc("CloakEffect", mountLoc, 0.5)
                MountMobile(target, mobile)    
                mobile:SetObjVar("Summoned",true)
            end

            EndMobileEffect(root)
        end, true)
    end,
    
    -- this is used for validating the spell cast as well (to avoid resource consumption)
    ValidTarget = function(mobile,target)
        if ( not target or not target:IsValid() ) then
            mobile:SystemMessage("Invalid target.", "info")
            return false
        end
        
        if not( target:IsPlayer() ) then
            mobile:SystemMessage("That is not a valid target.", "info")
            return false
        end
        if ( IsInCombat(target) ) then
            mobile:SystemMessage("Can not summon mount onto someone who is currently in combat.", "info")
            return false
        end
        if ( IsDead(target) ) then
            mobile:SystemMessage("Your target is dead!", "info")
            return false
        end
        if ( IsMounted(target) ) then
            mobile:SystemMessage("Target cannot be mounted.", "info")
            return false
        end
        if ( target:IsInRegion("NoMount") ) then
            mobile:SystemMessage("Cannot mount here.", "info")
            return false
        end
        
        return true
    end,
}

SpellData.AllSpells.Etherealize.TargetValidate = MobileEffectLibrary.SummonMount.ValidTarget