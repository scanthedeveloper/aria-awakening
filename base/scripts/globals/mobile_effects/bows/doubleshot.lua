--SCAN ADDED
-- Fires a doubleshot from a bow with the second attack dealing .50% damage.
MobileEffectLibrary.DoubleShot = 
{

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Target = target

		if ( self.Target == nil ) then
			EndMobileEffect(root)
			return
		end

	end,

	FireArrow = function(self)
		self.ParentObj:PlayEffect("BuffEffect_H")
		self.ParentObj:SendMessage("ExecuteRangedWeaponAttack", self.Target, "RightHand")
	end,

	OnExitState = function(self,root)
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		-- fire the second arrow
		self.ParentObj:NpcSpeech("[00FF00]+Double Shot[-]","combat")
		self.Target:NpcSpeech("[FF7777]Double Shot[-]","combat")
		--SECOND SHOT
		self.FireArrow(self)

		--SPECIAL EFFFECT
		--local damageRNG = math.random(10,30)
		--self.Target:PlayEffect("PrimedFire", 2)
		--self.Target:PlayEffect("FireballExplosionEffect")
		--self.Target:SendMessage("ProcessTrueDamage", self.Target, damageRNG)

		--SPELL
		--self.Target:SendMessage("RequestMagicalAttack", "Bombardment", self.Target, self.ParentObj)
		--self.Target:SendMessage("RequestMagicalAttack", "Fireball", self.Target, self.ParentObj)

		--MOBILE EFFECTe
		--self.Target:SendMessage("StartMobileEffect", "Bleed", self.Target, self.ParentObj{
			--PulseFrequency = TimeSpan.FromSeconds(2),
			--PulseMax = 5,
			--DamageMin = 100,
			--DamageMax = 200,
		--})
		EndMobileEffect(root)
	end,

	Duration = TimeSpan.FromSeconds(.50),
	WeaponDamageModifier = 0.5,
}