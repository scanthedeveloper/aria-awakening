MobileEffectLibrary.AutoFireArrow = 
{
    EndOnAction = true,
    ForceDismount = true,
    IgnoreEndOnActionTypes = {"FireArrow", "AutoFireArrow", "Combat", "Bandage"},

	OnEnterState = function(self,root,target,args)
		self.PulseDuration = args.PulseDuration or 2000
		self.Target = self.ParentObj:GetObjVar("CurrentTarget")
        self._Player = IsPlayerCharacter(self.ParentObj)
        self.WeaponAbility = args.WeaponAbility
        self._Weapon = Weapon.GetPrimary(self.ParentObj)

		-- Make sure we are a player
        if not ( self._Player or self.WeaponAbility == nil ) then
			EndMobileEffect(root)
			return false
        end

        local type = Weapon.GetType(Weapon.GetPrimary( self.ParentObj ))
        local attackSpeed = self.ParentObj:GetStatValue("AttackSpeed")
		local weaponBaseSpeed = Weapon.GetSpeed( type )
		self.PulseDuration = math.floor( self.PulseDuration * ( attackSpeed / weaponBaseSpeed ) )
        
        -- Make sure we have valid target
        self.CheckTargetExists(self, root)
        -- Make sure we have a bow
        self.CheckWeaponIsRanged(self, root)
		self.FireArrow(self,root)
    end,

    OnStack = function(self,root,target,args)
        --DebugMessage("EndedOn: " .. tostring( self._EndOnActionEventType ) )
        
        EndMobileEffect(root)
        return
    end,
    
    GetPulseFrequency = function(self,root)
		return TimeSpan.FromMilliseconds(self.PulseDuration)
	end,

    AiPulse = function(self,root)      

        -- Make sure we have valid target
        self.CheckTargetExists(self, root)
        -- Make sure we have a bow
        self.CheckWeaponIsRanged(self, root)

		self.FireArrow(self,root)

    end,

    CheckTargetExists = function( self, root )
        self.Target = self.ParentObj:GetObjVar("CurrentTarget")
        if ( self.Target == nil ) then
            self.ParentObj:SystemMessage("You need a target.", "info")
            EndMobileEffect(root)
            return false
        end
        return true
    end,

    CheckWeaponIsRanged = function(self,root)
        local equippedRightHand = self.ParentObj:GetEquippedObject("RightHand")
		local weaponType = EquipmentStats.BaseWeaponStats[Weapon.GetType(equippedRightHand)].WeaponClass
		-- Make sure the player has a bow equipped
		if( weaponType ~= "Bow" ) then
			EndMobileEffect(root)
			self.ParentObj:SystemMessage("You need a ranged weapon.", "info")
			return false
		end
    end,
    

    FireArrow = function(self,root)

        if ( self.CheckTargetExists(self,root) and QueueWeaponAbility(self.ParentObj, self.Target, self.WeaponAbility) ) then

            -- instant ability, trigger the cooldown.
            StartWeaponAbilityCooldown(self.ParentObj, true, TimeSpan.FromMilliseconds(self.PulseDuration))

            if not( self.WeaponAbility.NoCombat == true ) then
                -- force combat mode cause they used a weapon ability.
                self.ParentObj:SendMessage("ForceCombat")
            end
        end

        
	end,


}