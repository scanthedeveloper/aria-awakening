MobileEffectLibrary.FireArrow = 
{
	--ForceDismount = true,

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Target = target or self.ParentObj:GetObjVar("CurrentTarget") or nil
		self._Player = IsPlayerCharacter(self.ParentObj)
		self._NotchArrowTime = args.NotchArrowTime or 3000
		self._JustNotch = args.JustNotch or false

		-- Make sure we are a player
        if not ( self._Player ) then
			EndMobileEffect(root)
			return false
		end

		if( self._JustNotch ) then

			StartWeaponAbilityCooldown( self.ParentObj, true, TimeSpan.FromMilliseconds(self._NotchArrowTime) )
			ProgressBar.Show(
			{
				TargetUser = self.ParentObj,
				Label="Notching Arrow",
				DialogId="JustNotchingAnArrow",
				Duration=TimeSpan.FromMilliseconds(self._NotchArrowTime),
				PresetLocation="AboveHotbar",
				Callback = function()
					EndMobileEffect(root)
				end
			})
		else
			local type = Weapon.GetType(Weapon.GetPrimary( self.ParentObj ))
			local attackSpeed = self.ParentObj:GetStatValue("AttackSpeed")
			local weaponBaseSpeed = Weapon.GetSpeed( type )
			self._NotchArrowTime = math.floor( self._NotchArrowTime * ( attackSpeed / weaponBaseSpeed ) )

			if(
				-- Make sure we have valid target
				self.CheckTargetExists(self, root)
				-- Make sure we have a bow
				and self.CheckWeaponIsRanged(self, root)
				-- Check spell casting or primed
				and self.CheckSpellCastOrPrimed(self, root)
			) then
				self.FireArrow(self,root)
			end

		end
		
	end,

	OnStack = function(self,root,target,args)
		ProgressBar.Cancel("JustNotchingAnArrow",self.ParentObj)
		self.OnEnterState(self,root,target,args)
	end,

	OnExitState = function(self,root)
        SetMobileMod(self.ParentObj, "MoveSpeedTimes", "ArcherShooting", nil)
	end,

	CheckSpellCastOrPrimed = function(self, root)
		if( self.ParentObj:HasTimer("SpellPrimeTimer") ) then
			self.ParentObj:SystemMessage("Cannot shoot arrow while casting.", "info")
            EndMobileEffect(root)
            return false
		elseif( self.ParentObj:HasObjVar("PrimedSpell") ) then
			self.ParentObj:SystemMessage("Cannot shoot arrow with spell primed.", "info")
            EndMobileEffect(root)
            return false
		end
		return true
	end,

	CheckTargetExists = function( self, root )
        if ( self.Target == nil ) then
            self.ParentObj:SystemMessage("You need a target.", "info")
            EndMobileEffect(root)
            return false
		end
		return true
    end,

    CheckWeaponIsRanged = function(self,root)
        local equippedRightHand = self.ParentObj:GetEquippedObject("RightHand")
		local weaponType = EquipmentStats.BaseWeaponStats[Weapon.GetType(equippedRightHand)].WeaponClass
		-- Make sure the player has a bow equipped
		if( weaponType ~= "Bow" ) then
			EndMobileEffect(root)
			self.ParentObj:SystemMessage("You need a ranged weapon.", "info")
			return false
		end
		return true
    end,

	FireArrow = function(self,root)
		if not( ShouldCriminalProtect(self.ParentObj, self.Target) ) then
			self.ParentObj:SendMessage("ExecuteRangedWeaponAttack", self.Target, "RightHand")
			SetMobileMod(self.ParentObj, "MoveSpeedTimes", "ArcherShooting", PrestigeData.Barding.SongMovementSpeed) -- 70% reduction
			
			--SCAN ADDED LUMBERJACKING BONUS LOGIC
				-----------------------------------
				--get the players skill level:
				local archerySkill = GetSkillLevel(self.ParentObj,"ArcherySkill")	
				local notchBonus = 0
				--local displayName = GetResourceDisplayName(resourceType)

				--APPRENTICE
				if(archerySkill > 29.99 ) and (archerySkill < 50.0 ) then 
					notchBonus = .25
					self.ParentObj:NpcSpeech("[00FF00] +"..notchBonus.." Apprentice Notch Bonus[-]","combat")
				end
				--JOURNEYMAN
				if(archerySkill > 49.9 ) and (archerySkill < 80.0 ) then 
					notchBonus = .50
					self.ParentObj:NpcSpeech("[00FF00] +"..notchBonus.." Journeyman Notch Bonus[-]","combat")
				end
				--MASTER
				if(archerySkill > 79.9) and (archerySkill < 100.0 ) then 
					notchBonus = .75
					self.ParentObj:NpcSpeech("[00FF00] +"..notchBonus.." Master Notch Bonus[-]","combat")
				end
				--GRANDMASTER
				if(archerySkill == 100.00) then 
					notchBonus = 1.0
					self.ParentObj:NpcSpeech("[00FF00] +"..notchBonus.." Grandmaster Notch Bonus[-]","combat")
				end
				--GOD 100 HARVESTING
				if(archerySkill > 100.00) then 
					notchBonus = 100
					self.ParentObj:NpcSpeech("[00FF00] +"..notchBonus.." GOD Notch Bonus[-]","combat")
				end
			
			--StartWeaponAbilityCooldown(self.ParentObj, true, TimeSpan.FromMilliseconds(self._NotchArrowTime)) 
			--SCAN ADDED 
			local archeryAttackSpeed = self.ParentObj:GetStatValue("AttackSpeed")
			local archeryMasteryBonus = (archeryAttackSpeed-notchBonus)
			StartWeaponAbilityCooldown(self.ParentObj, true, TimeSpan.FromSeconds(archeryMasteryBonus)) 

			ProgressBar.Show(
			{
				TargetUser = self.ParentObj,
				Label="Notching Arrow ",
				--Duration=TimeSpan.FromMilliseconds(self._NotchArrowTime),
				--SCAN ADDED
				Duration=TimeSpan.FromSeconds(archeryMasteryBonus),
				PresetLocation="AboveHotbar",
				DialogId="JustNotchingAnArrow",
				Callback = function()
					EndMobileEffect(root)
				end
			})
		else
			EndMobileEffect(root)
			return false
		end
		
	end,


}