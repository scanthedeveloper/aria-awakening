--SCAN ADDED
-- Fires a doubleshot from a bow with the second attack dealing .50% damage.
MobileEffectLibrary.ExplosiveShot = 
{

	OnEnterState = function(self,root,target,args)
		self.Duration = args.Duration or self.Duration
		self.Target = target
		self.PulseMax = args.PulseMax or self.PulseMax
		self.Range = args.Range or self.Range

		if ( self.Target == nil ) then
			EndMobileEffect(root)
			return
		end

	end,

	FireArrow = function(self)
		self.ParentObj:PlayEffect("BuffEffect_H")
		self.ParentObj:SendMessage("ExecuteRangedWeaponAttack", self.Target, "RightHand")
	end,

	OnExitState = function(self,root)
	end,

	GetPulseFrequency = function(self,root)
		return self.Duration
	end,

	AiPulse = function(self,root)
		-- fire the second arrow
		self.ParentObj:NpcSpeech("[00FF00]+Explosive Shot[-]","combat")
		self.Target:NpcSpeech("[FF7777]Explosive Shot[-]","combat")
		
        --SECOND SHOT
		--self.FireArrow(self)

		--SPECIAL EFFFECT
		local damageRNG = math.random(10,40)
		self.Target:PlayEffect("FireballExplosionEffect")
        self.Target:PlayEffect("PrimedFire", 2)
        self.Target:PlayEffect("PrimedVoid", 2)
		self.Target:SendMessage("ProcessTrueDamage", self.Target, damageRNG)
		--SPELL
		self.Target:SendMessage("RequestMagicalAttack", "Bombardment", self.Target, self.ParentObj)
		--self.Target:SendMessage("RequestMagicalAttack", "Fireball", self.Target, self.ParentObj)
		--self.Target:SendMessage("RequestMagicalAttack", "PyroMoltenExplosion", self.Target, self.ParentObj)

		local targetLocation = self.Target:GetLoc()
		--local nearbyMobiles = FindObjects(SearchMobileInRange(self.Range, true))
		local nearbyMobiles = FindObjects(SearchMulti({
			SearchRange(targetLocation,4),
			SearchMobile()}), GameObj(0))
		for i=1,#nearbyMobiles do
			local mobile = nearbyMobiles[i]
        	if ( 
				ValidCombatTarget(self.Target, mobile, true)
				and
				self.ParentObj:HasLineOfSightToObj(mobile, ServerSettings.Combat.LOSEyeLevel)
			) then
				--self.ParentObj:SystemMessage("Valid Combat Target Detected","info")
				mobile:SendMessage("RequestMagicalAttack", "Fireball", mobile, self.ParentObj,true,targetLocation)
        	end
		EndMobileEffect(root)
		end
	end,

	Range = 4,
	Duration = TimeSpan.FromSeconds(.50),
	WeaponDamageModifier = 0.5,
	PulseMax = 2,
}