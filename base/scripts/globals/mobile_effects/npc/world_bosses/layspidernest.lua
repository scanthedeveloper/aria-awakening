MobileEffectLibrary.LaySpiderNest = 
{

   OnEnterState = function(self,root,target,args)
        args = args or {}
        
        local spawnLocation = GetRandomPassableLocationInRadiusWithMinimum(
            self.ParentObj:GetLoc(),
            6,
            12,
            true
        )

        if( spawnLocation ) then
            Create.AtLoc( "world_boss_spider_nest", spawnLocation, function(nest) nest:ClearCollisionBounds() end)
        end
        
        EndMobileEffect(root)
    end,

}