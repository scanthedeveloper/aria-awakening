MobileEffectLibrary.Shapeshift = 
{

   OnEnterState = function(self,root,target,args)
        
        --DebugMessage("Cloak me!")
        self._Args = args or {}
        self._Duration = self._Args.Duration or 7
        self._AttackModifier = self._Args.AttackModifier or 1.2
        self._SpeechTable = self._Args.SpeechTable or { "You look tasty.", "I can smell your fear.", "Lambs to the slaughter." }
        self._NewTemplate = self._Args.NewTemplate or "werewolf"
        self._BaseTemplate = self._Args.BaseTemplate or self.ParentObj:GetCreationTemplateId()
        self._Target = nil
        self._Loc = self.ParentObj:GetLoc()

        -- Transform us
        PlayEffectAtLoc("CloakEffect",self._Loc)
        local appearanceScale = GetTemplateObjectScale(self._NewTemplate)
        self.ParentObj:SetAppearanceFromTemplate(self._NewTemplate)
        self.ParentObj:SetScale(appearanceScale)

        -- Set attack
        SetCombatMod(self.ParentObj, "AttackTimes", "Shapeshift", self._AttackModifier)
        self.FindTarget(self,root)

    end,

    FindTarget = function(self, root)

        local players = GetNearbyPlayers( self.ParentObj, 20 )
        local health = 0
        self._Target = nil

        -- Find the player with the least health, yeah boy!
        for i=1, #players do 
            if( GetCurHealth(players[i]) > health ) then
                self._Target = players[i]
            end
        end

        -- If we didn't get a target we're done!
        if( self._Target == nil ) then EndMobileEffect(root) return end

        -- Perform execute shadowbite
        self.TrackTarget( self, root )

    end,

    TrackTarget = function(self, root)

        -- Do Speech
        if( self._SpeechTable ) then
            self.ParentObj:NpcSpeech(self._SpeechTable[math.random(1,#self._SpeechTable)])
        end

        -- Setup combat variables and attack!
        self.ParentObj:SendMessage("SetCurrentTarget", self._Target)

    end,

    OnExitState = function(self,root)
        SetCombatMod(self.ParentObj, "AttackTimes", "Shapeshift", nil)
        local appearanceScale = GetTemplateObjectScale(self._BaseTemplate)
        PlayEffectAtLoc("CloakEffect", self.ParentObj:GetLoc())
        self.ParentObj:SetAppearanceFromTemplate(self._BaseTemplate)
        self.ParentObj:SetScale(appearanceScale)
	end,

    GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds( self._Duration )
    end,

    AiPulse = function(self,root)

        EndMobileEffect(root)
        
	end,
    
    _Duration = 7,
    _AttackModifier = 2,
    _Target = nil,
    _SpeechTable = nil,
    _Loc = nil,

}