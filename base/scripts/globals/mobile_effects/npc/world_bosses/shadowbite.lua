MobileEffectLibrary.ShadowBite = 
{

   OnEnterState = function(self,root,target,args)
        
        --DebugMessage("Cloak me!")
        self._Args = args or {}
        self._Duration = self._Args.Duration or 4
        self._AttackModifier = self._Args.AttackModifier or 1.2
        self._SpeechTable = self._Args.SpeechTable or { "Rage, rage againist the dying of the light!", "What frail things you are.", "Go gently into the night.", "Embrace the darkness." }
        self._Target = nil
        self._Loc = self.ParentObj:GetLoc()

        -- Hide and disable the NPC
        PlayEffectAtLoc("CloakEffect",self._Loc)
        self.ParentObj:SetCloak(true)
        --SetMobileMod(self.ParentObj, "Disable", "GodFreeze", true)
        --SetMobileMod(self.ParentObj, "Freeze", "GodFreeze", true)

        CallFunctionDelayed(TimeSpan.FromMilliseconds(math.random( 1400, 3500 )), function()
            self.FindTarget(self, root)
        end)

    end,

    FindTarget = function(self, root)

        local players = GetNearbyPlayers( self.ParentObj, 20 )
        local health = 10000000
        self._Target = nil

        -- Find the player with the least health, yeah boy!
        for i=1, #players do 
            if( GetCurHealth(players[i]) < health ) then
                self._Target = players[i]
            end
        end

        -- If we didn't get a target we're done!
        if( self._Target == nil ) then EndMobileEffect(root) return end

        -- Perform execute shadowbite
        self.ExecuteShadowBite( self, root )

    end,

    ExecuteShadowBite = function(self, root)

        -- Put us behind the target!
        DebugMessage("Target: " .. self._Target:GetName())
        local targetLoc = self._Target:GetLoc()
        local offset = self._Target:GetObjVar("AngleOffset") or 0
        local angle = self._Target:GetFacing() + 180 + offset

        local destLoc = targetLoc:Project(angle,1)
        if IsValidLoc(destLoc, false, {"NoTeleport"}) then
            self.ParentObj:SetWorldPosition(destLoc)
        else
            local bufferedLoc = GetClosestLocationBuffered(self.ParentObj:GetLoc(), targetLoc:Project(angle,2), 0.3, false, {"NoTeleport"})
            if bufferedLoc then
                self.ParentObj:SetWorldPosition(bufferedLoc)
            else
                self.ParentObj:SetWorldPosition(targetLoc)
                --DebugMessage("Teleport blocked!")
                --self.ParentObj:SystemMessage("That foe blocked the teleportation!", "info")
            end
        end

        self.ParentObj:SetFacing(self._Loc:YAngleTo(targetLoc))
        
        -- Reveal and re-enable!
        self.ParentObj:SetCloak(false)
        --SetMobileMod(self.ParentObj, "Disable", "GodFreeze", nil)
        --SetMobileMod(self.ParentObj, "Freeze", "GodFreeze", nil)

        -- Do Speech
        if( self._SpeechTable ) then
            self.ParentObj:NpcSpeech(self._SpeechTable[math.random(1,#self._SpeechTable)])
        end

        -- Setup combat variables and attack!
        SetCombatMod(self.ParentObj, "AttackTimes", "Stab", self._AttackModifier)
        self.ParentObj:PlayEffect("BuffEffect_A")
		self.ParentObj:SendMessage("ExecuteHitAction", self._Target, "RightHand", false)
        SetCombatMod(self.ParentObj, "AttackTimes", "Stab", nil)
        self.ParentObj:SendMessage("ResetSwingTimer", 2)
        self.ParentObj:SendMessage("SetCurrentTarget", self._Target)
        
        EndMobileEffect(root)

    end,

    OnExitState = function(self,root)

        if( self.ParentObj:IsCloaked() ) then
            self.ParentObj:SetCloak(false)
        end

        SetMobileMod(self.ParentObj, "Disable", "GodFreeze", nil)
        SetMobileMod(self.ParentObj, "Freeze", "GodFreeze", nil)

	end,

    GetPulseFrequency = function(self,root)
		return self._PulseRate
    end,

    AiPulse = function(self,root)

        if( self._PulsesElapsed >= self._Duration ) then
            EndMobileEffect(root)
            return
        end

        self._PulsesElapsed = self._PulsesElapsed + 1
        
	end,
    
    _Duration = 0,
    _PulsesElapsed = 0,
    _PulseRate = TimeSpan.FromSeconds(1),
    _AttackModifier = 2,
    _Target = nil,
    _SpeechTable = nil,
    _Loc = nil,

}