MobileEffectLibrary.FireExplosionWave = 
{

   OnEnterState = function(self,root,target,args)
        
        self._Args = args or {}
        self._Duration = self._Args.Duration or 12
        self._Targeted = self._Args.Targeted or false
        self._SpawnMobile = self._Args.SpawnMobile or nil

        if ( not self.ParentObj:HasModule("sp_pyromancy_fire_vortex") )then
            self.ParentObj:AddModule("sp_pyromancy_fire_vortex")
        end

        --For spawning mobs at meteor locations
        RegisterEventHandler(EventType.CreatedObject,"MeteorMinionSpawned",
            function(success,objRef)
                objRef:SetObjVar("MobileLeader", self.ParentObj)
            end
        )

        --DebugMessage("MeteorSwawm Started!")

    end,

    OnExitState = function(self,root)
        UnregisterEventHandler("",EventType.CreatedObject,"MeteorMinionSpawned")
		self.ParentObj:SendMessage("MeteorRemove")
	end,

    FireMeteor = function(self, root) 
        --DebugMessage("MeteorSwawm Fired!")
        local targetLoc = self.ParentObj:GetLoc():Project(math.random(0,360), math.random(2,10))

        if( targetLoc ) then
            self.ParentObj:SendMessage("MeteorSpellTargetResult",targetLoc, true)

            if(self._SpawnMobile) then
                if( Success( self._SpawnMobile.Chance ) ) then
                    CallFunctionDelayed(TimeSpan.FromSeconds(2),function()
                        CreateObj(self._SpawnMobile.Template,targetLoc,"MeteorMinionSpawned")
                    end)
                end
            end

        end

    end,

    GetPulseFrequency = function(self,root)
		return self._PulseRate
	end,

    AiPulse = function(self,root)
        --DebugMessage("MeteorSwawm Pulsed!")
        if( self._PulsesElapsed <= self._Duration ) then
            self.FireMeteor(self,root)
        elseif( self._PulsesElapsed >= (self._Duration + 6) ) then
            EndMobileEffect(root)
            return
        end
		self._PulsesElapsed = self._PulsesElapsed + 1
	end,

    _Args = {},
    _Duration = 0,
    _PulsesElapsed = 0,
    _Targeted = false,
    _PulseRate = TimeSpan.FromMilliseconds(500),

}