-- SCAN ADDED
MobileEffectLibrary.BardDestruction = 
{
	Debuff = true,
	Resistable = true,

	OnEnterState = function(self,root,target,args)
		if(target) then SpellCaster = target end
		self.Target = target
		self.PulseFrequency = args.PulseFrequency or self.PulseFrequency
		self.PulseMax = args.PulseMax or self.PulseMax
		self.MinDamage = args.MinDamage or self.MinDamage
		self.MaxDamage = args.MaxDamage or self.MaxDamage
		if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "PoisonDebuff", "Barded", "Poison", self.MinDamage.."-"..self.MaxDamage.." damage every "..self.PulseFrequency.Seconds.." seconds." .. "\nThe music is actually hurting you.", true)
		end

		self.ParentObj:PlayEffect("MusicNoteIntensity3Effect",5)
		self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_cast_fire",false)
		AdvanceConflictRelation(target, self.ParentObj)
	end,

	OnExitState = function(self,root)
		if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "PoisonDebuff")
			ShowStatusElement(self.ParentObj,{IsSelf=true, HealthHue="FF0000"})
		end
        self.ParentObj:StopEffect("FireFliesEffect")
	end,

	GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

	AiPulse = function(self,root)
		self.CurrentPulse = self.CurrentPulse + 1
		if ( IsDead(self.ParentObj) or self.CurrentPulse > self.PulseMax ) then
			EndMobileEffect(root)
		else
			--self.ParentObj:PlayEffect("ArenaFlagRedEffect", 3)
			--self.ParentObj:PlayEffect("WarriorDemoStatus",2.5)
			--self.ParentObj:PlayEffect("WarriorEnrage",2.5)
			self.ParentObj:SendMessage("ProcessTypeDamage", self.Target, math.random(self.MinDamage, self.MaxDamage), "Poison")
		end
	end,

	PulseFrequency = TimeSpan.FromSeconds(.5),
	PulseMax = 6,
	CurrentPulse = 0,
	MinDamage = 3,
	MaxDamage = 10,
	SpellCaster = nil,
}