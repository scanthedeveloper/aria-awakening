MobileEffectLibrary.DrainedConstructHeal = 
{
    _HealIncreasePerStack = 50, 
    _ScalePerTick = -0.2,
    _MinScale = 1.1,
    _MaxStacks = 30,
    _Stacks = 0,
    _StackDelta = 0,
    _Registered = false,

   OnEnterState = function(self,root,target,args)
        
        self.Args = args or {}
        self._HealIncreasePerStack = self.Args.HealIncreasePerStack or self._HealIncreasePerStack
        self._StackDelta = self.Args.StackDelta or self._StackDelta
        self._Stacks = self.Args.Stacks or self._Stacks or 0
        self._Stacks = math.min(math.max(0, (self._Stacks + self._StackDelta)), self._MaxStacks)
        
        --DebugMessage("Healing Stacks : ", self._Stacks, self._StacksDelta) 

        if( self._Stacks > 0 ) then

            -- Adjust our scale if needed
            self.DoScaleChange(self)
            
            -- Heal
            self.DoHeal(self)
            
        end

        if( not self._Registered ) then
            self._Registered = true
            RegisterSingleEventHandler(EventType.Message, "DrainedConstructEndEffects", function() EndMobileEffect(root) end)
        end

    end,

    OnStack = function(self,root,target,args)
        self.OnEnterState( self, root, target, args )
    end,

    DoScaleChange = function( self )
        if( self._StackDelta > 0 ) then
            local scale = self.ParentObj:GetScale().X
            if( scale > self._MinScale ) then
                self.ParentObj:SetScale( Loc(scale + self._ScalePerTick, scale + self._ScalePerTick, scale + self._ScalePerTick ) )
            end
        end
    end,

    DoHeal = function(self)
        local healAmount = ( self._HealIncreasePerStack * self._Stacks )
        self.ParentObj:PlayObjectSound("event:/magic/misc/magic_water_restoration")
        self.ParentObj:SendMessage("HealRequest", healAmount, self.ParentObj, false)
        self.ParentObj:PlayEffect("GreaterHealEffect")
        --DebugMessage("Applying Heal : " .. healAmount)
    end,

}