MobileEffectLibrary.DrainedConstructEmpower = 
{
    _AttackPerStack = 0.3, 
    _ScalePerTick = 0.2,
    _MaxScale = 2,
    _MaxStacks = 30,
    _Stacks = 0,
    _StackDelta = 0,
    _Registered = false,

   OnEnterState = function(self,root,target,args)
        
        self.Args = args or {}
        self._AttackPerStack = self.Args.AttackPerStack or self._AttackPerStack
        self._StackDelta = self.Args.StackDelta or self._StackDelta
        self._Stacks = self.Args.Stacks or self._Stacks or 0
        self._Stacks = math.min(math.max(0, (self._Stacks + self._StackDelta)), self._MaxStacks)
        
        --DebugMessage("Empower Stacks : ", self._Stacks, self._StacksDelta)

        if( self._Stacks > 0 ) then

            -- Adjust our scale if needed
            self.DoScaleChange(self)

            -- Do Empower
            self.DoEmpower(self)
            
        end

        if( not self._Registered ) then
            self._Registered = true
            RegisterSingleEventHandler(EventType.Message, "DrainedConstructEndEffects", function() EndMobileEffect(root) end)
        end

    end,

    OnExitState = function(self,root)
        SetMobileMod(self.ParentObj, "MoveSpeedPlus", "Empower", nil)
		SetMobileMod(self.ParentObj, "AttackTimes", "Empower", nil)
	end,

    OnStack = function(self,root,target,args)
        self.OnEnterState( self, root, target, args )
    end,
    
    DoScaleChange = function( self )
        if( self._StackDelta > 0 ) then
            local scale = self.ParentObj:GetScale().X
            if( scale< self._MaxScale ) then
                self.ParentObj:SetScale( Loc(scale + self._ScalePerTick, scale + self._ScalePerTick, scale + self._ScalePerTick ) )
            end
        end
    end,

    DoEmpower = function(self)
        local attackTimes = ( (self._AttackPerStack * self._Stacks) )
        SetMobileMod(self.ParentObj, "MoveSpeedPlus", "Empower", attackTimes)
		SetMobileMod(self.ParentObj, "AttackTimes", "Empower", attackTimes)
        --DebugMessage("Applying Empower : " .. attackTimes)
    end,

}