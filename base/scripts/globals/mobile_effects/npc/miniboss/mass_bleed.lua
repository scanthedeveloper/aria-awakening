MobileEffectLibrary.MassBleed = 
{
   OnEnterState = function(self,root,target,args)
        args = args or {}

        local mobileTeamType = self.ParentObj:GetObjVar("MobileTeamType")

        if(HasHumanAnimations(self.ParentObj)) then
            self.ParentObj:PlayAnimation("roar")
            self.ParentObj:PlayEffect("WarriorExecute",5)
            self.ParentObj:PlayEffect("BloodDropsEffect",5)
            self.ParentObj:PlayEffect("Blood7",5)
        else
            self.ParentObj:PlayAnimation("cast")
            self.ParentObj:PlayEffect("WarriorExecute",5)
            self.ParentObj:PlayEffect("BloodDropsEffect",5)
            self.ParentObj:PlayEffect("Blood7",5)
        end
        
        PlayEffectAtLoc("WarriorExecute",self.ParentObj:GetLoc())
        PlayEffectAtLoc("MeatExplosion",self.ParentObj:GetLoc())
        PlayEffectAtLoc("BodyExplosion",self.ParentObj:GetLoc())
        PlayEffectAtLoc("Blood7",self.ParentObj:GetLoc())
        self.ParentObj:PlayObjectSound("event:/animals/worm/worm_pain", false, 0, true)


        local nearbyMobiles = FindObjects(SearchMobileInRange(self.Radius,true))
        for i,mobile in pairs (nearbyMobiles) do
            if ( ValidCombatTarget(self.ParentObj, mobile) ) then
                --Only enrage mobs on the same team
                local curMobTeamType = mobile:GetObjVar("MobileTeamType")
                if (curMobTeamType ~= mobileTeamType) then
                    mobile:SendMessage("StartMobileEffect", "Bleed", self.ParentObj, {
                        PulseFrequency = TimeSpan.FromSeconds(2),
                        PulseMax = 5,
                        DamageMin = 100,
                        DamageMax = 200,
                    })
                end
            end
        end

        EndMobileEffect(root)
    end,


	OnExitState = function(self,root)

	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(0.25)
	end,

	AiPulse = function(self,root)

	end,

    Radius = 10,
}