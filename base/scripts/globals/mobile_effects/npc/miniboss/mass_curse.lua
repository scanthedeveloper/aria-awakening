MobileEffectLibrary.MassCurse = 
{
   OnEnterState = function(self,root,target,args)
        args = args or {}

        local mobileTeamType = self.ParentObj:GetObjVar("MobileTeamType")

        if(HasHumanAnimations(self.ParentObj)) then
            self.ParentObj:PlayAnimation("roar")
            self.ParentObj:PlayEffect("GrimAuraDebuffEffect",5)
            self.ParentObj:PlayEffect("GrimAuraEffect",5)
            self.ParentObj:PlayEffect("ShockwaveEffect",5)
        else
            self.ParentObj:PlayAnimation("cast")
            self.ParentObj:PlayEffect("GrimAuraDebuffEffect",5)
            self.ParentObj:PlayEffect("GrimAuraEffect",5)
            self.ParentObj:PlayEffect("ShockwaveEffect",5)
        end
        
        PlayEffectAtLoc("FireballExplosionEffect",self.ParentObj:GetLoc())
        self.ParentObj:PlayObjectSound("event:/character/combat_abilities/adrenaline_rush", false, 0, true)


        local nearbyMobiles = FindObjects(SearchMobileInRange(self.Radius,true))
        for i,mobile in pairs (nearbyMobiles) do
            if ( ValidCombatTarget(self.ParentObj, mobile) ) then
                --Only enrage mobs on the same team
                local curMobTeamType = mobile:GetObjVar("MobileTeamType")
                if (curMobTeamType ~= mobileTeamType) then
                    mobile:SendMessage("StartMobileEffect", "NecroCurse", self.ParentObj)
                end
            end
        end

        EndMobileEffect(root)
    end,


	OnExitState = function(self,root)

	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(0.25)
	end,

	AiPulse = function(self,root)

	end,

    Radius = 15,
}