MobileEffectLibrary.MassPlague = 
{
   OnEnterState = function(self,root,target,args)
        args = args or {}

        local mobileTeamType = self.ParentObj:GetObjVar("MobileTeamType")

        self.ParentObj:PlayEffect("GrimAuraEffect",5)
        self.ParentObj:PlayEffect("ShockwaveEffect",5)
        self.ParentObj:PlayEffect("RedCoreImpactWaveEffect",5)
        self.ParentObj:PlayEffect("CastVoid",8)
        self.ParentObj:PlayEffect("BubbleEnergyEffect",10)
        
        PlayEffectAtLoc("WispSummonEffect", target:GetLoc())
        self.ParentObj:PlayObjectSound("event:/character/combat_abilities/adrenaline_rush", false, 0, true)

        local nearbyMobiles = FindObjects(SearchMobileInRange(self.Radius,true))
        for i,mobile in pairs (nearbyMobiles) do
            if ( ValidCombatTarget(self.ParentObj, mobile) ) then
                --Only enrage mobs on the same team
                local curMobTeamType = mobile:GetObjVar("MobileTeamType")
                if (curMobTeamType == mobileTeamType) then
                    mobile:SendMessage("StartMobileEffect", "Agony", self.ParentObj, {
                        MinDamage = 10,
                        MaxDamage = 50,
                        PulseMax = 10,
                        PulseFrequency = TimeSpan.FromSeconds(1)
                    })
                end
            end
        end

        EndMobileEffect(root)
    end,


	OnExitState = function(self,root)

	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(0.25)
	end,

	AiPulse = function(self,root)

	end,

    Radius = 10,
}