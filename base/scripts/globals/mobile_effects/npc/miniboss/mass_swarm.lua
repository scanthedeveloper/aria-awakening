MobileEffectLibrary.MassSwarm = 
{
   OnEnterState = function(self,root,target,args)
        args = args or {}

        local mobileTeamType = self.ParentObj:GetObjVar("MobileTeamType")

        if(HasHumanAnimations(self.ParentObj)) then
            self.ParentObj:PlayAnimation("roar")
            self.ParentObj:PlayEffect("PoisonCloudEffect",5)
            self.ParentObj:PlayEffect("BardSongEarthEffect",5)
            self.ParentObj:PlayEffect("BubbleEffect",5)
            self.ParentObj:PlayEffect("DestructionEffect",5)
        else
            self.ParentObj:PlayAnimation("cast")
            self.ParentObj:PlayEffect("PoisonCloudEffect",5)
            self.ParentObj:PlayEffect("BardSongEarthEffect",5)
            self.ParentObj:PlayEffect("BubbleEffect",5)
            self.ParentObj:PlayEffect("DestructionEffect",5)
        end
        
        PlayEffectAtLoc("FireballExplosionEffect",self.ParentObj:GetLoc())
        self.ParentObj:PlayObjectSound("event:/character/combat_abilities/adrenaline_rush", false, 0, true)


        local nearbyMobiles = FindObjects(SearchMobileInRange(self.Radius,true))
        for i,mobile in pairs (nearbyMobiles) do
            if ( ValidCombatTarget(self.ParentObj, mobile) ) then
                --Only enrage mobs on the same team
                local curMobTeamType = mobile:GetObjVar("MobileTeamType")
                if (curMobTeamType ~= mobileTeamType) then
                    mobile:SendMessage("StartMobileEffect", "VenomousAffliction", self.ParentObj, {
                        MinDamage = 1,
                        MaxDamage = 4,
                        PulseMax = 10,
                        PulseFrequency = TimeSpan.FromSeconds(1)
                    })
                end
            end
        end

        EndMobileEffect(root)
    end,


	OnExitState = function(self,root)

	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(0.25)
	end,

	AiPulse = function(self,root)

	end,

    Radius = 7,
}