require 'globals.mobile_effects.npc.basic'
require 'globals.mobile_effects.npc.charge'
require 'globals.mobile_effects.npc.devilhand'

require 'globals.mobile_effects.npc.dragonfire'
require 'globals.mobile_effects.npc.dragonfiresmall'
require 'globals.mobile_effects.npc.dart'
require 'globals.mobile_effects.npc.silence'

require 'globals.mobile_effects.npc.cerberus.charge'
require 'globals.mobile_effects.npc.cerberus.poisonbreath'

require 'globals.mobile_effects.npc.death.voidteleport'
require 'globals.mobile_effects.npc.death.deathwave'

require 'globals.mobile_effects.npc.world_bosses.layspidernest'
require 'globals.mobile_effects.npc.world_bosses.meteorswarm'
require 'globals.mobile_effects.npc.world_bosses.shadowbite'
require 'globals.mobile_effects.npc.world_bosses.shapeshift'
--SCAN ADDED
require 'globals.mobile_effects.npc.world_bosses.fireexplosionwave'

--guard
require 'globals.mobile_effects.npc.guard.swift_justice'

require 'globals.mobile_effects.npc.howl'
require 'globals.mobile_effects.npc.hibernate'
require 'globals.mobile_effects.npc.poisoncloud'
require 'globals.mobile_effects.npc.incapacitate'

-- Monolith
require 'globals.mobile_effects.npc.monolith.drained_construct_empower'
require 'globals.mobile_effects.npc.monolith.drained_construct_heal'

--SCAN ADDED
require 'globals.mobile_effects.npc.miniboss.darkenergy'
require 'globals.mobile_effects.npc.miniboss.totalchaos'
require 'globals.mobile_effects.npc.miniboss.mass_bleed'
require 'globals.mobile_effects.npc.miniboss.bard_destruction'
require 'globals.mobile_effects.npc.miniboss.mass_ignite'
require 'globals.mobile_effects.npc.miniboss.mass_curse'
require 'globals.mobile_effects.npc.miniboss.mass_swarm'
require 'globals.mobile_effects.npc.miniboss.mass_plague'
require 'globals.mobile_effects.npc.miniboss.mass_rejuvenation'

