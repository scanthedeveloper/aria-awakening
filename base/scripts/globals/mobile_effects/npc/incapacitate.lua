MobileEffectLibrary.Incapacitate = 
{
    EndOnDamage = true,   
    OnEnterState = function(self,root,target,args)
        self._ID = args.MobileModID or "mod_"..uuid()
        self._Args = args
        self._DurationInSeconds = args.DurationInSeconds or 8

        -- This cannot be cast on players
        if( self.ParentObj:IsPlayer() ) then
            EndMobileEffect(root)
            return false
        end

        -- We only apply the objvar and effect if we doing it for the first time
        if not( self._Stacking ) then
            -- Disable the NPC
            SetMobileMod(self.ParentObj, "Disable", "Incapacitate", true)
            SetMobileMod(self.ParentObj, "Freeze", "Incapacitate", true)

            -- Show the effect
            if( self._Args.TargetEffect  ) then
                self.ParentObj:PlayEffect(self._Args.TargetEffect)
            end
        end

    end,

    OnStack = function(self,root,target,args)
        self._Stacking = true
        self.PulsesCount = 0
        self.OnEnterState(self,root,target,args)
    end,

    OnExitState = function(self,root)
        if( self._Args.TargetEffect ) then
            self.ParentObj:StopEffect(self._Args.TargetEffect)
        end
        SetMobileMod(self.ParentObj, "Disable", "Incapacitate", nil)
        SetMobileMod(self.ParentObj, "Freeze", "Incapacitate", nil)
	end,

	GetPulseFrequency = function(self,root)
		return TimeSpan.FromSeconds(1)
	end,

	AiPulse = function(self,root)
        if( self.PulsesCount >= self._DurationInSeconds ) then
            EndMobileEffect(root)
        end
        self.PulsesCount = self.PulsesCount + 1
    end,
    
    PulsesCount = 0,
    _Args = nil,
    _DurationInSeconds = 3,
    _ID = nil,
    _Stacking = false,
}