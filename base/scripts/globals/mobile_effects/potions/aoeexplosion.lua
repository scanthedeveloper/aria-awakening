MobileEffectLibrary.AOEExplosion = 
{
    OnEnterState = function(self,root,target,args)  
        if ( self.ParentObj:HasTimer("RecentPotion") ) then
			self.ParentObj:SystemMessage("Cannot use again yet.", "info")
			EndMobileEffect(root)
			return false
        end
        
        self.QueueLocation = args.Location or nil
        self.ResourceType = target:GetObjVar("ResourceType")
        self.Items = args.Items or {}
        self.DoAnimation = args.DoAnimation
        self._Message = nil
        self._Args = args
        self._Args.Damage = self._Args.Damage or 1
        self._Args.Radius = self._Args.Radius or 3

        if( self.QueueLocation ) then
            return self.OnValidLocation(self, root)
        else
            -- We need to register getting our target back
            RegisterEventHandler(EventType.ClientTargetLocResponse, "AOEExplosionTarget", 
            function(success, location)
                if( success ~= true ) then EndMobileEffect(root) return end
                
                -- If the target isn't valid get out of here!
                if(location == nil ) then 
                    self._Message = "Invalid location." 
                    EndMobileEffect(root) 
                    return false 
                end
                
                -- Queue up our target
                self.QueueLocation = location

                return self.OnValidLocation(self, root)

            end)
        end

        RegisterEventHandler(EventType.Message, "CancelSpellCast", function() EndMobileEffect(root) end)
        self.ParentObj:RequestClientTargetLoc(self.ParentObj, "AOEExplosionTarget")

    end,

    OnValidLocation = function(self, root)

        if( self.ParentObj:GetLoc():Distance(self.QueueLocation) > SorceryHelper.HarmfulAbilityRange ) then
            self._Message = "Out of range." 
            EndMobileEffect(root) 
            return false 
        end

        -- Can we send a wisp to this target?
        if( SorceryHelper.CanSendWispToTargetLocation(self.ParentObj, self.QueueLocation) ) then
            -- Consume the potion
            ConsumeResourceBackpack(self.ParentObj, self.ResourceType, 1)

            if( self.DoAnimation ) then
                LookAtLoc(self.ParentObj, self.QueueLocation)
                self.ParentObj:PlayAnimation("spell_fire")
            end

            CallFunctionDelayed(TimeSpan.FromSeconds(0.2),function ( ... )
                SorceryHelper.DisplaySpellRadiusWarning( self.QueueLocation, tostring(self._Args.Radius), false, 600, function(success)
                    self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact")
                   
                    --SCAN UPDATED VISUAL EFFECTS
                    --PlayEffectAtLoc("MagicRingExplosionEffect",self.QueueLocation,0.5)
                    PlayEffectAtLoc("ImpactWaveEffect",self.QueueLocation,1)
                    PlayEffectAtLoc("FireballExplosionEffect",self.QueueLocation,1)

                    --SCAN REDUCED FROM 1 MINUTE TO 15 SECONDS
                    self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(15), "RecentPotion")
                    
                    -- This is where the magic happens!
                    local targets = GetNearbyCombatTargets( self.ParentObj, self._Args.Radius, self.QueueLocation, true )
        
                    -- Damage our targets
                    for i=1,#targets do 
                        if( SorceryHelper.CanAoEHitTarget( targets[i], self ) ) then
                            SorceryHelper.DealTrueDamageToTarget( self.ParentObj, targets[i], self._Args.Damage )
                        end 
                    end
                end)
            end)

            EndMobileEffect(root) 
            return true
        else -- wisp cannot get to target
            self._Message = "Cannot see target."
            EndMobileEffect(root)
            return false
        end
    end,

    OnStack = function(self,root,target,args)
        self.ParentObj:SendClientMessage("CancelSpellCast")
        EndMobileEffect(root)
	end,
    
    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "AOEExplosionTarget")
        UnregisterEventHandler("", EventType.Message, "CancelSpellCast")
        if( self._Message ~= nil ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
    end,

    
}