MobileEffectLibrary.PotionMend = 
{

	OnEnterState = function(self,root,target,args)
	
		if ( self.ParentObj:HasTimer("RecentPotion") ) then
			self.ParentObj:SystemMessage("Cannot use again yet.", "info")
			EndMobileEffect(root)
			return false
		end
		
		if not( HasAnyMobileEffect(self.ParentObj, {"MortalStruck", "Bleed"}) ) then
			self.ParentObj:SystemMessage("You are not wounded.", "info")
			EndMobileEffect(root)
			return false
		end

		if( not RankedArena.CanUsePotion(self.ParentObj) ) then
			self.ParentObj:NpcSpeechToUser("You have consumed the maximum amount of potions for an arena match.", self.ParentObj)
			EndMobileEffect(root)
			return false
		end
		
		--SCAN REDUCED FROM 1 MINUTE TO 15 SECONDS
		self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(15), "RecentPotion")

		self.ParentObj:PlayEffect("HealEffect")

		self.ParentObj:SendMessage("EndMortalStruckEffect")
		self.ParentObj:SendMessage("EndBleedEffect")

		self.ParentObj:SystemMessage("Your wounds have been mended.", "info")

		EndMobileEffect(root)
	end,

}