MobileEffectLibrary.PotionShrinking = 
{
    OnEnterState = function(self,root,target,args)
        -- We always want to set our scale back to one
        self._BaseScale = Loc(1,1,1)

        -- Increase our scale by our growth factor.
        self.ParentObj:SetScale( self._GrowthScale * self._BaseScale )

        -- Add buff icon
        if ( self.ParentObj:IsPlayer() ) then
			AddBuffIcon(self.ParentObj, "PotionShrinking", "Shrink", "Dispel", "You feel much shorter.", true)
		end

    end,

    OnExitState = function(self,root)
        self.ParentObj:SetScale( self._BaseScale )
        if ( self.ParentObj:IsPlayer() ) then
			RemoveBuffIcon(self.ParentObj, "PotionShrinking")
		end
    end,
    
    -- CleanUp is called if the user logs out and back in, or gets disconnected.
    CleanUp = function( parentObj )
        parentObj:SetScale( Loc(1,1,1) )
    end,

    GetPulseFrequency = function(self,root)
		return self.PulseFrequency
	end,

    AiPulse = function(self,root)
		self.CurrentPulse = self.CurrentPulse + 1
		if ( self.CurrentPulse > self.PulseMax ) then
			EndMobileEffect(root)
		else

		end
	end,
    
    PulseFrequency = TimeSpan.FromSeconds(30),
	PulseMax = 6,
	CurrentPulse = 0,
    _GrowthScale = 0.5,
    _BaseScale = 1,

}