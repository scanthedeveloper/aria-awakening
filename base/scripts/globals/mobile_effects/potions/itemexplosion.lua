MobileEffectLibrary.ItemExplosion = 
{
    OnEnterState = function(self,root,target,args)
        self.QueueLocation = args.Location or nil
        self.ResourceType = target:GetObjVar("ResourceType")
        self.Items = args.Items or {}
        self.DoAnimation = args.DoAnimation
        self._Message = nil

        if( self.QueueLocation ) then
            return self.OnValidLocation(self, root)
        else
            -- We need to register getting our target back
            RegisterEventHandler(EventType.ClientTargetLocResponse, "ItemExplosionTarget", 
            function(success, location)
                if( success ~= true ) then EndMobileEffect(root) return end
                
                -- If the target isn't valid get out of here!
                if(location == nil ) then 
                    self._Message = "Invalid location." 
                    EndMobileEffect(root) 
                    return false 
                end
                
                -- Queue up our target
                self.QueueLocation = location

                return self.OnValidLocation(self, root)

            end)
        end

        RegisterEventHandler(EventType.Message, "CancelSpellCast", function() EndMobileEffect(root) end)
        self.ParentObj:RequestClientTargetLoc(self.ParentObj, "ItemExplosionTarget")

    end,

    OnValidLocation = function(self, root)

        if( self.ParentObj:GetLoc():Distance(self.QueueLocation) > SorceryHelper.HarmfulAbilityRange ) then
            self._Message = "Out of range." 
            EndMobileEffect(root) 
            return false 
        end

        -- Can we send a wisp to this target?
        if( SorceryHelper.CanSendWispToTargetLocation(self.ParentObj, self.QueueLocation) ) then
            -- Consume the potion
            ConsumeResourceBackpack(self.ParentObj, self.ResourceType, 1)

            if( self.DoAnimation ) then
                LookAtLoc(self.ParentObj, self.QueueLocation)
                self.ParentObj:PlayAnimation("spell_fire")
            end

            CallFunctionDelayed(TimeSpan.FromSeconds(0.5),function ( ... )
                -- Place items around location
                for i=1, #self.Items do 
                    local item = self.Items[i]
                    local chance = item[3] or 0
                    if( Success(chance) ) then
                        loc = GetRandomPassableLocationInRadius(self.QueueLocation,5,true)
                        PlayEffectAtLoc("RedCoreImpactWaveEffect",loc,0.2)
                        if( item[2] > 1 ) then
                            Create.Stack.AtLoc( item[1], item[2], loc )
                        else
                            Create.AtLoc(item[1], loc)
                        end
                    end
                end
            end)

            EndMobileEffect(root) 
            return true
        else -- wisp cannot get to target
            self._Message = "Cannot see target."
            EndMobileEffect(root)
            return false
        end
    end,

    OnStack = function(self,root,target,args)
        self.ParentObj:SendClientMessage("CancelSpellCast")
        EndMobileEffect(root)
	end,
    
    OnExitState = function(self,root)
        UnregisterEventHandler("", EventType.Message, "ItemExplosionTarget")
        UnregisterEventHandler("", EventType.Message, "CancelSpellCast")
        if( self._Message ~= nil ) then
            self.ParentObj:SystemMessage(self._Message, "info")
        end
    end,

    
}