MobileEffectLibrary.PotionMana = 
{

	OnEnterState = function(self,root,target,args)
		self.Amount = args.Amount or self.Amount
		if ( self.ParentObj:HasTimer("RecentPotion") ) then
			self.ParentObj:SystemMessage("Cannot use again yet.", "info")
			EndMobileEffect(root)
			return false
		end
		
		if ( GetCurMana(self.ParentObj) >= GetMaxMana(self.ParentObj) ) then
			self.ParentObj:SystemMessage("You seem fine.", "info")
			EndMobileEffect(root)
			return false
		end

		if( not RankedArena.CanUsePotion(self.ParentObj) ) then
			self.ParentObj:NpcSpeechToUser("You have consumed the maximum amount of potions for an arena match.", self.ParentObj)
			EndMobileEffect(root)
			return false
		end

		RequestMobileMod( self.ParentObj, self.ParentObj, {"PotionEffectTimes"},
		function(MobileMod) 
			local modifier = GetMobileMod(MobileMod.PotionEffectTimes, 1)

			--SCAN REDUCED FROM 1 MINUTE TO 15 SECONDS
			self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(15), "RecentPotion")
			self.ParentObj:PlayEffect("HeadFlareEffect")--TODO: Change effect.
			AdjustCurMana(self.ParentObj, (self.Amount * modifier))
			EndMobileEffect(root)
		end)
		
	end,

	Amount = 1,
}