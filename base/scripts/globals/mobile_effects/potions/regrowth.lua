MobileEffectLibrary.PotionRegrowth = 
{

	OnEnterState = function(self,root,target,args)
		self.Amount = args.Amount or self.Amount
		if( not target ) then
			self.Message = "Invalid target."
			EndMobileEffect(root)
			return false
		end

		if( IsPet(target) and IsController(self.ParentObj,target) ) then
			if( GetDurabilityValue(target) <= 8 ) then
				target:PlayEffect("HealEffect",1)
				target:PlayObjectSound("event:/magic/misc/magic_water_restoration")
				AdjustDurability(target, self.Amount)
			else
				self.Message = "That pet looks fine."
				EndMobileEffect(root)
				return false
			end
		else
			self.Message = "Invalid target."
			EndMobileEffect(root)
			return false
		end

		EndMobileEffect(root)
	end,

	OnExitState = function(self,root)
		if( self.Message ) then
			self.ParentObj:SystemMessage(self.Message, "info")
		end
	end,

	Amount = 2,
	Message = nil,
}