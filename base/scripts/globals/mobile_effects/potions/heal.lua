MobileEffectLibrary.PotionHeal = 
{

	OnEnterState = function(self,root,target,args)
		self.Amount = args.Amount or self.Amount
		if ( self.ParentObj:HasTimer("RecentPotion") ) then
			self.ParentObj:SystemMessage("Cannot use again yet.", "info")
			EndMobileEffect(root)
			return false
		end
		
		if ( GetCurHealth(self.ParentObj) >= GetMaxHealth(self.ParentObj) ) then
			self.ParentObj:SystemMessage("You seem fine.", "info")
			EndMobileEffect(root)
			return false
		end

		if ( HasMobileEffect(self.ParentObj, "MortalStruck") ) then
			self.ParentObj:NpcSpeechToUser("You are mortally wounded.", self.ParentObj)
			EndMobileEffect(root)
			return false
		end

		--[[ SCAN DISABLED DUE TO CHANGING THE ENRAGE ABILITY TO A HEAL
		if ( HasMobileEffect(self.ParentObj, "Enrage") ) then
			self.ParentObj:NpcSpeechToUser("You cannot drink this whilst enraged.", self.ParentObj)
			EndMobileEffect(root)
			return false
		end
		]]

		if( not RankedArena.CanUsePotion(self.ParentObj) ) then
			self.ParentObj:NpcSpeechToUser("You have consumed the maximum amount of potions for an arena match.", self.ParentObj)
			EndMobileEffect(root)
			return false
		end

		RequestMobileMod( self.ParentObj, self.ParentObj, {"PotionEffectTimes"},
		function(MobileMod) 
			local modifier = GetMobileMod(MobileMod.PotionEffectTimes, 1)

			--SCAN REDUCED FROM 1 MINUTE TO 15 SECONDS
			self.ParentObj:ScheduleTimerDelay(TimeSpan.FromSeconds(15), "RecentPotion")
			self.ParentObj:PlayEffect("HealEffect")
			--DebugMessage( "HealRequest", tostring(self.Amount * modifier), tostring(target), tostring(false) )
			self.ParentObj:SendMessage("HealRequest", (self.Amount * modifier), target, false)
			EndMobileEffect(root)
		end)

	end,

	Amount = 1,
}