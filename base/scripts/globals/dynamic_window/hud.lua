function ShowStatusElement(mobileObj,args)
	args = {
		DialogId = args.DialogId or ("Status"..mobileObj.Id),
	    ScreenX = args.ScreenX or 300,
		ScreenY = args.ScreenY or 300,
		IsSelf = args.IsSelf or false,
		FrameImage = args.FrameImage or "UtilityBar_Frame",
		User = args.User or mobileObj,
		HealthHue = args.HealthHue or "FF0000"
	}

	local width = 166
	local height = (args.IsSelf and 58) or 10

	local statusWindow = DynamicWindow(args.DialogId,"",width,height,args.ScreenX,args.ScreenY,"TransparentDraggable")

	-- this is a special command that handles the click client side by targeting the mob with the id of the buttonid
	statusWindow:AddButton(0,24,"","",166,36,"","$target "..mobileObj.Id,false,"Invisible")

	statusWindow:AddImage(0,0,args.FrameImage)

	local modifiedName = StripColorFromString(mobileObj:GetName())
	statusWindow:AddLabel(16,6,modifiedName,140,20,15,"left",false,true,"SpectralSC-SemiBold")

	local statusFrameImage = (args.IsSelf and "UtilityBar_StatusFrame") or "UtilityBar_StatusFrameSingle"
	statusWindow:AddImage(14,20,statusFrameImage,135,0,"Sliced")

	statusWindow:AddStatBar(
		17,
		23,			
		129,
		7,
		"Health",		
		args.HealthHue,
		mobileObj)

	if(args.IsSelf) then
		statusWindow:AddStatBar(
			17,
			33,			
			129,
			4,
			"Mana",		
			"3388ff",
			mobileObj)

		statusWindow:AddStatBar(
			17,
			40,			
			129,
			4,
			"Stamina",		
			"fffd52",
			mobileObj)
	end

	args.User:OpenDynamicWindow(statusWindow)
end

function UpdateItemBar(mobileObj)
	local itemSlotStartIndex = 21

	--local curSlot = itemSlotStartIndex
	--local backpack = this:GetEquippedObject("Backpack")
	--for i,objRef in pairs(backpack:GetContainedObjects()) do
	--	if(i <= 8) then
	--		local curAction = GetItemUserAction(objRef,this) 
	--		curAction.Slot = curSlot			
	--		AddUserActionToSlot(curAction)

	--		curSlot = curSlot + 1
	--	end
	--end	

	-- two rows of 4
	local itemSize = 43
	local itembarHeight = itemSize * 4
	
	local spellBarWindow = DynamicWindow("itembar","",0,0,0,-(itembarHeight),"Transparent","Right")
	for itemIndex = 0,7 do
		local yIndex = (itemIndex)

		spellBarWindow:AddHotbarAction(-(2+itemSize),(yIndex*itemSize),itemSlotStartIndex + itemIndex,0,0,"Circle",true)
	end

	mobileObj:OpenDynamicWindow(spellBarWindow)
end

function UpdateSpellBar(mobileObj)
	--local curAction = GetSpellUserAction("Fireball") 
	--curAction.Slot = 1
	--AddUserActionToSlot(curAction)
	--curAction = GetSpellUserAction("Greaterheal") 
	--curAction.Slot = 2
	--AddUserActionToSlot(curAction)
	--curAction = GetSpellUserAction("Lightning") 
	--curAction.Slot = 3
	--AddUserActionToSlot(curAction)

	local spellItemSize = 48
	local spellbarHeight = spellItemSize * 10
	local spellSlotStartIndex = 1
	
	local spellBarWindow = DynamicWindow("spellbar","",0,0,0,-(spellbarHeight/2),"Transparent","Left")
	local curX = 2

	-- SCAN Added  (SPELL BAR ONE - 3 COLUMNS)
	for x=0,3 do	
		for y=0,9 do
			local index = (x * 10) + y
			if((index+spellSlotStartIndex) > 20) then index = index + 10 end
			spellBarWindow:AddHotbarAction(curX,y*spellItemSize,index+spellSlotStartIndex,0,0,"Square",true)
		end
		curX = curX + spellItemSize
	end

	mobileObj:OpenDynamicWindow(spellBarWindow)

	-- SCAN Added (SPELL BAR TWO - (LEFT))
	local spellBarWindow2 = DynamicWindow("spellbar2","",0,0,-312,-68,"Transparent","Bottom")
	for x=0,4 do
		spellBarWindow2:AddHotbarAction(x*spellItemSize,0,61+x,0,0,"Square",true)
	end
	mobileObj:OpenDynamicWindow(spellBarWindow2)
	
	-- SCAN Added (SPELL BAR THREE - (RIGHT))
	local spellBarWindow3 = DynamicWindow("spellbar3","",0,0,69,-68,"Transparent","Bottom")
	for x=0,4 do
		spellBarWindow3:AddHotbarAction(x*spellItemSize,0,68+x,0,0,"Square",true)
	end
	mobileObj:OpenDynamicWindow(spellBarWindow3)
	
	--[[ SCAN REMOVED; ORIGINAL UI BUTTONS
	for x=0,1 do		
		for y=0,9 do
			local index = (x * 10) + y
			spellBarWindow:AddHotbarAction(curX,y*spellItemSize,index+spellSlotStartIndex,0,0,"Square",true)
		end
		curX = curX + spellItemSize
	end
	
	-- third spellbar column starts at 31 so previous player keybindings/hotbar placements will not be disrupted + weapon abilities are tied to slots 29 and 30
	for y=0,9 do
		local index = 30 + y
		spellBarWindow:AddHotbarAction(curX,y*spellItemSize,index+spellSlotStartIndex,0,0,"Square",true)
	end
	]]
end


function UpdateHotbar(mobileObj)		
	local abilitySlotStartIndex = 29	

	local hasPrimary = GetWeaponAbility(mobileObj, true) ~= nil
	local hasSecondary = GetWeaponAbility(mobileObj, false) ~= nil
	--SCAN DISABLED
	--local isNewPlayer =  mobileObj:GetObjVar("InitiateMinutes") ~= nil

	local abilityCount = 0
	if(hasPrimary) then abilityCount = abilityCount + 1 end
	if(hasSecondary) then abilityCount = abilityCount + 1 end
	--SCAN DISABLED
	--if(isNewPlayer) then abilityCount = abilityCount + 1 end

	if(abilityCount == 0) then
		mobileObj:CloseDynamicWindow("actionbar")
	else
		local actionWindow = DynamicWindow("actionbar","",0,0,-90,-68,"Transparent","Bottom")
		local curX = 0
		if(abilityCount == 1) then
			curX = 65
		elseif(abilityCount == 2) then
			curX = 35
		end

		if(hasPrimary) then
			actionWindow:AddHotbarAction(curX,0,abilitySlotStartIndex,0,0,"SquareFixed",false)
			curX = curX + 60
		end

		if(hasSecondary) then
			actionWindow:AddHotbarAction(curX,0,abilitySlotStartIndex+1,0,0,"SquareFixed",false)
			curX = curX + 60
		end
		
		--[[ --SCAN DISABLED
		if(isNewPlayer) then
			data = {
					ID=	"magicalguide",
					ActionType="CustomCommand",
					DisplayName="Summon Guide",
					Tooltip="Summon or talk to your magical guide.\n\nYour magical guide can help you find places and give you tips as you explore this new land.",
					Icon="magicalguide",
					ServerCommand="Guide",
					Enabled=true,		
					}
			--GW need to revert this back to NoHotKey with new client.		
			actionWindow:AddUserAction(curX,0,data,0,0,"SquareFixedNoHotkey")
		end 
		]]
		
		mobileObj:OpenDynamicWindow(actionWindow)
	end
end

function UpdateBloodlust(mobileObj)
	--mobileObj:SetStatMaxValue("Bloodlust", 100)
	local bloodlust = GetCurBloodlust(mobileObj)
	if ( bloodlust and bloodlust <= 0 ) then
		mobileObj:CloseDynamicWindow("BloodlustBarWindow")
	else
		-- Open dynamic window to show stealth value
		local bloodlustBarWindow = DynamicWindow("BloodlustBarWindow","",10,100,-65,-50,"Transparent","Center")
		bloodlustBarWindow:AddImage(-15,3,"StaminaBar_Frame",0,50,"Sliced")
		bloodlustBarWindow:AddStatBar(
			-10,
			48,
			36,
			5,
			"Bloodlust",
			"ff0000",
			mobileObj,
			true)
			mobileObj:OpenDynamicWindow(bloodlustBarWindow)
	end
end

function UpdateRegionStatus(playerObj,areaName,curProtection)		
	areaName = areaName or GetRegionalName(playerObj:GetLoc())
	curProtection = curProtection or playerObj:GetObjVar("GuardProtection") or ""

	local dynWindow = DynamicWindow("regionstatus","",0,0,-196,190,"Transparent","TopRight")
	
	local curY = 0
	if(HasMultipleUniverses()) then
		dynWindow:AddLabel(100,-8,GetUniverseDisplayName(),0,0,16,"center",false,true,"SpectralSC-SemiBold")
		curY = 6
	end

	dynWindow:AddLabel(100,curY,areaName,0,0,20,"center",false,true,"SpectralSC-SemiBold")

	if(ChaosZoneHelper.IsPlayerInChaosZone(playerObj)) then
		dynWindow:AddLabel(100,curY+18,"[FF0000]Chaos[-]",0,0,16,"center",false,true,"SpectralSC-SemiBold")
		dynWindow:AddButton(125,curY+16,"","",12,12,"This is a lawless area with no punishment for criminal actions.","",false,"Help")
	elseif(curProtection == "None") then		
		dynWindow:AddLabel(100,curY+18,"[FF0000]Wilderness[-]",0,0,16,"center",false,true,"SpectralSC-SemiBold")
		dynWindow:AddButton(140,curY+16,"","",12,12,"You can be attacked and looted freely in this area.","",false,"Help")
	end

	-- not the best place to put the god button but it works
	if(IsImmortal(playerObj)) then
		dynWindow:AddButton(162,-107,"","",22,21,"","ToggleGodWindow",false,"God")
	end

	playerObj:OpenDynamicWindow(dynWindow)
end

function UpdateMilitiaScore(playerObj,type,endTime,score)		

	local types = {"Cleansing","King of the Hill"}
	local descs = {
		"Search this area for spirits",
		"Occupy and defend the hill"
	}
	local timeLeft = TimeSpanToWords(endTime:Subtract(DateTime.UtcNow))
	local dynWindow = DynamicWindow("MilitiaEventScore","",0,0,0,0,"Transparent","Top")
	dynWindow:AddLabel(0,30,types[type],0,0,24,"center",false,true,"SpectralSC-SemiBold")
	dynWindow:AddLabel(0,50,descs[type],0,0,18,"center",false,true,"SpectralSC-SemiBold")
    dynWindow:AddLabel(0,65,ServerSettings.Militia.Militias[1].TextColor..ServerSettings.Militia.Militias[1].Name.." [-]"..math.round(score[1]),0,0,18,"center",false,true,"SpectralSC-SemiBold")
    dynWindow:AddLabel(0,80,ServerSettings.Militia.Militias[2].TextColor..ServerSettings.Militia.Militias[2].Name.." [-]"..math.round(score[2]),0,0,18,"center",false,true,"SpectralSC-SemiBold")
    dynWindow:AddLabel(0,95,ServerSettings.Militia.Militias[3].TextColor..ServerSettings.Militia.Militias[3].Name.." [-]"..math.round(score[3]),0,0,18,"center",false,true,"SpectralSC-SemiBold")
    dynWindow:AddLabel(0,110,timeLeft,0,0,18,"center",false,true,"SpectralSC-SemiBold")

	playerObj:OpenDynamicWindow(dynWindow)
end

function UpdateDynamicEventHUD(playerObj,vars)		

	local title = vars.Title or "Dynamic Event Title"
	local desc = vars.Description or "Dynamic Event Description."
	local endTime = vars.EndTime or nil
	local stage = vars.Stage or 0
	local maxStage = vars.MaxStage or 0
	local progress = vars.Progress or 0

	
	local dynWindow = DynamicWindow("DynamicEventHUD","",0,0,0,0,"Transparent","Top")
	local yPos = 30
	dynWindow:AddLabel(0,yPos,title,0,0,24,"center",false,true,"SpectralSC-SemiBold")
	yPos = yPos + 20
	dynWindow:AddLabel(0,yPos,desc,0,0,18,"center",false,true,"SpectralSC-SemiBold")
	yPos= yPos + 15

	if( stage > 0 ) then
		dynWindow:AddLabel(0,yPos,"["..COLORS.FloralWhite.."]".."Stage: [-]"..stage.." of "..maxStage,0,0,18,"center",false,true,"SpectralSC-SemiBold")
		yPos= yPos + 15
	end
	
	if( endTime ) then
		local timeLeft = TimeSpanToWords(endTime:Subtract(DateTime.UtcNow), nil, {Seconds = true})
		dynWindow:AddLabel(0,yPos,timeLeft .. " Remaining",0,0,18,"center",false,true,"SpectralSC-SemiBold")
	end

	playerObj:OpenDynamicWindow(dynWindow)
end

function CloseDynamicEventHUD( playerObj )
	playerObj:CloseDynamicWindow("DynamicEventHUD")
end