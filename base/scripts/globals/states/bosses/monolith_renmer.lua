States.MonolithRenmerIdle = {
    Name = "MonolithRenmerIdle",
    Init = function(self)
        self.IdleCooldowns = {}
        self.Spawner = self.ParentObj:GetObjVar("Spawner")
        self.SpawnerLoc = self.Spawner:GetLoc()

    end,
    ShouldRun = function(self)
        return (
            (self.Spawner and self.SpawnerLoc)
            and not self.IsPathing 
            and not self.CurrentTarget 
            and self.ParentObj:GetLoc() ~= self.SpawnerLoc
        ) 
    end,
    ExitState = function(self)
        --self.ParentObj:SetFacing(180)
    end,
    Run = function(self)

        self.FightStarted = false

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

        -- Path back to spawn location
        if( self.ParentObj:GetLoc() ~= self.SpawnerLoc ) then
            self.PathTo(self.SpawnerLoc, 8)
        end

    end,
    EnterState = function(self)
        -- Remove portals
        if( self.Portals ~= nil ) then
            States.MonolithRenmerFight.DestroyPortals(self)
        end

        States.Aggro.ClearAggro(self)

    end,
}

States.MonolithRenmerFight = {
    Name = "MonolithRenmerFight",
    Init = function(self)
        States.Fight.Init(self)
        self.Location = self.ParentObj:GetLoc()
        self.FightStarted = false
        self.AggroRange = 100
        self.Cooldowns = {}
        self.GateStatus = 0
        self.LevelTwoLastGate = FindObject(SearchObjVar("GateTag", "Gate2a", 60))
        self.PortalLocations = { 
            Loc(-1185, 0, 40.50)
            , Loc(-1191.84, 0, 28.31) 
            , Loc(-1177.83, 0, 28.31) 
            , Loc(-1185, 0, -4.312)
        }

        self.Portals = nil

        self.Cooldowns.SpawnSparks = 0;
        self.Cooldowns.ShootWisp = 0;
        self.Cooldowns.DoMassAoE = 0;
        
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,
    Run = function(self)
        -- call base fight
        States.Fight.Run(self)
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Start Fight
        States.MonolithRenmerFight.StartFight(self)

        -- We need to constantly re-apply the conductor's dance in-case someone was ressed
        States.MonolithRenmerFight.ForEachPlayerInArea(self, function(player)
            self.ParentObj:SendMessage("AddAggro", player, 20)					
        end)
        
        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))

        local AggroList = {}
            for mobileObj, threat in pairs(self.AggroList) do 
                if( mobileObj:IsValid() and not IsDead(mobileObj) ) then
                    table.insert( AggroList, mobileObj )
                end
            end

        -- Spawn Sparks    
        if( GroupAI.CooldownCheck( self.Cooldowns, "SpawnSparks", unixTS, 15 ) ) then
            if(#AggroList > 0) then
                for i=1, #self.PortalLocations do 
                    local target = AggroList[math.random(1, #AggroList)]
                    local portalLoc = self.PortalLocations[i]
                    Create.AtLoc( "charged_spark", portalLoc, function(spark) 
                        CallFunctionDelayed(TimeSpan.FromMilliseconds(60), function() 
                            spark:SendMessage("SetCurrentTarget", target)
                        end) 
                    end)
                end
                self.Cooldowns.SpawnSparks = unixTS
            end
        end

        -- Shoot Wisps
        if( GroupAI.CooldownCheck( self.Cooldowns, "ShootWisp", unixTS, 6 ) ) then
            if(#AggroList > 0) then
                self.ParentObj:PlayAnimation("cast_heal")
                self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_force")
                
                for i=1, 4 do 
                    local target = AggroList[math.random(1, #AggroList)]
                    SorceryHelper.SendWispToTargetMobile( {1}, self.ParentObj, target, 300, function(success)
                        if( success ) then
                            target:PlayObjectSound("event:/magic/air/magic_air_lightning_impact")
                            target:SendMessage("ProcessTrueDamage", self.ParentObj, 100, true)
                        end
                    end)
                end
                self.Cooldowns.ShootWisp = unixTS
            end
        end


        -- Shoot Wisps
        if( GroupAI.CooldownCheck( self.Cooldowns, "DoMassAoE", unixTS, 10 ) ) then
            if(#AggroList > 0) then
                self.ParentObj:PlayAnimation("cast_heal")
                self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_force")
                self.ParentObj:PlayEffectWithArgs("WispLaunchEffect", 0.0, "WispIndex=1")

                for i=1, 4 do 
                    local target = AggroList[math.random(1, #AggroList)]
                    local targetLoc = target:GetLoc()
                    SorceryHelper.DisplaySpellRadiusWarning( targetLoc, "3.0", false, 1250, function(success)
                        PlayEffectAtLoc("StarfallEffect", targetLoc, 3)
                        -- We do a delay to trigger the damage when the animation hits the ground
                        CallFunctionDelayed(TimeSpan.FromMilliseconds(1000), function()
                            self.ParentObj:StopObjectSound("event:/magic/fire/magic_fire_wall_of_fire")
                            self.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact")
                            -- This is where the magic happens!
                            local targets = FindObjects(SearchMulti({SearchRange(targetLoc,3),SearchUser()}))

                            -- Damage our targets
                            for i=1,#targets do 
                                if( SorceryHelper.CanAoEHitTarget( targets[i], { QueueLocation = targetLoc, ParentObj = self.ParentObj } ) ) then
                                    targets[i]:SendMessage("ProcessTrueDamage", self.ParentObj, 200, true)
                                end 
                            end
                        end)
                    end)
                end
                self.Cooldowns.DoMassAoE = unixTS
            end
        end

    end,

    ExitState = function(self)
        -- Remove Wisps
        self.ParentObj:StopEffect("WispEffect1")
    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            -- Do Wisps
            self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_docility")
            self.ParentObj:PlayAnimation("cast_heal")
            self.ParentObj:PlayEffectWithArgs("WispEffect", 0.0, "WispIndex=1")

            -- Get Gate Status
            self.GateStatus = self.LevelTwoLastGate:GetObjVar("GateStatus")
            
            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            --self.Cooldowns.FightSpeech = unixTS+5;
            self.Cooldowns.SpawnSparks = 0;
            self.Cooldowns.ShootWisp = unixTS+6;
            self.Cooldowns.DoMassAoE = unixTS+10;

            -- Teleport Players to center
            States.MonolithRenmerFight.TeleportPlayersToCenter(self)

            -- Portals
            States.MonolithRenmerFight.CreatePortals(self)

            self.FightStarted = true
        end
    end,

    ForEachPlayerInArea = function(self, cb, includeDead)
        if(cb == nil) then cb = function()end end
        local players = FindObjects(SearchMulti({SearchUser(),SearchRegion("S2end",true)}))
        for i,player in pairs (players) do 
            if ( IsDead(player) ) then
                if( includeDead ) then
                    cb(player) 
                end
            else
                if( IsGod(player) and not TestMortal(player) ) then
                    -- we don't include gods
                else
                    cb(player) 
                end
            end
        end
    end,

    TeleportPlayersToCenter = function(self)
        States.MonolithRenmerFight.ForEachPlayerInArea(self, function(player)
            player:PlayEffect("TeleportFromEffect")	
            player:SetWorldPosition(Loc( -1185, 0, 17 ))
            player:SetFacing(360)
            player:PlayEffect("TeleportToEffect")						
        end)
    end,

    CreatePortals = function(self)
        if( self.Portals == nil ) then
            self.Portals = {}
            for i=1, #self.PortalLocations do 
                local portalLoc = self.PortalLocations[i]
                Create.AtLoc( "monolith_portal_renmer", portalLoc, function(portal)
                    table.insert( self.Portals,portal )
                end)
            end
            if( self.GateStatus == 1 ) then
                self.LevelTwoLastGate:SendMessage("GateClose")
            end

            Create.AtLoc( "monolith_small_gateway", Loc(-1185, 2.877, 33), function(gateway)
                table.insert( self.Portals,gateway )
            end)
            Create.AtLoc( "monolith_small_gateway", Loc(-1185, 2.877, 32), function(gateway)
                table.insert( self.Portals,gateway )
            end)
            Create.AtLoc( "monolith_small_gateway", Loc(-1185, 3.025, 31), function(gateway)
                table.insert( self.Portals,gateway )
            end)
            Create.AtLoc( "monolith_small_gateway", Loc(-1185, 3.7, 30), function(gateway)
                table.insert( self.Portals,gateway )
            end)
            Create.AtLoc( "monolith_small_gateway", Loc(-1185, 4.36, 29), function(gateway)
                table.insert( self.Portals,gateway )
            end)
            Create.AtLoc( "energy_gate", Loc(-1185, 0.114, 28.869), function(gateway)
                table.insert( self.Portals,gateway )
            end)
        end
    end,

    DestroyPortals = function(self)
        
        if( self.Portals ~= nil ) then
            for i=1, #self.Portals do 
                if( self.Portals[i] and self.Portals[i]:IsValid() ) then
                    self.Portals[i]:Destroy()    
                end
            end
        end

        if( self.GateStatus == 1 ) then
            self.LevelTwoLastGate:SendMessage("GateOpen")
        end
        
        self.Portals = nil
    end,

}

States.MonolithRenmerDeath = {
    Name = "MonolithRenmerDeath",
    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,

    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,

    Run = function(self)
        States.Death.Run(self)
        States.Death.DistributeLoot(self, "MonolithBossRenmer")
        States.MonolithRenmerDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        
        if not( self.DoEndComplete ) then
            self.DoEndComplete = true

            -- One last blast
            self.ParentObj:NpcSpeech("I was so close.")

            -- Remove gates
            States.MonolithRenmerFight.DestroyPortals(self)
        end
        
    end
}