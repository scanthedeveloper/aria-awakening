States.AwakeningBossAzuraIdle = {
    Name = "AwakeningBossAzuraIdle",

    Init = function(self)
        self.IdleCooldowns = {}
        self.SpawnLocation = self.ParentObj:GetLoc()
        self.AttackSpeedMod = 0
        self.AttackDamageMod = 0
        self.AggroRange = 10
        self.LeashDistance = self.ParentObj:GetObjVar("AI-LeashDistance") or 40
        self.WeakestPlayer = nil
        self.IgnoreAggro = true
    end,

    ShouldRun = function(self)
        return (
            (self.SpawnLocation)
            and not self.IsPathing 
            and not self.CurrentTarget 
            and self.ParentObj:GetLoc():Distance( self.SpawnLocation ) > 1 
        ) 
    end,

    ExitState = function(self)
        -- TODO: Set him facing (354)
        
    end,

    Run = function(self)
        --DebugMessage("Running Idle!")
        self.FightStarted = false

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

        -- Path back to spawn location
        if( self.ParentObj:GetLoc():Distance( self.SpawnLocation ) > 1 ) then
            self.PathTo(self.SpawnLocation, 8)
        end

    end,

    EnterState = function(self)
        States.Aggro.ClearAggro(self)
        self.AttackSpeedMod = 0;
        self.AttackDamageMod = 0;
        self.WeakestPlayer = nil
        SetMobileMod(self.ParentObj, "AttackSpeedTimes", "Enrage", nil)
        SetMobileMod(self.ParentObj, "AttackTimes", "Enrage", nil)
    end,
}

States.AwakeningBossAzuraFight = {
    Name = "AwakeningBossAzuraFight",

    Init = function(self)
        States.Fight.Init(self)
        self.Location = self.ParentObj:GetLoc()
        self.FightStarted = false
        self.Cooldowns = {}
        self.Cooldowns.Enrage = 0;
    end,

    ShouldRun = function(self)
        return ( States.Fight.ShouldRun(self) and self.CurrentTarget ~= nil )
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    Run = function(self)
        self.Location = self.ParentObj:GetLoc()

        -- call base fight
        States.Fight.Run(self)

        -- Have we moved too far away from our spawn location?
        if( self.Location:Distance( self.SpawnLocation ) > 23 ) then
            self.CurrentTarget = nil
            self.ParentObj:ClearPathTarget()
            self.IsPathing = false
        end
        
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Start Fight
        States.AwakeningBossAzuraFight.StartFight(self)

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))
        local healthPercent = GetCurHealth(self.ParentObj) / GetMaxHealth(self.ParentObj)

        -- Enrage
        if( GroupAI.CooldownCheck( self.Cooldowns, "Enrage", unixTS, 30 ) ) then
            self.ParentObj:PlayEffectWithArgs("WarriorEnrage",0.0,"Bone=Head")
            self.ParentObj:PlayObjectSound("event:/magic/martial/magic_martial_enrage")
            self.AttackSpeedMod = math.min(self.AttackSpeedMod + 0.05, 0.25)
            self.AttackDamageMod = math.min(self.AttackDamageMod + 0.05, 0.80)
            SetMobileMod(self.ParentObj, "AttackSpeedTimes", "Enrage", self.AttackSpeedMod)
            SetMobileMod(self.ParentObj, "AttackTimes", "Enrage", self.AttackDamageMod)
            CallFunctionDelayed(TimeSpan.FromSeconds(5),function()
                self.ParentObj:StopEffect("WarriorEnrage", 3.0)
            end)
            self.Cooldowns.Enrage = unixTS
        end

        -- We always want to target the weakest player (health wise)
        local awakenPlayers = FindObjects(SearchMulti{SearchRegion("FrozenTundraColiseumArena"),SearchPlayerInRange(80)})
        local lowestHP = 5000
        for i=1, #awakenPlayers do
            local player = awakenPlayers[i]
            if( not IsDead( player ) ) then
                local playerHP = GetMaxHealth( player )
                if( playerHP < lowestHP ) then
                    self.WeakestPlayer = player
                end
            end
        end

        if( self.WeakestPlayer and self.WeakestPlayer ~= self.CurrentTarget and self.WeakestPlayer:IsValid() and self.WeakestPlayer:IsInRegion("FrozenTundraColiseumArena") and self.ValidCombatTarget(self.WeakestPlayer) ) then
            self.SetTarget(self.WeakestPlayer)
        end

    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            self.Cooldowns.Enrage = unixTS+15;
            self.FightStarted = true
        end
    end,

}

States.AwakeningBossAzuraDeath = {
    Name = "AwakeningBossAzuraDeath",

    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,

    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,

    Run = function(self)
        States.Death.Run(self)
        States.Death.DistributeLoot(self, "AwakeningAzura")
        States.AwakeningBossAzuraDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        
        if not( self.DoEndComplete ) then
            self.DoEndComplete = true
        end
        
    end
}