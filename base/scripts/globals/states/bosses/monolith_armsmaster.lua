local DEBUG_AI = false

function shuffle(array)
    -- fisher-yates
    local output = { }
    local random = math.random

    for index = 1, #array do
        local offset = index - 1
        local value = array[index]
        local randomIndex = offset*random()
        local flooredIndex = randomIndex - randomIndex%1

        if flooredIndex == offset then
            output[#output + 1] = value
        else
            output[#output + 1] = output[flooredIndex + 1]
            output[flooredIndex + 1] = value
        end
    end

    return output
end

function DebugAI( string )
    if( DEBUG_AI ) then
        DebugMessage( string )
    end
end

States.MonolithArmsmasterIdle = {
    Name = "MonolithArmsmasterIdle",
    Init = function(self)
        self.IdleCooldowns = {}
        self.Spawner = self.ParentObj:GetObjVar("Spawner")
        self.SpawnerLoc = self.Spawner:GetLoc()

    end,
    ShouldRun = function(self)
        return (
            (self.Spawner and self.SpawnerLoc)
            -- and not self.IsPathing 
            and not self.CurrentTarget 
            --and self.ParentObj:GetLoc() ~= self.SpawnerLoc
        ) 
    end,
    ExitState = function(self)
        -- TODO: Set him facing (354)
        DebugAI( "ExitState : MonolithArmsmasterIdle" )
    end,
    Run = function(self)
        DebugAI( "Run : MonolithArmsmasterIdle" )

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

        -- Path back to spawn location
        if( self.ParentObj:GetLoc() ~= self.SpawnerLoc ) then
            self.PathTo(self.SpawnerLoc, 8)
        end

    end,
    EnterState = function(self)
        DebugAI( "EnterState : MonolithArmsmasterIdle" )
        
        self.FightStarted = false
        self.FightPhase = 0

        -- Remove gates
        if( self.Gateways ~= nil ) then
            States.MonolithArmsmasterFight.DestroyGateways(self)
        end

        States.Aggro.ClearAggro(self)
    end,
}

States.MonolithArmsmasterFight = {
    Name = "MonolithArmsmasterFight",
    Init = function(self)
        States.Fight.Init(self)
        self.AggroRange = 30
        self.Location = self.ParentObj:GetLoc()
        self.FightStarted = false
        self.SpeechDone = false
        self.Cooldowns = {}
        self.NorthPortalLoc = Loc(-1185.008, -0.0018, -46.19)
        self.SouthPortalLoc = Loc(-1185.008, -0.0018, -70.61)
        self.SpawnEnemyLocs = {
            Loc(-1185.008, -0.0018, -46.19),
            Loc(-1185.008, -0.0018, -70.61)
        }
        self.FightPhase = 0
        self.LastFightPhase = 0
        self.IsPathing = false
        self.ExecuteSpeech = 
        {
            "Let us hope you next batch don't suffer the same fate.",
            "I had such high hopes for you.",
            "It is better you die now than bring more shame upon this order.",
            "Let us see, who is next shall we?",
        }

        self.Cooldowns.FightSpeech = 0;
        
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,
    Run = function(self)
        DebugAI( "Run : MonolithArmsmasterFight" )

        if ( not self.ValidCombatTarget(self.CurrentTarget) ) then
            self.SetTarget(nil)
        end

        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        DebugAI( "Run -- 1 : MonolithArmsmasterFight" )

        -- Start Fight
        States.MonolithArmsmasterFight.StartFight(self)
        
        if( self.SpeechDone ) then
            
            -- Get our currect timestamp
            local unixTS = os.time(os.date("!*t"))

            if( GroupAI.CooldownCheck( self.Cooldowns, "DoFormation", unixTS, 13 ) ) then
                local formationRNG = math.random( 1, 99 )

                -- We are making the formation picked random so they don't see a pattern
                if( formationRNG <= 25 ) then -- 25% chance
                    States.MonolithArmsmasterFight.DoCombatFormation(self)
                elseif( formationRNG <= 63 ) then -- 38% chance
                    States.MonolithArmsmasterFight.DoSpreadFormation(self)
                else -- 37% chance
                    States.MonolithArmsmasterFight.DoGroupFormation(self)
                end

                States.MonolithArmsmasterFight.ForEachPlayerOnPlatform(self, function(player)
                    self.ParentObj:SendMessage("AddAggro", player, 20)						
                end)

                self.Cooldowns.DoFormation = unixTS
            end
        end
        
    end,

    DoGroupFormation = function(self)

        States.MonolithArmsmasterFight.MarkPlayerForGroupFormationLeader(self)
        self.ParentObj:NpcSpeech("Group Formation!")

        -- Give them 5 seconds to get to their leader
        CallFunctionDelayed(TimeSpan.FromSeconds(5),function()
            States.MonolithArmsmasterFight.ForEachPlayerOnPlatform(self, function(player)
                PlayEffectAtLoc("DeathwaveEffect",Loc( -1185, 0, -65.90 ))
                PlayEffectAtLoc("DeathwaveEffect",Loc( -1185, 0, -51.82 ))
                PlayEffectAtLoc("DeathwaveEffect",Loc( -1192.15, 0, -59 ))
                PlayEffectAtLoc("DeathwaveEffect",Loc( -1178, 0, -59 ))

                if not( HasMobileEffect(player, "ArmsmasterGroupFormation") ) then
                    ForeachMobileAndPet( player, function(mobile) 
                        local health = GetCurHealth(mobile)
                        mobile:SendMessage("ProcessTrueDamage", self.Parent, ( health * 0.75 ), true)
                    end)
                end

                player:SendMessage("RemoveGroupFormation")
            end)
        end)

    end,

    DoSpreadFormation = function(self)
        self.ParentObj:NpcSpeech("Spread Formation!")
        States.MonolithArmsmasterFight.ForEachPlayerOnPlatform(self, function(player)
            player:SendMessage("StartMobileEffect", "ArmsmasterSpreadFormation", self.ParentObj)
        end)

        CallFunctionDelayed(TimeSpan.FromSeconds(5),function()
            States.MonolithArmsmasterFight.ForEachPlayerOnPlatform(self, function(player)
                player:SendMessage("RemoveSpreadFormation")
            end)
        end)

    end,

    DoCombatFormation = function(self)
        self.ParentObj:NpcSpeech("Combat Formation!")
        local spawnLoc = self.SpawnEnemyLocs[ math.random( 1,2 ) ]
        Create.AtLoc( "construct_unstable", spawnLoc, nil)
    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            self.FightStarted = true
            self.SpeechDone = false
            DebugAI( "StartFight : MonolithArmsmasterFight" )

            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            self.Cooldowns.FightSpeech = unixTS+5;
            self.Cooldowns.DoFormation = unixTS;
            self.IsPathingToDest = false

            -- Teleport Players to center
            States.MonolithArmsmasterFight.TeleportPlayersToCenter(self)

            -- Gateways
            States.MonolithArmsmasterFight.CreateGateways(self)

            

            self.ParentObj:NpcSpeech("So you wish to complete your exams?")

            CallFunctionDelayed(TimeSpan.FromSeconds(4),function()
                self.ParentObj:NpcSpeech("Very well, let us test some formation drills.")
            end)

            CallFunctionDelayed(TimeSpan.FromSeconds(9),function()
                self.ParentObj:NpcSpeech("Let the exam begin!")
                self.SpeechDone = true
            end)

        end
    end,


    MarkPlayersForExecution = function( self )
        local players = {}
        local markedCount = 0
        States.MonolithArmsmasterFight.ForEachPlayerOnPlatform(self, function(player) table.insert( players, player) end)

        -- Shuffle our list
        players = shuffle( players )

        -- Determine how many players should be marked
        if( #players > 20 ) then markedCount = 5
        elseif( #players >= 15 ) then markedCount = 3 
        elseif( #players >= 10 ) then markedCount = 2 
        elseif( #players <= 9 ) then markedCount = 1 end

        -- Mark the players
        for i=1, markedCount do 
            if( players[i] and players[i]:IsValid() ) then
                players[i]:SendMessage("StartMobileEffect", "ArmsmasterExecutionMarked", self.ParentObj)
            end
        end
    end,

    MarkPlayerForGroupFormationLeader = function( self )
        local players = {}
        local markedCount = 0
        States.MonolithArmsmasterFight.ForEachPlayerOnPlatform(self, function(player) table.insert( players, player) end)

        -- Shuffle our list
        players = shuffle( players )

        -- Mark the players
        for i=1, #players do 
            if( players[i] and players[i]:IsValid() and not IsDead(players[i]) ) then
                players[i]:SendMessage("StartMobileEffect", "ArmsmasterGroupFormation", self.ParentObj, {IsLeader = true})
                return
            end
        end
    end,

    ExecuteMarkedPlayers = function(self)
        States.MonolithArmsmasterFight.ForEachPlayerOnPlatform(self, function(player)
            player:SendMessage("DoArmsmasterExecute")
        end)
    end,

    RemoveMarkedPlayersEffect = function(self)
        States.MonolithArmsmasterFight.ForEachPlayerOnPlatform(self, function(player)
            player:SendMessage("RemoveArmsmasterExecute")
        end)
    end,

    ForEachPlayerOnPlatform = function(self, cb, includeDead)
        if(cb == nil) then cb = function()end end
        local players = FindObjects(SearchMulti({SearchUser(),SearchRange(self.SpawnerLoc,16)}))
        for i,player in pairs (players) do 
            if ( IsDead(player) ) then
                if( includeDead ) then
                    cb(player) 
                end
            else
                if( IsGod(player) and not TestMortal(player) ) then
                    -- we don't include gods
                else
                    cb(player) 
                end
            end
        end
    end,

    TeleportPlayersToCenter = function(self)
        States.MonolithArmsmasterFight.ForEachPlayerOnPlatform(self, function(player)
            player:PlayEffect("TeleportFromEffect")	
            player:SetWorldPosition(self.SpawnerLoc)
            player:SetFacing(360)
            player:PlayEffect("TeleportToEffect")
            self.ParentObj:SendMessage("AddAggro", player, 1000)						
        end)
    end,


    CreateGateways = function(self)
        if( self.Gateways == nil ) then
            self.Gateways = {}

            -- North Gateway
            Create.AtLoc( "monolith_small_gateway", Loc(-1185.11, 4.277, -43.32), function(gate)
                table.insert( self.Gateways,gate )
            end)
            Create.AtLoc( "energy_gate", Loc(-1185.11, 0, -43.32), function(gate)
                table.insert( self.Gateways,gate )
            end)

            -- South Gateway
            Create.AtLoc( "monolith_small_gateway", Loc(-1185.11, 4.277, -74.71), function(gate)
                table.insert( self.Gateways,gate )
            end)
            Create.AtLoc( "energy_gate", Loc(-1185.11, 0, -74.71), function(gate)
                table.insert( self.Gateways,gate )
            end)

            -- East Gateway
            Create.AtLoc( "monolith_small_gateway", Loc(-1169.29, 4.277, -58.94), function(gate)
                gate:SetRotation(Loc(0,90,0))
                table.insert( self.Gateways,gate )
            end)
            Create.AtLoc( "energy_gate", Loc(-1169.29, 0, -58.94), function(gate)
                gate:SetRotation(Loc(0,90,0))
                table.insert( self.Gateways,gate )
            end)

            -- West Gateway
            Create.AtLoc( "monolith_small_gateway", Loc(-1200.67, 4.277, -58.94), function(gate)
                gate:SetRotation(Loc(0,-90,0))
                table.insert( self.Gateways,gate )
            end)
            Create.AtLoc( "energy_gate", Loc(-1200.67, 0, -58.94), function(gate)
                gate:SetRotation(Loc(0,-90,0))
                table.insert( self.Gateways,gate )
            end)

        end
    end,

    DestroyGateways = function(self)
        DebugAI( "DestroyGateways : MonolithArmsmasterFight" )
        if( self.Gateways ~= nil ) then
            for i=1, #self.Gateways do 
                self.Gateways[i]:Destroy()
            end
        end
        self.Gateways = nil
        States.MonolithArmsmasterFight.RemoveMarkedPlayersEffect(self)
    end,

}

States.MonolithArmsmasterDeath = {
    Name = "MonolithArmsmasterDeath",
    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,
    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,
    Run = function(self)
        DebugAI( "Run : MonolithArmsmasterDeath" )
        States.Death.Run(self)
        States.Death.DistributeLoot(self, "MonolithBossFinkle")
        States.MonolithArmsmasterDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        if not( self.DoEndComplete ) then
            self.DoEndComplete = true
            -- One last blast
            self.ParentObj:NpcSpeech("Ahh, well done recruits.")
            -- Remove gates
            States.MonolithArmsmasterFight.DestroyGateways(self)
        end
        
    end
}