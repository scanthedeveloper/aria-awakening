States.WendigoBossIdle = {
    Name = "WendigoBossIdle",

    Init = function(self)
        self.IdleCooldowns = {}
        self.SpawnerLoc = self.ParentObj:GetLoc()
    end,

    ShouldRun = function(self)
        return (
            (self.SpawnerLoc)
            and not self.IsPathing 
            and not self.CurrentTarget 
        ) 
    end,

    ExitState = function(self)
        -- TODO: Set him facing (354)
    end,

    Run = function(self)

        self.FightStarted = false

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

        -- Path back to spawn location
        if( self.ParentObj:GetLoc() ~= self.SpawnerLoc ) then
            self.PathTo(self.SpawnerLoc, 8)
        end

    end,

    EnterState = function(self)
        SetMobileMod(self.ParentObj, "AttackSpeedTimes", "Enrage", nil)
        States.Aggro.ClearAggro(self)
    end,
}

States.WendigoBossFight = {
    Name = "WendigoBossFight",

    Init = function(self)
        States.FightAdvanced.Init(self)
        self.Location = self.ParentObj:GetLoc()
        self.FightStarted = false
        self.AggroRange = 10
        self.Cooldowns = {}
        self.Cooldowns.Poison = 0;
    end,

    ShouldRun = function(self)
        return States.FightAdvanced.ShouldRun(self)
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
        SetMobileMod(self.ParentObj, "AttackSpeedTimes", "Enrage", 0.50)
        self.ParentObj:PlayEffect("SlimeTrailEffect")
    end,

    Run = function(self)
        self.Location = self.ParentObj:GetLoc()

        -- call base fight
        States.FightAdvanced.Run(self)

        -- Have we moved too far away from our spawn location?
        if( self.Location:Distance( self.SpawnerLoc ) > 25 ) then
            self.CurrentTarget = nil
            self.ParentObj:ClearPathTarget()
            self.IsPathing = false
        end
        
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Start Fight
        States.WendigoBossFight.StartFight(self)

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))
        local masterHealth = GetCurHealth(self.ParentObj) / GetMaxHealth(self.ParentObj)

        -- Posion
        if( GroupAI.CooldownCheck( self.Cooldowns, "Poison", unixTS, math.random( 6, 10 ) ) ) then
            if not( HasMobileEffect(self.CurrentTarget, "Poison") ) then
                self.ParentObj:PlayEffect("SpitAcidEffect")
                self.CurrentTarget:SendMessage("StartMobileEffect", "Poison", self.ParentObj, {
                    MinDamage = 100,
                    MaxDamage = 150,
                    PulseMax = 3,
                    PulseFrequency = TimeSpan.FromSeconds(1)
                })
            end
            self.Cooldowns.Poison = unixTS
        end

        self.ParentObj:PlayEffectWithArgs("SlimeTrailEffect",1.0,"Bone=Head")

    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            self.Cooldowns.Poison = unixTS + 4
            self.FightStarted = true
        end
    end,

}

States.WendigoBossDeath = {
    Name = "WendigoBossDeath",

    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,

    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,

    Run = function(self)
        States.Death.Run(self)
        States.Death.DistributeLoot(self, "TundraWendigo")
        States.WendigoBossDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        if not( self.DoEndComplete ) then
            self.DoEndComplete = true
        end
    end
}