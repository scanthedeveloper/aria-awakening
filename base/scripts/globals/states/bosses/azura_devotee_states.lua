States.AzuraDevoteeIdle = {
    Name = "AzuraDevoteeIdle",

    Init = function(self)
        self.IdleCooldowns = {}
        self.SpawnLocation = self.ParentObj:GetLoc()

        self.EngagementRegion = "FrozenTundraAzuraTemple";
        self.PlayersInRegion = {}

    end,

    ShouldRun = function(self)
        return ( #self.PlayersInRegion == 0 ) 
    end,

    ExitState = function(self)
        -- TODO: Set him facing (354) 
    end,

    Run = function(self)
        States.AzuraDevoteeFight.UpdatePlayersInRegion(self)
        
        self.FightStarted = false

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

    end,

    EnterState = function(self)
        States.Aggro.ClearAggro(self)
    end,
}

States.AzuraDevoteeFight = {
    Name = "AzuraDevoteeFight",

    Init = function(self)
        States.Fight.Init(self)
        self.Location = self.ParentObj:GetLoc()
        self.FightStarted = false

        self.Cooldowns = {}
        self.Cooldowns.AoELightning = 0;
    end,

    ShouldRun = function(self)
        return ( #self.PlayersInRegion > 0 )
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    ExitState = function(self)
        if ( IsDead( self.ParentObj ) ~= true ) then
            PlayEffectAtLoc("TeleportFromEffect",self.Location)
            self.ParentObj:Destroy()
        end
    end,

    Run = function(self)
        States.AzuraDevoteeFight.UpdatePlayersInRegion(self)
        self.Location = self.ParentObj:GetLoc()

        -- call base fight
        States.Fight.Run(self)

        -- Have all the players left the region?
        if( #self.PlayersInRegion == 0 ) then
            self.CurrentTarget = nil
            self.ParentObj:ClearPathTarget()
            self.IsPathing = false
            return
        else
            if( self.CurrentTarget == nil or ( self.CurrentTarget:IsValid() and not self.CurrentTarget:IsInRegion(self.EngagementRegion) ) ) then
                self.CurrentTarget = self.PlayersInRegion[ math.random( 1, #self.PlayersInRegion ) ] or nil
            end
        end
        
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Start Fight
        States.AzuraDevoteeFight.StartFight(self)

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))
        local healthPercent = GetCurHealth(self.ParentObj) / GetMaxHealth(self.ParentObj)

        -- AoELightning
        if( GroupAI.CooldownCheck( self.Cooldowns, "AoELightning", unixTS, 6 ) ) then

            self.Cooldowns.AoELightning = unixTS    
        end

    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            self.ParentObj:NpcSpeech("For the honor of Azura!")
            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            self.Cooldowns.AoELightning = unixTS+1;
            self.FightStarted = true
        end
    end,

    UpdatePlayersInRegion = function( self )
        local found = FindObjects(SearchMulti({SearchUser(),SearchRegion(self.EngagementRegion,true)}))
        self.PlayersInRegion = {}
        for i,player in pairs (found) do 
            if( not IsDead(player) ) then
                table.insert( self.PlayersInRegion, player )
            end
        end
    end,

}

States.AzuraDevoteeDeath = {
    Name = "AzuraDevoteeDeath",

    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,

    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,

    Run = function(self)
        States.Death.Run(self)
        States.AzuraDevoteeDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        self.ParentObj:NpcSpeech("I am sorry Azura, I have failed you.")

        if not( self.DoEndComplete ) then
            self.DoEndComplete = true
        end
        
    end
}