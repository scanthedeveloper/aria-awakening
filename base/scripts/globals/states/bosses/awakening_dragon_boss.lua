States.AwakeningBossDragonIdle = {
    Name = "AwakeningBossDragonIdle",

    Init = function(self)
        self.IdleCooldowns = {}
        self.SpawnerLoc = self.ParentObj:GetLoc()

    end,

    ShouldRun = function(self)
        return (
            (self.SpawnerLoc)
            and not self.IsPathing 
            and not self.CurrentTarget 
            and self.ParentObj:GetLoc() ~= self.SpawnerLoc
        ) 
    end,

    ExitState = function(self)
        -- TODO: Set him facing (354)
        
    end,

    Run = function(self)

        self.FightStarted = false

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

        -- Path back to spawn location
        if( self.ParentObj:GetLoc() ~= self.SpawnerLoc ) then
            self.PathTo(self.SpawnerLoc, 8)
        end

    end,

    EnterState = function(self)
        States.Aggro.ClearAggro(self)
    end,
}

States.AwakeningBossDragonFight = {
    Name = "AwakeningBossDragonFight",

    Init = function(self)
        States.Fight.Init(self)
        self.Location = self.ParentObj:GetLoc()
        self.FightStarted = false
        self.AggroRange = 10
        self.Cooldowns = {}
        self.Cooldowns.DragonFire = 0;
    end,

    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    Run = function(self)
        self.Location = self.ParentObj:GetLoc()

        -- call base fight
        States.Fight.Run(self)

        -- Have we moved too far away from our spawn location?
        if( self.Location:Distance( self.SpawnerLoc ) > 25 ) then
            self.CurrentTarget = nil
            self.ParentObj:ClearPathTarget()
            self.IsPathing = false
        end
        
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Start Fight
        States.AwakeningBossDragonFight.StartFight(self)

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))
        local healthPercent = GetCurHealth(self.ParentObj) / GetMaxHealth(self.ParentObj)

        -- DragonFire
        if( GroupAI.CooldownCheck( self.Cooldowns, "DragonFire", unixTS, math.random( 8,12 ) ) ) then
            DebugMessage("DragonFire")
            self.ParentObj:SendMessage("StartMobileEffect", "DragonFire", self.CurrentTarget, {PulseFrequency = TimeSpan.FromSeconds(1),PulseCount = 5,MinDamage = 30,MaxDamage = 60,})
            self.Cooldowns.DragonFire = unixTS
        end

    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            self.FightStarted = true
        end
    end,

}

States.AwakeningBossDragonDeath = {
    Name = "AwakeningBossDragonDeath",

    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,

    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,

    Run = function(self)
        States.Death.Run(self)
        States.Death.DistributeLoot(self, "AwakeningDragon")
        States.AwakeningBossEntDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        
        if not( self.DoEndComplete ) then
            self.DoEndComplete = true
        end
        
    end
}