States.YewardIdle = {
    Name = "YewardIdle",
    Init = function(self)
        self.IdleCooldowns = {}
        self.IdleSpeeches = 
        {
            "By Xor we shall see the rise of the Monolith once more.",
            "Monolithe mor we se of the rise.",
            "Monorisee of th Xolise!",
            "It must be perfect for Jianna.",
        }

        self.IdleCooldowns.Speech = 0;
    end,
    ShouldRun = function(self)
        return (not self.IsPathing)
    end,
    Run = function(self)
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))

        -- Speech
        if( GroupAI.CooldownCheck( self.IdleCooldowns, "Speech", unixTS, 30 ) ) then
            self.ParentObj:PlayAnimation("fabrication")
            self.ParentObj:NpcSpeech( self.IdleSpeeches[math.random( 1,#self.IdleSpeeches )] )
            CallFunctionDelayed(TimeSpan.FromSeconds(4),function() self.ParentObj:PlayAnimation("idle") end)
            self.IdleCooldowns.Speech = unixTS
        end

    end,
}

States.YewardFight = {
    Name = "YewardFight",
    Init = function(self)
        States.Fight.Init(self)
        self.Cooldowns = {}
        self.FightSpeech = 
        {
            "Why am I not left in peace?!?",
            "My work, it is important! You must cease this distraction.",
            "The sparks! They are of the Xor and I will harness them!",
            "Fools leave me to my studies!"
        }

        self.Cooldowns.FightSpeech = 0;
        self.Cooldowns.SpawnSparks = os.time(os.date("!*t"))
        self.Cooldowns.ShootWisp = 0;

        self.SpawnSparksInterval = 10
    end,
    ExitState = function(self)
        self.SpawnSparksInterval = 10
        self.ParentObj:StopEffect("WispEffect1")
    end,
    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
        self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_docility")
        self.ParentObj:PlayAnimation("cast_heal")
        self.ParentObj:PlayEffectWithArgs("WispEffect", 0.0, "WispIndex=1")
    end,
    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,
    Run = function(self)
        -- call base fight
        States.Fight.Run(self)
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))

        -- Speech
        if( GroupAI.CooldownCheck( self.Cooldowns, "FightSpeech", unixTS, 30 ) ) then
            self.ParentObj:NpcSpeech( self.FightSpeech[math.random( 1,#self.FightSpeech )] )
            self.Cooldowns.FightSpeech = unixTS
        end

        -- Shoot Wisps
        if( GroupAI.CooldownCheck( self.Cooldowns, "ShootWisp", unixTS, 12 ) ) then
            if(self.CurrentTarget) then
                self.ParentObj:PlayAnimation("cast_heal")
                self.ParentObj:PlayObjectSound("event:/magic/bard/riddle_of_force")
                
                SorceryHelper.SendWispToTargetMobile( {1}, self.ParentObj, self.CurrentTarget, 300, function(success)
                    if( success ) then
                        self.CurrentTarget:PlayObjectSound("event:/magic/air/magic_air_lightning_impact")
                        self.CurrentTarget:SendMessage("ProcessTrueDamage", self.ParentObj, 100, true)
                        self.CurrentTarget:SendMessage("StartMobileEffect", "Hamstring", self.ParentObj, { Duration = TimeSpan.FromSeconds(8), Modifier = -0.5, } )
                    end
                end)

                self.Cooldowns.ShootWisp = unixTS
            end
        end

        -- Spawn Sparks
        if( GroupAI.CooldownCheck( self.Cooldowns, "SpawnSparks", unixTS, self.SpawnSparksInterval ) ) then
            local AggroList = {}
            for mobileObj, threat in pairs(self.AggroList) do 
                if( mobileObj and mobileObj:IsValid() and not IsDead(mobileObj) ) then
                    table.insert( AggroList, mobileObj )
                end
            end

            if(#AggroList > 0) then
                
                local sparkTargOne = AggroList[math.random( 1,#AggroList )]
                local sparkTargTwo = AggroList[math.random( 1,#AggroList )]

                Create.AtLoc( "charged_spark", Loc(-490.92, 0, -153.79), function(spark) 
                    CallFunctionDelayed(TimeSpan.FromMilliseconds(60), function() 
                        spark:SendMessage("SetCurrentTarget", sparkTargOne)
                    end) 
                    
                end)

                Create.AtLoc( "charged_spark", Loc(-509.03, 0, -153.96), function(spark) 
                    CallFunctionDelayed(TimeSpan.FromMilliseconds(60), function() 
                        spark:SendMessage("SetCurrentTarget", sparkTargTwo)
                    end) 
                end)

                self.SpawnSparksInterval = math.max(5, (self.SpawnSparksInterval - 1))

                self.Cooldowns.SpawnSparks = unixTS
            end
            
        end

    end,
}

States.YewardDeath = {
    Name = "YewardDeath",
    Init = function(self)
        States.Death.Init(self)
    end,
    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,
    Run = function(self)
        States.Death.Run(self)
        States.Death.DistributeLoot(self, "MonolithBossSkullcarver")
        return true
    end,
}

States.ChargedSparkFollow = {
    Name = "ChargedSparkFollow",
    Init = function(self)
        States.ChargedSparkFollow.ClearPath(self)
        self.IsPathing = false

        CallFunctionDelayed(TimeSpan.FromSeconds(30),function()
            self.ParentObj:SendMessage("ProcessTrueDamage", self.ParentObj, 10000, true)
        end )

        self.Spawned = os.time(os.date("!*t"))
    end,
    ShouldRun = function(self)

        -- If our target isn't valid we need get a new one
        if( self.CurrentTarget == nil or IsDead(self.CurrentTarget) ) then
            local inRange = FindObjects(SearchMulti({SearchPlayerInRange(20)}))
            local targets = {}

            for i,mobile in pairs(inRange) do 
                if( ValidCombatTarget(self.ParentObj, mobile, true) ) then
                    table.insert( targets,mobile )
                end
            end

            if( self.CurrentTarget == nil and targets and #targets > 0 ) then
                local newTarget = targets[math.random( 1,#targets )]
                if( ValidCombatTarget(self.ParentObj, newTarget, true) ) then
                    self.CurrentTarget = newTarget
                    self.ParentObj:SendMessage("SetCurrentTarget", newTarget)
                end
            end
        end

        return (
            (self.CurrentTarget and self.CurrentTarget:IsValid())  -- Is it valid?
        )
    end,
    Run = function(self)

        if( CanPathTo( self.ParentObj:GetLoc(), self.CurrentTarget:GetLoc() ) ) then
            self.CurrentTarget:PlayEffect("AvoidArrowEffect")

            if ( IsMounted(self.CurrentTarget) ) then
                -- go mount speed
                States.SummonFollow.PathToTarget(
                    self,
                    self.CurrentTarget,
                    1,
                    ServerSettings.Pets.Follow.Speed.Mounted
                )
            else
                States.SummonFollow.PathToTarget(
                    self,
                    self.CurrentTarget,
                    1,
                    ServerSettings.Pets.Follow.Speed.OnFoot
                )
            end

            if( self.ParentObj:DistanceFrom(self.CurrentTarget) <= 3 ) then
                self.ParentObj:SendMessage("ProcessTrueDamage", self.ParentObj, 10000, true)
            end
        else
            self.CurrentTarget = nil
        end
        
    end,

    ClearPath = function( self )
        if ( self.IsPathing ) then
            self.ParentObj:ClearPathTarget()
            self.IsPathing = false
        end
    end,

    PathToTarget = function(self, target, distance, speed)
        States.SummonFollow.ClearPath(self)
        if not ( target ) then return end
        self.ParentObj:PathToTarget(target, distance, speed)
        self.IsPathing = true
    end
}