States.AzureHighpriestIdle = {
    Name = "AzureHighpriestIdle",

    Init = function(self)
        self.IdleCooldowns = {}
        self.SpawnLocation = self.ParentObj:GetLoc()
        --self.AggroRange = 10
        --self.LeashDistance = 2
        --self.IgnoreAggro = true
        --self.PositionLocked = true

        self.EngagementRegion = "FrozenTundraAzuraTemple";
        self.PlayersInRegion = {}
        self.DevoteesInRegion = {}

    end,

    ShouldRun = function(self)
        return ( #self.PlayersInRegion == 0 ) 
    end,

    ExitState = function(self)
        -- TODO: Set him facing (354)
        
    end,

    Run = function(self)
        States.AzuraHighpriestFight.UpdatePlayersInRegion(self)
        
        self.FightStarted = false

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

    end,

    EnterState = function(self)
        self.ParentObj:SetFacing(0)
        States.Aggro.ClearAggro(self)
    end,
}

States.AzuraHighpriestFight = {
    Name = "AzuraHighpriestFight",

    Init = function(self)
        States.Fight.Init(self)
        self.Location = self.ParentObj:GetLoc()
        self.FightStarted = false
        
        self.PortalLocations = { 
            Loc(1330.84, 0, 1968.04)
            , Loc(1344.36, 0, 1968.73)
        }

        self.Portals = nil
        
        self.Cooldowns = {}
        self.Cooldowns.AoELightning = 0;
        self.Cooldowns.SpawnDevotees = 0;
        self.Cooldowns.DevoteeHealPulse = 0;

        self.Symbols = { "PrimedEarth2", "PrimedFire2", "PrimedWater2" }
        self.MySymbol = nil
    end,

    ShouldRun = function(self)
        return ( #self.PlayersInRegion > 0 )
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    ExitState = function(self)
        SetMobileMod(self.ParentObj, "HealthRegenPlus", "DevoteeRegen", nil)
        States.AzuraHighpriestFight.DestroyPortals(self)
    end,

    Run = function(self)
        States.AzuraHighpriestFight.UpdatePlayersInRegion(self)
        self.Location = self.ParentObj:GetLoc()

        -- call base fight
        --States.Fight.Run(self)

        -- Have all the players left the region?
        if( #self.PlayersInRegion == 0 ) then
            self.CurrentTarget = nil
            self.ParentObj:ClearPathTarget()
            self.IsPathing = false
        else
            self.CurrentTarget = self.PlayersInRegion[1]
        end
        
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Set regen
        SetMobileMod(self.ParentObj, "HealthRegenPlus", "DevoteeRegen", ( #self.DevoteesInRegion * 10) or nil)

        -- Face target
        self.ParentObj:SetFacing(self.Location:YAngleTo(self.CurrentTarget:GetLoc()))

        -- Start Fight
        States.AzuraHighpriestFight.StartFight(self)

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))
        local healthPercent = GetCurHealth(self.ParentObj) / GetMaxHealth(self.ParentObj)

        -- AoELightning
        if( GroupAI.CooldownCheck( self.Cooldowns, "AoELightning", unixTS, math.random( 6, 9 ) ) ) then
            self.ParentObj:PlayAnimation("cast_heal")
            self.ParentObj:PlayEffectWithArgs("RadiationAuraEffect",2.0)
            self.ParentObj:NpcSpeech("Azura strike you down!")
            
            CallFunctionDelayed(TimeSpan.FromMilliseconds(500), function()
                States.AzuraHighpriestFight.ForeachPlayerInRegion(self, function(playerObj)
                    playerObj:PlayEffect("LightningCloudEffect")
                    playerObj:PlayObjectSound("event:/magic/air/magic_air_lightning_impact")
                    playerObj:SendMessage("ProcessTypeDamage", self.ParentObj, math.random( 40, 70 ), "MAGIC", false, false)
                end)
                
            end)

            self.Cooldowns.AoELightning = unixTS    
        end

        -- SpawnDevotees
        if( GroupAI.CooldownCheck( self.Cooldowns, "SpawnDevotees", unixTS, 20 ) ) then
            for i=1, #self.PortalLocations do 
                local portalLoc = self.PortalLocations[i]
                Create.AtLoc( "azura_devotee", portalLoc, function(devotee) 

                end)
            end
            self.Cooldowns.SpawnDevotees = unixTS    
        end

        -- DevoteeHealPulse
        if( GroupAI.CooldownCheck( self.Cooldowns, "DevoteeHealPulse", unixTS, 5 ) ) then
            if( #self.DevoteesInRegion > 0 ) then
                self.ParentObj:PlayEffect("PotionHealEffect")
            end
            self.Cooldowns.DevoteeHealPulse = unixTS    
        end

    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            States.AzuraHighpriestFight.CreatePortals(self)
            self.ParentObj:NpcSpeech("You dare step foot on this sacred ground. It will be your doom.")
            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            self.Cooldowns.AoELightning = unixTS + 5;
            self.Cooldowns.SpawnDevotees = unixTS - 15;
            self.Cooldowns.DevoteeHealPulse = unixTS;


            self.FightStarted = true
        end
    end,

    ChooseSymbol = function(self)

        self.MySymbol = math.random( 1, #self.Symbols )
        self.PlayerSymbol = self.MySymbol

        ForeachPlayerInRegion( self, function( playerObj ) 
            playerObj:SendMessage("StartMobileEffect", "HighPriestMark", self.ParentObj, { Symbol = self.Symbols[ self.PlayerSymbol ] })
            self.PlayerSymbol = self.PlayerSymbol + 1
        end)

        if( self.PlayerSymbol > #self.Symbols ) then
            self.PlayerSymbol = 1
        end

    end,

    UpdatePlayersInRegion = function( self )
        local found = FindObjects(SearchMulti({SearchUser(),SearchRegion(self.EngagementRegion,true)}))
        self.PlayersInRegion = {}
        for i,player in pairs (found) do 
            if( not IsDead(player) ) then
                table.insert( self.PlayersInRegion, player )
            end
        end

        self.DevoteesInRegion = {}
        local found = FindObjects(SearchMulti{SearchRegion(self.EngagementRegion),SearchObjVar("MobileTeamType","Undead")})
        for i,devotee in pairs (found) do 
            if( not IsDead(devotee) ) then
                table.insert( self.DevoteesInRegion, devotee )
            end
        end

    end,

    ForeachPlayerInRegion = function(self, cb)
        cb = cb or function()end
        for i=1, #self.PlayersInRegion do 
            cb( self.PlayersInRegion[i] )
        end
    end,

    ForeachDevoteeInRegion = function(self, cb)
        cb = cb or function()end
        for i=1, #self.DevoteesInRegion do 
            cb( self.DevoteesInRegion[i] )
        end
    end,

    CreatePortals = function(self)
        if( self.Portals == nil ) then
            self.Portals = {}
            for i=1, #self.PortalLocations do 
                local portalLoc = self.PortalLocations[i]
                Create.AtLoc( "portal", portalLoc, function(portal)
                    table.insert( self.Portals,portal )
                end)
            end
        end
    end,

    DestroyPortals = function(self)
        
        if( self.Portals ~= nil ) then
            for i=1, #self.Portals do 
                if( self.Portals[i] and self.Portals[i]:IsValid() ) then
                    self.Portals[i]:Destroy()    
                end
            end
        end
        
        self.Portals = nil
    end,


}

States.AzuraHighpriestDeath = {
    Name = "AzuraHighpriestDeath",

    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,

    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,

    Run = function(self)
        States.Death.Run(self)
        States.Death.DistributeLoot(self, "TundraHighPriest")
        States.AzuraHighpriestDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        
        if not( self.DoEndComplete ) then
            self.DoEndComplete = true
        end
        
    end
}