local DEBUG_AI = true

function shuffle(array)
    -- fisher-yates
    local output = { }
    local random = math.random

    for index = 1, #array do
        local offset = index - 1
        local value = array[index]
        local randomIndex = offset*random()
        local flooredIndex = randomIndex - randomIndex%1

        if flooredIndex == offset then
            output[#output + 1] = value
        else
            output[#output + 1] = output[flooredIndex + 1]
            output[flooredIndex + 1] = value
        end
    end

    return output
end

function DebugAI( string )
    if( DEBUG_AI ) then
        DebugMessage( string )
    end
end

States.MilitaChampionIdle = {
    Name = "MilitaChampionIdle",
    Init = function(self)
        self.IdleCooldowns = {}
        self.Spawner = self.ParentObj:GetObjVar("Spawner")
        self.SpawnerLoc = self.Spawner:GetLoc()
    end,
    ShouldRun = function(self)
        return (
            (self.Spawner and self.SpawnerLoc)
            and not self.IsPathing 
            and not self.CurrentTarget 
            and self.ParentObj:GetLoc() ~= self.SpawnerLoc
        ) 
    end,
    ExitState = function(self)
        DebugAI( "ExitState : MilitaChampionIdle" )
    end,
    Run = function(self)
        DebugAI( "Run : MilitaChampionIdle" )

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

        -- Path back to spawn location
        if( self.ParentObj:GetLoc() ~= self.SpawnerLoc ) then
            self.PathTo(self.SpawnerLoc, 8)
        end

    end,
    EnterState = function(self)
        DebugAI( "EnterState : MilitaChampionIdle" )
        self.FightStarted = false
        States.Aggro.ClearAggro(self)
        CallFunctionDelayed(TimeSpan.FromSeconds(6),function( ... )
            self.ParentObj:SetFacing(96)
        end)
    end,
}

States.MilitaChampionFight = {
    Name = "MilitaChampionFight",
    Init = function(self)
        States.Fight.Init(self)
        self.TargetDistance = 100
        self.TargetHealthPrecent = 100
        self.Cooldowns = {}
        self.Cooldowns.Execute = 0
        self.Cooldowns.Hamstring = 0
        self.Cooldowns.AddAggro = 0
        self.KeepRegion = self.ParentObj:GetObjVar("KeepRegion")
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    ExitState = function(self)
        self.ParentObj:StopEffect("WarriorBloodlustDefensive", 3.0)
    end,

    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,
    Run = function(self)
        DebugAI( "Run : MilitaChampionFight" )

        if ( not self.ValidCombatTarget(self.CurrentTarget) or not self.ParentObj:IsInRegion(self.KeepRegion) ) then
            self.SetTarget(nil)
        end

        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Get distance to target
        self.TargetDistance = self.ParentObj:DistanceFrom(self.CurrentTarget)
        self.TargetHealthPrecent = ((GetCurHealth(self.CurrentTarget)/GetMaxHealth(self.CurrentTarget)) * 100)

        DebugAI( "Run -- 1 : MilitaChampionFight" )
        -- Timestamp
        local unixTS = os.time(os.date("!*t"))

        -- Start Fight
        States.MilitaChampionFight.StartFight(self)

        -- Basic Fight
        States.Fight.Run(self)

        -- Execute
        if( GroupAI.CooldownCheck( self.Cooldowns, "Execute", unixTS, 20 ) ) then
            DebugAI( "Execute : " .. tostring(self.TargetDistance) .. " : " ..tostring(self.TargetHealthPrecent) )
            if( self.TargetDistance <= 5 and self.TargetHealthPrecent <= 20 ) then
                self.ParentObj:NpcSpeech("Wrong move, " .. self.CurrentTarget:GetName().."!")
                self.ParentObj:PlayAnimation("attack")
                self.ParentObj:PlayEffectWithArgs("WarriorExecute", 3.0, "Bone=Ground,Lifetime=0.8")
                self.CurrentTarget:SendMessage("ProcessTrueDamage", self.ParentObj, math.floor( ( GetMaxHealth(self.CurrentTarget) / 6 ) ), true)
                self.Cooldowns.Execute = unixTS
            end
        end

        -- Hamstring
        if( GroupAI.CooldownCheck( self.Cooldowns, "Hamstring", unixTS, 12 ) ) then
            DebugAI( "Hamstring : " .. tostring(self.TargetDistance) )
            if( self.TargetDistance > 3 ) then
                local overrideRate = 32
                local projectileTimer = (self.TargetDistance - .5) / overrideRate  
                self.CurrentTarget:PlayProjectileEffectToWithSound("WarriorTargetTaunt", self.ParentObj, "event:/magic/martial/magic_martial_taunt", overrideRate, math.max(0.8,projectileTimer), "Orient=2Loc")
                self.CurrentTarget:SendMessage("StartMobileEffect", "Hamstring", self.ParentObj, { Duration = TimeSpan.FromSeconds(10), Modifier = -0.7 })
                self.Cooldowns.Hamstring = unixTS
            end
        end

        -- AddAggro
        if( GroupAI.CooldownCheck( self.Cooldowns, "AddAggro", unixTS, 4 ) ) then
            DebugAI( "AddAggro : " .. tostring(self.TargetDistance) )
            local overrideRate = 32
            local projectileTimer = (self.TargetDistance - .5) / overrideRate  
            self.CurrentTarget:SendMessage("AddAggro", self.ParentObj, 400)
            self.CurrentTarget:PlayEffectWithArgs("WarriorTaunted", 0.0, "Bone=Head")
            self.Cooldowns.AddAggro = unixTS
        end

    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            self.FightStarted = true
            States.MilitaChampionFight.DoRoar(self)
            DebugAI( "StartFight : MilitaChampionFight" )

            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            self.Cooldowns.Execute = unixTS-30;
            self.Cooldowns.Hamstring = unixTS-30;
            self.Cooldowns.AddAggro = unixTS;
        end
    end,

    DoRoar = function(self)
        self.ParentObj:NpcSpeech("To arms my Eldierians, your champion is in need!")
        self.ParentObj:PlayAnimation("roar")
        self.ParentObj:PlayEffectWithArgs("WarriorBloodlustDefensive", 0.0, "Bone=Head")
    end,


    ForEachPlayerInKeep = function(self, cb, includeDead)
        if(cb == nil) then cb = function()end end
        local players = FindObjects(SearchMulti({SearchUser(),SearchRange(self.SpawnerLoc,16)}))
        for i,player in pairs (players) do 
            if ( IsDead(player) ) then
                if( includeDead ) then
                    cb(player) 
                end
            else
                if( IsGod(player) and not TestMortal(player) ) then
                    -- we don't include gods
                else
                    cb(player) 
                end
            end
        end
    end,

}

States.MilitaChampionDeath = {
    Name = "MilitaChampionDeath",
    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,
    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,
    Run = function(self)
        DebugAI( "Run : MilitaChampionDeath" )
        States.Death.Run(self)
       --States.Death.DistributeLoot(self, "MonolithBossFinkle")
        States.MilitaChampionDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        if not( self.DoEndComplete ) then
            
            self.DoEndComplete = true
            self.MilitiaScores = {0,0,0}
            self.WinningMilitiaDamage = 0
            self.WinningMilitiaID = 0

            -- Determine which faction won!
            if( self.AggroList ) then

                -- Get the damage from all the damagers
                for damager,damage in pairs(self.AggroList) do
                    local damagerMilitaID = Militia.GetId(damager)
                    self.MilitiaScores[damagerMilitaID] = self.MilitiaScores[damagerMilitaID] + damage
                end

                -- Which militia won?
                for i=1, #self.MilitiaScores do 
                    if( self.MilitiaScores[i] > self.WinningMilitiaDamage ) then
                        self.WinningMilitiaID = i
                    end
                end

                DebugAI( "DoEnd : MilitaChampionDeath : " .. tostring( self.WinningMilitiaID ) )

                -- And the winner is!
                -- self.WinningMilitiaID --> GETS THE KEEP
                
            end
            
            -- One last blast
            self.ParentObj:NpcSpeech("Ebris, forgive me.")

        end
        
    end
}