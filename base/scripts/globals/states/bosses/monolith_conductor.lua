States.MonolithConductorIdle = {
    Name = "MonolithConductorIdle",
    Init = function(self)
        self.IdleCooldowns = {}
        self.Spawner = self.ParentObj:GetObjVar("Spawner")
        self.SpawnerLoc = self.Spawner:GetLoc()

    end,
    ShouldRun = function(self)
        return (
            (self.Spawner and self.SpawnerLoc)
            and not self.IsPathing 
            and not self.CurrentTarget 
            and self.ParentObj:GetLoc() ~= self.SpawnerLoc
        ) 
    end,
    ExitState = function(self)
        -- TODO: Set him facing (354)
        
    end,
    Run = function(self)

        self.FightStarted = false

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

        -- Path back to spawn location
        if( self.ParentObj:GetLoc() ~= self.SpawnerLoc ) then
            self.PathTo(self.SpawnerLoc, 8)
        end

    end,
    EnterState = function(self)
        -- Remove gates
        if( self.Gateways ~= nil ) then
            States.MonolithConductorFight.DestroyGateways(self)
        end

        States.Aggro.ClearAggro(self)

    end,
}

States.MonolithConductorFight = {
    Name = "MonolithConductorFight",
    Init = function(self)
        States.Fight.Init(self)
        self.Location = self.ParentObj:GetLoc()
        self.FightStarted = false
        self.AggroRange = 40
        self.Cooldowns = {}
        self.FightSpeech = 
        {
            "My beautiful creatures, so light on your feet.",
            "Do you enjoy this dance?",
            "Come now, let us waltz you and I.",
        }

        self.SafeZoneSpeech = 
        {
            "Everyone to your positions!",
            "Partners together now.",
            "Everything must be perfect.",
        }

        self.SafeZoneLocs = { Loc(-512.16, 0, 38.99), Loc(-486.07, 0, 38.99) }

        self.Cooldowns.FightSpeech = 0;
        self.Cooldowns.SafeZones = 0;
        self.Cooldowns.AOEBlast = 0;
        self.Cooldowns.ChargedSparks = 0;
        
    end,
    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    Run = function(self)
        -- call base fight
        States.Fight.Run(self)
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Check all aggro targets are on the platform
        States.MonolithConductorFight.CheckAggroTargetsValid(self)

        -- Start Fight
        States.MonolithConductorFight.StartFight(self)

        -- We need to constantly re-apply the conductor's dance in-case someone was ressed
        States.MonolithConductorFight.ForEachPlayerOnBalcony(self, function(player)
            if not(  HasMobileEffect(player, "ConductorDance") ) then
                player:SendMessage("StartMobileEffect", "ConductorDance", self.ParentObj, {Stacks = 5, DebuffIndicatorColor = nil})
                self.ParentObj:SendMessage("AddAggro", player, 20)					
            end
        end)
        
        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))

        -- Speech
        if( GroupAI.CooldownCheck( self.Cooldowns, "FightSpeech", unixTS, 30 ) ) then
            self.ParentObj:NpcSpeech( self.FightSpeech[math.random( 1,#self.FightSpeech )] )
            self.Cooldowns.FightSpeech = unixTS
        end

        -- Safe Zones
        if( GroupAI.CooldownCheck( self.Cooldowns, "SafeZones", unixTS, math.random( 12,20 ) ) ) then
            self.ParentObj:NpcSpeech( self.SafeZoneSpeech[math.random( 1,#self.SafeZoneSpeech )] )
            self.SafeZones = {}
            local GreenZoneLoc = self.SafeZoneLocs[1]
            local RedZoneLoc = self.SafeZoneLocs[2]
            
            -- Swap our zones if needed
            local startWithZone = math.random( 1,100 )
            if( startWithZone > 50 ) then
                GreenZoneLoc = self.SafeZoneLocs[2]
                RedZoneLoc = self.SafeZoneLocs[1]
            end

            -- Create Red Zone
            Create.AtLoc( "zone_radius_red", RedZoneLoc, function(zone)
                zone:SetObjVar("SafeEffect", "MagicPrimeTrailRedEffect")
                table.insert( self.SafeZones,zone )
            end)

            -- Create Green Zone
            Create.AtLoc( "zone_radius_green", GreenZoneLoc, function(zone)
                zone:SetObjVar("SafeEffect", "MagicPrimeTrailGreenEffect")
                table.insert( self.SafeZones,zone )
            end)

            -- Remove zones after 8 seconds.
            CallFunctionDelayed(TimeSpan.FromMilliseconds(8000),function()
                for i=1, #self.SafeZones do 
                    self.SafeZones[i]:Destroy()
                end
            end)

            self.Cooldowns.SafeZones = unixTS
        end

        -- AOE Blast
        if( GroupAI.CooldownCheck( self.Cooldowns, "AOEBlast", unixTS, math.random( 20,30 ) ) ) then

            self.ParentObj:NpcSpeech("Let us see who's been out of step.")
            CallFunctionDelayed(TimeSpan.FromMilliseconds(1000),function()
                States.MonolithConductorFight.DoAOEExplosionEffects(self)
                States.MonolithConductorFight.ForEachPlayerOnBalcony(self, function(player)
                    player:SendMessage("ConductorDanceAOE")
                end)
            end)
            
            self.Cooldowns.AOEBlast = unixTS
            
        end

        -- Charged Sparks
        -- Check if we are below 20% health, if so we need to summon sparks
        local healthPercent = GetCurHealth(self.ParentObj) / GetMaxHealth(self.ParentObj)
        --DebugMessage("healthPercent : " .. healthPercent)
        if( healthPercent <= 0.25 ) then
            if( GroupAI.CooldownCheck( self.Cooldowns, "ChargedSparks", unixTS, 15 ) ) then
                Create.AtLoc( "charged_spark", Loc(-498.93, 0, 44.20),nil)

                self.Cooldowns.ChargedSparks = unixTS
            end
        end
    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            self.Cooldowns.FightSpeech = unixTS+5;
            self.Cooldowns.SafeZones = unixTS+10;
            self.Cooldowns.AOEBlast = unixTS+20;

            -- Teleport Players to center
            States.MonolithConductorFight.TeleportPlayersToCenter(self)

            -- Apply "ConductorDance" MobileEffect to all players
            local playerCount = 0
            States.MonolithConductorFight.ForEachPlayerOnBalcony(self, function(player)
                playerCount = playerCount + 1
                local debuffEffect = "MagicPrimeTrailRedEffect"
                if( playerCount % 2 == 0 ) then debuffEffect = "MagicPrimeTrailGreenEffect" end
                player:SendMessage("StartMobileEffect", "ConductorDance", self.ParentObj, {Stacks = 5, DebuffIndicatorColor = debuffEffect})
            end)

            -- Gateways
            States.MonolithConductorFight.CreateGateways(self)

            self.FightStarted = true
        end
    end,

    CheckAggroTargetsValid = function(self)
        States.Aggro.ForeachOnAggroList(self, function(aggroObj)
            if( not aggroObj:IsInRegion("1-F-arena") ) then
                aggroObj:PlayEffect("TeleportFromEffect")	
                aggroObj:SetWorldPosition(Loc( -498.84, 0, 41.17 ))
                aggroObj:SetFacing(360)
                aggroObj:PlayEffect("TeleportToEffect")			
            end
        end)
    end,

    ForEachPlayerOnBalcony = function(self, cb, includeDead)
        if(cb == nil) then cb = function()end end
        local players = FindObjects(SearchMulti({SearchUser(),SearchRegion("1-F-arena",true)}))
        for i,player in pairs (players) do 
            if ( IsDead(player) ) then
                if( includeDead ) then
                    cb(player) 
                end
            else
                if( IsGod(player) and not TestMortal(player) ) then
                    -- we don't include gods
                else
                    cb(player) 
                end
            end
        end
    end,

    TeleportPlayersToCenter = function(self)
        self.ParentObj:NpcSpeech("Gather here my pretties.")
        States.Aggro.ForeachOnAggroList(self, function(player)
            player:PlayEffect("TeleportFromEffect")	
            player:SetWorldPosition(Loc( -498.84, 0, 41.17 ))
            player:SetFacing(360)
            player:PlayEffect("TeleportToEffect")				
        end, nil, true)
    end,

    DoAOEExplosionEffects = function(self)
        PlayEffectAtLoc("DeathwaveEffect",Loc( -498.93, 0, 44.20 ))
        PlayEffectAtLoc("DeathwaveEffect",self.SafeZoneLocs[1])
        PlayEffectAtLoc("DeathwaveEffect",self.SafeZoneLocs[2])
    end,

    CreateGateways = function(self)
        if( self.Gateways == nil ) then
            self.Gateways = {}
            Create.AtLoc( "monolith_small_gateway", Loc(-513.93, 4.226, 32.77), function(gate)
                table.insert( self.Gateways,gate )
            end)
            Create.AtLoc( "energy_gate", Loc(-514.01, 0, 32.61), function(gate)
                table.insert( self.Gateways,gate )
            end)

            Create.AtLoc( "monolith_small_gateway", Loc(-483.90, 4.226, 32.77), function(gate)
                table.insert( self.Gateways,gate )
            end)
            Create.AtLoc( "energy_gate", Loc(-484.06, 0, 32.61), function(gate)
                table.insert( self.Gateways,gate )
            end)
        end
    end,

    DestroyGateways = function(self)
        if( self.Gateways ~= nil ) then
            for i=1, #self.Gateways do 
                self.Gateways[i]:Destroy()
            end
        end
        self.Gateways = nil
    end,

}

States.MonolithConductorDeath = {
    Name = "MonolithConductorDeath",
    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,

    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,

    Run = function(self)
        States.Death.Run(self)
        States.Death.DistributeLoot(self, "MonolithBossConductor")
        States.MonolithConductorDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        
        if not( self.DoEndComplete ) then
            self.DoEndComplete = true

            -- One last blast
            self.ParentObj:NpcSpeech("A parting gift for you.")
            States.MonolithConductorFight.DoAOEExplosionEffects(self)
            States.MonolithConductorFight.ForEachPlayerOnBalcony(self, function(player)
                player:SendMessage("ConductorDanceAOE")
            end)

            -- Remove gates
            States.MonolithConductorFight.DestroyGateways(self)

            -- Remove ConductorDance mobile effect from players
            States.MonolithConductorFight.ForEachPlayerOnBalcony(self, function(player)
                player:SendMessage("ConductorDanceEnd")
            end)
        end
        
    end
}