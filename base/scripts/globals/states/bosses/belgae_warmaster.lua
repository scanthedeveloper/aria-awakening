States.BelgaeWarmasterIdle = {
    Name = "BelgaeWarmasterIdle",

    Init = function(self)
        self.IdleCooldowns = {}
        self.SpawnerLoc = self.ParentObj:GetLoc()
        self.WarmistressObj = nil
        self.LastShieldTarget = nil
    end,

    ShouldRun = function(self)
        return (
            (self.SpawnerLoc)
            and not self.IsPathing 
            and not self.CurrentTarget 
        ) 
    end,

    ExitState = function(self)
        -- TODO: Set him facing (354)
    end,

    Run = function(self)

        self.FightStarted = false

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

        -- Path back to spawn location
        if( self.ParentObj:GetLoc() ~= self.SpawnerLoc ) then
            self.PathTo(self.SpawnerLoc, 8)
        end

        -- If we don't have the Warmistress try to find her!
        if( self.WarmistressObj == nil ) then
            self.WarmistressObj = FindObject(SearchMulti({SearchHasObjVar("IsWarmistress"),SearchObjectInRange(30)})) or nil
        end

        -- If we do have her is she engaged?
        if( self.WarmistressObj and self.WarmistressObj:IsValid() ) then
            local WarmistressTarget = self.WarmistressObj:GetObjVar("CurrentTarget")
            if( WarmistressTarget ) then
                self.SetTarget(WarmistressTarget)
                self.ParentObj:SendMessage("ForceAggro", WarmistressTarget)
            end
        end
    end,

    EnterState = function(self)
        States.Aggro.ClearAggro(self)
        self.MistressDead = false
        SetMobileMod(self.ParentObj, "AttackSpeedTimes", "Enrage", nil)
        SetMobileMod(self.ParentObj, "AttackTimes", "Enrage", nil)
        States.BelgaeWarmasterFight.RemoveShield(self)
    end,
}

States.BelgaeWarmasterFight = {
    Name = "BelgaeWarmasterFight",

    Init = function(self)
        States.Fight.Init(self)
        self.Location = self.ParentObj:GetLoc()
        self.FightStarted = false
        self.MistressDead = false
        self.AggroRange = 10
        self.Cooldowns = {}
    end,

    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    Run = function(self)
        self.Location = self.ParentObj:GetLoc()

        -- call base fight
        States.Fight.Run(self)

        -- Have we moved too far away from our spawn location?
        if( self.Location:Distance( self.SpawnerLoc ) > 25 ) then
            self.CurrentTarget = nil
            self.ParentObj:ClearPathTarget()
            self.IsPathing = false
        end
        
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Start Fight
        States.BelgaeWarmasterFight.StartFight(self)

        local nearbyBarbarian = FindObjects(SearchMulti(
        {
            SearchMobileInRange(30), --in 10 units
            SearchObjVar("MobileTeamType","Barbarian"), --find slaver guards
        }))

        for i,mobileObj in pairs (nearbyBarbarian) do
            --DebugMessage("Found",tostring(mobileObj))
            if( not IsInCombat( mobileObj ) ) then
                --DebugMessage("Commanded",tostring(mobileObj))
                mobileObj:NpcSpeech("To arms! The war council bleeds.")
                mobileObj:SendMessage("SetCurrentTarget", self.CurrentTarget)
                mobileObj:SendMessage("AddAggro", self.CurrentTarget, 10)
            end
        end

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))
        local masterHealth = GetCurHealth(self.ParentObj) / GetMaxHealth(self.ParentObj)

        if( self.WarmistressObj and self.WarmistressObj:IsValid() ) then
            if( IsDead( self.WarmistressObj ) ) then
                if not( self.MistressDead ) then
                    self.ParentObj:NpcSpeech("Nooo! Gods give me strength to defeat these foes!")
                    self.ParentObj:PlayEffectWithArgs("BuffEffect_I",0.0,"Bone=Spine")
                    SetMobileMod(self.ParentObj, "AttackTimes", "Enrage", 0.75)
                    SetMobileMod(self.ParentObj, "AttackSpeedTimes", "Enrage", 0.50)
                    States.BelgaeWarmasterFight.RemoveShield(self)
                    self.MistressDead = true
                end
            else
                local WarmistressTarget = self.WarmistressObj:GetObjVar("CurrentTarget")
                if( self.CurrentTarget ~= WarmistressTarget ) then
                    self.ParentObj:SendMessage("ForceAggro", WarmistressTarget)
                end

                self.MistressDead = false
            end
        end

    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            States.BelgaeWarmasterFight.ApplyShield(self)
            self.FightStarted = true
        end
    end,

    ApplyShield = function(self)
        if( self.WarmistressObj and self.WarmistressObj:IsValid() and not IsDead(self.WarmistressObj) ) then
            self.WarmistressObj:NpcSpeech("May my life blood drain before harm touches you Yurn!")
            self.ParentObj:NpcSpeech("My blood is yours Yelgi!")
            self.ParentObj:PlayEffectWithArgs("ForceShield",0.0,"Bone=Spine")
        self.ParentObj:SendMessage("StartMobileEffect", "Immune", nil, { Duration = TimeSpan.FromSeconds(1200) })
        end
    end,

    RemoveShield = function(self)
        if( self and self.ParentObj ) then
            self.ParentObj:SendMessage("EndImmuneEffect")
            self.ParentObj:StopEffect("ForceShield")
        end
    end,

}

States.BelgaeWarmasterDeath = {
    Name = "BelgaeWarmasterDeath",

    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,

    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,

    Run = function(self)
        States.Death.Run(self)
        States.BelgaeWarmasterFight.RemoveShield(self)
        States.Death.DistributeLoot(self, "TundraWarmaster")
        States.BelgaeWarmasterDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        if not( self.DoEndComplete ) then
            self.DoEndComplete = true
        end
    end
}