States.AwakeningBossEntIdle = {
    Name = "AwakeningBossEntIdle",

    Init = function(self)
        self.IdleCooldowns = {}
        self.SpawnerLoc = self.ParentObj:GetLoc()

    end,

    ShouldRun = function(self)
        return (
            (self.SpawnerLoc)
            and not self.IsPathing 
            and not self.CurrentTarget 
            and self.ParentObj:GetLoc() ~= self.SpawnerLoc
        ) 
    end,

    ExitState = function(self)
        -- TODO: Set him facing (354)
        
    end,

    Run = function(self)

        self.FightStarted = false

        -- Set health to full
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end

        -- Path back to spawn location
        if( self.ParentObj:GetLoc() ~= self.SpawnerLoc ) then
            self.PathTo(self.SpawnerLoc, 8)
        end

    end,

    EnterState = function(self)
        States.Aggro.ClearAggro(self)
    end,
}

States.AwakeningBossEntFight = {
    Name = "AwakeningBossEntFight",

    Init = function(self)
        States.Fight.Init(self)
        self.Location = self.ParentObj:GetLoc()
        self.FightStarted = false
        self.AggroRange = 10
        self.Cooldowns = {}
        self.Cooldowns.AOEBlast = 0;
    end,

    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    Run = function(self)
        self.Location = self.ParentObj:GetLoc()

        -- call base fight
        States.Fight.Run(self)

        -- Have we moved too far away from our spawn location?
        if( self.Location:Distance( self.SpawnerLoc ) > 25 ) then
            self.CurrentTarget = nil
            self.ParentObj:ClearPathTarget()
            self.IsPathing = false
        end
        
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Start Fight
        States.AwakeningBossEntFight.StartFight(self)

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))
        local healthPercent = GetCurHealth(self.ParentObj) / GetMaxHealth(self.ParentObj)

        -- AOE Blast
        if( GroupAI.CooldownCheck( self.Cooldowns, "AOEBlast", unixTS, math.random( 10,16 ) ) ) then

            -- Create Red Zone
            Create.AtLoc( "zone_radius_red", self.Location, function(zone)
                zone:SetObjVar("Radius", 10)
                local zoneLoc = zone:GetLoc()
                CallFunctionDelayed(TimeSpan.FromSeconds(2),function()
                    PlayEffectAtLoc("DeathwaveEffect",zoneLoc)
                    local players = FindObjects(SearchMulti({SearchRange(zoneLoc,10),SearchUser()}))
                    DebugTable( players )

                    for i=1, #players do 
                        local maxHealth = GetMaxHealth(players[i])
                        players[i]:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact")
                        players[i]:SendMessage("ProcessTrueDamage", self.ParentObj, math.floor( maxHealth / 4 ), true)
                    end
                    
                    zone:Destroy()
                end)
            end)
            
            self.Cooldowns.AOEBlast = unixTS
            
        end
    end,

    StartFight = function(self)
        if( self.FightStarted ~= true ) then
            -- Cooldowns
            local unixTS = os.time(os.date("!*t"))
            self.Cooldowns.AOEBlast = unixTS+10;
            self.FightStarted = true
        end
    end,

}

States.AwakeningBossEntDeath = {
    Name = "AwakeningBossEntDeath",

    Init = function(self)
        States.Death.Init(self)
        self.DoEndComplete = false
    end,

    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,

    Run = function(self)
        States.Death.Run(self)
        States.Death.DistributeLoot(self, "AwakeningEnt")
        States.AwakeningBossEntDeath.DoEnd(self)
        return true
    end,

    DoEnd = function(self)
        
        if not( self.DoEndComplete ) then
            self.DoEndComplete = true
        end
        
    end
}