States.EasyBossPhaseOne = {
    Name = "EasyBossPhaseOne",
    Init = function(self)
        -- call base fight
        States.Fight.Init(self)
        FSMHelper.SpellInit(self)
        FSMHelper.SelfMobileEffectsInit(self)
        FSMHelper.TargetMobileEffectsInit(self)
        FSMHelper.CombatAbilityInit(self)

        self.EasyBossPhaseOneList = {}

        -- Cast Spell
        if( self.HasSpells ) then
            self.EasyBossPhaseOneList[#self.EasyBossPhaseOneList+1] = function()
                FSMHelper.RandomSpell(self, 10)
            end
        end

        -- Use Weapon Ability
        if( self.HasWeaponAbilities ) then
            self.EasyBossPhaseOneList[#self.EasyBossPhaseOneList+1] = function()
                FSMHelper.RandomWeaponAbility(self, 10, self.CurrentTarget)
            end
        end

        -- Use Combat Ability
        if( self.HasCombatAbilities ) then
            self.EasyBossPhaseOneList[#self.EasyBossPhaseOneList+1] = function()
                FSMHelper.RandomCombatAbility(self, 10, self.CurrentTarget)
            end
        end
        
        -- Use Self Mobile Effect
        if( self.HasSelfMobileEffects ) then
            self.EasyBossPhaseOneList[#self.EasyBossPhaseOneList+1] = function()
                FSMHelper.RandomSelfMobileEffect(self, 10, self.CurrentTarget)
            end
        end

        -- Use Target Mobile Effect
        if( self.HasTargetMobileEffects ) then
            self.EasyBossPhaseOneList[#self.EasyBossPhaseOneList+1] = function()
                FSMHelper.RandomTargetMobileEffect(self, 10, self.CurrentTarget)
            end
        end

    end,

    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,

    Run = function(self)
        -- call base fight
        States.Fight.Run(self)

        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- For each of our performable actions we need to determine which should run!
        if ( #self.EasyBossPhaseOneList > 0 ) then
            if ( #self.EasyBossPhaseOneList == 1 ) then
                self.EasyBossPhaseOneList[1]()
            else
                self.EasyBossPhaseOneList[math.random(1,#self.EasyBossPhaseOneList)]()
            end
        end

    end,
}

States.EasyBossWander = {
    Name = "EasyBossWander",
    Init = function(self)
        States.Wander.Init(self)
    end,
    ShouldRun = function(self)
        return States.Wander.ShouldRun(self)
    end,
    Run = function(self)
        States.Wander.Run(self)
    end,
    EnterState = function(self)
        self.ParentObj:SetObjVar("Wandering", true)
    end,
    ExitState = function(self)
        self.ParentObj:DelObjVar("Wandering")
    end,
}

States.EasyBossDeath = {
    Name = "EasyBossDeath",
    Init = function(self)
        States.Death.Init(self)
    end,
    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,
    Run = function(self)
        States.Death.Run(self)

        -- Do we need to distribute loot?
        local distributedLootTable = self.ParentObj:GetObjVar("DistrbutedLootTable")
        if( distributedLootTable and not self.LootDistributed ) then
            loottable = {TemplateDefines.LootTable[distributedLootTable]}
            local looters = self.AggroList or {}
            local numIndexedTable = {}
            local counter = 1
            for player, aggro in pairs(looters) do
                if (player:IsValid() and not IsDead(player)) then
                    numIndexedTable[counter] = player
                    counter = counter + 1
                end
            end
            
            DistributeBossRewards(numIndexedTable, loottable, nil, false, GetGuardProtection(self.ParentObj))
            self.LootDistributed = true
        end

    end,
}