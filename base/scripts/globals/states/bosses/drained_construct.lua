States.DrainedConstructIdle = {
    Name = "DrainedConstructIdle",
    Init = function(self)
        self.IdleCooldowns = {}
        self.Spawner = self.ParentObj:GetObjVar("Spawner")
        self.SpawnerLoc = self.Spawner:GetLoc()

    end,
    ShouldRun = function(self)
        return (
            (self.Spawner and self.SpawnerLoc)
            and not self.IsPathing 
            and self.CurrentTarget == nil
            and self.ParentObj:GetLoc() ~= self.SpawnerLoc
        ) 
    end,
    Run = function(self)
        
        if( self.CurrentTarget == nil ) then

            --DebugMessage( "Running: DrainedConstructIdle" )
            -- Set health to full
            local maxHP = GetMaxHealth(self.ParentObj)
            if not( GetCurHealth(self.ParentObj) == maxHP ) then
                SetCurHealth(self.ParentObj, maxHP)
            end

            -- Path back to spawn location
            if( self.ParentObj:GetLoc() ~= self.SpawnerLoc ) then
                self.PathTo(self.SpawnerLoc, 8)
            end

        end

    end,

    EnterState = function(self)
        self.ParentObj:SetScale( Loc(1.1, 1.1, 1.1 ) )
        self.ParentObj:SendMessage("DrainedConstructEndEffects")
        self.ParentObj:SetName("Shard Construct")
        self.ParentObj:SendMessage("UpdateName")
        States.Aggro.ClearAggro(self)
    end,
}

States.DrainedConstructFight = {
    Name = "DrainedConstructFight",
    Init = function(self)
        States.Fight.Init(self)
        self.Location = self.ParentObj:GetLoc()
        self.Cooldowns = {}
        self.FightSpeech = 
        {
            "Vacate the area.",
            "Xor demands isolation, proceed to exits.",
            "Intruders will be purged!",
        }

        self.Cooldowns.FightSpeech = 0;
        self.Cooldowns.FightPulse = 0;
        self.Cooldowns.FightPulseSpeech = 0;

    end,

    EnterState = function(self)
        self.ParentObj:DelObjVar("Invulnerable")
    end,

    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,
    Run = function(self)
        --DebugMessage( "Running: DrainedConstructFight" )
        -- call base fight
        States.Fight.Run(self)
        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        local spawnerDistance = self.ParentObj:DistanceFrom(self.Spawner)

        -- If we are too far away from our spawner, reset ourselves!
        if( spawnerDistance >= 16.5 ) then
            self.ParentObj:SendMessage("ClearTarget")
            self.CurrentTarget = nil
            ClearConflictTable(self.ParentObj)
            States.Aggro.ClearAggro(self)
            return
        end

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))

        -- Speech
        if( GroupAI.CooldownCheck( self.Cooldowns, "FightSpeech", unixTS, 30 ) ) then
            self.ParentObj:NpcSpeech( self.FightSpeech[math.random( 1,#self.FightSpeech )] )
            self.Cooldowns.FightSpeech = unixTS
        end

        if( GroupAI.CooldownCheck( self.Cooldowns, "FightPulse", unixTS, 4 ) ) then
            local empowered = false
            if( spawnerDistance < 3.5 ) then
                self.ParentObj:SetName("Shard Construct")
                self.ParentObj:SendMessage("StartMobileEffect", "DrainedConstructHeal", nil, { StackDelta = 1 })
                self.ParentObj:SendMessage("StartMobileEffect", "DrainedConstructEmpower", nil, { StackDelta = -1 })
            else
                empowered = true
                self.ParentObj:SetName("Empowered Construct")
                self.ParentObj:SendMessage("StartMobileEffect", "DrainedConstructHeal", nil, { StackDelta = -1 })
                self.ParentObj:SendMessage("StartMobileEffect", "DrainedConstructEmpower", nil, { StackDelta = 1 })
            end
            if( GroupAI.CooldownCheck( self.Cooldowns, "FightPulseSpeech", unixTS, 8 ) ) then
                if( empowered ) then
                    self.ParentObj:NpcSpeech( "Executing: Purge Sequence" )
                else
                    self.ParentObj:NpcSpeech( "Executing: Restore Sequence" )
                end
                self.Cooldowns.FightPulseSpeech = unixTS
            end
            self.ParentObj:SendMessage("UpdateName")
            self.Cooldowns.FightPulse = unixTS
        end

    end,
}

States.DrainedConstructDeath = {
    Name = "DrainedConstructDeath",
    Init = function(self)
        States.Death.Init(self)
    end,
    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,
    Run = function(self)
        States.Death.Run(self)
        States.Death.DistributeLoot(self, "MonolithBossDrainedConstruct")
        return true
    end,
}