States.FightHealer = {
    Name = "FightHealer",

    Init = function(self)
        -- call base fight
        States.FightAdvanced.Init(self)
        FSMHelper.SpellInit(self)
        FSMHelper.SelfMobileEffectsInit(self)
        FSMHelper.TargetMobileEffectsInit(self)
        FSMHelper.CombatAbilityInit(self)

        self.HealMin = self.ParentObj:GetObjVar("AI-HealMin") or 100
        self.HealMax = self.ParentObj:GetObjVar("AI-HealMax") or 200

        self.Cooldowns = {}

        -- Set our initial cooldowns
        self.Cooldowns.Heal = 0;
        --self.Cooldowns.Poison = 0;
    end,

    ShouldRun = function(self)
        return States.FightAdvanced.ShouldRun(self)
    end,

    Run = function(self)
        -- call base fight
        States.FightAdvanced.Run(self)

        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))

        -- Instant Heal
        if( GroupAI.CooldownCheck( self.Cooldowns, "Heal", unixTS, 5 ) ) then
            local healAmount = math.random( self.HealMin, self.HealMax )
            local friendlyTarget = States.FightHealer.GetHealTarget(self)
            
            -- If we have a target to heal lets do that
            if( friendlyTarget ) then
                self.ParentObj:PlayObjectSound("event:/magic/misc/magic_water_restoration")
                self.ParentObj:PlayAnimation("cast_heal")
                friendlyTarget:SendMessage("HealRequest", healAmount, self.ParentObj, false)
                friendlyTarget:PlayEffect("GreaterHealEffect")
                self.Cooldowns.Heal = unixTS
            end
        end

    end,

    GetHealTarget =function(self)
        local groupMembers = GroupAI.GetNearbyGroupAIMembers( self.ParentObj )
        table.insert( groupMembers, self.ParentObj )

        local lowestHealth = 100
        local target = nil
        for i=1, #groupMembers do
            local groupMember = groupMembers[i] 
            if( States.FightHealer.ValidHealTarget( groupMember ) ) then
                local healthPercent = GetCurHealth(groupMember)/GetMaxHealth(groupMember)
                if( healthPercent < lowestHealth ) then
                    lowestHealth = healthPercent
                    target = groupMember
                end
            end
        end
        return target
    end,

    ValidHealTarget = function(_target)
        return ( _target and _target:IsValid() and not IsDead(_target) and ( GetCurHealth(_target) < GetMaxHealth(_target) ) )
    end,
}