States.FightTank = {
    Name = "FightTank",
    Init = function(self)
        -- call base fight
        States.FightAdvanced.Init(self)
        FSMHelper.SpellInit(self)
        FSMHelper.SelfMobileEffectsInit(self)
        FSMHelper.TargetMobileEffectsInit(self)
        FSMHelper.CombatAbilityInit(self)
        self.Cooldowns = {}
        --self.LastTarget = nil;

        -- Set our initial cooldowns
        self.Cooldowns.TargetUpdate = 0;
        self.Cooldowns.AOEStun = 0;
        self.Cooldowns.Overpower = 0;

    end,

    ShouldRun = function(self)
        return States.FightAdvanced.ShouldRun(self)
    end,

    Run = function(self)
        -- call base fight
        States.FightAdvanced.Run(self)

        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Target in weapon range?
        local isTargetInRange = WithinWeaponRange( self.ParentObj, self.CurrentTarget, nil )

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))

        -- If we have a target we need to broadcast it to our group
        if( GroupAI.CooldownCheck( self.Cooldowns, "TargetUpdate", unixTS, 2 ) ) then
            GroupAI.SendGroupMessage( self.ParentObj, GroupAI.MessageTags.UpdateTarget, { Target = self.CurrentTarget } )
            self.Cooldowns.TargetUpdate = unixTS
            -- TODO: Give a visual indication that the tank is targeting a player.
        end

        -- AOEStun
        if( isTargetInRange and GroupAI.CooldownCheck( self.Cooldowns, "AOEStun", unixTS, 8 ) ) then
            self.ParentObj:SendMessage( "StartMobileEffect", "StunStrike", self.ParentObj, { Radius = 5 } )
            self.Cooldowns.AOEStun = unixTS
        end

        -- Overpower
        if( isTargetInRange and  GroupAI.CooldownCheck( self.Cooldowns, "Overpower", unixTS, 8 ) ) then
            self.ParentObj:SendMessage( "StartMobileEffect", "Overpower", self.CurrentTarget, { AttackModifier = 2 } )
            self.Cooldowns.Overpower = unixTS
        end

    end,
}

States.AttackTank = {
    Name = "AggroTank",
    Init = function(self)
        -- call base fight
        States.Attack.Init(self)
    end,

    ShouldRun = function(self)
        if( States.Attack.ShouldRun(self) ) then
            return true
        else
            -- We want the tank to aggro any target a group member is fighting
            local groupMembers = GroupAI.GetNearbyGroupAIMembers( self.ParentObj )
            for i=1, #groupMembers do 
               local target = groupMembers[i]:GetObjVar("CurrentTarget")
                if( target ) then
                    self.AttackTarget = target
                    return true
                end
            end
            return false
        end
    end,

    Run = function(self)
        -- call base fight
        States.Attack.Run(self)
    end,
}