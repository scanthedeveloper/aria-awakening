States.FightDamage = {
    Name = "FightDamage",
    
    Init = function(self)
        -- call base fight
        States.FightAdvanced.Init(self)
        FSMHelper.SpellInit(self)
        self.Cooldowns = {}
    end,

    ShouldRun = function(self)
        return States.FightAdvanced.ShouldRun(self)
    end,

    Run = function(self)
        -- call base fight
        States.FightAdvanced.Run(self)

        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

    end,
}