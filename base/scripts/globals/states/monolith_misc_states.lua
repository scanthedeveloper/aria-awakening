States.FightSparkMaster = {
    Name = "FightSparkMaster",
    Init = function(self)
        -- call base fight
        States.FightAdvanced.Init(self)
        FSMHelper.SpellInit(self)
        FSMHelper.SelfMobileEffectsInit(self)
        FSMHelper.TargetMobileEffectsInit(self)
        FSMHelper.CombatAbilityInit(self)
        self.SpellDelayMin = 3
        self.SpellDelayMax = 5
        self.Cooldowns = {}
        --self.LastTarget = nil;

        -- Set our initial cooldowns
        self.Cooldowns.SummonSpark = 0;

    end,

    EnterState = function(self)
        self.ParentObj:PlayEffectWithArgs("WispEffect", 0.0, "WispIndex=1")
        self.ParentObj:PlayEffectWithArgs("WispEffect", 0.0, "WispIndex=2")
    end,

    ExitState = function(self)
        self.ParentObj:StopEffect("WispEffect1")
        self.ParentObj:StopEffect("WispEffect2")
    end,

    ShouldRun = function(self)
        return States.FightAdvanced.ShouldRun(self)
    end,

    Run = function(self)
        -- call base fight
        States.FightAdvanced.Run(self)

        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        -- Get our currect timestamp
        local unixTS = os.time(os.date("!*t"))

        -- If we have a target we need to broadcast it to our group
        if( GroupAI.CooldownCheck( self.Cooldowns, "SummonSpark", unixTS, 6 ) ) then
            self.ParentObj:PlayEffectWithArgs("WispLaunchEffect", 0.0, "WispIndex=1")
            PlayEffectAtLoc("WispSummonEffect", self.Loc, 3)
            CallFunctionDelayed(TimeSpan.FromSeconds(0.5),function()
                Create.AtLoc( "charged_spark", self.Loc)
            end)
            self.Cooldowns.SummonSpark = unixTS
        end


    end,
}

States.MonolithRisenDeath = {
    Name = "MonolithRisenDeath",
    Init = function(self)
        self.DoDeath = function( self )
            PlayEffectAtLoc("RedCoreImpactWaveEffect",self.ParentObj:GetLoc(),0.5)
            self.ParentObj:Destroy()
        end
    end,
    ShouldRun = function(self)
        return IsDead(self.ParentObj)
    end,
    Run = function(self)
        self.SetTarget(nil)
        self.DoDeath(self)
        return true
    end,
}

States.MonolithRisenWander = {
    Name = "MonolithRisenWander",
    Init = function(self)
        SetMobileMod(self.ParentObj, "HealthRegenPlus", "RisenRegin", 10)
        if not( self.ParentObj:HasObjVar("SpawnLocation") ) then
            self.SpawnLocation = self.Loc
            self.ParentObj:SetObjVar("SpawnLocation", self.SpawnLocation)
        end
        if not ( self.SpawnLocation ) then
            self.SpawnLocation = self.ParentObj:GetObjVar("SpawnLocation")
        end
    end,
    ShouldRun = function(self)
        return (not self.IsPathing)
    end,
    Run = function(self)
        local loc = self.SpawnLocation:Project(math.random(0,360), math.random(self.WanderMin or 2, self.WanderMax or 4))
        if ( IsPassable(loc) ) then
            self.PathTo(loc, self.WanderSpeed or 1)
        end
    end,
}