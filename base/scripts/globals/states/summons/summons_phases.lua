States.SummonsFight = {
    Name = "SummonsFight",
    Init = function(self)
        --DebugMessage("Init : SummonsFight")
        -- call base fight
        States.Fight.Init(self)
        FSMHelper.SpellInit(self)
        FSMHelper.SelfMobileEffectsInit(self)
        FSMHelper.TargetMobileEffectsInit(self)
        FSMHelper.CombatAbilityInit(self)

        self.SummonsFightList = {}

        -- Cast Spell
        if( self.HasSpells ) then
            self.SummonsFightList[#self.SummonsFightList+1] = function()
                FSMHelper.RandomSpell(self, 10)
            end
        end

        -- Use Weapon Ability
        if( self.HasWeaponAbilities ) then
            self.SummonsFightList[#self.SummonsFightList+1] = function()
                FSMHelper.RandomWeaponAbility(self, 10, self.CurrentTarget)
            end
        end

        -- Use Combat Ability
        if( self.HasCombatAbilities ) then
            self.SummonsFightList[#self.SummonsFightList+1] = function()
                FSMHelper.RandomCombatAbility(self, 10, self.CurrentTarget)
            end
        end
        
        -- Use Self Mobile Effect
        if( self.HasSelfMobileEffects ) then
            self.SummonsFightList[#self.SummonsFightList+1] = function()
                FSMHelper.RandomSelfMobileEffect(self, 10, self.CurrentTarget)
            end
        end

        -- Use Target Mobile Effect
        if( self.HasTargetMobileEffects ) then
            self.SummonsFightList[#self.SummonsFightList+1] = function()
                FSMHelper.RandomTargetMobileEffect(self, 10, self.CurrentTarget)
            end
        end

    end,

    ShouldRun = function(self)
        return States.Fight.ShouldRun(self)
    end,

    Run = function(self)
        --DebugMessage("Run : SummonsFight")

        -- call base fight
        States.Fight.Run(self)

        --DebugMessage("Run : SummonsFight 1")

        -- If we don't have a target return out
        if not( self.CurrentTarget ) then return end

        --DebugMessage("Run : SummonsFight 2")

        -- For each of our performable actions we need to determine which should run!
        if ( #self.SummonsFightList > 0 ) then
            if ( #self.SummonsFightList == 1 ) then
                self.SummonsFightList[1]()
            else
                self.SummonsFightList[math.random(1,#self.SummonsFightList)]()
            end
        end

        if( not self.Owner:IsValid() or self.ParentObj:DistanceFrom(self.Owner) > SorceryHelper.MaxSummonDistance ) then
            States.SummonDeath.Run(self)
        end

    end,
}

States.SummonDeath = {
    Name = "SummonDeath",
    Init = function(self)
        --DebugMessage("Init : SummonDeath")
        self.DoDeath = function( self )
            PlayEffectAtLoc("RedCoreImpactWaveEffect",self.ParentObj:GetLoc(),0.5)
            self.ParentObj:Destroy()
        end

        RegisterSingleEventHandler(EventType.Message, "SorceryMessage", function( message, args )
            if(message == "EndSorceryEffects") then
                self.DoDeath(self)
            elseif( message == "EmpowerSummon" ) then

                local healthTimes = args.MaxHealthTimes or 0
                local attackTimes = args.MaxAttackTimes or 0

                SetMobileMod(self.ParentObj, "MaxHealthTimes", "EmpowerSummon", healthTimes)
                SetMobileMod(self.ParentObj, "AttackTimes", "EmpowerSummon", attackTimes)
                
                CallFunctionDelayed(TimeSpan.FromMilliseconds(200), function() 
                    SetCurHealth(self.ParentObj,GetMaxHealth(self.ParentObj))
                end)
                
                self.ParentObj:PlayEffect("RedShadowFogEffect")

            end

        end)

        CallFunctionDelayed(TimeSpan.FromSeconds(1),function()
            local duration = self.ParentObj:GetObjVar("SummonDuration") or SorceryHelper.SummonDuration
            CallFunctionDelayed(TimeSpan.FromSeconds(duration),function()
                self.DoDeath(self)
            end)
        end)
        
    end,
    ShouldRun = function(self)
        return IsDead(self.ParentObj)
    end,
    Run = function(self)
        --DebugMessage("Run : SummonDeath")
        self.SetTarget(nil)
        self.DoDeath(self)
        return true
    end,
}

States.SummonAttack = {
    Name = "SummonAttack",
    Init = function(self)
        --DebugMessage("Init : SummonAttack")
        self.Owner = self.ParentObj:GetObjVar("controller")
        self.AttackTarget = nil
    end,
    ShouldRun = function(self)
        if(  self.CurrentTarget ~= nil ) then return false end
        
        local target = GetNearestCombatTarget( self.Owner, SorceryHelper.SummonTargetRange, self.Owner:GetLoc() )
        if( target and (ShareGroup( self.Owner, target ) == false or ShareGroupConsent(self.Owner, target)) ) then
            self.AttackTarget = target
            return true            
        end
        return false
    end,
    Run = function(self)
        --DebugMessage("Run : SummonAttack")
        self.CurrentTarget = self.AttackTarget
    end,
}

States.SummonFollow = {
    Name = "SummonFollow",
    Init = function(self)
        --DebugMessage("Init : SummonFollow")
        States.SummonFollow.ClearPath(self)
        self.IsPathing = false
        self.Owner = self.ParentObj:GetObjVar("controller")
    end,
    ShouldRun = function(self)
        return (not self.CurrentTarget)
    end,
    Run = function(self)
        --DebugMessage("Run : SummonFollow")

        if ( IsMounted(self.Owner) ) then
            -- go mount speed
            States.SummonFollow.PathToTarget(
                self,
                self.Owner,
                ServerSettings.Pets.Follow.Distance,
                ServerSettings.Pets.Follow.Speed.Mounted
            )
        else
            States.SummonFollow.PathToTarget(
                self,
                self.Owner,
                ServerSettings.Pets.Follow.Distance,
                ServerSettings.Pets.Follow.Speed.OnFoot
            )
        end
        
    end,

    ClearPath = function( self )
        if ( self.IsPathing ) then
            self.ParentObj:ClearPathTarget()
            self.IsPathing = false
        end
    end,

    PathToTarget = function(self, target, distance, speed)
        States.SummonFollow.ClearPath(self)
        if not ( target ) then return end
        self.ParentObj:PathToTarget(target, distance, speed)
        self.IsPathing = true
    end
}