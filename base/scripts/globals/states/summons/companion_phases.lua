States.CompanionFight = {
    Name = "CompanionFight",
    Init = function(self)
        --DebugMessage("Init : SummonsFight")
        -- call base fight
        States.FightAdvanced.Init(self)
    end,

    ShouldRun = function(self)
        return States.FightAdvanced.ShouldRun(self)
    end,

    Run = function(self)
        States.FightAdvanced.Run(self)
    end,
}

States.CompanionDeath = {
    Name = "CompanionDeath",

    ShouldRun = function(self)
        return IsDead(self.ParentObj)
    end,

    Run = function(self)
        self.SetTarget(nil)
        return true
    end,
}

States.CompanionAttack = {
    Name = "CompanionAttack",
    Init = function(self)
        self.Owner = self.ParentObj:GetObjVar("controller")
        self.AttackTarget = nil
    end,
    ShouldRun = function(self)
        if(  self.CurrentTarget ~= nil ) then return false end
        
        local target = GetNearestCombatTarget( self.Owner, SorceryHelper.SummonTargetRange, self.Owner:GetLoc() )
        if( target ) then
            self.AttackTarget = target
            return true
        else
            return false
        end
    end,
    Run = function(self)
        self.CurrentTarget = self.AttackTarget
    end,
}

States.CompanionFollow = {
    Name = "CompanionFollow",
    Init = function(self)
        --DebugMessage("Init : SummonFollow")
        States.CompanionFollow.ClearPath(self)
        self.IsPathing = false
        self.Owner = self.ParentObj:GetObjVar("controller")
    end,
    ShouldRun = function(self)
        return (not self.CurrentTarget)
    end,
    Run = function(self)
        if( self.Owner ) then
            if( self.Owner:IsValid() ) then
                self.SpawnLocation = self.Owner:GetLoc()
                if ( IsMounted(self.Owner) ) then
                    -- go mount speed
                    States.SummonFollow.PathToTarget(
                        self,
                        self.Owner,
                        ServerSettings.Pets.Follow.Distance,
                        ServerSettings.Pets.Follow.Speed.Mounted
                    )
                else
                    States.SummonFollow.PathToTarget(
                        self,
                        self.Owner,
                        ServerSettings.Pets.Follow.Distance,
                        ServerSettings.Pets.Follow.Speed.OnFoot
                    )
                end
            else
                -- Our owner is gone, decayed?, so we need to disappear.
                self.ParentObj:Destroy()
            end
        end
    end,

    ClearPath = function( self )
        if ( self.IsPathing ) then
            self.ParentObj:ClearPathTarget()
            self.IsPathing = false
        end
    end,

    PathToTarget = function(self, target, distance, speed)
        States.SummonFollow.ClearPath(self)
        if not ( target ) then return end
        self.ParentObj:PathToTarget(target, distance, speed)
        self.IsPathing = true
    end
}