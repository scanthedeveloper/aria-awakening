
States.Death = {

    Name = "Death",
    Init = function(self)
        RegisterEventHandler(EventType.Message, "OnResurrect", function()
            -- resume on a resurrect
            self.Schedule()
        end)
    end,

    ShouldRun = function(self)
        return IsDead(self.ParentObj)
    end,

    Run = function(self)
        self.SetTarget(nil)
        -- stop all ai while dead.
        local looters = self.AggroList or nil
        if( looters ) then
            local tag = self.ParentObj:GetObjVar("Tag") or {}
            local maxHealthThreshold = math.floor( GetMaxHealth(self.ParentObj) / 20)
            for player, aggro in pairs(looters) do
                -- Add players to the tagged list
                if( aggro >= maxHealthThreshold or aggro >= 5000 ) then
                    tag[player]= true
                end
            end

            self.ParentObj:SetObjVar("Tag", tag)
        end
        return true
    end,

    DistributeLoot = function(self, distributedLootTable)
        --DebugMessage( "Distribute Loot for " .. self.ParentObj:GetName() )
        -- Do we need to distribute loot?
        distributedLootTable = distributedLootTable or self.ParentObj:GetObjVar("DistrbutedLootTable")
        if( not self.LootDistributed and distributedLootTable ) then
            loottable = {TemplateDefines.LootTable[distributedLootTable]}
            local looters = self.AggroList or nil
            if( looters ) then
                local tag = self.ParentObj:GetObjVar("Tag") or {}
                local maxHealthThreshold = math.floor( GetMaxHealth(self.ParentObj) / 20)
                local numIndexedTable = {}
                local counter = 1
                for player, aggro in pairs(looters) do
                    if ( player and type(player) == "userdata" and player:IsValid() and not IsDead(player)) then
                        numIndexedTable[counter] = player
                        counter = counter + 1
                    end
                end
                DistributeBossRewards(numIndexedTable, loottable, nil, false, GetGuardProtection(self.ParentObj))

                for i=1, #numIndexedTable do
                    if distributedLootTable == "AwakeningAzura" then
                        Lore.EarnLoreIfNew(numIndexedTable[i], "ShatteringOfAria", {1}, true)
                    elseif distributedLootTable == "AwakeningEnt" then
                        Lore.EarnLoreIfNew(numIndexedTable[i], "ShatteringOfAria", {2}, true)
                    elseif distributedLootTable == "AwakeningDragon" then
                        Lore.EarnLoreIfNew(numIndexedTable[i], "ShatteringOfAria", {3}, true)
                    -- Awakening Cultist handled in ai_awakening_dragon for entry 4
                    elseif distributedLootTable == "TundraHighPriest" then
                        Lore.EarnLoreIfNew(numIndexedTable[i], "ShatteringOfAria", {5,6}, true)
                    end
                end
            end
            
            self.LootDistributed = true
        end
    end,
}