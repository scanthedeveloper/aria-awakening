
States.SummonCompanion = {
    Name = "SummonCompanion",
    Init = function(self)
        self.Companion = self.ParentObj:GetObjVar("Companion") or nil
        self.CompanionTemplate = self.ParentObj:GetObjVar("CompanionTemplate")

    end,
    ShouldRun = function(self)
        return (self.CompanionTemplate and self.Companion == nil)
    end,
    Run = function(self)

        Create.AtLoc( self.CompanionTemplate, self.ParentObj:GetLoc() ,function( summonObj )
            summonObj:SetObjVar("controller", self.ParentObj)
            summonObj:SetObjVar("IsSummonedPet", true)
            summonObj:DelObjVar("RecentlySummoned")
            self.Companion = summonObj
            self.ParentObj:SetObjVar("Companion", self.Companion)
        end)

    end,
}