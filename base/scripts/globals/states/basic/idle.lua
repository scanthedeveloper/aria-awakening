
States.Idle = {
    Name = "Idle",
    Init = function(self)
    end,
    ShouldRun = function(self)
        return (not self.IsPathing)
    end,
    Run = function(self)
        local maxHP = GetMaxHealth(self.ParentObj)
        if not( GetCurHealth(self.ParentObj) == maxHP ) then
            SetCurHealth(self.ParentObj, maxHP)
        end
    end,
}