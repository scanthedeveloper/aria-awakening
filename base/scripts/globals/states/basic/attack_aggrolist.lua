States.AttackAggroList = {
    Name = "AttackAggroList",
    Init = function(self)
        self.AggroRange = 10
    end,
    ShouldRun = function(self)
        if ( self.CurrentTarget ~= nil ) then return false end
        --DebugMessage( "ShouldRun: AttackAggroList" )
        self.AttackTarget = States.Aggro.GetHighestAggroInLeash( self )
        --DebugMessage( "AttackTarget", tostring( self.AttackTarget ) )
        return ( self.AttackTarget ~= nil )
    end,
    Run = function(self)
        --DebugMessage( "Running: AttackAggroList" )
        if ( self.AttackTarget ~= nil ) then
            self.PathTo(self.AttackTarget:GetLoc(), 4)
            self.SetTarget(self.AttackTarget)
            self.CurrentTarget = self.AttackTarget
            self.AttackTarget = nil
        end
    end,
}