
States.Leash = {
    Name = "Leash",
    Init = function(self)
        if not( self.ParentObj:HasObjVar("SpawnLocation") ) then
            self.SpawnLocation = self.Loc
            self.ParentObj:SetObjVar("SpawnLocation", self.SpawnLocation)
        end
        if not ( self.SpawnLocation ) then
            self.SpawnLocation = self.ParentObj:GetObjVar("SpawnLocation")
        end
        if not( self.LeashTimeout ) then
            self.LeashTimeout = TimeSpan.FromSeconds(30)
        end

        self.LeashDistance =  self.ParentObj:GetObjVar("AI-LeashDistance") or 45

    end,
    EnterState = function(self)
        States.Aggro.ClearAggro(self)
        SetMobileMod(self.ParentObj, "HealthRegenTimes", "Leashing", 1000)
        self.ParentObj:SetObjVar("Invulnerable", true)
        self.LeashTimeoutAt = DateTime.UtcNow:Add(self.LeashTimeout)
    end,
    ExitState = function(self)
        States.Aggro.ClearAggro(self)
        SetMobileMod(self.ParentObj, "HealthRegenTimes", "Leashing", nil)
        self.ParentObj:DelObjVar("Invulnerable")
        self.LeashTimeoutAt = nil
    end,
    ShouldRun = function(self)
        return (
            self.IsLeashing
            or
            (
                ( self.CurrentTarget == nil and self.Loc:Distance(self.SpawnLocation) > (self.WanderMax or 8) + 3 )
                or
                ( self.Loc:Distance(self.SpawnLocation) >= self.LeashDistance )
            )
        )
    end,
    Run = function(self)
        if ( self.IsLeashing ) then
            if ( self.LeashTimeoutAt ~= nil and DateTime.UtcNow >= self.LeashTimeoutAt ) then
                -- timed out, teleport them
                PlayEffectAtLoc("TeleportFromEffect", self.Loc)
                self.ParentObj:SetWorldPosition(self.SpawnLocation)
                self.PathClear()
            end
            if not( self.IsPathing ) then
                self.IsLeashing = nil
            end
        else
            self.SetTarget(nil)
            if ( self.Loc:Distance(self.SpawnLocation) >= MAX_PATHTO_DIST ) then
                -- teleport them, too far away
                PlayEffectAtLoc("TeleportFromEffect", self.Loc)
                self.ParentObj:SetWorldPosition(self.SpawnLocation)
                return
            end
            self.PathTo(self.SpawnLocation, (self.LeashSpeed or 8))
            if ( self.IsPathing ) then
                self.IsLeashing = true
            end
        end

        CallFunctionDelayed(TimeSpan.FromSeconds(5),function()
            self.ParentObj:DelObjVar("Invulnerable")
            SetMobileMod(self.ParentObj, "HealthRegenTimes", "Leashing", nil)
        end)
    end,
}