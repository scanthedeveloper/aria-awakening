
States.Patrol = {
    Name = "Patrol",
    Init = function(self)
        if not( self.ParentObj:HasObjVar("SpawnLocation") ) then
            self.SpawnLocation = self.Loc
            self.ParentObj:SetObjVar("SpawnLocation", self.SpawnLocation)
        end
        if not ( self.SpawnLocation ) then
            self.SpawnLocation = self.ParentObj:GetObjVar("SpawnLocation")
        end

        self.curPathIndex = self.ParentObj:GetObjVar("curPathIndex") or 1
        self.stopChance = self.ParentObj:GetObjVar("stopChance") or 0
        self.stopDelay = self.ParentObj:GetObjVar("stopDelay") or 3000
        self.isRunning = self.ParentObj:GetObjVar("isRunning") or 0  
        
        --DebugMessage( "self.ParentObj:GetObjVar(\"MyPath\")", self.ParentObj:GetObjVar("MyPath") )
        self.MyPath = GetPath(self.ParentObj:GetObjVar("MyPath")) or PatrolData[self.ParentObj:GetObjVar("MyPath")]
        --DebugMessage( "self.MyPath", tostring(self.MyPath) )
    end,
    ShouldRun = function(self)
        return (not self.IsPathing)
    end,
    Run = function(self)
        
        local shouldWait = math.random(0, 100)
        if( shouldWait <= self.stopChance ) then
            self.ParentObj:ScheduleTimerDelay(TimeSpan.FromMilliseconds(self.stopDelay), "patrolPause")
        else
            --DebugMessage( "#self.MyPath", #self.MyPath )
            self.curPathIndex = ((self.curPathIndex % #self.MyPath) + 1)
            local nextLoc = States.Patrol.GetNextLoc(self)
            self.ParentObj:PathTo(nextLoc,1.7,"patrol")
            self.SpawnLocation = nextLoc

        end  
    end,

    GetNextLoc = function(self)
        --DebugMessage( "self.curPathIndex", self.curPathIndex )
        --local deviation = Loc(math.random()*2-1,0,math.random()*2-1)
        currentDestination = self.MyPath[self.curPathIndex]
        return currentDestination
    end,

    GetNearestPathNode = function(self)
        local closestNode = nil
        local closestDistance = 0

        for index,loc in pairs(self.MyPath) do
            local dist = self.ParentObj:GetLoc():Distance(loc)
            if(closestNode == nil or dist < closestDistance) then
                closestNode = index
                closestDistance = dist
            end
        end

        return closestNode
    end,





}