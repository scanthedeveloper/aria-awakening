
States.Aggro = {
    Name = "Aggro",
    OnAggro = function(self, damager, amount)
        if ( self and damager and damager:IsValid() and damager:GetObjVar("MobileTeamType") ~= self.TeamType ) then

            if IsPet(damager) then
                --DebugMessage("PET HIT!")
                local owner = GetPetOwner(damager)
                if self.AggroList[owner] == nil then 
                    --DebugMessage("ADDED OWNER:", tostring(owner))
                    self.AggroList[owner] = 1 
                end
            end

            if not( self.AggroList[damager] ) then self.AggroList[damager] = 0 end
            self.AggroList[damager] = math.max(self.AggroList[damager] + (amount or 1) or 0)
            if ( self.AggroMost[2] < self.AggroList[damager] or not self.AggroMost[1]:IsValid() or IsDead( self.AggroMost[1] ) ) then
                self.AggroMost = {damager, self.AggroList[damager]}
            end
        end

        -- If we are ignoring aggro we don't want to let Aggro set a new target
        if( self.IgnoreAggro ) then return end
        
        -- attack the damager with the most aggro
        if ( self.AggroMost[1] and self.AggroMost[1] ~= self.CurrentTarget and self.ValidCombatTarget(self.AggroMost[1]) ) then
            self.SetTarget(self.AggroMost[1])
            --self.Pulse()
        end
    end,

    ClearAggro = function(self)
        self.AggroList = {}
        self.AggroMost = {nil,0}
    end,

    ForceAggro = function( self, _target )
        States.Aggro.OnAggro(self, _target, ( self.AggroMost[2] + 1000 ))
    end,

    -- Forces the aggro list to determine who the current aggro leader is and 
    -- sets them to the AggroMost key.
    SortAggro = function( self )
        local highestAggro = 0
        local newLeader = nil
        cb = cb or function() end
        for damager, aggro in pairs( self.AggroList ) do 
            if( aggro > highestAggro ) then
                newLeader = damager
                highestAggro = aggro
            end
        end

        self.AggroMost = {newLeader, highestAggro}
    end,

    -- Gets the high aggro target that is within range of the spawn location
    GetHighestAggroInLeash = function( self )
        local highestAggro = 0
        local newLeader = nil

        --DebugMessage( "Locating target..." )
        --DebugMessage( "AggroList", tostring( self.AggroList ) )
        --DebugMessage( "SpawnLocation", tostring( self.SpawnLocation ) )
        --DebugMessage( "LeashDistance", tostring( self.LeashDistance ) )

        if( self.AggroList and self.SpawnLocation and self.LeashDistance ) then
            --DebugMessage( "We are GREEN!" )
            for damager, aggro in pairs( self.AggroList ) do 
                if( aggro > highestAggro and damager:GetLoc():Distance( self.SpawnLocation ) < self.LeashDistance ) then
                    newLeader = damager
                    highestAggro = aggro
                    --DebugMessage( "NewAggroLeader", newLeader )
                end
            end
        end

        self.AggroMost = {newLeader, highestAggro}
        return newLeader
    end,

    ForeachOnAggroList = function( self, cb, includeDead, playersOnly )
        if(cb == nil) then cb = function()end end
        for damager, aggro in pairs( self.AggroList ) do
            if ( IsDead(damager) ) then
                if( includeDead ) then
                    cb(damager) 
                end
            else
                if( IsGod(damager) and not TestMortal(damager) ) then
                    -- we don't include gods
                else
                    if( playersOnly ) then
                        if( IsPlayerCharacter(damager) ) then
                            cb(damager)
                        end
                    else
                        cb(damager) 
                    end
                end
            end
        end
    end,

    Init = function(self)
        self.AggroList = {}
        self.AggroMost = {nil,0}
        RegisterEventHandler(EventType.Message, "DamageInflicted", function(damager, amount)
            States.Aggro.OnAggro(self, damager, amount)
        end)
        RegisterEventHandler(EventType.Message, "SwungOn", function(attacker)
            States.Aggro.OnAggro(self, attacker)
        end)
        RegisterEventHandler(EventType.Message, "AddAggro", function(attacker, amount)
            States.Aggro.OnAggro(self, attacker, amount)
        end)
        RegisterEventHandler(EventType.Message, "ForceAggro", function(attacker)
            States.Aggro.ForceAggro(self, attacker)
        end)
        RegisterEventHandler(EventType.Message, "RemoveAggro", function(attacker)
            if( self.AggroList[attacker] ) then
                self.AggroList[attacker] = nil
                States.Aggro.SortAggro(self)
            end
        end)
    end,
    --TODO: ShouldRun/Run to clear aggro list
}