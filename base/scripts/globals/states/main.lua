require 'globals.states.fsm'

States = {}
require 'globals.states.basic.aggro'
require 'globals.states.basic.attack'
require 'globals.states.basic.death'
require 'globals.states.basic.fight'
require 'globals.states.basic.fightadvanced'
require 'globals.states.basic.flee'
require 'globals.states.basic.leash'
require 'globals.states.basic.wander'
require 'globals.states.basic.patrol'
require 'globals.states.basic.idle'
require 'globals.states.basic.attack_aggrolist'
require 'globals.states.basic.summon_companion'

require 'globals.states.champions.medeina_phases'
require 'globals.states.champions.misha_phases'
require 'globals.states.champions.minion_phases'

require 'globals.states.bosses.easy_boss_phases'
require 'globals.states.bosses.yeward_skullcarver'
require 'globals.states.bosses.drained_construct'
require 'globals.states.bosses.monolith_conductor'
require 'globals.states.bosses.monolith_armsmaster'
require 'globals.states.bosses.monolith_renmer'
require 'globals.states.bosses.militia_champions'
require 'globals.states.bosses.awakening_ent_boss'
require 'globals.states.bosses.awakening_dragon_boss'
require 'globals.states.bosses.awakening_azura_boss'
require 'globals.states.bosses.belgae_warmaster'
require 'globals.states.bosses.wendigo_boss'
require 'globals.states.bosses.azure_highpriest_states'
require 'globals.states.bosses.azura_devotee_states'



require 'globals.states.summons.summons_phases'
require 'globals.states.summons.companion_phases'

require 'globals.states.groups.fight_damage'
require 'globals.states.groups.fight_healer'
require 'globals.states.groups.fight_tank'

require 'globals.states.monolith_misc_states'

