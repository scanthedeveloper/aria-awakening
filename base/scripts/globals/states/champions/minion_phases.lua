States.MinionWander = {
    Name = "MinionWander",
    Init = function(self)
        States.Wander.Init(self)
    end,
    ShouldRun = function(self)
        return States.Wander.ShouldRun(self)
    end,
    Run = function(self)
        States.Wander.Run(self)
        local leader = self.ParentObj:GetObjVar("MobileLeader") or nil
        
        -- Makes the minions despawn if their leader dies, is destroyed, or starts to wander.
        if( leader ) then
            if( not leader:IsValid() or IsDead(leader) or leader:HasObjVar("Wandering") ) then
                self.ParentObj:Destroy()
            end
        end
    end,
}