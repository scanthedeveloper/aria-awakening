States.MishaPhaseOne = {
    Name = "MishaPhaseOne",
    Init = function(self)
        -- call base fight
        States.Fight.Init(self)
        self.MishaPhaseOneList = {}
        if( self.StartingHealth == nil ) then
            self.StartingHealth = GetMaxHealth(self.ParentObj)
            self.CurrentHealthBuff = nil
        end
        -- Abilities
        
        -- Charge
        self.MishaPhaseOneList[#self.MishaPhaseOneList+1] = function()
            self.NPCSpeech = "*becomes enraged*"
            FSMHelper.UseCombatAbility(self, "Charge", 8)
        end

        -- AoE Stamina Reduction
        self.MishaPhaseOneList[#self.MishaPhaseOneList+1] = function()
            self.NPCSpeech = "*roars*"
            FSMHelper.TriggerSelfMobileEffect(
                self,
                "AoeAdjustStamina",
                30, -- delay in seconds
                self.CurrentTarget,
                {
                    Range = 20,
                    Amount = -20
                }
            )
        end
    end,
    ShouldRun = function(self)
        local run = false
        if( States.Fight.ShouldRun(self) and GetCurHealth(self.ParentObj) > (GetMaxHealth(self.ParentObj) * 0.66) ) then
            run = true
        end
        return run
    end,
    Run = function(self)
        -- call base fight
        States.Fight.Run(self)
        if not( self.CurrentTarget ) then return end

        if ( #self.MishaPhaseOneList > 0 ) then
            if ( #self.MishaPhaseOneList == 1 ) then
                self.MishaPhaseOneList[1]()
            else
                self.MishaPhaseOneList[math.random(1,#self.MishaPhaseOneList)]()
            end
        end
    end,
}

States.MishaPhaseTwo = {
    Name = "MishaPhaseTwo",
    Init = function(self)
        -- call base fight
        States.Fight.Init(self)
        self.MishaPhaseTwoList = {}
        
        -- Abilities

        -- Charge
        self.MishaPhaseTwoList[#self.MishaPhaseTwoList+1] = function()
            self.NPCSpeech = "*becomes enraged*"
            FSMHelper.UseCombatAbility(self, "Charge", 8)
        end

        -- Summon Bears
        self.MishaPhaseTwoList[#self.MishaPhaseTwoList+1] = function()
            self.NPCSpeech = "*calls on her cubs*"
            FSMHelper.TriggerSelfMobileEffect(
                self,
                "SummonMobiles",
                120,
                self.CurrentTarget,
                {
                    template = "fsm_enraged_dire_bear",
                    amount = 4,
                    range = 10,
                    leader = self.ParentObj
                }
            )
        end


    end,
    ShouldRun = function(self)
        local run = false
        if( States.Fight.ShouldRun(self) and GetCurHealth(self.ParentObj) < (GetMaxHealth(self.ParentObj) * 0.66) and GetCurHealth(self.ParentObj) > (GetMaxHealth(self.ParentObj) * 0.33 ) ) then
            run = true
        end
        return run
    end,
    Run = function(self)
        -- call base fight
        States.Fight.Run(self)
        if not( self.CurrentTarget ) then return end

        if ( #self.MishaPhaseTwoList > 0 ) then
            if ( #self.MishaPhaseTwoList == 1 ) then
                self.MishaPhaseTwoList[1]()
            else
                self.MishaPhaseTwoList[math.random(1,#self.MishaPhaseTwoList)]()
            end
        end
    end,
}

States.MishaPhaseThree = {
    Name = "MishaPhaseThree",
    Init = function(self)
        -- call base fight
        States.Fight.Init(self)
        self.MishaPhaseThreeList = {}
        -- Abilities

        -- Charge
        self.MishaPhaseThreeList[#self.MishaPhaseThreeList+1] = function()
            self.NPCSpeech = "*becomes enraged*"
            FSMHelper.UseCombatAbility(self, "Charge", 8)
        end

        -- Overpower
        self.MishaPhaseThreeList[#self.MishaPhaseThreeList+1] = function()
            self.NPCSpeech = "*growls*"
            FSMHelper.UseWeaponAbility(self, "Overpower", 15)
        end

        -- AoE Stamina Reduction
        self.MishaPhaseThreeList[#self.MishaPhaseThreeList+1] = function()
            self.NPCSpeech = "*roars loudly*"
            FSMHelper.TriggerSelfMobileEffect(
                self,
                "AoeAdjustStamina",
                30,
                self.CurrentTarget,
                {
                    Range = 20,
                    Amount = -30
                }
            )
        end

    end,
    ShouldRun = function(self)
        local run = false
        if( States.Fight.ShouldRun(self) and GetCurHealth(self.ParentObj) < (GetMaxHealth(self.ParentObj) * 0.33) ) then
            run = true
        end
        return run
    end,
    Run = function(self)
        -- call base fight
        States.Fight.Run(self)
        if not( self.CurrentTarget ) then return end

        if ( #self.MishaPhaseThreeList > 0 ) then
            if ( #self.MishaPhaseThreeList == 1 ) then
                self.MishaPhaseThreeList[1]()
            else
                self.MishaPhaseThreeList[math.random(1,#self.MishaPhaseThreeList)]()
            end
        end
    end,
}

States.MishaWander = {
    Name = "MishaWander",
    Init = function(self)
        States.Wander.Init(self)
    end,
    ShouldRun = function(self)
        return States.Wander.ShouldRun(self)
    end,
    Run = function(self)
        States.Wander.Run(self)
    end,
    EnterState = function(self)
        self.ParentObj:SetObjVar("Wandering", true)
    end,
    ExitState = function(self)
        self.ParentObj:DelObjVar("Wandering")
    end,
}

States.MishaDeath = {
    Name = "MishaDeath",
    Init = function(self)
        States.Death.Init(self)
    end,
    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,
    Run = function(self)
        States.Death.Run(self)
    end,
}