States.MedeinaPhaseOne = {
    Name = "MedeinaPhaseOne",
    Init = function(self)
        -- call base fight
        States.Fight.Init(self)
        self.MedeinaPhaseOneList = {}

        -- Summon Wolves
        self.MedeinaPhaseOneList[#self.MedeinaPhaseOneList+1] = function()
            self.NPCSpeech = "Come my children!"
            FSMHelper.TriggerSelfMobileEffect(
                self,
                "SummonMobiles",
                15,
                self.CurrentTarget,
                {
                    template = "fsm_enraged_dire_wolf",
                    amount = 2,
                    range = 10,
                    leader = self.ParentObj
                }
            )
        end

        -- Charge
        self.MedeinaPhaseOneList[#self.MedeinaPhaseOneList+1] = function()
            self.NPCSpeech = "Feel my bite!"
            FSMHelper.UseCombatAbility(self, "Charge", 8)
        end
    end,
    ShouldRun = function(self)
        local run = false
        if( States.Fight.ShouldRun(self) and GetCurHealth(self.ParentObj) > (GetMaxHealth(self.ParentObj) * 0.66) ) then
            run = true
        end
        return run
    end,
    Run = function(self)
        -- call base fight
        States.Fight.Run(self)
        if not( self.CurrentTarget ) then return end

        if ( #self.MedeinaPhaseOneList > 0 ) then
            if ( #self.MedeinaPhaseOneList == 1 ) then
                self.MedeinaPhaseOneList[1]()
            else
                self.MedeinaPhaseOneList[math.random(1,#self.MedeinaPhaseOneList)]()
            end
        end
    end,
}

States.MedeinaPhaseTwo = {
    Name = "MedeinaPhaseTwo",
    Init = function(self)
        -- call base fight
        States.Fight.Init(self)
        self.MedeinaPhaseTwoList = {}
        --SetMobileMod(self.ParentObj, "MaxHealthPlus", "healthplus", self.StartingMaxHealth)
        
        -- Summon Wolves
        self.MedeinaPhaseTwoList[#self.MedeinaPhaseTwoList+1] = function()
            self.NPCSpeech = "My children, your mother is in need!"
            FSMHelper.TriggerSelfMobileEffect(
                self,
                "SummonMobiles",
                15,
                self.CurrentTarget,
                {
                    template = "fsm_enraged_dire_wolf",
                    amount = 4,
                    range = 10,
                    leader = self.ParentObj
                }
            )
        end

        -- Charge
        self.MedeinaPhaseOneList[#self.MedeinaPhaseOneList+1] = function()
            self.NPCSpeech = "Feel my bite."
            FSMHelper.UseCombatAbility(self, "Charge", 8)
        end
    end,
    ShouldRun = function(self)
        local run = false
        if( States.Fight.ShouldRun(self) and GetCurHealth(self.ParentObj) < (GetMaxHealth(self.ParentObj) * 0.66) and GetCurHealth(self.ParentObj) > (GetMaxHealth(self.ParentObj) * 0.33 ) ) then
            run = true
        end
        return run
    end,
    Run = function(self)
        -- call base fight
        States.Fight.Run(self)
        if not( self.CurrentTarget ) then return end

        if ( #self.MedeinaPhaseTwoList > 0 ) then
            if ( #self.MedeinaPhaseTwoList == 1 ) then
                self.MedeinaPhaseTwoList[1]()
            else
                self.MedeinaPhaseTwoList[math.random(1,#self.MedeinaPhaseTwoList)]()
            end
        end
    end,
}

States.MedeinaPhaseThree = {
    Name = "MedeinaPhaseThree",
    Init = function(self)
        -- call base fight
        States.Fight.Init(self)
        self.MedeinaPhaseThreeList = {}
        --SetMobileMod(self.ParentObj, "MaxHealthPlus", "healthplus", (self.StartingMaxHealth * 3))

        -- Summon Wolves
        self.MedeinaPhaseThreeList[#self.MedeinaPhaseThreeList+1] = function()
            self.NPCSpeech = "Arise my children, I still stand."
            FSMHelper.TriggerSelfMobileEffect(
                self,
                "MassResurrectMobiles",
                20,
                self.CurrentTarget,
                {
                    template = "fsm_enraged_dire_wolf",
                    amount = 0,
                    range = 30,
                    leader = self.ParentObj
                }
            )
        end

        -- Charge
        self.MedeinaPhaseThreeList[#self.MedeinaPhaseThreeList+1] = function()
            self.NPCSpeech = "Feel my bite."
            FSMHelper.UseCombatAbility(self, "Charge", 8)
        end

    end,
    ShouldRun = function(self)
        local run = false
        if( States.Fight.ShouldRun(self) and GetCurHealth(self.ParentObj) < (GetMaxHealth(self.ParentObj) * 0.33) ) then
            run = true
        end
        return run
    end,
    Run = function(self)
        -- call base fight
        States.Fight.Run(self)
        if not( self.CurrentTarget ) then return end

        if ( #self.MedeinaPhaseThreeList > 0 ) then
            if ( #self.MedeinaPhaseThreeList == 1 ) then
                self.MedeinaPhaseThreeList[1]()
            else
                self.MedeinaPhaseThreeList[math.random(1,#self.MedeinaPhaseThreeList)]()
            end
        end
    end,
}

States.MedeinaWander = {
    Name = "MedeinaWander",
    Init = function(self)
        States.Wander.Init(self)
    end,
    ShouldRun = function(self)
        return States.Wander.ShouldRun(self)
    end,
    Run = function(self)
        States.Wander.Run(self)
    end,
    EnterState = function(self)
        self.ParentObj:SetObjVar("Wandering", true)
    end,
    ExitState = function(self)
        self.ParentObj:DelObjVar("Wandering")
    end,
}

States.MedeinaDeath = {
    Name = "MedeinaDeath",
    Init = function(self)
        States.Death.Init(self)
    end,
    ShouldRun = function(self)
        return States.Death.ShouldRun(self)
    end,
    Run = function(self)
        States.Death.Run(self)
    end,
}