function GetNearbyPassableLoc(targetObj,angleRange,minDist,maxDist)
	if(targetObj == nil) then
		targetObj = this
	end

	if(not(targetObj) or not(targetObj:IsValid())) then
		return nil
	end

	angleRange = angleRange or 360
	minDist = minDist or 3
	maxDist = maxDist or 10

	local maxTries = 20
    local moveAngle = math.random(angleRange,angleRange*2)-angleRange
    local nearbyLoc = targetObj:GetLoc():Project(moveAngle, math.random(minDist,maxDist))
    -- try to find a passable location
    while(maxTries > 0 and not(IsPassable(nearbyLoc)) ) do
    	local moveAngle = math.random(angleRange,angleRange*2)-angleRange
        nearbyLoc = targetObj:GetLoc():Project(moveAngle, math.random(minDist,maxDist))
        maxTries = maxTries - 1
    end

    return nearbyLoc
end

function GetNearbyPassableLocFromLoc(targetLoc,minDist,maxDist,user)	
	local angleRange = 360
	minDist = minDist or 3
	maxDist = maxDist or 10

	local maxTries = 20
    local moveAngle = math.random(angleRange,angleRange*2)-angleRange
    local nearbyLoc = targetLoc:Project(moveAngle, math.random(minDist,maxDist))
    -- try to find a passable location

    local controller = Plot.GetAtLoc(nearbyLoc)
    local isUserAStranger = false
    if user and controller then
        isUserAStranger = Plot.IsStranger(controller, user)
    end

    while(maxTries > 0 and not(IsPassable(nearbyLoc) and not isUserAStranger)) do
    	local moveAngle = math.random(angleRange,angleRange*2)-angleRange
        nearbyLoc = targetLoc:Project(moveAngle, math.random(minDist,maxDist))
        maxTries = maxTries - 1
        controller = Plot.GetAtLoc(nearbyLoc)
        if user and controller then
            isUserAStranger = Plot.IsStranger(controller, user)
        elseif not controller then
            isUserAStranger = false
        end
    end

    return nearbyLoc
end

function IsValidLoc(spawnLoc,excludeHousing,excludeRegions)
	if(spawnLoc == nil) then
		LuaDebugCallStack("Invalid Location!")
		return false
	end

	if not(IsPassable(spawnLoc)) then
		return false
	end

	if(excludeHousing) then
		local plotController = Plot.GetAtLoc(spawnLoc)
		if ( plotController ~= nil ) then return false end
	end
	if (excludeRegions ~= nil)then
		for i, region in pairs (excludeRegions) do
			if (IsLocInRegion(spawnLoc, region)) then
				return false
			end
		end
    end
    
	return true
end

function GetRandomPassableLocationFromRegion(region,excludeHousing, excludeRegions)
    local maxTries = 20
    local spawnLoc = region:GetRandomLocation()
    -- try to find a passable location
    while(maxTries > 0 
    		and not(IsValidLoc(spawnLoc,excludeHousing,excludeRegions)) ) do
        spawnLoc = region:GetRandomLocation()
        maxTries = maxTries - 1
    end

    if(maxTries > 0) then
	    return spawnLoc
	end

	return nil
end


function GetRandomMissionLocation(region,excludeHousing, excludeRegions, requireRegions)
    local maxTries = 20
    local spawnLoc = nil

    if( requireRegions ) then
        region = GetRegion( requireRegions[ math.random( 1, #requireRegions ) ] )        
    end

    spawnLoc = region:GetRandomLocation()

    -- try to find a passable location
    while(maxTries > 0 and not(IsValidLoc(spawnLoc,excludeHousing,excludeRegions)) ) do
        spawnLoc = region:GetRandomLocation()
        maxTries = maxTries - 1
    end

    if(maxTries > 0) then
	    return spawnLoc
	end

	return nil
end

-- Using the shortest distance between a given mobile and a given desired location, get the closest valid loc to that desired one.
function GetClosestLocationBuffered(mobileLoc, desiredLoc, stepSize, excludeHousing, excludeRegions)
    local maxTries = 9

    local distance = mobileLoc:Distance2(desiredLoc)
    local newLoc = mobileLoc:Project(mobileLoc:YAngleTo(desiredLoc), distance - stepSize)
    local keepTrying = true
    --DebugMessage("START",distance,newLoc)

    for i=1, maxTries do
        local isValid = IsValidLoc(newLoc,excludeHousing,excludeRegions)
        if keepTrying then
        --DebugMessage("TRY",i,distance,stepSize,isValid)
            if isValid then
                if stepSize > 0.25 then
                    stepSize = stepSize * 0.5
                    distance = distance + stepSize
                    newLoc = mobileLoc:Project(mobileLoc:YAngleTo(desiredLoc), distance)
                else
                    --DebugMessage("END",i,distance,stepSize,isValid)
                    keepTrying = false
                end
            else
                distance = distance - stepSize
                newLoc = mobileLoc:Project(mobileLoc:YAngleTo(desiredLoc), distance)
            end
        end
    end

    if(maxTries > 0) and IsValidLoc(newLoc,excludeHousing,excludeRegions) then
        return newLoc
    end

    return nil
end

function GetRandomPassableLocation(regionName,excludeHousing,excludeRegions)
	local region = GetRegion(regionName)
	if( region == nil or region:IsEmpty() ) then
		--LuaDebugCallStack("REGION IS NIL: "..tostring(regionName))
		return nil
	end
    return GetRandomPassableLocationFromRegion(region,excludeHousing, excludeRegions)
end 

function GetRandomPassableLocationInRadius(loc,radius,excludeHousing, excludeRegions)
    local maxTries = 20

    local spawnDist = math.random() * radius
    local spawnAngle = math.random() * 360
    local spawnLoc = loc:Project(spawnAngle,spawnDist)

    -- try to find a passable location
    while(maxTries > 0 
    		and not(IsValidLoc(spawnLoc,excludeHousing,excludeRegions)) ) do
        spawnDist = math.random() * radius
	    spawnAngle = math.random() * 360
	    spawnLoc = loc:Project(spawnAngle,spawnDist)
        maxTries = maxTries - 1
    end

    if(maxTries > 0) then
	    return spawnLoc
	end

	return nil
end 

function GetRandomPassableLocationInRadiusWithMinimum(loc,minimum,radius,excludeHousing, excludeRegions)
    local maxTries = 20

    local spawnDist = math.max(minimum, math.random() * radius)
    local spawnAngle = math.random() * 360
    local spawnLoc = loc:Project(spawnAngle,spawnDist)

    -- try to find a passable location
    while(maxTries > 0 
    		and not(IsValidLoc(spawnLoc,excludeHousing,excludeRegions)) ) do

        spawnDist = math.max(minimum, math.random() * radius)
	    spawnAngle = math.random() * 360
	    spawnLoc = loc:Project(spawnAngle,spawnDist)
        maxTries = maxTries - 1
    end

    if(maxTries > 0) then
	    return spawnLoc
	end

	return nil
end 

function GetRandomCoastalLoc()
    local loc = nil
    local waterLoc = nil
    local region = GetRegion("Water")
    if (region ~= nil) then
        for i=1,40 do
            if (loc == nil) then
                waterLoc = region:GetRandomLocation()
                loc = GetRandomPassableLocationInRadius(waterLoc,25,true, {"Water", "GuardZone"})  
            end
        end
    end
    return loc, waterLoc
end

function GetRandomPositionInRadius(loc,radius,excludeHousing, excludeRegions)
    local maxTries = 20

    local spawnDist = math.random() * radius
    local spawnAngle = math.random() * 360
    local spawnLoc = loc:Project(spawnAngle,spawnDist)

    local locPass = false

    -- try to find a passable location
    while(maxTries > 0 and not(locPass) ) do
        spawnDist = math.random() * radius
        spawnAngle = math.random() * 360
        spawnLoc = loc:Project(spawnAngle,spawnDist)
        maxTries = maxTries - 1

        locPass = true

        --Check regions
        if(spawnLoc == nil) then
            LuaDebugCallStack("Invalid Location!")
            locPass =  false
        end

        if(excludeHousing) then
            local plotController = Plot.GetAtLoc(spawnLoc)
            if ( plotController ~= nil ) then
                locPass = false
            end
        end

        if (excludeRegions ~= nil)then
            for i, region in pairs (excludeRegions) do
                if (IsLocInRegion(spawnLoc, region)) then
                    locPass = false
                end
            end
        end
    end

    return spawnLoc
end

function GetRandomLocationInObjectBounds(bounds)
    if(bounds == nil or #bounds == 0) then
        return nil
    end

    local bounds3 = bounds[math.random(#bounds)]
    local curBounds = bounds3:Flatten()

    return Loc(curBounds:GetRandomLocation())
end

function GetRandomDungeonSpawnLocationInRange( _loc, _range )

    if( _loc == nil or _range == nil ) then
        return nil
    end

    local searcher = PermanentObjSearchMulti{
            PermanentObjSearchRange(_loc, _range),
            PermanentObjSearchHasObjectBounds(),
            PermanentObjSearchVisualState("Default")
        }

    --local objs1 = FindPermanentObjects(PermanentObjSearchRegion(regionName))
    --local objs2 = FindPermanentObjects(PermanentObjSearchHasObjectBounds())
    --local objs3 = FindPermanentObjects(PermanentObjSearchVisualState("Default"))
    --DebugMessage("TEST",tostring(#objs1),tostring(#objs2),tostring(#objs3))

    local objs = FindPermanentObjects(searcher)
    if(#objs == 0) then
        --DebugMessage("ERROR: No dungeon tiles found in region "..tostring(regionName))
        return nil
    end

    local curPermObj = objs[math.random(#objs)]
    local curBounds = curPermObj:GetPermanentObjectBounds()
    local spawnLoc = GetRandomLocationInObjectBounds(curBounds)
    local maxTries = 20
    while(maxTries > 0 and (spawnLoc ~= nil and not(spawnLoc:IsValid()) or not(IsPassable(spawnLoc)) or TrapAtLocation(spawnLoc))) do--(spawnLoc,false))) do
        local curPermObj = objs[math.random(#objs)]
        local curBounds = curPermObj:GetPermanentObjectBounds()
        spawnLoc = GetRandomLocationInObjectBounds(curBounds)
        maxTries = maxTries - 1        
    end

    if(maxTries <= 0) then
        --DebugMessage("ERROR: Tried 20 times for a passable location and failed. "..regionName)
        return nil
    end

    return spawnLoc,curPermObj
    --return Loc(0,0,0),PermanentObj(0)

end

function GetRandomDungeonSpawnLocation(regionName)
    --DebugMessage("GetRandomSpawnLocation "..tostring(entry.Region))
    local region = GetRegion(regionName)
    if( region == nil ) then
        --LuaDebugCallStack("REGION IS NIL: "..regionName)
        return nil
    end

    local searcher = PermanentObjSearchMulti{
            PermanentObjSearchRegion(regionName),
            PermanentObjSearchHasObjectBounds(),
            PermanentObjSearchVisualState("Default")
        }

    --local objs1 = FindPermanentObjects(PermanentObjSearchRegion(regionName))
    --local objs2 = FindPermanentObjects(PermanentObjSearchHasObjectBounds())
    --local objs3 = FindPermanentObjects(PermanentObjSearchVisualState("Default"))
    --DebugMessage("TEST",tostring(#objs1),tostring(#objs2),tostring(#objs3))

    local objs = FindPermanentObjects(searcher)
    if(#objs == 0) then
        --DebugMessage("ERROR: No dungeon tiles found in region "..tostring(regionName))
        return nil
    end

    local curPermObj = objs[math.random(#objs)]
    local curBounds = curPermObj:GetPermanentObjectBounds()
    local spawnLoc = GetRandomLocationInObjectBounds(curBounds)
    local maxTries = 20
    while(maxTries > 0 and (spawnLoc ~= nil and not(spawnLoc:IsValid()) or not(IsPassable(spawnLoc)) or TrapAtLocation(spawnLoc))) do--(spawnLoc,false))) do
        local curPermObj = objs[math.random(#objs)]
        local curBounds = curPermObj:GetPermanentObjectBounds()
        spawnLoc = GetRandomLocationInObjectBounds(curBounds)
        maxTries = maxTries - 1        
    end

    if(maxTries <= 0) then
        --DebugMessage("ERROR: Tried 20 times for a passable location and failed. "..regionName)
        return nil
    end

    return spawnLoc,curPermObj
    --return Loc(0,0,0),PermanentObj(0)
end

function GetSubregionForDungeon(playerObj)
    if ( IsDungeonMap() ) then
		-- find the dungeon exit gameobj
		local exit = FindObject(SearchHasObjVar("DestinationSubregion"))
        if not( exit ) then
			-- cannot connect a dungeon without an exit
			LuaDebugCallStack("[WarnRegionOfMurder] Failed to find dungeon exit.")
			return cb(false)
		end
		-- this is the reason SeedObjects for SewerDungeons have this objvar.
        local subregion = exit:GetObjVar("DestinationSubregion")
		-- they already need this objvar for their lua module to work
		local destination = exit:GetObjVar("Destination")

		-- early exit if we don't have all the proper data
        if not( subregion ) then
			LuaDebugCallStack("[WarnRegionOfMurder] Dungeon exit does not have 'DestinationSubregion'")
			return cb(false)
		end
        if not( destination ) then
			LuaDebugCallStack("[WarnRegionOfMurder] Dungeon exit does not have 'Destination'")
			return cb(false)
		end

        local universeName = GetUniverseName()
        local clusterRegions = GetClusterRegions()
        
        -- determine region address from subregion name
        local address = nil
        for addr,info in pairs(clusterRegions) do
            -- if we have a universe name then we can only go to regions with the same universe name
            local skip = false
            if(universeName) then
                local curUniverse = GetUniverseName(addr)
                if(universeName ~= curUniverse) then
                    skip = true
                end
            end

            if ( skip and skip ~= true and info.SubregionName == subregion ) then
                address = addr
                return address
            end
        end
    end
end