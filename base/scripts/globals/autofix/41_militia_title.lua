local index = #AutoFixes + 1
local totalItems = 0

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end

        local template = object:GetCreationTemplateId()
        if(object:HasObjVar("Godly")) then
            Militia.RemoveMilitiaWeaponBlessing( object )

            local mobile = object:TopmostContainer()
            if(mobile and mobile:IsPlayer()) then
                Create.Stack.InBank("allegiance_talent", mobile, 50000)
            else
                AutoFixReplaceItem(object, "allegiance_talent", function (talentObj)
                        if(talentObj) then
                            RequestSetStack(talentObj,50000)
                        end
                    end, true)
            end
        elseif(template == "godly_weapon_scroll") then
            object:DelObjVar("MilitiaRankRequired")
            SetItemTooltip(object)
        elseif(template == "allegiance_talent") then
            object:DelObjVar("Blessed")
            SetItemTooltip(object)
        end

        -- recurse
        if ( object:IsContainer() ) then
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i])
                end
            end
        end
    end,
}

AutoFixes[index].Player = function(player)
    local completedAchievements = player:GetObjVar("CompletedAchievements")
    local newCompleted = {}
    for i,data in pairs(completedAchievements) do
        if(data.Subcategory ~= "Militia") then
            table.insert(newCompleted,data)       
        end
    end
    player:SetObjVar("CompletedAchievements",newCompleted)
    
    ForeachMobileAndPet(player, AutoFixes[index].DoFix)
end

AutoFixes[index].World = function(clusterController)
    local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
    local before = DateTime.UtcNow
    DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
    for i=1,#worldObjects do
        AutoFixes[index].DoFix(worldObjects[i])
    end
    DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
    DebugMessage("[AutoFix] "..totalItems.." Total items Updated.")
end