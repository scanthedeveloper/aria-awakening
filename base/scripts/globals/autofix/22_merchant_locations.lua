
--- Half all weapon damage and run the settooltip function on all items.
local index = #AutoFixes + 1

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end

        if( object:HasObjVar("HirelingOwner") and object:HasObjVar("SpawnLocation") and object:HasObjVar("ShopLocation") and object:HasObjVar("SpawnPosition") ) then
            object:SetObjVar("SpawnLocation", object:GetObjVar("ShopLocation") or object:GetObjVar("SpawnPosition") or object:GetLoc() )
        end
        
    end,
}

AutoFixes[index].World = function(clusterController)
    local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
    local before = DateTime.UtcNow
    DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
    for i=1,#worldObjects do
        AutoFixes[index].DoFix(worldObjects[i])
    end
    DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
end