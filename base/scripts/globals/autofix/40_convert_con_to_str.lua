
-- If players have completed the "Catacombs" questline, we force complete the new questline
local index = #AutoFixes + 1

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end
        local jewelryType = object:GetObjVar("JewelryType") or nil

        if( 
            jewelryType == "RubyFlawed" or 
            jewelryType == "RubyImperfect" or 
            jewelryType == "RubyPerfect" 
        ) then
            SetItemTooltip(object)
        end

        -- recurse
        if ( object:IsContainer() ) then
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i])
                end
            end
        end
        
    end,
}

AutoFixes[index].World = function(clusterController)
    local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
    local before = DateTime.UtcNow
    DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
    for i=1,#worldObjects do
        AutoFixes[index].DoFix(worldObjects[i])
    end
    DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
end

AutoFixes[index].Player = function(player)
        -- Set players STR to either their STR or CON (whichever higher)
        SetStr( player, math.max( GetBaseCon(player), GetBaseStr(player) ) )
        ForeachMobileAndPet(player, AutoFixes[index].DoFix)
end