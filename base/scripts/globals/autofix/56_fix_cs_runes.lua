
-- make fox mounts blessed
local index = #AutoFixes + 1
local totalItems = 0
local containers = 0

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end
        
        if(object:GetCreationTemplateId() == "rune_blank") then
            local regionAddress = object:GetObjVar("RegionAddress")            
            if(regionAddress) then
                local newRegionAddress = regionAddress:gsub("AzureSky","EtherealMoon")
                DebugMessage("BEFORE "..regionAddress.." AFTER "..newRegionAddress)
                if(regionAddress ~= newRegionAddress) then
                    object:SetObjVar("RegionAddress",newRegionAddress)
                end
                totalItems = totalItems + 1
            end
        end
        
        -- recurse
        if ( object:IsContainer() ) then
            containers = containers + 1
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i])
                end
            end
        end
    end,
}

AutoFixes[index].Player = function(player)
    --if ( ServerSettings.ClusterId == ServerSettings.Clusters["Ethereal Moon"].Id ) then
        ForeachMobileAndPet(player, AutoFixes[index].DoFix)
    --end
end

AutoFixes[index].World = function(clusterController)
    --if ( ServerSettings.ClusterId == ServerSettings.Clusters["Ethereal Moon"].Id ) then
        local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
        local before = DateTime.UtcNow
        DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
        for i=1,#worldObjects do
            AutoFixes[index].DoFix(worldObjects[i])
        end
        DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
        DebugMessage("[AutoFix] "..totalItems.." Total item tooltips Updated.")
        DebugMessage("[AutoFix] "..containers.." Total containers.")
    --end
end