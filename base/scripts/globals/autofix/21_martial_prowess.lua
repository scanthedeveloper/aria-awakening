--- Half all weapon damage and run the settooltip function on all items.
local index = #AutoFixes + 1

AutoFixes[index] = {
    Player = function(player)
        local skills = { GetSkillLevel(player,"LightArmorSkill"), GetSkillLevel(player,"HeavyArmorSkill"), GetSkillLevel(player,"MeleeSkill") }

        local highestSkill = 0
        for i,skill in pairs(skills) do
            if skill > highestSkill then
                highestSkill = skill
            end
        end

        if(highestSkill >= 30) then
            Create.InBackpack("shop_soul_stone",player,nil,function (item)
                item:SetObjVar("StoredSkill","MartialProwessSkill")
                item:SetObjVar("StoredSkillLevel",highestSkill)
                item:SetObjVar("Owner",player)
                item:SetObjVar("UserId",player:GetAttachedUserId())

                item:SetSharedObjectProperty("Variation","Activated")
                SetItemTooltip(item)

                player:SystemMessage("You have recieved a Soul Stone containing Martial Prowess skill.","info")
            end)
        end

        local skillDictionary = GetSkillDictionary(player)
        skillDictionary["LightArmorSkill"] = nil
        skillDictionary["HeavyArmorSkill"] = nil
        SetSkillDictionary(player, skillDictionary)
    end
}