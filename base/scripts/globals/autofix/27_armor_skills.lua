--- Half all weapon damage and run the settooltip function on all items.
local index = #AutoFixes + 1

AutoFixes[index] = {
    Player = function(player)
        local skillDictionary = GetSkillDictionary(player)
        skillDictionary["LightArmorSkill"] = nil
        skillDictionary["HeavyArmorSkill"] = nil
        SetSkillDictionary(player, skillDictionary)
    end
}