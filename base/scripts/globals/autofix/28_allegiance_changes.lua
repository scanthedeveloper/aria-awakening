local index = #AutoFixes + 1

AutoFixes[index] = {
    Player = function(player)
        player:SetSharedObjectProperty("Faction", "")
        player:SetSharedObjectProperty("FriendlyFactions", "")
        local objVars = {
            "Allegiance",
            "AllegiancePercentile",
            "AllegianceRankName",
            "AllegianceRankNumber",
            "AllegianceSeasonStart",
            "AllegianceStanding",
            "FavorAtLastUpdate",
            "FavorDR",
            "FavorFromEvents",
            "AllegianceResign",
            "AllegianceLastEventReward",
        }
        for i = 1, #objVars do
            if ( player:HasObjVar(objVars[i]) ) then
                player:DelObjVar(objVars[i])
            end
        end
    end
}