
AutoFixes = {}

-- This relies on the table size and ordering, never delete/reorder anything required below!!

require 'globals.autofix.1_karma_reset'
require 'globals.autofix.2_secure_house_containers'
require 'globals.autofix.3_books_containers_and_bless'
require 'globals.autofix.4_secure_toggle_friends'
require 'globals.autofix.5_allegiance_friendly_factions'
require 'globals.autofix.6_jewelry_durability'
require 'globals.autofix.7_tax_interval'
require 'globals.autofix.8_full_tooltip_update'
require 'globals.autofix.9_full_tooltip_update'
require 'globals.autofix.10_tooltips'
require 'globals.autofix.11_many_fixes'
require 'globals.autofix.12_statue_mount_to_pet'
require 'globals.autofix.13_tax_return'
require 'globals.autofix.14_pet_statues'
require 'globals.autofix.15_auction_start_time'
require 'globals.autofix.16_vile_leather'
require 'globals.autofix.17_mod_autofix'
require 'globals.autofix.18_allegiance_title'
require 'globals.autofix.19_karma_levels'
require 'globals.autofix.20_adjust_weapon_damage_call_settooltip'
require 'globals.autofix.21_martial_prowess'
require 'globals.autofix.22_merchant_locations'
require 'globals.autofix.23_essence'
require 'globals.autofix.24_summonmounthotbar'
require 'globals.autofix.25_dyerollback'
require 'globals.autofix.26_make_seeds_stackable'
require 'globals.autofix.27_armor_skills'
require 'globals.autofix.28_allegiance_changes'
require 'globals.autofix.29_official_servermerge'
require 'globals.autofix.30_wine_goblet_pork_platter'
require 'globals.autofix.31_reset_murder_count'
require 'globals.autofix.32_reset_murder_count_again'
require 'globals.autofix.33_reset_tooltip_gardens'
require 'globals.autofix.34_garden_seed_stacking'
require 'globals.autofix.35_remove_murderers_militia_township'
require 'globals.autofix.36_remove_torch_enchants'
require 'globals.autofix.37_remove_militia_godly_blessed'
require 'globals.autofix.38_make_crafted_linen_dyeable'
require 'globals.autofix.39_tier_four_profession_quest_autocomplete'
require 'globals.autofix.40_convert_con_to_str'
require 'globals.autofix.41_militia_title'
require 'globals.autofix.42_fix_over_cap_str'
require 'globals.autofix.43_fix_clothing_bless_artifacts'
require 'globals.autofix.44_fix_unholy_torch'
require 'globals.autofix.45_fix_tier_crafting_orders'
require 'globals.autofix.46_echo_fox_mounts'
require 'globals.autofix.47_fixed_fox_mount_tooltip'
require 'globals.autofix.48_strip_color_from_player_names'
require 'globals.autofix.49_add_resourcetype_shovel'
require 'globals.autofix.50_magery_skill'
require 'globals.autofix.51_remove_from_militia'
require 'globals.autofix.52_reduce_scroll_weight'
require 'globals.autofix.53_reset_tracked_skills'
require 'globals.autofix.54_removed_militia_rank_requirement_tooltip'
require 'globals.autofix.55_season_six_mount_bless'
require 'globals.autofix.56_fix_cs_runes'
require 'globals.autofix.57_2020_arena_reward'

function DoPlayerAutoFix(player)
    if ( player and player:IsValid() ) then
        local lastFix = player:GetObjVar("LastAutoFix") or 0
        while ( lastFix < #AutoFixes ) do
            lastFix = lastFix + 1
            if ( AutoFixes[lastFix] and AutoFixes[lastFix].Player ) then
                AutoFixes[lastFix].Player(player)
            end
            player:SetObjVar("LastAutoFix", lastFix)
        end
    end
end

function DoWorldAutoFix(clusterController)
    if ( clusterController and clusterController:IsValid() ) then
        local lastFix = clusterController:GetObjVar("LastAutoFix") or 0
        while ( lastFix < #AutoFixes ) do
            lastFix = lastFix + 1
            if ( AutoFixes[lastFix] and AutoFixes[lastFix].World ) then
                DebugMessage("Applying World Autofix #", lastFix)
                AutoFixes[lastFix].World(clusterController)
            end
            clusterController:SetObjVar("LastAutoFix", lastFix)
        end
    end
end

function AutoFixReplaceItem(item, template, cb, skipDestroy)
    local containedBy = item:ContainedBy()
    local loc = item:GetLoc()
    if ( containedBy ) then
        local locDown = false
        local controller = nil
        local topmost = containedBy:TopmostContainer() or containedBy
        if not(topmost:IsPlayer() and item:HasObjVar("LockedDown")) then 
            local topLoc = topmost:GetLoc()
            controller = Plot.GetAtLoc(topLoc)
            locDown = (controller and controller:IsValid()) 
        end

        if(item:IsEquipped()) then
            Create.Equipped(template, containedBy)
        else
            -- replace in container, easy peasy.
            Create.InContainer(template, containedBy, loc, function(itm) 
                if(locDown) then
                    itm:SetObjVar("LockedDown",true)
                    itm:SetObjVar("NoReset",true)
                    itm:SetObjVar("PlotController", controller)
                    SetTooltipEntry(itm,"locked_down","Locked Down",98)
                    
                    local house = Plot.GetHouseAt(controller, loc, false, true) -- checking roof bounds
                    if ( house ) then
                        itm:SetObjVar("PlotHouse", house)
                    end
                    
                    if ( itm:DecayScheduled() ) then
                        itm:RemoveDecay()
                    end
                end
                if(cb) then cb(itm) end 
            end)
        end
    else
        -- replace when locked down, little more difficult.
        local controller = Plot.GetAtLoc(loc)
        if ( controller and controller:IsValid() ) then
            Create.AtLoc(template, loc, function(itm)
                itm:SetObjVar("LockedDown",true)
                itm:SetObjVar("NoReset",true)
                itm:SetObjVar("PlotController", controller)
                SetTooltipEntry(itm,"locked_down","Locked Down",98)
                
                local house = Plot.GetHouseAt(controller, loc, false, true) -- checking roof bounds
                if ( house ) then
                    itm:SetObjVar("PlotHouse", house)
                end
                
                if ( itm:DecayScheduled() ) then
                    itm:RemoveDecay()
                end
                if ( cb ) then cb(itm) end
            end)
        else
            if ( cb ) then cb(nil) end
        end
    end

    if not(skipDestroy) then
        item:Destroy()
    end
end