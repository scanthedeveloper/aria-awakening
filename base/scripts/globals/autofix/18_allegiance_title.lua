--- Fixing some prestige books/container limits/blessed items
local index = #AutoFixes + 1

AutoFixes[index] = {
    Player = function(player)
        local completedAchievements = player:GetObjVar("CompletedAchievements")
        local newCompleted = {}
        for i,data in pairs(completedAchievements) do
            if(data.Subcategory ~= "Militia") then
                table.insert(newCompleted,data)       
            end
        end
        player:SetObjVar("CompletedAchievements",newCompleted)
    end
}

