
-- make fox mounts blessed
local index = #AutoFixes + 1
local totalItems = 0
local containers = 0

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end
        
        --DebugMessage("CHECKING "..object:GetName() .." in "..(object:ContainedBy() and object:ContainedBy():GetName() or ""))
        if(object:GetCreationTemplateId() == "item_statue_mount_pyros_warhorse") then
            local tooltipInfo = object:GetObjVar("TooltipInfo")
            if(tooltipInfo.ArenaReward and tooltipInfo.ArenaReward.TooltipString and tooltipInfo.ArenaReward.TooltipString:match("Fall 2020")) then
                local rewardInfo = object:GetObjVar("RewardInfo")
                local statueMount = object:GetObjVar("StatueMount")
                --DebugMessage("FOUND in "..object:ContainedBy():GetName())
                AutoFixReplaceItem(object, "item_statue_mount_tethys_warhorse", function (newObject)
                    newObject:SetObjVar("RewardInfo",rewardInfo)                    
                    newObject:SetObjVar("DestroyHorse",true)

                    if(statueMount) then
                        statueMount:SetObjVar("MountStatue",newObject)
                    end

                    CallFunctionDelayed(TimeSpan.FromMilliseconds(30), function()
                        SetItemTooltip(newObject)
                    end)
                    
                end)

                totalItems = totalItems + 1
            end
        end
        
        -- recurse
        if ( object:IsContainer() ) then
            containers = containers + 1
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i])
                end
            end
        end
    end,
}

AutoFixes[index].Player = function(player)
    --if ( ServerSettings.ClusterId == ServerSettings.Clusters["Ethereal Moon"].Id ) then
        ForeachMobileAndPet(player, AutoFixes[index].DoFix)
    --end
end

AutoFixes[index].World = function(clusterController)
    --if ( ServerSettings.ClusterId == ServerSettings.Clusters["Ethereal Moon"].Id ) then
        local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
        local before = DateTime.UtcNow
        DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
        for i=1,#worldObjects do
            AutoFixes[index].DoFix(worldObjects[i])
        end
        DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
        DebugMessage("[AutoFix] "..totalItems.." Total item tooltips Updated.")
        DebugMessage("[AutoFix] "..containers.." Total containers.")
    --end
end