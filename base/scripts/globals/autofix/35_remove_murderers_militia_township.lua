
-- rest murder counts and set every defaulted to peaceful

AutoFixes[#AutoFixes+1] = {
    Player = function(player)
        if(IsMurderer(player)  ) then
           
            TownshipsHelper.RemovePlayerFromTownship( player )
            Militia.RemovePlayer(player, true)

        end
    end
}