
--- Half all weapon damage and run the settooltip function on all items.
local index = #AutoFixes + 1

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end

        -- Half the damage bonus of all weapons
        if ( object:HasObjVar("CraftedBy") and object:HasObjVar("WeaponType") and object:HasObjVar("AttackBonus") ) then
            object:SetObjVar("AttackBonus", math.max(math.floor(object:GetObjVar("AttackBonus") / 2), 1))
        end

        -- Removing accuracy bonuses from weapons
        if ( object:HasObjVar("CraftedBy") and object:HasObjVar("WeaponType") and object:HasObjVar("AccuracyBonus") ) then
            object:DelObjVar("AccuracyBonus")
        end

        SetItemTooltip(object)

        -- convert ability books
        if(object:HasModule("prestige_ability_book")) then
            local prestigeAbility = object:GetObjVar("PrestigeAbility")
            if(prestigeAbility) then
                if(prestigeAbility == "Wound") then prestigeAbility = "Hamstring" end
                if(prestigeAbility == "StunShot") then prestigeAbility = "StunStrike" end
                if(prestigeAbility == "HuntersMark") then prestigeAbility = "AdrenalineRush" end

                local prestigeClass = GetPrestigeAbilityClass(prestigeAbility)
                local professionClass = GetRequiredProfession(prestigeClass,prestigeAbility)
                local professionTier = GetRequiredProfessionTier(prestigeClass,prestigeAbility)
                local newBookTemplate = "prestige_"..TaskIndexTitles[professionTier]:lower().."_"..professionClass:lower()
                AutoFixReplaceItem(object, newBookTemplate)
            end
        end

        -- convert founders cloaks
        if(object:GetCreationTemplateId() == "founders_cloak") then
            AutoFixReplaceItem(object, "founders_cloak")
        end

        -- recurse
        if ( object:IsContainer() ) then
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i],player)
                end
            end
        end
        
    end,
}

function ConvertAbility(player,abilityIndex)
    local objVarName = "PrestigeAbility"..abilityIndex
    local abilityTable = player:GetObjVar(objVarName)
    if not(abilityTable) then return end

    local oldAbility = abilityTable.AbilityName
    local prestigeAbility = abilityTable.AbilityName
    if(prestigeAbility == "Wound") then prestigeAbility = "Hamstring" end
    if(prestigeAbility == "StunShot") then prestigeAbility = "StunStrike" end
    if(prestigeAbility == "HuntersMark") then prestigeAbility = "AdrenalineRush" end

    local prestigeClass = GetPrestigeAbilityClass(prestigeAbility)
    local professionClass = GetRequiredProfession(prestigeClass,prestigeAbility)
    local professionTier = GetRequiredProfessionTier(prestigeClass,prestigeAbility)

    local points = GetAbilityPointsForAbility(prestigeClass,prestigeAbility)     
    player:SystemMessage("You have been refunded "..tostring(points).." ability points.")    

    local i = 2
    while(i <= professionTier) do
        local newBookTemplate = "prestige_"..TaskIndexTitles[i]:lower().."_"..professionClass:lower()
        player:SystemMessage("You have recieved a "..GetTemplateObjectName(newBookTemplate)..".")
        Create.InBackpack(newBookTemplate,player)
        i = i + 1
    end

    AddPrestigeXP(player,points*ServerSettings.Prestige.PrestigePointXP)

    player:DelObjVar(objVarName)
end

AutoFixes[index].Player = function(player)
    ConvertAbility(player,1)
    ConvertAbility(player,2)
    ConvertAbility(player,3)

    player:DelObjVar("TrackedSkills")

    ForeachMobileAndPet(player, AutoFixes[index].DoFix)
end

AutoFixes[index].World = function(clusterController)
    local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
    local before = DateTime.UtcNow
    DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
    for i=1,#worldObjects do
        AutoFixes[index].DoFix(worldObjects[i])
    end
    DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
end