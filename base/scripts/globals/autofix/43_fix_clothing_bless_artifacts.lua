local index = #AutoFixes + 1
local totalItems = 0

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end

        
        if(
            object:HasObjVar("Blessed") and
            not object:HasObjVar("Cursed") and 
            (object:HasObjVar("ArmorType") or object:HasObjVar("WeaponType")) and
            object:GetObjVar("WeaponType") ~= "Spellbook" and
            object:GetCreationTemplateId() ~= "torch_blue"
        ) then

            -- Remove the blessed state
            object:DelObjVar("Blessed")
            SetItemTooltip(object)

            -- Create the bless_deed for the player
            local mobile = object:TopmostContainer()
            if(mobile and mobile:IsPlayer()) then
                --DebugMessage("Creating bank bless for item: " .. object:GetName());
                --Create.InBank("bless_deed", mobile)
            else
                AutoFixReplaceItem(object, "bless_deed", nil, true)
            end
        end

        -- recurse
        if ( object:IsContainer() ) then
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i])
                end
            end
        end
    end,
}

AutoFixes[index].Player = function(player)
    ForeachMobileAndPet(player, AutoFixes[index].DoFix)
end

AutoFixes[index].World = function(clusterController)
    local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
    local before = DateTime.UtcNow
    DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
    for i=1,#worldObjects do
        AutoFixes[index].DoFix(worldObjects[i])
    end
    DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
    DebugMessage("[AutoFix] "..totalItems.." Total items Updated.")
end