
-- rest murder counts and set every defaulted to peaceful

AutoFixes[#AutoFixes+1] = {
    Player = function(player)
        -- set karma alignment to Peaceful
        player:DelObjVar("MurderCount")
        player:SendClientMessage("SetKarmaState", "Peaceful")
    end
}