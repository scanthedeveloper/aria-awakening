
local index = #AutoFixes + 1
local totalItems = 0

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end

        local template = object:GetCreationTemplateId()
        if ( template == "item_pork_platter_roasted" ) then
            totalItems = totalItems + 1
            object:SetObjVar("ResourceType", "RoastedPorkPlatter")
            SetItemTooltip(object)
        elseif ( template == "item_wine_goblet" ) then
            totalItems = totalItems + 1
            object:SetObjVar("ResourceType", "GobletOfWine")
            SetItemTooltip(object)
        end

        -- recurse
        if ( object:IsContainer() ) then
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i])
                end
            end
        end
        
    end,
}

AutoFixes[index].Player = function(player)
    ForeachMobileAndPet(player, AutoFixes[index].DoFix)
end

AutoFixes[index].World = function(clusterController)
    local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
    local before = DateTime.UtcNow
    DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
    for i=1,#worldObjects do
        AutoFixes[index].DoFix(worldObjects[i])
    end
    DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
    DebugMessage("[AutoFix] "..totalItems.." Total items Updated.")
end