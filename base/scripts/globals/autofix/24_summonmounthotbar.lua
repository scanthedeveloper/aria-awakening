
-- fixed a couple essence's ResourceType
local index = #AutoFixes + 1

AutoFixes[index] = {

}

AutoFixes[index].Player = function(player)
    local actions = player:GetObjVar("HotbarActions")
    
    if(actions) then
        local newActions = {}
        for i,action in pairs(actions) do            
            if(action.ID == "Etherealize") then
                player:SendMessage("RemoveUserActionFromSlot",i)
            end
        end
    end    
end
