
-- rest murder counts and set every defaulted to peaceful (again)(cause pets aren't people)
AutoFixes[#AutoFixes+1] = {
    Player = function(player)
        player:DelObjVar("MurderCount")
        -- set karma alignment to Peaceful
        ForcePeaceful(player)

        -- update the name
        player:SendMessage("UpdateName")
        -- update all pets
        ForeachActivePet(player, function(pet)
            pet:DelObjVar("MurderCount")
            pet:SendMessage("UpdateName")
        end, true)
    end
}