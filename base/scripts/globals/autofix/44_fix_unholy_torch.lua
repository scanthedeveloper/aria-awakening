local index = #AutoFixes + 1
local totalItems = 0

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end
      
        if(
            object:GetCreationTemplateId() == "torch_blue" and 
            not(object:HasObjVar("Blessed"))
        ) then

            -- Remove the blessed state
            object:SetObjVar("Blessed",true)
            RemoveTooltipEntry(object,"Durability")
            SetItemTooltip(object)

            -- Delete the bless_deed for the player
            local mobile = object:TopmostContainer()
            if(mobile and mobile:IsPlayer()) then
                local blessDeed = FindItemInContainerByTemplateRecursive(mobile,"bless_deed")
                if(blessDeed) then
                    local container = blessDeed:ContainedBy()                    
                    DebugMessage("[Autofix:44] Deleting bless deed on " .. mobile:GetName() .. ":"..mobile.Id.." UserId: "..mobile:GetAttachedUserId().." in container: " .. (container and container:GetName() or ""))
                    blessDeed:Destroy()
                else
                    DebugMessage("[Autofix:44] Bless deed not found on "..mobile:GetName() .. ":"..mobile.Id.." UserId: "..mobile:GetAttachedUserId())
                end
            end
        end

        -- recurse
        if ( object:IsContainer() ) then
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i])
                end
            end
        end
    end,
}

AutoFixes[index].Player = function(player)
    ForeachMobileAndPet(player, AutoFixes[index].DoFix)
end

AutoFixes[index].World = function(clusterController)
    local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
    local before = DateTime.UtcNow
    DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
    for i=1,#worldObjects do
        AutoFixes[index].DoFix(worldObjects[i])
    end
    DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
    DebugMessage("[AutoFix] "..totalItems.." Total items Updated.")
end