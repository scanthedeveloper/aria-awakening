
-- If players have completed the "Catacombs" questline, we force complete the new questline

AutoFixes[#AutoFixes+1] = {
    Player = function(player)
        if( Quests.IsComplete( player, "BlacksmithProfessionTierFour", 5 ) ) then
            Quests.ForceComplete(player, "BlacksmithProfessionTierFour")
        end

        if( Quests.IsComplete( player, "TailorProfessionTierFour", 5 ) ) then
            Quests.ForceComplete(player, "TailorProfessionTierFour")
        end

        if( Quests.IsComplete( player, "CarpenterProfessionTierFour", 5 ) ) then
            Quests.ForceComplete(player, "CarpenterProfessionTierFour")
        end
    end
}