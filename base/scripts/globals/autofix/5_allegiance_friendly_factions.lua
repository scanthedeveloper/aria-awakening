

AutoFixes[#AutoFixes+1] = {
    Player = function(player)
        local militia = Militia.GetId(player)
        if ( militia ) then
            local militiaData = Militia.GetDataById(militia)
            if ( militiaData ) then
                -- this fixes default interaction of attack on self/pets for current militia members.
                -- This is set for anyone new that joins a militia.
                player:SetSharedObjectProperty("FriendlyFactions", militiaData.Icon)
            end
        end
    end
}