
--- Half all weapon damage and run the settooltip function on all items.
local index = #AutoFixes + 1
local seeds = {
    OnionSeed = "seed_onion",
    CarrotSeed = "seed_carrot",
    StrawberrySeed = "seed_strawberry",
    CornSeed = "seed_corn",
    SquashSeed = "seed_squash",
    GreenLeafLettuceSeed = "seed_greenleaflettuce",
    RedLeafLettuceSeed = "seed_redleaflettuce",
    ButtonMushroomSeed = "seed_buttonmushroom",
    MelonSeed = "seed_melon",
    GreenPepperSeed = "seed_greenpepper",
    CucumberSeed = "seed_cucumber",
    EggplantSeed = "seed_eggplant",
}

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end
        local resourceType = object:GetObjVar("ResourceType")

        if( seeds[resourceType] ~= nil and not object:HasObjVar("StackCount") ) then
            AutoFixReplaceItem(object, seeds[resourceType])
        end

        -- recurse
        if ( object:IsContainer() ) then
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i])
                end
            end
        end
        
    end,
}

AutoFixes[index].World = function(clusterController)
    local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
    local before = DateTime.UtcNow
    DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
    for i=1,#worldObjects do
        AutoFixes[index].DoFix(worldObjects[i])
    end
    DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
end

AutoFixes[index].Player = function(player)
    ForeachMobileAndPet(player, AutoFixes[index].DoFix)
end