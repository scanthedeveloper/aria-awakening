
--- Fabled Beast Leather was a typo.
local index = #AutoFixes + 1

dyeHues = {
    828,
    107,
    529,
    241,
    886,
    891,
    850,
}

AutoFixes[index] = {
    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end

        if( object:GetCreationTemplateId() == "dye_tub" ) then
            object:Destroy()
        end

        if( object:GetObjVar("ResourceType") == "DyeTubDyes" ) then
            object:Destroy()
        end

        if( object:GetObjVar("ArmorType") ) then
            local hue = object:GetHue()
            if( IsInTableArray(dyeHues,hue) ) then
                local material = object:GetObjVar("Material")
                local oldHue
                if(material) then
                    oldHue = Materials[material]
                else    
                    local templateData = GetTemplateData(object:GetCreationTemplateId())
                    oldHue = templateData.Hue
                end
                object:SetHue(oldHue)
            end
        end

        -- recurse
        if ( object:IsContainer() ) then
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i])
                end
            end
        end
        
    end,
}

AutoFixes[index].Player = function(player)
    ForeachMobileAndPet(player, AutoFixes[index].DoFix)
end

AutoFixes[index].World = function(clusterController)
    local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
    local before = DateTime.UtcNow
    DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
    for i=1,#worldObjects do
        AutoFixes[index].DoFix(worldObjects[i])
    end
    DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
end