--- Half all weapon damage and run the settooltip function on all items.
local index = #AutoFixes + 1
local totalItems = 0
local containers = 0

AutoFixes[index] = {

    DoFix = function(object)
        if ( not object or not object:IsValid() ) then return end

        local storedSkill = object:GetObjVar("StoredSkill")
        if( storedSkill and (storedSkill == "EvocationSkill" or storedSkill == "ManifestationSkill" )) then
            object:SetObjVar("StoredSkill", "MagerySkill")
            SetItemTooltip(object)
        end

        -- recurse
        if ( object:IsContainer() ) then
            containers = containers + 1
            local items = object:GetContainedObjects() or {}
            for i=1,#items do
                if ( items[i] and items[i]:IsValid() ) then
                    AutoFixes[index].DoFix(items[i])
                end
            end
        end
    end,

    Player = function(player)
        ForeachMobileAndPet(player, AutoFixes[index].DoFix)

        local skillDictionary = GetSkillDictionary(player)
        maniSkill = 0
        evoSkill = 0

        if( skillDictionary["ManifestationSkill"] ) then
            maniSkill = skillDictionary["ManifestationSkill"].SkillLevel or 0
        end

        if( skillDictionary["EvocationSkill"] ) then
            evoSkill = skillDictionary["EvocationSkill"].SkillLevel or 0
        end

        if( evoSkill > 30 and maniSkill > 30 ) then
            local soulStoneSkill = math.min( maniSkill, evoSkill )
            Create.InBackpack("shop_soul_stone",player,nil,function (item)
                item:SetObjVar("StoredSkill","MagerySkill")
                item:SetObjVar("StoredSkillLevel",soulStoneSkill)
                item:SetObjVar("Owner",player)
                item:SetObjVar("UserId",player:GetAttachedUserId())

                item:SetSharedObjectProperty("Variation","Activated")
                SetItemTooltip(item)

                player:SystemMessage("You have recieved a Soul Stone containing Magery skill.","info")
            end)
        end
       
        local skillLevel = math.max( maniSkill, evoSkill )
        skillDictionary["MagerySkill"] = { SkillLevel = skillLevel }
        skillDictionary["ManifestationSkill"] = nil
        skillDictionary["EvocationSkill"] = nil
        SetSkillDictionary(player, skillDictionary)
    end,

    World = function(clusterController)
        local worldObjects = FindObjects(SearchObjectsInWorld(),clusterController)
        local before = DateTime.UtcNow
        DebugMessage("[AutoFix] "..#worldObjects.." World Objects found.")
        for i=1,#worldObjects do
            AutoFixes[index].DoFix(worldObjects[i])
        end
        DebugMessage("[AutoFix] World Objects Done. TotalMS: "..DateTime.UtcNow:Subtract(before).TotalMilliseconds)
        DebugMessage("[AutoFix] "..totalItems.." Total item tooltips Updated.")
        DebugMessage("[AutoFix] "..containers.." Total containers.")
    end,

}