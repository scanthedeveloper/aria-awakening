
-- If players have completed the "Catacombs" questline, we force complete the new questline
local index = #AutoFixes + 1

AutoFixes[index] = {    
}

AutoFixes[index].Player = function(player)
        -- Set players STR to either their STR or CON (whichever higher)
        SetStr( player, math.min( 50, GetBaseStr(player) ) )
end