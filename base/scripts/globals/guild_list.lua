AllGuilds = {}

lastGuildListRefresh = nil
guildListRefreshFrequency = TimeSpan.FromMinutes(5)

function RefreshGuildList()	
	if(not(lastGuildListRefresh) or DateTime.Compare(DateTime.UtcNow,lastGuildListRefresh:Add(guildListRefreshFrequency)) > 0) then
		AllGuilds = {}

		lastGuildListRefresh = DateTime.UtcNow

		local results = GlobalVarListRecords("Guild")
		for i,recordName in pairs(results) do
			if(recordName ~= "Guild.Tag") then		
				local data = GlobalVarReadKey(recordName,"Data")
				if(data) then
					table.insert(AllGuilds,{Name=data.Name,Id=data.Id})
				end
			end
		end
	end
end
RefreshGuildList()