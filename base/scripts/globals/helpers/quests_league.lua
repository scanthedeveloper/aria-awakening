--[[

    This file contains helpers that support the functionality of the "League" NPCs. These
    are seasonal quest givers that allow players to earn custom currency toward unique rewards.

]]

QuestsLeague = {}

QuestsLeague.FavorModifer = 1;
QuestsLeague.NormalFavor = 200;
QuestsLeague.GreaterFavor = 500;
QuestsLeague.MaximumBankedFavor = 48000;

QuestsLeague.LeagueNameToCurrency = 
{
    Dungeoneers     = "curr_league_dungeoneers",
    Artisans        = "curr_league_artisans",
    Procurers       = "curr_league_procurers",
    Explorers       = "curr_league_explorers",
}

QuestsLeague.TownshipNameToCurrency = 
{
    Helm            = "curr_township_board",
    Eldeir          = "curr_township_board",
    Pyros           = "curr_township_board",
    --Valus           = "curr_township_valus",
}

QuestsLeague.LeagueCurrencyName = {
    Eldeir          = "Crowns",
    Pyros           = "Crowns",
    Helm            = "Crowns",
    Dungeoneers     = "Favor",
    Artisans        = "Favor",
    Procurers       = "Favor",
    Explorers       = "Favor",
}

QuestsLeague.LeagueIcon = {    
    Dungeoneers     = "League_Dungeoneer",
    Artisans        = "League_Artisan",
    Procurers       = "League_Procurement",
    Explorers       = "League_Explorer",
}

QuestsLeague.LeagueNameDescription = 
{
    Dungeoneers     = "Used to purchase items from the League of Dungeoneers.",
    Artisans        = "Used to purchase items from the League of Artisans.",
    Procurers       = "Used to purchase items from the League of Procurers.",
    Explorers       = "Used to purchase items from the League of Explorers.",
    Helm            = "Used to perchase items from the trade boards.",
    Eldeir          = "Used to perchase items from the trade boards.",
    Pyros           = "Used to perchase items from the trade boards.",
    --Valus           = "Used to perchase items from the trade board in the Township of Valus.",
}

--[[ Dynamic Spawner Quests ]]
QuestsLeague.AddForceStartDynamicSpawn = function( playerObj, dynamicSpawnerKey )
    playerObj:SetObjVar("ForceStartSpawner_"..dynamicSpawnerKey, true)
end

QuestsLeague.RemoveForceStartDynamicSpawn = function( playerObj, dynamicSpawnerKey )
    playerObj:DelObjVar("ForceStartSpawner_"..dynamicSpawnerKey)
end

--[[ Collection Quests ]]

--[[ Crafting Quests ]]

--[[ Currency Helpers ]]

QuestsLeague.SetLeagueCurrecy = function( playerObj, leagueName, amount )
    local currencies = playerObj:GetObjVar("Currencies") or {}
    if( QuestsLeague.LeagueNameToCurrency[leagueName] ) then
        currencies[ QuestsLeague.LeagueNameToCurrency[leagueName] ] = amount
    elseif( QuestsLeague.TownshipNameToCurrency[leagueName] ) then
        currencies[ QuestsLeague.TownshipNameToCurrency[leagueName] ] = amount
    end
    playerObj:SetObjVar("Currencies", currencies)
end

QuestsLeague.GetLeagueCurrecy = function( playerObj, leagueName )
    if not( playerObj or leagueName )then return 0 end
    local currencies = playerObj:GetObjVar("Currencies") or {}
    local currency = currencies[ QuestsLeague.LeagueNameToCurrency[leagueName] ] or currencies[ QuestsLeague.TownshipNameToCurrency[leagueName] ] or 0
    return currency
end

QuestsLeague.AdjustLeagueCurrency = function( playerObj, leagueName, amount )
    if not( playerObj or leagueName )then return false end
    
    -- Get our currencies list off the player
    local currencies = playerObj:GetObjVar("Currencies") or {}
    local currency = currencies[ QuestsLeague.LeagueNameToCurrency[leagueName] ] or currencies[ QuestsLeague.TownshipNameToCurrency[leagueName] ] or 0
    
    -- If our currency is over the limit we cannot add anymore
    if( currency >= QuestsLeague.MaximumBankedFavor and amount > 0 ) then
        
        playerObj:SystemMessage(QuestsLeague.LeagueCurrencyName[leagueName].." favor cannot increase above [F2F5A9]"..tostring(QuestsLeague.MaximumBankedFavor).."[-].")
        return false

    else

        currency = currency + amount

        -- If they have less than the MaximumBankedFavor but adding the amount would set them over, we just set it to the max
        if( currency >= QuestsLeague.MaximumBankedFavor and amount > 0 ) then
            playerObj:SystemMessage(QuestsLeague.LeagueCurrencyName[leagueName].." favor cannot increase above [F2F5A9]"..tostring(QuestsLeague.MaximumBankedFavor).."[-].")
            currency = QuestsLeague.MaximumBankedFavor
        end

        -- If our currency is negative we cannot continue!
        if( currency < 0 ) then return false end
        
        -- Set our player ObjVar
        currencies[ QuestsLeague.LeagueNameToCurrency[leagueName] or QuestsLeague.TownshipNameToCurrency[leagueName] ] = currency
        playerObj:SetObjVar("Currencies", currencies)
        
        -- Determine how to represent this change to the player
        local type = "increased"
        if(amount < 0) then
            type = "decreased"
        end

        local reputationType = "League"
        if(  QuestsLeague.TownshipNameToCurrency[leagueName] ~= nil ) then
            reputationType = "Township"
        end

        -- Alert player and return true
        playerObj:SystemMessage(QuestsLeague.LeagueCurrencyName[leagueName].." "..type.." by [F2F5A9]"..tostring(amount).."[-] with the [F2F5A9]"..reputationType.." of " .. leagueName.."[-].")
        return true

    end
    
end

QuestsLeague.OnCompleteQuest = function(playerObj,leagueName,rewardAmount)
    CheckAchievementStatus(playerObj, "Leagues", leagueName, 1)
    playerObj:SendMessage("OnDailyRewardTaskComplete","quest")
    QuestsLeague.AdjustLeagueCurrency( playerObj, leagueName, (tonumber(rewardAmount) * QuestsLeague.FavorModifer) )
end