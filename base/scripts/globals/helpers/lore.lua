Lore = {}

Lore.TakeTomebookFromBookshelf = function(recipientObj, tomebookData, cb)
	Create.InBackpack("lore_tomebook", recipientObj, nil, function(obj)
        if obj then
            
            if tomebookData.TomeId ~= "EmptyTomebook" then
	            obj:SetObjVar("TomeId", tomebookData.TomeId)
	            obj:SetObjVar("CollectionIndex", tomebookData.CollectionIndex)
	            obj:SetObjVar("TomeIndex", tomebookData.TomeIndex)
	            obj:SetObjVar("Entries", tomebookData.Entries)
	            obj:SetObjVar("TranscribedBy", tomebookData.TranscribedBy)
	            
	            obj:SetHue(tomebookData.Hue or 0)
	            obj:SetSharedObjectProperty("Text", tomebookData.TomeIndex)
	            obj:SendMessage("UseObject", nil, "UpdateName")
        	end

            if cb then cb() end
        end
    end, true, true)
end

Lore.IsShelfEmpty = function(bookshelfObj)
	local shelfData = bookshelfObj:GetObjVar("ShelfData")
	if not shelfData then return true end

	local shelfCount = bookshelfObj:GetObjVar("ShelfCount") or 0
	local booksPerShelf = bookshelfObj:GetObjVar("BooksPerShelf") or 0

	for i=1, shelfCount do
		for j=1, booksPerShelf do
			local index = (i-1) * booksPerShelf + j
			if shelfData[index].TomeId then
				--DebugMessage("book",index,"is not empty")
				return false
			end
		end
	end
	--DebugMessage(bookshelfObj:GetName(),"IS empty")
	return true
end

Lore.ReadCatalog = function(user, catalogOrBookstandObj)
	if not user:HasModule("lore_catalog_window") then
		user:AddModule("lore_catalog_window")
	end
	user:SendMessage("OpenCatalog", catalogOrBookstandObj)

	if not catalogOrBookstandObj:HasObjVar("Owner") then

		ClientDialog.Show
		{
		    TargetUser = user,
		    TitleStr = "Unclaimed Catalog",
		    DescStr = "This catalog has no signed Owner. Would you like to claim it as yours?",
		    Button1Str = "Confirm",
		    Button2Str = "Cancel",
		    ResponseObj = catalogOrBookstandObj,
		    ResponseFunc = function (user,buttonId)
		    	if (buttonId == 0) then
		    		if catalogOrBookstandObj:HasObjVar("Owner") then
		    			user:SystemMessage("This Catalog has been claimed by someone else.", "info")
		    			return
	    			end
		    		catalogOrBookstandObj:SendMessage("ChangeUser", user)
		    		return
		    	end
			end,
		}
	end
end

Lore.IsCatalogOwner = function(catalogObj, playerToCheck)
	return catalogObj:GetObjVar("Owner") == playerToCheck
end

Lore.GetCatalogInBackpack = function(playerObj, cb)
	local catalogs = GetItemsInBackpack(playerObj, "lore_catalog")
	local needsSelection = false

	if catalogs then
		local sortedCatalogs = {PlayerOwned = {}, OtherOwned = {}, NotOwned = {}}
		for i=1, #catalogs do
			local owner = catalogs[i]:GetObjVar("Owner")
			if owner then
				if owner == playerObj then
					sortedCatalogs.PlayerOwned[#sortedCatalogs.PlayerOwned+1] = catalogs[i]
				else
					sortedCatalogs.OtherOwned[#sortedCatalogs.OtherOwned+1] = catalogs[i]
				end
			else
				sortedCatalogs.NotOwned[#sortedCatalogs.NotOwned+1] = catalogs[i]
			end
		end
		if #sortedCatalogs.PlayerOwned == 1 then
			cb(sortedCatalogs.PlayerOwned[1])
		elseif #sortedCatalogs.PlayerOwned > 1 then
			playerObj:SystemMessage("Multiple player-owned Catalogs found in Backpack. Please Disown the extra catalogs.","info")
			--needsSelection = true
		elseif #sortedCatalogs.NotOwned >= 1 then
			playerObj:SystemMessage("No signed Catalogs found in your Backpack, please claim one.","info")
			--needsSelection = true
		else
			playerObj:SystemMessage("New knowledge could not be recorded; no player-owned or un-owned Catalog found in your Backpack.","info")
		end
	else
		playerObj:SystemMessage("New knowledge could not be recorded; you don't have any Catalog in your Backpack.","info")
	end

	if needsSelection then
		--DebugMessage("REQ CATALOG")

		-- This logic gets called from a DynamicWindow Response EventHandler - for some reason this prevents me from registering new handlers, even one frame or 5 seconds later

		--[[RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse,"SpecifyCatalog", function(target, user)
            DebugMessage("Pressed?", target, user)
            -- NEVER GETS CALLED
            if not target or not (target:GetCreationTemplateId() == "lore_catalog") then return end
            --if not target:HasObjVar("Owner") or target:GetObjVar("Owner") == user then DebugMessage("PICKED") cb(target) end
        end)]]
		--playerObj:RequestClientTargetGameObj(playerObj, "SpecifyCatalog")

	end
end

Lore.AccreditCatalog = function(playerObj, catalogObj)
	if Lore.IsCatalogComplete(catalogObj) then
		playerObj:SetObjVar("LoreAccreditedCatalog", catalogObj)
		catalogObj:SetObjVar("Accreditee", playerObj)
		SetTooltipEntry(catalogObj,"accredit",
			"\nAccredited to "..playerObj:GetName(),
			1)
		return true
	else
		return false
	end
end

-- Valus Elder Mages check that a Catalog has recorded all currently available tomebook entries before Accreditting a player
Lore.IsCatalogComplete = function(catalogObj)
	local result = true
	local someValidTomes = false
	local catalogTomes = catalogObj:GetObjVar("Tomes") or {}

	for i=1, #AllTomeCollections do
		local collection = AllTomeCollections[i]
		local collectionTomeIds = collection[2] or {}
		
		--DebugMessage(collection[1],collection.Locked)
		if not collection.Locked then
			for j=1, #collectionTomeIds do
				local id = collectionTomeIds[j]
				local tomeData = AllTomes[id]
				local catalogTomeData = catalogTomes[id]

				if tomeData and catalogTomeData then
					someValidTomes = true
					for k=1, #tomeData.Entries do
						if not catalogTomeData or catalogTomeData[k] == 0 then
							result = false
							--DebugMessage(id,"entry",k," is empty or 0, FAILED")
						end
					end
				end
			end
		end
	end

	return someValidTomes and AllTomeCollections and #AllTomeCollections > 0 and result
end

-- do every load of catalogs and bookstands from backup
Lore.GetCatalogProgress = function(catalogOrBookstandObj)
	if catalogOrBookstandObj:HasModule("lore_bookstand") and not catalogOrBookstandObj:HasObjVar("Tomes") then return end
	local tomes = catalogOrBookstandObj:GetObjVar("Tomes") or {}
	local entriesFound = 0
	local tomesCompleted = 0
	local collectionsCompleted = 0
	local entryCount = 0
	local tomeCount = 0
	local collectionCount = 0
	local needTomeObjVarUpdate = false
	for i=1, #AllTomeCollections do

		local collection = AllTomeCollections[i]
		local collectionTomeIds = collection[2] or {}

		if not collection.Locked then
			local isCollectionCompleted = true
			for j=1, #collectionTomeIds do
				local id = collectionTomeIds[j]
				local tomeData = AllTomes[id] or {}
				local staticEntryCount = #tomeData.Entries

				local tome = tomes[id]

				-- ID not currently recorded in Catalog (Tome was added to game after Catalog's creation)
				local idNotRecordedInCatalog = not tome
				if idNotRecordedInCatalog then
					tome = {}
					--DebugMessage(id,"is not currently catalogued. Adding!")
					needTomeObjVarUpdate = true
				end

				local isTomeComplete = true
				for k=1, staticEntryCount do
					if not tome[k] then
						tome[k] = 0
						needTomeObjVarUpdate = true
					end
					if tome[k] == 0 then
						isTomeComplete = false
						isCollectionCompleted = false
					else
						entriesFound = entriesFound + 1
					end
				end
				if isTomeComplete then tomesCompleted = tomesCompleted + 1 end
				entryCount = entryCount + staticEntryCount

				if idNotRecordedInCatalog then
					tomes[id] = tome
				end
			end
			if isCollectionCompleted then collectionsCompleted = collectionsCompleted + 1 end
			tomeCount = tomeCount + #collectionTomeIds
			collectionCount = collectionCount + (collection.Locked and 0 or 1)
		end
	end

	if needTomeObjVarUpdate then
		--DebugMessage("Saved 'Tomes' changes.")
		catalogOrBookstandObj:SetObjVar("Tomes", tomes)
	end

	local isComplete = entriesFound == entryCount and tomesCompleted == tomeCount and collectionsCompleted == collectionCount
	catalogOrBookstandObj:SetObjVar("IsComplete", isComplete)

	if not isComplete then
		local accreditee = catalogOrBookstandObj:GetObjVar("Accreditee")

		if accreditee and accreditee:IsValid() and catalogOrBookstandObj:IsValid() then
			local currentTitle = GetActiveTitle(accreditee)

			--DebugMessage("Current Title",currentTitle)
			--catalogOrBookstandObj:SetObjVar("AccreditationExpired", true)
			if currentTitle == "Celadorian Historian" then
				accreditee:SystemMessage("New Lore is available! You no longer meet the requirement for the title "..currentTitle, "info")
				accreditee:SetSharedObjectProperty("Title","")
				RemoveTooltipEntry(accreditee,"Title")
				accreditee:SendMessage("ReopenAchievementWindow")
			end

			CheckAchievementStatus(accreditee, "Activity", "CeladorianHistorian", 0)
			RemoveOtherAchievement(accreditee, "CeladorianHistorian")
		end
	end

	local entriesStr = entriesFound == entryCount and "[F2F5A9]"..tostring(entriesFound).."[-]" or tostring(entriesFound)
	local tomesStr = tomesCompleted == tomeCount and "[F2F5A9]"..tostring(tomesCompleted).."[-]" or tostring(tomesCompleted)
	local collectionsStr = collectionsCompleted == collectionCount and "[F2F5A9]"..tostring(collectionsCompleted).."[-]" or tostring(collectionsCompleted)

	SetTooltipEntry(catalogOrBookstandObj,"progress",
		entriesStr.." entries found.\n"..
		tomesStr.." tomebooks filled.\n"..
		collectionsStr.." collections completed.",
		50)
end

Lore.EarnLoreIfNew = function(playerObj, tomeId, entryIndices, silent)
	if Lore.KnowsLore(playerObj, tomeId, entryIndices) then
		if not silent then playerObj:SystemMessage("You already know this information.","info") end
		return
	end
	Lore.GetCatalogInBackpack(playerObj, function(catalog)
        catalog:SendMessage("AddEntries", playerObj, tomeId, entryIndices)
    end)
end

Lore.KnowsLore = function(playerObj, tomeId, entryIndices)
	local loreDiscovery = playerObj:GetObjVar("LoreDiscovery") or Lore.InitializeLoreDiscovery(playerObj)
	local playerTomeData = loreDiscovery[tomeId]

	if playerTomeData then
		for i=1, #entryIndices do
			local entryIndex = entryIndices[i]
			if (not playerTomeData[entryIndex]) or playerTomeData[entryIndex] == 0 then
				return false
			end
		end
	else
		return false
	end

	return true
end

Lore.RecordPlayerLoreRecording = function(playerObj, catalogObj, tomeId, entryIndices)
	local loreDiscovery = playerObj:GetObjVar("LoreDiscovery") or Lore.InitializeLoreDiscovery(playerObj)
	local playerTomeData = loreDiscovery[tomeId]

	if playerTomeData and next(playerTomeData) then
		for i=1, #entryIndices do
			if playerTomeData[entryIndices[i]] > 0 then
				--user:SystemMessage("You have already recorded '"..AllTomes[tomeId].Name.."' Entry #"..tostring(entryIndices[i]).." into a Catalog.","info")
				user:SystemMessage("You have already recorded this '"..AllTomes[tomeId].Name.."' Entry into a Catalog.","info")
				--DebugMessage("ALREADY RECORDED",AllTomes[tomeId].Name,"Entry #",entryIndices[i])
				return
			end
		end
	else
		local tomeData = AllTomes[tomeId]
		playerTomeData = {Catalogs = {}, DatesEarned = {}}
		if tomeData then
			--DebugMessage("NEW TOMEDATA",AllTomes[tomeId].Name,",",#tomeData.Entries," Blank Entries")
			for i=1, #tomeData.Entries do
				playerTomeData[i] = 0
			end
		else
			DebugMessage("[Lore.RecordPlayerLoreRecording FAILED] tomeId '",tomeId,"' invalid.")
			return
		end
	end

	loreDiscovery = Lore.CleanOldVariables(loreDiscovery, tomeId)

	playerTomeData.Catalogs = playerTomeData.Catalogs or {}
	playerTomeData.DatesEarned = playerTomeData.DatesEarned or {}

	for i=1, #entryIndices do
		--DebugMessage("RECORDED:",AllTomes[tomeId].Name,", Entry",entryIndices[i])
		playerTomeData[entryIndices[i]] = 1
		playerTomeData.Catalogs[entryIndices[i]] = catalogObj
		playerTomeData.DatesEarned[entryIndices[i]] = DateTime.UtcNow
	end

	loreDiscovery[tomeId] = playerTomeData

	playerObj:SetObjVar("LoreDiscovery", loreDiscovery)
	--playerObj:SystemMessage("You have received Entry",entryIndices[i],"of the",AllTomes[tomeId].Name,"Tome")
	local maybePlural = (#entryIndices ~= 1) and "Entries were" or "Entry was"

	playerObj:SystemMessage("[F2F5A9]"..tostring(#entryIndices).."[-] '"..AllTomes[tomeId].Name.."' "..maybePlural.." recorded.","info")
	if playerObj:HasModule("lore_catalog_window") then
		playerObj:SendMessage("OpenCatalog", catalogObj)
	end
end

Lore.RecordPlayerLoreTranscribing = function(playerObj, tomebookObj, tomeId, entryIndices)
	local loreDiscovery = playerObj:GetObjVar("LoreDiscovery") or Lore.InitializeLoreDiscovery(playerObj)
	local playerTomeData = loreDiscovery[tomeId] or {Catalogs = {}, DatesEarned = {}}

	local tomeData = AllTomes[tomeId]
	if not tomeData then
		DebugMessage("[Lore.RecordPlayerLoreTranscribing FAILED] tomeId '",tomeId,"' invalid.")
		return
	end

	loreDiscovery = Lore.CleanOldVariables(loreDiscovery, tomeId)

	playerTomeData.Tomebook = tomebookObj
	playerTomeData.DateTranscribed = DateTime.UtcNow

	for i=1, #entryIndices do
		playerTomeData[entryIndices[i]] = 2	-- 0 = not found, 1 = recorded, 2 = transcribed into tomebook
		--DebugMessage("RECORDED:",AllTomes[tomeId].Name,", Entry",entryIndices[i])
	end

	loreDiscovery[tomeId] = playerTomeData

	playerObj:SetObjVar("LoreDiscovery", loreDiscovery)
	--playerObj:SystemMessage("You have received Entry",entryIndices[i],"of the",AllTomes[tomeId].Name,"Tome")
	local maybePlural = (#entryIndices ~= 1) and "Entries were" or "Entry was"

	playerObj:SystemMessage("[F2F5A9]"..tomeData.Name.."[-] was transcribed.","info")
end

Lore.InitializeLoreDiscovery = function(playerObj)
	local loreDiscovery = {}

	for i=1, #AllTomeCollections do
		local collection = AllTomeCollections[i]
		local collectionName = collection[1] or ""
		local collectionTomeIds = collection[2] or {}
		
		for j=1, #collectionTomeIds do
			local id = collectionTomeIds[j]
			local tome = {Catalogs = {}, DatesEarned = {}}
			local tomeData = AllTomes[id] or {}
			local staticEntryCount = #tomeData.Entries

			for k=1, staticEntryCount do
				tome[k] = 0
			end

			loreDiscovery[id] = tome
		end
	end

	playerObj:SetObjVar("LoreDiscovery", loreDiscovery)
	return loreDiscovery
end

-- older LIVE version of lore system created these variables. This will check for them and remove them if needed
Lore.CleanOldVariables = function(loreDiscovery, tomeId)
	local playerTomeData = loreDiscovery[tomeId] or {}

	if playerTomeData.Catalog or playerTomeData.DateEarned or loreDiscovery.Catalog or loreDiscovery.Timestamps or loreDiscovery.Collections then
		playerTomeData.Catalog = nil
		playerTomeData.DateEarned = nil
		loreDiscovery.Catalog = nil
		loreDiscovery.Timestamps = nil
		loreDiscovery.Collections = nil
		loreDiscovery[tomeId] = playerTomeData
		--DebugMessage("CLEANED OLD VARIABLES")
	end
	return loreDiscovery
end
