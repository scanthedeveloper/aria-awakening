


local SkillScrollLevels = {
    {"Apprentice", 30, 50},
    {"Journeyman", 50.1, 80},
    {"Master", 80.1, 90},
    {"Grandmaster", 90.1, 100}
}

SkillScroll = {}

SkillScroll.GetMastery = function(level)
    for i=1,#SkillScrollLevels do
        if ( level >= SkillScrollLevels[i][2] and level <= SkillScrollLevels[i][3] ) then
            return SkillScrollLevels[i][1]
        end
    end
    return nil
end

SkillScroll.GetMasteryMax = function(mastery)
    for i=1,#SkillScrollLevels do
        if ( SkillScrollLevels[i][1] == mastery ) then
            return SkillScrollLevels[i][3] or 0
        end
    end
    return 0
end

--- Dry run of applying a scroll
-- @param scrollObj | The skill scroll object
-- @param playerObj | Player to try to apply skill scroll to
SkillScroll.TryApply = function(scrollObj, playerObj)
    if ( scrollObj == nil ) then
        return false
    end
    if ( playerObj == nil ) then
        return false
    end
    local skillInfo = scrollObj:GetObjVar("SkillScrollInfo")
    if ( skillInfo == nil ) then
        playerObj:SystemMessage("Nothing can be learned from this scroll.", "info")
        return false
    end

    local mastery,amount,skillName = skillInfo[1],skillInfo[2],skillInfo[3]
    local skillDictionary = GetSkillDictionary(playerObj)
    local skillTotal = math.round(GetSkillTotal(nil, skillDictionary) + amount, 1)
    
    -- check for over total cap
    if ( skillTotal > ServerSettings.Skills.PlayerSkillCap.Total ) then
        playerObj:SystemMessage("There is nothing further this scroll can teach you.", "info")
        return false
    end

    -- check for over individual cap
    local newSkillLevel = math.round(GetSkillLevel(nil, skillName, skillDictionary, true) + amount, 1)
    if ( newSkillLevel > GetSkillCap(nil, skillName, skillDictionary) ) then
        playerObj:SystemMessage("There is nothing further this scroll can teach you.", "info")
        return false
    end

    -- finally check against masterY cap
    local masteryMax = SkillScroll.GetMasteryMax(mastery)
    if ( newSkillLevel > masteryMax ) then
        playerObj:SystemMessage("This scroll can only raise "..GetSkillDisplayName(skillName).." to a maximum of "..masteryMax..".", "info")
        return false
    end

    -- set new level
    skillDictionary[skillName].SkillLevel = newSkillLevel

    -- return some data
    return true, skillDictionary, skillName, amount, newSkillLevel
end

--- Apply the skill scroll to a player, verifies before applying
-- @param scrollObj
-- @param playerObj
-- @return bool | success
SkillScroll.Apply = function(scrollObj, playerObj)
    local success, sanitizedSkillDictionary, skillName, amount = SkillScroll.TryApply(scrollObj, playerObj)
    if ( success ) then
        -- save skill dictionary
        SetSkillDictionary(playerObj, sanitizedSkillDictionary)
            
        -- tell player the amount gained
        playerObj:SystemMessage(string.format("+%s to %s now %s", amount, GetSkillDisplayName(skillName), sanitizedSkillDictionary[skillName].SkillLevel or 0))

        -- check titles
        CheckTitleRequirement(playerObj, skillName)

        -- update client side
        playerObj:SendMessage("OnSkillLevelChanged", skillName)
    end
    return success
end

--- given the criminalSkills table (from their head) and turn it into SkillScrollInfo
-- @param criminalSkills
-- @return SkillScrollInfo
SkillScroll.BuildSkillScrollInfo = function(criminalSkills)
    if ( criminalSkills == nil ) then
        LuaDebugCallStack("[SkillScroll.FromCriminalSkills] criminalSkills not provided.")
        return nil
    end

    -- build the data used for weighted random
    local skills = {}
    local skillValueMax = {}
    local skillName,value,max
    for i=1,#criminalSkills do
        skillName,value,max = criminalSkills[i][1],criminalSkills[i][2],criminalSkills[i][3]
        if ( value > 0 ) then
            skillValueMax[skillName] = {value,max}
            for ii=1,math.round(value * 10) do
                skills[#skills+1] = skillName
            end
        end
    end

    -- no skills above 0 in the list
    if ( #skills == 0 ) then return nil end

    -- pick a random skill weighted by the skill level
    skillName = skills[math.random(1, #skills)]
    value,max = skillValueMax[skillName][1],skillValueMax[skillName][2]

    local rolls = {}
    for i=1,100 do
        if ( value >= 0.3 and i <= 5 ) then
            rolls[#rolls+1] = 0.3
        elseif ( value >= 0.2 and i <= 35 ) then
            rolls[#rolls+1] = 0.2
        else
            rolls[#rolls+1] = 0.1
        end
    end

    -- return the distilled data
    return {SkillScroll.GetMastery(max), rolls[math.random(1,#rolls)], skillName}

end

--- Create a skill scroll and place it in the player's backpack. this will destroy the head object
-- @param playerObj
-- @param criminalHead
-- @param cb(optional) | callback function(skillScroll, errorMessage)
SkillScroll.Create = function(playerObj, criminalHead, cb)
    if not( cb ) then cb = function(skillScroll, errorMessage) end end
    if ( playerObj == nil ) then
        LuaDebugCallStack("[SkillScroll.Create] playerObj not provided.")
        return cb(nil)
    end
    if ( criminalHead == nil or not criminalHead:IsValid() ) then
        LuaDebugCallStack("[SkillScroll.Create] criminalHead not provided (or invalid).")
        return cb(nil, "Error locating head.")
    end

    local resourceType = criminalHead:GetObjVar("ResourceType")

    -- give a more detailed response to rotten heads
    if ( resourceType == "RottenCriminalHead" ) then
        return cb(nil, "That head is rotten and no longer viable.")
    end
    
    if ( resourceType ~= "CriminalHead" ) then
        return cb(nil, "This process requires a severed human head.")
    end

    local criminalSkills = criminalHead:GetObjVar("CriminalSkillData")
    if ( criminalSkills == nil ) then
        return cb(nil, "That head contains no valuable knowledge.")
    end

    local skillScrollInfo = SkillScroll.BuildSkillScrollInfo(criminalSkills)
    if ( skillScrollInfo == nil ) then
        return cb(nil, "That head contains no valuable knowledge.")
    end

    -- destroy in same frame as to prevent exploit, sorry if it fails.
    criminalHead:Destroy()
    Create.InBackpack("skill_scroll", playerObj, nil, function(scrollObj)
        if ( scrollObj ) then
            local name, color = StripColorFromString(scrollObj:GetName() or "")
            scrollObj:SetName(string.format("%s%s %s[-]", color, skillScrollInfo[1], name))
            scrollObj:SetObjVar("SkillScrollInfo", skillScrollInfo)
            SetItemTooltip(scrollObj)
            cb(scrollObj)
        else
            cb(nil, "Failed to create skill scroll.")
        end
    end, true)

end