ChatHelper = {}

ChatHelper.GetFilters = function()
    return GlobalVarReadKey("Chat", "Filters") or {}
end

ChatHelper.PassFilter = function( _message )
    local words = ChatHelper.GetFilters()
    _message =  string.lower(string.gsub(_message, "%s+", ""))
    _message = _message:gsub('[%p%c%s]', '')
    --DebugMessage( _message )

    -- Check phrases
    for i=1, #words do 
        local word = string.lower(words[i])
        word = word:gsub("%-", "")
        if( string.match(_message, word ) ) then
            --DebugMessage( word )
            return false;
        end
    end

    return true;

end
