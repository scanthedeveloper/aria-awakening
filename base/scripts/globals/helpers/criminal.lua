--- Determine if a player is a criminal (criminal flagged or is a murderer)
-- @param playerObj
-- @param isMurderer - optional if already retrieved to save extra call to isMurderer
-- @return bool
function IsCriminal(playerObj, isMurderer)
    Verbose("Criminal", "IsCriminal", playerObj)
    if ( playerObj == nil ) then
        LuaDebugCallStack("[Criminal] Nil playerObj provided to IsCriminal")
        return false
    end

    if ( isMurderer == nil ) then isMurderer = IsMurderer(playerObj) end
    return playerObj:HasObjVar("IsCriminal") or isMurderer
end

--- Determine if a player is a murderer
-- @param playerObj
-- @return bool
function IsMurderer(playerObj, murderCount)
    Verbose("Criminal", "IsMurderer", playerObj)
    -- penitent murders are not technically murderers, even if they have a murder count
    -- a player that is in a chaos zone is also not counted as a murderer even if they have murderCounts
    if ( playerObj ~= nil and (playerObj:HasObjVar("IsPenitent") or ChaosZoneHelper.IsPlayerInChaosZone(playerObj)) ) then
        return false
    end
    return (murderCount ~= nil and murderCount or GetMurderCount(playerObj)) >= ServerSettings.Criminal.MurdererMurderCount
end

-- Determines if a players penitent status should expire
function TryPenitentExpire( _mobileObj )
    local expiresOn = _mobileObj:GetObjVar("PenitentExpires")

    if( expiresOn and ( DateTime.UtcNow > expiresOn ) ) then
        DebugMessage("Expiring Penitent")
        _mobileObj:DelObjVar("PenitentExpires")
        _mobileObj:DelObjVar("MurderCountExpire")
        _mobileObj:DelObjVar("MurderCount")
        _mobileObj:DelObjVar("IsPenitent")
        _mobileObj:SendMessage("UpdateName")
        
        -- Update all pets too
        ForeachActivePet(_mobileObj, function(pet)
            pet:DelObjVar("PenitentExpires")
            pet:DelObjVar("MurderCountExpire")
            pet:DelObjVar("MurderCount")
            pet:DelObjVar("IsPenitent")
            pet:SendMessage("UpdateName")
        end, true)
    end

end

function GetCriminalText(mobileObj)
    local criminalText = "Innocent"
	local isMurderer = IsMurderer(mobileObj)
	if(isMurderer) then
		criminalText = "Outcast"
	elseif(IsCriminal(mobileObj,isMurderer)) then
		criminalText = "Criminal"
    end
    return criminalText
end

--- Get the murder count for a player
-- @param playerObj
-- @return number
function GetMurderCount(playerObj)
    Verbose("Criminal", "GetMurderCount", playerObj)
    if ( playerObj == nil ) then
        LuaDebugCallStack("[Criminal] Nil playerObj provided to GetMurderCount")
        return 0
    end
    return playerObj:GetObjVar("MurderCount") or 0
end

--- Refresh the countdown to next murder count expire
-- @param playerObj
function RefreshMurderCountExpire(playerObj)
    playerObj:SetObjVar("MurderCountExpire", (playerObj:GetObjVar("PlayMinutes") or 0) + ServerSettings.Criminal.MurderCountExpireTimer.TotalMinutes)
end

--- Each player tick will check to see if a murder count has expired
-- @param playerObj
-- @param playMinutes (optional)
function UpdateMurderCountExpire(playerObj, playMinutes)
    local expire = playerObj:GetObjVar("MurderCountExpire")
    if ( expire ~= nil ) then
        if ( playMinutes == nil ) then
            playMinutes = playerObj:GetObjVar("PlayMinutes") or 0
        end
        -- if it's time to expire
        if ( playMinutes >= expire ) then
            local murderCount = GetMurderCount(playerObj)
            -- if there's murder counts to expire
            if ( murderCount > 0 ) then
                -- remove one
                murderCount = AdjustMurderCount(playerObj, -1, murderCount)
                -- if there's more still to be removed
                if ( murderCount > 0 ) then
                    RefreshMurderCountExpire(playerObj) -- refresh
                    return
                end
            end
            -- time to expire and a refresh didn't happen, delete variable
            playerObj:DelObjVar("MurderCountExpire")
        end
    end
end

--- Adjust a player's murder count by an amount
-- @param playerObj
-- @param amount
-- @param murderCount (optional)
-- @return number | updated murder count
function AdjustMurderCount(playerObj, amount, murderCount)
    Verbose("Criminal", "AdjustMurderCount", playerObj, amount)
    if ( amount == 0 or amount == nil ) then return end
    if ( murderCount == nil ) then
        murderCount = GetMurderCount(playerObj)
    end
    local newMurderCount = murderCount + amount
    playerObj:SetObjVar("MurderCount", newMurderCount)

    if ( amount > 0 ) then
        RefreshMurderCountExpire(playerObj)
        playerObj:NpcSpeech(string.format("[FF0000]+%d murder[-]", amount), "combat")
        WarnRegionOfMurder(playerObj)
    else
        playerObj:NpcSpeech(string.format("[00FF00]%d murder[-]", amount), "combat")
        if ( playerObj:HasObjVar("IsPenitent") ) then
            if ( newMurderCount < ServerSettings.Criminal.MurdererMurderCount ) then
                playerObj:DelObjVar("PenitentExpires")
                playerObj:DelObjVar("IsPenitent")
                -- update the name
                playerObj:SendMessage("UpdateName")
                -- update all pets
                ForeachActivePet(playerObj, function(pet)
                    pet:SetObjVar("MurderCount", newMurderCount)
                    pet:DelObjVar("PenitentExpires")
                    pet:DelObjVar("IsPenitent")
                    pet:SendMessage("UpdateName")
                end, true)
                playerObj:SystemMessage("You are truly penitent!", "event")
            end
            -- burning off murders, stop here
            return murderCount
        end
    end

    local update = false
    if ( newMurderCount >= ServerSettings.Criminal.MurdererMurderCount and murderCount < ServerSettings.Criminal.MurdererMurderCount ) then
        -- If they have mudercounts they don't need to be in a township or a militia
        TownshipsHelper.RemovePlayerFromTownship( playerObj )
        Militia.RemovePlayer(playerObj, true)

        CriminalEnd(playerObj)
        playerObj:SystemMessage("You are now an Outcast!", "event")
        playerObj:SendMessage("StartMobileEffect", "Outcast")
        update = true
    elseif ( newMurderCount < ServerSettings.Criminal.MurdererMurderCount and murderCount >= ServerSettings.Criminal.MurdererMurderCount ) then
        playerObj:SystemMessage("You are no longer an Outcast.", "event")
        playerObj:SendMessage("EndOutcastEffect")
        if ( playerObj:HasTimer("CriminalTimer") ) then
            playerObj:SendMessage("StartMobileEffect", "Criminal", nil, {Duration = playerObj:GetTimerDelay("CriminalTimer")})
        end
        update = true
    end

    if ( update ) then
        -- update the name
        playerObj:SendMessage("UpdateName")
        -- update all pets
        ForeachActivePet(playerObj, function(pet)
            pet:SetObjVar("MurderCount", newMurderCount)
            pet:SendMessage("UpdateName")
        end, true)
    end

    return murderCount
end

function WarnRegionOfMurder(playerObj)
    local regionAddresses = {}
    if ( ServerSettings.RegionAddress ) then table.insert(regionAddresses, ServerSettings.RegionAddress) end
    local dungeonSubregionAddress = GetSubregionForDungeon(playerObj)
    if ( dungeonSubregionAddress ) then table.insert(regionAddresses, dungeonSubregionAddress) end
    if ( regionAddresses and next(regionAddresses) ) then
        local name = GetRegionalName(playerObj:GetLoc()) or "Unknown"
        local message = "The "..GetCriminalText(playerObj).." "..playerObj:GetName().." has been spotted in "..name.."."
        for i = 1, #regionAddresses do
            if (regionAddresses[i] ~= ServerSettings.RegionAddress) then
                MessageRemoteClusterController(regionAddresses[i], "RegionalSystemMessage", message)
            elseif ( regionAddresses[i] == ServerSettings.RegionAddress ) then
                local clusterController = GetClusterController()
                if ( clusterController ) then
                    GetClusterController():SendMessage("RegionalSystemMessage", message)
                end
            end
        end
    end
end

--- Determine if an action to another player is a criminal action
-- @param mobileA | the player performing the action
-- @param mobileB | the mobile the action is performed on
-- @param beneficial
-- @return bool
function IsCriminalAction(mobileA, mobileB, beneficial)
	Verbose("Criminal", "IsCriminalAction", mobileA, mobileB, beneficial)
    if ( mobileA == nil ) then
        LuaDebugCallStack("[Criminal] Nil mobileA provided.")
        return false
    end
    if ( mobileB == nil ) then
        LuaDebugCallStack("[Criminal] Nil mobileB provided.")
        return false
    end

    -- ensure mobileA is not mobileB and mobileB is in the same region.
    if ( mobileB == mobileA or not mobileB:IsValid() ) then return false end
    
    -- criminal actions can only be applied to players
    if not( IsPlayerCharacter(mobileA) ) then return false end

    local owner = mobileB:GetObjVar("controller")

    -- explicitly stop all criminal actions on your own pets/followers.
    if ( mobileA == owner ) then return false end

    -- If this is a corpse we need to keep a refrence to it
    local corpse = nil
    if( owner and mobileB:GetCreationTemplateId() == "player_corpse" ) then
        corpse = mobileB
    end

    -- reassign to owner
    local isPlayerB = IsPlayerObject(mobileB)
    if ( owner and owner:IsValid() and not IsPossessed(mobileB)) then mobileB = owner end

    -- ensure they are both within an area crimes matter
    if ( mobileB ~= nil and (not WithinCriminalArea(mobileA) and not WithinCriminalArea(mobileB)) ) then
        return false
    end

    if ( isPlayerB ) then
        -- if target is criminal only a crime to benefit them, if not beneficial it's not criminal if they are criminal
        if ( beneficial ) then return IsCriminal(mobileB) elseif ( IsCriminal(mobileB) ) then return false end

        -- non beneficial actions are not crimes when both players have consented.
        if ( not beneficial and ShareGroupConsent(mobileA, mobileB) ) then return false end

        -- players in opposing factions are not commiting crimes
        if ( Militia.InOpposing(mobileA, mobileB) ) then return false end

        -- Are both players in a chaotic zone?
        --DebugMessage( "mobileA", tostring( ChaosZoneHelper.IsPlayerChaotic(mobileA) ) )
        --DebugMessage( "mobileB.1", tostring( ChaosZoneHelper.IsPlayerChaotic(mobileB) ) )
        --DebugMessage( "mobileB.2", tostring( (corpse and ChaosZoneHelper.IsLocInChaosZone(corpse:GetLoc())) ) )
        if( ChaosZoneHelper.IsPlayerChaotic(mobileA) and (ChaosZoneHelper.IsPlayerChaotic(mobileB) or (corpse and ChaosZoneHelper.IsLocInChaosZone(corpse:GetLoc())) ) ) then return false end

        -- players in an arena match
        if(RankedArena.InRankedArenaMatch(mobileA, mobileB) ) then return false end

        -- players in guild wars are not commiting crimes
        if ( GuildHelpers.InGuildWarWith(mobileA, mobileB) ) then return false end
    else
        if ( not mobileB:HasObjVar("ImportantNPC") and not mobileB:HasObjVar("GuardProtect") ) then
            return false
        elseif ( beneficial and (mobileB:HasObjVar("ImportantNPC") or mobileB:HasObjVar("GuardProtect") ) ) then
            return false
        end
    end

    if ( not beneficial and IsAggressorTo(mobileB, mobileA) ) then
        -- victim/defender action against aggressor, no crime.
        return false
    end

    return true
end

--- Perform the criminal action applying all consequences from the action
-- @param mobileA the mobile that is performing the criminal action
-- @return none
function ExecuteCriminalAction(playerObj)
    Verbose("Criminal", "ExecuteCriminalAction", playerObj)

    -- end their free ride for being a jerk
    if ( IsInitiate(playerObj) ) then EndInitiate(playerObj) end

    -- start/refresh the criminal timer (this controls OnTheRun and Criminal debuff expires)
    playerObj:ScheduleTimerDelay(ServerSettings.Criminal.TemporaryDuration, "CriminalTimer")

    if ( playerObj:HasObjVar("IsPenitent") ) then
        playerObj:DelObjVar("PenitentExpires")
        playerObj:DelObjVar("IsPenitent")
        -- update the name
        playerObj:SendMessage("UpdateName")
        -- update all pets
        ForeachActivePet(playerObj, function(pet)
            pet:DelObjVar("PenitentExpires")
            pet:DelObjVar("IsPenitent")
            pet:SendMessage("UpdateName")
        end, true)
        if ( IsMurderer(playerObj) ) then
            TownshipsHelper.RemovePlayerFromTownship( playerObj )
            Militia.RemovePlayer(playerObj, true)
            playerObj:SystemMessage("You are now an Outcast!", "event")
            playerObj:SendMessage("StartMobileEffect", "Outcast")
        end
    end

    if ( not IsMurderer(playerObj) ) then
        if not( playerObj:HasObjVar("IsCriminal") ) then
            playerObj:SetObjVar("IsCriminal", true)
            playerObj:SystemMessage("You are now a criminal!", "event")
            playerObj:SendMessage("UpdateName")
            -- update all pets
            ForeachActivePet(playerObj, function(pet)
                pet:SetObjVar("IsCriminal", true)
                pet:SendMessage("UpdateName")
            end, true)
        end
        -- start/refresh criminal debuff
        --DebugMessage("StartMobileEffect")
        playerObj:SendMessage("StartMobileEffect", "Criminal")
    end
    -- start/refresh on the run debuff
    playerObj:SendMessage("StartMobileEffect", "OnTheRun")
end

function ForcePeaceful(playerObj)
    playerObj:SetObjVar("CriminalProtect", true)
	playerObj:SendClientMessage("SetKarmaState", "Peaceful")
end

function CriminalEnd(playerObj)
    DebugMessage("CriminalEnd")
    if ( playerObj:HasObjVar("IsCriminal") ) then
        playerObj:DelObjVar("IsCriminal")
        playerObj:SendMessage("UpdateName")
        playerObj:SendMessage("EndCriminalEffect")
        -- update all pets
        ForeachActivePet(playerObj, function(pet)
            pet:DelObjVar("IsCriminal")
            pet:SendMessage("UpdateName")
        end, true)
        return true
    end
    return false
end

--- Function called when the CriminalTimer has expired
-- @param playerObj | player to clear of criminality
-- @return none
function CriminalTimerExpire(playerObj)
    Verbose("Criminal", "CriminalTimerExpire", playerObj)

    if ( CriminalEnd(playerObj) ) then
        playerObj:SendMessage("EndOnTheRunEffect")
        playerObj:SystemMessage("You are no longer a criminal.", "event")
        -- set their pvp flag to peaceful
        ForcePeaceful(playerObj)
    end

end

--- Check when a player performs a beneficial action on a mobile(player/npc/etc)
-- @param player(playerObj) DOES NOT ENFORCE IsPlayerCharacter()
-- @param target(mobileObj)
-- @return none
function CheckCriminalBeneficialAction(player, target)
    Verbose("Criminal", "CheckCriminalBeneficialAction", player, target)
    local owner = target:GetObjVar("controller")
    if ( owner and owner:IsValid() ) then target = owner end
    -- can't get in trouble for benefiting yourself or your pets
    if ( player == target ) then return end

    -- flag them a criminal if it's criminal to benefit this target
    if ( IsCriminalAction(player, target, true) ) then ExecuteCriminalAction(player) end
end

function CriminalProtectWarn(player, target, silent, isProtected)

    if ( not silent and not player:HasTimer("CriminalWarned") ) then
        player:ScheduleTimerDelay(ServerSettings.Criminal.MinimumBetweenNegativeWarnings, "CriminalWarned")
        if ( isProtected ) then
            ChangeToPossessee(player):SystemMessage("Cannot do that in a protected area.", "info")
        else
            ChangeToPossessee(player):SystemMessage("That is a criminal action.", "info")
        end
    end
end

--- Returns true if the player has opted into criminal protection and this is a criminal action
-- @param player
-- @param silent - If true, no user messages will be sent to inform player
-- @return true or false
function ShouldCriminalProtect(player, target, beneficial, silent)
    Verbose("Criminal", "ShouldCriminalProtect", player, silent)
    if not( player ) then
        LuaDebugCallStack("[ShouldCriminalProtect] player not provided.")
        return false
    end

    local protected = nil

    -- if pvp is completely disabled in guaranteed guard protection, allow beneficial "negative" actions
    if ( beneficial and not ServerSettings.Criminal.AllowNegativeActionsInProtected ) then
        protected = IsGuaranteedProtected(target, player)
        if ( protected ) then return false end
    end

    -- Stop beneficial spells on non-pet npcs
    if ( beneficial and not IsPlayerCharacter(target) and not IsPet(target) ) then return true end

    -- not opted in, doesn't need to be protected
    if not( player:HasObjVar("CriminalProtect") ) then return false end
    
    if ( IsCriminalAction(player, target, beneficial) ) then
        -- then protect them.
        protected = ((protected ~= nil) and protected) or IsGuaranteedProtected(target, player)
        --DebugMessage("Warn: 1")
        CriminalProtectWarn(player, target, silent, protected)
        return true
    end

    if not( ServerSettings.Criminal.AllowNegativeActionsInProtected ) then
        -- no one can be attacked in protected areas
        protected = ((protected ~= nil) and protected) or IsGuaranteedProtected(target, player)    
        if ( protected ) then
            -- then protect them.
            --DebugMessage("Warn: 2")
            CriminalProtectWarn(player, target, silent, protected)
            return true
        end
    end

    return false
end

--- Check player against a container on a loot (item removed from a container)
-- @param player(playerObj) Player doing the looting
-- @param container(Container) Container being looted from (mobileB:TopmostContainer())
-- @return false if should prevent, nil otherwise
function CheckCriminalLoot(player, container)
    Verbose("Criminal", "CheckCriminalLoot", player, container)
    if ( container == nil ) then
        LuaDebugCallStack("[CheckCriminalLoot] Nil container provided.")
        return false
    end
    if ( container:IsPermanent() ) then return end
    -- can't get in trouble doing stuff to yourself..
    if ( player == container ) then return end
    -- only mobiles supported right now
    if not( container:IsMobile() ) then return end
    -- can't get in trouble doing stuff to your pets
    if ( player == container:GetObjVar("controller") ) then return end

    if ( IsPlayerObject(container) ) then
        -- can't get in trouble looting your own corpse
        if ( player.Id == container:GetObjVar("PlayerId") ) then return end
        if ( Militia.CanLootPlayer(container, player) ) then return end
        if (
            -- disallow looting in protected areas.
            IsGuaranteedProtected(container, player)
            or
            -- prevent criminal actions if opted in
            ShouldCriminalProtect(player, container)
        ) then return false end
        -- if looting a player owned container that's not theirs, they performed a criminal action
        if ( IsCriminalAction(player, container) ) then
            ExecuteCriminalAction(player)
        end
        return
    else
        -- the container is not tagged by the player, therefor the player doesn't have the right to loot.
        if ( IsMobTaggedBy(container, player) == false ) then

            local protection = GetGuardProtection(player)
            if ( protection and protection ~= "None" ) then
                return false
            end

            local tag = container:GetObjVar("Tag") -- the player(s) crime is committed against (the winners)
            local wronged = nil
            -- loop all winners, stopping on first one that it's criminal to loot against
            for winner,bool in pairs(tag) do
                if ( winner and winner:IsValid() and IsCriminalAction(player, winner) ) then
                    wronged = winner
                    break
                end
            end

            if ( wronged ) then
                -- if criminal protected
                if ( ShouldCriminalProtect(player, wronged) ) then
                    if not( player:HasTimer("CriminalWarned") ) then
                        player:ScheduleTimerDelay(ServerSettings.Criminal.MinimumBetweenNegativeWarnings, "CriminalWarned")
                        local groupId = GetGroupId(player)
                        if ( groupId ~= nil and wronged:GetObjVar("TagGroup") == groupId ) then
                            player:SystemMessage("Not your turn, looting that is a criminal action.", "info")
                        else
                            player:SystemMessage("That is not your kill, looting it is a criminal action.", "info")
                        end
                    end
                    return false -- protect them
                end
                ExecuteCriminalAction(player)
            end
        end
    end
end

--- Anyone that is an aggressor to the victim will have a Murder Count added.
-- @param victim(mobileObj)
-- @return none
function PunishAllAggressorsForMurder(victim)

    if( ServerSettings.Criminal.IgnoreMurderPunishment ) then return end

    Verbose("Criminal", "PunishAllAggressorsForMurder", victim)
    ForeachAggressor(victim, function(aggressor)
        if (
            victim == nil or not victim:IsValid() -- if victim isn't provided/valid
            or
            victim == aggressor -- if themselves
            or
            not aggressor:IsValid() -- or not valid
            or
            not IsPlayerCharacter(aggressor) -- or not player character
            or
            ShareGroupConsent(victim, aggressor) -- or in a consenting group
            or
            Militia.InOpposing(victim, aggressor) -- or in opposing militia
            or
            GuildHelpers.InGuildWarWith(victim, aggressor) -- or in a guild war
            or 
            ChaosZoneHelper.IsPlayerChaotic(aggressor)
        ) then
            -- do nothing
        else
            -- clear the conflicts between each other
            ClearConflicts(victim, aggressor)
            -- give them a murder count
            AdjustMurderCount(aggressor, 1)
            -- offer victim a chance to forgive them
            OfferForgiveMurderDialog(victim, aggressor);
        end
	end)
end

--- will give a display window that allows you to forgive a specific murderer (remove a murder count)
function OfferForgiveMurderDialog(victim, murderer)
    local id = "ForgiveMurder"..murderer.Id
    victim:ScheduleTimerDelay(TimeSpan.FromMinutes(1), id)
    ClientDialog.Show{
        TargetUser = victim,
        DialogId = "Forgive"..id,
        TitleStr = "Forgive Murder?",
        DescStr = (murderer:GetCharacterName() or "Unknown").." has taken part in your murder. Forgive them? You have 60 seconds to decide.",
        Button1Str = "FORGIVE",
        Button2Str = "No",
        ResponseObj = victim,
        ResponseFunc = function( user, buttonId )
            buttonId = tonumber(buttonId)
            if ( user == victim and buttonId == 0 ) then
                if ( user:HasTimer(id) ) then
                    if ( murderer:IsValid() ) then
                        AdjustMurderCount(murderer, -1)
                        murderer:SystemMessage((victim:GetCharacterName() or "Unknown").." has forgiven you.", "event")
                    else
                        victim:SystemMessage("They are no where to be found, could not forgive them of their murder.", "info")
                    end
                else
                    victim:SystemMessage("It has been too long since the murder to forgive.", "info")
                end
            end
        end,
    }
end

--- Colorize a mobile name dependant on their criminal status
-- @param mobile(mobileObj) the mobile used to decide the name color
-- @param newName(string) The name to be colorized
-- @return colorized newName(string)
function ColorizeMobileName(mobile, newName)
    Verbose("Criminal", "ColorizeMobileName", mobile, newName)
    if ( mobile == nil ) then
        LuaDebugCallStack("[Criminal] Nil mobile supplied to ColorizeMobileName")
        return newName
    end

    local color = nil

    if ( mobile:HasObjVar("ImportantNPC") ) then
        color = "F2F5A9"
    else
        if ( mobile:HasObjVar("GuardProtect") ) then
            color = "FFFFFF"
        else
            color = "A7A7A7"
        end
    end
    
	return string.format("[%s]%s[-]", color, newName)
end

--- Colorize a player's name dependant on their criminal status
-- @param player(mobileObj) the playerObj used to decide the name color
-- @param newName(string) The name to be colorized
-- @return colorized newName(string)
function ColorizePlayerName(player, newName)
    Verbose("Criminal", "ColorizePlayerName", player, newName)
    if ( player == nil ) then
        LuaDebugCallStack("[Criminal] Nil player supplied to ColorizePlayerName")
        return newName
    end

    local color = player:GetObjVar("NameColorOverride")

    if ( color == nil and not(TRAILER_BUILD) ) then
        if ( IsImmortal(player) and not TestMortal(player) ) then color = "FFBF00" end
    end

    if ( color ) then
        -- do nothing
    elseif ( IsMurderer(player) ) then
        color = "FF0000"
    elseif ( player:HasObjVar("IsCriminal") ) then
        color = "A7A7A7"
    else
        color = "82ABFF"
    end

	return string.format("[%s]%s[-]", color, newName)
end

function ApplyPersistentCriminalEffects(playerObj)
    if ( not playerObj or not playerObj:IsValid() or not playerObj:IsPlayer() ) then
		LuaDebugCallStack("No valid playerObj for ApplyCriminalEffects.")
		return
    end
    local duration = nil
    if ( playerObj:HasTimer("CriminalTimer") ) then
        duration = playerObj:GetTimerDelay("CriminalTimer")
        playerObj:SendMessage("StartMobileEffect", "OnTheRun", nil, {Duration = duration})
    end
    if ( IsMurderer(playerObj) ) then
        playerObj:SendMessage("StartMobileEffect", "Outcast")
    elseif ( playerObj:HasObjVar("IsCriminal") ) then
        playerObj:SendMessage("StartMobileEffect", "Criminal", nil, {Duration = duration})
    end
end

--- Determine if a mobile is in a zone region that criminal actions matter (Checks only specific Zones)
-- @param mobileObj
-- @return true/false
function WithinCriminalZone(mobileObj)
    Verbose("Criminal", "WithinCriminalZone", mobileObj)
    local ss = ServerSettings.Criminal.DisableCriminalZones
	for i=1,#ss do
		if ( mobileObj:IsInRegion(ss[i]) ) then return false end
    end

    return true
end

--- Determine if a mobile is in a location that criminal actions matter (Check RegionAddress and Specific zones)
-- @param mobileObj
-- @return true/false
function WithinCriminalArea(mobileObj)
    Verbose("Criminal", "WithinCriminalArea", mobileObj)
    -- first check region address
    local regionAddress = ServerSettings.RegionAddress
    local ss = ServerSettings.Criminal.DisableCriminalRegionAddresses
	for i=1,#ss do
		if ( regionAddress == ss[i] ) then return false end
    end
    -- then check zone
    return WithinCriminalZone(mobileObj)
end

--- Will modify skills to player criminal and return a list of the modified skills containing amount taken and level before being taken
-- @param player
-- @return lua table
function CriminalStatLoss(player)

    if( ServerSettings.Criminal.IgnoreMurderPunishment ) then return end
    if( ChaosZoneHelper.IsPlayerInChaosZone( player ) ) then return end

    -- does not affect gods
    if ( IsImmortal(player) and not TestMortal(player) ) then return end
    local criminalSkills = nil
    local murderCount = GetMurderCount(player)
    if ( (murderCount > 0 and not player:HasObjVar("IsPenitent")) or player:HasObjVar("IsCriminal") ) then
    	-- no stat loss while debuffed
    	if ( HasMobileEffect(player, "CapitalPunishment") ) then return end

        criminalSkills = {}
        local skillDictionary = GetSkillDictionary(player)

        local min,max = 0.1,0.5
        local nSkills = 1
        if ( IsMurderer(player, murderCount) ) then
            min,max = 0.5,2
            nSkills = math.ceil(5*(math.min(50, murderCount)/50))

            -- punish them with a capital
            player:SendMessage("StartMobileEffect", "CapitalPunishment")
        end
        -- scale up for math.random (it doesn't work on floats)
        min,max = min*10,max*10
        
        local skillValues = {}
        for key,value in pairs(skillDictionary) do skillValues[#skillValues+1] = {key,skillDictionary[key].SkillLevel or 0} end

        -- sort skills highest to lowest
        table.sort(skillValues, function(a, b)
            return a[2] > b[2]
        end)

        -- take the highest skills, one skill is taken for each murder count
        local skillName, level
        local len = math.min(nSkills, #skillValues)
        for i=1,len do
            skillName = skillValues[i][1]
            level = skillDictionary[skillName].SkillLevel or 0
            if ( level > 0 ) then
                -- record the amount to be taken/re-added
                criminalSkills[#criminalSkills+1] = {skillName, math.round(math.min(level, math.random(min, max) * 0.1), 1), level}
                -- update the skill dictionary with the new value
                skillDictionary[skillName].SkillLevel = level - criminalSkills[#criminalSkills][2]
            end
        end

        -- sort the criminal skills top to bottom
        table.sort(criminalSkills, function(a, b)
            return a[2] > b[2]
        end)

        -- save skill dictionary
        SetSkillDictionary(player, skillDictionary)
        
        local skillName
        for i=1,#criminalSkills do
            skillName = criminalSkills[i][1]
            -- tell player the amount lost
            player:SystemMessage(string.format("-%s to %s now %s", math.round(criminalSkills[i][2], 1), GetSkillDisplayName(skillName), math.round(skillDictionary[skillName].SkillLevel or 0, 1)))

            -- check titles
            CheckTitleRequirement(player, skillName)

            -- update client side
	        player:SendMessage("OnSkillLevelChanged", skillName)
        end

    end
    return criminalSkills
end

function SanitizeCriminalStats(player, criminalSkills)
    if ( criminalSkills == nil ) then return nil end

    local skillDictionary = GetSkillDictionary(player)

    local skillName, value, max, newLevel
    local total = GetSkillTotal(nil, skillDictionary)
    local skillChanges = {}
    for i=1,#criminalSkills do
        skillName, value, max = criminalSkills[i][1], criminalSkills[i][2], criminalSkills[i][3]
        newLevel = SkillClampLevel(math.round((skillDictionary[skillName].SkillLevel or 0) + value, 1), skillName, skillDictionary)
        if ( newLevel > max ) then newLevel = max end
        
        -- done this way as we have max the skill can be increased to adhear by as well.
        value = math.round(newLevel - (skillDictionary[skillName].SkillLevel or 0), 1)
        if ( value > 0 ) then
            skillChanges[#skillChanges+1] = {skillName,value}
            total = total + value
        end
    end

    -- if they are going to be put over cap, take away some of the additions
    while ( total > ServerSettings.Skills.PlayerSkillCap.Total ) do
        if ( #skillChanges < 1 ) then
            total = ServerSettings.Skills.PlayerSkillCap.Total
        else
            skillChanges[#skillChanges][2] = math.round(skillChanges[#skillChanges][2] - 0.1, 1)
            if ( skillChanges[#skillChanges][2] <= 0 ) then
                table.remove(skillChanges, #skillChanges)
            end
            total = math.round(total - 0.1, 1)
        end
    end

    return skillChanges, skillDictionary

end

function ApplySanitizedCriminalStats(player, criminalSkills, skillChanges, skillDictionary)
    local skillName, value, any
    for i=1,#skillChanges do
        skillName, value, any = skillChanges[i][1], skillChanges[i][2], true
        skillDictionary[skillName].SkillLevel = math.round((skillDictionary[skillName].SkillLevel or 0) + value, 1)
    end

    if ( any == true ) then
        -- save skill dictionary
        SetSkillDictionary(player, skillDictionary)

        for i=1,#skillChanges do
            skillName, value = skillChanges[i][1], skillChanges[i][2]
            
            -- tell player the amount gained
            player:SystemMessage(string.format("+%s to %s now %s", value, GetSkillDisplayName(skillName), skillDictionary[skillName].SkillLevel or 0))

            -- check titles
            CheckTitleRequirement(player, skillName)

            -- update client side
            player:SendMessage("OnSkillLevelChanged", skillName)
        end
    end

    return any
end