
local VisualDeathSlots = {
	"BodyPartHair",
	"BodyPartFacialHair",
	"Head",
	"Chest",
	"Legs",
	"LeftHand",
	"RightHand"
}
local SLOTS = ITEM_SLOTS

AstralPlane = {
	Leave = function(player, force)
		if ( not force and not player:HasObjVar("InAstralPlane") ) then return end
		player:DelObjVar("InAstralPlane")
		-- allow them to see nearby mobiles again
		player:SetHideNearbyMobiles(false)
		-- force an update on all nearby mobiles
		local nearbyMobiles = FindObjects(SearchMobileInRange(player:GetUpdateRange(), false, true))
		for i=1,#nearbyMobiles do
			nearbyMobiles[i]:ForceObjectUpdate(player)
		end
		local corpse = player:GetObjVar("CorpseObject")
		if ( corpse and corpse:IsValid() ) then
			corpse:ForceObjectUpdate(player)
		end
	end,
	Join = function(player, force)
		if ( not force and player:HasObjVar("InAstralPlane") ) then return end
		player:SetObjVar("InAstralPlane", true)
		-- prevent from seeing any mobiles
		player:SetHideNearbyMobiles(true)
		-- force an update on all nearby mobiles
		local nearbyMobiles = FindObjects(SearchMobileInRange(player:GetUpdateRange(), false, true))
		for i=1,#nearbyMobiles do
			nearbyMobiles[i]:ForceObjectUpdate(player)
		end
	end
}

function PlayerDeathStart(player)

	local backpack = player:GetEquippedObject("Backpack")
	-- close all open container windows in the players backpack
	if ( backpack ~= nil ) then
		CloseContainerRecursive(player, backpack)
	end

	-- if you are carrying something under the mouse cursor and die, it should drop to the ground
	local carriedObject = player:CarriedObject()
	if ( carriedObject ~= nil and carriedObject:IsValid() ) then
		carriedObject:SetWorldPosition(player:GetLoc())
	end

	if ( player:HasObjVar("OverrideDeath") ) then return end
	
	SetMobileMod(player, "VitalityRegenPlus", "Death", -1000)

	-- temp release all pets
	local pets = player:GetOwnedObjects()
	if ( #pets > 0 ) then
		player:SetObjVar("LivingPets", pets)
		local pet
		for i=1,#pets do
			pet = pets[i]
			if ( IsPet(pet) ) then
				pet:SetObjectOwner(nil)
				pet:AddModule("pet_temp_release")
			end
		end
	end

	-- force them out of combat
	player:SendMessage("EndCombatMessage")

	player:SetMobileFrozen(true, true)

	player:PlayLocalEffect(player, "GrayScreenEffect")
	player:PlayMusic("Death")

	player:SystemMessage("[D70000]You have died![-]","info")
	--player:SystemMessage("[$1681]", "info")

	player:SetSharedObjectProperty("IsDead", true)
	-- create the corpse
	PlayerCorpseCreate(player, function(corpse)
		if not( corpse ) then return end

		-- 'hide' the player
		player:SetObjVar("LivingScale", player:GetScale())
		player:SetScale(Loc(0.001,0.001,0.001))

		-- Set the controller on the corpse
		corpse:SetObjVar("controller", player)

		if ( ShouldDropFullLoot(player) ) then
			-- move all backpack/equipped items to the corpse.
			PlayerTransferContentsToCorpse(player, corpse)
		end

		-- start the auto release countdown
		CallFunctionDelayed(TimeSpan.FromSeconds(3), function()
			corpse:SetObjVar("AutoReleaseAt", DateTime.UtcNow:Add(ServerSettings.Death.CorpseAutoRelease))
			PlayerCorpseReleaseDialog( player )
		end)

		-- set militia ID if player was in militia conflict on death
		Militia.SetIdAtDeath(corpse, player)

	end)
end

function PlayerReleaseCorpse( player, cb )
    if not( cb ) then cb = function(success) end end
    
    -- prevent releasing corpse if you've been resurrected recently
    if ( player:HasTimer("RecentlyResurrected") ) then return cb(false) end

	-- player can only release if they are infact a corpse
	if ( player:GetSharedObjectProperty("IsDead") ~= true ) then return cb(false) end

	-- prevent doing this multiple times
	if ( player:HasTimer("ReleasingCorpse") ) then return cb(false) end
	player:ScheduleTimerDelay(TimeSpan.FromSeconds(10), "ReleasingCorpse")

	-- don't need to count this down any longer
	player:RemoveTimer("AutoCorpseRelease")

	-- prevent the self res dialog from showing via corpse range event
	player:ScheduleTimerDelay(TimeSpan.FromSeconds(5), "RecentlyReleased")

	PlayerTurnIntoGhost(player, not ShouldDropFullLoot(player))
	CloseResurrectDialog(player)
    player:SetMobileFrozen(false, false)

	-- set this objvar that is cleared when near a shrine to ensure they cannot stop region transfer.
	player:SetObjVar("DeathShrineClear", true)
	-- move them to the closest shrine
	TeleportToClosestShrine(player, function(success)
		if not( success ) then
			if ( player and player:IsValid() ) then
				player:SystemMessage("Failed to move to closest resurrect shrine.", "info")
			end
		end
		cb(success)
	end)
end

function PlayerCorpseReleaseDialog( player )
	-- if they are not a corpse, no reason to continue.
	if not( player:GetSharedObjectProperty("IsDead") == true ) then return end

    local releaseAt = nil -- auto release if corpse isn't found
    local corpse = player:GetObjVar("CorpseObject")
    if ( corpse and corpse:IsValid() ) then
        releaseAt = corpse:GetObjVar("AutoReleaseAt")
    end

	local now = DateTime.UtcNow
	-- loaded from backup probably, past release time, do release.
	if ( releaseAt == nil or now >= releaseAt ) then
		PlayerReleaseCorpse(player)
		return
	end

	local diff = releaseAt:Subtract(now)

	-- schedule the auto release
	player:ScheduleTimerDelay(diff, "AutoCorpseRelease")
	
	-- present the release dialog
	PlayerResurrectDialog(player, {
		text = "Release",
		button = "Release Spirit",
		tooltip = "Release your spirit to the nearest resurrection shrine.",
		timer = GetTimerLabelString(diff, true),
		windowframe = "Death_ReleaseButtonFrame",
		symbol = "Death_SymbolRelease",
		cb = function(accept)
			if ( accept ) then
				PlayerReleaseCorpse(player)
			end
		end,
	})

end

--- Convenince function for creating a standard resurrect dialog box. The Template for Resurrect Dialog
-- @param player
function PlayerResurrectDialog( player, data )
	if not( data ) then data = {} end
	if not( data.cb ) then data.cb = function() end end
	if not( data.text ) then data.text = "Resurrect" end
	if not( data.button ) then data.button = "Accept" end
	if not( data.tooltip ) then data.tooltip = "" end
	if not( data.cancelable ) then data.cancelable = false end
	if not( data.response ) then data.response = player end
	if not( data.windowframe ) then data.windowframe = "Death_ReleaseButtonFrame_Resurrect" end
	if not( data.symbol ) then data.symbol = "Death_SymbolResurrect" end

	local dynamicWindow = DynamicWindow("ResurrectDialog", "", 0, 0, -230, 536, "Transparent", "Top")

	dynamicWindow:AddImage(
		0,  --(number) x position in pixels on the window
		0,  --(number) y position in pixels on the window
		data.windowframe,  --(string) name of image on client
		460,  --(number) width of the image
		260  --(number) height of the image
		--"" --(string) sprite type Simple, Sliced or Object (defaults to Simple)
		--"000000" --(string) sprite hue (defaults to white)
		--1.0 --(number) opacity (default 1.0)
	)
	
	dynamicWindow:AddButton(
		28,  --(number) x position in pixels on the window
		98,  --(number) y position in pixels on the window
		"Accept",  --(string) return id used in the DynamicWindowReponse event
		"",  --(string) text in the button (defaults to empty string)
		404,  --(number) width of the button (defaults to width of text)
		64,  --(number) height of the button (default decided by type of button)
		"",  --(string) mouseover tooltip for the button (default blank)
		"",  --(string) server command to send on button click (default to none)
		true,  --(boolean) should the window close when this button is clicked? (default true)
		"DeathButton"  --(string) button type (default "Default")
		--buttonState --(string) button state (optional, valid options are default,pressed,disabled)
		--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
	)

	dynamicWindow:AddImage(
		198,  --(number) x position in pixels on the window
		26,  --(number) y position in pixels on the window
		data.symbol,  --(string) name of image on client
		64,  --(number) width of the image
		64  --(number) height of the image
		--"" --(string) sprite type Simple, Sliced or Object (defaults to Simple)
		--"000000" --(string) sprite hue (defaults to white)
		--1.0 --(number) opacity (default 1.0)
	)
	
	dynamicWindow:AddLabel(
		231,  --(number) x position in pixels on the window
		118,  --(number) y position in pixels on the window
		data.text,  --(string) text in the label
		600,  --(number) width of the text for wrapping purposes (defaults to width of text)
		0,  --(number) height of the label (defaults to unlimited, text is not clipped)
		36,  --(number) font size (default specific to client)
		"center",  --(string) alignment "left", "center", or "right" (default "left")
		false,  --(boolean) scrollable (default false)
		false,  --(boolean) outline (default to false)
		"SpectralSC-SemiBold"  --(string) name of font on client (optional)
	)

	local tooltipAdd = 0

	if (data.timer) then
		dynamicWindow:AddLabel(
			228,  --(number) x position in pixels on the window
			187,  --(number) y position in pixels on the window
			data.timer,  --(string) text in the label
			600,  --(number) width of the text for wrapping purposes (defaults to width of text)
			0,  --(number) height of the label (defaults to unlimited, text is not clipped)
			24,  --(number) font size (default specific to client)
			"center",  --(string) alignment "left", "center", or "right" (default "left")
			false,  --(boolean) scrollable (default false)
			false,  --(boolean) outline (default to false)
			"SpectralSC-SemiBold"  --(string) name of font on client (optional)
		)

		tooltipAdd = 54
	end

	dynamicWindow:AddLabel(
		228,  --(number) x position in pixels on the window
		186 + tooltipAdd,  --(number) y position in pixels on the window
		data.tooltip,  --(string) text in the label
		500,  --(number) width of the text for wrapping purposes (defaults to width of text)
		100,  --(number) height of the label (defaults to unlimited, text is not clipped)
		24,  --(number) font size (default specific to client)
		"center",  --(string) alignment "left", "center", or "right" (default "left")
		false,  --(boolean) scrollable (default false)
		false,  --(boolean) outline (default to false)
		"SpectralSC-SemiBold"  --(string) name of font on client (optional)
	)

	if ( data.cancelable ) then
		dynamicWindow:AddButton(
			140, --(number) x position in pixels on the window
			236, --(number) y position in pixels on the window
			"Cancel", --(string) return id used in the DynamicWindowResponse event
			"", --(string) text in the button (defaults to empty string)
			180, --(number) width of the button (defaults to width of text)
			50,--(number) height of the button (default decided by type of button)
			"", --(string) mouseover tooltip for the button (default blank)
			"", --(string) server command to send on button click (default to none)
			true, --(boolean) should the window close when this button is clicked? (default true)
			"DeathButton" --(string) button type (default "Default")
			--buttonState --(string) button state (optional, valid options are default,pressed,disabled)
			--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
		)

		dynamicWindow:AddLabel(
			230,  --(number) x position in pixels on the window
			252,  --(number) y position in pixels on the window
			"Decline",  --(string) text in the label
			500,  --(number) width of the text for wrapping purposes (defaults to width of text)
			100,  --(number) height of the label (defaults to unlimited, text is not clipped)
			28,  --(number) font size (default specific to client)
			"center",  --(string) alignment "left", "center", or "right" (default "left")
			false,  --(boolean) scrollable (default false)
			false,  --(boolean) outline (default to false)
			"SpectralSC-SemiBold"  --(string) name of font on client (optional)
		)
	end
	
    RegisterSingleEventHandler(EventType.DynamicWindowResponse, "ResurrectDialog", function(user, returnId)
		if ( user == player ) then
            if ( returnId == "Accept" and not player:HasTimer("RecentlyReleased") ) then
                data.cb(true)
            else
                -- res not accepted while near their corpse
                --- or the player released recently
                ---- re-open the self res/release dialog
				if ( player:GetSharedObjectProperty("IsDead") == true ) then
					PlayerCorpseReleaseDialog(player)
				else
					local corpse = player:GetObjVar("CorpseObject")
					if ( corpse and corpse:IsValid() ) then
						corpse:SendMessage("SelfResDialog")
					end
                end
                data.cb(false)
			end
		end
	end)
	
	player:OpenDynamicWindow(dynamicWindow, data.response)

end

--- Closes the dialog used to offer the player a choice of being resurrected, be it from an other player, their own body, or a resurrect shrine.
function CloseResurrectDialog( player )
	player:CloseDynamicWindow("ResurrectDialog")
end

--- Ask/force a player to corpse resurrect, func will be called to perform the res, this is a UI shell
-- @param player
-- @param resurrector
-- @param force
-- @param func
-- @param response object (same luaVM context as caller)
function CorpseResurrectDialog( player, resurrector, force, func, response )
	if not( func ) then func = function() end end
	
	if ( force ) then
		func()
		return
	end

	local name = nil
	if ( resurrector ) then name = resurrector:GetCharacterName() end

	local text
	if ( name ) then
		text = name .. " wants to resurrect your corpse."
	else
		text = "Resurrect from your corpse."
	end

	-- ask them if they want to be resurrected at their corpse
	PlayerResurrectDialog(player, {
		text = "Resurrect",
		tooltip = text,
		cancelable = true,
		symbol = "Death_SymbolRevive",
		cb = function(accept)
			if ( accept ) then func() end
		end,
		response = response or player,
	})
end

-- corpse should only be passed when the resurrector is resurrecting a corpse vs resurrecting a ghost.
-- @param player
-- @param resurrector
-- @param corpse - if explicity false will skip old corpse updates
function PlayerResurrect( player, resurrector, corpse, statPercent )
	--DebugMessage( "Resurrector: " .. tostring(resurrector) )
	CloseResurrectDialog(player)
    if not( IsDead(player) ) then return end
    
    -- tag them as having been resurrected recently
    player:ScheduleTimerDelay(TimeSpan.FromSeconds(5), "RecentlyResurrected")

	local livingScale = player:GetObjVar("LivingScale")
	if ( livingScale ) then
		player:SetScale(livingScale)
		player:DelObjVar("LivingScale")
	end
	
	if ( corpse ~= nil ) then
		-- the player was resurrected from their corpse, so..

		-- let's validate the corpse.
		if ( corpse ~= player:GetObjVar("CorpseObject") ) then
			if ( resurrector:IsPlayer() ) then
				resurrector:SystemMessage("Invalid Corpse.", "info")
			end
			return
		end

        player:SetMobileFrozen(true,true)
		PlayerCorpseResurrect(player, corpse, resurrector, statPercent)

	else
		-- player was resurrected from their ghost.
	    player:SetMobileFrozen(true,true)
		player:PlayEffect("ResurrectEffect", 1.5)

		local oldCorpse = player:GetObjVar("CorpseObject")
		if ( oldCorpse ~= nil ) then oldCorpse:SendMessageGlobal("Resurrected") end

		-- Player resurrected themselves at a shrine
		if( resurrector == player ) then
			SafeAdjustCurVitality(ServerSettings.Vitality.AdjustOnGhostResurrect, player)
		-- Player ghost was ressurected by another player
		elseif( player:HasObjVar("IsGhost") ) then
			SafeAdjustCurVitality(ServerSettings.Vitality.AdjustOnGhostResurrect, player)
		end
		
		PlayerDeathEnd(player, statPercent)
	end
end

function PlayerCorpseResurrect( player, corpse, resurrector, statPercent )
	if ( player:HasTimer("Resurrecting") ) then
		if ( resurrector and player ~= resurrector and resurrector:IsPlayer() ) then
			resurrector:SystemMessage("They are already being resurrected.", "info")
		end
		return
	end
	player:ScheduleTimerDelay(TimeSpan.FromSeconds(2.5), "Resurrecting")
	-- play resurrect effect
	corpse:PlayEffect("ResurrectEffect")

	-- leave astral plane if in it (early so other players can see where we are about to res at)
	AstralPlane.Leave(player)

	CallFunctionDelayed(TimeSpan.FromSeconds(2), function()

		local fin = function(backpack)
			-- make corpse stand up
			corpse:SetSharedObjectProperty("IsDead", false)
			-- move the player to their res location.
			player:SetWorldPosition(
				(resurrector and resurrector:IsValid()) and resurrector:GetLoc() or player:GetLoc()
            )
            
            -- give back criminal stat loss if taken and available to give back
            local criminalSkills = corpse:GetObjVar("CriminalSkillData")
            if ( criminalSkills ~= nil  ) then
                local skillChanges, skillDictionary = SanitizeCriminalStats(player, criminalSkills)
                ApplySanitizedCriminalStats(player, criminalSkills, skillChanges, skillDictionary)
            end

			-- move equipment back if it was taken
			if ( ServerSettings.PlayerInteractions.FullItemDropOnDeath ) then
				local slot, item
				for i=1,#SLOTS do
					slot = SLOTS[i]
					if ( slot ~= "Familiar" ) then
						item = corpse:GetEquippedObject(slot)
						if ( item ~= nil ) then
							player:EquipObject(item)
						end
					end
				end
			end

			-- move the rest of the corpses contents back into their backpack
			local corpseObjects = corpse:GetContainedObjects()
			local slot, object
			for i=1,#corpseObjects do
				object = corpseObjects[i]
				slot = GetEquipSlot(object)
				if ( slot == nil or (slot ~= "TempPack" and slot ~= "Bank" and not slot:match("BodyPart")) ) then
					object:MoveToContainer(backpack,object:GetLoc())
				end
			end
	
			-- destroy the corpse next frame (after moving items over)
			OnNextFrame(function()
				corpse:Destroy()
				-- Player resurrected themselves at their corpse
				if( resurrector == player ) then
					SafeAdjustCurVitality(ServerSettings.Vitality.AdjustOnSelfCorpseResurrect, player)
				-- Player ghost was ressurected by another player
				else
					SafeAdjustCurVitality(ServerSettings.Vitality.AdjustOnCorpseResurrect, player)
				end
				PlayerDeathEnd(player, statPercent)
			end)
		end
		
		-- ensure a backpack and create one if not
		local backpack = player:GetEquippedObject("Backpack")
		if ( backpack == nil or not backpack:IsValid() ) then
			-- no backpack/invalid backpack, create them one now.
			Create.Equipped("backpack", player, function(bpack)
				if ( bpack and bpack:IsValid() ) then
					-- name the backpack proper
					RenameBackpack(player, bpack)
					fin(bpack)
				else
					-- failed to create a new backpack, put them back in astral plane and inform
					AstralPlane.Join(player)
					player:SystemMessage("Resurrect: Internal Server Error", "info")
				end
			end, true)
		else
			fin(backpack)
		end
		
	end)
end

function PlayerDeathEnd(player, statPercent)
	AstralPlane.Leave(player) -- leave again here since we aren't always corpse resurrected.
	player:StopEffect("GrayScreenEffect")
	
	if ( player:HasObjVar("IsGhost") ) then
		-- stop looking like a ghost
		PlayerChangeBackFromGhost(player)
	else
		-- stop looking like a corpse
		if ( player:GetSharedObjectProperty("IsDead") == true ) then
			player:SetSharedObjectProperty("IsDead", false)
			CloseResurrectDialog(player)
		end
	end

	SetMobileMod(player, "VitalityRegenPlus", "Death", nil)

	player:DelObjVar("DeathShrineClear") -- just in case remove this as well.
	player:DelObjVar("CorpseObject")
	player:DelObjVar("CorpseAddress")
	player:DelObjVar("CorpseWorld")
	player:DelObjVar("CorpseLoc")
	player:DelObjVar("CorpseSewerSubregion")
	player:FireTimer("UpdateMapMarkers")

	-- on player resurrection, 'clear' weapon ability cooldowns
	StartWeaponAbilityCooldown(player, true, TimeSpan.FromSeconds(10))
	StartWeaponAbilityCooldown(player, false, TimeSpan.FromSeconds(10))

	-- call the end death for all mobiles
	DeathEndAll(player, statPercent)
	
	-- reapply ownership to all living pets
	local pets = player:GetObjVar("LivingPets") or {}
	local pet
	for i=1,#pets do
		pet = pets[i]
		if ( pet and pet:IsValid() and pet:HasModule("pet_temp_release") ) then
			pet:SendMessage("ResetOwner")
		end
	end
	player:DelObjVar("LivingPets")

	Militia.ApplyBuffs(player)

end

function UpdateCorpseOnPlayerResurrected(corpseObj)
    if ( corpseObj and corpseObj:IsValid() ) then

		-- destroy 'appearance' items if not full loot
		if not( ServerSettings.PlayerInteractions.FullItemDropOnDeath ) then
			local obj
			for i=1,#VisualDeathSlots do
				obj = corpseObj:GetEquippedObject(VisualDeathSlots[i])
				if ( obj ~= nil ) then obj:Destroy() end
			end
		end

		-- disallow resing
		corpseObj:SetObjVar("BeenResurrected", true)
		-- turn the corpse into a skeleton
		corpseObj:SetAppearanceFromTemplate("skeleton")

		return true
	end
	return false
end

function PlayerCorpseCreate(player, cb)
	if not( cb ) then cb = function(corpse) end end
	if not( player ) then
		LuaDebugCallStack("[PlayerCorpseCreate] player not provided.")
		return cb(nil)
	end

	-- clear the conflict table
	SetAllRelationsGuardIgnore(player)

	-- create their corpse
	local template = "player_corpse"
	local templateData = GetTemplateData(template)

	templateData.ClientId = player:GetIconId()
	templateData.ScaleModifier = player:GetScale().X
	templateData.Color = player:GetColor()
	templateData.Hue = player:GetHue()

	local name = StripColorFromString(player:GetName())
	templateData.Name = "" -- set empty since name will be from corpse name object

	if not( templateData.ObjectVariables ) then templateData.ObjectVariables = {} end
	
	if ( not ServerSettings.PlayerInteractions.FullItemDropOnDeath or IsInitiate(player) ) then
		templateData.ObjectVariables.NoDisrobe = true
	end

	templateData.ObjectVariables.PlayerId = player.Id -- create custom and template data doesn't set GameObj values, so id works.
	templateData.ObjectVariables.PlayerName = player:GetName()
	templateData.ObjectVariables.IsDead = true
    templateData.ObjectVariables.MurderCount = player:GetObjVar("MurderCount") or 0
    if ( player:HasObjVar("IsCriminal") ) then templateData.ObjectVariables.IsCriminal = true end
	templateData.ObjectVariables.Militia = Militia.GetId(player)
	templateData.ObjectVariables.GroupConsent = player:GetObjVar("GroupConsent")
	templateData.ObjectVariables.Guild = player:GetObjVar("Guild")

	if not( templateData.SharedObjectProperties ) then
		templateData.SharedObjectProperties = {}
	end

	templateData.SharedObjectProperties.IsDead = true
    templateData.SharedObjectProperties.DefaultInteraction = "Open Pack"
    
    -- punish them for being a naughty player
    local criminalSkills = CriminalStatLoss(player)

	local loc = player:GetLoc()
	Create.Custom.AtLoc(template, templateData, loc, function(corpse)
		if ( corpse ~= nil ) then
            if ( criminalSkills ~= nil ) then -- cannot set this from custom template data
                corpse:SetObjVar("CriminalSkillData", criminalSkills)
            end
			corpse:SetFacing(player:GetFacing())
			corpse:SetMobileFrozen(true,true)
			corpse:ScheduleDecay(ServerSettings.Death.CorpseDecay)
			player:SetObjVar("CorpseObject", corpse)
			player:SetObjVar("CorpseAddress", ServerSettings.RegionAddress)
			if not( IsDungeonMap() ) then
				player:SetObjVar("CorpseWorld", ServerSettings.WorldName)
				player:SetObjVar("CorpseLoc", loc)
			else
				if ( IsSewerDungeon() ) then -- allow placing the corpse icon on top of sewer dungon entrances
					player:SetObjVar("CorpseSewerSubregion", ServerSettings.SubregionName)
				end
			end
			-- freeze the current conflict table on the corpse
			FreezeConflictTable(player, corpse)

			-- replicate the hair
			local hair = player:GetEquippedObject("BodyPartHair")
			if ( hair ~= nil ) then
				Create.Equipped(hair:GetCreationTemplateId(), corpse, function(part)
					if ( part ) then part:SetHue(hair:GetHue()) end
				end, true)
			end
			local facialHair = player:GetEquippedObject("BodyPartFacialHair")
			if ( facialHair ~= nil ) then
				Create.Equipped(facialHair:GetCreationTemplateId(), corpse, function(part)
					if ( part ) then part:SetHue(facialHair:GetHue()) end
				end, true)
			end
		end
		cb(corpse)
	end, true)

end

function PlayerTurnIntoGhost(player, hueEquipment)
	local GHOST_HUE = "0xC100FFFF"

	local livingScale = player:GetObjVar("LivingScale")
	if ( livingScale ) then
		player:SetScale(livingScale)
		player:DelObjVar("LivingScale")
	end

	player:SetSharedObjectProperty("IsDead", false)

	AstralPlane.Join(player)

	-- make them look like a ghost
	local hueTable = {}
	hueTable.Self = player:GetColor()
	player:SetColor(GHOST_HUE)

	-- make their clothes look like a ghost
	local slot, obj
	local len = hueEquipment and #VisualDeathSlots or 2
	for i=1,len do
		slot = VisualDeathSlots[i]
		obj = player:GetEquippedObject(slot)
		if ( obj ~= nil ) then
			hueTable[slot] = obj:GetColor()
			obj:SetColor(GHOST_HUE)
		end
	end

	player:SetObjVar("OldHues", hueTable)
	player:SetObjVar("IsGhost", true)
	player:SetObjVar("IsDead", true)
	UpdatePlayerWeaponAbilities(player)
end

function PlayerChangeBackFromGhost(player)
	local hueTable = player:GetObjVar("OldHues")
	if ( hueTable ~= nil ) then
		local obj
		for slot,value in pairs(hueTable) do
			if ( slot == "Self" ) then
				player:SetColor(value)
			else
				obj = player:GetEquippedObject(slot)
				if ( obj ~= nil ) then obj:SetColor(value) end
			end
		end
		player:DelObjVar("OldHues")
	end
	player:DelObjVar("IsGhost")
	-- stop being treated like a ghost
	player:DelObjVar("IsDead")
	UpdatePlayerWeaponAbilities(player)
end

function PlayerTransferContentsToCorpse(player, corpse)

	-- move over all equipped items to the corpse, skipping blessed
	local slot, obj
	for i=1,#SLOTS do
		slot = SLOTS[i]
		obj = player:GetEquippedObject(slot)
		if ( obj and obj:IsValid() and not obj:HasObjVar("Blessed") ) then
			corpse:EquipObjectWithLoc(obj, GetRandomDropPosition(corpse))
		end
	end

	local backpack = player:GetEquippedObject("Backpack")
	-- transfer all their items over to the corpse (except blessed) and move blessed items to top level of backpack.
	if ( backpack ~= nil ) then
		local corpseItems = {}
		local blessed = false
		ForEachItemInContainerRecursive(backpack, function(item)
			blessed = item:HasObjVar("Blessed")
			-- item is in top level of backpack
			if ( item:ContainedBy() == backpack ) then
				-- and item is not blessed (nothing must be done with blessed items already in top level of backpack)
				if not( blessed ) then
					-- move to corpse (eventually)
					corpseItems[#corpseItems+1] = item
				end
			else
			-- item is not in top level of backpack (sub container)
				if ( blessed ) then
					-- move to top level in backpack, item is blessed.
					item:MoveToContainer(backpack, item:GetLoc())
				end
			end
			return true -- keep going
		end)
		-- move top level non-blessed items to the corpse
		for i=1,#corpseItems do
			corpseItems[i]:MoveToContainer(corpse, corpseItems[i]:GetLoc())
		end
	end
end

function CheckCorpseValid(player)
	if not( player:HasObjVar("CorpseObject") ) then return end

	local address = player:GetObjVar("CorpseAddress")
	if ( address == ServerSettings.RegionAddress ) then
		local corpse = player:GetObjVar("CorpseObject")
		if ( corpse == nil or not corpse:IsValid() ) then
			-- corpse must of decayed.

			-- close the res dialog
			CloseResurrectDialog(player)

			-- clean up objvars
			player:DelObjVar("CorpseObject")
			player:DelObjVar("CorpseAddress")
			player:DelObjVar("CorpseWorld")
			player:DelObjVar("CorpseLoc")
			player:DelObjVar("CorpseSewerSubregion")
		end
	end
end

--- Must be called from within the player's LuaVM
-- @param player
-- @param cb function(success)
function TeleportToClosestShrine(player, cb)
	if not( cb ) then cb = function(success) end end
	if ( IsDungeonMap() ) then
		-- look for a local one first
		local loc = GetClosestShrineTeleportLocation(player:GetLoc(), IsMurderer(player))
		if ( loc ) then
			-- move them to closest one
			return cb(player:SetWorldPosition(loc))
		end
		-- find the dungeon exit gameobj
		local exit = FindObject(SearchHasObjVar("DestinationSubregion"))
		if not( exit ) then
			-- cannot teleport out of a dungeon without an exit
			LuaDebugCallStack("[TeleportToClosestShrine] Failed to find dungeon exit.")
			return cb(false)
		end
		-- this is the reason SeedObjects for SewerDungeons have this objvar.
		local subregion = exit:GetObjVar("DestinationSubregion")
		-- they already need this objvar for their lua module to work
		local destination = exit:GetObjVar("Destination")

		-- early exit if we don't have all the proper data
		if not( subregion ) then
			LuaDebugCallStack("[TeleportToClosestShrine] Dungeon exit does not have 'DestinationSubregion'")
			return cb(false)
		end
		if not( destination ) then
			LuaDebugCallStack("[TeleportToClosestShrine] Dungeon exit does not have 'Destination'")
			return cb(false)
		end

		local universeName = GetUniverseName()

		-- determine region address from subregion name
		local address = nil
		for addr,info in pairs(GetClusterRegions()) do
			-- if we have a universe name then we can only go to regions with the same universe name
			local skip = false
			if(universeName) then
				local curUniverse = GetUniverseName(addr)
				if(universeName ~= curUniverse) then
					skip = true
				end
			end

			if ( not(skip) and info.SubregionName == subregion ) then
				address = addr
				break
			end
		end

		-- ensure an address was found
		if not( address ) then
			LuaDebugCallStack("[TeleportToClosestShrine] Failed to find address out of dungeon.")
			return cb(false)
		end
		
		if ( IsClusterRegionOnline(address) ) then
			-- ask the remote region for where to put them
			RegisterSingleEventHandler(EventType.Message, "ShrineTeleportLocationResponse", function(loc)
				-- teleport them to that region at this location
				cb(player:TransferRegionRequest(address, loc))
			end)
			MessageRemoteClusterController(address, "ShrineTeleportLocationRequest", player, destination, IsMurderer(player))
		else
			LuaDebugCallStack("[TeleportToClosestShrine] Region Offline.")
			cb(false)
		end
	else
		local loc = GetClosestShrineTeleportLocation(player:GetLoc(), IsMurderer(player))
		if ( loc ) then
			-- move them to closest one
			cb(player:SetWorldPosition(loc))
		else
			cb(false)
			LuaDebugCallStack("[TeleportToClosestShrine] failed to find location via GetClosestShrineTeleportLocation.")
		end
	end
end

--- Finds the closest resurrect shrine to a location on the running region.
-- @param loc
-- @param red - true if looking for red only res shrines
-- @return loc or nil
function GetClosestShrineTeleportLocation(loc, red)
	local shrines
	if ( red ) then
		shrines = FindObjects(SearchTemplate("shrine_red"))
	else
		shrines = FindObjects(SearchTemplate("shrine"))
		for i,s in pairs(FindObjects(SearchTemplate("shrine_healing"))) do
			shrines[#shrines+1] = s
		end
		-- failed to find any blue shrines, look for red shrines.
		if ( #shrines < 1 ) then
			shrines = FindObjects(SearchTemplate("shrine_red"))
		end
	end
	if ( #shrines > 0 ) then
		-- find closest one
		local dist, closest, d = loc:Distance(shrines[1]:GetLoc()), shrines[1], nil
		for i=2,#shrines do
			d = loc:Distance(shrines[i]:GetLoc())
			if ( d < dist ) then
				dist, closest = d, shrines[i]
			end
		end
		return closest:GetLoc():Project(closest:GetFacing(), -1.6)
	end
	return nil
end

-- this allows cross region resurrection by marking the player to be resurrected next time they are loaded from backup
function SetPlayerResurrectFromBackup(player, resurrector)
	-- mark res on backup load
	player:SetObjVar("ResurrectLoadedFromBackup", true)
	if ( resurrector ) then
		player:SetObjVar("ResurrectorLoadedFromBackup", resurrector)
	end
end

function PlayerResurrectFromBackup(player)
	if ( player:HasObjVar("ResurrectLoadedFromBackup") ) then
		player:ScheduleTimerDelay(TimeSpan.FromSeconds(5), "RecentlyReleased") -- naming is a little off but effect is the same
		local resurrector = player:GetObjVar("ResurrectorLoadedFromBackup")
		player:DelObjVar("ResurrectLoadedFromBackup")
		player:DelObjVar("ResurrectorLoadedFromBackup")
		local corpse = player:GetObjVar("CorpseObject")
		if ( corpse and corpse:IsValid() ) then
			PlayerResurrect(player, resurrector, corpse, ServerSettings.Death.CorpseResurrectStatPercent)
		else
			PlayerResurrect(player, resurrector, nil, ServerSettings.Death.CorpseResurrectStatPercent)
		end
		return true
	end
	return false
end

function DeathEndAll( mobile, statPercent )
	statPercent = statPercent or ServerSettings.Death.CorpseResurrectStatPercent
	
	SetCurHealth(mobile,math.floor(GetMaxHealth(mobile) * statPercent))
	SetCurStamina(mobile,math.floor(GetMaxStamina(mobile) * statPercent))
	SetCurMana(mobile,math.floor(GetMaxMana(mobile) * statPercent))
	

	SetMobileMod(mobile, "HealthRegenPlus", "Death", nil)
	SetMobileMod(mobile, "ManaRegenPlus","Death", nil)
	SetMobileMod(mobile, "StaminaRegenPlus","Death", nil)
	SetMobileMod(mobile, "BloodlustRegenPlus","Death", nil)

	mobile:SetMobileFrozen(false, false)
	mobile:DelObjVar("Disabled")
end