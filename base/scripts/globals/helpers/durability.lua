

function GetDurabilityValue(obj)
	if(obj == nil) then LuaDebugCallStack("nil obj provided to GetDurabilityValue") end
	return obj:GetObjVar("Durability") or GetMaxDurabilityValue(obj)
end

function GetMaxDurabilityValue(obj)
	if(obj == nil) then LuaDebugCallStack("nil obj provided to GetMaxDurabilityValue") end
	return obj:GetObjVar("MaxDurability") or ServerSettings.Durability.DefaultMax
end

function GetDurabilityType(obj)
	if ( IsPet(obj) and IsTamable(obj) ) then
		local owner = obj:GetObjVar("controller")
		return "tamable",owner
	else
		local owner = obj:TopmostContainer()
		return "item",owner
	end
end

--- Get the string value of an obj's durability
-- @param obj
-- @param current(optional) double
-- @param max(optional) double
-- @return string
function GetDurabilityString(obj, current, max, type)
	type = type or GetDurabilityType(obj)
	max = max or GetMaxDurabilityValue(obj)
	current = current or GetDurabilityValue(obj)
	current = math.min(current, max)

	--DebugMessage( " Durability: "..tostring(current).."/"..tostring(max) )


	local percent = current / max
	
	if ( type == "tamable" ) then
		if ( percent <= 0.20 ) then
			return "Exhausted"
		end
		if ( percent <= 0.40 ) then
			return "Tired"
		end
		if ( percent <= 0.60 ) then
			return "Weary"
		end
		if ( percent <= 0.80 ) then
			return "Winded"
		end
		if ( percent <= 1 ) then
			return "Fresh"
		end
	else
		return "Durability: "..tostring(current).."/"..tostring(max)
	end
end

--- Adjust the durability of an obj. If current + amount is less than 1 the obj will BREAK.
-- @param obj
-- @param amount double(optional) Amount to adjust by, defaults to 0
-- @param current(optional) double Current obj durability, will be read from object if not provided. 
-- @param max(optional) double obj's max durability, will be read from object if not provided.
function AdjustDurability(obj, amount, current, max)
	if ( obj == nil ) then
		LuaDebugCallStack("ERROR: nil obj provided.")
		return
	end

	local type, owner = GetDurabilityType(obj)
	
	if ( not owner ) then
		LuaDebugCallStack("obj owner is nil.")
		return false
	end
	if ( not type ) then
		LuaDebugCallStack("obj type is nil.")
		return false
	end

	-- Blessed objs cannot take damage (unless they are cursed)
	if ( obj:HasObjVar("Blessed") and not obj:HasObjVar("Cursed") ) then return end
	amount = amount or 0
	max = max or GetMaxDurabilityValue(obj)
	current = ( current or GetDurabilityValue(obj) ) + amount
	current = math.min(current, max)

	-- anything less than 1 BREAKS the obj!
	if ( current < 1 ) then
		BreakObject(obj, type, owner)
		return
	end

	if ( current >= max ) then
		-- when an obj has full durability, it doesn't need this objvar.
		obj:DelObjVar("Durability")
	else
		-- warn the player their obj is about to break.
		if ( current <= ServerSettings.Durability.BreakWarnings ) then
			if ( owner:IsPlayer() and owner:IsValid() ) then
				if(type == "tamable") then
					owner:SystemMessage("Your " .. StripColorFromString(obj:GetName()) .." grows closer to withering away!", "event")
				else
					owner:SystemMessage("Your " .. StripColorFromString(obj:GetName()) .." is in danger of being destroyed!", "event")
				end
			end
		end
		obj:SetObjVar("Durability", current)
	end

	local durabilityString = GetDurabilityString(obj, current, max, type)
	-- if the durability string has changed
	if ( durabilityString ~= GetDurabilityString(obj, current - amount, max, type) ) then
		if ( durabilityString ) then
			-- update / add the string
			if ( type == "tamable" ) then
				obj:SetSharedObjectProperty("Title", GetOwnerTitleString(owner).." ("..durabilityString..")")
			else
				SetTooltipEntry(obj, "Durability", durabilityString, -99999)
			end
		else
			-- or remove the entry,
			if ( type == "tamable" ) then
				obj:SetSharedObjectProperty("Title", GetOwnerTitleString(owner))
			else
				RemoveTooltipEntry(obj, "Durability")
			end
		end
	end
end

--- Sets the maximum durability for an obj
-- @param obj
-- @param newMax
function SetMaxDurabilityValue(obj, newMax)
	if ( obj == nil ) then return LuaDebugCallStack("nil obj provided.") end

	obj:SetObjVar("MaxDurability", newMax)
	AdjustDurability(obj, nil, nil, newMax)
end

function BreakObject(obj, type, owner)
	if ( owner and owner:IsValid() and owner:IsPlayer() and type ) then
		if ( type == "item" ) then
			local weaponType = obj:GetObjVar("WeaponType")
			if ( EquipmentStats.BaseWeaponStats[weaponType] and EquipmentStats.BaseWeaponStats[weaponType].WeaponClass == "Tool" ) then
				obj:SendMessage("CancelHarvesting", owner)
			end
			
			-- do all the things that accompany unequipping an obj.
			local tempPack = owner:GetEquippedObject("TempPack")
			if ( tempPack ) then
				obj:MoveToContainer(tempPack, Loc(0,0,0))
			else
				obj:SetWorldPosition(owner:GetLoc())
			end

			owner:PlayObjectSound("event:/character/combat_abilities/plate_sunder")
			-- this is important enough to log it in the chat window and do an alert event
			owner:SystemMessage("Your " .. StripColorFromString(obj:GetName()) .." has been destroyed!", "event")
		elseif ( type == "tamable" ) then
			owner:PlayObjectSound("event:/magic/void/magic_void_cast_void", false, 3.0)
			PlayEffectAtLoc("RedCoreImpactWaveEffect",obj:GetLoc(),0.0,"Bone=Ground")
			PlayEffectAtLoc("EnergyExplosionEffect",obj:GetLoc(),0.0,"Bone=Ground")
			owner:SystemMessage("Your " .. StripColorFromString(obj:GetName()) .." is now at peace in the ether.", "event")
		end
	end

	obj:Destroy()
end