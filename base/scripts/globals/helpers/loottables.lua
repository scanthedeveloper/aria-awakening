LootTables = {}

-- LootTables is an array of loot tables
-- Each table can have the following values
--   NumCoins - The number of coins to spawn in the container
-- NOTE: itemCreatedOverride should still call LootItemCreated to set stack, color etc
function LootTables.SpawnLoot(lootTables,destContainer,objVars,autoStack,guardProtection,notify)
	--DebugMessage(1)
	-- fill the mob's backpack
	if( lootTables ~= nil ) then
		local topCont = destContainer:TopmostContainer()

		-- if this mob has money
		--D*ebugTable(curLootTable)
		--DebugMessage(2)

		-- add up all the coins
		local totalCoins = 0
		for key, subTable in pairs(lootTables) do
			local numCoins = subTable.NumCoins or 0
			if(subTable.NumCoinsMin or subTable.NumCoinsMax) then
				numCoins = math.random((subTable.NumCoinsMin or 0),(subTable.NumCoinsMax or 0))
			end
			totalCoins = totalCoins + numCoins
		end
		if(totalCoins > 0) then
			if(guardProtection == "None") then
				totalCoins = math.ceil(totalCoins * ServerSettings.Misc.WildernessModifiers.Gold)
			end
			local dropPos = GetRandomDropPosition(destContainer)
			RegisterSingleEventHandler(EventType.CreatedObject, "LootCoinPurse", LootCoinPurseCreated)

			if( topCont and topCont:IsMobile() and topCont:HasObjVar("TagKillers") ) then
				local killers = topCont:GetObjVar("TagKillers")

				if( killers[1] ) then
					RequestMobileMod( topCont, killers[1], {"GoldIncreaseTimes"},
					function(MobileMod)
						--DebugMessage("TotalCoins: " .. totalCoins)
						totalCoins = math.floor( totalCoins * ( 1 + (GetMobileMod(MobileMod.GoldIncreaseTimes, 0) / 100) ) )
						--DebugMessage("TotalCoins: " .. totalCoins)
						CreateObjInContainer("coin_purse", destContainer, dropPos, "LootCoinPurse", totalCoins,objVars)
						LoottablesNotify(notify, topCont, "coin_purse", totalCoins)
					end)
				else
					CreateObjInContainer("coin_purse", destContainer, dropPos, "LootCoinPurse", totalCoins,objVars)
					LoottablesNotify(notify, topCont, "coin_purse", totalCoins)
				end

			else
				CreateObjInContainer("coin_purse", destContainer, dropPos, "LootCoinPurse", totalCoins,objVars)
				LoottablesNotify(notify, topCont, "coin_purse", totalCoins)
			end
			
		end

		-- Does the container have enchanted items that can drop?
		local lootLevel = nil

		-- Is this a container?
		if( destContainer ) then
			lootLevel = destContainer:GetObjVar("LootLevel")
		end

		-- Is this a mobile? 
		if( topCont ) then
			lootLevel = topCont:GetObjVar("LootLevel")
		end


		--DebugMessage("LootLevel", lootLevel)

		if( lootLevel ) then
			ProcessRandomEnchantLoot( lootLevel, destContainer )				
		end
		
		local itemsSpawned = {}
		for key, subTable in pairs(lootTables) do
			local skipLootTable = false
			if(subTable.Chance) then
				skipLootTable = not(Success(subTable.Chance/100))
			end
			--DebugMessage(3)
			-- if the mob has items
			if( not(skipLootTable) and subTable.LootItems ~= nil ) then
				local itemCount = subTable.NumItems or #subTable.LootItems
				if(subTable.NumItemsMin or subTable.NumItemsMax) then
					itemCount = math.random((subTable.NumItemsMin or 0),(subTable.NumItemsMax or 0))
				end
				--DebugMessage("MIN: "..subTable.MinItems..", MAX: "..subTable.MaxItems..", COUNT: "..itemCount)
				if(itemCount > 0) then
					--DebugMessage(DumpTable(subTable.LootItems))
					local multiplier = 1
					if(guardProtection == "None") then
						multiplier = ServerSettings.Misc.WildernessModifiers.LootRoll
					end
					local availableItems = FilterLootItemsByChance(subTable.LootItems,multiplier)

					--DebugMessage(DumpTable(availableItems))

					for i=1,itemCount do
						if( availableItems and next(availableItems) ) then
							local itemIndex = GetRandomLootItemIndex(availableItems)
							local itemTemplateId = availableItems[itemIndex].Template
							local color = availableItems[itemIndex].Color
							local stackCount = availableItems[itemIndex].StackCount or 1
							if(availableItems[itemIndex].StackCountMin or availableItems[itemIndex].StackCountMax) then
								stackCount = math.random((availableItems[itemIndex].StackCountMin or 0),(availableItems[itemIndex].StackCountMax or 0))
							end
							--DebugMessage("itemIndex is "..tostring(itemIndex))
							--DebugMessage(tostring(availableItems[itemIndex]).." is bleh")
							--DebugMessage(tostring(availableItems[itemIndex].Templates).." is narm")
							-- if its unique then remove it from the list

							if (availableItems[itemIndex].Templates ~= nil) then
								for index,template in pairs(availableItems[itemIndex].Templates) do

									local dropPos = GetRandomDropPosition(destContainer)
									--DebugMessage("itemTemplateId: "..(tostring(template)))
									CreateObjInContainer(template, destContainer, dropPos, "LootObject",stackCount,color,callback,objVars)
									LoottablesNotify(notify, topCont, template, stackCount)
									table.insert(itemsSpawned, template)
								end

							else								
								local dropPos = GetRandomDropPosition(destContainer)

								if ( availableItems[itemIndex].Packed ) then
									Create.PackedObject.InContainer(itemTemplateId, destContainer, dropPos)
									LoottablesNotify(notify, topCont, itemTemplateId, 1)
									table.insert(itemsSpawned, itemTemplateId)
								else
									local shouldCreate = true
									if(autoStack) then
										local resourceType = GetTemplateObjVar(itemTemplateId, "ResourceType")
										shouldCreate = not(TryAddToStack(resourceType, destContainer, stackCount) )
									end

									if(shouldCreate) then
										CreateObjInContainer(itemTemplateId, destContainer, dropPos, "LootObject",stackCount,color,objVars)
									end

									LoottablesNotify(notify, topCont, itemTemplateId, stackCount)
									table.insert(itemsSpawned, itemTemplateId)
								end

								if( availableItems[itemIndex].Unique == true ) then								
									table.remove(availableItems,itemIndex)
								end
							end
							--itemsSpawned = itemsSpawned + 1
						end
					end					
				end
			end
		end

		if(#itemsSpawned > 0) then
			-- NOTE: This never gets unregistered
			RegisterEventHandler(EventType.CreatedObject, "LootObject", LootItemCreated)

			CallFunctionDelayed(TimeSpan.FromMilliseconds(1),function()
				--move items inside pouch
				if ( destContainer:IsMobile() and IsDead(destContainer) ) then
					local pouchObj = FindObjectInContainer(destContainer, "pouch")
					if ( pouchObj ) then
						TransferContainerContents(destContainer, pouchObj)
					end
				end
			end)
		end

		return itemsSpawned
	end
end

function LoottablesNotify(notify, playerObj, template, count)
	if ( not notify or notify ~= true or not template or not playerObj ) then return false end
	if ( not playerObj:IsPlayer() ) then return false end
	local templateName = GetTemplateObjectName(template) or "an unknown object"
	if ( templateName == "Coin Purse" ) then templateName = "coins" end
	local count = count or 1
	local message = "Received "..templateName
	if ( count > 1 ) then
		message = message.."("..count..")"
	end
	playerObj:SystemMessage(message,"info")
	playerObj:SystemMessage(message)
end

function DistributeBossRewards(nearbyCombatants, lootTables, achievementName, isQuiet, guardProtection, maxCount, customFunc)
	local maxCount = maxCount or 25

	--Shuffle list of players
	for i = #nearbyCombatants, 1, -1 do
		local rand = math.random(i)
		nearbyCombatants[i], nearbyCombatants[rand] = nearbyCombatants[rand], nearbyCombatants[i]
	end

	--Get 25 players to reward
	local playersToReward = {}
	counter = 1
	for index, player in pairs(nearbyCombatants) do
		if (player:IsPlayer() and counter < maxCount) then
			playersToReward[counter] = player
			counter = counter + 1
		end
	end

	for index,player in pairs(playersToReward) do
        local skipPlayer = false
        if (IsDead(player)) then skipPlayer = true end

        if not (skipPlayer) then

        	if (achievementName ~= nil) then
        		CheckAchievementStatus(player, "BossKills", achievementName, 1)
        	end

        	if (lootTables == nil) then
        		return
        	end

            local backpackObj = player:GetEquippedObject("Backpack")
            if (backpackObj ~= nil) then
	            local itemsSpawned = LootTables.SpawnLoot(lootTables, backpackObj, nil, true, guardProtection)

	            if not(isQuiet) then
	            	if(itemsSpawned and #itemsSpawned > 0) then
	            		backpackObj:SendOpenContainer(player)
	            		player:SystemMessage("You have been rewarded.","info")			            	        	
			        end
		        end

		        if(customFunc) then
		        	customFunc(player,itemsSpawned)
		        end
	        end
        end
    end
end

function LootItemCreated(success,objref,stackCount,color,objVars)
	if (color ~= nil) then
		objref:SetColor(color)
	end
	if (stackCount>1) then
		RequestSetStack(objref,stackCount)
	end

	SetItemTooltip(objref)

	if (objVars ~= nil) then
		for i, j in pairs(objVars) do
			objref:SetObjVar(i, j)
		end
	end
end

function LootCoinPurseCreated(success,objRef,numCoins,objVars)
	if( success == true ) then
		-- fill the mob's backpack
		if( numCoins ~= nil and numCoins > 0 ) then			
			objRef:SendMessage("AdjustStack",numCoins)
		end
	end
end

-- Takes the "RandomEnchantedLoot" objvar from the mobile that was killed and generates loot based on it.
function ProcessRandomEnchantLoot( lootLevel, containerObj )
	if( lootLevel and ServerSettings.EnchantedLoot.PremadeLootLevels[lootLevel] ) then
		
		local loot = { ServerSettings.EnchantedLoot.PremadeLootLevels[lootLevel] }

		for i=1, #loot do 
			local level = loot[i].Level or 1
			local chance = loot[i].Chance or ServerSettings.EnchantedLoot.QualityDropChance[level]
			local enchantCount = loot[i].EnchantCountMax or ServerSettings.EnchantedLoot.QualityEnchantCount[level]
			
			local type = loot[i].Type or nil
			local minItems = loot[i].MinItems or ServerSettings.EnchantedLoot.MinimumItems
			local itemsDropped = 0

			--DebugMessage("Chance",chance,"Level",level,"Type",type)

			if( type ) then 
				-- We need to force items to drop
				if( minItems > 0 ) then

					for k=1, minItems do
						local templateTypes = type[ math.random( 1, #type) ] or "FighterArmor"
						local itemTemplate = ServerSettings.EnchantedLoot.ItemTypeTemplates[templateTypes][math.random(1, #ServerSettings.EnchantedLoot.ItemTypeTemplates[templateTypes]) ]
						local numEnchants = math.random( 1, enchantCount )
						CreateRandomEnchantLootItem( itemTemplate, level, numEnchants, containerObj, function( createdItem ) 
						end)
					end
				else
					-- Ready to generate loot
					if( Success( chance ) ) then
						local templateTypes = type[ math.random( 1, #type) ] or "FighterArmor"
						local itemTemplate = ServerSettings.EnchantedLoot.ItemTypeTemplates[templateTypes][math.random(1, #ServerSettings.EnchantedLoot.ItemTypeTemplates[templateTypes]) ]
						local numEnchants = math.random( 1, enchantCount )
						CreateRandomEnchantLootItem( itemTemplate, level, numEnchants, containerObj, function( createdItem ) 
						end)
					end
				end
			end			
		end
	end
end

function CreateRandomEnchantLootItem( template, level, enchantCount, container, cb )
	level = level or 1
	color = ServerSettings.EnchantedLoot.QualityNameColors[level]
	enchantCount = enchantCount or ServerSettings.EnchantedLoot.QualityEnchantCount[level]
	cb = cb or function()end

	Create.InContainer( template, container, nil, function( createdObj ) 
		--DebugMessage("ILevel",level)
		EnchantingHelper.ApplyRandomEnchantsToItem( createdObj, nil, nil, true, level, enchantCount )
		local name,trash = StripColorFromString( createdObj:GetName() )

		createdObj:SetName("["..color.."]"..name.."[-]")

		cb( createdObj )
	end)
end