-- Init this helper
EnchantingHelper = {}

EnchantingHelper.RemoveMobileMods = function( mobile, enchants, mobileModId )
    if( mobile == nil or enchants == nil ) then return end

    -- Loop through enchants and remove any mobile mod ones
    for i=1, #enchants do
        if( EssenceData[enchants[i].essenceName] ~= nil and EssenceData[enchants[i].essenceName].Effect == "mobilemod" ) then
            SetMobileMod(mobile, EssenceData[enchants[i].essenceName].MobileMod, mobileModId, nil)
        end
    end

end

EnchantingHelper.RemoveCombatMods = function( mobile, enchants, mobileModId )
    if( mobile == nil or enchants == nil ) then return end

    -- Loop through enchants and remove any mobile mod ones
    for i=1, #enchants do
        if( EssenceData[enchants[i].essenceName] ~= nil and EssenceData[enchants[i].essenceName].Effect == "combatmod" ) then
            SetCombatMod(mobile, EssenceData[enchants[i].essenceName].MobileMod, mobileModId, nil)
        end
    end

end

EnchantingHelper.ApplyMobileMods = function( mobile, enchants, mobileModId )
    if( mobile == nil or enchants == nil ) then return end

    -- Loop through enchants and remove any mobile mod ones
    for i=1, #enchants do
        if( EssenceData[enchants[i].essenceName] ~= nil and EssenceData[enchants[i].essenceName].Effect == "mobilemod" ) then
            SetMobileMod(mobile, EssenceData[enchants[i].essenceName].MobileMod, mobileModId, enchants[i].essenceModifier)
        end
    end

end

EnchantingHelper.SearchAllEquipmentForEffect = function( effect, _weapon, _armor )
    
    local enchants = {}
    if( _weapon ~= nil and _armor ~= nil ) then
        
        -- loop through each weapon
        for k,v in pairs(_weapon) do
            -- loop through each enchant
            for i=1, #v.Enchants do 
                if( EssenceData[v.Enchants[i].essenceName] and EssenceData[v.Enchants[i].essenceName].Effect == effect ) then
                    table.insert(enchants, v.Enchants[i])
                end
            end
        end

        -- loop through each armor
        for k,v in pairs(_armor) do
            -- loop through each enchant
            for i=1, #v.Enchants do 
                if( EssenceData[v.Enchants[i].essenceName] and EssenceData[v.Enchants[i].essenceName].Effect == effect ) then
                    table.insert(enchants, v.Enchants[i])
                    
                end
            end
        end

    else
        DebugMessage("Trying to access enchants without proper scope. ["..effect.."]")
    end

    return enchants

end

EnchantingHelper.GetAllEquipmentEnchants = function( _weapon, _armor )
    
    local enchants = {}
    if( _weapon ~= nil and _armor ~= nil ) then
        
        -- loop through each weapon
        for k,v in pairs(_weapon) do
            -- loop through each enchant
            for i=1, #v.Enchants do 
                if( EssenceData[v.Enchants[i].essenceName] ) then
                    table.insert(enchants, EssenceData[v.Enchants[i].essenceName])
                end
            end
        end

        -- loop through each armor
        for k,v in pairs(_armor) do
            -- loop through each enchant
            for i=1, #v.Enchants do 
                if( EssenceData[v.Enchants[i].essenceName] ) then
                    table.insert(enchants, EssenceData[v.Enchants[i].essenceName])
                    
                end
            end
        end

    else
        DebugMessage("Trying to access enchants without proper scope. ["..effect.."]")
    end

    return enchants

end

EnchantingHelper.GetUniqueEquipmentEnchants = function( _weapon, _armor )
    
    local enchants = {}
    if( _weapon ~= nil and _armor ~= nil ) then
        
        -- loop through each weapon
        for k,v in pairs(_weapon) do
            -- loop through each enchant
            for i=1, #v.Enchants do 
                if( EssenceData[v.Enchants[i].essenceName] ) then
                    enchants[v.Enchants[i].essenceName] = EssenceData[v.Enchants[i].essenceName]
                end
            end
        end

        -- loop through each armor
        for k,v in pairs(_armor) do
            -- loop through each enchant
            for i=1, #v.Enchants do 
                if( EssenceData[v.Enchants[i].essenceName] ) then
                    enchants[v.Enchants[i].essenceName] = EssenceData[v.Enchants[i].essenceName]
                end
            end
        end

    else
        DebugMessage("Trying to access enchants without proper scope. ["..effect.."]")
    end

    return enchants

end


-- Determines if an object is an enchanting essense
-- @param: obj - the object you want to check
-- @return: boolean - true if an essence; false if not
EnchantingHelper.IsEssence = function( obj )
    if( obj:HasObjVar("EssenceType") ) then
        return true
    else
        return false
    end
end

EnchantingHelper.IsEnchantable = function(obj)
    local numEnchants = obj:GetObjVar("Enchants")
    if( 
        obj == nil -- Does it exist?
        or obj:HasObjVar("Artifact") -- Is it an artifact?
        or (numEnchants ~= nil and #numEnchants >= EssenceData.MaximumEnchantsPerItem) -- Does it already have an enchant?
        --or obj:HasObjVar("SecondaryEssence") -- Does it already have an enchant?
        --or obj:HasObjVar("TertiaryEssence") -- Does it already have an enchant?
        or not ( obj:HasObjVar("WeaponType") or obj:HasObjVar("ArmorType") or obj:HasObjVar("ShieldType") ) -- Is it not a weapon or armor?
        or obj:GetObjVar("WeaponType") == "Torch" -- Is this a torch?
        or obj:GetObjVar("WeaponType") == "Spellbook" -- Is this a spellbook?
        or not obj:HasObjVar("Crafter") -- Is Crafted!
    ) then
        return false
    end

    -- We can enchant it!
    return true
end

EnchantingHelper.UpdateItemTooltip = function( obj )
    if(obj == nil) then return end
    SetItemTooltip(obj) 
end

EnchantingHelper.GetEnchantTooltips = function( obj )

    local enchants = Weapon.GetEnchants( obj )
    local string = ""
    if( enchants == nil ) then return string end

    for i=1, #enchants do
        if( enchants[i].essenceName ~= nil ) then
            if( i > 1 ) then
                string = string .. "\n"
            end

            string = string .. EnchantingHelper.GetEnchantTooltip(enchants[i].essenceName,enchants[i].essenceModifier)
        end
    end

    -- Return colorized tooltip string
    return "[FFE691]" .. string .. "[-]"
end

EnchantingHelper.GetEnchantTooltip = function( essenseType, modifier )
    if( essenseType == nil ) then return "" end

    local data = EssenceData[essenseType]
    local tooltip = {}
    if( data ) then
        
        if( data.TooltipRanges ) then
            local set = false
            for i=1, #data.TooltipRanges do 
                --DebugMessage("MAX: ", data.TooltipRanges[i].Max, " Modifier: ", modifier)
                if( not set and data.TooltipRanges[i].Max >= modifier ) then
                    table.insert(tooltip, data.TooltipRanges[i].Tooltip )
                    set = true
                end
            end

            if( not set ) then
                table.insert(tooltip, data.TooltipRanges[#data.TooltipRanges].Tooltip )
            end

        else
            table.insert(tooltip, tostring(modifier))
            if( data.EffectMode == "percent" ) then
                table.insert(tooltip, "%")
            else
                table.insert(tooltip, "+")
            end
            table.insert(tooltip, " ")
            table.insert(tooltip, data.EffectName)
        end

        

    end

    return table.concat(tooltip)
end


EnchantingHelper.GetItemNameFromArgs = function( itemObj, enchantPouchArgs )
    if( itemObj == nill or enchantPouchArgs == nil ) then return false end
    local templateData = GetTemplateData(itemObj:GetCreationTemplateId())
    local itemName = templateData.Name

    local name = {}

    -- Check to see if we have a prefix
    if( enchantPouchArgs.Prefix ~= nil ) then
        table.insert(name, enchantPouchArgs.Prefix) 
        table.insert(name, " ")
    end

    local itemName, color = StripColorFromString(itemName)
    -- Item name
    table.insert(name, itemName) 

    -- Check to see if we have a suffix
    if( enchantPouchArgs.Suffix ~= nil ) then
        table.insert(name, " ")
        table.insert(name, enchantPouchArgs.Suffix)
    end

    return table.concat(name)
end

-- Destroys all essences in the enchanting pouch
-- @param: essencePouch - the pouch containing the essences
EnchantingHelper.EssencePouchConsumeContents = function( essencePouch )
    local essences = essencePouch:GetContainedObjects()
    for index, essence in pairs(essences) do
        essence:Destroy()
    end
end

-- Gets the modifier string for essences
-- @param: enchantPouchArgs - values generated by CalculateEssencesInPouch
-- @param [optional]: primary - should return primary string
-- @return: string - string showing what modifiers will be applied
EnchantingHelper.GetEnchantModifierLabel = function( enchantPouchArgs, index )
    if( enchantPouchArgs == nil ) then return "" end
    local returnString = {}
    local modifierArray = nil
    local essenceName = nil

    local enchant = enchantPouchArgs.Enchants[index]
    if( enchant ~= nil ) then
        -- We have a different min and max
        if( enchant.minMax[1] ~= enchant.minMax[2] ) then 
            table.insert(returnString, tostring(enchant.minMax[1]))
            table.insert(returnString, "-")
            table.insert(returnString, tostring(enchant.minMax[2]))
        -- Min and max are the same, only need to show min
        else
            table.insert(returnString, tostring(enchant.minMax[1]))
        end
        if( EssenceData[enchant.essenceName].EffectMode == "percent" ) then
            table.insert(returnString, "%")
        else
            table.insert(returnString, "+")
        end
        table.insert(returnString, " ")
        table.insert(returnString, EssenceData[enchant.essenceName].EffectName)

    end

    
    return table.concat(returnString)
end

-- Determines if a essence can be applied to an item based on it's restrictions
-- @param: essenceObj - essence to be applied
-- @param: enchantObj - object to be enchanted
-- @return: boolean - can the object be enchanted
EnchantingHelper.CanEssenceBeAppliedToObject = function( essenceObj, enchantObj )
    if not( EnchantingHelper.IsEssence(essenceObj) ) then return false end
    if not( enchantObj ) then return false end
    local essenceName = essenceObj:GetObjVar("EssenceName") or nil

    if( EssenceData[essenceName].EssenceRestrictions ) then
        if( #EssenceData[essenceName].EssenceRestrictions == 0 ) then return true end

        for i=1, #EssenceData[essenceName].EssenceRestrictions do
            --DebugMessage("Class: " .. Weapon.GetClass(enchantObj))
            if( EssenceData[essenceName].EssenceRestrictions[i] == "weapon" and enchantObj:HasObjVar("WeaponType") )then return true end
            if( EssenceData[essenceName].EssenceRestrictions[i] == "melee" and enchantObj:HasObjVar("WeaponType") and Weapon.GetClass(enchantObj) ~= "Bow" )then return true end
            if( EssenceData[essenceName].EssenceRestrictions[i] == "bow" and Weapon.GetClass(Weapon.GetType(enchantObj)) == "Bow" )then return true end
            if( EssenceData[essenceName].EssenceRestrictions[i] == "armor" and enchantObj:HasObjVar("ArmorType") )then return true end
            if( EssenceData[essenceName].EssenceRestrictions[i] == "shield" and enchantObj:HasObjVar("ShieldType") )then return true end
        end
    end

    --DebugMessage(essenceObj:GetName() .. " cannot enchant ".. enchantObj:GetName())

    return false
end

EnchantingHelper.CanEnchantBeAppliedToObject = function( essenceName, enchantObj )
    if( not enchantObj or not essenceName ) then return false end
    if( EssenceData[essenceName].EssenceRestrictions ) then
        if( #EssenceData[essenceName].EssenceRestrictions == 0 ) then return true end

        for i=1, #EssenceData[essenceName].EssenceRestrictions do
            --DebugMessage("Class: " .. Weapon.GetClass(enchantObj))
            if( EssenceData[essenceName].EssenceRestrictions[i] == "weapon" and enchantObj:HasObjVar("WeaponType") )then return true end
            if( EssenceData[essenceName].EssenceRestrictions[i] == "melee" and enchantObj:HasObjVar("WeaponType") and Weapon.GetClass(enchantObj) ~= "Bow" )then return true end
            if( EssenceData[essenceName].EssenceRestrictions[i] == "bow" and Weapon.GetClass(Weapon.GetType(enchantObj)) == "Bow" )then return true end
            if( EssenceData[essenceName].EssenceRestrictions[i] == "armor" and enchantObj:HasObjVar("ArmorType") )then return true end
            if( EssenceData[essenceName].EssenceRestrictions[i] == "shield" and enchantObj:HasObjVar("ShieldType") )then return true end
        end
    end

    return false
end


EnchantingHelper.ItemAlreadyHasEnchant = function( essenceName, enchantObj )
    -- We want to stay it has it if the item or essece cannot be found!
    if( essenceName == nil or enchantObj == nil ) then return true end

    local enchants = enchantObj:GetObjVar("Enchants") or {}
    for i=1, #enchants do 
        if( enchants[i].essenceName == essenceName ) then return true end
    end

    return false

end

EnchantingHelper.CountEnchantsOnItem = function( enchantObj )
    local enchants = enchantObj:GetObjVar("Enchants") or {}
    --DebugMessage( "Enchants Count: " .. tostring(#enchants) )
    return #enchants 
end

EnchantingHelper.CanEssenceBeUsedWithPouchContents = function( essenceObj, essensePouch )
    if( essenceObj == nil or essensePouch == nil ) then return false end
    
    local essences = essensePouch:GetContainedObjects()
    local countUnique = 0
    local essenceList = {}
    local mEssenceName = essenceObj:GetObjVar("EssenceName")
    local enchantObj = essensePouch:GetObjVar("enchantItem")

    -- Make sure this essence isn't already applied to the item.
    if( EnchantingHelper.ItemAlreadyHasEnchant( mEssenceName, enchantObj ) )then return false end

    -- Get each essence in the pouch and add it to the essenceList and up the count
    for index, essence in pairs(essences) do
        local essenceName = essence:GetObjVar("EssenceName")

        if( essenceName == mEssenceName ) then return false end
        
        if not ( essenceList[essenceName] ) then
            essenceList[essenceName] = true
            countUnique = countUnique + 1
        end
    end

    -- If our essenceObj isn't a type in the essenceList and 
    -- we've already got two types of essences; return false
    if( essenceList[mEssenceName] == nil and countUnique >= EssenceData.MaximumUniqueEssences ) then return false end

    -- Else we are good to go!
    return true
end

-- Determines if an object is an enchanting essense
-- @param: essensePouch - the container holding the essences
-- @return: array - values calculated based on essence pouch contents
EnchantingHelper.CalculateEssencesInPouch = function( essensePouch, enchantItem )
    if( essensePouch == nil ) then return nil end

    local essenceItems = {}
    local essenceList = {}
    local essences = essensePouch:GetContainedObjects()
    local cost = 0
    local canEnchant = true

    for index, essence in pairs(essences) do
        
        if not( EnchantingHelper.CanEssenceBeAppliedToObject( essence, enchantItem ) ) then
            canEnchant = false
        end

        table.insert(essenceItems,essence)

        local essenceType = essence:GetObjVar("EssenceType")
        local essenceName = essence:GetObjVar("EssenceName")
        local essenceLevel = tonumber(essence:GetObjVar("EssenceLevel"))

        if(EssenceData[essenceName]) then
            -- If this essence doesn't exist in the list, add it
            if not ( essenceList[essenceName] ) then
                essenceList[essenceName] = { MinMax = {0,0} }
            end

            essenceList[essenceName].MinMax[1] = math.min(EssenceData[essenceName].EffectRangeMax, essenceList[essenceName].MinMax[1] + ( EssenceData[essenceName].EffectRanges[essenceLevel][1] * GetStackCount(essence) ))
            essenceList[essenceName].MinMax[2] = math.min(EssenceData[essenceName].EffectRangeMax, essenceList[essenceName].MinMax[2] + ( EssenceData[essenceName].EffectRanges[essenceLevel][2] * GetStackCount(essence) ))
            cost = cost + ((EssenceData.BaseCopperCost * EssenceData[essenceName].GoldCostMultiplier[essenceLevel]) * GetStackCount(essence))
        end
        
    end


    local enchants = {}
    local namePrefix, nameSuffix = nil

    for essenceName, data in pairs(essenceList) do
        -- We are doing primary essence
        if( #enchants == 0 ) then
            namePrefix = EssenceData[essenceName].Prefix
        elseif( #enchants == 1 ) then
            nameSuffix = EssenceData[essenceName].Suffix
        end

        table.insert(enchants, { essenceName = essenceName, minMax = data.MinMax })
    end

    return 
    {
        Items = essenceItems,
        Enchants = enchants,
        Suffix = nameSuffix,
        Prefix = namePrefix,
        Cost = cost,
        CanEnchant = canEnchant,
    }

    --[[return { 
        primaryName, 
        secondaryName, 
        namePrefix, 
        nameSuffix, 
        primaryMinMax, 
        secondaryMinMax
    }
    ]]
end

EnchantingHelper.IsMageItem = function( item )
    if( item:GetObjVar("ArmorType") == "VeryLight" ) then return true end
    if( item:GetObjVar("ArmorType") == "Linen" ) then return true end
    if( item:GetObjVar("ArmorType") == "Padded" ) then return true end
    if( item:GetObjVar("ArmorType") == "MageRobe" ) then return true end
    local weaponType = Weapon.GetType( item )
    if( weaponType and weaponType == "SorcererWand") then return true end
    if( weaponType and weaponType == "MagicStaff") then return true end
    --SCAN ADDED
    if( weaponType and weaponType == "LightningStaff") then return true end
    if( weaponType and weaponType == "FireStaff") then return true end
    if( weaponType and weaponType == "HealStaff") then return true end
    if( weaponType and weaponType == "LightningWand") then return true end
    if( weaponType and weaponType == "FireWand") then return true end
    if( weaponType and weaponType == "HealWand") then return true end
    return false
end

EnchantingHelper.IsRangedItem = function( item )
    if( item:GetObjVar("ArmorType") == "Assassin" ) then return true end
    if( item:GetObjVar("ArmorType") == "Leather" ) then return true end
    if( item:GetObjVar("ArmorType") == "Hardened" ) then return true end
    if( item:GetObjVar("ArmorType") == "Chain" ) then return true end
    local weaponType = Weapon.GetType( item )
    if( Weapon.IsRanged(weaponType) ) then return true end

    return false
end

EnchantingHelper.IsMeleeItem = function( item )
    if( item:GetObjVar("ArmorType") == "Bone" ) then return true end
    if( item:GetObjVar("ArmorType") == "Scale" ) then return true end
    if( item:GetObjVar("ArmorType") == "Plate" ) then return true end
    if( item:GetObjVar("ArmorType") == "FullPlate" ) then return true end
    local weaponType = Weapon.GetType( item )
    if( weaponType and weaponType ~= "BareHand" and weaponType ~= "SorcererWand" and weaponType ~= "MagicStaff" and not Weapon.IsRanged(weaponType) ) then return true end
    return false
end

EnchantingHelper.GetItemNameFromEnchants = function( itemObj, enchants )
    enchants = enchants or itemObj:GetObjVar("Enchants") or {}
    if( itemObj == nil ) then return false end
    local templateData = GetTemplateData(itemObj:GetCreationTemplateId())
    local itemName = templateData.Name
    local itemName, color = StripColorFromString(itemName)
    local name = {}
    local prefix = nil
    local prefixEssence = nil
    local suffix = nil

    -- Check to see if we have a prefix
    for k,enchant in pairs(enchants) do  
        local essenceName = enchant.essenceName
        if( not prefix and EssenceData[essenceName] and  EssenceData[essenceName].Prefix and EssenceData[essenceName].Prefix ~= "" ) then
            prefix = EssenceData[essenceName].Prefix
            prefixEssence = essenceName
            
        end
        
        if( not suffix and essenceName ~= prefixEssence and EssenceData[essenceName] and EssenceData[essenceName].Prefix and EssenceData[essenceName].Prefix ~= "" ) then
            suffix = EssenceData[essenceName].Suffix
            
        end
    end

    if( color ) then
        table.insert(name, color)
    end

    if( prefix ) then
        table.insert(name, prefix) 
        table.insert(name, " ")
    end
    
    table.insert(name, itemName) 

    if( suffix ) then
        table.insert(name, " ")
        table.insert(name, suffix) 
    end

    if( color ) then
        table.insert(name, "[-]") 
    end
    
    return table.concat(name)
end

EnchantingHelper.ApplyRandomEnchantsToItem = function( item, crafterObj, materialUsed, nonCrafted, iLevel, maxEnchants )
    
    local shieldClasses = { "MeleeEnchants", "MageEnchants" }
    local enchantsType = nil
    local maxEnchants = 1

    --SCAN ADDED ENCHANTING VALUE FOR MAGIC CRAFTING VS. ARTIFICER SCROLLS
    if( item:HasObjVar("MagicCrafted") ) then
        maxEnchants = 1
    end
    if( item:HasObjVar("ArtifactCrafted") ) then
        maxEnchants = 3
    end
    
    -- If it's a shield, it can be one of the shieldClasses types
    if( item:HasObjVar("ShieldType") ) then enchantsType = shieldClasses[math.random( 1,#shieldClasses )]
    elseif( EnchantingHelper.IsMageItem(item) ) then enchantsType = "MageEnchants"
    elseif( EnchantingHelper.IsMeleeItem(item) ) then enchantsType = "MeleeEnchants"
    elseif( EnchantingHelper.IsRangedItem(item) ) then enchantsType = "RangedEnchants" end


    --DebugMessage("enchantsType : ".. enchantsType)

    -- Did we come up with a enchantType to use?
    if( enchantsType == nil ) then return false end

    if not( nonCrafted ) then

        -- If it is armor we want to make the armor bonus +3
        if( item:HasObjVar("ArmorBonus") ) then
            item:SetObjVar("ArmorBonus", 3)
        end

        -- If it is a weapon set the attack bonus to +50
        if( item:HasObjVar("AttackBonus") ) then
            item:SetObjVar("AttackBonus", 50)
        end

    end
  
    
    -- Lets enchant this thing!
    local enchants = {}

    -- Holds the suffix of the last enchant applied
    local namePrefix, nameSuffix = nil
    
    -- If this wasn't crafted it might already have enchants!
    if( nonCrafted ) then
        enchants = item:GetObjVar("Enchants") or enchants
    end

    local indexesUsed = {}
    local tries = 0
    local maxTries = 100

    while( #enchants < maxEnchants and tries < maxTries ) do 
        tries = tries + 1
        local enchant = EssenceData[enchantsType][math.random( 1,#EssenceData[enchantsType] )]
        local notRestricted = EnchantingHelper.CanEnchantBeAppliedToObject( enchant, item )
        --DebugMessage( "enchant : ", enchant, " notRestricted : ", tostring(notRestricted) )
        local enchantFound = false
        for i=1, #enchants do 
            if( enchants[i].essenceName == enchant ) then
                enchantFound = true
            end
        end

        if( enchantFound == false and notRestricted ) then
            
            -- If this item is not crafted we need to generate the 
            -- prefix and suffix from the enchants we are adding.
            if( nonCrafted ) then
                if( not namePrefix ) then
                    namePrefix = EssenceData[enchant].Prefix 
                else
                    nameSuffix = EssenceData[enchant].Suffix 
                end
            end
            
            local modifierValue  = 0

            -- If we are provided an iLevel then the enchant values need to be pulled from the DropEffectRanges[]
            -- in the static data essence entries.
            if( iLevel ) then
                if( EssenceData[enchant].DropEffectRanges ) then
                    modifierValue = math.random( EssenceData[enchant].DropEffectRanges[iLevel][1], EssenceData[enchant].DropEffectRanges[iLevel][2] )
                else
                    modifierValue = math.random( EssenceData[enchant].EffectRanges[2][1], EssenceData[enchant].EffectRanges[2][2] )
                end
            else
                modifierValue = math.random( EssenceData[enchant].EffectRanges[3][1], EssenceData[enchant].EffectRanges[3][2] )
            end

            table.insert(enchants, 
                { 
                    essenceName = enchant, 
                    essenceModifier = modifierValue,
                }
            )
            
        end
    end

    -- If this wasn't crafted it might already have enchants!
    if(  not nonCrafted ) then
        local artifactItemNameKey = enchantsType
        for i=1, #EssenceData[enchantsType] do
            for j=1, #enchants do
                if( enchants[j].essenceName == EssenceData[enchantsType][i] ) then
                    artifactItemNameKey = artifactItemNameKey .. enchants[j].essenceName
                end
            end
        end

        local itemName, crafterName = EnchantingHelper.GetRandomItemName( enchantsType, artifactItemNameKey, crafterObj:GetName() )
        --SCAN MODIFIED COLOR
        item:SetName("[FF9900]"..itemName..": "..StripColorFromString(GetTemplateData(item:GetCreationTemplateId()).Name).."[-]")
        if(  not nonCrafted ) then
            item:SetObjVar("Discoverer", crafterName)
        end
        item:SetObjVar("Durability", 200)
        item:SetObjVar("MaxDurability", 200)
        --SCAN MODIFIED HUE RANGE FOR RNG
        item:SetHue(math.random(100,210))

        -- ServerSettings.Crafting.MaterialBonus.ArtifactCurse

        local curseChanceBonus = ServerSettings.Crafting.MaterialBonus.ArtifactCurse[materialUsed] or 0
        -- If we pass the "Curse" success chance, do it!
        if( Success( ( ServerSettings.Crafting.ArtifactCurseChance + curseChanceBonus) ) ) then
            --item:SetObjVar("Blessed", true)
            item:SetObjVar("Cursed", true)
        end

    else
        local name = EnchantingHelper.GetItemNameFromEnchants( item, enchants )
        item:SetName(name)
    end

    --SCAN ADDED ARTIFCER SCROLL ARTIFACT PROPERTY
    if( item:HasObjVar("ArtifactCrafted") ) then
        item:SetObjVar("Artifact", true)
    end
    item:SetObjVar("Enchants", enchants)
    --SCAN ADDED
    item:SetObjVar("CanDye", true)
    EnchantingHelper.UpdateItemTooltip( item )

end

EnchantingHelper.GetRandomItemName = function( enchantsType, itemNameKey, crafterName )
    local itemName = GlobalVarReadKey("Items.EnchantedName", itemNameKey)

    if( itemName == nil ) then
        local prefix = EssenceData.ItemNames[enchantsType].Prefix[math.random( 1,#EssenceData.ItemNames[enchantsType].Prefix )]
        local suffix = EssenceData.ItemNames.Suffixes[math.random( 1,#EssenceData.ItemNames.Suffixes )]
        local itemKey = GlobalVarReadKey("Items.EnchantedKey", prefix.." "..suffix) or nil

        if( itemKey == nil ) then
            SetGlobalVar("Items.EnchantedKey", function(record) record[prefix.." "..suffix] = itemNameKey return true end)
            SetGlobalVar("Items.EnchantedName", function(record) record[itemNameKey] = { Name = prefix.." "..suffix, Crafter = crafterName } return true end)
            return prefix.." "..suffix, crafterName
        else
            return EnchantingHelper.GetRandomItemName( enchantsType, itemNameKey )
        end
    else
        return itemName.Name, itemName.Crafter
    end
    
end