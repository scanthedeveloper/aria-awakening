HelperPlayerTopBar = {}

HelperPlayerTopBar.ShowEnchantmentTrackingButton = function( playerObj, buttonId )
    local dynWindow = DynamicWindow("PlayerMobileModTracking","",0,0,0,0,"Transparent","TopLeft")
    local displayMods = false

    local mobileMods = {
        "HitChancePlus",
        "ManaReductionTimes",
        "AttackSpeedTimes",
        "AttackBonus",
        "DefenseChancePlus",
        "PowerPlus",
        "MaxHealthPlus",
        "MaxManaPlus",
        "MaxStaminaPlus",
        "MaxWeightPlus",
        "TameChanceTimes",
        "LockpickChanceTimes",
        "HealingReceivedPlus",
        "HarvestDelayReductionTimes",
        "LumberjackingDelayReductionTimes",
        "MiningDelayReductionTimes",
        "FishingDelayReductionTimes",
    }

    local MobileModMassage = 
    {
        HitChancePlus = { Name = "Hit Chance", Suffix = "%", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        ManaReductionTimes = { Name = "Mana Reduction", Suffix = "%", ValFunc = function(value) return value end, MaxFunc = function(value) return (1 - value) * 100 end },
        AttackSpeedTimes = { Name = "Attack Speed", Suffix = "%", ValFunc = function(value) return value end, MaxFunc = function(value) return (1 - value) * 100 end },
        AttackBonus = { Name = "Attack Bonus", Suffix = "%", ValFunc = function(value) return value * 100 end, MaxFunc = function(value) return value end },
        DefenseChancePlus = { Name = "Defense Bonus", Suffix = "%", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        PowerPlus = { Name = "Power Bonus", Suffix = "+", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        MaxHealthPlus = { Name = "Health Bonus", Suffix = "+", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        MaxManaPlus = { Name = "Mana Bonus", Suffix = "+", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        MaxStaminaPlus = { Name = "Stamina Bonus", Suffix = "+", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        MaxWeightPlus = { Name = "Weight Bonus", Suffix = "+", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        TameChanceTimes = { Name = "Tame Chance", Suffix = "%", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        LockpickChanceTimes = { Name = "Lockpick Chance", Suffix = "%", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        HealingReceivedPlus = { Name = "Healing Received", Suffix = "+", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        HarvestDelayReductionTimes = { Name = "Harvest Speed", Suffix = "%", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        LumberjackingDelayReductionTimes = { Name = "Lumberjack Speed", Suffix = "%", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        MiningDelayReductionTimes = { Name = "Mining Speed", Suffix = "%", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },
        FishingDelayReductionTimes = { Name = "Fishing Speed", Suffix = "%", ValFunc = function(value) return value end, MaxFunc = function(value) return value end },

    }

    --DebugMessage( "BUTTONID: " .. tostring(buttonId) )

    if( buttonId == "Show" ) then
        displayMods = true
        dynWindow:AddImage(1,25,"TextFieldChatUnsliced",300,180,"Sliced")

        dynWindow:AddLabel(120,30,"Bonus",0,0,24)
        dynWindow:AddLabel(200,30,"Maximum",0,0,24)

        local curY = 50

        RequestMobileMod( playerObj, playerObj, mobileMods,
            function(MobileMod) 

            for i=1, #mobileMods do 

                local massageData = MobileModMassage[mobileMods[i]]
                local modValue = massageData.ValFunc(GetMobileMod(MobileMod[mobileMods[i]]))
                local maxValue = massageData.MaxFunc(ServerSettings.Combat.MaxLimits[mobileMods[i]])

                if( modValue > 0 or modValue < 0 ) then
                    dynWindow:AddLabel(10,curY,massageData.Name,0,0,16)
                    dynWindow:AddLabel(135,curY,"[FF7F50]"..tostring(modValue)..massageData.Suffix.."[-]",0,0,16)
                    if( maxValue ~= nil ) then
                        dynWindow:AddLabel(230,curY,"[FF7F50]"..tostring(maxValue)..massageData.Suffix.."[-]",0,0,16)
                    end
                    curY = curY + 15
                end

            end

        end)

        playerObj:OpenDynamicWindow(dynWindow)
    else
        playerObj:CloseDynamicWindow("PlayerMobileModTracking")
    end

    return displayMods

end

HelperPlayerTopBar.ShowPlayerTopBar = function( playerObj )
    local dynWindow = DynamicWindow("PlayerTopBar","",0,0,0,0,"Transparent","TopLeft")
    dynWindow:AddImage(-10,-5,"TextFieldChatUnsliced",3000,32,"Sliced")

    -- Tracking Icons
    local trackingWindow = DynamicWindow("PlayerTopBarTracking","",0,0,0,0,"Transparent","TopRight")
    
    -- Backpack
    local backpack = playerObj:GetEquippedObject("Backpack")
    if( backpack ) then
        local maxWeight = GetContainerMaxWeight(backpack)
        local curWeight = math.floor( GetContentsWeight(backpack) )
        trackingWindow:AddImage(-400,-12,"BackpackIcon_Default",30,40,"Sliced")
        trackingWindow:AddButton(-400,-12,"","",30,40,"Backpack Weight","",false,"Invisible")
        trackingWindow:AddLabel(-370,5,"( "..curWeight.." / "..maxWeight.." )",0,0,22)
    end


    
    
    
    
    playerObj:OpenDynamicWindow(dynWindow)
    playerObj:OpenDynamicWindow(trackingWindow)
end