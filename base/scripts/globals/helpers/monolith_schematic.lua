MonolithSchematic = {}


MonolithSchematic.SetTooltip = function( _schematicObj, _tooltipInfo )
    if( _schematicObj:HasObjVar("EchoShardsRequired") ) then
        local skillUsed = _schematicObj:GetObjVar("SkillUsed")
        local skillLevelRequired = _schematicObj:GetObjVar("SkillLevelRequired")
        local shardsRequired = _schematicObj:GetObjVar("EchoShardsRequired")
        local recipeUsed = _schematicObj:GetObjVar("RecipeUsed")
        local materialUsed = _schematicObj:GetObjVar("MaterialUsed")

        _tooltipInfo.MonolithSchematicSkillName = {
            TooltipString = "Requires: " .. SkillData.AllSkills[skillUsed].DisplayName .. " ("..tostring(skillLevelRequired)..")",
            Priority = -8400,
        }


        local resourceNeeded = GetResourceAmountFromRecipe( skillUsed, recipeUsed, materialUsed )
        _tooltipInfo.MonolithSchematicEchoShards = {
            TooltipString = "Needs: " .. shardsRequired .. " Echo Shards, " .. resourceNeeded .. " " ..GetDisplayNameFromRecipe(materialUsed),
            Priority = -8401,
        }

    end
    return _tooltipInfo
end

MonolithSchematic.PlayerCanUseSchematic = function( _player, _schematicObj )

    -- Make sure our variables are good
    if( not _player or not _schematicObj ) then return false end

    local skill = _schematicObj:GetObjVar("SkillUsed")
    local skillRequired = _schematicObj:GetObjVar("SkillLevelRequired")
    local shardsRequired = _schematicObj:GetObjVar("EchoShardsRequired")

    
    if( not skill or not skillRequired or not shardsRequired ) then return false end

    
end
