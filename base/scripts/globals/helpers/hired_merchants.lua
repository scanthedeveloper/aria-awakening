--[[
    Provides functions to assist with player hired merchants.
]]


HelperHiredMerchants = {}
HelperHiredMerchants.SoldListLimit = 50
HelperHiredMerchants.MaximumItemsPerPage = 30


HelperHiredMerchants.IsHiredMerchant = function( mobile )

    if(
        mobile:HasModule("ai_hireling_merchant") -- Is it a merchant?
        and mobile:HasObjVar("PlotController") -- Is it attached to a plot?
    ) then
        return true;
    end
    return false;
end

-- Given a merchant will return a table of the objects being sold
HelperHiredMerchants.GetItemsForSale = function(merchant, sortBy, sortDirection)
    if( merchant == nil ) then return {} end
    local itemsForSale = merchant:GetObjVar("merchantSaleItem") or {}
    local results = {}
    sortBy = sortBy or "Name"
    sortDirection = sortDirection or false
    
    local index = 1
    for i=1, #itemsForSale do
        if( itemsForSale[i] and itemsForSale[i]:IsValid() ) then
            local stackCount = itemsForSale[i]:GetObjVar("StackCount") or 1
            local tooltip = tostring(itemsForSale[i]:GetSharedObjectProperty("TooltipString")).."\n\nWeight: "..tostring(itemsForSale[i]:GetSharedObjectProperty("Weight"))
            local name = itemsForSale[i]:GetName()
            local price = itemsForSale[i]:GetObjVar("itemPrice")
            table.insert(results, index, { ID = index, Name = StripColorFromString(StripStackCountFromString(name)), StackCount = stackCount, Tooltip = tooltip, Price = price } )    
            index = index + 1
        else
            itemsForSale[i] = nil
        end
    end
    
    merchant:SetObjVar("merchantSaleItem", itemsForSale)

    --if( sortDirection ~= nil ) then
        table.sort(results, function(a,b) 
            if( sortDirection == true ) then
                return a[sortBy] < b[sortBy] 
            else
                return not(a[sortBy] < b[sortBy]) 
            end
        end )
    --end

    return results
end

-- Given a merchant will return a table of sold records
HelperHiredMerchants.GetItemsSold = function(merchant)
    if( merchant == nil ) then return {} end
    return merchant:GetObjVar("merchantSoldItem") or {}
end

-- Given an item, merchant, and buyer, it will add the item to the merchant sold list
HelperHiredMerchants.AddToSaleLedger = function( soldItem, merchant, buyer, price )
    --DebugMessage("AddToSaleLedger : " ,tostring( soldItem ) ,tostring( merchant ) ,tostring( buyer ) ,tostring( price ))
    if( soldItem == nil or merchant == nil or price == nil ) then return end
    
    local soldItemList = merchant:GetObjVar("merchantSoldItem") or {}

    -- If we are at our max sold list, we pop the last one off and trash it
    if( #soldItemList >= HelperHiredMerchants.SoldListLimit ) then
        local trash = table.remove(soldItemList)
    end

    local stackCount = soldItem:GetObjVar("StackCount") or 1
    local ts = os.time()
    local buyerName = "Bazaar"
    if( buyer and buyer:IsValid() ) then 
        buyerName = buyer:GetName() 
    end 
    table.insert(soldItemList, 1, { Name = soldItem:GetName(), Quantity = tostring(stackCount), Price= ValueToAmountStrShort(price), Buyer= buyerName or "n/a", Timestamp = os.date('%m/%d/%Y', ts) })
    
    -- Update the objvar on our merchant
    merchant:SetObjVar("merchantSoldItem", soldItemList)
end

-- Get the table of sold items from the merchant and displays to the user
HelperHiredMerchants.ShowSaleList = function(user, merchant, windowID, selectedTab, sortBy, reverseSort, isOwner, currentPage)
    if( user == nil or merchant == nil or windowID == nil ) then return false end
    local newWindow = DynamicWindow(windowID,"Merchant Ledger",590,480)
    local mCurrentTab = selectedTab or "For Sale"
    local mSelectedItemId = nil
    local mCurPage = currentPage or 1
    
    if( isOwner ) then
        -- Add our tabs 
        AddTabMenu(newWindow,
        {
            ActiveTab = mCurrentTab, 
            Buttons = {
                { Text = "For Sale" },
                { Text = "Sale Log" },
            }
        })
    else
        -- Add our tabs 
        AddTabMenu(newWindow,
        {
            ActiveTab = mCurrentTab, 
            Buttons = {
                { Text = "For Sale" }
            }
        })
    end

    -- If we are on the "For Sale" tab, we need to display a list
    -- of all the items this merchant is actively selling.

    if(mCurrentTab == "For Sale") then

        newWindow:AddImage(8,52,"BasicWindow_Panel",554,340,"Sliced")
		newWindow:AddImage(10,35,"HeaderRow_Bar",550,21,"Sliced")

		newWindow:AddButton(10,35,"Sort|Name","Item",74,21,"","",false,"Text")
		newWindow:AddButton(272,35,"Sort|StackCount","Quantity",132,21,"","",false,"Text")
		newWindow:AddButton(380,35,"Sort|Price","Cost",144,21,"","",false,"Text")		

		local scrollWindow = ScrollWindow(10,60,535,312,24)		
        local itemsForSale = HelperHiredMerchants.GetItemsForSale(merchant, sortBy, reverseSort, mCurPage)
        local numPages = math.ceil( #itemsForSale / HelperHiredMerchants.MaximumItemsPerPage )

        -- Iterate through the items the merchant is selling and create
        -- a line item for each one.
        local startIndex = (((mCurPage - 1) * HelperHiredMerchants.MaximumItemsPerPage ) + 1)

        for i=startIndex, (startIndex + HelperHiredMerchants.MaximumItemsPerPage) do
            if ( i <= #itemsForSale ) then 
                
                if( itemsForSale[i].Name ~= nil and itemsForSale[i].Price ~= nil ) then
                    local isSelected = mSelectedItemId == i
                    local scrollElement = ScrollElement()
                    local buttonState = isSelected and "pressed" or ""
                    
                    scrollElement:AddButton(4,0,"Select|"..tostring(itemsForSale[i].ID),"",522,24,itemsForSale[i].Tooltip .. "\n\n ["..COLORS.GreenYellow.."]CLICK TO PURCHASE[-]","",false,"ThinFrameHover",buttonState)
                    scrollElement:AddLabel(18,6,itemsForSale[i].Name,300,20,18,"left")
                    scrollElement:AddLabel(330,6,tostring(itemsForSale[i].StackCount),120,20,18,"left")
                    scrollElement:AddLabel(430,6,ValueToAmountStrShort(itemsForSale[i].Price),120,20,18,"left") 

                    scrollWindow:Add(scrollElement)
                end
                
            end
        end

        newWindow:AddScrollWindow(scrollWindow)

        -- Add the pagination buttons and page labels
        if(numPages > 1 and mCurPage > 1) then
            newWindow:AddButton(217,398,"Prev|"..tostring(mCurPage),"",0,0,"","",false,"Previous")
        end

        newWindow:AddLabel(275,395,"Page "..mCurPage.." of "..numPages,250,24,20,"center")	
        
        if(numPages > 1 and mCurPage < numPages) then
            newWindow:AddButton(327,398,"Next|"..tostring(mCurPage+1),"",0,0,"","",false,"Next")
        end

        -- Check if the merchant has reserved items for the user
        local reservedItems = merchant:GetObjVar("ReservedItems") or {}
        local hasReservedItems = false
        for k,v in pairs( reservedItems ) do 
            if( v == user ) then
                hasReservedItems = true
            end
        end

        -- If the merchant has ReservedItems for this user we need to show them the button to collect them.
        if( hasReservedItems == true ) then
            newWindow:AddButton(410,395,"ClaimReservedItems","Collect Purchases",140,30,"","",false,"")
        end




    elseif(mCurrentTab == "Sale Log") then

        newWindow:AddImage(8,52,"BasicWindow_Panel",554,340,"Sliced")
		newWindow:AddImage(10,35,"HeaderRow_Bar",550,21,"Sliced")

		newWindow:AddButton(10,35,"Sort|Item","Item",74,21,"","",false,"Text")
		newWindow:AddButton(220,35,"Sort|Quantity","Quantity",30,21,"","",false,"Text")
        newWindow:AddButton(270,35,"Sort|Price","Price",74,21,"","",false,"Text")
        newWindow:AddButton(350,35,"Sort|Buyer","Buyer",100,21,"","",false,"Text")
        newWindow:AddButton(450,35,"Sort|SoldOn","Sold On",74,21,"","",false,"Text")		

		local scrollWindow = ScrollWindow(10,60,535,312,24)		
        local itemsSold = HelperHiredMerchants.GetItemsSold(merchant)

        -- Iterate through the items the merchant is selling and create
        -- a line item for each one.
        for i=1, #itemsSold do
            local isSelected = mSelectedItemId == i
        
            -- Color items based on affordability?
            local labelColor = (true and "") or "[918369]"

            local scrollElement = ScrollElement()
            
            scrollElement:AddLabel(18,6,StripColorFromString(StripStackCountFromString(itemsSold[i].Name)),300,20,18,"left")
            scrollElement:AddLabel(220,6,itemsSold[i].Quantity,50,20,18,"left")
            scrollElement:AddLabel(270,6,itemsSold[i].Price,75,20,18,"left")
            scrollElement:AddLabel(370,6,itemsSold[i].Buyer,100,20,18,"left")
            scrollElement:AddLabel(450,6,itemsSold[i].Timestamp,100,20,18,"left")
            
            scrollWindow:Add(scrollElement)
        end

        newWindow:AddScrollWindow(scrollWindow)

        -- Bottom buttons
        -- These would required the ai_hireling_merchant to be rewritten to use helper 
        -- functions instead of local ones -bphelps

        --newWindow:AddButton(20,395,"Action|Sell","Sell",100,28,"","",false,"")
        --newWindow:AddButton(120,395,"Action|CollectMoney","Collect Money",150,28,"","",false,"")
        --newWindow:AddButton(270,395,"Action|MoveLocation","Move Location",150,28,"","",false,"")
        --newWindow:AddButton(420,395,"Action|Dismiss","Dismiss",130,28,"","",false,"")

    end

    user:OpenDynamicWindow(newWindow)
    return true

end

