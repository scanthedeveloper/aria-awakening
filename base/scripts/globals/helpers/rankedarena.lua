-- TODO: add the ability to "Add Friendly"


RankedArena = {}
RankedArena.MatchTypes = {
    OneVersusOne = 
    {
        Title = "1v1 Match", 
        MaxPlayersPerTeam = 1,
    },
    TwoVersusTwo = 
    {
        Title = "2v2 Match", 
        MaxPlayersPerTeam = 2,
    },
    ThreeVersusThree = 
    {
        Title = "3v3 Match", 
        MaxPlayersPerTeam = 3,
    },

}
RankedArena.DefaultScore = 1000;
RankedArena.MaximumDistance = 20;
RankedArena.DefaultMatchCoutdown = 10;
RankedArena.StartingPositions = { TeamA = Loc( -2708.24, 0, -2227.94 ), TeamB = Loc( -2695.97, 0, -2215.21 ), End = Loc( -2702.52, 0, -2234.61 ) }
--RankedArena.StartingPositions = { TeamA = Loc( 27.73, 0, -36.13 ), TeamB = Loc( 16.84, 0, -35.68 ), End = Loc( 17.76, 0, -26.83 ) }
RankedArena.EloK = 30;
RankedArena.MaxPotionsPerMatch = 5;
RankedArena.EnableRatingCalculations = true;
RankedArena.MatchLengthSeconds = 300;
RankedArena.StatueTemplate = "arenaSeason2_statue"
RankedArena.MountTemplate = "item_statue_mount_pyros_warhorse"

RankedArena.Rewards = {
    -- 1
    { RankedArena.MountTemplate , RankedArena.StatueTemplate }, 
    -- 2
    { RankedArena.StatueTemplate },
    -- 3
    { RankedArena.StatueTemplate },
    -- 4
    { RankedArena.StatueTemplate },
    -- 5
    { RankedArena.StatueTemplate },
    -- 6
    { RankedArena.StatueTemplate },
    -- 7
    { RankedArena.StatueTemplate },
    -- 8
    { RankedArena.StatueTemplate },
    -- 9
    { RankedArena.StatueTemplate },
    -- 10
    { RankedArena.StatueTemplate },
}

RankedArena.CanUsePotion = function( playerObj )
    if( HasMobileEffect(playerObj, "RankedArenaMatch") ) then
        local potionsUsed = playerObj:GetObjVar("ArenaPotionsUsed") or 0
        if( potionsUsed >= RankedArena.MaxPotionsPerMatch ) then
            return false
        else
            potionsUsed = potionsUsed + 1
            playerObj:SetObjVar("ArenaPotionsUsed", potionsUsed)
            return true
        end
    end
    return true
end

RankedArena.InRankedArenaMatch = function( mobileA, mobileB )
    local mobileAVar = mobileA:GetObjVar("ArenaTeam")
    local mobileBVar = mobileB:GetObjVar("ArenaTeam")
    --DebugMessage( "InRankedArenaMatch : " .. tostring( ( mobileAVar ~= nil and mobileBVar ~= nil and mobileAVar ~= mobileBVar ) ) )
    return ( mobileAVar ~= nil and mobileBVar ~= nil and mobileAVar ~= mobileBVar )
end

-- Send a message to players
RankedArena.SendMessageToPlayers = function( TeamA, TeamB, args )
    
    if( TeamA and  type(TeamA)=="table" ) then
        for i=1, #TeamA do TeamA[i]:SendMessage("RankedArenaMessage", args) end
    end
    
    if( TeamB and  type(TeamB)=="table" ) then
        for i=1, #TeamB do TeamB[i]:SendMessage("RankedArenaMessage", args) end
    end

end

-- Remove a player from the match
RankedArena.RemovePlayer =function( TeamA, TeamB, playerObj )
    
    if( TeamB and  type(TeamB)=="table" ) then
        for i=1, #TeamA do if( TeamA[i] == playerObj ) then TeamA[i] = nil end end
    end

    if( TeamB and  type(TeamB)=="table" ) then
        for i=1, #TeamB do if( TeamB[i] == playerObj ) then TeamB[i] = nil end end
    end

    return TeamA, TeamB
end

RankedArena.ValidateMatchAndTeams = function( matchType, TeamB, TeamB )
    if( #TeamB == matchType.MaxPlayersPerTeam and #TeamB == matchType.MaxPlayersPerTeam ) then
        return true
    end
    return false
end

RankedArena.IsPlayerOnTeam = function( teamA, playerObj )
    for key,member in pairs(teamA) do 
        if( type(member) ~= "string" )then member = tostring(member.Id) end
        if( member == tostring(playerObj.Id) ) then return true end
    end
    return false
end

-- Take two scores and determine their probability of winning
RankedArena.EloProbability = function( ratingA, ratingB ) 
    local probA, probB = 0
    probA = ( 1 / ( 1 + ( math.pow(10, ((ratingB - ratingA) / 400)) ) ) )
    probB = ( 1 / ( 1 + ( math.pow(10, ((ratingA - ratingB) / 400)) ) ) )
    return probA, probB
end

RankedArena.CalculateEloRating = function( ratingA, ratingB, ratingAWon )

    local probA, probB = RankedArena.EloProbability( ratingA, ratingB )
    
    if( ratingAWon ) then
        ratingA = ratingA + RankedArena.EloK * ( 1 - probA ) 
        ratingB = ratingB + RankedArena.EloK * ( 0 - probB ) 
    else
        ratingA = ratingA + RankedArena.EloK * ( 0 - probA ) 
        ratingB = ratingB + RankedArena.EloK * ( 1 - probB ) 
    end

    ratingA = math.floor( ratingA )
    ratingB = math.floor( ratingB )

    return ratingA, ratingB
end

RankedArena.GetTeamAvgRating = function( teamA, matchTitle, arenaMaster )
    if( teamA == nil or arenaMaster == nil ) then return nil end
    local avgRating = 0

    -- Get the sum of all the team members rating
    for key,member in pairs(teamA) do 
        local playerRating = RankedArena.GetArenaRatingForPlayer( member, arenaMaster, matchTitle )
        avgRating = avgRating + playerRating
    end

    --DebugMessage( tostring(avgRating) .. "/" .. tostring(#teamA) )

    -- Return the average score for this team
    return math.floor( avgRating / #teamA )
end

RankedArena.GetArenaRatingForPlayer = function( playerObj, arenaMaster, matchTitle )
    local arenaRatings = arenaMaster:GetObjVar("PlayerRatings") or {}
    if( arenaRatings[playerObj] == nil ) then arenaRatings[playerObj] = {} end
    local playerRating = arenaRatings[playerObj][matchTitle] or RankedArena.DefaultScore
    return playerRating
end

RankedArena.SavePlayerName = function( _playerObj, _arenaMaster )
    local arenaRatings = _arenaMaster:GetObjVar("PlayerRatings") or {}
    if( arenaRatings[_playerObj] == nil ) then arenaRatings[_playerObj] = {} end
    if( arenaRatings[_playerObj]["PlayerName"] == nil ) then
        arenaRatings[_playerObj]["PlayerName"] = _playerObj:GetName()
    end
    _arenaMaster:SetObjVar("PlayerRatings", arenaRatings)
end

RankedArena.SetArenaRatingForPlayer = function( playerObj, arenaMaster, matchTitle, rating )
    if( not RankedArena.EnableRatingCalculations ) then return false end
    local arenaRatings = arenaMaster:GetObjVar("PlayerRatings") or {}
    if( arenaRatings[playerObj] == nil ) then arenaRatings[playerObj] = {} end
    arenaRatings[playerObj][matchTitle] = rating
    arenaMaster:SetObjVar("PlayerRatings", arenaRatings)
    playerObj:SetObjVar("ArenaRating", arenaRatings[playerObj] )
    local message = "Your Arena Rating for ("..matchTitle..") has changed and is now ("..arenaRatings[playerObj][matchTitle]..")."
    playerObj:SystemMessage(message, "info")
    playerObj:SystemMessage(message)
end

RankedArena.AdjustPlayerRating = function( playerObj, ratingDelta, arenaMaster, matchTitle, showRankWindow )
    if( playerObj == nil or ratingDelta == nil or arenaMaster == nil ) then return end
    local playerRating = RankedArena.GetArenaRatingForPlayer( playerObj, arenaMaster, matchTitle )
    RankedArena.SetArenaRatingForPlayer( playerObj, arenaMaster, matchTitle, (playerRating + ratingDelta))
end

RankedArena.GetSortedRanks = function( _arenaMaster, _playerRatingsData, _bracket )
    _bracket = _bracket or "1v1"

    --DebugMessage( DumpTable( _playerRatingsData ) )

    if( not _playerRatingsData ) then
        if( _arenaMaster ) then
            _playerRatingsData = _arenaMaster:GetObjVar("PlayerRatings") or {}
        else
            _playerRatingsData = {}
        end
    end

    local rankedSort = {}
    for player,rating in pairs(_playerRatingsData) do 
        if( type(player) ~= "string" ) then player = tostring(player.Id) end
        local playerName = rating.PlayerName or "Unknown Combatant"
        if( rating[_bracket] ) then
            table.insert( rankedSort, { PlayerId = player, ELOScore = rating[_bracket], PlayerName = playerName } )
        end
    end

    table.sort(rankedSort, function(a,b) return a.ELOScore>b.ELOScore end)

    --DebugMessage( DumpTable( rankedSort ) )

    return rankedSort
end

RankedArena.GetNamesOfTeamMembers =function( _arenaMaster, _playerRatingsData, _teamMembers )

    if( not _playerRatingsData ) then
        if( _arenaMaster ) then
            _playerRatingsData = _arenaMaster:GetObjVar("PlayerRatings") or {}
        else
            _playerRatingsData = {}
        end
    end

    local memberNames = {}
    for player,rating in pairs(_playerRatingsData) do 
        if( type(player) ~= "string" ) then player = tostring(player.Id) end
        local playerName = rating.PlayerName or "Unknown Combatant"
        for i=1,#_teamMembers do
            local member = _teamMembers[i]
            if( type(member) ~= "string" ) then member = tostring(member.Id) end
            if( member == player ) then
                local playerName = rating.PlayerName or "Unknown Combatant"
                table.insert( memberNames, playerName )
            end
        end
    end

    return memberNames

end

RankedArena.EndArenaSeason = function( _arenaMaster )
    local playerRatings = _arenaMaster:GetObjVar("PlayerRatings")
    local sortedOne = RankedArena.GetSortedRanks( _arenaMaster, playerRatings, "1v1" )
    local sortedTwo = RankedArena.GetSortedRanks( _arenaMaster, playerRatings, "2v2" )
    local sortedThree = RankedArena.GetSortedRanks( _arenaMaster, playerRatings, "3v3" )
    local lastRank = 0
    local playerRewards = {}
    local rewardsProcessed = 0

    rewardsProcessed = 1
    lastRank = 0
    for i=1, #sortedOne do 
        local rating = sortedOne[i]
        if( rewardsProcessed <= #RankedArena.Rewards ) then
            if( rating.ELOScore ~= lastRank ) then
                local rewards = RankedArena.Rewards[ rewardsProcessed ]
                table.insert( playerRewards, { PlayerId = rating.PlayerId, Rewards=rewards, Rank=rewardsProcessed, Bracket = "1v1" } )
                rewardsProcessed = rewardsProcessed + 1
            else
                table.insert( playerRewards, { PlayerId = rating.PlayerId, Rewards=rewards, Rank=rewardsProcessed, Bracket = "1v1" } )
            end
            lastRank = rating.ELOScore
        end
    end

    rewardsProcessed = 1
    lastRank = 0
    for i=1, #sortedTwo do 
        local rating = sortedTwo[i]
        if( rewardsProcessed <= #RankedArena.Rewards ) then
            if( rating.ELOScore ~= lastRank ) then
                local rewards = RankedArena.Rewards[ rewardsProcessed ]
                table.insert( playerRewards, { PlayerId = rating.PlayerId, Rewards=rewards, Rank=rewardsProcessed, Bracket = "2v2" } )
                rewardsProcessed = rewardsProcessed + 1
            else
                table.insert( playerRewards, { PlayerId = rating.PlayerId, Rewards=rewards, Rank=rewardsProcessed, Bracket = "2v2" } )
            end
            lastRank = rating.ELOScore
        end
    end

    rewardsProcessed = 1
    lastRank = 0
    for i=1, #sortedThree do 
        local rating = sortedThree[i]
        if( rewardsProcessed <= #RankedArena.Rewards ) then
            if( rating.ELOScore ~= lastRank ) then
                local rewards = RankedArena.Rewards[ rewardsProcessed ]
                table.insert( playerRewards, { PlayerId = rating.PlayerId, Rewards=rewards, Rank=rewardsProcessed, Bracket = "3v3" } )
                rewardsProcessed = rewardsProcessed + 1
            else
                table.insert( playerRewards, { PlayerId = rating.PlayerId, Rewards=rewards, Rank=rewardsProcessed, Bracket = "3v3" } )
            end
            lastRank = rating.ELOScore
        end
    end

    --DebugMessage( DumpTable( playerRewards ) )

    _arenaMaster:SetObjVar( "PlayerRewards", playerRewards )
    _arenaMaster:SetObjVar( "PlayerRatings", {} )
    _arenaMaster:SetObjVar( "ArenaMatchLog", {} )
end

RankedArena.GetMatchHistory = function( _arenaMaster, _matchHistoryData, _playerRatingsData, _playerObj, _bracket )
    _bracket = _bracket or "1v1"
    local matches = {}

    if( not _matchHistoryData ) then
        if( _arenaMaster ) then
            _matchHistoryData = _arenaMaster:GetObjVar("ArenaMatchLog") or {}
        else
            _matchHistoryData = {}
        end
    end

    if( not _playerRatingsData ) then
        if( _arenaMaster ) then
            _playerRatingsData = _arenaMaster:GetObjVar("PlayerRatings") or {}
        else
            _playerRatingsData = {}
        end
    end

    -- TeamA is "Friendly" : TeamB is "Enemy"

    for i=1, #_matchHistoryData do 
        local match = _matchHistoryData[i]
        if( match.Title and match.Title == _bracket ) then
            local OnTeamA = RankedArena.IsPlayerOnTeam( match.TeamA, _playerObj )
            local OnTeamB = RankedArena.IsPlayerOnTeam( match.TeamB, _playerObj )
            if( OnTeamA or OnTeamB ) then

                local wonGame = false
                if( (OnTeamA and match.Winner == "Enemy") or (OnTeamB and match.Winner == "Friendly") ) then
                    wonGame = true
                end

                local endTime = os.date("*t", match.EndTime)
                local endDate = endTime.month .. "/" .. endTime.day .. "/" .. endTime.year
                local TeamANames = RankedArena.GetNamesOfTeamMembers( _arenaMaster, _playerRatingsData, match.TeamA )
                local TeamBNames = RankedArena.GetNamesOfTeamMembers( _arenaMaster, _playerRatingsData, match.TeamB )
                local allies = {}
                local enemies = {}
                local ranked = match.Ranked

                if( OnTeamA ) then
                    allies = TeamANames
                    enemies = TeamBNames
                else
                    allies = TeamBNames
                    enemies = TeamANames
                end

                table.insert( matches, { Allies = allies, Enemies = enemies, Won = wonGame , Date = endDate, Ranked = ranked } )

            end
        end
    end

    --DebugMessage( "MATCHES", DumpTable( matches ) )

    return matches
end

RankedArena.SetArenaAwardTooltip = function( _object, _tooltipInfo )
    local tooltipLines = {}
    local rewardInfo = _object:GetObjVar("RewardInfo") or nil

    if( rewardInfo == nil ) then
        rewardInfo = 
        {
            AwardedTo = _object:GetObjVar("AwardedTo"),
            Bracket = _object:GetObjVar("Bracket"),
            Rank = _object:GetObjVar("Rank")
        }
    end

    if( rewardInfo and _object:GetCreationTemplateId() == RankedArena.StatueTemplate ) then
        --DebugMessage( "Statue", DumpTable( rewardInfo ) )
        tooltipLines[1] = rewardInfo.AwardedTo
        tooltipLines[2] = rewardInfo.Bracket
        tooltipLines[3] = "Rank #"..rewardInfo.Rank
        tooltipLines[4] = "Fall 2020 Arena Season"
    elseif( rewardInfo and _object:GetCreationTemplateId() == RankedArena.MountTemplate ) then
        --DebugMessage( "Mount", DumpTable( rewardInfo ) )
        tooltipLines[1] = rewardInfo.AwardedTo
        tooltipLines[2] = rewardInfo.Bracket
        tooltipLines[3] = "Rank #"..rewardInfo.Rank
        tooltipLines[4] = "Fall 2020 Arena Season"
    else
        return _tooltipInfo
    end

    -- Set the tooltip
    _tooltipInfo.ArenaReward = {
        TooltipString = "[E9967A]" .. CombineString(tooltipLines, "\r\n") .. "[-]",
        Priority = 100,
    }

    return _tooltipInfo
end