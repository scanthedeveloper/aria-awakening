-- This helper file implements functions around using the "Challenge System"

ChallengeSystemHelper = {}

ChallengeSystemHelper.GetGlobalVar = function (playerObj,propName)
    local userId = playerObj:GetAttachedUserId()
    return GlobalVarReadKey(string.format("ChallengeSystem.%s", userId), propName)
end

ChallengeSystemHelper.SetGlobalVar = function(playerObj,propName,propValue,cb)
    local userId = playerObj:GetAttachedUserId()
    SetGlobalVar(string.format("ChallengeSystem.%s", userId),
        function(record)
            record[propName] = propValue

            return true
        end,
        function(success)
            if(success and cb) then
                cb()
            end
        end)
end

ChallengeSystemHelper.DisplayActivateUI = function( _playerObj )
    local dynWindow = DynamicWindow("ChallegeSystemActivate","",200,180,-230,120,"Transparent","TopRight")
    dynWindow:AddImage(0,0,"TextFieldChatUnsliced",44,70,"Sliced")
    dynWindow:AddButton(0,0 ,"OpenChallengeSystem","Challenge System",44,70,"Review challenges and rewards.","",false,"Invisible")
    dynWindow:AddImage(0,0,"PowerHourPotion", 42,66)
    _playerObj:OpenDynamicWindow(dynWindow)
end

-- Gets the ObjVar for the "ChallengeTokens" based on PVP flag
function ChallengeSystemHelper.GetTokenObjVar( _pvp )
    return _pvp and ServerSettings.ChallengeSystem.PVPTokenObjVar or ServerSettings.ChallengeSystem.TokenObjVar
end

-- Returns the current season that the Challenge System is on.
function ChallengeSystemHelper.GetCurrentSeason()
    return ServerSettings.ChallengeSystem.Season
end

-- Returns the current season Challenge System season the player is on
function ChallengeSystemHelper.GetSeason( _player )
    if( not _player or not _player:IsValid() ) then return nil end
    --DebugMessage("Account Property : " .. ServerSettings.ChallengeSystem.SeasonObjVar .. " value is : " .. tostring(tonumber(_player:ChallengeSystemHelper.GetGlobalVar(ServerSettings.ChallengeSystem.SeasonObjVar))) )
    return tonumber(ChallengeSystemHelper.GetGlobalVar(_player,ServerSettings.ChallengeSystem.SeasonObjVar)) or nil
end

-- Sets the current season Challenge System season for the player
function ChallengeSystemHelper.SetSeason( _player )
    if( not _player or not _player:IsValid() ) then return false end
    ChallengeSystemHelper.SetGlobalVar(_player, ServerSettings.ChallengeSystem.SeasonObjVar, tostring(ChallengeSystemHelper.GetCurrentSeason()) )
    --DebugMessage("Account Property : " .. ServerSettings.ChallengeSystem.SeasonObjVar .. " set to : " .. tostring(ChallengeSystemHelper.GetCurrentSeason()) )
    return true
end

-- Applies a change to the amount of "ChallengeTokens" a player has; can be flagged to change normal or pvp tokens
function ChallengeSystemHelper.AdjustTokenAmount( _player, _delta, _pvp )
    if( not _player or not _player:IsValid() or not _delta ) then return false end
    local tokenObjVar = ChallengeSystemHelper.GetTokenObjVar( _pvp )
    local currentAmount = tonumber(ChallengeSystemHelper.GetGlobalVar(_player, tokenObjVar )) or 0
    --DebugMessage("Account Property : " .. tokenObjVar .. " value is : " .. tostring(currentAmount) )
    currentAmount = currentAmount + tonumber(_delta)
    currentAmount = math.min(currentAmount, ServerSettings.ChallengeSystem.PrizeThreeTokensNeeded)
    --DebugMessage("Account Property : " .. tokenObjVar .. " set to : " .. tostring(currentAmount) )
    ChallengeSystemHelper.SetGlobalVar(_player, tokenObjVar, tostring(currentAmount) )
    return true
end

-- Set the amount of "ChallengeTokens" a player has; can be flagged to change normal or pvp tokens
function ChallengeSystemHelper.SetTokenAmount( _player, _amount, _pvp )
    if( not _player or not _player:IsValid() or not _amount ) then return false end
    local tokenObjVar = ChallengeSystemHelper.GetTokenObjVar( _pvp )
    _amount = math.min(_amount, ServerSettings.ChallengeSystem.PrizeThreeTokensNeeded)
    ChallengeSystemHelper.SetGlobalVar(_player, tokenObjVar, tostring(tonumber(_amount)) )
    --DebugMessage("Account Property : " .. tokenObjVar .. " set to : " .. tostring(tonumber(_amount)) )
    return true
end

-- Set the amount of "ChallengeTokens" a player has; can be flagged to change normal or pvp tokens
function ChallengeSystemHelper.GetTokenAmount( _player, _pvp )
    if( not _player or not _player:IsValid() ) then return 0 end
    local tokenObjVar = ChallengeSystemHelper.GetTokenObjVar( _pvp )
    --DebugMessage("Account Property : " .. tokenObjVar .. " value is : " .. tostring(tonumber(_player:ChallengeSystemHelper.GetGlobalVar( tokenObjVar ))) )
    return tonumber(ChallengeSystemHelper.GetGlobalVar(_player, tokenObjVar )) or 0
end

-- Given a player and a reward number, attempts to claim that reward for the player
function ChallengeSystemHelper.TryClaimReward( _player, _rewardAccountProp)
    --DebugMessage( "1" )
    if( 
        not _player or 
        not _player:IsValid()
    ) then return end

    if not( _player:IsInRegion("WorldInns") or Plot.IsInHouse(_player,true) ) then
        _player:SystemMessage("Must be in your home or an inn to claim season rewards.","info")
        return false
    end

    --DebugMessage( "2" )
    local canClaim = ChallengeSystemHelper.CanClaimReward( _player, _rewardAccountProp )

    if( canClaim ) then
        ChallengeSystemHelper.SetGlobalVar(_player, _rewardAccountProp,"true",function ()
            local backpackObj = _player:GetEquippedObject("Backpack")
                if(backpackObj) then
                    LootTables.SpawnLoot({ServerSettings.ChallengeSystem.RewardLootTables[_rewardAccountProp]},backpackObj)
                    _player:SystemMessage("The rewards have been placed in your backpack.","info")
                end
        end)
    end

    _player:SendMessage("UpdateChallengeUI")
end

function ChallengeSystemHelper.CanClaimReward( _player, _rewardAccountProp )
    local int PVETokens = ChallengeSystemHelper.GetTokenAmount( _player )
    local int PVPTokens = ChallengeSystemHelper.GetTokenAmount( _player, true )
    local canClaim =  false
    if( ChallengeSystemHelper.HasClaimedReward( _player, _rewardAccountProp )  ) then return canClaim end

    -- We need to see if we have enough tokens to claim the reward
    if( _rewardAccountProp == ServerSettings.ChallengeSystem.PVE1Reward and PVETokens >= ServerSettings.ChallengeSystem.PrizeOneTokensNeeded ) then
        canClaim = true
    elseif( _rewardAccountProp == ServerSettings.ChallengeSystem.PVE2Reward and PVETokens >= ServerSettings.ChallengeSystem.PrizeTwoTokensNeeded ) then
        canClaim = true
    elseif( _rewardAccountProp == ServerSettings.ChallengeSystem.PVE3Reward and PVETokens >= ServerSettings.ChallengeSystem.PrizeThreeTokensNeeded ) then
        canClaim = true
    elseif( _rewardAccountProp == ServerSettings.ChallengeSystem.PVP1Reward and PVPTokens >= ServerSettings.ChallengeSystem.PrizeOneTokensNeeded ) then
        canClaim = true
    elseif( _rewardAccountProp == ServerSettings.ChallengeSystem.PVP2Reward and PVPTokens >= ServerSettings.ChallengeSystem.PrizeTwoTokensNeeded ) then
        canClaim = true
    elseif( _rewardAccountProp == ServerSettings.ChallengeSystem.PVP3Reward and PVPTokens >= ServerSettings.ChallengeSystem.PrizeThreeTokensNeeded ) then
        canClaim = true
    end

    return canClaim
end

function ChallengeSystemHelper.HasClaimedReward( _player, _rewardAccountProp )
    --DebugMessage( "HasClaimedReward : " .. _rewardAccountProp .. " : " .. tostring(( _player:ChallengeSystemHelper.GetGlobalVar(_rewardAccountProp) == "true" )) )
    return ( tostring(ChallengeSystemHelper.GetGlobalVar(_player,_rewardAccountProp)) == "true" )
end


function ChallengeSystemHelper.ClearRewardClaims( _player )
    if( _player and _player:IsValid() ) then
        local userId = _player:GetAttachedUserId()
        SetGlobalVar(string.format("ChallengeSystem.%s", userId),
            function(record)
                record[ServerSettings.ChallengeSystem.PVE1Reward] = false
                record[ServerSettings.ChallengeSystem.PVE2Reward] = false
                record[ServerSettings.ChallengeSystem.PVE3Reward] = false
                record[ServerSettings.ChallengeSystem.PVP1Reward] = false
                record[ServerSettings.ChallengeSystem.PVP2Reward] = false
                record[ServerSettings.ChallengeSystem.PVP3Reward] = false

                return true
            end)
    end
end

function ChallengeSystemHelper.CancelChallengeQuests( _player, _questType )
    
    local quests = _player:GetObjVar("Quests") or {}
    if( quests[_questType] ) then
        quests[_questType] = nil
    end
    _player:SetObjVar("Quests",  quests)
end

-- Performs tasks on the given player based on the current season and their season
function ChallengeSystemHelper.DoUpkeep( _player )
    if( not _player or not _player:IsValid() ) then return end
    local playerSeason = ChallengeSystemHelper.GetSeason(_player)
    local currentSeason = ChallengeSystemHelper.GetCurrentSeason()

    --DebugMessage("DoUpkeep", playerSeason, currentSeason)

    -- If our player is not on the right season we need to update them
    if( playerSeason ~= currentSeason ) then
        ChallengeSystemHelper.ClearRewardClaims( _player )          -- Resets all the reward claimed states
        ChallengeSystemHelper.SetSeason( _player )                  -- Update to newest season
        ChallengeSystemHelper.SetTokenAmount( _player, 0 )          -- Clear tokens
        ChallengeSystemHelper.SetTokenAmount( _player, 0, true )    -- Clear PVP tokens
        ChallengeSystemHelper.CancelChallengeQuests( _player, "HuntingChallenges" )
        ChallengeSystemHelper.CancelChallengeQuests( _player, "CraftingChallenges" )
        ChallengeSystemHelper.CancelChallengeQuests( _player, "GatheringChallenges" )
        ChallengeSystemHelper.CancelChallengeQuests( _player, "PVPChallenges" )
    end

    local int PVETokens = ChallengeSystemHelper.GetTokenAmount( _player )
    local int PVPTokens = ChallengeSystemHelper.GetTokenAmount( _player, true )

    if( PVETokens < ServerSettings.ChallengeSystem.PrizeThreeTokensNeeded  ) then
        -- Hunting Quests
        for i=1, #AllQuests.HuntingChallenges do 
            if( not Quests.IsActive( _player, "HuntingChallenges", i ) ) then 
                Quests.TryStart( _player, "HuntingChallenges", i )
            end
        end

        -- Crafting Quests
        for i=1, #AllQuests.CraftingChallenges do 
            if( not Quests.IsActive( _player, "CraftingChallenges", i ) ) then 
                Quests.TryStart( _player, "CraftingChallenges", i )
            end
        end

        -- Gathering Quests
        for i=1, #AllQuests.GatheringChallenges do 
            if( not Quests.IsActive( _player, "GatheringChallenges", i ) ) then 
                Quests.TryStart( _player, "GatheringChallenges", i )
            end
        end
    end
    
    if( PVPTokens < ServerSettings.ChallengeSystem.PrizeThreeTokensNeeded  ) then
        -- PVP Quests
        for i=1, #AllQuests.PVPChallenges do 
            if( not Quests.IsActive( _player, "PVPChallenges", i ) ) then 
                Quests.TryStart( _player, "PVPChallenges", i )
            end
        end
    end

end

-- Displays the UI for the "Challenge System"
function ChallengeSystemHelper.DisplayUI( _player )
    local dynWindow = DynamicWindow(ServerSettings.ChallengeSystem.ChallengeSystemUI, ServerSettings.ChallengeSystem.Title,1000,750,0,0,"Parchment")
    dynWindow:AddImage(5,30,"Prestige_Divider",965,0,"Sliced")
    _player:OpenDynamicWindow(dynWindow, _player)
end

function ChallengeSystemHelper.DisplayUIElement( _scrollWindow, _args )
    _args = _args or {}
    local Title = _args.Title or ""
    local Progress = _args.Progress or "[7CFC00][-]"
    local Description = _args.Description or ""
    local TokensAwarded = _args.TokensAwarded or 0
    local RepeatHours = _args.RepeatHours or nil
    local IsComplete = _args.IsComplete
    local IsPVP = _args.IsPVP

    local scrollElement = ScrollElement()
    local scrollY = 5
    scrollElement:AddImage(5,scrollY,"TextFieldChatUnsliced",437,90,"Sliced")

    scrollY = 10
    scrollElement:AddLabel(20,scrollY,Title,0,0,30,"", false, false, "PermianSlabSerif_Dynamic_Bold")
    scrollY = scrollY + 25
    if( IsComplete ) then
        scrollElement:AddLabel(20,scrollY,"[7CFC00]( Complete )[-]",0,0,20,"", false, false, "PermianSlabSerif_Dynamic_Bold")
    else
        scrollElement:AddLabel(20,scrollY,"[7CFC00]"..Progress.."[-]",0,0,20,"", false, false, "PermianSlabSerif_Dynamic_Bold")
    end
    
    scrollY = scrollY + 25
    scrollElement:AddLabel(20,scrollY,Description,350,100,18,"", false, false, "PermianSlabSerif_Dynamic_Bold")
    
    -- Token Icon
    scrollElement:AddImage(400,10,"empty_circle",30,30,"Sliced")
    scrollElement:AddLabel(408,18,tostring(TokensAwarded),0,0,18,"", false, false, "PermianSlabSerif_Dynamic_Bold")
    if( IsPVP ) then
        scrollElement:AddButton(400,10,"Nothing","CW",30,30,"Awards: "..tostring(TokensAwarded).." Skulls","",false,"Invisible")
    else 
        scrollElement:AddButton(400,10,"Nothing","CW",30,30,"Awards: "..tostring(TokensAwarded).." Tokens","",false,"Invisible")
    end
    
    -- Repeat Icon
    if( RepeatHours ) then
        scrollElement:AddImage(365,10,"empty_circle",30,30,"Sliced")
        scrollElement:AddImage(365,10,"CraftingCategory_Beds",30,30,"Sliced")
        scrollElement:AddButton(365,10,"Nothing","CW",30,30,"Repeatable every "..RepeatHours.." hour(s)","",false,"Invisible")
    end

    if( IsComplete ) then
        scrollElement:AddImage(5,5,"TextFieldChatUnsliced",437,90,"Sliced")
        scrollElement:AddImage(5,5,"TextFieldChatUnsliced",437,90,"Sliced")
    end
    
    _scrollWindow:Add(scrollElement)
end

function ChallengeSystemHelper.DigController( _playerObj )
    if( not ServerSettings.ChallengeSystem.Diggable ) then return end
    local InBlackZone = false
    for i=1, #ServerSettings.ChallengeSystem.DigBlackZones do 
        if( _playerObj:IsInRegion(ServerSettings.ChallengeSystem.DigBlackZones[i]) ) then
            InBlackZone = true
        end
    end
    
    if( 
        ServerSettings.ChallengeSystem.Diggable -- Can we do seasonal digs?
        and _playerObj -- do we have a player?
        and _playerObj:IsValid() -- are they valid?
        and GetGuardProtection(_playerObj) ~= "Town" -- are we not in a town?
        and InBlackZone == false
    ) then
        if(
            _playerObj:HasObjVar("LastSeasonalDigLoc") == false
            or (_playerObj:GetLoc():Distance(_playerObj:GetObjVar("LastSeasonalDigLoc")) > 10)
        ) then
            local digEntry = ChallengeSystemHelper.DigResponse( _playerObj ) or {}
            if( digEntry.Type ) then
                _playerObj:SetObjVar("LastSeasonalDigLoc", _playerObj:GetLoc())
                Quests.SendQuestEventMessage( _playerObj, "Digging", { "Seasonal", digEntry.Type, digEntry.Template }, 1 )
            else
                return false
            end
        else
            _playerObj:SystemMessage("Try digging somewhere else.", "info" )
        end
        return true
    end
    return false
end

function ChallengeSystemHelper.DigResponse( _playerObj )
    local diggables = ServerSettings.ChallengeSystem.Diggables or {}
    local success = false
    local digEntry = nil

    -- If we are on a plot, we cannot dig up anything
    if( Plot.GetAtLoc(_playerObj:GetLoc()) ) then
        return digEntry
    end

    for i=1,#diggables do
        if( success == false ) then
            success = Success(diggables[i].Chance/100)
            if( success ) then
                digEntry = diggables[i]
            end
        end
    end

    if( digEntry ~= nil ) then
        -- We've dug up a mobile!
        if( digEntry.Type == "mobile" ) then
            
            local spawnLocation = SorceryHelper.GetSummonSpawnLocation( _playerObj )
            Create.Temp.AtLoc( digEntry.Template, spawnLocation,function( summonObj )
                CallFunctionDelayed(TimeSpan.FromMilliseconds(40), function() 
                    summonObj:SendMessage("AttackEnemy",_playerObj)
                end)
                _playerObj:SystemMessage("Your digging disturbed a " .. summonObj:GetName()..".", "info" )
            end)

        -- We've dug up a item!
        elseif( digEntry.Type == "item" ) then
            local templateData = GetTemplateData(digEntry.Template)
            if( digEntry.Count and digEntry.Count > 1 ) then
                Create.Stack.InBackpack( digEntry.Template, _playerObj, digEntry.Count, nil, function(createdObj) 
                    _playerObj:SystemMessage("You dig up some " .. tostring(templateData.ObjectVariables.PluralName or templateData.Name) .. ".", "info" )
                end)
            else
                Create.InBackpack( digEntry.Template, _playerObj, nil, function(createdObj) 
                    _playerObj:SystemMessage("You dig up a " .. tostring(templateData.Name) ..".", "info" )
                end)
            end
            
        -- We've dug up a trigger!
        elseif( digEntry.Type == "trigger" ) then
            _playerObj:SystemMessage("Triggered: " .. digEntry.Template..".", "info" )
        end

    end

    return digEntry
end