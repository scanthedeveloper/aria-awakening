Bloodlust = {}

Bloodlust.TryApplyToCorpse = function(corpseObj)
    if ( not corpseObj:HasModule("corpse_bloodlust") ) then
        corpseObj:AddModule("corpse_bloodlust")
    end
end