Create = {}
Create.Custom = {}
Create.Stack = {}
Create.Temp = {}
Create.CustomTemp = {}

--- Create a template at location
-- @param template
-- @param loc - location to create at
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
Create.AtLoc = function(template, loc, cb, noTooltip)
    local id = template..uuid()
    RegisterSingleEventHandler(EventType.CreatedObject, id, function(success, obj)
        if ( success and noTooltip ~= true ) then SetItemTooltip(obj) end
        if ( cb ) then
            if ( success ) then
                cb(obj)
            else
                cb(nil)
            end
        end
    end)
    CreateObj(template, loc, id)
end

Create.AtLocWithHue = function(template, hue, loc, cb, noTooltip)

    Create.AtLoc( template, loc, function(gameObj)
        gameObj:SetHue( hue )
        gameObj:SetName("Hue: " .. tostring(hue))
        if ( cb ) then
            cb(gameObj)
        end
    end,noTooltip )


end

--- Create a template on a mobile's equipment
-- @param template
-- @param mobile - mobile to created equipped object on
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
Create.Equipped = function(template, mobile, cb, noTooltip)
    local id = template..uuid()
    RegisterSingleEventHandler(EventType.CreatedObject, id, function(success, obj)
        if ( success and noTooltip ~= true ) then SetItemTooltip(obj) end
        if ( cb ) then
            if ( success ) then
                cb(obj)
            else
                cb(nil)
            end
        end
    end)
    CreateEquippedObj(template, mobile, id)
end

--- Create a custom template at location
-- @param template
-- @param loc - location to create at
-- @param data - Template Data (GetTemplateData)
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
Create.Custom.AtLoc = function(template, data, loc, cb, noTooltip)
    local id = template..uuid()
    RegisterSingleEventHandler(EventType.CreatedObject, id, function(success, obj)
        if ( success and noTooltip ~= true ) then SetItemTooltip(obj) end
        if ( cb ) then
            if ( success ) then
                cb(obj)
            else
                cb(nil)
            end
        end
    end)
    CreateCustomObj(template, data, loc, id)
end

-- tableOfItemsToCreate are copies of Quest[step].Requirements, so { RequirementName (not used), objTemplate, count, specificObjVarName, specificObjVarValue }
Create.ItemsForCraftingProfession = function(target, tableOfItemsToCreate, container)
    if not tableOfItemsToCreate or #tableOfItemsToCreate < 1 then return end
    for i=1, #tableOfItemsToCreate do
        local template = tableOfItemsToCreate[i][2]
        local count = tableOfItemsToCreate[i][3]
        local objVarName = tableOfItemsToCreate[i][4] or nil
        local objVarValue = tableOfItemsToCreate[i][5] or nil

        local SetVars = function(obj)
            obj:SetObjVar("Crafter", target)
            obj:SetObjVar("CraftedBy", target:GetName())
            if objVarName and objVarValue then
                obj:SetObjVar(objVarName, objVarValue)
            end
        end

        local templateData = GetTemplateData(template)
        if ( not templateData.LuaModules or (not templateData.LuaModules.stackable and not templateData.LuaModules.coins) ) then
            for j=1, count do
                Create.InContainer(template, container, nil, SetVars)
            end
        else
            Create.Stack.InContainer(template, container, count, nil, SetVars)
        end
    end
    target:SystemMessage("Needed items have been crafted in "..tostring(container))
end

--Creates needed items for first incompleted Crafting Order found
Create.ItemsForCraftOrder = function(target, desiredSkill, container)
    local craftingOrders = GetItems(target, "crafting_order", "OrderSkill", desiredSkill)
    if not container then container = target:GetEquippedObject("Backpack") end
    if #craftingOrders > 0 then
        for i=1, #craftingOrders do
            if not craftingOrders[i]:GetObjVar("OrderComplete") then
                local orderInfo = craftingOrders[i]:GetObjVar("OrderInfo")
                local orderSkill = craftingOrders[i]:GetObjVar("OrderSkill")
                local template = AllRecipes[orderSkill][orderInfo["Recipe"]]["CraftingTemplateFile"]
                local count = orderInfo["Amount"]
                local objVarName = "Material"
                local objVarValue = orderInfo[objVarName]

                local SetVars = function(obj)
                    obj:SetObjVar("Crafter", target)
                    obj:SetObjVar("CraftedBy", target:GetName())
                    if objVarName and objVarValue then
                        obj:SetObjVar(objVarName, objVarValue)
                    end
                end

                weight = GetTemplateObjectProperty(template,"Weight")
             
                DebugMessage("CO making "..tostring(count).." "..tostring(template).." of "..tostring(objVarValue).." "..tostring(objVarName))
                local templateData = GetTemplateData(template)
                if ( not templateData.LuaModules or (not templateData.LuaModules.stackable and not templateData.LuaModules.coins) ) then
                    for j=1, count do
                        if weight == -1 then
                            local containerloc = GetRandomDropPosition(container)
                            Create.PackedObject.InContainer(template, container, containerloc, SetVars)
                        else
                            Create.InContainer(template, container, nil, SetVars)
                        end
                    end
                else
                    Create.Stack.InContainer(template, container, count, nil, SetVars)
                end
                return
            end
        end
        target:SystemMessage("No incomplete Crafting Orders found in "..tostring(container), "info")
    else
        target:SystemMessage("No Crafting Orders found in "..tostring(container), "info")
    end
end

--- Create a template in a container
-- @param template
-- @param container - container gameObj
-- @param containerloc - (optional) location in container
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
Create.InContainer = function(template, container, containerloc, cb, noTooltip)
    local id = template..uuid()
    RegisterSingleEventHandler(EventType.CreatedObject, id, function(success, obj)
        if ( success and noTooltip ~= true ) then SetItemTooltip(obj) end
        if ( cb ) then
            if ( success ) then
                cb(obj)
            else
                cb(nil)
            end
        end
    end)
    if not( containerloc ) then
        containerloc = GetRandomDropPosition(container)
    end
    CreateObjInContainer(template, container, containerloc, id)
end

--- Create a template in a container
-- @param template
-- @param data - Template Data (GetTemplateData)
-- @param container - container gameObj
-- @param containerloc - (optional) location in container
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
Create.Custom.InContainer = function(template, data, container, containerloc, cb, noTooltip)
    local id = template..uuid()
    RegisterSingleEventHandler(EventType.CreatedObject, id, function(success, obj)
        if ( success and noTooltip ~= true ) then SetItemTooltip(obj) end
        if ( cb ) then
            if ( success ) then
                cb(obj)
            else
                cb(nil)
            end
        end
    end)
    if not( containerloc ) then
        containerloc = GetRandomDropPosition(container)
    end
    CreateCustomObjInContainer(template, data, container, containerloc, id)
end

--- Create a custom template in a mobile's backpack
-- @param template
-- @param data - Template Data (GetTemplateData)
-- @param mobile - mobileObj
-- @param containerloc - (optional) location in container
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
-- @param noAutoOpen - if true, the bag won't be opended to the mobile
Create.Custom.InBackpack = function(template, data, mobile, containerloc, cb, noTooltip, noAutoOpen)
    if not( cb ) then cb = function(obj) end end
    local backpack = mobile:GetEquippedObject("Backpack")
    if ( backpack == nil ) then return cb(nil) end
    Create.Custom.InContainer(template, data, backpack, containerloc, function(obj)
        if ( obj and not noAutoOpen ) then backpack:SendOpenContainer(mobile) end
        cb(obj)
    end, noTooltip)
end

--- Create a template in a mobile's backpack
-- @param template
-- @param mobile - mobileObj
-- @param containerloc - (optional) location in container
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
-- @param noAutoOpen - if true, the bag won't be opended to the mobile
Create.InBackpack = function(template, mobile, containerloc, cb, noTooltip, noAutoOpen)
    if not( cb ) then cb = function(obj) end end
    local backpack = mobile:GetEquippedObject("Backpack")
    if ( backpack == nil ) then return cb(nil) end
    Create.InContainer(template, backpack, containerloc, function(obj)
        if ( obj and not noAutoOpen ) then backpack:SendOpenContainer(mobile) end
        cb(obj)
    end, noTooltip)
end

--- Create a template in a mobile's bank
-- @param template
-- @param mobile - mobileObj
-- @param containerloc - (optional) location in container
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
-- @param noAutoOpen - if true, the bag won't be opended to the mobile
Create.InBank = function(template, mobile, containerloc, cb, noTooltip, noAutoOpen)
    if not( cb ) then cb = function(obj) end end
    local bank = mobile:GetEquippedObject("Bank")
    if ( bank == nil ) then return cb(nil) end
    Create.InContainer(template, bank, containerloc, function(obj)
        cb(obj)
    end, noTooltip)
end

--- Create a template at a location, assigning StackCount before creation
-- @param template
-- @param count - stack count
-- @param loc - location in world
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
Create.Stack.AtLoc = function(template, count, loc, cb, noTooltip)
    if not( cb ) then cb = function(obj) end end
    if ( count < 1 ) then return cb(nil) end

    local id = template..uuid()
    RegisterSingleEventHandler(EventType.CreatedObject, id, function(success, obj)
        if ( success and noTooltip ~= true ) then SetItemTooltip(obj) end
        if ( success ) then
            cb(obj)
        else
            cb(nil)
        end
    end)
    
    -- set the stack count before creating
    local templateData = GetTemplateData(template)
    if not( templateData.ObjectVariables ) then templateData.ObjectVariables = {} end
    templateData.ObjectVariables.StackCount = count

    CreateCustomObj(template, templateData, loc, id)
end

--- Create a template in a container, assigning StackCount before creation
-- @param template
-- @param container - container gameObj
-- @param count - stack count
-- @param containerloc - (optional) location in container
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
-- @param noAutoStack - if true, will create a new object of this stack size, default behavior is to add to any existing stacks.
Create.Stack.InContainer = function(template, container, count, containerloc, cb, noTooltip, noAutoStack)
    if not( cb ) then cb = function(obj) end end
    if ( count < 1 ) then return cb(nil) end

    -- get the template id
    local templateData = GetTemplateData(template)

    -- if this template isn't stackable then create a single one of them
    if ( not templateData.LuaModules or (not templateData.LuaModules.stackable and not templateData.LuaModules.coins) ) then
        Create.InContainer(template, container, containerloc, cb, noTooltip)
        return
    end

    if not( noAutoStack ) then
        if ( templateData.ObjectVariables and templateData.ObjectVariables.ResourceType ) then
            -- look for an existing stack of this type
            local items = container:GetContainedObjects()
            for i=1,#items do
                if ( items[i]:GetObjVar("ResourceType") == templateData.ObjectVariables.ResourceType ) then
                    -- adjust the stack by amount.
                    items[i]:SendMessage("AdjustStack", count)
                    cb(items[i])
                    -- early exit cause task is complete.
                    return
                end
            end
        end
    end

    local id = template..uuid()
    RegisterSingleEventHandler(EventType.CreatedObject, id, function(success, obj)
        if ( success and noTooltip ~= true ) then SetItemTooltip(obj) end
        if ( success ) then
            UpdateStackName(count, obj)
            cb(obj)
        else
            cb(nil)
        end
    end)
    
    -- set the stack count before creating
    if not( templateData.ObjectVariables ) then templateData.ObjectVariables = {} end
    templateData.ObjectVariables.StackCount = count
    
    if not( containerloc ) then
        containerloc = GetRandomDropPosition(container)
    end

    CreateCustomObjInContainer(template, templateData, container, containerloc, id)
end

--- Create a template in a mobile's backpack, assigning StackCount before creation
-- @param template
-- @param mobile - mobileObj
-- @param count - stack count
-- @param containerloc - (optional) location in container
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
-- @param noAutoStack - if true, will create a new object of this stack size, default behavior is to add to any existing stacks.
Create.Stack.InBackpack = function(template, mobile, count, containerloc, cb, noTooltip, noAutoStack, noAutoOpen)
    local backpack = mobile:GetEquippedObject("Backpack")
    if not( cb ) then cb = function(obj) end end
    if ( backpack == nil ) then return cb(nil) end
    Create.Stack.InContainer(template, backpack, count, containerloc, function(obj)
        if ( obj and not noAutoOpen ) then backpack:SendOpenContainer(mobile) end
        cb(obj)
    end, noTooltip, noAutoStack)
end

--- Create a template in a player's bank, assigning StackCount before creation
-- @param template
-- @param player - playerObj
-- @param count - stack count
-- @param containerloc - (optional) location in container
-- @param cb - function(obj) callback
-- @param noTooltip - if true, will skip SetItemTooltip calls
-- @param noAutoStack - if true, will create a new object of this stack size, default behavior is to add to any existing stacks.
Create.Stack.InBank = function(template, player, count, containerloc, cb, noTooltip, noAutoStack)
    local bank = player:GetEquippedObject("Bank")
    if not( cb ) then cb = function(obj) end end
    if ( bank == nil ) then return cb(nil) end
    Create.Stack.InContainer(template, bank, count, containerloc, cb, noTooltip, noAutoStack)
end

--- Create a TEMPORARY (no backup) template at location
-- @param template
-- @param loc - location to create at
-- @param cb - function(obj) callback
-- @param tooltip - if true, will perform SetItemTooltip calls
Create.Temp.AtLoc = function(template, loc, cb, tooltip)
    local id = template..uuid()
    RegisterSingleEventHandler(EventType.CreatedObject, id, function(success, obj)
        if ( success and tooltip ) then SetItemTooltip(obj) end
        if ( cb ) then
            if ( success ) then
                cb(obj)
            else
                cb(nil)
            end
        end
    end)
    CreateTempObj(template, loc, id)
end

--- Create a custom TEMPORARY (no backup) template at location
-- @param template
-- @param loc - location to create at
-- @param data - Template Data (GetTemplateData)
-- @param cb - function(obj) callback
-- @param tooltip - if true, will perform SetItemTooltip calls
Create.CustomTemp.AtLoc = function(template, data, loc, cb, tooltip)
    local id = template..uuid()
    RegisterSingleEventHandler(EventType.CreatedObject, id, function(success, obj)
        if ( success and tooltip == true ) then SetItemTooltip(obj) end
        if ( cb ) then
            if ( success ) then
                cb(obj)
            else
                cb(nil)
            end
        end
    end)
    CreateCustomTempObj(template, data, loc, id)
end

Create.Coins = {}
Create.Coins.InContainer = function(container, amount, cb, noAutoStack)
    Create.Stack.InContainer("coin_purse", container, amount, nil, cb, true, noAutoStack)
end
Create.Coins.InBackpack = function(mobile, amount, cb, noAutoStack)
    Create.Stack.InBackpack("coin_purse", mobile, amount, nil, cb, true, noAutoStack)
end
Create.Coins.InBank = function(player, amount, cb, noAutoStack)
    Create.Stack.InBank("coin_purse", player, amount, nil, cb, true, noAutoStack)
end