Awakening = {}

Awakening.GetDisplayName = function (awakeningController)
	return awakeningController:GetObjVar("AwakeningId") .. " Awakening"
end


Awakening.AddAwakeningUI = function(playerObj,missionWindow,missionOffset)
	local awakeningControllers = FindObjectsWithTag("AwakeningController")
	for i,awakeningController in pairs(awakeningControllers) do
		local awakeningRegion = awakeningController:GetObjVar("AwakeningRegion")
		if(awakeningRegion and playerObj:IsInRegion(awakeningRegion)) then
			-- awakening level is 0 when the mob is sleeping
			if(awakeningController:GetObjVar("AwakeningLevel") > 0) then
				--missionWindow:AddImage(0,missionOffset - 4,"TextFieldChatUnsliced",180,60,"Sliced")
				
				local stageInfo = awakeningController:GetObjVar("StageInfo")
				local curStage = awakeningController:GetObjVar("AwakeningLevel")
				local curStageInfo = stageInfo[curStage]
				local totalKills = curStageInfo.KillCount
				local stageStr = "Stage "..ToRomanNumerals(curStage)
				if(#stageInfo == curStage) then
					stageStr = "Final Stage"
				end

				local killDesc = curStageInfo.KillDesc or "Kill Monsters"
				if(totalKills > 1) then
					local stageKills = awakeningController:GetObjVar("AwakeningKills")
					killDesc = killDesc .. ": " ..stageKills .. "/" .. totalKills
				end


				UpdateDynamicEventHUD( playerObj, {
					Title = Awakening.GetDisplayName(awakeningController),
					Description = stageStr .. "\n" .. killDesc
				})


				--missionWindow:AddLabel(5,0+missionOffset,"[ff6347]"..Awakening.GetDisplayName(awakeningController).."[-]\n"..stageStr.."\n"..killDesc,0,0,18,"left",false,true)

				return true
			end
		end
	end
end

Awakening.IsInAwakening = function(playerObj)
	local awakeningControllers = FindObjectsWithTag("AwakeningController")
	for i,awakeningController in pairs(awakeningControllers) do
		local awakeningRegion = awakeningController:GetObjVar("AwakeningRegion")
		if(awakeningRegion and playerObj:IsInRegion(awakeningRegion)) then
			return true
		end
	end
end