ChaosZoneHelper = {}
ChaosZoneHelper.RegionName = "WaterfallMine"

function ChaosZoneHelper.IsPlayerChaotic( _player )
    return ( _player and _player:IsValid() and HasMobileEffect( _player, "Chaos" ) )
end

function ChaosZoneHelper.IsPlayerInChaosZone( _player )
    if( _player and _player:IsValid() and _player:IsInRegion( ChaosZoneHelper.RegionName ) ) then
        return true
    end
    return false
end

function ChaosZoneHelper.IsLocInChaosZone( _loc )
    if( _loc and IsLocInRegion(_loc,ChaosZoneHelper.RegionName) ) then
        return true
    end
    return false
end

function ChaosZoneHelper.ChaosZoneCheck( _player )
    local hasChaosEffect = ChaosZoneHelper.IsPlayerChaotic(_player)

    if( _player and _player:IsValid() and _player:IsInRegion( ChaosZoneHelper.RegionName ) ) then
        if( not hasChaosEffect ) then
            _player:SendMessage("StartMobileEffect", "Chaos")
        end
    elseif( _player and _player:IsValid() ) then
        if( hasChaosEffect ) then
            _player:SendMessage("EndChaosEffect")
        end
    end
end