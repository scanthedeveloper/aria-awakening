require 'incl_crafting_orders'

function UpdateCraftOrderTooltip(orderObj, tooltipInfo)
	
	local orderInfo = orderObj:GetObjVar("OrderInfo")
	if( orderInfo == nil )then return tooltipInfo end
	local amountNeeded = orderInfo.Amount or 0
	local amountMade = orderObj:GetObjVar("CurrentAmount") or 0
	local orderSkill = orderObj:GetObjVar("OrderSkill")

	-- If this recipes cannot be a crafting order, we need to leave!
	if( not AllRecipes[orderSkill][orderInfo.Recipe] or AllRecipes[orderSkill][orderInfo.Recipe].EnableCraftOrder ~= true )then return tooltipInfo end
	
	local tier = OrderScoreToTier(orderSkill, GetCraftOrderScore(orderSkill, orderInfo))

	-- Set the tooltip
	if( tier ) then
		tooltipInfo.CraftOrderTier = {
			TooltipString = "[FFE691]Tier "..tostring(tier).."[-]",
			Priority = 120,
		}
	end
	
	tooltipInfo.CraftOrderAmount = {
        TooltipString = "[ffffff]"..amountMade.."/"..tostring(amountNeeded) .."[-]",
        Priority = 100,
	}

	if( tonumber(amountMade) == tonumber(amountNeeded) ) then
		tooltipInfo.CraftOrderComplete = {
			TooltipString = "[ffffff]Completed[-]",
			Priority = 90,
		}
	end
    
    return tooltipInfo
end

function GetCraftOrderRewardTier(craftOrderSkill, orderInfo)
	return OrderScoreToTier(craftOrderSkill, GetCraftOrderScore(craftOrderSkill, orderInfo))
end

function PickCraftingOrder(user, craftOrderSkill)
	-- get player crafting skill level
	local skillLevel = GetSkillLevel(user, craftOrderSkill)
	-- add all recipe names to a table
	recipes = GetRecipeTableFromSkill(craftOrderSkill)
	local recipeNames = {}
	for recipeName, recipeInfo in pairs(recipes) do
        recipeNames[#recipeNames+1] = recipeName
	end
	-- pick a random recipe to start evaluation on
	local index = math.random(1, #recipeNames)
	local count = 0
	local recipeName = nil
	local validResources = {}
	-- check recipe/material combinations until a recipe meets skill-appropriate and recipe criteria for at least one resource
	local highestTier = math.ceil(math.max(skillLevel, 0.1) / 10) / 1
	local tiersDown = math.random(0, 3)
	local selectedTier = math.max(highestTier - tiersDown, 1)
	local tries = 0
	local success = 0
	repeat
		local valid = 0
		repeat
			count = count + 1
			-- get skill appropriate resources for this recipe
			recipeName = recipeNames[index]
			local needsRecipe = recipes[recipeName].NeedRecipe
			--local category = recipes[recipeName].NeedRecipe
			local enableCraftOrder = recipes[recipeName].EnableCraftOrder
			if ( ((not needsRecipe or needsRecipe == false) or HasRecipe(user, recipeName))
			and ( enableCraftOrder and enableCraftOrder == true )
			) then
				validResources = TryGetSkillAppropriateMaterialsFromRecipe(user, craftOrderSkill, recipeName, skillLevel)
				if ( validResources and next(validResources) ) then
					valid = 1
				end
			end
			if ( index >= #recipeNames ) then index = 1 else index = index + 1 end
		until ( valid == 1 or count >= #recipeNames )
		-- generate a craft order from valid recipe/resource and amount
		if ( valid == 1 ) then
			local materialIndex = math.random(1, #validResources)
			local material = validResources[materialIndex]
			local orderInfo = {
				Recipe = recipeName,
				Material = material,
			}

			local scoreBeforeAmount = GetCraftOrderScore(craftOrderSkill, orderInfo)
			local tier = GetCraftOrderRewardTier(craftOrderSkill, orderInfo)

			

			local desiredScore = OrderTierToScore(craftOrderSkill, selectedTier)
			local amount = math.max( math.ceil(desiredScore / scoreBeforeAmount * 1) / 1, CraftingOrders.MinAmount )
			
			if ( amount <= math.max(CraftingOrders.MaxAmount * skillLevel/100, CraftingOrders.BaseAmount) ) then
				local orderInfo = {
					Recipe = recipeName,
					Amount = amount,
				}

				local canImprove = recipes[recipeName].CanImprove
				if ( canImprove and canImprove == true ) then
					orderInfo.Material = material
				end
				success = 1
				return orderInfo
			end
		end
		tries = tries + 1
    until ( tries >= 30 or success == 1 )
end

function TryGetSkillAppropriateMaterialsFromRecipe(user, craftOrderSkill, recipeName, skillLevel)
	local recipeData = AllRecipes[craftOrderSkill][recipeName]
	if ( recipeData and recipeData.Resources ) then
		local resourceName = nil
		local validResources = {}
		for resource, value in pairs(recipeData.Resources) do
			if ( type(value) == "number" ) then
				resourceName = tostring(resource)
			elseif ( type(value) == "table" ) then
				for resource2, value2 in pairs(value) do
					resourceName = tostring(resource2)
				end
			end
			if ( IsPrimaryMaterial(craftOrderSkill, resourceName) ) then
				local skillRequired, maxGain = GetRecipeSkillRequired(recipeName, resourceName)
				--DebugMessage(skillLevel, ">=", skillRequired, "?", (skillLevel >= skillRequired), ",", skillRequired, ">=", math.max(skillLevel-50,0), "?", (skillRequired >= math.max(skillLevel-50,0)))
				if ( skillRequired and skillLevel >= skillRequired and skillRequired >= math.max(skillLevel-50,0) ) then
					local needsRecipe = AllRecipes[craftOrderSkill][resourceName].NeedRecipe
					if ( (not needsRecipe or needsRecipe == false) or HasRecipe(user, resourceName) ) then
						local skillRequired, maxGain = GetRecipeSkillRequired(resourceName, resourceName)
						if ( skillLevel >= skillRequired ) then
							table.insert(validResources, resourceName)
						end
					end
				end
			end
		end
		return validResources
	end
end

function IsPrimaryMaterial(craftSkill, materialName)
	local materialTable = MaterialIndex[craftSkill]
	for i = 1, #materialTable do
		if ( materialTable[i] == materialName ) then
			return true
		end
	end
	return false
end

function GetMaterialCountForRecipe(craftOrderSkill, recipe, material)
	local recipeData = AllRecipes[craftOrderSkill][recipe]
	local materialsPerCraft = nil
	if ( recipeData and recipeData.Resources and recipeData.Resources[material] ) then
		local materialCount = recipeData.Resources[material]
		if ( type(materialCount) == "number" ) then
			materialsPerCraft = materialCount
		elseif ( type(materialCount) == "table" ) then
			materialsPerCraft = materialCount[material]
		end
		return materialsPerCraft
	else
		LuaDebugCallStack("No material count for recipe: "..recipe.." material: "..material.." craftOrderSkill: "..craftOrderSkill)
		--DebugThis(recipeData,"recipeData")
	end
end

function GetCraftOrderScore(craftOrderSkill, orderInfo)
	if ( craftOrderSkill ) then
		if ( orderInfo and next(orderInfo) ) then
			local amount = orderInfo.Amount or 1
			local recipe = orderInfo.Recipe
			local material = orderInfo.Material
			-- if no material specified on order, use base
			if ( not material ) then
				if ( craftOrderSkill == "MetalsmithSkill" ) then
					material = "Iron"
				elseif ( craftOrderSkill == "WoodsmithSkill" ) then
					material = "Boards"
				elseif ( craftOrderSkill == "FabricationSkill" ) then
					--DebugMessage( "Recipe : " .. recipe )
					local resources = AllRecipes[craftOrderSkill][recipe].Resources
					for resource, count in pairs(resources) do
						if resource == "Cloth" then 
							material = "Cloth" 
						elseif resource == "Leather" then
							material = "Leather"
						end
					end
				end
			end
			local materialsPerCraft = GetMaterialCountForRecipe(craftOrderSkill, recipe, material)
			local skillRequired, maxGain = GetRecipeSkillRequired(recipe, material)
			if ( skillRequired == 0 ) then skillRequired = 5 end
			local materialValue = CraftingOrders.MaterialValues[material]
			
			local finalScore = materialsPerCraft * materialValue * amount * skillRequired
			return finalScore
		end
	end
end

function OrderTierToScore(craftOrderSkill, tier)
	return ( math.sqrt(GetMaxCraftOrderScore(craftOrderSkill)) * ( tier / 10 ) ^ 2 ) ^ 2
end

function OrderScoreToTier(craftOrderSkill, score)
	if ( score ) then
		return math.min(math.round((math.sqrt( math.sqrt(score) / math.sqrt(GetMaxCraftOrderScore(craftOrderSkill)) ) * 10)), 10)
	end
end

function GetMaxCraftOrderScore(craftOrderSkill)
	local mostDifficult = CraftingOrders.MostDifficult[craftOrderSkill]
	local skill = 100
	local materialsPerCraft = GetMaterialCountForRecipe(craftOrderSkill, mostDifficult.Recipe, mostDifficult.Material)
	local materialValue = CraftingOrders.MaterialValues[mostDifficult.Material]
	local amount = CraftingOrders.BaseAmount
	return materialsPerCraft * materialValue * amount * skill
end

function GetCraftOrderItemName(orderInfo)
    if (orderInfo.Material ~= nil) then
        return orderInfo.Amount.." "..ResourceData.ResourceInfo[orderInfo.Material].CraftedItemPrefix.." "..GetItemNameFromRecipe(orderInfo.Recipe)
    else
        return orderInfo.Amount.." "..GetItemNameFromRecipe(orderInfo.Recipe)
	end
end

function CraftingOrderReward(user, orderObj, orderSkill, orderInfo)

	--SCAN ADDED CHECK CRAFT ORDER TIMER FUNCTION:
	if ( user:HasTimer("RecentCraftOrder") ) then
		user:SystemMessage("Craft orders can only be completed once, every 20 minutes.", "info")
		EndMobileEffect(root)
		return false

	--
	else
	local backpack = user:GetEquippedObject("Backpack")
	local orderSearch = FindItemInContainerRecursive(backpack, function(item)
        if ( item and item:IsValid()  ) then--and item == orderObj
            return true
        end
    end)
	if ( not orderSearch ) then
		return false
	end
	orderObj:Destroy()
	local message = "Handed in a crafting order."
	user:SystemMessage(message, "info")
	user:SystemMessage(message)
	--DebugMessage(tostring("Crafter Order "..orderObj:GetCreationTemplateId()).." turned in")
	-- give coin
	local score = GetCraftOrderScore(orderSkill, orderInfo)
	local tier = GetCraftOrderRewardTier(orderSkill, orderInfo)
	local coins = math.round(score * CraftingOrders.CoinsPerScore)
	Create.Coins.InBackpack(user, coins)
	local message = "Received coins("..coins..")"
	user:SystemMessage(message, "info")
	user:SystemMessage(message)

	local skillLevel = GetSkillLevel(user, orderSkill)
	local recipeChance = ( 100 / skillLevel ) * ( tier / (skillLevel / 10) ) * 30
	local random = math.random(1,100)
	if ( random <= recipeChance ) then
		-- give recipe
		local recipes = CraftingOrders.RecipeRewards[orderSkill]
		local index = math.random(1, #recipes)
		local count = 0
		local valid = 0
		repeat
			count = count + 1
			local recipe = GetTemplateObjVar(recipes[index], "Recipe")
				local hasRecipe = HasRecipe(user, recipe)
				if ( not hasRecipe or hasRecipe == false or math.random(1,100) <= 5 ) then
					local skillRequired, maxGain = GetRecipeSkillRequired(recipe)
					if ( skillRequired and skillLevel >= skillRequired ) then
						valid = 1
					end
				end
			if ( valid ~= 1 ) then
				if ( index >= #recipes ) then index = 1 else index = index + 1 end
			end
		until ( valid == 1 or count >= #recipes )
		if ( valid == 1 ) then Create.Stack.InBackpack(recipes[index], user, 1) end
	end

	-- give lewt
	local backpack = user:GetEquippedObject("Backpack")
	local lootTables = {TemplateDefines.LootTable[orderSkill.."Tier"..tier]}
	local items = LootTables.SpawnLoot(lootTables,backpack,nil,true,nil,true)

	CheckAchievementStatus(user, "Crafting", "CraftingOrder", 1)
	user:SendMessage("OnDailyRewardTaskComplete","crafting")

	--SCAN ADDED SET CRAFT ORDER TIMER FUNCTION:
	user:ScheduleTimerDelay(TimeSpan.FromMinutes(20), "RecentCraftOrder")
	
	--SCAN ADDED TIMER FUNCTION:
	local TargetUser = user
	ProgressBar.Show(
            {
                TargetUser = TargetUser,
                Label = "Craft Order Cooldown",
                Duration = TimeSpan.FromMinutes(20),
                PresetLocation = "AboveHotbar",
                DialogId = "CraftOrder",
            })

	return tier, coins, items
	end
end

function SimulateCOs(user, craftOrderSkill, iterations, highestOfThree)
	print("--- Simulating CO Handout ---")
	user:SystemMessage("--- Simulating CO Handout ---")
	local iterations = iterations or 1
	RegisterEventHandler(EventType.CreatedObject, "CreatedCraftingOrder", 
	function (success, objRef, orderInfo, user)
		objRef:SetObjVar("OrderInfo", orderInfo)
		objRef:SetObjVar("OrderOwner", user)
		objRef:SetObjVar("OrderSkill", craftOrderSkill)
		--hack because name isn't getting set properly by CreateObjInContainer
		CallFunctionDelayed(TimeSpan.FromSeconds(2),function()
			if ( objRef:IsValid() ) then
				objRef:SetName(objRef:GetName())
			end
		end)
	end)
	local stats = {}
	for i = 1, iterations do
		local order = PickCraftingOrder(user, craftOrderSkill)
		if ( order ) then
			local scoreOne = GetCraftOrderScore(craftOrderSkill, order)
			if ( highestOfThree and highestOfThree ~= false ) then
				local orderTwo = PickCraftingOrder(user, craftOrderSkill)
				local scoreTwo = GetCraftOrderScore(craftOrderSkill, orderTwo)
				if ( scoreTwo > scoreOne ) then order = orderTwo end
				local orderThree = PickCraftingOrder(user, craftOrderSkill)
				local scoreThree = GetCraftOrderScore(craftOrderSkill, orderThree)
				if ( scoreThree > scoreTwo ) then order = orderThree end
			end
			CreateCraftingOrder(user, order)
			local tier = GetCraftOrderRewardTier(craftOrderSkill, order)
			if ( not stats[tostring(tier)] ) then stats[tostring(tier)] = {} end
			local count = stats[tostring(tier)]["Tier Count"] or 0
			stats[tostring(tier)]["Tier Count"] = count + 1
			local orderMaterial = order.Material or "Basic"
			local count = stats[tostring(tier)][orderMaterial.." "..order.Recipe] or 0
			stats[tostring(tier)][orderMaterial.." "..order.Recipe] = count + 1
		end
	end
	if ( stats and next(stats) ) then
		for stat, value in pairs(stats) do
			--print("Tier "..stat.."")
			--user:SystemMessage("Tier "..stat.."")
			--io.write( "Tier = {".. tostring(value) .."} \n" )
		end
	end
	ClientDialog.Show{
		TargetUser = user,
		DialogId = "GetRewards",
		TitleStr = "GetRewards",
		DescStr = "Press confirm to simulate rewards in the debug console.",
		Button1Str = "Confirm",
		Button2Str = "Cancel",
		ResponseObj = user,
		ResponseFunc = function( user, buttonId )
			buttonId = tonumber(buttonId)
			if ( buttonId == 0 ) then
				print("--- Simulating CO Rewards ---")
				user:SystemMessage("--- Simulating CO Rewards ---")
				local backpackObj = user:GetEquippedObject("Backpack")
				if(backpackObj) then
					craftingOrders = FindItemsInContainerByTemplateRecursive(backpackObj,"crafting_order")
					local stats = {}
					local MyFile = io.open("essence_generation/craftorders.txt", "w+")
					io.output(MyFile)
					for i = 1, #craftingOrders do	
						local orderInfo = craftingOrders[i]:GetObjVar("OrderInfo")
						local tier, coins, items = CraftingOrderReward(user, craftingOrders[i], craftOrderSkill, orderInfo)
						if ( not stats[tostring(tier)] ) then stats[tostring(tier)] = {} end
						if ( items and next(items) ) then
							for i = 1, #items do
								local count = stats[tostring(tier)][items[i]] or 0
								stats[tostring(tier)][items[i]] = count + 1
								local count = stats[tostring(tier)]["Tier Count"] or 0
								stats[tostring(tier)]["Tier Count"] = count + 1
								local materialCost = stats[tostring(tier)]["Material Cost"] or 0
								local material = orderInfo.Material
								local recipe = orderInfo.Recipe
								-- if no material specified on order, use base
								if ( not material ) then
									if ( craftOrderSkill == "MetalsmithSkill" ) then
										material = "Iron"
									elseif ( craftOrderSkill == "WoodsmithSkill" ) then
										material = "Boards"
									elseif ( craftOrderSkill == "FabricationSkill" ) then
										local resources = AllRecipes[craftOrderSkill][recipe].Resources
										for resource, count in pairs(resources) do
											if resource == "Cloth" then 
												material = "Cloth" 
											elseif resource == "Leather" then
												material = "Leather"
											end
										end
									end
								end
								stats[tostring(tier)]["Material Cost"] = materialCost + (GetMaterialCountForRecipe(craftOrderSkill, orderInfo.Recipe, material) * (CraftingOrders.MaterialValues[material]) or 0) * orderInfo.Amount
								if ( coins ) then
									local count = stats[tostring(tier)]["Coins"] or 0
									stats[tostring(tier)]["Coins"] = count + coins
									local score = GetCraftOrderScore(craftOrderSkill, orderInfo)
									local resourceNum = 0							
									DebugMessage("Recipe : " .. orderInfo.Recipe .. " : ".. orderInfo.Material)
									if( type(AllRecipes.FabricationSkill[orderInfo.Recipe]["Resources"][orderInfo.Material]) == "table" ) then
										resourceNum = AllRecipes.FabricationSkill[orderInfo.Recipe]["Resources"][orderInfo.Material][orderInfo.Material]
									else
										resourceNum = AllRecipes.FabricationSkill[orderInfo.Recipe]["Resources"][orderInfo.Material]
									end
									
									io.write( tostring(tier)..", "..tostring(score)..","..tostring(coins)..","..tostring(orderInfo.Material)..","..tostring(orderInfo.Amount)..","..tostring((tonumber(orderInfo.Amount) * tonumber(resourceNum)))..","..orderInfo.Recipe.."\n" )
								end
							end
						end
					end
					io.close(MyFile)

					if ( stats and next(stats) ) then
						for stat, value in pairs(stats) do
							--io.write( "Tier = {".. tostring(value) .."} \n" )
						end
					end
				end
			end
		end,
	}
end

--[[
deprecated

function RewardTest(craftOrderSkill, orderScore)
	for i=1,100000 do
		PickCraftOrderReward(craftOrderSkill, orderScore)
	end
end

function VerifyCraftingOrderRecipeRewards(craftOrderDefinesTable)

	local passedTest = true

	for recipeTableIndex, recipeTemplate in pairs(craftOrderDefinesTable.RecipeRewards) do
		local templateData = GetTemplateData(craftOrderDefinesTable.RecipeRewards[recipeTableIndex])
		if (templateData == nil) then
			DebugMessage("> Recipe '"..craftOrderDefinesTable.RecipeRewards[recipeTableIndex].."' does not exist")
			passedTest = false
		else
			--DebugMessage(craftOrderDefinesTable.RecipeRewards[recipeTableIndex])
		end	
	end

	if (passedTest) then
		DebugMessage("> All recipe reward templates are valid")
	end
end

function VerifyCraftOrderRewards(craftOrderDefinesTable)
	local rewardTable = craftOrderDefinesTable.Rewards
	
	local passedTest = true

	for i=1,#rewardTable do
		for k=1,#rewardTable[i] do
			local rewardInfo = rewardTable[i][k]
			local rewardTemplateData = GetTemplateData(rewardInfo.Template)
			if (rewardTemplateData == nil) then
				DebugMessage("> Reward template '", rewardInfo.Template, "' does not exist")
				passedTest = false
			end
		end
	end

	if (passedTest) then
		DebugMessage("> All reward templates are valid")
	end
end

function GenerateCraftOrderCSV()
	local craftOrderFile = io.open("craftorders.csv","w")

	local craftOrderSkills =
	{
		"MetalsmithSkill",
		"FabricationSkill",
		"WoodsmithSkill",
	}
	io.output(craftOrderFile)

	io.write("Skill,Recipe,Amount,Material,Score,RewardTier,CoinAmount".."\n")
	for k=1,#craftOrderSkills do
		local craftOrderSkill = tostring(craftOrderSkills[k])
		--DebugMessage(DumpTable(CraftingOrderDefines[craftOrderSkill]))
		--DebugMessage(#CraftingOrderDefines[craftOrderSkill].CraftingOrders)
		local craftOrderTable = GenerateCraftOrderTable(craftOrderSkill)
		for i=1,#craftOrderTable do
			local orderInfo = craftOrderTable[i]
			io.write(craftOrderSkill..","..orderInfo.Recipe..","..tostring(orderInfo.Amount)..","..tostring(orderInfo.Material or "")..","..tostring(orderInfo.Score)..","..tostring(GetCraftOrderRewardTier(craftOrderSkill, orderInfo.Score))..","..tostring(PickCraftOrderCoinReward(orderInfo.Score)).."\n")
		end

	end
	io.close(craftOrderFile)
end

function CraftOrderTest(user, craftOrderSkill, rolls)
	rolls = rolls or 100
	local orderList = {}
	DebugMessage("---RUNNING CRAFT ORDER TEST---")
	DebugMessage("Picking orders")
	for i=1,rolls do
		local orderInfo = PickCraftingOrder(user,craftOrderSkill)
		table.insert(orderList, orderInfo)
	end
	DebugMessage("Orders picked!")

	local fileName = craftOrderSkill.."CraftOrderTest.csv"
	local craftOrderFile = io.open("export/"..fileName,"w")
	io.output(craftOrderFile)
	local outputString = "Craft Order,RewardTier,Reward,Stack,Coins\n"
	for i, j in pairs(orderList) do
		local craftOrderReward = PickCraftOrderReward(craftOrderSkill, j.Score)

		local orderString =
		GetCraftOrderItemName(j)..','..
		GetCraftOrderRewardTier(craftOrderSkill, j.Score)..','..
		craftOrderReward.Template..','


		if (craftOrderReward.StackMin) then
			orderString = orderString..tostring(math.random(craftOrderReward.StackMin, craftOrderReward.StackMax))..','
		else
			orderString = orderString.."1"..','
		end
		
		orderString = orderString..
		PickCraftOrderCoinReward(j.Score)..
		'\n'

		DebugMessage(orderString)
		outputString = outputString..orderString
	end
	io.write(outputString)
	io.close(craftOrderFile)
	DebugMessage("Craft order test complete!")
end

function VerifyCraftOrders(craftOrderSkill)
	local craftOrderSkillTable = CraftingOrderDefines[craftOrderSkill]
	if (craftOrderSkillTable == nil) then return end
	DebugMessage("--"..craftOrderSkill.." Craft Order Test--")
	VerifyCraftOrderRewards(craftOrderSkillTable)
	VerifyCraftingOrderRecipeRewards(craftOrderSkillTable)
end

CraftingOrderDefines.MetalsmithSkill.CraftingOrders = GenerateCraftOrderTable("MetalsmithSkill")
CraftingOrderDefines.FabricationSkill.CraftingOrders = GenerateCraftOrderTable("FabricationSkill")
CraftingOrderDefines.WoodsmithSkill.CraftingOrders = GenerateCraftOrderTable("WoodsmithSkill")
]]