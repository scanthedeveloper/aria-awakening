
CombatDamageType = {
    MAGIC = {
        Magic = true,
    },
	Frost = {
        Magic = true,
    },
    Fire = {
        Magic = true,
    },
    Energy = {
        Magic = true
    },
    Sorcery = 
    {
        Magic = true,
        SkipPower = true,
        SkipCastInterrupt = true,
    },
    MagicSkipPower = 
    {
        Magic = true,
        SkipPower = true
    },
    Void = {
        Magic = true
    },
    Poison = {
        Magic = true,
        NoGuards = true, -- this damage type does not call guards
    },
    Bashing = {
        Physical = true,
        Durability = true,
        Blockable = true,
    },
    Slashing = {
        Physical = true,
        Durability = true,
        Blockable = true,
    },
    Piercing = {
        Physical = true,
        Durability = true,
        Blockable = true,
    },
    Bow = {
        Physical = true,
        Durability = true,
        Blockable = true,
    },
    Bleed = {
        NoGuards = true,
    },
	TrueDamage = {},
}

function CalculateDamageInfo(damageInfo)

    --DebugTable( damageInfo )
    if not( damageInfo.Victim ) then return damageInfo end
    if not( damageInfo.Damage ) then damageInfo.Damage = 1 end
	if not( damageInfo.WeaponType ) then damageInfo.WeaponType = "BareHand" end
	if not( damageInfo.WeaponClass ) then damageInfo.WeaponClass = "Fist" end

	local typeData = CombatDamageType[damageInfo.Type]
	-- not a real combat damage type, stop here.
    if not( typeData ) then return end
    
    -- calculate damage blocked if blockable otherwise set to 0
	damageInfo.Blocked = typeData.Blockable and CalculateBlockDefense(damageInfo) or 0
    
    if ( typeData.Magic ) then

        if( not typeData.SkipPower ) then
            local nPower = damageInfo.Attacker:GetStatValue("Power")
            if( damageInfo.ScrollUsed ) then
                nPower = math.max( nPower, ServerSettings.Combat.ScrollCastPower )
            end
            damageInfo.Damage = ( nPower * damageInfo.Damage) / 8
        end

        -- calculate variance if not passed in
        if not( damageInfo.Variance ) then
            if ( damageInfo.Source and damageInfo.VictimIsPlayer ) then
                -- KH TODO: Make this configurable
                damageInfo.Variance = 0.05
            else
                -- DAB COMBAT CHANGES: Make this configurable
                damageInfo.Variance = 0.05
            end
        end

        damageInfo.Damage = randomGaussian(damageInfo.Damage, damageInfo.Damage * damageInfo.Variance)

        if ( CheckActiveSpellResist(damageInfo.Victim) ) then
            -- successful magic resist, half base damage
            damageInfo.Damage = damageInfo.Damage * 0.5
        end

        if not( damageInfo.VictimIsPlayer ) then
            -- mobs take 150% spell damage.
            damageInfo.Damage = damageInfo.Damage * 1.5
        end
    end

    if ( typeData.Physical ) then
        if not( damageInfo.Attack ) then
            if ( damageInfo.Attacker and damageInfo.Attacker:IsValid() ) then
                damageInfo.Attack = damageInfo.Attacker:GetStatValue("Attack")
            else
                LuaDebugCallStack("ERROR: Attempting to calculate physical damage with no attacker")
            end
        end
    
        local defense = 45
        if ( HasMobileEffect(damageInfo.Victim, "Sunder") ) then
            -- end sunder, no armor defense reduction this hit
            damageInfo.Victim:SendMessage("EndSunderEffect")
        else
            defense = math.max(damageInfo.Victim:GetStatValue("Defense") or 0, defense)
        end
    
        damageInfo.Damage = ( damageInfo.Attack * 70 ) / (defense + (damageInfo.Blocked or 0))
    
        --DebugMessage("DO IT",tostring(finalDamage),tostring(damageInfo.Attack),tostring(defense))
    
        -- calculate variance if not passed in
        if not( damageInfo.Variance ) then
            if ( damageInfo.Source and damageInfo.VictimIsPlayer ) then
                damageInfo.Variance = EquipmentStats.BaseWeaponStats[damageInfo.WeaponType] and EquipmentStats.BaseWeaponStats[damageInfo.WeaponType].Variance or 0
            else
                -- DAB COMBAT CHANGES: Make this configurable
                damageInfo.Variance = 0.20
            end
        end

        damageInfo.delaySeconds = EquipmentStats.BaseWeaponStats[damageInfo.WeaponType].delaySeconds
    
        --DebugMessage("DAMAGE: "..tostring(finalDamage))
        --DebugMessage("DAMAGE: "..tostring(finalDamage),tostring(damageInfo.Attack),tostring(defense),tostring(victimArmorRating),tostring(GetCombatMod(CombatMod.DefenseTimes,1)),tostring(GetCombatMod(CombatMod.DefensePlus)),tostring(victimArmorBonus))
    
        damageInfo.Damage = randomGaussian(damageInfo.Damage, damageInfo.Damage * damageInfo.Variance)
        --DebugMessage("VARIANCE",tostring(finalDamage),tostring(damageInfo.Variance))
    
        if ( damageInfo.Owner ~= nil ) then
            -- add damage buff to pet attacks
            if ( damageInfo.Owner:IsValid() and damageInfo.Victim:DistanceFrom(damageInfo.Owner) <= ServerSettings.Pets.Command.Distance ) then
                damageInfo.Damage = damageInfo.Damage * (0.5 + (1 * (GetSkillLevel(damageInfo.Owner, "BeastmasterySkill") / ServerSettings.Skills.PlayerSkillCap.Single) ) )
            end
        elseif ( damageInfo.Source ) then
            local executioner = damageInfo.Source:GetObjVar("Executioner")
            if ( executioner ~= nil and executioner == damageInfo.Victim:GetObjVar("MobileKind") ) then
                damageInfo.Damage = damageInfo.Damage * (
                    ServerSettings.Executioner.LevelModifier[
                        damageInfo.Source:GetObjVar("ExecutionerLevel") or 1
                    ] or 1
                )
            end
        end
    end
    
    if ( typeData.Durability ) then ApplyDamageDurability(damageInfo) end
    
    return damageInfo
end

function IsMagicDamageType(damageType)
    return damageType == "MAGIC" or damageType == "FIRE" or damageType == "FROST" or damageType == "ENERGY"
    or damageType == "VOID"
end 

function ApplyDamageDurability(damageInfo)
    if not( damageInfo.Victim ) then return end

    -- jewelry always has a chance to be damaged.
    if ( Success(ServerSettings.Durability.Chance.OnJewelryHit) ) then
        local item = damageInfo.Victim:GetEquippedObject(JEWELRYSLOTS[math.random(1,#JEWELRYSLOTS)])
        if ( item ) then
            AdjustDurability(item, -1)
        end
    end

    -- Shields always have a chance to be damaged.
    local shield = damageInfo.Victim:GetEquippedObject("LeftHand") or nil
    local shieldType = GetShieldType(shield)
    if ( shieldType and EquipmentStats.BaseShieldStats[shieldType] and Success(ServerSettings.Durability.Chance.OnEquipmentHit) ) then
        AdjustDurability(shield, -1)
    end

    -- all but true damage/poison will also hurt the equipment.
    damageInfo.Slot = damageInfo.Slot or GetHitLocation()
    local hitItem = damageInfo.Victim:GetEquippedObject(damageInfo.Slot)
    local soundParameter = 0
    if ( hitItem ~= nil ) then
        local hitArmorType = GetArmorType(hitItem)
        soundParameter = GetArmorSoundParameter(hitArmorType)	

        if ( damageInfo.VictimIsPlayer ) then
            -- damage equipment that was hit
            if ( Success(ServerSettings.Durability.Chance.OnEquipmentHit) ) then
                AdjustDurability(hitItem, -1)
            end

            --[[
            -- perform proficiency check
            local armorClass = GetArmorClassFromType(hitArmorType)
            if ( (armorClass == "Light" or armorClass == "Heavy") and ValidCombatGainTarget(damageInfo.Attacker, damageInfo.Victim) ) then
                CheckSkill(damageInfo.Victim, string.format("%sArmorSkill", armorClass), GetSkillLevel(damageInfo.Attacker, EquipmentStats.BaseWeaponClass[damageInfo.WeaponClass].WeaponSkill))					
            end
            ]]
        end
    end

    if not(IsMagicDamageType(damageInfo.Type)) then
        PlayImpactSound(damageInfo.Victim, "armor", soundParameter, damageInfo.Attacker, damageInfo.delaySeconds)
    end
end

function CalculateBlockDefense(damageInfo)
	Verbose("Combat", "CalculateBlockDefense", damageInfo)
	local shield = damageInfo.Victim:GetEquippedObject("LeftHand")
	if not( shield ) then return 0 end
	local shieldType = GetShieldType(shield)
	if ( shieldType and EquipmentStats.BaseShieldStats[shieldType] ) then
		-- skill gain dependant on the attacker's weapon skill level
		CheckSkill(damageInfo.Victim, "BlockingSkill", GetSkillLevel(damageInfo.Attacker,EquipmentStats.BaseWeaponClass[damageInfo.WeaponClass].WeaponSkill))
		if ( Success(GetSkillLevel(damageInfo.Victim, "BlockingSkill")/335) ) then
            damageInfo.Victim:PlayAnimation("block")
            damageInfo.Victim:NpcSpeech("[08FFFF]*blocked*[-]","combat")
            damageInfo.Victim:PlayObjectSound("event:/weapons/shield/shield_wood_block")
			return math.max(0, (EquipmentStats.BaseShieldStats[shieldType].ArmorRating or 0))  --GetMagicItemArmorBonus(damageInfo.WeaponClass))
		end
    end
    return 0
end
