SorceryHelper = {}
SorceryHelper.HarmfulAbilityRange = 12
SorceryHelper.SummonRadius = 4
SorceryHelper.SummonDuration = 20
SorceryHelper.SummonTargetRange = 15
SorceryHelper.MaxSummonDistance = 20
SorceryHelper.WispArmorIncrease = 35 -- how much damaged mitigation is added per wisp
SorceryHelper.WispArmorMaxValue = (SorceryHelper.WispArmorIncrease * 4) -- four wisps worth
SorceryHelper.MagicMissleMinMaxDamage = { 15, 30 }
SorceryHelper.MaxAfflictionStacks = 3
SorceryHelper.AfflictionIncreasePerStacks = 0.2 --20%
SorceryHelper.StarfallMinMaxDamage = { 25, 50 }
SorceryHelper.BeamMinMaxDamage = { 7, 15 }
SorceryHelper.ConsumeWispHealAmount = 50
SorceryHelper.MaximumSummons = 
{
    monolith_fox_summon = 4,
    monolith_construct_summon = 2,
}

SorceryHelper.GetMaximumWispsAllowed = function( _player )
    if( not _player ) then return 0 end

    skillLevel = GetSkillLevel(_player, "SummoningSkill")
    local wispCount = 0
    if( skillLevel < 60 ) then
        wispCount = 1
    elseif( skillLevel >= 60 and skillLevel < 90 ) then
        wispCount = 2
    elseif( skillLevel >= 90 and skillLevel < 100 ) then
        wispCount = 3
    elseif( skillLevel == 100 ) then
        wispCount = 4
    end

    local weaponObj = _player:GetEquippedObject("RightHand")
    if( weaponObj ) then
        return math.min(wispCount, (weaponObj:GetObjVar("WispCount") or 0))
    else
        return 0
    end
end

SorceryHelper.GetSummonDuration = function( _player, _baseDuration )
    skillLevel = GetSkillLevel(_player, "SummoningSkill")
    return math.floor( ( _baseDuration * (1 + skillLevel/100) ))

end

SorceryHelper.CanSummon = function( _player, _template )
    local summonCount = SorceryHelper.GetSummonsCount( _player, _template )
    return (summonCount < SorceryHelper.MaximumSummons[_template]) 
end

SorceryHelper.GetAllSummons = function( _player )
    return FindObjects(SearchMulti({SearchRange(_player:GetLoc(),25),SearchHasObjVar("IsSummonedPet"),SearchObjVar("controller", _player)}))
end

SorceryHelper.GetSummonsCount = function( _player, _template )
    local count = 0
    local allSummons = SorceryHelper.GetAllSummons( _player )

    for i,summon in pairs(allSummons) do 
        if( summon:GetCreationTemplateId() == _template ) then
            count = count + 1
        end
    end

    return count
end

-- Toggle the socery effect off/on based on the weapon the player has equipped
SorceryHelper.ToggleSorceryEffect = function( playerObj, weaponObj )  
    -- If we don't have a player, get out of here
    if( not playerObj ) then return false end
    weaponObj = weaponObj or playerObj:GetEquippedObject("RightHand")

    if( weaponObj and weaponObj:HasObjVar("WispCount") ) then
        local hasEffect = HasMobileEffect( playerObj, "Sorcery" )
        if( not hasEffect ) then
            playerObj:SendMessage("StartMobileEffect", "Sorcery", playerObj )
        else
            SorceryHelper.SorceryMessageSender( playerObj, "RemoveAllWisps" )
        end
    end

    return true
end

-- Sends message to remove the mobile effect from a player
SorceryHelper.RemoveMobileEffect = function( playerObj )
    SorceryHelper.SorceryMessageSender( playerObj, "EndMobileEffect" )
end

-- Trys to consume a wisp on the given sorcerer
SorceryHelper.TryConsumeWisp = function( playerObj, count, cb )
    if( not cb ) then cb = function()end end 
    if( not playerObj and ( not count or count < 1 ) ) then cb( false ) end
    local messageTag = "TryConsumeWisp"

    RegisterSingleEventHandler(EventType.Message, messageTag, 
    function( success, wispsConsumed, doNotTriggerCooldown, increaseSummonPower, beamCooldownReduction  )
        if(success == true ) then
            CheckSkillChance(playerObj, "SorcerySkill")
        end
        cb( success, wispsConsumed, doNotTriggerCooldown, increaseSummonPower, beamCooldownReduction )
    end)

    SorceryHelper.SorceryMessageSender( playerObj, "ConsumeWisp", { Count = count, MessageTag = messageTag, Player = playerObj } )
end

SorceryHelper.GetSummonSpawnLocation = function( playerObj, _radius )
    isDungeon = IsDungeonMap()
    _radius = _radius or SorceryHelper.SummonRadius
    if(isDungeon) then
        return playerObj:GetLoc()
        --return GetRandomDungeonSpawnLocationInRange(playerObj:GetLoc(), _radius)
    else
        for i=1,30 do 
            local loc = GetRandomPassableLocationInRadius(playerObj:GetLoc(), _radius, true)
            if( playerObj:HasLineOfSightToLoc(loc,ServerSettings.Combat.LOSEyeLevel)) then
                return loc
            end
        end
    end

    return playerObj:GetLoc()
end

-- Returns true if a wisp is able to make it from the player to the target
SorceryHelper.CanSendWispToTargetMobile = function( playerObj, targetObj )
    return playerObj:HasLineOfSightToObj(targetObj,ServerSettings.Combat.LOSEyeLevel)
end

-- Sends a wisp to a given target and performed a callback when it gets there
SorceryHelper.SendWispToTargetMobile = function( wispsConsumed, playerObj, targetObj, travelTimeMS, cb )
    cb = cb or function() end

    --DebugMessage("Wisps Consumed: ")
    --DebugTable(wispsConsumed)

    -- If no path exists, return false to the callback
    local travelSuccess = false

    -- If we can path to the target, lets do it!
    if( SorceryHelper.CanSendWispToTargetMobile(playerObj, targetObj) ) then
        travelSuccess = true
        
        -- Trigger the callback to be called when the wisp should arrive at the target
        CallFunctionDelayed(TimeSpan.FromMilliseconds(travelTimeMS), function() 
            targetObj:PlayEffect("WispExplosionEffect")
            cb(travelSuccess)
        end)

        -- MATT; THIS IS WHERE YOU WOULD DO THE ANIMATIONS AND WHATNOT TO GET THE WISP WHERE IT NEEDS TO GO!
        -- Code here to make the wisp path from the playerObj to the targetObj in the travelTimeMS
        local speed = travelTimeMS/playerObj:DistanceFrom(targetObj) 

        for i=1,#wispsConsumed do
            playerObj:PlayProjectileEffectTo("WispEffect", targetObj, speed, travelTimeMS / 1000, "WispIndex="..tostring(wispsConsumed[i]), 0)
        end
        
    else -- wisp couldn't make it!
        cb( travelSuccess )
    end
end

-- Returns true if a wisp is able to make it from the player to the location
SorceryHelper.CanSendWispToTargetLocation = function( playerObj, targetLoc )
    return playerObj:HasLineOfSightToLoc(targetLoc,ServerSettings.Combat.LOSEyeLevel)
end

-- Sends a wisp to a given location and performed a callback when it gets there
SorceryHelper.SendWispToTargetLocation = function( wispsConsumed, playerObj, targetLoc, travelTimeMS, cb )
    cb = cb or function() end

    -- If no path exists, return false to the callback
    local travelSuccess = false

    -- If we can path to the location, lets do it!
    if( SorceryHelper.CanSendWispToTargetLocation(playerObj, targetLoc) ) then
        travelSuccess = true
        
        -- Trigger the callback to be called when the wisp should arrive at the location
        CallFunctionDelayed(TimeSpan.FromMilliseconds(travelTimeMS), function() 
            cb(travelSuccess)
        end)

        -- MATT; THIS IS WHERE YOU WOULD DO THE ANIMATIONS AND WHATNOT TO GET THE WISP WHERE IT NEEDS TO GO!
        -- Code here to make the wisp path from the playerObj to the targetLoc in the travelTimeMS
        -- Speed = playerObj:DistanceFrom(targetObj) / travelTimeMS
    else -- wisp couldn't make it!
        cb( travelSuccess )
    end
end

-- Centralizes all sorcery messages into a single listener
SorceryHelper.SorceryMessageSender = function( playerObj, message, args )
    if( playerObj and playerObj:IsValid() ) then
        playerObj:SendMessage( "SorceryMessage", message, args )
        return true
    end

    return false
end

-- Creates a radius effect on the ground at target location and triggers callback when radius warning ends
SorceryHelper.DisplaySpellRadiusWarning = function( targetLoc, radius, helpful, durationMS, cb )
    cb = cb or function() end
    local seconds = math.min(durationMS / 1000, 2.0)
    durationMS = durationMS - (seconds * 1000)
    if( seconds > 0 ) then
        if( helpful ) then
            PlayEffectAtLoc( "GroundCircleGreen2", targetLoc, seconds, "Radius="..radius )
        else
            PlayEffectAtLoc( "GroundCircleRed2", targetLoc, seconds, "Radius="..radius )
        end
    end

    if( durationMS > 0 and seconds > 0 ) then
        --DebugMessage("CallFunctionDelay REPEAT: " .. tostring(seconds))
        CallFunctionDelayed(TimeSpan.FromSeconds(seconds),function ()
            SorceryHelper.DisplaySpellRadiusWarning( targetLoc, radius, helpful, durationMS, cb )
        end)
    else
        --DebugMessage("CallFunctionDelay CALLBACK: " .. tostring(seconds))
        CallFunctionDelayed(TimeSpan.FromSeconds(seconds),function ()
            cb(true)
        end)
    end
end

-- Sorcery Abilities
SorceryHelper.TriggerEchofall = function( args )
    SorceryHelper.DisplaySpellRadiusWarning( args.QueueLocation, "3.0", false, 1250, function(success)
        PlayEffectAtLoc("StarfallEffect", args.QueueLocation, 3)
        args.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_wall_of_fire")
        -- We do a delay to trigger the damage when the animation hits the ground
        CallFunctionDelayed(TimeSpan.FromMilliseconds(1000), function()
            args.ParentObj:StopObjectSound("event:/magic/fire/magic_fire_wall_of_fire")
            args.ParentObj:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact")
            local damageAmount = math.floor( math.min(math.max( ((GetSkillLevel( args.ParentObj, "SorcerySkill" ) / 100) * SorceryHelper.StarfallMinMaxDamage[2]) , SorceryHelper.StarfallMinMaxDamage[1]), SorceryHelper.StarfallMinMaxDamage[2]) )
            -- This is where the magic happens!
            local targets = GetNearbyCombatTargets( args.ParentObj, 3, args.QueueLocation, true )
            --DebugTable( targets )

            -- Damage our targets
            for i=1,#targets do 
                if( SorceryHelper.CanAoEHitTarget( targets[i], args ) ) then
                    SorceryHelper.DealSorceryDamageToTarget( args.ParentObj, targets[i], damageAmount )
                end 
            end
        end)
    end)
end

SorceryHelper.CanAoEHitTarget = function( _target, args )
    if( 
        not _target:HasLineOfSightToLoc(args.QueueLocation,ServerSettings.Combat.LOSEyeLevel ) -- Can the target see the location that was hit?
        or ShouldCriminalProtect(args.ParentObj, _target)
    ) then 
        return false 
    else 
        return true
    end
end

SorceryHelper.CanUseSorceryAbilities = function( _player, _ability )
    -- If they don't have the DLC they cannot do this!
    if not( HasDarkSorceryDlc(_player) )then 
        _player:SystemMessage("Must own Dark Sorcery DLC to use this ability.")
        return false 
    end

    -- Are we silenced?
    if( HasMobileEffect(_player, "Silence") ) then
        _player:SystemMessage("Cannot do that while silenced.")
        return false 
    end

    local abilityData = PrestigeData.Sorcery.Abilities[_ability]
    if( abilityData ) then
        if( abilityData.SkillsRequired and abilityData.SkillsRequired.SorcerySkill ) then
            local skillLevel = GetSkillLevel(_player, "SorcerySkill")
            if( skillLevel < abilityData.SkillsRequired.SorcerySkill ) then
                _player:SystemMessage("You need more Sorcery skill to use this.")
                return false 
            end
        end
    end


    return true
end

SorceryHelper.DealSorceryDamageToTarget = function( _player, _target, _damage )
    _target:SendMessage("ProcessTypeDamage", _player, _damage, "Sorcery", false, false)
end

SorceryHelper.DealTrueDamageToTarget = function( _player, _target, _damage )
    _target:SendMessage("ProcessTrueDamage", _player, _damage)
end

SorceryHelper.CanTargetBeHealed = function( _healer, _target )
    if( 
        ( _healer and _healer:IsValid() and _healer:IsPlayer() )
        and ( _target and _target:IsValid() ) 
        and _healer ~= _target
        and (HasMobileEffect(_target, "Sorcery") or HasMobileEffect(_target, "SorceryCooldown"))
    ) then
        _healer:SystemMessage("Your attempt to heal " .. _target:GetName() .. " has no effect.")
        return false
    end
    return true
end