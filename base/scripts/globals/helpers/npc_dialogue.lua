Dialogue = {}

-- @params: dialogue - Table of dialogue keys set in the root of a .lua module. Provides possible trees to climb.
-- @params: indices - Table of indices used to narrow down specific TraversalPoint within "dialogue" table. Finds which tree to climb.
-- @params: depth - Integer index to mark current point along the static_data tree based on TraversalPoint's key. Finds how high up the tree to climb.
-- @params: instruction - String compared with all branches of selected tree at selected height. Finds what branch to rest on at the height up the tree.
Dialogue.GoTo = function(npc, player, dialogue, indices, depth, instruction)
    local traversalPoint = Dialogue.GetTraversalPoint(dialogue, indices)
    if not (traversalPoint and npc and player) then DebugMessage("GoTo() FAILED",traversalPoint, npc, player) return end
    --DebugMessage("Dialogue.GoTo() -> traversal point:",traversalPoint,"at depth:",depth,"using instruction:",instruction)

    local potentialSpeech = {}
    local playerResponses = {}
    local totalSpeechPriority = 0

    indices = indices or {}
    depth = depth or 1
    instruction = instruction or ""

    Dialogue.SetTempVariables(npc, player, {Indices = indices, Depth = depth, Instruction = instruction })

    local isTableOfTrees = #indices < 1 or instruction == "G"
    local c = isTableOfTrees and #traversalPoint or 1
    for i=1, c do
        local entry = isTableOfTrees and traversalPoint[i] or traversalPoint
        local index = isTableOfTrees and i

        if Dialogue.IsTable(entry) then
            if entry.QuestName then
                -- Is Quest: entry.QuestName (string), entry.Steps (table of ints), entry.CanResume (bool)
                local readSpeech, readResponses = Dialogue.ReadQuestBranch(npc, player, entry, depth, instruction, index)
                totalSpeechPriority = Dialogue.AddBranchIfPossible(readSpeech, readResponses, potentialSpeech, playerResponses, totalSpeechPriority)
            else
                -- Is Group: entry is [1] groupName (string), [2] keys (table of strings/tables)
                local branch
                local readSpeech, readResponses = {}, {}
                local isGroupBase = type(entry[1]) == "string" and type(entry[2]) == "table" and #entry == 2
                if isTableOfTrees and isGroupBase then
                    branch = AllNPCDialogueTrees[Dialogue.GetLanguage()][entry[1]]
                    readSpeech, readResponses = Dialogue.ReadBranch(npc, player, branch, {Index = index})
                    totalSpeechPriority = Dialogue.AddBranchIfPossible(readSpeech, readResponses, potentialSpeech, playerResponses, totalSpeechPriority)
                else
                    indices[#indices + 1] = 2
                    Dialogue.GoTo(npc, player, dialogue, indices, depth, instruction)
                    return
                end
            end
        else
            -- Is Dialogue: entry is key (string)
            local readArgs = {Index = i}
            local divisorIndex = string.match(entry, ':')
            if divisorIndex then
                local entrySplits = StringSplit(entry, ':')
                entry = entrySplits[1]
                local commaSplits = StringSplit(entrySplits[2], ',')
                readArgs.OnBegin = commaSplits
            end

            local tree = AllNPCDialogueTrees[Dialogue.GetLanguage()][entry]
            local branch = (tree and tree.Branches and tree.Branches[depth]) or tree
            branch = branch[instruction] or branch

            local readSpeech, readResponses = Dialogue.ReadBranch(npc, player, branch, readArgs)
            totalSpeechPriority = Dialogue.AddBranchIfPossible(readSpeech, readResponses, potentialSpeech, playerResponses, totalSpeechPriority)
        end
    end
    
    if playerResponses and next(playerResponses) then
        Dialogue.DrawDialogueWindow(Dialogue.DecideNPCSpeech(potentialSpeech, totalSpeechPriority), npc, player, "NPCInteraction", playerResponses)
    else
        --DebugMessage("No Player Responses; Closing Dialogue Window.")
        player:CloseDynamicWindow("NPCInteraction")
    end
end

-- Processes "commandString" for new Instruction, runs final logic on previous Branch before traversing to new one.
-- @params: dialogue - The root table of dialogue keys set in a .lua module per-object. Usually called NPCDialogue.
-- @params: commandString - Must follow format of "[NewIndex]:[Instruction]|[InstructionSplitA]|[..SplitB]|.." or "[NewIndex]:[Instruction]" or just "[Instruction]"
Dialogue.ProcessCommandString = function(npc, player, dialogue, commandString)
    local temp = Dialogue.GetTempInteractionRecord(npc, player) or {}
    local depth = temp.Depth or 1
    temp.NeedContext = false
    local previousInstruction = temp.Instruction
    --DebugMessage("Dialogue.ProcessCommandString() CommandString='"..tostring(commandString).."'")
    
    local traversalPoint = Dialogue.GetTraversalPoint(dialogue, temp.Indices, 0, true)

    local commandSplits = StringSplit(commandString, ":")
    commandString = commandSplits[2] or commandString

    -- Traverses to new index, if there is one
    local newIndex = tonumber(commandSplits[1] or nil)
    if newIndex then
        if type(traversalPoint) == "table" and traversalPoint[newIndex] then
            temp.Indices = temp.Indices or {}
            temp.Indices[#temp.Indices+1] = newIndex
            --DebugMessage("NewIndex:",newIndex,"("..tostring(#temp.Indices).." total)")
            traversalPoint = traversalPoint[newIndex]
        end
    end

    -- Calls final logic from branch we are leaving, can alter the Instruction before the upcoming traversal.
    if type(traversalPoint) == "string" then
        local pointSplits = StringSplit(traversalPoint, ":")
        traversalPoint = pointSplits[1] or traversalPoint

        local tree = AllNPCDialogueTrees[Dialogue.GetLanguage()][traversalPoint]
        if tree then
            -- this is the branch we are leaving
            local branch = tree.Branches[depth] or tree
            branch = branch[previousInstruction] or branch
            if branch.InterpretInstruction then
                local overwriteDetails
                commandString, overwriteDetails = branch.InterpretInstruction(npc, player, commandString)
                if overwriteDetails then
                    temp.OverwriteDetails = overwriteDetails
                end
            end
        end
    end

    -- Recognizes multi-step newInstruction like 'GoTo|2|YES'
    local compoundSplits = StringSplit(commandString, "|")
    local newInstruction = compoundSplits[1] or commandString

    Dialogue.SetTempInteractionRecord(npc, player, temp)

    return {
        Depth = depth,
        Indices = temp.Indices,
        CompoundSplits = compoundSplits,
        PreviousInstruction = previousInstruction,
        Instruction = newInstruction,
    }
end

-- @params: dialogue - The root table of dialogue keys set in a .lua module per-object. Usually called NPCDialogue.
-- @params: commandInfo - table that must include Instruction, Depth, Indices, CompoundSplits, PreviousInstruction
Dialogue.GlobalProcessInstruction = function(npc, player, dialogue, commandInfo)
    local newInstruction = commandInfo.Instruction
    local success = true

    -- GLOBAL logic (that applies to any tree or branch) only. Branch-specific logic should be handled in the static_data/npc_dialogue.lua file.
    if newInstruction == "" or newInstruction == "X" or newInstruction == "Close" then
        Dialogue.ClearTempPlayerInteractionRecord(npc, player)
        player:CloseDynamicWindow("NPCInteraction")
    elseif newInstruction == "H" then
        --DebugMessage("---- HOME ----")
        Dialogue.GoTo(npc, player, dialogue, {}, 1, nil)
    elseif newInstruction == "G" then
        Dialogue.GoTo(npc, player, dialogue, commandInfo.Indices, commandInfo.Depth, newInstruction)
    elseif newInstruction == "R" then
        if commandInfo.Indices and commandInfo.Depth and commandInfo.PreviousInstruction then
            Dialogue.GoTo(npc, player, dialogue, commandInfo.Indices, commandInfo.Depth, commandInfo.PreviousInstruction)
            --DebugMessage("Returned")
        end
    elseif newInstruction == "GoTo" then
        Dialogue.GoTo(npc, player, dialogue, commandInfo.Indices, tonumber(commandInfo.CompoundSplits[2]), commandInfo.CompoundSplits[3])
    else
        success = false
    end
    return success
end

-- TODO: TEST does this overwrite NPCDialogue in lua module?
Dialogue.GetTraversalPoint = function(dialogue, indices, stepsBack, isSilent)
    local traversalPoint = dialogue
    indices = indices or {}
    stepsBack = stepsBack or 0
    local s = "* dialogue"
    for i=1, #indices - stepsBack do
        local deeperGet = traversalPoint[indices[i]]
        --DebugMessage("> ",i,": index", indices[i],"is",deeperGet)
        s = s.."["..tostring(indices[i]).."]"
        if deeperGet then
            traversalPoint = deeperGet 
        end
    end
    s = s.." *"
    --if not isSilent then DebugMessage("New TraversalPoint:",s) end
    return traversalPoint
end

Dialogue.IsTable = function(obj)
    return type(obj) == "table"
end

Dialogue.AddBranchIfPossible = function(readSpeech, readResponses, outSpeech, outResponses, outTotalSpeechPriority)
    if readSpeech then
        outTotalSpeechPriority = outTotalSpeechPriority + readSpeech[2]
        readSpeech[2] = outTotalSpeechPriority
        outSpeech[#outSpeech+1] = readSpeech
        for j=1, #readResponses do
            outResponses[#outResponses+1] = readResponses[j]
        end
    end
    return outTotalSpeechPriority
end

-- Quest Branches have custom logic and speech/response overrides to take advantage of static_data/quests.lua
Dialogue.ReadQuestBranch = function(npc, player, localTree, depth, instruction, index)
    depth = depth or 1
    local speech = {}
    local response = {}
    local tree = AllNPCDialogueTrees[Dialogue.GetLanguage()]["Quest"]
    local branch = tree.Branches[depth]
    if not branch then return nil, nil end
    branch = branch[instruction] or branch
    local questName = localTree.QuestName
    local stepIndex = Quests.GetRelevantStep(player, questName)
    --DebugMessage("Dialogue.ReadQuestBranch()", tostring(questName).." (Step:"..tostring(stepIndex)..", Depth:"..tostring(depth)..") -> "..tostring(instruction))
    if depth == 1 then
        local questSteps = localTree.Steps or {}
        local canResumeQuest = localTree.CanResume or false
        if questName and questSteps and tree and branch then
            local speech = {}
            local response = {}
            if questName and stepIndex and IsInTableArray(questSteps, stepIndex) then
                local prefix = tostring(index)..":"
                local hasResponse = false
                local wasQuestFailed = Quests.HasFailedSpecific(player, questName, stepIndex)
                if not wasQuestFailed then
                    local quest = AllQuests[questName][stepIndex]
                    local startDialogue = quest.StartDialogue
                    if startDialogue then
                        if Quests.IsPlayerEligible(player, questName, stepIndex, nil, true, true) then
                            response.text = startDialogue[1]
                            --speech = {startDialogue[2], 5}
                            speech = {"Hello. I have an opportunity for you.", 3}
                            response.handle = prefix.."START"
                            hasResponse = true
                        end
                    end
                    local endDialogue = quest.EndDialogue
                    if not hasResponse and endDialogue then
                        local doesEndNeedEligible = quest.EndDialogueHasEligibleCheck
                        local isEligible = Quests.IsPlayerEligible(player, questName, stepIndex, nil, true, false)
                        if (doesEndNeedEligible and isEligible) or (not doesEndNeedEligible and isEligible) then
                            response.text = endDialogue[1]
                            --speech = {endDialogue[2], 5}
                            speech = {"Hello again. How is your quest going?", 3}
                            response.handle = prefix.."END"
                            hasResponse = true
                        end
                    end
                    local ineligibleDialogue = quest.IneligibleDialogue
                    if not hasResponse and ineligibleDialogue then
                        local isEligible = Quests.IsPlayerEligible(player, questName, stepIndex, nil, true, false)
                        local hasContext = Quests.HasContextForIneligibleDialogue(player, questName, stepIndex)
                        if not isEligible and hasContext then
                            response.text = ineligibleDialogue[1]
                            --speech = {ineligibleDialogue[2], 5}
                            speech = {"Hmm. I think I need someone else for the quest I have in mind.", 1}
                            response.handle = prefix.."INELIGIBLE"
                            hasResponse = true
                        end
                    end
                else
                    --[[ Disabling this as it would be offered for specific steps covered by NPC, while other steps could be Abandoned and the NPC would say nothing - could confuse player. Use Profession Window, instead
                    if canResumeQuest then
                        --speech = {ineligibleDialogue[2], 5}
                        local index = string.find(questName, "Profession")
                        if index then
                            response.text = "["..string.sub(questName, 1, (index - 1)).."] What did you need me to do, again?"
                        else
                            response.text = "What did you need me to do, again?"
                        end

                        speech = {"Have you thought about giving my quest another go?", 2}
                        response.handle = prefix.."RETRY"
                        hasResponse = true
                    end]]
                end
                if hasResponse then
                    return speech, {response}
                end
            end
        end
        return nil, nil
    elseif depth == 2 then
        speech, response = Dialogue.ReadBranch(npc, player, branch, {Index = index, OnBegin={questName, stepIndex}})

        local startDialogue = AllQuests[questName][stepIndex].StartDialogue
        if startDialogue and instruction == "START" then
            speech = {startDialogue[2], 5}
        end
        local endDialogue = AllQuests[questName][stepIndex].EndDialogue
        if endDialogue and instruction == "END" then
            speech = {endDialogue[2], 5}
        end
        local ineligibleDialogue = AllQuests[questName][stepIndex].IneligibleDialogue
        if ineligibleDialogue and instruction == "INELIGIBLE" then
            speech = {ineligibleDialogue[2], 5}
        end
        return speech, response
    elseif depth == 3 then
        return Dialogue.ReadBranch(npc, player, branch, {Index = index, OnBegin={questName, stepIndex}})
    else
        return nil, nil
    end
end

Dialogue.ReadBranch = function(npc, player, branch, args)
    if not (branch and player and npc) then DebugMessage("Invalid Parameters: Missing Branch, Player, or NPC") return end
    --DebugMessage("ReadBranch() start")
    local responseIndex = 1
    local response = {}
    branch.MenuText = branch.MenuText or {}
    args = args or {}

    if branch.OnBegin then branch.OnBegin(npc, player, args.OnBegin) end
    if not branch.Speech or branch.Speech == false then return nil, nil end

    if next(branch.MenuText) then
        for i=1, #branch.MenuText do
            local skip = false
            if args.OnBegin and args.OnBegin.DontSkip then
                skip = not IsInTableArray(args.OnBegin.DontSkip, branch.Instruction[i])
            end
            if not skip then
                response[responseIndex] = {}
                response[responseIndex].text = branch.MenuText[i]

                local index = args.Index or i
                local prefix = tostring(args.Index)..":"
                local goTo = branch.GoTo and branch.GoTo[i] ~= nil
                if goTo then
                    response[responseIndex].handle = prefix.."GoTo|"..tostring(branch.GoTo[i]).."|"..tostring(branch.Instruction[i])
                else
                    response[responseIndex].handle = prefix..branch.Instruction[i]
                end

                responseIndex = responseIndex + 1
            end
        end
    elseif branch.UseResponseVars then
        --DebugMessage("Use ResponseVars")
        response = Dialogue.GetTempVariables(npc, player, "ResponseVars")
    elseif branch.GetResponses then
        response = branch.GetResponses(npc, player, args.GetResponses)
    end

    local speech = branch.Speech
    if string.match(branch.Speech, '@') then
        local speechVars = Dialogue.GetTempVariables(npc, player, "SpeechVars")
        for i=1, #speechVars do
            local x = "@"..tostring(i)
            local var = tostring(speechVars[i])
            if string.match(speech, x) and var then
                speech = string.gsub(speech, x, var) 
            end
        end
        Dialogue.SetTempVariables(npc, player, {SpeechVars = nil})
    end

    return {speech, branch.SpeechPriority or 1}, response
end

Dialogue.DecideNPCSpeech = function(potentialSpeech, totalSpeechPriority)
    local decidedSpeech = "[Error]"
    if totalSpeechPriority > 1 and #potentialSpeech > 1 then
        local rand = math.random(1, totalSpeechPriority)
        for i=1, #potentialSpeech do
            local speech = potentialSpeech[i][1]
            local high = potentialSpeech[i][2]
            local low = 0
            if potentialSpeech[i-1] then low = potentialSpeech[i-1][2] end
            if rand <= high and rand > low then
                decidedSpeech = speech
            end
        end
    else
        if potentialSpeech[1] then decidedSpeech = potentialSpeech[1][1] end
    end
    return decidedSpeech
end

-- @param: rangeString - String declaring range of desired ints like a web browser's printer prompt, such as "1,2,4" or "1-9" or "1,3,6,10-15"
Dialogue.ProcessNumberRange = function(rangeString)
    if type(rangeString) ~= "string" then return nil end
    local splits = StringSplit(rangeString, ",")
    local output = {}
    for i=1, #splits do
        local maybeNumber = tonumber(splits[i])
        if maybeNumber then
            table.insert(output, maybeNumber)
        else
            local rangeSplits = StringSplit(splits[i], "-")
            local first = tonumber(rangeSplits[1])
            local last = tonumber(rangeSplits[2] or "")
            if first and last then
                for i=first, last do
                    table.insert(output, i)
                end
            end
        end
    end
    return output
end

Dialogue.RequestGameObject = function(npc, player, openBackpack)
    if openBackpack then
        local backpackObj = player:GetEquippedObject("Backpack")
        backpackObj:SendOpenContainer(player)
    end
    player:SystemMessage("Select the item (Press ESC to cancel).","info")
    Dialogue.SetTempVariables(npc, player, {NeedContext = true})
    player:RequestClientTargetGameObj(npc, "DialogueObjectSelection")
end

Dialogue.GetTrainOptions = function(npc, user)
    menuItems = {}

    for skillName,skillEntry in pairs(SkillData.AllSkills) do
        if( not(skillEntry.Skip) and GetSkillLevel(npc, skillName) >= 30 ) then
            local nextSkill = {
                SkillName = skillName,
                DisplayName = skillEntry.DisplayName
            }
            table.insert(menuItems,nextSkill)
        end
    end 

    local response = {}
    local n = 0
    for i,j in pairs(menuItems) do
        if (i < 5) then
            local skillData = SkillData.AllSkills[j.SkillName]
            local nextResponse = {}
            nextResponse = {}
            nextResponse.text = "I want to learn ".. skillData.DisplayName
            nextResponse.handle = "Train:SKILL|"..j.SkillName.."|"..skillData.DisplayName
            table.insert(response,nextResponse)
            n = i
        else
            DebugMessage("[base_skill_trainer|SkillTrainer.ShowTrainContextMenu] ERROR: No more than 4 skills supported! Implement Scroll Bars!")
        end
    end

    response[n+1] = {}
    response[n+1].text = "Nevermind."
    response[n+1].handle = "H"

    return response
end


------ Saving Data (Temporary & Permanent) ------
--

Dialogue.GetTempVariables = function(npc, player, keys)
    if not (npc and player) then LuaDebugCallStack("NPC?",npc~=nil,"Player?",player~=nil) end
    local temp = Dialogue.GetTempInteractionRecord(npc, player) or {}
    if type(keys) == "table" then
        local get = {}
        for i=1, #keys do
            get[keys[i]] = temp[keys[i]]
        end
        return get
    else
        return temp[keys]
    end
end

Dialogue.SetTempVariables = function(npc, player, keyValuePairs)
    if not (npc and player) then LuaDebugCallStack("NPC?",npc~=nil,"Player?",player~=nil) end
    local temp = Dialogue.GetTempInteractionRecord(npc, player) or {}
    for key, value in pairs(keyValuePairs) do
        temp[key] = value
    end
    Dialogue.SetTempInteractionRecord(npc, player, temp)
end

Dialogue.GetTempInteractionRecord = function(npc, player)
    if not npc then LuaDebugCallStack("No NPC") end
    local tempInteractionRecord = npc:GetObjVar("TempInteractionRecord") or {}
    local playerRecord = tempInteractionRecord[player:GetAttachedUserId()] or {}
    return playerRecord
end

Dialogue.SetTempInteractionRecord = function(npc, player, newRecord)
    local tempInteractionRecord = npc:GetObjVar("TempInteractionRecord") or {}
    local playerRecord = tempInteractionRecord[player:GetAttachedUserId()] or {}

    tempInteractionRecord[player:GetAttachedUserId()] = newRecord

    npc:SetObjVar("TempInteractionRecord", tempInteractionRecord)
end

Dialogue.ClearTempInteractionRecord = function(npc)
    npc:SetObjVar("TempInteractionRecord", {})
end

-- might cause trouble if two players leave dialogue at the same time
Dialogue.ClearTempPlayerInteractionRecord = function(npc, player)
    local tempInteractionRecord = npc:GetObjVar("TempInteractionRecord") or {}
    local playerRecord = tempInteractionRecord[player:GetAttachedUserId()]
    if playerRecord then
        tempInteractionRecord[player:GetAttachedUserId()] = {}
        npc:SetObjVar("TempInteractionRecord", tempInteractionRecord)
    end
end

Dialogue.ClearInteractionRecord = function(npc)
    npc:SetObjVar("InteractionRecord", {})
end

Dialogue.GetPlayerInteractionRecord = function(npc, player, interactionType)
    local interactionRecord = npc:GetObjVar("InteractionRecord") or {}
    local typeRecord = interactionRecord[interactionType] or {}
    local id = player:GetAttachedUserId()..">"..tostring((player.Id or "0000"))
    --DebugMessage("playerId", id)
    return typeRecord[id] or {}
end
Dialogue.SetPlayerInteractionRecord = function(npc, player, interactionType, newRecord)
    local interactionRecord = npc:GetObjVar("InteractionRecord") or {}
    local typeRecord = interactionRecord[interactionType] or {}
    local id = player:GetAttachedUserId()..">"..tostring((player.Id or "0000"))
    --DebugMessage("playerId", id)
    typeRecord[id] = newRecord
    npc:SetObjVar("InteractionRecord", interactionRecord)
end

Dialogue.GetLanguage = function()
    return "ENG"
end

Dialogue.DrawDialogueWindow = function(text,npc,user,windowHandle,responses,title,max_distance)
    if (user == nil) then return end
    if not responses and not next(responses) then
        player:CloseDynamicWindow("NPCInteraction")
        return
    end

    local userType = GetValueType(user)
    if(userType ~= "GameObj") then
        LuaDebugCallStack("ERROR: User parameter is of wrong type: "..userType)
        return
    end

    if not user:IsValid() then return end
    if (npc ~= nil and IsDead(npc) and not npc:HasObjVar("UseableWhileDead")) then return end

    max_distance = max_distance or OBJECT_INTERACTION_RANGE
    if (type(max_distance) == "string") then max_distance = OBJECT_INTERACTION_RANGE end
    if (npc ~= nil and (max_distance >= 0 and npc:DistanceFrom(user) > max_distance)) then return end

    if (npc ~= nil) then npc:SendMessage("WakeUp") end

    if((title == nil or type(title) ~= "string") and npc ~= nil) then
        title = npc:GetName()
    end

    width = 877
    height = 200 
    local npcWindow = DynamicWindow(windowHandle,"",width,height,-(width/2),-280,"Transparent","Bottom")
    npcWindow:AddButton(860,28,"","",46,28,"","",true,"ScrollClose")
    npcWindow:AddImage(0,0,"ConversationWindow_BG")

    npcWindow:AddLabel(40,24,"[433518]"..StripColorFromString(title):upper(),0,0,24,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
    npcWindow:AddLabel(40,50,"[433518]"..text,440,120,20,"left",true,false,"PermianSlabSerif_Dynamic_Bold")

    if (responses == nil) then
        responses = {}
        responses[1] = {}
        responses[1].handle = "Close"
        responses[1].text = "Okay."
    end

    local k = 1
    local curY = 24
    if (type(responses) == "string") then
        LuaDebugCallStack("[incl_dialogwindow] responses is a string value")
    end

    local scrollWindow = ScrollWindow(506,curY,310,156,26)
    local responseCount = 0
    for i,j in pairs( responses ) do 
        responseCount = responseCount + 1
    end

    for i=1, #responses do
        local response = responses[i]
        if (response ~= nil) then

            local closeOnClick = not(response.handle) or response.handle == "" or response.handle == "Close" or response.close ~= nil
            local scrollElement = ScrollElement()

            if( responseCount > 6 ) then
                scrollElement:AddButton(10,0,response.handle,response.text,245,20,"","",closeOnClick,"ScrollSelection")
                scrollWindow:Add(scrollElement)
            else
                npcWindow:AddButton(506,curY,response.handle,response.text,245,20,"","",closeOnClick,"ScrollSelection")
            end

            curY = curY + 26
            k = k + 1
        end
    end

    if( responseCount > 6 ) then npcWindow:AddScrollWindow(scrollWindow) end

    user:OpenDynamicWindow(npcWindow,npc)   
    if (npc ~= nil) then
        user:SendMessage("DynamicWindowRangeCheck",npc,windowHandle,max_distance)
    end
end