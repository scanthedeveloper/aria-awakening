Gardening = {}

-- Determine if the obj passed is a plant
-- @param: obj the object you want to determine is a plant
-- @return: boolean (true/false) if plant or not.
Gardening.IsPlant = function( obj )
    if( obj:HasObjVar("SoilType") and obj:HasObjVar("Planted") ) then return true end
    return false
end

-- Determine if the gardening pot/raise-bed can be accessed
-- @param: playerObj the player that is trying to access.
-- @param: plantObj the plant that is being accessed.
-- @return: boolean if access was successful (true/false)
Gardening.CanPlantContainerBeUsed = function( playerObj, plantObj )
    local useCase = {}
    -- The order these "Restrictions = " apply is very IMPORTANT, don't reorder
    -- unless you know what you're doing.
    useCase[1] = {} useCase[1].Restrictions = {"IsLockedDown", "HasPlotControl"}
    useCase[2] = {} useCase[2].Restrictions = {"IsLockedDown", "HasHouseControl"}
    useCase[3] = {} useCase[3].Restrictions = {"IsLockedDown", "HasPlotHouseCoOwner"}
    useCase[4] = {} useCase[4].Restrictions = "IsInBackpack"
    useCase[5] = {} useCase[5].Restrictions = "IsInBank"
    local usedCasePassed = false

    -- We interate through the use cases and see if any passed; if so it's usable
    for i=1, #useCase do
        if( CanUseCase(playerObj, plantObj, useCase[i]) == true ) then
            --DebugMessage( useCase[i].Restrictions[1] .. " Passed!" )
            usedCasePassed = true
        else
            --DebugMessage( useCase[i].Restrictions[1] .. " Failed!" )
        end
    end

    return usedCasePassed
end

-- Watering Plant
-- @param: playerObj the player that is watering.
-- @param: plantObj the plant that is being watered.
-- @return: boolean if watering was successful (true/false)
Gardening.WaterPlant = function( playerObj, plantObj )
    if not( Gardening.CanPlantContainerBeUsed(playerObj, plantObj) ) then
        playerObj:SystemMessage("You cannot water that.", "info")
        return false
    end

    local playerBackpack = playerObj:GetEquippedObject("Backpack")
    if( Gardening.ConsumeWater(playerBackpack, ServerSettings.Gardening.WaterskinsConsumedPerWater) ) then
        -- Get our updated soilType
        local newSoilType = Gardening.GetSoilWetter( plantObj:GetObjVar("SoilType") )
        plantObj:SetObjVar("SoilType", newSoilType)
        -- Let the player know
        playerObj:SystemMessage("The soil looks " .. string.lower(newSoilType) .. " now.", "info")
        Gardening.UpdatePlantTooltip(plantObj)
    else
        playerObj:SystemMessage("Missing enough filled waterskins.", "info")
    end
end

--- Consumes water from water containers
-- @param container (container to look for WaterContainer in)
-- @param count number of water containers to empty
-- @return boolean (true/false) based on success
Gardening.ConsumeWater = function(container,count)
    -- Get all WaterContainers that are Full from the container (recursively)
    local contents = FindItemsInContainerRecursive(container,function(item) 
        return item:GetObjVar("ResourceType") == "WaterContainer" and item:GetObjVar("State") == "Full"
    end) or {}

    if ( #contents < count ) then return false end
    
    for i=1, count do
        -- Did we find a full water container?
        if( contents[i]:GetObjVar("State") == "Full" ) then
            UpdateWaterContainerState(contents[i],"Empty") 
        end
    end
    
    return true
end

-- Gathering Plant
-- @param: playerObj the player that is gathering.
-- @param: plantObj the plant that is being gathered.
-- @return: boolean if gathering was successful (true/false)
Gardening.GatherPlant = function( playerObj, plantObj )
    if not( Gardening.CanPlantContainerBeUsed(playerObj, plantObj) ) then
        playerObj:SystemMessage("You cannot gather that.", "info")
        return false
    end

    -- Make sure it's a player; and off we go!
    if( playerObj:IsPlayer() ) then
        -- This gets the ResourceEffectData[] table key
        local plantType = plantObj:GetObjVar("PlantVariation") .. "Seed"
        Create.Stack.InBackpack( ResourceEffectData[plantType].CreatesTemplate, playerObj, Gardening.GetGatherAmount(plantObj) )
        -- We need to empty the plant when it's done.
        Gardening.EmptyPlant(playerObj, plantObj) 
    else
        -- Is this even needed? Maybe for possessed stuff?
        playerObj:SystemMessage("Cannot do that.", "info")
    end
end

-- Emptying Plant
-- @param: playerObj the player that is emptying.
-- @param: plantObj the plant that is being emptied.
-- @return: boolean if emptying was successful (true/false)
Gardening.EmptyPlant = function( playerObj, plantObj )
    if not( Gardening.CanPlantContainerBeUsed(playerObj, plantObj) ) then
        playerObj:SystemMessage("You cannot empty that.", "info")
        return false
    end

    -- Remove Use Cases
    if( HasUseCase(plantObj,"Water") ) then RemoveUseCase(plantObj, "Water") end
    if( HasUseCase(plantObj,"Gather") ) then RemoveUseCase(plantObj, "Gather") end

    -- Reset/Remove ObjVars
    local plantContainerType = plantObj:GetObjVar("PlantContainerType") or nil
    if ( plantContainerType ~= nil ) then
        plantObj:SetObjVar("PlantVariation", "")
        plantObj:SetSharedObjectProperty("Variation", Gardening.GetMeshVariationForContainer(plantObj, ""))
        plantObj:SetObjVar("Planted", false)
        plantObj:SetObjVar("PlantType", "")
        plantObj:DelObjVar("NextMaturityDateTime")
        plantObj:SetObjVar("MaturityInDays", ServerSettings.Gardening.InitialMaturityInDays)
        plantObj:SetObjVar("SoilType", ServerSettings.Gardening.InitialSoilType)
        Gardening.UpdatePlantTooltip(plantObj)
    else
        LuaDebugCallStack("PLANT CONTAINER COULD NOT BE EMPTIED: " .. plantObj:ToString())
    end
    
end

-- Given a string of the soil type it tells which is next "wetter" type.
-- @param: string containing plants ObjVar("SoilType")
-- @return: string of next wettest SoilType
Gardening.GetSoilWetter = function( soilType )
    for i=1, #ServerSettings.Gardening.SoilWaterLevels do
        -- Did we find our soilType and we're not at top of the list?
        if( ServerSettings.Gardening.SoilWaterLevels[i] == soilType ) then
            if( i ~= #ServerSettings.Gardening.SoilWaterLevels ) then
                -- Return the next wettest soil type
                return ServerSettings.Gardening.SoilWaterLevels[i+1]
            else
                -- We are already at the wettest type
                return ServerSettings.Gardening.SoilWaterLevels[#ServerSettings.Gardening.SoilWaterLevels]
            end
        end
    end
    
    -- We didn't find the soil type, reset it.
    return ServerSettings.Gardening.InitialSoilType
end

-- Given a string of the soil type it tells which is next "wetter" type.
-- @param: string containing plants ObjVar("SoilType")
-- @return: string of next wettest SoilType
Gardening.GetSoilDryer = function( soilType )
    for i=1, #ServerSettings.Gardening.SoilWaterLevels do
        -- Did we find our soilType and we're not at bottom of list?
        if( ServerSettings.Gardening.SoilWaterLevels[i] == soilType ) then
            if( i ~= 1 ) then
                -- Return the next dryest soil type
                return ServerSettings.Gardening.SoilWaterLevels[i-1]
            else
                -- We are already at the dryest type
                return ServerSettings.Gardening.SoilWaterLevels[1]
            end
        end
    end

    -- We didn't find the soil type, reset it.
    return ServerSettings.Gardening.InitialSoilType
end

-- Given a plant object it will update the tooltip based on the ObjVars
-- @param: plantObj the plant that is being updated
-- @return: boolean if planting was successful
Gardening.UpdatePlantTooltip = function( plantObj )
    if(Gardening.IsPlant(plantObj)) then
        SetItemTooltip(plantObj)
    end
end

Gardening.GeneratePlantTooltip = function( plantObj, tooltipInfo )

    -- Encased all of these in tostring() to make sure we don't throw an error.
    local plantType = plantObj:GetObjVar("PlantType") or ""
    local tooltipLines = {}
    
    if( plantType ~= "" ) then
        tooltipLines[1] = tostring(plantObj:GetObjVar("PlantType")) .. " Plant"
        tooltipLines[2] = "Grown: " .. tostring( Gardening.GetGatherAmount(plantObj) ) .. "/" .. tostring(plantObj:GetObjVar("MaximumYield"))
        tooltipLines[3] = "Matures: " .. tostring( Gardening.GetDaysTilMature(plantObj) ) .. " days"
        tooltipLines[4] = "Soil: " .. tostring(plantObj:GetObjVar("SoilType"))
    else
        tooltipLines[1] = "Nothing Planted"
        tooltipLines[2] = "Use a seed to plant something here."
    end

    -- Set the tooltip
    tooltipInfo.Gardening = {
        TooltipString = CombineString(tooltipLines, "\r\n"),
        Priority = 100,
    }
    
    return tooltipInfo

end

-- Get the number of currently gatherable items from the plant
-- @param: plantObj the plant that you want gather form
-- @return: number of gatherables or false if error
Gardening.GetGatherAmount = function( plantObj )
    -- How many days does this plant take to mature?
    local matureDays = plantObj:GetObjVar("DaysToMature") or 0
    -- The system only supports 7 and 10 day mature rates; if we got something else that's bad!!
    if( matureDays ~= 7 and matureDays ~= 10 ) then return false end

    local maxYield = plantObj:GetObjVar("MaximumYield") or 0
    local daysMatured = plantObj:GetObjVar("MaturityInDays") or 0
    local YieldModifer = plantObj:GetObjVar("YieldModifer") or 1
    -- This is done just to make it work with the ServerSettings table (Day 0 && 1 output are the same)
    if( daysMatured < 1 ) then daysMatured = 1 end

    local gatherAmount = 0
    -- Do we mature in 10 days? If so we need to use the right server settings.
    if( matureDays == 10 ) then
        gatherAmount = ServerSettings.Gardening.GatherAmountTenDayMaturity[daysMatured]
    else
        gatherAmount = ServerSettings.Gardening.GatherAmountSevenDayMaturity[daysMatured]
    end

    -- Use our gatherAmount (percentage of max), maximum yeild, and pot yield modifer to get final amount
    return  math.round( gatherAmount * maxYield * YieldModifer )
end

-- Get the number of days this plant will take to mature
-- @param: plantObj the plant that you want to mature
-- @return: number of days or false if error
Gardening.GetDaysTilMature = function( plantObj )
    local matureDays = plantObj:GetObjVar("DaysToMature") or 0
    local daysMatured = plantObj:GetObjVar("MaturityInDays") or 0
    return (matureDays - daysMatured)
end

-- Get the percentage the plant has matured 0% - 100%
-- @param: plantObj the plant that you want to mature
-- @return: percentage matured
Gardening.GetMaturePercentage = function( plantObj )
    local matureDays = plantObj:GetObjVar("DaysToMature") or 0
    local daysMatured = plantObj:GetObjVar("MaturityInDays") or 0
    return math.round( ( daysMatured / matureDays ) * 100 )
end

-- This function drives the "Growth" of seeds once they are planted
-- @param: plantObj - the pot that needs to check growth
-- @return: boolean - false if growth failed; true if growth succeeded
Gardening.DoGrowth = function( plantObj, force )
    -- We want to get the soilType before we dry it
    local soilTypeInit = plantObj:GetObjVar("SoilType")
    local now = DateTime.UtcNow
    local matureDateTime = plantObj:GetObjVar("NextMaturityDateTime") or now:Add(TimeSpan.FromMinutes(1))


    --[[
    -- Uncomment to DEBUG
    -- All these should be FALSE
    DebugMessage("Doing Growth!")
    DebugMessage("Now: " .. tostring(now))
    DebugMessage("MartureDateTime: " .. tostring(matureDateTime))
    DebugMessage("Planted : " .. tostring( plantObj:GetObjVar("Planted") ~= true ))
    DebugMessage("Dryest : " .. tostring( soilTypeInit == ServerSettings.Gardening.SoilWaterLevels[1] ))
    DebugMessage("CanMature : " .. tostring( now < matureDateTime ))
    DebugMessage("MaximumMaturity : " .. tostring( Gardening.GetDaysTilMature(plantObj) < 1 ))
    DebugMessage("Wetest : " .. tostring( soilTypeInit == ServerSettings.Gardening.SoilWaterLevels[#ServerSettings.Gardening.SoilWaterLevels] ))
    DebugMessage("SoilMissing : " .. tostring( soilTypeInit == nil ))
    ]]

    -- If any of these pass, we don't need to grow! These are ordered 
    -- by most common --> least common to occur for speedz!!
    if(
        not force and 
        (
            plantObj:GetObjVar("Planted") ~= true or -- Are we not planted?
            soilTypeInit == ServerSettings.Gardening.SoilWaterLevels[1] or  -- Are we at dryest?
            now < matureDateTime or -- Is it time to grow?
            Gardening.GetDaysTilMature(plantObj) < 1 or -- Are we already at maximum maturity?
            soilTypeInit == nil -- Are we missing the soil type?
        )
    ) then
        --DebugMessage( "Failed CAN GROW check" );
        return false 
    end

    -- If we are not at the wettest soil level we can grow!
    if not (soilTypeInit == ServerSettings.Gardening.SoilWaterLevels[#ServerSettings.Gardening.SoilWaterLevels] ) then
        --DebugMessage("Can Mature!")
        plantObj:SetObjVar("NextMaturityDateTime", now:Add(TimeSpan.FromMinutes(ServerSettings.Gardening.MaturityIntervalMinutes)))
        plantObj:SetObjVar("MaturityInDays", plantObj:GetObjVar("MaturityInDays") + 1)
        Gardening.UpdateVisuals(plantObj)
        
    end
    
    -- Set our new soil wetness level
    plantObj:SetObjVar("SoilType", Gardening.GetSoilDryer(plantObj:GetObjVar("SoilType")))
    Gardening.UpdatePlantTooltip(plantObj)

    return true
    -- That's it, easy peasy.
end

-- Update the visual look of the plant
-- @param: plantObj - the pot that needs to have it's visuals updated
Gardening.UpdateVisuals = function( plantObj )
    
    -- Do we have everything we need?
    if( plantObj:GetObjVar("Planted") ) then
        local maturePercent = Gardening.GetMaturePercentage(plantObj)
        --DebugMessage( "Mature Percent: " .. maturePercent )

        if( maturePercent <= 25 ) then 
            plantObj:SetSharedObjectProperty("Variation", Gardening.GetMeshVariationForContainer(plantObj, "Default"))
        elseif( maturePercent <= 50 ) then 
            plantObj:SetSharedObjectProperty("Variation", Gardening.GetMeshVariationForContainer(plantObj, "Stage1"))
        elseif( maturePercent < 95 ) then 
            plantObj:SetSharedObjectProperty("Variation", Gardening.GetMeshVariationForContainer(plantObj, "Stage2"))
        elseif( maturePercent >= 95 ) then 
            plantObj:SetSharedObjectProperty("Variation", Gardening.GetMeshVariationForContainer(plantObj, "Stage3"))
        else
            plantObj:SetSharedObjectProperty("Variation", Gardening.GetMeshVariationForContainer(plantObj, "Default"))
            LuaDebugCallStack("PLANT MATURE PERCENTAGE WRONG!! : " .. plantObj:ToString())
        end
    end
end


-- Plants a given seed in a given pot
-- @param: plantObj - the pot you wish to plant the seed in
-- @param: seedArgs - ResourceEffectData.[Seed].MobileEffectArgs defined in seeds.lua
-- @return: boolean (true/false) based on planting success
Gardening.PlantSeed = function( plantObj, seedArgs )
    -- Check will this plant fit in this planter box?
    if( seedArgs.PlantSize > plantObj:GetObjVar("MaximumPlantSize") or not plantObj:HasObjVar("MaximumPlantSize") ) then
        return false
    end

    -- Set our plantObject vars
    plantObj:SetObjVar("DaysToMature", seedArgs.DaysToMature)
    plantObj:SetObjVar("PlantType", seedArgs.PlantType)
    plantObj:SetObjVar("PlantVariation", seedArgs.PlantVariation)
    plantObj:SetObjVar("MaximumYield", seedArgs.MaximumYield)
    plantObj:SetObjVar("MaturityInDays", ServerSettings.Gardening.InitialMaturityInDays)
    plantObj:SetObjVar("SoilType", ServerSettings.Gardening.InitialSoilType)
    plantObj:SetObjVar("Planted", true)
    --plantObj:SetObjVar("NextMaturityDateTime", DateTime.UtcNow:Add(TimeSpan.FromHours(24)))
    plantObj:SetObjVar("NextMaturityDateTime", DateTime.UtcNow:Add(TimeSpan.FromMinutes(ServerSettings.Gardening.MaturityIntervalMinutes)))

    -- Make sure we have our use cases
    if not(HasUseCase(plantObj, "Water")) then AddUseCase(plantObj,"Water",true) end
    if not(HasUseCase(plantObj, "Gather")) then AddUseCase(plantObj,"Gather",false) end
    if not(HasUseCase(plantObj, "Empty")) then AddUseCase(plantObj,"Empty",false) end
    plantObj:SetSharedObjectProperty("Variation", Gardening.GetMeshVariationForContainer(plantObj, "Default"))
    Gardening.UpdatePlantTooltip(plantObj)
    return true
end

-- Determine what mesh variation to use based on plant container type
    -- @param: plantObj - the container the plant is in
    -- @param: the base stage you want to use Default, Stage1, Stage2, Stage3
    -- @return: the variation string to pass to SetSharedObjectProperty() based on the container
    Gardening.GetMeshVariationForContainer = function( plantObj, stage )
        local plantContainerType = plantObj:GetObjVar("PlantContainerType") or "" 
        local plantVariation = plantObj:GetObjVar("PlantVariation") or "" 
        local returnStr = plantVariation .. stage .. plantContainerType
        --DebugMessage(returnStr)
        return returnStr

    end

-- Tells all plants across all cluster controllers to grow
-- @param: cluster controller to start on
-- @param: callback function when done
-- @param: force growth regardless of time since last growth; RISKY!
Gardening.GrowAll = function(clusterController, cb, force)
    --DebugMessage("GrowAll Called!")
    if not( cb ) then cb = function() end end
    if not( ServerSettings.Gardening.GrowEnabled ) then return cb() end

    

    local now
    if ( not force ) then
        now = DateTime.UtcNow
        -- already grown this interval
        local lastGrown = GlobalVarReadKey("Gardening", "LastGrown")
        if ( lastGrown ~= nil and now:Subtract(lastGrown) <= ServerSettings.Gardening.GrowTimeResolution ) then return cb() end
    end

    --DebugMessage("GrowAll Passed!")

    -- tell all cluster controllers accross all regions to grow all plants.
    for regionAddress,data in pairs(GetClusterRegions()) do
        if ( regionAddress == ServerSettings.RegionAddress ) then
            clusterController:SendMessage("DoPlantGrowth")
        else
            if ( IsClusterRegionOnline(regionAddress) ) then
                MessageRemoteClusterController(regionAddress, "DoPlantGrowth")
            end
        end
    end
    
    if ( force == true ) then return cb(true) end

    -- record the last time we did this
    SetGlobalVar("Gardening", function(record)
        record.LastGrown = now
        return true
    end, cb)
end

