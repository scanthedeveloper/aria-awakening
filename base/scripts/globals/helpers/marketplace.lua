--[[ Stockpiles ]]

-- List of skills and recipe categories to be loaded into the marketplace stockpile
MarketplaceBoard.StockSkillCategories = {
    MetalsmithSkill = { "Resources", "Armor", "Weapons", "Materials" },
    FabricationSkill = { "Resources", "Armor", "Weapons", "Materials" },
    WoodsmithSkill = { "Resources", "Armor", "Weapons", "Materials" },
    AlchemySkill = { "Potions", "Materials" },
    InscriptionSkill = { "Scrolls", "Materials" },
    WorldDrops = {"All"}
}



-- Loads all the items that can be donated or purchased into the stockpile objvar of the marketplace board
-- @param: marketplaceBoardObj - the marketplace board that needs to be initialized
-- @param: stockpile (optional) - the stockpile list to used instead of querying marketplaceBoardObj for it
MarketplaceBoard.InitStockpile = function( marketplaceBoardObj, stockpile )
    
    -- Get the stockpile from the township board
    local stockpile = stockpile or marketplaceBoardObj:GetObjVar("stockpile") or {}

    -- Check/set the stock counts for each skill / category
    for skill, categories in pairs(MarketplaceBoard.StockSkillCategories) do
        for i=1, #categories do 
            local recipes = GetRecipeCategoryForSkill( categories[i], skill )
            for type,recipe in pairs(recipes) do
                if not( stockpile[skill] ) then 
                    stockpile[skill] = {}
                end
                if not( stockpile[skill][categories[i]] ) then 
                    stockpile[skill][categories[i]] = {}
                end
                if not ( stockpile[skill][categories[i]][recipe.CraftingTemplateFile] ) then
                    stockpile[skill][categories[i]][recipe.CraftingTemplateFile] = { Count = 0, ResourceType = type }
                end
            end
        end
    end

    -- Check/set the stock counts for raw materials
    for skill, categories in pairs(MarketplaceBoard.StockSkillCategories) do
        local recipes = GetRecipeCategoryForSkill("Resources", skill)
        for key,recipe in pairs( recipes ) do
            for k,count in pairs( recipe.Resources ) do
                if not( stockpile[skill]["Materials"] ) then 
                    stockpile[skill]["Materials"] = {}
                end
                if not( stockpile[skill]["Materials"][ResourceData.ResourceInfo[k].Template] ) then
                    stockpile[skill]["Materials"][ResourceData.ResourceInfo[k].Template] = { Count = 0, ResourceType = k }
                end
            end
        end
    end

    -- Check/set world drop counts
    if not( stockpile["WorldDrops"] ) then 
        stockpile["WorldDrops"] = {}
    end

    if not( stockpile["WorldDrops"]["All"] ) then 
        stockpile["WorldDrops"]["All"] = {}
    end

    for template,data in pairs( MarketplaceBoard.WorldDropList ) do
        if not( stockpile["WorldDrops"]["All"][template] ) then
            stockpile["WorldDrops"]["All"][template] = { Count = 0, ResourceType = nil, DisplayName = data.DisplayName }
        
        -- If we've changed the display name we need to update it.
        elseif( stockpile["WorldDrops"]["All"][template].DisplayName ~= data.DisplayName ) then
            stockpile["WorldDrops"]["All"][template].DisplayName = data.DisplayName
        end

    end
    
    -- We need to set the stockpile list back onto the township board object
    marketplaceBoardObj:SetObjVar("stockpile", stockpile)

end

-- Take an item or stack of items and adds their quantity to the stockpile counts ( will consume them as well )
-- @param: gameObj - the object that should be consumed (donated) to the marketplace board
-- @return: how many times the object was consumed, how many stacks were consumed each time
MarketplaceBoard.ConsumeDonatedObject = function( gameObj )
    local stackCount = gameObj:GetObjVar("StackCount") or 1
    local gameObjTemplate = gameObj:GetCreationTemplateId()

    --DebugMessage(  stackCount .. " : [" .. tostring(MarketplaceBoard.StockStackCountsToDonate[gameObjTemplate]) .. "]" )

    -- Determine if this item has a minimum donate amount
    if( MarketplaceBoard.StockStackCountsToDonate[gameObjTemplate] ~= nil ) then
        if( stackCount >= MarketplaceBoard.StockStackCountsToDonate[gameObjTemplate] ) then
            local consumedStacks = 1 --math.floor(stackCount / MarketplaceBoard.StockStackCountsToDonate[gameObjTemplate])
            local consumedItems = consumedStacks * MarketplaceBoard.StockStackCountsToDonate[gameObjTemplate]
            --DebugMessage( "AdjustStack: " .. -consumedItems )
            gameObj:SendMessage("AdjustStack", -consumedItems )
            stackCount = consumedStacks -- so we can adjust counts
        else
            return nil, MarketplaceBoard.StockStackCountsToDonate[gameObjTemplate]
        end
    else
    -- Consume object
        gameObj:Destroy()
    end

    return stackCount, MarketplaceBoard.StockStackCountsToDonate[gameObjTemplate]
end

-- Given a player and a faction it will determine if the player is aligned with that
-- faction based on their currency objvar
-- @param: playerObj - player to check
-- @param: faction - name of faction ie. (Ebris, Tethys, Pyros)
-- @return: boolean - true if player is faction, false if not
MarketplaceBoard.CheckIfPlayerIsFaction = function( playerObj, faction )
    return true
end

-- Will remove all faction currecy objvars from a player
-- @param: playerObj - player to remove the objvars from
-- @return: boolean - true
MarketplaceBoard.ClearPlayerFactionCurrencies = function( playerObj )
    for townshipName, currencyObjVar in pairs( QuestsLeague.TownshipNameToCurrency ) do 
        QuestsLeague.SetLeagueCurrecy( playerObj, townshipName, 0 )
    end
    return true
end

-- Adjusts the faction currency for a given player based on the amount passed
-- @param: playerObj - the player to adjust currency for
-- @param: faction - the faction the currecy should be adjusted for
-- @param: amount - how much currecy should be added/subtracted
-- @param: reason (optional) - why was the adjustment made, will be annouced to player
-- @param: slient (optional) - if true the adjustment will not be alerted to the player 
MarketplaceBoard.AdjustFactionCurrency = function( playerObj, faction, amount, reason, silent )

    if( playerObj == nil ) then return false end
    for i=1, #ServerSettings.Militia.Militias do
        if( ServerSettings.Militia.Militias[i].Town == faction ) then
            return QuestsLeague.AdjustLeagueCurrency( playerObj, ServerSettings.Militia.Militias[i].Town, amount )
        end
    end

end

MarketplaceBoard.GetFactionCurrency = function( playerObj, faction )

    if( playerObj == nil ) then return false end
    for i=1, #ServerSettings.Militia.Militias do
        if( ServerSettings.Militia.Militias[i].Town == faction ) then
            return QuestsLeague.GetLeagueCurrecy( playerObj, ServerSettings.Militia.Militias[i].Town )
        end
    end

end

-- Given a object and a marketplace, will attempt to add the item to the stockpile for the marketplace
-- @param: gameObj - object or stack of objects that are to be donated
-- @param: marketplaceBoardObj - marketplace object that is be donated to
-- @param: playerObj - player who is donated the item(s)
-- @param: stockpile (optional) - the stockpile objvar from the marketplace board so it doesn't have to querried
-- @param: force (optional) - if true it will force the function to clear other faction currencies and donate to the one designated on the marketplace board
-- @return: boolean - did the donate succeed?
MarketplaceBoard.TryDonateObjectToStockpile = function( gameObj, marketplaceBoardObj, playerObj, stockpile, force )

    if( marketplaceBoardObj == nil or playerObj == nil )then return false end
    if( gameObj ~= nil ) then
        -- We need the template
        local gameObjTemplate = gameObj:GetCreationTemplateId()
        local isFaction, factionName = MarketplaceBoard.GetFactionData( marketplaceBoardObj )
        local currency = MarketplaceBoard.FactionCurrencyObjVar[factionName] or nil
        --DebugMessage( "Template: " .. gameObjTemplate)

        local stockpile = stockpile or marketplaceBoardObj:GetObjVar("stockpile") or {}
        for skill, categories in pairs( MarketplaceBoard.StockSkillCategories ) do 
            for i=1, #categories do 
                if( stockpile[skill] ~= nil and stockpile[skill][categories[i]] ~= nil ) then
                    --DebugMessage( "SKILL: " .. skill )
                    for template, data in pairs( stockpile[skill][categories[i]] ) do
                        --DebugMessage( template )
                        if( template == gameObjTemplate ) then
                            local stockCountAverage = MarketplaceBoard.GetStockpileAverageStockCount(marketplaceBoardObj)
                            local currencyAwarded = MarketplaceBoard.DonateAdjust( stockCountAverage, stockpile[skill][categories[i]][template].Count )
                            if( currencyAwarded > 0 ) then

                                -- Confirm the player can gain this currency!
                                if ( not MarketplaceBoard.CheckIfPlayerIsFaction(playerObj, factionName) and not force ) then
                                    ClientDialog.Show( 
                                        { 
                                            TargetUser = playerObj,
                                            TitleStr = "Currency Wipe",
                                            DescStr = "You have faction currency with another faction. If you continue you will lose all existing faction currency.",
                                            ResponseFunc = function( user, buttonId )
                                                if( buttonId == 0 ) then
                                                    --DebugMessage("DelObjVar: " .. currencyObjVar)
                                                    MarketplaceBoard.ClearPlayerFactionCurrencies( playerObj )
                                                    return MarketplaceBoard.TryDonateObjectToStockpile( gameObj, marketplaceBoardObj, playerObj, stockpile, true )
                                                end
                                            end
                                        }
                                    )
                                    return false
                                end

                                local stackCount, minimumCount = MarketplaceBoard.ConsumeDonatedObject( gameObj )

                                if( stackCount ~= nil ) then
                                    --DebugMessage("StackCount Consumed: " .. stackCount)
                                    -- Adjust counts
                                    stockpile[skill][categories[i]][template].Count = stockpile[skill][categories[i]][template].Count + stackCount
                                    marketplaceBoardObj:SetObjVar("stockpile", stockpile)
                                    currencyAwarded = currencyAwarded * stackCount

                                    -- We need to award the player their currency points
                                    if( currency == nil ) then -- gold
                                        --DebugMessage("Awarding gold: " .. currencyAwarded)
                                        -- TODO Change this to make coins in bank
                                        Create.Coins.InBackpack( playerObj, currencyAwarded )
                                        local message = "[DAA520]" .. playerCurrencyAmount .. "[-] gold has been placed in your backpack."
                                        playerObj:SystemMessage(message, "info")
                                        playerObj:SystemMessage(message)
                                    else -- faction tokens
                                        --DebugMessage("Awarding faction: " .. currencyAwarded)
                                        return MarketplaceBoard.AdjustFactionCurrency( playerObj, factionName, currencyAwarded)
                                    end

                                    return true
                                else
                                    playerObj:SystemMessage("You need a minimum quantity of " .. tostring(minimumCount) .. " to list that item.", "info")    
                                    return false -- ConsumeDonateObject failed
                                end
                            else
                                playerObj:SystemMessage("The market has too many of that item.", "info")
                                return false -- No currency would be awarded!
                            end
                        end
                    end
                end
            end
        end

        -- World drops
        if( stockpile["WorldDrops"]["All"] ~= nil ) then
            for template, data in pairs( stockpile["WorldDrops"]["All"] ) do
                if( template == gameObjTemplate ) then
                    local stockCountAverage = MarketplaceBoard.GetStockpileAverageStockCount(marketplaceBoardObj)
                    local currencyAwarded = MarketplaceBoard.DonateAdjust( stockCountAverage, stockpile["WorldDrops"]["All"][template].Count )
                    if( currencyAwarded > 0 ) then

                        local stackCount, minimumCount = MarketplaceBoard.ConsumeDonatedObject( gameObj )

                        if( stackCount ~= nil ) then
                            -- Adjust counts
                            stockpile["WorldDrops"]["All"][template].Count = stockpile["WorldDrops"]["All"][template].Count + stackCount
                            marketplaceBoardObj:SetObjVar("stockpile", stockpile)

                            -- We need to award the player their currency points
                            if( currency == nil ) then -- gold
                                Create.Coins.InBackpack( playerObj, currencyAwarded )
                            else -- faction tokens
                                MarketplaceBoard.AdjustFactionCurrency( playerObj, factionName, currencyAwarded)
                            end
                            return true
                        else
                            playerObj:SystemMessage("You need a minimum quantity of " .. minimumCount .. " to list that item.", "info")    
                            return false -- ConsumeDonateObject failed
                        end
                    else
                        playerObj:SystemMessage("The market has too many of that item.", "info")
                        return false -- No currency would be awarded!
                    end
                end
            end
        end
    end


    playerObj:SystemMessage("That item cannot be traded on this marketplace.", "info")
    return false
end

-- Given a marketplace board will get the faction data from it
-- @param: marketplaceBoardObj - the marketplace board to get faction data from
-- @return bool, string - if the board is faction based, and what faction it is
MarketplaceBoard.GetFactionData = function( marketplaceBoardObj ) 
    local factionId = marketplaceBoardObj:GetObjVar("MarketplaceBoardFaction")
    if( factionId and ServerSettings.Militia.Militias[factionId].Town ) then
        return true, ServerSettings.Militia.Militias[factionId].Town
    end
    return false, nil
end

-- Tries to purchase a given template from a marketplace for a given player.
-- @param: template - what template the player wishes to purchase
-- @param: marketplaceBoardObj - what market the template is to be purchase from
-- @param: playerObj - what player is purchasing the template
-- @param: stockpile (optional) - the stockpile objvar from the marketplace board so it doesn't have to querried
-- @return: none
MarketplaceBoard.TryPurchaseMarketItem = function( template, marketplaceBoardObj, playerObj, stockpile )
    local stockpile = stockpile or marketplaceBoardObj:GetObjVar("stockpile") or {}
    local stockCount = MarketplaceBoard.GetStockpileCountForTemplate( template, marketplaceBoardObj, stockpile )
    local averageCount = MarketplaceBoard.GetStockpileAverageStockCount( marketplaceBoardObj, stockpile )
    local price = MarketplaceBoard.PurchaseAdjust( averageCount, stockCount )
    local isFaction, factionName = MarketplaceBoard.GetFactionData( marketplaceBoardObj )
    local playerObjCurrencyAmount = playerObj:GetObjVar(MarketplaceBoard.FactionCurrencyObjVar[factionName]) or 0
    local count = MarketplaceBoard.StockStackCountsToDonate[template] or 1


    -- We don't want them to be able to stockpile items in their backpack that they don't have room for!
    if not( CanCreateItemInContainer( template, playerObj:GetEquippedObject("Backpack"), count ) ) then
            playerObj:SystemMessage("You do not have room on your backpack for that item.", "info")
            return
    end

    -- We need to consume faction currency
    if( isFaction ) then
        if not( MarketplaceBoard.AdjustFactionCurrency( playerObj, factionName, -price ) ) then
            playerObj:SystemMessage("You do not have enough township favor to purchase this.", "info")
            return
        else
            playerObj:SendMessage("ConsumeResourceResponse", true, "PurchaseMarketplaceItem", template)
        end
    -- We need to consume gold
    else
        local backpackCanBuy = ( CountCoins(playerObj) >= price )
        local bankCanBuy = ( CountCoinsInBank(playerObj) >= price )
        
        if( not backpackCanBuy and not bankCanBuy ) then
            playerObj:SystemMessage("You do not have enough gold.", "info")
            return
        end
        -- Can our backpack afford the purchase, if not take from the bank!
        if( backpackCanBuy ) then
            RequestConsumeResource(playerObj,"coins",price,"PurchaseMarketplaceItem",template)
        else
            RequestConsumeResourceFromBank(playerObj,"coins",price,"PurchaseMarketplaceItem",template)
        end
                
        
    end

end

-- Given a marketplace this returns the average stock counts
-- @param: marketplaceBoardObj - marketplace to get the average stock from
-- @param: stockpile (optional) - the stockpile objvar from the marketplace board so it doesn't have to querried
-- @return: int - average stockpile counts
MarketplaceBoard.GetStockpileAverageStockCount = function( marketplaceBoardObj, stockpile )
    local totalCount = 0
    local totalItems = 0

    local stockpile = stockpile or marketplaceBoardObj:GetObjVar("stockpile") or {}
    for skill, categories in pairs( MarketplaceBoard.StockSkillCategories ) do 
        for i=1, #categories do 
            if( stockpile[skill] ~= nil and stockpile[skill][categories[i]] ~= nil ) then
                for template, data in pairs( stockpile[skill][categories[i]] ) do
                    totalCount = totalCount + data.Count
                    totalItems = totalItems + 1
                end
            end
        end
    end

    --DebugMessage("Total Count: " .. totalCount)
    --DebugMessage("Total Items: " .. totalItems)

    return ( totalCount / totalItems ) -- average count
end

-- Given a template and marketplace will return the stock count for that template
-- @param: myTemplate - template to get the counts for
-- @param: marketplaceBoardObj - marketplace to get the counts from
-- @param: stockpile (optional) - the stockpile objvar from the marketplace board so it doesn't have to querried
-- @return: int - count of the template items
MarketplaceBoard.GetStockpileCountForTemplate = function(myTemplate, marketplaceBoardObj, stockpile)
    local stockpile = stockpile or marketplaceBoardObj:GetObjVar("stockpile") or {}
    for skill, categories in pairs( MarketplaceBoard.StockSkillCategories ) do 
        for i=1, #categories do 
            if( stockpile[skill] ~= nil and stockpile[skill][categories[i]] ~= nil ) then
                for template, data in pairs( stockpile[skill][categories[i]] ) do
                    if( template == myTemplate ) then
                        return data.Count
                    end
                end
            end
        end
    end
    return 0
end

-- Given a template, marketplace and amount it will adjust the stock count for that template on that marketplace
-- @param: myTemplate - template to adjust the count for
-- @param: marketplaceBoardObj - marketplace to adjust the stock form
-- @param: stockpile (optional) - the stockpile objvar from the marketplace board so it doesn't have to querried
-- @param: amount - the amount the stock needs to be adjusted (added/subtracted)
MarketplaceBoard.AdjustStockpileCountForTemplate = function(myTemplate, marketplaceBoardObj, stockpile, amount)
    local stockpile = stockpile or marketplaceBoardObj:GetObjVar("stockpile") or {}
    for skill, categories in pairs( MarketplaceBoard.StockSkillCategories ) do 
        for i=1, #categories do 
            if( stockpile[skill] ~= nil and stockpile[skill][categories[i]] ~= nil ) then
                for template, data in pairs( stockpile[skill][categories[i]] ) do
                    if( template == myTemplate ) then
                        stockpile[skill][categories[i]][template].Count = stockpile[skill][categories[i]][template].Count + amount 
                    end
                end
            end
        end
    end
    marketplaceBoardObj:SetObjVar("stockpile", stockpile)
end