require 'globals.helpers.create'
require 'globals.helpers.loottables'
require 'globals.helpers.object'
require 'globals.helpers.container'
require 'globals.helpers.crafting'
require 'globals.helpers.durability'
require 'globals.helpers.damage'
require 'globals.helpers.death'
require 'globals.helpers.dungeon'
require 'globals.helpers.stats'
require 'globals.helpers.skills'
require 'globals.helpers.pets'
require 'globals.helpers.equipmentstats'
require 'globals.helpers.weapon_abilities'
require 'globals.helpers.prestige'
require 'globals.helpers.combat'
require 'globals.helpers.mobile'
require 'globals.helpers.spellbook'
require 'globals.helpers.recipes'
require 'globals.helpers.hirelings'
require 'globals.helpers.item_value'
require 'globals.helpers.world_location'
require 'globals.helpers.progressbar'
require 'globals.helpers.guard'
require 'globals.helpers.housing'
require 'globals.helpers.prefabs'
require 'globals.helpers.spells'
require 'globals.helpers.time'
require 'globals.helpers.teleporter'
require 'globals.helpers.cooking'
require 'globals.helpers.resource'
require 'globals.helpers.food'
require 'globals.helpers.weapon'
require 'globals.helpers.currency'
require 'globals.helpers.vitality'
require 'globals.helpers.initiate'
require 'globals.helpers.player'
require 'globals.helpers.guild'
require 'globals.helpers.guild_wars'
require 'globals.helpers.group'
require 'globals.helpers.conflict'
require 'globals.helpers.criminal'
require 'globals.helpers.fame'
require 'globals.helpers.merchant'
require 'globals.helpers.militia'
require 'globals.helpers.militia_events'
require 'globals.helpers.verbose'
require 'globals.helpers.maps'
require 'globals.helpers.decay'
require 'globals.helpers.plot'
require 'globals.helpers.door'
require 'globals.helpers.missions'
require 'globals.helpers.packedobject'
require 'globals.helpers.achievement'
require 'globals.helpers.craftingorders'
require 'globals.helpers.friend'
require 'globals.helpers.gmmessage'
require 'globals.helpers.instrument'
require 'globals.helpers.gardening'
require 'globals.helpers.enchanting'
require 'globals.helpers.quests'
require 'globals.helpers.awakening'
require 'globals.helpers.professions'
require 'globals.helpers.awakening'
require 'globals.helpers.hired_merchants'
require 'globals.helpers.spawns'
require 'globals.helpers.actions'
require 'globals.helpers.bloodlust'
require 'globals.helpers.marketplace'
require 'globals.helpers.skillscroll'
require 'globals.helpers.townships'
require 'globals.helpers.quests_league'
require 'globals.helpers.globalstorage'
require 'globals.helpers.bazaar'
require 'globals.helpers.http'
require 'globals.helpers.rankedarena'
require 'globals.helpers.sorcery'
require 'globals.helpers.player_topbar'
require 'globals.helpers.group_ai'
require 'globals.helpers.monolith_schematic'
require 'globals.helpers.seasons'
require 'globals.helpers.chaos'
require 'globals.helpers.npc_dialogue'
require 'globals.helpers.lore'
require 'globals.helpers.chat'
require 'globals.helpers.heirlooms'