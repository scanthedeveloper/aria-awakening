Instrument = {}

--inspect = require 'inspect'

-- Find instruments in the player's backpack
-- @param: playerObj - the player who's backpack you want to search
-- @return instrument found or nil of none found
Instrument.GetInstrumentFromBackpack = function(playerObj, equipIt)
    local instrument = nil
    local backpack = playerObj:GetEquippedObject("Backpack")
	if ( backpack ) then
		FindItemsInContainerRecursive(backpack, function(fobj)
			if ( Weapon.GetToolType(fobj) == "Instrument" ) then
				instrument = fobj
			end
		end)
    end

    if( instrument and equipIt ) then
        DoEquip(instrument, playerObj)
    end
    
    return instrument
end

-- Finds a hearth close to the bard
-- @param: playerObj - the player who is the bard
-- @return hearth object or nil
Instrument.GetNearbyHearth = function(playerObj)

    local hearths = FindObjects(SearchMulti(
    {
        SearchRange(playerObj:GetLoc(), (ServerSettings.Instrument.MaximumHearthRange + 3)),
            SearchModule("hearth"), --find hearths
    }))

    --We only want to return the first hearth we find
    for index, hearth in pairs(hearths) do 
        -- Make sure it's a hearth though!
        if( hearth:HasModule("hearth") ) then
            return hearth
        end
    end

    return nil
end

-- Finds a campfire close to the bard
-- @param: playerObj - the player who is the bard
-- @return hearth object or nil
Instrument.GetNearbyCampfire = function(playerObj)

    local campfires = FindObjects(SearchMulti(
    {
        SearchRange(playerObj:GetLoc(), (ServerSettings.Campfire.MaxRange)),
            SearchModule("tinderbox_campfire"), --find hearths
    }))

    --We only want to return the first hearth we find
    for index, campfire in pairs(campfires) do 
        -- Make sure it's a hearth though!
        if( campfire:HasModule("tinderbox_campfire") ) then
            return campfire
        end
    end

    return nil
end

-- Update a listeners vitality during a solo (flurry)
Instrument.GiveSoloVitalityBonus = function( listenerObj )
    if( listenerObj == nil ) then return end
    local maxVit = GetMaxVitality(listenerObj)
    local curVit = GetCurVitality(listenerObj)
    --DebugMessage( "Cur: " .. curVit .. " Max: " .. maxVit )
    if( curVit >= maxVit) then return end

    -- The new vitality would be higher then the player max
    if( (curVit + ServerSettings.Instrument.FlurryVitalityBonus) >= maxVit ) then
        --DebugMessage("Set Vit To Max")
        SetCurVitality(listenerObj, maxVit)
    -- We can add the vitality bonus and go
    else
        --DebugMessage("Set Vit to Bonus")
        SetCurVitality(listenerObj, (curVit + ServerSettings.Instrument.FlurryVitalityBonus))
    end
end

Instrument.GetEquippedInstrument = function(playerObj)
    local instrumentObj = playerObj:GetEquippedObject("RightHand") or nil
    if (not instrumentObj or not Instrument.IsInstrument(instrumentObj)) then
        instrumentObj = nil
    end
    return instrumentObj
end

Instrument.IsInstrument = function(weaponObj)
    if weaponObj:GetObjVar("ToolType") == "Instrument" then
        return true
    else
        return false
    end
end


Instrument.GetInstrumentParameterName = function(playerObj)
    local instrumentRaw = Instrument.GetEquippedInstrument(playerObj)
    if instrumentRaw ~= nil then
        return EquipmentStats.BaseWeaponStats[instrumentRaw:GetObjVar("WeaponType")].ParameterName
    else
        return nil
    end
end


--- Determines if there are band members nearby and syncs animations / movement if so
-- @param: playerObj the player looking for band members
-- @return: true if in a band; false if not
Instrument.IsBandLeaderPlaying = function( playerObj )
    local nearbyBards = FindObjects(
        SearchHasObjVar("BandLeader", ServerSettings.Instrument.MaximumBandMemberRange)
    )
    
    -- Loop through all the bards around us and see if any are in our group;
    -- if so we want to return the first we find.
	for i,bard in pairs (nearbyBards) do
        if( ShareGroup(playerObj, bard) ) then
            return bard
        end
    end
    
    return nil
end

--- Determines if there are band members nearby and syncs animations / movement if so
-- @param: playerObj the player who is playing an instrument
-- @return: true if in a band; false if not
Instrument.SyncWithBand = function( playerObj, bandState )
    --## TODO: Make this actually sync with you band.
end

--- Returns the number of unique instruments in the band; omitted the playerObj equippepd one
-- @param: playerObj the player who is playing an instrument
-- @return: integer count of unique instruments in band
Instrument.GetBandUniqueInstrumentCount = function( playerObj )
    local instruments = Instrument.AllInstruments
    local hasInstruments = {}

    -- Get the bards around us based on the bandId we are in
    -- Change this back to HasObjVar
    local myBards = FindObjects(
	    SearchObjVar("BandID", playerObj:GetObjVar("BandID") , ServerSettings.Instrument.MaximumBandMemberRange)
    )

    --## TODO: If this doesn't inlcude the player just add the player to #myBards
    -- myBards[#myBards+1] = playerObj

    -- Loop through each bard and set their instrument flags
    local count = 0
    for i=1, #myBards do
        local weaponType = Weapon.GetType(myBards[i]:GetEquippedObject("RightHand"))
        if not( hasInstruments[weaponType] ) then
            hasInstruments[weaponType] = true
            count = count + 1
        end
    end

    -- We return the #hasInstruments; minus 1 for the current players instrument
    return count
end


--- Determines if the player has active listeners
-- @param: playerObj the player who is playing an instrument
-- @return: # of listeners if active listen found; false otherwise
Instrument.PlayerHasListeners = function( playerObj, returnListeners )
    if( playerObj:GetObjVar("BandID") == nil ) then return false end
    
    local myListeners = FindObjects(
        SearchObjVar("ListeningToBandID", playerObj:GetObjVar("BandID") ,ServerSettings.Instrument.MaximumListenRange)
    )

    --DebugMessage( inspect( myListeners ) )

    if( #myListeners > 0 ) then 
        if not ( returnListeners ) then
            return #myListeners
        else
            return myListeners
        end
    else 
        return false 
    end
end

--- Sets player as active listener of target
-- @param bandState: the self._BandState table from the "Listen To Music" target 
Instrument.StartListening = function( bandState )

    -- Get our song details or; return false 
    --DebugMessage(bandState.song.EventName)
    if not( bandState.song ) then return false end
    local bardRange = ServerSettings.Instrument.MaximumListenRange

    if( bandState.hearth ) then
        if not( bandState.hearth:IsValid() ) then return false end
        if( bandState.hearth:HasModule("hearth") ) then
            bardRange = ServerSettings.Instrument.MaximumHearthRange
        else
            bardRange = ServerSettings.Campfire.MaxRange
        end
    end

    -- Get the bards around us based on the bandId we are listening to
    local myBards = FindObjects(
        SearchMulti(
	        {
		        SearchMobileInRange(bardRange),
		        SearchObjVar("BandID", bandState.bandId), 
            }
        )
    )

    -- If there are no bards in range; return false
    if( #myBards == 0 ) then return false end

    return true

end

--- Stops player from listening to target
-- @param bandState: the self._BandState table from the "Listen To Music" target 
Instrument.StopListening = function( bandState )

end

Instrument.DoInstrumentAnimationAndEffect = function( playerObj, effect, skipAnimation )
    
    if( skipAnimation ~= true ) then
        -- Play the instrument animations/effects
        local equipInstrument = playerObj:GetEquippedObject("RightHand")
        local instrumentType = Weapon.GetType(equipInstrument)

        if( instrumentType ~= nil ) then
            local instrument = string.lower(instrumentType)
            playerObj:PlayAnimation(instrument)
        end
    end

    if( effect ~= nil ) then
        playerObj:PlayEffect(effect)
    end
end

Instrument.DoStopInstrumentAnimationAndEffect = function( playerObj, effect, skipAnimation )
    if( skipAnimation ~= true ) then
        playerObj:PlayAnimation("idle")
    end
    
    if( effect ~= nil ) then
        playerObj:StopEffect(effect)
    end
end

--- Triggers the animations and sound for a player playing an instrument by themselves
-- @param: playerObj the player who is playing an instrument
-- @param: _bandState current self._BandState of "Play" mobile effect; if nil then we genearte one
-- @return: table of bandstate
Instrument.PlayInstrument = function( playerObj, _bandState )
    -- Here we go!
    local bandState = {}
    
    playerObj:StopMoving()
    playerObj:PlayEffect("MusicNoteIntensity1Effect")
    
    -- If we passed the _bandState use it; else generate a new one
    if not( _bandState.bandId == nil ) then
        bandState = _bandState
    else
        bandState.song = Instrument.GetRandomSongToPlay()
        bandState.bandId = uuid()
        bandState.startedPlaying = ServerTimeSecs()
    end

    --DebugMessage("BandState: ")
    --DebugTable(bandState)

    return bandState
end

--- Stops animations and sound for instrument on a player
-- @param: playerObj the player who is playing an instrument
Instrument.StopPlayInstrument = function( playerObj )
    playerObj:PlayAnimation("idle")
    playerObj:StopEffect("MusicNoteIntensity1Effect")
end

--- Picks a random listner end message
Instrument.GetRandomListenerEndMessage = function()
    local messages =
    {
        "A pateron walks away."
    }

    return messages[1]
end

--- Picks a random listner begin message
Instrument.GetRandomListenerBeganMessage = function()
    local messages =
    {
        "Your melody has attracted the attention someone.",
        "Your song has sparked the interest of a pateron.",
        "You notice someone listening to your tune."
    }

    return messages[math.random( 1,#messages )]
end

--- Picks a random ended message
Instrument.GetRandomSongEndedMessage = function()
    local messages =
    {
        "Your song ends peacefully.",
        "Your song ends on a high note.",
        "Your melody soars to a rousing close."
    }

    return messages[math.random( 1,#messages )]
end

--- Picks a random interrupt message
Instrument.GetRandomSongInterruptedMessage = function()
    local messages = 
    {
        "Your song ends abrutly.",
        "Your song ends on a missed note.",
        "Your melody falls flat.",
        "You failed to maintain the melody."
    }

    return messages[math.random( 1,#messages )]
end

--- Picks a random interrupt message
Instrument.GetRandomRiddleInterruptedMessage = function()
    local messages = 
    {
        "Riddle fails to incite.",
    }

    return messages[math.random( 1,#messages )]
end

--- Picks a random interrupt message
Instrument.GetRandomDanceAnimation = function()
    local dances = 
    {
        "dance_wave",
        "dance_gangamstyle",	
        "dance_bellydance",
        "dance_cabbagepatch",
        "dance_chickendance",
        "dance_guitarsolo",
        "dance_headbang",
        "dance_hiphop",
        "dance_macarena",
        "dance_raisetheroof",
        "dance_robot",
        "dance_runningman",
        "dance_twist",
    }

    return dances[math.random( 1,#dances )]
end

--- Get the track, length, and event details for a given song
Instrument.GetSongDetails = function(song)
    local songs = {}
    songs["Green Fields"] =
    {
        Name = "Green Fields",
        EventName = "event:/music/bard/bard_music",
        TrackDuration = 120,
        Instruments =
        {
            flute = 0,
            lute = 0,
            drum = 0
        }
    }

    return songs[song] or false
end

--- Picks a random song to play
Instrument.GetRandomSongToPlay = function()
    local songs =
    {
        "Green Fields"
    }

    --## TODO: make this use the songs [LENGTH] to get ceiling
    return Instrument.GetSongDetails(songs[math.random( 1,#songs )])
end

--- Picks a random flurry success message
Instrument.GetRandomSongFlurrySuccessMessage = function()
    local messages =
    {
        "The notes fly from your instrument.",
        "Your melody soars on a high note."
    }

    --## TODO: make this use the messages [LENGTH] to get ceiling
    return messages[math.random( 1,#messages )]
end

--- Picks a random flurry failure message
Instrument.GetRandomSongFlurryFailureMessage = function()
    local messages =
    {
        "You miss a note.",
        "Your melody skips on a missed note."
    }

    --## TODO: make this use the messages [LENGTH] to get ceiling
    return messages[math.random( 1,#messages )]
end