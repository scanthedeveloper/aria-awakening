-- Init this helper
BazaarHelper = {}

BazaarHelper.FilterObjVars = { }

-- These are ObjVar keys that are ignored when BazaarHelper.ObjVarsCollapse is called.
BazaarHelper.ObjVarBlacklist = 
{
    TooltipString = true,
    Priority = true,
    essenceModifier = true,
    CraftedBy = true,
    Crafter = true,
    MaxDurability = true,
    MenuStr = true,
    Restrictions = true,
    SingularName = true,
    NoReset = true,
    DefaultInteractionOveridden = true,
    StackCount = true,
    LockedDown = true,
    merchantOwner = true,
    UnitWeight = true,
    OldDefaultInteraction = true,
    itemOwner = true,
}

BazaarHelper.BlacklistedTemplates = 
{
    rune_blank = true
    ,runebook = true
    ,book_blue = true
    ,book_brown = true
    ,book_green = true
    ,book_grey = true
    ,book_red = true
    ,book_yellow = true
    ,pouch = true
    ,lockbox = true
    ,lockbox_blank = true
    ,gold_rank_box = true
    ,obsidian_rank_box = true
    ,cobalt_rank_box = true
    ,copper_rank_box = true
    ,reagent_bag_noob = true
    ,reward_pack_crowdfunding = true
    ,reward_pack_grandmaster_starter_pack = true
    ,reward_pack_master_starter_pack = true
    ,reward_pack_ornate = true
    ,reward_pack_pax_2019 = true
    ,reward_pack_savage = true
}

BazaarHelper.CollapseObjVars = function( obj )
    local objVars = obj:GetAllObjVars() or {}
    local newObjVars = {}
    local merchant = obj:GetObjVar("merchantOwner") or nil
    if( merchant == nil ) then return newObjVars end
    local plot = merchant:GetObjVar("PlotController") or nil

    if( merchant and plot ) then
        for k,v in pairs(objVars) do 
            BazaarHelper.AddValueToTable(k, v, newObjVars)
        end

        table.insert(newObjVars, { shopName = plot:GetObjVar("PlotName") })
        table.insert(newObjVars, { ObjectId = obj.Id })
        table.insert(newObjVars, { Hue = obj:GetHue() })
        table.insert(newObjVars, { ObjectTooltip = tostring(obj:GetSharedObjectProperty("TooltipString")).."\n\nWeight: "..tostring(obj:GetSharedObjectProperty("Weight")) })
        table.insert(newObjVars, { CreationTemplate = obj:GetCreationTemplateId() })
        table.insert(newObjVars, { Name = StripColorFromString(obj:GetName()) })
        table.insert(newObjVars, { Loc = string.sub(StringSplit( tostring(merchant:GetLoc()), ":" )[1], 2, -2) })
    end
    
    return newObjVars
end

-- Recursively dives into a table adding key->value pairs to a new table as values.
BazaarHelper.AddValueToTable = function( key, value, newObjVars )
    if( BazaarHelper.ObjVarBlacklist[key] == true ) then return end
    if( type(value) == "table" ) then
        for k,v in pairs(value) do 
            BazaarHelper.AddValueToTable(k, v, newObjVars)
        end
    else
        value = tostring(value)
        if( not string.match(value, "GameObj") ) then
            table.insert(newObjVars, { [key] = value } )
        end
    end
end

BazaarHelper.FiltersToObjVar = function( filterStates )
    if( not filterStates ) then return {} end
    local filterObjVars = {}

    for k,v in pairs( filterStates ) do 
        if( v.State == "pressed" ) then
            --DebugTable( v )
            if( v.Type == "Essence" ) then
                table.insert(filterObjVars, { essenceName = tostring(k) } )
            elseif( v.Type == "BookType" ) then
                if( k == "Spellbooks" ) then
                    table.insert(filterObjVars, { WeaponType = { "Spellbook" } })
                elseif( k == "Runebooks" ) then
                    table.insert(filterObjVars, { ResourceType = { "Runebook" } })
                elseif( k == "StoryBooks" ) then
                    table.insert(filterObjVars, { Rewriteable = true })
                elseif( k == "TomeBooks" ) then
                    table.insert(filterObjVars, { ResourceType = { "PrestigeGrandmasterBard","PrestigeGrandmasterFighter","PrestigeGrandmasterMage","PrestigeGrandmasterRogue","PrestigeGrandmasterTamer","PrestigeJourneymanBard","PrestigeJourneymanFighter","PrestigeJourneymanMage","PrestigeJourneymanRogue","PrestigeJourneymanTamer","PrestigeMasterBard","PrestigeMasterFighter","PrestigeMasterMage","PrestigeMasterRogue","PrestigeMasterTamer" } })
                end
            elseif( v.Type == "FurnishingsType" ) then
                local templates = {}
                for r,e in pairs( AllRecipes.WoodsmithSkill ) do
                    if( e.Category == "Furnishings" and k == e.Category..e.Subcategory ) then
                        table.insert(templates, e.CraftingTemplateFile)
                    end
                end
                --DebugTable( templates )
                table.insert(filterObjVars, { UnpackedTemplate = templates })
            elseif( v.Type == "CraftingResources" ) then
                if( not v.ResourceType ) then
                    local found = false
                    
                    for r,e in pairs( AllRecipes.FabricationSkill ) do
                        if( e.Category == "Resources" and k == e.Category..r ) then
                            table.insert(filterObjVars, { ResourceType = { r } })
                        end
                    end

                    if( found == false ) then
                        for r,e in pairs( AllRecipes.WoodsmithSkill ) do
                            if( e.Category == "Resources" and k == e.Category..r ) then
                                table.insert(filterObjVars, { ResourceType = { r } })
                            end
                        end
                    end
                    
                    if( found == false ) then
                        for r,e in pairs( AllRecipes.MetalsmithSkill ) do
                            if( e.Category == "Resources" and k == e.Category..r ) then
                                table.insert(filterObjVars, { ResourceType = { r } })
                            end
                        end
                    end
                else
                    table.insert(filterObjVars, { ResourceType = { v.ResourceType } })
                end
                
                
            elseif( v.Type == "ScrollTypes" ) then
                if( k == "ScrollCircle1" ) then
                    local spells = {}
                    for r,e in pairs( SpellData.AllSpells ) do 
                        if( e.Circle == 1 ) then table.insert(spells, r) end
                    end
                    table.insert(filterObjVars, { Spell = spells })
                elseif( k == "ScrollCircle2" ) then
                    local spells = {}
                    for r,e in pairs( SpellData.AllSpells ) do 
                        if( e.Circle == 2 ) then table.insert(spells, r) end
                    end
                    table.insert(filterObjVars, { Spell = spells })
                elseif( k == "ScrollCircle3" ) then
                    local spells = {}
                    for r,e in pairs( SpellData.AllSpells ) do 
                        if( e.Circle == 3 ) then table.insert(spells, r) end
                    end
                    table.insert(filterObjVars, { Spell = spells })
                elseif( k == "ScrollCircle4" ) then
                    local spells = {}
                    for r,e in pairs( SpellData.AllSpells ) do 
                        if( e.Circle == 4 ) then table.insert(spells, r) end
                    end
                    table.insert(filterObjVars, { Spell = spells })
                elseif( k == "ScrollCircle5" ) then
                    local spells = {}
                    for r,e in pairs( SpellData.AllSpells ) do 
                        if( e.Circle == 5 ) then table.insert(spells, r) end
                    end
                    table.insert(filterObjVars, { Spell = spells })
                elseif( k == "ScrollCircle6" ) then
                    local spells = {}
                    for r,e in pairs( SpellData.AllSpells ) do 
                        if( e.Circle == 6 ) then table.insert(spells, r) end
                    end
                    table.insert(filterObjVars, { Spell = spells })
                elseif( k == "ScrollCircle7" ) then
                    local spells = {}
                    for r,e in pairs( SpellData.AllSpells ) do 
                        if( e.Circle == 7 ) then table.insert(spells, r) end
                    end
                    table.insert(filterObjVars, { Spell = spells })
                elseif( k == "ScrollCircle8" ) then
                    local spells = {}
                    for r,e in pairs( SpellData.AllSpells ) do 
                        if( e.Circle == 8 ) then table.insert(spells, r) end
                    end
                    table.insert(filterObjVars, { Spell = spells })
                end

            elseif( v.Type == "Weapons" ) then
                if( k == "WeaponDamage3" ) then
                    table.insert(filterObjVars, { AttackBonus = { 45,46,47,48,49 } })
                elseif( k == "WeaponDamage2" ) then
                    table.insert(filterObjVars, { AttackBonus = { 40,41,42,43,44 } })
                elseif( k == "WeaponDamage1" ) then
                    table.insert(filterObjVars, { AttackBonus = { 35,37,38,39,39 } })
                elseif( k == "WeaponDamage4" ) then
                    table.insert(filterObjVars, { AttackBonus = {50} })
                end
            elseif( v.Type == "WandAttunement" ) then
                if( k == "WandAttunement1" ) then
                    table.insert(filterObjVars, { Attunement = { 100 } })
                elseif( k == "WandAttunement2" ) then
                    table.insert(filterObjVars, { Attunement = { 50 } })
                elseif( k == "WandAttunement3" ) then
                    table.insert(filterObjVars, { Attunement = { 33.3 } })
                elseif( k == "WandAttunement4" ) then
                    table.insert(filterObjVars, { Attunement = { 25 } })
                end
            elseif( v.Type == "StaffAttunement" ) then
                if( k == "StaffAttunement1" ) then
                    table.insert(filterObjVars, { Attunement = { 50 } })
                elseif( k == "StaffAttunement2" ) then
                    table.insert(filterObjVars, { Attunement = { 25 } })
                elseif( k == "StaffAttunement3" ) then
                    table.insert(filterObjVars, { Attunement = { 16.6 } })
                elseif( k == "StaffAttunement4" ) then
                    table.insert(filterObjVars, { Attunement = { 12.5 } })
                end
            elseif( v.Type == "WeaponTypes" ) then
                if( k == "Slashing" ) then
                    table.insert(filterObjVars, { WeaponType = { "Longsword", "Broadsword", "Saber", "Katana", "BattleAxe", "LargeAxe", "GreatAxe" } })
                elseif( k == "Bashing" ) then
                    table.insert(filterObjVars, { WeaponType = { "Mace","Hammer","Maul","WarMace", "Quarterstaff", "Warhammer" } })
                elseif( k == "Piercing" ) then
                    table.insert(filterObjVars, { WeaponType = { "Dagger","Kryss","Poniard", "BoneDagger" } })
                elseif( k == "Lancing" ) then
                    table.insert(filterObjVars, { WeaponType = { "Warfork", "Voulge", "Spear", "Halberd", "Scythe" } })
                elseif( k == "Instrument" ) then
                    table.insert(filterObjVars, { WeaponType = { "Lute", "Drum", "Flute" } })
                elseif( k == "Ranged" ) then
                    table.insert(filterObjVars, { WeaponType = { "Shortbow", "Longbow", "Warbow", "SavageBow", "BoneBow" } })
                elseif( k == "Sorcery Orb" ) then
                    table.insert(filterObjVars, { WeaponType = {"SorcererWand"} })
                elseif( k == "Magic Staff" ) then
                    table.insert(filterObjVars, { WeaponType = {"MagicStaff"} })
                elseif( k == "Magic Wand" ) then
                    table.insert(filterObjVars, { WeaponType = {"MagicWand"} })
                end
            elseif( v.Type == "ArmorType" ) then
                if( k == "ArmorType1" ) then
                    table.insert(filterObjVars, { ArmorType = { "VeryLight", "Clothing", "Padded", "Linen", "MageRobe" } })
                elseif( k == "ArmorType2" ) then
                    table.insert(filterObjVars, { ArmorType = { "Leather", "Bone", "Hardened", "Assassin" } })
                elseif( k == "ArmorType3" ) then
                    table.insert(filterObjVars, { ArmorType = { "Brigandine", "Chain", "Scale", "Plate", "FullPlate" } })
                end
            elseif( v.Type == "Armor" ) then
                if( k == "Defense2" ) then
                    table.insert(filterObjVars, { ArmorBonus = {3} })
                elseif( k == "Defense1" ) then
                    table.insert(filterObjVars, { ArmorBonus = {2} })
                elseif( k == "Defense3" ) then
                    table.insert(filterObjVars, { ArmorBonus = {5} })
                end
            elseif( v.Type == "RecipesFabrication" or v.Type == "RecipesBlacksmithing" or v.Type == "RecipesCarpentry" ) then
                table.insert(filterObjVars, { Recipe = v.Recipe } )
            elseif( v.Type == "Category" and v.Name == "Armor" ) then
                table.insert(filterObjVars, { ArmorType = true } )
            elseif(v.Type == "Category" and v.Name == "Potions" ) then
                table.insert(filterObjVars, { CreationTemplate = {"potion_heal", "potion_gmana", "potion_gheal", "potion_etherealize", "potion_cure", "potion_lstamina", "potion_stamina", "potion_gstamina", "potion_mending", "potion_mana", "potion_lmana", "potion_lheal"} } )
            elseif( v.Type == "Category" and v.Name == "Scrolls" ) then
                local templates = {}
                for k,v in pairs( AllRecipes.InscriptionSkill) do 
                    if( v.Category == "Scrolls" ) then
                        table.insert(templates, v.CraftingTemplateFile)
                    end
                end
                table.insert(filterObjVars, { CreationTemplate =  templates } )
            elseif( v.Type == "Category" and v.Name == "Clothing" ) then
                table.insert(filterObjVars, { CreationTemplate = {"clothing_dress_chest", "clothing_bandit_hood_helm", "clothing_chest_blacksmith", "clothing_shorts_legs", "clothing_short_sleeve_shirt_chest", "clothing_chest_cloak_basic", "clothing_long_sleeve_shirt_chest", "clothing_bandana_helm", "clothing_apron_chest", "clothing_mage_hat_helm", "clothing_skirt_legs"} } )
            elseif( v.Type == "Category" and v.Name == "Essences" ) then
                table.insert(filterObjVars, { EssenceLevel = true } )
            elseif( v.Type == "Category" and v.Name == "Items" ) then
                table.insert(filterObjVars, { CreationTemplate = {"waterskin", "saddle_packed", "pouch", "saddlebags_packed"} } )
            elseif( v.Type == "Category" and v.Name == "Tools" ) then
                table.insert(filterObjVars, { CreationTemplate = {"garden_raised_bed_large", "garden_raised_bed_medium", "garden_raised_bed_small", "tool_drum", "tool_fishing_rod", "tool_lute", "tool_crook", "garden_plant_pot", "tool_flute", "tool_hunting_knife", "tool_shovel", "tool_engraving", "tool_hatchet", "tool_mining_pick", "tool_lockpick", "tool_cookingpot"} } )
            elseif( v.Type == "Category" and v.Name == "Artifacts" ) then
                table.insert(filterObjVars, { Artifact = true } )
            elseif( v.Type == "Category" and v.Name == "Furnishings" ) then
                table.insert(filterObjVars, { CreationTemplate = {"packed_object","fence_small", "campfire", "furniture_chair_wooden", "furniture_bench_fancy", "furniture_fireplace_stone", "furniture_stool_wooden", "furniture_table_round", "furniture_table_wooden_large", "tool_carpentry_table", "tool_anvil", "crate_empty", "furniture_shelf", "fence_door", "furniture_standingtorch", "furniture_lantern_hanging", "furniture_lantern_wall", "workbench_alchemy", "stove", "furniture_table_wooden", "tool_loom", "barrel", "lockbox", "furniture_bed_small", "chest_secure", "torch", "workbench_inscription", "furniture_desk_fancy", "furniture_dresser", "furniture_table_blacksmith", "furniture_table_inn", "furniture_chair_fancy", "furniture_bookshelf_wooden", "furniture_bed_medium", "furniture_bed_large", "tool_forge_round"} } )
            elseif( v.Type == "Category" and v.Name == "Weapons" ) then
                table.insert(filterObjVars, { WeaponType = true } )
            elseif( v.Type == "Category" and v.Name == "Resources" ) then
                --table.insert(filterObjVars, { CreationTemplate = {"resource_boards_ash", "resource_boards", "resource_brick", "resource_blightwood_boards", "resource_beast_leather", "resource_bolt_of_cloth_quilted", "resource_leather", "resource_silk_cloth", "resource_bolt_of_cloth", "resource_vile_leather", "resource_gold", "resource_iron", "resource_copper", "resource_obsidian", "resource_cobalt"} } )
            elseif( v.Type == "Category" and v.Name == "Books" ) then
                table.insert(filterObjVars, { CreationTemplate = {"runebook", "book_blue", "brown_blue", "book_green", "book_grey", "book_red", "book_yellow", "spellbook", "prestige_grandmaster_bard", "prestige_grandmaster_fighter", "prestige_grandmaster_mage", "prestige_grandmaster_rogue", "prestige_grandmaster_tamer", "prestige_journeyman_bard", "prestige_journeyman_fighter", "prestige_journeyman_mage", "prestige_journeyman_rogue", "prestige_journeyman_tamer", "prestige_master_bard", "prestige_master_fighter", "prestige_master_mage", "prestige_master_rogue", "prestige_master_tamer"} } )
            end
        end
    end

    return filterObjVars

end

-- Uses the fieldData and self._FilterStates from the Bazaar to construct
-- a search JSON packet to send to the RESTful service.
BazaarHelper.BuildSearchPacket = function( fieldData, filterStates )

    if( not fieldData or not filterStates ) then return false, "" end

    -- Remove newlines from search
    fieldData.itemSearch = string.gsub(fieldData.itemSearch, "[\r\n]", "") 
    --DebugMessage("Search: " .. fieldData.itemSearch .. " **")



    local filters = BazaarHelper.FiltersToObjVar( filterStates )
    --DebugTable(filters)

    for k,v in pairs(fieldData) do 
        table.insert( filters, { [k] = v } )
    end
    
    --DebugMessage( json.encode( filters ) )

    return json.encode( filters )
end

-- 
BazaarHelper.PutItemInBazaar = function( object )
    status = xpcall( DoItemPut, DoItemPutError, object )
end

function DoItemPut( object )
    if not(object) then return false end
    local objVars = BazaarHelper.CollapseObjVars( object )
    local jsonStr = json.encode( objVars )
    if( jsonStr ~= "{}" ) then
        DoHttpRequest(ServerSettings.Bazaar.PutURL.."?data="..jsonStr)
    end
end

function DoItemPutError( err )
   print( "ERROR:", err )
end

