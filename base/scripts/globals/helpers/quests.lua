Quests = {}

Quests.MaxActive = 3



---------------------------------------------------------------------------------------------
--  Quest Events

Quests.DebugQuestEvents = false

-- New Quest Event Listener
Quests.SendQuestEventMessage = function( _playerObj, _questType, _args, _quantity )
	if( _playerObj and _playerObj:IsValid() ) then
		if(Quests.DebugQuestEvents) then DebugMessage( "QuestEventFired", _questType, _args, _quantity ) DebugTable( _args ) end
		_playerObj:SendMessage("QuestEventFired", _questType, _args, _quantity)
	end
end

-- New Quest Event Handeler
Quests.ProcessQuestEventMessage = function( _playerObj, _questType, _args, _quantity )
	_args = _args or {}
	if(Quests.DebugQuestEvents) then DebugMessage( "HandleQuestEvent", _playerObj, _args, _quantity, _questType ) DebugTable( _args ) end
	Quests.HandleQuestEvent( _playerObj, _args, _quantity, _questType )
end

Quests.HandleQuestEvent = function( _playerObj, _args, _quantity, _questType )

	if(Quests.DebugQuestEvents) then DebugMessage( "Quests.HandleQuestEvent", tostring(_playerObj), tostring(_args), tostring(_quantity),tostring(_questType) ) end
	local activeQuests = {}
	activeQuests = Quests.GetActive( _playerObj )
	if(Quests.DebugQuestEvents) then DebugMessage( "Active Quests" ) end
	if(Quests.DebugQuestEvents) then DebugTable( activeQuests.GatheringChallenges ) end


	for questName, questSteps in pairs( activeQuests ) do 
		if( AllQuests[questName] ) then
			if(Quests.DebugQuestEvents) then DebugMessage( "Processing Quests: ", questName ) end
			for stepIndex, trash in pairs( questSteps ) do
				if( trash.Active == 1 ) then
					if( AllQuests[questName][stepIndex] and AllQuests[questName][stepIndex].QuestData ) then
						local questData = AllQuests[questName][stepIndex].QuestData
						if( Quests.VerifyQuestData( questData ) ) then
							-- If this is not the same quest type we can ignore it!
							if( questData.Type == _questType ) then
								-- Are we region limited?
								local questRegion = questData.Region
								if( not questRegion or _playerObj:IsInRegion( questRegion ) ) then
									if(Quests.DebugQuestEvents) then DebugMessage( "Checking", AllQuests[questName][stepIndex].Name ) end
									if(Quests.DebugQuestEvents) then DebugMessage( "Args : ") DebugTable( _args ) end
									if(Quests.DebugQuestEvents) then DebugMessage( "QuestArgs : ") DebugTable( questData.QuestArgs ) end
	
									if( #_args == #questData.QuestArgs ) then
	
										if(Quests.DebugQuestEvents) then DebugMessage( "Args Passed: ", AllQuests[questName][stepIndex].Name ) end
	
										local passed = true
										for i=1, #questData.QuestArgs do
											-- We want to stop comparing arguements if we've already failed one.
											if( passed ) then
												-- passing a string will require a direct match
												if( type(questData.QuestArgs[i]) == "string" and _args[i] ~= questData.QuestArgs[i] ) then
													passed = false
													if(Quests.DebugQuestEvents) then DebugMessage( "String Match Failed: ", AllQuests[questName][stepIndex].Name ) end
	
												-- passing a table will require the event to match a string in the table
												elseif( type(questData.QuestArgs[i]) == "table" ) then
													local found = false
													for k=1, #questData.QuestArgs[i] do 
														if( _args[i] == questData.QuestArgs[i][k] ) then
															found = true
														end
													end
	
													if( not found )then 
														if(Quests.DebugQuestEvents) then DebugMessage( "Table Match Failed: ", AllQuests[questName][stepIndex].Name ) end
														passed = false 
													end 
	
												-- passing true will accept any value
												elseif( type(questData.QuestArgs[i]) == "boolean" and questData.QuestArgs[i] ~= true ) then
													passed = false
													if(Quests.DebugQuestEvents) then DebugMessage( "Boolean Match Failed: ", AllQuests[questName][stepIndex].Name ) end
												end
											end
										end
	
										if ( passed ) then 
											if(Quests.DebugQuestEvents) then DebugMessage( "PASSED", questName, AllQuests[questName][stepIndex].Name ) end
											local quantity = _quantity or 1
											local playerQuestData = Quests.GetQuestData( _playerObj, questData.Key )
											--DebugTable( questData )
											playerQuestData.Needed = questData.Count or playerQuestData.Needed
	
											if( playerQuestData.Needed ~= nil ) then 
												playerQuestData.Count = math.min(((playerQuestData.Count or 0) + quantity), playerQuestData.Needed)
												Quests.SetQuestData( _playerObj, questData.Key, nil, playerQuestData )
												Quests.AlertQuestUpdate( _playerObj, questData, playerQuestData )
												Quests.ForceCompleteQuest( _playerObj, questName, stepIndex, questData, playerQuestData )
												_playerObj:SendMessage("UpdateChallengeUI")
											end
										else
											if(Quests.DebugQuestEvents) then DebugMessage( "FAILED", questName, AllQuests[questName][stepIndex].Name ) end
										end
									end
								end
							end
						end
					end
				end
			end
		end
	end
end

Quests.VerifyQuestData = function( _questData )
	return(
		_questData and 
		_questData.Key and 
		_questData.QuestArgs and 
		_questData.Count and 
		_questData.Name and 
		_questData.NamePlural and 
		_questData.Type 
	)
end

Quests.AlertQuestUpdate = function( _playerObj, _questData, _playerQuestData )
	_playerObj:SystemMessage("[F2F5A9]".._playerQuestData.Count.." / ".._playerQuestData.Needed.."[-] ".._questData.NamePlural..".","info")
	_playerObj:SystemMessage("[F2F5A9]".._playerQuestData.Count.." / ".._playerQuestData.Needed.."[-] ".._questData.NamePlural..".")
end

Quests.ForceCompleteQuest = function( _playerObj, _questName, _stepIndex, _questData, _playerQuestData )
	if( _playerQuestData.Count >= _playerQuestData.Needed ) then
		Quests.TryEnd(_playerObj, _questName, _stepIndex, nil, true, activeQuests)
		--Quests.ForceCompleteStep( _playerObj, _questName, _stepIndex, true )
	end
end

-- Quest Events
---------------------------------------------------------------------------------------------


Quests.TryStart = function(playerObj, questName, stepIndex, force)
	local stepIndex = stepIndex or 1
	
	if ( Quests.IsPlayerEligible(playerObj, questName, stepIndex, force) ) then
		local quests = playerObj:GetObjVar("Quests") or {}
		local quest = quests[questName]
		if ( not quest ) then
			quests[questName] = {}
			quest = quests[questName]
		end
		local step = quests[questName][stepIndex] or {Time = 0, Active = 0, Completions = 0}
		step["Time"] = DateTime.UtcNow--set start time
		step["Active"] = 1--set active
		quests[questName][stepIndex] = step
		playerObj:SetObjVar("Quests", quests)
		--do start of quest function
		Quests.OnStartOrEnd(playerObj, questName, stepIndex, "Start")
		--starting text
		if ( AllQuests[questName][stepIndex].Silent == nil or AllQuests[questName][stepIndex].Silent == false ) then
			--local name = AllQuests[questName][stepIndex].Name
			--if ( name ) then playerObj:SystemMessage("Started "..tostring(name)..".", "info") end
			local instructions = AllQuests[questName][stepIndex].Instructions
			if ( instructions and not AllQuests[questName][stepIndex].HideTracking ) then playerObj:SystemMessage("[F2F5A9]Quest[-]: "..tostring(instructions)) end
		end
		--update tracker
		playerObj:SendMessage("UpdateMissionUI")
		return true
	end
	return false
end

Quests.HasContextForIneligibleDialogue = function(playerObj, questName, stepIndex)
	if ( playerObj == nil or not playerObj:IsValid() or not AllQuests[questName][stepIndex] ) then
		LuaDebugCallStack("No valid playerObj or questName")
		return false
	end

	local checks = AllQuests[questName][stepIndex].IneligibleCheck
	if ( not checks or #checks < 1 ) then
		return true
	end

	--[[local quests = playerObj:GetObjVar("Quests") or {}
	local meetCount = 0
	local bool = true]]

	return Quests.CheckRequirements(playerObj, checks, false)
end

--Isolated from IsPlayerEligible so it could be called using different tables of Requirements (Like for IneligibleContext)
Quests.CheckRequirements = function(playerObj, requirements, silent)
	
	--iterate through each quest requirement
	for i = 1, #requirements do
		local type = requirements[i][1]
		if ( type == "Disabled" ) then
			if ( not silent or silent ~= true ) then
				playerObj:SystemMessage("Not available yet.", "info")
			end
			return false
		elseif ( type == "Custom" ) then
			if ( not requirements[i][2](playerObj) ) then return false end
		elseif ( type == "QuestActive" ) then
			local quest = requirements[i][2]
			local bool = requirements[i][3]
			if ( bool == nil ) then bool = true end
			if ( Quests.IsActive(playerObj, quest) ~= bool ) then
				return false
			end
		elseif ( type == "MinTimesQuestDone" ) then
			local quest = requirements[i][2]
			local count = requirements[i][3]
			local completions = Quests.GetCompletions(playerObj, quest)
			if completions then
				if ( completions < count ) then
					return false
				end
			else
				return false
			end
		elseif ( type == "MaxTimesQuestDone" ) then
			local quest = requirements[i][2]
			local count = requirements[i][3]
			local completions = Quests.GetCompletions(playerObj, quest)
			if completions then
				if ( completions >= count ) then
					return false
				end
			end
		elseif ( type == "DoneAchievement" ) then
			local achievement = requirements[i][2]
			if ( not HasAchievement(playerObj, achievement) ) then
				return false
			end
		elseif ( type == "Skill" ) then
			local skill = requirements[i][2]
			local value = requirements[i][3] or 0.1
			local skillLevel = GetSkillLevel(playerObj, skill)
			if ( not skillLevel or skillLevel < value ) then
				return false
			end
		elseif ( type == "HasItem" ) then
			local template = requirements[i][2]
			local count = requirements[i][3] or 1
			if ( CountItemsInContainer(playerObj, template) < count ) then
				return false
			end
		elseif ( type == "HasSpecificItem" ) then
			local template = requirements[i][2]
			local objVarName = requirements[i][3]
			local objVarValue = requirements[i][4]

			if not GetItemSpecific(playerObj, template, objVarName, objVarValue) then return false end
		elseif ( type == "HasCraftedItem" ) then
			local template = requirements[i][2]
			local count = requirements[i][3] or 1
			local objVarName = requirements[i][4]
			local objVarValue = requirements[i][5]

			if not HasCraftedItems(playerObj, template, count, objVarName, objVarValue) then return false end
		elseif ( type == "HasEquippedCraftedItem") then
			local template = requirements[i][2]
			local count = requirements[i][3] or 1
			local objVarName = requirements[i][4]
			local objVarValue = requirements[i][5]

			if not HasEquippedCraftedItem(playerObj, template, count, objVarName, objVarValue) then return false end
		elseif ( type == "HasItemInBackpack" ) then
			local template = requirements[i][2]
			local count = requirements[i][3] or 1
			local backpack = playerObj:GetEquippedObject("Backpack")
			if ( backpack ) then
				if ( CountItemsInContainer(backpack, template) < count ) then
					return false
				end
			end
		elseif ( type == "HasCompletedCraftingOrder" ) then
			local desiredSkill = requirements[i][2]
			if not HasCompletedCraftingOrder( playerObj, desiredSkill ) then
				return false
			end
		elseif ( type == "HasMobileEffect" ) then
			local mobileEffect = requirements[i][2]
			local bool = requirements[i][3]
			if ( bool == nil ) then bool = true end
			if ( HasMobileEffect(playerObj, mobileEffect) ~= bool ) then
				return false
			end
		elseif ( type == "StepMustBeActive" ) then
			local professionTier = requirements[i][2]
			local stepToCheck = requirements[i][3]
			local isActive = Quests.IsActive(playerObj, professionTier, stepToCheck);
			if (not isActive) then
				return false
			end
		elseif ( type == "CapStepCompletions" ) then
			local professionTier = requirements[i][2]
			local stepToCheck = requirements[i][3]
			local maxAllowed = requirements[i][4]
			local completions = Quests.GetCompletions(playerObj, professionTier, stepToCheck)
			if completions then
				local underCap = completions < maxAllowed
				if (not underCap) then
					return false
				end
			end
		end -- if end
	end -- loop end
	return true
end


Quests.IsPlayerEligible = function(playerObj, questName, stepIndex, force, silent, activeCheck)
	if ( playerObj == nil or not playerObj:IsValid() or not AllQuests[questName][stepIndex] ) then
		LuaDebugCallStack("No valid playerObj or questName")
		return false
	end
	if ( force ) then return true end
	--check if there is an entry for this quest in the player Quests ObjVar
	local quests = playerObj:GetObjVar("Quests") or {}
	local questKnown = false
	if ( quests and quests[questName] and quests[questName][stepIndex] ) then questKnown = true end
	--false if already at max active quests
	local activeQuests = Quests.GetActive(playerObj)
	if ( not silent and silent == true and activeQuests and CountTable(activeQuests) >= Quests.MaxActive ) then
		playerObj:SystemMessage("Cannot have more than "..Quests.MaxActive.." quests active!","info")
		return false
	end
	--return false if quest is active already
	if ( activeCheck and questKnown and quests[questName][stepIndex]["Active"] == 1 ) then
		--DebugMessage("! IsPlayerEligible() cancelled because Quest is active already.")
		return false
	end
	--check to see if cooldown is up
	local cooldown = AllQuests[questName][stepIndex].Cooldown
	if ( cooldown ) then
		if ( questKnown and quests[questName][stepIndex]["Time"]
		and DateTime.Compare(DateTime.UtcNow, quests[questName][stepIndex]["Time"]:Add(cooldown)) < 0 ) then
			return false
		end
	end
	--return true if quest has no requirements
	local requirements = AllQuests[questName][stepIndex].Requirements
	if ( not requirements or #requirements < 1 ) then
		return true
	end
	
	return Quests.CheckRequirements(playerObj, requirements, silent)

end

Quests.TryEnd = function(playerObj, questName, stepIndex, rewardChoice, force, quests)
	if ( Quests.GoalsMet(playerObj, questName, stepIndex, rewardChoice, force, quests) ) then
		local quests = playerObj:GetObjVar("Quests") or {}
		local quest = quests[questName]
		if ( not quest ) then
			quests[questName] = {}
			quest = quests[questName]
		end
		local step = quests[questName][stepIndex] or {Time = 0, Active = 0, Completions = 0}
		step["Time"] = DateTime.UtcNow--set end time
		step["Active"] = 0--set active
		step["Completions"] = step["Completions"] + 1--increase completion count
		quests[questName][stepIndex] = step
		playerObj:SetObjVar("Quests", quests)
		--give rewards
		Quests.GiveRewards(playerObj, questName, stepIndex, rewardChoice)
		--do end of quest stuff
		Quests.OnStartOrEnd(playerObj, questName, stepIndex, "End")
		--update tracker
		playerObj:SendMessage("UpdateMissionUI")
		--start next step in chain
		local nextStep = AllQuests[questName][stepIndex].NextStep
		local string = ""
		if ( nextStep ) then
			if ( AllQuests[questName][stepIndex] ) then
				Quests.TryStart(playerObj, questName, nextStep, true)
				string = " Updated"
			end
		else
			string = " Completed"
		end
		Quests.UpdateQuestMarkers(playerObj, questName)
		local silent = AllQuests[questName][stepIndex].Silent
			if ( silent == nil or silent == false ) then
				--info text
				local name = AllQuests[questName][stepIndex].Name
				if ( name ) then
					playerObj:SystemMessage("[F2F5A9]"..tostring(name)..string..".[-]", "info")
				end
				--end of quest chain effects
				playerObj:PlayEffect("HolyEffect")
				playerObj:PlayObjectSound("event:/ui/quest_complete", false)
			end
		return true
	end
	return false
end

Quests.GoalsMet = function(playerObj, questName, stepIndex, rewardChoice, force, quests)
	if ( playerObj == nil or not playerObj:IsValid() or not AllQuests[questName][stepIndex] ) then
		--LuaDebugCallStack("No valid playerObj or questName")
		return false
	end

	if force then return true end

	if( AllQuests[questName][stepIndex].QuestData and AllQuests[questName][stepIndex].QuestData.QuestArgs ) then return false end

	--check if there is an entry for this quest in the player Quests ObjVar
	local quests = quests or playerObj:GetObjVar("Quests") or {}
	local questKnown = false
	if not quests[questName] then return false end
	if ( quests and quests[questName][stepIndex] ) then questKnown = true end
	--return false if quest is not active
	if ( quests and quests[questName][stepIndex] and quests[questName][stepIndex]["Active"] == 0 ) then
		return false
	end
	local fails = AllQuests[questName][stepIndex].Fails
	if ( fails ) then
		for i = 1, #fails do
			local type = fails[i][1]
			if ( type == "Custom" ) then
				if ( fails[i][2](playerObj) ) then
					Quests.Fail(playerObj, questName, stepIndex, AllQuests[questName][stepIndex].Silent)
				end
			end
		end
	end
	--check to see if time limit is exceeded
	local timeLimit = AllQuests[questName][stepIndex].TimeLimit
	if ( timeLimit ) then
		if ( quests and DateTime.Compare(DateTime.UtcNow, quests[questName][stepIndex]["Time"]:Add(timeLimit)) > 0 ) then
			Quests.Fail(playerObj, questName, stepIndex, AllQuests[questName][stepIndex].Silent)
		end
	end
	--return false if quest has no goals
	local goals = AllQuests[questName][stepIndex].Goals or {}

	-- Handle QuestData information
	if( AllQuests[questName][stepIndex].QuestData ) then
		if( AllQuests[questName][stepIndex].QuestData.KillCount ) then			
			if not Quests.RunTrackMobileKillCount
			( 
				playerObj, 
				AllQuests[questName][stepIndex].QuestData.Key, 
				AllQuests[questName][stepIndex].QuestData.MobileTemplate, 
				AllQuests[questName][stepIndex].QuestData.MobileName, 
				AllQuests[questName][stepIndex].QuestData.KillCount 
			) then 
				return false 
			end
		end
	end

	if ( not goals or #goals < 1 ) then
		return true
	end
	--iterate through each quest goal
	for i = 1, #goals do
		local type = goals[i][1]
		if ( type == "Custom" ) then
			if ( not goals[i][2](playerObj) ) then return false end
		elseif ( type == "StillEligible") then
			local questName = goals[i][2] 
			local stepIndex = goals[i][3]

			if not Quests.IsPlayerEligible(playerObj, questName, stepIndex) then return false end

		elseif ( type == "Skill" ) then
			local skill = goals[i][2]
			local value = goals[i][3] or 0.1
			local skillLevel = GetSkillLevel(playerObj, skill)
			if ( not skillLevel or skillLevel < value ) then
				return false
			end
		elseif ( type == "HasItem" ) then
			local template = goals[i][2]
			local count = goals[i][3] or 1
			if not ( CountItemsInContainer(playerObj, template) >= count ) then
				return false
			end
			--return false
		elseif ( type == "HasItemInBackpack" ) then
			local template = goals[i][2]
			local count = goals[i][3] or 1
			local crafterObj = goals[i][4] or nil
			local backpack = playerObj:GetEquippedObject("Backpack")
			if ( backpack ) then

				-- Are we looking for crafted items?
				if( crafterObj ) then
					if not ( CountCraftedItemsInContainer(backpack, template, crafterObj) >= count ) then
						return false
					end
				else
					if not ( CountItemsInContainer(backpack, template) >= count ) then
						return false
					end
				end
				
			end
			--return false
		elseif ( type == "HasCraftedItem" ) then
			local template = goals[i][2]
			local count = goals[i][3] or 1
			local objVarName = goals[i][4]
			local objVarValue = goals[i][5]

			if not HasCraftedItems(playerObj, template, count, objVarName, objVarValue) then return false end
		elseif ( type == "DoneAchievement" ) then
			local achievement = goals[i][2]
			if ( not HasAchievement(playerObj, achievement) ) then
				return false
			end
		elseif ( type == "HasMobileEffect" ) then
			local mobileEffect = goals[i][2]
			local bool = goals[i][3] or true
			if ( HasMobileEffect(playerObj, mobileEffect) ~= bool ) then
				return false
			end
		elseif ( type == "MobTeamTypeKills" ) then
			local teamType = goals[i][2]
			local count = goals[i][3]
			if ( not Quests.GetMobKills(playerObj, "TeamType", teamType) >= count ) then
				return false
			end
		elseif ( type == "MobKindKills" ) then
			local kind = goals[i][2]
			local count = goals[i][3]
			if ( not Quests.GetMobKills(playerObj, "Kind", kind) >= count ) then
				return false
			end
		elseif ( type == "MobTemplateKills" ) then
			local template = goals[i][2]
			local count = goals[i][3]
			if ( not Quests.GetMobKills(playerObj, "Template", template) >= count ) then
				return false
			end
		elseif ( type == "InRegion" ) then
			local string = goals[i][2]
			local bool = goals[i][3]
			if ( bool == nil ) then bool = true end
			local regions = GetRegionsAtLoc(playerObj:GetLoc())
			for i = 1, #regions do
				if ( (string.match(tostring(regions[i]), tostring(string)) ~= nil ) == bool ) then
					return true
				end
			end
			return false
		elseif ( type == "Tamed" ) then
			local template = goals[i][2]
			local pets = GetActivePets(playerObj)
			for pet = 1, #pets do
				if ( pets[pet]:GetCreationTemplateId() == template ) then
					local owners = pets[pet]:GetObjVar("PreviousOwners")
					if ( owners and next(owners) ) then
						for owner = 1, #owners do
							if ( GameObj(tonumber(owners[owner])) ~= playerObj ) then
								return false
							end
						end
					end
					return true
				end
			end
			return false
		elseif ( type == "TakeItem" ) then
			local template = goals[i][2]
			local count = goals[i][3] or 1
			if ( not RemoveItemsInBackpack(playerObj, template, count) ) then
				return false
			end
		elseif ( type == "TurnIn" ) then
			if ( not rewardChoice ) then
				return false
			end
		end

	end
	return true
end

Quests.Fail = function (playerObj, questName, stepIndex, silent)
	local quests = playerObj:GetObjVar("Quests") or {}
	if ( quests ) then
		local quest = quests[questName]
		if ( quest and next(quest) ) then
			if ( stepIndex and quest[stepIndex] ) then
				local step = quest[stepIndex]
				active = step["Active"]
				if ( active and active == 1 ) then
					step["Time"] = nil
					step["Active"] = 0--set inactive
					step["Cancelled"] = 1
					quests[questName][stepIndex] = step
					playerObj:SetObjVar("Quests", quests)
					playerObj:SendMessage("UpdateMissionUI")
					if ( not silent and silent ~= false ) then
						local name = AllQuests[questName][stepIndex].Name
						if ( name ) then playerObj:SystemMessage("[F2F5A9]"..tostring(name).." failed.[-]", "info") end
					end
						Quests.UpdateQuestMarkers(playerObj, questName)
						Quests.OnStartOrEnd(playerObj, questName, stepIndex, "Fail")
				end
			elseif ( not stepIndex ) then
				for stepId, step in pairs(quest) do
					active = step["Active"]
					if ( active and active == 1 ) then
						step["Time"] = nil
						step["Active"] = 0--set inactive
						step["Cancelled"] = 1
						quests[questName][stepId] = step
						playerObj:SetObjVar("Quests", quests)
						playerObj:SendMessage("UpdateMissionUI")
						--failure text
						if ( not silent and silent ~= false ) then
							local name = AllQuests[questName][stepId].Name
							if ( name ) then playerObj:SystemMessage("[F2F5A9]"..tostring(name).." failed.[-]", "info") end
							-- This needs a stepIndex which isn't set here. (bphelps)
							--Quests.OnStartOrEnd(playerObj, questName, nil, "Fail")
						end
						Quests.UpdateQuestMarkers(playerObj, questName)
					end
				end
			end
		end
	end
end

Quests.GetStepList = function(playerObj, questName)
	local playerQuests = playerObj:GetObjVar("Quests") or {}
	local steps = AllQuests[questName] or {}
	return steps
end

Quests.GetRelevantStep = function(playerObj, questName)
	local playerQuests = playerObj:GetObjVar("Quests") or {}
	local steps = playerQuests[questName] or {}
	for i=1, #steps do
		local step = steps[i]
		if (step) then 
			local activeInt = step["Active"]
			local completionInt = step["Completions"]
			if activeInt and completionInt then
				if completionInt == 0 then
					--DebugMessage("> Not Complete?",questName, i)
					if activeInt == 1 then
						--DebugMessage("-> Active?",questName, i)
					else
						--DebugMessage("-> Not Active?",questName, i)
					end
					return i
				end
			end
		end		
	end
	return 1
end

Quests.SetQuest = function(targetObj, caller, clippedProfessionName, tierStep)
	if not tierStep or tierStep == "" then DebugMessage("No Tier.Step specified", "info") return end

	local isCommand = caller ~= nil

	local allTiersOfProfession = ProfessionsHelpers.GetProfessionQuests(clippedProfessionName)

	local t, s = tierStep:match"([^.]*).(.*)"
	t = tonumber(t) or 0
	s = tonumber(s) or 0
	local playerQuests = targetObj:GetObjVar("Quests") or {}
	if playerQuests and next(playerQuests) and allTiersOfProfession then
		for i=1, #allTiersOfProfession do
			local tier = allTiersOfProfession[i]
			if tier and AllQuests[tier] then
				for j=1, #AllQuests[tier] do
					local step = AllQuests[tier][j]
					if step then
						local iTier = i - 1
						local lessThan = iTier < t or (iTier == t and j < s)
						local greaterThan = iTier > t or (iTier == t and j > s)
						local equalTo = iTier == t and j == s

						if lessThan then
							--DebugMessage(tostring(clippedProfessionName).." "..tostring(i-1).."."..tostring(j).." is lower than "..tostring(t).."."..tostring(s))
							playerQuests[tier] = playerQuests[tier] or {}
							playerQuests[tier][j] = playerQuests[tier][j] or {}
							playerQuests[tier][j]["Active"] = 0
							playerQuests[tier][j]["Cancelled"] = 0
							if playerQuests[tier][j]["Completions"] then
								if playerQuests[tier][j]["Completions"] < 1 then
									playerQuests[tier][j]["Completions"] = 1
								end
							else
								playerQuests[tier][j]["Completions"] = 1
							end
						elseif greaterThan then
							--DebugMessage(tostring(clippedProfessionName).." "..tostring(i-1).."."..tostring(j).." is higher than "..tostring(t).."."..tostring(s))
							playerQuests[tier] = playerQuests[tier] or {}
							playerQuests[tier][j] = playerQuests[tier][j] or {}
							playerQuests[tier][j] = nil
						elseif equalTo then
							if isCommand then
								DebugMessage("----> "..tostring(clippedProfessionName).." "..tostring(i-1).."."..tostring(j))
								local tierTitle = ProfessionsHelpers.GetProfessionTierTitle(t + 1)
								targetObj:SystemMessage("[F2F5A9]"..tostring(clippedProfessionName).." "..tierTitle..", Step "..tostring(j)..": '"..AllQuests[tier][j].Name.."' is now active.[-]", "info")
								caller:SystemMessage(targetObj:GetName().." OVERRIDDEN TO [F2F5A9]"..tostring(clippedProfessionName).." Tier "..tostring(t).." Step "..tostring(j)..".[-]", "info")
							end
							playerQuests[tier] = playerQuests[tier] or {}
							playerQuests[tier][j] = playerQuests[tier][j] or {}
							playerQuests[tier][j]["Completions"] = 0
							playerQuests[tier][j]["Cancelled"] = 0
							playerQuests[tier][j]["Active"] = 1
							Quests.TryStart(targetObj, tier, j, true)
						end
					end
				end
			end
		end
	end
	targetObj:SetObjVar("Quests", playerQuests)
	targetObj:SendMessage("UpdateMissionUI")
end

--Allows base_ai_npc to get failed quest steps
Quests.GetFailedQuests = function(playerObj, questsInvolvedIn)
	if ( not questsInvolvedIn or not next(questsInvolvedIn) ) then return nil end
	local quests = playerObj:GetObjVar("Quests") or {}
	local failedQuests = {}
	
	for questListIndex = 1, #questsInvolvedIn do
		local questName = questsInvolvedIn[questListIndex][1]
		local stepIndex = questsInvolvedIn[questListIndex][2]
		local canResume = questsInvolvedIn[questListIndex][3] or false
		
		if quests[questName] and quests[questName][stepIndex] then
			if quests[questName][stepIndex]["Cancelled"] and quests[questName][stepIndex]["Cancelled"] == 1 and canResume then
				table.insert(failedQuests, { questName, stepIndex } )
			end
		end
	end
	if #failedQuests > 0 then return failedQuests or nil end
end

Quests.HasFailed = function(playerObj, questsInvolvedIn)
	local failedQuests = Quests.GetFailedQuests(playerObj, questsInvolvedIn)
	if failedQuests then return true else return false end
end

Quests.HasFailedSpecific = function(playerObj, questName, stepIndex)
	local quests = playerObj:GetObjVar("Quests") or {}
	if quests[questName] and quests[questName][stepIndex] then
		return quests[questName][stepIndex]["Cancelled"] and quests[questName][stepIndex]["Cancelled"] == 1 and true
	else
		--DebugMessage("ERROR: Player does not have a record of '",questName,"', step",stepIndex)
		return false
	end
end

Quests.HasFailedAnyStep = function(playerObj, questName)
	local quests = playerObj:GetObjVar("Quests") or {}
	local quest = quests[questName]

	if ( quest and next(quest) ) then
		for i=1, #quest do
			if ( quest[i] and quest[i]["Cancelled"] and quest[i]["Cancelled"] == 1 ) then
				return true
			end
		end
	end
	return false
end

Quests.Resume = function (playerObj, failedQuestNameAndStep)
	local name = failedQuestNameAndStep[1]
	local step = failedQuestNameAndStep[2]
	local quests = playerObj:GetObjVar("Quests") or {}

	if quests[name] and quests[name][step] then 
		quests[name][step]["Cancelled"] = 0
		Quests.TryStart(playerObj, name, step, true)	-- will cause problems if player is given something OnStart
		quests[name][step]["Active"] = 1
		playerObj:SystemMessage("[F2F5A9]"..tostring(AllQuests[name][step].Name).." resumed.[-]", "info")
	end
	playerObj:SetObjVar("Quests", quests)
	playerObj:SendMessage("UpdateMissionUI")
end

Quests.ResumeFailedStep = function(playerObj, questName)
	local quests = playerObj:GetObjVar("Quests") or{}
	local quest = quests[questName]

	if quest then
		for i=1, #quest do
			if quest[i]["Cancelled"] and quest[i]["Cancelled"] == 1 then
				quest[i]["Cancelled"] = 0
				quest[i]["Active"] = 1
				playerObj:SystemMessage("[F2F5A9]"..tostring(AllQuests[questName][i].Name).." resumed.[-]", "info")
				playerObj:SetObjVar("Quests", quests)
				playerObj:SendMessage("UpdateMissionUI")
				return true
			end
		end
	end
	return false
end

Quests.GetFailedStep = function(playerObj, questName)
	local quests = playerObj:GetObjVar("Quests") or{}
	local quest = quests[questName]

	if quest then
		for i=1, #quest do
			if quest[i]["Cancelled"] and quest[i]["Cancelled"] == 1 then
				--DebugMessage("yes cancelled",i)
				return i
			end
		end
	end
	return 1
end

Quests.ForceCompleteStep = function (playerObj, questName, stepIndex, doRewards)
	if(AllQuests[questName]) then
		
		local quests = playerObj:GetObjVar("Quests") or {}
		if ( not quests ) then
			return
		end

		local quest = quests[questName]
		if ( not quest  ) then
			return
		end

		for stepId, step in pairs(quest) do
			if( stepIndex == stepId ) then
				step["Time"] = DateTime.UtcNow--set end time
				step["Active"] = 0--set inactive
				step["Completions"] = (step["Completions"] or 0) + 1
				quests[questName][stepId] = step

				if( doRewards == true ) then
					--give rewards
					Quests.GiveRewards(playerObj, questName, stepId, nil)
				end
				
				Quests.OnStartOrEnd(playerObj, questName, stepId, "End")
				playerObj:SetObjVar("Quests", quests)
			end
		end
			Quests.UpdateQuestMarkers(playerObj, questName)
			playerObj:SendMessage("UpdateMissionUI")
			--end of quest chain effects
			playerObj:PlayEffect("HolyEffect")
			playerObj:PlayObjectSound("event:/ui/quest_complete", false)
	else
		playerObj:SystemMessage("Quest does not exist: "..tostring(questName), "info")
		DebugMessage("[Quests.ForceComplete] Quest does not exist: "..tostring(questName))
	end
end

Quests.ForceComplete = function (playerObj, questName, doRewards)
	if(AllQuests[questName]) then
		local quests = playerObj:GetObjVar("Quests") or {}
		if ( not quests or not next(quests) ) then
			quests = {}
		end
		local quest = quests[questName]
		if ( not quest or not next(quest) ) then
			quests[questName] = {}
			quest = quests[questName]
			local endStep = #AllQuests[questName]
			quest[endStep] = {}
		end
		local steps = 0
		for stepId, step in pairs(quest) do
			steps = steps + 1
			step["Time"] = DateTime.UtcNow--set end time
			step["Active"] = 0--set inactive
			step["Completions"] = (step["Completions"] or 0) + 1
			quests[questName][stepId] = step
			local name = AllQuests[questName][stepId].Name
			if ( name ) then playerObj:SystemMessage("[F2F5A9]"..tostring(name).." forced to complete.[-]", "info") end
			if( doRewards == true ) then
				--give rewards
				Quests.GiveRewards(playerObj, questName, stepId, nil)
			end
			Quests.OnStartOrEnd(playerObj, questName, stepId, "End")
		end
			playerObj:SetObjVar("Quests", quests)
			if ( quests[steps-1] and next(quest, steps - 1) ) then
				Quests.TryStart(playerObj, questName, steps + 1, true)
			else
				Quests.TryEnd(playerObj, questName, steps, 1, true)
			end
			Quests.UpdateQuestMarkers(playerObj, questName)
			playerObj:SendMessage("UpdateMissionUI")
	else
		playerObj:SystemMessage("Quest does not exist: "..tostring(questName), "info")
		DebugMessage("[Quests.ForceComplete] Quest does not exist: "..tostring(questName))
	end
end

Quests.GetRewards = function(playerObj, questName, stepIndex)
	local questInfo = AllQuests[questName]
	if not(questInfo) then return nil end
	
	-- if stepIndex isnt specified, assume they want the rewards for the last step
	if not(stepIndex) then
		stepIndex = #questInfo
	end

	return questInfo[stepIndex].Rewards
end

Quests.GiveRewards = function (playerObj, questName, stepIndex, rewardChoice)
	local choice = rewardChoice or 1
	local rewards = AllQuests[questName][stepIndex].Rewards or {}
	
	-- Handle QuestData information
	if( AllQuests[questName][stepIndex].QuestData ) then
		if( AllQuests[questName][stepIndex].QuestData.Tokens ) then
			ChallengeSystemHelper.AdjustTokenAmount(playerObj, AllQuests[questName][stepIndex].QuestData.Tokens)
		end
		if( AllQuests[questName][stepIndex].QuestData.PVPTokens ) then
			ChallengeSystemHelper.AdjustTokenAmount(playerObj, AllQuests[questName][stepIndex].QuestData.PVPTokens, true)
		end
	end
	
	if ( not rewards or #rewards < 1 or choice < 1 ) then return false end
	if ( #rewards > 1 and not choice ) then return false end
	
	rewards = rewards[choice]
	for i = 1, #rewards do
		if ( rewards[i][1] == "Custom" ) then
			rewards[i][2](playerObj)
		elseif ( rewards[i][1] == "Item" ) then
			local template = rewards[i][2]
			local count = rewards[i][3] or 1
			Create.Stack.InBackpack(template, playerObj, count)
			local name = GetTemplateObjectName(template)
			local str = "You have been rewarded with "
			if ( name ) then
				if ( count > 1 ) then
					playerObj:SystemMessage(str..name.." x"..count..".","info")
					playerObj:SystemMessage(str..name.." x"..count..".")
				else
					playerObj:SystemMessage(str..name..".","info")
					playerObj:SystemMessage(str..name..".")
				end
			end
		elseif ( rewards[i][1] == "SetSkill" ) then
			local skillName = rewards[i][2]
			local newSkillLevel = rewards[i][3]
			local skillLevel = GetSkillLevel(playerObj, skillName) or 0
			if ( skillLevel < newSkillLevel ) then
				if ( CanGainSkill( playerObj, skillName, newSkillLevel) ) then
					SetSkillLevel( playerObj, skillName, newSkillLevel, true )
				end
			end
		end
	end
end

Quests.OnStartOrEnd = function(playerObj, questName, stepIndex, startOrEnd)
	if ( not startOrEnd ) then
		return false
	end

	-- Handle QuestData information
	if( AllQuests[questName][stepIndex].QuestData ) then
		if( AllQuests[questName][stepIndex].QuestData.Key ) then
			Quests.ClearQuestData( playerObj, AllQuests[questName][stepIndex].QuestData.Key )
		end
	end

	local tasks = {}
	if ( startOrEnd == "Start" ) then
		-- Handle QuestData information
		if( AllQuests[questName][stepIndex].QuestData ) then
			
			if( AllQuests[questName][stepIndex].QuestData.KillCount ) then
				Quests.InitTrackMobileKillCount
				( 
					playerObj, 
					AllQuests[questName][stepIndex].QuestData.Key, 
					AllQuests[questName][stepIndex].QuestData.MobileTemplate 
				)
			end

			Quests.InitEventTracker
			( 
				playerObj, 
				questName,
				stepIndex
			)

		end

		tasks = AllQuests[questName][stepIndex].OnStart
		
	elseif ( startOrEnd == "End" ) then
		tasks = AllQuests[questName][stepIndex].OnEnd
		
	elseif ( startOrEnd == "Fail" ) then
		-- Handle QuestData information
		if( AllQuests[questName][stepIndex].QuestData ) then
			if( AllQuests[questName][stepIndex].QuestData.Key ) then
				Quests.ClearQuestData( playerObj, AllQuests[questName][stepIndex].QuestData.Key )
			end
		end

		tasks = AllQuests[questName][stepIndex].OnFail
	end

	Quests.UpdateQuestMarkers(playerObj, questName)
	
	if ( not tasks or #tasks < 1 ) then return false end
	for i = 1, #tasks do
		local type = tasks[i][1]
		if ( type == "Custom" ) then
			tasks[i][2](playerObj)
		elseif ( type == "TakeItem" ) then
			local template = tasks[i][2]
			local count = tasks[i][3] or 1
			RemoveItemsInBackpack(playerObj, template, count)
		elseif ( type == "GiveItem" ) then
			local template = tasks[i][2]
			local count = tasks[i][3] or 1
			Create.Stack.InBackpack(template, playerObj, count)
		elseif ( tasks[i][1] == "SetSkill" ) then
			local skillName = tasks[i][2]
			local newSkillLevel = tasks[i][3]
			local skillLevel = GetSkillLevel(playerObj, skillName) or 0
			if ( skillLevel < newSkillLevel ) then
				SetSkillLevel( playerObj, skillName, newSkillLevel, true )
			end
		elseif ( type == "AcceptCraftingOrder" ) then
			local desiredSkill = tasks[i][2]
			TakeCraftingOrderGiveReward( playerObj, desiredSkill )
		elseif ( type == "TakeCraftedItems" ) then
			local template = tasks[i][2]
			local count = tasks[i][3] or 1
			local objVarName = tasks[i][4]
			local objVarValue = tasks[i][5]
			RemoveCraftedItems(playerObj, template, count, objVarName, objVarValue)
		elseif ( type == "TakeCraftedItemFromBackpack" ) then
			local template = tasks[i][2]
			local count = tasks[i][3] or 1
			local objVarName = tasks[i][4]
			local objVarValue = tasks[i][5]
			TakeCraftedItemFromBackpack(playerObj, template, count, objVarName, objVarValue)
		end
	end
end

Quests.GetMobKills = function(playerObj, category, name)
	local playerStats = playerObj:GetObjVar("LifetimePlayerStats")
	if ( playerStats
	and playerStats["CreatureKills"]
	and playerStats["CreatureKills"][category]
	and playerStats["CreatureKills"][category][name] ) then
		return playerStats["CreatureKills"][category][name]
	end
	return 0
end

Quests.GetDialogue = function(playerObj, questsInvolvedIn, startOrEnd)
	if ( not questsInvolvedIn or not next(questsInvolvedIn) ) then return nil end
	local dialogue = {}
	if ( startOrEnd == "Start" ) then
		for questStep = 1, #questsInvolvedIn do
			local questName = questsInvolvedIn[questStep][1]
			local stepIndex = questsInvolvedIn[questStep][2]
			if not stepIndex then LuaDebugCallStack("GetDialogue stepIndex is nil.") end
			if not questName then LuaDebugCallStack("GetDialogue questName is nil.") end
			if not AllQuests[questName] then LuaDebugCallStack("AllQuests["..questName.."] is invalid!") end
			if not AllQuests[questName][stepIndex] then LuaDebugCallStack("AllQuests["..questName.."]["..tostring(stepIndex).."] is invalid!") end
			
			local startDialogue = (AllQuests[questName] and AllQuests[questName][stepIndex]) and AllQuests[questName][stepIndex].StartDialogue
			if ( startDialogue ) then
				if ( Quests.IsPlayerEligible(playerObj, questName, stepIndex, nil, true, true) ) then
					if ( not dialogue[questName] ) then dialogue[questName] = {} end
					dialogue[questName][stepIndex] = startDialogue
				end
			end
		end
	elseif ( startOrEnd == "End" ) then
		local quests = playerObj:GetObjVar("Quests") or {}
		for questStep = 1, #questsInvolvedIn do
			local questName = questsInvolvedIn[questStep][1]
			local stepIndex = questsInvolvedIn[questStep][2]
			if ( quests[questName]
			and quests[questName][stepIndex]
			and quests[questName][stepIndex]["Active"]
			and quests[questName][stepIndex]["Active"] == 1 ) then
				local isEligible = Quests.IsPlayerEligible(playerObj, questName, stepIndex, nil, true, false)
				local endDialogue = AllQuests[questName][stepIndex].EndDialogue
				local doesEndNeedEligible = AllQuests[questName][stepIndex].EndDialogueHasEligibleCheck
				if ( endDialogue ) then
					if ( doesEndNeedEligible ) then
						if ( isEligible ) then
							if ( not dialogue[questName] ) then dialogue[questName] = {} end --used to be 'not dialogue[quest]''
							dialogue[questName][stepIndex] = endDialogue
						end
					else
						if ( not dialogue[questName] ) then dialogue[questName] = {} end
						dialogue[questName][stepIndex] = endDialogue
					end
				end
			end
		end
	elseif ( startOrEnd == "Ineligible" ) then
		local quests = playerObj:GetObjVar("Quests") or {}
		for questStep = 1, #questsInvolvedIn do
			local questName = questsInvolvedIn[questStep][1]
			local stepIndex = questsInvolvedIn[questStep][2]
			local isEligible = Quests.IsPlayerEligible(playerObj, questName, stepIndex, nil, true, false)
			local hasContext = Quests.HasContextForIneligibleDialogue(playerObj, questName, stepIndex)
			local ineligibleDialogue = AllQuests[questName][stepIndex].IneligibleDialogue
			if ( quests[questName]
			and quests[questName][stepIndex]
			and quests[questName][stepIndex]["Active"]
			and quests[questName][stepIndex]["Active"] == 1 ) then
				if ( ineligibleDialogue ) then
					if ( not isEligible ) and hasContext then
						if ( not dialogue[questName] ) then dialogue[questName] = {} end
						dialogue[questName][stepIndex] = ineligibleDialogue
					end
				end
			else
				if stepIndex == 0 then
					if questName and stepIndex and not isEligible then
						if hasContext then 
							if ineligibleDialogue then 
								if ( not dialogue[questName]) then dialogue[questName] = {} end
								dialogue[questName][stepIndex] = ineligibleDialogue 
							end
						end
					end
				end
			end
		end
	end
	return dialogue
end

Quests.GetActiveTierAndStep = function(playerObj, professionName)
	local quests = playerObj:GetObjVar("Quests") or {}
	local hasMatch = false
	local questName
	local tierIndex
	local step
	local questsWithinProfession = {}
	if quests then
		for k,v in pairs(quests) do
			local index = string.find(k, "Profession")
			if index then
				local prof = string.sub(k, 1, (index - 1))
				if prof == professionName then 
					hasMatch = true
					table.insert(questsWithinProfession, k)
				end
			else
				if k == professionName then
					hasMatch = true
					table.insert(questsWithinProfession, k)
				end
			end

		end
	end

	for i=1, #questsWithinProfession do
		local quest = quests[questsWithinProfession[i]]

		for j=1, #quest do
			if quest[j] and quest[j]["Active"] and quest[j]["Active"] == 1 then
				questName = questsWithinProfession[i]
				step = j
			end
		end
	end

	tierIndex = ProfessionsHelpers.GetProfessionTierIndex(professionName, questName)

	return { Tier = tierIndex, Step = step }
end

Quests.IsActive = function(playerObj, questName, stepIndex)
	local quests = playerObj:GetObjVar("Quests") or {}
	if ( quests ) then
		local quest = quests[questName]
		if ( quest and next(quest) ) then
			if ( stepIndex ) then
				if (not quest[stepIndex]) then	-- stepIndex given but not available
					return false
				end
				local active = quest[stepIndex]["Active"]
				if ( active and active == 1 ) then	-- stepIndex given and available
					return true
				end
			else
				for stepId, step in pairs(quest) do 	-- no stepIndex given, checking all of them
					local active = step["Active"]
					if ( active and active == 1 ) then
						return true
					end
				end
			end
		end
	end
	return false
end

Quests.GetActive = function(playerObj)
	local quests = playerObj:GetObjVar("Quests") or {}
	local activeQuests = {}
	for questName, quest in pairs(quests) do
		for stepIndex, step in pairs (quest) do
			local active = step.Active
			if ( active and tonumber(active) == 1 ) then
				activeQuests[questName] = quest
			end
		end
	end
	return activeQuests
end

Quests.UpdateActive = function(playerObj)
	local quests = playerObj:GetObjVar("Quests") or {}
	for questName, quest in pairs (quests) do
		for stepIndex, step in pairs (quest) do
			local active = step["Active"]
			if ( active and active == 1 ) then
				Quests.TryEnd(playerObj, questName, stepIndex,nil,nil,quests)
			end
		end
	end
end

--does not care if quest is active, use IsActive for that
Quests.IsComplete = function(playerObj, questName, stepIndex)
	local quests = playerObj:GetObjVar("Quests") or {}
	if ( quests ) then
		local quest = quests[questName]
		if ( quest and next(quest) ) then
			local stepIndex = stepIndex
			if ( not stepIndex ) then
				local complete = false
				for stepIndex, step in pairs(quest) do
					local completions = step["Completions"]
					if ( completions and completions > 0 ) then complete = true else complete = false end
				end
				return complete
			elseif ( stepIndex ) then
				local step = quest[stepIndex]
				if ( step ) then
					local completions = step["Completions"]
					if ( completions and completions > 0 ) then return true end
				end
			end
		end
	end
	return false
end

Quests.GetCompletions = function(playerObj, questName, stepIndex)
	local quests = playerObj:GetObjVar("Quests") or {}
	if ( quests ) then
		local quest = quests[questName]
		if ( quest and next(quest) ) then
			local completions = 0
			local stepIndex = stepIndex
			if ( not stepIndex ) then
				for stepIndex, step in pairs(quest) do
					completions = step["Completions"]
				end
				return completions
			elseif ( stepIndex ) then
				local step = quest[stepIndex]
				if ( step ) then
					completions = step["Completions"]
					return completions or 0
				end
			end
		else
			return nil
		end
	end
	return 0
end

Quests.GetNodesHarvested = function(playerObj, nodeName)
	local playerStats = playerObj:GetObjVar("LifetimePlayerStats")
	if ( playerStats
	and playerStats["NodesHarvested"]
	and playerStats["NodesHarvested"][nodeName] ) then
		return playerStats["NodesHarvested"][nodeName]
	end
	return 0
end

Quests.InitTrackNodesHarvested = function( playerObj, questKey, mobileTemplate )
	if( type(mobileTemplate) == "string" ) then
		
		local nodesHarvested = Quests.GetNodesHarvested(playerObj, mobileTemplate)
		DebugMessage("INIT: "..tostring(nodesHarvested).." of "..tostring(mobileTemplate).." harvested.")

		Quests.SetQuestData( playerObj, questKey, mobileTemplate.."NodeCount", nodesHarvested )
		Quests.SetQuestData( playerObj, questKey, mobileTemplate.."LastNodeCount", nodesHarvested )
	
	elseif( type(mobileTemplate) == "table" ) then
		
		local killCount = 0
		local lastKillCount = 0

		for i=1, #mobileTemplate do 
			killCount = killCount + Quests.GetNodesHarvested(playerObj, mobileTemplate[i])
		end

		Quests.SetQuestData( playerObj, questKey, "NodeCount", killCount )
		Quests.SetQuestData( playerObj, questKey, "LastNodeCount", killCount )

	end
end

Quests.RunTrackNodesHarvested = function(playerObj, questKey, mobileTemplate, nodeCount, silent)
	nodeCount = nodeCount or 20
	mobileName = mobileName or "Unknown"
	local nodesHarvested = 0
	local nodesNeeded = 0
	local lastNodeCount = 0

	if( type( mobileTemplate ) == "string" ) then

		nodesHarvested = tonumber(Quests.GetNodesHarvested(playerObj, mobileTemplate))
		nodesNeeded = Quests.GetQuestData(playerObj, questKey, mobileTemplate.."NodeCount") + nodeCount
		lastNodeCount = tonumber(Quests.GetQuestData(playerObj, questKey, mobileTemplate.."LastNodeCount"))
		Quests.SetQuestData( playerObj, questKey, mobileTemplate.."LastNodeCount", nodesHarvested )
		
	elseif( type( mobileTemplate ) == "table" ) then

		nodesNeeded = Quests.GetQuestData( playerObj, questKey, "NodeCount" ) + nodeCount
		lastNodeCount = Quests.GetQuestData(playerObj, questKey, "LastNodeCount")

		for i=1, #mobileTemplate do 
			nodesHarvested = nodesHarvested + Quests.GetNodesHarvested(playerObj, "Template", mobileTemplate[i])
		end

		Quests.SetQuestData( playerObj, questKey, "LastNodeCount", nodesHarvested )

	end

	--DebugMessage( "nodesHarvested: " .. nodesHarvested .. " | nodesNeeded: " .. nodesNeeded .. " | lastNodeCount: " .. lastNodeCount )

	if not(silent) then
		if( 
			nodesHarvested > lastNodeCount
			and nodeCount >= (nodeCount - (nodesNeeded - nodesHarvested))
		) then
			playerObj:SystemMessage("You have harvested [F2F5A9]"..(nodeCount - (nodesNeeded - nodesHarvested)).." / "..nodeCount.."[-] "..mobileName..".","info")
			playerObj:SystemMessage("You have harvested [F2F5A9]"..(nodeCount - (nodesNeeded - nodesHarvested)).." / "..nodeCount.."[-] "..mobileName..".")
		end
	end

	if ( nodesHarvested >= nodesNeeded ) then return true else return false end

end


Quests.UpdateQuestMarkers = function(playerObj, curQuestName, silent)
	--Remove existing markers
	Quests.RemoveQuestMarkers(playerObj, curQuestName)

	if (AllQuests[curQuestName] and AllQuests[curQuestName].Markers) then
		return
	end

	if not (Quests.IsActive(playerObj, curQuestName)) then
		return
	end

	--Get current quest step data
	local curStepData = nil
	local curStepIndex = -1
	local quests = Quests.GetActive(playerObj)
	for questName, quest in pairs(quests) do
		if (questName == curQuestName) then
			for stepIndex, step in pairs(quest) do
				if (step["Active"] == 1) then
					curStepData = AllQuests[questName][stepIndex]
					curStepIndex = stepIndex
				end
			end
		end
	end

	--Add quest markers
	local regions = GetRegionsAtLoc(playerObj:GetLoc())
	local expandedLocList = {}
	if (curStepData ~= nil and curStepData.MarkerLocs) then	-- allows for QuestMarkerGroups to work
		for i=1, #curStepData.MarkerLocs do
			if type(curStepData.MarkerLocs[i][1]) == "table" then
				for j=1, #curStepData.MarkerLocs[i] do
					table.insert(expandedLocList, curStepData.MarkerLocs[i][j])
				end
			else
				table.insert(expandedLocList, curStepData.MarkerLocs[i])
			end
		end
	end

	if (curStepData ~= nil and curStepData.MarkerLocs and expandedLocList) then
		for index, coord in pairs(expandedLocList) do
			local loc = Loc(coord[1], coord[2], coord[3])
			local mySubregion = nil
			for i = 1, #regions do
				if ( string.match(regions[i], "Subregion") ) then
					mySubregion = regions[i]
				end
			end
			if ( mySubregion and IsLocInRegion(loc, mySubregion) ) then
				local name = AllQuests[curQuestName][curStepIndex].Name or "Unknown"
				if ( not silent or silent == false ) then
					playerObj:SystemMessage("A marker has been added to your map.", "info")
				end
				AddDynamicMapMarker(playerObj,{Icon="marker_quest", Location=loc, Tooltip=name},"Quest|"..curQuestName.."|"..curStepIndex.."|"..index)
			end
		end
	end
end

Quests.RemoveQuestMarkers = function(playerObj,questName)
	--Remove existing quest markers
	local mapMarkers = playerObj:GetObjVar("MapMarkers")
	if not (mapMarkers) then return end

	local questMarkers = {}
	for index, marker in pairs(mapMarkers) do
		if (marker.Id:match(questName)) then
			RemoveDynamicMapMarker(playerObj,marker.Id)
		end
	end
end

Quests.HasActiveQuestMarkers = function(playerObj,questName)
	local mapMarkers = playerObj:GetObjVar("MapMarkers") or {}
	if not (mapMarkers) then return end

	for index, marker in pairs(mapMarkers) do
		if (marker.Id:match(questName)) then
			return true
		end
	end
	return false
end

Quests.RemoveQuestStep = function( playerObj, questName, stepIndex )
	local quests = playerObj:GetObjVar("Quests") or {}
	if( quests[questName] and quests[questName][stepIndex] ) then
		quests[questName][stepIndex] = nil
	end
	playerObj:SetObjVar("Quests", quests)
end

-- Sets data to be stored for a given quest.
-- @param: playerObj - the player to store the data for
-- @param: questKey - the array key from the AllQuests static data
-- @param: var - a friendly name for this variable, if not given will assume value is a table
-- @param: value - the value to set the variable to
Quests.SetQuestData = function( playerObj, questKey, var, value ) 
	local questData = {}
	if( var ) then
		questData = playerObj:GetObjVar("QuestData"..questKey) or {}
		questData[ var ] = value
	else
		questData = value or {}
	end
	playerObj:SetObjVar("QuestData"..questKey, questData)
end

-- Retrieves a given variables value from the player's quest data.
-- @param: playerObj - the player to retrieve the data from
-- @param: questKey - the array key from the AllQuests static data
-- @param: var - the name of the stored variable (optional)
-- @return: [mixed] - the value of the stored variable or the questData table
Quests.GetQuestData = function( playerObj, questKey, var ) 
	local questData = playerObj:GetObjVar("QuestData"..questKey) or {}
	if( var ) then return questData[var] end
	return questData
end

-- Removes quest data from a player
-- @param: playerObj - the player to erase the data from
-- @param: questKey - the array key from the AllQuests static data
Quests.ClearQuestData = function( playerObj, questKey ) 
	playerObj:DelObjVar("QuestData"..questKey)
end

Quests.InitEventTracker = function( _playerObj, _questName, _stepIndex )
	local quest = AllQuests[_questName][_stepIndex]
	if( not quest or not quest.QuestData or not quest.QuestData.Key or not quest.QuestData.Count ) then return end

	-- Set our initial quest data values
	Quests.SetQuestData( _playerObj, quest.QuestData.Key, "Count", 0 )
	--Quests.SetQuestData( _playerObj, _questKey, "CraftTotal", 0 )
	Quests.SetQuestData( _playerObj, quest.QuestData.Key, "Needed", quest.QuestData.Count )
end

-- Initialzes the variables used to track kill counts with Quests.RunTrackMobileKillCounts()
-- @param: playerObj - the player you want to track mobile kills for
-- @param: questKey - the key from AllQuests that you're tracking kills for
-- @param: mobileTemplate - the template of the mobile you are wanting to track kills of or a table of templates to track multiple
Quests.InitTrackMobileKillCount = function( playerObj, questKey, mobileTemplate )
	if( type(mobileTemplate) == "string" ) then
		
		-- Set how many kills the player already has for this template
		Quests.SetQuestData( playerObj, questKey, mobileTemplate.."KillCount",  Quests.GetMobKills(playerObj, "Template", mobileTemplate) )
		-- Set what the last kill count was, for progress tracking purposes
		Quests.SetQuestData( playerObj, questKey, mobileTemplate.."LastKillCount",  Quests.GetMobKills(playerObj, "Template", mobileTemplate) )
	
	elseif( type(mobileTemplate) == "table" ) then
		
		local killCount = 0
		local lastKillCount = 0

		-- iterate through the mobile templates and set the variable for each
		for i=1, #mobileTemplate do 
			killCount = killCount + Quests.GetMobKills(playerObj, "Template", mobileTemplate[i])
		end

		-- Set how many kills the player already has for this template
		Quests.SetQuestData( playerObj, questKey, "KillCount", killCount )
		-- Set what the last kill count was, for progress tracking purposes
		Quests.SetQuestData( playerObj, questKey, "LastKillCount", killCount )

	end
end

-- Given a player will return how many kills have been completed for the given questkey and templates
-- @param: playerObj - the player you want to track mobile kills for
-- @param: questKey - the key from AllQuests that you're tracking kills for
-- @param: mobileTemplate - the template of the mobile you are wanting to track kills of or a table of templates to track multiple
Quests.GetTrackedKillCount = function( _playerObj, _questKey, _mobileTemplate, _killCount  )
	local killsMade = 0
	local killsNeeded = 0
	local lastKillCount = 0

	if( type( _mobileTemplate ) == "string" ) then
		-- Get the kills the player has made
		killsMade = Quests.GetMobKills(_playerObj, "Template", _mobileTemplate)
		-- Get how many kills are needed to complete the quest.
		killsNeeded = (Quests.GetQuestData(_playerObj, _questKey, _mobileTemplate.."KillCount") or 0) + _killCount
		-- Get last kill count
		lastKillCount = (Quests.GetQuestData(_playerObj, _questKey, _mobileTemplate.."LastKillCount") or 0)

	elseif( type( _mobileTemplate ) == "table" ) then
		killsNeeded = (Quests.GetQuestData( _playerObj, _questKey, "KillCount" ) or 0) + _killCount
		lastKillCount = (Quests.GetQuestData(_playerObj, _questKey, "LastKillCount") or 0)

		-- iterate through the mobile templates and set the variable for each
		for i=1, #_mobileTemplate do 
			killsMade = killsMade + Quests.GetMobKills(_playerObj, "Template", _mobileTemplate[i])
		end
	end

	-- Total kills made
	return (_killCount - (killsNeeded - killsMade))
end

-- Tracks mobile kills progress, reports progress to players, and return if the kills are completed.
-- @param: playerObj - the player you want to track mobile kills for
-- @param: questKey - the key ffrom AllQuests that you're tracking kills for
-- @param: mobileTemplte - the template you're tracking kills of or an table of templates to track multiples
-- @param: mobileName - the player friendly name this mobile is indentified as ie ("Skeleton Mage, Bandit Archer")
-- @param: killCount - how many kills of this mobile are required for success
-- @param: silent (optional) - true if the function should not alert the playerObj when a kill is made
-- @return: boolean - true if kills are complete; false otherwise
Quests.RunTrackMobileKillCount = function(playerObj, questKey, mobileTemplate, mobileName, killCount, silent)
	killCount = killCount or 20
	mobileName = mobileName or ""
	local killsMade = 0
	local killsNeeded = 0
	local lastKillCount = 0

	if( type( mobileTemplate ) == "string" ) then

		-- Get the kills the player has made
		killsMade = Quests.GetMobKills(playerObj, "Template", mobileTemplate)
		-- Get how many kills are needed to complete the quest.
		killsNeeded = Quests.GetQuestData(playerObj, questKey, mobileTemplate.."KillCount") + killCount
		-- Get last kill count
		lastKillCount = Quests.GetQuestData(playerObj, questKey, mobileTemplate.."LastKillCount")
		Quests.SetQuestData( playerObj, questKey, mobileTemplate.."LastKillCount", killsMade )

	elseif( type( mobileTemplate ) == "table" ) then

		killsNeeded = Quests.GetQuestData( playerObj, questKey, "KillCount" ) + killCount
		lastKillCount = Quests.GetQuestData(playerObj, questKey, "LastKillCount")

		-- iterate through the mobile templates and set the variable for each
		for i=1, #mobileTemplate do 
			killsMade = killsMade + Quests.GetMobKills(playerObj, "Template", mobileTemplate[i])
		end

		Quests.SetQuestData( playerObj, questKey, "LastKillCount", killsMade )

	end

	if not(silent) then
		-- If the player made a new kill lets alert them.
		if( 
			killsMade > lastKillCount -- have we progressed in kills?
			and killCount >= (killCount - (killsNeeded - killsMade)) -- are we not over our quota (ie. 6/5 kills) ?
		) then
			playerObj:SystemMessage("You have slain [F2F5A9]"..(killCount - (killsNeeded - killsMade)).." / "..killCount.."[-] "..mobileName..".","info")
			playerObj:SystemMessage("You have slain [F2F5A9]"..(killCount - (killsNeeded - killsMade)).." / "..killCount.."[-] "..mobileName..".")
		end
	end

	-- If our kill count is above or equal to are required amount, we're good to go!
	if ( killsMade >= killsNeeded ) then return true else return false end

end