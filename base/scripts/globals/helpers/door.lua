Door = {}

Door.AutoCloseDelay = TimeSpan.FromSeconds(5)
Door.OpenCloseDelay = TimeSpan.FromSeconds(1)

Door.Open = function(door, autoclose, template)
    if not( door ) then
        LuaDebugCallStack("[Door.Open] door object not provided.")
        return false
    end
    local Door = Door
    if ( Door.IsOpen(door) ) then return end
    door:SetSharedObjectProperty("IsOpen", true)
    door:ClearCollisionBounds()
    if ( autoclose ) then
        if not( door:HasModule("door_auto_close") ) then
            door:AddModule("door_auto_close")
        end
    end
end

Door.Close = function(door, template)
    if not( door ) then
        LuaDebugCallStack("[Door.Close] door object not provided.")
        return false
    end
    if ( door:HasModule("door_auto_close") ) then
        door:DelModule("door_auto_close")
    end
    if not( Door.IsOpen(door) ) then return end
    door:SetSharedObjectProperty("IsOpen", false)
    door:SetCollisionBoundsFromTemplate(template or door:GetCreationTemplateId())
    -- auto unstuck them
    MoveMobilesOutOfObject(door)
end

Door.IsOpen = function(door)
    if not( door ) then
        LuaDebugCallStack("[Door.IsOpen] door object not provided.")
        return false
    end
    return ( door:GetSharedObjectProperty("IsOpen") == true )
end

Door.Lock = function(door, silent)
    if not( door ) then
        LuaDebugCallStack("[Door.Lock] door object not provided.")
        return false
    end
    if ( Door.IsLocked(door) ) then return end
    door:SetObjVar("locked",true)
    if not( silent ) then
        door:PlayObjectSound("event:/objects/doors/door/door_lock")
    end
    SetTooltipEntry(door,"lock","[FF0000]*Locked*[-]",-1)
end

Door.Unlock = function(door, silent)
    if not( door ) then
        LuaDebugCallStack("[Door.Unlock] door object not provided.")
        return false
    end
    if not( Door.IsLocked(door) ) then return end
    door:DelObjVar("locked")
    if not( silent ) then
        door:PlayObjectSound("event:/objects/doors/door/door_unlock")
    end
    RemoveTooltipEntry(door,"lock")
end

Door.IsLocked = function(door)
    if not( door ) then
        LuaDebugCallStack("[Door.IsLocked] door object not provided.")
        return false
    end
    return door:HasObjVar("locked")
end

Door.GetPassThroughLoc = function(door,user)
    local myLoc = door:GetLoc()
    local userLoc = user:GetLoc()

    if userLoc:Distance2(myLoc) > 5 then 
        return nil,"distance"
    end

    local distance = 0
    local loop = 1
    local projectionStep = 1
    local chosenPos

    while loop < 4 do
        local step = loop * projectionStep
        distance = 0
        local cardinals = 
        {
            myLoc:Project(45, step),
            myLoc:Project(135, step),
            myLoc:Project(225, step),
            myLoc:Project(315, step),
            myLoc:Project(0, step),
            myLoc:Project(90, step),
            myLoc:Project(180, step),
            myLoc:Project(270, step),
        }

        for i=1, #cardinals do
            local cardinalDistance = userLoc:Distance2(cardinals[i])
            if cardinalDistance > distance and IsPassable(cardinals[i]) then
                distance = cardinalDistance
                chosenPos = cardinals[i]
            end
        end

        if chosenPos then loop = 5 else loop = loop + 1 end
    end

    if chosenPos then 
        return chosenPos        
    else
        return nil,"passable"
    end
end