--[[
    Provides functions to assist spawning monsters and mobiles.
]]


HelperSpawns = {}
HelperSpawns.DefaultRadius = 35
HelperSpawns.DefaultSpawnSpeedModifier = 1
HelperSpawns.MaximumSpawnSpeedModifier = 0.3 -- 70% faster spawns
HelperSpawns.PowerLevels = { 4, 8, 16, 30, 48, 99999 } -- minimum ObjVar("Attack") values
HelperSpawns.PlayerSkillPointsPerLevel = 20 -- how many player skill points should each level represent?
HelperSpawns.MinimumSpawnWeightIncluded = 10 -- what is the minimum spawn weight a monster has to be to be included the power calculations?
HelperSpawns.CheckControllerExists = function( spawn_controller )
    if not (spawn_controller ) then 
        LuaDebugCallStack("Called without a spawn_controller defined.")
        return false
    end

    return true
end

-- Get the amount of power a mob level is worth
HelperSpawns.GetPowerFromLevel = function( level )
    level = level or 0
    return level * HelperSpawns.PlayerSkillPointsPerLevel
end

-- Determines if the spawn_controller needs to adjust spawn based on player proximity
HelperSpawns.IsDynamicProximitySpawner = function( spawn_controller )

    if not( HelperSpawns.CheckControllerExists(spawn_controller) ) then return false end

    return spawn_controller:HasObjVar("ProximitySpawnIncrease")

end

-- Takes the power of the spawn and the spawn_controller to determine how much spawn speeds should be reduced
HelperSpawns.CalculateSpawnSpeedModifier = function( spawn_controller,  mSpawnPower)
    local modifier = HelperSpawns.DefaultSpawnSpeedModifier
    mSpawnPower = mSpawnPower or 0
    -- Do we need to use the modifier?
    if( HelperSpawns.IsDynamicProximitySpawner(spawn_controller) ) then
        local playersPower = HelperSpawns.GetPlayersPowerInSpawnRegionOrRange(spawn_controller)
        
        modifier = (mSpawnPower / playersPower)
    end

    modifier = math.min( math.max( HelperSpawns.MaximumSpawnSpeedModifier, modifier ), HelperSpawns.DefaultSpawnSpeedModifier )
    spawn_controller:SetObjVar("SpawnSpeedModifier", modifier)
    return modifier
end

-- Gets the spawnTable from the spawn_controller
HelperSpawns.GetSpawnTable = function(  spawn_controller )
    
    if not( HelperSpawns.CheckControllerExists(spawn_controller) ) then return {} end
    return spawn_controller:GetObjVar("spawnTable") or {}
end

HelperSpawns.GetMaximumSpawnCount = function(  spawn_controller )
    
    if not( HelperSpawns.CheckControllerExists(spawn_controller) ) then return {} end
    
    return spawn_controller:GetObjVar("spawnCount") or 1
end

-- Calculates the power of the monsters of the spawn_controller at full spawn
HelperSpawns.CalculateFullSpawnPower = function( spawn_controller )
    local power = 0
    
    if not( HelperSpawns.CheckControllerExists(spawn_controller) ) then return power end
    
    -- Get spawnTable
    local spawnTable = HelperSpawns.GetSpawnTable( spawn_controller )

    -- Loop through the spawnTable and get the attacks
    for i=1, #spawnTable do 
        local attack = GetTemplateObjVar( spawnTable[i].Template, "Attack" )
        for level=1, #HelperSpawns.PowerLevels do 
            if( attack <= HelperSpawns.PowerLevels[level] and (not spawnTable[i].Weight or spawnTable[i].Weight >= HelperSpawns.MinimumSpawnWeightIncluded) ) then
                power = power + level
            end
        end
    end

    return (
        (power / #spawnTable) * HelperSpawns.PlayerSkillPointsPerLevel -- We average out the power level and multiply it by our player skill levels
        * HelperSpawns.GetMaximumSpawnCount( spawn_controller) -- we then multiple by the maximum number of mobs that can be spawned at a given time
    ) 

end

HelperSpawns.CalculateMobLevel = function( attackValue )
    if (attackValue == nil ) then return 0 end

    for level=1, #HelperSpawns.PowerLevels do 
        if( tonumber(attackValue) <= HelperSpawns.PowerLevels[level] ) then
            return level
        end
    end
end

-- Returns a table of players in the given spawn_controller's area of influence.
HelperSpawns.GetPlayersPowerInSpawnRegionOrRange = function( spawn_controller, players )
    local power = 0

    -- We did pass any players
    if( not players ) then
        -- Foreach player in our spawn radius add up their power
        HelperSpawns.ForeachPlayerInSpawnRegion( spawn_controller, 
        function(player)
            if( not IsGod(player) or TestMortal(player) ) then
                power = power + math.min(GetSkillTotal( player ), ServerSettings.Skills.PlayerSkillCap.Total)
            end
        end)
    else
        for player,v in pairs(players) do 
            if( not IsGod(player) or TestMortal(player) ) then
                power = power + math.min(GetSkillTotal( player ), ServerSettings.Skills.PlayerSkillCap.Total)
            end
        end
    end

    -- Return our total players power
    return power

end

HelperSpawns.ForeachPlayerInSpawnRegion = function( spawn_controller, cb )
    local players = {}
    if not( HelperSpawns.CheckControllerExists(spawn_controller) ) then return false end

    -- Determine if we have a radius or region we spawn in
    local spawnRegion = spawn_controller:GetObjVar("spawnRegion") or nil
    local spawnRadius = spawn_controller:GetObjVar("spawnRadius") or HelperSpawns.DefaultRadius

    -- Get players in region or radius
    if( spawnRegion ) then
        players = FindObjects(SearchMulti({SearchUser(),SearchRegion(spawnRegion,true)}))
    else
        players = FindObjects(SearchPlayerInRange(spawnRadius,true))
    end

    -- Calculate the power of players in the Region or Range
    for k, player in pairs( players ) do
        if(cb) then cb(player) end
    end

end

-- Calulcate the power vacuum between players and monsters
HelperSpawns.CalculatePowerVacuum = function( totalPlayersPower, totalSpawnsPower )
    return math.max( totalPlayersPower - totalSpawnsPower, 0 )
end

