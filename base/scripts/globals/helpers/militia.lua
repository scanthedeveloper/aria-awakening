require 'stackable_helpers'
Militia = {}
Militia._UpdatePlayersPerFrame = 100

--[[ Major Functions ]]

--- Puts a player into a militia, fails if player already in militia
-- @param playerObj
-- @param militiaId
-- @return the GlobalVarUpdateResult event id or false if already in militia/bad params
Militia.AddPlayer = function(playerObj, militiaId)
    if ( playerObj == nil ) then
        LuaDebugCallStack("[Militia.AddPlayer] Nil playerObj provided.")
        return false
    end
    if ( Militia.GetDataById(militiaId) == nil ) then
        LuaDebugCallStack("[Militia.AddPlayer] Invalid Militia Id provided '"..tostring(militiaId).."'")
        return false
    end

    -- already in a militia, stop here.
    if ( Militia.GetId(playerObj) ~= nil ) then return false end
    
    -- save the militia members list
    Militia.SetVar("Members."..militiaId, function(record)
        -- set the value as the time joined
        record[playerObj] = DateTime.UtcNow
        return true
    end,
    function(success)
        if ( success ) then
            -- set the militia id on the player
            Militia.SetId(playerObj, militiaId)
        end
    end)
end

--- Determines whether a player meets basic criteria to join a Militia
-- @param playerObj
-- @return true or false
Militia.ValidateJoin = function(playerObj)
    if ( GetTotalBaseStats(playerObj) >= ServerSettings.Stats.TotalPlayerStatsCap - 20 and not IsMurderer(playerObj) ) then return true end
    return false
end

--- Remove a player from a militia if they are in one
-- @param playerObj
Militia.RemovePlayer = function(playerObj, force)
    if ( playerObj == nil ) then
        LuaDebugCallStack("[Militia.RemovePlayer] Nil playerObj provided.")
        return false
    end
    local militiaId = Militia.GetId(playerObj)
    if ( militiaId == nil ) then return false end

    Militia.SetVar("Members."..militiaId, function(record)
        -- remove the player from the militia
        record[playerObj] = nil
        return true
    end,
    function(success)
        if ( success ) then
            -- remove the militia from the player
            Militia.SetId(playerObj, nil)
        end
    end)
end

--- Set/Delete a player's militia Id, also tons of other stuff
-- @param playerObj
-- @param value New militia id
Militia.SetId = function(playerObj, value)
    if ( value ) then
        local militiaData = Militia.GetDataById(value)
        if ( militiaData == nil ) then return end
        playerObj:SetObjVar("Militia", value)
        -- set the militia icon
        --playerObj:SetSharedObjectProperty("Faction", militiaData.Town)
        --playerObj:SetSharedObjectProperty("FriendlyFactions", militiaData.Town)
        -- set militia player vars to defaults, unless they have favor already
        local favor = ServerSettings.Militia.SignupFavor or 1
        favor = favor + Militia.GetFavor(playerObj)
        Militia.SetPlayerVarsDefault(playerObj, favor)
        Militia.UpdatePlayerVars(playerObj)
        -- update pets
        ForeachActivePet(playerObj, function(pet)
        UpdatePetMilitia(pet, value, militiaData)
        end, true)
        playerObj:SystemMessage("You are now loyal to "..militiaData.Name..".", "event")
    else
        -- create a unique event identifier
        local eventId = uuid()
        -- setup a listener for the global var update result
        RegisterSingleEventHandler(EventType.GlobalVarUpdateResult, eventId, function(success, name, record)
            if ( success ) then
                playerObj:SetSharedObjectProperty("Faction", "None")
                playerObj:SetSharedObjectProperty("FriendlyFactions", "")
                playerObj:SystemMessage("You have denounced your militia.", "event")

                Militia.RemovePlayerFromFavorTable(playerObj)
                Militia.ClearPlayerVars(playerObj)
                if playerObj:HasObjVar("MilitiaResign") then playerObj:DelObjVar("MilitiaResign") end
                if playerObj:HasObjVar("Militia") then playerObj:DelObjVar("Militia") end
                -- update pets
                ForeachActivePet(playerObj, UpdatePetMilitia, true)
            end
        end)

        local militiaId = Militia.GetId(playerObj)
        if ( militiaId ~= nil ) then
            local amount = Militia.GetFavor(playerObj)
            local writeFunction = function(record)
                -- default militia total favor to 0
                if ( record[militiaId] == nil ) then record[militiaId] = 0 end

                -- remove their favor from militia total favor
                record[militiaId] = record[militiaId] - amount

                return true
            end
            -- write to the global variable
            GlobalVarWrite("MilitiaFavor", eventId, writeFunction)
        end
    end
    Militia.UpdateTitle(playerObj)
    playerObj:SendMessage("UpdateCharacterWindow")
end

--- Resignation for a militia takes time, this starts that 'countdown'
-- @param playerObj
Militia.BeginResignation = function(playerObj)
    local militiaId = Militia.GetId(playerObj)

    if ( militiaId == nil ) then return end

    playerObj:SetObjVar("MilitiaResign", DateTime.UtcNow:Add(ServerSettings.Militia.ResignTime))
end

--- Take a player that just died and reward the player with the most damage that's within range.
-- @param player
Militia.RewardKill = function(player)
    local damagers = player:GetObjVar("Damagers")
    if ( damagers and next(damagers) ) then
        -- get a list of all groups/solos involved in all the damage
        local most = 0
        local winner = nil
        local loc = player:GetLoc()
        for damager,data in pairs(damagers) do
            if ( 
                damager ~= nil
                and
                damager:IsValid()
                and
                Militia.InOpposing(damager, player)
                and
                damager:GetLoc():Distance(loc) < 60
            ) then
                if ( data.Amount > most ) then
                    most = data.Amount
                    winner = damager
                end
            end
        end
        -- transfer points from the player to the winner
        if ( winner ) then
            Militia.TransferFavor(player, winner)
        end
    end
end

-- Rank level is the total tier number (1-50)
Militia.CalculateFavorLevel = function(favorValue)
    favorValue = favorValue or 0

    for i,val in pairs(ServerSettings.Militia.FavorLevels) do
        if(favorValue < val) then
            return i-1
        end
    end

    return #ServerSettings.Militia.FavorLevels
end

Militia.GetRankFromFavorLevel = function (favorLevel)
    if not(favorLevel) then
        LuaDebugCallStack("WHA")
    end
    return math.max(1,math.ceil(favorLevel/10))
end

Militia.GetTierFromFavorLevel = function (favorLevel)
    local tier = favorLevel % 10
    if(tier == 0) then tier = 10 end
    return tier
end

Militia.GetRankNumber = function (playerObj)
    local favor = Militia.GetFavor(playerObj)
    local favorLevel = Militia.CalculateFavorLevel(favor)
    return Militia.GetRankFromFavorLevel(favorLevel), Militia.GetTierFromFavorLevel(favorLevel)
end

Militia.CalculateBaseRPValue = function(favorMember)
    local memberLevel = Militia.CalculateFavorLevel(favorMember)    
    local memberRank = Militia.GetRankFromFavorLevel(memberLevel)    
    --DebugMessage("FAVOR: ".. tostring(favorMember).." RANK: "..tostring(memberRank))
    
    -- value is base kill value * rank
    return ServerSettings.Militia.FavorBaseKillValue * math.pow(2,memberRank - 1)
end

Militia.CalculateFavorFromKill = function(favorAggressor, favorVictim)
    -- value is base kill value * rank
    local aggressorValue = Militia.CalculateBaseRPValue(favorAggressor)
    local victimValue = Militia.CalculateBaseRPValue(favorVictim)

    local aggressorGain = math.ceil(victimValue * (victimValue/aggressorValue))
    local victimMaxLoss = victimValue * 2
    local victimLoss = math.floor(math.min(aggressorGain / 3, victimMaxLoss))

    --DebugMessage("RANK: "..aggressorValue.." : "..victimValue .. " : "..aggressorGain)

    return aggressorGain, victimLoss
end

--- Transfer favor from victim to aggressor, does not enforce they are in opposing militias tho they must both be in opposing militias.
-- @param victim
-- @param aggressor
Militia.TransferFavor = function(victim, aggressor)
    -- cannot gain points while resigning
    --if ( victim:HasObjVar("MilitiaResign") ) then return end
    local aMilitia = Militia.GetId(victim)
    local bMilitia = Militia.GetId(aggressor)
    -- if either are not in a militia stop here
    if ( aMilitia == nil or bMilitia == nil ) then return end
    -- get the favor of A and B
    local favorVictim = Militia.GetFavor(victim)
    if ( favorVictim == nil ) then return end
    -- nothing if in same group
    if ( ShareGroup(victim, aggressor) ) then return end
    -- nothing if you are in a plot
    local agressorLoc = aggressor:GetLoc()
    if ( Plot.GetAtLoc(agressorLoc) ) then return end
    -- nothing if same guild
    if ( GuildHelpers.IsInGuildWith(aggressor,victim) ) then return end
    -- nothing if on the same IP
    if ( not IsGod(aggressor) ) then
        local victimIP = tostring(victim:GetIPAddress())
        local aggressorIP = tostring(aggressor:GetIPAddress())
        if ( victimIP and aggressorIP and victimIP == aggressorIP ) then return end
    end

    -- todo: we could do this more efficiently
    local killerTeam = { {ObjRef=aggressor} }
    local group = aggressor:GetObjVar("Group")
    if(group) then
        local groupRecord = GetGroupRecord(group)
        if(groupRecord) then
            local members = groupRecord.Members
            for i=1,#members do
                local member = members[i]
                if(member and member ~= aggressor and member:IsValid() and Militia.GetId(member) == bMilitia and not(IsDead(member)) and victim:DistanceFrom(member) <= 30) then
                    table.insert(killerTeam, {ObjRef=member})
                end
            end
        end
    end

    if(victim:HasTimer("MilitiaRecentlyKilled")) then
        for i,killerMember in pairs(killerTeam) do
            killerMember.ObjRef:SystemMessage("Victim was recently killed and is not worth any militia rank points.", "info")  
        end
        return
    end
    victim:ScheduleTimerDelay(ServerSettings.Militia.RecentlyKilledTime,"MilitiaRecentlyKilled")

    -- limit rp sharing to 8 playerStats
    while(#killerTeam > 8) do
        local randomIndex = math.random(#killerTeam)
        if(killerTeam[randomIndex].ObjRef ~= aggressor) then
            killerTeam[randomIndex].ObjRef:SystemMessage("You were awarded no rank points due to group size. (exceeds 8 party members)", "info")        
            table.remove(killerTeam,randomIndex)
        end
    end

    -- calculate average rank to determine aggressor rp value
    local totalFavor = 0
    local totalValue = 0
    for i,killerMember in pairs(killerTeam) do
        killerMember.Favor = Militia.GetFavor(killerMember.ObjRef)
        -- value is base kill value * rank
        killerMember.Value = Militia.CalculateBaseRPValue(killerMember.Favor)

        totalValue = totalValue + killerMember.Value
        totalFavor = totalFavor + killerMember.Favor

        --DebugMessage("ADDING "..killerMember.Value.. " "..killerMember.ObjRef:GetName())
    end
    favorAggressor = totalFavor / #killerTeam
    
    local aggressorGain, victimLoss = Militia.CalculateFavorFromKill(favorAggressor, favorVictim)
    -- dont let loss go negative
    victimLoss = math.min(victimLoss,favorVictim)

    local favorVictimNew = favorVictim - victimLoss

    --DebugMessage("aggressorGain "..tostring(aggressorGain).. " victimLoss "..tostring(victimLoss) .. " favorAggressor "..tostring(favorAggressor).." totalFavor "..tostring(totalFavor))

    if ( victimLoss <= 0 ) then
        victim:SystemMessage("You are worth no militia rank points to your killer at this time.", "info")        
    else
        victim:SystemMessage("You have lost "..victimLoss.." militia rank points.", "info")

        --update victim
        local controllerIDVictim = Militia.GetRosterControllerID(victim)
        if ( controllerIDVictim == nil ) then return nil end
        Militia.SetFavor(victim, favorVictimNew)
        controllerIDVictim:SendMessageGlobal("AdjustFavorTable", victim, favorVictimNew)
    end    

    if( aggressorGain > 0 ) then
        --update aggressor
        local controllerIDAggressor = Militia.GetRosterControllerID(aggressor)
        --DebugMessage("controllerIDAggressor "..tostring(controllerIDAggressor))
        if ( controllerIDAggressor == nil ) then return nil end        

        local militiaBonus = GlobalVarReadKey("Militia.RPBonus",bMilitia) or 0
        local killerRPMultiplier = 1 + militiaBonus

        -- split up games among gruop
        for i,killerMember in pairs(killerTeam) do
            -- todo: distribute more rps to higher rank members
            local killerMemberGain = math.ceil(aggressorGain * killerMember.Value / totalValue)

            -- calculate the amount of favor to transfer
            local favorMultiplier = Militia.GetFavorDRMultiplier(victim, killerMember.ObjRef)
            -- factor in diminishing returns
            killerMemberGain = killerMemberGain * favorMultiplier
            -- apply rp cap and multiplier
            killerMemberGain = math.min(killerMember.Value * 2,killerMemberGain) * killerRPMultiplier

            killerMember.Favor = killerMember.Favor + math.ceil(killerMemberGain)

            if ( killerMemberGain <= 0 ) then
                killerMember.ObjRef:SystemMessage("Victim is not worth any militia rank points at this time.", "info")        
            else
                killerMember.ObjRef:SystemMessage("You have gained "..killerMemberGain.." militia rank points.", "info")
                killerMember.ObjRef:SystemMessage("You have gained "..killerMemberGain.." militia rank points.")
                killerMember.ObjRef:SystemMessage("Total RP Value: "..tostring(aggressorGain))
                killerMember.ObjRef:SystemMessage("Kill Participants: "..tostring(#killerTeam))
                killerMember.ObjRef:SystemMessage("Your share: "..tostring(math.ceil(aggressorGain * killerMember.Value / totalValue)))
                killerMember.ObjRef:SystemMessage("Diminished Return: "..tostring((1 - favorMultiplier) * 100).."%")
                if(militiaBonus > 0) then
                    killerMember.ObjRef:SystemMessage("Militia Rally Bonus: "..tostring(militiaBonus*100).."%")
                end
                
                Militia.SetFavor(killerMember.ObjRef, killerMember.Favor)
                Militia.UpdateFavorDR(victim, killerMember.ObjRef)
                controllerIDAggressor:SendMessageGlobal("AdjustFavorTable", killerMember.ObjRef, killerMember.Favor)
            end
        end        
    else
        for i,killerMember in pairs(killerTeam) do
            killerMember:SystemMessage("Victim is not worth any militia rank points at this time.", "info")  
        end
    end
end

Militia.ForceSetFavor = function (target, amount)
    local favor = Militia.GetFavor(target)
    if(amount ~= favor) then
        if(amount > favor) then
            target:SystemMessage("You have gained "..amount.." militia rank points.", "info")
        else
            target:SystemMessage("You have lost "..amount.." militia rank points.", "info")
        end

        local controllerIDVictim = Militia.GetRosterControllerID(target)
        if ( controllerIDVictim == nil ) then return nil end
        Militia.SetFavor(target, amount)
        controllerIDVictim:SendMessageGlobal("AdjustFavorTable", target, amount)
    end
end

--- Checks if current season should end, updates global with new season, triggers reset of favor tables, recalculates standings
-- @param none
Militia.CheckForAndStartNewSeason = function()
    local season = GlobalVarRead("Militia.CurrentSeason")
    if ( season == nil or season.StartDate == nil or DateTime.UtcNow > season.EndDate ) then
        Militia.DoNewSeason()
    end
end

Militia.DoNewSeason = function(durationDays)
    Militia.SetVar("CurrentSeason", function(record)
        local durationDays = durationDays or ServerSettings.Militia.SeasonDurationDays or 90
        local durationDays = TimeSpan.FromDays(durationDays)
        local now = DateTime.UtcNow
        local monthsTable = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }
        local seasonTable = {
            Name = monthsTable[now.Month].." "..now.Year,
            StartDate = now,
            EndDate = now:Add(durationDays),
        }
        for k, v in pairs(record) do
            record[k] = nil
        end
        for k, v in pairs(seasonTable) do
            record[k] = v
        end

        return true
    end,
    function(success)
        if ( success ) then
            Militia.UpdateValidCharactersVars()
            Militia.WipeAllFavorTables()
            DebugMessage("New Militia season successfully started")
        end
    end)
end

Militia.SetDaysUntilEnd = function(durationDays)
    Militia.SetVar("CurrentSeason", function(record)
        local durationDays = durationDays or ServerSettings.Militia.SeasonDurationDays or 90
        local durationDays = TimeSpan.FromDays(durationDays)
        local now = DateTime.UtcNow
        local seasonTable = {
            EndDate = now:Add(durationDays),
        }
        for k, v in pairs(seasonTable) do
            record[k] = v
        end
        return true
    end,
    function(success)
        if ( success ) then
            DebugMessage("New Militia end date set.")
        end
    end)
end

--- Sends a message to the controller that matches the militia ID to delete it's favor table
-- @param militiaId
Militia.WipeFavorTable = function(militiaId)
    local rosterController = GlobalVarRead("Militia.RosterController."..militiaId)
    if ( rosterController == nil or rosterController["ID"] == nil ) then return nil end
    rosterController["ID"]:SendMessageGlobal("WipeFavorTable")
end
Militia.WipeAllFavorTables = function()
    for i = 1, 3 do
        Militia.WipeFavorTable(i)
    end
end

--- Sends a message to the controller that matches the militia ID to calculate ranks for that Militia
-- @param militiaId
Militia.CalculateRanks = function(militiaId)
    local rosterController = GlobalVarRead("Militia.RosterController."..militiaId)
    if ( rosterController == nil or rosterController["ID"] == nil ) then return nil end
    rosterController["ID"]:SendMessageGlobal("CalculateAndUpdateRanks")
end
Militia.CalculateAllRanks = function()
    for i = 1, 3 do
        Militia.CalculateRanks(i)
    end
end

Militia.GetHighestRankedPlayer = function(militiaId)
    local leader = "None"
    local standings = GlobalVarRead("Militia.Standings."..militiaId)
    if ( standings == nil ) then return leader end
    for k, v in pairs(standings) do
        if ( type(k) == "userdata" and v[1] == 1 ) then
            leader = k:GetCharacterName()
        end
    end
    return leader
end

Militia.GetOnlinePlayers = function()
    local onlineMilitiaMembers = {}
    for i = 1, 3 do
        local militiaMembers = GlobalVarRead("Militia.Members."..i)
        if ( militiaMembers ~= nil ) then
            for memberObj, v in pairs(militiaMembers) do
                if ( GlobalVarReadKey("User.Online", memberObj) ) then
                    table.insert(onlineMilitiaMembers, memberObj)
                end
            end
        end
    end
    return onlineMilitiaMembers
end

--- Gets and updates a player's militia ObjVars
-- @param playerObj
-- @return rank name, rank number, if applicable
Militia.UpdateValidCharactersVars = function()
    -- tell all cluster controllers accross all regions to reset all players.
    for regionAddress, v in pairs(GetClusterRegions()) do
        if ( regionAddress == ServerSettings.RegionAddress ) then
            --cluster control should always be master, this is called by master controller pulse
            local clusterController = GlobalVarReadKey("ClusterControl", "Master")
            if ( clusterController ~= nil ) then
                clusterController:SendMessage("Militia.UpdateValidCharactersVars")
            end
        elseif ( IsClusterRegionOnline(regionAddress) ) then
            MessageRemoteClusterController(regionAddress, "Militia.UpdateValidCharactersVars")
        end
    end
end

Militia.Chat = function(from, militiaId , line)
    local name = ""
	if ( from ~= nil) then
		local actualName = from:GetObjVar("actualName")

		if (actualName == nil) then
			name = from:GetName()
		else
			name = actualName
		end
	end

    local rankNumber,tier = Militia.GetRankNumber(from)

    local militiaMembers = GlobalVarRead("Militia.Members."..militiaId)
    if ( militiaMembers ~= nil ) then
        from:SetObjVar("MilitiaChatCooldown", DateTime.UtcNow:Add(ServerSettings.Militia.ChatCooldown))
        for memberObj, v in pairs(militiaMembers) do
            if ( GlobalVarReadKey("User.Online", memberObj) ) then                
                local broadcast = false
                local rankString = "("..Militia.GetRankName(from,rankNumber,tier)..")"
                if ( rankNumber >= 5 ) then
                    broadcast = true
                    rankString = "([FF0000]"..Militia.GetRankName(from,rankNumber,tier).."[-])"
                end
                memberObj:SendMessageGlobal("Militia.Chat", name, broadcast, rankString, line)
            end
        end
    end
end

Militia.EventMessageMilitia = function(message, militiaId)
    local militiaMembers = GlobalVarRead("Militia.Members."..militiaId)
    if ( militiaMembers ~= nil ) then
        for memberObj, v in pairs(militiaMembers) do
            if ( GlobalVarReadKey("User.Online", memberObj) ) then                
                memberObj:SendMessageGlobal("MilitiaEventMessage", message)
            end
        end
    end
end

Militia.EventMessagePlayers = function(message)
    if ( message == nil ) then return false end
    local onlineMilitiaPlayers = Militia.GetOnlinePlayers()
    if ( onlineMilitiaPlayers and next(onlineMilitiaPlayers) ) then
        for i = 1, #onlineMilitiaPlayers do
            onlineMilitiaPlayers[i]:SendMessageGlobal("MilitiaEventMessage", message)
        end
    end
end

Militia.BuffAllPlayers = function()
    local usersFrames = {}
    local frameCount = 1
    local processedUsers = 0

    local onlineUsers = GlobalVarRead("User.Online")
    for user,y in pairs(onlineUsers) do
        if( processedUsers <= Militia._UpdatePlayersPerFrame ) then
            if not( usersFrames[frameCount] ) then usersFrames[frameCount] = {} end
            table.insert( usersFrames[frameCount], user)
        else
            frameCount = frameCount + 1
            processedUsers = 0
            if not( usersFrames[frameCount] ) then usersFrames[frameCount] = {} end
            table.insert( usersFrames[frameCount], user)
        end
        processedUsers = processedUsers + 1
    end

    -- Do our updates spread across frames
    for i=1, #usersFrames do 
        CallFunctionDelayed(TimeSpan.FromMilliseconds( i * 20 ), function()
            for j=1, #usersFrames[i] do 
                Militia.BuffPlayer( usersFrames[i][j] )
            end
        end)
    end
end

Militia.BuffPlayer = function( user )
    if ( user:IsValid() ) then
        user:SendMessage("MilitiaApplyBuffs")
    else
        user:SendMessageGlobal("MilitiaApplyBuffs")
    end
end

Militia.RewardMilitiaCurrency = function(playerObj, currencyAmount, silent)
    if ( playerObj == nil or not playerObj:IsValid() ) then return false end
    if ( currencyAmount == nil or currencyAmount < 1 ) then return false end
    if ( IsDead(playerObj) ) then return false end
    local backpack = playerObj:GetEquippedObject("Backpack")
    if ( backpack == nil ) then return false end
    Create.Stack.InBackpack("allegiance_talent", playerObj, currencyAmount, nil, function(obj)
        if ( obj ) then
                if not( silent ) then
                    playerObj:NpcSpeech("+ "..currencyAmount.." talents", "combat")
                end
        else
                DebugMessage("Talent creation to backpack failed for playerId: "..playerObj.Id)
        end
    end, false, false, true)
end

Militia.AddGlobalResource = function (militiaId,resourceType,amount)
    local curAmount = GlobalVarReadKey("Militia.GlobalResources."..militiaId,resourceType)
    if not(curAmount) or curAmount < ServerSettings.Militia.GlobalResources.MaximumValue then                    
        Militia.SetVar("GlobalResources."..militiaId,function (record)
            local newValue = (record[resourceType] or 0) + amount
            record[resourceType] = math.min(ServerSettings.Militia.GlobalResources.MaximumValue,newValue)

            return true
        end)
    end
end

Militia.ConsumeGlobalResource = function (militiaId,resourceType,amount)
    local curAmount = GlobalVarReadKey("Militia.GlobalResources."..militiaId,resourceType)
    if (curAmount) and (curAmount - amount >= 0) then                    
        Militia.SetVar("GlobalResources."..militiaId,function (record)
            local newValue = (record[resourceType] or 0) - amount
            record[resourceType] = newValue

            return true
        end)

        return true
    else
        return false
    end
end

Militia.AccrueGlobalResources = function ()
    -- spiritwood for spiritwood mill 
    local events = GlobalVarRead("Militia.Events")
    for controller,eventData in pairs(events) do
        if(eventData[7] == "MilitiaEventSpiritwoodLumber") then
            local winner = eventData[2]
            if(winner) then
                Militia.AddGlobalResource(winner,"Spiritwood",ServerSettings.Militia.GlobalResources.AccrualPerMinute)
            end
        end
    end    
end

Militia.CalculateMilitiaBonuses = function ()
    local totalPlayers = 0
    local militiaCounts = { 0, 0, 0 }
    local militiaBonus = { 0, 0, 0 }
    for militiaId=1, #ServerSettings.Militia.Militias do
        local militiaMembers = GlobalVarRead("Militia.Members."..militiaId)
        if ( militiaMembers ~= nil ) then
            for memberObj, v in pairs(militiaMembers) do
                if ( GlobalVarReadKey("User.Online", memberObj) ) then                
                    militiaCounts[militiaId] = militiaCounts[militiaId] + 1
                    totalPlayers = totalPlayers + 1
                end
            end            
        end
    end

    for militiaId=1, #ServerSettings.Militia.Militias do
        local militiaPct = militiaCounts[militiaId] / totalPlayers
        if(militiaPct <= ServerSettings.Militia.UnderpopulatedPct) then
            -- 30% bonus for underpopulated milititas
            militiaBonus[militiaId] = ServerSettings.Militia.UnderpopulatedBonus + ServerSettings.Militia.GlobalBonus
        end
    end

    Militia.SetVar("RPBonus",function (record)            
            for militiaId=1, #ServerSettings.Militia.Militias do
                record[militiaId] = militiaBonus[militiaId]
            end

            return true
        end)
end

Militia.GetGlobalResource = function (militiaId,resourceType)
    return GlobalVarReadKey("Militia.GlobalResources."..militiaId,resourceType) or 0
end


--[[ Updaters ]]

--- Searches for the roster controller for given ID, and creates it if necessary, used by militia recruiter/leader
-- @param militiaLeader (mobileObj) Militia leader
Militia.CheckAndCreateRosterController = function(militiaLeader)
    local militiaId = Militia.GetId(militiaLeader)
    if ( militiaId ~= nil ) then
        --search for controller, create if necessary
        local templateNames = {"pyros_militia_roster_controller", "helm_militia_roster_controller", "eldeir_militia_roster_controller"}
        local template = templateNames[militiaId]
        local searchResult = FindObjects(SearchTemplate(template), militiaLeader)
        local next = next
        --check if table is empty
        if not( next(searchResult) ) then
            --create controller
            Create.AtLoc(template, militiaLeader:GetLoc(), function(controller)
                controller:SetObjVar("NoReset", true)
            end)
        end
    end
end

--- Gets and updates a player's militia ObjVars
-- @param playerObj
-- @return rank name, rank number, if applicable
Militia.UpdatePlayerVars = function(playerObj)
    if ( playerObj == nil or type(playerObj) ~= "userdata" or not playerObj:IsValid() ) then return false end
    --don't update anything if there is no season
    local currentGlobalSeason = GlobalVarRead("Militia.CurrentSeason")
    if ( currentGlobalSeason == nil or currentGlobalSeason.StartDate == nil ) then return false end
    --reset militia player vars if the season is unknown or doesn't match, even for non-militia
    local playerVarSeason = playerObj:GetObjVar("MilitiaSeasonStart")
    if ( playerVarSeason == nil or playerVarSeason ~= currentGlobalSeason.StartDate ) then
        if ( playerObj:HasObjVar("Favor") ) then playerObj:DelObjVar("Favor") end
		if ( playerObj:HasObjVar("FavorDR") ) then playerObj:DelObjVar("FavorDR") end
		Militia.ClearPlayerVars(playerObj)
		Militia.SetPlayerVarsDefault(playerObj)
		if ( Militia.GetId(playerObj) ~= nil ) then
            CallFunctionDelayed(TimeSpan.FromSeconds(5),function()playerObj:SystemMessage("A new Militia season has started! Rankings have been reset!","event")end)
		end
    end

    local militiaId = Militia.GetId(playerObj)
    if ( militiaId == nil ) then return false end

    --get percentile and standing from globals
    local playerStats = GlobalVarReadKey("Militia.Standings."..militiaId, playerObj)
    local playerVarSeason = playerObj:GetObjVar("MilitiaSeasonStart")
    local standingsSeason = GlobalVarReadKey("Militia.Standings."..militiaId, "SeasonStartDate")
    if ( playerStats ~= nil and 
    playerStats[1] ~= nil and 
    playerStats[2] ~= nil and 
    playerVarSeason ~= nil and 
    standingsSeason ~= nil and
    playerVarSeason == standingsSeason ) then
        local percentile = playerStats[1]
        local standing = playerStats[2]
        local favor = playerStats[3]
        playerObj:SetObjVar("MilitiaPercentile", percentile)
        playerObj:SetObjVar("MilitiaStanding", standing)
        playerObj:SetObjVar("FavorAtLastUpdate", favor)            
    else
        playerObj:SetObjVar("MilitiaPercentile", 0)
        playerObj:SetObjVar("MilitiaStanding", 0)
    end
    
    --StartMobileEffect(playerObj, "MilitiaTopRank")
end

--- Set a player's militia ObjVars to default values for a new enlistee
-- @param playerObj
-- @param (optional) favor
Militia.SetPlayerVarsDefault = function(playerObj, favor)
    if not( playerObj:HasObjVar("Militia") ) then return false end
    local newFavor = favor or ServerSettings.Militia.SignupFavor or 1
    Militia.SetFavor(playerObj, newFavor)
    Militia.SetFavorFromEvents(playerObj, ServerSettings.Militia.FavorFromEvents)

    local currentGlobalSeason = GlobalVarRead("Militia.CurrentSeason")
    if ( currentGlobalSeason ~= nil and currentGlobalSeason.StartDate ~= nil ) then
        playerObj:SetObjVar("MilitiaSeasonStart", currentGlobalSeason.StartDate)
    end
end

--- Removes all militia ObjVars on the player EXCEPT for Favor, MilitiaResign, Militia
-- @param playerObj
Militia.ClearPlayerVars = function(playerObj)
    playerObj:DelObjVar("FavorFromEvents")
    playerObj:DelObjVar("MilitiaStanding")
    playerObj:DelObjVar("MilitiaPercentile")
    playerObj:DelObjVar("MilitiaSeasonStart")
    -- these are no longer used but cleared for legacy reasons
    playerObj:DelObjVar("MilitiaRankName")
    playerObj:DelObjVar("MilitiaRankNumber")
end

--[[ Favor ]]

Militia.GetFavor = function(player)
    if not(player) then
        LuaDebugCallStack("NIL PLAYER")
    end
    return player:GetObjVar("Favor") or 0
end

Militia.GetMilitiaName = function(playerObj)
    local militiaId = Militia.GetId(playerObj)
    if ( militiaId == nil ) then return nil end
    local militiaData = Militia.GetDataById(militiaId)
    if ( militiaData ) then
        return militiaData.Name
    end
    return nil
end

Militia.SetFavor = function(player, favor)
    player:SetObjVar("Favor", favor)

    Militia.UpdateTitle(player)
end

Militia.SetFavorFromEvents = function(player, favor)
    player:SetObjVar("FavorFromEvents", favor)
end

--- Remove a player's entry from their respective roster controller
-- @param playerObj
Militia.RemovePlayerFromFavorTable = function(playerObj)
    local controllerID = Militia.GetRosterControllerID(playerObj)
    if ( controllerID == nil ) then return nil end
    controllerID:SendMessageGlobal("AdjustFavorTable", playerObj, 0, true)
end

--- Gets a player's corresponding roster controller ID
-- @param playerObj
Militia.GetRosterControllerID = function(playerObj)
    local militiaId = Militia.GetId(playerObj)
    if ( militiaId == nil ) then return nil end
    local rosterController = GlobalVarRead("Militia.RosterController."..militiaId)
    if ( rosterController == nil or rosterController.ID == nil ) then return nil end

    return rosterController.ID
end

--[[ Diminishing Returns ]]

--- Adds a kill count to the aggressor(killer) FavorDR ObjVar, creates expiration time if it's the first kill in the entry
-- @param victim (playerObj)
-- @param aggressor (playerObj)
Militia.UpdateFavorDR = function(victim, aggressor)
    local FavorDR = aggressor:GetObjVar("FavorDR") or {}
    if ( FavorDR[victim] == nil ) then
        --set kills to 1, set entry expire date
        FavorDR[victim] = {1, DateTime.UtcNow:Add(ServerSettings.Militia.DRResetTime)}
    else
        FavorDR[victim][1] = FavorDR[victim][1] + 1
    end
    aggressor:SetObjVar("FavorDR", FavorDR)
end

--- Returns the diminishing returns modifier for a kill on a victim by an aggressor, based on FavorDR table
-- @param victim (playerObj)
-- @param aggressor (playerObj)
-- @return number from 0 to 1, to be used as multiplier for Favor transfer
Militia.GetFavorDRMultiplier = function(victim, aggressor)
    Militia.PruneFavorDR(aggressor)
    local KillsToZero = ServerSettings.Militia.DRKillsToZero
    local FavorDR = aggressor:GetObjVar("FavorDR") or {}
    local KillsOnTarget = 0
    if ( FavorDR[victim] ~= nil and FavorDR[victim][1] ~= nil ) then
        KillsOnTarget = FavorDR[victim][1]
    end
    if ( KillsOnTarget >= KillsToZero ) then 
        return 0
    else
        return ( 1 - ( KillsOnTarget / KillsToZero ) )
    end
end

--- Removes expired diminishing returns entries from a player's FavorDR ObjVar table
-- @param playerObj
Militia.PruneFavorDR = function(playerObj)
    local FavorDR = playerObj:GetObjVar("FavorDR") or {}
    for k, v in pairs(FavorDR) do
        if ( FavorDR[k] ~= nil ) then
            if ( DateTime.UtcNow > v[2] ) then
                FavorDR[k] = nil
            end
        end
    end
    playerObj:SetObjVar("FavorDR", FavorDR)
end

--[[ Gets, Basic Convenience Functions ]]

--- Set a Global militia variable by name
-- @param id
-- @param name The variable name
-- @param writeFunction
-- @param cb
Militia.SetVar = function(name, writeFunction, cb)
    if ( name == nil or writeFunction == nil ) then return false end
    SetGlobalVar(string.format("Militia.%s", name), writeFunction, cb)
end

--- Get a Global militia variable by name
-- @param id
-- @param name The variable name
-- @return value of the global variable or nil
Militia.GetVar = function(id, name)
    if ( id == nil or name == nil ) then return nil end
    return GlobalVarRead("Militia."..name.."."..tostring(id))
end

--- Determine if two players are in opposing militias to eachother (both in militias but are not the same militia)
-- @param victim
-- @param aggressor
-- @return true if both players are in opposing militias
Militia.InOpposing = function(victim, aggressor)
    Verbose("Militia", "Militia.InOpposing", victim, aggressor)
    local militiaA = victim:GetSharedObjectProperty("Faction")
    if ( militiaA ~= nil and militiaA ~= "" and militiaA ~= "None" ) then
        local militiaB = aggressor:GetSharedObjectProperty("Faction")
        if ( militiaB ~= nil and militiaB ~= "" and militiaB ~= "None" ) then
            return militiaA ~= militiaB
        end
    end
    return false
end

Militia.CanLootPlayer = function(corpse, player)
    if ( IsDead(corpse) ) then
        local militiaConflict = corpse:GetObjVar("MilitiaConflict")
        if ( militiaConflict ) then
            local playerMilitia = player:GetObjVar("Militia")
            if ( playerMilitia ) then
                if ( playerMilitia ~= militiaConflict ) then
                    return true
                end
            end
        end
    end
    return false
end

--- determines whether a mobile meets criteria to attack another mobile, in context of Militias
-- @param victim
-- @param aggressor
-- @return true or false
Militia.CanAttackMilitiaAttackable = function(aggressor, victim)
    --if victim does not have MilitiaAttackable, victim is attackable
    local militiaAttackable = victim:GetObjVar("MilitiaAttackable")
    if ( militiaAttackable == nil or militiaAttackable ~= true ) then return true
    --if aggressor is not in a Militia and assuming victim has MilitiaAttackable, return false
    elseif ( aggressor:GetObjVar("Militia") == nil ) then return false
    else
        --otherwise return true
        return true
    end
end

--- Get a player's militia id
-- @param playerObj
-- @return the players militia id or nil
Militia.GetId = function(mobileObj)
    if not(mobileObj) then
        LuaDebugCallStack("ERROR")
        return
    end

    -- If we have an owner we need to use them as the militia check
    local mobileOwner = mobileObj:GetObjVar("controller")
    if( mobileOwner ) then mobileObj = mobileOwner end

    return mobileObj:GetObjVar("Militia")
end

Militia.CheckItemRequirements = function(playerObj, itemObj)
    return true
end

--- The the details about a militia by the militia's id
-- @param militiaId
-- @return The militia data for the militia id or nil
Militia.GetDataById = function(militiaId)
    for i=1,#ServerSettings.Militia.Militias do
        if ( ServerSettings.Militia.Militias[i].Id == militiaId ) then
            return ServerSettings.Militia.Militias[i]
        end
    end
    return nil
end

Militia.GetPercentile = function(playerObj)
    return playerObj:GetObjVar("MilitiaPercentile") or 0
end

Militia.GetStanding = function(playerObj)
    return playerObj:GetObjVar("MilitiaStanding") or 0
end

Militia.GetRankName = function(playerObj,rank,tier)
    if not(rank) then
        rank,tier = Militia.GetRankNumber(playerObj)
    end

    return ServerSettings.Militia.RankNames[rank] .. " " .. ToRomanNumerals(tier)
end

--[[ Titles ]]

--- Update a player's title for their militia, does not enfore or check much and is pretty raw
-- @param player
-- @param militia
Militia.UpdateTitle = function(player)
    local checks = {}
    local militia = Militia.GetId(player)
    if ( militia ) then
        table.insert(checks, militia)
    else
        for i = 1, #ServerSettings.Militia.Militias do
            table.insert(checks, i)
        end
    end
    
    local rankNumber = Militia.GetRankNumber(player)

    for i = 1, #checks do
        -- get the militia data
        local militiaData = Militia.GetDataById(checks[i])
        if ( militiaData == nil ) then
            LuaDebugCallStack("[Militia.UpdateTitle] Nil militiaData, is militia valid?")
            return
        end

        --Check if player should earn achievement for current militia
        CheckAchievementStatus(player, "PvP", militiaData.Town.."Militia", rankNumber, {TitleCheck = "Militia"}, "Militia")

        --Check if a player who lost rank can use militia title
        CheckTitleRequirement(player, militiaData.Town.."Militia")
    end
end

Militia.ApplyBuffs = function(player)
    -- DAB MILITIAS DISABLED
    --[[if ( not player:IsPlayer() ) then return false end
    if ( IsDead(player) ) then return false end
    local militiaId = player:GetObjVar("Militia") or TownshipsHelper.GetMilitaId(nil, player)
	if ( militiaId ) then
        player:SendMessage("StartMobileEffect", "MilitiaTerritoryBuff", player, {Duration = TimeSpan.FromDays(1)})
        --player:SendMessage("StartMobileEffect", "MilitiaTopRank", player, {Duration = TimeSpan.FromDays(1)})
    end
    if ( not Militia.HasConflictEffect(player) ) then
        player:SetSharedObjectProperty("Faction", "")
    end]]
end

Militia.FlagForConflict = function(gameObj, radius)
    local targets = {}
    if ( gameObj and gameObj:IsValid() ) then
        table.insert(targets, gameObj)
    else
        return false
    end
    if ( radius ) then
        local radiusTargets = FindObjects(SearchMulti(
			{
				SearchPlayerInRange(radius, true),
				SearchHasObjVar("Militia"),
            }))
        for i = 1, #radiusTargets do
            table.insert(targets, radiusTargets[i])
        end
    end
    for i = 1, #targets do
        if ( targets[i]:IsValid() ) then
            --DebugMessage("Flagging")
            targets[i]:SendMessage("StartMobileEffect", "MilitiaConflict", targets[i], {Duration = ServerSettings.Militia.VulnerabilityDuration})
        end
    end
end

Militia.RefreshConflict = function(playerObj)
    if ( HasMobileEffect(playerObj, "MilitiaConflict") ) then
        --DebugMessage("Refreshing")
        playerObj:SendMessage("StartMobileEffect", "MilitiaConflict", playerObj, {Duration = ServerSettings.Militia.VulnerabilityDuration})
    end
end

Militia.HasConflictEffect = function(mobileObj)
    if ( HasMobileEffect(mobileObj, "MilitiaConflict") ) then return true else return false end
end

-- if mobile died with a militia shared property, save it as an objvar
Militia.SetIdAtDeath = function(corpseObj, mobileObj)
    local militiaProperty = mobileObj:GetSharedObjectProperty("Faction")
    if ( militiaProperty and militiaProperty ~= "" and militiaProperty ~= "None" ) then
        local militiaId = mobileObj:GetObjVar("Militia")
        if ( militiaId ) then
            corpseObj:SetObjVar("MilitiaConflict", militiaId)
        end
    end
end

Militia.RemoveMilitiaWeaponBlessing = function( weaponObj )
    if(weaponObj == nil) then return false end

    local templateName = weaponObj:GetCreationTemplateId()
    local templateData = GetTemplateData(templateName)

    if( weaponObj:HasObjVar("Godly") ) then
        -- Remove ObjVars
        weaponObj:DelObjVar("Cursed")
        weaponObj:DelObjVar("Blessed")
        weaponObj:DelObjVar("Godly")
        weaponObj:DelObjVar("MilitiaRankRequired")
        weaponObj:DelObjVar("RequiresUser")

        -- let them keep double durability
        weaponObj:SetObjVar("Durable",true)

        -- Reset hue and name
        weaponObj:SetHue(templateData.Hue)
        
        -- Set tooltip
        SetItemTooltip(weaponObj)
    end
end

Militia.UpdateKeepHUD = function( playerObj, keepDoor, needsClosed )

    if( keepDoor ) then
        local curHP = GetCurHealth(keepDoor)
        local maxHP = GetMaxHealth(keepDoor)

        UpdateDynamicEventHUD( playerObj,
            {
                Title = "Keep Siege Event",
                Description = "Keep Door Health: ("..curHP.." / "..maxHP..")"
            }
        )

        return true
    else
        needsClosed = true
    end

    if( needsClosed ) then
        CloseDynamicEventHUD( playerObj )
    end

    return false

end

--[[DebugMessage("AggressorLevel,VictimLevel,AggressorGain,VictimLoss")
for i=5,#ServerSettings.Militia.FavorLevels,5 do
    for j=5,#ServerSettings.Militia.FavorLevels,5 do        
        local result,result2 = Militia.CalculateFavorFromKill(ServerSettings.Militia.FavorLevels[i],ServerSettings.Militia.FavorLevels[j],1.0)
        DebugMessage(i..","..j..","..result..","..result2)
    end    
end]]