-- NOTE: Just adding to this table does not add new stats. There are many places in code that reference the stat names directly (We should fix this one day)
allStatsTable = {
	"Strength",
	"Agility",
	"Intelligence",
	--"Constitution",
	"Wisdom",
	"Will"
}

-- This is not in server settings because it is deeply engrained in the combat system. Not something that is easily modified
ARMORSLOTS ={
	"Head",
	"Chest",
	"Legs"
}

function HasArrowsInBackpack(mobile, silent)
	local backpack = mobile:GetEquippedObject("Backpack")
	local perferredArrow = mobile:GetObjVar("PreferredArrowType") or "Arrows"
	
	if ( backpack ) then
		for i=1,#ArrowTypes do
			local type = ArrowTypes[i]
			if ( type == perferredArrow and FindItemInContainerRecursive(backpack, function(item) return item:GetObjVar("ResourceType") == type end) ) then
				return true
			end
		end
	end

	if( silent ~= true ) then
		mobile:SystemMessage("Out of "..ArrowTypesNames[perferredArrow]..".", "info")
	end

	return false
end

--- Determine if a mobile is disabled (cannot use items or abilites)
-- @param mobileObj
-- @return true if mobile is disabled.
function IsMobileDisabled(mobileObj)
    Verbose("Combat", "IsMobileDisabled", mobileObj)
	return mobileObj:HasObjVar("Disabled")
end

--- Determine if the mobile has an effect that makes them immune.
-- @param mobileObj
-- @return true if mobile is immune
function IsMobileImmune(mobileObj)
    Verbose("Combat", "IsMobileImmune", mobileObj)
	return ( HasMobileEffect(mobileObj, "Immune") )
end

--- Display the indicator that a mobile is immune
-- @param mobileObj
function DoMobileImmune(mobileObj, type)
	if ( type ) then
		mobileObj:NpcSpeech(string.format("[08FFFF]%s immune[-]", type), "combat")
	else
		mobileObj:NpcSpeech("[08FFFF]immune[-]", "combat")
	end
end

--- Determine if a is withing range of b
-- @param objA gameObj
-- @param objB gameObj
-- @param range double
-- @return true if within in range.
function WithinRange(objA, objB, range)
    Verbose("Combat", "WithinRange", objA, objB, range)
	if( objA == nil or not objA:IsValid() or objB == nil or not objB:IsValid() ) then return false end
	if ( objA == objB ) then return true end
	local distance = objA:DistanceFrom(objB)
    if( distance == nil ) then return false end
    return ( distance <= range )
end

--- Determine if an attacker is within range of a defender given a weaponType.
function WithinWeaponRange(attacker, defender, weaponType)
    Verbose("Combat", "WithinWeaponRange", attacker, defender, weaponType)
    return WithinRange(attacker, defender, GetCombatWeaponRange(attacker, defender, weaponType))
end

--- Determine if an attacker is within range of a defender given a range.
function WithinCombatRange(attacker, defender, range)
    Verbose("Combat", "WithinCombatRange", attacker, defender, range)
    return WithinRange(attacker, defender, GetCombatRange(attacker, defender, range))
end

--- Get the distance attacker must be from defender for combat.
-- @param attacker mobileObj
-- @param defender mobileObj
-- @param range double, weapon range or barehand range if not provided
-- @return distance to consider attacker close enough to defender for combat
function GetCombatRange(attacker, defender, range)
    Verbose("Combat", "GetCombatRange", attacker, defender, range)
	return ( range or Weapon.GetRange("BareHand") ) + GetBodySize(attacker) + GetBodySize(defender)
end

--- Convenience function that does what GetCombatRange does, but instead takes a weaponType as last parameter
-- @param attacker mobileObj (for body size/weapon range)
-- @param defender mobileObj (for body size)
-- @param weaponType(optional) string Defaults to weapon type of primary weapon for attacker
-- @return distance to consider attacker close enough to defender for combat
function GetCombatWeaponRange(attacker, defender, weaponType)
    Verbose("Combat", "GetCombatWeaponRange", attacker, defender, weaponType)
	weaponType = weaponType or GetPrimaryWeaponType(attacker)
	return GetCombatRange(attacker, defender, Weapon.GetRange(weaponType))
end

--- Determine if a gameObj is valid to gain skill on
-- @param gameObj
-- @param gainer
-- @return true if valid to gain skill from this gameObj.
function ValidCombatGainTarget(target,gainer)
	Verbose("Combat", "ValidCombatGainTarget", target,gainer)
	-- players cannot gain against players.
	if ( IsPlayerObject(target) ) then return false end

	-- players cannot gain against mobs with owners/previous owners.
	if ( target:HasObjVar("PreviousOwners") ) then return false end

	if ( target:HasObjVar("InvalidTarget") ) then return false end

	-- objects with attackable can be gained, they will default to 0 skill, (set a skill dictionary on them to change it)
	return ( target:HasModule("attackable_object_skill_gain") or (target:IsMobile() and not IsDead(target) and not target:GetObjVar("Invulnerable")) )
end

--- Determine if an attacker can attack/damage a victim
-- @param attack mobileObj, the mobile doing the targeting.
-- @param victim mobileObj, the mobile being targeted.
-- @return true if valid combat target
function ValidCombatTarget(attacker, victim, silent)
    Verbose("Combat", "ValidCombatTarget", attacker, victim, silent)
    -- all combat is disabled in limbo.
    if ( ServerSettings.RegionAddress == "Limbo" and IsPlayerCharacter(attacker) ) then return false end
	-- validate our attacker and victim
	if ( attacker == nil or victim == nil ) then return false end
	if ( not attacker:IsValid() or not victim:IsValid() ) then return false end
	-- explicitly force a mobile non valid always
	if ( victim:HasObjVar("InvalidTarget") ) then
		if ( IsPlayerCharacter(attacker) and silent ~= true ) then
			attacker:SystemMessage("Invalid Target.", "info")
		end
		return false
	end

	-- Militia Champions can only be damaged by militia members of opposing militias
	if( victim:HasObjVar("MilitiaBoss") ) then
		local attackerMilitiaID = Militia.GetId(attacker)
		local victimMilitiaID = Militia.GetId(victim)
		if( not attackerMilitiaID or ( attackerMilitiaID == victimMilitiaID ) ) then
			if( attacker:IsPlayer() and not silent ) then
				attacker:SystemMessage("You are unable to harm " .. victim:GetName() .. ".")
			end
			return false
		end
	end

	--if victim is related to a militia, determine if it is attackable by the attacker
	if ( Militia.CanAttackMilitiaAttackable(attacker, victim) ~= true ) then
		if not( silent ) then
			attacker:SystemMessage("Cannot interfere with Militia matters.", "info")
		end
		return false
	end

	if ( victim:IsMobile() ) then
		if ( IsDead(victim) ) then return false end
		-- reassign to owners if applicable
		local victimOwner = victim:GetObjVar("controller")
		-- don't reassign owner if victim is possessed
		if ( victimOwner and victimOwner:IsValid() and not IsPossessed(victim)) then victim = victimOwner end
		local attackerOwner = attacker:GetObjVar("controller")
		if ( attackerOwner and attackerOwner:IsValid() ) then attacker = attackerOwner end
	else
		if ( victim:HasSharedObjectProperty("IsDestroyed") ) then
			return not(victim:GetSharedObjectProperty("IsDestroyed"))
		end
		if ( victim:GetObjVar("Attackable") ~= true ) then return false end
	end

	if ( IsImmortal(attacker) and not TestMortal(attacker) ) then return true end

	local attackerInZone = WithinCriminalArea(attacker)
	local victimInZone = WithinCriminalArea(victim)
	if ( 
		-- mobiles in arena (or similar) are not valid to mobiles outside of arena
		(
			victimInZone and not attackerInZone
		)
		or
		-- vice versa
		(
			attackerInZone and not victimInZone
		)
	) then
		return false
	end

	-- prevent attacking guard protected stuff when the karma protection flag is on
		-- and both involved are in a karma protected zone
	if (
		attackerInZone
		and
		victimInZone
		and
		(
			ShouldCriminalProtect(attacker, victim, false, silent)
			or
			IsPlayerCharacter(attacker) and IsGuaranteedProtected(victim, attacker)
		)
	) then
		return false
	end

	-- default to attack everything (sandbox!)
	return true
end

---- TODO: Transition this to IsFriend function ( to help with auto targets and AoE effects )
--- Allow friendly actions to a potential friend?
-- @param mobileObj
-- @param potentialFriend mobileObj
-- @param notSilent(optional) Boolean
-- @param return true or false
function AllowFriendlyActions(mobileObj, potentialFriend, notSilent)
    Verbose("Combat", "AllowFriendlyActions", mobileObj, potentialFriend, notSilent)    

	-- reassign variables to the owner if applicable
	mobileObj = mobileObj:GetObjVar("controller") or mobileObj
	potentialFriend = potentialFriend:GetObjVar("controller") or potentialFriend

	-- always friendly to yourself and your pets.
	if ( mobileObj == potentialFriend ) then return true end

	--can't benefit militia mobs
	if ( potentialFriend:HasObjVar("MilitiaAttackable") ) then
		mobileObj:SystemMessage("Cannot interfere with Militia matters.", "info")
		return false
	end

	-- prevent friendly actions against criminals if opted in
	if ( ShouldCriminalProtect(mobileObj, potentialFriend, true, not notSilent) ) then
		return false
	end

	-- npcs don't follow further rules
    if ( not IsPlayerCharacter(mobileObj) or not IsPlayerObject(potentialFriend) ) then return true end

	-- militia rules
	-- prevent people in a militia being benefited by anyone not in their militia
	local theirMilitiaId = Militia.GetId(potentialFriend)
	-- if potential friend is in a Militia
	if ( theirMilitiaId ~= nil ) then
		local myMilitiaId = Militia.GetId(mobileObj)
		-- if mobileObj is not in the same Militia
		if ( myMilitiaId ~= theirMilitiaId ) then
			-- if potential friend is in a player conflict
			if ( HasAnyActiveConflictRecords(potentialFriend, true) ) then
				if ( notSilent ) then
					mobileObj:SystemMessage("Cannot interfere with active Militia conflicts.", "info")
				end
				-- potential friend in a Militia and it's not mobileObj's Militia, end here.
				return false
			end
		end
	end

	-- allow friendly actions.
	return true
end

function IsCombatMap()
	local worldName = ServerSettings.WorldName
	for i,j in pairs(ServerSettings.Misc.NonCombatMaps) do
		if (worldName == j) then
			return false
		end
	end
	return true
end

--Randomly Selects a location from the list of available slots 
function GetHitLocation()	
	local rnd = math.random(1,100)
	if ( rnd <= 33 ) then return "Head" end
	if ( rnd <= 66 ) then return "Legs" end
	return "Chest"
end

function CheckActiveSpellResist(target)
	-- 0 to 50% chance, 10 will = 0, 50 will = 50%
	if ( Success(0.5*((( GetWis(target) or 0 ) - ServerSettings.Stats.IndividualStatMin) / (ServerSettings.Stats.IndividualPlayerStatCap - ServerSettings.Stats.IndividualStatMin))) ) then
		target:PlayEffect("HoneycombShield", 1)
		return true
	end
	return false
end

function GetArmorProficiencyType(target)
	-- Added by bphelps; getting exception when added to equipment cache
	if( target == nil ) then return nil end

	local armorClass = nil
	for i,slot in pairs(ARMORSLOTS) do
		local item = target:GetEquippedObject(slot)
		local curArmorClass = EquipmentStats.BaseArmorStats.Natural.ArmorClass
		if(item) then
			curArmorClass = GetArmorClass(item)			
		end

		if not(armorClass) then
			armorClass = curArmorClass
		elseif(curArmorClass ~= armorClass) then
			return nil
		end
	end

	return armorClass
end

function PlayWeaponSound(target, audioId, weapon, soundParameterName, soundParameterValue)
	if(weapon == nil) then
		weapon = GetPrimaryWeapon(target)
	end
	if( weapon ~= nil) then
		if soundParameterName~=nil and soundParameterValue ~= nil then
			local soundParameters = {}
			soundParameters[soundParameterName] = soundParameterValue

			weapon:PlayObjectSoundWithParameter(audioId, soundParameters, true)
		else
			weapon:PlayObjectSound(audioId, true)
		end
	else
		local soundPrefix = "event:/weapons/general/general_"
		if soundParameterName~=nil and soundParameterValue ~= nil then
			local soundParameters = {}
			soundParameters[soundParameterName] = soundParameterValue
						
			target:PlayObjectSoundWithParameter(soundPrefix..audioId, soundParameters, false)
		else
			target:PlayObjectSound(soundPrefix..audioId, false)
		end
	end
end


function PlayImpactSound(target, soundParameterName, soundParameterValue, attacker, delaySeconds)
	local soundParameters = {}
	soundParameters[soundParameterName] = soundParameterValue
	local weapon = GetPrimaryWeapon(attacker)
	local weaponSFX = EquipmentStats.BaseWeaponStats[Weapon.GetType(weapon)].SFXDir
	if weapon~=nil then
		if (delaySeconds==nil) then
			target:PlayObjectSoundWithParameter("event:/weapons/"..weaponSFX.."/"..weaponSFX.."_impact", soundParameters, false)
		else
			target:ScheduleTimerDelay(TimeSpan.FromSeconds(delaySeconds),"DelayedImpactSound",
				target, soundParameterName, soundParameterValue, attacker)
		end
	else
		if (delaySeconds==nil) then
			target:PlayObjectSoundWithParameter("event:/weapons/general/general_impact", soundParameters, false)
		else
			target:ScheduleTimerDelay(TimeSpan.FromSeconds(delaySeconds),"DelayedImpactSound",
				target, soundParameterName, soundParameterValue, attacker)
		end
	end
end

function PlayShieldSound(target,audioId, shield)
	if(shield == nil) then
		shield = target:GetEquippedObject("LeftHand")
	end
	if( shield ~= nil ) then
		shield:PlayObjectSound(audioId, true)
	end
end

function RandomAttackSoundChance(attacker)
	--if(math.random(1,5) == 1) then 
		----D*ebugMessage(attacker:GetName().. " OUCH!")
		attacker:PlayObjectSound("Attack", true)
		local attackerWeapon = GetPrimaryWeapon(attacker)
		if attackerWeapon~=nil then
			attackerWeapon:PlayObjectSound("Attack", true)
		end
	--end
end

--- Does the graphic/sound of an arrow flying from one mobile to another
-- @param from mobileObj
-- @param to mobileObj
-- @param weapon gameObj
function PerformClientArrowShot(from, to, weapon)
	PlayAttackAnimation(from)
	from:PlayProjectileEffectTo("ArrowEffect", to, 100, 0, "Bone=Bone_Proj_Source")
	PlayWeaponSound(from, "Shoot", weapon)
end

--- Plays the attack animation for mobileObj
-- @param mobile mobileObj
function PlayAttackAnimation(mobile)
	if ( mobile ) then mobile:PlayAnimation("attack") end
end