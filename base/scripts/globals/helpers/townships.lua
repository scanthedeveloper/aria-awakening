
TownshipsHelper = {}
TownshipsHelper.TownshipObjVar = "Township"

TownshipsHelper.TownshipIcons = {
    Eldeir = "icon_Eldeir",
    Pyros = "icon_Pyros",
    Helm = "icon_Helm"
}

TownshipsHelper.Hues = {
    Eldeir = 543,
    Pyros = 204,
    Helm = 447
}

function TownshipsHelper.GetMilitaId( townshipName, playerObj ) 
    townshipName = townshipName or playerObj:GetObjVar("Township")

    if( townshipName ) then

        for i=1, #ServerSettings.Militia.Militias do 
            if( ServerSettings.Militia.Militias[i].Town == townshipName ) then
                return ServerSettings.Militia.Militias[i].Id
            end
        end
    end

    return nil

end

function TownshipsHelper.AddPlayerToTownship( playerObj, townshipName )
    if( not playerObj or not townshipName ) then return end
    playerObj:SetObjVar(TownshipsHelper.TownshipObjVar, townshipName)
end

function TownshipsHelper.RemovePlayerFromTownship( playerObj )
    if( not playerObj ) then return end
    playerObj:DelObjVar(TownshipsHelper.TownshipObjVar)
end

function TownshipsHelper.GetPlayerTownship( playerObj )
    return playerObj:GetObjVar(TownshipsHelper.TownshipObjVar) or nil
end

function TownshipsHelper.IsPlayerInTownship( playerObj, townshipName )
    if( not playerObj or not townshipName ) then return false end
    if( townshipName == TownshipsHelper.GetPlayerTownship(playerObj) ) then
        return true
    else
        return false
    end
end





