MilitiaEvent = {}

MilitiaEvent.GetControllerToStart = function()
    local militiaEventsTable = GlobalVarRead("Militia.Events")
    local militiaNextEventTime = GlobalVarReadKey("Militia.NextEvent", "Time")
    local militiaNextEventWarnings = GlobalVarReadKey("Militia.NextEvent", "StartWarnings")
    if ( militiaEventsTable ~= nil ) then
        if ( militiaNextEventTime == nil or militiaNextEventWarnings == nil ) then return nil end
        if ( DateTime.Compare(DateTime.UtcNow, militiaNextEventTime) < 0 ) then
            --send out a warning that an event is about to start
            if ( militiaNextEventWarnings == 0 and
            DateTime.Compare(DateTime.UtcNow, militiaNextEventTime:Subtract(TimeSpan.FromMinutes(ServerSettings.Militia.EventWarnMinutesBeforeStart))) > 0 ) then
                local message = "Next Militia event will start in "..TimeSpanToWords(militiaNextEventTime:Subtract(DateTime.UtcNow)).."."
                DebugMessage(message)
                Militia.EventMessagePlayers(message)
                Militia.SetVar("NextEvent", function(record)
                    record["StartWarnings"] = 1
                    return true
                end)
            end
        elseif ( DateTime.Compare(DateTime.UtcNow, militiaNextEventTime) > 0 ) then
            --get the number of active, add inactive to a table
            local activeCount = 0
            local inactiveControllers = {}
            --get event type
            local type = MilitiaEvent.ChooseEventType()
            --get events of chosen type
            for k, v in pairs(militiaEventsTable) do
                if ( v[3] == nil ) then return nil end
                if ( v[3] == 1 ) then activeCount = activeCount + 1 end
                if ( v[5] == type ) then
                    if ( v[3] == 0 ) then table.insert(inactiveControllers, k) end
                end
            end
            if ( activeCount < ServerSettings.Militia.EventMaxActive ) then
                --return a random inactive event controller of the chosen event
                if ( inactiveControllers ~= nil and #inactiveControllers > 0 ) then
                math.randomseed(os.time())
                local random = math.random(#inactiveControllers)
                    return inactiveControllers[random]
                end
            end
        end
    end
end

MilitiaEvent.ChooseEventType = function()
    local nTypes = 2
    local lastType = GlobalVarReadKey("Militia.LastEvent", "Type")
    if not( lastType ) then return 1 end
    if ( ServerSettings.Militia.EventTypeRotation == 1 ) then
        if ( lastType == nTypes ) then
            return 1
        else
            return lastType + 1
        end
    elseif ( ServerSettings.Militia.EventTypeRotation == 2 ) then
        math.randomseed(os.time())
        local random = math.random(1, 100)
        for i = 1, nTypes do
            if ( random <= ServerSettings.Militia.EventTypeWeights[i] ) then
                return i
            end
        end
    end
end

MilitiaEvent.GetActiveControllers = function()
    local militiaEventsTable = GlobalVarRead("Militia.Events")
    if ( militiaEventsTable ~= nil ) then
        --get the number of active, add inactive to a table
        local activeControllers = {}
        for k, v in pairs(militiaEventsTable) do
            if ( v[3] == nil ) then return nil end
            if ( v[3] == 1 ) then activeControllers[k] = v end
        end
        --if conditions are met, return all active controllers
		return activeControllers
    end
end

MilitiaEvent.TryStart = function()
    if ( ServerSettings.Militia.EventMaxActive > 0 ) then
        local eventControllerToStart = MilitiaEvent.GetControllerToStart()
        if ( eventControllerToStart ~= nil ) then
            DebugMessage("Attempting to activate event controller "..tostring(eventControllerToStart))
            eventControllerToStart:SendMessageGlobal("StartEvent")
        end
    end
end

MilitiaEvent.FavorReward = function(playerObj)
    -- Favor from events disabled
    --check for and add event favor bonus
    --[[if not( playerObj == nil or playerObj:IsValid() ) then return false end
    local favorFromEvents = playerObj:GetObjVar("FavorFromEvents")
    local favor = playerObj:GetObjVar("Favor")
    local controllerId = Militia.GetRosterControllerID(playerObj)
    local rank = Militia.GetRankNumber(playerObj)
    if ( favor ~= nil and favorFromEvents ~= nil and controllerId ~= nil and favorFromEvents > 0 and rank > 0 and rank < 4 ) then
        favorFromEvents = favorFromEvents - 1
        if ( favorFromEvents < 1 ) then
            playerObj:DelObjVar("FavorFromEvents")
        else
            playerObj:SetObjVar("FavorFromEvents", favorFromEvents)
        end
        favor = favor + 1
        playerObj:SetObjVar("Favor", favor)
        --below line commented so favor doesn't actually enter the pool until death or kill
        --controllerId:SendMessageGlobal("AdjustFavorTable", playerObj, favor)
    end]]
end

MilitiaEvent.UpdateGlobalNextTime = function()
    local militiaNextEventTime = GlobalVarReadKey("Militia.NextEvent", "Time")
    if ( militiaNextEventTime ~= nil and
    DateTime.Compare(DateTime.UtcNow, militiaNextEventTime) < 0 or
    ServerSettings.Militia.EventMaxActive <= 0 ) then return false end
    local random = 1
    Militia.SetVar("NextEvent", function(record)
        math.randomseed(os.time())
        random = math.random(ServerSettings.Militia.EventCooldownMinutesMin, ServerSettings.Militia.EventCooldownMinutesMax)
        record["Time"] = DateTime.UtcNow:Add(TimeSpan.FromMinutes(random))
        record["StartWarnings"] = 0
        return true
    end,
    function(success)
        if ( success ) then
            local militiaNextEventTime = GlobalVarReadKey("Militia.NextEvent", "Time")
            local message = "Next Militia event will start in "..TimeSpanToWords(militiaNextEventTime:Subtract(DateTime.UtcNow)).."."
            DebugMessage(message)
            Militia.EventMessagePlayers(message)
        end
    end)
end

MilitiaEvent.ClearGlobalTable = function()
    DebugMessage("Attempting to clear Militia.Events")
    Militia.SetVar("Events", function(record)
        for k, v in pairs(record) do
            record[k] = nil
        end
        return true
    end,
    function(success)
        if ( success ) then
            DebugMessage("Militia.Events cleared.")
        end
    end)
end

MilitiaEvent.RegisterControllerToGlobalTable = function(controller, pulseInterval)
    if ( GlobalVarReadKey("Militia.Events", controller) == nil ) then
        MilitiaEvent.UpdateGlobalTable(controller)
    end
    controller:ScheduleTimerDelay(pulseInterval, "RegisterControllerToGlobalTable")
end

MilitiaEvent.UpdateGlobalTable = function(controller)
    local loc = controller:GetLoc() or 0
    local winner = controller:GetObjVar("Winner") or 0 
    local active = controller:GetObjVar("Active") or 0
    local label = controller:GetObjVar("Label") or "Unknown"
    local tag = controller:GetObjVar("ControllerTag") or ""
    local type = controller:GetObjVar("Type") or 0
    
    --local entryExists = GlobalVarReadKey("Militia.Events", controller)
    --if ( not entryExists ) then
        --DebugMessage("Militia event controller: "..tostring(controller).." updating active="..active.." event to Militia.Events with label: "..label)
    --end
    Militia.SetVar("Events", function(record)
        record[controller] = {loc, winner, active, label, type, nil, tag}
        return true
    end,
    function(success)
        if ( success ) then
            if ( active ) == 0 then
                MilitiaEvent.UpdateGlobalNextTime()
                local militiaPlayers = Militia.GetOnlinePlayers()
                if ( militiaPlayers and next(militiaPlayers) ) then
                    for i = 1, #militiaPlayers do
                        if ( militiaPlayers[i]:IsValid() ) then
                            militiaPlayers[i]:SendMessage("ApplyMilitiaBuffs")
                        else
                            militiaPlayers[i]:SendMessageGlobal("ApplyMilitiaBuffs")
                        end
                    end
                end
            else
                --save info of the last event
                Militia.SetVar("LastEvent", function(record)
                    record["Type"] = type
                    record["Winner"] = winner
                    return true
                end)
            end
        end
    end)
end

MilitiaEvent.FlagToWinner = function(controller, winner, score)
    local score = score or {0,0,0}
    for i = 1, 3 do
        if ( score[i] == nil ) then score[i] = 0 end
    end
    if ( winner == nil ) then return false end
    if ( winner ~= 1 and score[1] > score[2] and score[1] > score[3] ) then
        controller:SetObjVar("Winner", 1)
        controller:PlayEffect("FireTornadoEffect",5)
        controller:SetSharedObjectProperty("Variation", "Pyros")
        controller:PlayObjectSound("event:/magic/fire/magic_fire_flame_wave")
    elseif ( winner ~= 2 and score[2] > score[1] and score[2] > score[3] ) then
        controller:SetObjVar("Winner", 2)
        controller:PlayEffect("LightningEffect")
        controller:PlayEffect("IceRainEffect")
        controller:SetSharedObjectProperty("Variation", "Helm")
        controller:PlayObjectSound("event:/magic/water/magic_water_ice_rain")
        controller:PlayObjectSound("event:/magic/air/magic_air_lightning_impact")
    elseif ( winner ~= 3 and score[3] > score[1] and score[3] > score[2] ) then
        controller:SetObjVar("Winner", 3)
        controller:PlayEffect("RockDropEffect")
        controller:PlayEffect("DustTrailEffect")
        controller:SetSharedObjectProperty("Variation", "Eldeir")
        controller:PlayObjectSound("event:/magic/earth/magic_earth_bombardment_throw")
        CallFunctionDelayed(TimeSpan.FromSeconds(0.5),function()controller:PlayObjectSound("event:/magic/earth/magic_earth_bombardment_impact")end)
    end
end

MilitiaEvent.AmWinningTerritoryCount = function(mobileObj)
    local militiaId = Militia.GetId(mobileObj)
    local controlPoints = 0
    local ownedPoints = 0
    local events = GlobalVarRead("Militia.Events")
    if ( events and militiaId ) then
        for k, v in pairs(events) do
            if ( v[5] == 2 ) then
                controlPoints = controlPoints + 1
                if ( v[2] == militiaId ) then
                    ownedPoints = ownedPoints + 1
                end
            end
        end
        if ( ownedPoints > controlPoints/2 ) then
            return true
        end
    end
    return false
end

MilitiaEvent.EligibleFlagbearer = function(mobileObj, flagObj, silent, pickup)
    if ( not mobileObj ) then
        LuaDebugCallStack("No mobileObj for Flagbearer.")
        return false
    end
    if ( not mobileObj:IsValid() ) then
        LuaDebugCallStack("mobileObj for Flagbearer not valid.")
        return false
    end
    if ( flagObj and not flagObj:IsValid() ) then
        LuaDebugCallStack("flagObj for EligibleFlagbearer not valid.")
        return false
    end
    if ( mobileObj:IsCloaked() ) then
        if ( not silent or silent ~= true ) then
            mobileObj:SystemMessage("Cannot carry a flag while cloaked.","info")
        end
        return false
    end
    local bearerMilitiaId = mobileObj:GetObjVar("Militia")
    if ( not bearerMilitiaId ) then
        if ( not silent or silent ~= true ) then
            mobileObj:SystemMessage("Must be in a Militia to take flags!","info")
        end
        return false
    end
    if ( IsDead(mobileObj) ) then
        if ( not silent or silent ~= true ) then
            mobileObj:SystemMessage("Can't be a flagbearer while dead!","info")
        end
        return false
    end
    if ( pickup and pickup == true ) then
        if ( mobileObj:DistanceFrom(flagObj) > OBJECT_INTERACTION_RANGE * 2 ) then
            if ( not silent or silent ~= true ) then
                mobileObj:SystemMessage("Flag is too far away.","info")
            end
            return false
        end
        if ( Plot.GetAtLoc(mobileObj:GetLoc()) ) then
            if ( not silent or silent ~= true ) then
                mobileObj:SystemMessage("Can't be a flagbearer on private property!","info")
            end
            return false
        end
        if ( HasMobileEffect(mobileObj, "Flagbearer") ) then
            if ( not silent or silent ~= true ) then
                mobileObj:SystemMessage("Already carrying a flag!","info")
            end
            return false
        end
        local standMilitiaId = flagObj:GetObjVar("StandMilitia")
        if ( flagObj:HasModule("ctf_flag_at_base") and bearerMilitiaId == standMilitiaId ) then
            if ( not silent or silent ~= true ) then
                mobileObj:SystemMessage("That flag is already captured by your Militia!","info")
            end
            return false
        end
        local scale = flagObj:GetScale()
        if ( not scale or scale ~= Loc(1,1,1) ) then
            return false
        end
    end
    return true
end

MilitiaEvent.MakeEventTooltip = function(eventEntryData)
    local loc = eventEntryData[1]
    local winner = eventEntryData[2]
    local active = eventEntryData[3]
    local label = eventEntryData[4]
    local type = eventEntryData[5]
    local tooltip = ""
    --add label
    if ( ServerSettings.Militia.Events[type].Name == "Capture the Flag" and ServerSettings.Militia.Militias[tonumber(label)].Name ) then
        tooltip = tooltip..ServerSettings.Militia.Militias[tonumber(label)].TextColor..ServerSettings.Militia.Militias[tonumber(label)].Name.." Flag[-]"
    else
        tooltip = tooltip.."[F2F5A9]"..label.."[-]"
    end
    --add event type
    tooltip = tooltip.."\n"..ServerSettings.Militia.Events[type].Name
    --add owner/winner/controller
    if ( winner and winner ~= 0 ) then
        tooltip = tooltip.."\nOwned by "..ServerSettings.Militia.Militias[tonumber(winner)].TextColor..ServerSettings.Militia.Militias[tonumber(winner)].Name.."[-]"
    else
        tooltip = tooltip.."\nUnclaimed"
    end
    --add status/cooldown
    local timeLeft = nil
    if ( active == 1 ) then
        tooltip = tooltip.."\nActive"
    end
    return tooltip
end

MilitiaEvent.ChangeTownshipFlag = function(flagObj, militiaId)
    if ( flagObj and flagObj:IsValid() ) then
        if ( militiaId == 1 ) then
            flagObj:PlayEffect("FireTornadoEffect",5)
            flagObj:PlayObjectSound("event:/magic/fire/magic_fire_flame_wave")
            flagObj:SetSharedObjectProperty("Variation", "Pyros")
            flagObj:SetName(ServerSettings.Militia.Militias[militiaId].TextColor.."Pyros Banner[-]")
            SetTooltipEntry(flagObj, "tooltip", "This territory is currently controlled by "..ServerSettings.Militia.Militias[militiaId].TextColor..ServerSettings.Militia.Militias[militiaId].Name.."[-].")
        elseif ( militiaId == 2 ) then
            flagObj:PlayEffect("LightningEffect")
            flagObj:PlayEffect("IceRainEffect")
            flagObj:PlayObjectSound("event:/magic/water/magic_water_ice_rain")
            flagObj:PlayObjectSound("event:/magic/air/magic_air_lightning_impact")
            flagObj:SetSharedObjectProperty("Variation", "Helm")
            flagObj:SetName(ServerSettings.Militia.Militias[militiaId].TextColor.."Helm Banner[-]")
            SetTooltipEntry(flagObj, "tooltip", "This territory is currently controlled by "..ServerSettings.Militia.Militias[militiaId].TextColor..ServerSettings.Militia.Militias[militiaId].Name.."[-].")
        elseif ( militiaId == 3 ) then
            flagObj:PlayEffect("RockDropEffect")
            flagObj:PlayEffect("DustTrailEffect")
            flagObj:PlayObjectSound("event:/magic/earth/magic_earth_bombardment_throw")
            CallFunctionDelayed(TimeSpan.FromSeconds(0.5),function()flagObj:PlayObjectSound("event:/magic/earth/magic_earth_bombardment_impact")end)
            flagObj:SetSharedObjectProperty("Variation", "Eldeir")
            flagObj:SetName(ServerSettings.Militia.Militias[militiaId].TextColor.."Eldeir Banner[-]")
            SetTooltipEntry(flagObj, "tooltip", "This territory is currently controlled by "..ServerSettings.Militia.Militias[militiaId].TextColor..ServerSettings.Militia.Militias[militiaId].Name.."[-].")
        end
    end
end

MilitiaEvent.SetTownshipFlags = function(militiaId)
    local townshipFlags = FindObjects(SearchMulti(
	    {
	        SearchObjVar("TownshipBanner", true),
        }))
    if ( townshipFlags and next(townshipFlags) ) then
        for i = 1, #townshipFlags do
            CallFunctionDelayed(TimeSpan.FromSeconds(i*2),function()
                MilitiaEvent.ChangeTownshipFlag(townshipFlags[i], militiaId)
            end)
        end
    end
end

MilitiaEvent.GetNumberFlagsOwned = function(allegianceId)
    local events = GlobalVarRead("Militia.Events")
    if ( events and next(events) ) then
        local flagsOwned = 0
        local flagsOwnedGlobally = 0
        for controller, data in pairs(events) do
            if ( data[5] and data[5] == 3 ) then
                if ( data[2] and data[2] ~= 0 ) then
                    flagsOwnedGlobally = flagsOwnedGlobally + 1
                    if ( data[2] == allegianceId ) then
                        flagsOwned = flagsOwned + 1
                    end
                end
            end
        end
        return flagsOwned, flagsOwnedGlobally
    end
    return 0, 0
end

-- MilitiaEventSpiritwoodLumber
MilitiaEvent.OwnsLocation = function(controllerTag,militiaId,playerObj)
    if(playerObj) then
        militiaId = TownshipsHelper.GetMilitaId( nil, playerObj )
    end

    local events = GlobalVarRead("Militia.Events")
    for controller,eventData in pairs(events) do
        if(eventData[7] == controllerTag) then
            local winner = eventData[2]
            if(winner and winner == militiaId) then
                return true
            else
                return false
            end
        end
    end    

    return false
end

MilitiaEvent.IsKeepVulnerable = function(keepDoorObj)
    return true
end