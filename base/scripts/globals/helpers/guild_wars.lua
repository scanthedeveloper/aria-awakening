-- Additional helper functions for guild wars

GuildHelpers.GetWarEnd = function(warRecord)
	if not(warRecord.StartDate) or not(warRecord.Duration) then return end

	return warRecord.StartDate:Add(warRecord.Duration)
end

GuildHelpers.CreateWarDeclaration = function(user,sourceGuildId,targetGuildId,duration)
	-- can't war yourself
	if(sourceGuildId == targetGuildId) then
		if(user) then
			-- player is trying to do something funky
			user:SystemMessage("War declaration failed.","info")
		end
		return
	end

	local sourceGuild = GetGuildRecord(sourceGuildId)
	local targetGuild = GetGuildRecord(targetGuildId)

	-- make sure both guilds exist
	if(not(sourceGuild) or not(targetGuild)) then
		if(user) then
			-- player is trying to do something funky
			user:SystemMessage("War declaration failed.","info")
			return
		end
	end

	if(GuildHelpers.CountOutgoingDeclarations(sourceGuildId) >= ServerSettings.GuildWars.MaximumOutgoingDeclarations ) then
		if(user) then
			-- player is trying to do something funky
			user:SystemMessage("You can not have more than "..ServerSettings.GuildWars.MaximumOutgoingDeclarations.." outgoing war declarations at one time.","info")
		end
		return
	end

	local existingWar = GlobalVarReadKey("GuildWars."..sourceGuildId,targetGuildId)
	-- make sure we don't already have a wardec with this guild
	if(existingWar) then
		user:SystemMessage("War declaration or guild war already exists. If the war has ended, you must wait 24 hours before you can declare war again.","info")
		return			
	end

	-- first write the record for the target guild, the bool just says that the record exists in the source guild table
	SetGlobalVar("GuildWars."..targetGuildId,function (record)
		record[sourceGuildId] = true

		return true
	end, function (success)
		-- next write the record with the war data into the source guild table
		if(success) then
			SetGlobalVar("GuildWars."..sourceGuildId,function (record)
				record[targetGuildId] = {
					SourceGuild = sourceGuildId,
					TargetGuild = targetGuildId,
					Duration = duration,
					ExpireDate = DateTime.UtcNow:Add(TimeSpan.FromDays(7)),
				}
				return true
			end)

			GuildHelpers.SendToAll(nil,sourceGuild,"Your guild has declared war on "..targetGuild.Name.."!")
			GuildHelpers.SendToAll(nil,targetGuild,sourceGuild.Name.." has declared war on your guild!")

			user:SendMessage("UpdateGuildInfo")
		end
	end)
end

GuildHelpers.GetWars = function (guildId)
	local warRecords = {}
	local warIndexTable = GlobalVarRead("GuildWars."..guildId)
	if(warIndexTable) then
		for targetGuildId,warRecord in pairs(warIndexTable) do
			-- the war data is in the other guild's entry
			if (warRecord == true) then
				warRecord = GlobalVarReadKey("GuildWars."..targetGuildId,guildId)
			end

			if(warRecord) then
				table.insert(warRecords,warRecord)
			end
		end
	end
	return warRecords
end

-- get the shared object property for the guild war which is a list of guild ids
GuildHelpers.GetWarsSharedProperty = function(guildId)	
	local warRecords = GuildHelpers.GetWars(guildId)
	local propStr = ""
	for targetGuildId,warRecord in pairs(warRecords) do
		if (warRecord == true) then
			warRecord = GlobalVarReadKey("GuildWars."..targetGuildId,guildId)
		end

		if(GuildHelpers.IsWarActive(warRecord)) then			
			if(guildId == warRecord.SourceGuild) then
				propStr = propStr .. warRecord.TargetGuild .. ","
			else
				propStr = propStr .. warRecord.SourceGuild .. ","
			end
		end
	end

	return StripTrailingComma(propStr)
end

GuildHelpers.IsInActiveWar = function(guildId)
	local warRecords = GuildHelpers.GetWars(guildId)
	for targetGuildId,warRecord in pairs(warRecords) do
		if (warRecord == true) then
			warRecord = GlobalVarReadKey("GuildWars."..targetGuildId,guildId)
		end

		if(GuildHelpers.IsWarActive(warRecord)) then			
			return true
		end
	end

	return false
end

GuildHelpers.ScrubWars = function()	
	local now = DateTime.UtcNow

	local warGuildRecords = GlobalVarListRecords("GuildWars")
	DebugMessage("[GuildHelpers.ScrubWars] Validating "..#warGuildRecords.." records.")
	for i,recordName in pairs(warGuildRecords) do
		local warGuildRecord = GlobalVarRead(recordName)
		for targetGuildId, warRecord in pairs(warGuildRecord) do
			if(warRecord ~= true) then
				DebugMessage("[GuildHelpers.ScrubWars] Checking "..warRecord.SourceGuild.."-"..warRecord.TargetGuild)
				-- check if declaration has expired
				if( (warRecord.ExpireDate and not(warRecord.StartDate) and now > warRecord.ExpireDate)
						-- check if war has been surrendered for more than 24 hours		
						or (warRecord.SurrenderDate and now > warRecord.SurrenderDate:Add(ServerSettings.GuildWars.WarExpiration))
						-- check if war has been ended for more than 24 hours		
						or (warRecord.StartDate and now > GuildHelpers.GetWarEnd(warRecord):Add(ServerSettings.GuildWars.WarExpiration))
						-- check if either guild disbanded
						or not(GuildHelpers.GuildExists(warRecord.SourceGuild)) or not(GuildHelpers.GuildExists(warRecord.TargetGuild)) ) then
					-- guild war record should be scrubbed
					-- DAB NOTE: Should we notify people in some of these cases
					DebugMessage("[GuildHelpers.ScrubWars] Scrubbing "..warRecord.SourceGuild.."-"..warRecord.TargetGuild)
					GuildHelpers.ScrubGuildWar(warRecord.SourceGuild,warRecord.TargetGuild,warGuildRecord)
					GuildHelpers.WarEnded(warRecord)
				end
			end
		end
	end
end

GuildHelpers.ScheduleWarEndEvents = function(masterControllerObj)
	local warGuildRecords = GlobalVarListRecords("GuildWars")
	for i,recordName in pairs(warGuildRecords) do
		local warGuildRecord = GlobalVarRead(recordName)
		for targetGuildId, warRecord in pairs(warGuildRecord) do
			if(warRecord ~= true and GuildHelpers.IsWarActive(warRecord)) then
				GuildHelpers.ScheduleWarEndEvent_Internal(masterControllerObj,warRecord)
			end
		end
	end
end

GuildHelpers.ScheduleWarEndEvent = function (masterControllerObj,sourceGuildId,targetGuildId)
	local warRecord = GlobalVarReadKey("GuildWars."..sourceGuildId,targetGuildId)
	if(warRecord) then
		GuildHelpers.ScheduleWarEndEvent_Internal(masterControllerObj,warRecord)	
	end
end

GuildHelpers.ScheduleWarEndEvent_Internal = function(masterControllerObj,warRecord)
	local eventId = warRecord.SourceGuild.."-"..warRecord.TargetGuild
	local warEnd = GuildHelpers.GetWarEnd(warRecord)
	masterControllerObj:ScheduleTimerAtUTC(warEnd,eventId,warRecord.SourceGuild,warRecord.TargetGuild)

	RegisterSingleEventHandler(EventType.Timer,eventId,function (sourceGuild,targetGuild)
		local warRecord = GlobalVarReadKey("GuildWars."..sourceGuild,targetGuild)
		if(warRecord and not(warRecord.SurrenderedBy) and DateTime.Compare(DateTime.UtcNow,GuildHelpers.GetWarEnd(warRecord)) > 0) then
			GuildHelpers.WarEnded(warRecord)
		end
	end)
end

GuildHelpers.WarEnded = function(warRecord)
	local sourceGuild = GetGuildRecord(warRecord.SourceGuild)
	local targetGuild = GetGuildRecord(warRecord.TargetGuild)
	local sourceGuildName = sourceGuild and sourceGuild.Name or "Unknown"
	local targetGuildName = targetGuild and targetGuild.Name or "Unknown"

	if(sourceGuild) then
		GuildHelpers.SendToAll(nil,sourceGuild,"Your guild war with "..targetGuildName.." has ended!")
		GuildHelpers.SendMessageToAll(sourceGuild,"UpdateGuildMemberInfo")
	end

	if(targetGuild) then
		GuildHelpers.SendToAll(nil,targetGuild,"Your guild war with "..sourceGuildName.." has ended!")
		GuildHelpers.SendMessageToAll(targetGuild,"UpdateGuildMemberInfo")
	end
end

GuildHelpers.ScrubGuildWar = function(sourceGuildId,targetGuildId,warGuildRecord,cb)
	if not(warGuildRecord) then
		warGuildRecord = GlobalVarRead("GuildWars."..sourceGuildId)
	end

	if( CountTable(warGuildRecord) == 1 ) then
		-- make sure this one record is the one we are scrubbing!
		if(warGuildRecord[targetGuildId]) then
			DelGlobalVar("GuildWars."..sourceGuildId)
		end
	else
		SetGlobalVar("GuildWars."..sourceGuildId,function (record)
			record[targetGuildId] = nil
			return true
		end)
	end

	warGuildRecord = GlobalVarRead("GuildWars."..targetGuildId)
	if( CountTable(warGuildRecord) == 1 ) then
		-- make sure this one record is the one we are scrubbing!
		if(warGuildRecord[sourceGuildId]) then
			DelGlobalVar("GuildWars."..targetGuildId,cb)
		end
	else
		SetGlobalVar("GuildWars."..targetGuildId,function (record)
			record[sourceGuildId] = nil
			return true
		end,cb)
	end
end

GuildHelpers.InGuildWarWith = function(playerA,playerB)
	local sourceGuild = nil
	local targetGuild = nil

	-- DAB NOTE: do we need to verify the guild membership or can we trust the objvar?
	local playerAGuild = playerA:GetObjVar("Guild")
	if not(playerAGuild) or not(GuildHelpers.GuildExists(playerAGuild) ) then
		--DebugMessage("[GuildHelpers.InGuildWarWith] Result false (no a guild)")
		return false
	end
	local playerBGuild = playerB:GetObjVar("Guild")
	if not(playerBGuild) or not(GuildHelpers.GuildExists(playerBGuild) ) then
		--DebugMessage("[GuildHelpers.InGuildWarWith] Result false (no b guild)")
		return false
	end

	sourceGuild = playerAGuild
	targetGuild = playerBGuild
	
	local warRecord = GlobalVarReadKey("GuildWars."..sourceGuild,targetGuild)
	if not(warRecord) then
		--DebugMessage("[GuildHelpers.InGuildWarWith] Result false (no war record)")
		return false		
	-- the war data is in the other guild's entry
	elseif (warRecord == true) then
		sourceGuild = playerBGuild
		targetGuild = playerAGuild
		warRecord = GlobalVarReadKey("GuildWars."..sourceGuild,targetGuild)
	end

	local result = GuildHelpers.IsWarActive(warRecord)
	--DebugMessage("[GuildHelpers.InGuildWarWith] Result",tostring(result))
	return result,sourceGuild,targetGuild
end

GuildHelpers.IsWarActive = function(warRecord)
	if not(warRecord) then return false end

	--LuaDebugCallStack("WHA")
	-- war never started
	if not(warRecord.StartDate) then
		return false
	end

	-- war was surrendered
	if(warRecord.SurrenderedBy) then
		return false
	end

	-- is war ended
	if(DateTime.UtcNow > GuildHelpers.GetWarEnd(warRecord)) then
		return false
	end

	return true
end

GuildHelpers.CountOutgoingDeclarations = function (guildId)
	local count = 0
	local warRecords = GuildHelpers.GetWars(guildId)
	for targetGuildId,warRecord in pairs(warRecords) do
		-- when warRecord is true then they are the target guild (incoming declaration)
		if (warRecord ~= true and warRecord.SourceGuild == guildId and not(warRecord.StartDate)) then
			count = count + 1
		end
	end

	return count
end

GuildHelpers.StartGuildWar = function(warRecord,targetGuild,user)
	local sourceGuild = GetGuildRecord(warRecord.SourceGuild)
	if not(sourceGuild) then
		user:SystemMessage("The guild that issued this war declaration no longer exists.","info")
		GuildHelpers.ScrubGuildWar(warRecord.SourceGuild,warRecord.TargetGuild)
		return
	end

	SetGlobalVar("GuildWars."..warRecord.SourceGuild,function (warGuildRecord)
			if(warGuildRecord[warRecord.TargetGuild]) then
				warGuildRecord[warRecord.TargetGuild].StartDate = DateTime.UtcNow
				warGuildRecord[warRecord.TargetGuild].ExpireDate = nil
				return true
			end			
		end,function(success)
			if(success) then
				GuildHelpers.SendToAll(nil,sourceGuild,"Your guild is now at war with "..targetGuild.Name.."!")
				GuildHelpers.SendToAll(nil,targetGuild,"Your guild is now at war with "..sourceGuild.Name.."!")
				-- this updates the guild war icons on peoples names
				GuildHelpers.SendMessageToAll(sourceGuild,"UpdateGuildMemberInfo")
				GuildHelpers.SendMessageToAll(targetGuild,"UpdateGuildMemberInfo")

				MessageAllClusterControllers("ScheduleWarEndEvent",warRecord.SourceGuild,warRecord.TargetGuild)

				DebugMessage("[WarDetails] War started!")
				user:SendMessage("UpdateGuildInfo")
			end
		end)
end

GuildHelpers.RewardWarKill = function(victim,killer)
	local inWar,sourceGuild,targetGuild = GuildHelpers.InGuildWarWith(victim,killer)
	local killerIsInSource = killer:GetObjVar("Guild") == sourceGuild
	if(inWar) then
		SetGlobalVar("GuildWars."..sourceGuild,function (warGuildRecord)
			if(warGuildRecord[targetGuild]) then
				if(killerIsInSource) then
					warGuildRecord[targetGuild].SourceKills = (warGuildRecord[targetGuild].SourceKills or 0) + 1
				else
					warGuildRecord[targetGuild].TargetKills = (warGuildRecord[targetGuild].TargetKills or 0) + 1
				end
				return true
			end			
		end)
	end
end