function AddUserActionToSlot(actionData, player )
	--DebugMessage("Add user action to slot:" .. actionData.Slot .. " ".. actionData.ID)
	if not(actionData) then
		LuaDebugCallStack("ERROR: Invalid action")
	end

	if(actionData.ID == nil or actionData.ActionType == nil or not(type(actionData.ID)=="string")) then
		DebugMessage("ERROR: Invalid id for action")
		return
	end

	player = player or this or "Care"
    local hotbarActions = player:GetObjVar("HotbarActions") or hotbarActions or {}

	--[[
	
	]]--

	if(actionData.Slot ~= nil and (type(actionData.Slot) ~= "number" or actionData.Slot <= 0 or actionData.Slot > 100)) then
		DebugMessage("ERROR: Hotbar currently only supports numbered slots between 1 and 100")
		return
	elseif(actionData.Slot == nil) then 
		actionData.Slot = 1
		while(hotbarActions[actionData.Slot] ~= nil) do
			actionData.Slot = actionData.Slot + 1
		end
	end
	
	
	RemoveExistingActionsForSlot(actionData.Slot, hotbarActions, player)

	hotbarActions[actionData.Slot] = actionData

	--PrintHotBarTable(hotbarActions)

	player:SetObjVar("HotbarActions",hotbarActions)
	-- just send the one action that has changed
	--DebugMessage("--UpdateUserAction-- (Add)")
	--DebugMessage(DumpTable({actionData}))
	player:SendClientMessage("UpdateUserAction",{actionData})
end

function RemoveExistingActionsForSlot(itemslot, table, player)

	--DebugMessage("Remove existing items for slot")
    local removed = false
    player = player or this

	for slot,slottedAction in pairs(table) do
		if(slottedAction.Slot == itemslot) then
			--DebugMessage("Removed item for slot:" .. itemslot .. " itemID: " .. slottedAction.ID)
			table[slot] = nil
			removed = true
		end
	end

	if(removed == false) then
		--DebugMessage("No hotbar items found matching slot:" .. itemslot)
	else
		player:SetObjVar("HotbarActions",table)
	end
	
end

function RemoveActionsFromSlotRange( start, stop, player ) 

    if ( player and stop > start ) then
        local hotbarActions = player:GetObjVar("HotbarActions") or hotbarActions or {}
        for i=start, stop do 
            RemoveUserActionFromSlot(i, player)
        end
	end
	
end

function RemoveUserActionFromSlot(slotIndex, player)

	--DebugMessage("Remove Action From: " .. slotIndex)
	hotbarActions = player:GetObjVar("HotbarActions") or hotbarActions or {}
	hotbarActions[slotIndex] = nil
	player:SetObjVar("HotbarActions",hotbarActions)
	player:SendClientMessage("UpdateUserAction",{{Slot=slotIndex}})

end


HelperMacros = {}
HelperMacros.MacroHotbarSlotStart = 70
HelperMacros.MacroHotbarSlotStop = 90
HelperMacros.DefaultMSDelay = 300

-- Returns the actionTable from the MacroConditions array based on a provided actionId
-- @param: actionId - MacroConditions.ID value you want
-- returns table - MacroConditions[actionId]
HelperMacros.GetMacroConditionActionDataFromActionID = function( actionId ) 

    for i=1, #MacroConditions do 
        if( MacroConditions[i].ID == actionId ) then
            return MacroConditions[i]
        end
	end
	
end

-- Given a player and a macro name it will save the actions in hotbar slots
-- 70-90 as a entry in SaveMacros ObjVar on the playerObj.
-- @param: playerObj - player the macro should be saved to
-- @param: macroName - name for this macro; must be unique
-- @return: boolean - true/false if the save succedded 
HelperMacros.SaveMacroForPlayer = function( playerObj, macroName, overwrite )

	local macroTable = playerObj:GetObjVar("SavedMacros") or {}

	local tempMacro = {}
	local hotbarSlots = playerObj:GetObjVar("HotbarActions") or {}
	local macroTableID = #macroTable+1

	-- If we are overwriting a name we need to see if one exists?
	if( overwrite ) then
		for i=1, #macroTable do 
			if( macroTable[i] and macroTable[i].MacroName == macroName ) then
				macroTableID = i
			end
		end
	end

	-- For each hotbar action in our macro range, we need to save them.
	for i=HelperMacros.MacroHotbarSlotStart, HelperMacros.MacroHotbarSlotStop do 

		if( hotbarSlots[i] ~= nil and hotbarSlots[i].ActionType ~= "MacroSlot" ) then
			tempMacro[#tempMacro+1] = hotbarSlots[i]
		end

	end

	local action = {
		ID = "Macro_"..tostring(macroTableID),
		ActionType = "Macro",
		DisplayName = macroName,
		IconText = macroName:sub(0,5),
		Enabled = true,
		Tooltip = macroName,
		ServerCommand = "playmacro " .. tostring(macroTableID),
	}

	-- Our tempMacro variable now has the full macro saved, skipping blank spots.
	macroTable[macroTableID] = { MacroName = macroName, Macro = tempMacro, MacroAction = action }
	
	
	
	playerObj:SetObjVar("SavedMacros", macroTable) 
	return true

end

-- Given a playerObj and a macroId [SavedMacros] this will remove the macroId entry from the [SavedMacros] ObjVar table
HelperMacros.DeleteMacroForPlayer = function( playerObj, macroId )

	local macroTable = playerObj:GetObjVar("SavedMacros") or {}

	if( macroTable[macroId] ~= nil ) then
		macroTable[macroId] = nil
	end

	playerObj:SetObjVar("SavedMacros", macroTable)

end

HelperMacros.LoadMacroForPlayer = function( playerObj, macroId )
	
	local macroTable = playerObj:GetObjVar("SavedMacros") or {}

	if( macroTable[macroId] ~= nil ) then
		
		-- Clear the macro slots 70-90
		RemoveActionsFromSlotRange(70, 90, playerObj)

		for i=1, #macroTable[macroId].Macro do
			AddUserActionToSlot( macroTable[macroId].Macro[i], playerObj )
		end


	end

end

-- Given a playerObj and a macroId will return the value from the ObjVar("SavedMacros")
-- for the given marcoId.
HelperMacros.GetMacroForPlayer = function( playerObj, macroId )
	
	local macroTable = playerObj:GetObjVar("SavedMacros") or {}

	if( macroTable[macroId] ~= nil ) then
		
		return macroTable[macroId]
	end

	return nil

end

-- Given a macroTable ObjVar[SavedMacros] and a macroName will determine if the
-- macroName is unique.
-- @param: macroTable - SaveMacros ObjVar value for a player
-- @param: macroName - unique name for the macro you are looking for
-- @return boolean - true/false if the macroName is unique in the macroTable
HelperMacros.CheckMacroNameUnique = function( macroTable, macroName )

	return true

end

-- Performs
HelperMacros.PerformMacroStep = function( macroData, stepNum, hotbarActions, playerObj, repeatCount )
	
	-- Validate all our data and ensure the stepNum isn't bigger than our macro step count
	if( macroData and stepNum and tonumber(stepNum) <= #macroData and HasMobileEffect(playerObj,"PlayMacro") ) then

		-- Get a reference to the current step in the macro
		local currentStep = macroData[stepNum]
		
		-- Get delay
		local delay = HelperMacros.DefaultMSDelay

		-- Set number of times repeated
		macroData.RepeatCount = macroData.RepeatCount or 0

		-- If this is a macro condition we need to do some special work.
		if( currentStep.ActionType == "MacroCondition" ) then

			local args = StringSplit( currentStep.ID, "_" )
			

			-- Sleep? If so, lets adjust our "delay" as needed.
			if( args and args[2] == "sleep" ) then
				delay = currentStep.Delay or HelperMacros.DefaultMSDelay

			-- Target? If so, we need to target something.
			elseif( args and args[3] and args[2] == "target" ) then
				local target = nil
				if( args[3] == "current" ) then
					target = playerObj:GetObjVar("CurrentTarget")
				elseif( args[3] == "self" ) then
					target = playerObj
				end

				SendClientTargetForUser( target )
				playerObj:SendClientMessage("CancelSpellCast")

			-- Use Primary Weapon Ability
			elseif( currentStep.ID == "macro_use_primary" and hotbarActions ) then
				if( hotbarActions[29] ) then
					SendServerCommandForUser( StringSplit( hotbarActions[29].ServerCommand, " " ) )
				end

			-- Use Secondary Weapon Ability
			elseif( currentStep.ID == "macro_use_secondary" and hotbarActions ) then
				if( hotbarActions[30] ) then
					--DebugMessage("30:ServerCommand = " .. hotbarActions[30].ServerCommand)
					SendServerCommandForUser( StringSplit( hotbarActions[30].ServerCommand, " " ) )
				end

			-- Handle macro repeat calls
			elseif( args and args[3] and args[2] == "repeat" ) then
				macroData.RepeatMax = macroData.RepeatMax or tonumber(args[3])

				--DebugMessage("Macro repeats " .. macroData.RepeatMax .. " times and has repeated " .. macroData.RepeatCount .. " times already.")

				if( macroData.RepeatMax > 0 and macroData.RepeatCount < macroData.RepeatMax  ) then
					stepNum = 0 -- repeat macro
				elseif( macroData.RepeatMax == 0 ) then
					stepNum = 0 -- repeat macro
				else
					-- Macro ended
					playerObj:SendMessage("UpdateMacroWindow", nil, nil, true)
					return false
				end

				macroData.RepeatCount = macroData.RepeatCount + 1

			end

		-- This is a non-macro condition we can use the base ServerCommand
		-- ie( craft, spell, ability, ect...)
		else
			--DebugMessage("ServerCommand : " .. currentStep.ServerCommand)
			SendServerCommandForUser( StringSplit( currentStep.ServerCommand, " " ) )
		end

		-- Call our delay for the next step
		--DebugMessage("Delaying next macro step by:" .. delay .. " MS; and step number: " .. tostring( stepNum+1 ))
		CallFunctionDelayed(TimeSpan.FromMilliseconds(delay), function()
			HelperMacros.PerformMacroStep( macroData, stepNum+1, hotbarActions, playerObj )
		end)

		playerObj:SendMessage("UpdateMacroWindow", stepNum, macroData.RepeatCount)

		return true
	end

	-- We cannot go any further.
	playerObj:SendMessage("UpdateMacroWindow", nil, nil, true)
	return false

end

HelperMacros.StartMacro = function( playerObj, macroId ) 
	
	-- Make sure our player is valid and has saved macros
	if not ( playerObj and playerObj:HasObjVar("SavedMacros") ) then return false end

	--DebugMessage("Start Macro has SavedMacros")

	--DebugMessage("Looking for SavedMacros macroId: " .. tostring(macroId))
	-- Get the SavedMacros form the player and ensure our macroId exists in the table
	local savedMacros = playerObj:GetObjVar("SavedMacros")
	local hotbarActions = playerObj:GetObjVar("HotbarActions")
	if not ( savedMacros[macroId] ) then return false end

	--DebugMessage("Found SavedMacros macroId: " .. tostring(macroId))

	-- Get the macro record
	local macroTable = savedMacros[macroId]

	-- If our macro has steps, lets fire these ENGINES!!
	if( macroTable.Macro and #macroTable.Macro > 0 ) then
		--DebugMessage( "We are starting the macro!" )
		return HelperMacros.PerformMacroStep( macroTable.Macro, 1, hotbarActions, playerObj, 0 )
	end

	-- We failed :/
	return false

end