function ValidateTeleport(user,targetLoc)
	--DebugMessage("Debuggery Deh Yah")
	if (user:IsInRegion("NoTeleport") or user:IsInRegion("NoMark") or IsLocInRegion(destLoc, "Arena")) then
		user:SystemMessage("[$1902]","info")
		return false
	end

	if( not(IsPassable(targetLoc)) ) then
		user:SystemMessage("[FA0C0C] You cannot teleport to that location.[-]","info")
		return false
	end
	
	return true
end

function ValidatePortalSpawnLoc (user, destLoc, destRegionAddress)
	local invalidMessage = ""

	--Is destLoc in a dungeon?
	if (IsDungeonMap()) then 
		invalidMessage = "You cannot create a portal in a dungeon"
		return invalidMessage, destLoc
	end

	--Is destLoc in a NoTeleport region?
	if (IsLocInRegion(destLoc, "NoTeleport") or IsLocInRegion(destLoc, "NoMark") or IsLocInRegion(destLoc, "Arena")) then
		invalidMessage = "Cannot create a portal here."
		return invalidMessage, destLoc
	end

	--[[
	--Is destLoc not in a passable location? Try a nearby location.
	local newDestLoc = destLoc
	if not (IsPassable(destLoc)) then
		newDestLoc = GetNearbyPassableLocFromLoc(destLoc, 1, 2)
	end
	]]--

	--Is destLoc in a housing bound?
	local controller = Plot.GetAtLoc(destLoc)
	if ( controller ~= nil ) then
		invalidMessage = "You cannot create a portal in a house."
		return invalidMessage, destLoc
	end

	--Is destLoc not passable?
	if not (IsPassable(destLoc)) then
		invalidMessage = "Cannot create a portal here."
		return invalidMessage, destLoc
	end

	--Is destLoc near other teleporters? If so, create it nearby.
	local newDestLoc = destLoc
	local nearbyTeleporters = FindObjects(SearchMulti({SearchRange(destLoc, 1), SearchModule("teleporter")}))
	if (nearbyTeleporters ~= nil) then
		for i, j in pairs(nearbyTeleporters) do
			newDestLoc = GetNearbyPassableLocFromLoc(j:GetLoc(), 1, 2, user)
		end

		controller = Plot.GetAtLoc(newDestLoc)
	    if not IsPassable(newDestLoc) or (controller and not isUserAStranger) then
	        invalidMessage = "Please wait for other Portals to close."
			return invalidMessage, newDestLoc
	    end
	end

	return invalidMessage, newDestLoc
end

function GetNearbyFollowers(user)
	return FindObjects(SearchMulti
	{
		SearchMobileInRange(10,true),
		SearchLuaFunction(function(targetObj)
			return (targetObj:GetObjVar("controller") == user
					or targetObj:GetObjVar("MySuperior") == user
					or targetObj:GetObjVar("FollowTarget") == user)
		end)
	})
end

function CanTeleportToWorldMap(user,destRegionAddress)
--Is--Under no circumstances should anyone without DarkSorcery DLC be allowed to teleport into Monolith
	--DebugMessage("Monolith Message: " .. tostring(destRegionAddress))
	if (GetRegionAddressForName("Monolith") == destRegionAddress) then
		if not(HasDarkSorceryDlc(user)) then
			user:SystemMessage("Requires Dark Sorcery DLC", "info")
			return false
		end
	end

	return true
end

function TeleportUser(teleporter,user,targetLoc,destRegionAddress,destFacing, overrideTimeCheck, ignoreAttune, force)
	if(user == nil or not(user:IsValid()) or user:HasTimer("RecentlyDied")) then
		return
	end

	local universeName = GetUniverseName(destRegionAddress)
	if(universeName and not(ignoreAttune) and not(IsImmortal(user))) then
		local homeUniverse = user:GetObjVar("HomeUniverse")
		if(homeUniverse ~= nil and homeUniverse ~= universeName and not(IsUserAttuned(user))) then
			user:SystemMessage("You have not yet been attuned for cross universe travel.","info")
			return
		end
	end

	overrideTimeCheck = overrideTimeCheck or false
	-- prevent teleport loops from region transfers
	if(skipCheck == false and TimeSinceLogin(user) < TimeSpan.FromSeconds(5)) then
		return
	end

	if ( not force and not CanFastTravel(user) ) then
		return
	end

	--Close any interaction user is making with npc
	user:CloseDynamicWindow("Responses")
	user:CloseDynamicWindow("merchant_interact")

	--Close bank when teleporting
	local bankObj = user:GetEquippedObject("Bank")
	if( bankObj ~= nil ) then
		CloseContainerRecursive(user,bankObj)			
	end

	user:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"TeleportDelay")

	if (teleporter:HasObjVar("TeleporterConsumeItems")) then
		local itemList = teleporter:GetObjVar("TeleporterConsumeItems")
		user:GetEquippedObject("Backpack"):ConsumeItemsInContainer(itemList,true)
	end

	-- if it's not passed in, try to grab it from the teleporter object
	destFacing = destFacing or teleporter:GetObjVar("DestinationFacing") or user:GetFacing()	
	
	--DebugMessage("Monolith Address: " .. tostring(GetRegionAddressForName("Monolith")))
	--DebugMessage("Destination Address: " .. tostring(destRegionAddress))

	if(destRegionAddress ~= nil and destRegionAddress ~= ServerSettings.RegionAddress) then
		
		-- If the user is "On The Run" they cannot change regions
		if( HasMobileEffect( user, "OnTheRun" ) ) then
			user:SystemMessage('You are "On The Run" and cannot perform that action.', "info")
			return
		end

		if not(CanTeleportToWorldMap(user,destRegionAddress)) then
			return
		end

		-- pets will be stabled automatically
		-- DAB TODO: Store which pets should be unstabled on the other side
        if(user:TransferRegionRequest(destRegionAddress,targetLoc)) then
			if( destFacing ~= nil ) then
				user:SetFacing(destFacing)
			end

			if not(teleporter:HasObjVar("NoTeleportEffect")) then
				user:PlayEffect("TeleportToEffect")
				user:PlayObjectSound("event:/magic/air/magic_air_teleport")	
			end

			if (teleporter:HasObjVar("CreatePortalOnExit")) then
	    		MessageRemoteClusterController(destRegionAddress,"CreateObject","spawn_portal",targetLoc)
	    	end	

	    	local bindOnTeleport = teleporter:GetObjVar("BindOnTeleport")
			if( bindOnTeleport ) then
				user:SetObjVar("SpawnPosition",{Region=destRegionAddress,Loc=targetLoc})
            end
		end
	else
		local objsToTeleport = {user}
		TableConcat(objsToTeleport,GetNearbyFollowers(user))

		local plotController = Plot.GetAtLoc(targetLoc)
		if plotController then
			-- Prevents Strangers from entering a house through a portal
			local isUserAStranger = Plot.IsStranger(plotController, user)
			if isUserAStranger then
				local isInHouse = Plot.GetHouseAt(plotController, targetLoc) ~= nil
                if isInHouse then
                    Plot.KickMobile(plotController, user)
					targetLoc = user:GetLoc()
	            	user:SystemMessage("Cannot portal into a Stranger's house.", "info")
	            	user:SystemMessage("Cannot portal into a Stranger's house.")
                end
			end
		end

		for i, follower in pairs(objsToTeleport) do
			if not(teleporter:HasObjVar("NoTeleportEffect")) then
				follower:PlayEffect("TeleportToEffect")						
				follower:PlayObjectSound("event:/magic/air/magic_air_teleport")
			end
			follower:SetWorldPosition(targetLoc)
			if( destFacing ~= nil ) then
				follower:SetFacing(destFacing)
			end
		end

		if (teleporter:HasObjVar("CreatePortalOnExit")) then
	    	CreateObj("spawn_portal",targetLoc)
	    end

	    local bindOnTeleport = teleporter:GetObjVar("BindOnTeleport")
		if( bindOnTeleport ) then
			user:SendMessage("BindToLocation",targetLoc)
		end		
	end
end

function DispellPortals(user, portalA, id, destLoc, destRegionAddress)
	if not portalA or not destLoc then DebugMessage("Dispell failed (PortalA or DestLoc are not valid)") return end
	local isLocal = not destRegionAddress or destRegionAddress == ServerSettings.RegionAddress
	--DebugMessage("DispellPortals() ",user,portalA,id,destLoc,destRegionAddress)
	if isLocal then
		local portalB = FindObject(SearchMulti(
		{
			SearchRange(destLoc, 1),
			SearchObjVar("Type", id),
		}))
		if portalA and portalB then
			Decay(portalA, 0.5)
			Decay(portalB, 0.5)
			user:SystemMessage("The portals have been dispelled.")
		end
	else
		MessageRemoteClusterController(destRegionAddress, "RemoteObjRequest", user, portalA, destLoc, "Decay", { Type = id })
	end
end

function OpenTwoWayPortal(sourceLoc,destLoc,portalDuration, summoner, protection)
	local portalId = uuid()	

	protection = protection or GetGuardProtectionForLoc(destLoc)
	local sourcePortalTemplate = (protection =="None") and "portal_red" or "portal"
	local sourceProtection = GetGuardProtectionForLoc(sourceLoc) 
	local destPortalTemplate = (sourceProtection == "None") and "portal_red" or "portal"

	PlayEffectAtLoc("TeleportFromEffect",sourceLoc)
	CreateObj(sourcePortalTemplate,sourceLoc,"source_portal_created",portalDuration,portalId, destLoc)
	RegisterSingleEventHandler(EventType.CreatedObject,"source_portal_created",
		function(success, objRef)
			if(success) then
				if (summoner ~= nil) then
					objRef:SetObjVar("Summoner", summoner)
				end
				objRef:SetObjVar("PortalName", PortalDisplayName(destLoc, false))
				objRef:SetObjVar("Destination", destLoc)
				if(protection) then
					objRef:SetObjVar("DestProtection", protection)
				end
				Decay(objRef, portalDuration)
				if(portalId ~= nil) then
					objRef:SetObjVar("Type",portalId)
				end
			end
		end)


	PlayEffectAtLoc("TeleportFromEffect",destLoc)
	CreateObj(destPortalTemplate,destLoc,"dest_portal_created",portalDuration,portalId, sourceLoc)	
	RegisterSingleEventHandler(EventType.CreatedObject,"dest_portal_created",
		function(success, objRef)
			if(success) then
				if (summoner ~= nil) then
					objRef:SetObjVar("Summoner", summoner)
				end
				objRef:SetObjVar("PortalName", PortalDisplayName(sourceLoc, false))
				objRef:SetObjVar("Destination", sourceLoc)
				if(protection) then
					objRef:SetObjVar("DestProtection", sourceProtection)
				end
				Decay(objRef, portalDuration)

				if(portalId ~= nil) then
					objRef:SetObjVar("Type",portalId)
				end
			end
		end)
end

-- DAB TODO: Validate destination location by creating a nodraw object at destination first
function OpenRemoteTwoWayPortal(sourceLoc,destLoc,destRegionAddress,portalDuration, destRegionalName, summoner, protection)
	if(not(destRegionAddress) or destRegionAddress == ServerSettings.RegionAddress) then
		OpenTwoWayPortal(sourceLoc,destLoc,portalDuration, summoner, protection)
		return
	end

	portalDuration = portalDuration or 25

	local portalId = uuid()	

	local sourcePortalTemplate = (protection == "None") and "portal_red" or "portal"
	local sourceProtection = GetGuardProtectionForLoc(sourceLoc) 
	local destPortalTemplate = (sourceProtection == "None") and "portal_red" or "portal"

	-- create local source portal
	PlayEffectAtLoc("TeleportFromEffect",sourceLoc)
	CreateObj(sourcePortalTemplate,sourceLoc,portalId, destLoc)
	RegisterSingleEventHandler(EventType.CreatedObject,portalId,
		function(success,objRef)
			if(success) then
				if (summoner ~= nil) then
					objRef:SetObjVar("Summoner", summoner)
				end
				objRef:SetObjVar("Destination",destLoc)
				objRef:SetObjVar("PortalName", PortalDisplayName(destLoc, false, destRegionalName))
				objRef:SetObjVar("RegionAddress",destRegionAddress)
				if(protection) then
					objRef:SetObjVar("DestProtection",protection)
				end
				Decay(objRef, portalDuration)
			end
		end)

	-- create remote dest portal
	local targetModules = {"decay"}
	local targetObjVars = { Destination=sourceLoc, RegionAddress=ServerSettings.RegionAddress, DecayTime = portalDuration, PortalName = PortalDisplayName(sourceLoc, false), Summoner = summoner}
	if(sourceProtection) then
		targetObjVars.DestProtection = sourceProtection
	end
	MessageRemoteClusterController(destRegionAddress,"CreateObject",destPortalTemplate,destLoc,targetModules,targetObjVars)
end

function OpenOneWayPortal(sourceLoc,destLoc,portalDuration,summoner, protection)
	local portalId = uuid()

	protection = protection or GetGuardProtectionForLoc(destLoc)
	local sourcePortalTemplate = (protection =="None") and "portal_red" or "portal"
	local sourceProtection = GetGuardProtectionForLoc(sourceLoc) 
	local destPortalTemplate = (sourceProtection == "None") and "portal_red" or "portal"

	--Source portal
	PlayEffectAtLoc("TeleportFromEffect",sourceLoc)
	CreateObj(sourcePortalTemplate,sourceLoc,"source_portal_created",portalDuration,portalId, destLoc)
	RegisterSingleEventHandler(EventType.CreatedObject,"source_portal_created",
		function(success,objRef)
			if (summoner ~= nil) then
				objRef:SetObjVar("Summoner", summoner)
			end
			objRef:SetObjVar("Destination",destLoc)
			objRef:SetObjVar("PortalName", PortalDisplayName(destLoc, false))
			if(protection) then
				objRef:SetObjVar("DestProtection",protection)
			end
			Decay(objRef, portalDuration)
		end)

	--Destination portal
	--Players cannot return though destination portal
	PlayEffectAtLoc("TeleportFromEffect",sourceLoc)
	CreateObj(destPortalTemplate,destLoc,"dest_portal_created",portalDuration,portalId)
	RegisterSingleEventHandler(EventType.CreatedObject,"dest_portal_created",
		function(success,objRef)
			if (summoner ~= nil) then
				objRef:SetObjVar("Summoner", summoner)
			end
			objRef:SetObjVar("PortalName", PortalDisplayName(sourceLoc, true))
			Decay(objRef, portalDuration)
		end)
end

function OpenRemoteOneWayPortal(sourceLoc,destLoc,destRegionAddress,portalDuration, destRegionalName, summoner, protection)
	if(not(destRegionAddress) or destRegionAddress == ServerSettings.RegionAddress) then
		OpenOneWayPortal(sourceLoc,destLoc,portalDuration, summoner)
		return
	end

	local sourcePortalTemplate = (protection =="None") and "portal_red" or "portal"
	local sourceProtection = GetGuardProtectionForLoc(sourceLoc) 
	local destPortalTemplate = (sourceProtection == "None") and "portal_red" or "portal"

	PlayEffectAtLoc("TeleportFromEffect",sourceLoc)
	CreateObj(sourcePortalTemplate,sourceLoc,"transfer_portal_created")	
	RegisterSingleEventHandler(EventType.CreatedObject,"transfer_portal_created",
		function (success,objRef)
			if(success) then
				if (summoner ~= nil) then
					objRef:SetObjVar("Summoner", summoner)
				end
				objRef:SetObjVar("Destination",destLoc)
				objRef:SetObjVar("PortalName", PortalDisplayName(destLoc, false, destRegionalName))
				objRef:SetObjVar("RegionAddress",destRegionAddress)
				if(protection) then
					objRef:SetObjVar("DestProtection",protection)
				end
				Decay(objRef, portalDuration)
			end
		end)
	-- create remote dest portal
	local targetModules = {"decay"}
	local targetObjVars = {RegionAddress = ServerSettings.RegionAddress, DecayTime = portalDuration, PortalName = PortalDisplayName(sourceLoc, true), Summoner = summoner}
	MessageRemoteClusterController(destRegionAddress,"CreateObject",destPortalTemplate,destLoc,targetModules,targetObjVars)
end

function IsOneWay(destination)
	local destSubregions = GetRegionsAtLoc(destination)
	for i=1, #ServerSettings.Teleport.NoEntryUserPortal do
		for j, dest in pairs(destSubregions) do
			if (ServerSettings.Teleport.NoEntryUserPortal[i] == dest) then
				return true
			end
		end
	end
	return false
end

function PortalDisplayName(destLoc, isOneWay, regionalName)
	local name = "Portal "
	if (destLoc ~= nil) then

		if not (isOneWay) then
			name = name.."to "
		else
			name = name.."from "
		end

		if (regionalName ~= nil) then
			name = name..regionalName.." "
		else
			name = name..GetRegionalName(destLoc).." "
		end
	end
	return name
end

function GetStaticPortalSpawn(staticDest)
	if ( ServerSettings.Teleport.Destination[staticDest] ) then
		local clusterRegions = GetClusterRegions()
		local destInfo = ServerSettings.Teleport.Destination[staticDest]
		for i, region in pairs(clusterRegions) do
			--DebugMessage(region.SubregionName.." "..destInfo.Subregion)
			if (region.SubregionName == destInfo.Subregion) then
				return i, destInfo.Destination
				--destRegionAddress = i
				--destination = destInfo.Destination
			end
		end	
	end
end

-- is a user attuned for cross universe travel
function IsUserAttuned(playerObj)
	return playerObj:HasObjVar("IsAttuned")
end

-- this is static so it only needs to be called once
hasMultipleUniverses = nil

-- returns true if multiple universes are running for the same map
function HasMultipleUniverses()
	if(hasMultipleUniverses ~= nil) then
		return hasMultipleUniverses
	else
		hasMultipleUniverses = false
		local universeName, worldName = GetUniverseName()
		if (universeName) then			
			hasMultipleUniverses = false
			local clusterRegions = GetClusterRegions()
			for regionName, regionInfo in pairs(clusterRegions) do
				local myUniverseName, otherWorldName = GetUniverseName(regionName)
				if(myUniverseName ~= nil and myUniverseName ~= universeName and otherWorldName == worldName) then
					hasMultipleUniverses = true
					break				
				end
			end	
		end

		return hasMultipleUniverses
	end
end

function IsRune(runeObj)
	return (runeObj and runeObj:IsValid() and runeObj:GetObjVar("ResourceType") == "Rune")		
end

function IsBlankRune(runeObj)
	if not(IsRune(runeObj))
		then return false
	end

	return not(GetRuneDestination(runeObj))
end

function GetRuneDestination(runeObj)
	local destination = nil
	local destRegionAddress = nil 

	-- handle recall runes for teleport towers
	-- DAB TODO: Static destination runes are broken but we don't use them anymore
	local staticDest = runeObj:GetObjVar("StaticDestination")
	if (staticDest ~= nil) then		
		destRegionAddress, destination = GetStaticPortalSpawn(staticDest)
	else
		destination = runeObj:GetObjVar("Destination")
		destRegionAddress = runeObj:GetObjVar("RegionAddress")
	end

	return destRegionAddress, destination
end

function GetRunebookCharges(runebookObj)
	return (runebookObj:GetObjVar("RuneCount") or 0), (runebookObj:GetObjVar("MaxRuneCount") or ServerSettings.Teleport.DefaultRunebookMaxCharges)
end

function AddRuneLocationToRunebook(runebookObj,runeObj,destination,destRegionAddress,destFacing,name)
	name = name or runeObj:GetName()
	destination = destination or runeObj:GetObjVar("Destination")
	destRegionAddress = destRegionAddress or runeObj:GetObjVar("RegionAddress")
	destFacing = destFacing or runeObj:GetObjVar("DestinationFacing")	

	local runeInfo = runebookObj:GetObjVar("RuneInfo") or {}
	if( #runeInfo >= ServerSettings.Teleport.RunebookMaxLocations ) then
		user:SystemMessage("That runebook is full.", "info")
		return
	end

	-- DAB TODO: This is expensive we might want to autofix and set this on the rune as an objvar
	local appearanceTemplate = GetTemplateForRune(runeObj)

	local runeEntry = {Name=name, Destination=destination, RegionAddress=destRegionAddress, Facing=destFacing, RegionalName=GetTooltipEntry(runeObj,"regional_name"), Universe=GetTooltipEntry(runeObj,"universe_name"), Appearance = appearanceTemplate }
	table.insert(runeInfo,runeEntry)

	runebookObj:SetObjVar("RuneInfo",runeInfo)
end

function MarkRune(runeObj,destLoc,destRegionAddress,destFacing,appearance,regionalName,universeName)
	regionalName = regionalName or ("Portal to "..GetRegionalName(destLoc))
	appearance = appearance or GetRuneTemplateForSubregion()

	if( not(universeName) and HasMultipleUniverses() ) then
		universeName = GetUniverseDisplayName().." Universe"
	end
	
	runeObj:SetAppearanceFromTemplate(appearance)
	runeObj:SetObjVar("RegionAddress",destRegionAddress)
	runeObj:SetObjVar("Destination",destLoc)
	runeObj:SetObjVar("DestinationFacing",destFacing)
	runeObj:DelObjVar("BlankRune")

	SetTooltipEntry(runeObj,"regional_name",regionalName,1000)
	if(universeName) then
		SetTooltipEntry(runeObj,"universe_name",universeName,999)
	end
end

SubregionRunes = {
	SouthernHills = "rune_valus",
	SouthernRim = "rune_pyros",
	UpperPlains = "rune_eldeirvillage",
	EasternFrontier = "rune_helm",
	BarrenLands = "rune_oasis",
	BlackForest = "rune_blackforest"
}

function GetTemplateForRune(runeObj)
	for subregionName,subregionTemplate in pairs(SubregionRunes) do
		local appearance = GetTemplateIconId(subregionTemplate)
		if(appearance == runeObj:GetIconId()) then
			return subregionTemplate
		end
	end

	return "rune_blackforest"
end

function GetRuneTemplateForSubregion(subregionName)
	subregionName = subregionName or ServerSettings.SubregionName
	if(SubregionRunes[subregionName]) then
		return SubregionRunes[subregionName]
	else
		return "rune_blackforest"
	end
end

function ChangeRuneAppearance(target, subregionName)
	--DebugMessage(ServerSettings.SubregionName)
	local template = GetRuneTemplateForSubregion(subregionName)
	if(template) then
		target:SetAppearanceFromTemplate(template)
	end
end