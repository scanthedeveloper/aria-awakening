
GlobalStorage = {}

--- Store an item in global storage
-- @param key
-- @param obj
-- @param cb | function(success)
GlobalStorage.Store = function(key, obj, cb)
    if not( cb ) then cb = function(success) end end
    if ( key == nil or key == "" ) then
        LuaDebugCallStack("[GlobalStorage.Store] key not provided.")
        return cb(false)
    end
    if ( obj == nil or not obj:IsValid() ) then
        LuaDebugCallStack("[GlobalStorage.Store] invalid object provided:"..tostring(obj))
        return cb(false)
    end
    local id = uuid()
    RegisterSingleEventHandler(EventType.SendItemToStorageResult,id,cb)
    SendItemToStorage(id,key,nil,obj)
end

--- Retrieve an item from global storage
-- @param key
-- @param container | If nil will place the object in the world
-- @param loc
-- @param cb | function(object)
GlobalStorage.Retrieve = function(key, container, loc, cb)
    if not( cb ) then cb = function(object) end end
    if ( key == nil or key == "" ) then
        LuaDebugCallStack("[GlobalStorage.Retrieve] key not provided.")
        return cb(nil)
    end
    if ( loc == nil ) then
        LuaDebugCallStack("[GlobalStorage.Retrieve] loc not provided.")
        return cb(nil)
    end

    local id = uuid()
    RegisterSingleEventHandler(EventType.RequestItemFromStorageResult, id, function(objHeader, objRef)
        cb(objRef)
    end)
    RequestItemFromStorage(id,key,container,loc)
end