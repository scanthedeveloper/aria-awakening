function GetVitalityDisplayString(vitality)
    local vitPct = vitality / ServerSettings.Stats.BaseVitality
    --DebugMessage("PCT: "..tostring(vitPct))
    for i,vitDisplayInfo in pairs(ServerSettings.Vitality.DisplayStrings) do
        if(vitPct >= vitDisplayInfo[1]) then
            return vitDisplayInfo[2]
        end
    end

    return "Rested"
end

function VitalityCheck(player)
    if not( HasMobileEffect(player, "Vitality") ) then
        StartMobileEffect(player, "Vitality", nil, nil)
    end
end

function VitalityNumberToString(amount)
    if ( amount <= 2 ) then
        return "a little "
    end
    if ( amount <= 5 ) then
        return "some "
    end
    if ( amount <= 10 ) then
        return "a lot of "
    end
end

function SafeAdjustCurVitality( amount, mobile )
    local curVitality = GetCurVitality(mobile)
    if ( curVitality < 0 ) then curVitality = 0 end
    if ( amount and amount ~= 0 and not IsInitiate(mobile) and (curVitality + amount >= 0) and GetMaxVitality(mobile) >= (curVitality + amount) ) then
        AdjustCurVitality(mobile, amount)
        if( IsPlayerCharacter(mobile) ) then
            local msgStr = "You have "
            if( amount > 0 ) then
                msgStr = msgStr .. "gained "
            else
                msgStr = msgStr .. "lost "
            end
            msgStr = msgStr .. VitalityNumberToString(math.abs(amount)) .. "vitality."
            
            mobile:SystemMessage(msgStr, "info")

            local combatStr = ""
            if( amount > 0 ) then
                combatStr = "+"
            else
                combatStr = "-"
            end
            combatStr = combatStr .. math.abs(amount) .. " vitality"
            mobile:NpcSpeech(combatStr,"combat")
            mobile:SendMessage("VitalityAdjusted", (curVitality + amount))
        end
        
    end
end