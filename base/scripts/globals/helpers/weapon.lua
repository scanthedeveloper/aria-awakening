
Weapon = {}

Weapon.GetPrimary = function(mobileObj)
	if ( mobileObj ~= nil and mobileObj:GetEquippedObject("RightHand")~=nil) then
		return mobileObj:GetEquippedObject("RightHand")
	end
	return nil
end

Weapon.GetSecondary = function(mobileObj)
	if ( mobileObj ~= nil and mobileObj:GetEquippedObject("LeftHand")~=nil) then
		return mobileObj:GetEquippedObject("LeftHand")
	end
	return nil
end

Weapon.GetType = function(weaponObj)
	if ( weaponObj ~= nil ) then		
		local weaponType = weaponObj:GetObjVar("WeaponType") or "BareHand"
		if not( EquipmentStats.BaseWeaponStats[weaponType] ) then
			LuaDebugCallStack("Invalid weapon type: "..tostring(weaponType)..", Template: "..weaponObj:GetCreationTemplateId())
		else
			return weaponType
		end
	end
	return "BareHand"
end

Weapon.UseBestWeaponSkill = function(weaponObj)
	
	if ( weaponObj ~= nil and weaponObj:HasObjVar("UseBestWeaponSkill") ) then		
		return true
	end

	return false
end

--- Gets the tooltype of the weaponObj
Weapon.GetToolType = function(weaponObj)
	if ( weaponObj ~= nil ) then		
		local toolType = weaponObj:GetObjVar("ToolType") or "BareHand"
			return toolType
	end
	return "BareHand"
end

-- Given a weapon object will return true if it's attuned
Weapon.IsAttuned = function( weaponObj )
	return ( 
		weaponObj 
		and 
		(
			Weapon.GetType(weaponObj) == "MagicStaff" 
			or 
			Weapon.GetType(weaponObj) == "MagicWand"
			--SCAN ADDED
			or 
			Weapon.GetType(weaponObj) == "LightningStaff"
			or 
			Weapon.GetType(weaponObj) == "FireStaff"
			or 
			Weapon.GetType(weaponObj) == "HealStaff"
			or 
			Weapon.GetType(weaponObj) == "LightningWand"
			or 
			Weapon.GetType(weaponObj) == "FireWand"
			or 
			Weapon.GetType(weaponObj) == "HealWand"
		) 
	)
end

--- Causes damage to the object passed
Weapon.DamageDurability = function(weaponObj)
	if( Success(ServerSettings.Durability.Chance.OnToolUse) ) then 
		AdjustDurability(weaponObj, -1)
	end
end

Weapon.GetClass = function(weaponType)
	weaponType = weaponType or "BareHand"
	local ES = EquipmentStats
	if ( ES.BaseWeaponStats[weaponType] ) then
		return ES.BaseWeaponStats[weaponType].WeaponClass
	end
	return "Fist"
end

Weapon.GetSpeed = function(weaponType)
	weaponType = weaponType or "BareHand"
	local ES = EquipmentStats
	if ( ES.BaseWeaponStats[weaponType] ) then
		return ES.BaseWeaponStats[weaponType].Speed
	end
	return 5
end

Weapon.GetPower = function(weaponObj, mobileObj)
	local power = 0	
	if( weaponObj ~= nil and weaponObj:HasObjVar("Attunement") ) then
		power = math.floor(GetSkillLevel(mobileObj,"MagicalAttunementSkill") / weaponObj:GetObjVar("Attunement"))
	end
	
	return power
end

Weapon.GetSkill = function(weaponType)
	local ES = EquipmentStats
	if ( weaponType and ES.BaseWeaponStats[weaponType] 
		and ES.BaseWeaponStats[weaponType].WeaponClass 
		and ES.BaseWeaponClass[ES.BaseWeaponStats[weaponType].WeaponClass]
		and ES.BaseWeaponClass[ES.BaseWeaponStats[weaponType].WeaponClass].WeaponSkill ) then
		return ES.BaseWeaponClass[ES.BaseWeaponStats[weaponType].WeaponClass].WeaponSkill
	end
	return "BrawlingSkill"
end

Weapon.GetDamageType = function(weaponType)
	local ES = EquipmentStats
	if ( weaponType and ES.BaseWeaponStats[weaponType] 
		and ES.BaseWeaponStats[weaponType].WeaponClass 
		and ES.BaseWeaponClass[ES.BaseWeaponStats[weaponType].WeaponClass]
		and ES.BaseWeaponClass[ES.BaseWeaponStats[weaponType].WeaponClass].WeaponDamageType ) then
		return ES.BaseWeaponClass[ES.BaseWeaponStats[weaponType].WeaponClass].WeaponDamageType
	end
	return "Bashing"
end

Weapon.GetRange = function(weaponType)
	local ES = EquipmentStats
	if ( weaponType and ES.BaseWeaponStats[weaponType] 
		and ES.BaseWeaponStats[weaponType].WeaponClass 
		and ES.BaseWeaponClass[ES.BaseWeaponStats[weaponType].WeaponClass]
		and ES.BaseWeaponClass[ES.BaseWeaponStats[weaponType].WeaponClass].Range ) then
		return ES.BaseWeaponClass[ES.BaseWeaponStats[weaponType].WeaponClass].Range
	end
	return ServerSettings.Stats.DefaultWeaponRange
end

Weapon.IsRanged = function(weaponType)
	return ( weaponType and EquipmentStats.BaseWeaponStats[weaponType] and (
		EquipmentStats.BaseWeaponStats[weaponType].WeaponClass == "Bow"
		--or EquipmentStats.BaseWeaponStats[weaponType].WeaponClass == "Bow" -- as an example to check for a secondary Class
	))
end

Weapon.IsTwoHanded = function(weaponType)
	return ( weaponType and EquipmentStats.BaseWeaponStats[weaponType].TwoHandedWeapon )
end

Weapon.GetAttackBonus = function(weaponObj)
	if ( weaponObj == nil ) then return 0 end
	return weaponObj:GetObjVar("AttackBonus") or 0
end

Weapon.GetAccuracyBonus = function(weaponObj)
	if ( weaponObj == nil ) then return 0 end
	return weaponObj:GetObjVar("Accuracy") or 0
end

Weapon.GetEnchantsForEffect = function( weaponObj, effect, checkForEffect )
	if(weaponObj == nil)then return nil end
	if(effect == nil)then return nil end

	local procs = {}

	local enchants = Weapon.GetEnchants( weaponObj )
	if(enchants == nil)then return nil end

	for i=1, #enchants do
		-- Is this enchant a hitproc? If so add it to our list.
		if( EssenceData[enchants[i].essenceName] ~= nil and EssenceData[enchants[i].essenceName].Effect == effect ) then
			if( checkForEffect ) then return true end
			table.insert(procs, enchants[i])
		end
	end

	-- Did we find any procs, if so return them
	if( #procs > 0 ) then
		return procs
	else
		return nil
	end
end

Weapon.GetEnchants = function( weaponObj )
	if( weaponObj == nil ) then return {} end
	return weaponObj:GetObjVar("Enchants") or {}
end