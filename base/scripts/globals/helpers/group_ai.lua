-- Helper to assist with GroupAI brains
GroupAI = {};
GroupAI.Radius = 15;

-- GroupAI Message Tags
GroupAI.MessageTags = {
    UpdateTarget = 1,
}

-- Finds nearby mobiles that are part of the GroupAI
-- @param _searcher : the gameObject that is requesting the search
GroupAI.GetNearbyGroupAIMembers = function( _searcher )
    return FindObjects(SearchMulti({SearchRange(_searcher:GetLoc(),GroupAI.Radius),SearchHasObjVar("GroupAI")}))
end

-- Send a group message to all mobiles within the GroupAI.Radius
-- @param _sender : gameObject sending the message
-- @param _message : message being sent
-- @param _args : arguments to pass with the message
GroupAI.SendGroupMessage = function( _sender, _message, _args )
    local nearbyGroupAI = GroupAI.GetNearbyGroupAIMembers(_sender);
    
    --DebugMessage("nearbyGroupAI : ")
    --DebugTable( nearbyGroupAI )

    for i,mobile in pairs( nearbyGroupAI ) do 
        -- Make sure they are on the same MobileTeamType
        if( _sender:GetObjVar("MobileTeamType") == mobile:GetObjVar("MobileTeamType") ) then
            mobile:SendMessage("GroupAIMessage", _sender, _message, _args )
        end
    end
end

-- Determines if an ability is off cooldown and ready to use
-- @params: _cooldowns : table of cooldowns
-- @params: _ability : the ability (_cooldowns.KEY) that should be checked
-- @params: _unixTS : the current timestamp in UTC
-- @params: _interval : how long needs to have passed before the ability is off cooldown 
GroupAI.CooldownCheck = function( _cooldowns, _ability, _unixTS, _interval )

    for key, lastUsed in pairs(_cooldowns) do
        if( 
            key == _ability -- Is this the right ability?
        ) then
            if( (lastUsed + _interval) <= _unixTS ) then
                return true -- ability found and is off cooldown
            else
                return false -- ability found but not off cooldown
            end
            
        end
    end

    -- We didn't find the ability or it isn't on cooldown
    return false

end