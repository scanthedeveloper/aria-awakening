ProfessionsHelpers = {}

ProfessionsHelpers.HasSkillRequirement = function(playerObj,profession,taskIndex,skillModifier)
    -- If this is isn't a player we can just return true -bphelps
    if not( IsPlayerCharacter(playerObj) )then return true end
    
    skillModifier = skillModifier or 0
    
    local skillAmount = 1
    if(taskIndex) then    
        skillAmount = ProfessionSkillTiers[taskIndex]
    end

    local profInfo = Professions[profession]
    for i, skillReq in pairs(profInfo.Skills) do        
        if(skillReq == "Weapon") then
            local found = false
            for i,skill in pairs(WeaponSkills) do
                local skillLevel = GetSkillLevel(playerObj,skill) + skillModifier
                if(skillLevel >= skillAmount) then
                    found = true
                end
            end
            if not(found) then
                return false
            end
        elseif(skillReq == "Spell") then
            local found = false
            for i,skill in pairs(SpellSkills) do
                local skillLevel = GetSkillLevel(playerObj,skill) + skillModifier
                if(skillLevel >= skillAmount) then
                    found = true
                end
            end
            if not(found) then
                return false
            end
        else
            local skill = skillReq .. "Skill"
            local skillLevel = GetSkillLevel(playerObj,skill) + skillModifier
            if(skillLevel < skillAmount) then
                return false
            end
        end
    end

    return true
end

-- does the player have atleast enough skill for the first task
-- or have they completed atleast one task
ProfessionsHelpers.IsProfessionUnlocked = function(playerObj,profession)
    local profInfo = Professions[profession]

    -- is intro quest done?
    if profInfo.IntroQuest and not(ProfessionsHelpers.IsTaskCompleted(playerObj,profession,0)) then
        return false
    end

    -- do they have apprentice in the required profession?
    if profInfo.RequiredProfession and not(ProfessionsHelpers.IsTaskCompleted(playerObj,profInfo.RequiredProfession,1)) then
        return false
    end
    
    -- the profession only requires 1 skill point (playerObj lets you clear the profession from the window if you forget all the skill points)
    return true
end

-- returns false if there is no quest for this task yet
ProfessionsHelpers.IsTaskAvailable = function(profession,taskIndex)
    local profInfo = Professions[profession]

    local questId = ""
    if(taskIndex == 0) then
        questId = profInfo.IntroQuest
    else
        questId = profInfo.Quests[taskIndex]
    end
    
    return (questId ~= nil)
end

ProfessionsHelpers.IsTaskLocked = function(playerObj,profession,taskIndex)
    local profInfo = Professions[profession]

    local questId = ""
    if(taskIndex == 0) then
        questId = profInfo.IntroQuest
    else
        questId = profInfo.Quests[taskIndex]
    end
    -- quest does not exist so locked
    if not(questId) then return true end

    -- testing
    --return (profession == "MageWarrior" and taskIndex > 2) or (profession == "Manifester" and taskIndex > 1) or (profession ~= "MageWarrior" and profession ~= "Manifester")
    if not(ProfessionsHelpers.IsPreviousTaskComplete(playerObj,profession,taskIndex)) then
        return true
    end

    if(profInfo.RequiredProfession and not(ProfessionsHelpers.IsTaskCompleted(playerObj,profInfo.RequiredProfession,taskIndex))) then
        return true
    end

    return not(ProfessionsHelpers.HasSkillRequirement(playerObj,profession,taskIndex))
end

ProfessionsHelpers.IsTaskCompleted = function(playerObj,profession,taskIndex,checkSkill)
    local profInfo = Professions[profession]
    
    -- Check if the player has the skill to have the task completed
    if(checkSkill) then
        if( not ProfessionsHelpers.HasSkillRequirement( playerObj,profession,taskIndex ) ) then return false end
    end

    if(profInfo) then
        local questId = ""
        if(taskIndex == 0) then
            questId = profInfo.IntroQuest
        else
            questId = profInfo.Quests[taskIndex]
        end

        if not(questId) then return false end

        return Quests.IsComplete(playerObj,questId)
    end
    
    return false
end

ProfessionsHelpers.GetTaskRewards = function(playerObj,profession,taskIndex)
    local profInfo = Professions[profession]

    -- profession system doesnt support choices
    local rewardList = {}
    local rewards = Quests.GetRewards(playerObj,profInfo.Quests[taskIndex])    
    if(rewards) then
        for i,reward in pairs(rewards[1]) do
            local rewardType = reward[1]
            if(rewardType == "Item") then
                local templateId = reward[2]
                local stackCount = reward[3] or 1
                local objectName = ""
                if(stackCount > 1) then
                    objectName = tostring(stackCount) .. " " .. (GetTemplateObjVar(templateId,"PluralName") or GetTemplateObjectName(templateId))
                else
                    objectName = GetTemplateObjectName(templateId)
                end
                table.insert(rewardList,{Icon=tostring(GetTemplateIconId(templateId)),Desc=objectName,IconType="Object",IconSize=32,IconOffset=-6})
            elseif(rewardType == "Custom") then
                table.insert(rewardList,{Icon="unlock",Desc=reward[3],IconType="Simple",IconSize=20,IconOffset=0})
            end
        end
    end

    return rewardList
end

ProfessionsHelpers.GetTaskName = function(profession,taskIndex)
    local profInfo = Professions[profession]

    local questName = profInfo.Quests[taskIndex]
    return (questName and AllQuests[questName] and AllQuests[questName][1].Name) or ""
end

ProfessionsHelpers.IsTaskActive = function(playerObj,profession,taskIndex)
    local profInfo = Professions[profession]
    local questId = ""
    if(taskIndex == 0) then
        questId = profInfo.IntroQuest
    else
        questId = profInfo.Quests[taskIndex]
    end
    -- testing
    return Quests.IsActive(playerObj,questId)
end

ProfessionsHelpers.GetProfessionQuests = function(professionName)
    local allTiersOfProfession = {}
    local profession = Professions[professionName]
    if profession then
        table.insert(allTiersOfProfession, profession.IntroQuest)
        for i=1, #profession.Quests do
            table.insert(allTiersOfProfession, profession.Quests[i])
        end
    else
        table.insert(allTiersOfProfession, professionName)
    end
    return allTiersOfProfession
end

ProfessionsHelpers.GetProfessionTierTitle = function(index)
    return TaskIndexTitles[tonumber(index) - 1] or "Intro"
end

ProfessionsHelpers.GetProfessionTierIndex = function(professionName, questName)
    local profession = Professions[professionName]
    local index
    if profession then
        for i=1, #profession.Quests do
            if profession.Quests[i] == questName then
                index = i
            end
        end
    end
    return index or -1
end

ProfessionsHelpers.CanResumeLaterStep = function (playerObj, profession)
    local profInfo = Professions[profession]
    if profInfo.CanResumeLaterStep and profInfo.CanResumeLaterStep == true then return true else return false end
end

ProfessionsHelpers.IsTaskCancelled = function(playerObj,profession,taskIndex)
    local profInfo = Professions[profession]
    local questId = ""
    if(taskIndex == 0) then
        questId = profInfo.IntroQuest
    else
        questId = profInfo.Quests[taskIndex]
    end
    return Quests.HasFailedAnyStep(playerObj, questId)
end

ProfessionsHelpers.IsPreviousTaskComplete = function(playerObj,profession,taskIndex)
    if(taskIndex == 1) then
        return true
    end

    -- testing
    return ProfessionsHelpers.IsTaskCompleted(playerObj,profession,taskIndex-1)
end

ProfessionsHelpers.GetTaskCompleteDisplay = function( profName, taskIndex, profElement, xOffset )

    --DebugMessage( "Profname: " .. profName .. " | Index: " .. taskIndex )
    local romanNumerals = ""
    for i=1, (taskIndex -1) do romanNumerals = romanNumerals .. "I" end

    local tooltipCrafting = 
    {
        I = "Reduces crafting time of trivial recipes to 1.5 seconds.",
        II = "Reduces crafting time of trivial recipes to 1 seconds.",
        III = "Reduces crafting time of trivial recipes to 0.5 seconds.",
    }

    if( profName == "Carpenter" and taskIndex > 1 ) then
        profElement:AddLabel(xOffset+80,180,"[d8a54d]Woodworking "..romanNumerals.."[-]",150,60,22,"center",false,false,"Kingthings_Dynamic")
        profElement:AddImage(xOffset+55, 125, "Carpenter_"..romanNumerals.."",50,50)
        profElement:AddButton(xOffset+35, 125, "tooltip", "", 90, 75, tooltipCrafting[romanNumerals], "",false,"Invisible")
    elseif( profName == "Blacksmith" and taskIndex > 1 ) then
        profElement:AddLabel(xOffset+80,180,"[d8a54d]Manufacturing "..romanNumerals.."[-]",150,60,22,"center",false,false,"Kingthings_Dynamic")
        profElement:AddImage(xOffset+55, 125, "Smelting_"..romanNumerals.."",50,50)
        profElement:AddButton(xOffset+35, 125, "tooltip", "", 90, 75, tooltipCrafting[romanNumerals], "",false,"Invisible")
    elseif( profName == "Tailor" and taskIndex > 1 ) then
        profElement:AddLabel(xOffset+80,180,"[d8a54d]Fabricator "..romanNumerals.."[-]",150,60,22,"center",false,false,"Kingthings_Dynamic")
        profElement:AddImage(xOffset+55, 125, "Fabricator_"..romanNumerals.."",50,50)
        profElement:AddButton(xOffset+35, 125, "tooltip", "", 90, 75, tooltipCrafting[romanNumerals], "",false,"Invisible")
    
    elseif( profName == "Alchemist" and taskIndex > 1 ) then
        local tooltip = 
        {
            I = "Increases potion effects by 10%.",
            II = "Increases potion effects by 30%.",
            III = "Increases potion effects by 50%.",
        }
        profElement:AddLabel(xOffset+80,180,"[d8a54d]Mixing "..romanNumerals.."[-]",150,60,22,"center",false,false,"Kingthings_Dynamic")
        profElement:AddImage(xOffset+54, 124, "alchemy-passive",50,50)
        profElement:AddButton(xOffset+35, 125, "tooltip", "", 90, 75, tooltip[romanNumerals], "",false,"Invisible")
    
    elseif( profName == "Chef" and taskIndex > 1 ) then
        local tooltip = 
        {
            I = "Increases meal effects by 50%.",
            II = "Increases meal effects by 75%.",
            III = "Increases meal effects by 100%.",
        }
        profElement:AddLabel(xOffset+80,180,"[d8a54d]Fusion "..romanNumerals.."[-]",150,60,22,"center",false,false,"Kingthings_Dynamic")
        profElement:AddImage(xOffset+54, 124, "cooking-passive",50,50)
        profElement:AddButton(xOffset+35, 125, "tooltip", "", 90, 75, tooltip[romanNumerals], "",false,"Invisible")

    elseif( profName == "Lumberjack" and taskIndex > 1 ) then
        local tooltip = 
        {
            I = "Increases slashing and lancing damage by 5%.",
            II = "Increases slashing and lancing damage by 10%.",
            III = "Increases slashing and lancing damage by 15%.",
        }
        profElement:AddLabel(xOffset+80,180,"[d8a54d]Feller "..romanNumerals.."[-]",150,60,22,"center",false,false,"Kingthings_Dynamic")
        profElement:AddImage(xOffset+54, 124, "lumberjack-passive",50,50)
        profElement:AddButton(xOffset+35, 125, "tooltip", "", 90, 75, tooltip[romanNumerals], "",false,"Invisible")
    
    elseif( profName == "Miner" and taskIndex > 1 ) then
        local tooltip = 
        {
            I = "Increases bashing and piercing damage by 5%.",
            II = "Increases bashing and piercing damage by 10%.",
            III = "Increases bashing and piercing damage by 15%.",
        }
        profElement:AddLabel(xOffset+80,180,"[d8a54d]Brute "..romanNumerals.."[-]",150,60,22,"center",false,false,"Kingthings_Dynamic")
        profElement:AddImage(xOffset+54, 124, "mining-passive",50,50)
        profElement:AddButton(xOffset+35, 125, "tooltip", "", 90, 75, tooltip[romanNumerals], "",false,"Invisible")

    elseif( profName == "Fisher" and taskIndex > 1 ) then
        local tooltip = 
        {
            I = "Increases archery damage by 5%.",
            II = "Increases archery damage by 10%.",
            III = "Increases archery damage by 15%.",
        }
        profElement:AddLabel(xOffset+80,180,"[d8a54d]Angler "..romanNumerals.."[-]",150,60,22,"center",false,false,"Kingthings_Dynamic")
        profElement:AddImage(xOffset+54, 124, "fishing-passive",50,50)
        profElement:AddButton(xOffset+35, 125, "tooltip", "", 90, 75, tooltip[romanNumerals], "",false,"Invisible")
    
    elseif( profName == "Scribe" and taskIndex > 1 ) then
        local tooltip = 
        {
            I = "Reduce cast penalty by 30% and increases damage by 5% for scrolls.",
            II = "Reduce cast penalty by 60% and increases damage by 10% for scrolls.",
            III = "Reduce cast penalty by 100% and increases damage by 15% for scrolls.",
        }
        profElement:AddLabel(xOffset+80,180,"[d8a54d]Scholar "..romanNumerals.."[-]",150,60,22,"center",false,false,"Kingthings_Dynamic")
        profElement:AddImage(xOffset+54, 124, "scribe-passive",50,50)
        profElement:AddButton(xOffset+35, 125, "tooltip", "", 90, 75, tooltip[romanNumerals], "",false,"Invisible")
        
    else    
        profElement:AddLabel(xOffset+80,180,"[d8a54d]Unlocked[-]",150,60,22,"center",false,false,"Kingthings_Dynamic")
        profElement:AddImage(xOffset+49, 130, "task_complete_crown",0,0)
    end

    return xOffset

end

ProfessionsHelpers.Traits = 
{
    Alchemist = {{ "PotionEffectTimes", { nil, 0.1, 0.3, 0.5 } }},
    Chef = {{ "MealEffectTimes", { nil, 0.5, 0.75, 1.0 } }},
    Lumberjack = {{ "SlashingSkillDamageTimes", { nil, 0.05, 0.1, 0.15 } }, { "LancingSkillDamageTimes", { nil, 0.05, 0.1, 0.15 } }},
    Miner = {{ "BashingSkillDamageTimes", { nil, 0.05, 0.1, 0.15 } }, { "PiercingSkillDamageTimes", { nil, 0.05, 0.1, 0.15 } }},
    Fisher = {{ "ArcherySkillDamageTimes", { nil, 0.05, 0.1, 0.15 } }},
    Scribe = {{ "ScrollCastPenaltyTimes", { nil, -0.5, -1, -2 } }, { "ScrollDamageTimes", { nil, 0.02, 0.4, 0.08 } }},
}

ProfessionsHelpers.ApplyTraits = function( _playerObj )

    for skill,options in pairs( ProfessionsHelpers.Traits ) do
        for i=1, #options do 

            local mobileMod = options[i][1]
            local traits = options[i][2]
            local traitSet = false
            local i = 4
            while(true) do
                if( ProfessionsHelpers.IsTaskCompleted(_playerObj,skill,i, true) ) then
                    SetMobileMod( _playerObj, mobileMod, "Trait", traits[i] )
                    SetCombatMod( _playerObj, mobileMod, "Trait", traits[i] )
                    traitSet = true
                    break;
                end
                i = i - 1
                if (i == 1 or traitSet) then break; end
            end

            if( traitSet == false ) then
                SetMobileMod( _playerObj, mobileMod, "Trait", traits[1] )
                SetCombatMod( _playerObj, mobileMod, "Trait", traits[1] )
            end
        end
    end
end