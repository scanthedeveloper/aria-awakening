
ServerSettings.Durability = {
    -- max durability an item will have if it doesn't have a MaxDurability ObjVar on it.
    DefaultMax = 10,
    -- number of times to warn the item is about to break
    BreakWarnings = 3,
    Chance = {
        -- the percent chance a weapon will be damaged and loose durability when swung (missing doesn't count)
        OnWeaponSwing = 0.25,
        AttunedWeapon = 0.1,--should be about half of OnWeaponSwing
        -- the percent chance an item will be damaged and loose durability when being struck
        --SCAN Adjusted from .02
        OnEquipmentHit = 0.5,
        --SCAN Added
        AttunedArmor = 0.1,
        -- jewelry is always 'hit' for durability calculations.
        OnJewelryHit = 0.05,

        -- Use these values when repair works
        -- OnWeaponSwing = 0.01,
        -- OnEquipmentHit = 0.05,


        -- the percent chance an item will be damaged and loose durability when being used
        --SCAN Adjusted from .01
        OnToolUse = 0.05,
        OnMapDecipher = 0.5,
    },

    FailHit = {
        OnMapDecipher = -5,
    },

    Pets = {
        OnDeath = -2,
    }
}