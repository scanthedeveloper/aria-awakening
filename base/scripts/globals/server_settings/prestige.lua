ServerSettings.Prestige = {
	PrestigePointXP = 1000,

	AbilityRankPointCost = { -- amount of xp consumed when unlocking an ability of that level
		3, -- level 1,
		15, -- level 2,
		30, -- level 3,
	},

	-- Amount of skill you can lose and still perform ability
	AbilitySkillModifier = 5,
}