ServerSettings.GuildWars = {
	-- What are the lengths of wars that players who are declaring war can choose from
    -- You can only have 3 durations currently
	Durations = {
		TimeSpan.FromDays(1),
		TimeSpan.FromDays(7),
		TimeSpan.FromDays(30),
	},

	-- How long after a war ends before you can declare war again
	WarExpiration = TimeSpan.FromDays(1),

	-- Maximum number of outgoing war declarations per guild
	MaximumOutgoingDeclarations = 10,
}