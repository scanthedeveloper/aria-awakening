ServerSettings.Militia = {
    CurSeason = 3,

    ResignTime = TimeSpan.FromDays(7),
    SignupFavor = 0.1,--default is 0.1
    FavorFromEvents = 6,--default 6
        
    -- Rank 1, even kill is worth this amount, all other ranks is base * (2 ^ rank-1)
    FavorBaseKillValue = 80,
    -- How long after somone is killed are they worth rps
    RecentlyKilledTime = TimeSpan.FromMinutes(3),
    -- If less than x pct of the population is this militia give the bonus
    UnderpopulatedPct = 0.2,
    -- Percentage of RPs awarded as a bonus for being underpopulated
    UnderpopulatedBonus = 0.3,
    GlobalBonus = 0.0,

    DRKillsToZero = 5,--default 5
    DRResetTime = TimeSpan.FromDays(7),--default TimeSpan.FromDays(7)
    MaxSaltOnKill = 300,
    InactiveTime = TimeSpan.FromDays(14),--default TimeSpan.FromDays(14)
    SeasonDurationDays = 30,--default 90
    ChatCooldown = TimeSpan.FromMinutes(1),--default TimeSpan.FromMinutes(1)
    ChatRankRequired = 0,--default 0
    VulnerabilityDuration = TimeSpan.FromMinutes(10),--default TimeSpan.FromMinutes(10)
    VulnerabilityRadius = 30,--default 30
    FlagAtBaseVulnerabilityRadius = 30,--default 30

    EventCooldownMinutesMin = 110,
    EventCooldownMinutesMax = 130,
    EventWarnMinutesBeforeStart = 20,
    EventMaxActive = 0,--default 0
    EventDefaultCooldown = TimeSpan.FromHours(2),--default TimeSpan.FromMinutes(30)
    EventTypeRotation = 1,--default 1, 1=round robin, 2=weighted random
    EventTypeWeights = {50, 100},--roll 1-100, first number that is less than the weight is selected

    Events = {
        {   Name = "Cleansing",
            PointsLimit = 1000000,--can be thought of as damage to mobs from a single team, usually best to just rely on time limit
            MinutesLimit = TimeSpan.FromMinutes(30),--absolute time limit for event duration
        },
        {
            Name = "King of the Hill",
            PointsLimit = 1000000,
            MinutesLimit = TimeSpan.FromMinutes(15),--absolute time limit for event duration

            MilitiaMobTalentPool = 500,
            MilitiaMobMinReward = 100,
            MilitiaMobMaxReward = 300,
        },
        {
            Name = "Capture the Flag",
            CurrencyRewardCapture = 500,
            CurrencyRewardReturn = 300,
            RewardCooldown = TimeSpan.FromMinutes(30),
            FlagbearerDuration = TimeSpan.FromDays(2),--maximum time one person can continuously hold a flag
            FlagPickupTime = TimeSpan.FromSeconds(15), -- amount of time it takes to pick up flags
            GroundDuration = TimeSpan.FromMinutes(10),--maximum time the flag can be on the ground before being returned
            MinCooldown = TimeSpan.FromHours(1),--minimum lockout after a flag is captured
            MaxCooldown = TimeSpan.FromHours(12),--maximum lockout after a flag is captured, modified by amount of flags you own at capture
            MinutesLimit = TimeSpan.FromMinutes(30),--absolute time limit flag can be gone before resetting

            DoorMaxHP = 20000,
            RamBaseDamage = 100,
            RamMaxHP = 10000,
            RamDamagePerSecond = 10,
            RamAttackSpeed = TimeSpan.FromSeconds(5),
            RamAttackNumPlayers = 2,

            DoorRepairPerSecond = 5,
            DoorRepairSpiritwoodPerHP = 0.25,
        },
    },

    GlobalResources = {
        MaximumValue = 10000,
        AccrualPerMinute = 50,

        RamResourceCost = 5000,
    },

    Militias = {
        {
            Id = 1,
            Name = "Warriors of Pyros",
            TextColor = "[e83034]",--base 9a1113
            Town = "Pyros",
            IconLarge = "Militia_Icon_PyrosShield_Large",
            IconTown = "icon_Pyros",
            MilitiaLeaderTitle = "Warriors of Pyros",

            RankRewardMounts = {
                [3] = "pyros_militia_horse",
            },

            RankRewardCloaks = {
                [2] = "pyros_cloak_epic",
            },

            RankRewardBoxes = {
                [2] = "copper_rank_box",
                [3] = "gold_rank_box",
                [4] = "cobalt_rank_box",
                [5] = "obsidian_rank_box",
            }
        },
        {
            Id = 2,
            Name = "Helm Militia",
            TextColor = "[ef902a]",--base d37410
            Town = "Helm",
            IconLarge = "Militia_Icon_HelmShield_Large",
            IconTown = "icon_Helm",
            MilitiaLeaderTitle = "Helm Militia",

            RankRewardMounts = {
                [3] = "helm_militia_horse",
            },

            RankRewardCloaks = {
                [2] = "helm_cloak_epic",
            },

            RankRewardBoxes = {
                [2] = "copper_rank_box",
                [3] = "gold_rank_box",
                [4] = "cobalt_rank_box",
                [5] = "obsidian_rank_box",
            }
        },
        {
            Id = 3,
            Name = "Guardians of Eldeir",
            TextColor = "[1c9ffd]",--base 025fa1
            Town = "Eldeir",
            IconLarge = "Militia_Icon_EldeirShield_Large",
            IconTown = "icon_Eldeir",
            MilitiaLeaderTitle = "Guardians of Eldeir",

            RankRewardMounts = {
                [3] = "eldeir_militia_horse",
            },

            RankRewardCloaks = {
                [2] = "eldeir_cloak_epic",
            },

            RankRewardBoxes = {
                [2] = "copper_rank_box",
                [3] = "gold_rank_box",
                [4] = "cobalt_rank_box",
                [5] = "obsidian_rank_box",
            }
        },
    },

    RankNames = {
        "Iron",
        "Coppper",
        "Gold",
        "Cobalt",
        "Obsidian",
    },

    -- How much favor does each rank require
    -- This table is copied from the militia spreadsheet
    -- The actual formula for 1-41 is RP = (R ^ 3.4112) * 1.4
    -- and for 42-50 is RP = ((Rank + 8) ^ 4.0374324) * 3
    FavorLevels = {
        0,
        25,
        125,
        350,
        775,
        1425,
        2400,
        3800,
        5675,
        8125,
        11250,
        15125,
        19875,
        25600,
        32375,
        40350,
        49625,
        60325,
        72525,
        86400,
        102050,
        119600,
        139175,
        160925,
        184975,
        211450,
        240525,
        272275,
        306900,
        344525,
        385300,
        429375,
        476900,
        528025,
        582900,
        641700,
        704575,
        771675,
        843175,
        919225,
        1000000,
        1250725,
        1605925,
        2094150,
        2748300,
        3605950,
        4709625,
        6106925,
        7850850,
        10000000,
    }
}