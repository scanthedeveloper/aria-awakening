ServerSettings.Campfire = {
    -- max range to get the campfire effect
    MaxRange = 5,
    -- do players have to be in a group together to benefit from the campfire?
    
    --MALEVOLENT MOD DJM ADDED
    RequireGroup = false,
    --RequireGroup = true,

    -- Regen bonus for each stat the campfire 'refuels'
    Bonus = {
        Health = 0.05,
        Mana = 0.01,
        Stamina = 0.01,
    },
    Disturb = {
        -- do player disturb campfires while in war mode?
        Players = false,
        -- do non-player characters disturb campfires while in war mode?
        NPCs = false
    },
    -- how long does a campfire last when created by players?
    Expire = TimeSpan.FromMinutes(1),
    DecaySeconds = 2, -- number of seconds a campfire will hang around after it's been extinguished (disturbed)
}