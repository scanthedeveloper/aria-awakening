ServerSettings.Plot = {

    LandDeedCostMultiplier = 0.1,

    --- limit how often they can rename their plot since it updates global variables
    MaxRenameInterval = TimeSpan.FromSeconds(30),

    -- Since all plots should always be alligned and when first placing a plot it will be minimum, 
        --it's important to keep MinimumSize an even number. (Or add support to detect an odd number and allow rounding to .5 in Plot.New)
    MinimumSize = 12,

    -- maxiumsize is a required so we can look for controllers nearby
    --MALEVOLENT MOD DJM ADDED
    MaximumSize = 60,
    --MaximumSize = 36,

    -- multiply the resource cost of building a house by this amount
    HouseResourceCostModifier = 2,

    -- multiply the coin cost of a house blueprint by this amount
    HouseBlueprintCostModifier = 2,

    -- how much commit cost scales with plot size, expoentially.
    CostCoefficient = 2.2,

    -- the minimum time between being warned about taxes on login
    DailyTaxWarnInterval = TimeSpan.FromHours(20),

    -- maximum number of tax ledger details to keep in detail history.
    DetailedLedgerMaxLength = 100,

    -- allow players to pack their houses into a moving crate
    AllowPackPlot = false,

    Tax = {
        -- disable for free plot tax.
        --MALEVOLENT MOD DJM ADDED
        Enabled = true,
        --Enabled = true,

        -- how much tax rate scales with plot size, expoentially.
        RateCoefficient = .50,

        -- minimum tax payment
        MinimumPayment = 100,

        -- use this as the beginning date to base the taxing interval off of
        --         DateTime(year,month,day,hour,min,sec)
        TaxEpoch = DateTime(2019, 1, 4, 20, 0, 0),
        TaxInterval = TimeSpan.FromDays(7),
        -- number of seconds past tax time that will still count it as tax time
        --- The larger this resolution the more time a server can be offline and taxes still be collected when it returns.
        --- if the server is offline longer than this, and tax time didn't take, consider if a free week for all.
        -- NOTE: This needs to be LESS THAN the taxing interval, for example if you're taxing hourly, this must be < 1 hour
        TaxTimeResolution = TimeSpan.FromHours(8),

        -- all negative plots will be removed from their owners and put up for sale on this interval
        SaleEpoch = DateTime(2019, 1, 6, 20, 0, 0),
        SaleInterval = TimeSpan.FromDays(7),
        -- see TaxTimeResolution
        SaleTimeResolution = TimeSpan.FromHours(8),

        -- rate times this modifier will be the maximum tax a plot can have (by player added funds)
        MaxBalanceRateModifier = 4,

        -- how long it will take the region to tax all houses, rough guess, not important to be accurate.
        TaxingMaxTimeSpent = TimeSpan.FromMinutes(5),

    },

    Auction = {

        -- what day will auction start
        StartEpoch = DateTime(2019, 1, 8, 20, 0, 0),
        StartInterval = TimeSpan.FromDays(7),

        -- how long an auction lasts from bidding open to bidding closed.
        Length = TimeSpan.FromDays(4),

        -- these are the time slots, 
            --for example if sale date is at 8PM, MinumumStart is 1 day(24 hrs)
            --then the next day at 8PM, each auction will be spaced until interval, 
            --if there are more auctions than interval then some auctions will begin to run en tandem. 
        Slots = {
            Interval = TimeSpan.FromHours(1),
            Max = 8, -- interval * Max = Window of time all auctions run
        },

        -- percent of total bid that will never be refunded
        BidFee = 0.01,

    }
}

ServerSettings.Plot.SearchSize = math.ceil(CalculateHypotenuse(ServerSettings.Plot.MaximumSize, ServerSettings.Plot.MaximumSize))