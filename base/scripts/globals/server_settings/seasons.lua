ServerSettings.ChallengeSystem = {
    
    ---------------------------------------------------
    -- CHANGING THIS WILL RESET THE TOKENS FOR ALL PLAYERS!
    
    --SCAN CHANGED SEASON
    Season = 1,
    ---------------------------------------------------
    SeasonTitle = "Season I:\n    The Great Awakening",
    
    -- Season variables
    --[[
    Diggable = true,
    Diggables = 
    {
        { Chance = 0.25, Template = "seasonal_wyvern", Type = "mobile" },
        
        { Chance = 1.5, Template = "treasure_map", Type = "item", Count = 1 },

        { Chance = 2.5, Template = "animalparts_wild_feather", Type = "item", Count = 2 },
        { Chance = 2.5, Template = "animalparts_beast_leather_hide", Type = "item", Count = 2 },
        { Chance = 2.5, Template = "resource_cotton_fluffy", Type = "item", Count = 2 },
        { Chance = 2.5, Template = "resource_fine_scroll", Type = "item", Count = 2 },
        { Chance = 2.5, Template = "animalparts_eye_sickly", Type = "item", Count = 2 },
        { Chance = 2.5, Template = "animalparts_blood_beast", Type = "item", Count = 2 },
        { Chance = 2.5, Template = "resource_ash", Type = "item", Count = 2 },
        { Chance = 2.5, Template = "resource_gold_ore", Type = "item", Count = 2 },
        { Chance = 2.5, Template = "animalparts_bone_cursed", Type = "item", Count = 2 },
        { Chance = 2.5, Template = "seed_redleaflettuce", Type = "item", Count = 2 },

        { Chance = 5, Template = "seasonal_rock_sprite", Type = "mobile" },
        { Chance = 5, Template = "seasonal_lich", Type = "mobile" },
        { Chance = 5, Template = "seasonal_lizardman_scout", Type = "mobile" },
        { Chance = 5, Template = "seasonal_beetle_giant", Type = "mobile" },

        { Chance = 8, Template = "seasonal_goblin", Type = "mobile" },
        { Chance = 8, Template = "seasonal_lizardman", Type = "mobile" },

        { Chance = 10, Template = "seasonal_worm", Type = "mobile" },
    },
    
    DigBlackZones = 
    {
        "EldeirLarge",
        "TrinitLarge",
        "PyrosLarge",
        "OasisLarge",
        "ValusLarge",
        "BelhavenLarge",
        "HelmLarge",
        "OutpostLarge",
    },
    ]]

    -- ObjVar Settings
    TokenObjVar = "ChallengeTokens", -- ObjVar key for tokens
    PVPTokenObjVar = "ChallengeTokensPVP", -- ObjVar key for PVP tokens
    SeasonObjVar = "ChallengeSeason", -- ObjVar key for season the player is on
    TokenRewardObjVar = "ChallengeRewards", -- ObjVar for rewards claimed this season

    -- Reward GlobalVar Properties
    PVE1Reward = "PVE1RewardClaimed",
    PVE2Reward = "PVE2RewardClaimed",
    PVE3Reward = "PVE3RewardClaimed",

    PVP1Reward = "PVP1RewardClaimed",
    PVP2Reward = "PVP2RewardClaimed",
    PVP3Reward = "PVP3RewardClaimed",

    -- Reward Templates
    RewardTemplates = {
        PVE1RewardClaimed = "season_one_pve1_reward_box",
        PVE2RewardClaimed = "season_one_pve2_reward_box",
        PVE3RewardClaimed = "season_one_pve3_reward_box",

        PVP1RewardClaimed = "season_one_pvp1_reward_box",
        PVP2RewardClaimed = "season_one_pvp2_reward_box",
        PVP3RewardClaimed = "season_one_pvp3_reward_box",
    },

    -- Reward Loot Tables
    RewardLootTables = {

        PVE1RewardClaimed = {
            NumItems = 1,
            LootItems = 
            {
                { Chance = 100, Template = "reward_crate_pve_season_tier1", Unique = true, StackCount = 1 },
            }
        },	
        
        
        PVE2RewardClaimed = {
            NumItems = 1,
            LootItems = 
            {
                { Chance = 100, Template = "reward_crate_pve_season_tier2", Unique = true, StackCount = 1 },
            } 
        },

        PVE3RewardClaimed = {
            NumItems = 1,
            LootItems = 
            { 
                { Chance = 100, Template = "reward_crate_pve_season_tier3", Unique = true, StackCount = 1 },
            } 
        },

        PVP1RewardClaimed = {
            NumItems = 1,
            LootItems = 
            {
                { Chance = 100, Template = "reward_crate_pvp_season_tier1", Unique = true, StackCount = 1 },
            } 
        },

        PVP2RewardClaimed = {
            NumItems = 1,
            LootItems = 
            {
                { Chance = 100, Template = "reward_crate_pvp_season_tier2", Unique = true, StackCount = 1 },
            }
        },

        PVP3RewardClaimed = {
            NumItems = 1,
            LootItems = 
            {
                { Chance = 100, Template = "reward_crate_pvp_season_tier3", Unique = true, StackCount = 1 },
            }
        },

    },

    -- Reward Settings
    PrizeOneTokensNeeded = 2000,    -- How many tokens are needed to qualify for the first prize?
    PrizeTwoTokensNeeded = 3000,    -- How many tokens are needed to qualify for the second prize?
    PrizeThreeTokensNeeded = 4000,    -- How many tokens are needed to qualify for the third prize?

    -- UI Settings
    WindowID = "ChallengeSystemUI",
    Title = "Challenges",

}