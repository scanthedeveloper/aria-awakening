ServerSettings.Gardening = {
    -- How often should the master controller perform Gardening.GrowAll()
    -- Set this to 30 minutes (24x per day)
    GrowTimeResolution = TimeSpan.FromMinutes(30),
    -- Are plants able to be grown with Plant.GrowAll()?
    GrowEnabled = true,
    -- How far away from the plant can you be to interact with it?
    MaximumRange = 5, 
    -- Waterskins required to water plant
    WaterskinsConsumedPerWater = 1,
    InitialSoilType = "Dry",
    InitialMaturityInDays = 0,
    -- Dryest (left) to Wettest (right)
    SoilWaterLevels = { "Dry", "Damp", "Wet", "Soaked" },
    MaturityIntervalMinutes = 1040, -- 24 hours (1,040 minutes)
    GatherAmountSevenDayMaturity = {
        0,      -- Day 1
        0.1,    -- Day 2
        0.2,    -- Day 3
        0.3,    -- Day 4
        0.5,    -- Day 5
        0.75,   -- Day 6
        1       -- Day 7
    },
    GatherAmountTenDayMaturity = {
        0,      -- Day 1
        0.05,   -- Day 2
        0.1,    -- Day 3
        0.15,   -- Day 4
        0.2,    -- Day 5
        0.25,   -- Day 6
        0.3,    -- Day 7
        0.5,    -- Day 8
        0.75,   -- Day 9
        1,      -- Day 10
    },
    PlantContainterPlantOffset = {
        Pot = { 0.03, 0.4, 0 },
        SmallRaisedBed = { 0.03, 0.0, 0 },
        MediumRaisedBed = { 0.03, 0.0, 0 },
        LargeRaisedBed = { 0.03, 0.0, 0 },
    }
}
