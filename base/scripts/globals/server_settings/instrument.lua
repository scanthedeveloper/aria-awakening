ServerSettings.Instrument = {
    -- Maximum range a listen can move before song ends
    MaximumListenRange = 15,
    -- Maximum range another bard has to be to join a band
    MaximumBandMemberRange = 5,
    -- Maximum range a hearth has to be to allow the bard to regen vitality
    MaximumHearthRange = 10,
    -- Vitality to regen on solo (flurry) success
    FlurryVitalityBonus = 2,
    -- All instrument types
    AllInstruments = { "flute", "lute", "drum" },
}