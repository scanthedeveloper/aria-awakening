ServerSettings.Misc = {
	WildernessModifiers = { 
		Gold = 1.5,
		Resource = 1,
		LootRoll = 1,
	},

	--MALEVOLENT MOD DJM ADDED
	HearthstoneCooldown = TimeSpan.FromMinutes(10),
	--HearthstoneCooldown = TimeSpan.FromMinutes(30),
	CursedHearthstoneCooldown = TimeSpan.FromMinutes(10),
	--CursedHearthstoneCooldown = TimeSpan.FromMinutes(15),

	MustLearnMaps = false,
	MaxAtlasWaypoints = 25,
	MaxMapWaypoints = 10,
	StarvationEnabled = true,
	HungerTickTimer = 240,
	LethalStarvation = true,
	RestEnabled = false,
	RestTimer = 20,
	ObjectDecayTimeSecs = 300,
	FoodFillingnessCap = 40,
	FoodMaxStarvationLevel = -3,
	FoodStarvationDivder = 5,		
	
	EnforceBadWordFilter = true,
	
	--MALEVOLENT MOD DJM ADDED
	BankWeightLimit = 100000,
    --BankWeightLimit = 10000,
	FreePlayerBankWeightLimit = 10000,
    --FreePlayerBankWeightLimit = 1000,
	BackpackBaseWeightLimit = 300,
    --BackpackBaseWeightLimit = 200,
	DefaultContainerWeightLimit = 5000,
    --DefaultContainerWeightLimit = 2000,
	CoinWeight = 0.001,
    --CoinWeight = 0.004,
    
	PetDecayTime = 3600,
	NonCombatMaps = {
		"Founders"
	},

	HelpReportEnabled = true,
}