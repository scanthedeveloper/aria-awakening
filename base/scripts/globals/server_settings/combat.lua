ServerSettings.Combat = {
	LOSEyeLevel = 1.9,
	DefaultBodySize = 0.8,

	MaxMinions = 3,

	ScrollCastPower = 5,
	ScrollBaseMagerySkill = 50,

	NoSelfDamageOnAOE = true,
	AoEGuildDamageDefault = true,

	PlayerEndCombatPulse = 5,
	NPCEndCombatPulse = 20,

	MinimumSwingSpeed = 0.5, --fastest possible swing speed

	NoHideRange = 8, -- can not hide if within X units of another mob

	BowStopMinDelay = TimeSpan.FromSeconds(0), --Minimum amount of time a bow will fire after stopping

	CastSwingDelay = TimeSpan.FromSeconds(0), --Amount of time to add to swing on a successful spell cast

	DazeChance = 100,
	DismountWhileDazedChance = 50, -- Chance to be dismounted when sustaining damage while dazed.
	DazedOnDamageWhileMounted = true, --Apply Daze to mounted mobs when they receive damage
	DismountWhileDazed = true, --Check to see if dazed targets have a chance to be dismounted.	

	-- Combat limits
	MaxLimits = {
		AttackSpeed = 1.25, -- per second
		AttackBonus = 150, -- percent (AttackBonus)
		AttackSpeedTimes = 0.70, -- 30% reduced delay
		ManaReductionTimes = 0.70, -- 30% reduced cost
		AccuracyPlus = 25, -- points maximum
		EvasionTimes = 25, -- points maximum
		HitChancePlus = 25, -- points maximum
		DefenseChancePlus = 25, -- points maximum
		PowerPlus = 10, -- points maximum
	}
}