--SCAN ADDED
--Removed Enchanted Loot System from default spawns.

ServerSettings.EnchantedLoot = {
    
    -- What chance does each Quality level have to drop?
    -- Corresponds to the Level in each RandomEnchantedLoot.Loot[] entry on mobile XML
    QualityDropChance = { 0.1, 0.08, 0.04, 0.02, 0.01 },
    QualityEnchantCount = { 1, 1, 2, 2, 3 },
    QualityNameColors = { "ADFF2F", "ADFF2F", "ADFF2F", "ADFF2F", "ADFF2F" },


    -- Minimum amount of items that drop per loot entry
    MinimumItems = 0,

    -- Item type templates
    ItemTypeTemplates = {
        
        Weapons = { "weapon_longsword_dropped", "weapon_bone_dagger_dropped", "weapon_broadsword_dropped", "weapon_dagger_dropped", "weapon_greataxe_dropped", "weapon_halberd_dropped", "weapon_hammer_dropped", "weapon_katana_dropped", "weapon_kryss_dropped", "weapon_largeaxe_dropped", "weapon_longbow_dropped", "weapon_mace_dropped", "weapon_maul_dropped", "weapon_poniard_dropped", "weapon_quarterstaff_dropped", "weapon_saber_dropped", "weapon_voulge_dropped", "weapon_war_mace_dropped", "weapon_warfork_dropped", "weapon_warhammer_dropped" },
        Armors = { 
            "robe_robe","robe_linen_helm","robe_linen_leggings","robe_linen_tunic",
            "robe_padded_tunic", "robe_padded_leggings", "robe_padded_helm", 
            "armor_hardened_helm", "armor_hardened_leggings", "armor_hardened_chest", 
            "armor_leather_helm", "armor_leather_leggings", "armor_leather_chest", 
            "armor_plate_helm", "armor_plate_leggings", "armor_plate_tunic",
            "armor_scale_helm", "armor_scale_leggings", "armor_scale_tunic" 
        },
        Mages = { "attuned_master_wand", "attuned_novice_wand", "attuned_white_staff", "attuned_pearl_staff", "attuned_apprentice_wand", "attuned_staff", "attuned_iron_staff" },
        Bows = { "weapon_shortbow", "weapon_longbow", "weapon_warbow",},
        Shields = { "shield_buckler", "shield_kite", "shield_smallshield" },
    },

    PremadeLootLevels =
    {
        -- 1 MOBS
        {
            Level = 1, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 0,
            --SCAN Changed
            Chance = 0,
            --Chance = .5,
            EnchantCountMax = 1,
        },
        -- 2
        {
            Level = 2, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 0,
            --SCAN Changed
            Chance = 0,
            --Chance = .5,
            EnchantCountMax = 2,
        },
        -- 3
        {
            Level = 3, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 0,
            --SCAN Changed
            Chance = 0,
            --Chance = .5,
            EnchantCountMax = 3,
        },
        -- 4
        {
            Level = 4, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 0,
            --SCAN Changed
            Chance = 0,
            --Chance = .75,
            EnchantCountMax = 4,
        },
        -- 5
        {
            Level = 5, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 0,
            --SCAN Changed
            Chance = 0,
            --Chance = .9,
            EnchantCountMax = 4,
        },
        -- 6 CHESTS
        {
            Level = 1, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 1,
            --SCAN Changed
            Chance = 0,
            --Chance = 1.0,
            EnchantCountMax = 1,
        },
        -- 7
        {
            Level = 2, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 2,
            --SCAN Changed
            Chance = 0,
            --Chance = 1.0,
            EnchantCountMax = 2,
        },
        -- 8
        {
            Level = 3, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 3,
            --SCAN Changed
            Chance = 0,
            --Chance = 1.0,
            EnchantCountMax = 3,
        },
        -- 9
        {
            Level = 4, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 4,
            --SCAN Changed
            Chance = 0,
            --Chance = 1.0,
            EnchantCountMax = 4,
        },
        -- 10
        {
            Level = 5, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 5,
            --SCAN Changed
            Chance = 0,
            --Chance = 1.0,
            EnchantCountMax = 4,
        },
        -- 11 BOSSES
        {
            Level = 5, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 2,
            --SCAN Changed
            Chance = 0,
            --Chance = 1.0,
            EnchantCountMax = 4,
        },
        -- 12
        {
            Level = 5, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 3,
            --SCAN Changed
            Chance = 0,
            --Chance = 1.0,
            EnchantCountMax = 4,
        },
        -- 13
        {
            Level = 5, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 5,
            --SCAN Changed
            Chance = 0,
            --Chance = 1.0,
            EnchantCountMax = 4,
        },
        -- 14
        {
            Level = 5, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 10,
            --SCAN Changed
            Chance = 0,
            --Chance = 1.0,
            EnchantCountMax = 4,
        },
        -- 15
        {
            Level = 5, 
            Type = { "Weapons", "Armors", "Mages", "Bows", "Shields" },
            MinItems = 20,
            --SCAN Changed
            Chance = 0,
            --Chance = 1.0,
            EnchantCountMax = 4,
        },
    },

}