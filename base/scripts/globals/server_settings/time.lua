ServerSettings.Time = {
	DayNightCycleEnabled = true,
	DayDuration = 3600,
	NightDuration = 0,
	DaylightStartOffset = 0
}