require 'globals.server_settings.ai'
require 'globals.server_settings.combat'
require 'globals.server_settings.crafting'
require 'globals.server_settings.death'
require 'globals.server_settings.hunger'
require 'globals.server_settings.merchants'
require 'globals.server_settings.misc'
require 'globals.server_settings.pets'
require 'globals.server_settings.player_interactions'
require 'globals.server_settings.prestige'
require 'globals.server_settings.skills'
require 'globals.server_settings.stats'
require 'globals.server_settings.system'
require 'globals.server_settings.time'
require 'globals.server_settings.weapon_abilities'
require 'globals.server_settings.character_creation'
require 'globals.server_settings.new_player'
require 'globals.server_settings.campfire'
require 'globals.server_settings.vitality'
require 'globals.server_settings.resources'
require 'globals.server_settings.durability'
require 'globals.server_settings.criminal'
require 'globals.server_settings.conflict'
require 'globals.server_settings.militia'
require 'globals.server_settings.group'
require 'globals.server_settings.teleport'
require 'globals.server_settings.executioner'
require 'globals.server_settings.plot'
require 'globals.server_settings.friend'
require 'globals.server_settings.instrument'
require 'globals.server_settings.gardening'
require 'globals.server_settings.guild_wars'
require 'globals.server_settings.seasons'
require 'globals.server_settings.enchanted_loot'
require 'globals.server_settings.arena'