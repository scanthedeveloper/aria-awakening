ServerSettings.Crafting = {

    ArtifactChance = 0.05,
    ArtifactCurseChance = 0.01,

    MakersMark = {
        Enabled = true, -- should items have a "Created by" in the tooltip?
        MakersMark = "Crafted by %s" -- %s will be replaced with the crafter's name.
    },

    --SCAN ADDED
    MagicDurability = {
        Enabled = true, -- should items have a "Created by" in the tooltip?
        DuraBonus = "[00FF00]Durability Bonus +%s[-]" -- %s will be replaced with the crafter's name.
    },

    MaterialSkillDifficultyModifier = 5,

    NewSkillBonus = {

        Attack = {
            Max = 50,-- max a skill bonus can give to + attack %
            Weight = function(skillLevel) -- different materials are weighted to give better results
                if( skillLevel > 90 ) then return 60 
                elseif( skillLevel > 70 ) then return 25 
                elseif( skillLevel > 50 ) then return 10
                elseif( skillLevel > 30 ) then return 5
                else return 0
                end  
            end
        },
        Accuracy = {
            Max = 0,-- max a skill bonus can give to + attack %
            Weight = function(skillLevel) -- different materials are weighted to give better results
                if( skillLevel > 90 ) then return 60 
                elseif( skillLevel > 70 ) then return 25 
                elseif( skillLevel > 50 ) then return 10
                elseif( skillLevel > 30 ) then return 5
                else return 0
                end  
            end
        },
        Armor = {
            Max = 3,-- max a skill bonus can give to + attack %
            Weight = function(skillLevel) -- different materials are weighted to give better results
                if( skillLevel > 90 ) then return 2 
                elseif( skillLevel > 70 ) then return 1 
                else return 0
                end  
            end
        },
        

    },

    -- The weighted roll works by performing a roll this many times,
    -- the roll closest to the weight of the material will be chosen.
    MaterialBonusCandidates = 3,
    -- holds data used to roll for a type of bonus upon crafting an item
    MaterialBonus = {
        -- these are in percents
        Attack = {
            Max = 0, -- max a material bonus can give to + attack %
            Weight = { -- different materials are weighted to give better results
                Iron = 0,
                Copper = 5,
                Gold = 10,
                Cobalt = 25,
                Obsidian = 60,

                Boards = 0,
                AshBoards = 10,
                BlightwoodBoards = 60,
            }
        },
        Accuracy = {
            Max = 0, -- max a material bonus can give to + accuracy
            Weight = { -- different materials are weighted to give better results
                Iron = 0,
                Copper = 5,
                Gold = 10,
                Cobalt = 25,
                Obsidian = 60,

                Boards = 0,
                AshBoards = 10,
                BlightwoodBoards = 60,
            }
        },
        -- these are NOT in percents
        Armor = {
            Max = 0, -- max amount of extra armor rating a material can give (This max is HALFED in final calculation)
            Weight = { -- different materials are weighted to give better results
                Iron = 1,
                Copper = 2,
                Gold = 3,
                Cobalt = 4,
                Obsidian = 5,

                Cloth = 1,
                QuiltedCloth = 3,
                SilkCloth = 5,

                Leather = 1,
                BeastLeather = 3,
                VileLeather = 5,

                Boards = 1,
                AshBoards = 3,
                BlightwoodBoards = 5,
            }
        },
        -- non-weighted, this is just added to the created item.
        Durability = {
            Iron = 25,
            Copper = 50,
            Gold = 100,
            Cobalt = 150,
            Obsidian = 200,

            Cloth = 25,
            QuiltedCloth = 50,
            SilkCloth = 100,

            Leather = 25,
            BeastLeather = 75,
            VileLeather = 150,

            Boards = 25,
            AshBoards = 100,
            BlightwoodBoards = 200,
        },

        -- Increase in ServerSettings.Crafting.ArtifactCurseChance
        ArtifactCurse = {
            Iron = 0.01,
            Copper = 0.02,
            Gold = 0.03,
            Cobalt = 0.04,
            Obsidian = 0.06,

            Cloth = 0.02,
            QuiltedCloth = 0.04,
            SilkCloth = 0.06,

            Leather = 0.02,
            BeastLeather = 0.04,
            VileLeather = 0.06,

            Boards = 0.02,
            AshBoards = 0.04,
            BlightwoodBoards = 0.06,
        }
    },
    -- bonus that can be gain frm the skill of crafting the item, the skill level is turned into a percent and multiplied against these numbers
    SkillBonus = {
        Attack = {
            Max = 50,
        },
        Accuracy = {
            Max = 0,
        },
        Armor = {
            Max = 3, -- (This max is HALFED in final calculation)
        },
        -- non-weighted, this is just added to the created item.
        Durability = {
            Max = 25, 
        }
    },
}