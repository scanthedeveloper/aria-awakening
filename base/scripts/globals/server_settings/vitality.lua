ServerSettings.Vitality = {
    -- vitality is adjusted by this amount when a player is resurrected from the ghost and given a new body
    AdjustOnGhostResurrect = -5,
    AdjustOnCorpseResurrect = -2,
    AdjustOnSelfCorpseResurrect = -10,
    AdjustOnShrineResurrect = -5,
    AdjustOnBardListeningPulse = 1,
    AdjustOnHearthNearPulse = 0.50,

    AdjustOnSkillCheck = -1,
    AdjustOnSkillCheckChance = 0.05, -- percentage 1-100
    AdjustOnSkillCheckTimer = TimeSpan.FromSeconds(5), -- how often can you lose vitality from skill checks

    -- what's considered low enough to apply LowVitality mobile effect
    Low = 20,
    -- when to start warning
    Warn = 40,

    Chance = {
        OnDeath = 0.5,
        OnWeaponSwing = 0.1,
        
        --MALEVOLENT MOD SCAN ADDED
        OnToolUse = 0.05,
        --OnToolUse = 0.005,
    },

    DisplayStrings = {
        {1,"Well Rested"},
        {0.8,"Rested"},
        {0.6,"Content"},
        {0.4,"Restless"},
        {0.2,"Fatigued"},
        {0.0,"Exhausted"},
    },

    -- settings for hearth objects, primary purpose is to regenerate vitality of players around it
    Hearth = {
        -- enable hearth vitality regeneration

        --MALEVOLENT MOD DJM ADDED
        EnableHearthVitalityRegen = true,
        --EnableHearthVitalityRegen = false,
        
        -- max distance one can be from a hearth object to receive the effect
        MaxRange = 12,
        -- the regeneration bonus for a hearth that's not affected by anything.
        BaseBonus = 0.365,
        -- this pulse is a fail safe to prevent players from getting the buff stuck on them for whatever reason.
        PulseFrequency = TimeSpan.FromSeconds(1),
    },
}


--[[

    -- Adjust vitality
    if ( Success(ServerSettings.Vitality.Chance.OnDeath) ) then
        SafeAdjustCurVitality(ServerSettings.Vitality.AdjustOnGhostResurrect, player)
    end

]]
