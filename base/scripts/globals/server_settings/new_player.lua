ServerSettings.NewPlayer = {
	InitiateSystemEnabled = true,
	InitiateDurationMinutes = 3 * 60,
	-- how long an initiate can be out of protection before initiate status is removed, set nil to disable.
	InitiateProtectionWarning = TimeSpan.FromSeconds(30),

	MagicalGuideEventTimeout = TimeSpan.FromSeconds(30),
	MagicalGuideEventMaxDistance = 10,

	-- amount of days a character must stick around before deleting them works.
	MinimumDeleteDays = 30,
}