ServerSettings.Criminal = {

	-- zones in-game that will have criminal consequences disabled
	DisableCriminalZones = {
	},
	-- Regions with this address will have criminal consequences disabled
	DisableCriminalRegionAddresses = {
		"TwoTowers"
    },

    -- if a player has >= this murder count, they are a murderer (permanent criminal, red name, etc)
    MurdererMurderCount = 5,

    -- how often murder counts expire
    MurderCountExpireTimer = TimeSpan.FromHours(24),

    -- how long before a criminal's head expires
    HeadExpire = TimeSpan.FromHours(10),
    
    -- how long does a player stay a temporary crimnal (grey) for
    TemporaryDuration = TimeSpan.FromMinutes(5),

    -- to prevent spam on blocking criminal actions
    MinimumBetweenNegativeWarnings = TimeSpan.FromSeconds(3),

    -- allow negative actions in protected areas
    AllowNegativeActionsInProtected = false,

    -- do guards attack murderer on site?
    GuardHostileMurderer = true,

    -- do guards attack temporary criminals (grey) on site?
    GuardHostileTempCriminal = true,

    IgnoreMurderPunishment = false,
}