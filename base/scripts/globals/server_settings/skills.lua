ServerSettings.Skills = {
	MustLearnSkills = false,
		
	PlayerSkillCap = {
		Single = 100,
		--MALEVOLENT MOD DJM ADDED
		Total = 800,
		--Total = 600,
	},

	-- multiplies all gain chances by this amount, set to 1 for no effect, lower for slower gains, higher for quicker gains.
	GainFactor = 1,
	-- upon a successful gain attempt, this amount will be added to the skill level.
	GainAmount = 0.1,
	-- the gain chance will be multiplied by this amount if they are attempting to gain a skill point they have gained before but used the artifical cap to lower.
	RegainBonusMultiplier = 3,
	-- the gain chance will be multiplied by this amount if they are under the effect of a power hour potion
	PowerHourMultiplier = 2,
	-- the gain chance for IncreaseSkillGain mobile effect
	IncreaseSkillGainMultiplier = 1.05,
	-- these are bonuses to skill gain, given the threshold criteria is met, GainAmount will be increased, respectively.
	LowerLevelGains = {
		-- skills under this level will always gain when they would gain.
		GuaranteedGainThreshold = 10,
		-- under this level, UpperThresholdGainAmount will be applied.
		UpperThreshold = 20,
		UpperThresholdGainAmount = 0.2,
		-- under this level, LowerThresholdGainAmount will be applied.
		LowerThreshold = 10,
		LowerThresholdGainAmount = 0.3,
		-- under this level skills gains will be easier (higher chance to gain)
		DifficultyThreshold = 20,
	},
	HigherLevelGains = {
		-- above this level, gains will become more difficult (lower chance to gain)
		
		--SCAN REMOVED DIFFICULTY THRESHOLD.
		DifficultyThreshold = 100
		--DifficultyThreshold = 80
	},
	AntiMacro = {
		-- this is per skill, prevents spam gains
		TimeSpanBetweenGains = TimeSpan.FromSeconds(4),
	}
}