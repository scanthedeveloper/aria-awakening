
WeaponAbilitiesData.Focus = {
    MobileEffect = "Focus",
    Stamina = 0,
    Instant = true,
    NoTarget = true,
    NoCombat = true,
    Cooldown = TimeSpan.FromSeconds(2),
    Action = {
        DisplayName = "Focus",
        Tooltip = "Enter a trance and quickly regenerate mana.",
        Icon = "Thunder Storm",
        Enabled = true
    }
}

WeaponAbilitiesData.Tame = {
    MobileEffect = "Tame",
    Stamina = 0,
    Instant = true,
    QueueTarget = "Obj",
    NoCombat = true,
    Cooldown = TimeSpan.FromSeconds(2),
    Action = {
        DisplayName = "Tame",
        Tooltip = "Tame a creature to become your pet.",
        Icon = "tamecreature",
        Enabled = true
    }
}

WeaponAbilitiesData.Play = {
    MobileEffect = "Play",
    Stamina = 0,
    Instant = true,
    NoTarget = true,
    --QueueTarget = "Obj",
    NoCombat = true,
    Cooldown = TimeSpan.FromSeconds(60),
    Action = {
        DisplayName = "Play",
        Tooltip = "Make sweet, sweet music.",
        Icon = "Bard_Songs",
        Enabled = true
    }
}

WeaponAbilitiesData.Solo = {
    MobileEffect = "Solo",
    Stamina = 5,
    Instant = true,
    NoTarget = true,
    --AllowCloaked = true,
    --QueueTarget = "Obj",
    NoCombat = true,
    Cooldown = TimeSpan.FromSeconds(5),
    Action = {
        DisplayName = "Solo",
        Tooltip = "Attempt to perform a flurry.",
        Icon = "Phantom",
        Enabled = true
    }
}

--[[
WeaponAbilitiesData.SummonWisp = {
    MobileEffect = "SummonWisp",
    Stamina = 0,
    Instant = true,
    NoTarget = true,
    NoCombat = true,
    Cooldown = TimeSpan.FromSeconds(6),
    Action = {
        DisplayName = "Summon Wisps",
        Tooltip = "Summons 1-4 wisps to surround you, number of wisps is based on your summoning skill.",
        Icon = "Sorcerer_Wisp",
        Enabled = true
    }
}
]]

WeaponAbilitiesData.ConsumeWisp = {
    MobileEffect = "ConsumeWisp",
    Stamina = 0,
    Instant = true,
    NoTarget = true,
    NoCombat = true,
    Cooldown = TimeSpan.FromSeconds(60.0),
    Action = {
        DisplayName = "Consume Wisp",
        Tooltip = "Absorbs a wisp into the caster granting 50 health points for each wisp consumed.",
        Icon = "Regrowth_purple",
        Enabled = true
    }
}
