
WeaponAbilitiesData.Stun = {
    TargetMobileEffect = "Stun",
    TargetMobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(1.5),
        PlayerDuration = TimeSpan.FromSeconds(1),
    },
    Stamina = 25,
    Action = {
        DisplayName = "Stun",
        Tooltip = "Stun your opponent for 1.5 seconds. Reduced to 1 second against players.",
        Icon = "Flame Mark",
        Enabled = true
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Cleave = {      
    MobileEffect = "Cleave",
    MobileEffectArgs = {
        Range = 5
    },
    Stamina = 30,
    Action = {
        DisplayName = "Cleave",
        Tooltip = "Damage all targets 4 yards in front of you.",
        Icon = "Cleave",
        Enabled = true
    },
    SkipHitAction = true
}

WeaponAbilitiesData.StunPunch = {
    MobileEffect = "GeneralEffect",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
        --SCAN ADDED
        VisualEffects = {
            "BuffEffect_D"
        },
        SoundEffects = {
            --"event:/character/combat_abilities/pommel"
        },
        AttackEffects = {
            "jump_attack",
        }
    },
    TargetMobileEffect = "Daze",
    TargetMobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(4),
    },
    Stamina = 25,
    Action = {
        DisplayName = "Stun Punch",
        Tooltip = "Daze your target for up to 4 seconds.",
        Icon = "Fire Shackles",
        Enabled = true,
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Concus = {
    MobileEffect = "GeneralEffect",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
        --SCAN ADDED
        VisualEffects = {
            "BuffEffect_D"
        },
        SoundEffects = {
            "event:/character/combat_abilities/male_shout",
        },
        AttackEffects = {
            "wideswept_attack",
        }
    },
    TargetMobileEffect = "Concus",
    TargetMobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(5),
        Modifier = -0.5
    },
    Stamina = 25,
    Action = {
        DisplayName = "Concus",
        Tooltip = "Your opponent's mana is reduced by 50% for 5 seconds.",
        Icon = "Sure Strike",
        Enabled = true,
    }
}

WeaponAbilitiesData.FollowThrough = {
    MobileEffect = "GeneralEffect",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
        VisualEffects = {
            "BuffEffect_H"
        },
        SoundEffects = {
            "event:/character/combat_abilities/male_shout",
        },
        AttackEffects = {
            "followthrough",
        }
    },
    TargetMobileEffect = "AdjustStamina",
    TargetMobileEffectArgs = {
        Amount = -40,
    },
    Stamina = 25,
    Action = {
        DisplayName = "Follow Through",
        Tooltip = "Remove 40 stamina from your opponent.",
        Icon = "Shield Breaker",
        Enabled = true,
    },
}

WeaponAbilitiesData.Eviscerate = {
    MobileEffect = "GeneralEffect",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
        VisualEffects = {
            "BuffEffect_M"
        },
        SoundEffects = {
            "event:/character/combat_abilities/male_shout",
        },
        AttackEffects = {
            "impale",
        }
    },  
    TargetMobileEffect = "Eviscerate",
    TargetMobileEffectArgs = {
        Duration = TimeSpan.FromMilliseconds(1500),
        WeaponDamageModifier = 1.35
    },
    Stamina = 25,
    Action = {
        DisplayName = "Eviscerate",
        Tooltip = "Cause 35% additional weapon damage 1.5 seconds after a strike.",
        Icon = "Sap2",
        Enabled = true
    }
}

WeaponAbilitiesData.Bleed = {       
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
    },
    
    TargetMobileEffect = "Bleed",
    TargetMobileEffectArgs = {
        PulseFrequency = TimeSpan.FromSeconds(1),
        PulseMax = 6,
        DamageMin = 40,
        DamageMax = 60,
    },
    
    Stamina = 25,
    Action = {
        DisplayName = "Bleed",
        Tooltip = "Cause up to 40-60 damage a second for 5 seconds.",
        Icon = "Shred",
        Enabled = true,
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Dismount = {        
    TargetMobileEffect = "Dismount",
    Stamina = 20,
    Action = {
        DisplayName = "Dismount",
        Tooltip = "Remove your foe from his mount. Cannot use while mounted.",
        Icon = "Revenge2",
        Enabled = true
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Overpower = {
    MobileEffect = "Overpower",
    MobileEffectArgs = {
        AttackModifier = 0.50,
        },
    Stamina = 25,
    Action = {
        DisplayName = "Overpower",
        Tooltip = "Increase your attack by 50%.",
        Icon = "Whirlwind",
        Enabled = true,
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Stab = {
    MobileEffect = "Stab",
    MobileEffectArgs = {
        AttackModifier = 0.50,
        StealthAttackModifier = 2,
    },
    Stamina = 25,
    Action = {
        DisplayName = "Stab",
        Tooltip = "Attempt to stab your target with a 50% attack bonus. Stabbing from stealth rewards 200% attack bonus.",
        Icon = "Fatal Strike",
        Enabled = true,
    },
    SkipHitAction = true,
    AllowCloaked = true,
}

WeaponAbilitiesData.MortalStrike = {
    MobileEffect = "MortalStrike",
    MobileEffectArgs = {
        AttackModifier = 0,
    },
    Stamina = 25,
    Action = {
        DisplayName = "Mortal Strike",
        Tooltip = "Causes target to be mortally struck. They cannot heal with bandages/magic.",
        Icon = "Deep Cuts",
        Enabled = true,
    },
    SkipHitAction = true
}

WeaponAbilitiesData.Sunder = {
    MobileEffect = "GeneralEffect",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
        VisualEffects = {
            "BuffEffect_M"
        },
        SoundEffects = {
            "event:/character/combat_abilities/male_shout",
        },
        AttackEffects = {
            "sunder",
        }
    },
    TargetMobileEffect = "Sunder",
    Stamina = 25,
    Action = {
        DisplayName = "Sunder",
        Tooltip = "Sunder target for up to 10 seconds. Next physical attack will ignore all equipped armor and clear the effect.",
        Icon = "Windshot",
        Enabled = true,
    },
    SkipHitAction = true
}

--CUSTOM WEAPON ABILITIES
--SCAN ADDED
WeaponAbilitiesData.DazeShot = {
    MobileEffect = "GeneralEffect",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(2),
        VisualEffects = {
            "BuffEffect_M"
        },
        SoundEffects = {
            --"event:/character/combat_abilities/male_shout",
        },
        AttackEffects = {
            "impale",
        }
    },
    TargetMobileEffect = "Daze",
    TargetMobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(4),
    },
    Stamina = 25,
    Action = {
        DisplayName = "Dazing Shot",
        Tooltip = "Daze your target for up to 4 seconds.",
        Icon = "Windshot",
        Enabled = true,
    },
    SkipHitAction = true
}

--SCAN ADDED
WeaponAbilitiesData.MarkedShot = {
    MobileEffect = "MarkedShot",
    MobileEffectArgs = {
        AttackModifier = 0.30,
    },
    Stamina = 20,
    Action = {
        DisplayName = "Marked Shot",
        Tooltip = "Increase your attack by 30%.",
        Icon = "Sharpshooter",
        Enabled = true,
    },
    SkipHitAction = true
}

--SCAN ADDED
WeaponAbilitiesData.WandFireball = {
    MobileEffect = "WandFireball",
    MobileEffectArgs = {
        --AttackModifier = 0.50,
        },
    Stamina = 20,
    Action = {
        DisplayName = "Fire Bolt",
        Tooltip = "Attack your enemy with a series of fire damage attacks.",
        Icon = "Imbue Fire",
        Enabled = true,
    },
    SkipHitAction = true
}

--SCAN ADDED
WeaponAbilitiesData.WandLightning = {
    MobileEffect = "WandLightning",
    MobileEffectArgs = {
        --AttackModifier = 0.50,
        },
    Stamina = 20,
    Action = {
        DisplayName = "Lightning Strike",
        Tooltip = "AOE Attacks all enemies in front of you with lightning damage.",
        Icon = "Imbue Lightning",
        Enabled = true,
    },
    SkipHitAction = true
}

--SCAN ADDED
WeaponAbilitiesData.WandHeal = {
    MobileEffect = "WandHeal",
    MobileEffectArgs = {
        --AttackModifier = 0.50,
        },
    Stamina = 20,
    Action = {
        DisplayName = "Empowerment",
        Tooltip = "Temporarily cause your heals to AOE heal party members for the next 15 seconds.",
        Icon = "heal",
        Enabled = true,
    },
    SkipHitAction = true
}

--SCAN ADDED
WeaponAbilitiesData.WandMeteor = {
    MobileEffect = "WandMeteor",
    MobileEffectArgs = {
        --AttackModifier = 0.50,
        },
    Stamina = 20,
    Action = {
        DisplayName = "Earth Spike",
        Tooltip = "Attack your enemies with the earth beneath their feet.",
        Icon = "Volcanic Eruption",
        Enabled = true,
    },
    SkipHitAction = true
}

--SCAN ADDED (BRAWLING)
WeaponAbilitiesData.Jawbreaker = {
    TargetMobileEffect = "Jawbreaker",
    TargetMobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(4),
        PlayerDuration = TimeSpan.FromSeconds(3),
    },
    Stamina = 25,
    Action = {
        DisplayName = "Jaw Breaker",
        Tooltip = "Damage & stun your opponent for 4 seconds. Attack has a chance to critical strike.",
        Icon = "drivingstrike",
        Enabled = true
    },
    SkipHitAction = true
}
