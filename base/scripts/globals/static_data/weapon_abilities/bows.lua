WeaponAbilitiesData.FireArrowShort = {      
    MobileEffect = "FireArrow",
    Instant = true,
    CancelOnSpellPrime = true,
    NeedLOS = true,
    AllowCloaked = true,
    NeedArrows = true,
    --NoResetSwing = true,
    --Cooldown = TimeSpan.FromSeconds(2),
    NoCooldown = true,
    MobileEffectArgs = { NotchArrowTime = 4000, },
    Stamina = 2,
    Action = {
        DisplayName = "Fire Arrow",
        Tooltip = "Shoot your bow.",
        Icon = "Sharpshooter",
        Enabled = true
    }
}
WeaponAbilitiesData.FireArrowLong = {      
    MobileEffect = "FireArrow",
    Instant = true,
    CancelOnSpellPrime = true,
    NeedLOS = true,
    NeedArrows = true,
    --NoResetSwing = true,
    --Cooldown = TimeSpan.FromSeconds(3),
    NoCooldown = true,
    MobileEffectArgs = { NotchArrowTime = 5000, },
    Stamina = 2,
    Action = {
        DisplayName = "Fire Arrow",
        Tooltip = "Shoot your bow.",
        Icon = "Sharpshooter",
        Enabled = true
    }
}
WeaponAbilitiesData.FireArrowWar = {      
    MobileEffect = "FireArrow",
    Instant = true,
    CancelOnSpellPrime = true,
    NeedLOS = true,
    NeedArrows = true,
    --NoResetSwing = true,
    --Cooldown = TimeSpan.FromSeconds(4),
    NoCooldown = true,
    MobileEffectArgs = { NotchArrowTime = 6000, },
    Stamina = 2,
    Action = {
        DisplayName = "Fire Arrow",
        Tooltip = "Shoot your bow.",
        Icon = "Sharpshooter",
        Enabled = true
    }
}

--CUSTOM ARCHERY ABILITIES
--SCAN ADDED 
--180 Damage
--184 Damage
WeaponAbilitiesData.DoubleShot = {      
    MobileEffect = "DoubleShot",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(.5),
    },
    Stamina = 20,
    Cooldown =  TimeSpan.FromSeconds(3),
    Action = {
        DisplayName = "Double Shot",
        Tooltip = "Shoot your bow, with a follow up shot a half second later.",
        Icon = "Multishot",
        Enabled = true
    }
}
--SCAN ADDED 
--167 Damage
--165 Damage
--172 Damage
WeaponAbilitiesData.ExplosiveShot = {      
    MobileEffect = "ExplosiveShot",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(.5),
    },
    Stamina = 20,
    Cooldown =  TimeSpan.FromSeconds(4),
    Action = {
        DisplayName = "Explosive Shot",
        Tooltip = "Shoot your bow, followed by a series of magic explosions at your enemy.",
        Icon = "Flame Whirl",
        Enabled = true
    }
}
--SCAN ADDED 
--179 Damage
--177 Damage
--200 Damage
WeaponAbilitiesData.FlamingArrow = {      
    TargetMobileEffect = "Ignite",
    TargetMobileEffectArgs = {
        PulseFrequency = TimeSpan.FromSeconds(.30),
        PulseMax = 6,
        MinDamage = 1,
        MaxDamage = 2,
    },
    Stamina = 20,
    Cooldown =  TimeSpan.FromSeconds(5),
    Action = {
        DisplayName = "Fire Shot",
        Tooltip = "Set your target on fire, damaging your opponent 6 times in 2 seconds.",
        Icon = "Burning Arrow",
        Enabled = true
    }
}
--SCAN ADDED 
--138 Damage
--128 Damage
--125 Damage
WeaponAbilitiesData.FrostShot = {      
    MobileEffect = "FrostShot",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(.5),
    },
    Stamina = 20,
    Cooldown =  TimeSpan.FromSeconds(4),
    Action = {
        DisplayName = "Frost Shot",
        Tooltip = "Shoot your bow, causing your enemy to become disoriented and slowed.",
        Icon = "Arrow Storm",
        Enabled = true
    }
}
--SCAN ADDED 
WeaponAbilitiesData.HuntersMark = {      
    TargetMobileEffect = "HuntersMark",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(5),
        Modifier = 0.75,
    },
    Stamina = 20,
    Action = {
        DisplayName = "Hunter's Mark",
        Tooltip = "Mark your target for 5 seconds, increasing bow damage by 75%.",
        Icon = "impale",
        Enabled = true
    }
}
--SCAN ADDED 
WeaponAbilitiesData.PoisonShot = {      
    TargetMobileEffect = "VenomousAffliction",
    TargetMobileEffectArgs = {
        PulseFrequency = TimeSpan.FromSeconds(.75),
        PulseMax = 3,
        MinDamage = 1,
        MaxDamage = 25,
    },
    Stamina = 30,
    Action = {
        DisplayName = "Swarm Shot",
        Tooltip = "Swarm your target.",
        Icon = "Poison Arrow",
        Enabled = true
    }
}
--SCAN ADDED 
WeaponAbilitiesData.Power = {
    MobileEffect = "Power",
    MobileEffectArgs = {
        AttackModifier = .50,
    },
    Stamina = 75,
    Action = {
        DisplayName = "Overdraw",
        Tooltip = "Shoots a single powerful shot for 50% additional weapon damage, exhausting you.",
        Icon = "Windshot",
        Enabled = true,
    },
    SkipHitAction = true
}
--SCAN ADDED 
WeaponAbilitiesData.AutoFireArrowShort = {      
    MobileEffect = "AutoFireArrow",
    Instant = true,
    NeedLOS = true,
    NoResetSwing = true,
    Cooldown =  TimeSpan.FromSeconds(1),
    MobileEffectArgs = {
        PulseDuration = WeaponAbilitiesData.FireArrowShort.MobileEffectArgs.NotchArrowTime,
        WeaponAbility = WeaponAbilitiesData.FireArrowShort,
    },
    Stamina = 4,
    Action = {
        DisplayName = "Auto Fire Arrow",
        Tooltip = "Toggle shooting your bow continuously.",
        Icon = "Multishot",
        Enabled = true
    }
}
--SCAN ADDED 
WeaponAbilitiesData.AutoFireArrowLong = {      
    MobileEffect = "AutoFireArrow",
    Instant = true,
    NeedLOS = true,
    NoResetSwing = true,
    Cooldown =  TimeSpan.FromSeconds(1),
    MobileEffectArgs = {
        PulseDuration = WeaponAbilitiesData.FireArrowLong.MobileEffectArgs.NotchArrowTime,
        WeaponAbility = WeaponAbilitiesData.FireArrowLong,
    },
    Stamina = 4,
    Action = {
        DisplayName = "Auto Fire Arrow",
        Tooltip = "Toggle shooting your bow continuously.",
        Icon = "Multishot",
        Enabled = true
    }
}
--SCAN ADDED 
WeaponAbilitiesData.AutoFireArrowWar = {      
    MobileEffect = "AutoFireArrow",
    Instant = true,
    NeedLOS = true,
    NoResetSwing = true,
    Cooldown =  TimeSpan.FromSeconds(1),
    MobileEffectArgs = {
        PulseDuration = WeaponAbilitiesData.FireArrowWar.MobileEffectArgs.NotchArrowTime,
        WeaponAbility = WeaponAbilitiesData.FireArrowWar,
    },
    Stamina = 6,
    Action = {
        DisplayName = "Auto Fire Arrow",
        Tooltip = "Toggle shooting your bow continuously.",
        Icon = "Multishot",
        Enabled = true
    }
}