EquipmentStats = {
	BaseWeaponClass = {
		Fist = {
			Accuracy = 30,
			Critical = 0,
			Speed = 1.5,
			WeaponSkill = "BrawlingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		Dagger = {
			DisplayName = "Piercing Weapon",

			Accuracy = 0,
			Critical = 0,
			WeaponSkill = "PiercingSkill",
			WeaponDamageType = "Piercing",
			WeaponHitType = "Melee",
		},

		Sword = {
			DisplayName = "Slashing Weapon",

			Accuracy = 0,
			Critical = 0,
			WeaponSkill = "SlashingSkill",
			WeaponDamageType = "Slashing",
			WeaponHitType = "Melee",
		},

		Sword2H = {
			DisplayName = "Slashing Weapon",

			Accuracy = 30,
			Critical = 0,
			WeaponSkill = "SlashingSkill",
			WeaponDamageType = "Slashing",
			WeaponHitType = "Melee",
			TwoHandedWeapon = true,
		},

		Blunt = {
			DisplayName = "Bashing Weapon",

			Accuracy = 0,
			Critical = 0,
			WeaponSkill = "BashingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		Blunt2H = {
			DisplayName = "Bashing Weapon",

			Accuracy = 30,
			Critical = 0,
			WeaponSkill = "BashingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
			TwoHandedWeapon = true,
		},

		PoleArm = {
			DisplayName = "Lancing Weapon",

			Accuracy = 30,
			Critical = 0,
			WeaponSkill = "LancingSkill",
			WeaponDamageType = "Piercing",
			WeaponHitType = "Melee",
			TwoHandedWeapon = true,
		},

		Bow = {
			DisplayName = "Ranged Weapon",

			Accuracy = 30,
			Critical = 0,
			WeaponSkill = "ArcherySkill",
			WeaponDamageType = "Bow",
			WeaponHitType = "Ranged",
			TwoHandedWeapon = true,
			Range = 14,
		},

		Stave = {
			DisplayName = "Bashing Weapon",

			Accuracy = 0,
			Critical = 40,
			WeaponSkill = "BashingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
			TwoHandedWeapon = true,
		},
		
		Tool = {
			DisplayName = "Tool",

			Accuracy = 0,
			Critical = 0,
			WeaponSkill = "BashingSkill",
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		Crook = {
			DisplayName = "Crook",

			Accuracy = 0,
			Critical = 0,
			WeaponSkill = "AnimalTamingSkill",
			WeaponSkillGainsDisabled = true,
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		Instrument = {
			DisplayName = "Instrument",

			Accuracy = 0,
			Critical = 0,
			WeaponSkill = "MusicianshipSkill",
			WeaponSkillGainsDisabled = true,
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		Magic = {
			DisplayName = "Magic Weapon",

			Accuracy = 0,
			Critical = 0,
			--SCAN ADDED RANGE
			Range = 8,
			WeaponSkill = "MagerySkill",
			WeaponSkillGainsDisabled = true,
			WeaponDamageType = "Bashing",
			WeaponHitType = "Melee",
		},

		NoCombat = {
			-- To prevent exceptions since NoCombat was added late
			Accuracy = 0,
			Critical = 0,
			WeaponHitType = "Melee",
		}
	},

	-- DAB COMBAT CHANGES IMPLEMENT MIN SKILL FOR WEAPONS
	BaseWeaponStats = {
		BareHand = {
			WeaponClass = "Fist",
			Attack = 1,
			MinSkill = 0,
			Speed = 2.0,
			Variance = 0.05,
			PrimaryAbility = "Jawbreaker",
			SecondaryAbility = "StunPunch",
			SFXDir = "hand",
			delaySeconds = 0.15,
		},
		-- Daggers
		Dagger = {
			WeaponClass = "Dagger",
			Attack = 12,
			Variance = 0.02,
			MinSkill = 0,
			Speed = 4.5,
			PrimaryAbility = "StunPunch",
			SecondaryAbility = "Bleed",
			SFXDir = "dagger",
			delaySeconds = 0.25,
		},
		Kryss = {
			WeaponClass = "Dagger",
			Variance = 0.02,
			Attack = 11,
			Speed = 4.0,
			MinSkill = 0,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Bleed",
			SFXDir = "dagger",
			delaySeconds = 0.25,
		},
		Poniard = {
			WeaponClass = "Dagger",
			Variance = 0.02,
			Attack = 14,
			Speed = 4.75,
			MinSkill = 0,
			PrimaryAbility = "MortalStrike",
			SecondaryAbility = "Bleed",
			SFXDir = "dagger",
			delaySeconds = 0.25,
		},
		BoneDagger = {
			WeaponClass = "Dagger",
			Variance = 0.02,
			Attack = 12,
			Speed = 4.25,
			MinSkill = 0,
			PrimaryAbility = "Stab",
			SecondaryAbility = "Poison",
			SFXDir = "dagger",
			delaySeconds = 0.25,
		},
		-- Blunts
		Mace = {
			WeaponClass = "Blunt",
			Variance = 0.05,
			Attack = 13,
			MinSkill = 0,
			Speed = 4.75,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "MortalStrike",
			SFXDir = "warhammer",
			delaySeconds = 0.25,
		},
		Hammer = {
			WeaponClass = "Blunt",
			Variance = 0.05,
			Attack = 16,
			MinSkill = 0,
			Speed = 5,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "MortalStrike",
			SFXDir = "hammer",
			delaySeconds = 0.25,
		},
		Maul = {
			WeaponClass = "Blunt",
			Variance = 0.05,
			Attack = 16,
			MinSkill = 0,
			Speed = 5.25,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Bleed",
			SFXDir = "warhammer",
			delaySeconds = 0.25,
		},
		WarMace = {
			WeaponClass = "Blunt",
			Variance = 0.05,
			Attack = 18,
			MinSkill = 0,
			Speed = 5.5,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "FollowThrough",
			SFXDir = "warhammer",
			delaySeconds = 0.25,
		},
		-- 2H Blunts
		Quarterstaff = {
			WeaponClass = "Blunt2H",
			Variance = 0.20,
			Attack = 11,
			MinSkill = 0,
			Speed = 4.25,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Concus",
			TwoHandedWeapon = true,
			SFXDir = "staff",
			delaySeconds = 0.15,
		},
		Warhammer = {
			WeaponClass = "Blunt2H",
			Variance = 0.20,
			Attack = 23,
			MinSkill = 0,
			Speed = 5,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "FollowThrough",
			TwoHandedWeapon = true,
			SFXDir = "warhammer",
			delaySeconds = 0.25,
		},
		-- Polearms
		Warfork = {
			WeaponClass = "PoleArm",
			Variance = 0.10,
			Attack = 19,
			MinSkill = 0,
			Speed = 5.5,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Bleed",
			TwoHandedWeapon = true,
			SFXDir = "dagger",
			delaySeconds = 0.15,
		},
		Voulge = {
			WeaponClass = "PoleArm",
			Variance = 0.10,
			Attack = 17,
			MinSkill = 0,
			Speed = 5.25,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Cleave",
			TwoHandedWeapon = true,
			SFXDir = "dagger",
			delaySeconds = 0.15,
		},
		Spear = {
			WeaponClass = "PoleArm",
			Variance = 0.10,
			Attack = 16,
			MinSkill = 0,
			Speed = 5,
			PrimaryAbility = "Stab",
			SecondaryAbility = "MortalStrike",
			TwoHandedWeapon = true,
			SFXDir = "dagger",
			delaySeconds = 0.15,
		},
		Halberd = {
			WeaponClass = "PoleArm",
			Variance = 0.10,
			Attack = 23,
			MinSkill = 0,
			Speed = 6.5,
			PrimaryAbility = "Overpower",
			--SCAN UPDATED
			SecondaryAbility = "Cleave",
			TwoHandedWeapon = true,
			SFXDir = "dagger",
			delaySeconds = 0.15,
		},
		Scythe = {
			WeaponClass = "PoleArm",
			Variance = 0.10,
			Attack = 23,
			MinSkill = 0,
			Speed = 6.5,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Bleed",
			TwoHandedWeapon = true,
			SFXDir = "sword",
			delaySeconds = 0.25,
		},
		-- Bows
		Shortbow = {
			WeaponClass = "Bow",
			Variance = 0.20,
			Attack = 12,
			MinSkill = 0,
			Speed = 4,
			PrimaryAbility = "FireArrowShort",
            -- SCAN CHANGED
			SecondaryAbility = "MarkedShot",
            --SecondaryAbility = "Stab",
			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
		Longbow = {
			WeaponClass = "Bow",
			Variance = 0.20,
			Attack = 17,
			MinSkill = 0,
			Speed = 5,
			PrimaryAbility = "FireArrowLong",
            -- SCAN CHANGED
			SecondaryAbility = "MarkedShot"
            --SecondaryAbility = "MortalStrike"
,			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
		Warbow = {
			WeaponClass = "Bow",
			Variance = 0.20,
			Attack = 24,
			MinSkill = 0,
			Speed = 6,
			PrimaryAbility = "FireArrowWar",
            -- SCAN CHANGED
			SecondaryAbility = "MarkedShot",
			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
		SavageBow = {
			WeaponClass = "Bow",
			Variance = 0.20,
			Attack = 24,
			MinSkill = 0,
			Speed = 6,
			PrimaryAbility = "FireArrowWar",
			SecondaryAbility = "Bleed",
			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
		BoneBow = {
			WeaponClass = "Bow",
			Variance = 0.20,
			Attack = 24,
			MinSkill = 0,
			Speed = 6,
			PrimaryAbility = "FireArrowWar",
			SecondaryAbility = "Overpower",
			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
        --SCAN ADDED
		--Custom Bows 
        AutoBow = {
			WeaponClass = "Bow",
			Variance = 0.05,
			Attack = 15,
			MinSkill = 35,
			Speed = 5.5,
			PrimaryAbility = "AutoFireArrowShort",
            SecondaryAbility = "MarkedShot",
			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
		 --SCAN ADDED
		Dazebow = {
			WeaponClass = "Bow",
			Variance = 0.20,
			Attack = 17,
			MinSkill = 0,
			Speed = 5,
			PrimaryAbility = "FireArrowLong",
			SecondaryAbility = "DazeShot",
			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
		 --SCAN ADDED
		DoubleShotBow = {
			WeaponClass = "Bow",
			Variance = 0.20,
			Attack = 20,
			MinSkill = 0,
			Speed = 5,
			PrimaryAbility = "FireArrowWar",
            -- SCAN CHANGED
			SecondaryAbility = "DoubleShot",
			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
		 --SCAN ADDED
		 ExplosiveShotBow = {
			WeaponClass = "Bow",
			Variance = 0.20,
			Attack = 20,
			MinSkill = 0,
			Speed = 6,
			PrimaryAbility = "FireArrowWar",
            -- SCAN CHANGED
			SecondaryAbility = "ExplosiveShot",
			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
		--SCAN ADDED
		FireShotBow = {
			WeaponClass = "Bow",
			Variance = 0.20,
			Attack = 20,
			MinSkill = 0,
			Speed = 5.5,
			PrimaryAbility = "FireArrowWar",
            -- SCAN CHANGED
			SecondaryAbility = "FlamingArrow",
			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
		 --SCAN ADDED
		 FrostShotBow = {
			WeaponClass = "Bow",
			Variance = 0.20,
			Attack = 18,
			MinSkill = 0,
			Speed = 5.25,
			PrimaryAbility = "FireArrowWar",
            -- SCAN CHANGED
			SecondaryAbility = "FrostShot",
			TwoHandedWeapon = true,
			SFXDir = "bow",
			delaySeconds = 0.1,
		},
		
		-- 1H Swords
		Longsword = {
			WeaponClass = "Sword",
			Variance = 0.05,
			Attack = 13,
			MinSkill = 0,
			Speed = 4.75,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Concus",
			SFXDir = "sword",
			delaySeconds = 0.3,
		},
		Broadsword = {
			WeaponClass = "Sword",
			Variance = 0.05,
			Attack = 18,
			MinSkill = 0,
			Speed = 5.5,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Cleave",
			SFXDir = "sword",
			delaySeconds = 0.3,
		},
		Saber = {
			WeaponClass = "Sword",
			Variance = 0.05,
			Attack = 17,
			MinSkill = 0,
			Speed = 5.25,
			PrimaryAbility = "Stab",
			SecondaryAbility = "MortalStrike",
			SFXDir = "sword",
			delaySeconds = 0.3,
		},
		Katana = {
			WeaponClass = "Sword",
			Variance = 0.05,
			Attack = 12,
			MinSkill = 0,
			Speed = 4.5,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Bleed",
			SFXDir = "sword",
			delaySeconds = 0.3,
		},
		BattleAxe = {
			WeaponClass = "Sword",
			Variance = 0.20,
			Attack = 12,
			MinSkill = 0,
			Speed = 5.5,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Bleed",
			SFXDir = "axe_small",
			delaySeconds = 0.3,
		},
		-- 2H Swords
		LargeAxe = {
			WeaponClass = "Sword2H",
			Variance = 0.20,
			Attack = 21,
			MinSkill = 0,
			Speed = 6.5,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "Bleed",
			TwoHandedWeapon = true,
			SFXDir = "axe_large",
			delaySeconds = 0.3,
		},
		GreatAxe = {
			WeaponClass = "Sword2H",
			Variance = 0.20,
			Attack = 23,
			MinSkill = 0,
			Speed = 6.5,
			PrimaryAbility = "Overpower",
			SecondaryAbility = "MortalStrike",
			TwoHandedWeapon = true,
			SFXDir = "axe_large",
			delaySeconds = 0.3,
		},
		Claymore = {
			WeaponClass = "Sword2H",
			Variance = 0.05,
			Attack = 22,
			MinSkill = 50,
			Speed = 6,
			PrimaryAbility = "Cleave",
			SecondaryAbility = "Bleed",
			TwoHandedWeapon = true,
			SFXDir = "axe_large",
			delaySeconds = 0.3,
		},
		-- Tools
		Hatchet = {
			WeaponClass = "Sword2H",
			Attack = 16,
			MinSkill = 0,
			PrimaryAbility = "Lumberjack",
			--SCAN CHANGED false
			TwoHandedWeapon = false,
			Speed = 5,
			SFXDir = "axe_small",
			delaySeconds = 0.3,
		},
		MiningPick = {
			WeaponClass = "Blunt",
			Attack = 16,
			MinSkill = 0,
			PrimaryAbility = "Mine",
			--SCAN CHANGED false
			TwoHandedWeapon = false,
			Speed = 5,
			SFXDir = "sword",
			delaySeconds = 0.3,
		},
		FishingRod = {
			WeaponClass = "Tool",
			Attack = 3,
			MinSkill = 0,
			PrimaryAbility = "Fish",
			TwoHandedWeapon = true,
			Speed = 6,
			SFXDir = "staff",
			delaySeconds = 0.3,
		},
		HuntingKnife = {
			WeaponClass = "Dagger",
			Attack = 16,
			MinSkill = 0,
			PrimaryAbility = "HuntingKnife",
			Speed = 4,
			SFXDir = "dagger",
			delaySeconds = 0.3,
		},
		Crook = {
			WeaponClass = "Crook",
			Attack = 6,
			MinSkill = 0,
			PrimaryAbility = "Tame",
			Speed = 3,
			SFXDir = "warhammer",
			delaySeconds = 0.3,
		},
		Shovel = {
			WeaponClass = "Tool",
			Attack = 7,
			MinSkill = 0,
			PrimaryAbility = "Dig",
			Speed = 3,
			SFXDir = "staff",
			delaySeconds = 0.3,
		},
		-- mage weapons
		MagicStaff = {
			WeaponClass = "Magic",
			Variance = 0.20,
			Attack = 12,
			MinSkill = 0,
			Speed = 6.5,
			TwoHandedWeapon = true,
			PrimaryAbility = "Focus",
			SecondaryAbility = "WandMeteor",
			SFXDir = "staff",
			delaySeconds = 0.15,
		},
		LightningStaff = {
			WeaponClass = "Magic",
			Variance = 0.20,
			Attack = 12,
			MinSkill = 0,
			Speed = 6,
			TwoHandedWeapon = true,
			PrimaryAbility = "Focus",
			SecondaryAbility = "WandLightning",
			SFXDir = "staff",
			delaySeconds = 0.15,
		},
		FireStaff = {
			WeaponClass = "Magic",
			Variance = 0.20,
			Attack = 12,
			MinSkill = 0,
			Speed = 6.25,
			TwoHandedWeapon = true,
			PrimaryAbility = "Focus",
			SecondaryAbility = "WandFireball",
			SFXDir = "staff",
			delaySeconds = 0.15,
		},
		HealStaff = {
			WeaponClass = "Magic",
			Variance = 0.20,
			Attack = 12,
			MinSkill = 0,
			Speed = 6,
			TwoHandedWeapon = true,
			PrimaryAbility = "Focus",
			SecondaryAbility = "WandHeal",
			SFXDir = "staff",
			delaySeconds = 0.15,
		},
		MagicWand = {
			WeaponClass = "Magic",
			Variance = 0.05,
			Attack = 10,
			MinSkill = 0,
			Speed = 5.5,
			PrimaryAbility = "Focus",
			SecondaryAbility = "WandMeteor",
			SFXDir = "staff",
			delaySeconds = 0.25,
		},
		LightningWand = {
			WeaponClass = "Magic",
			Variance = 0.20,
			Attack = 10,
			MinSkill = 0,
			Speed = 5,
			TwoHandedWeapon = false,
			PrimaryAbility = "Focus",
			SecondaryAbility = "WandLightning",
			SFXDir = "staff",
			delaySeconds = 0.15,
		},
		FireWand = {
			WeaponClass = "Magic",
			Variance = 0.20,
			Attack = 10,
			MinSkill = 0,
			Speed = 5,
			TwoHandedWeapon = false,
			PrimaryAbility = "Focus",
			SecondaryAbility = "WandFireball",
			SFXDir = "staff",
			delaySeconds = 0.15,
		},
		HealWand = {
			WeaponClass = "Magic",
			Variance = 0.20,
			Attack = 10,
			MinSkill = 0,
			Speed = 5,
			TwoHandedWeapon = false,
			PrimaryAbility = "Focus",
			SecondaryAbility = "WandHeal",
			SFXDir = "staff",
			delaySeconds = 0.15,
		},
		Spellbook = {
			WeaponClass = "Magic",
			Attack = 1,
			Speed = 3,
			MinSkill = 0,
			SFXDir = "staff",
			delaySeconds = 0.3,
		},
		
		SorcererWand = {
			WeaponClass = "Magic",
			Attack = 1,
			MinSkill = 0,
			PrimaryAbility = "ConsumeWisp",
			--SecondaryAbility = "SummonWisp",
			Speed = 5,
			SFXDir = "staff",
			delaySeconds = 0.3,
		},

		-- Instruments
		Lute = {
			WeaponClass = "Instrument",
			Attack = 3,
			MinSkill = 0,
			PrimaryAbility = "Play",
			SecondaryAbility = "Solo",
			TwoHandedWeapon = true,
			Speed = 6.5,
			ParameterName = "lute",
			SFXDir = "staff",
			delaySeconds = 0.3,
		},

		Drum = {
			WeaponClass = "Instrument",
			Attack = 3,
			MinSkill = 0,
			PrimaryAbility = "Play",
			SecondaryAbility = "Solo",
			TwoHandedWeapon = true,
			Speed = 7.0,
			ParameterName = "drum",
			SFXDir = "staff",
			delaySeconds = 0.3,
		},

		Flute = {
			WeaponClass = "Instrument",
			Attack = 3,
			MinSkill = 0,
			PrimaryAbility = "Play",
			SecondaryAbility = "Solo",
			TwoHandedWeapon = true,
			Speed = 5,
			ParameterName = "flute",
			SFXDir = "staff",
			delaySeconds = 0.3,
		},

		-- no combat
		Torch = {
			WeaponClass = "NoCombat",
			NoCombat = true,
			Attack = 0, -- to prevent exceptions (should not check these if weaponclass is NoCombat)
			MinSkill = 0, -- to prevent exceptions
			Speed = 0, -- to prevent exceptions
			SFXDir = "staff",
			delaySeconds = 0.3,
		}
	},

	BaseArmorClass = {
		Cloth = {
			Head = {
				StaRegenModifier = 0,
				ManaRegenModifier = 1,
			},
			Chest = {
				StaRegenModifier = 0,
				ManaRegenModifier = 1,
			},
			Legs = {
				StaRegenModifier = 0,
				ManaRegenModifier = 1,
			},
		},

		Light = {
			Head = {
				StaRegenModifier = 0,
				ManaRegenModifier = 0.5,
			},
			Chest = {
				StaRegenModifier = 0,
				ManaRegenModifier = 0.5,
			},
			Legs = {
				StaRegenModifier = 0,
				ManaRegenModifier = 0.5,
			},
		},

		Heavy = {
			Head = {
				AgiBonus = -3,
				StaRegenModifier = 0,
				ManaRegenModifier = 0.1,
			},
			Chest = {
				AgiBonus = -3,
				StaRegenModifier = 0,
				ManaRegenModifier = 0.1,
			},
			Legs = {
				AgiBonus = -3,
				StaRegenModifier = 0,
				ManaRegenModifier = 0.1,
			},
		},
	},

	BaseArmorStats = {
		Natural = { --naked
			ArmorClass = "Cloth",
			Head = {
				ArmorRating = 10,
			},
			Chest = {
				ArmorRating = 12,
			},
			Legs = {
				ArmorRating = 13,
			}
		},

		VeryLight = { --naked
			ArmorClass = "Cloth",
			Head = {
				ArmorRating = 10,
			},
			Chest = {
				ArmorRating = 12,
			},
			Legs = {
				ArmorRating = 13,
			}
		},

		Clothing = { 
			ArmorClass = "Cloth",
			Head = {
				ArmorRating = 12,
			},
			Chest = {
				ArmorRating = 16,
			},
			Legs = {
				ArmorRating = 17,
			}
		},

		Padded = {
			ArmorClass = "Cloth",
			SoundType = "leather",
			Head = {
				ArmorRating = 13,
			},
			Chest = {
				ArmorRating = 18,
			},
			Legs = {
				ArmorRating = 18,
			}
		},
		Linen = {
			ArmorClass = "Cloth",
			SoundType = "leather",
			Head = {
				ArmorRating = 13,
			},
			Chest = {
				ArmorRating = 18,
			},
			Legs = {
				ArmorRating = 18,
			}
		},
		MageRobe = {
			ArmorClass = "Cloth",
			SoundType = "leather",
			Head = {
				ArmorRating = 13,
			},
			Chest = {
				ArmorRating = 18,
			},
			Legs = {
				ArmorRating = 18,
			}
		},

		Leather = {
			ArmorClass = "Light",
			SoundType = "leather",
			Head = {
				ArmorRating = 15,
			},
			Chest = {
				ArmorRating = 21,
			},
			Legs = {
				ArmorRating = 21,
			}
		},

		Bone = {
			ArmorClass = "Light",
			SoundType = "leather",
			Head = {
				ArmorRating = 15,
			},
			Chest = {
				ArmorRating = 21,
			},
			Legs = {
				ArmorRating = 21,
			}
		},

		Hardened = {
			ArmorClass = "Light",
			SoundType = "leather",
			Head = {
				ArmorRating = 15,
			},
			Chest = {
				ArmorRating = 21,
			},
			Legs = {
				ArmorRating = 21,
			}
		},

		Assassin = {
			ArmorClass = "Light",
			SoundType = "leather",
			Head = {
				ArmorRating = 15,
			},
			Chest = {
				ArmorRating = 21,
			},
			Legs = {
				ArmorRating = 21,
			}
		},

		Brigandine = {
			ArmorClass = "Heavy",
			SoundType = "chainmail",			
			Head = {
				AgiBonus = -1,
				ArmorRating = 17,
			},
			Chest = {
				AgiBonus = -1,
				ArmorRating = 24,
			},
			Legs = {
				AgiBonus = -1,
				ArmorRating = 24,
			}
		},

		Chain = {
			ArmorClass = "Heavy",
			SoundType = "chainmail",
			AgiBonus = -1,
			Head = {
				AgiBonus = -1,
				ArmorRating = 16,
			},
			Chest = {
				AgiBonus = -1,
				ArmorRating = 22,
			},
			Legs = {
				AgiBonus = -1,
				ArmorRating = 22,
			}
		},

		Scale = {
			ArmorClass = "Heavy",
			SoundType = "plate",
			Head = {
				AgiBonus = -2,
				ArmorRating = 17,
			},
			Chest = {
				AgiBonus = -2,
				ArmorRating = 23,
			},
			Legs = {
				AgiBonus = -2,
				ArmorRating = 23,
			}
		},
		Plate = {
			ArmorClass = "Heavy",
			SoundType = "plate",
			Head = {
				AgiBonus = -3,
				ArmorRating = 17,
			},
			Chest = {
				AgiBonus = -3,
				ArmorRating = 24,
			},
			Legs = {
				AgiBonus = -3,
				ArmorRating = 24,
			}
		},
		FullPlate = {
			ArmorClass = "Heavy",
			SoundType = "plate",
			Head = {
				AgiBonus = -3,
				ArmorRating = 17,
			},
			Chest = {
				AgiBonus = -3,
				ArmorRating = 24,
			},
			Legs = {
				AgiBonus = -3,
				ArmorRating = 24,
			}
		},
	},

	BaseShieldStats = {
		--SCAN Craftable Shields
		SmallShield = {
			BaseBlockChance = 0,
			ArmorRating = 25,
			MinSkill = 0,
		},
		Buckler = {
			BaseBlockChance = 0,
			ArmorRating = 30,
			MinSkill = 20,
		},
		BoneShield = {
			BaseBlockChance = 0,
			ArmorRating = 35,
			MinSkill = 20,
		},
		SpikedShield = {
			BaseBlockChance = 0,
			ArmorRating = 35,
			MinSkill = 30,
		},
		CurvedShield = {
			BaseBlockChance = 1,
			ArmorRating = 40,
			MinSkill = 40,
		},
		KiteShield = {
			BaseBlockChance = 2,
			ArmorRating = 45,
			MinSkill = 50,
		},
		RoundShield = {
			BaseBlockChance = 3,
			ArmorRating = 50,
			MinSkill = 60,
		},
		OrnateShield = {
			BaseBlockChance = 4,
			ArmorRating = 55,
			MinSkill = 70,
		},
		HeaterShield = {
			BaseBlockChance = 5,
			ArmorRating = 60,
			MinSkill = 80,
		},
		--SCAN Magic Shields
		MarauderShield = {
			BaseBlockChance = 0,
			ArmorRating = 35,
			MinSkill = 0,
		},
		LargeShield = {
			BaseBlockChance = 0,
			ArmorRating = 35,
			MinSkill = 0,
		},
		DwarvenShield = {
			BaseBlockChance = 0,
			ArmorRating = 35,
			MinSkill = 0,
		},
		Silence = {
			BaseBlockChance = 2,
			ArmorRating = 45,
			MinSkill = 50,
		},
		DragonGuard = {
			BaseBlockChance = 2,
			ArmorRating = 45,
			MinSkill = 50,
		},
		Temper = {
			BaseBlockChance = 0,
			ArmorRating = 35,
			MinSkill = 0,
		},
		Guardian = {
			BaseBlockChance = 0,
			ArmorRating = 35,
			MinSkill = 0,
		},
	}
}

ArrowTypes ={
	"Arrows",
	"FeatheredArrows",
	"AshArrows",
	"BlightwoodArrows",
}

ArrowTypesNames ={
	Arrows = "Arrows",
	FeatheredArrows = "Feathered Arrows",
	AshArrows = "Ash Arrows",
	BlightwoodArrows = "Blightwood Arrows",
}

ArrowTypeData = {
	Arrows = {
		DamageBonus = 0,
		Name = "Arrows",
	},
	FeatheredArrows = {
		DamageBonus = 1,
		Name = "Feathered Arrows",
	},
	AshArrows = {
		DamageBonus = 3,
		Name = "Ash Arrows",
	},
	BlightwoodArrows = {
		DamageBonus = 5,
		Name = "Blightwood Arrows",
	},
}

JewelryTypes = {
	"RubyFlawed",
	"RubyImperfect",
	"RubyPerfect",
	"SapphireFlawed",
	"SapphireImperfect",
	"SapphirePerfect",
	"TopazFlawed",
	"TopazImperfect",
	"TopazPerfect",
}

JewelryTypeData = {
	RubyFlawed = {
		Con = 1,
		Health = 10,
	},
	RubyImperfect = {
		Con = 3,
		Health = 30,
	},
	RubyPerfect = {
		Con = 5,
		Health = 50,
	},
	SapphireFlawed = {
		Wis = 1,
	},
	SapphireImperfect = {
		Wis = 3,
	},
	SapphirePerfect = {
		Wis = 5,
	},
	TopazFlawed = {
		Will = 1,
	},
	TopazImperfect = {
		Will = 3,
	},
	TopazPerfect = {
		Will = 5,
	},
}