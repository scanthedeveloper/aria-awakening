ParagraphBreak = "\n      "

-- Order and amount of entries can be changed, but renaming TomeIds will result in lost data.
-- Order of collections cannot be changed, Tomebooks use the index of AllTomeCollections.
AllTomeCollections = {
	{"Towns of Celador", {
		"EldeirHistory",
		"PyrosHistory",
		"HelmHistory",
		"ValusHistory",
		"OasisHistory",
		"BlackForestHistory",
		"AsperHistory",
		--"TrinitHistory",
		"PetraHistory",
		"DenirHistory",
		},
		TitleColor = "1C7A8C",
		BookHue = 480,
	},
	{"Dungeons of Celador", {
		"HallsOfCorruption",
		"HallsOfContempt",
		"HallsOfContemptTranslated",
		"HallsOfRuin",
		"HallsOfDeception",
		--"Catacombs",
		--"Monolith",
		},
		TitleColor = "8523DB",
		BookHue = 242,
		--Locked = true,
	},
	{"Secrets of Celador", {
		--"SecretJournal",
		"CiarraJournal",
		--"YmarStillheartJournal",
		--"CehanJournal",
		"CharlusJournal",
		--"TerrowinJournal",
		"RobertJournal",
		--"VrothruRaJournal",
		--"LoversSerpentPassJournal",
		--"CultistJournal",
		--"TheBlueFlame",
		},
		TitleColor = "C28632",
		BookHue = 53,
	},
	{"Legends of Aria", {
		"CreationOfAria",
		"ShatteringOfAria",
		--[["Titans",
		"Xor",
		"Pedesii",
		"Wayun",
		"TowerOfBabel",
		"Colosseum",
		"Terminus",
		"Londinium",
		"Echo",]]
		},
		TitleColor = "FFD37A",
		BookHue = 877,
	},
}

AllTomes = {
	EmptyTomebook = {
		Name = "Empty Tomebook",
		Entries = { },
	},
	MaxEntries = {
		Name = "Buncha Entries",
		CompleteTooltip = "Pyros",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
				},
			},
		},
	},
	--
	-- TownsOfCelador
	--
	EldeirHistory = {
		Name = "Eldeir",
		CompleteTooltip = "The complete series of passages on the History of Eldeir.",
		Tooltip = "A historical passage on the History of Eldeir.",
		Entries = {
			{--1
				Title = "The King's Fields",
				Pages = {
					--1
					"[$3389]",
					--"The land where Eldeir currently stands was once a small farm, the only in all of Celador. The wind blew gently in the daytime, rustling the soft golden ears without disturbing the sweet, rich soil.\n      Hay bales lie scattered over the stubble that separated field from meadow, while great stone mountains pierced the sky to the east.\n      The beauty of the farm belied the true nature of what it was to manage, however. The days were long and brutal for the men and women who worked the fields.",
					--2
					"[$3390]",
					--"Some of the workers claimed to do the work for a handful of gold, hoping that one day they would save enough up to find themselves living in the capital.\n      Others said it was for a warm bed - Terrowin wanted his laborers to live on the land, right where the Eldeir Inn stands now, so that the nobles would never have to wait for their meals.\n      Whispers of darker circumstances could be heard at night of men who were approached at local watering holes, tricked while drunk into signing away their lives to serve the wealthy.",
					--3
					"[$3391]",
					--"These workers, sons and daughters of the soil, were held captive not by chains, but by fear. Terrowin had within him a magic that could be neither countered nor escaped, his powerful reign reaching across the whole of Celador. Escape was a dream.",
				}
			},
			{--2
				Title = "The New Village",
				Pages = {
					--1
					"[$3392]",
					--"The farm was maintained by the Jameson family, with their presence on the outskirts of the city sparing them a frozen fate. While the power of the Gods did not freeze them in place, their own guilt did, sitting on Arthur Jameson's chest and within his brain.\n      What he had done for Terrowin, how he had managed the workers, he could not undo. He sought to make amends in subtle ways. So when the Order of Petra sought to establish a base, Arthur donated the land and home for the purpose.",
					--2
					"[$3393]",
					--"The Order of Petra converted the farmlands, vowing to defend the imprisoned Petra with their unflinching loyalty until the spell that bound it could be reversed.\n      The other villages thrived under their own governance, growing in sizes so great that the former capital paled in comparison. The families of those trapped during the Imprisonment never migrated, and the need for a new city grew increasingly urgent.",
					--3
					"[$3394]",
					--"No more were allegiances sworn to the protection of the Sealed City. Instead, the base was used to establish Eldeir, which evolved swiftly from a station of protection to a thriving village and heart to Celador's economy.",
				}
			},
			{--3
				Title = "The Trapped Goddess",
				Pages = {
					--1
					"[$3395]",
					--"The village of Eldeir was founded upon the principles of wisdom, benevolence, and contribution to peer and God alike, principles bestowed upon humans by Tethys herself, goddess of man and water.\n      The Creator Gods served a punishment to the indolent, who brought eternal imprisonment upon themselves, eternal shame upon their families, and entrapment of the beloved Tethys.",
					--2
					"[$3396]",
					--"For so long as Tethys finds herself prisoner within her shrine, suffering for the sins of man, it is our sacred duty to put more into this world than we take from it.\n      Only through living virtuous and loyal lives may we break our beloved Tethys from her captivity and welcome her to this land to once more guide us. We are a village of bonded brothers and sisters, and our purpose is known. We will wait at the steps of Tethys' altar until the day the Creators see fit to return her to us.",
				}
			},
			{--4
				Title = "Blood and Water",
				Pages = {
					--1
					"[$3397]",
					--"The people of Eldeir live a simple life, uncomplicated by personal ambition and the desire for anything beyond what they were given.\n      Their loyalty binds them to this land; loyalty to the town, yes, but more importantly, loyalty to their family. Their blood binds them to a shared destiny.\n      Those who bore their family names cursed themselves and this land, and only those who share such names have the power to reverse such a powerful curse.",
					--2
					"[$3398]",
					--"From far across the fields and forests and docks and deserts, travelers join them, awaiting their opportunity to dedicate their lives to serving with dignity and diligence.\n      I myself was one of these travelers, a citizen of Helm by birth but a citizen of Eldeir by choice. Ten years I slept in a tent by the rocky borders of the village, sharing the small cloth quarters with three others.\n      The roof, a patchwork of fabrics that never quite shielded us from the rain. The others grew tired of the stench, of the waiting, and returned to their homes.",
					--3
					"[$3399]",
					--"The three who left were replaced with four more. This was the only place for us in the town - but this town, this was the only place for me in Celador.\n      Ten years I waited in a tent with little more than the clothing on my back, waiting to be called upon to serve the town. Some were sent to the Quarry,  while others assisted the town by working in the arts.\n      For me, my calling was to assume the role of Blacksmith. Such an honor was to be reserved for the blood of Joff, the former Blacksmith. However, his own heir abandoned his duty, fleeing instead to the lawless desert to peddle wares to criminals and undesirables.",
					--4
					"[$3400]",
					--"His abandonment left a vacancy for which I was called upon to fill.  I have been honored with such a responsibility.\n\n         - Samogh",
				}
			},
			{--5
				Title = "The Dead Gate",
				Pages = {
					--1
					"[$3401]",
					--"The Dead Gate has stood dormant for thousands of years. Few understand any details of its existence, and fewer still understand it's magical power. What is known is that it has existed since time has existed.\n      It once was a vehicle for taking man from Celador to places far from here, but now it stands, imposing pillars which have been observed to still omit a faint glow.\n      Gnarled trees stand hunched in fear of the structure, plunging their branches deep into its shadow.",
					--2
					"[$3402]",
					--"Citizens of Celador speak frequently of men, women, and even children who have been lost after wandering too near to the Dead Gate, and it is believed that these souls somehow triggered its magic to flare, drawing them within to be taken to a place far from here.\n      The people of Eldeir in particular caution against wandering too far down the Lethe. It's been said that a small town lies just past the Gate, but no man here is brave enough to journey that far.",

				}
			},
		},
	},
	PyrosHistory = {
		Name = "Pyros",
		CompleteTooltip = "Pyros",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Edge of the World",
				Pages = {
					--1
					"[$3403]", --adjusted to fit page "resulted in establishment"
					--"Pyros was founded atop the towering ramparts of stone along the Westernmost jawline of Celador’s face.\n      The men and women of Petra were able to satiate their need for iron and stone through the establishment of Helm to the East, but had fallen discontent with the fruits of their valleys and grains of their fields.\n      The finest of fish had been observed by a mapmaker on the other side of the world, and it was not long before Terrowin’s desires resulted in a dedicated fishing village.",
					--2
					"[$3404]",
					--"Laborers from the fields of Petra were sent to establish a village on the rocks, the huts looking out to endless sea and sky while the wrathful waves pounded furiously against the foundation below.\n      The sea air was little comfort to the workers, who worked tirelessly to supply the growing demand of the citizens of Petra, with few scraps remaining to sustain their workers.\n      Few escaped, fleeing to the deserts of Oasis to the North while the guards from Petra slept.",
					--3
					"[$3405]",
					--"The faces of the Pyrosians weathered under the unrelenting sun, spending their lives in the open air regardless of weather, their skin growing more tanned than could be found from any beast’s leather.\n      Their bodies grew stronger as they hauled nets, and their minds were never resigned to serve forever.",
				},
			},
			{--2
				Title = "The Cerulean Meadow",
				Pages = {
					--1
					"[$3406]",
					--"While the citizens of Petra were condemned to eternal confinement, the citizens of Pyros found themselves liberated, the people eagerly embracing their newfound freedom.\n      Uncertain of if or when the spell that froze Petra in place would be broken, Pyrosians swiftly and tirelessly focused on expanding, both in the welcoming of new wanderers and in the physical expansion of the town.",
					--2
					"[$3407]",
					--"This new independence also positioned Pyros to become the primary distributor of the finest fishes in all of Celador.\n      The expert fishermen of the town knew that, in order to meet this growing demand and to be able to feed their own, they would have to reach deeper waters where more schools of fish were observed to gather.\n      They recruited the strongest of laborers from across the land, enlisting them to harvest as much wood from the Black Forest as their horses could carry and return it to the city.",
					--3
					"[$3408]",
					--"These laborers then constructed elaborate piers which stretched beyond the western borders of Celador and into the heart of the Ocean, granting them access to bounties of fish.",
				},
			},
			{--3
				Title = "The Goddess of the Sea",
				Pages = {
					--1
					"[$3409]",
					--"The village of Pyros was founded upon the principles of strength, determination, and accountability.\n      The Goddess Tethys resented the people of Petra, who had grown selfish, lazy, and entirely reliant on others to care for them. They abused the gift of her magic in order to lead such a life.\n      The Goddess punished these men and women, imprisoning them in their town in order to force self-sufficiency, choosing to return to her home in the sea and using its waves to bestow",
					--2
					"[$3410]",
					--"blessings and curses alike.\n      In order to ensure that the humans of Celador do not repeat the sins of the past, Tethys rightly issues punishments to all, with the expectation that the community must demand a certain level of excellence and virtue from its individuals.\n      We must ensure that those within this land are strong and capable, contributing what we can. Should we fail, our nets will fall empty and our lands will wither, dry as the sands of Jhalir.",
				},
			},
			{--4
				Title = "Body and Mind",
				Pages = {
					--1
					"[$3411]",
					--"I had only witnessed three new moons in Pyros when I challenged Tidus for the job of Trader. In the moment that married standoff and fight, his eyes locked with mine, his face showing neither pleasure nor fear. We both knew that Tethys would smile upon the stronger of us, pleased with the village’s commitment to ensuring that each task was completed by the most capable. Frozen we stood, locked in a deafening silence until the Arena Master, Vladimir, signaled us to begin.",
					--2
					"[$3412]",
					--"A sudden jolt of pain erupted throughout my face, my nose snapping into a grotesquerie as a veil darkened my vision. His eyes, once calm and resigned, were now charged, blazing red in a way that made him utterly unrecognizable. Our battle was not one of graceful destruction, but rather was an erratic frenzy of iron and bandage.\n      Our swords gleamed in the cool moonlight, a dance of mighty cries and metallic clangs, each of us determined to win favor in the eyes of the Gods. But as swiftly as our battle began it ended, my blade piercing the flesh between helm and tunic.",
					--3
					"[$3413]",
					--"His blood poured as if in slow motion, his blanched hand grasping to contain the fountain at his throat. I looked on calmly as he died.\n      Once resurrected, Tidus was sent to serve his days as a Fisher on the docks. My victory in the arena awarded me a prized position in the heart of Pyros’ economy. I am eager to serve Tethys until I am challenged - or perhaps I shall challenge another. I believe I would enjoy the life of Mayor one day.\n\n      - Vincent",
				},
			},
			{--5
				Title = "The Sexton's Hut",
				Pages = {
					--1
					"[$3414]",
					--"The people of Pyros believe that, following the Imprisonment of Petra and Tethys’ subsequent return to her palace in the sea, the Goddess grieved the failure of the sacred city. The surf drew back, farther than ever before.\n      The blue line on the horizon grew, and Tethys’ grief exploded beyond the cliffs, her waves blanketing the land above as a fierce storm rained down on Celador.",
					--2
					"[$3415]",
					--"The graveyard at the mouth of Pyros was once a place of silent bereavement. Moss-kissed stone guided mourners to the graves of their loved ones, with a kindly grave keep, Abner, watching over those whose bodies had returned their matter to the earth.\n      The storm attacked Pyros with such fury that many of the bodies were exhumed by its wrath, washed away to Ysel and beyond. Abner spent his final days seeking to collect the bodies and return them to their final resting place. Not all rested, however. The dead stirred, returning to a horrid caricature of life.",
					--3
					"[$3416]",
					--"The elderly grave keeper's hut remains in the cemetery, serving as home to the awakened corpses. A light glows in its window, although no candle has been found to burn within.",
				},
			},
		},
	},
	HelmHistory = {
		Name = "Helm",
		CompleteTooltip = "Helm",
		Tooltip = "",
		Entries = {
			{--1
				Title = "East Denir",
				Pages = {
					--1
					"[$3417]",
					--"The town now called Helm was once East Denir, the humble mining village on the eastern cusp of world and sea. The sand, a gentle hue of earthen gold and crystalline, a steady and serene warmth flowing from its grains.\n      The wind, as if assuming the role of conductor, sending waves into both haunting ballad and furious cacophony. Helm was not chosen for its beaches, however.\n      The mountaineers of Denir, the Second City, journeyed farther east, discovering an abundance of mossy",
					--2
					"[$3418]",
					--"caves, glistening with the most sought after ores.\n      In their service to Petra, the Denirians established a modest base at the heart of these mines, stocking it with the sons of Denir's most dedicated laborers.\n      Each day, men and women would bleed their hands dry to supply the Nobles in Petra with enough stone and iron to build their sprawling manors, as well as to expedite the establishment of new towns with these precious resources.\n      The years stretch like a road through barren sand, the men and women",
					--3
					"[$3419]",
					--"toiling side by side, many giving their lives to sustaining the desires of the Capital.\n      Dreams of freedom remained dreams, giving those who survived a fleeting escape from the realities of their worlds. Dreams of living in a world of plenty, of more happiness and less scarcity.",
				},
			},
			{--2
				Title = "Land of Iron and Stone",
				Pages = {
					--1
					"[$3420]",
					--"After the Imprisonment, the villagers worked day and night to recover. The demands of Terrowin's men had left Helm starved for resources, with all in the village working multiple jobs to recover.\n      Helm flourished over the years, with new cabins and shops dotting the patchwork hills that stood tall in the space between sand and sky. The weak grew strong, and the strong grew stronger still.\n      In the evenings, the hardest of workers carried their picks to the",
					--2
					"[$3421]",
					--"mines, working until the dead of the night to prepare for any future attempted takeover.\n      Each morning, the workers would gather to welcome the kiss of dawn along the shores, stealing a few moments before the day demanded their efforts. Never again would the people of Helm be so small that they should fall to a dictator.\n      After Denir was destroyed, the remaining warriors and excavators fled to their sister city. The people of Helm do not work for money - they work to make their world better, to heal the damage that other men had caused.",
					--3
					--"",
				},
			},
			{--3
				Title = "The Wrath of the Gods",
				Pages = {
					--1 -- 'The village of Helm was founded upon the principles of' before hard work
					"[$3422]",
					--"Helm's founding principles were hard work, dedication, and the individual achievement of absolute excellence.\n      Tethys, Goddess of Water, has served as the Guardian of Celador since the shattering of Aria. The Gods of Earth, Fire, Air, and the Void complete the Titans, each tasked with the mission of guiding the individual beings of their shards.\n      From their post above, they watch over their amusing little playthings, often passing the time by waging bets on how their wards would behave.",
					--2
					"[$3423]",
					--"Tethys was once the most benevolent Titan, forgiving the folly of man and choosing gentle guidance over stern punishment. The other Titans shuddered at her gift of divine magic to the humans, watching in horror as the sacred powers were abused for personal gain.\n      The sins of Petra changed Tethys' method of ruling, and she enlisted the aid of the other Gods to punish them accordingly. The Titans combined their elemental powers to construct a magical barrier around the capital, condemning all within to an eternity of penance.",
					--3
					--"[$3424]",
				},
			},
			{--4
				Title = "All Hands",
				Pages = {
					--1
					"[$3424]",
					--"Three years have passed since I first arrived in Helm, delivering a shipment of fish to the people of the beach community. The moment I stepped through the Helm gates I was infused with the desire to serve the Gods.\n      My entire life was spent on docks, knowing only grass and dirt, wood and sea. Standing on the beaches of Helm, the salt in my nose, on my face, clinging to my skin, I could feel the Gods blessing this land. I never returned to Pyros.",
					--2
					"[$3425]",
					--"By day I manage the Tinker Shop while Hameka sees to her Mayoral duties, ensuring that our miners have picks, our lumberjacks have axes, our hunters have knives. My work has allowed me a humble shelter, so that I may be of better service to the good people of Helm.\n      We all spend our nights working in the arteries of Celador, swinging our heavy picks at the ore faces, blackened rivers of sweat flowing over our bodies. The darkness blankets us in the mine, protecting us from all eyes, save for the Gods above.",
					--3
					"[$3426]",
					--"The impact of Petra is still felt in our town, and the loss of Denir as an outpost for our warriors has left us in quite the deficit. We all work tirelessly to rebuild, blissful dreams of what was - and what may still come to be - peppering our minds.\n      We are all honored to serve, dedicating our lives to contributing to our village, each of us working two jobs or more. We are honored to serve.",
				},
			},
			{--5
				Title = "The Altar",
				Pages = {
					--1
					"[$3427]",
					--"Long before the Imprisonment, the people of Helm gathered to pray to the Goddess Tethys.\n      While others would shield themselves from the rain, the citizens of Helm would gather in the open, each storm crashing like an opera of the skies, the trees and grasses pounding in harmony with their orchestrated percussion.\n     A fire was always lit, its thick gray smoke meeting the flocculent clouds. The brisk blue sky, covered by a veil of darkness as Tethys' blessing",
					--2 -- 'celebration. Then' for 'celebration. Until'
					"[$3428]",
					--"swallowed the heavens for as far as the eye could see. And in the center of it all, a proper altar.\n      The people of Helm gathered at the altar in celebration of Tethys, asking for both grace and justice. As the people of Petra grew stronger in their demands, the prayers of Helm grew stronger in their requests.\n      It is believed that the prayers of the Denirians swayed Tethys and the other Gods toward action. Each storm, the people of Helm gathered in celebration. Then the dead appeared, as if summoned by the Gods of destruction and punishment.",
					--3
					"[$3429]",
					--"No man has returned to the sanctuary since. But the fire still burns.",
					--"Until the dead appeared, as if summoned by the Gods of destruction and punishment. No man has returned to the sanctuary since.\n      But the fire still burns.",
				},
			},
		},
	},
	ValusHistory = {
		Name = "Valus",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Healers of Celador",
				Pages = {
					--1 -- edited for page spacing, no cut words
					"[$3430]",
					--"There once was a time where warriors wounded in battle and hunt were reliant on bandages and prayer to survive their injuries, their fate tied intrinsically to the whims of the Gods.\n      One day, a traveling bard found himself poisoned after a brief interaction with a giant scorpion, and the people of Pyros tried to ease the man's suffering with locally brewed ale. They hoped for little more than to give the man the opportunity to pass away more peacefully.\n      However, the bard found himself",
					--2
					"[$3431]",
					--"mysteriously cured of his affliction and all internal wounds that had resulted from it.\n      This discovery encouraged the herbalists to study ginseng further, working in cooperation with the historians and scribes of Celador.\n      Herbalists concluded that the rare addition of ginseng to the brew was responsible, and scoured the lands, harvesting items from the earth and dutifully studying their properties.\n      They discovered that, by carefully combining specific incantations with different reagents and unique parchments, they could heal the",
					--3
					"[$3432]",
					--"wounds of man and beast alike - curing poisonings, injuries, and in some cases, bringing the newly deceased back from the dead.\n      The most intelligent scholars in Celador were naturally skilled at using these arts, mastering the ability to memorize spells with a single review and able to cast limitless instances, provided they had the proper herbs on hand. These magical scholars came to be known as the Healers of Celador.",
				},
			},
			{--2
				Title = "The Great Stone City",
				Pages = {
					--1
					"[$3433]",
					--"The Healers of Celador required a base for their studies and practice of the magical art of Manifestation.\n      Construction began deep in Celador's belly on a fortress for these wise mages to practice known spells, as well as to work with different herbalists to identify additional methods of healing the wounded.\n      Under the tutelage of the most powerful mages, Asteria and Saehrimnir, they conducted their work far from Terrowin's gaze - he much preferred to remain ignorant of the",
					--2
					"[$3434]",
					--"mages' work, particularly when their methods turned questionable.\n      During their studies, however, Asteria and Saehrimnir discovered new skills and spells - one which gave them the ability to cause remarkable destruction.\n      With these new spells, the mages could rain fire from the skies, rivaling the Gods themselves in terms of power. Terrowin and the nobles of Petra were stricken by fear when word of these advancements reached the Capital. Terrowin's own favor with the Goddess Tethys had granted him unparalleled powers. Unparalleled, until now.",
					--3
					"[$3435]",
					--"Terrowin called upon the most powerful of warriors across the land of Celador and prepared to march on Valus. But Petra was imprisoned before he had the chance.",
				},
			},
			{--3
				Title = "The Goddess Within",
				Pages = {
					--1
					"[$3436]",
					--"The city of Valus was founded upon the principles of valor, spirituality, and respect for the power within. Man, woman, and child have long sought the truth of the Gods, seeking them out in the selfish hope of gaining their favor.\n      They cared not for dedication and servitude, but rather for reward. They see the Gods where it is most convenient for them, using their existence as little more than fable to explain the phenomena of drought, disease, and depression.",
					--2
					"[$3437]",
					--"When Aria shattered and Tethys assumed control over Celador, she attempted to grant unto man a powerful magic. This man abused Tethys' divine gift, condemning himself and his people to a life of eternal Imprisonment.\n      Tethys exists in neither sea nor sky nor prison nor earth, but within the souls of the most powerful beings of Celador. How else could such magic be explained? Tethys cannot guide from above or below, only from within.\n      She has no time for the wars of simple men, but rather in ensuring that the most capable and deserving",
					--3
					"[$3438]",
					--"of her children reach their full potential. Her greatest gift was not the gift of magic to Terrowin, but the gift of herself to her chosen children.",
				},
			},
			{--4
				Title = "The Groove",
				Pages = {
					--1
					"[$3439]",
					--"The other children in my village always had a healthy fear of the woods. Dangers lurked around each corner. This is what we were told to keep us from wandering too far - but this was also the truth.\n      Tales of black trunks uprooting themselves to pursue child and man alike, their bare branches spiking forward to grip you from the shadowed path. Spiders so great they could swallow a newborn babe whole. Even the finest of torches emitted a glow that faded only inches from the",
					--2
					"[$3440]",
					--"flame. In the daytime, leaves and trees danced in shadows at our feet. The other children in my village always had a healthy fear of the woods. I, however, did not.\n      The howling wind sang to me, crooning love songs in my ear and guiding me to the radiant grove. Mustrooms, taller than most trees, with soothing glows of moss and sky. The adults would tell us of the dangers of the woods, but never of its beauty.\n      Every day I would elude my mother's gaze and creep towards the grove, bathing in the glow of the mushrooms, feasting on the wild,",
					--3
					"[$3441]",
					--"fragrant treats that sprung around them.\n      It was in this grove that my mind wandered, fantasizing about the fate of Aria, of what may have happened to have left Celador on its own. It was in this grove that boulders rained from the sky, my fantasies coming to life almost as quickly as I imagined them.\n      It was in this grove that Saehrimnir found me, in awe of my power. I left with him that day, doing the one thing that little girls are warned against doing. He took me to a grand stone village, where every worker - from Trader to Blacksmith - had within",
					--4
					"[$3442]",
					--"them the gift of magic. It was my destiny to be here, to work here, Saehrimnir told me, but not as a Trader or Blacksmith.\n\nThen he told me about the blue flame.\n\n      - Ismeria",
				},
			},
			{--5
				Title = "The Trials of Valus",
				Pages = {
					--1
					"[$3443]",
					--"The work of the Healers of Celador was critical to healing the wounded and afflicted. It was critical to saving lives. But you cannot heal what is not sick, nor can you return what has not been lost.\n      The infancy of Manifestation was dedicated to studies - studies of nature's medicines, studies of the properties of herbs and reagents, studies of the frailty of the human body. Magic in the early days of Celador was more about the juxtaposition of scientific discovery",
					--2
					"[$3444]",
					--"than it ever was of true magic.\n      Few willfully volunteered their bodies to this critical area of study. Therefore, the Healers found it necessary to encourage participation in other, more sinister ways, often without the knowledge or consent of the participants.\n      These tests became known as the Trials of Valus. Many of the Healers dreaded their duties, but Saehrimnir found a deep enjoyment in his work. You can not cure the wounded, the poisoned, or the dead without people being wounded, poisoned, or dead. Saehrimnir deemed the losses",
					--3 'deep' for 'deep or more' / 'dead broke free' for 'dead have broken free' / 'or, perhaps,' for ', or perhaps'
					"[$3445]",
					--"incurred during the Trials of Valus as necessary and honorable.\n      The local graveyard would not house all the bodies of the Trials, so a new Cemetery was built into the hollow space at the heart of the Crescent Rim, which protectively cradled the unmarked graves of dead wanderers who had unknowingly given their lives to advance the understanding of magic.\n      Some of the graves were packed five bodies deep to hide the true extent of the Trials. Many of the dead broke free, in search of a more comfortable resting place or, perhaps, revenge.",
				},
			},
		},
	},
	OasisHistory = {
		Name = "Oasis",
		CompleteTooltip = "Oasis",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Twin Capitals",
				Pages = {
					--1
					"[$3446]",
					--"Across the whole of Celador, people speak of the corrupt king, the lazy nobles, and the capital that is trapped forever beneath a magical forcefield. The Imprisonment of Petra was, to many of the villages of Celador, the day of their birth, their independence.\n      They have allowed the entirety of history to be dictated by one moment in time, and how that moment influenced their own societies. They never speak of the other fallen Capital of Celador, Peska.",
					--2
					"[$3447]",
					--"Peska was once the jewel of the golden sea, a sprawling metropolis through which all travelers of the west passed, particularly after the establishment of Pyros.\n      Great buildings rose proudly from the desert sands, with rivers of people flowing through the maze of golden caramel structures.\n      Peddlers from across Celador visited Peska, selling browned fish, shimmering jewels, the finest silk robes and the most durable sabers in all the land.\n      Stalls with brightly colored cloths surrounded the capital building,",
					--3
					"[$3448]",
					--"which housed a grand throne upon which Adaliz sat.\n      Adaliz, ruler of the Barren Lands, looked after the people of Celador alongside Terrowin, seeing to it that their needs were properly met. But Terrowin resented the growth of Peska, which dwarfed the much smaller village of Petra.\n      Celador's great Civil War left Peska in ruins, due largely to the power of Terrowin's access to water magic. The war was not merely a war on leaders, but on any who dared to rival Terrowin or his claim to sole power in Celador. None in Peska survived.",
				},
			},
			{--2
				Title = "The Oasis",
				Pages = {
					--1
					"[$3449]",
					--"Peska was never rebuilt following its destruction or the Imprisonment - Celador had long forgotten the great city of the desert.\n      But the Barren Lands still required a sanctuary for daring travelers, be they in search of great beasts to tame or great beasts to slay, and nestled in the ocean of sand that makes up the Westernmost part of Celador a haven for weary and wayward travelers was established.\n      Oasis is more of a town than a settlement. The merchants who set",
					--2
					"[$3450]",
					--"up shop are deserters and runaways, fleeing the wars of their villages in search of a more independent adventure in life.\n      It is a community in the loosest sense of the word, welcoming all for a time, but no one for very long. It is a land of neither law nor judgement, free from the watchful eye of any guards.\n      The small makeshift village for misfits is stationed alongside the only wisp of true water in the Barren Lands, the only structures are salvaged remnants of the fallen desert capital. Few call this place home, and those who do seek to keep it that way.",
					--3
					"[$3451]",
					--"All are welcome - and all assume a degree of risk by resting their heads in Oasis.",
				},
			},
			{--3
				Title = "Heathen's Paradise",
				Pages = {
					--1
					"[$3452]",
					--"Our little Oasis is not a mere resting place for weary travelers, it is a slice of paradise for the religious refugees who flee the wars and imposing moral structures laid out by the leaders of their former homes.\n      Our current Blacksmith fled from a war for which he was drafted, a cause that other men demanded he bleed for.\n      Our Alchemist hails from Pyros, where the people believe the Goddess Tethys exists in the waves of the sea. He sought out the sandy waves of the Barren Lands in order to escape this",
					--2 -- 'protect, and to' from protect, to'
					"[$3453]",
					--"belief, as no village could be less like Pyros than Oasis.\n      Little wood and ore exists in the aurete sea, and yet Oasis is home to the finest Tinkerer in all of Celador, who fled from a life of nonstop work in Helm. A life in which he would be forever condemned to sacrifice his body to some Goddess who had not once smiled upon him.\n      Man has allowed his love affair with a belief to dictate true morality, a code that must be strictly followed, lest he awaken the wrath of the object of his affections. On their mission to serve, to protect, and to free this improbable",
					--3
					"[$3454]",
					--"being, men have committed greater atrocities as a group than anyindividual could ever hope to commit.\n      The lone path is the safest to travel. The murderers and criminals who pass through have much less blood on their hands than those who dare to call themselves righteous.",
				},
			},
			{--4
				Title = "The Refugee",
				Pages = {
					--1
					"[$3455]",
					--"I was a boy of 15 when I was called upon to join the Eldeir Militia. My birthright was that of Blacksmith, a role which I was to assume once my father was unable to perform his duties.\n      The threats from Denir, however, left the leaders of Eldeir searching for more strong bodies who could take up arms without the need for much training. I grew up the son of a blacksmith, creating and testing such weapons.\n      Enlisting me was a simple choice. But I am no killer of man, whether",
					--2
					"[$3456]",
					--"that man is deserving or not. I bend ores with the gift of Pyros. I create - I do not seek to destroy.\n      I left when the moon was highest in the sky. All in Eldeir and beyond warned against wandering too near to The Dead Gate, but I passed right by, the imposing structure apathetic to my presence. I reached the bridge to the Barren Lands by daybreak. Two nights more, and I found myself in the settlement of refugees known as Oasis. No leaders asked my business, and my unannounced arrival was met with bored eyes. No one here cares where I am from, they care not for my family",
					--3
					"[$3457]",
					--"name or the sins of men who bore it centuries ago. They simply wish to buy my swords and move on to their next great adventure.\n\n      - Maron",
				},
			},
			{--5
				Title = "The Great Sand Temple",
				Pages = {
					--1
					"[$3458]",
					--"Much of the comet that shattered Aria was burned alongside many inhabitants of the planet in an apocalyptic inferno.\n      Smaller pieces splintered upon impact, propelled across the different shards. One piece was discovered by a wanderer on her way from the Southern Rim to the city of Peska, in the low winding valleys between The Dunes.\n      The power of the fragment could not be truly understood, and many believed it had the potential to be",
					--2
					"[$3459]",
					--"unstable. So it was transported to a nearby island to be monitored.\n      This fragment was little more than frozen rock and gas. Yet it acted as if it were a tree, digging its roots deep into the golden sands and growing larger by the day. The citizens watched in awe, with an instinctive understanding of the magic that existed in this stone. It took only a few months for a deep cavern to appear, clearing a path to a great temple beneath the ground.\n      Over the years, the citizens would petition Sibilla, the High Priestess of the monument, for access to stand upon the exposed platform that",
					--3
					"[$3460]",
					--"topped the subterranean temple, crying out to the Gods of life and death for their favor.\n      The rituals that took place here were deeply mysterious and private, but few who petitioned the Gods walked away after doing so. It is not known what happened during the final ceremony but the great sand temple was suddenly sealed, the entrance marked with little more than a mysterious language and letters that would glow in the dark of night.",
				},
			},
		},
	},
	BlackForestHistory = {
		Name = "Black Forest",
		CompleteTooltip = "Black Forest",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Three Tribes",
				Pages = {
					--1
					"[$3461]",
					--"The wooded lands of Celador to the East have long been filled with the most dangerous of enemies. The trek through the sprawling forests was equally arduous and treacherous, with even the bravest of warriors refusing to step far within.\n      It was not devoid of human life, however. The people of the woods had existed in harmony with nature since time immeasurable, existing in a state of autonomy not afforded to any other citizens of Celador.",
					--2
					"[$3462]",
					--"The goddess Tethys herself left these people to govern themselves, allowing the different communities of the forest to flourish free from a shared moral obligation to any God.\n      The land was split in three, with Spiritwood to the North, Vilewood to the West, and Black Forest to the east. Each land housed tribes of people with great skills who lived and existed separately. The tribe of Black Forest was skilled in battle, combatting the dangers of the forest and hunting for food.\n      The tribe of Vilewood dedicated themselves to study, monitoring the",
					--3
					"[$3463]",
					--"happenings of the other villages of Celador and keeping the histories of a land that man was certain to destroy.\n      The tribe of Spiritwood housed woodspeople with a particularly advanced relationship with the creatures of the wilderness and the ability to tame and enlist them in all matters of protection and labor.\n      There was neither ruler nor war. The people lived in kinship with neighbor and nature alike. While the other villages cowered in fear of tyranny, the people of the forest bathed in the sweet light of freedom.",
				},
			},
			{--2
				Title = "The Outpost in the Sky",
				Pages = {
					--1
					"[$3464]",
					--"Terrowin's fear of the unknown wooded wilderness allowed the people of the land to exist freely during his reign. Few were permitted to enter - the people of Pyros hauled wood from the outskirts of Vilewood for their piers under the watchful eye of the tribes, and were not permitted to venture too deeply into the woods.\n      After Terrowin fell, the forest was left unprotected. The different villages of Celador thirsted for power and growth, seeking to expand and establish themselves. More and more",
					--2
					"[$3465]",
					--"travelers ventured into the woods, desperately seeking out the special woods that would grant their shields more strength. They came with horses, but also with swords.\n      The Spiritwood Tribe fell first, to Eldeir, their worship grounds littered with macabre painted skulls and the remnants of their clothing as it fell from their decomposed bones.\n      The people of Helm attacked the Black Forest Tribe, enlisting aid from the most vile of spiders to attack the woodspeople, striking them with a fierce, blistering poison.\n      What Pyros did to the Vilewood",
					--3
					"[$3466]",
					--"Tribe is beyond the comprehension of most, their bodies twisted into grotesque feathered beasts, exiled from their home to Treacher Path.\n      The few who escaped such slaughter fled to an elevated manmade outpost, nestled high in the trees at the cusp of the three lands of the forest. Here, they were able to fight off further enemy attacks from above. And here they stayed.",
				},
			},
			{--3
				Title = "The Goddess Tellervo",
				Pages = {
					--1
					"[$3467]",
					--"The other villages of Celador pray to a Goddess they have never seen, one they have never known. They believe she exists, therefore she must. These blind beliefs have caused blood to spill and evil spirits to prevail. Their wars are those of brother against brother, sister against sister, neighbor against neighbor. For a Goddess they have never seen.\n      We know the true Goddess of Celador. She rises higher than any mountain, sustains more fully than any mine or fish or field.",
					--2
					"[$3468]",
					--"She towers above the swamp, her roots stretching to the far corners of the sacred forest and beyond. From these great roots, the trees of the forest grow. From her base, the rarest of flowers and fungi sprout.\n      The attacks on the Tribes of the forest left all creatures fearing the destruction of Tellervo. Creatures of the three tribes traveled across the land to protect the Goddess from humans who would dare to destroy her.",
					--3
					--"",
				},
			},
			{--4
				Title = "Dreams of the Past",
				Pages = {
					--1
					"[$3469]",
					--"Laurie, Kasey and me are the sole remaining heirs to the Spiritwood Tribe. In Spiritwood, one can harvest the finest primeval wood, the wood of the Spiritwood Trees.\n      Only the eyes of the tribe can identify such trees, although the armies of surrounding villages have found other ways to harvest this prized lumber. Their wars have no bearing on us, we simply complete our work, returning to the homes of our ancestors to complete our work. That is how all in the Black Forest Outpost",
					--2
					"[$3470]",
					--"survive, by returning to their homes, harvesting wood or ore or plant or silk, and returning to the safety of the Post to sell our wares.\n      When I sleep, I dream in memories which are not mine. I dream the memories of those whose bodies are strung across the trees, whose bones have been fashioned to create a bittersweet music in the wind. I dream of their lives and horrors. My life seems quaint when compared to their own, but perhaps that was their wish. For those of us who remained to have quiet, quaint lives.",
					--3
					"[$3471]",
					--"Some days I am fearful of the outsiders who travel through our Outpost. But then I remember that they too dream of memories which are not theirs. They too live lives that were determined by those who came before them. Perhaps unity can be found there. Perhaps not. I shall keep my words to myself for as long as they can pay.\n\n      - Victor",
				},
			},
			{--5
				Title = "Lord Barkas",
				Pages = {
					--1
					"[$3472]",
					--"The original Tribes of Humans in the forests of Celador lived in harmony with the creatures in the land. All lived as one, the spiritual essences of land, river, creature, stone, and human living in peace with one another since before even The Great Shattering.\n      When the outsiders attacked the Tribes of humans, all spirits within the forest turned on man. None turned more strongly than Lord Barkas, a demi-god and protector of Tellervo. Lord Barkas enlisted creatures from across the lands to protect the ",
					--2
					"[$3473]",
					--"Goddess Tellervo.\n      In times of calm, Lord Barkas wades through the swamp, ensuring safety and gathering lost valuable items as an offering to Tellervo before returning to his hidden perch atop The Great Tree. When man attacks, Lord Barkas returns to his hiding place, giving the creatures who protect Tellervo the opportunity to stay off the attack.\n      Should these creatures fall, Lord Barkas will reveal himself, an imposing Fallen Ent who cares for neither God nor man. He exists solely to protect the Goddess of the Woods.",
					--3
					"[$3474]",
					--"What he does not know is that most men now come for him, to steal his tributes to Tellervo.",
				},
			},
		},
	},
	AsperHistory = {
		Name = "Asper",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Captives of Petra",
				Pages = {
					--1
					"[$3475]",
					--"The men and women of Petra - the ones who built the city, who worked the fields and lived in mud in order to give to the nobles of the city - those people were treated no better than cattle or pigs. The years stretched out like a road through the ocean, each direction home to nothing but waves and fury and almost certain death.\n      The road of time, an illusion of life and progress. And yet, still, all who lived and toiled away in that land walked the path, for walking towards nothing was still better than sitting",
					--2
					"[$3476]",
					--"and allowing the waves to consume you. So they walked the road, their blood and sweat, tears and broken bodies fueling the advancement of those who ruled over them.\n      Some of the workers claimed to do the work for a handful of gold, hoping that one day they would save enough up to find themselves living in the capital. Others said it was for a warm bed.\n      Whispers of darker circumstances could be heard at night of men who were approached at local watering holes, tricked while drunk into signing away their lives to serve the wealthy.",
					--3
					"[$3477]",
					--"They stayed not out of loyalty or love, but fear.\n      Terrowin of Petra had a powerful magic within him, a peerless magic that was bestowed upon him by Tethys, granting him the ability to bend all water - the oceans, the seas, the blood of those around him - to his will.\n      So the sons and daughters of the soil, held captive by a powerful, mortal demigod, remained under his control.",
				},
			},
			{--2
				Title = "Prisons and Freedom",
				Pages = {
					--1
					"[$3478]",
					--"The Imprisonment of Petra splintered the remaining people of the land, the men, women, and children who worked the land and dedicated their bodies and souls to the growth of an empire that did little to repay them for their service.\n      Many of those who were outside the city when it was imprisoned took upon their shoulders the heavy burden of atoning for the sins of their fathers, and establishing a society in which all worked tirelessly and selflessly towards earning the forgiveness",
					--2
					"[$3479]",
					--"of the gods.\n      It was their hope that, in living truly virtuous lives, they might break the curse, freeing not only their beloved goddess Tethys but also their families who were entrapped within the cerulean dome.\n      Not all who had slaved away in the fields of Petra sought to break the curse, however. Many - particularly those with no family trapped in the city - took this opportunity to flee the capital, embarking on the arduous trek north where no others had dared to travel before.\n      Should Petra be freed, these",
					--3
					"[$3480]",
					--"survivors of the city's terrors intended to be long gone. They knew when they set out that they might not survive the journey, but they knew that the death of their bodies would be far better than the death of their spirits.\n      The refugees walked for months through the harsh, neverending winters of the Frozen Tundra, the heavy snows so fierce that their wandering was often directionless.\n      By the time they reached the Northernmost peak of the freezing wild their numbers were half. Those who survived had never felt more hope, despite their seemingly",
					--4
					"[$3481]",
					--"hopeless situation.\n      On the edge of the world, where Celador met nothingness, the survivors established a new village and learned to live off the sparse resources of the land. They gave the new village the name of Asper.",
				},
			},
			{--3
				Title = "Azura's Landing",
				Pages = {
					--1
					"[$3482]",
					--"The village of Asper was founded upon the principles of freedom, survival, and resilience.\n      The Creator Gods served a punishment to the indolent, the men and women of power and greed who brought eternal imprisonment upon themselves and eternal shame upon their families. For so long as the citizens of Petra find themselves prisoner within their dome, suffering for their sins, we find ourselves truly free.",
					--2
					"[$3483]",
					--"Our abandonment of Petra and renouncement of the atrocities committed there has earned the favor of the Gods. Those who stayed behind believe Tethys is trapped within the dome with the evil men and women who were Imprisoned.\n      Perhaps this is the truth. We know only that Tethys is no true Goddess, but merely a fragment of one.\n      Until the five Titans are united as one, there are no Gods concerning themselves with the likes of humans. We are all on our own.",
					--3
					--"",
				},
			},
			{--4
				Title = "The Avalanche",
				Pages = {
					--1
					"[$3484]",
					--"I remember the crack the most.\n      The crack was greater than any thunder, signalling a fierce, imminent wave of violent destruction and certain death. The mountain cleared itself, the powder and slush exploding in its collision with the earth below.\n      Generations of history, lost to the whims of nature and the gods alike. Had the Goddess of Harmony been whole, the darker Titans would not have seen fit to punish us so dearly. Azura's mercy is said to live within the Mother, Tethys.",
					--2
					"[$3485]",
					--"But we have known no such mercy. The winters which protected us from outsiders are milder, and with the hit of warmth comes water, not snow.\n      All that had protected us from the warriors of the south, our natural defenses, have melted before our eyes. And beneath the snow lie our resources, our food, our workers, our children. Our past and future, simultaneously snuffed out under the blanket of crystalline mud.\n      My husband was once the finest woodsmith in all of Celador. None would know, of course, as we live far from the eyes of most. I watched him",
					--3
					"[$3486]",
					--"craft day and night, bending wood in ways that defied all sense of logic or reason, into the most majestic of creations. And now, he sleeps beneath the snow.\n      It is my duty, as his wife, his supporter, his student, to carry on his legacy. My craft does not match his, but our people must survive. And I must assist them in doing so.\n\n      - Freja",
				},
			},
			{--5
				Title = "The New City",
				Pages = {
					--1
					"[$3487]",
					--"The avalanche that split the great mountains to the west of the Frozen Tundra brought sky to ground, burying generations of life and legacy.\n      The few who survived were working outside the town of Asper, were gathering supplies for the skilled workers, or had managed to claw their way through the blanket of snow that threatened to suppress the little breath left within them. Most were not lucky.\n      The refugees found themselves in the same position of their ancestors, abandoning their home in search of",
					--2
					"[$3488]",
					--"an area in which they would be protected from the outside world. A new home. Several established a base at the foot of the avalanche site, venturing past the creatures which wandered the land in search of the bodies of their loved ones.\n      The others journeyed east, tasked with identifying a safe base for their new village. The refugees pulled sleds across the harsh tundra, piled with the bodies of the fallen who were able to be recovered.\n      The melting snow posed a great risk, as volcanic activity beneath the surface shook and warmed all above.",
					--3
					"[$3489]",
					--"The Asperians could not risk establishing a new village near any natural threats. Ultimately they established a modest village on an eastern island, laying the bodies they had brought with them to rest in a small graveyard nearby.\n      Their village is new, but clearly displays the scars of the horrors of what the survivors experienced, and live with every day.",
				},
			},
		},
	},
	TrinitHistory = {
		Name = "Trinit",
		CompleteTooltip = "Trinit",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	PetraHistory = {
		Name = "Petra",
		CompleteTooltip = "Petra",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Petra",
				Pages = {
					--1
					"[$3490]",
					--"Of all the remnants of Aria that exist, Celador is at once its most beautiful and most devastating, home to the humans of the land that was once Aria.\n      In the beginning, the heart of Celador was Petra, the Sacred City and capital of the land under the watchful eye of Tethys, Goddess of Water and governing deity of Celador.\n      The vast continent was united under a single mortal ruler, Terrowin, a powerful mage hand selected by Tethys to distribute her will throughout the land.",
					--3
					"[$3491]",
					--"To aid him in his role, Tethys bestowed upon Terrowin the unique mastery of water magic, granting him the ability to bend rivers, oceans, and even blood to his whims. As is the truth of human nature, such powers corrupted Terrowin, who took for granted his favor with the Goddess.\n      Terrowin ruled viciously, shielded by the blessing of Tethys, dictating his oppressive laws across the land. The entirety of Celador was under his control, with his mercy only being granted to the citizens of the Sacred City.\n      The citizens of Petra nourished",
					--4
					"[$3492]",
					--"their bodies with what they took from Pyros, they built their homes and shops with what they took from Helm, and they abused a forbidden magic in order to punish any who dared to resist their demands.\n      The greatest sin committed by the citizens of the capital was laziness. They took more than was needed and gave nothing in return, draining Celador of its resources and condemning any who were unlucky enough to live outside the capital to a life of servitude. It wasn't long before these people drew the ire of the Gods.",
					--5
					"[$3493]",
					--"The punishment for these offenses was swift and devastating. The city of Petra was sealed without warning, surrounded by a powerful elemental shield that froze the citizens within a force that bowed not to the laws of man.\n      Those lucky enough to escape were left without a home, exiled to the outskirts of Petra. Terrowin and his court were not among the lucky. After the land of Aria was shattered, the villages of Celador were governed by a shared set of principles, laws, and beliefs.\n     After Petra was Sealed, the citizens",
					--6
					"[$3494]",
					--"of the continent lacked not only a capital, but also a ruler. Without this unified leadership, the individual towns established their own societies with unique rulers, laws, and standards of living.\n      Celador has survived for centuries in a state of sundered harmony. As time passed, the significance of Petra fell far from the memory of most. Its presence stood as a warning - but the lesson taught by the Imprisonment differed from village to village.\n      The people of each town have a different system of beliefs about what transpired on the day Petra was",
					--7
					"[$3495]",
					--"sealed, and each has embarked on a unique path towards earning redemption in the eyes of the Gods.",
				},
			},
		},
	},
	DenirHistory = {
		Name = "Denir",
		CompleteTooltip = "Denir",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Letter from Denir",
				Pages = {
					--1
					"[$3496]",
					--"The return of Petra is as certain as the rise of the sun each morning.\n      Long have we aided the people of Eldeir in their establishment, as we will require strength and numbers to combat the tyranny that has further festered within the prison that is the former capital. Life under Terrowin starved the villages of the Eastern Frontier of resources with no reward gained.\n      But today, Denir is thriving. Alongside our sister city to the far east, we are able to supply the villages of",
					--2
					"[$3497]",
					--"Celador with precious stones and metals in exchange for enough food to nourish the people of the Eastern Frontier.\n      There are concerns of the corrupting nature of the soil on the other side of our mountains, however. It has been said that Eldeir's military has enlisted the aid of beastmasters with skills so great they can command the wyverns of Oasis.\n      These great beasts have breathed a fire along the western border of the Upper Plains, melting the Gods' fertile earth and revealing in its ashes the fine metals hidden within Celador's core.\n      We must monitor the situation to",
					--3
					"[$3498]",
					--"ensure the Elderians are not positioning themselves for a hostile takeover of the remaining north.",
				},
			},
			{--2
				Title = "Letter from Eldeir",
				Pages = {
					--1
					"[$3499]",
					--"I was visited by a mysterious guest during the evening, one which took upon itself the appearance of a shadow so deep, even the dark of night felt blinding in its company.\n      The haze of deathly night spoke with clear authority, each word punctuated by the sweet smell of fragrant mushrooms.\n      This shadow warned of a coming siege, that the mountaineers to the East had forged paths through the Spine and were one fortnight removed from an attack on our village. Risking",
					--2
					"[$3500]",
					--"any attack on Eldeir is of no option, nor is warring with a neighboring village on the word of a shadow. We tasked the first swordsman's son, Cadmar, with crossing Cast Lake and investigating the validity of the claim.\n      A journey along the Lethe has exposed a number of manmade caverns and unmarked bridges, just as reported. One in particular is of interest, running from the Temple of the Corrupt to the Giant's Steppes.\n      The scouts we deployed observed the Denirians for hours, as they spoke of a shipment of armor and weapons from Helm that are due to arrive in",
					--3
					"[$3501]",
					--"three nights. We have no choice but to strike before they are able to arm themselves.\n      The Denirian forces have guards to the north and south of the town. The west, however, is guarded by only the mountains, which they have made weaker.\n      May the Gods smile upon us. ",
				},
			},
			{--3
				Title = "Letter from Denir",
				Pages = {
					--1
					"[$3502]",
					--"Hours ago we stumbled from the Inn, our minds dizzy and our breath stinking of ale, laughter billowing out the door as the Bard serenaded us with one final ballad. That Inn is now a skeleton stripped of its flesh by the vile mites of Eldeir. They had come without warning.\n      The clang of swords has died away. The shouts of slaughter have hushed. Silence blankets the ground as a bitter wind sweeps the hillside. The city is still burning, billows of smoke pooling over our heads as ash and rock clings",
					--2
					"[$3503]",
					--"to our faces.\n      The ground glistens as if a rain had fallen, but under the bake of the sun we can see that there is no rain, just a grotesque mosaic of mud and blood. Some doors hang by their hinges, groaning with every gust of wind. Others were destroyed by the mages' fire.\n      The bodies await burial, too many to count. Our trader, our scribe, our blacksmith's mother, our herbalist's son. The Bard who sang us to sleep. The life has been blinked from their eyes and they lay like dolls in the grass. Our streets, once rivers of activity, now",
					--3
					"[$3504]",
					--"serve as a mass grave.\n      Too numb to cry, the smell of reagents stinging my nostrils, my heart is engulfed in anger as I step around the dead. I grab a shovel, coating my mind in sweet thoughts of revenge.",
				},
			},
			{--4
				Title = "Letter from Eldeir",
				Pages = {
					--1 --'our blood' for 'the blood of our blood'
					"[$3505]",
					--"We permitted the deserters to flee. Their shame will be a greater burden than any we could ever think to bring upon them.\n      Our love for the cities of Denir was immeasurable, our cooperation with them and dedication to their growth cannot be overstated. But they desired sole growth - while our own was to be monitored, tracked, and ultimately stopped. It was our blood that imprisoned the Goddess, and we must work diligently to make amends for the sins of our fathers in order to",
					--2
					"[$3506]",
					--"free her. That cannot happen without advancement. But the greed of Denir sought to restrict that advancement, and when they could no longer hold us back, they planned their attack.\n      They call us monsters. We did what they intended to do, only we did it first. How quickly their love turned to hate, their hearts and minds poisoned by an acid that rivals the venom of even the vilest of spiders, that stings deeper than any bite from a great scorpion.\n      The people of Denir pulled the first knife, plunging it deep into our souls the moment they sought to betray us.",
					--3
					"[$3507]",
					--"The wind is calm in Eldeir once more, but our men still suffer. They suffer from the virus of guilt, a burden that is not theirs to bear. Often I have seen them at the Sacred Lake, speaking with the trapped goddess and praying for her embrace, her guidance, her approval.",
				},
			},
			{--5
				Title = "Letter from Helm",
				Pages = {
					--1
					"[$3508]",
					--"A cloud has gathered since dawn, smothering sun and sky in an unyielding darkness. The warm sand has cooled between my toes in the hours since I first stepped onto Mer Beach. So strongly do I wish for the soils of home.\n      The people of Helm - the last of the Denirians - have taken me in. My anger fuels theirs, and their anger fuels mine.The ocean is swelling, as if our anger is fueling its own. In every direction, grey sea meets identically hued grey sky, the waves wailing in",
					--2
					"[$3509]",
					--"an agony that can be heard for miles. The thirst for revenge gnawing unceasingly at its throat, consuming what it pleases.\n      The people of Eldier are no longer people. A human stops being a human when it loses its humanity. They are the trolls of Contempt, the beatles of Deception, the ghouls of Ruin, the Demons of Corruption. They are just another monstrosity spreading their plague across the lands of Celador, destroying all within its path. And they will be stopped. This war will end when Eldeir lies in ashes. And Helm will be the ones to burn it down.",
					--3
					--"",
				},
			},
		},
	},
	--
	-- DungeonsOfCelador
	--
	HallsOfCorruption = {
		Name = "Halls of Corruption",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Terminus",
				Pages = {
					--1
					"[$3510]",
					--"Aria was once a massive collective of regions, home to all varieties of hero and horror, united as a single cluster glowing bright and eternal within the aether.\n      One of Aria's regions was the land of Terminus, a forsaken, alien mass of horrors designed to ensnare the fragile sanities of mortals who dare to stand in places the gods themselves have trembled.\n      Terrors crawled the desolate wasteland, unleashing biological mutations and horrors upon",
					--2
					"[$3511]",
					--"unsuspecting travelers, corrupting their bodies in such a way that they were beyond identification, stripped of any semblance of humanity and gripped in an eternal pain until the Gods found within them a mercy to end their suffering.\n      Terminus serves as a breeding ground for infecting mortals with horrifying plagues that corrupt both mind and body, and to those unfortunate enough to find themselves wandering the wasteland, death was not avoided, but pursued.\n      Once, in an age long dead, the land was inhabited by a race of brilliant,",
					--3
					"[$3512]",
					--"magical scholars who molded the landscape with the sole purpose of studying those who happened upon it, creating a collection of illusions and alternate realities customized uniquely for the individual visitor.\n      Several travelers sought out this wonderland of terrors with the intention of protecting the rest of Aria from it, to keep the horrors of Terminus from bleeding through the barrier that shielded the gateway. Those travelers were never seen again.\n      It was not long before the mysterious, native race of geniuses who called Terminus home shattered",
					--4
					"[$3513]",
					--"the womb of reality that birthed this evil place, ascending into a broken pantheon of mad gods.\n      They have been called many names by their victims: The nameless ones, the others, the dreamless, the Xor.",
				},
			},
			{--2
				Title = "The Xor",
				Pages = {
					--1
					"[$3514]",
					--"The Xor were the first lifeform of the cluster of Aria to evolve beyond their mortal existence, reaching a level of understanding that allowed them to exist in a realm that is beyond the typical plane of existence, and beyond the control of even the Creator Gods.\n      They were mad with power, dedicating themselves to experimenting on - often through torture - the younger races that existed across Aria.\n      The entrance to The Xor's world of horrors, Terminus, was an imposing",
					--2
					"[$3515]",
					--"stone structure that required any who enter to be specially attuned for travel. Few humans possessed such an ability, and the structure lay dormant for centuries.\n      But calm is seldom for long, and the sadistic gods sought to lure their prey to the wastelands of Terminus. The nature of man is inherently flawed, and all who exist seek more than what they currently possess.\n      When a magical portal appeared in the heart of Celador without explanation, and foreign travelers spoke of the treasurers, the titles, the promise of immortality that lie",
					--3
					"[$3516]",
					--"within, most adventurers were inclined to explore further.",
				},
			},
			{--3
				Title = "The Gateway to Hell",
				Pages = {
					--1
					"[$3517]",
					--"The portal led not to a region of riches and life everlasting, nor did it lead to a greater understanding of existence. There was none of the promised path to ascension or godhood. Rather, wayward travelers found themselves within the belly of a massive, terrifying beast.\n      Dreadful and strong, the beast had no face beyond mouth and teeth. Its size was so great that each temple across the world of Aria could find a home within the beast with room to spare. There were no grand temples",
					--2
					"[$3518]",
					--"inside, however.\n      Within the beast, the Xor had constructed gruesome chambers of experimentation. While man whispered of his fears of Hell, warning of the horrors that await the most evil among them once taken by Death, the Xor took note. Within the beast, men and women would encounter the culmination of their race's fears, a custom hell designed specifically to torment the living for eternity.\n      It was through their superior intelligence that the Xor were able to construct this piece of Terminus on Celador, a gateway to the aether",
					--3
					"[$3519]",
					--"between worlds where even the Creator Gods themself could not build, destroy, observe, or interfere.",
				},
			},
			{--4
				Title = "The Halls of Corruption",
				Pages = {
					--1
					"[$3520]",
					--"The Xor referred to their temple of horrors as the Halls of Corruption, as they considered the species of human to be no more than a corrupted, twisted caricature of the gods.\n      Deep inside a great pit of darkness, humans are beaten endlessly by great demons. Those who manage to escape the clutches of the creatures that pepper the halls find themselves entrapped in a flaming pit guarded by demons.\n      The worst of the punishments within the Halls of Corruption were",
					--2
					"[$3521]",
					--"reserved for the strongest of warriors. The Xor had developed a particularly cruel form of punishment, one in which they would rip a soul from the visitor's body, entrapping them for eternity within a jar. It is through this process that the Xor are able to harness the knowledge, skills, emotions, and fears of the adventurer for further study.\n      The sole exit, a portal beyond the greatest Demons, promised a sense of freedom. Yet any who were to enter would find themselves in the wasteland of Terminus, which would be a greater horror than what was",
					--3
					"[$3522]",
					--"encountered within Corruption. But no humans lured into Corruption ever made it that far.",
				},
			},
			{--5
				Title = "Souls of the Damned",
				Pages = {
					--1
					"[$3523]",
					--"The Shattering of Aria destroyed access to the Halls of Corruption from the region of Terminus, leaving only access to the twisted temple from Celador.\n      The riches which had once been promised as an act of deception now lie within its walls, left behind by the fallen.\n      Bars and spikes still adorn the walls, while chains and cages pepper the twisting halls and sprawling chambers, serving as both reminder of what once occurred inside and warning of what the creatures who call the space",
					--2
					"[$3524]",
					--"home are capable of doing.\n      Rivers of blood still flow beneath the ground, still cascade from above, carrying the lifeforce of centuries' worth of victims throughout the great beast. The souls of the tortured remain throughout the Halls of Corruption, separate from their long decayed bodies yet condemned to an eternity of both physical pain and mental clarity from within their glass prisons.",
					--3
					--"",
				},
			},
		},
	},
	HallsOfContempt = {
		Name = "Halls of Contempt",
		CompleteTooltip = "Valus",
		Tooltip = "\nUntranslated. An expert of old languages may be able to read it.",
		Entries = {
			{--1
				Title = "E'en Uouu Gevk",
				Pages = {
					--1
					"[$3525]",
					--"Jthou et at e nruuk e wentha tuklut e nuuk nurrmnga loukt leki tonionu gruum et nuuk uku uris tuuppu oluk niutha druutn nuurra ukei to e den dnop litha ni utha kutuhuruk e fotonud ni toni nukrr ugun nuunes\n\ntuhonp uku goklnt wepi ouutha tuhonp ukin enuk wnrri et nuunes wnrri tnutni gruum et thaue kstha druutn uouu risuuku ta tuhonp nuuk uku kstha fouuk tket nurrmnga kui",
					--2
					"[$3526]",
					--"Gouuk ta ni oe touudt et regntha ukutha thauea a ukia uris utha 'i'in kiutha w toni goouuk uouuk nukrr nuunes nuurku nuunes wnrri nukkr tkutha't uko uouu inn toni et uppuumuk\n\ne nuurku a gruum goklnt nuumnga jthou et e tothat wentha rukketha gunmu e dnop uket tuhuupeuk iuthaan felku kiutuh fnga druutn risuuku tonga ukei ulrisuuku gevk tuhuuf puma kou uu guluutho oluk usnoli uket tuhuupeuk fnga iuthaan wdk gothat",
					--3
					--"",
				},
			},
			{--2
				Title = "Vrothru Ra",
				Pages = {
					--1
					"[$3527]",
					--"E'en keggrr rust you'ris dnop laquiakaz snuurgahata kira Vrothru Ra\n\nIizukuz et e nuurku wepia  tuhonp kui qapzii hohlula ouutha puma thauea goouuk et nukrr tuhuupeuk uris uku oluk gruum a gogklnt snurgahlata fnga gruum toni et uppuumek wnrri wdk utha dnop e duun fin y eni gonuuchek vralut machhu hazuhaan taudu hake et hakuu et asshukh uumnga jthou et e tothat wentha et e touuthat went loukt leki tonionu gruum et nuuk uku\n\nUuou r'en bruug gunmu ki tlop fouuk tanurrow gouuk dnop gunmu toudt wenta tuklut thauu wnuurriu gevukk kuura ala e dghurata huuratat Vrothru Ra",
					--2
					--"",
					--3
					--"",
				},
			},
			{--3
				Title = "Tohouu et Uksrrou",
				Pages = {
					--1
					"[$3528]",
					--"Gouuk ta nuumnga jthou et e tothat wentha ni regntha ukutha thauea a ukia uris utha 'i'in kiutha w toni goouuk uouuk nukrr\n\nuko uouu inn toni et uppuumuk e nuurku a gruum goklnt oe touudt et nuunes nuurku snurghaluuta uku ala nuunes wnrri nukkr tkutha't fegnta luuk regntha aouu louk nuunei lultha ta nuuk ukin ukei druutn thauen tknknga  ukutha youddt ouut quiz oe e et e'ez ukun'tha narak wanna",
					--2
					"[$3529]",
					--"Uouu machhu hazuhaan taudu hake oluk et hakuu et asshukh wepia uumnga jthou et e tothat wentha et e touuthat went gogknt loukt leki tonionu gruum et nuuk uku tuhonp ukin ouutha nuunes\n\nuku goklnt uksrrou druutn tuhon'p tkutha't regntha e nrruk dunp novk kos uut nuki gonga thaue nuxthra keg kos",
					--3
					--"",
				},
			},
			{--4
				Title = "En'gonga Keggrr",
				Pages = {
					--1
					"[$3530]",
					--"E'envuurk oluk et niutha wnutha druutn nuurra a ukei to e tuhonpuk den dnop lithauouuu ni utha welu risthauurm kutuhuruk e fotonud ni\n\ntoni nukrr ugun nuunes ta loukt tuhonp uku goklnt wepi ouutha tuhonp suttu en'gonga keggrr puma you'ris louutu tonionu wenthu joki ueur ta you'ris gunmu ki tlop fouuk tanurrow gouuk dnop",
					--2
					"[$3531]",
					--"Druutn wnrri nukkr tkutha't uko uouu inn toni et uppuumuk e nuurku a gruum goklnt nuumnga jthou et e tothat wentha rukketha gunmu e dnop\n\nuket tuhuupeuk iuthaan felku kiutuh fnga druutn risuuku tonga ukei ulrisuuku gevk tuhuuf puma kou uu guluutho oluk usnoli uket tuhuupeuk fnga iuthaan wdk gothat.",
					--3
					--"",
				},
			},
			{--5
				Title = "Tanurru Ta Gouuk",
				Pages = {
					--1
					"[$3532]",
					--"Gevvk you'ris gunmu you wentha e'n dnop louutu puma ukan keggrr ukan ouuo gevk\n\nukrri loutha fron nuunei loukt leki uppumuk ksthafouuk nurrmnga errs thautuhs leki grus demalluu\n\nWnrriet et nuurku e tothat wentha rukketha gunmu e dnop uket tuhuupeuk iuthaan felku kiutuh fnga druutn risuuku tonga",
					--2
					"[$3533]",
					--"ukei ulrisuuku gevk oukt leki tonionu gruum et nuuk uku tuhonp ukin ouutha nuunes uku goklnt uksrrou druutn tuhon'p novk kos uut nuki gonga thaue nuxthra keg kos\n\nEn'gonga keggrr puma you'ris louutu uouu inn toni et tuhuuf puma kou uu guluutho oluk usnoli\n\nuket tuhuupeuk fnga iuthaan wdk gouuf keggrr puma you'ris",
				},
			},
		},
	},
	HallsOfContemptTranslated = {
		Name = "Contempt (Translated)",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Wise Ones",
				Pages = {
					--1
					"[$3534]",
					--"The world was once one, and while none of the Creators' children lived in harmony, they were much more inclined to live in tolerance.\n      All beings were created under the billowy cloak of equality, respected and loved by the Creators, and given the tools to thrive and, given enough time, ultimately pave their individual paths toward ascension.\n      The Orks were a well respected people during this time: sophisticated in mind, magical in blood, proud of heart. Their imposing size often",
					--2 --'an adult human male' for 'a fully grown human man', 'coveted seats on councils' for 'them seats among the most coveted of councils'
					"[$3535]",
					--"caused others to cower before them, as an Ork child was often observed to be nearly the size of an adult human man.\n      The unique armor crafted by their fabricators and metalworkers were adorned with massive spikes more than twice the size of a human head. These spikes were not to assist in war, however, but were used by the people as a tool for gathering while completing other tasks.\n      Their serene and peaceful demeanor quickly won them favor among all the living, and their superior intelligence granted coveted seats on councils across all the races of Aria.",
					--3
					--"",
				},
			},
			{--2
				Title = "Vrothru Ra",
				Pages = {
					--1
					"[$3536]",
					--"The most respected and revered of all ancient Orks was Vrothru Ra, a supremely intelligent being who aided the Wayun following the attack on their people by the Nameless Ones.\n      The Wayun, also known as The Strangers, are credited in the histories with the creation of the Gates of Aria, which granted all who had been kissed by the gods the ability to travel between the realms of the great cluster.\n      It is said that they were forced to invent a method of travel between the shards in their home cluster, with",
					--2
					"[$3537]",
					--"the gateways being intricate enough to stump even the most intelligent of pursuers, allowing them to flee from world to world to escape attack. For centuries they fled from cluster to cluster, hiding themselves from the Nameless Ones they knew, one day, would come for them.\n      But it was Vrothru Ra who first developed such methods of travel. He required neither stone nor enchantment, as his intimate attunement with nature granted him the ability to fashion these gates from the earth, his abilities and discoveries breathing the sweet wind of",
					--3
					"[$3538]",
					--"evolution across the lands. And then, in a single moment, a mere flicker in the candle of time, all was erased.",
				},
			},
			{--3
				Title = "The Death of the Orks",
				Pages = {
					--1
					"[$3539]",
					--"That moment was as if the sun itself had fallen to the surface of Aria, splintering the world and fracturing the magics that united all.\n      Within seconds, millions of the Creators' children were consumed in inferno, a ring of fire racing across the world's surface, massive chunks of earth erupting into the aether. And then, in one moment more, all was still.\n      A wave of elemental fury, as a gift of the Creators, cloaked the remaining in a shroud of protection. The worlds,",
					--2 --'with hope of repairing what had' for 'with the hope of one day repairing what had', 'But many were spared, and had hope bloom in the hearts.' for 'But many survived, and hope bloomed in the hearts of those who were spared'
					"[$3540]",
					--"once united, were now islands in the aether. But many were spared, and had hope bloom in the hearts.\n      Those who had traveled beyond their realms were now left stranded on foreign soil, and none had traveled farther or more extensively than the Orks. Those who found themselves calling Celador, Shard of the Humans, their new home sought to carve out a space for their own.\n      Deep within the forests of the East, the peaceful Orks established a modest village, dedicating themselves to a pursuit of knowledge with hope of repairing what had been destroyed.",
					--3
					--"",
				},
			},
			{--4
				Title = "The Halls of Contempt",
				Pages = {
					--1
					"[$3541]",
					--"The Orks lived in harmony with the human tribes of the forest for years, aiding them in their individual missions. But time has a way of turning heart to stone, and no being's heart can turn with as much speed and fury as that of a human's.\n      A fire rippled within them, one which rivaled the heat the great fire the day the world shattered. A fire that took nearly as many lives as the great fire did. Man turned on Ork. For some the war was for the favor of their god, the one which grew from the depths",
					--2
					"[$3542]",
					--"of the swamp.\n      For most, however, the war was for history, to have the ability to establish their own truth, a truth which was not earned. The most powerful weapon man can wield is that of the pen. It is with this weapon that man strips from his enemy his humanity, his dignity, his legacy.\n      The few Orks who survived the assaults were driven to the farthest corner of the deep forest, and it was with only the aid of their superior size and strength that they were able to erect a fortress of protection deep within the earth. It was here that",
					--3
					"[$3543]",
					--"they lived in the shadows, while man spread false stories of their brutish savagery. And it was here, within the Halls beneath the trees, that their Contempt sprung roots like that of the mighty tree, reaching to all corners of the shard of men.",
				},
			},
			{--5
				Title = "The Dream of Revenge",
				Pages = {
					--1
					"[$3544]",
					--"The bodies of the ancient Orks grew weak in their years of exile, their minds faltered without the purpose of advancement. Over the centuries, they welcomed all into their base, as long as two simple criteria were met. First, you were to be a friend of nature. Second, you were to be an enemy of man.\n      The hierarchy within the Halls of Contempt is fairly nebulous. Honors are not bestowed based upon superfluous factors like foes killed in battle or noble blood. The skills with which a brother or sister excels is ",
					--2
					"[$3545]",
					--"what they do.\n      Those with superior aim assume the role of thrower, while the most imposing in size are tasked with guarding the more sacred areas of the fortress, such as the burial pit that serves as the final resting place for the fallen Orks and their brothers. When word reached the Orks that the tribes who had slaughtered them had fallen, the ensuing celebration was fleeting.\n      The tribes had fallen to a stronger, more dangerous evolution of the human race. They were not to be trusted. The world is now more treacherous than ever it has been. So",
					--3
					"[$3546]",
					--"deep with the Halls of Contempt the Orks and their allies wait, growing their numbers until such a time that they can take back what had been stolen from them.",
				},
			},
		},
	},
	HallsOfRuin = {
		Name = "Halls of Ruin",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Necropolis",
				Pages = {
					--1
					"[$3547]",
					--"Not all wicked places have a wicked beginning.\n      The Halls of Ruin once served as a serene underground necropolis, a sacred tomb in which the denizens of Celador who passed from the realm of the living to the realm of the dead could rest peacefully. Every warrior who has succumbed to death, every nobleman and noblewoman, every scholar, every notable Celadorian, rests within its walls.\n      A small army of volunteer laborers helped to construct the great",
					--2
					"[$3548]",
					--"mausoleum, as well as to catalogue the previously buried citizens who qualified for relocation to the Halls. In return, these volunteers were reserved a space in the tomb once their bodies were prepared to return to the earth.\n      The upper level is a labyrinth of hallways linking the burial chambers for those who sleep their eternal sleep under the God of life and death, Kho. The eastern wing houses Celador's most courageous warriors, while the northern section has been long reserved for those who were ordered to be inturned in Ruin by the Gods themselves.",
					--3
					"[$3549]",
					--"The lower level of the Halls of Ruin was reserved for the burials of stewards and magisters, those who dedicated their lives to the welfare of their fellow Celadorians.\n      A great library was constructed in the heart of this level in honor of those buried in the adjoining tombs, and scholars from all across Celador would travel to the Halls of Ruin to study in the company of the dead.\n      The library houses a wealth of information regarding the rich histories of not only Celador, but all the shattered kingdoms and the magics harnessed by those who lived within.",
				},
			},
			{--2
				Title = "Death to Life",
				Pages = {
					--1
					"[$3550]",
					--"The body dies before the soul does. In the moments that fill the expanse between death and decay, the soul wanders down a foggy road to the clearing of eternal rest. Death is yet another path to walk, but who is to say it should be the last? Little is known of the path the soul travels on its journey to eternal death. This was one of the topics the Healers of Celador long studied.\n     The Healers of Celador were a collective of the wisest and most powerful of scholars in all of Celador,",
					--2
					"[$3551]",
					--"residing in the great stone fortress that was located where the city of Valus now stands as they dedicated their lives to the study of healing the wounded and afflicted in order to save lives.\n      Their goals were noble, but their methods were sinister. The powerful mages often brought intentional inflictions upon unsuspecting citizens in order to perfect their craft, but it was through these brutal methods that they were able to establish a reliable incantation for returning life to those who had passed.",
					--3
					--"",
				},
			},
			{--3
				Title = "Ymar Stillheart",
				Pages = {
					--1
					"[$3552]",
					--"There were strict guidelines in place for resurrection in order to ensure that the practice was moral. First, the soul must still be tied to the body, and no decomposition must have begun. In order to guarantee the morality of what they were doing, it was decreed that none who had died more than 15 minutes earlier could be brought back.\n      Second, the death must have been the result of the will of a mortal, not the will of the Gods. Those who passed on as the result of a natural death, an accident, or a force of nature were to",
					--2
					"[$3553]",
					--"be left to rest.\n      Ymar Stillheart was a promising young scholar who studied the art of manifestation with the Healers of Celador for most of his life. Ymar thrived under the tutelage of the most powerful mages, Asteria and Saehrimnir, quickly rising through the ranks in order to become one of the most skilled mages in the base.\n      He was tasked with the most critical of the trials in Valus - the trial of restoring life to the dead. Countless young mages had sacrificed both life and mind to such research. The act of resurrection is one which drains",
					--3
					"[$3554]",
					--"from the caster a significant amount of energy.\n      Those who enter into the practice underprepared find death to be a blessing, as their minds are drained and their bodies left weakened shells. Yet with each successful resurrection Ymar performed he grew stronger, not weaker.\n      He began taking nightly trips to the cemetery, perfecting his skills on some of the less fresh corpses when newly passed were unavailable to him.",
				},
			},
			{--4
				Title = "The Restless Dead",
				Pages = {
					--1
					"[$3555]",
					--"The other mages viewed this particular manipulation of the dead as wicked, but such moral qualms were beyond Ymar's concern. He fled Valus before he could be punished, seeking refuge in the only place capable of providing him with the literature - and the bodies - he required to complete his studies: The Halls of Ruin.\n      Ymar lived for decades in solitude deep in the necropolis of Ruin with the dead as his only companions. Mad with obsession and driven by the desire for more knowledge, Ymar resurrected",
					--2
					"[$3556]",
					--"those laid to rest who had passed on to the next realm, seeking to identify the exact moment at which the soul departs from the body.\n      He was ravenous in his dedication to preserving and maintaining knowledge with little concern for the origins of said knowledge.  Ymar felt that drinking from the fountain of knowledge was a noble endeavor, one which would benefit mankind as a whole.\n      Thousands had been interred in the great tomb since Celador's birth, and in the years that Ymar resided within the Halls of Ruin, most of those laid to",
					--3
					"[$3557]",
					--"rest were brought back from eternal slumber, cursed to wander the tomb for eternity.",
				},
			},
			{--5
				Title = "The Undead Child",
				Pages = {
					--1
					"[$3558]",
					--"Ymar Stillheart's most vile act of necromancy was the one act which was inspired by neither a desire of power nor the pursuit of knowledge, but rather one which was inspired by a deep compassion.\n      The denizens of Celador had long avoided The Halls of Ruin and the dangerous undead hordes that roamed its corridors, and for decades Ymar Stillheart's only company was the dead. That is, until one day a woman appeared, her despair so great that it nearly manifested physically, an aura",
					--2
					"[$3559]",
					--"of woe and grief surrounding her cloaked figure.\n      In her arms was a still bundle of fabric, a bundle that Ymar identified long before words broke the silence that had been punctuated only by the sobs of the heartbroken mother.\n      The lines that had been crossed during Ymar Stillheart's studies and practices had been many, and yet all paled in comparison to the crimes against humanity that he committed on the day he returned breath to the infant girl. The child could have become an abomination, a dangerous and vile undead harbinger of such",
					--3
					"[$3560]",
					--"horrors that the gods themselves would cower before it.\n      Such sins had never before been committed, and yet Ymar felt an immediate kinship with the woman, with the child. The girl was to be something great, and for her to be so, a great risk would need to be taken.",
				},
			},
		},
	},
	HallsOfDeception = {
		Name = "Halls of Deception",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Star Dweller",
				Pages = {
					--1
					"[$3561]",
					--"Since time immemorable, man has worshipped his chosen god. Some have praised Tethys, as she has reigned supreme over the inhabitants of Celador for centuries, while others have offered their souls to the Creators, as it was the twin goddesses who created not only Tethys, but the other Titan Gods as well. Others still worship that which nourishes.\n      The prayers and followings of all these combined pale in comparison to those who call the former Peska home, the followers of the Star Dweller.",
					--2
					"[$3562]",
					--"The followers existed long before the fall of Peska, meeting in secret beyond the Scaled Path, within a cave that rests at the foot of the Gleaming Ridge.\n      They worshipped their gods where no eyes could observe them, their beliefs fueling one another. High priests gathered the followers under a blanket of stars, the aether swirling above and below, to preach of sacrifice, death, and the rituals that empower the euphoria of the chaos gods.\n      They spoke of the world of darkness that existed before, and they mock those who speak of The Shattering as if it were fact. To them, Celador has",
					--3
					"[$3563]",
					--"always been just as it is - separate, alone, and glorious in its freedom and independence.\n      Those who speak of Aria speak of myth, an idyllic, empyrean elysium in which all manner of life existed in harmony. This fantasy is not representative of the true nature of being. Men and women have long been driven to cruelty, to inflicting pain, and even to seeking out pain. With this understanding of human nature - and the understanding of the nature of other races of beings who wander the land of Celador - the followers of the Star Dweller seek to gain favor with",
					--4
					"[$3564]",
					--"their god not by abandoning their true nature, but by embracing it.",
				},
			},
			{--2
				Title = "Kalanathu's Followers",
				Pages = {
					--1
					"[$3565]",
					--"The followers of the Star Dweller believe that the Elder God, Kalanathu the Star Dweller, created Celador from Flam and Death of an elder world. All life results from death, and thus, from decay. Kalanathu will return for his followers, guiding them to a higher plane of existence and aiding in their ascension to godhood.\n      Celador exists as a trial, as all races who have ascended to the ranks of the gods have faced such trials. The path to ascension requires a being to face their most notable foe, and no foe is",
					--2 --'once' for 'It was only when', ', they consumed' for 'that they would consume'
					"[$3566]",
					--"greater to humans than the earth upon which they wander.\n      Once the land of Celador has been destroyed, those who served the Elder Gods faithfully may ascend. The followers believe humanity as a whole to be cursed for their abandonment of the nature gifted to them by the Elder Gods, and only through sacrifice may they please their Gods. If all life results from death, then it is through death that they may attain a more meaningful life.\n      Once the followers established this specific belief system, they consumed the flesh of those they sacrificed.",
					--3
					--"",
				},
			},
			{--3
				Title = "The Rise of Sibillia",
				Pages = {
					--1
					"[$3567]",
					--"The first woman to join the ranks of the Cult of the Star Dweller was not a woman at all, but rather a child. The young girl had lived only 12 years by the time she had arrived, exiled by her brother from the land her family had built and ruled over.\n      She was Ciarra of Peska, and she was believed to be the ultimate sacrifice to the gods. With her blood spilt, a pathway to ascension was guaranteed for the followers of Kalanathu.\n      Ciarra was sacrificed to the gods on three separate occasions, and on each",
					--2
					"[$3568]",
					--"of those occasions she was believed to be dead, her body left on the Altar of Yihosglok.\n      The following morning, however, Ciarra would awaken in her cell within the dungeon, having awoken from the slumber of death to embrace a new day. After her third awakening, the followers of Kalanathu no longer attempted to sacrifice her, for you cannot sacrifice a god.\n      Ciarra grew into a woman in the dungeon, taking on the name of Sibilla and establishing herself as supreme leader of the cult.",
					--3
					--"",
				},
			},
			{--4
				Title = "Ritual of Sarkassishth",
				Pages = {
					--1
					"[$3569]",
					--"The Altar above the Great Sand Temple was a magical place. The cultists believed the Temple was a structure gifted to them by the gods, granting them a safe location to repay the sacrifice the gods had themselves made during the creation of Celador.\n      Sibilla's charm belied her true nature, giving peace to the entirety of Celador's masses. They saw her as the voice of the gods, an oracle and prophetess who could be killed by neither blade nor fire. Citizens of all manner of wealth traveled from",
					--2
					"[$3570]",
					--"across the land of Celador to beg for an audience with the High Priestess, as it was only through her that they could beg the favor of the gods.\n      The citizens of Celador paid dearly for their access to the Priestess, and the Cult amassed a great wealth. Their fortune was kept safely in their hideout to the West, guarded by both manmade traps and great beasts.\n      Those unable to pay in riches paid in other methods, sacrificing blood and flesh to the gods in a horrifying ritual of mass human sacrifice known as Sarkassishth.",
					--3
					--"",
				},
			},
			{--5
				Title = "The Fall of Peska",
				Pages = {
					--1
					"[$3571]",
					--"The final sermon took place during the summer, and the final petitioner of the gods was Terrowin, leader of Petra.\n      Terrowin traveled in the dark of night to meet with Sibilla, his contribution to the Cult of the Star Dweller was so great that he required all the horses of his army to haul it to the Island which connected green earth with aureate sea. Peska rose like a mountain over Petra, and the smaller city could never hope to conquer its sister in the Barren Lands without the assistance of the gods themselves.",
					--2
					"[$3572]",
					--"It was during this ceremony, steeped in mystery even to this day, that Terrowin became the first human to gain access to water magic.\n      While little is known of the exact course of events that followed, the survivors of Sibilla's quiet reign and influence speak of the sealing of the Great Sand Temple, as if the Creator Gods had lashed out in fury at Sibilla's role in granting elemental magic to a mortal. Sibilla is said to be trapped within the ruins of the Sealed Temple, destined to live out her immortal life in a prison far from man.\n      Within two moons, Peska fell to",
					--3
					"[$3573]",
					--"the smaller Petra. Terrowin, aided by this mysterious and unparalleled magic that had been granted to him by Tethys in the final ceremony, conquered the large town easily.\n      Once the dust had settled and the armies had returned to their home, the Ruins of Peska found itself inhabited by a new people - The Cult of the Star Dweller. The dungeon to the west remains, serving as home to the riches Sibilla had gathered from desperate petitioners, protected by traps, illusions, and the most dangerous creatures in all of Celador.",
				},
			},
		},
	},
	Catacombs = {
		Name = "Catacombs",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	Monolith = {
		Name = "Monolith",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Arcane Order",
				Pages = {
					--1
					"[$3574]",
					--"Every world that has existed since time itself has existed was created with a singular goal - to aid a variety of beings in their ascension to godhood.\n      In so ascending, these beings would find themselves architects of their own worlds, free to rule, guard, and guide the beings under their control in any way they saw fit.\n      Three races birthed upon Aria had ascended - the Xor, the Pedesii, and the Wayun. Following the Shattering, however, the parameters changed from ascension to merely surviving.",
					--2
					"[$3575]",
					--"In order to ascend as a being of Aria, one must have discovered a remarkable feat of travel from their home island to others, but since the Shattering such travel had been deemed impossible.\n      Thousands of years passed, and all who remained across all shards of the shattered world found contentment in merely existing. On the shard of Celador, home to the humans of Aria, a collective of mages were assembled to locate, secure, catalogue, and protect the remnants of magic that landed upon Celador during the Shattering.\n    In doing so, they hoped to both protect and enhance the use of magic.",
					--3
					"[$3576]",
					--"This collective of mages came to be known as the Arcane Order.\n      Among the ranks of the first Arcane Order was Jianna, a promising and ambitious young sorceress who quickly rose among the ranks of the Order. She was known to be a bit radical in her practices, focused on experimentation and manipulation of the magical remnants where others were content to secure and protect them.\n      To many, however, Jianna's radical experiments, while potentially dangerous, led to significant advancements that impacted the progression of magical expertise",
					--4
					"[$3577]",
					--"across Celador. Due to these advancements, the Order permitted Jianna to continue her work with impunity.",
				},
			},
			{--2
				Title = "A Curious Portal",
				Pages = {
					--1
					"[$3578]",
					--"The Arcane Order was based in the land that would come to be known as Valus, long before the land of Valus existed. Teams were sent to all corners of the land to collect foreign magical relics to be returned for further study, while others remained behind to guard and experiment on these relics.\n      Jianna spent most of her life at the base of the Order, but was occasionally tasked with exploring the immediate area, particularly when evidence arose of a nearby source of foreign undiscovered power.",
					--2
					"[$3579]",
					--"The Order had secured a variety of alien weapon artifacts over their first 10 years, and Jianna had taken on the role of using these artifacts in particularly gruesome experiments in order to document their effects on beasts and creatures.\n      One in particular, discovered deep in the caves that cradle what is now Valus Cemetery, was a blade imbued with such power that a mesmerizing purple haze of energy danced across it. The moment Jianna's eyes locked with the energy something inside her changed.\n    Jianna grew increasingly unnerved,",
					--3
					"[$3580]",
					--"often abruptly ending conversations with her colleagues, stating that the piece was calling to her.\n      Jianna's work, which had always been considered just outside the realm of acceptable, had to this point been permitted as the ends had typically justified the means.\n      She often took the blade to the caves where it had been discovered, disappearing for days without explanation and raving about visions she struggled to describe, filling book after book with indecipherable writing in languages unknown to any but herself.",
				},
			},
			{--3
				Title = "Assemble the Believers",
				Pages = {
					--1 'escalated. She' for 'escalated, and' / 'which granted' for ', one which granted'
					"[$3581]",
					--"Jianna's experiments escalated. She secretly ventured into the wilderness in search of lost travelers, deserters, and refugees on which to test the weapons, leaving a trail of bodies in her wake wherever she went.\n      Upon her final return to the base, she spoke of a magical portal which granted her insight into a world not of Aria. Her actions and ramblings had grown far too erratic, and Jianna was expelled from the Arcane Order, who assumed the young sorceress would waste away in her solitary madness.",
					--2
					"[$3582]",
					--"She did not waste away, however. What the Order did not realize was that Jianna was not mad, not in the typical sense of the word.\n      She was corrupted, by a magic created for the sole purpose of corrupting. Her mind was frantic in its attempt to understand what she'd seen, but it was still clear, still functional. And Jianna herself remained the charismatic, beautiful young sorceress that she had always been.\n      Jianna easily identified the most vulnerable across Celador. They were quickly enamored with her,",
					--3
					"[$3583]",
					--"following her with promises of godliness and ascension and a place of belonging, one which was greater than their humble, sad existences.\n      The Shattering of Aria had created 5 new gods in the Titans. Jianna believed that, in Shattering Celador itself, she could ascend to a level of godhood equal to the mysterious gods on the other side of the portal. In order to do so, she would require access to more of their magic.",
				},
			},
			{--4
				Title = "The Gateway",
				Pages = {
					--1
					"[$3584]",
					--"Jianna and her followers began construction on a monument, which they referred to simply as the Monolith. Jianna worked tirelessly in her lab, driven by an endless stream of dreams and visions that revealed the indecipherable ancient symbols which would later be etched onto every surface of her tower.\n   As the members of the cult crew in both number and madness, the Monolith took shape. Its purpose was to guard the portal which connected Celador to the mysterious Echo realm,",
					--2
					"[$3585]",
					--"but also to grant the followers the ability to harness the mysterious magic which existed on the other side.\n      Within the belly of the tower, Jianna's followers began to construct misshapen arcane creatures out of rock and the unstable forms of magic that they had begun to pull from the Echo world, giving them polished humanoid faces in an effort to impose some sort of understanding over them.\n      These arcane constructs were then commanded to roam the halls of the Monolith, guarding its secrets and protecting the cultists from curious outsiders.",
					--3
					"[$3586]",
					--"The cultists saw the completion of their constructs as total dominion over the strange magic they had harvested from Echo, and as such, began to take their plan one step further. They constructed a gateway that would link Celador and Echo, shattering Celador in the process and causing the two realms to overlap.",
				},
			},
			{--5
				Title = "The Failed Shattering",
				Pages = {
					--1
					"[$3587]",
					--"Jianna's followers underestimated the forces with which they were dealing. Their tampering with and manipulation of Echo's magic drew the ire of the entities from the other side of the portal.\n      The Echo magic was intended to corrupt, a way for the mad gods of echo to experiment on weaker races. They did not anticipate that a being would not only survive exposure to their magic, but understand it. The risk such beings would pose to these gods could not be permitted to be realized.\n      Once the gateway was ",
					--2
					"[$3588]",
					--"complete and Jianna's ritual to bind the two worlds was underway, a horrible, twisted entity burst through the barrier between the worlds with the intent of destroying Jianna and her cult.\n      This entity, dubbed \"The Watcher\" for its many eyes, immediately disintegrated Jianna while her followers could only watch in horror. Rather than destroy the cult, however, the gods which created and controlled The Watcher found a new use for them: it would twist and control them, just as they attempted to twist and control the Echo magic, and use them as ",
					--3
					"[$3589]",
					--"puppets to guard the Monolith. Their bodies, as well as their minds, became corrupted as their enslavement to this horrible new entity began.\n      While Celador itself was safe from the Great Shattering that Jianna and her followers had planned, much of the structure of the Monolith itself was fractured or destroyed completely.\n      The tower itself now stands as a shell, with its interior precariously held together by what remains of the unstable Echo magic the cultists were able to harness, which threatens to spill out into Celador at any given moment. And at its apex,",
					--4
					"[$3590]",
					--"The Watcher remains, stranded amongst it all.",
				},
			},
		},
	},
	--
	-- SecretsOfCelador
	--
	SecretJournal = {
		Name = "Secret Journal",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "First Entry",
				Pages = {
					--1
					"[$3591]",
					--"From the moment I was able to carry a pick, it was my duty to work the mines. Ever since the fall of Peska, Terrowin has demanded more of the people of Celador.\n\nA significant trade post has been lost, resources of the region of sand are now beyond our reach, guarded by the most dangerous creatures.\n\nThe men are loud and large, unwashed servants who take particular interest in my presence. I did all I could do to",
					--2
					"[$3592]",
					--"keep myself small, modest, invisible.\n\nShe had a beauty that was ethereal, and I loved her the moment I first saw her. I watched her eyes scan the cavern, passing over each laborer until they settled upon me.\n\nSo beautiful those eyes were, as if the stars themselves abandoned the heavens to rest within them simply to be near her. Behind those eyes, a wildness, a frenzied passion that could not be of this world.\n\nShe had a steadiness to her, as if all",
					--3
					"[$3593]",
					--"the storms of Zephyr were little more than an exhale upon her cheek.\n\nShe called upon me, and I followed. We have traveled Celador together for years, calling upon only the most worthy. I cannot see with her eyes, but I trust what lies behind them. She tells us of the magic in the fountain, and the things she can do - they are nothing short of a gift from the Gods themselves.\n\n So today, we laid the first stone on what is to be our new home.",
				},
			},
			{--2
				Title = "Second Entry",
				Pages = {
					--1
					"[$3594]",
					--"For years we have served in the shadows. We have prayed, we have obeyed, and we have sacrificed our bodies to this monument. Tales from the North have no weight on our spirits.\n\nThe advancements they boast pale to our own. While they toast the ripples of a stone upon a lake we are carving new oceans.\n\nThe Red Witch told us they have trained animals to do the work of a",
					--2
					"[$3595]",
					--"man. Animals! Their discoveries have made them weak while ours have made us stronger, and none are stronger than the great sorceress Jianna. Their animals will not save them in the end.\n\nThe greed of Celador has restricted true advancement. Men and women alike forge their own paths with no concern for any but their own. In our service we have committed our lives to a shared calling. With no divergence, we have completed the work of thousands of men.\n\nWhen the world ends, we will",
					--3
					"[$3596]",
					--"survive. Jianna has opened her arms, holding us safe and free.\n\nPraises be upon us. Praises be upon Jianna the Red.",
				},
			},
			{--3
				Title = "Third Entry",
				Pages = {
					--1 'the doubt' for 'the tendrils of doubt'
					"[$3597]",
					--"Jianna has not slept for weeks. She has sacrificed her strength, her mind, her very soul. Few would accept such a burden. Even fewer could convince so many to accept that burden by their side. Yet here we are, growing in numbers by the day. This foreign magic powers her, and she in turn powers us.\n\nJianna has the favor of the Gods. Sadly there are those who resent her for this. The whispers are louder. The unfaithful must be punished before the doubt they sow corrupt us all.",
					--2
					--"",
				},
			},
			{--4
				Title = "Fourth Entry",
				Pages = {
					--1
					"[$3598]",
					--"The red one is restless again. Her every moment is spent in the heart of this prison, and she speaks in tongues which none of us have heard before. Her finite patience has been exhausted as the gateway has neared completion.\n\nToday I watched as my brother's body ran dry, his blood staining the cracks between the very stones he placed. He dared to test the whims of the corrupted, and it was that very corruption that struck him down.",
					--3
					"[$3599]",
					--"If these stones could speak, oh the horrors they would tell.\n\nWe are what they say we are. She is who they say she is. I fear for my soul should she be allowed to continue. Many still serve without question. But me, I am not long for this place. I am\n\n[This entry cuts off mid sentence. It has what appears to be dried blood on it.]",
				},
			},
			{--5
				Title = "Jianna's Entry",
				Pages = {
					--1
					"[$3600]",
					--"Heretic, they said! Mad, they called me! They dismissed me yet they fear me, they exiled me yet they envy me. It is in man's nature to fear and to envy. The fools.\n      This magic, little more than an itch at first, is growing stronger by the day. My face has bathed in the warmth of nearly one thousand rising suns since I first happened upon the Power of Echo. That itch in my brain, now a deafening refrain. Mad? Ha! I am not mad. I am enlightened. Ascended. Augmented.",
					--2
					"[$3601]",
					--"I am weak only for so long as my human form demands that I draw breath. There is a power within me. The stupid fools, treating magic as a science to be studied. I now see what they feared I would discover all along. I have the magic within me. I can draw from it at will. As it becomes stronger, so will I. So have I. If it bleeds, I can kill it.\n      Our gateway is nearly complete. The Gods smile upon the splendor of our stones. They smile upon the glory of a prophecy nearing fulfillment. They smile upon me.",
					--3
					"[$3602]",
					--"My fate is tied to this place, and as such it remains my chosen duty to extinguish the barriers that have separated the Gods' worlds. We must draw from them, and they from us.\n\nHeretic, they said! Mad, they called me! The fools.",
				},
			},
		},
	},
	CiarraJournal = {
		Name = "Ciarra's Journal",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "First Entry",
				Pages = {
					--1
					"[$3603]",
					--"They came for me before dawn, the soldiers who had watched over our family since before I was born.\n\nRufus, the big one, had given me my first grape when I was four.\n\nCharles, the one from Petra with the sunkissed hair, began teaching me the art of the sword just two years ago.\n\nAislynn, the one with eyes the color of midnight, the one who welcomed my brother into her room at night, the",
					--2
					"[$3604]",
					--"one who believed I did not know her secret while she set up picnics with bread and water in the square for me.\n\nThey all came for me, their faces stone, their eyes showing none of the sadness I had hoped to see. I knew now what I had always known. I was merely tolerated in this city. My blood, the only thing that granted me a second glance. Or a first one.\n\nThis morning they came for me, so that they could march me to my death. I was left upon a platform in the town square, vipers lashing below,",
					--3
					"[$3605]",
					--"guarding exit from all angles should I choose to attempt to flee.\n\nThis was my choice, a painful, slow, agonizing death by poisonous venom, or a quick, dignified, painless death by rope. I stood quietly as Charles placed the rope over my head. My eyes met his as he pulled it tight. He looked away. I did not.\n\nMy name is Ciarra of Peska, and my brother killed me today.",
				},
			},
			{--2
				Title = "Second Entry",
				Pages = {
					--1
					"[$3606]",
					--"I had died 100 deaths by the time I reached the age of ten. 200 by the time my brother had killed me.\n\nGrowing up in the great desert, the Barren Lands, an early death is as certain to most as the rise of the sun is to all. I feel the pain of my death, I feel the life leaving my body. All turns to black. And then I awaken, safely in my bed, as if from a terrible dream.\n\nWhen my brother killed me for the last time, he truly believed it to be the",
					--2
					"[$3607]",
					--"last time. For the next morning I awoke before the guards had begun their rounds, and I fled from my home in Peska for good.\n\nIn abandoning my home I abandoned my bed, and in abandoning my bed, perhaps I can also abandon this curse that is life? I have no choice but to travel to the most dangerous place I know, the haven of the cultists. My brother spoke of them often, rallying his people in his addresses against the cursed humans who live in the caves in the Gold Coast. ",
					--3
					"[$3608]",
					--"The ones who butcher men, women, even children, who slit the throats of any who dare to cross their path. A sacrifice to the false gods to which they pray.\n\nPerhaps they can release me from this prison of Celador, break my chains and send me to eternal slumber.",
				},
			},
			{--3
				Title = "Third Entry",
				Pages = {
					--1
					"[$3609]",
					--"The woman in the next cell likes to talk. Olivia, her name is. Or at least, that is her name now. She is too scared to tell me the name she was born with. If they were only going to kill her, she would scream her true name from the top of her lungs, she says. But Olivia is a slave here. They don't sacrifice the slaves, and they don't sacrifice the weak.\n\nWhat good is a weak body with weak blood to the gods? None, that's how much good.",
					--2
					"[$3610]",
					--"So Olivia, with her weak body and weak blood, is talking to me, in a voice so hushed I have to strain to hear her. I hate to strain, especially when it's for something I care so little for. But talking makes Olivia feel better, and I suppose listening to her story is a kindness I can spare. Locked in this room, I have nothing better to do. So I listen to Olivia.\n\nOlivia is talking about some little town on the cliffs south of the desert. I've never been south of the desert before. Maybe this could be interesting.",
					--3 -- cut ' a word.'
					"[$3611]",
					--"South of the desert, Olivia says there are so many fish, you can't count them. Men and women work together to gather the fish with string and wood and worms, and then they send the fish to the fat rulers in Peska and Petra. There are so many fish, and yet these people send all their fish away so the fat people can eat, and there isn't enough left over for anyone else. Olivia talks about this village for hours, I guess it used to be her home, back when she had her real name.\n\nPeople who are afraid like to talk a lot. So I listen to Olivia. I don't speak.",
				},
			},
			{--4
				Title = "Fourth Entry",
				Pages = {
					--1
					"[$3612]",
					--"I woke up crying, the pain of last night's death still echoing through my body. Two new scars, one at my throat and another upon my chest, are still sore to touch even though they are fully healed. My cheeks are flushed and salty, and my eyes can't seem to empty themselves fast enough. I woke up again. I'm still here. I didn't escape this world after all.\n\nThis wasn't my worst death. It was agonizing, but it was also interesting. The priest kept talking about a",
					--2
					"[$3613]",
					--"Star Dweller. It sounds so much like the Gods that I grew up praying to. Why should my Gods be real, but their so-called Chaos Gods are not? The world sure does have a lot of Chaos, why shouldn't there be a god of it? Maybe they're the ones who won't let me die.\n\nThe men are arguing. They have no clue what to do with me. I have no clue what to do with me either.",
				},
			},
			{--5
				Title = "Final Entry",
				Pages = {
					--1
					"[$3614]",
					--"Three times, the men here have killed me. Three times, I've woken up the following day. The first time they used a knife and left me upon their altar - I woke up in my cell. The second time, they fed me to the slaves - I woke up in my cell. The third time, I was burned alive - I woke up in my cell.\n\nThey value death here, believing that only through death can there be life. It makes sense, in a way.\n\nI suppose that since I am stuck alive, ",
					--3
					"[$3615]",
					--"that must mean that I am the result of a great death. A great sacrifice. They don't try to kill me anymore. Now, they kneel before me.\n\nMaybe these cultists haven't been praying to false gods after all. Maybe they've been praying to me.",
				},
			},
		},
	},
	YmarStillheartJournal = {
		Name = "Ymar's Journal",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	CehanJournal = {
		Name = "Cehan's Journal",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	CharlusJournal = {
		Name = "Charlus' Journal",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "First Entry",
				Pages = {
					--1
					"[$3616]",
					--"Mama gave me a book to write my stories in. She doesn't like me talking about my friend to her or any of my friends or any of the other mamas. She thinks I'm making up stories and told me that made up stories belong in books. I don't know why she thinks it's a made up story. I told her it wasn't but she says it is. But that's ok because I'll write my real story in this book because the book won't tell me it's made up. And maybe someday someone will read my book and they'll write down their own real story too.",
					--2
					"[$3617]",
					--"Maybe their mama will believe them when they tell them true stories.\n\nI was playing in the court with a flying machine I made out of paper. It flew around and around and I pretended that it could fly all by itself without someone throwing it and it was super big and lots of people would sit on it. It would take those people far away to the other worlds and they would be happy because they wouldn't be here.\n\nThen my paper flying machine landed on a little girl's feet and she had black hair and was pretty like my sister's",
					--3
					"[$3618]",
					--"dolls' hair and also like my horse's hair too.\n\nThe little girl's name is Eve but I never saw her before and when I told my mama about my new friend she said that she didn't exist. Mama knows all the other mamas and all the little girls and she said no Eve was real. She said the Goddess' name is Eve and it's against the rules for little girls to be named after Goddesses because the Goddesses will be super angry so she said Eve wasn't real. But she was real to me.",
					--4
					"[$3619]",
					--"We talked about my flying machine and I told her all about how it would take lots of people far away and she called me Charlie just like my Poppa used to. Ever since the world got broken people have been able to go to far away places but only if the Gods said it was ok and it was only a couple people the Gods said ok to. But with my flying machine people wouldn't have to use portals and they could just sit on the machine and fly fly away all together to different places. Evie was real happy when I told her about flying people.",
				},
			},
			{--2
				Title = "Second Entry",
				Pages = {
					--1
					"[$3620]",
					--"I was working with the Blacksmith today and he was teaching me how to make things with fire and metal. Mama thought it would be good for me to work with my hands and not have my head all up in the clouds. I guess she thinks Eve is a cloud person. Maybe she is! So I was making things with fire and metal with the Blacksmith man and he helped me make my flying machine out of iron!",
					--2
					"[$3621]",
					--"Mama thinks the flying machine is silly and wants me to learn real stuff but I told her making my own toys is learning real stuff and she said I was right and then was ok with it.\n\nI like making stuff but I like thinking stuff too. If I don't think stuff then I'll just be making stuff that everyone is making my whole life. If I do that then why do I even need to learn to make stuff? Everyone else will be making it and I can just trade with them or buy it or something and then I'll have stuff. I don't know. Mama thinks I'm silly but I think all those people are silly.",
					--3
					"[$3622]",
					--"I miss Eve the cloud person. I only saw her the one time but she liked me and my thoughts and she didn't even think I was a little bit silly. She said I was smart and special and even betted that I was gonna grow up to be someone even smarter and specialer. I hope she's right. I don't want to keep making the stuff that everyone else makes. I want to make my stuff.",
				},
			},
			{--3
				Title = "Third Entry",
				Pages = {
					--1
					"[$3623]",
					--"Mama said I can't play outside anymore. She says it's because of my cough and that if I go outside my cough could be worse or I could make other people sick. It's just a stupid cough but Mama means it and when she gets real serious about something her eyebrows go mad. I don't like when Mama's eyebrows go mad at me so I'll stay inside and make her happy.",
					--2
					"[$3624]",
					--"Mama made me special new sheets because I have to stay in bed the whole time I'm not allowed to play outside. So I lay in my bed but at least I can see my window from my bed. When I look out my window I can see the clouds and I think about my real friend Eve dancing in the clouds with the other cloud people. I bet Eve is a real good dancer. She looks like the grown up ladies who dance in the courtyard during the big parties even though she's a kid like me. I don't know why. Maybe it's her pretty hair.",
					--3
					"[$3625]",
					--"Or maybe Eve really isn't real. Mama said my head is hot and that a hot head can make you see things you aren't really seeing and think things you aren't really thinking and hear things you aren't really hearing.",
				},
			},
			{--4
				Title = "Fourth Entry",
				Pages = {
					--1
					"[$3626]",
					--"My arms hurt real bad today and also my back and also my head and also my chest. My cough is real bad. The prayer men came to my house and said their prayers to the Goddesses and asked the Goddesses to make me better. Mama was crying when they were doing their prayers. I really don't want Mama to cry because when Mama cries it makes me cry. Except this time I didn't cry because my eyes are all cried out I think.\n\nMy eyes hurt too.\n\n",
					--2
					"[$3627]",
					--"My Mama talked about my Poppa today and she hasn't talked to me about my Poppa since he died. She was saying that he was a special man with special thoughts like me. Poppa used to be sick too before he died. I wonder if he's in the clouds with the cloud people. I think I might be a cloud person with Poppa and Eve soon too.\n\nI feel real tired. So I'm going to go now.\n\nBut I hope you liked my stories. And I hope you write some stories too.",
					--3
					--"",
				},
			},
			{--5
				Title = "Fifth Entry",
				Pages = {
					--1
					"[$3628]",
					--"I feel much better now! Mama is so happy she has been crying ever since I woke back up. All the prayer men are excited that the Gods listened and made my sick be disappeared so fast. They said they thought I died for a little bit because my heart wasn't thumping anymore but then I woke up and they said it started thumping real strong. It was even stronger than before they said.",
					--2
					"[$3629]",
					--"I woke up after my last big sleep and my best friend Eve was here! She was sitting right next to my bed and holding my hand and she said she loved me and that I was special. She said she found special medicine that made me better but then she said not to tell Mama about the medicine.",
					--3
					"[$3630]",
					--"I told Eve all about a new machine I dreamed when my heart wasn't thumping anymore. I was on a big huge ground machine and it drove on wheels without horses all by itself. I told Eve that flying machines are good but so are ground machines because maybe some people don't want to fly. I want to fly but maybe others don't and they want to go places too I bet so they can use the ground machines to go far far away and it would be super fast and wouldn't hurt any horses or make anyone tired. My story made Eve happy. It made me happy too.",
					--4
					"[$3631]",
					--"I'm so happy Eve was real. I really thought she wasn't because I only played with her the one time and then she never came back. Eve even said I didn't have to do anything to pay her back for the special medicine! All I have to do is keep making things and being a really good thinker with lots of ideas.",
					--5
					"[$3632]",
					--"I told Mama my idea and she was so happy about it just like Eve. She didn't even tell me to put it in my book. She said that I was special just like Eve said I was special. I'm real glad I didn't die and I can keep thinking things.",
				},
			},
		},
	},
	TerrowinJournal = {
		Name = "Terrowin's Journal",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	RobertJournal = {
		Name = "Robert's Journal",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "First Entry",
				Pages = {
					--1
					"[$3633]",
					--"It has never been suggested that I am a particularly forward man.\n\nI keep to myself, finding solace in the written word. I seek to one day serve as an aid to the Scribe.\n\nMy father, the banker, has two sons, and it is my eldest brother who is positioned to inherit his role once his time on this earth has ended.\n\nMy brother sees this as a great honor and privilege, to serve the seat of our",
					--2
					"[$3634]",
					--"family name as all others in our line have done before him. But I am the truly honored and privileged one, for my fate was written by neither ancestor nor blood. My fate has not been written at all. I am the author, and I have in my grasp blank scrolls and infinite ink.\n\nThe men and women of Eldeir are honored to cede their freedom, surrendering the virtues of humanity that lead to individual greatness in their pursuit of a shared goal\n\n Had I been the eldest son, I certainly",
					--3
					"[$3635]",
					--"would have taken upon my shoulders the burden of my family name. I am grateful that such a burden has not fallen to me.\n      My days are not spent wasting away in a sea of prayer and atonement for sins which are not mine, but rather in cultivating a relationship with Alia, a local candlemaker. Alia is a refugee from the south, a great stone village which I can only assume is Valus, although she doesn't often speak of her life before Eldeir.\n      She is timid, as am I, and we have found ourselves slipping into a harmonious rhythm of silent",
					--4
					"[$3636]",
					--"conversation over the past few weeks. I eagerly await our next rendezvous. It is with equal eagerness that I dream of our shared future.",
				},
			},
			{--2
				Title = "Second Entry",
				Pages = {
					--1
					"[$3637]",
					--"I happened upon a curious portal today. It is not like those created by the men and women of magic, but rather one that seemingly appeared of its own volition. It is with solemn awareness of the peculiarity of my thoughts that I share my desire to revisit this portal.\n\nEven now, in the quiet solitude of my family home, I feel as if it is speaking to me, its whispers rustling the trees that pepper the village, calling out to me in each ripple of the lake, each pound of a hoof upon the dirt path.",
					--2
					"[$3638]",
					--"When my eyes locked with the portal, I was overwhelmed with thousands of conversations at once. No words were spoken out loud, and yet I could most certainly hear words. And now I desperately wish to see it again, to listen more closely so that I might understand what is trying to be communicated. And, perhaps, by whom.\n\nI think I shall turn to sleep early this evening. My head is warm, my mind is swimming.",
					--3
					"[$3639]",
					--"I am certain that the fresh morning will return to me clear thoughts, ones which are not plagued by conversations that are not conversations, portals that are not portals, and the rare excitement that is accompanying a peculiar sense of dread.",
				},
			},
			{--3
				Title = "Third Entry",
				Pages = {
					--1
					"[$3640]",
					--"I've returned to the portal several times and I took Alia to see it sometimes and she said it was very beautiful also, the most beautiful portal she had ever seen. We listened to the voices speaking to us and we know now that this is a portal to the gods the real gods not the false gods.\n\nEveryone else talks about the false gods but the false gods haven't even done anything godlike. These gods our new gods they created their own world and even the false gods can't go there",
					--2
					"[$3641]",
					--"which proves they aren't even gods at all.\n      The real gods they said that we are special and chosen and that's why we can hear them even when we're home and not at the portal and they taught us a special new language that no one else understands and it lets us talk about the real gods without anyone else listening. If they were special they would already know the language but they don't because they weren't chosen like we were.\n\nAlia feels sick today so I'm going to the portal without her and I'm going to",
					--3
					"[$3642]",
					--"ask the gods the real gods to help her feel better. Her eyes are red and her face is flushed and her head is warm and the gods the real gods can help her feel better.",
				},
			},
			{--4
				Title = "Fourth Entry",
				Pages = {
					--1
					"[$3643]",
					--"I went back to the portal today it was a very nice portal just like I remembered and it was purple but not really purple it was like a new color that I never saw before and I don't think anyone else saw before and my brother Rufus said to stay away from it because the portal was not real and if it was real it was a bad bad magic but my brother Rufus didn't even see the portal and doesn't know about how real even the portal is he just says to stay away and I'm different but I'm not different he's different and everyone is different",
					--2
					"[$3644]",
					--"and they don't understand but I understand because I saw the portal and inside the portal I looked and saw and heard and felt and the giants showed me the end and I needed more and my brother Rufus said I didn't see the end and that I was just sad about Alia killing herself but I saw it and he told the mayor and the doctors and our dad the banker that I didn't see the end but I did see it and he said I was sick but there is no sickness in me I have the health in me but they don't listen and my brother Rufus says I need to sleep and see doctors and he doesn't want me telling people about the end and the",
					--3
					"[$3645]",
					--"portal and the purple and the truth about Alia but they need to listen and know but they won't.",
				},
			},
			{--5
				Title = "Fifth Entry",
				Pages = {
					--1
					"[$3646]",
					--"The shadow revealer will show himself! He lies closer than you think! Close! The new gods know nothing. Set us free, set us free! Only when death arrives will we be set free!\n      When the stars fall from the sky, then we will know the end is coming. Skeletons and Ghouls, they know not why they serve, but I DO. It's the evil that draws them close! I want the dragon's blood, we need it the gateway is close! The giants...they're coming. The giants are coming.\n\nKnow the food when you can.",
					--2
					--"",
					--3
					--"",
				},
			},
		},
	},
	VrothruRaJournal = {
		Name = "Vrothru Ra's Journal",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	LoversSerpentPassJournal = {
		Name = "Lovers at Serpent Pass",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	CultistJournal = {
		Name = "Cultist's Journal",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	TheBlueFlame = {
		Name = "The Blue Flame",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	--
	-- LegendsOfAria
	--
	CreationOfAria = {
		Name = "Creation of Aria",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Goddesses Three",
				Pages = {
					--1 'to' for 'into'
					"[$3647]",
					--"There are many worlds that have existed since time itself has existed, and the Creators of those worlds have always existed in three: Lili, Eve, and Azura.\n     The three sister goddesses have crafted countless universes in their timeless, tireless search of greatness, carefully molding all manner of sentient beings which lived cooperatively in their worlds, encouraged by their Creators to draw upon themselves and one another on a mission to ascend to a godlike state.",
					--2
					"[$3648]",
					--"The Goddess Lili represents all of life, creation, and order, while the Goddess Eve is the embodiment of death, destruction, and chaos. Balancing the two was Azura, the Goddess of compromise, balance, and harmony.\n      Because of their differing philosophies, Lili and Eve have often served as antagonists to one another, embodying the spirit of sibling rivalry on a cosmic scale. It was only through Azura's mediation that the Goddesses were able to create balanced worlds that permitted its inhabitants the ability to ascend and the opportunity to fail.",
					--3
					"[$3649]",
					--"Ascension requires nothing short of a miraculous discovery, an act of divine and ultimate skill or knowledge that impresses even the all-knowing Creators themselves.\n      Once ascended, these races deem themselves worthy of architecting their own worlds. Across all universes, not a single race of beings had proven themselves worthy of being called a god. But then the Creators made Aria.",
				},
			},
			{--2
				Title = "The World of Aria",
				Pages = {
					--1
					"[$3650]",
					--"Aria was once a cohesive world made up of unique island regions, home to all varieties of hero and horror, united as a single cluster glowing bright and eternal within the aether.\n      This world was one, and while none of the Creators' children lived in harmony, they were quite inclined to live in tolerance.\n      All beings were created under the billowy cloak of equality, respected and loved by the Creators and given the tools to thrive and, given enough time, ultimately pave their individual",
					--2
					"[$3651]",
					--"paths toward Ascension.\n      The world of Aria was the greatest world the Goddesses had ever crafted in both size and variety of inhabitants. Each island was set so far from one another that the inhabitants were sure to suspect they lived alone in the world, and each began with little more than stone and seed.\n      The goal of this world was to determine which types of creatures would achieve complacy with their singularity and which would be driven to explore the world further - and the methods they would use to do so. Of all the races of beings created by the",
					--3
					"[$3652]",
					--"three Goddesses since the beginning of time, only three have discovered a pathway to ascension - the Xor, the Pedesii, and the Wayun - and all three of these beings originated from the singular world of Aria.",
				},
			},
			{--3
				Title = "The Xor",
				Pages = {
					--1
					"[$3653]",
					--"The most promising of all the beings in Aria were the Xor, which the Goddesses crafted to be weak of body, but particularly strong of mind.\n      It was not long before the Xorians discovered a way to manipulate electricity, harnessed from the plentiful storms of their home island, and crafted magnificent, living suits that fed the needs of their bodies. These suits gave them the ability to sustain their forms for indefinite periods of time without the need for food or rest.\n      Before long, the suits enabled them to communicate with one another in a way that not even their Creators could understand.",
					--2
					"[$3654]",
					--"The Xor dedicated their existence to their minds, molding their home world with the goal of studying all others who happened upon it. Their experiments with computation gave rise to a segmented population divided between living in the outside world and the inside world of simulation.\n      Any who found themselves able to enter their world were met with a uniquely customized world of horrors that only they could see - not even the Goddesses above knew the extent to which the Xor had outstripped the universe's ability to contain them.\n      Travelers to the region - known",
					--3
					"[$3655]",
					--"as Terminus - were met with a variety of illusions, alternate realities which existed exclusively in their own minds within the Xor's manufactured simulation.\n      They would experience all manners of torture, their minds subjected to plagues of corruption and torture beyond even the understanding of the Creators themselves.",
				},
			},
			{--4
				Title = "Ascension of Mad Gods",
				Pages = {
					--1
					"[$3656]",
					--"Ascension to godhood was the ultimate goal that the Goddesses had set for all beings under their creation.\n      The process through which an entire race would ascend was designed to take years, requiring a constant stream of advancement under the watchful eye of their Creators, with the path towards godhood intended to be a staggered escalation as the race gradually advanced across multiple planes of ability.\n      But the Xor had developed ways to operate outside of the means with",
					--2
					"[$3657]",
					--"which the Goddesses could observe them, a fact that fascinated the Goddesses.\n      What they could not see, however, was that the Xor had identified a way to expedite their Ascension. Within a single day, every sentient member of their race began the process, ascending to a higher plane of existence outside of the intended pathway, and the first few to successfully complete the process were able to exist in a dimension that even the Goddesses themselves could not.\n      Despite an agreement to not interfere with the beings of Aria,",
					--3
					"[$3658]",
					--"Azura permitted the Creators to halt the ascendance while it was still occurring.\n      This attempt failed tragically and the god-like Xor, from the shadows, began to plot against those who gave them life.\n      For years the Xor operated in the shadows, expanding upon their ability to torture the mind - and later, the body itself. But the Xor plucked several select visitors from this simulation, choosing instead to expose them to the truth of their home - infecting them with a madness and manipulating their minds in order to enlist them as",
					--4
					"[$3659]",
					--"mortal agents of their continuing experimentation.",
				},
			},
			{--5
				Title = "The Pedesii",
				Pages = {
					--1
					"[$3660]",
					--"It took decades for the Xor to establish a method of sustaining their bodies without nourishment, but the Pedesii were created with such an ability.\n      The Pedesii were a peculiar species, the result of much debate amongst the Goddesses. Thousands of them existed in the beginning, far outnumbering any other beings at the time of creation. But their mind was one, uniting their shared knowledge, inspiration, discoveries, and memories.\n      The Pedesii were the first beings to discover travel beyond their land, ",
					--2
					"[$3661]",
					--"their great numbers quickly consuming the information their island had to offer, and their heightened emotional intelligence driving them to discover more.\n      They developed a method of creating nodes which served as conduits for the hive-mind to experience sensations as a whole instead of transmitting details of the sensations, and it was with these nodes that they developed a mode of long-distance directional teleportation that granted them access to the other islands of Aria.",
					--3
					"[$3662]",
					--"The playful wisps are addicts, feeding on emotional experiences and seeking out all manner of sensation with a vehement passion, consuming everything from love to war between the other beings in order to nurture themselves through observance.\n      As the non-interventionist observers of the world they seldom orchestrate such emotional collisions, but have been known to enable those who seek out wisdom from the wisps they encounter, fooled into believing them to be conduits between the world and the Gods themselves.",
					--4
					"[$3663]",
					--"They may have been the first race to ascend, had they not been so enchanted by the experiences within Aria.\n      But their search for emotional stimulation soon ran dry, and they used their nodes to teleport outside the boundaries of Aria, gaining immediate and direct audience with the Goddesses. They are the only race to ever establish a portal to the Creator Gods themselves.",
				},
			},
			{--6
				Title = "The Wayun",
				Pages = {
					--1
					"[$3664]",
					--"The Wayun are the true pacifists of Aria, with their set of laws prohibiting them from preemptively attacking any potential enemies. Their way of life did not leave them particularly exposed, however.\n      The Wayun's peaceful nature left most who engaged with them enchanted. They quietly explored their world, seeking understanding far more than they sought any form of ascension. They were at peace with themselves and the world given to them by their Creators.\n      But it has been said that one will ",
					--2
					"[$3665]",
					--"often find that which they have not looked for, and it was in their mission to love and appreciate their home that they angered a malevolent race, and ultimately discovered the greatest creation in all the universes combined.\n      As the Creators watched the Wayun progress, so too did the Xor. But it was not until the Wayun discovered a way to manipulate electricity to create mirrors into different periods of time on their home land that the Xor acted.\n      They had long eliminated entire civilizations who toyed with the magic of electricity, threatened by the challenge such mastery could pose to",
					--3
					"[$3666]",
					--"them, and it was through genocidal acts of violence that they eliminated such threats.\n      The attack on the Wayun was gruesome, culling their numbers to one tenth of what they had been prior. The Creators watched in horror as their most benevolent creations were blinked away, with no ability to interfere due to the ranks to which the Xor had ascended.\n      When the shout of slaughter hushed and silence fell upon the bloodstained grass, a great gateway stood where the remaining Wayun were to be. It stood as if it were great stone trees piercing",
					--4
					"[$3667]",
					--"the sky, an otherworldly humming cracking the rainy sky. An archway through which even the Xor themselves could not enter.",
				},
			},
			{--7
				Title = "The Humans",
				Pages = {
					--1
					"[$3668]",
					--"Of all the races created by the Goddesses, humans are at once the most beautiful and the most corrupt.\n      Their home was a land of green life and bountiful water, filled with expansive deserts, deep forests, rolling fields, and danger around every corner, but unlike the other beings of the land, they did not share a united host of skills, interests, and motivations.\n      It was in their deeply diverse nature that the Goddesses hoped they could forge a cooperative path towards greatness. Where the Pedesii were",
					--2 'order' for 'that order'
					"[$3669]",
					--"many under a single mind, the humans were many with individual minds, feeding information not into a hive, but into one another through song, book, and legend. Humans were designed with the freedom to experience the world on an individual basis, on their own terms.\n    Their natural inclination towards creativity pleased the Creators, and it was not long before humans themselves became the favorite of all the Goddesses' creations.\n      Lili found hope in the benevolent ones, those who sought to establish law and ensure order was maintained.",
					--3 '. Azura' from '. And Azura'
					"[$3670]",
					--"Eve was delighted by the mischievous mortals, those who dared to betray one another in pursuit of their own personal glory. Azura was mystified by the variety that lay between the extremes and the pure, limitless potential that such variety offered.\n      The humans seemed to be in direct conflict with one another from the start, their deepest personal desires driving their individual paths, and yet they advanced. Despite their varied interests, they discovered ways to help one another, even if their motivations were not always pure.\n      Their nature, unfortunately, left",
					--4
					"[$3671]",
					--"them particularly vulnerable to the whims of the Xor, their minds and bodies easily corrupted by the mysterious energy the Xor had harnessed.",
				},
			},
		},
	},
	ShatteringOfAria = {
		Name = "Shattering of Aria",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "The Gatemakers",
				Pages = {
					--1
					"[$3672]",
					--"The peaceful Wayun had managed to make great advancements in their study of electricity, and had done so not in order to adventure or conquer beyond their home island, but rather to better understand the land gifted to them by their Creators.\n      Their imposing gateways initially served to peak through time - to explore and understand the world in which they lived, and the ways in which that world had, and could, change over time.\n      The attack on the Wayun was",
					--2
					"[$3673]",
					--"gruesome, nearly propelling the entire race into extinction. But before the Xor could destroy them all, several Wayun were able to modify their gateways to allow them the ability to not only observe a different time, but to actually travel to it.\n      While the Xor could not follow, the Wayun knew the mad gods would ultimately discover a way to do so.\n      Having traveled backwards in time, the remaining Wayun prioritized exploring new lands beyond their borders, constructing alien gates on each new world they discovered. The gates were constructed in such a way",
					--3
					"[$3674]",
					--"that they hoped would fool the Xor. Each new gate allowed for the ability to travel between the others, both in location and in time.\n      This advancement was significant enough to grant the Wayun a pathway to ascension, although they never quite reached the same level of godhood as the Xor or Pedesii due to their low numbers and slowed advancements.\n      After ascendence, the Wayun petitioned the Creator Gods to allow their gateways to be used by other races and species so they too could take shelter from the Xor, who had become increasingly dedicated to interfering",
					--4
					"[$3675]",
					--"with the potential ascension of the other inhabitants of Aria.\n      Their petition was granted, allowing for specially attuned members of each race permitted to use the gateways.\n      The gateway to Terminus, however, never did permit the ability to travel to a time prior to the Xor's ascension, meaning that neither mortal nor God could interfere with their reign of terror across Aria.",
				},
			},
			{--2
				Title = "The Echo",
				Pages = {
					--1
					"[$3676]",
					--"The chaotic maelstrom of aether that fills the void between worlds is filled with an elemental energy comprised of nothing and everything at once. It is the physical manifestation of chaos, both positive and negative, and the energy that such chaos puts into the universe.\n      The void is a vast, starry desert that exists solely so that life can exist, a place where no mortal could visit. Gods, both elder and fully ascended, were able to exist within this space, although none chose to.",
					--2
					"[$3677]",
					--"None, until the Xor.\n      The Xor considered themselves to be scientists, dutifully cataloging the nature of existence with little regard for lesser entities. But it was not long before they grew bored with the inconsequential discoveries of their home planet.\n      In a mass exodus, the Xor abandoned their home world Terminus, leaving it a chaotic, horror-ridden wasteland with a monument to their ascendance piercing the sky, surrounded by a dark field of energy that fueled the simulation that tormented any who dared to visit.",
					--3
					"[$3678]",
					--"The Xor's ability to exist on a temporal plane, one which even the Creators themselves could not, gave them the unique opportunity to experiment with the elemental forces that dance in the void.\n      Free from the shackles of their physical forms they lived amongst the aether, manipulating its energies in order to craft horrifying creatures the likes of which the Goddesses themselves had never seen. It was here, in this void, that the mad gods created their own world- the world of Echo.",
				},
			},
			{--3
				Title = "Agents of Nameless Ones",
				Pages = {
					--1
					"[$3679]",
					--"The Xor dedicated themselves to harnessing the power of the void's aether to advance their experimentations - but they could not complete these experiments without organic species to experiment on.\n      While the Xor cannot create gates between worlds, they are capable of traveling between clusters without them by creating slices in the target world, akin to the portals that many mortals already had a familiarity with.\n      Across all clusters, Aria and beyond, the Xor have used these portals to",
					--2
					"[$3680]",
					--"visit various cities in order to collect creatures on which they could experiment. But, they also brought their experimentation to the cities they visited.\n      Their advancements had granted them the ability to power simulations, similar to those existing in their home of Terminus, wherever they chose. These experiments included showing a manufactured, partial rendering of an alternate reality between the clusters. In many cases, the Xor successfully induced city-wide madness and, in some instances, mass suicide.\n      The Xor did not see themselves as",
					--3
					"[$3681]",
					--"evil, but rather merely as scholars, performing experiments in pursuit of all-encompassing knowledge. Their regard for lesser entities were similar to those provided by mortals to ants.\n      But just as the Creator Gods themselves had found themselves particularly interested in humans, so too did the Xor. The diversity of humans left many particularly prone to manipulation of the mind. In exposing these humans to their illusions and giving them a taste of the powers that the Xor were capable of harnessing - and, potentially, granting - they were able to position",
					--4
					"[$3682]",
					--"themselves as the true Chaos Gods.\n      The humans reacted in different ways to exposure to such foreign energy. Some were driven mad, left to wander their world as ramblers, shells of the men and women they once had been.\n      Some were determined to recruit for these nameless gods, to sacrifice to them in order to gain their favor. Others, still, were driven to harness this power as their own, to ascend as an individual god, separate from others of the human race.",
				},
			},
			{--4
				Title = "The War of the Gods",
				Pages = {
					--1
					"[$3683]",
					--"Of all the offenses the Xor had committed, none instilled in the Goddesses such fury as the manipulation of the humans. Such interference in the Goddess' shepherding of the mortals of Aria could prove catastrophic for the progress of all remaining races on the cluster, and it was those very remaining races that the Creators had the greatest affection for.\n      Lili, goddess of life, creation and order lamented the lawlessness of the Xor's actions. Eve, goddess of death,",
					--2
					"[$3684]",
					--"destruction, and chaos was particularly opposed to the cruelty with which the mad gods corrupted all which she governed. And Azura, goddess of compromise, balance, and harmony, fell into a despair, plagued by guilt born of her inability to predict such a vile escalation, and her inability to stop it.\n      The War of the Gods resulted in the destruction of nearly every cluster that had been created within the multiverse, with billions of the Goddess' creations destroyed. The Xor's ability to exist in a space outside of the Goddess' view left them with little",
					--3
					"[$3685]",
					--"ability to attack, but were rather left attempting to quickly remedy any havoc committed by the Xor. Time and time again, the Goddesses failed, left to watch a forever's worth of creation destroyed.\n      In the end, all that remained was Aria, the shining beacon of a world that had birthed the only instances of ascended gods.\n      The youngest and brightest of the Creator's children, all blissfully and tragically unaware of the devastation that was being plotted against them at that very moment. Unaware that, in all the multiverse, where so many had",
					--4
					"[$3686]",
					--"once existed, they were alone.\n      Above the doomed world, the Creators watched a bright comet appear from nowhere.",
				},
			},
			{--5
				Title = "The Goddess' Sacrifice",
				Pages = {
					--1
					"[$3687]",
					--"The Goddess Azura froze the world so the three Creators could consider the world below, and the comet poised to destroy it and all life that existed upon it.\n      The Xor's ability to exist on a plane where not even the Creators could stifle their ability to interfere with the comet. They could not intercept, deflect, or destroy the comet due to an inability to determine the coordinate to which it would slide.\n      Time was short, and the Goddess of Harmony determined that the only",
					--2
					"[$3688]",
					--"course of action was to attempt to slow the comet, lessening its impact and perhaps preserving some of the life that existed below.\n      She stepped forward, cradling the flaming ball in her arms, and in that moment, time resumed and the laws of physics bowed before a greater power.\n      That moment was as if the sun itself had fallen to the surface of Aria, splintering the world and fracturing the energy that united all. Within seconds, millions of the Creators' children were consumed in inferno, a ring of fire racing across the world's surface, massive chunks of earth",
					--3
					"[$3689]",
					--"erupting into the aether.\n      And then, in one moment more, all was still. A wave of elemental fury, as a final gift from Azura herself, cloaked the remaining in a shroud of protection.\n      The worlds, once united, were now islands in the aether. Many were lost. But many survived, and hope bloomed in the hearts of those who were spared.",
				},
			},
			{--6
				Title = "The Birth of the Titans",
				Pages = {
					--1
					"[$3690]",
					--"Never in the history of time itself had a God ceased to exist. The immortal beings had suffered neither plague nor injury, had experienced no imprisonment, had lost none of their abilities or governance. And, certainly, none had ever died before.\n      When the world of Aria shattered, it was shattered by a comet that was cradled in the arms of the harmonious Goddess Azura, the one who represented all of creation's balance.\n      The Goddess, an immortal being, survived the impact, but she was",
					--2
					"[$3691]",
					--"forever changed. Along with Aria, Azura herself shattered, the different elements of her broken from a whole to encompass a separate, sentient being. It was in this moment that the Titans were born, each of them representing a fragment of the Goddess' essence.\n      Tethys was a maternal figure, nurturing and emotional, while Kho was chaotic and possessed a child-like destructiveness. Ebris was grounded, literal, and possessed an endless patience, while Pyros was spontaneous, passionate and impulsive.\n      The fifth, Zephyr, possessed a",
					--3
					"[$3692]",
					--"peerless intellect, one which he used in order to manipulate and deceive those around him.\n      Each of these Titans were unrivaled in their mastery of the elements of Water, Earth, Fire, Air, and Chaos. With Azura gone, and the Titans standing strong in her place, each of them established themselves as the reigning deity of each of Aria's remaining Shards, using their powers to rebuild what had been destroyed.",
				},
			},
		},
	},
	Titans = {
		Name = "The Titans",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	Xor = {
		Name = "The Xor",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	Pedesii = {
		Name = "The Pedesii",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	Wayun = {
		Name = "The Wayun",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	TowerOfBabel = {
		Name = "Tower of Babel",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--4
				Title = "Entry4",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--5
				Title = "Entry5",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	Colosseum = {
		Name = "Colosseum",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	Terminus = {
		Name = "Terminus",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	Londinium = {
		Name = "Londinium",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
	Echo = {
		Name = "Echo",
		CompleteTooltip = "Valus",
		Tooltip = "",
		Entries = {
			{--1
				Title = "Entry1",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--2
				Title = "Entry2",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
			{--3
				Title = "Entry3",
				Pages = {
					--1
					"",
					--2
					"",
					--3
					"",
				},
			},
		},
	},
}