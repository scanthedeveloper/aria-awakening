FoodStats = {

	FoodEatTimeInSecounds = 5,
	BuffPulseTimeInSeconds = 5,
	-- How long do food based buffs last?
	BuffDurationInMinutes = 15,
	TierStamRegen = { 3, 6, 12 },
	TierHealthRegen = { 3, 6, 12 },

	-- Food stat to SetMobileMod
	FoodStatToMobileMod = {
		health = "MaxHealthPlus",
		stamina = "MaxStaminaPlus",
		mana = "MaxManaPlus",
		weight = "MaxWeightPlus",

		--SCAN ADDED CUSTOM FOOD BUFFS
		HealthRegen = "HealthRegenPlus",
		ManaRegen = "ManaRegenPlus",
		StaminaRegen = "StaminaRegenPlus",
		--Harvesting = "HarvestDelayReductionTimes",
		--Vitality = "MaxVitalityPlus",
		Bloodlust = "BloodlustRegenPlus",
		Defense = "DefensePlus",
		Attack = "AttackPlus",
	},

	-- This is the string that will append to food items
	TierSuffix = { "Snack", "Meal", "Banquet" },
	
	-- This is the name that will prepended to food items
	StatPrefix = {
		health = "Hearty",
		stamina = "Dexterous",
		mana = "Refreshing",
		weight = "Burly",
	},

	BaseFoodClass = {
		-- Ingredients don't need CookingDifficulty, if you want to 'cook' an ingredient, create a new cooked template version of that ingredient and require the 'raw' one to make it.
		
		Refreshment = {
			Replenish = 0,
		},
		Ingredient = {
			Replenish = 0.5,
		},
		Snack = {
			Replenish = 3,
		},
		Meal = {
			Replenish = 6,
			CookingDifficulty = 50,
		},
		Delicacy = {
			Replenish = 12,
			CookingDifficulty = 75,
		},
		Banquet = {
			Replenish = 12,
			CookingDifficulty = 75,
		},
	},

	BaseFoodStats = {
		-- ## COOKING TIER I (INGREDIENTS)
		MysteryMeat = {
			FoodClass = "Ingredient",
			Name = "Mystery Meat",
			Raw = true,
			MobileModAmount = 15,
			MobileMod = "health",
			MinimumSkill = 20,
			Tier = 1,
			CookedClientID = 600,
		},
		StringyMeat = {
			FoodClass = "Ingredient",
			Name = "Stringy Meat",
			Raw = true,
			MobileModAmount = 15,
			MobileMod = "health",
			MinimumSkill = 20,
			Tier = 1,
			CookedClientID = 600,
		},
		FishFilletBarrel = {
			FoodClass = "Ingredient",
			Name = "Barrel Fish",
			Raw = true,
			MobileModAmount = 15,
			MobileMod = "health",
			MinimumSkill = 20,
			Tier = 1,
			CookedClientID = 110,
		},
		FishFilletTero = {
			FoodClass = "Ingredient",
			Name = "Tero Fish",
			Raw = true,
			MobileModAmount = 15,
			MobileMod = "health",
			MinimumSkill = 20,
			Tier = 1,
			CookedClientID = 110,
		},
		Onion = {
			FoodClass = "Ingredient",
			Name = "Onion",
			Raw = true,
			MobileModAmount = 15,
			MobileMod = "weight",
			MinimumSkill = 5,
			Tier = 1,
			CookedClientID = 358,
		},
		Carrot = {
			FoodClass = "Ingredient",
			Name = "Carrot",
			Raw = true,
			MobileModAmount = 8,
			MobileMod = "stamina",
			MinimumSkill = 10,
			Tier = 1,
			CookedClientID = 2542,
		},
		Strawberry = {
			FoodClass = "Ingredient",
			Name = "Strawberry",
			Raw = true,
			MobileModAmount = 8,
			MobileMod = "mana",
			MinimumSkill = 15,
			Tier = 1,
			CookedClientID = 2538,
		},

		-- ## COOKING TIER II (INGREDIENTS)
		ToughMeat = {
			FoodClass = "Ingredient",
			Name = "Tough Meat",
			Raw = true,
			MobileModAmount = 30,
			MobileMod = "health",
			MinimumSkill = 55,
			Tier = 2,
			CookedClientID = 600,
		},
		TenderMeat = {
			FoodClass = "Ingredient",
			Name = "Tender Meat",
			Raw = true,
			MobileModAmount = 30,
			MobileMod = "health",
			MinimumSkill = 55,
			Tier = 2,
			CookedClientID = 600,
		},
		FishFilletFourEyedSalar = {
			FoodClass = "Ingredient",
			Name = "Four-eyed Salar Fish",
			Raw = true,
			MobileModAmount = 30,
			MobileMod = "health",
			MinimumSkill = 55,
			Tier = 2,
			CookedClientID = 110,
		},
		FishFilletSpottedTero = {
			FoodClass = "Ingredient",
			Name = "Spotted Tero Fish",
			Raw = true,
			MobileModAmount = 30,
			MobileMod = "health",
			MinimumSkill = 55,
			Tier = 2,
			CookedClientID = 110,
		},
		Corn = {
			FoodClass = "Ingredient",
			Name = "Corn",
			Raw = true,
			MobileModAmount = 25,
			MobileMod = "weight",
			MinimumSkill = 25,
			Tier = 2,
			CookedClientID = 2550,
		},
		ButtonMushroom = {
			FoodClass = "Ingredient",
			Name = "Button Mushroom",
			Raw = true,
			MobileModAmount = 25,
			MobileMod = "weight",
			MinimumSkill = 40,
			Tier = 2,
			CookedClientID = 2551,
		},
		GreenLeafLettuce = {
			FoodClass = "Ingredient",
			Name = "Green-leaf Lettuce",
			Raw = true,
			MobileModAmount = 16,
			MobileMod = "stamina",
			MinimumSkill = 30,
			Tier = 2,
			CookedClientID = 2545,
		},
		RedLeafLettuce = {
			FoodClass = "Ingredient",
			Name = "Red-leaf Lettuce",
			Raw = true,
			MobileModAmount = 16,
			MobileMod = "stamina",
			MinimumSkill = 45,
			Tier = 2,
			CookedClientID = 2544,
		},
		Squash = {
			FoodClass = "Ingredient",
			Name = "Squash",
			Raw = true,
			MobileModAmount = 16,
			MobileMod = "mana",
			MinimumSkill = 35,
			Tier = 2,
			CookedClientID = 2539,
		},
		Melon = {
			FoodClass = "Ingredient",
			Name = "Melon",
			Raw = true,
			MobileModAmount = 16,
			MobileMod = "mana",
			MinimumSkill = 50,
			Tier = 2,
			CookedClientID = 2537,
		},

		-- ## COOKING TIER III (INGREDIENTS)
		GourmetMeat = {
			FoodClass = "Ingredient",
			Name = "Gourmet Meat",
			Raw = true,
			MobileModAmount = 45,
			MobileMod = "health",
			MinimumSkill = 85,
			Tier = 3,
			CookedClientID = 600,
		},
		FishFilletRazor = {
			FoodClass = "Ingredient",
			Name = "Razor Fish",
			Raw = true,
			MobileModAmount = 45,
			MobileMod = "health",
			MinimumSkill = 85,
			Tier = 3,
			CookedClientID = 110,
		},
		FishFilletGoldenAether = {
			FoodClass = "Ingredient",
			Name = "Golden Aether Fish",
			Raw = true,
			MobileModAmount = 45,
			MobileMod = "health",
			MinimumSkill = 85,
			Tier = 3,
			CookedClientID = 110,
		},
		GreenPepper = {
			FoodClass = "Ingredient",
			Name = "Green Pepper",
			Raw = true,
			MobileModAmount = 50,
			MobileMod = "weight",
			MinimumSkill = 65,
			Tier = 3,
			CookedClientID = 357,
		},
		Cucumber = {
			FoodClass = "Ingredient",
			Name = "Cucumber",
			Raw = true,
			MobileModAmount = 25,
			MobileMod = "stamina",
			MinimumSkill = 75,
			Tier = 3,
			CookedClientID = 2543,
		},
		Eggplant = {
			FoodClass = "Ingredient",
			Name = "Eggplant",
			Raw = true,
			MobileModAmount = 25,
			MobileMod = "mana",
			MinimumSkill = 85,
			Tier = 3,
			CookedClientID = 2540,
		},

		-- TIER RECIPES
		TenderMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1 },
			Template = "item_food_tendermeat_meal",
		},
		TenderMeatStrawberryMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,Strawberry = 1 },
			Template = "item_food_tendermeatstrawberry_meal",
		},
		TenderMeatRedLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,RedLeafLettuce = 1 },
			Template = "item_food_tendermeatredleaflettuce_meal",
		},
		TenderMeatButtonMushroomMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,ButtonMushroom = 1 },
			Template = "item_food_tendermeatbuttonmushroom_meal",
		},
		TenderMeatCarrotMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,Carrot = 1 },
			Template = "item_food_tendermeatcarrot_meal",
		},
		TenderMeatGreenLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,GreenLeafLettuce = 1 },
			Template = "item_food_tendermeatgreenleaflettuce_meal",
		},
		TenderMeatSquashMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,Squash = 1 },
			Template = "item_food_tendermeatsquash_meal",
		},
		TenderMeatMelonMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,Melon = 1 },
			Template = "item_food_tendermeatmelon_meal",
		},
		TenderMeatOnionMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,Onion = 1 },
			Template = "item_food_tendermeatonion_meal",
		},
		TenderMeatCucumberMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,Cucumber = 1 },
			Template = "item_food_tendermeatcucumber_meal",
		},
		TenderMeatCornMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,Corn = 1 },
			Template = "item_food_tendermeatcorn_meal",
		},
		TenderMeatEggplantMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { TenderMeat = 1,Eggplant = 1 },
			Template = "item_food_tendermeateggplant_meal",
		},
		MysteryMeatSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { MysteryMeat = 1 },
			Template = "item_food_mysterymeat_snack",
		},
		MysteryMeatStrawberrySnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { MysteryMeat = 1,Strawberry = 1 },
			Template = "item_food_mysterymeatstrawberry_snack",
		},
		MysteryMeatCarrotSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { MysteryMeat = 1,Carrot = 1 },
			Template = "item_food_mysterymeatcarrot_snack",
		},
		StrawberrySnack = {
			FoodClass = "Snack",
			CookingDifficulty = 15,
			Ingredients = { Strawberry = 1 },
			Template = "item_food_strawberry_snack",
		},
		RedLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 45,
			Ingredients = { RedLeafLettuce = 1 },
			Template = "item_food_redleaflettuce_meal",
		},
		RedLeafLettuceMysteryMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 45,
			Ingredients = { RedLeafLettuce = 1,MysteryMeat = 1 },
			Template = "item_food_redleaflettucemysterymeat_meal",
		},
		RedLeafLettuceStrawberryMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 45,
			Ingredients = { RedLeafLettuce = 1,Strawberry = 1 },
			Template = "item_food_redleaflettucestrawberry_meal",
		},
		RedLeafLettuceStringyMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 45,
			Ingredients = { RedLeafLettuce = 1,StringyMeat = 1 },
			Template = "item_food_redleaflettucestringymeat_meal",
		},
		RedLeafLettuceFishFilletTeroMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 45,
			Ingredients = { RedLeafLettuce = 1,FishFilletTero = 1 },
			Template = "item_food_redleaflettucefishfillettero_meal",
		},
		RedLeafLettuceFishFilletBarrelMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 45,
			Ingredients = { RedLeafLettuce = 1,FishFilletBarrel = 1 },
			Template = "item_food_redleaflettucefishfilletbarrel_meal",
		},
		RedLeafLettuceOnionMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 45,
			Ingredients = { RedLeafLettuce = 1,Onion = 1 },
			Template = "item_food_redleaflettuceonion_meal",
		},
		ButtonMushroomMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1 },
			Template = "item_food_buttonmushroom_meal",
		},
		ButtonMushroomMysteryMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1,MysteryMeat = 1 },
			Template = "item_food_buttonmushroommysterymeat_meal",
		},
		ButtonMushroomStrawberryMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1,Strawberry = 1 },
			Template = "item_food_buttonmushroomstrawberry_meal",
		},
		ButtonMushroomRedLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1,RedLeafLettuce = 1 },
			Template = "item_food_buttonmushroomredleaflettuce_meal",
		},
		ButtonMushroomCarrotMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1,Carrot = 1 },
			Template = "item_food_buttonmushroomcarrot_meal",
		},
		ButtonMushroomGreenLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1,GreenLeafLettuce = 1 },
			Template = "item_food_buttonmushroomgreenleaflettuce_meal",
		},
		ButtonMushroomStringyMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1,StringyMeat = 1 },
			Template = "item_food_buttonmushroomstringymeat_meal",
		},
		ButtonMushroomFishFilletTeroMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1,FishFilletTero = 1 },
			Template = "item_food_buttonmushroomfishfillettero_meal",
		},
		ButtonMushroomFishFilletBarrelMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1,FishFilletBarrel = 1 },
			Template = "item_food_buttonmushroomfishfilletbarrel_meal",
		},
		ButtonMushroomSquashMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1,Squash = 1 },
			Template = "item_food_buttonmushroomsquash_meal",
		},
		ButtonMushroomMelonMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 40,
			Ingredients = { ButtonMushroom = 1,Melon = 1 },
			Template = "item_food_buttonmushroommelon_meal",
		},
		CarrotSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 10,
			Ingredients = { Carrot = 1 },
			Template = "item_food_carrot_snack",
		},
		GreenLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 30,
			Ingredients = { GreenLeafLettuce = 1 },
			Template = "item_food_greenleaflettuce_meal",
		},
		GreenLeafLettuceMysteryMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 30,
			Ingredients = { GreenLeafLettuce = 1,MysteryMeat = 1 },
			Template = "item_food_greenleaflettucemysterymeat_meal",
		},
		GreenLeafLettuceStrawberryMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 30,
			Ingredients = { GreenLeafLettuce = 1,Strawberry = 1 },
			Template = "item_food_greenleaflettucestrawberry_meal",
		},
		GreenLeafLettuceStringyMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 30,
			Ingredients = { GreenLeafLettuce = 1,StringyMeat = 1 },
			Template = "item_food_greenleaflettucestringymeat_meal",
		},
		GreenLeafLettuceFishFilletTeroMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 30,
			Ingredients = { GreenLeafLettuce = 1,FishFilletTero = 1 },
			Template = "item_food_greenleaflettucefishfillettero_meal",
		},
		GreenLeafLettuceFishFilletBarrelMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 30,
			Ingredients = { GreenLeafLettuce = 1,FishFilletBarrel = 1 },
			Template = "item_food_greenleaflettucefishfilletbarrel_meal",
		},
		GreenLeafLettuceOnionMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 30,
			Ingredients = { GreenLeafLettuce = 1,Onion = 1 },
			Template = "item_food_greenleaflettuceonion_meal",
		},
		StringyMeatSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { StringyMeat = 1 },
			Template = "item_food_stringymeat_snack",
		},
		StringyMeatStrawberrySnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { StringyMeat = 1,Strawberry = 1 },
			Template = "item_food_stringymeatstrawberry_snack",
		},
		StringyMeatCarrotSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { StringyMeat = 1,Carrot = 1 },
			Template = "item_food_stringymeatcarrot_snack",
		},
		FishFilletTeroSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { FishFilletTero = 1 },
			Template = "item_food_fishfillettero_snack",
		},
		FishFilletTeroStrawberrySnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { FishFilletTero = 1,Strawberry = 1 },
			Template = "item_food_fishfilletterostrawberry_snack",
		},
		FishFilletTeroCarrotSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { FishFilletTero = 1,Carrot = 1 },
			Template = "item_food_fishfilletterocarrot_snack",
		},
		GourmetMeatBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1 },
			Template = "item_food_gourmetmeat_banquet",
		},
		GourmetMeatStrawberryBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,Strawberry = 1 },
			Template = "item_food_gourmetmeatstrawberry_banquet",
		},
		GourmetMeatRedLeafLettuceBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,RedLeafLettuce = 1 },
			Template = "item_food_gourmetmeatredleaflettuce_banquet",
		},
		GourmetMeatButtonMushroomBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,ButtonMushroom = 1 },
			Template = "item_food_gourmetmeatbuttonmushroom_banquet",
		},
		GourmetMeatCarrotBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,Carrot = 1 },
			Template = "item_food_gourmetmeatcarrot_banquet",
		},
		GourmetMeatGreenLeafLettuceBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,GreenLeafLettuce = 1 },
			Template = "item_food_gourmetmeatgreenleaflettuce_banquet",
		},
		GourmetMeatSquashBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,Squash = 1 },
			Template = "item_food_gourmetmeatsquash_banquet",
		},
		GourmetMeatMelonBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,Melon = 1 },
			Template = "item_food_gourmetmeatmelon_banquet",
		},
		GourmetMeatOnionBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,Onion = 1 },
			Template = "item_food_gourmetmeatonion_banquet",
		},
		GourmetMeatCucumberBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,Cucumber = 1 },
			Template = "item_food_gourmetmeatcucumber_banquet",
		},
		GourmetMeatCornBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,Corn = 1 },
			Template = "item_food_gourmetmeatcorn_banquet",
		},
		GourmetMeatEggplantBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { GourmetMeat = 1,Eggplant = 1 },
			Template = "item_food_gourmetmeateggplant_banquet",
		},
		FishFilletBarrelSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { FishFilletBarrel = 1 },
			Template = "item_food_fishfilletbarrel_snack",
		},
		FishFilletBarrelStrawberrySnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { FishFilletBarrel = 1,Strawberry = 1 },
			Template = "item_food_fishfilletbarrelstrawberry_snack",
		},
		FishFilletBarrelCarrotSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 20,
			Ingredients = { FishFilletBarrel = 1,Carrot = 1 },
			Template = "item_food_fishfilletbarrelcarrot_snack",
		},
		SquashMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 35,
			Ingredients = { Squash = 1 },
			Template = "item_food_squash_meal",
		},
		SquashMysteryMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 35,
			Ingredients = { Squash = 1,MysteryMeat = 1 },
			Template = "item_food_squashmysterymeat_meal",
		},
		SquashCarrotMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 35,
			Ingredients = { Squash = 1,Carrot = 1 },
			Template = "item_food_squashcarrot_meal",
		},
		SquashStringyMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 35,
			Ingredients = { Squash = 1,StringyMeat = 1 },
			Template = "item_food_squashstringymeat_meal",
		},
		SquashFishFilletTeroMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 35,
			Ingredients = { Squash = 1,FishFilletTero = 1 },
			Template = "item_food_squashfishfillettero_meal",
		},
		SquashFishFilletBarrelMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 35,
			Ingredients = { Squash = 1,FishFilletBarrel = 1 },
			Template = "item_food_squashfishfilletbarrel_meal",
		},
		SquashOnionMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 35,
			Ingredients = { Squash = 1,Onion = 1 },
			Template = "item_food_squashonion_meal",
		},
		ToughMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1 },
			Template = "item_food_toughmeat_meal",
		},
		ToughMeatStrawberryMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,Strawberry = 1 },
			Template = "item_food_toughmeatstrawberry_meal",
		},
		ToughMeatRedLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,RedLeafLettuce = 1 },
			Template = "item_food_toughmeatredleaflettuce_meal",
		},
		ToughMeatButtonMushroomMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,ButtonMushroom = 1 },
			Template = "item_food_toughmeatbuttonmushroom_meal",
		},
		ToughMeatCarrotMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,Carrot = 1 },
			Template = "item_food_toughmeatcarrot_meal",
		},
		ToughMeatGreenLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,GreenLeafLettuce = 1 },
			Template = "item_food_toughmeatgreenleaflettuce_meal",
		},
		ToughMeatSquashMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,Squash = 1 },
			Template = "item_food_toughmeatsquash_meal",
		},
		ToughMeatMelonMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,Melon = 1 },
			Template = "item_food_toughmeatmelon_meal",
		},
		ToughMeatOnionMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,Onion = 1 },
			Template = "item_food_toughmeatonion_meal",
		},
		ToughMeatCucumberMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,Cucumber = 1 },
			Template = "item_food_toughmeatcucumber_meal",
		},
		ToughMeatCornMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,Corn = 1 },
			Template = "item_food_toughmeatcorn_meal",
		},
		ToughMeatEggplantMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { ToughMeat = 1,Eggplant = 1 },
			Template = "item_food_toughmeateggplant_meal",
		},
		FishFilletRazorBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1 },
			Template = "item_food_fishfilletrazor_banquet",
		},
		FishFilletRazorStrawberryBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,Strawberry = 1 },
			Template = "item_food_fishfilletrazorstrawberry_banquet",
		},
		FishFilletRazorRedLeafLettuceBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,RedLeafLettuce = 1 },
			Template = "item_food_fishfilletrazorredleaflettuce_banquet",
		},
		FishFilletRazorButtonMushroomBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,ButtonMushroom = 1 },
			Template = "item_food_fishfilletrazorbuttonmushroom_banquet",
		},
		FishFilletRazorCarrotBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,Carrot = 1 },
			Template = "item_food_fishfilletrazorcarrot_banquet",
		},
		FishFilletRazorGreenLeafLettuceBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,GreenLeafLettuce = 1 },
			Template = "item_food_fishfilletrazorgreenleaflettuce_banquet",
		},
		FishFilletRazorSquashBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,Squash = 1 },
			Template = "item_food_fishfilletrazorsquash_banquet",
		},
		FishFilletRazorMelonBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,Melon = 1 },
			Template = "item_food_fishfilletrazormelon_banquet",
		},
		FishFilletRazorOnionBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,Onion = 1 },
			Template = "item_food_fishfilletrazoronion_banquet",
		},
		FishFilletRazorCucumberBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,Cucumber = 1 },
			Template = "item_food_fishfilletrazorcucumber_banquet",
		},
		FishFilletRazorCornBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,Corn = 1 },
			Template = "item_food_fishfilletrazorcorn_banquet",
		},
		FishFilletRazorEggplantBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletRazor = 1,Eggplant = 1 },
			Template = "item_food_fishfilletrazoreggplant_banquet",
		},
		MelonMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 50,
			Ingredients = { Melon = 1 },
			Template = "item_food_melon_meal",
		},
		MelonMysteryMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 50,
			Ingredients = { Melon = 1,MysteryMeat = 1 },
			Template = "item_food_melonmysterymeat_meal",
		},
		MelonCarrotMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 50,
			Ingredients = { Melon = 1,Carrot = 1 },
			Template = "item_food_meloncarrot_meal",
		},
		MelonStringyMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 50,
			Ingredients = { Melon = 1,StringyMeat = 1 },
			Template = "item_food_melonstringymeat_meal",
		},
		MelonFishFilletTeroMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 50,
			Ingredients = { Melon = 1,FishFilletTero = 1 },
			Template = "item_food_melonfishfillettero_meal",
		},
		MelonFishFilletBarrelMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 50,
			Ingredients = { Melon = 1,FishFilletBarrel = 1 },
			Template = "item_food_melonfishfilletbarrel_meal",
		},
		MelonOnionMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 50,
			Ingredients = { Melon = 1,Onion = 1 },
			Template = "item_food_melononion_meal",
		},
		FishFilletGoldenAetherBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1 },
			Template = "item_food_fishfilletgoldenaether_banquet",
		},
		FishFilletGoldenAetherStrawberryBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,Strawberry = 1 },
			Template = "item_food_fishfilletgoldenaetherstrawberry_banquet",
		},
		FishFilletGoldenAetherRedLeafLettuceBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,RedLeafLettuce = 1 },
			Template = "item_food_fishfilletgoldenaetherredleaflettuce_banquet",
		},
		FishFilletGoldenAetherButtonMushroomBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,ButtonMushroom = 1 },
			Template = "item_food_fishfilletgoldenaetherbuttonmushroom_banquet",
		},
		FishFilletGoldenAetherCarrotBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,Carrot = 1 },
			Template = "item_food_fishfilletgoldenaethercarrot_banquet",
		},
		FishFilletGoldenAetherGreenLeafLettuceBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,GreenLeafLettuce = 1 },
			Template = "item_food_fishfilletgoldenaethergreenleaflettuce_banquet",
		},
		FishFilletGoldenAetherSquashBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,Squash = 1 },
			Template = "item_food_fishfilletgoldenaethersquash_banquet",
		},
		FishFilletGoldenAetherMelonBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,Melon = 1 },
			Template = "item_food_fishfilletgoldenaethermelon_banquet",
		},
		FishFilletGoldenAetherOnionBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,Onion = 1 },
			Template = "item_food_fishfilletgoldenaetheronion_banquet",
		},
		FishFilletGoldenAetherCucumberBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,Cucumber = 1 },
			Template = "item_food_fishfilletgoldenaethercucumber_banquet",
		},
		FishFilletGoldenAetherCornBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,Corn = 1 },
			Template = "item_food_fishfilletgoldenaethercorn_banquet",
		},
		FishFilletGoldenAetherEggplantBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { FishFilletGoldenAether = 1,Eggplant = 1 },
			Template = "item_food_fishfilletgoldenaethereggplant_banquet",
		},
		OnionSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 5,
			Ingredients = { Onion = 1 },
			Template = "item_food_onion_snack",
		},
		OnionStrawberrySnack = {
			FoodClass = "Snack",
			CookingDifficulty = 5,
			Ingredients = { Onion = 1,Strawberry = 1 },
			Template = "item_food_onionstrawberry_snack",
		},
		OnionCarrotSnack = {
			FoodClass = "Snack",
			CookingDifficulty = 5,
			Ingredients = { Onion = 1,Carrot = 1 },
			Template = "item_food_onioncarrot_snack",
		},
		FishFilletSpottedTeroMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1 },
			Template = "item_food_fishfilletspottedtero_meal",
		},
		FishFilletSpottedTeroStrawberryMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,Strawberry = 1 },
			Template = "item_food_fishfilletspottedterostrawberry_meal",
		},
		FishFilletSpottedTeroRedLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,RedLeafLettuce = 1 },
			Template = "item_food_fishfilletspottedteroredleaflettuce_meal",
		},
		FishFilletSpottedTeroButtonMushroomMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,ButtonMushroom = 1 },
			Template = "item_food_fishfilletspottedterobuttonmushroom_meal",
		},
		FishFilletSpottedTeroCarrotMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,Carrot = 1 },
			Template = "item_food_fishfilletspottedterocarrot_meal",
		},
		FishFilletSpottedTeroGreenLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,GreenLeafLettuce = 1 },
			Template = "item_food_fishfilletspottedterogreenleaflettuce_meal",
		},
		FishFilletSpottedTeroSquashMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,Squash = 1 },
			Template = "item_food_fishfilletspottedterosquash_meal",
		},
		FishFilletSpottedTeroMelonMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,Melon = 1 },
			Template = "item_food_fishfilletspottedteromelon_meal",
		},
		FishFilletSpottedTeroOnionMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,Onion = 1 },
			Template = "item_food_fishfilletspottedteroonion_meal",
		},
		FishFilletSpottedTeroCucumberMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,Cucumber = 1 },
			Template = "item_food_fishfilletspottedterocucumber_meal",
		},
		FishFilletSpottedTeroCornMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,Corn = 1 },
			Template = "item_food_fishfilletspottedterocorn_meal",
		},
		FishFilletSpottedTeroEggplantMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletSpottedTero = 1,Eggplant = 1 },
			Template = "item_food_fishfilletspottedteroeggplant_meal",
		},
		CucumberBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 75,
			Ingredients = { Cucumber = 1 },
			Template = "item_food_cucumber_banquet",
		},
		CucumberMysteryMeatBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 75,
			Ingredients = { Cucumber = 1,MysteryMeat = 1 },
			Template = "item_food_cucumbermysterymeat_banquet",
		},
		CucumberStrawberryBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 75,
			Ingredients = { Cucumber = 1,Strawberry = 1 },
			Template = "item_food_cucumberstrawberry_banquet",
		},
		CucumberStringyMeatBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 75,
			Ingredients = { Cucumber = 1,StringyMeat = 1 },
			Template = "item_food_cucumberstringymeat_banquet",
		},
		CucumberFishFilletTeroBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 75,
			Ingredients = { Cucumber = 1,FishFilletTero = 1 },
			Template = "item_food_cucumberfishfillettero_banquet",
		},
		CucumberFishFilletBarrelBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 75,
			Ingredients = { Cucumber = 1,FishFilletBarrel = 1 },
			Template = "item_food_cucumberfishfilletbarrel_banquet",
		},
		CucumberSquashBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 75,
			Ingredients = { Cucumber = 1,Squash = 1 },
			Template = "item_food_cucumbersquash_banquet",
		},
		CucumberMelonBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 75,
			Ingredients = { Cucumber = 1,Melon = 1 },
			Template = "item_food_cucumbermelon_banquet",
		},
		CucumberOnionBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 75,
			Ingredients = { Cucumber = 1,Onion = 1 },
			Template = "item_food_cucumberonion_banquet",
		},
		CornMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1 },
			Template = "item_food_corn_meal",
		},
		CornMysteryMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1,MysteryMeat = 1 },
			Template = "item_food_cornmysterymeat_meal",
		},
		CornStrawberryMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1,Strawberry = 1 },
			Template = "item_food_cornstrawberry_meal",
		},
		CornRedLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1,RedLeafLettuce = 1 },
			Template = "item_food_cornredleaflettuce_meal",
		},
		CornCarrotMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1,Carrot = 1 },
			Template = "item_food_corncarrot_meal",
		},
		CornGreenLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1,GreenLeafLettuce = 1 },
			Template = "item_food_corngreenleaflettuce_meal",
		},
		CornStringyMeatMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1,StringyMeat = 1 },
			Template = "item_food_cornstringymeat_meal",
		},
		CornFishFilletTeroMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1,FishFilletTero = 1 },
			Template = "item_food_cornfishfillettero_meal",
		},
		CornFishFilletBarrelMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1,FishFilletBarrel = 1 },
			Template = "item_food_cornfishfilletbarrel_meal",
		},
		CornSquashMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1,Squash = 1 },
			Template = "item_food_cornsquash_meal",
		},
		CornMelonMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 25,
			Ingredients = { Corn = 1,Melon = 1 },
			Template = "item_food_cornmelon_meal",
		},
		GreenPepperBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1 },
			Template = "item_food_greenpepper_banquet",
		},
		GreenPepperTenderMeatBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,TenderMeat = 1 },
			Template = "item_food_greenpeppertendermeat_banquet",
		},
		GreenPepperMysteryMeatBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,MysteryMeat = 1 },
			Template = "item_food_greenpeppermysterymeat_banquet",
		},
		GreenPepperStrawberryBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,Strawberry = 1 },
			Template = "item_food_greenpepperstrawberry_banquet",
		},
		GreenPepperRedLeafLettuceBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,RedLeafLettuce = 1 },
			Template = "item_food_greenpepperredleaflettuce_banquet",
		},
		GreenPepperCarrotBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,Carrot = 1 },
			Template = "item_food_greenpeppercarrot_banquet",
		},
		GreenPepperGreenLeafLettuceBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,GreenLeafLettuce = 1 },
			Template = "item_food_greenpeppergreenleaflettuce_banquet",
		},
		GreenPepperStringyMeatBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,StringyMeat = 1 },
			Template = "item_food_greenpepperstringymeat_banquet",
		},
		GreenPepperFishFilletTeroBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,FishFilletTero = 1 },
			Template = "item_food_greenpepperfishfillettero_banquet",
		},
		GreenPepperGourmetMeatBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,GourmetMeat = 1 },
			Template = "item_food_greenpeppergourmetmeat_banquet",
		},
		GreenPepperFishFilletBarrelBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,FishFilletBarrel = 1 },
			Template = "item_food_greenpepperfishfilletbarrel_banquet",
		},
		GreenPepperSquashBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,Squash = 1 },
			Template = "item_food_greenpeppersquash_banquet",
		},
		GreenPepperToughMeatBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,ToughMeat = 1 },
			Template = "item_food_greenpeppertoughmeat_banquet",
		},
		GreenPepperFishFilletRazorBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,FishFilletRazor = 1 },
			Template = "item_food_greenpepperfishfilletrazor_banquet",
		},
		GreenPepperMelonBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,Melon = 1 },
			Template = "item_food_greenpeppermelon_banquet",
		},
		GreenPepperFishFilletGoldenAetherBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,FishFilletGoldenAether = 1 },
			Template = "item_food_greenpepperfishfilletgoldenaether_banquet",
		},
		GreenPepperFishFilletSpottedTeroBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,FishFilletSpottedTero = 1 },
			Template = "item_food_greenpepperfishfilletspottedtero_banquet",
		},
		GreenPepperCucumberBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,Cucumber = 1 },
			Template = "item_food_greenpeppercucumber_banquet",
		},
		GreenPepperFishFilletFourEyedSalarBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,FishFilletFourEyedSalar = 1 },
			Template = "item_food_greenpepperfishfilletfoureyedsalar_banquet",
		},
		GreenPepperEggplantBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 65,
			Ingredients = { GreenPepper = 1,Eggplant = 1 },
			Template = "item_food_greenpeppereggplant_banquet",
		},
		FishFilletFourEyedSalarMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1 },
			Template = "item_food_fishfilletfoureyedsalar_meal",
		},
		FishFilletFourEyedSalarStrawberryMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,Strawberry = 1 },
			Template = "item_food_fishfilletfoureyedsalarstrawberry_meal",
		},
		FishFilletFourEyedSalarRedLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,RedLeafLettuce = 1 },
			Template = "item_food_fishfilletfoureyedsalarredleaflettuce_meal",
		},
		FishFilletFourEyedSalarButtonMushroomMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,ButtonMushroom = 1 },
			Template = "item_food_fishfilletfoureyedsalarbuttonmushroom_meal",
		},
		FishFilletFourEyedSalarCarrotMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,Carrot = 1 },
			Template = "item_food_fishfilletfoureyedsalarcarrot_meal",
		},
		FishFilletFourEyedSalarGreenLeafLettuceMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,GreenLeafLettuce = 1 },
			Template = "item_food_fishfilletfoureyedsalargreenleaflettuce_meal",
		},
		FishFilletFourEyedSalarSquashMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,Squash = 1 },
			Template = "item_food_fishfilletfoureyedsalarsquash_meal",
		},
		FishFilletFourEyedSalarMelonMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,Melon = 1 },
			Template = "item_food_fishfilletfoureyedsalarmelon_meal",
		},
		FishFilletFourEyedSalarOnionMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,Onion = 1 },
			Template = "item_food_fishfilletfoureyedsalaronion_meal",
		},
		FishFilletFourEyedSalarCucumberMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,Cucumber = 1 },
			Template = "item_food_fishfilletfoureyedsalarcucumber_meal",
		},
		FishFilletFourEyedSalarCornMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,Corn = 1 },
			Template = "item_food_fishfilletfoureyedsalarcorn_meal",
		},
		FishFilletFourEyedSalarEggplantMeal = {
			FoodClass = "Meal",
			CookingDifficulty = 55,
			Ingredients = { FishFilletFourEyedSalar = 1,Eggplant = 1 },
			Template = "item_food_fishfilletfoureyedsalareggplant_meal",
		},
		EggplantBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { Eggplant = 1 },
			Template = "item_food_eggplant_banquet",
		},
		EggplantMysteryMeatBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { Eggplant = 1,MysteryMeat = 1 },
			Template = "item_food_eggplantmysterymeat_banquet",
		},
		EggplantRedLeafLettuceBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { Eggplant = 1,RedLeafLettuce = 1 },
			Template = "item_food_eggplantredleaflettuce_banquet",
		},
		EggplantCarrotBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { Eggplant = 1,Carrot = 1 },
			Template = "item_food_eggplantcarrot_banquet",
		},
		EggplantGreenLeafLettuceBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { Eggplant = 1,GreenLeafLettuce = 1 },
			Template = "item_food_eggplantgreenleaflettuce_banquet",
		},
		EggplantStringyMeatBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { Eggplant = 1,StringyMeat = 1 },
			Template = "item_food_eggplantstringymeat_banquet",
		},
		EggplantFishFilletTeroBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { Eggplant = 1,FishFilletTero = 1 },
			Template = "item_food_eggplantfishfillettero_banquet",
		},
		EggplantFishFilletBarrelBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { Eggplant = 1,FishFilletBarrel = 1 },
			Template = "item_food_eggplantfishfilletbarrel_banquet",
		},
		EggplantOnionBanquet = {
			FoodClass = "Banquet",
			CookingDifficulty = 85,
			Ingredients = { Eggplant = 1,Onion = 1 },
			Template = "item_food_eggplantonion_banquet",
		},
		


		-- NON TIERED INGREDIENTS
		Wheat = {
			FoodClass = "Ingredient",
			Replenish = 0 -- at zero it disallows eating
		},
		Pumpkin = {
			FoodClass = "Ingredient",
			Replenish = 0
		},
		-- Edible ingredients
		Apple = {
			FoodClass = "Snack",
		},
		Broccoli = {
			FoodClass = "Snack",
		},
		Cabbage = {
			FoodClass = "Snack",
		},
		Potato = {
			FoodClass = "Snack",
		},
		CheeseWheel = {
			FoodClass = "Snack",
		},
		Grapes = {
			FoodClass = "Snack",
		},
		Lemon = {
			FoodClass = "Snack",
		},
		Orange = {
			FoodClass = "Snack",
		},
		Pear = {
			FoodClass = "Snack",
		},
		Tomato = {
			FoodClass = "Snack",
		},
		-- gross ingredients
		MushroomPoison = {
			FoodClass = "Ingredient",
			Replenish = 0, -- optional overwrite from base food class
			Gross = true,
		},
		MushroomNoxious = {
			FoodClass = "Ingredient",
			Replenish = 0, -- optional overwrite from base food class
			Gross = true,
		},

		--[[ reagents
		Ginseng = {
			FoodClass = "Ingredient",
			Replenish = 0, -- optional overwrite from base food class
		},
		LemonGrass = {
			FoodClass = "Ingredient",
			Replenish = 0, -- optional overwrite from base food class
		},
		Moss = {
			FoodClass = "Ingredient",
			Replenish = 0, -- optional overwrite from base food class
		},
		Mushrooms = {
			FoodClass = "Ingredient",
			Replenish = 0, -- optional overwrite from base food class
		},
		]]

		
		
		
		ChickenMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		CoyoteMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		CrocodileMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		DeerMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		DragonMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		FoxMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		HorseMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		RabbitMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		RatMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		TurkeyMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		WolfMeat = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		GoldenTreeBark = {
			FoodClass = "Ingredient",
			Raw = true,
		},
		
		
		

		-- DRINKS
		Wine = {
			FoodClass = "Refreshment",
			UseCases = {
				"Drink"
			},
			DrugEffect = "DrunkenEffect",
			DrugDuration = 10,
			Template = "item_wine",
		},
		Beer = {
			FoodClass = "Refreshment",
			UseCases = {
				"Drink"
			},
			CookingDifficulty = 5,
			DrugEffect = "DrunkenEffect",
			DrugDuration = 10,
			Ingredients = { Wheat = 5, Ginseng = 2, },
			Template = "item_beer",
		},
		Ale = {
			FoodClass = "Refreshment",
			UseCases = {
				"Drink"
			},
			CookingDifficulty = 5,
			DrugEffect = "DrunkenEffect",
			DrugDuration = 20,
			Ingredients = { Wheat = 5, LemonGrass = 2, },
			Template = "item_ale",
		},
		Mead = {
			FoodClass = "Refreshment",
			UseCases = {
				"Drink"
			},
			CookingDifficulty = 5,
			DrugEffect = "DrunkenEffect",
			DrugDuration = 60,
			Ingredients = { Wheat = 5, Moss = 2, },
			Template = "item_mead",
		},
		
		CookedMeat = {
			FoodClass = "Snack",
			CookingDifficulty = 10,
			--Ingredients = { MysteryMeat = 1 },
			Template = "item_cooked_meat",
		},
		
		Jerky = {
			FoodClass = "Snack",
			CookingDifficulty = 30,
			--Ingredients = { StringyMeat = 1 },
			Template = "item_jerky",
		},
		MeatLoaf = {
			FoodClass = "Meal",
			CookingDifficulty = 50,
			--Ingredients = { ToughMeat = 1 },
			Template = "item_meat_loaf"
		},
		WildSteak = {
			FoodClass = "Delicacy",
			CookingDifficulty = 90,
			--Ingredients = { GourmetMeat = 1 },
			Template = "item_wild_steak"
		},
		CookedAetherFish = {
			FoodClass = "Delicacy",
			CookingDifficulty = 95,
			--Ingredients = { FishFilletGoldenAether = 1 },
			Template = "item_cooked_aetherfish"
		},		
		CookedBarrelFish = {
			FoodClass = "Snack",
			CookingDifficulty = 10,
			--Ingredients = { FishFilletBarrel = 1 },
			Template = "item_cooked_barrelfish",
		},
		CookedTeroFish = {
			FoodClass = "Snack",
			CookingDifficulty = 30,
			--Ingredients = { FishFilletTero = 1 },
			Template = "item_cooked_terofish",
		},
		CookedSpottedTero = {
			FoodClass = "Meal",
			CookingDifficulty = 50,
			--Ingredients = { FishFilletSpottedTero = 1 },
			Template = "item_cooked_spottedterofish"
		},
		CookedFourEyeSalar = {
			FoodClass = "Meal",
			CookingDifficulty = 70,
			--Ingredients = { FishFilletFourEyedSalar = 1 },
			Template = "item_cooked_salarfish"
		},
		CookedRazorFish = {
			FoodClass = "Delicacy",
			CookingDifficulty = 90,
			--Ingredients = { FishFilletRazor = 1 },
			Template = "item_cooked_razorfish"
		},

		SacredDewFlower = {
			FoodClass = "Ingredient",		
			DrugEffect = "ClubDrugEffect",
			DrugDuration = 10,
			Template = "ingredient_sacred_dew"
		},
		SacredCactus = {
			FoodClass = "Ingredient",		
			DrugEffect = "SacredCactusEffect",
			DrugDuration = 10,
			Template = "ingredient_sacred_cactus_extract"
		},

		
		-- Ingredients table MUST be unique, 
			--if two different foods share the exact same ingredients, there's no guarantee which one will be created.
			--{Wheat=100} and {Wheat=100} are exactly the same, whereas {Wheat=100} and {Wheat=101} are different, the 101 will always get priority
		
		BowlOfStew = {
			FoodClass = "Meal",
		},

		-- MEALS
		Bread = {
			FoodClass = "Meal",
			--- These options only apply to stuff that can be cooked (ie, non-ingredients)
			CookingDifficulty = 25, -- optional, defaults to FoodClass CookingDifficulty
			Ingredients = { Wheat = 5, }, -- required to be craftable (cooked)
			Template = "item_bread", -- required to be craftable (cooked)
			---
		},
		RoastedChicken = {
			FoodClass = "Meal",
			--- These options only apply to stuff that can be cooked (ie, non-ingredients)
			CookingDifficulty = 35, -- optional, defaults to FoodClass CookingDifficulty
			Ingredients = { ChickenMeat = 5, GreenPepper = 1 }, -- required to be craftable (cooked)
			Template = "item_roasted_chicken", -- required to be craftable (cooked)
			---
		},
		RabbitStew = {
			FoodClass = "Meal",
			--- These options only apply to stuff that can be cooked (ie, non-ingredients)
			CookingDifficulty = 20, -- optional, defaults to FoodClass CookingDifficulty
			Ingredients = { RabbitMeat = 5, Water = 1, Onion = 1 }, -- required to be craftable (cooked)
			Template = "item_rabbit_stew", -- required to be craftable (cooked)
			---
		},
		RatStew = {
			FoodClass = "Meal",
			--- These options only apply to stuff that can be cooked (ie, non-ingredients)
			CookingDifficulty = 30, -- optional, defaults to FoodClass CookingDifficulty
			Ingredients = { RatMeat = 5, Water = 1, Onion = 1 }, -- required to be craftable (cooked)
			Template = "item_rat_stew", -- required to be craftable (cooked)
			---
		},
		FoxStew = {
			FoodClass = "Meal",
			--- These options only apply to stuff that can be cooked (ie, non-ingredients)
			CookingDifficulty = 30, -- optional, defaults to FoodClass CookingDifficulty
			Ingredients = { FoxMeat = 3, Water = 1, Onion = 1 }, -- required to be craftable (cooked)
			Template = "item_fox_stew", -- required to be craftable (cooked)
			---
		},
		RoastedTurkey = {
			FoodClass = "Meal",
			--- These options only apply to stuff that can be cooked (ie, non-ingredients)
			CookingDifficulty = 40, -- optional, defaults to FoodClass CookingDifficulty
			Ingredients = { TurkeyMeat = 5, GreenPepper = 1 }, -- required to be craftable (cooked)
			Template = "item_roasted_turkey", -- required to be craftable (cooked)
			---
		},
		CoyoteStew = {
			FoodClass = "Meal",
			--- These options only apply to stuff that can be cooked (ie, non-ingredients)
			CookingDifficulty = 40, -- optional, defaults to FoodClass CookingDifficulty
			Ingredients = { CoyoteMeat = 5, Water = 1, Onion = 1 }, -- required to be craftable (cooked)
			Template = "item_coyote_stew", -- required to be craftable (cooked)
			---
		},


		

		-- DelicacyS
		ApplePie = {
			FoodClass = "Delicacy",
			CookingDifficulty = 55,
			Ingredients = { Wheat = 20, Apple = 5, },
			Template = "item_pie_apple"
		},
		PumpkinPie = {
			FoodClass = "Delicacy",
			CookingDifficulty = 55,
			Ingredients = { Wheat = 20, Pumpkin = 5, },
			Template = "item_pie_pumpkin"
		},
		WolfSteak = {
			FoodClass = "Delicacy",
			CookingDifficulty = 60,
			Ingredients = { WolfMeat = 3, Mushrooms = 1, },
			Template = "item_wolf_steak"
		},
		DeerSteak = {
			FoodClass = "Delicacy",
			CookingDifficulty = 65,
			Ingredients = { DeerMeat = 3, Mushrooms = 1, },
			Template = "item_deer_steak"
		},
		HorseSteak = {
			FoodClass = "Delicacy",
			CookingDifficulty = 70,
			Ingredients = { HorseMeat = 3, Mushrooms = 1, },
			Template = "item_horse_steak"
		},
		BearSteak = {
			FoodClass = "Delicacy",
			CookingDifficulty = 75,
			Ingredients = { BearMeat = 3, Mushrooms = 1, },
			Template = "item_bear_steak"
		},
		FriedCrocodile = {
			FoodClass = "Delicacy",
			CookingDifficulty = 80,
			Ingredients = { CrocodileMeat = 5, GreenPepper = 2, },
			Template = "item_fried_crocodile"
		},
		SpiderEyeSoup = {
			FoodClass = "Delicacy",
			CookingDifficulty = 85,
			Ingredients = { SpiderEye = 5, Water = 1, Onion = 1 },
			Template = "item_spider_eye_soup"
		},

		SmokedDragonSteak = {
			FoodClass = "Delicacy",
			CookingDifficulty = 100,
			Ingredients = { DragonMeat = 1, GoldenTreeBark = 1 },
			Template = "item_dragon_steak"
		},					
	}
}