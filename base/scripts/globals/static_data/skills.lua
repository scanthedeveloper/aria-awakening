SkillData = {
	AllSkills= {
		MeleeSkill = {
			DisplayName = "Vigor",
			SkillIcon = "Skill_Endurance",
			PrimaryStat = "Will",
			Description = "Vigor determines the effectiveness of bandaging and offers a damage bonus with melee and ranged weapons.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.13,
		},
		PiercingSkill = {
			DisplayName = "Piercing",
			PrimaryStat = "Agility",
			Description = "Piercing determines your effeciency when fighting with piercing weapons.",
			SkillType = "CombatTypeSkill",
			IsWeaponSkill = true,
			GainFactor = 0.13,
		},
		SlashingSkill = {
			DisplayName = "Slashing",
			PrimaryStat = "Strength",
			Description = "Slashing determines your effeciency when fighting with slashing weapons.",
			SkillType = "CombatTypeSkill",
			IsWeaponSkill = true,
			GainFactor = 0.13,
		},
		LancingSkill = {
			DisplayName = "Lancing",
			PrimaryStat = "Strength",
			Description = "Lancing determines your effeciency when fighting with lancing weapons.",
			SkillType = "CombatTypeSkill",
			IsWeaponSkill = true,
			GainFactor = 0.13,
		},
		BashingSkill = {
			DisplayName = "Bashing",
			PrimaryStat = "Strength",
			Description = "Bashing determines your effeciency when fighting with bashing weapons.",
			SkillType = "CombatTypeSkill",
			IsWeaponSkill = true,
			GainFactor = 0.13,
		},
		BrawlingSkill = {
			DisplayName = "Brawling",
			--SCAN UPDATED FOR TRAINING DUMMY HACK
			PrimaryStat = "Agility",
			Description = "Brawling determines your effeciency with your fists.",
			SkillType = "CombatTypeSkill",
			IsWeaponSkill = true,
			GainFactor = 0.15,
		},
		HealingSkill = {
			DisplayName = "Healing",
			Description = "Healing determines your ability to heal humans and animals.",
			PrimaryStat = "Wisdom",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_Regeneration",
			GainFactor = 0.7,
			Options = {
				SkillRequiredToResurrect = 80,
				HealTimeBaseSelf = 8000,
				HealTimeBaseOther = 8000,
				MinHealTime = 3000,
				InterruptDamagePlayer = 19,
				InterruptDamageNPC = 26,
				BandageRange = 5
			}
		},
		-- SORCERY SKILLS
		--SCAN DISABLED DLC SKILLS
		SorcerySkill = {
			DisplayName = "Sorcery",
			PrimaryStat = "Wisdom",
			Description = "Sorcery allows players to utilize forbidden magic from the Monolith.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_Sorcery",
			GainFactor = 0.2,
			Skip = true,
			--Skip = false,
			NoEdit = false,
		},
		SummoningSkill =
		{
			DisplayName = "Summoning",
			PrimaryStat = "Wisdom",
			Description = "Increases the fortitude and duration of your summoned minions.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.13,
			Skip = false,
		},
		-- MAGIC SKILLS
		MagicAffinitySkill = {
			DisplayName = "Magic Affinity",
			PrimaryStat = "Intelligence",
			NoStatGain = true,
			Description = "Magic Affinity determines the power of your offensive spells.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.4,
		},
		MagerySkill = {
			DisplayName = "Magery",
			PrimaryStat = "Intelligence",
			Description = "Magery determines your knowledge of magic spells and provides evasion whilst wielding mage weapons.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.4,
			SkillIcon = "Skill_Evocation"
		},

		ManifestationSkill = {
			DisplayName = "Manifestation",
			PrimaryStat = "Intelligence",
			Description = "Manifestation determines your knowledge of beneficial spells and provides evasion whilst wielding mage weapons.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.4,
			Skip = true,
		},
		EvocationSkill = {
			DisplayName = "Evocation",
			PrimaryStat = "Intelligence",
			Description = "Evocation determines your knowledge of damage dealing spells and provides evasion whilst wielding mage weapons.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.4,
			Skip = true,
		},
		
		ChannelingSkill = {
			DisplayName = "Channeling",
			PrimaryStat = "Wisdom",
			Description = "Channeling determines your ability to actively and passively regenerate mana.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.2,
			Abilities = { "Focus" },
		},
		
		MagicalAttunementSkill = {
			DisplayName = "Magical Attunement",
			PrimaryStat = "Intelligence",
			NoStatGain = true,
			Description = "Magic Attunement determines your ability to augment your casting with magical wands and staves.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.4,
		},
		-- END MAGIC SKILLS
		BlockingSkill = {
			DisplayName = "Blocking",
			PrimaryStat = "Will",
			Description = "Blocking determines your effectiveness using a shield.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.3,
		},
		MetalsmithSkill = {
			DisplayName = "Blacksmithing",
			Description = "Blacksmithing is the capacity to be able to work metal and allows creation of metal items.",
			PrimaryStat = "Strength",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.4,
			Practice = {
				ResourceType = "IronIngots",
				ConsumeAmount = 5,
				Seconds = 3
			}
		},
		WoodsmithSkill = {
			DisplayName = "Carpentry",
			Description = "Carpentry is the capacity to work wood and allows creation of wooden items.",
			PrimaryStat = "Agility",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.4,
			Practice = {
				ResourceType = "Boards",
				ConsumeAmount = 5,
				Seconds = 3
			}
		},
		AlchemySkill = {
			DisplayName = "Alchemy",
			PrimaryStat = "Wisdom",
			Description = "[$3287]",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.5,
			Skip = false,
		},
		MarksmanshipSkill = {
			DisplayName = "Marksmanship",
			PrimaryStat = "Strength",
			Description = "Accuracy with projectile weapons",
			SkillType = "CombatTypeSkill",
			Skip = true,
		},
		NecromancySkill =
		{
			DisplayName = "Necromancy",
			PrimaryStat = "Intelligence",
			Description = "Summon the powers of the undead and other unholy energies.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.13,
			Skip = true,
		},
		CookingSkill = {
			DisplayName = "Cooking",
			PrimaryStat = "Intelligence",
			Description = "[$3290]",
			SkillType = "TradeTypeSkill",
			AllowCampfireGains = true,
		},
		FishingSkill = {
			DisplayName = "Fishing",
			PrimaryStat = "Intelligence",
			Description = "[$3291]",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.6,
		},	
		LumberjackSkill = {
			DisplayName = "Lumberjack",
			PrimaryStat = "Strength",
			Description = "[$3295]",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.1,
			--Skip = true,
		},
		MiningSkill = {
			DisplayName = "Mining",
			PrimaryStat = "Strength",
			Description = "[$3296]",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.1,
			--Skip = true,
		},
		FabricationSkill = {
			DisplayName = "Fabrication",
			PrimaryStat = "Intelligence",
			Description = "[$3297]",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.4,
			Practice = {
				ResourceType = "Fabric",
				ConsumeAmount = 5,
				Seconds = 3
			}
		},
		MusicianshipSkill = {
			DisplayName = "Musicianship",
			PrimaryStat = "Intelligence",
			Description = "The ability to play a musical instrument well.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.2,
		},
		InscriptionSkill = {
			DisplayName = "Inscription",
			PrimaryStat = "Intelligence",
			Description = "The ability to craft spell scrolls & spellbooks.",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.5,
		},
		EntertainmentSkill = {
			DisplayName = "Entertainment",
			PrimaryStat = "Intelligence",
			Description = "Use music to accomplish more than sound.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.25,
		},
		AnimalTamingSkill = {
			DisplayName = "Animal Taming",
			PrimaryStat = "Intelligence",
			Description = "Animal Taming is your ability to control certain creatures.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_AnimalKen",
			Abilities = { "Command" },
			GainFactor = 0.2,
		},
		BeastmasterySkill = {
			DisplayName = "Beastmastery",
			PrimaryStat = "Intelligence",
			Description = "Beastmastery is your ability to effectively command your pets in battle, granting a bonus to their damage.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_BeastMastery",
			GainFactor = 0.1,
		},
		AnimalLoreSkill = {
			DisplayName = "Animal Lore",
			PrimaryStat = "Intelligence",
			Description = "Your knowledge of animals. Determines your ability to control and adds a bonus to healing your pets.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.2,
		},
		ArcherySkill = {
			DisplayName = "Archery",
			PrimaryStat = "Strength",
			Description = "Archery determines your effectiveness with a bow and arrow.",
			SkillType = "CombatTypeSkill",
			IsWeaponSkill = true,
			GainFactor = 0.2,
		},
		HidingSkill = {
			DisplayName = "Hiding",
			SkillIcon = "Skill_Rogue",
			PrimaryStat = "Agility",
			Description = "Hiding determines how well you can conceal yourself from the sight of others.",
			SkillType = "CombatTypeSkill",
			Abilities = { "Hide" },
			GainFactor = 0.6,
		},
		StealthSkill = {
			DisplayName = "Stealth",
			SkillIcon = "Skill_Rogue",
			PrimaryStat = "Agility",
			Description = "Stealth determines how effective you are at remaining concealed whilst moving. Start stop movement will reveal you quickly.",
			SkillType = "CombatTypeSkill",			
			GainFactor = 0.5,
		},
		MartialProwessSkill = {
			DisplayName = "Martial Prowess",
			SkillIcon = "Skill_LightArmorProficiency",
			PrimaryStat = "Agility", 
			Description = "Martial Prowess determines the effectiveness of martial abilities and offers a damage bonus with melee and ranged weapons.",
			SkillType = "CombatTypeSkill",
			GainFactor = 0.13,
		},

		TreasureHuntingSkill = {
			DisplayName = "Treasure Hunting",
			PrimaryStat = "Intelligence",
			Description = "Your ability to decipher treasure maps and locate buried treasure.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_Inscription",
			GainFactor = 0.7,
		},
		LockpickingSkill = {
			DisplayName = "Lockpicking",
			PrimaryStat = "Agility",
			Description = "Your effectiveness using lockpicking tools to open locked chests.",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.5,
		},
		StealingSkill = {
			DisplayName = "Stealing",
			PrimaryStat = "Agility",
			Description = "Your effectiveness at taking what does not belong to you without being seen.",
			SkillType = "TradeTypeSkill",
			SkillIcon = "Skill_Rogue",
			GainFactor = 0.5,
		},
		SnoopingSkill = {
			DisplayName = "Snooping",
			PrimaryStat = "Agility",
			Description = "Your effectiveness at opening peoples' packs without them noticing.",
			SkillType = "TradeTypeSkill",
			SkillIcon = "Skill_Rogue",
			GainFactor = 0.5,
			Abilities = { "Snoop" },
		},
		--[[
		ForensicEvaluationSkill = {
			DisplayName = "Forensic Evaluation",
			PrimaryStat = "Intelligence",
			Description = "Use clues to determine the perpetrator of a crime.",
			SkillType = "TradeTypeSkill",
			GainFactor = 1,
		}]]
		--SCAN ADDED
		SkinningSkill = {
			DisplayName = "Skinning",
			PrimaryStat = "Agility",
			SkillIcon = "Skill_AnimalKen",
			Description = "The ability to gain bonuses when harvesting creatures.",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.1,
			--Skip = true,
		},
		--SCAN ADDED
		ForagingSkill = {
			DisplayName = "Foraging",
			PrimaryStat = "Intelligence",
			Description = "The ability to gain bonuses on harvesting plants and items on the ground.",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.1,
			--Skip = true,
		},
		--SCAN ADDED
		--[[
		CampingSkill = {
			DisplayName = "Camping",
			PrimaryStat = "Strength",
			Description = "The ability to gain regenerative bonuses on campfires.",
			SkillType = "TradeTypeSkill",
			GainFactor = 0.1,
			--Skip = true,
		},
		]]
		--SCAN ADDED
		NatureMagicSkill = {
			DisplayName = "Nature Magic",
			PrimaryStat = "Wisdom",
			Description = "Control the primal powers of nature.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_Foraging",
			GainFactor = 0.13,
			Skip = false,
		},
		--SCAN ADDED
		PyromancyMagicSkill = {
			DisplayName = "Pyromancy",
			PrimaryStat = "Intelligence",
			Description = "Control the primal powers of fire.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_Summoning",
			GainFactor = 0.13,	
		},
		--SCAN ADDED
		DemonologySkill =
		{
			DisplayName = "Demonology",
			PrimaryStat = "Intelligence",
			Description = "Summon the powers of Demons and other unholy energies.",
			SkillType = "CombatTypeSkill",
			SkillIcon = "Skill_Necromancy",
			GainFactor = 0.13,
			Skip = false,
		},
	},
}