CustomItemNoMerchantSale = {
	resource_stone = true
	, resource_brick = true
	, resource_iron_ore = true
	, resource_copper_ore = true
	, resource_gold_ore = true
	, resource_cobalt_ore = true
	, resource_obsidian_ore = true
	, resource_iron = true
	, resource_copper = true
	, resource_gold = true
	, resource_cobalt = true
	, resource_obsidian = true
	, resource_wood = true
	, resource_ash = true
	, resource_blightwood = true
	, resource_cotton = true
	, animalparts_leather_hide = true
	, animalparts_beast_leather_hide = true
	, animalparts_vile_leather_hide = true
	, resource_fish_barrel = true
	, resource_fish_tero = true
	, resource_fish_spotted_tero = true
	, resource_fish_razor = true
	, resource_fish_aether = true
	, item_cooked_barrelfish = true
	, item_cooked_terofish = true
	, item_cooked_spottedterofish = true
	, item_cooked_razorfish = true
	, item_cooked_aetherfish = true
	, resource_boards = true
	, resource_boards_ash = true
	, resource_blightwood_boards = true
	, resource_bolt_of_cloth = true
	, resource_bolt_of_cloth_quilted = true
	, resource_silk_cloth = true
	, resource_leather = true
	, resource_beast_leather = true
	, resource_vile_leather = true
}

CustomItemValues = {

	--Resources
	resource_stone = 5,
	resource_brick = 5,
	
	resource_wood = 5,
	resource_ash = 10,
	resource_blightwood = 20,
	resource_boards = 25,
	resource_boards_ash = 50,
	resource_blightwood_boards = 100,

	resource_iron_ore = 5,
	resource_copper_ore = 7,
	resource_gold_ore = 10,
	resource_cobalt_ore = 15,
	resource_obsidian_ore = 20,
	resource_iron = 25,
	resource_copper = 35,
	resource_gold = 50,
	resource_cobalt = 75,
	resource_obsidian = 100,
	
	animalparts_feather = 25,
	animalparts_wild_feather = 50,
	animalparts_leather_hide = 25,
	animalparts_beast_leather_hide = 50,
	animalparts_vile_leather_hide = 60,
	--resource_cotton = 25,
	--resource_cotton_fluffy = 50,
	animalparts_spider_silk = 50,
	resource_bolt_of_cloth = 25,
	resource_bolt_of_cloth_quilted = 50,
	resource_silk_cloth = 50,
	resource_leather = 25, 
	resource_beast_leather = 50,
	resource_vile_leather = 50,

	animalparts_blood = 5,
	animalparts_blood_beast = 25,
	animalparts_blood_vile = 50,

	animalparts_eye = 4,
	animalparts_eye_sickly = 20,
	animalparts_eye_decrepid = 40,

	resource_cotton = 10,
	resource_cotton_fluffy = 20,
	
	resource_sand = 0.20,
	resource_blankscroll = 10,

	resource_frayed_scroll = 15,
	resource_fine_scroll = 30,
	resource_ancient_scroll = 60,

	rune_blank = 200,

	--Animal Parts
	
	animalparts_human_skull = 10,

	animalparts_mystery_meat = 1,
	animalparts_fish_fillet = 1,
	animalparts_stringy_meat = 5,
	animalparts_tough_meat = 10,
	animalparts_tender_meat = 20,
	animalparts_gourmet_meat = 50,

	item_cooked_meat = 2,
	item_jerky = 10,
	item_meat_loaf = 20,
	item_brisket = 40,
	item_wild_steak = 100,

	resource_fish_barrel = 1,
	resource_fish_tero = 5,
	resource_fish_spotted_tero = 10,
	resource_fish_razor = 20,
	resource_fish_aether = 50,

	item_cooked_barrelfish = 2,
	item_cooked_terofish = 10,
	item_cooked_spottedterofish = 20,
	item_cooked_razorfish = 40,
	item_cooked_aetherfish = 100,

	animalparts_bone = 5,
	animalparts_bone_cursed = 35,
	animalparts_bone_ethereal = 50,
	animalparts_bone_spectral = 20,

	--Harvest Resources
	ingredient_lemongrass = 4,
	ingredient_ginsengroot = 4,
	ingredient_mushroom = 5,
	ingredient_moss = 5,
	ingredient_olive_oil = 1.8,
	ingredient_olive_oil_pure = 18,
	ingredient_cactus = 2.5,
	ingredient_sacred_cactus = 42,
	ingredient_wine = 1,
	ingredient_broccoli = 4,
	ingredient_cabbage = 4,
	ingredient_cucumber = 4,
	ingredient_green_pepper = 4,
	ingredient_onion = 4,
	ingredient_potato = 4,
	ingredient_tomato = 4,

	ingredient_melon = 15,
	ingredient_strawberry = 4,
	ingredient_carrot = 4, 



	--Items
	candle = 10,
	pouch = 10,
	lockbox = 30,
	celador_map_scroll = 40,
	dye_tub = 50,

	ruby_gem = 900,
	sapphire_gem = 400,
	topaz_gem = 200,

	inn_stew = 15,
	item_ale = 3,
	item_apple = 4,
	item_beer = 4,
	item_bread = 7,
	item_mead = 4,
	item_bowl = 3,
	item_pear = 4,
	item_orange = 4,
	item_lemon = 4,
	cooked_stew = 7,

	songbook = 200,
	spellbook = 200,
	martialbook = 200,
	spellbook_noob = 200,
	map_atlas_blank = 20,

	book_blue = 7,
	book_brown = 7,
	book_green = 7,
	book_grey = 7,
	book_red = 7,
	book_yellow = 7,

	artifact_gold_bracelet = 35,
	artifact_mask = 35,
	artifact_ornate_goblet = 35,
	artifact_pendant = 35,
	artifact_small_statue = 35,

	key = 8,

	cultist_scripture_a = 20,
	cultist_scripture_b = 20,
	cultist_scripture_c = 20,
	cultist_scripture_d = 20,
	cultist_scripture_e = 20,
	cultist_scripture_f = 20,
	cultist_scripture_g = 20,
	cultist_scripture_h = 20,
	cultist_scripture_i = 20,
	cultist_scripture_j = 20,

	bandage = 1,


	blueprint_woodhouse_worthless = 22500*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_woodhousecottage = 45450*ServerSettings.Plot.HouseBlueprintCostModifier,

	blueprint_tudorhousemahogany = 55250*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_tudorhousebirch = 97500*ServerSettings.Plot.HouseBlueprintCostModifier,

	blueprint_woodhousemahogany = 55250*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_woodhousebirch = 97500*ServerSettings.Plot.HouseBlueprintCostModifier,

	blueprint_stonehousetuscan = 55250*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_stonehousecottage = 97500*ServerSettings.Plot.HouseBlueprintCostModifier,

	blueprint_terracottahousetuscan = 55250*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_terracottahousecottage = 97500*ServerSettings.Plot.HouseBlueprintCostModifier,

	blueprint_tudorhousemayor = 216600*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_terracottahousevilla = 216600*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_stonehousevilla = 216600*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_woodhousemayor = 216600*ServerSettings.Plot.HouseBlueprintCostModifier,

	blueprint_stonehouseestate = 332600*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_woodhousetavern = 332600*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_tudorhousetavern = 332600*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_terracottahouseestate = 332600*ServerSettings.Plot.HouseBlueprintCostModifier,

	blueprint_tundra1 = 55250*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_tundra2 = 97500*ServerSettings.Plot.HouseBlueprintCostModifier,
	blueprint_tundra6 = 332600*ServerSettings.Plot.HouseBlueprintCostModifier,
	

	fruit_catacombs = 150,

	holy_water = 0,

	tool_engraving = 20,

	packed_bind_stone = 100000,
	land_deed = 99999999, -- this is calculated and overwritten at the bottom of plot.lua helper
	guild_charter = 50000,
	rapid_escape_scroll = 10000,
	hearthstone = 1000,

	arrow = 1,
	arrow_ash = 0.6,
	arrow_blightwood = 0.2,

	recipe_ash=320,
	recipe_blightwood=320,
	recipe_arrow_ash=320,
	recipe_arrow_blightwood=320,
	recipe_longbow=320,
	recipe_warbow=320,
	recipe_stoolwooden=320,
	recipe_chairfancy=320,
	recipe_benchfancy=320,
	recipe_smallfence=320,
	recipe_fencedoor=320,
	recipe_barrel=320,
	recipe_lockbox=320,
	recipe_shelf=320,
	recipe_dresser=320,
	recipe_tablewoodenlarge=320,
	recipe_deskfancy=320,
	recipe_bedsmall=320,
	recipe_bedmedium=320,
	recipe_bedlarge=320,
	recipe_anvil=320,
	recipe_forge=320,
	recipe_alchemytable=320,
	recipe_inscriptiontable=320,
	recipe_woodsmithtable=320,
	recipe_loom=320,
	recipe_plantbedsmall=320,
	recipe_plantbedmedium=320,
	recipe_plantbedlarge=320,
	recipe_stove=320,
	recipe_fireplacestone=320,
	recipe_buckler=320,
	recipe_kiteshield=320,
	recipe_bookshelfwooden=320,
	recipe_tableround=320,
	recipe_tableinn=320,
	recipe_table_blacksmith=320,
	recipe_standingtorch=320,
	recipe_walllantern=320,
	recipe_hanginglantern=320,
	recipe_shorts=320,
	recipe_skirts=320,
	recipe_apron=320,
	recipe_blacksmith=320,
	recipe_bandana=320,
	recipe_dress=320,
	recipe_bandithood=320,
	recipe_magehat=320,
	recipe_linenhelm=320,
	recipe_linenlegs=320,
	recipe_linenchest=320,
	recipe_leatherhelm=320,
	recipe_leatherlegs=320,
	recipe_leatherchest=320,
	recipe_hardenedhood=320,
	recipe_hardenedleggings=320,
	recipe_hardenedchest=320,
	recipe_magerobes=320,
	recipe_quiltedcloth=320,
	recipe_silkcloth=320,
	recipe_saddlebags=320,
	recipe_vile_leather=320,
	recipe_beast_leather=320,
	recipe_cloak=320,
	recipe_kryss=320,
	recipe_poniard=320,
	recipe_bonedagger=320,
	recipe_broadsword=320,
	recipe_saber=320,
	recipe_katana=320,
	recipe_hammer=320,
	recipe_greataxe=320,
	recipe_maul=320,
	recipe_largeaxe=320,
	recipe_warmace=320,
	recipe_voulge=320,
	recipe_spear=320,
	recipe_scalehelm=320,
	recipe_warhammer=320,
	recipe_halberd=320,
	recipe_scalelegs=320,
	recipe_scaletunic=320,
	recipe_fullplatehelm=320,
	recipe_fullplatelegs=320,
	recipe_fullplatetunic=320,
	recipe_cobalt=320,
	recipe_obsidian=320,
	recipe_lockpick=320,


	--SCAN ADDED CUSTOM ITEMS
	--custom weapons
	recipe_claymore=320,
	recipe_automatic_bow=320,

	weapon_claymore=300,
	weapon_automatic_bow=300,

	--custom potions
	potion_lweight=100,
	potion_weight=320,
	potion_gweight=500,
	potion_skill=500,
	potion_lexplosion=100,
	potion_explosion=320,
	potion_gexplosion=500,

	--patron items
	enchanted_golden_bow=1000,
	enchanted_golden_crook=1000,
	enchanted_golden_fishing_rod=1000,
	enchanted_golden_halberd=1000,
	enchanted_golden_hatchet=1000,
	enchanted_golden_kryss=1000,
	enchanted_golden_longsword=1000,
	enchanted_golden_mace=1000,
	enchanted_golden_mining_pick=1000,
	enchanted_golden_staff=1000,
	journeyman_skill_token=10000,

	--faction items
	mal_pirate_cloak=1000,
	mal_pirate_cloth_blackdress=1000,
	mal_pirate_cloth_helm=1000,
	mal_pirate_cloth_leggings=1000,
	mal_pirate_cloth_reddress=1000,
	mal_pirate_cloth_tunic=1000,
	mal_pirate_light_helm=1000,
	mal_pirate_light_leggings=1000,
	mal_pirate_light_tunic=1000,
	mal_pirate_plate_helm=1000,
	mal_pirate_plate_leggings=1000,
	mal_pirate_plate_tunic=1000,
	mal_pirate_pirate_shield=1000,

	--event items
	easter_egg_blue=1000,
	easter_egg_green=1000,
	easter_egg_pink=1000,
	easter_egg_purple=1000,
	easter_egg_red=1000,
	easter_egg_yellow=1000,

	event_currency=5000,

	--season rewards
	item_statue_season1_pve_warhorse=15000,
	item_statue_season1_warhorse=15000,

	--dyes
	scan_black_clothing_dye = 320,
	scan_blue_clothing_dye = 320,
	scan_bronze_clothing_dye = 320,
	scan_brown_clothing_dye = 320,
	scan_darkgray_clothing_dye = 320,
	scan_desert_clothing_dye = 320,
	scan_electrum_clothing_dye = 320,
	scan_felgreen_clothing_dye = 320,
	scan_frozen_clothing_dye = 320,
	scan_fuchsia_clothing_dye = 320,
	scan_gloom_clothing_dye = 320,
	scan_gold_clothing_dye = 320,
	scan_green_clothing_dye = 320,
	scan_hope_clothing_dye = 320,
	scan_jade_clothing_dye = 320,
	scan_jinn_clothing_dye = 320,
	scan_lightblue_clothing_dye = 320,
	scan_lightgray_clothing_dye = 320,
	scan_neon_clothing_dye = 320,
	scan_nightborn_clothing_dye = 320,
	scan_orange_clothing_dye = 320,
	scan_patron_clothing_dye = 5000,
	scan_pink_clothing_dye = 320,
	scan_polarized_clothing_dye = 320,
	scan_purple_clothing_dye = 320,
	scan_red_clothing_dye = 320,
	scan_scourge_clothing_dye = 320,
	scan_undeath_clothing_dye = 320,
	scan_white_clothing_dye = 320,
	scan_yellow_clothing_dye = 320,

	treasure_map = 100,
	treasure_map_1 = 200,
	treasure_map_2 = 300,
	treasure_map_3 = 400,
	treasure_map_4 = 500,
	treasure_map_colossal_medeina_precise = 100,
	treasure_map_colossal_medeina = 100,
	treasure_map_colossal_misha_precise = 100,
	treasure_map_colossal_misha = 100,

	weapon_daze_bow = 300,
	weapon_doubleshot_bow = 300,
	weapon_explosiveshot_bow = 300,
	weapon_fireshot_bow = 300,
	weapon_freezing_bow = 300,
	weapon_frostshot_bow = 300,
	staff_celestial = 300,
	staff_monk = 300,
	staff_necromancer = 300,
	staff_pearl = 300,
	wand_celestial = 300,
	wand_bone = 300,
	wand_pearl = 300,
	wand_spiked = 300,
	weapon_boneclub = 300,
	weapon_club = 300,
	weapon_spikedclub = 300,
	weapon_spikedmace = 300,
	weapon_woodstaff = 300,

}