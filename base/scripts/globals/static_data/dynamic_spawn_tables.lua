-- Light the engines
DynamicSpawnTables = {}
DynamicSpawnTables.BossScaleHPToLootLevel =
{
    { Health = 3000, LootLevel = 11  },
    { Health = 5000, LootLevel = 12  },
    { Health = 8000, LootLevel = 13  },
    { Health = 12000, LootLevel = 14  },
    { Health = 9999999, LootLevel = 15  },
}

DynamicSpawnTables.SetScaledHealth = function( _playerPower, _mBossObject )
    local healthScale = (( _playerPower / 300 ) - 1)
    local newHealth = math.floor( GetMaxHealth(_mBossObject) * healthScale)
    SetMobileMod(_mBossObject, "MaxHealthTimes", "DynamicBossScale", healthScale)
end

DynamicSpawnTables.SetScaledAttack = function( _playerPower, _mBossObject )
    local attackStart = _mBossObject:GetObjVar("Attack") or 40
    local attackScale = math.min( 120, math.max( 40, math.ceil( (_playerPower / 500) * attackStart )))
    _mBossObject:SetObjVar("Attack", attackScale)
    DebugMessage( "_playerPower", _playerPower )
    DebugMessage( "Attack", attackScale )
    _mBossObject:SendMessage("RecalculateStats", {Attack=true})
end

DynamicSpawnTables.SetScaledLootLevel = function( _mBossObject )
    local newHealth = GetMaxHealth(_mBossObject)
    local lootLevel = nil
    for i=1, #DynamicSpawnTables.BossScaleHPToLootLevel do 
        if( lootLevel == nil and newHealth <= DynamicSpawnTables.BossScaleHPToLootLevel[i].Health ) then
            lootLevel = DynamicSpawnTables.BossScaleHPToLootLevel[i].LootLevel
        end
    end
    _mBossObject:SetObjVar("LootLevel", lootLevel)
end

DynamicSpawnTables.MobileTeamType = {}
DynamicSpawnTables.LootDistribution = {}

-- Templates can be arrays or strings, arrays will cause the spawner to 
-- do a random() to get the template to spawn.

-- Level * 20 = Player Skill Equivalent

-- Wolf Den Progressive
DynamicSpawnTables.MobileTeamType.Wolf = {}

-- Corruption 1 Progressive
DynamicSpawnTables.MobileTeamType.Corruption1 = {} 
DynamicSpawnTables.MobileTeamType.Corruption1[1] = { Template = "imp", Level = 3 }
DynamicSpawnTables.MobileTeamType.Corruption1[2] = { Template = "hell_hound", Level = 3 }
DynamicSpawnTables.MobileTeamType.Corruption1[3] = { Template = "greater_imp", Level = 5 }
DynamicSpawnTables.MobileTeamType.Corruption1[4] = { Template = "corruption1_dynamic_boss", Level = 1, Boss = true }

-- Corruption 2 Progressive
DynamicSpawnTables.MobileTeamType.Corruption2 = {} 
DynamicSpawnTables.MobileTeamType.Corruption2[1] = { Template = "greater_imp", Level = 3 }
DynamicSpawnTables.MobileTeamType.Corruption2[2] = { Template = "gargoyle", Level = 6 }
DynamicSpawnTables.MobileTeamType.Corruption2[3] = { Template = "gargoyle", Level = 6 }
DynamicSpawnTables.MobileTeamType.Corruption2[4] = { Template = "corruption2_dynamic_boss", Level = 1, Boss = true }

-- Sewer Progressive
DynamicSpawnTables.MobileTeamType.Sewer = {} 
DynamicSpawnTables.MobileTeamType.Sewer[1] = { Template = {"giant_bat", "sewer_bat"}, Level = 3 } 
DynamicSpawnTables.MobileTeamType.Sewer[2] = { Template = {"rat_giant", "sewer_rat"}, Level = 3 } 
DynamicSpawnTables.MobileTeamType.Sewer[3] = { Template = "sewer_lurker", Level = 6 }
DynamicSpawnTables.MobileTeamType.Sewer[4] = { Template = "sewer_dynamic_boss", Level = 1, Boss = true }

-- Bandit Progressive
DynamicSpawnTables.MobileTeamType.Bandit = {} 
DynamicSpawnTables.MobileTeamType.Bandit[1] = { Template = {"bandit", "bandit_female"} , Attack = 15, Level = 3 } 
DynamicSpawnTables.MobileTeamType.Bandit[2] = { Template = {"bandit_archer", "bandit_archer_female"} , Attack = 20, Level = 6 } 
DynamicSpawnTables.MobileTeamType.Bandit[3] = { Template = "bandit_dynamic_boss", Level = 1, Boss = true }

-- Rebel Progressive
DynamicSpawnTables.MobileTeamType.Rebel = {} 
DynamicSpawnTables.MobileTeamType.Rebel[1] = { Template = {"rebel", "rebel_female"} , Attack = 15, Level = 3 } 
DynamicSpawnTables.MobileTeamType.Rebel[2] = { Template = {"rebel_looter", "rebel_looter_female"} , Attack = 20, Level = 6 } 
DynamicSpawnTables.MobileTeamType.Rebel[3] = { Template = {"rebel_sharpshooter", "rebel_sharpshooter_female", "rebel_looter", "rebel_looter_female"}, Attack = 28, Level = 8 }
DynamicSpawnTables.MobileTeamType.Rebel[4] = { Template = "rebel_dynamic_boss", Level = 1, Boss = true }

-- Harpy Progressive
DynamicSpawnTables.MobileTeamType.Harpy = {}
DynamicSpawnTables.MobileTeamType.Harpy[1] = { Template = "harpy_fledgling", Level = 3 }
DynamicSpawnTables.MobileTeamType.Harpy[2] = { Template = "harpy_adolescent", Level = 6 }
DynamicSpawnTables.MobileTeamType.Harpy[3] = { Template = "harpy", Level = 9 }
DynamicSpawnTables.MobileTeamType.Harpy[4] = { Template = "wild_harpy", Level = 12 }
DynamicSpawnTables.MobileTeamType.Harpy[5] = { Template = "stone_harpy", Level = 15 }
DynamicSpawnTables.MobileTeamType.Harpy[6] = { Template = "harpy_matriarch", Level = 1, Boss = true }

-- Undead Progressive (Shrine)
DynamicSpawnTables.MobileTeamType.UndeadShrine = {}
DynamicSpawnTables.MobileTeamType.UndeadShrine[1] = { Template = {"skeleton_mage"}, Level = 6 }
DynamicSpawnTables.MobileTeamType.UndeadShrine[2] = { Template = {"lich"}, Level = 9 }
DynamicSpawnTables.MobileTeamType.UndeadShrine[3] = { Template = "undead_shrine_boss", Level = 1, Boss = true }

-- Undead Progressive (Ruins)
DynamicSpawnTables.MobileTeamType.UndeadRuins = {}
DynamicSpawnTables.MobileTeamType.UndeadRuins[1] = { Template = {"skeleton_mage"}, Level = 5 }
DynamicSpawnTables.MobileTeamType.UndeadRuins[2] = { Template = {"lich"}, Level = 9 }
DynamicSpawnTables.MobileTeamType.UndeadRuins[3] = { Template = "undead_ruins_boss", Level = 1, Boss = true }

-- Undead Progressive (Cemetery)
DynamicSpawnTables.MobileTeamType.UndeadCemetery = {}
DynamicSpawnTables.MobileTeamType.UndeadCemetery[1] = { Template = {"lich"}, Level = 4 }
DynamicSpawnTables.MobileTeamType.UndeadCemetery[2] = { Template = {"lich"}, Level = 6 }
DynamicSpawnTables.MobileTeamType.UndeadCemetery[3] = { Template = {"lich"}, Level = 8 }
DynamicSpawnTables.MobileTeamType.UndeadCemetery[4] = { Template = "undead_shrine_boss", Level = 1, Boss = true }

-- Undead Progressive (Hard)
DynamicSpawnTables.MobileTeamType.UndeadHard = {}
DynamicSpawnTables.MobileTeamType.UndeadHard[1] = { Template = {"wraith","skeleton"}, Level = 3 }
DynamicSpawnTables.MobileTeamType.UndeadHard[2] = { Template = {"skeleton_archer","skeleton_mage"}, Level = 6 }
DynamicSpawnTables.MobileTeamType.UndeadHard[3] = { Template = {"ghoul","lich"}, Level = 9 }
DynamicSpawnTables.MobileTeamType.UndeadHard[4] = { Template = "undead_hard_boss", Level = 1, Boss = true }

-- Undead Progressive (Easy)
DynamicSpawnTables.MobileTeamType.UndeadEasy = {}
DynamicSpawnTables.MobileTeamType.UndeadEasy[1] = { Template = "skeleton", Level = 3 }
DynamicSpawnTables.MobileTeamType.UndeadEasy[2] = { Template = "zombie", Level = 6 } -- double
DynamicSpawnTables.MobileTeamType.UndeadEasy[3] = { Template = "skeleton_mage", Level = 12 } -- double
DynamicSpawnTables.MobileTeamType.UndeadEasy[4] = { Template = "ghoul", Level = 24 }
DynamicSpawnTables.MobileTeamType.UndeadEasy[5] = { Template = "undead_easy_boss", Level = 1, Boss = true } -- setting this to level 1 make it spawn faster :)

-- Ruin 1 Progressive
DynamicSpawnTables.MobileTeamType.Ruin1 = {} 
DynamicSpawnTables.MobileTeamType.Ruin1[1] = { Template = "ruin_skeleton", Level = 3 }
DynamicSpawnTables.MobileTeamType.Ruin1[2] = { Template = "ruin_skeleton_archer", Level = 3 }
DynamicSpawnTables.MobileTeamType.Ruin1[3] = { Template = "ruin_skeleton_mage", Level = 5 }
DynamicSpawnTables.MobileTeamType.Ruin1[4] = { Template = "skeleton_watcher", Level = 1, Boss = true }

-- Ruin 2 Progressive
DynamicSpawnTables.MobileTeamType.Ruin2 = {} 
DynamicSpawnTables.MobileTeamType.Ruin2[1] = { Template = "ruin_servant", Level = 3 }
DynamicSpawnTables.MobileTeamType.Ruin2[2] = { Template = "ruin_servant", Level = 3 }
DynamicSpawnTables.MobileTeamType.Ruin2[3] = { Template = "ruin_servant", Level = 5 }
DynamicSpawnTables.MobileTeamType.Ruin2[4] = { Template = "enraged_lich_servant", Level = 1, Boss = true }

-- Spider Nest
DynamicSpawnTables.MobileTeamType.SpiderNest = {} 
DynamicSpawnTables.MobileTeamType.SpiderNest[1] = { Template = "spider_large", Level = 2 }
DynamicSpawnTables.MobileTeamType.SpiderNest[2] = { Template = {"spider_huntress", "spider_hunter"}, Level = 3 }
DynamicSpawnTables.MobileTeamType.SpiderNest[3] = { Template = "spider_vile", Level = 6 }
DynamicSpawnTables.MobileTeamType.SpiderNest[4] = { Template = "spider_vile", Level = 1, Boss = true }

-- Golbin Camp
DynamicSpawnTables.MobileTeamType.GoblinCamp = {} 
DynamicSpawnTables.MobileTeamType.GoblinCamp[1] = { Template = "goblin", Level = 2 }
DynamicSpawnTables.MobileTeamType.GoblinCamp[2] = { Template = "goblin", Level = 4 }
DynamicSpawnTables.MobileTeamType.GoblinCamp[3] = { Template = "goblin_mage", Level = 6 }
DynamicSpawnTables.MobileTeamType.GoblinCamp[4] = { Template = "goblin_mage", Level = 8 }
DynamicSpawnTables.MobileTeamType.GoblinCamp[5] = { Template = "goblin_hq", Level = 1, Boss = true }

-- Gazers
DynamicSpawnTables.MobileTeamType.GazerIsle = {} 
DynamicSpawnTables.MobileTeamType.GazerIsle[1] = { Template = "half_gazer", Level = 3 }
DynamicSpawnTables.MobileTeamType.GazerIsle[2] = { Template = "gazer", Level = 6 }
DynamicSpawnTables.MobileTeamType.GazerIsle[3] = { Template = "gazer_hq", Level = 1, Boss = true }

-- GraveLake
DynamicSpawnTables.MobileTeamType.GraveLake = {} 
DynamicSpawnTables.MobileTeamType.GraveLake[1] = { Template = "skeleton_ice", Level = 3 }
DynamicSpawnTables.MobileTeamType.GraveLake[2] = { Template = "skeleton_ice_mage", Level = 6 }
DynamicSpawnTables.MobileTeamType.GraveLake[3] = { Template = "skeleton_ice_boss", Level = 1, Boss = true }

-- HotSprings
DynamicSpawnTables.MobileTeamType.HotSprings = {} 
DynamicSpawnTables.MobileTeamType.HotSprings[1] = { Template = "beetle_flying", Level = 3 }
DynamicSpawnTables.MobileTeamType.HotSprings[2] = { Template = "beetle_giant_ice", Level = 6 }
DynamicSpawnTables.MobileTeamType.HotSprings[3] = { Template = "beetle_flying_boss", Level = 1, Boss = true }

-- YetiCavern
DynamicSpawnTables.MobileTeamType.YetiCavern = {} 
DynamicSpawnTables.MobileTeamType.YetiCavern[1] = { Template = "yeti", Level = 6 }
DynamicSpawnTables.MobileTeamType.YetiCavern[2] = { Template = "yeti", Level = 9 }
DynamicSpawnTables.MobileTeamType.YetiCavern[3] = { Template = "yeti_boss", Level = 1, Boss = true }

-- Flat Wilds Common
DynamicSpawnTables.MobileTeamType.FlatWildsCommon = {} 
DynamicSpawnTables.MobileTeamType.FlatWildsCommon[1] = { Template = {"easter_bandit", "bandit_archer"}, Level = 4 }
DynamicSpawnTables.MobileTeamType.FlatWildsCommon[2] = { Template = {"bandit", "easter_bandit_female"}, Level = 8 }
DynamicSpawnTables.MobileTeamType.FlatWildsCommon[3] = { Template = "miniboss_ork_warrior", Level = 1, Boss = true }

-- Flat Wilds Uncommon
DynamicSpawnTables.MobileTeamType.FlatWildsUncommon = {}
DynamicSpawnTables.MobileTeamType.FlatWildsUncommon[1] = { Template = "beetle_giant", Level = 4 }
DynamicSpawnTables.MobileTeamType.FlatWildsUncommon[2] = { Template = "beetle_giant_flame", Level = 8 }
DynamicSpawnTables.MobileTeamType.FlatWildsUncommon[3] = { Template = "beetle_giant_poison", Level = 16 }
DynamicSpawnTables.MobileTeamType.FlatWildsUncommon[4] = { Template = "beetle_gargantuan", Level = 1, Boss = true }

-- Flat Wilds Rare
DynamicSpawnTables.MobileTeamType.FlatWildsRare = {}
DynamicSpawnTables.MobileTeamType.FlatWildsRare[1] = { Template = "deception_viper", Level = 2 }
DynamicSpawnTables.MobileTeamType.FlatWildsRare[2] = { Template = "deception_serpent", Level = 16 }
DynamicSpawnTables.MobileTeamType.FlatWildsRare[3] = { Template = "gorgon", Level = 16 }
DynamicSpawnTables.MobileTeamType.FlatWildsRare[4] = { Template = "gorgon_cunning", Level = 1, Boss = true }

-- Flat Ultra Rare
DynamicSpawnTables.MobileTeamType.FlatWildsUltraRare = {}
DynamicSpawnTables.MobileTeamType.FlatWildsUltraRare[1] = { Template = "drake", Level = 16 }
DynamicSpawnTables.MobileTeamType.FlatWildsUltraRare[2] = { Template = "dragon", Level = 48 }
DynamicSpawnTables.MobileTeamType.FlatWildsUltraRare[3] = { Template = "mature_wyvern", Level = 1, Boss = true }

-- Golem
DynamicSpawnTables.MobileTeamType.Golem = {} 
DynamicSpawnTables.MobileTeamType.Golem[1] = { Template = "rock_sprite", Level = 6 }
DynamicSpawnTables.MobileTeamType.Golem[2] = { Template = "rock_golem", Level = 9 }
DynamicSpawnTables.MobileTeamType.Golem[3] = { Template = "golem_boss", Level = 1, Boss = true }

-- SCAN ADDED EASTER EVENT
DynamicSpawnTables.MobileTeamType.EasterHillsCommon = {} 
DynamicSpawnTables.MobileTeamType.EasterHillsCommon[1] = { Template = {"easter_bandit", "bandit_archer"}, Level = 4 }
DynamicSpawnTables.MobileTeamType.EasterHillsCommon[2] = { Template = {"bandit", "easter_bandit_female"}, Level = 8 }
DynamicSpawnTables.MobileTeamType.EasterHillsCommon[3] = { Template = "miniboss_ork_warrior", Level = 1, Boss = true }