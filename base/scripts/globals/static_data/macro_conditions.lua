MacroConditions = {

    -- Targeting
    {
        ID="macro_target_current",
        ActionType="MacroCondition",
        DisplayName="Target Current",
        Icon="attack_target",
        Tooltip="Target current target.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },

    {
        ID="macro_target_self",
        ActionType="MacroCondition",
        DisplayName="Target Self",
        Icon="attack_target",
        Tooltip="Target self.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },
    --[[
    {
        ID="macro_target_closest_enemy",
        ActionType="MacroCondition",
        DisplayName="Target Closest Enemy",
        Icon="attack_target",
        Tooltip="Target closest enemy.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },
    ]]

    -- Sleep
    {
        ID="macro_sleep_X_milliseconds",
        ActionType="MacroCondition",
        DisplayName="Sleep X Milliseconds ",
        Icon="Night",
        Tooltip="Waits a set number of milliseconds before continuing macro.",
        Enabled=true,
        UserInput=true,
        UserPromt="How many milliseconds?",
        ServerCommand="",
        Locked=true,
    },
    --[[
    {
        ID="macro_sleep_1_second",
        ActionType="MacroCondition",
        DisplayName="Sleep 1 Second",
        Icon="Night",
        Tooltip="Sleep macro for 1 second.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },
    {
        ID="macro_sleep_3_second",
        ActionType="MacroCondition",
        DisplayName="Sleep 3 Second",
        Icon="Night",
        Tooltip="Sleep macro for 3 second.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },
    {
        ID="macro_sleep_5_second",
        ActionType="MacroCondition",
        DisplayName="Sleep 5 Second",
        Icon="Night",
        Tooltip="Sleep macro for 5 second.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },
    {
        ID="macro_sleep_10_second",
        ActionType="MacroCondition",
        DisplayName="Sleep 10 Second",
        Icon="Night",
        Tooltip="Sleep macro for 10 second.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },
    {
        ID="macro_sleep_60_second",
        ActionType="MacroCondition",
        DisplayName="Sleep 60 Second",
        Icon="Night",
        Tooltip="Sleep macro for 60 second.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },
    ]]

    -- Abilities
    {
        ID="macro_use_primary",
        ActionType="MacroCondition",
        DisplayName="Use Primary Ability",
        Icon="Thunder Strike 03",
        Tooltip="Uses primary weapon ability [Q].",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },
    {
        ID="macro_use_secondary",
        ActionType="MacroCondition",
        DisplayName="Use Secondary Ability",
        Icon="Thunder Strike 02",
        Tooltip="Uses secondary weapon ability [E].",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },

    -- Repeat
    {
        ID="macro_repeat_1",
        ActionType="MacroCondition",
        DisplayName="Repeat 1 Times",
        IconText="Rpt 1",
        Tooltip="Repeats this macro one time.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },

    {
        ID="macro_repeat_5",
        ActionType="MacroCondition",
        DisplayName="Repeat 5 Times",
        IconText="Rpt 5",
        Tooltip="Repeats this macro five time.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },

    {
        ID="macro_repeat_10",
        ActionType="MacroCondition",
        DisplayName="Repeat 10 Times",
        IconText="Rpt 10",
        Tooltip="Repeats this macro ten time.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },

    {
        ID="macro_repeat_0",
        ActionType="MacroCondition",
        DisplayName="Repeat Endless",
        IconText="RptEnd",
        Tooltip="Repeats this macro endlessly.",
        Enabled=true,
        ServerCommand="",
        Locked=true,
    },

    
}

