AllNPCDialogueTrees = {}

--[[

Careful when adding earlier branches, GoTo will become one index off.
]]

AllNPCDialogueTrees.ENG = {

    Quest = {
        Branches = {
            {--1
                Speech = "[$3876]",
                Instruction = { "START", "END", "INELIGIBLE", },
                MenuText = { "[StartDialogue[1]]", "[EndDialogue[1]]", "[IneligibleDialogue[1]]", },
            },
            {--2
                START = {
                    Speech = "[$3877]",
                    Instruction = { "ACCEPT", "H" },
                    MenuText = { "I accept.", "Perhaps later." },
                },
                END = {
                    Speech = "[$3880]",
                    Instruction = { "END" },
                    MenuText = { "Continue." },
                },
                INELIGIBLE = {
                    Speech = "[$3878]",
                    Instruction = { "H", },
                    MenuText = { "Okay." },
                },
                RETRY = {
                    OnBegin = function(npcObj, playerObj, args)
                        local questName, step = args[1], args[2]
                        step = tonumber(step)
                        Quests.Resume(playerObj, {questName, step})
                    end,
                    Speech = "[$3879]",
                    Instruction = { "H", },
                    MenuText = { "Thank you." },
                },
            },
            {--3
                ACCEPT = {
                    OnBegin = function(npcObj, playerObj, args)
                        --DebugMessage("ACCEPT.OnBegin()")
                        local quest, step = args[1], args[2]
                        step = tonumber(step)
                        if ( not Quests.TryStart(playerObj, quest, step) ) then
                            DebugMessage("Quests.TryStart() failed: player is not prepared.")
                            --QuickDialogMessage(npcObj, playerObj, "You are not prepared.", 10)
                        end
                        playerObj:CloseDynamicWindow("NPCInteraction")
                    end,
                    Speech = false,
                },
                END = {
                    OnBegin = function(npcObj, playerObj, args)
                        --DebugMessage("END.OnBegin()")
                        local quest, step = args[1], args[2]
                        --DebugMessage(quest,step)
                        step = tonumber(step)
                        local reqs = AllQuests[quest][step].Requirements
                        if reqs then
                            for i=1, #reqs do
                                if reqs[i][1] == "HasCompletedCraftingOrder" then
                                    local commissions = npcObj:GetObjVar("Commissions") or {}
                                    if commissions then
                                        commissions[playerObj] = nil
                                        npcObj:SetObjVar("Commissions", commissions)
                                    end
                                end
                            end
                        end
                        local forceEnd = step == 1 and true or false
                        if ( not Quests.TryEnd(playerObj, quest, step, 1, forceEnd) ) then
                            DebugMessage("Quests.TryEnd(",quest, step,") failed: player has not completed task.")
                            --QuickDialogMessage(this, user, "Come back when you have completed the task.", 10)
                        end
                        playerObj:CloseDynamicWindow("NPCInteraction")
                    end,
                    Speech = false,
                },
            },
        },
    },

    --
    -- SERVICES
    --
    ServiceGroup = {
        Speech = "[$3845]",
        SpeechPriority = 2,
        Instruction = { "G", },
        MenuText = { "I request a service...", },
    },
    Merchant = {
        Branches = {
            {--1
                Speech = "[$3845]",
                SpeechPriority = 2,
                Instruction = { "TRADE", },
                MenuText = { "I want to trade...", },
            },
            {--2
                Speech = "[$3846]",
                Instruction = { "BUY", "SELL", "H" },
                MenuText = { "I want to buy...", "I wish to sell...", "Actually..." },
            },
            {--3
                BUY = {
                    OnBegin = function(npcObj, playerObj, args)
                        Dialogue.RequestGameObject(npcObj, playerObj, false)
                    end,
                    Speech = "[$3847]",
                    Instruction = { "ELSE", },
                    MenuText = { "Nevermind.", },
                    GoTo = { 2 },
                },
                SELL = {
                    OnBegin = function(npcObj, playerObj, args)
                        Dialogue.RequestGameObject(npcObj, playerObj, true)
                    end,
                    Speech = "[$3848]",
                    Instruction = { "ELSE", },
                    MenuText = { "Nevermind.", },
                    GoTo = { 2 },
                },
            },
            {--4
                SELLSUCCESS = {
                    Speech = "[$3851]",
                    Instruction = { "SELL", "H" },
                    MenuText = { "I wish to sell something else...", "That is all, for now.", },
                    GoTo = { 3, nil },
                },
                SELLFAIL = {
                    Speech = "[$3852]",
                    Instruction = { "SELL", "H" },
                    MenuText = { "I have something else to sell...", "That is all, for now.", },
                    GoTo = { 3, nil },
                },
                BUYSUCCESS = {
                    Speech = "[$3849]",
                    Instruction = { "BUY", "H" },
                    MenuText = { "I want to buy something else.", "That is all, for now.", },
                    GoTo = { 3, nil },
                },
                BUYFAIL = {
                    Speech = "[$3850]",
                    Instruction = { "BUY", "H" },
                    MenuText = { "I want to buy something else.", "That is all, for now.", },
                    GoTo = { 3, nil },
                },
            },
        }
    },
    Train = {
        Branches = {
            {--1
                Speech = "[$3708]",
                Instruction = { "TRAIN", },
                MenuText = { "Train me in a skill...", },
            },
            {--2
                Speech = "[$3709]",
                GetResponses = function(npcObj, playerObj)
                    return Dialogue.GetTrainOptions(npcObj, playerObj)
                end,
            },
            {--3
                TEACH = {
                    Speech = "I can teach you the basics of @1 for @2",
                    -- @1 = skill display name, @2 = training price
                    Instruction = { "SKILLSUCCESS", "H", },
                    MenuText = { "Yes, please.", "Nevermind.", },
                },
                OVER = {
                    Speech = "[$3714]",
                    Instruction = { "H", },
                    MenuText = { "Okay.", },
                },
                MAX = {
                    Speech = "[$3710]",
                    Instruction = { "H", },
                    MenuText = { "Okay.", },
                },
                CAP = {
                    Speech = "[$3712]",
                    Instruction = { "H", },
                    MenuText = { "Okay.", },
                },
                POOR = {
                    Speech = "[$3711]",
                    Instruction = { "H", },
                    MenuText = { "Okay.", },
                },
            },
            {--4
                SKILLSUCCESS = {
                    Speech = "[$3716]",
                    Instruction = { "H", },
                    MenuText = { "Thank you.", },
                },
                SKILLFAIL = {
                    Speech = "[$3715]",
                    Instruction = { "H", },
                    MenuText = { "Okay.", },
                },
            },
        }
    },
    CraftingOrder = {
        Branches = {
            {--1
                Speech = "[$4040]",
                Instruction = { "CO", },
                MenuText = { "About crafting orders...", },
            },
            {--2
                Speech = "[$3846]",
                Instruction = { "REQ", "TURNIN", "H" },
                MenuText = { "What orders do you need completed?", "I have a completed order.", "Actually..." },
            },
            {--3
                REQ = {
                    Speech = "[$4041]",
                    UseResponseVars = true,
                },
                TURNIN = {
                    OnBegin = function(npcObj, playerObj, args)
                        Dialogue.RequestGameObject(npcObj, playerObj, true)
                    end,
                    Speech = "[$3848]",
                    Instruction = { "ELSE", }, -- does this work? I thought it would fail
                    MenuText = { "Nevermind.", },
                    GoTo = { 2 },
                },
            },
            {--4
                REQSUCCESS = {
                    Speech = "[$4044]",
                    Instruction = { "H" },
                    MenuText = { "Thanks for the information.", },
                },
                GAVE = {
                    Speech = "[$4048]",
                    Instruction = { "H" },
                    MenuText = { "Okay.", },
                },
                INEXP = {
                    Speech = "[$4049]",
                    Instruction = { "H" },
                    MenuText = { "Okay.", },
                },
                TURNINSUCCESS = {
                    Speech = "[$4043]",
                    Instruction = { "H" },
                    MenuText = { "Thank you.", },
                },
                TURNINFAIL = {
                    Speech = "[$4045]",
                    Instruction = { "TURNIN", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 3, nil },
                },
                OWN = {
                    Speech = "[$4050]",
                    Instruction = { "TURNIN", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 3, nil },
                },
                QUALIFY = {
                    Speech = "[$4047]",
                    Instruction = { "TURNIN", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 3, nil },
                },
                DONE = {
                    Speech = "[$4046]",
                    Instruction = { "TURNIN", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 3, nil },
                },
                NOT = {
                    Speech = "[$4042]",
                    Instruction = { "TURNIN", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 3, nil },
                },
            },
        }
    },
    AcceptCriminalHeads = {
        Branches = {
            {--1
                Speech = "[$3775]",
                Instruction = { "HEAD", },
                MenuText = { "I have a criminal head to turn in.", },
            },
            {--2
                HEAD = {
                    OnBegin = function(npcObj, playerObj, args)
                        Dialogue.RequestGameObject(npcObj, playerObj, true)
                    end,
                    Speech = "[$3776]",
                    Instruction = { "H", },
                    MenuText = { "Nevermind.", },
                },
            },
            {--3
                HEADSUCCESS = {
                    Speech = "[$3778]",
                    Instruction = { "HEAD", "H" },
                    MenuText = { "I have another head to turn in.", "You're welcome.", },
                    GoTo = { 2, nil },
                },
                HEADFAIL = {
                    Speech = "[$3780]",
                    Instruction = { "HEAD", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 2, nil },
                },
                SCROLLFAIL = {
                    Speech = "[$3777]",
                    Instruction = { "HEAD", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 2, nil },
                },
                ROTTEN = {
                    Speech = "[$3779]",
                    Instruction = { "HEAD", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 2, nil },
                },
                NOT = {
                    Speech = "[$3782]",
                    Instruction = { "HEAD", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 2, nil },
                },
                EMPTY = {
                    Speech = "[$3783]",
                    Instruction = { "HEAD", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 2, nil },
                },
                RAND = {
                    Speech = "[$3781]",
                    Instruction = { "HEAD", "H" },
                    MenuText = { "Let me look again.", "Actually...", },
                    GoTo = { 2, nil },
                },
            },
        }
    },
    Bank = {
        Branches = {
            {--1
                Speech = "[$3793]",
                Instruction = { "BANK", },
                MenuText = { "I want to bank items.", },
            },
            {--2
                OnBegin = function(npcObj, playerObj, args)
                    playerObj:CloseDynamicWindow("NPCInteraction")
                    OpenBank(playerObj, npcObj)
                end,
                Speech = false,
            },
        }
    },
    PayTax = {
        Branches = {
            {--1
                Speech = "[$4001]",
                Instruction = { "TAX", },
                MenuText = { "I want to pay taxes.", },
            },
            {--2
                OnBegin = function(npcObj, playerObj, args)
                    OpenTax(playerObj, npcObj)
                    playerObj:CloseDynamicWindow("NPCInteraction")
                end,
                Speech = false,
            },
        }
    },

    RockPaperScissors = {--Tree
        Branches = {
            {--1 (Branch)
                Speech = "[$4014]",
                SpeechPriority = 1,
                Instruction = { "RPS" },
                MenuText = { "Rock, Paper, Scissors?" },
            },
            {--2
                Speech = "[$4015]",
                Instruction = { "YES", "HOW", "H" },
                MenuText = { "Yes!", "How do I play?", "Not right now."},
            },
            {--3
                YES = {
                    Speech = "[$4016]",
                    Instruction = { "Rock", "Paper", "Scissors" },
                    MenuText = { "Rock.", "Paper.", "Scissors."},
                    InterpretInstruction = function(npcObj, playerObj, instruction)
                        local choices = { "Rock", "Paper", "Scissors" }
                        local opId = choices[math.random(1,3)]
                        local result = "X"
                        if opId == instruction then
                            result = "TIE"
                        elseif (opId == "Rock" and instruction == "Paper") or (opId == "Paper" and instruction == "Scissors") or (opId == "Scissors" and instruction == "Rock") then
                            result = "WIN"
                        else
                            result = "LOSE"
                        end
                        local details = {P = instruction, Opp = opId}
                        return result, details
                    end,
                },
                HOW = {
                    Speech = "[$4017]",
                    Instruction = { "YES", "H" },
                    MenuText = { "Let's play.", "No." },
                    GoTo = { 3, nil },
                },
            },
            {--4
                WIN = {
                    OnBegin = function(npcObj, playerObj, args)
                        local result = Dialogue.GetTempVariables(npcObj, playerObj, "OverwriteDetails")
                        playerObj:NpcSpeech(result.P.."!")
                        npcObj:NpcSpeech(result.Opp.."!")
                        --playerObj:SystemMessage(CombineArgs(result.P, "(YOU) vs", result.Opp, "(THEM): WIN"), "info")
                        --DebugMessage("Player earned 10g")
                    end,
                    Speech = "[$4018]",
                    Instruction = { "YES", "H" },
                    MenuText = { "Want to play again?", "Actually..."},
                    GoTo = { 3, nil },
                },
                LOSE = {
                    OnBegin = function(npcObj, playerObj, args)
                        local result = Dialogue.GetTempVariables(npcObj, playerObj, "OverwriteDetails")
                        playerObj:NpcSpeech(result.P.."!")
                        npcObj:NpcSpeech(result.Opp.."!")
                        --playerObj:SystemMessage(CombineArgs(result.P, "(YOU) vs", result.Opp, "(THEM): LOSE"), "info")
                        --DebugMessage("Player gave up 10g")
                    end,
                    Speech = "[$4020]",
                    Instruction = { "YES", "H" },
                    MenuText = { "Let's do another round!", "Actually..."},
                    GoTo = { 3, nil },
                },
                TIE = {
                    OnBegin = function(npcObj, playerObj, args)
                        local result = Dialogue.GetTempVariables(npcObj, playerObj, "OverwriteDetails")
                        playerObj:NpcSpeech(result.P.."!")
                        npcObj:NpcSpeech(result.Opp.."!")
                        --playerObj:SystemMessage(CombineArgs(result.P, "(YOU) vs", result.Opp, "(THEM): TIE"), "info")
                        --DebugMessage("TIE")
                    end,
                    Speech = "[$4019]",
                    Instruction = { "YES", "H" },
                    MenuText = { "Want to try again?", "I'm good."},
                    GoTo = { 3, nil },
                },
            },
        }
    },
    Township = {
        Branches = {
            {--1
                OnBegin = function(npcObj, playerObj, args)
                    local townshipName = args[1]
                    if townshipName then
                        args.DontSkip = {}
                        local canJoin = playerObj:GetObjVar(TownshipsHelper.TownshipObjVar) ~= townshipName
                        local townshipCaps = string.upper(townshipName)

                        args.DontSkip[#args.DontSkip + 1] = (canJoin and "JOIN"..townshipCaps) or "LEAVE"..townshipCaps
                        --DebugMessage("TOWNSHIP:",townshipName.." (CanJoin? ".. tostring(canJoin)..")")
                    end
                end,
                Speech = "[$3858]",
                Instruction = {
                    "JOINELDEIR", 
                    "LEAVEELDEIR",
                    "JOINHELM",
                    "LEAVEHELM",
                    "JOINPYROS",
                    "LEAVEPYROS",
                },
                MenuText = {
                    "Eldeir Citizenship?", 
                    "Renounce Eldeir Citizenship?",
                    "Helm Citizenship?", 
                    "Renounce Helm Citizenship?",
                    "Pyros Citizenship?", 
                    "Renounce Pyros Citizenship?",
                },
                InterpretInstruction = function(npcObj, playerObj, instruction)
                    local result = instruction
                    --DebugMessage("Interpret IN:",instruction)

                    if string.match(result, "JOIN") then
                        --DebugMessage("Interpreting JOIN")
                        local name = string.gsub(result, "JOIN", "")
                        local playerTownship = playerObj:GetObjVar(TownshipsHelper.TownshipObjVar) or "" 

                        if IsMurderer(playerObj) then --isMurderer
                            --DebugMessage("Failed (MURDERER)")
                            result = "MURDERER"
                        elseif playerObj:HasObjVar(TownshipsHelper.TownshipObjVar) then --hasTownship
                            --DebugMessage("Failed (Has other Township)")
                            result = "CANT"
                        elseif Militia.GetId(playerObj) ~= nil then --hasMilitia
                            --DebugMessage("Failed Join (JMILITIA):",Militia.GetId(playerObj)," : This should not get called.")
                            result = "JMILITIA"
                        end
                    elseif string.match(result, "LEAVE") then
                        --DebugMessage("Interpreting LEAVE")
                        local name = string.gsub(result, "LEAVE", "")
                        local playerTownship = playerObj:GetObjVar(TownshipsHelper.TownshipObjVar) or ""

                        if Militia.GetId(playerObj) ~= nil then --hasMilitia
                            --DebugMessage("Failed (LMILITIA):",Militia.GetId(playerObj)," -> is not nil.")
                            result = "LMILITIA"
                        elseif DateTime.Compare(DateTime.UtcNow, playerObj:GetObjVar("CanLeaveTownship") or DateTime.MinValue) < 0 then --leaveTimerPassed
                            --DebugMessage("Failed (TIMER):",DateTime.UtcNow, playerObj:GetObjVar("CanLeaveTownship"), DateTime.Compare(DateTime.UtcNow, playerObj:GetObjVar("CanLeaveTownship") or DateTime.MinValue))
                            result = "TIMER"
                        end
                    end

                    --DebugMessage("Interpret OUT:",result)
                    return result, details
                end,
            },
            {--2
                JOINELDEIR = {
                    Speech = "[$3868]",
                    Instruction = { "JOINELDEIR", "H" },
                    MenuText = { "I'm interested!", "Let me think it over."},
                },
                LEAVEELDEIR = {
                    Speech = "[$3866]",
                    Instruction = { "LEAVEELDEIR", "H" },
                    MenuText = { "Yes, I am sure.", "Nevermind."},
                },
                JOINHELM = {
                    Speech = "[$3863]",
                    Instruction = { "JOINHELM", "H" },
                    MenuText = { "I want to join.", "I need more time to think about it."},
                },
                LEAVEHELM = {
                    Speech = "[$3859]",
                    Instruction = { "LEAVEHELM", "H" },
                    MenuText = { "Yes, I am sure.", "Nevermind."},
                },
                JOINPYROS = {
                    Speech = "[$3861]",
                    Instruction = { "JOINPYROS", "H" },
                    MenuText = { "I'm in!", "I got to think about it."},
                },
                LEAVEPYROS = {
                    Speech = "[$3864]",
                    Instruction = { "LEAVEPYROS", "H" },
                    MenuText = { "Yes, I am sure.", "Nevermind."},
                },
                MURDERER = {
                    Speech = "[$3869]",
                    Instruction = { "H" },
                    MenuText = { "Okay."},
                },
                CANT = {
                    Speech = "[$3862]",
                    Instruction = { "H" },
                    MenuText = { "Okay."},
                },
                JMILITIA = {
                    Speech = "[$3867]",
                    Instruction = { "H" },
                    MenuText = { "Okay."},
                },
                LMILITIA = {
                    Speech = "[$3860]",
                    Instruction = { "H" },
                    MenuText = { "Okay."},
                },
                TIMER = {
                    Speech = "[$3865]",
                    Instruction = { "H" },
                    MenuText = { "Okay."},
                },
            },
            {--3
                JOINELDEIR = {
                    OnBegin = function(npcObj, playerObj, args)
                        DebugMessage("JOIN ELDEIR CONFIRMED")
                        TownshipsHelper.AddPlayerToTownship( playerObj, "Eldeir" )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                        playerObj:SetObjVar("CanLeaveTownship", DateTime.UtcNow:Add(TimeSpan.FromHours(1)))
                        playerObj:SystemMessage("You have joined the Eldeir Township.","info")
                    end,
                    Speech = "[$3874]",
                    Instruction = { "H", "" },
                    MenuText = { "I have something else to speak with you about.", "That is all, for now." },
                },
                LEAVEELDEIR = {
                    OnBegin = function(npcObj, playerObj, args)
                        DebugMessage("LEAVE ELDEIR CONFIRMED")
                        TownshipsHelper.RemovePlayerFromTownship( playerObj )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                        playerObj:SystemMessage("You have left the Eldeir Township.","info")
                    end,
                    Speech = "[$3872]",
                    Instruction = { "H", "" },
                    MenuText = { "I have something else to speak with you about.", "Goodbye." },
                },
                JOINHELM = {
                    OnBegin = function(npcObj, playerObj, args)
                        DebugMessage("JOIN HELM CONFIRMED")
                        TownshipsHelper.AddPlayerToTownship( playerObj, "Helm" )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                        playerObj:SetObjVar("CanLeaveTownship",  DateTime.UtcNow:Add(TimeSpan.FromHours(1)))
                        playerObj:SystemMessage("You have joined the Helm Township.","info")
                    end,
                    Speech = "[$3875]",
                    Instruction = { "H", "" },
                    MenuText = { "I have something else to speak with you about.", "That is all, for now." },
                },
                LEAVEHELM = {
                    OnBegin = function(npcObj, playerObj, args)
                        DebugMessage("LEAVE HELM CONFIRMED")
                        TownshipsHelper.RemovePlayerFromTownship( playerObj )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                        playerObj:SystemMessage("You have left the Helm Township.","info")
                    end,
                    Speech = "[$3870]",
                    Instruction = { "H", "" },
                    MenuText = { "I have something else to speak with you about.", "Goodbye." },
                },
                JOINPYROS = {
                    OnBegin = function(npcObj, playerObj, args)
                        DebugMessage("JOIN PYROS CONFIRMED")
                        TownshipsHelper.AddPlayerToTownship( playerObj, "Pyros" )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                        playerObj:SetObjVar("CanLeaveTownship",  DateTime.UtcNow:Add(TimeSpan.FromHours(1)))
                        playerObj:SystemMessage("You have joined the Pyros Township.","info")
                    end,
                    Speech = "[$3873]",
                    Instruction = { "H", "" },
                    MenuText = { "I have something else to speak with you about.", "That is all, for now." },
                },
                LEAVEPYROS = {
                    OnBegin = function(npcObj, playerObj, args)
                        DebugMessage("LEAVE PYROS CONFIRMED")
                        TownshipsHelper.RemovePlayerFromTownship( playerObj )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                        playerObj:SystemMessage("You have left the Pyros Township.","info")
                    end,
                    Speech = "[$3871]",
                    Instruction = { "H", "" },
                    MenuText = { "I have something else to speak with you about.", "Goodbye." },
                },
            },
        },
    },

    --
    -- PLAIN DIALOGUE
    --
    Home = {
        Branches = {
            {--1
                Speech = "[$4094]",
                SpeechPriority = 0,
                Instruction = { "H" },
                MenuText = { "I have something else to ask..." },
            },
        },
    },
    GeneralMayor = {
        Branches = {
            {--1
                Speech = "[$3958]",
                SpeechPriority = 1,
                Instruction = { "ASK" },
                MenuText = { "Question for a Mayor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3785]",
                    Instruction = { "WHO", "KNOW", "WHAT", "H" },
                    MenuText = { "I wanna know-", "Whoooooooo are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3786]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                KNOW = {
                    Speech = "[$3787]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                WHAT = {
                    Speech = "[$3788]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    GeneralBlacksmith = {
        Branches = {
            {--1
                Speech = "[$3784]",
                SpeechPriority = 1,
                Instruction = { "ASK" },
                MenuText = { "Question for a Blacksmith..." },
            },
            {--2
                ASK = {
                    Speech = "[$3785]",
                    Instruction = { "WHO", "KNOW", "WHAT", "H" },
                    MenuText = { "I wanna know-", "Whoooooooo are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3786]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                KNOW = {
                    Speech = "[$3787]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                WHAT = {
                    Speech = "[$3788]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },


    -- Upper Plains: Eldeir

    --cel_mayor
    EldeirMayor = {
        Branches = {
            {--1
                Speech = "[$4060]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Eldeir Mayor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3731]",
                    Instruction = { "WHO", "KNOW", "WHAT", "H" },
                    MenuText = { "Who are you?", "I want to know about...", "What is this village?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4061]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Mayor?", "Actually..."},
                },
                KNOW = {
                    Speech = "[$3731]",
                    Instruction = { "PETRA", "TETHYS", "H" },
                    MenuText = { "Petra?", "Tethys?", "Actually..."},
                },
                WHAT = {
                    Speech = "[$4062]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                PETRA = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "PetraHistory", {1}, true)
                    end,
                    Speech = "[$4063]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                TETHYS = {
                    Speech = "[$4064]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                WHY = {
                    Speech = "[$4065]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --cel_merchant_scribe
    EldeirScribe = {
        Branches = {
            {--1
                Speech = "[$3730]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Eldeir Scribe..." },
            },
            {--2
                ASK = {
                    Speech = "[$3731]",
                    Instruction = { "WHO", "KNOW", "H" },
                    MenuText = { "Who are you?", "What do you know about the giant forcefield behind you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    -- 'As Scribe... see to' for 'I'm the Scribe... seein' to
                    Speech = "[$3732]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Scribe?", "Actually..."},
                },
                KNOW = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "EldeirHistory", {1}, true)
                    end,
                    Speech = "[$3733]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3734]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --cel_merchant_bartender
    EldeirInnkeeper = {
        Branches = {
            {--1
                Speech = "[$3853]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Eldeir Innkeeper..." },
            },
            {--2
                ASK = {
                    Speech = "[$3854]",
                    Instruction = { "WHO", "WHAT", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3855]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Innkeeper?", "Actually..."},
                },
                WHAT = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "EldeirHistory", {2}, true)
                    end,
                    Speech = "[$3856]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3857]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --cel_merchant_general_store
    EldeirTrader = {
        Branches = {
            {--1
                Speech = "[$3968]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Eldeir Trader..." },
            },
            {--2
                ASK = {
                    Speech = "[$3740]",
                    Instruction = { "WHO", "KNOW", "H" },
                    MenuText = { "Who are you?", "What do you know about the Dead Gate?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3969]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Trader?", "Actually..."},
                },
                KNOW = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "EldeirHistory", {5}, true)
                    end,
                    Speech = "[$3970]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3971]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --cel_merchant_bartenders_wife
    EldeirInnkeeperWife = {
        Branches = {
            {--1
                Speech = "[$4051]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for you, ma'am..." },
            },
            {--2
                ASK = {
                    Speech = "[$4052]",
                    Instruction = { "WHO", "WHAT", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4053]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why do you work here?", "Actually..."},
                },
                WHAT = {
                    Speech = "[$4054]",
                    Instruction = { "REMEMBER", "H" },
                    MenuText = { "I don't remember.", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$4056]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                REMEMBER = {
                    Speech = "[$4055]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --cel_blacksmith
    EldeirBlacksmith = {
        Branches = {
            {--1
                Speech = "[$3798]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Eldeir Blacksmith..." },
            },
            {--2
                ASK = {
                    Speech = "[$3698]",
                    Instruction = { "WHO", "JOBS", "H" },
                    MenuText = { "Who are you?", "What do you know about Jobs in Eldeir?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3799]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Blacksmith?", "Actually..."},
                },
                JOBS = {
                    Speech = "[$3800]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3801]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --cel_merchant_armorer
    EldeirTailor = {
        Branches = {
            {--1
                Speech = "[$4116]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Eldeir Tailor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3936]",
                    Instruction = { "WHO", "TETHYS", "H" },
                    MenuText = { "Who are you?", "What do you know about Tethys?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4117]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tailor?", "Actually..."},
                },
                TETHYS = {
                    Speech = "[$4118]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4119]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --cel_merchant_carpenter
    EldeirCarpenter = {
        Branches = {
            {--1
                Speech = "[$3911]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Eldeir Carpenter..." },
            },
            {--2
                ASK = {
                    Speech = "[$3912]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3913]",
                    Instruction = { "WHY", "TOWN", "H" },
                    MenuText = { "Why are you the Carpenter?", "The little town?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3915]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                TOWN = {
                    Speech = "[$3914]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --cel_merchant_alchemist
    EldeirAlchemist = {
        Branches = {
            {--1
                Speech = "[$4073]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Eldeir Alchemist..." },
            },
            {--2
                ASK = {
                    Speech = "[$3703]",
                    Instruction = { "WHO", "PLACE", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4113]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you an Alchemist?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$4114]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--3
                WHY = {
                    Speech = "[$4115]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --cel_merchant_tinkerer
    EldeirTinkerer = {
        Branches = {
            {--1
                Speech = "[$4002]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Eldeir Tinkerer..." },
            },
            {--2
                ASK = {
                    Speech = "[$3985]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4003]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tinkerer?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$4004]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },
    -- END Eldeir

    -- Trinit

    --trin_blacksmith
    TrinitBlacksmith = {
        Branches = {
            {--1
                Speech = "[$3755]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Trinit Blacksmith..." },
            },
            {--2
                ASK = {
                    Speech = "[$3756]",
                    Instruction = { "WHO", "PLACE", "TEACH", "SELL", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Can you teach me...", "I want to sell...", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3757]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Blacksmith?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$3758]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                TEACH = {
                    Speech = "[$3759]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                SELL = {
                    Speech = "[$3760]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3761]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --trin_tailor
    TrinitTailor = {
        Branches = {
            {--1
                Speech = "[$4026]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Trinit Tailor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3718]",
                    Instruction = { "WHO", "MOHANJOT", "TEACH", "SELL", "H" },
                    MenuText = { "Who are you?", "What do you know about Mohanjot?", "Can you teach me...", "I want to sell...", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4027]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tailor?", "Actually..."},
                },
                MOHANJOT = {
                    Speech = "[$4030]",
                    Instruction = { "ASK", "X" },
                    MenuText = { "I have another question.", "Goodbye."},
                    GoTo = { 2, nil },
                },
                TEACH = {
                    Speech = "[$4028]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                SELL = {
                    Speech = "[$4029]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4031]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --trinit_architect
    TrinitCarpenter = {
        Branches = {
            {--1
                Speech = "[$4106]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Trinit Carpenter..." },
            },
            {--2
                ASK = {
                    Speech = "[$4107]",
                    Instruction = { "WHO", "MOHANJOT", "TEACH", "SELL", "H" },
                    MenuText = { "Who are you?", "What do you know about Mohanjot?", "Can you teach me...", "I want to sell...", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4108]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Carpenter?", "Actually..."},
                },
                MOHANJOT = {
                    Speech = "[$4111]",
                    Instruction = { "ASK", "X" },
                    MenuText = { "I have another question.", "Goodbye."},
                    GoTo = { 2, nil },
                },
                TEACH = {
                    Speech = "[$4109]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                SELL = {
                    Speech = "[$4110]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4112]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    -- Southern Rim: Pyros

    --cel_mayor_pyros
    PyrosMayor = {
        Branches = {
            {--1
                Speech = "[$4099]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Pyros Mayor..." },
            },
            {--2
                ASK = {
                    Speech = "[$4100]",
                    Instruction = { "WHO", "KNOW", "WHAT", "H" },
                    MenuText = { "Who are you?", "I want to know about...", "What is this village?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4101]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Mayor?", "Actually..."},
                },
                KNOW = {
                    Speech = "[$3809]",
                    Instruction = { "PETRA", "TETHYS", "H" },
                    MenuText = { "Petra?", "Tethys?", "Actually..."},
                },
                WHAT = {
                    Speech = "[$4102]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                PETRA = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "PetraHistory", {1}, true)
                    end,
                    Speech = "[$4103]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                TETHYS = {
                    Speech = "[$4104]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                WHY = {
                    Speech = "[$4105]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --pyros_scribe
    PyrosScribe = {
        Branches = {
            {--1
                Speech = "[$3959]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Pyros Scribe..." },
            },
            {--2
                ASK = {
                    Speech = "[$3731]",
                    Instruction = { "WHO", "KNOW", "H" },
                    MenuText = { "Who are you?", "What do you know about Petra?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3960]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Scribe?", "Actually..."},
                },
                KNOW = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "PyrosHistory", {1}, true)
                    end,
                    Speech = "[$3961]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3962]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --pyros_merchant_bartender
    PyrosInnkeeper = {
        Branches = {
            {--1
                Speech = "[$3917]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Pyros Innkeeper..." },
            },
            {--2
                ASK = {
                    Speech = "[$3918]",
                    Instruction = { "WHO", "WHAT", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3919]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Innkeeper?", "Actually..."},
                },
                WHAT = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "PyrosHistory", {2}, true)
                    end,
                    Speech = "[$3920]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3921]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --pyros_carpenter
    PyrosCarpenter = {
        Branches = {
            {--1
                Speech = "[$3993]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Pyros Carpenter..." },
            },
            {--2
                ASK = {
                    Speech = "[$3905]",
                    Instruction = { "WHO", "WHAT", "H" },
                    MenuText = { "Who are you?", "What do you know about the graveyard?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3994]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Carpenter?", "Actually..."},
                },
                WHAT = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "PyrosHistory", {5}, true)
                    end,
                    Speech = "[$3995]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3996]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --pyros_merchant_general_store
    PyrosTrader = {
        Branches = {
            {--1
                Speech = "[$3739]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Pyros Trader..." },
            },
            {--2
                ASK = {
                    Speech = "[$3740]",
                    Instruction = { "WHO", "JOBS", "H" },
                    MenuText = { "Who are you?", "What do you know about Jobs in Pyros?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3741]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Trader?", "Actually..."},
                },
                JOBS = {
                    Speech = "[$3742]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3743]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --pyros_blacksmith
    PyrosBlacksmith = {
        Branches = {
            {--1
                Speech = "[$3789]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Pyros Blacksmith..." },
            },
            {--2
                ASK = {
                    Speech = "[$3698]",
                    Instruction = { "WHO", "TETHYS", "H" },
                    MenuText = { "Who are you?", "What do you know about Tethys?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3790]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Blacksmith?", "Actually..."},
                },
                TETHYS = {
                    Speech = "[$3791]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3792]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --pyros_alchemist
    PyrosAlchemist = {
        Branches = {
            {--1
                Speech = "[$3721]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Pyros Alchemist..." },
            },
            {--2
                ASK = {
                    Speech = "[$3703]",
                    Instruction = { "WHO", "PLACE", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3722]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you an Alchemist?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$3723]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--3
                WHY = {
                    Speech = "[$3724]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --pyros_tailor
    PyrosTailor = {
        Branches = {
            {--1
                Speech = "[$4095]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Pyros Tailor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3936]",
                    Instruction = { "WHO", "PLACE", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4096]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tailor?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$4097]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4098]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --pyros_merchant_fish2
    PyrosFisherTidus = {
        Branches = {
            {--1
                Speech = "[$3766]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for you..." },
            },
            {--2
                ASK = {
                    Speech = "[$3767]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3768]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you a Fisher?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3769]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --pyros_merchant_fish
    PyrosFisherJustin = {
        Branches = {
            {--1
                Speech = "[$4120]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for you..." },
            },
            {--2
                ASK = {
                    Speech = "[$4121]",
                    Instruction = { "H" },
                    MenuText = { "Okay."},
                },
            },
        }
    },
    
    --pyros_tinkerer
    PyrosTinkerer = {
        Branches = {
            {--1
                Speech = "[$3984]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Pyros Tinkerer..." },
            },
            {--2
                ASK = {
                    Speech = "[$3985]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3986]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tinkerer?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3987]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },
    -- END Pyros

    -- Eastern Frontier: Helm

    --cel_mayor_helm
    HelmMayor = {
        Branches = {
            {--1
                Speech = "[$3806]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Helm Mayor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3807]",
                    Instruction = { "WHO", "KNOW", "WHAT", "H" },
                    MenuText = { "Who are you?", "I want to know about...", "What is this village?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3808]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Mayor?", "Actually..."},
                },
                KNOW = {
                    Speech = "[$3809]",
                    Instruction = { "PETRA", "TETHYS", "H" },
                    MenuText = { "Petra?", "Tethys?", "Actually..."},
                },
                WHAT = {
                    Speech = "[$3810]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                PETRA = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "PetraHistory", {1}, true)
                    end,
                    Speech = "[$3811]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                TETHYS = {
                    Speech = "[$3812]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                WHY = {
                    Speech = "[$3813]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --helm_merchant_scribe
    HelmScribe = {
        Branches = {
            {--1
                Speech = "[$3794]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Helm Scribe..." },
            },
            {--2
                ASK = {
                    Speech = "[$3731]",
                    Instruction = { "WHO", "KNOW", "H" },
                    MenuText = { "Who are you?", "What do you know about Petra?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3795]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Scribe?", "Actually..."},
                },
                KNOW = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "HelmHistory", {1}, true)
                    end,
                    Speech = "[$3796]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3797]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --helm_merchant_bartender
    HelmInnkeeper = {
        Branches = {
            {--1
                Speech = "[$3890]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Helm Innkeeper..." },
            },
            {--2
                ASK = {
                    Speech = "[$3891]",
                    Instruction = { "WHO", "WHAT", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3892]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Innkeeper?", "Actually..."},
                },
                WHAT = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "HelmHistory", {2}, true)
                    end,
                    Speech = "[$3893]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3894]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --helm_merchant_general_store
    HelmTrader = {
        Branches = {
            {--1
                Speech = "[$3950]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Helm Trader..." },
            },
            {--2
                ASK = {
                    Speech = "[$3740]",
                    Instruction = { "WHO", "KNOW", "H" },
                    MenuText = { "Who are you?", "What do you know about the Lich Area?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3951]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Trader?", "Actually..."},
                },
                KNOW = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "HelmHistory", {5}, true)
                    end,
                    Speech = "[$3952]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3953]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --helm_tinkerer
    HelmTinkerer = {
        Branches = {
            {--1
                Speech = "[$3935]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Helm Tinkerer..." },
            },
            {--2
                ASK = {
                    Speech = "[$3936]",
                    Instruction = { "WHO", "JOBS", "H" },
                    MenuText = { "Who are you?", "What do you know about Jobs in Helm?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3937]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tinkerer?", "Actually..."},
                },
                JOBS = {
                    Speech = "[$3938]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3939]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --helm_smith
    HelmBlacksmith = {
        Branches = {
            {--1
                Speech = "[$3735]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Helm Blacksmith..." },
            },
            {--2
                ASK = {
                    Speech = "[$3698]",
                    Instruction = { "WHO", "TETHYS", "H" },
                    MenuText = { "Who are you?", "What do you know about Tethys?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3736]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Blacksmith?", "Actually..."},
                },
                TETHYS = {
                    Speech = "[$3737]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3738]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --helm_carpenter
    HelmCarpenter = {
        Branches = {
            {--1
                Speech = "[$4081]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Helm Carpenter..." },
            },
            {--2
                ASK = {
                    Speech = "[$3905]",
                    Instruction = { "WHO", "FOREST", "H" },
                    MenuText = { "Who are you?", "What do you know about the forest to the south?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4082]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Carpenter?", "Actually..."},
                },
                FOREST = {
                    Speech = "[$4083]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4084]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --helm_tailor
    HelmTailor = {
        Branches = {
            {--1
                Speech = "[$4090]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Helm Tailor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3936]",
                    Instruction = { "WHO", "PLACE", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4091]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tailor?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$4092]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4093]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --helm_herbalist
    HelmAlchemist = {
        Branches = {
            {--1
                Speech = "[$4073]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Helm Alchemist..." },
            },
            {--2
                ASK = {
                    Speech = "[$3703]",
                    Instruction = { "WHO", "VALUS", "H" },
                    MenuText = { "Who are you?", "What can you tell me about Valus?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4074]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you an Alchemist?", "Actually..."},
                },
                VALUS = {
                    Speech = "[$4075]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--3
                WHY = {
                    Speech = "[$4076]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },
    -- END Helm

    -- Southern Hills: Valus

    --val_scribe
    ValusScribe = {
        Branches = {
            {--1
                Speech = "[$3988]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Valus Scribe..." },
            },
            {--2
                ASK = {
                    Speech = "[$3989]",
                    Instruction = { "WHO", "WHAT", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3990]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Scribe?", "Actually..."},
                },
                WHAT = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "ValusHistory", {1}, true)
                    end,
                    Speech = "[$3991]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3992]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --val_bartender
    ValusInnkeeper = {
        Branches = {
            {--1
                Speech = "[$3770]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Valus Innkeeper..." },
            },
            {--2
                ASK = {
                    Speech = "[$3771]",
                    Instruction = { "WHO", "WHAT", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3772]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Innkeeper?", "Actually..."},
                },
                WHAT = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "ValusHistory", {2}, true)
                    end,
                    Speech = "[$3773]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3774]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --val_merchant
    ValusTrader = {
        Branches = {
            {--1
                Speech = "[$3744]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Valus Trader..." },
            },
            {--2
                ASK = {
                    Speech = "[$3740]",
                    Instruction = { "WHO", "GRAVE", "H" },
                    MenuText = { "Who are you?", "Why do you need both a cemetery and a graveyard?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3745]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Trader?", "Actually..."},
                },
                GRAVE = {
                    Speech = "[$3746]",
                    Instruction = { "PLEASE", "H" },
                    MenuText = { "Please tell me.", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3748]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                PLEASE = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "ValusHistory", {5}, true)
                    end,
                    Speech = "[$3747]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --val_tailor
    ValusTailor = {
        Branches = {
            {--1
                Speech = "[$3954]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Valus Tailor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3827]",
                    Instruction = { "WHO", "FLAME", "H" },
                    MenuText = { "Who are you?", "What do you know about the blue flame in the mage tower?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3955]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tailor?", "Actually..."},
                },
                FLAME = {
                    Speech = "[$3956]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3957]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --val_mage_elder_didacus
    ElderMageDidacus = {
        Branches = {
            {--1
                Speech = "[$3820]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for you..." },
            },
            {--2
                ASK = {
                    Speech = "[$3767]",
                    Instruction = { "WHO", "PLACE", "FLAME", "H" },
                    MenuText = { "Who are you?", "What is this place?", "What is the Blue Flame?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3821]",
                    Instruction = { "WHY", "VALUS", "H" },
                    MenuText = { "Why are you an Elder Mage?", "Valus Citizenship?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$3822]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                FLAME = {
                    Speech = "[$3823]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3825]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                VALUS = {
                    Speech = "[$3824]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --val_mage_elder_ismeria
    ElderMageIsmeria = {
        Branches = {
            {--1
                Speech = "[$3942]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for you..." },
            },
            {--2
                ASK = {
                    Speech = "[$3767]",
                    Instruction = { "WHO", "PLACE", "FLAME", "H" },
                    MenuText = { "Who are you?", "What is this place?", "What is the Blue Flame?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3943]",
                    Instruction = { "WHY", "VALUS", "H" },
                    MenuText = { "Why are you an Elder Mage?", "Valus Citizenship?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$3822]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                FLAME = {
                    Speech = "[$3823]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3945]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                VALUS = {
                    Speech = "[$3944]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    ElderMageLoreAccreditation = {
        Branches = {
            {--1
                Speech = "Do you happen to be collecting knowledge from around New Celador?",
                SpeechPriority = 5,
                Instruction = { "ACC"},
                MenuText = { "Can you accredit this Catalog?"},
            },
            {--2
                ACC = {
                    Speech = "We sure can. Be aware that once accredited, we will mark this catalog in your name and will only accept it by you and no one else in the future!",
                    Instruction = { "ACCREDIT", "H" },
                    MenuText = { "I agree to what Accreditation means.", "Actually..."},
                },
            },
            {--3
                ACCREDIT = {
                    OnBegin = function(npcObj, playerObj, args)
                        Dialogue.RequestGameObject(npcObj, playerObj, true)
                    end,
                    Speech = "Let me see your progress, then.",
                    Instruction = { "ELSE", },
                    MenuText = { "Nevermind.", },
                    GoTo = { 2 },
                },
            },
            {--4
                ACCREDITSUCCESS = {
                    OnBegin = function(npcObj, playerObj, args)
                        CheckAchievementStatus(playerObj, "Lore", "CeladorianHistorian", 1)
                    end,
                    Speech = "Great work gathering all of this information! We are happy to accredit an adventurer as excited to gather the world's histories as we are.",
                    Instruction = { "H" },
                    MenuText = { "Thanks for the help.", },
                },
                ACCREDITFAIL = {
                    Speech = "We won't accredit a Catalog until it is entirely completed. Please continue your search.",
                    Instruction = { "H" },
                    MenuText = { "Okay.", },
                },
                DIFFCATALOG = {
                    Speech = "You have already been accredited to another Catalog.",
                    Instruction = { "H" },
                    MenuText = { "Okay.", },
                },
                NOTCATALOG = {
                    Speech = "That is not a Catalog.",
                    Instruction = { "H" },
                    MenuText = { "Thanks for checking.", },
                },
                NOTOWNER = {
                    Speech = "You aren't this Catalog's owner.",
                    Instruction = { "H" },
                    MenuText = { "Thanks for checking.", },
                },
                NOTACCREDITEE = {
                    Speech = "This Catalog was already accredited to someone else.",
                    Instruction = { "H" },
                    MenuText = { "Thanks for checking.", },
                },
            },
        }
    },
    
    --val_tinkerer
    ValusTinkerer = {
        Branches = {
            {--1
                Speech = "[$4070]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Valus Tinkerer..." },
            },
            {--2
                ASK = {
                    Speech = "[$3985]",
                    Instruction = { "WHO", "FLAME", "H" },
                    MenuText = { "Who are you?", "What do you know about the blue flame in the mage tower?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4071]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tinkerer?", "Actually..."},
                },
                FLAME = {
                    Speech = "[$3956]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4072]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },
    
    --val_herbalist
    ValusAlchemistApprentice = {
        Branches = {
            {--1
                Speech = "[$3802]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for you..." },
            },
            {--2
                ASK = {
                    Speech = "[$3703]",
                    Instruction = { "WHO", "PLACE", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3803]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you an Alchemist?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$3804]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--3
                WHY = {
                    Speech = "[$3805]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --val_alchemist
    ValusAlchemist = {
        Branches = {
            {--1
                Speech = "[$3702]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Valus Alchemist..." },
            },
            {--2
                ASK = {
                    Speech = "[$3703]",
                    Instruction = { "WHO", "GODS", "H" },
                    MenuText = { "Who are you?", "What do you know about the Elemental Gods?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3704]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you an Alchemist?", "Actually..."},
                },
                GODS = {
                    Speech = "[$3705]",
                    Instruction = { "WHERE", "H" },
                    MenuText = { "Where is that?", "Actually..."},
                },
            },
            {--3
                WHY = {
                    Speech = "[$3707]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                WHERE = {
                    Speech = "[$3706]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --val_blacksmith
    ValusBlacksmith = {
        Branches = {
            {--1
                Speech = "[$3697]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Valus Blacksmith..." },
            },
            {--2
                ASK = {
                    Speech = "[$3698]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3699]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Blacksmith?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3700]",
                    Instruction = { "HELM", "H" },
                    MenuText = { "Why did you leave Helm?", "Actually..."},
                },
            },
            {--5
                HELM = {
                    Speech = "[$3701]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --val_carpenter
    ValusCarpenter = {
        Branches = {
            {--1
                Speech = "[$3904]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Valus Carpenter..." },
            },
            {--2
                ASK = {
                    Speech = "[$3905]",
                    Instruction = { "WHO", "TOWNS", "H" },
                    MenuText = { "Who are you?", "What can you tell me about the other towns?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3906]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Carpenter?", "Actually..."},
                },
                TOWNS = {
                    Speech = "[$3907]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3819]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },
    -- END Valus

    -- Valus Cemetery

    --monolith_explorer
    DoranTheExplorer = {
        Branches = {
            {--1
                Speech = "[$3977]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for you, sir..." },
            },
            {--2
                ASK = {
                    Speech = "[$3978]",
                    Instruction = { "WHO", "PLACE", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3979]",
                    Instruction = { "FAMILY", "H" },
                    MenuText = { "Your family?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$3980]",
                    Instruction = { "JIANNA", "TREASURE", "H" },
                    MenuText = { "Jianna the Red?", "What treasures might I find?", "Actually..."},
                },
            },
            {--4
                FAMILY = {
                    Speech = "[$3981]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                JIANNA = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "Monolith", {1}, true)
                    end,
                    Speech = "[$3982]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                TREASURE = {
                    Speech = "[$3983]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    -- Belhaven

    --bel_merchant_fish
    BelhavenFisherPop = {
        Branches = {
            {--1
                Speech = "[$4066]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for you..." },
            },
            {--2
                ASK = {
                    Speech = "[$3767]",
                    Instruction = { "WHO", "PLACE", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4067]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you a Fisher?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$4068]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4069]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --bel_alchemist
    BelhavenAlchemist = {
        Branches = {
            {--1
                Speech = "[$3762]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Belhaven Alchemist..." },
            },
            {--2
                ASK = {
                    Speech = "[$3763]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3764]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Alchemist?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3765]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --bel_blacksmith
    BelhavenBlacksmith = {
        Branches = {
            {--1
                Speech = "[$4057]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Belhaven Blacksmith..." },
            },
            {--2
                ASK = {
                    Speech = "[$4022]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4058]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Blacksmith?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$4059]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --bel_architect
    BelhavenCarpenter = {
        Branches = {
            {--1
                Speech = "[$3814]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Belhaven Carpenter..." },
            },
            {--2
                ASK = {
                    Speech = "[$3815]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3895]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Carpenter?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3896]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    -- END Southern Hills

    -- Barren Lands: Oasis

    --barrens_general_store
    OasisTrader = {
        Branches = {
            {--1
                Speech = "[$4035]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Oasis Trader..." },
            },
            {--2
                ASK = {
                    Speech = "[$4036]",
                    Instruction = { "WHO", "PETRA", "H" },
                    MenuText = { "Who are you?", "What do you know about Petra?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4037]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Trader?", "Actually..."},
                },
                PETRA = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "OasisHistory", {1}, true)
                    end,
                    Speech = "[$4038]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4039]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --barrens_alchemist
    OasisAlchemist = {
        Branches = {
            {--1
                Speech = "[$3835]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Oasis Alchemist..." },
            },
            {--2
                ASK = {
                    Speech = "[$3836]",
                    Instruction = { "WHO", "WHAT", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3837]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Alchemist?", "Actually..."},
                },
                WHAT = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "OasisHistory", {2}, true)
                    end,
                    Speech = "[$3838]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3839]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --barrens_tinkerer
    OasisTinkerer = {
        Branches = {
            {--1
                Speech = "[$3935]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Oasis Tinkerer..." },
            },
            {--2
                ASK = {
                    Speech = "[$3827]",
                    Instruction = { "WHO", "GAZER", "H" },
                    MenuText = { "Who are you?", "What do you know about Gazer Isle?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4032]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tinkerer?", "Actually..."},
                },
                GAZER = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "OasisHistory", {5}, true)
                    end,
                    Speech = "[$4033]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4034]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --barrens_blacksmith
    OasisBlacksmith = {
        Branches = {
            {--1
                Speech = "[$4021]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Oasis Blacksmith..." },
            },
            {--2
                ASK = {
                    Speech = "[$4022]",
                    Instruction = { "WHO", "PLACE", "H" },
                    MenuText = { "Who are you?", "What can you tell me about this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4023]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Blacksmith?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$4024]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4025]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --barrens_merchant_armorer
    OasisTailor = {
        Branches = {
            {--1
                Speech = "[$3881]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Oasis Tailor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3827]",
                    Instruction = { "WHO", "CULT", "H" },
                    MenuText = { "Who are you?", "What do you know about the cult in the ruins?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3882]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tailor?", "Actually..."},
                },
                CULT = {
                    Speech = "[$3883]",
                    Instruction = { "WOMAN", "H" },
                    MenuText = { "The eternal woman?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3885]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                WOMAN = {
                    Speech = "[$3884]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --barrens_carpenter
    OasisCarpenter = {
        Branches = {
            {--1
                Speech = "[$3814]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Oasis Carpenter..." },
            },
            {--2
                ASK = {
                    Speech = "[$3815]",
                    Instruction = { "WHO", "VALUS", "H" },
                    MenuText = { "Who are you?", "What can you tell me about Valus?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3816]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Carpenter?", "Actually..."},
                },
                VALUS = {
                    Speech = "[$3817]",
                    Instruction = { "FLAME", "H" },
                    MenuText = { "The flame?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3819]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                FLAME = {
                    Speech = "[$3818]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    -- END Oasis

    --Oasis Graveyard

    --barrens_graveyard_loner
    Golgatha = {
        Branches = {
            {--1
                Speech = "[$4005]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for you..." },
            },
            {--2
                ASK = {
                    Speech = "[$4006]",
                    Instruction = { "WHO", "PLACE", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4007]",
                    Instruction = { "FLAW", "GOD", "H" },
                    MenuText = { "How are you flawed?", "A false god?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$4008]",
                    Instruction = { "BODIES", "H" },
                    MenuText = { "Where are the bodies?", "Actually..."},
                },
            },
            {--4
                FLAW = {
                    Speech = "[$4011]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                GOD = {
                    Speech = "[$4009]",
                    Instruction = { "GIRL", "H" },
                    MenuText = { "What girl?", "Actually..."},
                },
                BODIES = {
                    Speech = "[$4010]",
                    Instruction = { "RESOURCE", "H" },
                    MenuText = { "Resource?", "Actually..."},
                },
            },
            {--5
                GIRL = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "CiarraJournal", {1,2,3,4,5}, true)
                    end,
                    Speech = "[$4013]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                RESOURCE = {
                    Speech = "[$4012]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    -- END Barren Lands

    -- Black Forest: Outpost

    --treehouse_general_store
    BlackForestOutpostTrader = {
        Branches = {
            {--1
                Speech = "[$3725]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Black Forest Outpost Trader..." },
            },
            {--2
                ASK = {
                    Speech = "[$3726]",
                    Instruction = { "WHO", "KNOW", "H" },
                    MenuText = { "Who are you?", "What do you know about this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3727]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Trader?", "Actually..."},
                },
                KNOW = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "BlackForestHistory", {1}, true)
                    end,
                    Speech = "[$3728]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3729]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --treehouse_merchant_bartender
    BlackForestOutpostInnkeeper = {
        Branches = {
            {--1
                Speech = "[$3840]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Black Forest Outpost Innkeeper..." },
            },
            {--2
                ASK = {
                    Speech = "[$3841]",
                    Instruction = { "WHO", "WHAT", "H" },
                    MenuText = { "Who are you?", "What is this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3842]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Innkeeper?", "Actually..."},
                },
                WHAT = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "BlackForestHistory", {2}, true)
                    end,
                    Speech = "[$3843]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3844]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --treehouse_alchemist
    BlackForestOutpostAlchemist = {
        Branches = {
            {--1
                Speech = "[$3831]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Black Forest Outpost Alchemist..." },
            },
            {--2
                ASK = {
                    Speech = "[$3763]",
                    Instruction = { "WHO", "SWAMP", "H" },
                    MenuText = { "Who are you?", "What do you know about the swamp?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3832]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Alchemist?", "Actually..."},
                },
                SWAMP = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "BlackForestHistory", {5}, true)
                    end,
                    Speech = "[$3833]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3834]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --treehouse_tinkerer
    BlackForestOutpostTinkerer = {
        Branches = {
            {--1
                Speech = "[$3997]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Black Forest Outpost Tinkerer..." },
            },
            {--2
                ASK = {
                    Speech = "[$3827]",
                    Instruction = { "WHO", "GODS", "H" },
                    MenuText = { "Who are you?", "What do you know about the Gods?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3998]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tinkerer?", "Actually..."},
                },
                GODS = {
                    Speech = "[$3999]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4000]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --treehouse_blacksmith
    BlackForestOutpostBlacksmith = {
        Branches = {
            {--1
                Speech = "[$3717]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Black Forest Outpost Blacksmith..." },
            },
            {--2
                ASK = {
                    Speech = "[$3718]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3719]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Blacksmith?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3720]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --treehouse_tailor
    BlackForestOutpostTailor = {
        Branches = {
            {--1
                Speech = "[$3826]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Black Forest Outpost Tailor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3827]",
                    Instruction = { "WHO", "DUNGEON", "H" },
                    MenuText = { "Who are you?", "What do you know about the dungeon in the forest?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3828]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tailor?", "Actually..."},
                },
                DUNGEON = {
                    Speech = "[$3829]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3830]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --treehouse_carpenter
    BlackForestOutpostCarpenter = {
        Branches = {
            {--1
                Speech = "[$4077]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Black Forest Outpost Carpenter..." },
            },
            {--2
                ASK = {
                    Speech = "[$3815]",
                    Instruction = { "WHO", "HISTORY", "H" },
                    MenuText = { "Who are you?", "What can you tell me about the other history of this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4078]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Carpenter?", "Actually..."},
                },
                HISTORY = {
                    Speech = "[$4079]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4080]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    -- Treacher Path

    --talking_harpy_translator
    HarpyTranslator = {
        Branches = {
            {--1
                Speech = "[$3897]",
                SpeechPriority = 3,
                Instruction = { "WHO", "WHAT", "X" },
                MenuText = { "Who are you?", "What is this place?", "Goodbye."},
            },
            {--2
                WHO = {
                    Speech = "[$3898]",
                    Instruction = { "TRIBE", "SHOWOBJ", "H" },
                    MenuText = { "The Vilewood Tribe?", "Can you translate this?", "Actually..."},
                },
                WHAT = {
                    Speech = "[$3899]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 1, nil },
                },
            },
            {--3
                TRIBE = {
                    Speech = "[$3901]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 1, nil },
                },
                SHOWOBJ = {
                    OnBegin = function(npcObj, playerObj, args)
                        Dialogue.RequestGameObject(npcObj, playerObj, true)
                    end,
                    Speech = "[$3900]",
                    Instruction = { "ELSE", },
                    MenuText = { "Nevermind.", },
                    GoTo = { 2 },
                },
            },
            {--4
                SHOWOBJSUCCESS = {
                    OnBegin = function(npcObj, playerObj, args)
                        Lore.EarnLoreIfNew(playerObj, "HallsOfContemptTranslated", Dialogue.ProcessNumberRange("1-5"))
                    end,
                    Speech = "[$3903]",
                    Instruction = { "H" },
                    MenuText = { "Thanks for the help.", },
                },
                SHOWOBJFAIL = {
                    Speech = "[$3902]",
                    Instruction = { "H" },
                    MenuText = { "Thanks for trying.", },
                },
            },
        }
    },

    -- END Black Forest

    -- Frozen Tundra: Asper Town

    --asper_blacksmith
    AsperBlacksmith = {
        Branches = {
            {--1
                Speech = "[$3972]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Asper Blacksmith..." },
            },
            {--2
                ASK = {
                    Speech = "[$3947]",
                    Instruction = { "WHO", "KNOW", "WHAT", "H" },
                    MenuText = { "Who are you?", "What do you know about this place?", "What is that statue behind you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3973]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Blacksmith?", "Actually..."},
                },
                KNOW = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "AsperHistory", {1}, true)
                    end,
                    Speech = "[$3974]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                WHAT = {
                    Speech = "[$3975]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3976]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --asper_scribe
    AsperScribe = {
        Branches = {
            {--1
                Speech = "[$3963]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Asper Scribe..." },
            },
            {--2
                ASK = {
                    Speech = "[$3964]",
                    Instruction = { "WHO", "KNOW", "H" },
                    MenuText = { "Who are you?", "What do you know about this place?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3965]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Scribe?", "Actually..."},
                },
                KNOW = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "AsperHistory", {2}, true)
                    end,
                    Speech = "[$3966]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3967]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --asper_bartender
    AsperInnkeeper = {
        Branches = {
            {--1
                Speech = "[$4085]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Asper Innkeeper..." },
            },
            {--2
                ASK = {
                    Speech = "[$4086]",
                    Instruction = { "WHO", "WHAT", "H" },
                    MenuText = { "Who are you?", "What happened here?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$4087]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Innkeeper?", "Actually..."},
                },
                WHAT = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "AsperHistory", {5}, true)
                    end,
                    Speech = "[$4088]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$4089]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --asper_general_store
    AsperGeneralStore = {
        Branches = {
            {--1
                Speech = "[$3749]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Asper General Store..." },
            },
            {--2
                ASK = {
                    Speech = "[$3750]",
                    Instruction = { "WHO", "KNOW", "H" },
                    MenuText = { "Who are you?", "What do you know about the Ancient City?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3751]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Trader?", "Actually..."},
                },
                KNOW = {
                    Speech = "[$3752]",
                    Instruction = { "GODDESS", "H" },
                    MenuText = { "The Shattered Goddess?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3754]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                GODDESS = {
                    Speech = "[$3753]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --asper_tailor
    AsperTailor = {
        Branches = {
            {--1
                Speech = "[$3693]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Asper Tailor..." },
            },
            {--2
                ASK = {
                    Speech = "[$3694]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3695]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tailor?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3696]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --asper_carpenter
    AsperCarpenter = {
        Branches = {
            {--1
                Speech = "[$3946]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Asper Carpenter..." },
            },
            {--2
                ASK = {
                    Speech = "[$3947]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3948]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Carpenter?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3949]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --asper_alchemist
    AsperAlchemist = {
        Branches = {
            {--1
                Speech = "[$3749]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Asper Alchemist..." },
            },
            {--2
                ASK = {
                    Speech = "[$3931]",
                    Instruction = { "WHO", "H" },
                    MenuText = { "Who are you?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3940]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Alchemist?", "Actually..."},
                },
            },
            {--4
                WHY = {
                    Speech = "[$3941]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --asper_tinkerer
    AsperTinkerer = {
        Branches = {
            {--1
                Speech = "[$3930]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for the Asper Tinkerer..." },
            },
            {--2
                ASK = {
                    Speech = "[$3931]",
                    Instruction = { "WHO", "KNOW", "H" },
                    MenuText = { "Who are you?", "What do you know about the Avalanche?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3932]",
                    Instruction = { "WHY", "H" },
                    MenuText = { "Why are you the Tinkerer?", "Actually..."},
                },
                KNOW = {
                    Speech = "[$3933]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHY = {
                    Speech = "[$3934]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --[[AsperChef = {
        Branches = {
            {--1
                Speech = "Sorry, let's talk another time.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },

    AsperBard = {
        Branches = {
            {--1
                Speech = "Sorry, let's talk another time.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },

    AsperHealer = {
        Branches = {
            {--1
                Speech = "Sorry, let's talk another time.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },

    AsperMage = {
        Branches = {
            {--1
                Speech = "Sorry, let's talk another time.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },

    AsperThief = {
        Branches = {
            {--1
                Speech = "Sorry, let's talk another time.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },

    AsperBanker = {
        Branches = {
            {--1
                Speech = "Sorry, let's talk another time.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },

    AsperMayor = {
        Branches = {
            {--1
                Speech = "Sorry, let's talk another time.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },

    AsperMissionDispatcher = {
        Branches = {
            {--1
                Speech = "Sorry, we have not scouted these lands enough to set up Missions, yet.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },]]

    -- END Asper Town

    -- Frozen Tundra: Valdheim Guard Outpost

    --[[ValdheimOutpostBlacksmith = {
        Branches = {
            {--1
                Speech = "Sorry, let's talk another time.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },

    ValdheimOutpostCarpenter = {
        Branches = {
            {--1
                Speech = "Sorry, let's talk another time.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },

    ValdheimOutpostStableMaster = {
        Branches = {
            {--1
                Speech = "Sorry, let's talk another time.",
                SpeechPriority = 3,
                Instruction = { "X" },
                MenuText = { "Okay..." },
            },
        }
    },]]

    -- END Valdheim Guard Outpost

    -- Dungeons Of Celador

    -- Corruption

    CorruptionJar1 = {
        Branches = {
            {--1
                Speech = "WHAT IS YOUR COMMAND? RECITATION?",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Recitation?" },
            },
            {--2
                ASK = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "HallsOfCorruption", {1}, true)
                    end,
                    Speech = string.upper("Recitation Start. Aria was once a massive collective of regions, home to all varieties of hero and horror, united as a single cluster glowing bright and eternal within the aether. One of Aria's regions was the land of Terminus, a forsaken, alien mass of horrors designed to ensnare the fragile sanities of mortals who dare to stand in places the gods themselves have trembled. Terrors crawled the desolate wasteland, unleashing biological mutations and horrors upon..."),
                    Instruction = { "X" },
                    MenuText = { "Stop Recitation." },
                },
            },
        }
    },
    CorruptionJar2 = {
        Branches = {
            {--1
                Speech = "WHAT IS YOUR COMMAND? RECITATION?",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Recitation?" },
            },
            {--2
                ASK = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "HallsOfCorruption", {2}, true)
                    end,
                    Speech = string.upper("Recitation Start. The Xor were the first lifeform of the cluster of Aria to evolve beyond their mortal existence, reaching a level of understanding that allowed them to exist in a realm that is beyond the typical plane of existence, and beyond the control of even the Creator Gods. They were mad with power, dedicating themselves to experimenting on - often through torture - the younger races that existed across Aria. The entrance to The Xor's world of horrors, Terminus, was an imposing..."),
                    Instruction = { "X" },
                    MenuText = { "Stop Recitation." },
                },
            },
        }
    },
    CorruptionJar3 = {
        Branches = {
            {--1
                Speech = "WHAT IS YOUR COMMAND? RECITATION?",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Recitation?" },
            },
            {--2
                ASK = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "HallsOfCorruption", {3}, true)
                    end,
                    Speech = string.upper("Recitation Start. The portal led not to a region of riches and life everlasting, nor did it lead to a greater understanding of existence. There was none of the promised path to ascension or godhood. Rather, wayward travelers found themselves within the belly of a massive, terrifying beast. Dreadful and strong, the beast had no face beyond mouth and teeth. Its size was so great that each temple across the world of Aria could find a home within the beast with room to spare. There were no grand temples..."),
                    Instruction = { "X" },
                    MenuText = { "Stop Recitation." },
                },
            },
        }
    },
    CorruptionJar4 = {
        Branches = {
            {--1
                Speech = "WHAT IS YOUR COMMAND? RECITATION?",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Recitation?" },
            },
            {--2
                ASK = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "HallsOfCorruption", {4}, true)
                    end,
                    Speech = string.upper("Recitation Start. The Xor referred to their temple of horrors as the Halls of Corruption, as they considered the species of human to be no more than a corrupted, twisted caricature of the gods. Deep inside a great pit of darkness, humans are beaten endlessly by great demons. Those who manage to escape the clutches of the creatures that pepper the halls find themselves entrapped in a flaming pit guarded by demons. The worst of the punishments within the Halls of Corruption were..."),
                    Instruction = { "X" },
                    MenuText = { "Stop Recitation." },
                },
            },
        }
    },
    CorruptionJar5 = {
        Branches = {
            {--1
                Speech = "WHAT IS YOUR COMMAND? RECITATION?",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Recitation?" },
            },
            {--2
                ASK = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "HallsOfCorruption", {5}, true)
                    end,

                    Speech = string.upper("Recitation Start. The Shattering of Aria destroyed access to the Halls of Corruption from the region of Terminus, leaving only access to the twisted temple from Celador. The riches which had once been promised as an act of deception now lie within its walls, left behind by the fallen. Bars and spikes still adorn the walls, while chains and cages pepper the twisting halls and sprawling chambers, serving as both reminder of what once occurred inside and warning of what the creatures who call the space..."),
                    Instruction = { "X" },
                    MenuText = { "Stop Recitation." },
                },
            },
        }
    },

    -- Monolith

    --monolith_keeper
    TheKeeperLevel1 = {
        Branches = {
            {--1
                Speech = "[$3922]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Question for you..." },
            },
            {--2
                ASK = {
                    Speech = "[$3923]",
                    Instruction = { "WHO", "PLACE", "HOW", "H" },
                    MenuText = { "Who are you?", "What is this place?", "How can I assist?", "Actually..."},
                },
            },
            {--3
                WHO = {
                    Speech = "[$3924]",
                    Instruction = { "WHAT", "H" },
                    MenuText = { "What are you?", "Actually..."},
                },
                PLACE = {
                    Speech = "[$3925]",
                    Instruction = { "MOTHER", "GATEWAY", "H" },
                    MenuText = { "Your mother?", "The Gateway?", "Actually..."},
                },
                HOW = {
                    Speech = "[$3926]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
            {--4
                WHAT = {
                    Speech = "[$3928]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                MOTHER = {
                    Speech = "[$3927]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                GATEWAY = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "Monolith", {3}, true)
                    end,
                    Speech = "[$3929]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    --monolith_keeper_level_two
    TheKeeperLevel2 = {
        Branches = {
            {--1
                Speech = "[$3886]",
                SpeechPriority = 3,
                Instruction = { "ASK" },
                MenuText = { "Another Question for you, Keeper..." },
            },
            {--2
                ASK = {
                    Speech = "[$3887]",
                    Instruction = { "JIANNA", "FAIL", "H" },
                    MenuText = { "Why did Jianna want to Shatter Celador?", "Why did the Shattering fail?", "Actually..."},
                },
            },
            {--3
                JIANNA = {
                    Speech = "[$3888]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
                FAIL = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "Monolith", {5}, true)
                    end,
                    Speech = "[$3889]",
                    Instruction = { "ASK", "H" },
                    MenuText = { "I have another question.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },

    -- END Dungeons



    -- EXAMPLES

    ExampleTree = {
        Branches = {
            {--1
                Speech = "[$3916]",
                SpeechPriority = 3,
                Instruction = { "A2" },
                MenuText = { "" },
            },
            {--2
                A2 = {
                    Speech = "[$3916]",
                    Instruction = { "A3", "B3", "C3", "H" },
                    MenuText = { "", "", "", ""},
                },
            },
            {--3
                A3 = {
                    Speech = "[$3916]",
                    Instruction = { "A4", "B4", "H" },
                    MenuText = { "", "", ""},
                },
                B3 = {
                    Speech = "[$3916]",
                    Instruction = { "C4", "H" },
                    MenuText = { "", ""},
                },
                C3 = {
                    Speech = "[$3916]",
                    Instruction = { "A2", "H" },
                    MenuText = { "", ""},
                    GoTo = { 2, nil },
                },
            },
            {--4
                A4 = {
                    OnBegin = function(npcObj, playerObj)

                    end,
                    Speech = "[$3916]",
                    Instruction = { "A2", "H" },
                    MenuText = { "", ""},
                    GoTo = { 2, nil },
                },
                B4 = {
                    Speech = "[$3916]",
                    Instruction = { "A2", "H" },
                    MenuText = { "", ""},
                    GoTo = { 2, nil },
                },
                C4 = {
                    Speech = "[$3916]",
                    Instruction = { "A2", "H" },
                    MenuText = { "", ""},
                    GoTo = { 2, nil },
                },
            },
        }
    },


    TestEldeirTomebooks = {
        Branches = {
            {--1
                Speech = "[$3908]",
                SpeechPriority = 150,
                Instruction = { "TOME" },
                MenuText = { "Help me with my Tomebook Collection." },
            },
            {--2
                TOME = {
                    Speech = "[$3909]",
                    Instruction = { "ONE", "TWO", "THREE", "FOUR", "FIVE" },
                    MenuText = { "The first entry.", "Entry #2.", "Number three, please.", "IIII.", "Give me the 5th"},
                },
            },
            {--3
                ONE = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "EldeirHistory", {1}, true)
                    end,
                    Speech = "[$3910]",
                    Instruction = { "TOME", "H" },
                    MenuText = { "I want another Entry.", "Actually..."},
                    GoTo = { 2, nil },
                },
                TWO = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "EldeirHistory", {2}, true)
                    end,
                    Speech = "[$3910]",
                    Instruction = { "TOME", "H" },
                    MenuText = { "I want another Entry.", "Actually..."},
                    GoTo = { 2, nil },
                },
                THREE = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "EldeirHistory", {3}, true)
                    end,
                    Speech = "[$3910]",
                    Instruction = { "TOME", "H" },
                    MenuText = { "I want another Entry.", "Actually..."},
                    GoTo = { 2, nil },
                },
                FOUR = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "EldeirHistory", {4}, true)
                    end,
                    Speech = "[$3910]",
                    Instruction = { "TOME", "H" },
                    MenuText = { "I want another Entry.", "Actually..."},
                    GoTo = { 2, nil },
                },
                FIVE = {
                    OnBegin = function(npcObj, playerObj)
                        Lore.EarnLoreIfNew(playerObj, "EldeirHistory", {5}, true)
                    end,
                    Speech = "[$3910]",
                    Instruction = { "TOME", "H" },
                    MenuText = { "I want another Entry.", "Actually..."},
                    GoTo = { 2, nil },
                },
            },
        }
    },
}