-- init data struct
EssenceData = {}

-- These are the prefixes for each essense level ( 1 - 3 )
-- The stats for each of these is found in the EffectRanges for each essence listed below.
EssenceData.Levels = { "Damaged", "", "Pristine" }
EssenceData.LevelsTemplateSuffix = { "_lesser", "", "_greater" }
EssenceData.LevelsTooltip = { "marginal ", "", "substantial " }
EssenceData.MaximumEnchantsPerItem = 3
EssenceData.MaximumEssencePouchContents = 3 -- How many total essences can be placed in the enchanting interface?
EssenceData.MaximumUniqueEssences = 3 -- How many unique essences can be used in the enchanting interface?
EssenceData.BaseCopperCost = 300 -- 10000 = 1gp (manipulated with GoldCostMultiplier{} for each essence)

EssenceData.ShinyTrinket = {
    Name = "Shiny Trinket",
    Prefix = "Spell Eating",
    Suffix = "of Spell Eating",
    EssenceType = "Primary",
    EssenceRestrictions = {"shield"},
    Effect = "mobilemod",
    EffectName = "Chance to Spell Block",
    EffectMode = "percent",
    MobileMod = "BlockSpellPlus",
    MobileEffect = "BlockSpell",
    PreventMagicDamage = true,
    SpellTriggered = true,
    MobileEffectDuration = 20,
    MobieEffectCanStack = false,
    EffectRanges = { -- chance to occur (%)
        {1,3},
        {3,5},
        {5,10}
    },
    DropEffectRanges = 
    {
        {1,3},
        {1,4},
        {1,5},
        {1,9},
        {1,10}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_shinytrinket"
}

EssenceData.ExpandingTrinket = {
    Name = "Expanding Trinket",
    Prefix = "Broad",
    Suffix = "of Missile Protection",
    EssenceType = "Primary",
    EssenceRestrictions = {"shield"},
    Effect = "mobilemod",
    EffectName = "Ranged Resistance",
    EffectMode = "percent",
    MobileMod = "BowDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,8},
        {8,15},
        {15,30}
    },
    DropEffectRanges = 
    {
        {1,8},
        {1,12},
        {1,15},
        {1,25},
        {1,30}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_expandingtrinket"
}

EssenceData.RuneofSpeed = {
    Name = "Rune of Speed",
    Prefix = "Singing",
    Suffix = "of Speed",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "mobilemod", -- Needs to be mobile mod
    EffectName = "Increased Attack Speed",
    EffectMode = "percent",
    MobileMod = "AttackSpeedTimes",
    EffectRanges = { -- percent to increase attack speed
        {2,8},
        {8,15},
        {15,30}
    },
    DropEffectRanges = 
    {
        {1,8},
        {1,12},
        {1,15},
        {1,25},
        {1,30}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "essence_speed"
}

EssenceData.RuneofMending = {
    Name = "Rune of Mending",
    Suffix = "of the Physician",
    Prefix = "Physician's",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Increased Healing Received",
    EffectMode = "plus",
    MobileMod = "HealingReceivedPlus",
    EffectRanges = { -- amount to increase HEALING recieved
        {1,2},
        {2,4},
        {4,8}
    },
    DropEffectRanges = 
    {
        {1,2},
        {1,3},
        {1,5},
        {1,7},
        {1,8}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_mending"
}

EssenceData.RuneofVanquishing = {
    Name = "Rune of Vanquishing",
    Prefix = "Vanquishing",
    Suffix = "of Vanquishing",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "mobilemod",
    EffectName = "Increased Attack Damage",
    EffectMode = "percent",
    MobileMod = "AttackBonus",
    EffectRanges = { -- amount to increase HEALING recieved
        {3,12},
        {12,25},
        {25,50}
    },
    DropEffectRanges = 
    {
        {1,25},
        {1,35},
        {1,50},
        {1,75},
        {1,100}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 50,
    ChangeToPercent = true,
    ClientId = 1108,
    Template = "essence_vanquishing"
}

EssenceData.RuneOfIshi = {
    Name = "Rune of Ishi",
    Prefix = "Ishi",
    Suffix = "of Ishi",
    EssenceType = "Primary",
    EssenceRestrictions = {"bow"},
    Effect = "mobilemod",
    EffectName = "Increased Range Attack",
    EffectMode = "percent",
    MobileMod = "AttackBonus",
    EffectRanges = { -- amount to increase HEALING recieved
        {3,12},
        {12,25},
        {25,50}
    },
    DropEffectRanges = 
    {
        {1,12},
        {1,18},
        {1,25},
        {1,37},
        {1,50}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 50,
    ChangeToPercent = true,
    ClientId = 1108,
    Template = "essence_ishi"
}

EssenceData.RuneOfRoving = {
    Name = "Rune of Roving",
    Prefix = "Roving",
    Suffix = "of Roving",
    EssenceType = "Primary",
    EssenceRestrictions = {"bow"},
    Effect = "mobilemod",
    EffectName = "Increased Hit Chance",
    EffectMode = "percent",
    MobileMod = "HitChancePlus",
    EffectRanges = { -- percent to increase attack speed
        {1,7},
        {7,12},
        {12,25}
    },
    DropEffectRanges = 
    {
        {1,7},
        {1,10},
        {1,12},
        {1,19},
        {1,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "essence_roving"
}

EssenceData.RuneOfTaming = {
    Name = "Rune of Taming",
    Prefix = "Tamer's",
    Suffix = "of Taming",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "mobilemod",
    EffectName = "Increased Tame Chance",
    EffectMode = "percent",
    MobileMod = "TameChanceTimes",
    EffectRanges = { -- percent to increase attack speed
        {1,7},
        {7,12},
        {5,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.RuneOfLockpicking = {
    Name = "Rune of Lockpicking",
    Prefix = "Lockpicker's",
    Suffix = "of Lockpicking",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "mobilemod",
    EffectName = "Increased Lockpicking Chance",
    EffectMode = "percent",
    MobileMod = "LockpickChanceTimes",
    EffectRanges = { -- percent to increase attack speed
        {1,7},
        {7,12},
        {5,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.RuneofPatience = {
    Name = "Rune of Patience",
    Prefix = "Patient",
    Suffix = "of Patience",
    EssenceType = "Primary",
    EssenceRestrictions = {"bow"},
    Effect = "combatmod",
    EffectName = "Chance to gain Centershot",
    EffectMode = "percent",
    MobileMod = "CentershotPlus",
    MobileEffect = "Centershot",
    SwingProc = true,
    MobileEffectDuration = 8,
    MobieEffectCanStack = true,
    EffectDamage = 100, -- %stamina reduction
    EffectRanges = { -- chance to occur (%)
        {1,2},
        {2,4},
        {4,8}
    },
    DropEffectRanges = 
    {
        {1,2},
        {1,3},
        {1,4},
        {1,6},
        {1,8}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "essence_patience"
}

EssenceData.RuneofPower = {
    Name = "Rune of Power",
    Prefix = "Reckoning",
    Suffix = "of Power",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "mobilemod",
    EffectName = "Increased Spell Power",
    EffectMode = "plus",
    MobileMod = "PowerPlus",
    EffectRanges = { -- amount to increase POWER
        {1,2},
        {2,3},
        {3,5}
    },
    DropEffectRanges = 
    {
        {1,1},
        {1,2},
        {1,3},
        {1,4},
        {1,5}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_power"
}

EssenceData.RuneofFire = {
    Name = "Rune of Fire",
    Prefix = "Burning",
    Suffix = "of Flames",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "combatmod",
    EffectName = "Chance to Burn Enemy on Hit",
    EffectMode = "percent",
    MobileMod = "WeaponProcBurnPlus",
    MobileEffect = "WeaponProc",
    HitProc = true,
    EffectDamage = 30, -- TrueDamage
    EffectProc = "FireballExplosionEffect",
    EffectSound = "event:/magic/fire/magic_fire_fireball_impact",
    EffectRanges = { -- chance to occur (%)
        {3,5},
        {5,12},
        {12,25}
    },
    DropEffectRanges = 
    {
        {1,5},
        {1,8},
        {1,12},
        {1,19},
        {1,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_fire"
}

EssenceData.RuneOfIce = {
    Name = "Rune of Ice",
    Prefix = "Freezing",
    Suffix = "of Frost",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "combatmod",
    HitProc = true,
    EffectName = "Chance to Chill Enemy on Hit",
    EffectMode = "percent",
    MobileEffect = "WeaponProc",
    MobileMod = "WeaponProcFreezePlus",
    EffectDamage = 30, -- TrueDamage
    EffectProc = "IceExplosionEffect",
    EffectSound = "event:/magic/air/magic_air_energy_bolt_impact",
    EffectRanges = { -- chance to occur (%)
        {3,5},
        {5,12},
        {12,25}
    },
    DropEffectRanges = 
    {
        {1,5},
        {1,8},
        {1,12},
        {1,18},
        {1,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_ice"
}

EssenceData.RuneOfLightning = {
    Name = "Rune of Lightning",
    Prefix = "Shocking",
    Suffix = "of Static",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "combatmod",
    HitProc = true,
    EffectName = "Chance to Shock Enemy on Hit",
    EffectMode = "percent",
    MobileEffect = "WeaponProc",
    MobileMod = "WeaponProcShockPlus",
    EffectDamage = 30, -- TrueDamage
    EffectProc = "LightningCloudEffect",
    EffectSound = "event:/magic/air/magic_air_lightning_impact",
    EffectRanges = { -- chance to occur (%)
        {3,5},
        {5,7},
        {12,25}
    },
    DropEffectRanges = 
    {
        {1,5},
        {1,6},
        {1,7},
        {1,20},
        {1,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_lightning"
}

EssenceData.RuneOfSunder = {
    Name = "Rune of Sundering",
    Prefix = "Sundering",
    Suffix = "of Sundering",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "combatmod",
    HitProc = true,
    EffectName = "Chance to Sunder Enemy on Hit",
    EffectMode = "percent",
    MobileEffect = "Sunder",
    MobileMod = "WeaponProcSunderPlus",
    EffectRanges = { -- chance to occur (%)
        {3,5},
        {5,7},
        {5,15}
    },
    DropEffectRanges = 
    {
        {1,5},
        {1,6},
        {1,7},
        {1,11},
        {1,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.RuneOfSilence = {
    Name = "Rune of Silencing",
    Prefix = "Silencing",
    Suffix = "of Silencing",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "combatmod",
    HitProc = true,
    EffectName = "Chance to Silence Enemy on Hit",
    EffectMode = "percent",
    MobileEffect = "Silence",
    MobileMod = "WeaponProcSilencePlus",
    EffectRanges = { -- chance to occur (%)
        {3,5},
        {5,7},
        {5,15}
    },

    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.RuneofWater = {
    Name = "Rune of Water",
    Prefix = "Enlightened",
    Suffix = "of Awareness",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "combatmod",
    CastProc = true,
    EffectName = "Chance to gain Clearcasting",
    EffectMode = "percent",
    MobileEffect = "Clearcast",
    MobileMod = "ClearcastPlus",
    MobieEffectCanStack = true,
    MobileEffectDuration = 8,
    EffectDamage = 100, -- %mana reduced on next cast
    EffectRanges = { -- chance to occur (%)
        {1,2},
        {2,4},
        {4,8}
    },
    DropEffectRanges = 
    {
        {1,2},
        {1,3},
        {1,4},
        {1,6},
        {1,8}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "essence_water"
}


EssenceData.EffectofStarfall = {
    Name = "Rune of Starfall",
    Prefix = "Starcalling",
    Suffix = "of Starcalling",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "combatmod",
    HitProc = true,
    EffectName = "Chance to Trigger Starfall on Hit",
    EffectMode = "percent",
    MobileEffect = "WeaponProc",
    MobileMod = "WeaponProcStarfallPlus",
    EffectDamage = 60, -- TrueDamage
    EffectType = "location",
    EffectProc = "MeteorEffect",
    EffectSound = "event:/magic/earth/magic_earth_cast_meteor",
    AOERange = 5, 
    EffectProcDelay = "ImpactWaveEffect",
    ProcDelay = TimeSpan.FromSeconds(1.5),
    EffectRanges = { -- chance to occur (%)
        {3,5},
        {5,7},
        {5,15}
    },

    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.EffectofMagicAmplification = {
    Name = "Rune of Magic Amplification",
    Prefix = "Magic Amplification",
    Suffix = "of Magic Amplification",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "combatmod",
    CastProc = true,
    EffectName = "Chance to Apply Magic Amplification Debuff",
    EffectMode = "percent",
    MobileEffect = "MagicAmplification",
    MobileMod = "MagicAmplificationPlus",
    MobieEffectCanStack = false,
    MobileEffectDuration = 8,
    EffectDamage = 100, -- %mana reduced on next cast
    EffectRanges = { -- chance to occur (%)
        {1,2},
        {2,4},
        {4,8}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.EffectOfHarvesting = {
    Name = "Rune of Harvesting",
    Prefix = "Plentiful",
    Suffix = "of Plenty",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "mobilemod",
    EffectName = "Increased Harvest Speed",
    EffectMode = "percent",
    MobileMod = "HarvestDelayReductionTimes",
    EffectRanges = { -- percent to increase attack speed
        {1,7},
        {7,12},
        {5,15}
    },

    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.EffectManaRegenPlus = {
    Name = "Rune of Mana Regeneration",
    Prefix = "Layfinding",
    Suffix = "of Layfinding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Increased Mana Regen",
    EffectMode = "plus",
    MobileMod = "ManaRegenPlus",
    TooltipRanges = 
    {
        { Max = 0.1, Tooltip = "Mana Regen I" },
        { Max = 0.15, Tooltip = "Mana Regen II" },
        { Max = 0.20, Tooltip = "Mana Regen III" },
        { Max = 0.25, Tooltip = "Mana Regen IV" },
        { Max = 0.3, Tooltip = "Mana Regen V" },
    },
    EffectRanges = { -- percent to increase attack speed
        {0.05,0.1},
        {0.15,0.2},
        {0.25,0.3}
    },
    DropEffectRanges = 
    {
        {0.05,0.1},
        {0.05,0.1},
        {0.15,0.2},
        {0.15,0.2},
        {0.25,0.3}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.EffectHealthRegenPlus = {
    Name = "Rune of Health Regeneration",
    Prefix = "Regrown",
    Suffix = "of Regrowth",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Increased Health Regen",
    EffectMode = "plus",
    MobileMod = "HealthRegenPlus",
    TooltipRanges = 
    {
        { Max = 0.5, Tooltip = "Health Regen I" },
        { Max = 0.75, Tooltip = "Health Regen II" },
        { Max = 1, Tooltip = "Health Regen III" },
        { Max = 1.50, Tooltip = "Health Regen IV" },
        { Max = 2, Tooltip = "Health Regen V" },
    },
    EffectRanges = { -- percent to increase attack speed
        {0.5,1},
        {0.5,1},
        {1,2}
    },
    DropEffectRanges = 
    {
        {0.25,0.5},
        {0.25,0.5},
        {0.5,1},
        {0.5,1},
        {1,2}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.EffectStaminaRegenPlus = {
    Name = "Rune of Stamina Regeneration",
    Prefix = "Enduring",
    Suffix = "of Endurance",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Increased Stamina Regen",
    EffectMode = "plus",
    MobileMod = "StaminaRegenPlus",
    TooltipRanges = {
        { Max = 0.1, Tooltip = "Stamina Regen I" },
        { Max = 0.15, Tooltip = "Stamina Regen II" },
        { Max = 0.20, Tooltip = "Stamina Regen III" },
        { Max = 0.25, Tooltip = "Stamina Regen IV" },
        { Max = 0.3, Tooltip = "Stamina Regen V" },
    },
    EffectRanges = { -- percent to increase attack speed
        {0.05,0.1},
        {0.15,0.2},
        {0.25,0.3}
    },
    DropEffectRanges = 
    {
        {0.05,0.1},
        {0.05,0.1},
        {0.15,0.2},
        {0.15,0.2},
        {0.25,0.3}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.RuneofAccuracy = {
    Name = "Rune of Accuracy",
    Prefix = "Accurate",
    Suffix = "of Accuracy",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "mobilemod",
    EffectName = "Increased Hit Chance",
    EffectMode = "percent",
    MobileMod = "HitChancePlus",
    EffectRanges = { -- percent to increase attack speed
        {1,7},
        {7,12},
        {13,25}
    },
    DropEffectRanges = 
    {
        {1,7},
        {1,9},
        {1,12},
        {1,19},
        {1,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "essence_accuracy"
}

EssenceData.RuneofEvasion = {
    Name = "Rune of Blocking",
    Prefix = "Agile",
    Suffix = "of Evasion",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "shield"},
    Effect = "mobilemod",
    EffectName = "Increased Damage Reduction Chance",
    EffectMode = "percent",
    MobileMod = "DefenseChancePlus",
    EffectRanges = { -- percent to increase attack speed
        {1,2},
        {2,4},
        {4,8}
    },
    DropEffectRanges = 
    {
        {1,2},
        {1,3},
        {1,4},
        {1,6},
        {1,8}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "essence_evasion"
}

EssenceData.LizardmanScales = {
    Hidden = true,
    Name = "Lizard Scales",
    Prefix = "Lizard Warding",
    Suffix = "of Lizard Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Damage from Reptiles",
    EffectMode = "percent",
    MobileMod = "ReptileDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,3},
        {3,4}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_lizermanscales"
}

EssenceData.CharredBones = {
    Hidden = true,
    Name = "Charred Bones",
    Prefix = "Undead Warding",
    Suffix = "of Undead Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Damage from Undead",
    EffectMode = "percent",
    MobileMod = "UndeadDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,3},
        {3,4}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_charredbones"
}

EssenceData.DragonScales = {
    Hidden = true,
    Name = "Dragon Scales",
    Prefix = "Dragon Warding",
    Suffix = "of Dragon Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Damage from Dragons",
    EffectMode = "percent",
    MobileMod = "DragonDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,3},
        {3,4}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_dragonscales"
}

EssenceData.SeveredHand = {
    Hidden = true,
    Name = "Severed Hand",
    Prefix = "Terran Warding",
    Suffix = "of Terran Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Damage from Humanoids",
    EffectMode = "percent",
    MobileMod = "HumanoidDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,3},
        {3,4}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_severedhand"
}

EssenceData.OrkScalp = {
    Hidden = true,
    Name = "Ork Scalp",
    Prefix = "Ork Warding",
    Suffix = "of Ork Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Damage from Orks",
    EffectMode = "percent",
    MobileMod = "OrkDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,3},
        {3,4}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_orkscalp"
}

EssenceData.SpiderLegs = {
    Hidden = true,
    Name = "Spider Legs",
    Prefix = "Arachnid Warding",
    Suffix = "of Arachnid Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Damage from Spiders",
    EffectMode = "percent",
    MobileMod = "ArachnidDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,3},
        {3,4}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_spiderlegs"
}

EssenceData.GrizzlyBearTooth = {
    Hidden = true,
    Name = "Grizzly Bear Tooth",
    Prefix = "Beast Warding",
    Suffix = "of Beast Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Damage from Animals",
    EffectMode = "percent",
    MobileMod = "AnimalDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,3},
        {3,4}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_grizzlybeartooth"
}

EssenceData.GargoyleEyeball = {
    Hidden = true,
    Name = "Gargoyle Eyeball",
    Prefix = "Demon Warding",
    Suffix = "of Demon Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Damage from Demons",
    EffectMode = "percent",
    MobileMod = "DemonDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,3},
        {3,4}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_gargoyleeyeball"
}

EssenceData.OgreEars = {
    Hidden = true,
    Name = "Ogre Ears",
    Prefix = "Giant Warding",
    Suffix = "of Giant Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Damage from Giants",
    EffectMode = "percent",
    MobileMod = "GiantDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,3},
        {3,4}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_ogreears"
}

EssenceData.EntRoots = {
    Hidden = true,
    Name = "Ent Roots",
    Prefix = "Ent Warding",
    Suffix = "of Ent Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Damage from Ents",
    EffectMode = "percent",
    MobileMod = "EntDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,3},
        {3,4}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_entroots"
}

EssenceData.SturdyThreads = {
    Name = "Sturdy Threads",
    Prefix = "Sturdy",
    Suffix = "of Brawniness",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Bashing Resistance",
    EffectMode = "percent",
    MobileMod = "BashingDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,4},
        {4,8}
    },
    DropEffectRanges = 
    {
        {1,2},
        {1,3},
        {1,4},
        {1,6},
        {1,8}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_sturdythreads"
}

EssenceData.ResilientThreads = {
    Name = "Resilient Threads",
    Prefix = "Resilient",
    Suffix = "of Hardiness",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Slashing Resistance",
    EffectMode = "percent",
    MobileMod = "SlashingDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,4},
        {4,8}
    },
    DropEffectRanges = 
    {
        {1,2},
        {1,3},
        {1,4},
        {1,6},
        {1,8}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_resilientthreads"
}

EssenceData.UnyieldingThreads = {
    Name = "Unyielding Threads",
    Prefix = "Unyielding",
    Suffix = "of Unyielding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Piercing Resistance",
    EffectMode = "percent",
    MobileMod = "PiercingDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,4},
        {4,8}
    },
    DropEffectRanges = 
    {
        {1,2},
        {1,3},
        {1,4},
        {1,6},
        {1,8}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_unyieldingthreads"
}

EssenceData.TheurgyThreads = {
    Name = "Theurgy Threads",
    Prefix = "Theurgy",
    Suffix = "of Spell Warding",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Magic Resistance",
    EffectMode = "percent",
    MobileMod = "MagicDamageReductionTimes",
    EffectRanges = { -- percent to reduce damage
        {1,2},
        {2,4},
        {4,8}
    },
    DropEffectRanges = 
    {
        {1,2},
        {1,3},
        {1,4},
        {1,6},
        {1,8}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_theurgythreads"
}

EssenceData.FlowingThreads = {
    Name = "Flowing Threads",
    Prefix = "Lucid",
    Suffix = "of Mindfulness",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Reduced Mana Cost",
    EffectMode = "percent",
    MobileMod = "ManaReductionTimes",
    EffectRanges = { -- percent to reduce mana per cast
        {1,3},
        {3,5},
        {5,10}
    },
    DropEffectRanges = 
    {
        {1,3},
        {1,4},
        {1,5},
        {1,7},
        {1,10}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "essence_flowingthreads"
}

EssenceData.RuneOfHealth = {
    Name = "Rune of Health",
    Prefix = "Hearty",
    Suffix = "of Heartiness",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Increased Health",
    EffectMode = "plus",
    MobileMod = "MaxHealthPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,10},
        {5,15},
        {10,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.RuneOfCarrying = {
    Name = "Rune of Carrying",
    Prefix = "Stout",
    Suffix = "of Stoutness",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Increased Weight Capacity",
    EffectMode = "plus",
    MobileMod = "MaxWeightPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,10},
        {5,15},
        {10,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 25,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

------------------------------------------------------------------------------------
-- MONOLITH
------------------------------------------------------------------------------------

EssenceData.SorceryChanceToNotConsumeWisp = {
    Name = "Rune of Wisp Preservation",
    Prefix = "Wisp Preserving",
    Suffix = "of Wisp Preserving",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "weapon"},
    Effect = "mobilemod",
    EffectName = "Chance to not consume Wisps",
    EffectMode = "percent",
    MobileMod = "SorceryChanceToNotConsumeWisp",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.SorceryChanceToNotTriggerCooldown = {
    Name = "Rune of Sorcery",
    Prefix = "Echo",
    Suffix = "of Lasting Echos",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "weapon"},
    Effect = "mobilemod",
    EffectName = "Chance to not trigger sorcery cooldown",
    EffectMode = "percent",
    MobileMod = "SorceryChanceToNotTriggerCooldown",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.SorceryChanceToSummonMorePowerful = {
    Name = "Rune of Enhanced Summoning",
    Prefix = "Summoning",
    Suffix = "of Summoning",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "weapon"},
    Effect = "mobilemod",
    EffectName = "Chance for enhanced summons",
    EffectMode = "percent",
    MobileMod = "SorceryChanceToSummonMorePowerful",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.SorceryBeamCooldownReduction = {
    Name = "Rune of Echo Beam",
    Prefix = "Summoning",
    Suffix = "of Summoning",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "weapon"},
    Effect = "mobilemod",
    EffectName = "Reduced cooldown for Echobeam",
    EffectMode = "percent",
    MobileMod = "SorceryBeamCooldownReduction",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToCastEchofallOnMeleeStruck = {
    Name = "Rune of Echo Shielding",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to trigger Echofall when struck",
    EffectMode = "percent",
    MobileMod = "ChanceToCastEchofallOnMeleeStruck",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToGainEchoHealOnMeleeStruck = {
    Name = "Rune of Life Shielding",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to heal when struck",
    EffectMode = "percent",
    MobileMod = "ChanceToGainEchoHealOnMeleeStruck",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToGainWispOnMeleeStruck = {
    Name = "Rune of Wisp Defense",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to gain a wisp when struck",
    EffectMode = "percent",
    MobileMod = "ChanceToGainWispOnMeleeStruck",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToSummonConstructOnDamageSpellHit = {
    Name = "Rune of Construct Summoning",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to summon construct on offensive spell",
    EffectMode = "percent",
    MobileMod = "ChanceToSummonConstructOnDamageSpellHit",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToHealCasterOnDamageSpellHit = {
    Name = "Rune of Vampiro Magic",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to heal on offensive spell",
    EffectMode = "percent",
    MobileMod = "ChanceToHealCasterOnDamageSpellHit",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToIgniteTargetOnDamageSpellHit = {
    Name = "Rune of Inferno Magic",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to ignite on offensive spell",
    EffectMode = "percent",
    MobileMod = "ChanceToIgniteTargetOnDamageSpellHit",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToSummonWispChaserOnMeleeHit = {
    Name = "Rune of the Wisp Chaser",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to summon wisp chaser on attack",
    EffectMode = "percent",
    MobileMod = "ChanceToSummonWispChaserOnMeleeHit",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToEchoBurnOnMeleeHit = {
    Name = "Rune of Echo Strike",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to afflict fire damage on attack",
    EffectMode = "percent",
    MobileMod = "ChanceToEchoBurnOnMeleeHit",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToGainWispOnMeleeHit = {
    Name = "Rune of Wisp Generation",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to gain wisp on attack",
    EffectMode = "percent",
    MobileMod = "ChanceToGainWispOnMeleeHit",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToGainEchoHealOnMeleeHit = {
    Name = "Rune of Vampiro Strike",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to regenerate health on melee hit",
    EffectMode = "percent",
    MobileMod = "ChanceToGainEchoHealOnMeleeHit",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

EssenceData.ChanceToChillOnMeleeHit = {
    Name = "Rune of Chilling Strike",
    Prefix = "",
    Suffix = "",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Chance to afflict ice damage on attack",
    EffectMode = "percent",
    MobileMod = "ChanceToChillOnMeleeHit",
    EffectRanges = { -- percent to reduce mana per cast
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfStrength = {
    Name = "Rune of Strength",
    Prefix = "Strong",
    Suffix = "of Strength",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Bonus Strength",
    EffectMode = "plus",
    MobileMod = "StrengthPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,1},
        {2,2},
        {3,3}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "essence_strength"
}

--SCAN ADDED
EssenceData.RuneOfConstitution = {
    Name = "Rune of Health",
    Prefix = "Healthy",
    Suffix = "of Constitution",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "shield"},
    Effect = "mobilemod",
    EffectName = "Bonus Health",
    EffectMode = "plus",
    MobileMod = "MaxHealthPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,10},
        {5,15},
        {10,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 100,
    ClientId = 1108,
    Template = "essence_constitution"
}

--SCAN ADDED
EssenceData.RuneOfAgility = {
    Name = "Rune of Agility",
    Prefix = "Agile",
    Suffix = "of Agility",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Bonus Agility",
    EffectMode = "plus",
    MobileMod = "AgilityPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,1},
        {2,2},
        {3,3}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfIntelligence = {
    Name = "Rune of Intelligence",
    Prefix = "Intelligent",
    Suffix = "of Intelligence",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor"},
    Effect = "mobilemod",
    EffectName = "Bonus Intelligence",
    EffectMode = "plus",
    MobileMod = "IntelligencePlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,1},
        {2,2},
        {3,3}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfWisdom = {
    Name = "Rune of Wisdom",
    Prefix = "Wise",
    Suffix = "of Wisdom",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "shield"},
    Effect = "mobilemod",
    EffectName = "Bonus Wisdom",
    EffectMode = "plus",
    MobileMod = "WisdomPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,1},
        {2,2},
        {3,3}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfWill = {
    Name = "Rune of Will",
    Prefix = "Willful",
    Suffix = "of Will",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "shield"},
    Effect = "mobilemod",
    EffectName = "Bonus Will",
    EffectMode = "plus",
    MobileMod = "WillPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,1},
        {2,2},
        {3,3}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfPoison = {
    Name = "Rune of Poisoning",
    Prefix = "Toxic",
    Suffix = "of Poisoning",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "combatmod",
    HitProc = true,
    EffectName = "Chance to Poison Enemy on Hit",
    EffectMode = "percent",
    MobileEffect = "Poison",
    MobileMod = "WeaponProcPoisonPlus",
    EffectRanges = { -- chance to occur (%)
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfBleed = {
    Name = "Rune of Bleeding",
    Prefix = "Bloody",
    Suffix = "of Bleeding",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "combatmod",
    HitProc = true,
    EffectName = "Chance to Bleed Enemy on Hit",
    EffectMode = "percent",
    MobileEffect = "Bleed",
    MobileMod = "WeaponProcBleedPlus",
    EffectRanges = { -- chance to occur (%)
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfStun = {
    Name = "Rune of Stunning",
    Prefix = "Stunning",
    Suffix = "of Stunning",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "combatmod",
    HitProc = true,
    EffectName = "Chance to Stun Enemy on Hit",
    EffectMode = "percent",
    MobileEffect = "Stun",
    MobileMod = "WeaponProcStunPlus",
    EffectRanges = { -- chance to occur (%)
        {1,5},
        {5,10},
        {10,15}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 30,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfRunSpeed = {
    Name = "Rune of Speed",
    Prefix = "Speedy",
    Suffix = "of Quickness",
    EssenceType = "Primary",
    EssenceRestrictions = {"weapon"},
    Effect = "mobilemod",
    EffectName = "Bonus Run Speed",
    EffectMode = "plus",
    MobileMod = "MoveSpeedPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {.10,.10},
        {.10,.10},
        {.10,.10},
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = .10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfDefense = {
    Name = "Rune of Armor Defense",
    Prefix = "Reinforced",
    Suffix = "of Sturdiness",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "shield"},
    Effect = "mobilemod",
    EffectName = "Bonus Armor Defense",
    EffectMode = "plus",
    MobileMod = "DefensePlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,1},
        {2,2},
        {3,3}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 10,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfStamina = {
    Name = "Rune of Stamina",
    Prefix = "Healthy",
    Suffix = "of Physical Energy",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "shield"},
    Effect = "mobilemod",
    EffectName = "Bonus Stamina",
    EffectMode = "plus",
    MobileMod = "MaxStaminaPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,10},
        {5,15},
        {10,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 50,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfMana = {
    Name = "Rune of Mana",
    Prefix = "Quenched",
    Suffix = "of Total Mana",
    EssenceType = "Primary",
    EssenceRestrictions = {"armor", "shield"},
    Effect = "mobilemod",
    EffectName = "Bonus Mana",
    EffectMode = "plus",
    MobileMod = "MaxManaPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,10},
        {5,15},
        {10,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 50,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfBloodlustRegen = {
    Name = "Rune of Bloodlust Regeneration",
    Prefix = "Blood thirsty",
    Suffix = "of Bloodlust",
    EssenceType = "Primary",
    EssenceRestrictions = {"shield"},
    Effect = "mobilemod",
    EffectName = "Bloodlust Regeneration",
    EffectMode = "plus",
    MobileMod = "BloodlustRegenPlus",
    EffectRanges = { -- percent to reduce mana per cast
        { Max = 0.1, Tooltip = "Bloodlust Regen I" },
        { Max = 0.15, Tooltip = "Bloodlust Regen II" },
        { Max = 0.20, Tooltip = "Bloodlust Regen III" },
        { Max = 0.25, Tooltip = "Bloodlust Regen IV" },
        { Max = 0.3, Tooltip = "Bloodlust Regen V" },
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 1,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

--SCAN ADDED
EssenceData.RuneOfBloodlustPlus = {
    Name = "Rune of Bloodlust Plus",
    Prefix = "Blood thirsty",
    Suffix = "of Bloodlust",
    EssenceType = "Primary",
    EssenceRestrictions = {"shield"},
    Effect = "mobilemod",
    EffectName = "Increased Bloodlust",
    EffectMode = "plus",
    MobileMod = "MaxBloodlustPlus",
    EffectRanges = { -- percent to reduce mana per cast
        {1,10},
        {5,15},
        {10,25}
    },
    GoldCostMultiplier = { 
        0.5, -- 1st tier essence ( 0.5 = 50%, 2.0 = 200% )
        1.5, -- 2nd tier essence
        2 -- 3rd tier essence
    },
    EffectRangeMax = 100,
    ClientId = 1108,
    Template = "NOTEMPLATE"
}

------------------------------------------------------------------------------------

------------------------------------------------------------------------------------

-- 35 unique sets
EssenceData.MageEnchants = 
{
    -- Armor
    "RuneofMending",
    "RuneofWater",
    "RuneofEvasion",
    "TheurgyThreads",
    "FlowingThreads",
    "RuneofEvasion",
    "RuneOfHealth",
    "EffectManaRegenPlus",
    "EffectHealthRegenPlus",
    "EffectStaminaRegenPlus",
    --SCAN ADDED
    "RuneOfMana",
    "RuneOfDefense",
    "RuneOfIntelligence",
    "RuneOfWisdom",
    "RuneOfWill",
    "RuneOfConstitution",


    -- Hybrid Option
    "RuneofSpeed",
    "RuneofAccuracy",
    "RuneofVanquishing",
    "RuneofPower",
    "RuneofFire",
    "RuneOfIce",
    "RuneOfLightning",
    "RuneOfSunder",
    "RuneOfSilence",
    --SCAN ADDED
    "ChanceToHealCasterOnDamageSpellHit",
    "ChanceToIgniteTargetOnDamageSpellHit",
    "RuneOfStun",
    "RuneOfBleed",
    "RuneOfPoison",
    "EffectofStarfall",

    -- Shield
    "ShinyTrinket",
    "ExpandingTrinket",
}

-- 70 unique sets
EssenceData.MeleeEnchants = 
{
    -- Armor
    "RuneofMending",
    "RuneofWater",
    "RuneofEvasion",
    "TheurgyThreads",
    "FlowingThreads",
    "RuneofEvasion",
    "RuneOfHealth",
    "EffectManaRegenPlus",
    "EffectHealthRegenPlus",
    "EffectStaminaRegenPlus",
    --SCAN ADDED
    "ChanceToGainEchoHealOnMeleeStruck",
    "RuneOfStamina",
    "RuneOfDefense",
    "RuneOfStrength",
    "RuneOfWill",
    "RuneOfAgility",
    "RuneOfWisdom",
    "RuneOfConstitution",

    -- Weapons
    "RuneofSpeed",
    "RuneofAccuracy",
    "RuneofFire",
    "RuneOfIce",
    "RuneOfLightning",
    "RuneOfSunder",
    "RuneOfSilence",
    "RuneofVanquishing",
    --SCAN ADDED
    "ChanceToEchoBurnOnMeleeHit",
    "ChanceToGainEchoHealOnMeleeHit",
    "ChanceToChillOnMeleeHit",
    "RuneOfBloodlustPlus",
    "RuneOfBloodlustRegen",
    "RuneOfStun",
    "RuneOfBleed",
    "RuneOfPoison",


    -- Shield
    "ShinyTrinket",
    "ExpandingTrinket",
}

-- 126 unique sets
EssenceData.RangedEnchants = 
{
    -- Armor
    "RuneofMending",
    "RuneofWater",
    "RuneofEvasion",
    "TheurgyThreads",
    "FlowingThreads",
    "RuneofEvasion",
    "RuneOfHealth",
    "EffectManaRegenPlus",
    "EffectHealthRegenPlus",
    "EffectStaminaRegenPlus",
    --SCAN ADDED
    "RuneOfStamina",
    "RuneOfDefense",
    "RuneOfStrength",
    "RuneOfWill",
    "RuneOfAgility",
    "RuneOfWisdom",
    "RuneOfConstitution",

    -- Weapons
    "RuneofSpeed",
    "RuneOfRoving",
    "RuneofPatience",
    "RuneofFire",
    "RuneOfIce",
    "RuneOfLightning",
    "RuneOfSunder",
    "RuneOfSilence",
    "RuneofVanquishing",
    --SCAN ADDED
    "RuneOfStun",
    "RuneOfBleed",
    "RuneOfPoison",
    "RuneOfBloodlustRegen",
    "RuneOfBloodlustPlus",
    "EffectofStarfall",
    "RuneofPower",
}

EssenceData.Miscs = 
{
    "RuneOfTaming",
    "RuneOfLockpicking",
    "EffectOfHarvesting",
    "RuneOfCarrying"
}

EssenceData.ItemNames = 
{
    MageEnchants = 
    {
        Prefix = 
        {
            "Magi's",
            "Desire's",
            "Witch's",
            "Philosopher's",
            "Wizard's",
            "Sage's",
            "Seer's",
        },
    },
    RangedEnchants = 
    {
        Prefix = 
        {
            "Assassin's",
            "Rogue's",
            "Rascal's",
            "Blackguard's",
            "Outlaw's",
            "Trickster's",
            "Swindler's",
        },
    },
    MeleeEnchants = 
    {
        Prefix = 
        {
            "Hero's",
            "Guardian's",
            "Hatred's",
            "Figher's",
            "Soldier's",
            "Brawler's",
            "Conscript's",
        },
    },

    Suffixes = 
    {
        "Pillar",
        "Genesis",
        "Mania",
        "Spite",
        "Sanctity",
        "Charm",
        "Dread",
        "Oblivion",
        "Spirit",
        "Aether",
        "Yell",
        "Call",
        "Fount",
        "Onyx",
        "Chaos",
        "Grace",
        "Desire",
        "Malady",
        "Seduction",
        "Resistance",
        "Judgement",
        "Wisdom",
        "Chance",
        "Blight",
        "Paramount",
        "Faith",
        "Soul",
        "Pride",
        "Betrayal",
        "Isolation",
        "Justice",
        "Lunacy",
        "Pestilence",
        "Thunder",
        "Shade",
        "Remedy",
        "Obedience",
        "Decadence",
        "Peace",
        "Riches",
        "Lifeblood",
        "Solitude",
        "Liberty",
        "Treachery",
        "Binding",
        "Torment",
        "Resurrection",
        "Storm",
        "Rain",
        "Riddle",
        "Grail",
        "Aurora",
        "Slumber",
        "Sight",
        "Root",
        "Shadow",
        "Ichor",
        "Oracle",
        "Muse",
        "Siren",
        "Mystery",
        "Grace",
        "Youth",
        "Radiance",
        "Gleam",
        "Depravity",
        "Birth",
        "Torture",
        "Downfall",
        "Fortuity",
        "Oath",
        "Promise",
        "Symbol",
        "Vengeance",
        "Lust",
        "Void",
        "Bane",
        "Prosperity",
        "Wish",
        "Pain",
        "Virility",
        "Specter",
        "Seal",
        "Revival",
        "Occult",
        "Flame",
        "Scorch",
        "Slumber",
        "Wakening",
        "Fortune"
    }
}

