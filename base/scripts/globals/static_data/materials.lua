
--[[
    A Material is something that transfers its hue to the crafted product
]]


-- this is a list of materials per crafting skill (so we can build out a nice selectable list)
MaterialIndex = {
	MetalsmithSkill = {
		"Iron",
        "Copper",
        "Gold",
        "Cobalt",
		"Obsidian",
	},
	FabricationSkill = {
        "Cloth",
        "QuiltedCloth",
        "SilkCloth",

        "Leather",
        "BeastLeather",
        "VileLeather",
	},
    WoodsmithSkill = {
        "Boards",
        "AshBoards",
        "BlightwoodBoards",
    }
}

-- a resource must be defined in here to count as a material, the value is the hue
Materials = {
    --[[ DEFAULT
    Iron = 22,
    Gold = 787,
    Copper = 667,
    Cobalt = 836,
    Obsidian = 893,

    Cloth = 139,
    QuiltedCloth = 934,
    SilkCloth = 872,

    Leather = 800,
    BeastLeather = 893,
    VileLeather = 941,

    Boards = 775,
    AshBoards = 866,
    BlightwoodBoards = 865,
    ]]

    --SCAN UPDATED CRAFTED MATERIAL COLORS
    Iron = 870,
    Gold = 787,
    Copper = 661,
    Cobalt = 454,
    Obsidian = 7,

    Cloth = 866,
    QuiltedCloth = 827,
    SilkCloth = 872,

    Leather = 868,
    BeastLeather = 16,
    VileLeather = 948,

    Boards = 775,
    AshBoards = 866,
    BlightwoodBoards = 865,

}

FabricationResourceNodes = {
    LeatherHide = 800,
    BeastLeatherHide = 893,
    VileLeatherHide = 941,
    CottonFluffy = 79,
}

OreVeins = {
    --[[ DEFAULT
    IronOre = 127,
    CopperOre = 93,
    GoldOre = 934,
    CobaltOre = 836,
    ObsidianOre = 973,
    ]]
    
    --SCAN ADDED
    IronOre = 127,
    CopperOre = 93,
    GoldOre = 830,
    CobaltOre = 822,
    ObsidianOre = 973,

}

MaterialTooltipColors = {
    Iron = "[969696]",
    Gold = "[e6b032]",
    Copper = "[9cff3c]",
    Cobalt = "[d7f5f2]",
    Obsidian = "[b459ff]",

    Cloth = "[b3a489]",
    QuiltedCloth = "[e6cfa5]",
    SilkCloth = "[fff2d9]",

    Leather = "[ab805c]",
    BeastLeather = "[b459ff]",
    VileLeather = "[d61c1c]",

    Boards = "[b5751b]",
    AshBoards = "[dbb47d]",
    BlightwoodBoards = "[b459ff]",
}