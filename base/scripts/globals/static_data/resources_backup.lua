--[[
require 'default:globals.static_data.resources'

-- update all basic resources with basic minstack and maxstack
-- the table entry naturemagicbonus is the max bonus from the nature magic spell
-- you can easily add your own entries for any other harvestable plant you wish by copy + paste

--------------------------
-- Cotton
--------------------------
ResourceData.ResourceInfo.Cotton.MinStack = 1
ResourceData.ResourceInfo.Cotton.MaxStack = 4
-- nature spell buff
ResourceData.ResourceInfo.Cotton.NatureMagicBonus = 4

--------------------------
-- Cotton Fluffy
--------------------------
ResourceData.ResourceInfo.CottonFluffy.MinStack = 1
ResourceData.ResourceInfo.CottonFluffy.MaxStack = 4
-- nature spell buff
ResourceData.ResourceInfo.CottonFluffy.NatureMagicBonus = 4

--------------------------
-- Barrens Cotton
--------------------------
ResourceData.ResourceInfo.BarrensCotton.MinStack = 2
ResourceData.ResourceInfo.BarrensCotton.MaxStack = 4
-- nature spell buff
ResourceData.ResourceInfo.BarrensCotton.NatureMagicBonus = 4

--------------------------
-- Barrens Cotton Fluffy
--------------------------
ResourceData.ResourceInfo.BarrensCottonFluffy.MinStack = 2
ResourceData.ResourceInfo.BarrensCottonFluffy.MaxStack = 4
-- nature spell buff
ResourceData.ResourceInfo.BarrensCottonFluffy.NatureMagicBonus = 4

--------------------------
-- Ginseng
--------------------------
ResourceData.ResourceInfo.Ginseng.MinStack = 2
ResourceData.ResourceInfo.Ginseng.MaxStack = 4
-- nature spell buff
ResourceData.ResourceInfo.Ginseng.NatureMagicBonus = 4

--------------------------
-- Moss
--------------------------
ResourceData.ResourceInfo.Moss.MinStack = 2
ResourceData.ResourceInfo.Moss.MaxStack = 4
-- nature spell buff
ResourceData.ResourceInfo.Moss.NatureMagicBonus = 4

--------------------------
-- LemonGrass
--------------------------
ResourceData.ResourceInfo.LemonGrass.MinStack = 2
ResourceData.ResourceInfo.LemonGrass.MaxStack = 4
-- nature spell buff
ResourceData.ResourceInfo.LemonGrass.NatureMagicBonus = 4

--------------------------
-- Mushrooms
--------------------------
ResourceData.ResourceInfo.Mushrooms.MinStack = 2
ResourceData.ResourceInfo.Mushrooms.MaxStack = 4
-- nature spell buff
ResourceData.ResourceInfo.Mushrooms.NatureMagicBonus = 4  

ResourceData.ResourceInfo.GiantMushrooms.MinStack = 1
ResourceData.ResourceInfo.GiantMushrooms.MaxStack = 4
-- nature spell buff
ResourceData.ResourceInfo.GiantMushrooms.NatureMagicBonus = 4  

--------------------------
-- Cactus
--------------------------
ResourceData.ResourceInfo.Cactus.MinStack = 1
ResourceData.ResourceInfo.Cactus.MaxStack = 3
-- nature spell buff
ResourceData.ResourceInfo.Cactus.NatureMagicBonus = 3

ResourceData.ResourceInfo.SacredCactus.MinStack = 1
ResourceData.ResourceInfo.SacredCactus.MaxStack = 3
-- nature spell buff
ResourceData.ResourceInfo.SacredCactus.NatureMagicBonus = 3

--------------------------
-- Kindling
--------------------------
ResourceData.ResourceInfo.Kindling.MinStack = 1
ResourceData.ResourceInfo.Kindling.MaxStack = 2
-- nature spell buff
ResourceData.ResourceInfo.Kindling.NatureMagicBonus = 3
]]