PrestigeData.Militia = {
	NoUnlock = true,

	Abilities = {
		SummonRam = {
			Action = {
				DisplayName = "Construct Battering Ram",
				Icon = "Bash",	
				Enabled = true
			},

			NoResetSwing = true,
			NoCombat = true,
			Instant = true,
			NoDismount = true,

			Tooltip = "Constructs a battering ram. Can only be used at an enemy militia keep.",

			MobileEffect = "SummonRam",
			--MobileEffectArgs = {},
			Cooldown = TimeSpan.FromSeconds(5),

			MilitiaRankRequirement = 1
		},

		EquipCloak = {
			Action = {
				DisplayName = "Equip Militia Cloak",
				Icon = "Phantom",	
				Enabled = true
			},

			NoResetSwing = true,
			NoCombat = true,
			Instant = true,
			NoDismount = true,

			Tooltip = "Equips your faction cloak.",

			MobileEffect = "EquipMilitiaCloak",
			--MobileEffectArgs = {},
			Cooldown = TimeSpan.FromMinutes(1),

			MilitiaRankRequirement = 2
		},

		SummonMount = {
			Action = {
				DisplayName = "Summon Militia Mount",
				Icon = "Summon White Horse",	
				Enabled = true
			},

			NoResetSwing = true,
			NoCombat = true,
			Instant = true,
			NoDismount = true,

			Tooltip = "Summons your militia mount.",

			MobileEffect = "SummonMilitiaMount",
			--MobileEffectArgs = {},
			Cooldown = TimeSpan.FromSeconds(1),--TimeSpan.FromMinutes(1),

			MilitiaRankRequirement = 3
		},
		
	}
}