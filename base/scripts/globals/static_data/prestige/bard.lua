PrestigeData.Bard = {
	DisplayName = "Bard",
	Description = "A bard.",
	
	Abilities = {
		
		TricksterFooting = {
			Rank = 1,
			Action = {
				DisplayName = "Trickster's Footing",
				Icon = "bard-trickers-footing",
				Enabled = true
			},
			Tooltip = "Removes movement penalty when playing an instrument. (Passive)",
			MobileEffect = "Passive",
			Cooldown = TimeSpan.FromSeconds(1),
		},

		TricksterPsalm = {
			Rank = 1,
			Action = {
				DisplayName = "Trickster's Psalm",
				Icon = "bard-trickers-psalm",
				Enabled = true
			},
			Tooltip = "Songs can be played to 50% effectiveness without an instrument equipped. (Passive)",
			MobileEffect = "Passive",
			Cooldown = TimeSpan.FromSeconds(1)
		},

		ResoundingEchoes = {
			Rank = 2,
			NoCombat = true,
			NoResetSwing = true,
			Action = {
				DisplayName = "Resounding Echoes",
				Icon = "bard-resounding-echoes",
				Enabled = true
			},

			Tooltip = "Removes and prevents movement impairing effects for 10 seconds.",
			MobileEffect = "ResoundingEchoes",
			--Cooldown = TimeSpan.FromSeconds(1),
			Cooldown = TimeSpan.FromMinutes(5),
		},

		ResoundingDrums = {
			Rank = 2,
			Action = {
				DisplayName = "Resounding Drums",
				Icon = "bard-resounding-drums",
				Enabled = true
			},

			Tooltip = "Removes and prevents bleed, mortal strike, and poison effects for 10 seconds.",
			MobileEffect = "ResoundingDrums",
			--Cooldown = TimeSpan.FromSeconds(1),
			Cooldown = TimeSpan.FromMinutes(5)
		},

		YurinsLullaby = {
			Rank = 3,
			Action = {
				DisplayName = "Yurin's Lullaby",
				Icon = "bard-yurin-lullaby",
				Enabled = true
			},

			Tooltip = "Puts target to sleep for 10 seconds. Any damage taken will wake the target.",
			MobileEffect = "YurinsLullaby",
			Cooldown = TimeSpan.FromSeconds(1),
			--PostCooldown = TimeSpan.FromSeconds(1),
			Cooldown = TimeSpan.FromMinutes(5),
			IgnoreCooldown = true,
		},

		AzurasBlessing = {
			Rank = 3,
			NoCombat = true,
			NoResetSwing = true,
			Action = {
				DisplayName = "Azura's Blessing",
				Icon = "bard-azura-blessing",
				Enabled = true
			},

			Tooltip = " Instantly resurrects, a recently deceased, target to full health and mana.",
			MobileEffect = "AzurasBlessing",
			Cooldown = TimeSpan.FromSeconds(1),
			--PostCooldown = TimeSpan.FromSeconds(1),
			Cooldown = TimeSpan.FromMinutes(20)
		},

	},
}