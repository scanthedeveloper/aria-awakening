PrestigeData.Fighter = {
	DisplayName = "Fighter",
	Description = "A Fighter.",
	Abilities = {
		Dart = {
			Rank = 1,
			Action = {
				DisplayName = "Dart",
				Icon = "Blazing Speed",
				Enabled = true
			},

			NoCombat = true, -- don't need to force them into combat mode for this
			NoResetSwing = true,
			--RequireLightArmor = true,

					Tooltip = "Increase movement speed by 30% for 8 seconds.",

					MobileEffect = "Dart",
					MobileEffectArgs = {
						Modifier = 0.3,
						Duration = TimeSpan.FromSeconds(8),
					},
					Cooldown = TimeSpan.FromSeconds(60)
		},
		Hamstring = {
			Rank = 1,
			Action = {
				DisplayName = "Hamstring",
				Icon = "Weapon Throw",				
				Enabled = true
			},

			RequireCombatTarget = true,
			PreventRanged = false,
					
					Tooltip = "Reduces your target's runspeed by 50% for 8 seconds.",

					TargetMobileEffect = "Hamstring",
					TargetMobileEffectArgs = {
						Duration = TimeSpan.FromSeconds(8),
						Modifier = -0.5,
					},
					Cooldown = TimeSpan.FromSeconds(60),		
		},
		ShieldBash = {
			Rank = 2,
			Action = {
				DisplayName = "Shield Bash",
				Icon = "Block",				
				Enabled = true
			},

			RequireShield = true,
			RequireCombatTarget = true,

            Tooltip = "Stun the target for 12 seconds. Reduced to 4 second for players.",

            MobileEffect = "ShieldBash",
            MobileEffectArgs = {
                Duration = TimeSpan.FromSeconds(12),
                PlayerDuration = TimeSpan.FromSeconds(4),
            },
            Cooldown = TimeSpan.FromSeconds(60),
		},
		Charge = {
			Rank = 2,
			Action = {
				DisplayName = "Relentless Hate",
				Icon = "Summon Bear",				
				Enabled = true,
			},

			RequireCombatTarget = true,

			NoResetSwing = true,

				    Tooltip = "Teleport to your target and stun them for 3 second.",

					Range = 10,

					MobileEffect = "RelentlessHate",
					MobileEffectArgs = {
						Duration = TimeSpan.FromSeconds(3),
					},
					Cooldown = TimeSpan.FromSeconds(45),
		},
		StunStrike = {
			Rank = 2,
			Action = {
				DisplayName = "Stun Strike",
				Icon = "Blink Strike",				
				Enabled = true
			},

			RequireCombatTarget = true,

					Tooltip = "Stun all enemies 4 yards in front of you for 5 seconds. Stuns players for 3 second.",

					MobileEffect = "StunStrike",
					MobileEffectArgs = {
						Radius = 5,
						Duration = TimeSpan.FromSeconds(5),
						PlayerDuration = TimeSpan.FromSeconds(3),
					},
					Cooldown = TimeSpan.FromSeconds(20),
		},
		Vanguard = {
			Rank = 3,
			Action = {
				DisplayName = "Vanguard",
				Icon = "Fire Shield",				
				Enabled = true,
			},
			
			NoResetSwing = true,
			--RequireHeavyArmor = true,
			--RequireShield = true,

				    Tooltip = "Physical damage received reduced by 80%. Half of all damage recieved is reflected. Movement speed reduced by 20%. Duration 5 seconds.",

					MobileEffect = "Vanguard",
					MobileEffectArgs = {
						RunSpeedModifier = -0.2,
						DamageReturnModifier = -0.5,
						DamageReductionModifier = -0.8,
						Duration = TimeSpan.FromSeconds(5),
					},
					Cooldown = TimeSpan.FromMinutes(2),
		},
		Evasion = {
			Rank = 3,
			Action = {
				DisplayName = "Evasion",
				Icon = "Quick Shot",
				Enabled = true
			},

			NoCombat = true, -- don't need to force them into combat mode for this
			NoResetSwing = true,
			--RequireLightArmor = true,

					Tooltip = "Increase evasion by 10 for 7 seconds.",

					MobileEffect = "Evasion",
					MobileEffectArgs = {
						Duration = TimeSpan.FromSeconds(7),
						Amount = 250,
					},
					Cooldown = TimeSpan.FromSeconds(30)
		},
		AdrenalineRush = {
			Rank = 3,
			Action = {
				DisplayName = "Adrenaline Rush",
				Icon = "Berserker Rage",				
				Enabled = true
			},

			Instant = true,
		    NoTarget = true,
		    NoCombat = false,	
					
					Tooltip = "Feel a rush of adrenaline and restore stamina rapidly.",

					MobileEffect = "AdrenalineRush",
					MobileEffectArgs = {
						Modifier = 0.15,
					},
					Cooldown = TimeSpan.FromSeconds(120),		
		},
	},
}