PrestigeData.Barding = {
    DisplayName = "Barding",
    NoUnlock = true,
    --SCAN Changed
    SongMovementSpeed = -0.0,
    --SongMovementSpeed = -0.7,
    SkillGainCheckInSeconds = 3, -- you can only gain skill every 3 seconds
    Abilities = {

        SongOfThew = {
            NoTrainingRequired = true,
            NeedSongBook = true,
            --NeedLOS = true,
            AbilityType = "Song",
            Action = {
                DisplayName = "Song of Thew",
                Icon = "Bard_Thew",			
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Rank = 2,
            Tooltip = "Regenerates the health of nearby allies.",
            RequiresInstrument = true, -- Does this song require an instrument?
            RequiresResource = { Mana = 10 },
            MobileEffect = "SongAOE",
            MobileEffectArgs = {
                SkillRequirement = 25, -- music/entertaining skill required
                PulseDuration = 5, -- How long between each song pulse?
                MaxPulseCount = 3, -- How many pulses should occur?
                MobileMod = "HealthRegenPlus", -- What mobile mod should be applied?
                MobileModID = "SongOfThew", -- leave alone, unless developer!
                MobileModAmount = 10, -- Value to set for the MobileMod applied.
                AreaOfEffect = 8, -- How far around the player should this AoE occur?
                BuffIcon = "Bard_Thew", -- What icon should appear on affected players?
                BuffDescription = "Regenerating health.", -- The description the buff icon will show.
                FriendOnly = true, -- Should the only affect friendly units? If false will only affect enemies.
                BardEffect = "MusicNoteIntensity2Effect", -- What effect should play? nil if none
                TargetEffect = "BardAbsorbThewEffect", -- The effect that should be shown on the target of the song
                SongSFX = "event:/magic/bard/song_of_thew",
            },
            Cooldown = TimeSpan.FromSeconds(15),
            CastTime = TimeSpan.FromSeconds(1.5),
        },

        SongOfWater = {
            NoTrainingRequired = true,
            NeedSongBook = true,
            --NeedLOS = true,
            AbilityType = "Song",
            
            Action = {
                DisplayName = "Song of Water",
                Icon = "Bard_Water",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Tooltip = "Regenerates the mana of nearby allies.",
            RequiresInstrument = true, -- Does this song require an instrument?
            RequiresResource = { Mana = 10 },
            MobileEffect = "SongAOE",
            Rank = 8,
            MobileEffectArgs = {
                SkillRequirement = 75, -- music/entertaining skill required
                PulseDuration = 5,
                MaxPulseCount = 3,
                MobileMod = "ManaRegenPlus",
                MobileModID = "SongOfWater",
                MobileModAmount = 3,
                AreaOfEffect = 8,
                BuffIcon = "Bard_Water",
                BuffDescription = "Regenerating mana.",
                FriendOnly = true,
                BardEffect = "MusicNoteIntensity2Effect",
                TargetEffect = "BardAbsorbWaterEffect",
                SongSFX = "event:/magic/bard/song_of_water",
            },
            Cooldown = TimeSpan.FromSeconds(15),
            CastTime = TimeSpan.FromSeconds(1.5),
        },

        SongOfEarth = {
            NoTrainingRequired = true,
            AbilityType = "Song",
            NeedSongBook = true,
            --NeedLOS = true,
            Action = {
                DisplayName = "Song of Earth",
                Icon = "Bard_Earth",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Tooltip = "Regenerates the stamina of nearby allies.",
            RequiresInstrument = true, -- Does this song require an instrument?
            RequiresResource = { Mana = 10 },
            MobileEffect = "SongAOE",
            Rank = 5,
            MobileEffectArgs = {
                SkillRequirement = 50, -- music/entertaining skill required
                PulseDuration = 5,
                MaxPulseCount = 3,
                MobileMod = "StaminaRegenPlus",
                MobileModID = "SongOfEarth",
                MobileModAmount = 8,
                AreaOfEffect = 8,
                BuffIcon = "Bard_Earth",
                BuffDescription = "Regenerating stamina.",
                FriendOnly = true,
                BardEffect = "MusicNoteIntensity2Effect",
                TargetEffect = "BardAbsorbEarthEffect",
                SongSFX = "event:/magic/bard/song_of_earth",
            },
            Cooldown = TimeSpan.FromSeconds(15),
            CastTime = TimeSpan.FromSeconds(1.5),
        },

        --SCAN ADDED
        SongOfIron = {
            NoTrainingRequired = true,
            NeedSongBook = true,
            --NeedLOS = true,
            AbilityType = "Song",
            
            Action = {
                DisplayName = "Song of Iron",
                Icon = "Holy Shield",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Tooltip = "Provides defense to nearby allies.",
            RequiresInstrument = true, -- Does this song require an instrument?
            RequiresResource = { Mana = 10 },
            MobileEffect = "SongAOE",
            Rank = 7,
            MobileEffectArgs = {
                SkillRequirement = 75, -- music/entertaining skill required
                PulseDuration = 5,
                MaxPulseCount = 3,
                MobileMod = "DefensePlus",
                MobileModID = "SongOfIron",
                MobileModAmount = 10,
                AreaOfEffect = 8,
                BuffIcon = "Holy Shield",
                BuffDescription = "Increase party defense by +10.",
                FriendOnly = true,
                BardEffect = "MusicNoteIntensity2Effect",
                TargetEffect = "VanguardEffect",
                SongSFX = "event:/magic/bard/song_of_water",
            },
            Cooldown = TimeSpan.FromSeconds(15),
            CastTime = TimeSpan.FromSeconds(1.5),
        },

        --SCAN ADDED
        SongOfWind = {
            NoTrainingRequired = true,
            NeedSongBook = true,
            --NeedLOS = true,
            AbilityType = "Song",
            
            Action = {
                DisplayName = "Song of Wind",
                Icon = "Tracking",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Tooltip = "Provides increased 20% movement speed to nearby allies.",
            RequiresInstrument = true, -- Does this song require an instrument?
            RequiresResource = { Mana = 10 },
            MobileEffect = "SongAOE",
            Rank = 4,
            MobileEffectArgs = {
                SkillRequirement = 40, -- music/entertaining skill required
                PulseDuration = 5,
                MaxPulseCount = 3,
                MobileMod = "MoveSpeedPlus",
                MobileModID = "SongOfWind",
                MobileModAmount = .20,
                AreaOfEffect = 20,
                BuffIcon = "Tracking",
                BuffDescription = "Increase party movement speed by 20%",
                FriendOnly = true,
                BardEffect = "MusicNoteIntensity2Effect",
                TargetEffect = "PrimedAir",
                SongSFX = "event:/magic/bard/song_of_thew",
            },
            Cooldown = TimeSpan.FromSeconds(15),
            CastTime = TimeSpan.FromSeconds(1.5),
        },

        --SCAN ADDED
        SongOfRage = {
            NoTrainingRequired = true,
            NeedSongBook = true,
            --NeedLOS = true,
            AbilityType = "Song",
            
            Action = {
                DisplayName = "Song of Rage",
                Icon = "Berserker Rage",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Tooltip = "Provides Bloodlust regeneration to nearby allies.",
            RequiresInstrument = true, -- Does this song require an instrument?
            RequiresResource = { Mana = 10 },
            MobileEffect = "SongAOE",
            Rank = 6,
            MobileEffectArgs = {
                SkillRequirement = 60, -- music/entertaining skill required
                PulseDuration = 1,
                MaxPulseCount = 15,
                MobileMod = "BloodlustRegenPlus",
                MobileModID = "SongOfRage",
                MobileModAmount = 2,
                AreaOfEffect = 8,
                BuffIcon = "Berserker Rage",
                BuffDescription = "Increase party bloodlust regeneration by +2 per second.",
                FriendOnly = true,
                BardEffect = "MusicNoteIntensity2Effect",
                TargetEffect = "WarriorDemoStatus",
                SongSFX = "event:/magic/bard/song_of_earth",
            },
            Cooldown = TimeSpan.FromSeconds(15),
            CastTime = TimeSpan.FromSeconds(1.5),
        },

        RiddleOfIndulgence = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Indulgence",
                Icon = "Bard_Indulgence",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Rank = 1,
            Tooltip = "Reduces target's resistance to spells by 25%.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "RiddleCurrentTarget",
            MobileEffectArgs = {
                SkillRequirement = 15, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                BuffIcon = "Bard_Indulgence",
                BuffDescription = "Your combat target has reduced spell resistance.",
                MobileMod = "WisdomPlus",
                MobileModID = "RiddleOfIndulgence",
                MobileModAmount = -25,
                ForceWholeNumber = true,
                BardEffect = "BardProjectileMagenta",
                TargetEffect = "BardDistortionMagenta",
                Range = 15,
                ProjSFX = "event:/magic/bard/riddle_of_indulgence",
            },
            Cooldown = TimeSpan.FromSeconds(12),
            CastTime = TimeSpan.FromSeconds(0.5),
        },

        --SCAN REMOVED
        --[[
        RiddleOfStupidity = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Stupidity",
                Icon = "Bard_Susceptibility",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Rank = 3,
            Prerequisites = {},
            Tooltip = "Reduces target's resistance to abilities for 10 seconds.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "RiddleCurrentTarget",
            MobileEffectArgs = {
                SkillRequirement = 30, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                MobileMod = "WillPlus",
                MobileModID = "RiddleOfStupidity",
                MobileModAmount = -25,
                BardEffect = "BardProjectileGreen",
                TargetEffect = "BardDistortionGreen",
                Range = 15,
                BuffIcon = "Bard_Susceptibility",
                BuffDescription = "Your combat target has reduced ability resistance.",
                ProjSFX = "event:/magic/bard/riddle_of_stupidity",
            },
            Cooldown = TimeSpan.FromSeconds(10),
            CastTime = TimeSpan.FromSeconds(0.5),
        },

        RiddleOfVirility = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Virility",
                Icon = "Bard_Infirmity",
                Enabled = true
            },
            NoCombat = true,
            Rank = 4,
            NoResetSwing = true,
            Prerequisites = {},
            Tooltip = "Reduces healing effects on target for 10 seconds.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "RiddleCurrentTarget",
            MobileEffectArgs = {
                SkillRequirement = 45, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                MobileMod = "HealingReceivedTimes",
                MobileModID = "RiddleOfVirility",
                MobileModAmount = -0.30, -- -30% healing
                BardEffect = "BardProjectileOrange",
                TargetEffect = "BardDistortionOrange",
                Range = 15,
                BuffIcon = "Bard_Infirmity",
                BuffDescription = "Your combat target has reduced healing.",
                ProjSFX = "event:/magic/bard/riddle_of_virility",
            },
            Cooldown = TimeSpan.FromSeconds(10),
            CastTime = TimeSpan.FromSeconds(0.5),
        },
        
        RiddleOfForce = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            --NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Force",
                Icon = "Bard_Force",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Rank = 6,
            Tooltip = "Nearby creatures will receive 30% increased physical damage for 10 seconds.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "RiddleAOE",
            MobileEffectArgs = {
                SkillRequirement = 60, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                MobileMod = "PhysicalDamageTakenTimes",
                MobileModID = "RiddleOfForce",
                MobileModAmount = 0.3,
                --MobileModAmountMin = 0.3,
                AreaOfEffect = 8,
                CreatureOnly = true,
                BardEffect = "BardExplosionRed",
                TargetEffect = "BardDistortionRed",
                MobileEffect = "ApplyMobileMod",
                AoESFX = "event:/magic/bard/riddle_of_force",
            },
            Cooldown = TimeSpan.FromSeconds(5),
            CastTime = TimeSpan.FromSeconds(0.5),
        },

        RiddleOfHavoc = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            --NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Havoc",
                Icon = "Bard_Havoc",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Rank = 7,
            Tooltip = "Nearby creatures receive 30% increased magic damage for 10 seconds.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "RiddleAOE",
            MobileEffectArgs = {
                SkillRequirement = 75, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                MobileMod = "MagicDamageTakenTimes",
                MobileModID = "RiddleOfHavoc",
                MobileModAmount = 0.3,
                --MobileModAmountMin = 1,
                AreaOfEffect = 8,
                BuffIcon = "Healing Hands",
                BuffDescription = "Magic damage verus creatures increased.",
                CreatureOnly = true,
                BardEffect = "BardExplosionViolet",
                TargetEffect = "BardDistortionViolet",
                MobileEffect = "ApplyMobileMod",
                AoESFX = "event:/magic/bard/riddle_of_havoc",
            },
            Cooldown = TimeSpan.FromSeconds(5),
            CastTime = TimeSpan.FromSeconds(0.5),
        },
        ]]

        RiddleOfDocility = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            --NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Docility",
                Icon = "Bard_Docility",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Tooltip = "Small chance to render nearby creatures incapacitated for 10 seconds. Lasts 10 seconds.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "RiddleAOE",
            Rank = 8,
            MobileEffectArgs = {
                SkillRequirement = 80, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                AreaOfEffect = 8,
                MobileModID = "RiddleOfDocility",
                ChanceToLand = 10, -- %chance to land on target
                FriendOnly = false,
                BardEffect = "BardExplosionCyan",
                TargetEffect = "BardDistortionCyan",
                CreatureOnly = true,
                MobileEffect = "Incapacitate",
                AoESFX = "event:/magic/bard/riddle_of_docility",
            },
            Cooldown = TimeSpan.FromSeconds(1),
            CastTime = TimeSpan.FromSeconds(0.5),
        },

         --SCAN ADDED
         RiddleOfSlowing = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            --NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Slowing",
                Icon = "shadowrun",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Rank = 6,
            Tooltip = "Nearby creatures will receive 30% speed reduction debuff for 10 seconds.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "RiddleAOE",
            MobileEffectArgs = {
                SkillRequirement = 60, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                MobileMod = "MoveSpeedTimes",
                MobileModID = "RiddleOfForce",
                MobileModAmount = -0.3,
                --MobileModAmountMin = 0.3,
                AreaOfEffect = 8,
                CreatureOnly = true,
                BuffDescription = "Your combat target has reduced movement speed.",
                BardEffect = "BardExplosionRed",
                TargetEffect = "BuffEffect_H",
                MobileEffect = "ApplyMobileMod",
                AoESFX = "event:/magic/bard/riddle_of_force",
            },
            Cooldown = TimeSpan.FromSeconds(5),
            CastTime = TimeSpan.FromSeconds(0.5),
        },

        --SCAN ADDED
        RiddleOfWeakening = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            --NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Weakening",
                Icon = "Martial Arts 03",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Rank = 7,
            Tooltip = "Reduce nearby enemy attack damage by 30%.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "RiddleAOE",
            MobileEffectArgs = {
                SkillRequirement = 75, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                MobileMod = "AttackTimes",
                MobileModID = "RiddleOfHavoc",
                MobileModAmount = -0.3,
                --MobileModAmountMin = 1,
                AreaOfEffect = 8,
                BuffIcon = "Healing Hands",
                BuffDescription = "Your combat target has reduced attack damage.",
                CreatureOnly = true,
                BardEffect = "BardExplosionViolet",
                TargetEffect = "BuffEffect_C",
                MobileEffect = "ApplyMobileMod",
                AoESFX = "event:/magic/bard/riddle_of_havoc",
            },
            Cooldown = TimeSpan.FromSeconds(5),
            CastTime = TimeSpan.FromSeconds(0.5),
        },

        --SCAN ADDED
        RiddleOfBlindness = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Blinding",
                Icon = "Bard_Susceptibility",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Rank = 3,
            Prerequisites = {},
            Tooltip = "Reduces the enemies ability to hit with attacks by 30%.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "RiddleCurrentTarget",
            MobileEffectArgs = {
                SkillRequirement = 30, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                MobileMod = "HitChanceTimes",
                MobileModID = "RiddleOfStupidity",
                MobileModAmount = -.30,
                BardEffect = "BardProjectileGreen",
                TargetEffect = "BuffEffect_B",
                Range = 10,
                BuffIcon = "Bard_Susceptibility",
                BuffDescription = "Your combat target has reduced ability to hit with attacks.",
                ProjSFX = "event:/magic/bard/riddle_of_stupidity",
            },
            Cooldown = TimeSpan.FromSeconds(12),
            CastTime = TimeSpan.FromSeconds(0.5),
        },

        --SCAN ADDED
        RiddleOfTounges = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Tounges",
                Icon = "Bard_Infirmity",
                Enabled = true
            },
            NoCombat = true,
            Rank = 4,
            NoResetSwing = true,
            Prerequisites = {},
            Tooltip = "Reduces the enemies spell damage by 30% for 10 seconds.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "RiddleCurrentTarget",
            MobileEffectArgs = {
                SkillRequirement = 45, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                MobileMod = "PowerTimes",
                MobileModID = "RiddleOfVirility",
                MobileModAmount = -.30, -- -30% healing
                BardEffect = "BardProjectileOrange",
                TargetEffect = "BuffEffect_J",
                Range = 10,
                BuffIcon = "Bard_Infirmity",
                BuffDescription = "Your combat target has reduced spell damage.",
                ProjSFX = "event:/magic/bard/riddle_of_virility",
            },
            Cooldown = TimeSpan.FromSeconds(10),
            CastTime = TimeSpan.FromSeconds(0.5),
        },
    }
}

        --[[SCAN ADDED
        RiddleOfDestruction = {
            NoTrainingRequired = true,
            AbilityType = "Riddle",
            NeedSongBook = true,
            --NeedLOS = true,
            Action = {
                DisplayName = "Riddle of Destruction",
                Icon = "Force Push 01",
                Enabled = true
            },
            NoCombat = true,
            NoResetSwing = true,
            Prerequisites = {},
            Tooltip = "Damage all nearby enemies, does not provide combat credit on kill.",
            RequiresResource = { Mana = 10 },
            MobileEffect = "Tangledroots",
            Rank = 5,
            MobileEffectArgs = {
                SkillRequirement = 80, -- music/entertaining skill required
                DurationInSeconds = 10, -- Force buff to stay on target for X seconds
                AreaOfEffect = 10,
                MobileModID = "RiddleOfDestruction",
                ChanceToLand = 80, -- %chance to land on target
                FriendOnly = false,
                BardEffect = "RipplesEffect",
                TargetEffect = "BardDistortionOrange",
                CreatureOnly = true,
                MobileEffect = "BardDestruction",
                AoESFX = "event:/magic/bard/riddle_of_docility",
            },
            Cooldown = TimeSpan.FromSeconds(5),
            CastTime = TimeSpan.FromSeconds(0.5),
        },
        ]]
