PrestigeData.Rogue = {
	DisplayName = "Rogue",
	Description = "A Rogue.",
	
	--[[HuntersMark = {
			Rank = 1,
			Action = {
				DisplayName = "Hunters Mark",
				Icon = "Burning Arrow",				
				Enabled = true
			},

			RequireRanged = true,
			RequireCombatTarget = true,
			NoResetSwing = true,


					Tooltip = "Mark your target, preventing them from hiding/cloaking and increases all bow damage received by 9.99% for 30 seconds.",

					TargetMobileEffect = "HuntersMark",
					TargetMobileEffectArgs = {
						Modifier = 0.099,
					},
					Cooldown = TimeSpan.FromMinutes(2),
					CastTime = TimeSpan.FromSeconds(1),
		},
		StunShot = {
			Rank = 2,
			Action = {
				DisplayName = "Stun Shot",
				Icon = "Windshot",				
				Enabled = true
			},

			RequireRanged = true,
			RequireCombatTarget = true,
			-- functions to make the cast look better
			PreCast = PrestigePreCastBow,
			PostCast = PrestigePostCastBow,


					Tooltip = "Stun your target for 5 seconds, 3 seconds for players.",

					MobileEffect = "StunShot",
					MobileEffectArgs = {
						Duration = TimeSpan.FromSeconds(5),
						PlayerDuration = TimeSpan.FromSeconds(3),
					},
					Cooldown = TimeSpan.FromSeconds(60),
					CastTime = TimeSpan.FromSeconds(1),
		},]]

			Abilities = {
		Vanish = {
			Rank = 3,
			Action = {
				DisplayName = "Vanish",
				Icon = "Phantom",
				Enabled = true
			},

					Tooltip = "Instantly hide. Removes all player debuffs.",

					MobileEffect = "Vanish",
					Cooldown = TimeSpan.FromMinutes(1)
		},
		--[[Wound = {
			Rank = 1,
			Action = {
				DisplayName = "Wound",
				Icon = "Poison Arrow",				
				Enabled = true
			},

			RequireRanged = true,
			RequireCombatTarget = true,
			-- functions to make the cast look better
			PreCast = PrestigePreCastBow,
			PostCast = PrestigePostCastBow,

			Prerequisites = {
						ArcherySkill = 40,
					},

					Tooltip = "Wound your target, slowing them by 70% for 3 seconds. 0.5 second cast time.",

					TargetMobileEffect = "Hamstring",
					TargetMobileEffectArgs = {
						Modifier = -0.7,
						Duration = TimeSpan.FromSeconds(3),
					},

					Cooldown = TimeSpan.FromSeconds(15),
					CastTime = TimeSpan.FromSeconds(1),
		},]]
	},
}