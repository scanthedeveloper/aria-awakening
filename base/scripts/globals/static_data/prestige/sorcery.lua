PrestigeData.Sorcery = {
	NoUnlock = true,

	Abilities = {

		SummonFox = {
			Action = {
				DisplayName = "Summon: Wisp Chaser",
				Icon = "Sorcerer_SpiritWolf",	
				Enabled = true
			},
			Instant = true,
			Tooltip = "Consumes 1 wisp to summon a Chasers to fight alongside you for 20 seconds. Only four foxes can be summoned at any given time.\n\nRequires Sorcery: 20.0",
			MobileEffect = "SummonFox",
			Cooldown = TimeSpan.FromSeconds(0.5),
			SkillsRequired = { SorcerySkill = 20.0 }
		},
		
		SummonConstruct = {
			Action = {
				DisplayName = "Summon: Construct",
				Icon = "Unholy Mastery",	
				Enabled = true
			},
			Instant = true,
			Tooltip = "Consumes 2 wisps to summon a Construct to fight alongside you for 40 seconds. Only 2 constructs can be summoned at any given time.\n\nRequires Sorcery: 80.0",
			MobileEffect = "SummonConstruct",
			Cooldown = TimeSpan.FromSeconds(0.5),
			SkillsRequired = { SorcerySkill = 80 }
        },
        
        MagicMissile = {
			Action = {
				DisplayName = "Affliction",
				Icon = "Unholy Blast",	
				Enabled = true
			},
			Instant = true,
			Tooltip = "Shoots a wisp at the target, stacking them with the debuff Affliction (maximum of 3 stacks). Afflicted targets will take increased Echo damage.\n\nRequires Sorcery: 0.0",
			MobileEffect = "MagicMissile",
			Cooldown = TimeSpan.FromSeconds(0.2),
			PostCooldown = TimeSpan.FromSeconds(0.5),
			IgnoreCooldown = true,
			SkillsRequired = { SorcerySkill = 0 }
		},
        
        Starfall = {
			Action = {
				DisplayName = "Echofall",
				Icon = "Hellstorm 03",	
				Enabled = true
			},
			Instant = true,
			Tooltip = "Consumes a wisp to summon a ball of echo energy that falls at the targeted location dealing 25-50 Echo damage, based on your sorcery skill, within a 3 meter radius.\n\nRequires Sorcery: 50.0",
			MobileEffect = "SummonStarfall",
			Cooldown = TimeSpan.FromSeconds(0.2),
			PostCooldown = TimeSpan.FromSeconds(0.5),
			IgnoreCooldown = true,
			SkillsRequired = { SorcerySkill = 50 }
        },        
		
        SummonBeam = {
			Action = {
				DisplayName = "Echo Beam",
				Icon = "Spectral Ball",	
				Enabled = true
			},
			Instant = true,
			Tooltip = "Consumes a wisp and projects an echo beam in the direction you are facing dealing Echo damage. Enemies that stand in the beam take increased damage the longer they are in it. Your movement speed is greatly reduced.\n\nRequires Sorcery: 90.0",
			MobileEffect = "SummonBeam",
			Cooldown = TimeSpan.FromSeconds(3),
			IgnoreCooldown = true,
			SkillsRequired = { SorcerySkill = 90 }
        },
        
        WispArmor = {
			Action = {
				DisplayName = "Wisp Armor",
				Icon = "Arcane Shield2",	
				Enabled = true
			},
			Instant = true,
			Tooltip = "Consume all available wisps, giving you a shield that absorbs 40 damage for each wisp. Shields can prevent spell interrupt but are immediately consumed.\n\nMinimum Sorcery: 30.0",
			MobileEffect = "WispArmor",
			Cooldown = TimeSpan.FromSeconds(20),
			IgnoreCooldown = true,
			SkillsRequired = { SorcerySkill = 30 }
		},
	}
}