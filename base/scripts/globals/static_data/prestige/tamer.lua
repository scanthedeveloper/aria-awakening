PrestigeData.Tamer = {
	DisplayName = "Tamer",
	Description = "A tamer.",
	
	Abilities = {
		
		NaturesFocus = {
			Rank = 1,
			NoCombat = true,
			NoResetSwing = true,
			Action = {
				DisplayName = "Nature's Focus",
				Icon = "tamer-natures-focus",
				Enabled = true
			},
			Tooltip = "All pets gain 50% increase movement speed for 10 seconds.",
			MobileEffect = "NaturesFocus",
			Cooldown = TimeSpan.FromMinutes(3),
		},

		NaturesGrace = {
			Rank = 1,
			NoCombat = true,
			NoResetSwing = true,
			Action = {
				DisplayName = "Nature's Grace",
				Icon = "tamer-natures-grace",
				Enabled = true
			},
			Tooltip = "All pets are immune to stun and mortal strike. (Passive)",
			MobileEffect = "Passive",
			Cooldown = TimeSpan.FromMinutes(1),
		},

		PrimalRage = {
			Rank = 2,
			NoCombat = true,
			NoResetSwing = true,
			Action = {
				DisplayName = "Primal Rage",
				Icon = "tamer-primal-rage",
				Enabled = true
			},
			Tooltip = "All pets gain 50% increased damage for 10 seconds.",
			MobileEffect = "PrimalRage",
			Cooldown = TimeSpan.FromMinutes(5),
		},

		PrimalFury = {
			Rank = 2,
			NoCombat = true,
			NoResetSwing = true,
			Action = {
				DisplayName = "Primal Fury",
				Icon = "tamer-primal-fury",
				Enabled = true
			},
			Tooltip = "All pets take 50% less physical and magical damage for 10 seconds.",
			MobileEffect = "PrimalFury",
			Cooldown = TimeSpan.FromMinutes(3),
		},

		FaunasRest = {
			Rank = 3,
			NoCombat = true,
			NoResetSwing = true,
			Action = {
				DisplayName = "Fauna's Rest",
				Icon = "tamer-faunas-rest",
				Enabled = true
			},
			Tooltip = "All pets are restored to full health and become invulnerable for 5 seconds.",
			MobileEffect = "FaunasRest",
			Cooldown = TimeSpan.FromMinutes(5),
		},

	},
}