MarketplaceBoard = {}

-- Default stockpile item donate amount
MarketplaceBoard.DefaultDonateAmount = 10

-- Formula to determine favor adjustments
MarketplaceBoard.DonateAdjust = function ( averageCount, stockCount )
    local percent = math.max((math.max(averageCount, 1) / math.max(stockCount, 1)), 0.1)
    return math.round( MarketplaceBoard.DefaultDonateAmount * percent ), math.round(percent * 100)
end

-- Purchase Adjust
MarketplaceBoard.PurchaseAdjust = function ( averageCount, stockCount )
    local amount,percent = MarketplaceBoard.DonateAdjust( averageCount, stockCount )
    return math.round(amount * 2)
end

-- Objvar to store faction currency
MarketplaceBoard.FactionCurrencyObjVar = 
{
    Eldeir = "eldeir_faction_currency",
    Helm = "helm_faction_currency",
    Pyros = "pyros_faction_currency"
}

-- World drops to sell
MarketplaceBoard.WorldDropList = 
{
    ingredient_frayed_scroll = { DisplayName = "Scribe: Frayed Scroll" },
    ingredient_fine_scroll = { DisplayName = "Scribe: Fine Scroll" },
    ingredient_ancient_scroll = { DisplayName = "Scribe: Ancient Scroll" },
    
    animalparts_bone = { DisplayName = "Alchemy: Bones" },
    animalparts_bone_cursed = { DisplayName = "Alchemy: Cursed Bones" },
    animalparts_bone_ethereal = { DisplayName = "Alchemy: Ethereal Bones" },

    necklace_ruby_imperfect     = { DisplayName = " Imperfect Ruby Necklace" },
    necklace_sapphire_imperfect = { DisplayName = " Imperfect Sapphire Necklace" },
    necklace_topaz_imperfect    = { DisplayName = " Imperfect Topaz Necklace" },
    ring_sapphire_imperfect     = { DisplayName = " Imperfect Sapphire Ring" },
    ring_ruby_imperfect         = { DisplayName = " Imperfect Ruby Ring" },
    ring_topaz_imperfect        = { DisplayName = " Imperfect Topaz Ring" },
    necklace_ruby_perfect       = { DisplayName = " Perfect Ruby Necklace" },
    necklace_sapphire_perfect   = { DisplayName = " Perfect Sapphire Necklace" },
    necklace_topaz_perfect      = { DisplayName = " Perfect Topaz Necklace" },
    ring_sapphire_perfect       = { DisplayName = " Perfect Sapphire Ring" },
    ring_ruby_perfect           = { DisplayName = " Perfect Ruby Ring" },
    ring_topaz_perfect          = { DisplayName = " Perfect Topaz Ring" },

    attuned_master_wand = { DisplayName = "Attuned: Master's Wand" },
    attuned_novice_wand = { DisplayName = "Attuned: Novice's Wand" },
    attuned_apprentice_wand = { DisplayName = "Attuned: Apprentices's Wand" },
    attuned_staff = { DisplayName = "Attuned: Iron Staff" },
    attuned_pearl_staff = { DisplayName = "Attuned: Pearl Staff" },
    attuned_white_staff = { DisplayName = "Attuned: White Staff" },

    animalparts_eye = { DisplayName = "Alchemy: Eye" },
    animalparts_eye_sickly = { DisplayName = "Alchemy: Sickly Eye" },
    animalparts_eye_decrepid = { DisplayName = "Alchemy: Decrepid Eye" },

    animalparts_blood = { DisplayName = "Alchemy: Blood" },
    animalparts_blood_beast = { DisplayName = "Alchemy: Beast Blood" },
    animalparts_blood_vile = { DisplayName = "Alchemy: Vile Blood" },

    seed_buttonmushroom = { DisplayName = "Seed: Button Mushroom" },
    seed_carrot = { DisplayName = "Seed: Carrot" },
    seed_corn = { DisplayName = "Seed: Corn" },
    seed_cucumber = { DisplayName = "Seed: Cucumber" },
    seed_eggplant = { DisplayName = "Seed: Eggplant" },
    seed_greenleaflettuce = { DisplayName = "Seed: Green-leaf Lettuce" },
    seed_greenpepper = { DisplayName = "Seed: Green Pepper" },
    seed_melon = { DisplayName = "Seed: Melon" },
    seed_onion = { DisplayName = "Seed: Onion" },
    seed_redleaflettuce = { DisplayName = "Seed: Red-leaf Lettuce" },
    seed_squash = { DisplayName = "Seed: Squash" },
    seed_strawberry = { DisplayName = "Seed: Strawberry" },
    
}

-- Stack counts for items to donate
MarketplaceBoard.StockStackCountsToDonate = 
{
    animalparts_eye                     = 5,
    animalparts_eye_sickly              = 5,
    animalparts_eye_decrepid            = 5,
    animalparts_blood                   = 5,
    animalparts_blood_beast             = 5,
    animalparts_blood_vile              = 5,

    ingredient_frayed_scroll            = 5,
    ingredient_fine_scroll              = 5,
    ingredient_ancient_scroll           = 5,

    animalparts_bone                    = 5,
    animalparts_bone_cursed             = 5,
    animalparts_bone_ethereal           = 5,

    resource_cobalt_ore					= 20,
    resource_gold_ore                	= 20,
    resource_copper_ore              	= 20,
    resource_obsidian_ore            	= 20,
    resource_iron_ore                	= 20,

    resource_leather                 	= 20,
    resource_vile_leather            	= 20,
    resource_silk_cloth              	= 20,
    resource_beast_leather           	= 20,
    resource_bolt_of_cloth           	= 20,
    resource_bolt_of_cloth_quilted   	= 20,
    
    resource_iron                    	= 20,
    resource_gold                    	= 20,
    resource_copper                  	= 20,
    resource_cobalt                  	= 20,
    resource_obsidian                	= 20,
    
    animalparts_spider_silk          	= 20,
    resource_cotton_fluffy           	= 20,
    animalparts_leather_hide         	= 20,
    animalparts_beast_leather_hide   	= 20,
    animalparts_vile_leather_hide    	= 20,
    resource_cotton                  	= 20,
    
    resource_boards_ash              	= 20,
    resource_brick                   	= 20,
    resource_boards                  	= 20,
    resource_blightwood_boards       	= 20,
    resource_stone                   	= 20,
    resource_ash                     	= 20,
    resource_blightwood              	= 20,
    resource_wood                    	= 20,
    
    arrow	                         	= 20, 
    arrow_blightwood                 	= 20, 
    arrow_ash                        	= 20, 

    seed_buttonmushroom 	            = 5,
    seed_carrot 			            = 5,
    seed_corn 				            = 5,
    seed_cucumber 			            = 5,
    seed_eggplant 			            = 5,
    seed_greenleaflettuce 	            = 5,
    seed_greenpepper 		            = 5,
    seed_melon 				            = 5,
    seed_onion 				            = 5,
    seed_redleaflettuce 	            = 5,
    seed_squash 			            = 5,
    seed_strawberry 		            = 5,
}

-- Purchase conversion list
-- When purchasing items from a faction marketplace the items are converted to their faction versions
MarketplaceBoard.FactionPurchaseConversionList = 
{
      armor_leather_chest = { Eldeir = "ebris_militia_armor_leather_chest", Pyros = "pyros_militia_armor_leather_chest", Helm = "tethys_militia_armor_leather_chest",  }
    , armor_leather_helm = { Eldeir = "ebris_militia_armor_leather_helm", Pyros = "pyros_militia_armor_leather_helm", Helm = "tethys_militia_armor_leather_helm",  }
    , weapon_hammer = { Eldeir = "ebris_militia_weapon_hammer", Pyros = "pyros_militia_weapon_hammer", Helm = "tethys_militia_weapon_hammer",  }
    , weapon_katana = { Eldeir = "ebris_militia_weapon_katana", Pyros = "pyros_militia_weapon_katana", Helm = "tethys_militia_weapon_katana",  }
    , weapon_war_mace = { Eldeir = "ebris_militia_weapon_war_mace", Pyros = "pyros_militia_weapon_war_mace", Helm = "tethys_militia_weapon_war_mace",  }
    , shield_buckler = { Eldeir = "ebris_militia_shield_buckler", Pyros = "pyros_militia_shield_buckler", Helm = "tethys_militia_shield_buckler",  }
    , weapon_spear = { Eldeir = "ebris_militia_weapon_spear", Pyros = "pyros_militia_weapon_spear", Helm = "tethys_militia_weapon_spear",  }
    , shield_smallshield = { Eldeir = "ebris_militia_shield_smallshield", Pyros = "pyros_militia_shield_smallshield", Helm = "tethys_militia_shield_smallshield",  }
    , robe_linen_tunic = { Eldeir = "ebris_militia_robe_linen_tunic", Pyros = "pyros_militia_robe_linen_tunic", Helm = "tethys_militia_robe_linen_tunic",  }
    , weapon_warhammer = { Eldeir = "ebris_militia_weapon_warhammer", Pyros = "pyros_militia_weapon_warhammer", Helm = "tethys_militia_weapon_warhammer",  }
    , armor_leather_leggings = { Eldeir = "ebris_militia_armor_leather_leggings", Pyros = "pyros_militia_armor_leather_leggings", Helm = "tethys_militia_armor_leather_leggings",  }
    , armor_hardened_helm = { Eldeir = "ebris_militia_armor_hardened_helm", Pyros = "pyros_militia_armor_hardened_helm", Helm = "tethys_militia_armor_hardened_helm",  }
    , weapon_voulge = { Eldeir = "ebris_militia_weapon_voulge", Pyros = "pyros_militia_weapon_voulge", Helm = "tethys_militia_weapon_voulge",  }
    , weapon_largeaxe = { Eldeir = "ebris_militia_weapon_largeaxe", Pyros = "pyros_militia_weapon_largeaxe", Helm = "tethys_militia_weapon_largeaxe",  }
    , armor_scale_helm = { Eldeir = "ebris_militia_armor_scale_helm", Pyros = "pyros_militia_armor_scale_helm", Helm = "tethys_militia_armor_scale_helm",  }
    , weapon_saber = { Eldeir = "ebris_militia_weapon_saber", Pyros = "pyros_militia_weapon_saber", Helm = "tethys_militia_weapon_saber",  }
    , weapon_quarterstaff = { Eldeir = "ebris_militia_weapon_quarterstaff", Pyros = "pyros_militia_weapon_quarterstaff", Helm = "tethys_militia_weapon_quarterstaff",  }
    , armor_chain_leggings = { Eldeir = "ebris_militia_armor_chain_leggings", Pyros = "pyros_militia_armor_chain_leggings", Helm = "tethys_militia_armor_chain_leggings",  }
    , robe_robe = { Eldeir = "ebris_militia_robe_robe", Pyros = "pyros_militia_robe_robe", Helm = "tethys_militia_robe_robe",  }
    , weapon_poniard = { Eldeir = "ebris_militia_weapon_poniard", Pyros = "pyros_militia_weapon_poniard", Helm = "tethys_militia_weapon_poniard",  }
    , robe_padded_helm = { Eldeir = "ebris_militia_robe_padded_helm", Pyros = "pyros_militia_robe_padded_helm", Helm = "tethys_militia_robe_padded_helm",  }
    , weapon_shortbow = { Eldeir = "ebris_militia_weapon_shortbow", Pyros = "pyros_militia_weapon_shortbow", Helm = "tethys_militia_weapon_shortbow",  }
    , weapon_warfork = { Eldeir = "ebris_militia_weapon_warfork", Pyros = "pyros_militia_weapon_warfork", Helm = "tethys_militia_weapon_warfork",  }
    , armor_scale_leggings = { Eldeir = "ebris_militia_armor_scale_leggings", Pyros = "pyros_militia_armor_scale_leggings", Helm = "tethys_militia_armor_scale_leggings",  }
    , weapon_maul = { Eldeir = "ebris_militia_weapon_maul", Pyros = "pyros_militia_weapon_maul", Helm = "tethys_militia_weapon_maul",  }
    , weapon_mace = { Eldeir = "ebris_militia_weapon_mace", Pyros = "pyros_militia_weapon_mace", Helm = "tethys_militia_weapon_mace",  }
    , weapon_longsword = { Eldeir = "ebris_militia_weapon_longsword", Pyros = "pyros_militia_weapon_longsword", Helm = "tethys_militia_weapon_longsword",  }
    , weapon_dagger = { Eldeir = "ebris_militia_weapon_dagger", Pyros = "pyros_militia_weapon_dagger", Helm = "tethys_militia_weapon_dagger",  }
    , weapon_greataxe = { Eldeir = "ebris_militia_weapon_greataxe", Pyros = "pyros_militia_weapon_greataxe", Helm = "tethys_militia_weapon_greataxe",  }
    , weapon_bone_dagger = { Eldeir = "ebris_militia_weapon_bone_dagger", Pyros = "pyros_militia_weapon_bone_dagger", Helm = "tethys_militia_weapon_bone_dagger",  }
    , weapon_kryss = { Eldeir = "ebris_militia_weapon_kryss", Pyros = "pyros_militia_weapon_kryss", Helm = "tethys_militia_weapon_kryss",  }
    , armor_fullplate_leggings = { Eldeir = "ebris_militia_armor_fullplate_leggings", Pyros = "pyros_militia_armor_fullplate_leggings", Helm = "tethys_militia_armor_fullplate_leggings",  }
    , weapon_longbow = { Eldeir = "ebris_militia_weapon_longbow", Pyros = "pyros_militia_weapon_longbow", Helm = "tethys_militia_weapon_longbow",  }
    , weapon_broadsword = { Eldeir = "ebris_militia_weapon_broadsword", Pyros = "pyros_militia_weapon_broadsword", Helm = "tethys_militia_weapon_broadsword",  }
    , armor_hardened_chest = { Eldeir = "ebris_militia_armor_hardened_chest", Pyros = "pyros_militia_armor_hardened_chest", Helm = "tethys_militia_armor_hardened_chest",  }
    , armor_chain_tunic = { Eldeir = "ebris_militia_armor_chain_tunic", Pyros = "pyros_militia_armor_chain_tunic", Helm = "tethys_militia_armor_chain_tunic",  }
    , weapon_warbow = { Eldeir = "ebris_militia_weapon_warbow", Pyros = "pyros_militia_weapon_warbow", Helm = "tethys_militia_weapon_warbow",  }
    , shield_kite = { Eldeir = "ebris_militia_shield_kite", Pyros = "pyros_militia_shield_kite", Helm = "tethys_militia_shield_kite",  }
    , armor_scale_tunic = { Eldeir = "ebris_militia_armor_scale_tunic", Pyros = "pyros_militia_armor_scale_tunic", Helm = "tethys_militia_armor_scale_tunic",  }
    , armor_fullplate_tunic = { Eldeir = "ebris_militia_armor_fullplate_tunic", Pyros = "pyros_militia_armor_fullplate_tunic", Helm = "tethys_militia_armor_fullplate_tunic",  }
    , robe_padded_leggings = { Eldeir = "ebris_militia_robe_padded_leggings", Pyros = "pyros_militia_robe_padded_leggings", Helm = "tethys_militia_robe_padded_leggings",  }
    , armor_chain_helm = { Eldeir = "ebris_militia_armor_chain_helm", Pyros = "pyros_militia_armor_chain_helm", Helm = "tethys_militia_armor_chain_helm",  }
    , armor_hardened_leggings = { Eldeir = "ebris_militia_armor_hardened_leggings", Pyros = "pyros_militia_armor_hardened_leggings", Helm = "tethys_militia_armor_hardened_leggings",  }
    , weapon_halberd = { Eldeir = "ebris_militia_weapon_halberd", Pyros = "pyros_militia_weapon_halberd", Helm = "tethys_militia_weapon_halberd",  }
    , robe_padded_tunic = { Eldeir = "ebris_militia_robe_padded_tunic", Pyros = "pyros_militia_robe_padded_tunic", Helm = "tethys_militia_robe_padded_tunic",  }
    , robe_linen_leggings = { Eldeir = "ebris_militia_robe_linen_leggings", Pyros = "pyros_militia_robe_linen_leggings", Helm = "tethys_militia_robe_linen_leggings",  }
    , armor_fullplate_helm = { Eldeir = "ebris_militia_armor_fullplate_helm", Pyros = "pyros_militia_armor_fullplate_helm", Helm = "tethys_militia_armor_fullplate_helm",  }
    , robe_linen_helm = { Eldeir = "ebris_militia_robe_linen_helm", Pyros = "pyros_militia_robe_linen_helm", Helm = "tethys_militia_robe_linen_helm",  }
}

-- Enable/disable the donating of item subsets
MarketplaceBoard.AllowAllFabricationResources = true
MarketplaceBoard.AllowAllMetalsmithResources = true
MarketplaceBoard.AllowAllWoodsmithResources = true

MarketplaceBoard.AllowAllFabricationArmor = true

MarketplaceBoard.AllowAllMetalsmithArmor = true
MarketplaceBoard.AllowAllMetalsmithWeapons = true

MarketplaceBoard.AllowAllWoodsmithWeapons = true

MarketplaceBoard.AllowAllCraftedWeapons = true

-- List of template_id(s) that cannot be donated
MarketplaceBoard.DonateBlacklist = 
{
    
}