ResourceEffectData.PotionGreaterHeal = {
    MobileEffect = "PotionHeal",
    MobileEffectArgs = {
        --SCAN INCREASED FROM 200
        Amount = 250,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 200 health.",
    }
}

ResourceEffectData.PotionHeal = {
    MobileEffect = "PotionHeal",
    MobileEffectArgs = {
        --SCAN INCREASED FROM 100
        Amount = 150,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 100 health.",
    }
}

ResourceEffectData.PotionLesserHeal = {
    MobileEffect = "PotionHeal",
    MobileEffectArgs = {
        --SCAN INCREASED FROM 50
        Amount = 100,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 50 health.",
    }
}

ResourceEffectData.PotionGreaterStamina = {
    MobileEffect = "PotionStamina",
    MobileEffectArgs = {
        Amount = 60,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 60 stamina.",
    }
}

ResourceEffectData.PotionStamina = {
    MobileEffect = "PotionStamina",
    MobileEffectArgs = {
        Amount = 40,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 40 stamina.",
    }
}

ResourceEffectData.PotionLesserStamina = {
    MobileEffect = "PotionStamina",
    MobileEffectArgs = {
        Amount = 20,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 20 stamina.",
    }
}

ResourceEffectData.PotionGreaterMana = {
    MobileEffect = "PotionMana",
    MobileEffectArgs = {
        Amount = 80,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 80 mana.",
    }
}

ResourceEffectData.PotionMana = {
    MobileEffect = "PotionMana",
    MobileEffectArgs = {
        Amount = 40,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 40 mana.",
    }
}

ResourceEffectData.PotionLesserMana = {
    MobileEffect = "PotionMana",
    MobileEffectArgs = {
        Amount = 25,
    },
    SelfOnly = true,
    Tooltip = {
        "Instantly restores 25 mana.",
    }
}

ResourceEffectData.PotionCure = {
    MobileEffect = "PotionCure",
    SelfOnly = true,
    Tooltip = {
        "Instantly cure yourself of poison.",
    }
}

ResourceEffectData.PotionMend = {
    MobileEffect = "PotionMend",
    SelfOnly = true,
    Tooltip = {
        "Instantly mends mortal strikes and bleeds.",
    }
}

ResourceEffectData.PotionGrowing = {
    MobileEffect = "PotionGrowing",
    SelfOnly = true,
    Tooltip = {
        "Makes the whole world smaller!",
    }
}

ResourceEffectData.PotionShrinking = {
    MobileEffect = "PotionShrinking",
    SelfOnly = true,
    Tooltip = {
        "Makes the whole world bigger!",
    }
}

ResourceEffectData.PotionDetectEvil = {
    MobileEffect = "PotionDetectEvil",
    MobileEffectArgs = {
        Range = 70,
    },
    SelfOnly = true,
    Tooltip = {
        "Detects the presence of nearby evil.",
    }
}

ResourceEffectData.PotionEtherealize = {
    MobileEffect = "SummonMount",
    RequireMobileTarget = true,
    Tooltip = {
        "Instantly places your target onto a magically summoned mount. The mount will disappear when dismounted.",
    }
}

ResourceEffectData.PotionRegrowth = {
    MobileEffect = "PotionRegrowth",
    RequireMobileTarget = true,
    MobileEffectArgs = {
        Amount = math.abs( ServerSettings.Durability.Pets.OnDeath ),
    },
    Tooltip = {
        "Instantly removes some weariness from your pet.",
    }
}

ResourceEffectData.PotionEntSap = {
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "ElixirOfHolding",
    MobileEffectArgs = {
        Duration = TimeSpan.FromMinutes(30),
        Amount = 300,
        Name = "Entish Strength"
    },
    Tooltip = {
        "Increases your maximum carrying capacity by 300 for 30 minutes. Persists through death.",
    }
}

ResourceEffectData.PotionCovenTreats = {
    NoAutoTarget = true,
    SelfOnly = true,
    NoConsume = true,
    MobileEffect = "ItemExplosion",
    MobileEffectArgs = {
        Items = {
            { "ingredient_moss", 10, 1 },
            { "ingredient_lemongrass", 10, 1 },
            { "ingredient_mushroom", 10, 1 },
            { "ingredient_ginsengroot", 10, 1 },
            { "seed_cucumber", 1, 0.1 },
            { "seed_eggplant", 1, 0.1 },
            { "seed_greenpepper", 1, 0.1 },
            { "seed_melon", 2, 0.2 },
            { "seed_squash", 2, 0.2 },
            { "seed_corn", 2, 0.2 },
        },
        DoAnimation = true
    },
    Tooltip = {
        "Use and target a location. Causes a cornucopia of treats to appear around the location.",
    }
}

ResourceEffectData.PotionUnstableConcoction = {
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "AOEExplosion",
    MobileEffectArgs = {
        Radius = 2,
        Damage = 50,
        DoAnimation = true,
    },
    Tooltip = {
        "Use and target a location. Causes an explosion to occur damaging all enemies within a 2 meter radius.",
    }
}

--SCAN ADDED
ResourceEffectData.LesserExplosionPotion = {
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "AOEExplosion",
    MobileEffectArgs = {
        Radius = 1,
        Damage = 50,
        DoAnimation = true,
    },
    Tooltip = {
        "Use and target a location. Causes an explosion to occur damaging all enemies within a 1 meter radius.",
    }
}

--SCAN ADDED
ResourceEffectData.ExplosionPotion = {
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "AOEExplosion",
    MobileEffectArgs = {
        Radius = 2,
        Damage = 100,
        DoAnimation = true,
    },
    Tooltip = {
        "Use and target a location. Causes an explosion to occur damaging all enemies within a 2 meter radius.",
    }
}

--SCAN ADDED
ResourceEffectData.GreaterExplosionPotion = {
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "AOEExplosion",
    MobileEffectArgs = {
        Radius = 3,
        Damage = 200,
        DoAnimation = true,
    },
    Tooltip = {
        "Use and target a location. Causes an explosion to occur damaging all enemies within a 3 meter radius.",
    }
}

--SCAN ADDED
ResourceEffectData.LesserWeightPotion = {
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "ElixirOfHolding",
    MobileEffectArgs = {
        Duration = TimeSpan.FromMinutes(30),
        Amount = 50,
        Name = "Lesser Weight Potion"
    },
    Tooltip = {
        "Increases your maximum carrying capacity by 50 for 30 minutes. Persists through death.",
    }
}

--SCAN ADDED
ResourceEffectData.WeightPotion = {
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "ElixirOfHolding",
    MobileEffectArgs = {
        Duration = TimeSpan.FromMinutes(30),
        Amount = 75,
        Name = "Weight Potion"
    },
    Tooltip = {
        "Increases your maximum carrying capacity by 75 for 30 minutes. Persists through death.",
    }
}

--SCAN ADDED
ResourceEffectData.GreaterWeightPotion = {
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "ElixirOfHolding",
    MobileEffectArgs = {
        Duration = TimeSpan.FromMinutes(30),
        Amount = 100,
        Name = "Greater Weight Potion"
    },
    Tooltip = {
        "Increases your maximum carrying capacity by 100 for 30 minutes. Persists through death.",
    }
}

--SCAN ADDED
ResourceEffectData.SkillPotion = {
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "PowerHourBuff",
    MobileEffectArgs = {
        Duration = TimeSpan.FromMinutes(10),
        Amount = .2,
        Name = "Skil Potion"
    },
    Tooltip = {
        "Increases your chance to gain skill points when performing an action by 20%, for 10 minutes.",
    }
}

--SCAN ADDED
ResourceEffectData.PowerHourBuff = {
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "PowerHourBuff",
    MobileEffectArgs = {
        Duration = TimeSpan.FromMinutes(60),
        Amount = 2.0,
        Name = "Skil Potion"
    },
    Tooltip = {
        "Increases your chance to gain skill points when performing an action by 200%, for 60 minutes.",
    }
}