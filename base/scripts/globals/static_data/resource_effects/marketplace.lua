ResourceEffectData.MarketplaceBoard = {
    NoConsume = true,
    SelfOnly = true,
	NoAutoTarget = true,
	Range = 5,
    MobileEffect = "MarketplaceBoard",
    MobileEffectArgs = {},
    Tooltip = {
        "Used to sell and commission items.",
    }
}

ResourceEffectData.BazaarBoard = {
    NoConsume = true,
    SelfOnly = true,
	NoAutoTarget = true,
	Range = 5,
    MobileEffect = "BazaarBoard",
    MobileEffectArgs = {},
    Tooltip = {
        "Used to locate and purchase items from player merchants.",
    }
}