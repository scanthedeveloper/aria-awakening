-- Prestige quest books
for prestigeName,data in pairs(PrestigeData) do
    for i=2,#TaskIndexTitles do
        local resourceName = "Prestige"..TaskIndexTitles[i]..prestigeName
        
        ResourceEffectData[resourceName] = {
            SelfOnly = true,
            Tooltip = {"Quest Item: "..TaskIndexTitles[i].." "..prestigeName.." Profession"}
        }
    end
end

ResourceEffectData.MountStatue = {
    MobileEffect = "Mount",
    MobileEffectObjectAsTarget = true, -- pass the used object as the target.
    SelfOnly = true,
    NoConsume = true,
    Tooltip = {
        "Summon this mount.",
    },

    UseCases = {
        "Summon",
    },
    UseCaseConditions = { 
        "HasObject",
    }
}

ResourceEffectData.Shovel = {
    MobileEffect = "QueueWeaponAbility",
    MobileEffectObjectAsTarget = true, -- pass the used object as the target.
    NoConsume = true,
    SelfOnly = true,
    TargetMessage = "Where would you like to dig?",
    UseCases = {
        "Dig",
    },
    UseCaseConditions = { 
        "HasObject",
    },
    MobileEffectArgs = {
        Primary = true,
        WeaponAbility = "Dig",
    }
}

ResourceEffectData.MountSaddle = {
    MobileEffect = "Saddle",
    NoAutoTarget = true,
    NoConsume = true,
    MobileEffectObjectAsArgs = true,
    Tooltip = {
        "Creates mount from a tamed creature.\n\nCan be applied to: Horse, Llama, Great Bear",
    },

    UseCases = {
        "Unpack",
    },
    UseCaseConditions = { 
        "HasObject",
    }
}

ResourceEffectData.PetStatue = {
    MobileEffect = "PetSummon",
    MobileEffectObjectAsTarget = true, -- pass the used object as the target.
    SelfOnly = true,
    NoConsume = true,
    Tooltip = {
        "Summon this dismissed pet.",
    },

    UseCases = {
        "Summon",
    },
    UseCaseConditions = { 
        "HasObject",
    }
}

ResourceEffectData.Hearthstone = {
    MobileEffect = "Hearthstone",
    SelfOnly = true,
    NoConsume = true,
    NoDismount = true,
    Tooltip = {
        "Teleports you to your bind location.\n\nCooldown: "..ServerSettings.Misc.HearthstoneCooldown.TotalMinutes.." minutes",
    },

    UseCases = {
        "Activate",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.CursedHearthstone = {
    MobileEffect = "Hearthstone",
    SelfOnly = true,
    NoConsume = false,
    NoDismount = true,
    Tooltip = {
        "Teleports you to your bind location.\n\nCooldown: "..ServerSettings.Misc.CursedHearthstoneCooldown.TotalMinutes.." minutes",
    },

    UseCases = {
        "Activate",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.RapidEscapeScroll = {
    MobileEffect = "EscapeScroll",
    MobileEffectArgs = {
    },
    SelfOnly = true,
    NoConsume = true,
    NoDismount = true,
    Tooltip = {
        "Teleports you to the nearest escape location.",
    },

    UseCases = {
        "Recite",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.BlessDeed = {
    NoDismount = true,
    NoAutoTarget = true,
    MobileEffect = "Bless",
    TargetMessage = "Select the item you wish to bless.",
    Tooltip = {
        "Bless one item of clothing. Blessed clothing stays with you upon death.",
    }
}

ResourceEffectData.GodlyWeaponScroll = {
    NoDismount = true,
    NoAutoTarget = true,
    NoConsume = true,
    MobileEffect = "GodlyWeaponScroll",
    TargetMessage = "Select the weapon or shield you wish to make godly.",
    Tooltip = {
        "\n[FF0000]This item has lost it's magical properties[-]",
    }
}

ResourceEffectData.ArtisanCrafterComission = {
    NoDismount = true,
    NoAutoTarget = true,
    Tooltip = {
        "Given ",
    }
}

ResourceEffectData.ElixirOfHolding = {
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "ElixirOfHolding",

    MobileEffectArgs = {
        Duration = TimeSpan.FromMinutes(30),
        Amount = 200,
    },

    Tooltip = {},
    TooltipFunc = function(tooltipInfo, item)
        tooltipInfo["description"] = {
            TooltipString = "Increase the maximum weight you can carry.",
            Priority = 3,
        }
        tooltipInfo["amount"] = {
            TooltipString = "Amount: "..ResourceEffectData.ElixirOfHolding.MobileEffectArgs.Amount,
            Priority = 2,
        }
        tooltipInfo["duration"] = {
            TooltipString = "Duration: "..TimeSpanToWords(ResourceEffectData.ElixirOfHolding.MobileEffectArgs.Duration),
            Priority = 1,
        }
        return tooltipInfo
    end,
}

ResourceEffectData.FlaskOfMending = {
    MobileEffect = "FlaskOfMending",
    SelfOnly = true,
    HasUsesRemainingCounter = true,
    NoConsume = true,
    UseCases = {
        "Drink",
        "Fill",
    },
    MobileEffectUseCases = {
        Drink = {
            MobileEffect = "FlaskOfMending",
        },
        Fill = {
            MobileEffect = "RefillFlaskOfMending",
        },
    },
    Tooltip = {},
    TooltipFunc = function(tooltipInfo, item)
        tooltipInfo["description"] = {
            TooltipString = "A strange flask that seems to double the liquid poured inside.",
            Priority = 3,
        }
        tooltipInfo["refills"] = {
            TooltipString = "Refills: "..item:GetObjVar("Recharges") or 0,
            Priority = 2,
        }
        tooltipInfo["sips"] = {
            TooltipString = "Sips: "..item:GetObjVar("Charges") or 0,
            Priority = 1,
        }
        return tooltipInfo
    end,
}

ResourceEffectData.AllegianceSalt = {
    NoConsume = true,
    SelfOnly = true,
    Tooltip = {
        "Used as currency by Militia fighters and sought after by Belhaven merchants."
    }
}

ResourceEffectData.AllegianceIchor = {
    NoConsume = true,
    SelfOnly = true,
    Tooltip = {
        "Used as currency by Militia fighters and sought after by Belhaven merchants."
    }
}

ResourceEffectData.CurseScroll = {
    NoDismount = true,
    NoAutoTarget = true,
    MobileEffect = "Curse",
    TargetMessage = "Select the item you wish to curse.",
    Tooltip = {
        "Curse one equippable item. A cursed item stays with you upon death.",
    }
}

ResourceEffectData.Lockpick = {
    NoConsume = true,
    NoAutoTarget = true,
    MobileEffect = "Lockpick",
    MobileEffectObjectAsArgs = true, -- pass the used object as the arg, negates MobileEffectArgs parameter.
    Tooltip = {
        "Used to pick the lock on chests."
    }
}

ResourceEffectData.WaterContainer = {    
    SelfOnly = true,
    NoConsume = true,
    Tooltip = {
        "Once filled, can be used to water plants.",
    },

    MobileEffect = "FillWaterContainer",

    InitFunc = function(item) UpdateWaterContainerState(item) end,
}

ResourceEffectData.LotteryBox = {
    NoDismount = true,
    NoConsume = true,
    SelfOnly = true,
    MobileEffect = "LotteryBox",
}

ResourceEffectData.LotteryBoxAllegiance = {
    NoDismount = true,
    NoConsume = true,
    SelfOnly = true,
    MobileEffect = "LotteryBox",
}

ResourceEffectData.GuildCharter = {
    NoDismount = true,
    SelfOnly = true,
    NoConsume = true,
    MobileEffect = "GuildCharter",
    Tooltip = {
        "Used to form a guild.",
    },

    UseCases = {
        "Edit",
    }
}

ResourceEffectData.Runebook = {
    NoDismount = true,
    SelfOnly = true,
    NoConsume = true,
    MobileEffect = "Runebook",
    Range = 5,

    HasSecuritySettings = true,
    
    UseCases = {
        "Open",
        "Security",
        "Rename Runebook",
    }
}

ResourceEffectData.CriminalHead = {
    NoDismount = true,
    SelfOnly = true,
    NoConsume = true,
    MobileEffect = "CriminalHead",
    MobileEffectObjectAsTarget = true,
    TooltipFunc = function(tooltipInfo, item)
        local criminalSkills = item:GetObjVar("CriminalSkillData")
        if ( criminalSkills ~= nil ) then
            for i=1,#criminalSkills do
                tooltipInfo[criminalSkills[i][1]] = {
                    TooltipString = string.format("+%s %s up to %s", math.round(criminalSkills[i][2], 1), GetSkillDisplayName(criminalSkills[i][1]), math.round(criminalSkills[i][3], 1)),
                    Priority = -i,
                }
            end
        end
        return tooltipInfo
    end,
}

ResourceEffectData.RottenCriminalHead = {
    NoDismount = true,
    SelfOnly = true,
    NoConsume = true,
    MobileEffect = nil,
    MobileEffectObjectAsTarget = nil,
    TooltipFunc = function(tooltipInfo, item)
        tooltipInfo["Rotten"] = {
            TooltipString = "\nRotten\n",
            Priority = 9999
        }
        return ResourceEffectData.CriminalHead.TooltipFunc(tooltipInfo, item)
    end,
}

ResourceEffectData.SkillScroll = {
    SelfOnly = true,
    NoConsume = true, -- mobile effect does the consume
    MobileEffect = "SkillScroll",
    MobileEffectObjectAsTarget = true,
    TooltipFunc = function(tooltipInfo, item)
        local skillInfo = item:GetObjVar("SkillScrollInfo")
        if ( skillInfo ~= nil ) then
            local mastery,amount,skillName = skillInfo[1],skillInfo[2],skillInfo[3]
            tooltipInfo["SkillScrollType"] = {
                TooltipString = "\n" .. mastery .. "\n",
                Priority = 9999
            }
            tooltipInfo["SkillInfo"] = {
                TooltipString = string.format("+%s %s up to %s", amount, GetSkillDisplayName(skillName), SkillScroll.GetMasteryMax(mastery)),
                Priority = 9998
            }
        end
        return tooltipInfo
    end,
}

--[[ LEAGUE CONSUMABLES ]]

ResourceEffectData.PotionOfLight = {
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "MobileModConsumable",

    MobileEffectArgs = {
        Name = "Potion of Light",
        Duration = TimeSpan.FromMinutes(30),
        MobileMods = {
            { MobileMod = "DemonDamageReductionTimes", Amount = 25 },
        },
        BuffIcon = "Summon Raven 05",
        BuffText = "Damaged from Demons decreased by 25%.",
        FadeMessage = "Your resistance to Demons fades.",
    },

    Tooltip = {},
    TooltipFunc = function(tooltipInfo, item)
        tooltipInfo["description"] = {
            TooltipString = "Decreases damage taken from Demons by 25%. Persists through death.",
            Priority = 2,
        }
        tooltipInfo["duration"] = {
            TooltipString = "Duration: "..TimeSpanToWords(ResourceEffectData.PotionOfLight.MobileEffectArgs.Duration),
            Priority = 1,
        }
        return tooltipInfo
    end,
}

ResourceEffectData.PotionofLiving = {
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    MobileEffect = "MobileModConsumable",

    MobileEffectArgs = {
        Name = "Potion of Living",
        Duration = TimeSpan.FromMinutes(30),
        MobileMods = {
            { MobileMod = "UndeadDamageReductionTimes", Amount = 25 },
        },
        BuffIcon = "Summon Raven 05",
        BuffText = "Damaged from Undead decreased by 25%.",
        FadeMessage = "Your resistance to Undead fades.",
    },

    Tooltip = {},
    TooltipFunc = function(tooltipInfo, item)
        tooltipInfo["description"] = {
            TooltipString = "Decreases damage taken from Undead by 25%. Persists through death.",
            Priority = 2,
        }
        tooltipInfo["duration"] = {
            TooltipString = "Duration: "..TimeSpanToWords(ResourceEffectData.PotionofLiving.MobileEffectArgs.Duration),
            Priority = 1,
        }
        return tooltipInfo
    end,
}

ResourceEffectData.DungeoneerCorruptionTitleScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "DungeoneersCorruption",
        Achievement = "Demonslayer"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'Demonslayer' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.DungeoneerRuinTitleScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "DungeoneersRuin",
        Achievement = "Undead Slayer"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'Undead Slayer' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.DungeoneerTundraTitleScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "DungeoneersTundra",
        Achievement = "The Cold-Hearted"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Cold-Hearted' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ExplorersSeasonOneTitleScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ExplorersSeasonOne",
        Achievement = "Mercenary"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'Mercenary' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ExplorersSeasonTwoTitleScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ExplorersSeasonTwo",
        Achievement = "Tactician"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'Tactician' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ExplorersSeasonThreeTitleScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ExplorersSeasonThree",
        Achievement = "The Pioneer"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Pioneer' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ArtisansSeasonOneTitleScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ArtisansSeasonOne",
        Achievement = "Outfitter"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'Outfitter' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ArtisansSeasonTwoTitleScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ArtisansSeasonTwo",
        Achievement = "Farrier"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'Farrier' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ArtisansSeasonThreeTitleScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ArtisansSeasonThree",
        Achievement = "The Mason"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Mason' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ProcurersSeasonOneTitleOneScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ProcurersSeasonOneTitleOne",
        Achievement = "The Merciless"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Merciless' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ProcurersSeasonOneTitleTwoScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ProcurersSeasonOneTitleTwo",
        Achievement = "The Valorous"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Valorous' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ProcurersSeasonOneTitleThreeScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ProcurersSeasonOneTitleThree",
        Achievement = "The Cunning"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Cunning' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ProcurersSeasonTwoTitleOneScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ProcurersSeasonTwoTitleOne",
        Achievement = "The Bloodletter"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Bloodletter' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ProcurersSeasonTwoTitleTwoScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ProcurersSeasonTwoTitleTwo",
        Achievement = "The Bargainer"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Bargainer' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ProcurersSeasonTwoTitleThreeScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ProcurersSeasonTwoTitleThree",
        Achievement = "The Betrayer"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Betrayer' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ProcurersSeasonThreeTitleOneScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ProcurersSeasonThreeTitleOne",
        Achievement = "The Heretic"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Heretic' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ProcurersSeasonThreeTitleTwoScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ProcurersSeasonThreeTitleTwo",
        Achievement = "The Broken"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Broken' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ProcurersSeasonThreeTitleThreeScroll = {
    MobileEffect = "AddAchievement",
    MobileEffectArgs = {
        Category = "Leagues",
        Type = "ProcurersSeasonThreeTitleThree",
        Achievement = "The Humble"
    },
    NoDismount = true,
    NoAutoTarget = true,
    SelfOnly = true,
    Tooltip = {
        "Grants 'The Humble' title.",
    },

    UseCases = {
        "Consume",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ArtifactSchematicLesser = {
    MobileEffect = "ArtifactSchematicLesser",
    SelfOnly = true,
    NoConsume = false,
    NoDismount = true,
    Tooltip = {
        "Consume to gain the ability to craft artifact weapons, armor, and shields for 1 minutes.",
    },

    UseCases = {
        "Craft",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ArtifactSchematic = {
    MobileEffect = "ArtifactSchematic",
    SelfOnly = true,
    NoConsume = false,
    NoDismount = true,
    Tooltip = {
        "Consume to gain the ability to craft artifact weapons, armor, and shields for 1 minutes.",
    },

    UseCases = {
        "Craft",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.ArtifactSchematicGreater = {
    MobileEffect = "ArtifactSchematicGreater",
    SelfOnly = true,
    NoConsume = false,
    NoDismount = true,
    Tooltip = {
        "Consume to gain the ability to craft artifact weapons, armor, and shields for 1 minutes.",
    },

    UseCases = {
        "Craft",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.SorcererOrb = {
    MobileEffect = "SorceryAbilities",
    SelfOnly = true,
    NoConsume = true,
    NoDismount = true,
    CanBeUsedEquipped = true,
    Tooltip = {
        "Activate to access Sorcery abilities.",
    },
    UseCases = {
        "Activate",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.EchoSchematic = {
    MobileEffect = "MonolithSchematic",
    SelfOnly = true,
    --NoConsume = true,
    NoDismount = true,
    Tooltip = {
        "Use to craft a piece of Echo Armor.",
    },
    UseCases = {
        "Craft",
    },
    MobileEffectUseCases = {}
}

ResourceEffectData.EchoSchematicLeatherChest = ResourceEffectData.EchoSchematic
ResourceEffectData.EchoSchematicLeatherLegs = ResourceEffectData.EchoSchematic
ResourceEffectData.EchoSchematicLeatherHelm = ResourceEffectData.EchoSchematic
ResourceEffectData.EchoSchematicPlateChest = ResourceEffectData.EchoSchematic
ResourceEffectData.EchoSchematicPlateLegs = ResourceEffectData.EchoSchematic
ResourceEffectData.EchoSchematicPlateHelm = ResourceEffectData.EchoSchematic
ResourceEffectData.EchoSchematicClothChest = ResourceEffectData.EchoSchematic
ResourceEffectData.EchoSchematicClothHelm = ResourceEffectData.EchoSchematic

ResourceEffectData.EchoShard = {
    MobileEffect = nil,
    SelfOnly = true,
    NoConsume = true,
    NoDismount = true,
    Tooltip = {
        "A shard fused with Echo energy.",
    },
    UseCases = {
    },
    MobileEffectUseCases = {}
}

-- Quest Items
ResourceEffectData.CrimpedNote = {
    NoDismount = true,
    SelfOnly = true,
    NoConsume = true,
    Range = 5,
    MobileEffect = "QuestEvent",
    MobileEffectArgs = {
        EventType = "Profession",
        EventVars = {"CrimpedNote"},
        EventCount = 1,
    },
    Tooltip = {
        "This note looks old. It most likely isn't worth your time.",
    }
}