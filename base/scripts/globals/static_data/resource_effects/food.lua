ResourceEffectData.MushroomsPoison = {
    MobileEffect = "Poison",
    MobileEffectArgs = {
        PulseMax = 12,
        PulseFrequency = TimeSpan.FromSeconds(2),
    },
    SelfOnly = true,
    Tooltip = {
        "Will make you sick.",
    },
}

for key,value in pairs(FoodStats.BaseFoodStats) do
    if ( ResourceEffectData[key] == nil ) then
        ResourceEffectData[key] = {
            MobileEffect = "Food",
            SelfOnly = true,
        }
    end
end

--- TEST ITEMS

--useful for stunning yourself when testing.
ResourceEffectData.StunBread = {
    SelfOnly = true,
    Tooltip = {
        "Will stun you.",
    },
    -- default stun is short.
    MobileEffect = "Stun",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(1),
    },
    UseCases = {
        "1 Second Stun", -- first one is always default interaction
        "5 Second Stun",
        "10 Second Stun",
    },
    MobileEffectUseCases = {}
}
-- continuation of StunBread
ResourceEffectData.StunBread.MobileEffectUseCases["5 Second Stun"] = {
    MobileEffect = "Stun",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(5),
    },
}
ResourceEffectData.StunBread.MobileEffectUseCases["10 Second Stun"] = {
    MobileEffect = "Stun",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(10),
    },
}

ResourceEffectData.JusticeBread = {
    MobileEffect = "SwiftJustice",
    MobileEffectArgs = {
        Duration = TimeSpan.FromSeconds(15),
    },
    SelfOnly = true,
    Tooltip = {
        "The Justice of the Guardian Order is swift.",
    },
}


-- HERE WE GO!!
ResourceEffectData.EggplantBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 25 }
	},
}

ResourceEffectData.EggplantGreenLeafLettuceBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 25 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 25 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.EggplantFishFilletBarrelBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.EggplantMysteryMeatBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.EggplantFishFilletTeroBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.EggplantRedLeafLettuceBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 25 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 25 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.EggplantStringyMeatBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.EggplantOnionBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 25 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 25 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.EggplantCarrotBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 25 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 25 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletSpottedTeroMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 }
	},
}

ResourceEffectData.FishFilletSpottedTeroEggplantMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletSpottedTeroGreenLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletSpottedTeroCornMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletSpottedTeroSquashMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletSpottedTeroStrawberryMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletSpottedTeroCucumberMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletSpottedTeroRedLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletSpottedTeroMelonMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletSpottedTeroButtonMushroomMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletSpottedTeroOnionMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.FishFilletSpottedTeroCarrotMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarEggplantMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarGreenLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarCornMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarSquashMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarStrawberryMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarCucumberMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarRedLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarMelonMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarButtonMushroomMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarOnionMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.FishFilletFourEyedSalarCarrotMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.GreenLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.GreenLeafLettuceFishFilletBarrelMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.GreenLeafLettuceStrawberryMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.GreenLeafLettuceMysteryMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.GreenLeafLettuceFishFilletTeroMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.GreenLeafLettuceStringyMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.GreenLeafLettuceOnionMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.TenderMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 }
	},
}

ResourceEffectData.TenderMeatEggplantMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 25 }
	},
}

ResourceEffectData.TenderMeatGreenLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.TenderMeatCornMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.TenderMeatSquashMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.TenderMeatStrawberryMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.TenderMeatCucumberMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 25 }
	},
}

ResourceEffectData.TenderMeatRedLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.TenderMeatMelonMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.TenderMeatButtonMushroomMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.TenderMeatOnionMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.TenderMeatCarrotMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.GourmetMeatBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 }
	},
}

ResourceEffectData.GourmetMeatEggplantBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 25 }
	},
}

ResourceEffectData.GourmetMeatGreenLeafLettuceBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.GourmetMeatCornBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.GourmetMeatSquashBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.GourmetMeatStrawberryBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.GourmetMeatCucumberBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 25 }
	},
}

ResourceEffectData.GourmetMeatRedLeafLettuceBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.GourmetMeatMelonBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.GourmetMeatButtonMushroomBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.GourmetMeatOnionBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.GourmetMeatCarrotBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.CornMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.CornGreenLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.CornFishFilletBarrelMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.CornSquashMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.CornStrawberryMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.CornMysteryMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.CornFishFilletTeroMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.CornRedLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.CornMelonMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.CornStringyMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.CornCarrotMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletBarrelSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.FishFilletBarrelStrawberrySnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletBarrelCarrotSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.SquashMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.SquashFishFilletBarrelMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.SquashMysteryMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.SquashFishFilletTeroMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.SquashStringyMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.SquashOnionMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.SquashCarrotMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletRazorBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 }
	},
}

ResourceEffectData.FishFilletRazorEggplantBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletRazorGreenLeafLettuceBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletRazorCornBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletRazorSquashBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletRazorStrawberryBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletRazorCucumberBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletRazorRedLeafLettuceBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletRazorMelonBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletRazorButtonMushroomBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletRazorOnionBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.FishFilletRazorCarrotBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.StrawberrySnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletGoldenAetherBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 }
	},
}

ResourceEffectData.FishFilletGoldenAetherEggplantBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletGoldenAetherGreenLeafLettuceBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletGoldenAetherCornBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletGoldenAetherSquashBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletGoldenAetherStrawberryBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletGoldenAetherCucumberBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletGoldenAetherRedLeafLettuceBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletGoldenAetherMelonBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.FishFilletGoldenAetherButtonMushroomBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.FishFilletGoldenAetherOnionBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.FishFilletGoldenAetherCarrotBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 45 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 45 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.MysteryMeatSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.MysteryMeatStrawberrySnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.MysteryMeatCarrotSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletTeroSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.FishFilletTeroStrawberrySnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.FishFilletTeroCarrotSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.CucumberBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 25 }
	},
}

ResourceEffectData.CucumberFishFilletBarrelBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.CucumberSquashBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 25 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 25 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.CucumberStrawberryBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 25 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 25 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.CucumberMysteryMeatBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.CucumberFishFilletTeroBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.CucumberMelonBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 25 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 25 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.CucumberStringyMeatBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.CucumberOnionBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 25 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 25 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.RedLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.RedLeafLettuceFishFilletBarrelMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.RedLeafLettuceStrawberryMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.RedLeafLettuceMysteryMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.RedLeafLettuceFishFilletTeroMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.RedLeafLettuceStringyMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.RedLeafLettuceOnionMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 16 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 16 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.MelonMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.MelonFishFilletBarrelMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.MelonMysteryMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.MelonFishFilletTeroMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.MelonStringyMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.MelonOnionMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.MelonCarrotMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your mana by 16 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "mana", buffAmount = 16 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.ToughMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 }
	},
}

ResourceEffectData.ToughMeatEggplantMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 25 }
	},
}

ResourceEffectData.ToughMeatGreenLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.ToughMeatCornMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.ToughMeatSquashMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.ToughMeatStrawberryMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.ToughMeatCucumberMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 25 }
	},
}

ResourceEffectData.ToughMeatRedLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.ToughMeatMelonMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.ToughMeatButtonMushroomMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.ToughMeatOnionMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.ToughMeatCarrotMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 30 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 30 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.ButtonMushroomMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 }
	},
}

ResourceEffectData.ButtonMushroomGreenLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.ButtonMushroomFishFilletBarrelMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.ButtonMushroomSquashMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.ButtonMushroomStrawberryMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.ButtonMushroomMysteryMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.ButtonMushroomFishFilletTeroMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.ButtonMushroomRedLeafLettuceMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.ButtonMushroomMelonMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.ButtonMushroomStringyMeatMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.ButtonMushroomCarrotMeal = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 25 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 25 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.StringyMeatSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.StringyMeatStrawberrySnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.StringyMeatCarrotSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your health by 15 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "health", buffAmount = 15 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.OnionSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 15 }
	},
}

ResourceEffectData.OnionStrawberrySnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 15 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 15 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.OnionCarrotSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 15 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 15 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.CarrotSnack = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "stamina", buffAmount = 8 }
	},
}

ResourceEffectData.GreenPepperBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 }
	},
}

ResourceEffectData.GreenPepperEggplantBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and mana by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "mana", buffAmount = 25 }
	},
}

ResourceEffectData.GreenPepperFishFilletSpottedTeroBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 30 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 30 }
	},
}

ResourceEffectData.GreenPepperFishFilletFourEyedSalarBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 30 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 30 }
	},
}

ResourceEffectData.GreenPepperGreenLeafLettuceBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.GreenPepperTenderMeatBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 30 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 30 }
	},
}

ResourceEffectData.GreenPepperGourmetMeatBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 45 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 45 }
	},
}

ResourceEffectData.GreenPepperFishFilletBarrelBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.GreenPepperSquashBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.GreenPepperFishFilletRazorBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 45 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 45 }
	},
}

ResourceEffectData.GreenPepperStrawberryBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and mana by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "mana", buffAmount = 8 }
	},
}

ResourceEffectData.GreenPepperFishFilletGoldenAetherBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 45 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 45 }
	},
}

ResourceEffectData.GreenPepperMysteryMeatBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.GreenPepperFishFilletTeroBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.GreenPepperCucumberBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and stamina by 25 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "stamina", buffAmount = 25 }
	},
}

ResourceEffectData.GreenPepperRedLeafLettuceBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and stamina by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "stamina", buffAmount = 16 }
	},
}

ResourceEffectData.GreenPepperMelonBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and mana by 16 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "mana", buffAmount = 16 }
	},
}

ResourceEffectData.GreenPepperToughMeatBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 30 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 30 }
	},
}

ResourceEffectData.GreenPepperStringyMeatBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and health by 15 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "health", buffAmount = 15 }
	},
}

ResourceEffectData.GreenPepperCarrotBanquet = {
	SelfOnly = true,
	Tooltip = {
		"Eat to increase your weight by 50 and stamina by 8 points.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "weight", buffAmount = 50 },{ buffStat = "stamina", buffAmount = 8 }
	},
}

--SCAN ADDED
ResourceEffectData.EasterEggRed = {
	SelfOnly = true,
	Tooltip = {
		"Gain +25 bonus hit points and regenerate 1.5 hit points per second, for 15 minutes.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "HealthRegen", buffAmount = 1.5 },{ buffStat = "health", buffAmount = 25 }
	},
}

--SCAN ADDED
ResourceEffectData.EasterEggBlue= {
	SelfOnly = true,
	Tooltip = {
		"Gain +25 bonus mana points and regenerate 1.5 mana points per second, for 15 minutes.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "ManaRegen", buffAmount = 1.5 },{ buffStat = "mana", buffAmount = 25 }
	},
}

--SCAN ADDED
ResourceEffectData.EasterEggYellow= {
	SelfOnly = true,
	Tooltip = {
		"Gain +25 bonus stamina points and regenerate 1.5 stamina points per second, for 15 minutes.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "StaminaRegen", buffAmount = 1.5 },{ buffStat = "stamina", buffAmount = 25 }
	},
}

--SCAN ADDED
ResourceEffectData.EasterEggPink= {
	SelfOnly = true,
	Tooltip = {
		"Quickly gain maximum Bloodlust!",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "Bloodlust", buffAmount = 50 },
	},
}

--SCAN ADDED
ResourceEffectData.EasterEggPurple= {
	SelfOnly = true,
	Tooltip = {
		"Add a +10 Defense bonus to yourself for 15 minutes.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "Defense", buffAmount = 10 },
	},
}

--SCAN ADDED
ResourceEffectData.EasterEggGreen= {
	SelfOnly = true,
	Tooltip = {
		"Add a +10 Attack bonus to yourself for 15 minutes.",
	},
	MobileEffect = "EatFood",
	MobileEffectArgs = {
		{ buffStat = "Attack", buffAmount = 10 },
	},
}