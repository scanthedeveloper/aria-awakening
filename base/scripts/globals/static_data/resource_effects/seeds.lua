

-- Need to make this only usablable if it's in your backpack, or 
-- on your property. Use engine_callbacks.lua [ Restrictions ]
ResourceEffectData.PlantContainer = {
    NoConsume = true,
    SelfOnly = true,
    NoAutoTarget = true,
    Range = ServerSettings.Gardening.MaximumRange, -- allows to be used outside of backpack
    UseCases = {
        "Empty"
    },
    UseCaseConditions = {
        ""
    },
}

-- Tier I Seeds
ResourceEffectData.OnionSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_onion",
    MobileEffectArgs = {
        PlantType = "Onion",
        PlantVariation = "Onion",
        PlantTemplate = "garden_onion_plant",
        DaysToMature = 7,
        MaximumYield = 50,
        PlantSize = 1
    },
    Tooltip = {
        "Plant in a pot or raised bed to grow onions.",
    }
}

ResourceEffectData.CarrotSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_carrot",
    MobileEffectArgs = {
        PlantType = "Carrot",
        PlantVariation = "Carrot",
        PlantTemplate = "garden_carrot_plant",
        DaysToMature = 7,
        MaximumYield = 50,
        PlantSize = 1
    },
    Tooltip = {
        "Plant in a pot or raised bed to grow carrots.",
    }
}

ResourceEffectData.StrawberrySeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_strawberry",
    MobileEffectArgs = {
        PlantType = "Strawberry",
        PlantVariation = "Strawberry",
        PlantTemplate = "garden_strawberry_plant",
        DaysToMature = 10,
        MaximumYield = 50,
        PlantSize = 1
    },
    Tooltip = {
        "Plant in a pot or raised bed to grow strawberries.",
    }
}

-- Tier II Seeds
ResourceEffectData.CornSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_corn",
    MobileEffectArgs = {
        PlantType = "Corn",
        PlantVariation = "Corn",
        PlantTemplate = "garden_corn_plant",
        DaysToMature = 10,
        MaximumYield = 30,
        PlantSize = 2
    },
    Tooltip = {
        "Plant in a pot or raised bed to grow corn.",
    }
}

ResourceEffectData.SquashSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_squash",
    MobileEffectArgs = {
        PlantType = "Squash",
        PlantVariation = "Squash",
        PlantTemplate = "garden_squash_plant",
        DaysToMature = 10,
        MaximumYield = 30,
        PlantSize = 2
    },
    Tooltip = {
        "Plant in a pot or raised bed to grow squash.",
    }
}

ResourceEffectData.GreenLeafLettuceSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_greenleaflettuce",
    MobileEffectArgs = {
        PlantType = "Green-leaf Lettuce",
        PlantVariation = "GreenLeafLettuce",
        PlantTemplate = "garden_greenleaflettuce_plant",
        DaysToMature = 10,
        MaximumYield = 30,
        PlantSize = 2
    },
    Tooltip = {
        "Plant in a pot or raised bed to grow green-leaf lettuce.",
    }
}

ResourceEffectData.RedLeafLettuceSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_redleaflettuce",
    MobileEffectArgs = {
        PlantType = "Red-Leaf Lettuce",
        PlantVariation = "RedLeafLettuce",
        PlantTemplate = "garden_redleaflettuce_plant",
        DaysToMature = 10,
        MaximumYield = 30,
        PlantSize = 2
    },
    Tooltip = {
        "Plant in a pot or raised bed to grow red-leaf lettuce.",
    }
}

ResourceEffectData.ButtonMushroomSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_buttonmushroom",
    MobileEffectArgs = {
        PlantType = "Button Mushroom",
        PlantVariation = "ButtonMushroom",
        PlantTemplate = "garden_buttonmushroom_plant",
        DaysToMature = 10,
        MaximumYield = 30,
        PlantSize = 2
    },
    Tooltip = {
        "Plant in a pot or raised bed to grow button mushrooms.",
    }
}

ResourceEffectData.MelonSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_melon",
    MobileEffectArgs = {
        PlantType = "Melon",
        PlantVariation = "Melon",
        PlantTemplate = "garden_melon_plant",
        DaysToMature = 10,
        MaximumYield = 30,
        PlantSize = 2
    },
    Tooltip = {
        "Plant in a pot or raised bed to grow melons.",
    }
}

-- Tier III Seeds
ResourceEffectData.GreenPepperSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_green_pepper",
    MobileEffectArgs = {
        PlantType = "Green Pepper",
        PlantVariation = "GreenPepper",
        PlantTemplate = "garden_greenpepper_plant",
        DaysToMature = 10,
        MaximumYield = 20,
        PlantSize = 3
    },
    Tooltip = {
        "Plant in a raised bed to grow green peppers.",
    }
}

ResourceEffectData.CucumberSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_cucumber",
    MobileEffectArgs = {
        PlantType = "Cucumber",
        PlantVariation = "Cucumber",
        PlantTemplate = "garden_cucumber_plant",
        DaysToMature = 10,
        MaximumYield = 20,
        PlantSize = 3
    },
    Tooltip = {
        "Plant in a raised bed to grow cucumbers.",
    }
}

ResourceEffectData.EggplantSeed = {
    NoAutoTarget = true,
    MobileEffect = "PlantSeed",
    CreatesTemplate = "ingredient_eggplant",
    MobileEffectArgs = {
        PlantType = "Eggplant",
        PlantVariation = "Eggplant",
        PlantTemplate = "garden_eggplant_plant",
        DaysToMature = 7,
        MaximumYield = 20,
        PlantSize = 3
    },
    Tooltip = {
        "Plant in a raised bed to grow eggplants.",
    }
}