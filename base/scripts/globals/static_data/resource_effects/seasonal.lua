ResourceEffectData.SpookyCauldron = {
    MobileEffect = "UseSpookyCauldron",
    MobileEffectArgs = {
    },
    SelfOnly = true,
    Range = 2,
    Tooltip = {
        "A strange glow eminates from this cauldron. Use at your own risk!",
    }
}

ResourceEffectData.EntSapling = {
    MobileEffect = "BurnEntSapling",
    MobileEffectArgs = {
    },
    SelfOnly = true,
    Range = 2,
    Tooltip = {
        "This beautiful flower is growing a Tree Lord inside. It can be burned with a torch.",
    }
}