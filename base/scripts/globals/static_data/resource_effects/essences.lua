ResourceEffectData.EnchantingTable = {
    NoConsume = true,
    SelfOnly = true,
	NoAutoTarget = true,
	Range = 5,
    MobileEffect = "Enchant",
    MobileEffectArgs = {},
    Tooltip = {
        "Used to apply magically properties to weapons and armor.",
    }
}

ResourceEffectData.SpiderLegsDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Reduced Damage from Spiders. Only works on armor.",
	},
}

ResourceEffectData.SpiderLegs = {
	SelfOnly = true,
	Tooltip = {
		"Provides Reduced Damage from Spiders. Only works on armor.",
	},
}

ResourceEffectData.SpiderLegsPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Reduced Damage from Spiders. Only works on armor.",
	},
}

ResourceEffectData.TheurgyThreadsDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Magic Resistance. Only works on armor.",
	},
}

ResourceEffectData.TheurgyThreads = {
	SelfOnly = true,
	Tooltip = {
		"Provides Magic Resistance. Only works on armor.",
	},
}

ResourceEffectData.TheurgyThreadsPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Magic Resistance. Only works on armor.",
	},
}

ResourceEffectData.RuneofPowerDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Increased Spell Power. Only works on weapon.",
	},
}

ResourceEffectData.RuneofPower = {
	SelfOnly = true,
	Tooltip = {
		"Provides Increased Spell Power. Only works on weapon.",
	},
}

ResourceEffectData.RuneofPowerPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Increased Spell Power. Only works on weapon.",
	},
}

ResourceEffectData.ShinyTrinketDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Chance to Spell Block. Only works on shield.",
	},
}

ResourceEffectData.ShinyTrinket = {
	SelfOnly = true,
	Tooltip = {
		"Provides Chance to Spell Block. Only works on shield.",
	},
}

ResourceEffectData.ShinyTrinketPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Chance to Spell Block. Only works on shield.",
	},
}

ResourceEffectData.RuneOfRovingDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Accuracy. Only works on bow.",
	},
}

ResourceEffectData.RuneOfRoving = {
	SelfOnly = true,
	Tooltip = {
		"Provides Accuracy. Only works on bow.",
	},
}

ResourceEffectData.RuneOfRovingPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Accuracy. Only works on bow.",
	},
}

ResourceEffectData.RuneOfIceDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Chance to Chill Enemy. Only works on weapon.",
	},
}

ResourceEffectData.RuneOfIce = {
	SelfOnly = true,
	Tooltip = {
		"Provides Chance to Chill Enemy. Only works on weapon.",
	},
}

ResourceEffectData.RuneOfIcePristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Chance to Chill Enemy. Only works on weapon.",
	},
}

ResourceEffectData.OgreEarsDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Reduced Damage from Giants. Only works on armor.",
	},
}

ResourceEffectData.OgreEars = {
	SelfOnly = true,
	Tooltip = {
		"Provides Reduced Damage from Giants. Only works on armor.",
	},
}

ResourceEffectData.OgreEarsPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Reduced Damage from Giants. Only works on armor.",
	},
}

ResourceEffectData.RuneofEvasionDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Increased Evasion. Only works on armor.",
	},
}

ResourceEffectData.RuneofEvasion = {
	SelfOnly = true,
	Tooltip = {
		"Provides Increased Evasion. Only works on armor.",
	},
}

ResourceEffectData.RuneofEvasionPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Increased Evasion. Only works on armor.",
	},
}

ResourceEffectData.LizardmanScalesDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Reduced Damage from Reptiles. Only works on armor.",
	},
}

ResourceEffectData.LizardmanScales = {
	SelfOnly = true,
	Tooltip = {
		"Provides Reduced Damage from Reptiles. Only works on armor.",
	},
}

ResourceEffectData.LizardmanScalesPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Reduced Damage from Reptiles. Only works on armor.",
	},
}

ResourceEffectData.OrkScalpDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Reduced Damage from Orks. Only works on armor.",
	},
}

ResourceEffectData.OrkScalp = {
	SelfOnly = true,
	Tooltip = {
		"Provides Reduced Damage from Orks. Only works on armor.",
	},
}

ResourceEffectData.OrkScalpPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Reduced Damage from Orks. Only works on armor.",
	},
}

ResourceEffectData.EntRootsDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Reduced Damage from Ents. Only works on armor.",
	},
}

ResourceEffectData.EntRoots = {
	SelfOnly = true,
	Tooltip = {
		"Provides Reduced Damage from Ents. Only works on armor.",
	},
}

ResourceEffectData.EntRootsPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Reduced Damage from Ents. Only works on armor.",
	},
}

ResourceEffectData.RuneOfIshiDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Increased Attack. Only works on bow.",
	},
}

ResourceEffectData.RuneOfIshi = {
	SelfOnly = true,
	Tooltip = {
		"Provides Increased Attack. Only works on bow.",
	},
}

ResourceEffectData.RuneOfIshiPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Increased Attack. Only works on bow.",
	},
}

ResourceEffectData.SturdyThreadsDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Bashing Resistance. Only works on armor.",
	},
}

ResourceEffectData.SturdyThreads = {
	SelfOnly = true,
	Tooltip = {
		"Provides Bashing Resistance. Only works on armor.",
	},
}

ResourceEffectData.SturdyThreadsPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Bashing Resistance. Only works on armor.",
	},
}

ResourceEffectData.RuneofFireDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Chance to Burn Enemy. Only works on weapon.",
	},
}

ResourceEffectData.RuneofFire = {
	SelfOnly = true,
	Tooltip = {
		"Provides Chance to Burn Enemy. Only works on weapon.",
	},
}

ResourceEffectData.RuneofFirePristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Chance to Burn Enemy. Only works on weapon.",
	},
}

ResourceEffectData.RuneofAccuracyDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Accuracy. Only works on weapon.",
	},
}

ResourceEffectData.RuneofAccuracy = {
	SelfOnly = true,
	Tooltip = {
		"Provides Accuracy. Only works on weapon.",
	},
}

ResourceEffectData.RuneofAccuracyPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Accuracy. Only works on weapon.",
	},
}

ResourceEffectData.GargoyleEyeballDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Reduced Damage from Demons. Only works on armor.",
	},
}

ResourceEffectData.GargoyleEyeball = {
	SelfOnly = true,
	Tooltip = {
		"Provides Reduced Damage from Demons. Only works on armor.",
	},
}

ResourceEffectData.GargoyleEyeballPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Reduced Damage from Demons. Only works on armor.",
	},
}

ResourceEffectData.RuneOfLightningDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Chance to Shock Enemy. Only works on weapon.",
	},
}

ResourceEffectData.RuneOfLightning = {
	SelfOnly = true,
	Tooltip = {
		"Provides Chance to Shock Enemy. Only works on weapon.",
	},
}

ResourceEffectData.RuneOfLightningPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Chance to Shock Enemy. Only works on weapon.",
	},
}

ResourceEffectData.RuneofSpeedDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Increased Attack Speed. Only works on weapon.",
	},
}

ResourceEffectData.RuneofSpeed = {
	SelfOnly = true,
	Tooltip = {
		"Provides Increased Attack Speed. Only works on weapon.",
	},
}

ResourceEffectData.RuneofSpeedPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Increased Attack Speed. Only works on weapon.",
	},
}

ResourceEffectData.RuneofWaterDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Chance to enter Clearcasting. Only works on armor.",
	},
}

ResourceEffectData.RuneofWater = {
	SelfOnly = true,
	Tooltip = {
		"Provides Chance to enter Clearcasting. Only works on armor.",
	},
}

ResourceEffectData.RuneofWaterPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Chance to enter Clearcasting. Only works on armor.",
	},
}

ResourceEffectData.ExpandingTrinketDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Ranged Resistance. Only works on shield.",
	},
}

ResourceEffectData.ExpandingTrinket = {
	SelfOnly = true,
	Tooltip = {
		"Provides Ranged Resistance. Only works on shield.",
	},
}

ResourceEffectData.ExpandingTrinketPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Ranged Resistance. Only works on shield.",
	},
}

ResourceEffectData.DragonScalesDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Reduced Damage from Dragons. Only works on armor.",
	},
}

ResourceEffectData.DragonScales = {
	SelfOnly = true,
	Tooltip = {
		"Provides Reduced Damage from Dragons. Only works on armor.",
	},
}

ResourceEffectData.DragonScalesPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Reduced Damage from Dragons. Only works on armor.",
	},
}

ResourceEffectData.ResilientThreadsDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Slashing Resistance. Only works on armor.",
	},
}

ResourceEffectData.ResilientThreads = {
	SelfOnly = true,
	Tooltip = {
		"Provides Slashing Resistance. Only works on armor.",
	},
}

ResourceEffectData.ResilientThreadsPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Slashing Resistance. Only works on armor.",
	},
}

ResourceEffectData.FlowingThreadsDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Reduced Mana Cost. Only works on armor.",
	},
}

ResourceEffectData.FlowingThreads = {
	SelfOnly = true,
	Tooltip = {
		"Provides Reduced Mana Cost. Only works on armor.",
	},
}

ResourceEffectData.FlowingThreadsPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Reduced Mana Cost. Only works on armor.",
	},
}

ResourceEffectData.CharredBonesDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Reduced Damage from Undead. Only works on armor.",
	},
}

ResourceEffectData.CharredBones = {
	SelfOnly = true,
	Tooltip = {
		"Provides Reduced Damage from Undead. Only works on armor.",
	},
}

ResourceEffectData.CharredBonesPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Reduced Damage from Undead. Only works on armor.",
	},
}

ResourceEffectData.GrizzlyBearToothDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Reduced Damage from Animals. Only works on armor.",
	},
}

ResourceEffectData.GrizzlyBearTooth = {
	SelfOnly = true,
	Tooltip = {
		"Provides Reduced Damage from Animals. Only works on armor.",
	},
}

ResourceEffectData.GrizzlyBearToothPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Reduced Damage from Animals. Only works on armor.",
	},
}

ResourceEffectData.RuneofPatienceDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Chance to gain Centershot. Only works on bow.",
	},
}

ResourceEffectData.RuneofPatience = {
	SelfOnly = true,
	Tooltip = {
		"Provides Chance to gain Centershot. Only works on bow.",
	},
}

ResourceEffectData.RuneofPatiencePristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Chance to gain Centershot. Only works on bow.",
	},
}

ResourceEffectData.RuneofMendingDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Increased Healing Received. Only works on .",
	},
}

ResourceEffectData.RuneofMending = {
	SelfOnly = true,
	Tooltip = {
		"Provides Increased Healing Received. Only works on .",
	},
}

ResourceEffectData.RuneofMendingPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Increased Healing Received. Only works on .",
	},
}

ResourceEffectData.RuneofVanquishingDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Increased Attack. Only works on weapon.",
	},
}

ResourceEffectData.RuneofVanquishing = {
	SelfOnly = true,
	Tooltip = {
		"Provides Increased Attack. Only works on weapon.",
	},
}

ResourceEffectData.RuneofVanquishingPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Increased Attack. Only works on weapon.",
	},
}

ResourceEffectData.SeveredHandDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Deduced Damage from Humanoids. Only works on armor.",
	},
}

ResourceEffectData.SeveredHand = {
	SelfOnly = true,
	Tooltip = {
		"Provides Deduced Damage from Humanoids. Only works on armor.",
	},
}

ResourceEffectData.SeveredHandPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Deduced Damage from Humanoids. Only works on armor.",
	},
}

ResourceEffectData.UnyieldingThreadsDamaged = {
	SelfOnly = true,
	Tooltip = {
		"Provides marginal Piercing Resistance. Only works on armor.",
	},
}

ResourceEffectData.UnyieldingThreads = {
	SelfOnly = true,
	Tooltip = {
		"Provides Piercing Resistance. Only works on armor.",
	},
}

ResourceEffectData.UnyieldingThreadsPristine = {
	SelfOnly = true,
	Tooltip = {
		"Provides substantial Piercing Resistance. Only works on armor.",
	},
}