ResourceEffectData.DyeTub = {
    NoAutoTarget = true,
    NoDismount = true,
    NoConsume = true,
    SelfOnly = true,

    Range = 5,

    HasSecuritySettings = true,

    UseCases = {
        "Dye",
        --"Change",
    },
    MobileEffectUseCases = {
        Dye = {
            MobileEffect = "DyeTub",
        },
        --[[Change = {
            MobileEffect = "ChangeDyeTub",
        },]]
    },
    Tooltip = {},
    TooltipFunc = function(tooltipInfo, item)
        tooltipInfo["description"] = {
            TooltipString = "Fill the tub with dyes to use it to dye items.",
            Priority = 4,
        }
        tooltipInfo["type"] = {
            TooltipString = "Dye type: "..item:GetObjVar("TubType"),
            Priority = 3,
        }
        tooltipInfo["dyeName"] = {
            TooltipString = "Color: "..item:GetObjVar("DyeColorName"),
            Priority = 2,
        }
        tooltipInfo["Uses"] = {
            TooltipString = "Uses: "..item:GetObjVar("Charges"),
            Priority = 1,
        }
        return tooltipInfo
    end,
}

ResourceEffectData.DyeTubDyes = {
    NoAutoTarget = true,
    NoDismount = true,
    NoConsume = true,
    MobileEffect = "DyeTubDyes",
    MobileEffectObjectAsArgs = true,
    TooltipFunc = function(tooltipInfo, item)
        tooltipInfo["description"] = {
            TooltipString = "When applied to a dye tub, adds "..(item:GetObjVar("Charges") or 0).." uses of this dye.\n\n",
            Priority = 4,
        }
        tooltipInfo["type"] = {
            TooltipString = "Dye type: "..item:GetObjVar("TubType"),
            Priority = 3,
        }
        tooltipInfo["dyeName"] = {
            TooltipString = "Color: "..item:GetObjVar("DyeColorName"),
            Priority = 2,
        }
        return tooltipInfo
    end,
}

ResourceEffectData.HairDye = {
    NoAutoTarget = true,
    NoDismount = true,
    NoConsume = true,
    SelfOnly = true,

    Range = 5,

    --HasSecuritySettings = true,

    --[[UseCases = {
        "Dye",
        --"Change",
    },]]
    MobileEffect = "HairDye",
    --[[MobileEffectUseCases = {
        Dye = {
            MobileEffect = "DyeTub",
        },
        Change = {
            MobileEffect = "ChangeDyeTub",
        },
    },]]
    Tooltip = {},
    TooltipFunc = function(tooltipInfo, item)
        tooltipInfo["description"] = {
            TooltipString = "The dye seems to be high quality, but it smells pretty bad.",
            Priority = 2,
        }
        tooltipInfo["dyeName"] = {
            TooltipString = "Color: "..item:GetObjVar("DyeColorName"),
            Priority = 1,
        }
        return tooltipInfo
    end,
}

ResourceEffectData.BlazeDye = {
    NoAutoTarget = true,
    NoDismount = true,
    MobileEffect = "Dye",
    MobileEffectArgs = {
        Hue = 826,
    },
    Tooltip = {
        "Dye your clothing a unique hue.",
    }
}

ResourceEffectData.CrimsonDye = {
    NoAutoTarget = true,
    NoDismount = true,
    MobileEffect = "Dye",
    MobileEffectArgs = {
        Hue = 865,
    },
    Tooltip = {
        "Dye your clothing a unique hue.",
    }
}

ResourceEffectData.ShadeDye = {
    NoAutoTarget = true,
    NoDismount = true,
    MobileEffect = "Dye",
    MobileEffectArgs = {
        Hue = 893,
    },
    Tooltip = {
        "Dye your clothing a unique hue.",
    }
}

ResourceEffectData.VileDye = {
    NoAutoTarget = true,
    NoDismount = true,
    MobileEffect = "Dye",
    MobileEffectArgs = {
        Hue = 941,
    },
    Tooltip = {
        "Dye your clothing a unique hue.",
    }
}

ResourceEffectData.IceDye = {
    NoAutoTarget = true,
    NoDismount = true,
    MobileEffect = "Dye",
    MobileEffectArgs = {
        Hue = 819,
    },
    Tooltip = {
        "Dye your clothing a unique hue.",
    }
}