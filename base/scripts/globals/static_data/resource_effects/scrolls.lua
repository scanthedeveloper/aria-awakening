-- build static data to fit our resource effect system

for spell,data in pairs(SpellData.AllSpells) do
    ResourceEffectData[spell] = {
        SendUseObject = true,
        UseCases = {
            "Cast",
        },
        OldSchoolUseCases = {
        },
        Tooltip = {
            SpellData.AllSpells[spell].SpellTooltipString,
            "Difficulty "..(SpellData.AllSpells[spell].Circle or 8),
        }
    }
end

-- Executioner Scrolls

ResourceEffectData.AnimalExecutionerScroll = {
    NoAutoTarget = true,
    MobileEffect = "ApplyExecutionerScroll",
    MobileEffectArgs = {
        ExecutionerType = "Animal",
        ExecutionerPowerRange = { 1, 2, 3 }
    },
    Tooltip = {
        "Use on a weapon to apply the [ffb523]Animal[-] executioner effect.",
    }
}

ResourceEffectData.ArachnidExecutionerScroll = {
    NoAutoTarget = true,
    MobileEffect = "ApplyExecutionerScroll",
    MobileEffectArgs = {
        ExecutionerType = "Arachnid",
        ExecutionerPowerRange = { 1, 2, 3 }
    },
    Tooltip = {
        "Use on a weapon to apply the [ffb523]Arachnid[-] executioner effect.",
    }
}

ResourceEffectData.DemonExecutionerScroll = {
    NoAutoTarget = true,
    MobileEffect = "ApplyExecutionerScroll",
    MobileEffectArgs = {
        ExecutionerType = "Demon",
        ExecutionerPowerRange = { 1, 2, 3 }
    },
    Tooltip = {
        "Use on a weapon to apply the [ffb523]Demon[-] executioner effect.",
    }
}

ResourceEffectData.DragonExecutionerScroll = {
    NoAutoTarget = true,
    MobileEffect = "ApplyExecutionerScroll",
    MobileEffectArgs = {
        ExecutionerType = "Dragon",
        ExecutionerPowerRange = { 1, 2, 3 }
    },
    Tooltip = {
        "Use on a weapon to apply the [ffb523]Dragon[-] executioner effect.",
    }
}

ResourceEffectData.EntExecutionerScroll = {
    NoAutoTarget = true,
    MobileEffect = "ApplyExecutionerScroll",
    MobileEffectArgs = {
        ExecutionerType = "Ent",
        ExecutionerPowerRange = { 1, 2, 3 }
    },
    Tooltip = {
        "Use on a weapon to apply the [ffb523]Ent[-] executioner effect.",
    }
}

ResourceEffectData.GiantExecutionerScroll = {
    NoAutoTarget = true,
    MobileEffect = "ApplyExecutionerScroll",
    MobileEffectArgs = {
        ExecutionerType = "Giant",
        ExecutionerPowerRange = { 1, 2, 3 }
    },
    Tooltip = {
        "Use on a weapon to apply the [ffb523]Giant[-] executioner effect.",
    }
}

ResourceEffectData.HumanoidExecutionerScroll = {
    NoAutoTarget = true,
    MobileEffect = "ApplyExecutionerScroll",
    MobileEffectArgs = {
        ExecutionerType = "Humanoid",
        ExecutionerPowerRange = { 1, 2, 3 }
    },
    Tooltip = {
        "Use on a weapon to apply the [ffb523]Humanoid[-] executioner effect.",
    }
}

ResourceEffectData.OrkExecutionerScroll = {
    NoAutoTarget = true,
    MobileEffect = "ApplyExecutionerScroll",
    MobileEffectArgs = {
        ExecutionerType = "Ork",
        ExecutionerPowerRange = { 1, 2, 3 }
    },
    Tooltip = {
        "Use on a weapon to apply the [ffb523]Ork[-] executioner effect.",
    }
}

ResourceEffectData.ReptileExecutionerScroll = {
    NoAutoTarget = true,
    MobileEffect = "ApplyExecutionerScroll",
    MobileEffectArgs = {
        ExecutionerType = "Reptile",
        ExecutionerPowerRange = { 1, 2, 3 }
    },
    Tooltip = {
        "Use on a weapon to apply the [ffb523]Reptile[-] executioner effect.",
    }
}

ResourceEffectData.UndeadExecutionerScroll = {
    NoAutoTarget = true,
    MobileEffect = "ApplyExecutionerScroll",
    MobileEffectArgs = {
        ExecutionerType = "Undead",
        ExecutionerPowerRange = { 1, 2, 3 }
    },
    Tooltip = {
        "Use on a weapon to apply the [ffb523]Undead[-] executioner effect.",
    }
}

ResourceEffectData.ScrollOfPenitence = {
    NoDismount = true,
    SelfOnly = true,
    MobileEffect = "ScrollOfPenitence",
    MobileEffectObjectAsArgs = true, -- pass the used object as the target parameter

    UseCases = {
        "Use",
    },

    UseCaseConditions = {
        "IsInBackpack"
    },

    Tooltip = {
        "Allows an Outcast to immediately drop Outcast status.",
        "Any criminal action, performed witin 30 days, will undo this."
    }
}