--[[Denominations = {
	{ Name = "Copper"  , Value = (1)              , Abbrev = "c", Color = "[B87333]" },
	{ Name = "Silver"  , Value = (1 * 100)        , Abbrev = "s", Color = "[C0C0C0]" }, -- 100 copper
	{ Name = "Gold"    , Value = (100 * 100)      , Abbrev = "g", Color = "[FFD700]" }, -- 10,000 copper
	{ Name = "Platinum", Value = (100 * 100 * 100), Abbrev = "p", Color = "[E0E0E0]" }, -- 1,000,000 copper  
}]]

Denominations = {
	{ Name = "GoldV2"    , DisplayName = "Gold", Value = (1)      , Abbrev = "g", Color = "[FFD700]" }, 
	{ Name = "PlatinumV2", DisplayName = "Platinum", Value = (10000), Abbrev = "p", Color = "[E0E0E0]" },   
}