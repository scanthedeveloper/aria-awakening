--[[     entries in [] are optional
Name = string, shown to player EX: "Learning to Heal"
Instructions = string, describes the objective(s) EX: "Purchase bandages from Willy."
[TimeLimit] = TimeSpan to complete the quest before it fails, EX: TimeSpan.FromMinutes(30)
[Cooldown] = TimeSpan before quest can be started again, nil is never, EX: TimeSpan.FromMilliseconds(1)
NextStep = number, index of step to start after this one completes successfully, use this to create quest chains or multiple steps, optional EX: "Learning to Cure Poison"
[Silent] = boolean, don't send player quest text, EX: true
StartDialogue = table, EX: {"Go do the thing?","Please go do the thing."}, (label for the dialogue response, text for the dialogue response)
EndDialogue = table, EX: {"Done the thing?","Thanks for doing the thing"}, (label for the dialogue response, text for the dialogue response)
[IneligibleDialogue] = table {"Is this enough?", "Not even close"}, (Label, Text for dialogue response)
[IneligibleChecks] = table similar to requirements, needed to safely use IneligibleDialogue on steps that aren't active yet (Like the first step of a new Tier)

Table Structures and Options:
MarkerLocs = {
    {10, 0, -10},
    {20, 0, -20},
},
OnStart/OnEnd = {
    {"Custom", function(playerObj)
        if ( 1 == 1 )
        then return true else return false end
    end},
    {"TakeItem", "weapon_spear", 5}, (type, template, [quantity])
    {"GiveItem", "bandage", 10}, (type, template, [quantity])
    {"AcceptCraftingOrder", "WoodsmithSkill"}, (Removes a CO of 'OrderSkill' and then gives player Crafting Reward)
},
Requirements = {
    {"Custom", function(playerObj)
        if ( 1 == 1 )
        then return true else return false end
    end},
    {"QuestActive", "Test", true}, (Iterates through a quest by Id to determine if any of its steps are active, can check true or false)
    {"MinTimesQuestDone", "Test", 1}, (required times a quest step has been completed - questName, stepIndex, value) Example checks quest "Test" step 1 has been completed at least once
    {"MaxTimesQuestDone", "Test", 1}, (max times a quest step can be completed - questName, stepIndex, value) Example checks quest "Test" step 1 has been completed no more than 0 times(never)
    {"Skill", "MeleeSkill", 100}, (require a skill is at least x)
    {"DoneAchievement", "Journeyman Tamer", true}, (true/false can require it to be done or not done)
    {"HasItem", "arrow", 100}, (type, template, [quantity])
    {"HasItemInBackpack", "arrow", 100},
    {"HasMobileEffect", "NoBandage", true}, (true/false can require it to be done or not done)
    {"Disabled"}, (quest cannot be started)
    {"HasCraftedItem", "packed_object", 5, "UnpackedTemplate", "crate_empty"}, (True if player crafted Amount of specific object - Template, Amount, ObjVar Key, ObjVar Value)
    {"HasCompletedCraftingOrder", "WoodsmithSkill"}, (True if player has CO completed and completed by them - OrderSkill)
    {"CapStepCompletions", "CarpenterProfessionTierOne", 1, 1}, (False if Step has certain MaxAllowed amount of completions - QuestName, StepToCheck, MaxAllowed)
    {"StepMustBeActive", "CarpenterProfessionTierOne", 2}, (True if step is Active - QuestName, StepToCheck)
},
Goals = {
    {"Custom", function(playerObj)
        if ( 1 == 1 )
        then return true else return false end
    end},
    {"Skill", "MeleeSkill", 100}, (require a skill is at least x)
    {"DoneAchievement", "Journeyman Tamer", true}, (true/false can require it to be done or not done)
    {"HasItem", "arrow", 100}, (type, template, [quantity])
    {"HasItemInBackpack", "arrow", 100},
    {"HasMobileEffect", "NoBandage", true}, (true/false can require it to be done or not done)
    {"MobTeamTypeKills", "Reptile", 5},
    {"MobKindKills", "Dragon", 5},
    {"MobTemplateKills", "dragon", 5},
    {"Tamed", "chicken", 2}
    {"InRegion", "Ruin", true}, checks if string matches any part of player's current region info
    {"TurnIn"}, requires the quest to be manually ended with TryEnd(quest giver, UI interaction, etc)
    {"TakeItem", "weapon_spear", 5}, (type, template, [quantity])
    {"StillEligible", "CarpenterProfessionIntro", 3}, (True if player passes IsEligible check again - QuestName, StepToCheck) (Needed for deliveries)
},
Rewards = { --note that this table is 3 deep, rewards are grouped into "choices", all rewards within a choice are given
    {
        {"Item", "arrow", 100}, (type, template, [quantity])
        {"Item", "weapon_katana", 5},
        {"Item", "weapon_spear", 1},
    },
    {
        {"Item", "bandage", 5},
        {"Custom", function()
            if ( 1 == 1 ) then return true else return false end
        end},
    },
},
OnEnd = {
    see OnStart
},
]]

QuestMarkers = {
    RogueMessenger = { -2247.97, 0, -809.66 },
    YewellSteeltounge = {-1191.10, 0, -2103.7},
    GatekeeperSouthernHills = {984.168, 0, -1278.326},
    GatekeeperBlackForest = {1832.369, 0.333, -371.48},
    GatekeeperUpperPlains = {259.989, 0, 794.169},
    GatekeeperSouthernRim = {-2652.173, -0.033, -2078.37},
    GatekeeperCrossroads = {-662.044, 0.015, -1604.877},
    GatekeeperBarrenLands = {-2113.219, -0.024, -684.13},
    GatekeeperEasternFrontier = {2448.619, -0.311, 628.02},
    SewerHelm = {2801.63, 0, 735.31},
    SewerEldeir = {247.559, 0, 869.25},
    SewerPyros = {-2757.698, 1.708, -2187.948},
    SewerValus = {827.5, 0, -1121.9},
    GraveyardHelm = {2854.61, 0, 1161.73},
    GraveyardEldeir = {407, 0, 980},
    GraveyardValus = {749, 0, -1272},
    GraveyardPyros = {-2911, 0, -2150},
    RuinEntrance = {-1175.4, -0.376, -1738.3},
    DeceptionEntrance = {-2600.6, 0.173, -64.599},
    ContemptEntrance = {2979.42, 0, -475.11},
    FighterPyros = {-2709.72, 0, -2234.49},
    FighterHelm = {2739.26, 0, 679.71},
    FighterEldeir = {125.489, 0, 816.309},
    FighterValus = {795.11, 0.076, -1182.02},
    MageBarrenLands = {-2287.227, 0.062, -803.08},
    MageEldeir = {285.23, 0, 839.929},
    MageValus = {939.16, 0.076, -1140.38},
    HealerBarrenLands = {-2344.52, 0, -827.84},
    HealerPyros = {-2828.20, 0, -2199.61},
    HealerHelm = {2796.17, 0, 740.49},
    HealerEldeir = {336.6, 0, 938.5},
    HealerValus = {869.90, 0, -1099.42},
    RoguePyros = {-2854.91, 0, -2232.69},
    RogueBlackForest = {2154.635, -0.08, -349.088},
    RogueEldeir = {325.399, 0, 955.059},
    MissionsValus = {808.29, 0, -1129.46},
    MissionsEldeir = {230.867, 0, 858.109},
    MissionsHelm = {2774.399, 0.032, 804.219},
    MissionsOasis = {-2353.88, 0.013, -842.83},
    MissionsPyros = {-2816.718, 0, -2223.314},
    MissionsBlackForest = {2146.75, -0.145, -433.54},
    HarvestMissionsOasis = {-2346.62, -0.217, -847.049},
    HarvestMissionsBelhaven = {-52.18, 32.82975, -1829.069},
    HarvestMissionsEldeir = {293.81, 26.21639, 919.48},
    HarvestMissionsHelm = {2772.496, 46.16549, 808.1362},
    HarvestMissionsBlackForest = {2198.57, 49.95739, -376.77},
    HarvestMissionsPyros = {-2804.945, 36.99, -2235.215},
    HarvestMissionsTrinit = {531.86, 31.56, -203.39},
    HarvestMissionsValus = {792.45, 40.92, -1129.81},
    CatacombsCraftingGrandmaster = {3.709, 22.48, -15.585},
    StablePyros = {-2806.01, 0, -2139.114},
    StableValus = {761.4, 0.375, -1090.55},
    StableEldeir = {169.52, -0.131, 933.02},
    StableHelm = {2706.385, 0.006, 681.619},
    StableBlackForest = {2121.82, 0, -350.75},
    BlacksmithPyros = {-2718.989, 0.213, -2248.156},
    BlacksmithValus = {803.341, -0.012, -1195.255},
    BlacksmithEldeir = {261.519, 0.225, 929.158},
    BlacksmithHelm = {2730.35, -0.076, 751.348},
    BlacksmithBlackForest = {2214.927, -0.069, -390.6},
    BlacksmithBelhaven = {-28.707, 0.117, -1804.265},
    BlacksmithTrinit = {527.361, 0.248, -182.957},
    BlacksmithOasis = {-2340.083, 0.09, -853.27},
    CarpenterPyros = {-2783.322, 0.236, -2191.103},
    CarpenterHelm = {2897.903, -0.006, 829.054},
    CarpenterBarrenLands = {-2329.177, 0.204, -848.539},
    CarpenterEldeir = {218.46, 0.01, 942.369},
    CarpenterBlackForest = {2214.927, -0.069, -390.6},
    CarpenterValus = {789.726, 0.277, -1175.618},
    CarpenterBelhaven = { -22.7, 0.131, -1823.437},
    CarpenterTrinit = {555.049, 0.02, -189.96},
    CarpenterOasis = {-2329.177, 0.204, -848.539},
    TailorValus = {913.023, 0.027, -1117.998},
    TailorPyros = {-2714.08, -0.006, -2278.78},
    TailorHelm = {2855.947, 0.208, 665.992},
    TailorBlackForest = {2201.36, 0.221, -340.368},
    TailorTrinit = {532.098, 0.034, -214.078},
    TailorOasis = {-2352.037, 0.284, -833.716},
    FisherHelm = {2845, 0.0, 767.69},
    FisherPyros = {-2806.5, 0, -2269.688},
    FisherValus = {-40.299, 0, -1838},
    BardPyros = {-2819.64, 0, -2208.56},
    BardValus = {889.54, 0, -1098.65},
    BardHelm = {2803.65, 0, 760},
    BardEldeir = {245.06, 0, 868.22},
    HelmChef = {2762.62, 0.006, 817.289},
    EldeirChef = {297.393, 0.027, 889.151},
    PyrosAlchemist = {-2788.648, 0, -2230.148},
    ValusAlchemist = {820.656, 0.02, -1167.65},
    BarrenLandsAlchemist = {-2340.968, -0.003, -840.489},
    EldeirAlchemist = {296.915, 0.795, 905.158},
    BlackForestAlchemist = {2188.76, 0.055, -349.85},
    PyrosScribe = {-2788.115, 0.002, -2222.125},
    ValusScribe = {894.731, 0.095, -1187.706},
    EldeirScribe = {307.121, 0.006, 936.58},
    HelmScribe = {2763.5, 0.032, 791.789},
    PyrosCotton = {-2783.46, 0, -2144.33},
    EldeirCotton = {239.66, 0, 898.85},
    Trinit = {545.098, 0, -205.5},
    DestroyedTown = {1781.20, 0, 973.24},
    LichArea = {2893.38, 0, 112.26},
    AbandonedShrine = {1456.41, 0, 1170.65},
    HauntedRuins = {52.40178, 0, 78.06},
    LichRuins = {945, 0 ,-2179},
    ValusCemetaryPeak = {302,0,-1307},
    GuildmasterFighter = {105.24, 0, 829.44},
    GuildmasterMage = {939.16, 0, -1140.38},

}

QuestMarkerGroups = {
    AllBlacksmiths = { 
        QuestMarkers.GatekeeperCrossroads,
        QuestMarkers.GatekeeperBarrenLands,
        QuestMarkers.BlacksmithPyros,
        QuestMarkers.BlacksmithValus,
        QuestMarkers.BlacksmithEldeir,
        QuestMarkers.BlacksmithHelm,
        QuestMarkers.BlacksmithBlackForest, 
        QuestMarkers.BlacksmithBelhaven, 
        QuestMarkers.BlacksmithTrinit,
        QuestMarkers.BlacksmithOasis,
    },
    AllCarpenters = { 
        QuestMarkers.GatekeeperCrossroads,
        QuestMarkers.GatekeeperBarrenLands,
        QuestMarkers.CarpenterPyros,
        QuestMarkers.CarpenterHelm,
        QuestMarkers.CarpenterBarrenLands,
        QuestMarkers.CarpenterEldeir,
        QuestMarkers.CarpenterBlackForest,
        QuestMarkers.CarpenterValus,
        QuestMarkers.CarpenterBelhaven,
        QuestMarkers.CarpenterTrinit,
        QuestMarkers.CarpenterOasis,
    },
    AllTailors = {
        QuestMarkers.GatekeeperCrossroads,
        QuestMarkers.GatekeeperBarrenLands,
        QuestMarkers.TailorValus,
        QuestMarkers.TailorPyros,
        QuestMarkers.TailorHelm,
        QuestMarkers.TailorBlackForest,
        QuestMarkers.TailorTrinit,
        QuestMarkers.TailorOasis,
    },
    AllHarvestMissions = {
        QuestMarkers.HarvestMissionsOasis,
        QuestMarkers.HarvestMissionsBelhaven,
        QuestMarkers.HarvestMissionsEldeir,
        QuestMarkers.HarvestMissionsHelm,
        QuestMarkers.HarvestMissionsBlackForest,
        QuestMarkers.HarvestMissionsPyros,
        QuestMarkers.HarvestMissionsTrinit,
        QuestMarkers.HarvestMissionsValus,
    },
}

QuestMobTargets =
{
    Undead = {"skeleton", "ruin_skeleton", "ruin_zombie","zombie","skeleton_hq"},
    Vermin = {"sewer_lurker","giant_bat","sewer_rat","rat_giant","sewer_bat"},
    Lich = {"lich","ruin_lich","ruin_lich_with_servant"},
    Ork = {"ork_mage","ork_archer","ork_warrior","ork_chief"},
    Contempt = {"contempt_harpy","contempt_warg_alpha","contempt_warg"},
}

AllQuests = 
{
    Example = {
        {--1    <-- step index/ID
            Name = "Example Quest",
            Instructions = "Go do the thing.",
            Silent = false,
            MarkerLocs = {
                {10, 0, -10},
            },
            TimeLimit = TimeSpan.FromMinutes(1),
            Cooldown = TimeSpan.FromMilliseconds(1),
            NextStep = 2,
            StartDialogue = {"Go do the thing.","Please go do the thing."},
            EndDialogue = {"Done the thing?","Thanks for doing the thing."},
            Requirements = {
                {"Custom", function(playerObj)
                    if ( 1 == 1 )
                    then return true else return false end
                end},
                {"Skill", "MeleeSkill", 100},
                {"DoneAchievement", "Journeyman Tamer", true},
                {"HasItem", "arrow", 100},
                {"HasMobileEffect", "NoBandage", true},
                {"QuestActive", 4, true},
                {"MinTimesQuestDone", 4, 1, 1},
                {"MaxTimesQuestDone", 3, 1, 1},
            },
            OnStart = {
                {"Custom", function()
                    if ( 1 == 1 )
                    then return true else return false end
                end},
                {"TakeItem", "weapon_spear", 5},
                {"GiveItem", "bandage", 10},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if ( 1 == 1 )
                    then return true else return false end
                end},
                {"Skill", "MeleeSkill", 100},
                {"DoneAchievement", "Journeyman Tamer", true},
                {"HasItem", "arrow", 100},
                {"HasItemInBackpack", "arrow", 100},
                {"HasMobileEffect", "NoBandage", true},
                {"MobTeamTypeKills", "Reptile", 5},
                {"MobKindKills", "Dragon", 5},
                {"MobTemplateKills", "dragon", 5},
                {"InRegion", "Ruin", true},
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item", "arrow", 100},
                    {"Item", "weapon_katana", 5},
                    {"Item", "weapon_spear", 1},
                },
                {--choice 2
                    {"Item", "bandage", 5},
                    {"Custom", function()
                        if ( 1 == 1 ) then return true else return false end
                    end},
                },
            },
            OnEnd = {
                {"Custom", function()
                    if ( 1 == 1 )
                    then return true else return false end
                end},
                {"TakeItem", "weapon_spear", 5},
                {"GiveItem", "bandage", 10},
            },
        },
    },
    Test = {
        {--1
            Name = "Test",
            Instructions = "Just go test stuff.",
            Silent = false,
            MarkerLocs = {
                {10, 0, -10},
            },
            TimeLimit = TimeSpan.FromMinutes(1),
            Cooldown = TimeSpan.FromMilliseconds(1),
            NextStep = 2,
            StartDialogue = {"Go do the thing?","Please go do the thing."},
            EndDialogue = {"Done the thing?","Thanks for doing the thing"},
            Requirements = {
                --{"HasMobileEffect", "NoBandage", true},
                --{"DoneAchievement", "Journeyman Tamer", true},
            },
            OnStart = {
                --{"GiveItem", "weapon_katana"}
            },
            Goals = {
                --{"Kill", "chicken", 1},
                --{"HasMobileEffect", "NoBandage", true},
                {"MobTeamTypeKills", "Reptile", 200},
            },
            Rewards = {
                --{--choice 1
                    --{"Item", "bandage", 50}
                --},
                --{--choice 2
                --  {"Item", "weapon_spear"}
                --},
            },
            OnEnd = {
                {"TakeItem", "weapon_katana", 5}
            },
        },
        {--2
            Name = "Test Part 2",
            Instructions = "Test stuff again.",
            Silent = false,
            MarkerLocs = {
                {10, 0, -10},
            },
            TimeLimit = TimeSpan.FromMinutes(1),
            Cooldown = nil,
            NextStep = nil,
            StartDialogue = {"Go do the thing?","Please go do the thing."},
            EndDialogue = {"Done the thing?","Have you done the thing I asked of you?"},
            Requirements = {
            },
            OnStart = {
            },
            Goals = {
                {"MobTeamTypeKills", "Reptile", 200},
            },
            Rewards = {
                {--choice 1
                },
            },
            OnEnd = {
            },
        },
    },
    --Professions
    StartAProfQuest = {
        {--1
            Name = "Start a Profession",
            Instructions = "Start a [F2F5A9]profession[-] by opening your [F2F5A9]professions window[-] from the menu in the bottom right of your screen and clicking [F2F5A9]Train Profession[-].",
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "StartAProfQuest", 1},
                {"QuestActive", "StartAProfQuest", false},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if ( playerObj:HasModule("profession_guide_window") ) then return true else return false end
                end},
            },
        },
        {--2
            Name = "Start a Profession",
            Instructions = "Click [F2F5A9]Train Profession[-] under a profession to begin it.",
            Fails = {
                {"Custom", function(playerObj)
                    if ( not playerObj:HasModule("profession_guide_window") ) then
                        Quests.Fail(playerObj, "StartAProfQuest", 2, true)
                        Quests.TryStart(playerObj, "StartAProfQuest", 1, true)
                        return true
                    else
                        return false
                    end
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local quests = playerObj:GetObjVar("Quests") or {}
                    if ( quests and next(quests) ) then
                        for questName, quest in pairs(quests) do
                            local match = string.match(questName, "Profession")
                            if ( match and match == "Profession" ) then
                                if (quest[1] and quest[1]["Active"] and quest[1]["Active"] == 1 ) then
                                    return true
                                end
                            end
                        end
                    end
                    return false
                end},
            },
        },
    },
    FighterProfessionIntro = {
        {--1
            Name = "Learn to Fight",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            NextStep = 2,
            EndDialogue = {"[Fighter Intro] Teach me to fight!","First you will need to acquire and equip a weapon if you have not already. Each type of weapon has its own set of powerful abilities in addition to their normal attacks. Speak to me again with a weapon equipped."},
            Requirements = {
                {"MaxTimesQuestDone", "FighterProfessionIntro", 1},
                --{"QuestActive", "FighterProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn to Fight",
            Instructions = "Equip [F2F5A9]a weapon[-] by double clicking it in your backpack or dragging it onto yourself in the character screen.",
            NextStep = 3,
            StartDialogue = {"[Fighter Intro] Teach me to fight!","First you will need to acquire and equip a weapon if you have not already. Each type of weapon has its own set of powerful abilities in addition to their normal attacks. Speak to me again with a weapon equipped."},
            Requirements = {
                {"MaxTimesQuestDone", "FighterProfessionIntro", 1},
                {"QuestActive", "FighterProfessionIntro", false},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local handObjects = {}
                    table.insert(handObjects, playerObj:GetEquippedObject("LeftHand"))
                    table.insert(handObjects, playerObj:GetEquippedObject("RightHand"))
                    local weapons = 0
                    for i = 1, #handObjects do
                        if ( handObjects[i]:HasObjVar("WeaponType") ) then
                            weapons = 1
                        end
                    end
                    if ( weapons == 0 ) then return false end
                    return true
                end},
            },
        },
        {--3
            Name = "Learn to Fight",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            NextStep = 4,
            EndDialogue = {"[Fighter Intro] How's this for a weapon?","I... guess it will have to do. Let's see if you know how to use it. First, enter combat. Your stance will change and your weapon will be at the ready. In this stance, you are ready to attack."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Learn to Fight",
            Instructions = "Press [F2F5A9]SPACEBAR[-] to toggle into combat stance.",
            NextStep = 5,
            Goals = {
                {"Custom", function(playerObj)
                    if ( not IsInCombat(playerObj) ) then return false end
                    return true
                end},
            },
        },
        {--5
            Name = "Learn to Fight",
            Instructions = "Press [F2F5A9]SPACEBAR[-] to toggle back into peaceful stance.",
            NextStep = 6,
            Goals = {
                {"Custom", function(playerObj)
                    if ( IsInCombat(playerObj) ) then return false end
                    return true
                end},
            },
        },
        {--6
            Name = "Learn to Fight",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            NextStep = 7,
            EndDialogue = {"[Fighter Intro] Can I hit things yet?","I thought you would never ask! Now that you have demonstrated you can enter and exit combat stance, you are ready to attack a victim! Enter combat stance once more and find your first target, maybe this training dummy over here. Make sure to get close enough!"},
            Goals = {
                {"TurnIn"},
            },
        },
        {--7
            Name = "Learn to Fight",
            Instructions = "Press [F2F5A9]SPACEBAR[-] to toggle into combat stance once more.",
            NextStep = 8,
            Goals = {
                {"Custom", function(playerObj)
                    if ( not IsInCombat(playerObj) ) then return false end
                    return true
                end},
            },
        },
        {--8
            Name = "Learn to Fight",
            Instructions = "Left click a [F2F5A9]Training Dummy[-] to target it. Your target will become highlighted.",
            NextStep = 9,
            Goals = {
                {"Custom", function(playerObj)
                    local target = playerObj:GetObjVar("CurrentTarget")
                    if ( not target
                    or not ValidCombatTarget(playerObj, target, true)
                    or IsMobileImmune(target)
                    or target:HasObjVar("Invulnerable") ) then
                        return false
                    end
                    return true
                end},
            },
        },
        {--9
            Name = "Learn to Fight",
            Instructions = "Move into range to automatically attack your target.",
            NextStep = 10,
            Goals = {
                {"Custom", function(playerObj)
                    if ( playerObj:GetTimerDelay(string.format("SWING_TIMER_%s", "RightHand")) 
                    or playerObj:GetTimerDelay(string.format("SWING_TIMER_%s", "LeftHand")) ) then
                        return true
                    end
                    return false
                end},
            },
        },
        {--10
            Name = "Learn to Fight",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-]",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            EndDialogue = {"[Fighter Intro] I feel powerful!","You have learned how to hurt things... good for you. Remember, in most situations you will also need a way to heal yourself, such as magic or bandages. Maybe the town healer could teach you."},
            Goals = {
                {"TurnIn"},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.TryStart(playerObj, "HealerProfessionIntro", 1)
                end},
            },
            OnStart = {
                {"Custom", function(playerObj)
                    local skill = GetPrimaryWeaponSkill(playerObj)
                    if ( skill ) then
                        local skillLevel = GetSkillLevel(playerObj, skill) or 0
                        if ( skillLevel < 30 ) then
                            if ( CanGainSkill( playerObj, skill, 30) ) then
                                SetSkillLevel( playerObj, skill, 30, true )
                            end
                        end
                    end
                    local skills = {"MartialProwessSkill", "MeleeSkill", "HealingSkill"}
                    for i = 1, #skills do
                        local skillLevel = GetSkillLevel(playerObj, skills[i]) or 0
                        if ( skillLevel < 30 ) then
                            if ( CanGainSkill( playerObj, skill, 30) ) then
                                SetSkillLevel( playerObj, skills[i], 30, true )
                            end
                        end
                    end
                end},
            },
        },
    },
    FighterProfessionTierOne = {
        {--1
            Name = "The Basics",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            NextStep = 2,
            EndDialogue = {"[Fighter I] Apprentice Fighter.","So you wish to become an Apprentice Fighter? You managed to put a few holes in that dummy over there, I wonder if you can help us out with a greater problem."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            Requirements = {
                {"MaxTimesQuestDone", "FighterProfessionTierOne", 1},
                --{"QuestActive", "FighterProfessionTierOne", false},
                {"MinTimesQuestDone", "FighterProfessionIntro", 1},
                {"Custom", function(playerObj)
                    local archery = GetSkillLevel(playerObj, "ArcherySkill") or 0
                    local brawling = GetSkillLevel(playerObj, "BrawlingSkill") or 0
                    local slashing = GetSkillLevel(playerObj, "SlashingSkill") or 0
                    local bashing = GetSkillLevel(playerObj, "BashingSkill") or 0
                    local lancing = GetSkillLevel(playerObj, "LancingSkill") or 0
                    local piercing = GetSkillLevel(playerObj, "PiercingSkill") or 0
                    local melee = GetSkillLevel(playerObj, "MeleeSkill") or 0
                    local prowess = GetSkillLevel(playerObj, "MartialProwessSkill") or 0
                    local healing = GetSkillLevel(playerObj, "HealingSkill") or 0
                    if ( (archery >= 30
                    or brawling >= 30
                    or slashing >= 30
                    or bashing >= 30
                    or lancing >= 30
                    or piercing >= 30)
                    and melee >= 30
                    and prowess >= 30
                    and healing >= 30 ) then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "The Basics",
            Instructions = "Enter [F2F5A9]The Sewers[-].",
            NextStep = 3,
            StartDialogue = {"[Fighter I] Apprentice Fighter.","So you wish to become an Apprentice Fighter? You've arrived at an opportune moment, we need help with an infestation in the town sewer. Lend a hand and we'll talk."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.SewerHelm,
                QuestMarkers.SewerEldeir,
                QuestMarkers.SewerPyros,
                QuestMarkers.SewerValus,
            },
            Requirements = {
                {"MaxTimesQuestDone", "FighterProfessionTierOne", 1},
                {"QuestActive", "FighterProfessionTierOne", false},
                {"MinTimesQuestDone", "FighterProfessionIntro", 1},
                {"Custom", function(playerObj)
                    local archery = GetSkillLevel(playerObj, "ArcherySkill") or 0
                    local slashing = GetSkillLevel(playerObj, "SlashingSkill") or 0
                    local brawling = GetSkillLevel(playerObj, "BrawlingSkill") or 0
                    local bashing = GetSkillLevel(playerObj, "BashingSkill") or 0
                    local lancing = GetSkillLevel(playerObj, "LancingSkill") or 0
                    local piercing = GetSkillLevel(playerObj, "PiercingSkill") or 0
                    local melee = GetSkillLevel(playerObj, "MeleeSkill") or 0
                    local prowess = GetSkillLevel(playerObj, "MartialProwessSkill") or 0
                    local healing = GetSkillLevel(playerObj, "HealingSkill") or 0
                    if ( (archery >= 30
                    or brawling >= 30
                    or slashing >= 30
                    or bashing >= 30
                    or lancing >= 30
                    or piercing >= 30 )
                    and melee >= 30
                    and prowess >= 30
                    and healing >= 30 ) then return true else return false end
                end},
            },
            Goals = {
                {"InRegion", "SewerDungeon", true},
            },
        },
        {--3
            Name = "The Basics",
            Instructions = "Hunt 20 [F2F5A9]Lurkers, Bats & Rats[-].",
            NextStep = 4,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.SewerHelm,
                QuestMarkers.SewerEldeir,
                QuestMarkers.SewerPyros,
                QuestMarkers.SewerValus,
            },
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "FighterProfessionTierOne3", QuestMobTargets.Vermin )
                end
                },
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount( playerObj, "FighterProfessionTierOne3", QuestMobTargets.Vermin, "Vermin", 20 )
                end
                },
            },
        },
        {--4
            Name = "The Basics",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            EndDialogue = {"[Fighter I] It is taken care of.","Fantastic news. You look like you can hold your own and the village is in search of hired swords. Seek out the Mission Dispatcher and return once you have completed a mission. Also ... take a bath"},
            NextStep = 5,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--5
            Name = "The Basics",
            Instructions = "Accept a mission from a [F2F5A9]mission dispatcher[-].",
            NextStep = 6,
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.MissionsValus,
                QuestMarkers.MissionsEldeir,
                QuestMarkers.MissionsHelm,
                QuestMarkers.MissionsOasis,
                QuestMarkers.MissionsPyros,
                QuestMarkers.MissionsBlackForest,
            },
            Goals = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("Missions")
                    if ( missions and next(missions) ) then return true else return false end
                end},
            },
        },
        {--6
            Name = "The Basics",
            Instructions = "Complete [F2F5A9]a mission[-].",
            NextStep = 7,
            Goals = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("LifetimePlayerStats")
                    if ( missions
                    and missions["NumberMissionCompleted"]
                    and missions["NumberMissionCompleted"] > 0 ) then
                        return true else return false
                    end
                end},
            },
        },
        {--7
            Name = "The Basics",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            EndDialogue = {"[Fighter I] I was victorious.","I am most impressed. You have certainly proven to me your acumen and ability as a fighter. As an officer of the Fighter Guild I hereby declare you an Apprentice Fighter."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        local weapons = {
                            LancingSkill = "weapon_spear_training",
                            PiercingSkill = "weapon_dagger_training",
                            BashingSkill = "weapon_mace_training",
                            SlashingSkill = "weapon_longsword_training",
                            ArcherySkill = "weapon_bow_training",
                        }
                        
                        local skill, level = GetHighestWeaponSkill(playerObj)
                        local template = nil
                        if ( skill and level and level > 0 and weapons[skill] ) then
                            template = weapons[skill]
                        else
                            local random = math.random(1,CountTable(weapons))
                            local count = 1
                            for skill, weapon in pairs(weapons) do
                                if ( count == random ) then
                                    template = weapon
                                end
                                count = count + 1
                            end
                        end
                        Create.InBackpack(template, playerObj)
                        local name = GetTemplateObjectName(template)
                        local str = "You have been rewarded with "
                        if ( name ) then
                            playerObj:SystemMessage(str..name..".","info")
                        end
                    end},
                },
            },
        },
    },
    FighterProfessionTierTwo = {
        {--1
            Name = "The Dead Rise",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            NextStep = 2,
            EndDialogue = {"[Fighter II] Journeyman Fighter.","The challenge of the Journeyman Fighter requires a perilous task and we live in perilous times. An evil is spreading through Celador. The unliving rise from our own graveyards and must be defeated."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            Requirements = {
                {"MaxTimesQuestDone", "FighterProfessionTierTwo", 1},
                --{"QuestActive", "FighterProfessionTierTwo", false},
                {"MinTimesQuestDone", "FighterProfessionTierOne", 1},
                {"Custom", function(playerObj)
                    local archery = GetSkillLevel(playerObj, "ArcherySkill") or 0
                    local brawling = GetSkillLevel(playerObj, "BrawlingSkill") or 0
                    local slashing = GetSkillLevel(playerObj, "SlashingSkill") or 0
                    local bashing = GetSkillLevel(playerObj, "BashingSkill") or 0
                    local lancing = GetSkillLevel(playerObj, "LancingSkill") or 0
                    local piercing = GetSkillLevel(playerObj, "PiercingSkill") or 0
                    local melee = GetSkillLevel(playerObj, "MeleeSkill") or 0
                    local prowess = GetSkillLevel(playerObj, "MartialProwessSkill") or 0
                    local healing = GetSkillLevel(playerObj, "HealingSkill") or 0
                    if ( (archery >= 50
                    or brawling >= 50
                    or slashing >= 50
                    or bashing >= 50
                    or lancing >= 50
                    or piercing >= 50) 
                    and melee >= 50
                    and prowess >= 50
                    and healing >= 50 ) then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "The Dead Rise",
            Instructions = "Travel to [F2F5A9]a graveyard[-].",
            NextStep = 3,
            StartDialogue = {"[Fighter II] Journeyman Fighter.","The challenge of the Journeyman Fighter requires a perilous task and we live in perilous times. An evil is spreading through Celador. The unliving rise from our own graveyards and must be defeated. "},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GraveyardHelm,
                QuestMarkers.GraveyardEldeir,
                QuestMarkers.GraveyardValus,
                QuestMarkers.GraveyardPyros,
            },
            Requirements = {
                {"MaxTimesQuestDone", "FighterProfessionTierTwo", 1},
                {"QuestActive", "FighterProfessionTierTwo", false},
                {"MinTimesQuestDone", "FighterProfessionTierOne", 1},
                {"Custom", function(playerObj)
                    local archery = GetSkillLevel(playerObj, "ArcherySkill") or 0
                    local slashing = GetSkillLevel(playerObj, "SlashingSkill") or 0
                    local brawling = GetSkillLevel(playerObj, "BrawlingSkill") or 0
                    local bashing = GetSkillLevel(playerObj, "BashingSkill") or 0
                    local lancing = GetSkillLevel(playerObj, "LancingSkill") or 0
                    local piercing = GetSkillLevel(playerObj, "PiercingSkill") or 0
                    local melee = GetSkillLevel(playerObj, "MeleeSkill") or 0
                    local prowess = GetSkillLevel(playerObj, "MartialProwessSkill") or 0
                    local healing = GetSkillLevel(playerObj, "HealingSkill") or 0
                    if ( (archery >= 50
                    or brawling >= 50
                    or slashing >= 50
                    or bashing >= 50
                    or lancing >= 50
                    or piercing >= 50) 
                    and melee >= 50
                    and prowess >= 50
                    and healing >= 50 ) then return true else return false end
                end},
            },
            Goals = {
                {"InRegion", "Graveyard", true},
            },
        },
        {--3
            Name = "The Dead Rise",
            Instructions = "Fight back the [F2F5A9]undead[-].",
            NextStep = 4,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GraveyardHelm,
                QuestMarkers.GraveyardEldeir,
                QuestMarkers.GraveyardValus,
                QuestMarkers.GraveyardPyros,
            },
            OnStart={
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "FighterProfessionTierTwo3", QuestMobTargets.Undead)
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "FighterProfessionTierTwo3", QuestMobTargets.Undead, "Undead", 20)
                end},
            },
        },
        {--4
            Name = "The Dead Rise",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            EndDialogue = {"[Fighter II] I defeated the Undead.","Good job, but I must ask you to complete an additional task.  The unliving have masters. Seek one out and vanquish him."},
            NextStep = 5,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--5
            Name = "The Dead Rise",
            Instructions = "Hunt a [F2F5A9]Lich[-].",
            NextStep = 6,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.LichArea,
                QuestMarkers.AbandonedShrine,
                QuestMarkers.HauntedRuins,
                QuestMarkers.LichRuins,
                QuestMarkers.ValusCemetaryPeak,
            },
            OnStart={
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "FighterProfessionTierTwo5", QuestMobTargets.Lich)
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "FighterProfessionTierTwo5", QuestMobTargets.Lich, "Lich", 1)
                end},
            },
        },
        {--6
            Name = "The Dead Rise",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            EndDialogue = {"[Fighter II] I defeated a Lich.","Good job. Take this book and bring it to Guildmaster Kato."},
            NextStep = 7,
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","prestige_journeyman_fighter"},
                },
            },
        },
        {--7
            Name = "The Dead Rise",
            Instructions = "Speak to [F2F5A9]Guildmaster Kato[-] at [F2F5A9]Eldeir[-] training grounds.",
            EndDialogue = {"[Fighter II] I wish to become a Journeyman Fighter.","As the Guildmaster of the Fighter Guild I hereby bestow upon you the rank of Journeyman."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GuildmasterFighter,
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_journeyman_fighter"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Journeyman Fighter abilities.","info")
                    end, "Journeyman Fighter Abilities"},
                },                
            },
        },
    },
    FighterProfessionTierThree = {
        {--1
            Name = "Road to Ruin",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            NextStep = 2,
            EndDialogue = {"[Fighter III] Master Fighter.","Another Journeyman huh. You should take my advice and quit whilst you're ahead. No? Well head to Dungeon Ruin and seek out Bladius Dart - he'll break the good news."},
            Requirements = {
                {"MaxTimesQuestDone", "FighterProfessionTierThree", 1},
                --{"QuestActive", "FighterProfessionTierThree", false},
                {"MinTimesQuestDone", "FighterProfessionTierTwo", 1},
                {"Custom", function(playerObj)
                    local archery = GetSkillLevel(playerObj, "ArcherySkill") or 0
                    local brawling = GetSkillLevel(playerObj, "BrawlingSkill") or 0
                    local slashing = GetSkillLevel(playerObj, "SlashingSkill") or 0
                    local bashing = GetSkillLevel(playerObj, "BashingSkill") or 0
                    local lancing = GetSkillLevel(playerObj, "LancingSkill") or 0
                    local piercing = GetSkillLevel(playerObj, "PiercingSkill") or 0
                    local melee = GetSkillLevel(playerObj, "MeleeSkill") or 0
                    local prowess = GetSkillLevel(playerObj, "MartialProwessSkill") or 0
                    local healing = GetSkillLevel(playerObj, "HealingSkill") or 0
                    if ( (archery >= 80
                    or brawling >= 80
                    or slashing >= 80
                    or bashing >= 80
                    or lancing >= 80
                    or piercing >= 80) 
                    and melee >= 80
                    and prowess >= 80
                    and healing >= 80 ) then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Road to Ruin",
            Instructions = "Speak with [F2F5A9]Bladius Dart[-] outside [F2F5A9]Ruin[-].",
            NextStep = 3,
            StartDialogue = {"[Fighter III] Master Fighter.","Another Journeyman huh. You should take my advice and quit whilst you're ahead. No? Well head to Dungeon Ruin and seek out Bladius Dart - he'll break the good news."},
            Requirements = {
                {"MaxTimesQuestDone", "FighterProfessionTierThree", 1},
                {"QuestActive", "FighterProfessionTierThree", false},
                {"MinTimesQuestDone", "FighterProfessionTierTwo", 1},
                {"Custom", function(playerObj)
                    local archery = GetSkillLevel(playerObj, "ArcherySkill") or 0
                    local brawling = GetSkillLevel(playerObj, "BrawlingSkill") or 0
                    local slashing = GetSkillLevel(playerObj, "SlashingSkill") or 0
                    local bashing = GetSkillLevel(playerObj, "BashingSkill") or 0
                    local lancing = GetSkillLevel(playerObj, "LancingSkill") or 0
                    local piercing = GetSkillLevel(playerObj, "PiercingSkill") or 0
                    local melee = GetSkillLevel(playerObj, "MeleeSkill") or 0
                    local prowess = GetSkillLevel(playerObj, "MartialProwessSkill") or 0
                    local healing = GetSkillLevel(playerObj, "HealingSkill") or 0
                    if ( (archery >= 80
                    or brawling >= 80
                    or slashing >= 80
                    or bashing >= 80
                    or lancing >= 80
                    or piercing >= 80) 
                    and melee >= 80
                    and prowess >= 80
                    and healing >= 80 ) then return true else return false end
                end},
            },
        },
        {--3
            Name = "Road to Ruin",
            Instructions = "Speak with [F2F5A9]Bladius Dart[-] outside [F2F5A9]Ruin[-].",
            NextStep = 4,
            EndDialogue = {"[Fighter III] You sent for me.","Dark forces are at work here. These halls are rife with forsaken souls. Travel into the depths of Dungeon Ruin so that we might learn what evil brings this menace upon us."},
            MarkerLocs = {
                --QuestMarkers.BladiusDart,
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperSouthernHills,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Road to Ruin",
            Instructions = "Enter [F2F5A9]Ruin[-]",
            NextStep = 5,
            Goals = {
                {"InRegion", "Ruin: Level 1", true},
            },
        },
        {--5
            Name = "Road to Ruin",
            Instructions = "Hunt 10 [F2F5A9]Liches[-].",
            NextStep = 6,
            OnStart = {
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "FighterProfessionTierThree5", "ruin_lich")
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "FighterProfessionTierThree5", "ruin_lich", "Lich", 10)
                end},
            },
        },
        {--6
            Name = "Road to Ruin",
            Instructions = "Descend to the [F2F5A9]lower level[-].",
            NextStep = 7,
            Goals = {
                {"InRegion", "Ruin: Level 2", true},
            },
        },
        {--7
            Name = "Road to Ruin",
            Instructions = "Hunt 15 [F2F5A9]Mummies[-].",
            NextStep = 8,
            OnStart = {
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "FighterProfessionTierThree7", "ruin_mummy")
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "FighterProfessionTierThree7", "ruin_mummy", "Mummy", 15)
                end},
            },
        },
        {--8
            Name = "Road to Ruin",
            Instructions = "Hunt 3 [F2F5A9]Lich Lords[-].",
            NextStep = 9,
            OnStart = {
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "FighterProfessionTierThree8", "ruin_lich_lord")
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "FighterProfessionTierThree8","ruin_lich_lord", "Lich Lord", 3)
                end},
            },
        },
        {--9
            Name = "Road to Ruin",
            Instructions = "Slay a [F2F5A9]Crypt Dragon[-].",
            NextStep = 10,
            OnStart = {
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "FighterProfessionTierThree9", "zombie_dragon")
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "FighterProfessionTierThree9","zombie_dragon", "Crypt Dragon", 1)
                end},
            },
        },
        {--10
            Name = "Road to Ruin",
            Instructions = "Speak to [F2F5A9]Bladius Dart[-].",
            MarkerLocs = {
                --QuestMarkers.BladiusDart,
                QuestMarkers.RuinExit,
            },
            NextStep = 11,
            EndDialogue = {"[Fighter III] It is done.","A Crypt Dragon? The source of this evil must be uncovered and defeated. The Fighter's Guild will be sure to receive a commendation of your valor in my report. Return to the city speak with a trainer."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--11
            Name = "Road to Ruin",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            NextStep = 12,
            EndDialogue = {"[Fighter III] I have returned.","Word has reached us of your acts and we are assured of your ability. One final challenge awaits you. Deep within the Black Forest lies an ancient champion. Defeat him to earn the Tome of the Master Fighter and present it to Guildmaster Kato."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--12
            Name = "Road to Ruin",
            Instructions = "Get a [F2F5A9]Tome: Master Fighter[-]. These can be traded for or found from killing [F2F5A9]Lord Barkus[-] in [F2F5A9]Black Forest[-] swamp.",
            Goals = {
                {"HasItemInBackpack","prestige_master_fighter"},
            },
            NextStep = 13,
        },
        {--13
            Name = "Road to Ruin",
            Instructions = "Bring a [F2F5A9]Tome: Master Fighter[-] to [F2F5A9]Guildmaster Kato[-] at [F2F5A9]Eldeir[-] training grounds.",
            EndDialogue = {"[Fighter III] I wish to become a Master Fighter.","As the Guildmaster of the Fighter Guild I hereby bestow upon you the rank of Master."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GuildmasterFighter,
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_master_fighter"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Tier Master Fighter abilities.","info")
                    end, "Master Fighter Abilities"},
                },                
            },
        },
    },
    FighterProfessionTierFour = {
        {--1
            Name = "The Catacombs",
            Instructions = "Speak to a [F2F5A9]fighter trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.FighterPyros,
                QuestMarkers.FighterHelm,
                QuestMarkers.FighterEldeir,
                QuestMarkers.FighterValus,
            },
            NextStep = 2,
            EndDialogue = {"[Fighter IV] Tell me how to become a Grandmaster.","There is not much I can tell you. Go speak with Il-Ves in The Catacombs."},
            Requirements = {
                {"MaxTimesQuestDone", "FighterProfessionTierFour", 1},
                --{"QuestActive", "FighterProfessionTierFour", false},
                {"MinTimesQuestDone", "FighterProfessionTierThree", 1},
                {"Custom", function(playerObj)
                    local archery = GetSkillLevel(playerObj, "ArcherySkill") or 0
                    local brawling = GetSkillLevel(playerObj, "BrawlingSkill") or 0
                    local slashing = GetSkillLevel(playerObj, "SlashingSkill") or 0
                    local bashing = GetSkillLevel(playerObj, "BashingSkill") or 0
                    local lancing = GetSkillLevel(playerObj, "LancingSkill") or 0
                    local piercing = GetSkillLevel(playerObj, "PiercingSkill") or 0
                    local melee = GetSkillLevel(playerObj, "MeleeSkill") or 0
                    local prowess = GetSkillLevel(playerObj, "MartialProwessSkill") or 0
                    local healing = GetSkillLevel(playerObj, "HealingSkill") or 0
                    if ( (archery >= 100
                    or brawling >= 100
                    or slashing >= 100
                    or bashing >= 100
                    or lancing >= 100
                    or piercing >= 100) 
                    and melee >= 100
                    and prowess >= 100
                    and healing >= 100 ) then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "The Catacombs",
            Instructions = "Travel to [F2F5A9]The Catacombs[-].",
            NextStep = 3,
            StartDialogue = {"[Fighter IV] Tell me how to become a Grandmaster.","There is not much I can tell you. Go speak with Il-Ves in The Catacombs."},
            Requirements = {
                {"MaxTimesQuestDone", "FighterProfessionTierFour", 1},
                {"QuestActive", "FighterProfessionTierFour", false},
                {"MinTimesQuestDone", "FighterProfessionTierThree", 1},
                {"Custom", function(playerObj)
                    local archery = GetSkillLevel(playerObj, "ArcherySkill") or 0
                    local brawling = GetSkillLevel(playerObj, "BrawlingSkill") or 0
                    local slashing = GetSkillLevel(playerObj, "SlashingSkill") or 0
                    local bashing = GetSkillLevel(playerObj, "BashingSkill") or 0
                    local lancing = GetSkillLevel(playerObj, "LancingSkill") or 0
                    local piercing = GetSkillLevel(playerObj, "PiercingSkill") or 0
                    local melee = GetSkillLevel(playerObj, "MeleeSkill") or 0
                    local prowess = GetSkillLevel(playerObj, "MartialProwessSkill") or 0
                    local healing = GetSkillLevel(playerObj, "HealingSkill") or 0
                    if ( (archery >= 100
                    or brawling >= 100
                    or slashing >= 100
                    or bashing >= 100
                    or lancing >= 100
                    or piercing >= 100) 
                    and melee >= 100
                    and prowess >= 100
                    and healing >= 100 ) then return true else return false end
                end},
            },
        },
        {--3
            Name = "The Catacombs",
            Instructions = "Travel to [F2F5A9]The Catacombs[-].",
            NextStep = 4,
            Goals = {
                {"InRegion", "FollowerHubRegion", true},
            },
        },
        {--4
            Name = "The Catacombs",
            Instructions = "Speak to [F2F5A9]Il-Ves[-].",
            NextStep = 5,
            EndDialogue = {"[Fighter IV] Tell me how to become a Grandmaster.","You may earn that rank by proving your abilities down below. Go kill the Void Channeler."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--5
            Name = "The Catacombs",
            Instructions = "Kill the [F2F5A9]Void Channeler[-] within [F2F5A9]The Catacombs[-].",
            NextStep = 6,
            Goals = {
                {"Custom", function(playerObj)
                    local mob = FindObjects(SearchMulti(
                    {
                        SearchObjectInRange(30),
                        SearchTemplate("quest_fighter_mob"),
                        SearchObjVar("CurrentState", "Dead"),
                    }))
                    if ( mob and next(mob) ) then return true else return false end
                end},
            },
        },
        {--6
            Name = "The Catacombs",
            Instructions = "Kill the Void Guardian, [F2F5A9]Cerberus[-].",
            NextStep = 7,
            Goals = {
                {"Custom", function(playerObj)
                    local mob = FindObjects(SearchMulti(
                    {
                        SearchObjectInRange(30),
                        SearchTemplate("cerberus"),
                        SearchObjVar("CurrentState", "Dead"),
                    }))
                    if ( mob and next(mob) ) then return true else return false end
                end},
            },
        },
        {--7
            Name = "The Catacombs",
            Instructions = "Kill [F2F5A9]Death[-].",
            NextStep = 8,
            Goals = {
                {"Custom", function(playerObj)
                    local mob = FindObjects(SearchMulti(
                    {
                        SearchObjectInRange(30),
                        SearchTemplate("grim_reaper"),
                        SearchObjVar("CurrentState", "Dead"),
                    }))
                    if ( mob and next(mob) ) then return true else return false end
                end},
            },
        },
        {--8
            Name = "The Catacombs",
            Instructions = "Speak to [F2F5A9]Per-Thuas[-] in the [F2F5A9]Cathedral,[-] the top floor of [F2F5A9]The Catacombs.[-]",
            NextStep = 9,
            EndDialogue = {"[Fighter IV] Killed Death.","I watched the battles from here! That was the last test of strength, now go get a Grandmaster Book from an Awakening in the Barrens. Presenting that to a Guildmaster prove your certification to become a Grandmaster."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--9
            Name = "Road to Ruin",
            Instructions = "Get a [F2F5A9]Tome: Grandmaster Fighter[-]. These can be traded for or found from [F2F5A9]Barren Lands[-] awakenings.",
            NextStep = 10,
            Goals = {
                {"HasItemInBackpack","prestige_grandmaster_fighter"},
            },
        },
        {--10
            Name = "The Catacombs",
            Instructions = "Bring a [F2F5A9]Tome: Grandmaster Fighter[-] to [F2F5A9]Guildmaster Kato[-] at [F2F5A9]Eldeir[-] training grounds.",
            EndDialogue = {"[Fighter IV] I'm ready to be a Grandmaster Fighter.","I have heard of your accomplishments. As the Guildmaster of the Fighter Guild I hereby bestow upon you the rank of Grandmaster."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GuildmasterFighter,
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_grandmaster_fighter"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Grandmaster Fighter abilities.","info")
                    end, "Grandmaster Fighter Abilities"},      
                },                
            },
        },
    },
    MageProfessionIntro = {
        {--1
            Name = "Learn Magic",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            NextStep = 2,
            EndDialogue = {"[Mage Intro] Can you teach me magic?","I can teach you once you have gathered the required materials. First, obtain a spellbook. A spellbook keeps all of a mage's known spells safely within, and stays with the mage even through death."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Requirements = {
                {"MaxTimesQuestDone", "MageProfessionIntro", 1},
                --{"QuestActive", "MageProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn Magic",
            Instructions = "Get a [F2F5A9]Spellbook[-]. You can buy them from scribes.",
            NextStep = 3,
            StartDialogue = {"[Mage Intro] Can you teach me magic?","I can teach you once you have gathered the required materials. First, obtain a spellbook. A spellbook keeps all of a mage's known spells safely within, and stays with the mage even through death."},
            Requirements = {
                {"MaxTimesQuestDone", "MageProfessionIntro", 1},
                {"QuestActive", "MageProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill", "MagicAffinitySkill", 30},
                {"SetSkill", "ChannelingSkill", 30},
                {"SetSkill", "MagerySkill", 30},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if ( CountItemsInContainer(playerObj, "spellbook") > 0
                    or CountItemsInContainer(playerObj, "spellbook_noob") > 0
                    or CountItemsInContainer(playerObj, "spellbook_full") > 0 ) then
                        return true
                    end
                    return false
                end},
            },
        },
        {--3
            Name = "Learn Magic",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            NextStep = 4,
            EndDialogue = {"[Mage Intro] I have a spellbook.","It looks a bit light, but you can fill it with spell scrolls over time. Open it up and take a look at which spells are inside. When you are finished, bring me 10 Lemongrass. They are sold by alchemists."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Learn Magic",
            Instructions = "Get 10 [F2F5A9]Lemongrass[-]. It is sold by alchemists and herbalists.",
            NextStep = 5,
            Goals = {
                {"HasItemInBackpack","ingredient_lemongrass"},
            },
        },
        {--5
            Name = "Learn Magic",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            NextStep = 6,
            EndDialogue = {"[Mage Intro] I come bearing lemongrass.","Mmm... smells fresh, like lemons... and feet. You finally have all you need to cast a spell. Go into your spellbook and retrieve the Mana Missile spell. Make sure you can easily access it. Use the spell to begin casting. Then unleash it on one of those training dummies outside."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--6
            Name = "Learn Magic",
            Instructions = "Cast [F2F5A9]Mana Missile[-]. You can find this spell in your spellbook. Click the name of the spell to view its details. You can drag the spell icon onto your hotbar.",
            NextStep = 7,
            Goals = {
                {"Custom", function(playerObj)
                    local primedSpell = playerObj:GetObjVar("PrimedSpell")
                    if ( not primedSpell or primedSpell ~= "ManaMissile" ) then
                        return false
                    end
                    return true
                end},
            },
        },
        {--7
            Name = "Learn Magic",
            Instructions = "Target the [F2F5A9]Training Dummy[-] with your mana missile spell by left clicking the dummy.",
            NextStep = 8,
            Goals = {
                {"Custom", function(playerObj)
                    local primedSpell = playerObj:GetObjVar("PrimedSpell")
                    if ( primedSpell and primedSpell == "ManaMissile" ) then
                        return false
                    end
                    return true
                end},
            },
        },
        {--8
            Name = "Learn Magic",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            EndDialogue = {"[Mage Intro] I did it! I think?","There are varying difficulties of spells, all requiring different skill levels to cast successfully. Spells can fizzle out if your technique falters. Each attempt consumes a spell reagent, so don't eat them! Magic is tough but it gets easier!"},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item", "ingredient_lemongrass", 100},
                    {"Item", "ingredient_mushroom", 100},
                    {"Item", "ingredient_moss", 100},
                    {"Item", "ingredient_ginsengroot", 100},
                },
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.TryStart(playerObj, "HealerProfessionIntro", 1)
                end},
            },
        },
    },
    MageProfessionTierOne = {
        {--1
            Name = "You're a Wizard",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            NextStep = 2,
            EndDialogue = {"[Mage I] Apprentice Mage.","So you wish to become an Apprentice Mage? You managed to put a few holes in that dummy over there, I wonder if you can help us out with a greater problem."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Requirements = {
                {"MaxTimesQuestDone", "MageProfessionTierOne", 1},
                --{"QuestActive", "MageProfessionTierOne", false},
                {"MinTimesQuestDone", "MageProfessionIntro", 1},
                {"Custom", function(playerObj)
                    local manifestation = GetSkillLevel(playerObj, "MagerySkill") or 0
                    local magicaffinity = GetSkillLevel(playerObj, "MagicAffinitySkill") or 0
                    local channeling = GetSkillLevel(playerObj, "ChannelingSkill") or 0
                    if ( manifestation >= 30
                    and magicaffinity >= 30
                    and channeling >= 30 ) then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "You're a Wizard",
            Instructions = "Enter [F2F5A9]The Sewers[-].",
            NextStep = 3,
            StartDialogue = {"[Mage I] Apprentice Mage.","So you wish to become an Apprentice Mage? You've arrived at an opportune moment, we need help with an infestation in the town sewer. Lend a hand and we'll talk."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Requirements = {
                {"MaxTimesQuestDone", "MageProfessionTierOne", 1},
                {"QuestActive", "MageProfessionTierOne", false},
                {"MinTimesQuestDone", "MageProfessionIntro", 1},
                {"Custom", function(playerObj)
                    local manifestation = GetSkillLevel(playerObj, "MagerySkill") or 0
                    local magicaffinity = GetSkillLevel(playerObj, "MagicAffinitySkill") or 0
                    local channeling = GetSkillLevel(playerObj, "ChannelingSkill") or 0
                    if ( manifestation >= 30
                    and magicaffinity >= 30
                    and channeling >= 30 ) then return true else return false end
                end},
            },
            Goals = {
                {"InRegion", "SewerDungeon", true},
            },
        },
        {--3
            Name = "You're a Wizard",
            Instructions = "Hunt 20 [F2F5A9]Lurkers, Bats & Rats[-].",
            NextStep = 4,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.SewerHelm,
                QuestMarkers.SewerEldeir,
                QuestMarkers.SewerPyros,
                QuestMarkers.SewerValus,
            },
            OnStart = {
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "MageProfessionTierThree3", QuestMobTargets.Vermin)
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "MageProfessionTierThree3",QuestMobTargets.Vermin, "Vermin", 15)
                end},
            },
        },
        {--4
            Name = "You're a Wizard",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            EndDialogue = {"[Mage I] It is taken care of.","Fantastic news. You look like you can hold your own and the village is in search of hired swords. Seek out the Mission Dispatcher and return once you have completed a mission. Also ... take a bath."},
            NextStep = 5,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--5
            Name = "You're a Wizard",
            Instructions = "Accept a mission from a [F2F5A9]mission dispatcher[-].",
            NextStep = 6,
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.MissionsValus,
                QuestMarkers.MissionsEldeir,
                QuestMarkers.MissionsHelm,
                QuestMarkers.MissionsOasis,
                QuestMarkers.MissionsPyros,
                QuestMarkers.MissionsBlackForest,
            },
            Goals = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("Missions")
                    if ( missions and next(missions) ) then return true else return false end
                end},
            },
        },
        {--6
            Name = "You're a Wizard",
            Instructions = "Complete [F2F5A9]a mission[-].",
            NextStep = 7,
            Goals = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("LifetimePlayerStats")
                    if ( missions
                    and missions["NumberMissionCompleted"]
                    and missions["NumberMissionCompleted"] > 0 ) then
                        return true else return false
                    end
                end},
            },
        },
        {--7
            Name = "You're a Wizard",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            EndDialogue = {"[Mage I] I was victorious.","I am most impressed. You have certainly proven to me your acumen and ability as a mage. As an officer of the Mage Guild I hereby declare you an Apprentice Mage."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","attuned_staff_training"},
                },
            },
        },
    },
    MageProfessionTierTwo = {
        {--1
            Name = "Day of the Dead",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            NextStep = 2,
            EndDialogue = {"[Mage II] Journeyman Mage.","The challenge of the Journeyman Mage requires a perilous task and we live in perilous times. An evil is spreading through Celador. The unliving rise from our own graveyards and must be defeated."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Requirements = {
                {"MaxTimesQuestDone", "MageProfessionTierTwo", 1},
                --{"QuestActive", "MageProfessionTierTwo", false},
                {"MinTimesQuestDone", "MageProfessionTierOne", 1},
                {"Custom", function(playerObj)
                    local manifestation = GetSkillLevel(playerObj, "MagerySkill") or 0
                    local magicaffinity = GetSkillLevel(playerObj, "MagicAffinitySkill") or 0
                    local channeling = GetSkillLevel(playerObj, "ChannelingSkill") or 0
                    if ( manifestation >= 50
                    and magicaffinity >= 50
                    and channeling >= 50 ) then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Day of the Dead",
            Instructions = "Travel to [F2F5A9]a graveyard[-].",
            NextStep = 3,
            StartDialogue = {"[Mage II] Journeyman Mage.","The challenge of the Journeyman Mage requires a perilous task and we live in perilous times. An evil is spreading through Celador. The unliving rise from our own graveyards and must be defeated. "},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GraveyardHelm,
                QuestMarkers.GraveyardEldeir,
                QuestMarkers.GraveyardValus,
                QuestMarkers.GraveyardPyros,
            },
            Requirements = {
                {"MaxTimesQuestDone", "MageProfessionTierTwo", 1},
                {"QuestActive", "MageProfessionTierTwo", false},
                {"MinTimesQuestDone", "MageProfessionTierOne", 1},
                {"Custom", function(playerObj)
                    local manifestation = GetSkillLevel(playerObj, "MagerySkill") or 0
                    local magicaffinity = GetSkillLevel(playerObj, "MagicAffinitySkill") or 0
                    local channeling = GetSkillLevel(playerObj, "ChannelingSkill") or 0
                    if ( manifestation >= 50
                    and magicaffinity >= 50
                    and channeling >= 50 ) then return true else return false end
                end},
            },
            Goals = {
                {"InRegion", "Graveyard", true},
            },
        },
        {--3
            Name = "Day of the Dead",
            Instructions = "Fight back the [F2F5A9]undead[-].",
            NextStep = 4,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GraveyardHelm,
                QuestMarkers.GraveyardEldeir,
                QuestMarkers.GraveyardValus,
                QuestMarkers.GraveyardPyros,
            },
            OnStart = {
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "MageProfessionTierTwo3", QuestMobTargets.Undead)
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "MageProfessionTierTwo3",QuestMobTargets.Undead, "Undead", 20)
                end},
            },
        },
        {--4
            Name = "Day of the Dead",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            EndDialogue = {"[Mage II] I defeated the Undead.","Good job, but I must ask you to complete an additional task. The unliving have masters. Seek one out and vanquish him."},
            NextStep = 5,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--5
            Name = "Day of the Dead",
            Instructions = "Hunt a [F2F5A9]Lich[-].",
            NextStep = 6,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.LichArea,
                QuestMarkers.AbandonedShrine,
                QuestMarkers.HauntedRuins,
                QuestMarkers.LichRuins,
                QuestMarkers.ValusCemetaryPeak,
            },
            OnStart = {
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "MageProfessionTierTwo5", "lich")
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "MageProfessionTierTwo5","lich", "Undead", 1)
                end},
            },
        },
        {--6
            Name = "Day of the Dead",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            EndDialogue = {"[Mage II] I defeated a Lich.","You are most valiant! Here, take this book as evidence of your knowledge and experience. Bring it to the Mage Guildmaster Magica in the city of Valus."},
            NextStep = 7,
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","prestige_journeyman_mage"},
                },
            },
        },
        {--7
            Name = "Day of the Dead",
            Instructions = "Speak to [F2F5A9]Guildmaster Magica[-] at [F2F5A9]Valus[-] or [F2F5A9]Oasis[-] mage tower.",
            EndDialogue = {"[Mage II] I wish to become a Journeyman Mage.","As the Guildmaster of the Mage Guild I hereby bestow upon you the rank of Journeyman."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GuildmasterMage,
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_journeyman_mage"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Tier I Mage abilities.","info")
                    end, "Tier I Mage Abilities"},
                },                
            },
        },
    },
    MageProfessionTierThree = {
        {--1
            Name = "Beneath Contempt",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                --QuestMarkers.MagePyros,
                --QuestMarkers.MageHelm,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            NextStep = 2,
            EndDialogue = {"[Mage III] Master Mage.","The trial of the Journeyman Mage awaits you. Travel to Dungeon Contempt and seek out Vesper Wolf."},
            Requirements = {
                {"MaxTimesQuestDone", "MageProfessionTierThree", 1},
                --{"QuestActive", "MageProfessionTierThree", false},
                {"MinTimesQuestDone", "MageProfessionTierTwo", 1},
                {"Custom", function(playerObj)
                    local manifestation = GetSkillLevel(playerObj, "MagerySkill") or 0
                    local magicaffinity = GetSkillLevel(playerObj, "MagicAffinitySkill") or 0
                    local channeling = GetSkillLevel(playerObj, "ChannelingSkill") or 0
                    if ( manifestation >= 80
                    and magicaffinity >= 80
                    and channeling >= 80 ) then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Beneath Contempt",
            Instructions = "Speak with [F2F5A9]Vesper Wolf[-] outside [F2F5A9]Contempt[-].",
            NextStep = 3,
            StartDialogue = {"[Mage III] Master Mage.","The trial of the Journeyman Mage awaits you. Travel to Dungeon Contempt and seek out Vesper Wolf."},
            Requirements = {
                {"MaxTimesQuestDone", "MageProfessionTierThree", 1},
                {"QuestActive", "MageProfessionTierThree", false},
                {"MinTimesQuestDone", "MageProfessionTierTwo", 1},
                {"Custom", function(playerObj)
                    local manifestation = GetSkillLevel(playerObj, "MagerySkill") or 0
                    local magicaffinity = GetSkillLevel(playerObj, "MagicAffinitySkill") or 0
                    local channeling = GetSkillLevel(playerObj, "ChannelingSkill") or 0
                    if ( manifestation >= 80
                    and magicaffinity >= 80
                    and channeling >= 80 ) then return true else return false end
                end},
            },
        },
        {--3
            Name = "Beneath Contempt",
            Instructions = "Speak with [F2F5A9]Vesper Wolf[-] outside [F2F5A9]Contempt[-].",
            NextStep = 4,
            EndDialogue = {"[Mage III] You sent for me.","Well met. The forest groans in the shadow of this vile place. Orks have made home here...and much worse. Travel into the depths of Dungeon Contempt and defeat it's evil."},
            MarkerLocs = {
                --QuestMarkers.VesperWolf,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperCrossroads,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Beneath Contempt",
            Instructions = "Enter [F2F5A9]Contempt[-].",
            NextStep = 5,
            Goals = {
                {"InRegion", "Contempt: Level 1", true},
            },
        },
        {--5
            Name = "Beneath Contempt",
            Instructions = "Hunt 15 [F2F5A9]Orks[-].",
            NextStep = 6,
            OnStart = {
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "MageProfessionTierThree5", QuestMobTargets.Ork)
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "MageProfessionTierThree5",QuestMobTargets.Ork, "Ork", 15)
                end},
            },
        },
        {--6
            Name = "Beneath Contempt",
            Instructions = "Descend to the [F2F5A9]lower level[-].",
            NextStep = 7,
            Goals = {
                {"InRegion", "Contempt: Level 2", true},
            },
        },
        {--7
            Name = "Beneath Contempt",
            Instructions = "Hunt 15 [F2F5A9]Stone Harpies and Wargs[-].",
            NextStep = 8,
            OnStart = {
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "MageProfessionTierThree7", QuestMobTargets.Contempt)
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "MageProfessionTierThree7", QuestMobTargets.Contempt, "Beasts", 15)
                end},
            },
        },
        {--8
            Name = "Beneath Contempt",
            Instructions = "Hunt 5 [F2F5A9]Ogres[-].",
            NextStep = 9,
            OnStart = {
                {"Custom", function(playerObj)
                    return Quests.InitTrackMobileKillCount(playerObj, "MageProfessionTierThree8", { "contempt_ogre", "ogre" })
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    return Quests.RunTrackMobileKillCount(playerObj, "MageProfessionTierThree8",{ "contempt_ogre", "ogre" }, "Ogres", 5)
                end},
            },
        },
        {--9
            Name = "Beneath Contempt",
            Instructions = "Speak to [F2F5A9]Vesper Wolf[-].",
            MarkerLocs = {
                --QuestMarkers.VesperWolf,
                QuestMarkers.ContemptExit,
            },
            NextStep = 10,
            EndDialogue = {"[Mage III] I have returned.","You have performed valiantly. The Mage Guild will be sure to receive a commendation of your valor in my report. Return to the city speak with a trainer."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--10
            Name = "Beneath Contempt",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                --QuestMarkers.MagePyros,
                --QuestMarkers.MageHelm,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            NextStep = 11,
            EndDialogue = {"[Mage III] I have returned.","Word has reached us of your acts and we are assured of your ability. One final challenge awaits you. Deep within the Black Forest lies an ancient champion. Defeat him to earn the Tome of the Master Mage and present it to Guildmaster Magica."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--11
            Name = "Beneath Contempt",
            Instructions = "Get a [F2F5A9]Tome: Master Mage[-]. These can be traded for or found from killing [F2F5A9]Lord Barkus[-] in [F2F5A9]Black Forest[-] swamp.",
            NextStep = 12,
            Goals = {
                {"HasItemInBackpack","prestige_master_mage"},
            },
        },
        {--12
            Name = "Beneath Contempt",
            Instructions = "Bring a [F2F5A9]Tome: Master Mage[-] to [F2F5A9]Guildmaster Magica[-] at [F2F5A9]Valus[-] or [F2F5A9]Oasis[-] mage tower.",
            EndDialogue = {"[Mage III] I wish to become a Master Mage.","As the Guildmaster of the Mage Guild I hereby bestow upon you the rank of Master."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GuildmasterMage,
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_master_mage"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Tier Master Mage abilities.","info")
                    end, "Master Mage Abilities"},
                },                
            },
        },
    },
    MageProfessionTierFour = {
        {--1
            Name = "The Catacombs",
            Instructions = "Speak to a [F2F5A9]mage trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                --QuestMarkers.MagePyros,
                --QuestMarkers.MageHelm,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            NextStep = 2,
            EndDialogue = {"[Mage IV] I am ready to become a Grandmaster.","I am pleased to see you progress this far. Enter the Catacombs and speak with Ali-Larus for your final test."},
            Requirements = {
                {"MaxTimesQuestDone", "MageProfessionTierFour", 1},
                --{"QuestActive", "MageProfessionTierFour", false},
                {"MinTimesQuestDone", "MageProfessionTierThree", 1},
                {"Custom", function(playerObj)
                    local manifestation = GetSkillLevel(playerObj, "MagerySkill") or 0
                    local magicaffinity = GetSkillLevel(playerObj, "MagicAffinitySkill") or 0
                    local channeling = GetSkillLevel(playerObj, "ChannelingSkill") or 0
                    if ( manifestation >= 100
                    and magicaffinity >= 100
                    and channeling >= 100 ) then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "The Catacombs",
            Instructions = "Travel to [F2F5A9]The Catacombs[-].",
            NextStep = 3,
            StartDialogue = {"[Mage IV] I am ready to become a Grandmaster.","I am pleased to see you progress this far. Enter the Catacombs for your final test."},
            Requirements = {
                {"MaxTimesQuestDone", "MageProfessionTierFour", 1},
                {"QuestActive", "MageProfessionTierFour", false},
                {"MinTimesQuestDone", "MageProfessionTierThree", 1},
                {"Custom", function(playerObj)
                    local manifestation = GetSkillLevel(playerObj, "MagerySkill") or 0
                    local magicaffinity = GetSkillLevel(playerObj, "MagicAffinitySkill") or 0
                    local channeling = GetSkillLevel(playerObj, "ChannelingSkill") or 0
                    if ( manifestation >= 100
                    and magicaffinity >= 100
                    and channeling >= 100 ) then return true else return false end
                end},
            },
        },
        {--3
            Name = "The Catacombs",
            Instructions = "Travel to [F2F5A9]The Catacombs[-].",
            NextStep = 4,
            Goals = {
                {"InRegion", "FollowerHubRegion", true},
            },
        },
        {--4
            Name = "The Catacombs",
            Instructions = "Speak to [F2F5A9]Ali-Larus[-].",
            NextStep = 5,
            EndDialogue = {"[Mage IV] I was told to speak with you.","Here to prove your skills as a Mage, eh? Your first task is to find me a Crimped Note. They can be found on either of the lower floors."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--5
            Name = "The Catacombs",
            Instructions = "Find a [F2F5A9]Crimped Note[-] inside the [F2F5A9]Crimped Note[-].",
            NextStep = 6,
            QuestData = {
                Key = "MageProfessionTier4Quest5",
                Type = "Profession",
                QuestArgs = 
                {
                    "CrimpedNote"
                },
                Count = 1,
                Name = "Crimped Note Found",
                NamePlural = "Crimped Note Found",
            },
        },
        {--6
            Name = "The Catacombs",
            Instructions = "Find and defeat the Void Guardian, [F2F5A9]Cerberus[-].",
            NextStep = 7,
            Goals = {
                {"Custom", function(playerObj)
                    local mob = FindObjects(SearchMulti(
                    {
                        SearchObjectInRange(30),
                        SearchTemplate("cerberus"),
                        SearchObjVar("CurrentState", "Dead"),
                    }))
                    if ( mob and next(mob) ) then return true else return false end
                end},
            },
        },
        {--7
            Name = "The Catacombs",
            Instructions = "Find and defeat [F2F5A9]Death[-].",
            NextStep = 8,
            Goals = {
                {"Custom", function(playerObj)
                    local mob = FindObjects(SearchMulti(
                    {
                        SearchObjectInRange(30),
                        SearchTemplate("grim_reaper"),
                        SearchObjVar("CurrentState", "Dead"),
                    }))
                    if ( mob and next(mob) ) then return true else return false end
                end},
            },
        },
        {--8
            Name = "The Catacombs",
            Instructions = "Speak to [F2F5A9]Per-Thuas[-] in the [F2F5A9]Cathedral,[-] the top floor of [F2F5A9]The Catacombs.[-]",
            NextStep = 9,
            EndDialogue = {"[Mage IV] Death is dead.","What a wonderful show! Your road to Grandmaster is almost complete. Get a Grandmaster Book from an Awakening in the Barrens."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--9
            Name = "Beneath Contempt",
            Instructions = "Get a [F2F5A9]Tome: Grandmaster Mage[-]. These can be traded for or found from [F2F5A9]Barren Lands[-] awakenings.",
            NextStep = 10,
            Goals = {
                {"HasItemInBackpack","prestige_grandmaster_mage"},
            },
        },
        {--10
            Name = "The Catacombs",
            Instructions = "Bring a [F2F5A9]Tome: Grandmaster Mage[-] to [F2F5A9]Guildmaster Magica[-] at [F2F5A9]Valus[-] or [F2F5A9]Oasis[-] mage tower.",
            EndDialogue = {"[Mage IV] I have completed your challenge.","You have proven yourself as one of our best. As the Guildmaster of the Mage Guild I hereby bestow upon you the rank of Grandmaster."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GuildmasterMage,
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_grandmaster_mage"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Grandmaster Mage abilities.","info")
                    end, "Grandmaster Mage Abilities"},      
                },                
            },
        },
    },
    RogueProfessionIntro = {
        {--1
            Name = "Learn to Hide",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            NextStep = 2,
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.RoguePyros,
                QuestMarkers.RogueBlackForest,
                QuestMarkers.RogueEldeir,
            },
            EndDialogue = {"[Rogue Intro] I wish to learn subtlety.","I will show you the basics of hiding. Hiding is a way to become invisible to others. You will only be revealed from close range, by being hurt, or taking other actions. You can even move in stealth if you are skilled enough! To hide, simply move away from any onlookers and slip into the shadows."},
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionIntro", 1},
                --{"QuestActive", "RogueProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn to Hide",
            Instructions = "Hide by using the [F2F5A9]Hide[-] skill from your skill book under Hiding. You can drag the Hide skill icon from your skill book onto your hotbar.",
            NextStep = 3,
            StartDialogue = {"[Rogue Intro] I wish to learn subtlety.","I will show you the basics of hiding. Hiding is a way to become invisible to others. You will only be revealed from close range, by being hurt, or taking other actions. You can even move in stealth if you are skilled enough! To hide, simply move away from any onlookers and slip into the shadows."},
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionIntro", 1},
                {"QuestActive", "RogueProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill","HidingSkill",30},
                {"SetSkill","StealthSkill",30},
            },
            Goals = {
                {"HasMobileEffect", "Hide", true},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    local mobiles = FindObjects(SearchMulti(
                    {
                        SearchMobileInRange(30),
                        SearchTemplate("prestige_rogue_trainer"),
                    }))
                    if ( mobiles and next(mobiles) ) then
                        for i = 1, #mobiles do
                            mobiles[i]:NpcSpeech("Where did "..playerObj:GetCharacterName().." go??") 
                        end
                    end
                end},
            },
        },
        {--3
            Name = "Learn to Hide",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.RoguePyros,
                QuestMarkers.RogueBlackForest,
                QuestMarkers.RogueEldeir,
            },
            EndDialogue = {"[Rogue Intro] I am nothingness.","You have perfected the art of staying perfectly still and vanishing from plain sight! There are endless uses for hiding and stealth, from ambushing enemies, avoiding them, or anything that requires a bit of... subtlety."},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    RogueProfessionTierOne = {
        {--1
            Name = "Eavesdropping",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.RoguePyros,
                QuestMarkers.RogueBlackForest,
                QuestMarkers.RogueEldeir,
            },
            EndDialogue = {"[Rogue I] What else can you teach me?.","A new recruit, eh? We have information that the town of Trinit has been buzzing with covert activity which may be against our interests. Your first assignment is to travel there and gather information. Do a good job and we might have more work for you."},
            NextStep = 2,
            MarkerLocs = {
                {261.5, 0, 929.2},--eldeir blacksmith
            },
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionTierOne", 1},
                --{"QuestActive", "RogueProfessionTierOne", false},
                {"MinTimesQuestDone", "RogueProfessionIntro", 1},
                {"Skill", "HidingSkill", 30},
                {"Skill", "StealthSkill", 30},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Eavesdropping",
            Instructions = "Locate [F2F5A9]Conspirators[-] in [F2F5A9]Trinit[-].",
            StartDialogue = {"[Rogue I] I wish to learn subtlety.","A new recruit, eh? We have information that the town of Trinit has been buzzing with covert activity which may be against our interests. Your first assignment is to travel there and gather information. Do a good job and we might have more work for you."},
            NextStep = 3,
            MarkerLocs = {
                QuestMarkers.Trinit,
            },
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionTierOne", 1},
                {"QuestActive", "RogueProfessionTierOne", false},
                {"MinTimesQuestDone", "RogueProfessionIntro", 1},
                {"Skill", "HidingSkill", 30},
                {"Skill", "StealthSkill", 30},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local mobiles = FindObjects(SearchMulti(
                    {
                        SearchMobileInRange(15),
                        SearchModule("ai_npc_conspirator_rogue_quest"),
                    }))
                    if ( mobiles and next(mobiles) ) then return true else return false end
                end},
            },
        },
        {--3
            Name = "Eavesdropping",
            Instructions = "[F2F5A9]Hide[-] near the [F2F5A9]Conspirators[-] in [F2F5A9]Trinit[-].",
            MarkerLocs = {
                {525.23, 0, -178.78},--trinit blacksmith
            },
            NextStep = 4,
            Goals = {
                {"Custom", function(playerObj)
                    local mobiles = FindObjects(SearchMulti(
                    {
                        SearchMobileInRange(15),
                        SearchTemplate("conspirator_rogue_quest"),
                    }))
                    if ( mobiles and next(mobiles) ) then return true else return false end
                end},
                {"HasMobileEffect", "Hide", true}
            },
        },
        {--4
            Name = "Eavesdropping",
            Instructions = "Listen to the conversation between the [F2F5A9]Conspirators[-] in [F2F5A9]Trinit[-].",
            MarkerLocs = {
                {525.23, 0, -178.78},--trinit blacksmith
            },
            NextStep = 5,
            OnStart = {
                {"Custom", function(playerObj)
                    CallFunctionDelayed(TimeSpan.FromSeconds(20),function()
                        Quests.TryEnd(playerObj, "RogueProfessionTierOne", 4, 1)
                        Quests.TryStart(playerObj, "RogueProfessionTierOne", 5, true)
                    end)
                end},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--5
            Name = "Eavesdropping",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            EndDialogue = {"[Rogue I] Some interesting information...","Keep your voice down! Truth be told, we were already aware of this, but you had to be tested somehow. Pirates have been smuggling illegal rum and avoiding taxes for some time now. Our order has been tasked with getting to the bottom of it. These smugglers are extremely dangerous. We must learn the identity of their leader if we are going to dismantle their operation. Be warned: many have died in this pursuit, and their leader is said to be ruthless."},
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.RoguePyros,
                QuestMarkers.RogueBlackForest,
                QuestMarkers.RogueEldeir,
            },
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionTierOne", 1},
                {"QuestActive", "RogueProfessionTierOne", false},
                {"Skill", "HidingSkill", 30},
                {"Skill", "StealthSkill", 30},
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    RogueProfessionTierTwo = {
        {--1
            Name = "Compromised",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.RoguePyros,
                QuestMarkers.RogueBlackForest,
                QuestMarkers.RogueEldeir,
            },
            EndDialogue = {"[Rogue II] Am I needed?","SHH! Trust no one. The pirates have started bribing the guards. Any one of them could be a spy. They can't know you are working with us. We need your help. One of our agents discovered key information regarding the smuggler leader's identity, he has escaped detention by corrupt guards just outside of Helm. We need to find a way to learn what he knows, without the use of force. Find our agent by the old guard wall and retrieve his information without any witnesses."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionTierTwo", 1},
                --{"QuestActive", "RogueProfessionTierTwo", false},
                {"MinTimesQuestDone", "RogueProfessionTierOne", 1},
                {"Skill", "HidingSkill", 50},
                {"Skill", "StealthSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Compromised",
            Instructions = "Go to the [F2F5A9]old guard wall[-] Northwest of the [F2F5A9]Helm[-] gatekeeper.",
            MarkerLocs = {
                {2349.457, 0, 696.2012},--rogue prisoner
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
            },
            StartDialogue = {"[Rogue II] Am I needed?","SHH! Trust no one. The pirates have started bribing the guards. Any one of them could be a spy. They can't know you are working with us. We need your help. One of our agents discovered key information regarding the smuggler leader's identity, he has escaped detention by corrupt guards just outside of Helm. We need to find a way to learn what he knows, without the use of force. Find our agent by the old guard wall and retrieve his information without any witnesses."},
            NextStep = 3,
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionTierTwo", 1},
                {"QuestActive", "RogueProfessionTierTwo", false},
                {"MinTimesQuestDone", "RogueProfessionTierOne", 1},
                {"Skill", "HidingSkill", 50},
                {"Skill", "StealthSkill", 50},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local mobiles = FindObjects(SearchMulti(
                    {
                        SearchMobileInRange(20),
                        SearchTemplate("prisoner_rogue_quest"),
                    }))
                    if ( mobiles and next(mobiles) ) then return true else return false end
                end},
            },
        },
        {--3
            Name = "Compromised",
            Instructions = "While hidden, sneak up to [F2F5A9]Dusty[-] north of the guard tower at the [F2F5A9]old wall[-] Northwest of the [F2F5A9]Helm[-] gatekeeper.",
            MarkerLocs = {
                {2350.32, 0, 698.36},--rogue prisoner
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
            },
            NextStep = 4,
            Goals = {
                {"Custom", function(playerObj)
                    local mobiles = FindObjects(SearchMulti(
                    {
                        SearchMobileInRange(5),
                        SearchTemplate("prisoner_rogue_quest"),
                    }))
                    if ( mobiles and next(mobiles) ) then
                        if ( mobiles[1]:HasLineOfSightToObj(playerObj,ServerSettings.Combat.LOSEyeLevel) ) then
                            return true
                        end
                    end
                    return false
                end},
                {"HasMobileEffect", "Hide", true}
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    local mobiles = FindObjects(SearchMulti(
                    {
                        SearchMobileInRange(10),
                        SearchTemplate("prisoner_rogue_quest"),
                    }))
                    if ( mobiles and next(mobiles) ) then
                        mobiles[1]:NpcSpeech("*whispers something into the ear of "..playerObj:GetCharacterName().."*")
                    end
                end},
            },
        },
        {--4
            Name = "Compromised",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.RoguePyros,
                QuestMarkers.RogueBlackForest,
                QuestMarkers.RogueEldeir,
            },
            NextStep = 5,
            EndDialogue = {"[Rogue II] Pssst.","I can't tell you how happy I am that you managed to get this information. So far, Dusty was the only agent to get close enough to their leader without getting... ahem... discovered. They are all definitely still alive, though. Take this Journeyman Rogue book and bring it to Das-Tal, the rogue guildmaster."},
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","prestige_journeyman_rogue"},     
                },                
            },
        },
        {--5
            Name = "Compromised",
            Instructions = "Bring a [F2F5A9]Tome: Journeyman Rogue[-] to [F2F5A9]Guildmaster Das-Tal[-]. He can be found in [F2F5A9]Black Forest[-].",
            EndDialogue = {"[Rogue II] I have a profession tome.","Within these pages lies the key to the next level of ability. Enjoy."},
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperEasternFrontier,
                {2167.23, 0, -348.40},
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_journeyman_rogue"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Tier I Rogue abilities.","info")
                    end, "Tier I Rogue Abilities"},      
                },                
            },
        },
    },
    RogueProfessionTierThree = {
        {--1
            Name = "Agent-in-Place",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            EndDialogue = {"[Rogue III] Are we ready?","Patience. Covert operations have to proceed with precision and timing. I do indeed have a task for you. It is time for you to meet one of our higher ranking friends. First, meet up with Mohana at the Oasis. He will direct you further."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionTierThree", 1},
                --{"QuestActive", "RogueProfessionTierThree", false},
                {"MinTimesQuestDone", "RogueProfessionTierTwo", 1},
                {"Skill", "HidingSkill", 80},
                {"Skill", "StealthSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Agent-in-Place",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            StartDialogue = {"[Rogue III] Are we ready?","Patience. Covert operations have to proceed with precision and timing. I do indeed have a task for you. It is time for you to meet one of our higher ranking friends. First, meet up with Mohana at the Oasis. He will direct you further."},
            NextStep = 3,
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionTierThree", 1},
                {"QuestActive", "RogueProfessionTierThree", false},
                {"MinTimesQuestDone", "RogueProfessionTierTwo", 1},
                {"Skill", "HidingSkill", 80},
                {"Skill", "StealthSkill", 80},
            },
        },
        {--3
            Name = "Agent-in-Place",
            MarkerLocs = {
                QuestMarkers.RogueMessenger,
            },
            Instructions = "Speak to [F2F5A9]Mohana[-] at the [F2F5A9]Oasis[-].",
            EndDialogue = {"[Rogue III] I was told to report to you.","We are trusting you with this. You had better not fail us. Deliver this message to Ushukem. He is waiting east of Helm, on the beach under a rocky overhang. Stay out of sight of guards, and don't use any flashy methods of travel. If you get caught, you know what you must do."},
            NextStep = 4,
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Agent-in-Place",
            Instructions = "Deliver the message to [F2F5A9]Ushukem[-]. He is waiting East of [F2F5A9]Helm[-], on the beach under a rocky overhang. Stay out of sight of guards, and don't use any flashy methods of travel. If you get caught, you know what you must do.",
            NextStep = 6,
            EndDialogue = {"[Rogue III] Two belts for the king.","Yes, yes. Password accepted. I have been expecting you. You will be contacted when we are ready for you again."},
            OnStart = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("[00FF00]Now safe from the guards.[-]","info")
                end},
            },
            Fails = {
                {"Custom", function(playerObj)
                    local stealthed = HasMobileEffect(playerObj, "Hide")
                    if ( not stealthed ) then
                        local guards = FindObjects(SearchMulti(
                        {
                            SearchMobileInRange(15),
                            SearchModule("ai_guard"),
                        }))
                        if ( (guards and next(guards)) ) then
                            Quests.Fail(playerObj, "RogueProfessionTierThree", 4, true)
                            Quests.TryStart(playerObj, "RogueProfessionTierThree", 5, true)
                            return true
                        end
                        return false
                    end
                    return false
                end},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--5
            Name = "Agent-in-Place",
            Instructions = "Evade the guards!",
            TimeLimit = TimeSpan.FromSeconds(5),
            OnStart = {
                {"Custom", function(playerObj)
                    playerObj:PlayObjectSound("event:/ui/click_fail",false,0,false,false)
                    playerObj:SystemMessage("[800000]Evade the guards![-]","info")
                end},
            },
            Fails = {
                {"Custom", function(playerObj)
                    local quests = playerObj:GetObjVar("Quests") or {}
                    local timeLimit = AllQuests.RogueProfessionTierThree[5].TimeLimit
                    if ( quests and DateTime.Compare(DateTime.UtcNow, quests["RogueProfessionTierThree"][5]["Time"]:Add(timeLimit)) > 0 ) then
                        playerObj:SystemMessage("You have been caught! You take the information to the grave.", "info")
                        playerObj:SendMessage("ProcessTrueDamage", playerObj, GetCurHealth(playerObj), true)
                        Quests.Fail(playerObj, "RogueProfessionTierThree", nil, true)
                        Quests.TryStart(playerObj, "RogueProfessionTierThree", 3, true)
                        return true
                    end
                    return false
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local stealthed = HasMobileEffect(playerObj, "Hide")
                    local guards = FindObjects(SearchMulti(
                    {
                        SearchMobileInRange(15),
                        SearchModule("ai_guard"),
                    }))
                    if ( stealthed or not (guards and next(guards)) ) then
                        Quests.Fail(playerObj, "RogueProfessionTierThree", 5, true)
                        Quests.TryStart(playerObj, "RogueProfessionTierThree", 4, true)
                    end
                    return false
                end},
            },
        },
        {--6
            Name = "Agent-in-Place",
            Instructions = "Deliver the message to [F2F5A9]Ushukem[-]. He is waiting East of [F2F5A9]Helm[-], on the beach under a rocky overhang. Stay out of sight of guards, and don't use any flashy methods of travel. If you get caught, you know what you must do.",
            NextStep = 7,
        },
        {--7
            Name = "Agent-in-Place",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            EndDialogue = {"[Rogue III] I have returned.","Word has reached us of your acts and we are assured of your ability. One final challenge awaits you. Deep within the Black Forest lies an ancient champion. Defeat him to earn the Tome of the Master Rogue and present it to Guildmaster Das-Tal."},
            NextStep = 8,
        },
        {--8
            Name = "Agent-in-Place",
            Instructions = "Get a [F2F5A9]Tome: Master Rogue[-]. These can be traded for or found from killing [F2F5A9]Lord Barkus[-] in [F2F5A9]Black Forest[-] swamp.",
            NextStep = 9,
            Goals = {
                {"HasItemInBackpack","prestige_master_rogue"},
            },
        },
        {--9
            Name = "Agent-in-Place",
            Instructions = "Bring a [F2F5A9]Tome: Master Rogue[-] to [F2F5A9]Guildmaster Das-Tal[-]. He can be found in [F2F5A9]Black Forest[-].",
            EndDialogue = {"[Rogue III] I have a profession tome.","Within these pages lies the key to the next level of ability. Enjoy."},
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperEasternFrontier,
                {2167.23, 0, -348.40},
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_master_rogue"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Master Rogue Abilities.","info")
                    end, "Tier II Rogue Abilities"},      
                },                
            },
        },
    },
    RogueProfessionTierFour = {
        {--1
            Name = "Francis the Fearless",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            EndDialogue = {"[Rogue IV] I was summoned.","Indeed, we are in need of your skills. The identity of the pirate smugglers' leader has finally been determined, and a course of action decided upon. You are to travel to Pirate's Grotto where he was last seen. Locate the boss without engaging in combat. Warn him that if he does not cease his illegal activity, there will be harsh consequences. Be tactful and don't stay long, as he is known for his brutality. Good luck, and I hope we see each other again."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionTierFour", 1},
                --{"QuestActive", "RogueProfessionTierFour", false},
                {"MinTimesQuestDone", "RogueProfessionTierThree", 1},
                {"Skill", "HidingSkill", 100},
                {"Skill", "StealthSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Francis the Fearless",
            Instructions = "Go to [F2F5A9]Pirate's Grotto[-] in the Southeastern corner of [F2F5A9]Southern Hills[-].",
            StartDialogue = {"[Rogue IV] I was summoned.","Indeed, we are in need of your skills. The identity of the pirate smugglers' leader has finally been determined, and a course of action decided upon. You are to travel to Pirate's Grotto where he was last seen. Locate the boss without engaging in combat. Warn him that if he does not cease his illegal activity, there will be harsh consequences. Be tactful and don't stay long, as he is known for his brutality. Good luck, and I hope we see each other again."},
            NextStep = 3,
            Requirements = {
                {"MaxTimesQuestDone", "RogueProfessionTierFour", 1},
                {"QuestActive", "RogueProfessionTierFour", false},
                {"MinTimesQuestDone", "RogueProfessionTierThree", 1},
                {"Skill", "HidingSkill", 100},
                {"Skill", "StealthSkill", 100},
            },
            Goals = {
                {"InRegion", "Grotto", true},
            },
        },
        {--3
            Name = "Francis the Fearless",
            Instructions = "Locate the [F2F5A9]smuggler leader[-] in [F2F5A9]Pirate's Grotto[-] without engaging in combat.",
            NextStep = 4,
            Fails = {
                {"Custom", function(playerObj)
                    if ( HasAnyActiveConflictRecords(playerObj) ) then
                        return true
                    end
                    return false
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local mobiles = FindObjects(SearchMulti(
                    {
                        SearchMobileInRange(10),
                        SearchTemplate("boss_rogue_quest"),
                    }))
                    if ( mobiles and next(mobiles) ) then return true else return false end
                end},
            },
        },
        {--4
            Name = "Francis the Fearless",
            Instructions = "Confront the [F2F5A9]smuggler leader[-] about his illegal activities.",
            NextStep = 5,
            Goals = {
                {"Custom", function(playerObj)
                    local mobiles = FindObjects(SearchMulti(
                    {
                        SearchMobileInRange(5),
                        SearchTemplate("boss_rogue_quest"),
                    }))
                    if ( mobiles and next(mobiles) and not playerObj:IsCloaked() ) then return true else return false end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    local mobiles = FindObjects(SearchMulti(
                    {
                        SearchMobileInRange(10),
                        SearchTemplate("boss_rogue_quest"),
                    }))
                    if ( mobiles and next(mobiles) ) then
                        for k, boss in pairs(mobiles) do
                            if ( boss and boss:IsValid() and not IsDead(boss) ) then
                                boss:SetFacing(boss:GetLoc():YAngleTo(playerObj:GetLoc()))
                                boss:NpcSpeech("Wha?! Who's there?!")
                                CallFunctionDelayed(TimeSpan.FromSeconds(2),function()
                                    if ( boss:HasObjVar("Invulnerable") ) then
                                        boss:DelObjVar("Invulnerable")
                                    end
                                    boss:NpcSpeech("*dies of surprise*")
                                    boss:SendMessage("ProcessTrueDamage", boss, GetCurHealth(boss), true)
                                end)
                            end
                        end
                    end
                end},
            },
        },
        {--5
            Name = "Francis the Fearless",
            Instructions = "Speak to a [F2F5A9]rogue trainer[-].",
            NextStep = 6,
            EndDialogue = {"[Rogue IV] He's dead.","What?! You're alive?! I mean... of course you're alive. Welcome back, good friend. I knew you would be fine, and what a great job you did as well. Congratulations! You are a hero now. You didn't have to murder him, but hopefully this will discourage their kind of activity even more. Bring the Guildmaster a Grandmaster Rogue Tome so that you may further your abilities. Thank you again, agent."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--6
            Name = "Francis the Fearless",
            Instructions = "Get a [F2F5A9]Tome: Grandmaster Rogue[-]. These can be traded for or found from [F2F5A9]Barren Lands[-] awakenings.",
            NextStep = 7,
            Goals = {
                {"HasItemInBackpack","prestige_grandmaster_rogue"},
            },
        },
        {--7
            Name = "Francis the Fearless",
            Instructions = "Bring a [F2F5A9]Tome: Grandmaster Rogue[-] to [F2F5A9]Guildmaster Das-Tal[-] in [F2F5A9]Black Forest[-].",
            EndDialogue = {"[Rogue IV] I have a profession tome.","Within these pages lies the key to the final level of ability. Congratulations."},
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.GatekeeperEasternFrontier,
                {2167.23, 0, -348.40},
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_grandmaster_rogue"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Grandmaster Rogue abilities.","info")
                    end, "Tier IV Rogue Abilities"},      
                },                
            },
        },
    },
    LumberjackProfessionIntro = {
        {--1
            Name = "Learn to Lumberjack",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            NextStep = 2,
            EndDialogue = {"[Lumberjack Intro] Can you teach me Lumberjacking?","Sure can! It doesn't require much brains. Go find a hatchet and speak with me again. You can buy them from tinkerers."},
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            Requirements = {
                {"MaxTimesQuestDone", "LumberjackProfessionIntro", 1},
                --{"QuestActive", "LumberjackProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn to Lumberjack",
            Instructions = "Get a [F2F5A9]Hatchet[-]. You can buy them from tinkerers.",
            NextStep = 3,
            StartDialogue = {"[Lumberjack Intro] Can you teach me Lumberjacking?","Sure can! It doesn't require much brains. Go find a hatchet and speak with me again. You can buy them from tinkerers."},
            Requirements = {
                {"MaxTimesQuestDone", "LumberjackProfessionIntro", 1},
                {"QuestActive", "LumberjackProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill", "LumberjackSkill", 30},
            },
            Goals = {
                {"HasItemInBackpack", "tool_hatchet"},
            },
        },
        {--3
            Name = "Learn to Lumberjack",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            NextStep = 4,
            EndDialogue = {"[Lumberjack Intro] I found a hatchet.","Right! That will do! Now grip your hatchet tight and swing it like you are as mad as I am! How relaxing. Go get lost in the forest and don't come back until you have 10 wood."},
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Learn to Lumberjack",
            Instructions = "Equip a [F2F5A9]Hatchet[-] by double clicking it in your backpack or dragging it onto yourself in the character screen.",
            NextStep = 5,
            Goals = {
                {"Custom", function(playerObj)
                    local handObjects = {}
                    table.insert(handObjects, playerObj:GetEquippedObject("LeftHand"))
                    table.insert(handObjects, playerObj:GetEquippedObject("RightHand"))
                    local hatchet = 0
                    for i = 1, #handObjects do
                        if ( handObjects[i]:HasModule("tool_lumberaxe") ) then
                            hatchet = 1
                        end
                    end
                    if ( hatchet == 0 ) then return false end
                    return true
                end},
            },
        },
        {--5
            Name = "Learn to Lumberjack",
            Instructions = "Chop 10 [F2F5A9]Wood[-].",
            NextStep = 6,
            Goals = {
                {"HasItemInBackpack", "resource_wood", 10},
            },
        },
        {--6
            Name = "Learn to Lumberjack",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            EndDialogue = {"[Lumberjack Intro] I've got wood for you.","Ahem... yes well that's nice I guess. Now, once you have gathered wood, you will need to refine it into boards with carpentry. There are several varieties of wood in Celador, each with more rarity and value than the last. See if you can tell the difference in their appearance."},
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    LumberjackProfessionTierOne = {
        {--1
            Name = "Chopping Celador",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            NextStep = 2,
            EndDialogue = {"[Lumberjack I] I want all the wood.","Don't we all? The first step is to just chop, chop, chop. Prove yourself by amassing 400 wood, then come back and I will judge your lumberjack merit!"},
                MarkerLocs = {
                    QuestMarkers.CarpenterPyros,
                    QuestMarkers.CarpenterHelm,
                    QuestMarkers.CarpenterBarrenLands,
                    QuestMarkers.CarpenterEldeir,
                    QuestMarkers.CarpenterBlackForest,
                    QuestMarkers.CarpenterValus,
                },
            Requirements = {
                {"MaxTimesQuestDone", "LumberjackProfessionTierOne", 1},
                --{"QuestActive", "LumberjackProfessionTierOne", false},
                {"MinTimesQuestDone", "LumberjackProfessionIntro", 1},
                {"Skill", "LumberjackSkill", 30},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Chopping Celador",
            Instructions = "Amass 400 [F2F5A9]Wood[-].",
            NextStep = 3,
            StartDialogue = {"[Lumberjack I] I want all the wood.","Don't we all? The first step is to just chop, chop, chop. Prove yourself by amassing 400 wood, then come back and I will judge your lumberjack merit!"},
            Requirements = {
                {"MaxTimesQuestDone", "LumberjackProfessionTierOne", 1},
                {"QuestActive", "LumberjackProfessionTierOne", false},
                {"MinTimesQuestDone", "LumberjackProfessionIntro", 1},
                {"Skill", "LumberjackSkill", 30},
            },
            Goals = {
                {"HasItem", "resource_wood", 400},
            },
        },
        {--3
            Name = "Chopping Celador",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            EndDialogue = {"[Lumberjack I] I'm very sore...","Yes! Glorious muscular gain! In no time, you will be bringing in the lumber like a champion woodcutter. Here, try this baby out."},
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","tool_hatchet_light"},
                },
            },
        },
    },
    LumberjackProfessionTierTwo = {
        {--1
            Name = "Chopping Celador Part 2",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            EndDialogue = {"[Lumberjack II] I feel ready for another challenge.","And a challenge you shall have! Go forth and chop 800 wood this time, show those trees what you're made of!"},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "LumberjackProfessionTierTwo", 1},
                --{"QuestActive", "LumberjackProfessionTierOne", false},
                {"MinTimesQuestDone", "LumberjackProfessionTierOne", 1},
                {"Skill", "LumberjackSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 2
            Name = "Chopping Celador Part 2",
            Instructions = "Chop[F2F5A9]800 wood[-] from trees.",
            NextStep = 3,
            Requirements = {
                {"Skill", "LumberjackSkill", 50},
            },
            QuestData = {
                Key = "LumberjackProfessionTierTwo2",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Lumberjacking",
                    true
                },
                Count = 800,
                Name = "Chopped Wood",
                NamePlural = "Chopped Wood",
            },
        },

        {--3
            Name = "Chopping Celador Part 2",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Lumberjack II] How much wood did I need to chop?", "You need to chop 800 wood, doesn't matter the type."},
            EndDialogue = {"[Lumberjack II] I cannot feel my arms.","Yeah, you get used to that after a while. Last time I felt mine with near on 12 years ago, but their still here."},
            Requirements = {
                {"Skill", "LumberjackSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    LumberjackProfessionTierThree = {
        {--1
            Name = "Chopping Celador Part 3",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            EndDialogue = {"[Lumberjack III] Bring the pain!","Now you're getting the idea! I tell you what, 2,000 wood should do this time, by the end of this you'll be ready to wrestle bears!"},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "LumberjackProfessionTierThree", 1},
                --{"QuestActive", "LumberjackProfessionTierOne", false},
                {"MinTimesQuestDone", "LumberjackProfessionTierTwo", 1},
                {"Skill", "LumberjackSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 2
            Name = "Chopping Celador Part 3",
            Instructions = "Chop [F2F5A9]2,000 wood[-] from trees.",
            NextStep = 3,
            Requirements = {
                {"Skill", "LumberjackSkill", 80},
            },
            QuestData = {
                Key = "LumberjackProfessionTierThree2",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Lumberjacking",
                    true
                },
                Count = 2000,
                Name = "Chopped Wood",
                NamePlural = "Chopped Wood",
            },
        },

        {--3
            Name = "Chopping Celador Part 3",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Lumberjack II] It was 2,000 wood?", "Yep, that's a two will three zeros."},
            EndDialogue = {"[Lumberjack II] Bring on that bear!","Congratulations! I'm proud you welcome you into the esteemed ranks of master lumberjacks."},
            Requirements = {
                {"Skill", "LumberjackSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    LumberjackProfessionTierFour = {
        {--1
            Name = "Chopping Celador Part 4",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            NextStep = 2,
            EndDialogue = {"[Lumberjack IV] I can't get any stronger!","Hah, you do look very strong. It's time you learned the best places to chop. I'll share some of my favorite with you. First visit the Steel Coast in Earstern Valusia and chop 200 wood there."},
            Requirements = {
                {"MaxTimesQuestDone", "LumberjackProfessionTierFour", 1},
                --{"QuestActive", "LumberjackProfessionTierFour", false},
                {"MinTimesQuestDone", "LumberjackProfessionTierThree", 1},
                {"Skill", "LumberjackSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 2
            Name = "Chopping Celador Part 4",
            Instructions = "Chop [F2F5A9]200 wood[-] at the Steel Coast in Eastern Valusia.",
            NextStep = 3,
            Requirements = {
                {"Skill", "LumberjackSkill", 100},
            },
            QuestData = {
                Key = "LumberjackProfessionTierFour2",
                Type = "Gathering",
                Region = "Area-Steel Coast",
                QuestArgs = 
                {
                    "Lumberjacking",
                    true
                },
                Count = 200,
                Name = "Chopped Wood",
                NamePlural = "Chopped Wood",
            },
        },

        {--3
            Name = "Chopping Celador Part 4",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            EndDialogue = {"[Lumberjack IV] Steel Coast was pretty.","It truly is, hopefully the wildlife wasn't too rough on you. Next, I'd like you to venture into the Black Forest and chop 200 wood from the Radiant Woodlands."},
            NextStep = 4,
            Requirements = {
                {"Skill", "LumberjackSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 4
            Name = "Chopping Celador Part 4",
            Instructions = "Chop [F2F5A9]200 wood[-] at the Radiant Woodlands within the Black Forest.",
            NextStep = 5,
            Requirements = {
                {"Skill", "LumberjackSkill", 100},
            },
            QuestData = {
                Key = "LumberjackProfessionTierFour2",
                Type = "Gathering",
                Region = "Area-Radiant Woodlands",
                QuestArgs = 
                {
                    "Lumberjacking",
                    true
                },
                Count = 200,
                Name = "Chopped Wood",
                NamePlural = "Chopped Wood",
            },
        },

        {--5
            Name = "Chopping Celador Part 4",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            EndDialogue = {"[Lumberjack IV] Sooo many spiders.","Spiders? Oh I suppose I should have mentioned those. But you lived! You're off to Dagger Isle in Upper Plains now, 200 more wood."},
            NextStep = 6,
            Requirements = {
                {"Skill", "LumberjackSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 6
            Name = "Chopping Celador Part 4",
            Instructions = "Chop [F2F5A9]200 wood[-] on Dagger Isle in the Upper Plains.",
            NextStep = 7,
            Requirements = {
                {"Skill", "LumberjackSkill", 100},
            },
            QuestData = {
                Key = "LumberjackProfessionTierFour2",
                Type = "Gathering",
                Region = "Area-Dagger Isle",
                QuestArgs = 
                {
                    "Lumberjacking",
                    true
                },
                Count = 200,
                Name = "Chopped Wood",
                NamePlural = "Chopped Wood",
            },
        },

        {--7
            Name = "Chopping Celador Part 4",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            EndDialogue = {"[Lumberjack IV] Finished up with the isle.","Well done, just one more place to show you. Venture to the Howling Cape in the Southern Rim and chop 200 more wood."},
            NextStep = 8,
            Requirements = {
                {"Skill", "LumberjackSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 8
            Name = "Chopping Celador Part 4",
            Instructions = "Chop [F2F5A9]200 wood[-] along the Howling Cape in Southern Rim.",
            NextStep = 9,
            Requirements = {
                {"Skill", "LumberjackSkill", 100},
            },
            QuestData = {
                Key = "LumberjackProfessionTierFour2",
                Type = "Gathering",
                Region = "Area-Howling Cape",
                QuestArgs = 
                {
                    "Lumberjacking",
                    true
                },
                Count = 200,
                Name = "Chopped Wood",
                NamePlural = "Chopped Wood",
            },
        },

        {--9
            Name = "Chopping Celador Part 4",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
                QuestMarkers.CarpenterHelm,
                QuestMarkers.CarpenterBarrenLands,
                QuestMarkers.CarpenterEldeir,
                QuestMarkers.CarpenterBlackForest,
                QuestMarkers.CarpenterValus,
            },
            EndDialogue = {"[Lumberjack IV] Trees were a bit sparce there.","Aye, they are but so are the humans. A good woodsman will find places like I've shown you, off the beaten path where you can chop in peace with just the winds and the wildlife. You have the tools you need now and have earned the rank of grandmaster lumberjack."},
            Requirements = {
                {"Skill", "LumberjackSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    MinerProfessionIntro = {
        {--1
            Name = "Learn to Mine",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            NextStep = 2,
            EndDialogue = {"[Miner Intro] Can you teach me to mine?","I can try. Go find a mining pick and speak with me again. You can buy them from tinkerers."},
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            Requirements = {
                {"MaxTimesQuestDone", "MinerProfessionIntro", 1},
                --{"QuestActive", "MinerProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn to Mine",
            Instructions = "Get a [F2F5A9]Mining Pick[-]. You can buy them from tinkerers.",
            NextStep = 3,
            StartDialogue = {"[Miner Intro] Can you teach me to mine?","I can try. Go find a mining pick and speak with me again. You can buy them from tinkerers."},
            Requirements = {
                {"MaxTimesQuestDone", "MinerProfessionIntro", 1},
                {"QuestActive", "MinerProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill", "MiningSkill", 30},
            },
            Goals = {
                {"HasItemInBackpack", "tool_mining_pick"},
            },
        },
        {--3
            Name = "Learn to Mine",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            NextStep = 4,
            EndDialogue = {"[Miner Intro] I found a mining pick.","It looks more like a hammer, but it will work. Stone and ore veins are all over the place if you know what to look for. Grab your pick and go loosen up 10 stone. Remember to bend your knees, not your back... heh."},
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Learn to Mine",
            Instructions = "Equip a [F2F5A9]Mining Pick[-] by double clicking it in your backpack or dragging it onto yourself in the character screen.",
            NextStep = 5,
            Goals = {
                {"Custom", function(playerObj)
                    local handObjects = {}
                    table.insert(handObjects, playerObj:GetEquippedObject("LeftHand"))
                    table.insert(handObjects, playerObj:GetEquippedObject("RightHand"))
                    local pick = 0
                    for i = 1, #handObjects do
                        if ( handObjects[i]:HasModule("tool_miningpick") ) then
                            pick = 1
                        end
                    end
                    if ( pick == 0 ) then return false end
                    return true
                end},
            },
        },
        {--5
            Name = "Learn to Mine",
            Instructions = "Mine 10 [F2F5A9]Stone[-].",
            NextStep = 6,
            Goals = {
                {"HasItemInBackpack", "resource_stone", 10},
            },
        },
        {--6
            Name = "Learn to Mine",
            Instructions = "Speak to a blacksmith.",
            EndDialogue = {"[Miner Intro] Derr... rocks!","Ohhh... cool collection! All rocks and metals need to be refined by a blacksmith to be used and to save weight. Mines are particularly good places to find more valuable materials."},
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    MinerProfessionTierOne = {
        {--1
            Name = "Mining Celador",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            NextStep = 2,
            EndDialogue = {"[Miner I] Where are all the rocks?","Look around you! They are all over the place! Probably inside your skull too! Stone is the most common resource that you can mine. It is used in bulk for things such as housing. Go mine 200 and speak to me again."},
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            Requirements = {
                {"MaxTimesQuestDone", "MinerProfessionTierOne", 1},
                --{"QuestActive", "MinerProfessionTierOne", false},
                {"MinTimesQuestDone", "MinerProfessionIntro", 1},
                {"Skill", "MiningSkill", 30},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        {--2
            Name = "Mining Celador",
            Instructions = "Amass 200 [F2F5A9]Iron Ore[-]. Look for veins which are colored differently than others.",
            NextStep = 3,
            --StartDialogue = {"[Miner I] Where are all the rocks?","Look around you! They are all over the place! Probably inside your skull too! Stone is the most common resource that you can mine. It is used in bulk for things such as housing. Go mine 500 and speak to me again."},
            Requirements = {
                {"MaxTimesQuestDone", "MinerProfessionTierOne", 1},
                {"QuestActive", "MinerProfessionTierOne", false},
                {"MinTimesQuestDone", "MinerProfessionIntro", 1},
                {"Skill", "MiningSkill", 30},
            },
            QuestData = {
                Key = "MinerProfessionTierOne2",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Mining",
                    { "IronOre" }
                },
                Count = 200,
                Name = "Iron Ore",
                NamePlural = "Iron Ore",
            },
        },

        {--3
            Name = "Mining Celador",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            EndDialogue = {"[Miner I] I can't swing anymore.","Don't be such a wimp. Your back is just getting stronger. Maybe using pack horses to carry the load would help. Try this pick out."},
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","tool_mining_pick_light"},
                },
            },
        },
    },
    MinerProfessionTierTwo = {
        {--1
            Name = "Mining Celador: Caverns",
            Instructions = "Speak to a blacksmith.",
            NextStep = 2,
            EndDialogue = {"[Miner II] I need some solid mining locations.","What you're looking for is a cavern, they provide the best mining. Why don't you head over to West Valusia and visit the Breca Mines, come back after you've mined 200 ore there."},
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            Requirements = {
                {"MaxTimesQuestDone", "MinerProfessionTierTwo", 1},
                --{"QuestActive", "MinerProfessionTierTwo", false},
                {"MinTimesQuestDone", "MinerProfessionTierOne", 1},
                {"Skill", "MiningSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 2
            Name = "Mining Celador: Caverns",
            Instructions = "Mine [F2F5A9]200 ore[-] in the Breca Mines of West Valusia.",
            MarkerLocs = {
                {391, 0, -1574},--Breca Mines
            },
            NextStep = 3,
            Requirements = {
                {"Skill", "MiningSkill", 50},
            },
            QuestData = {
                Key = "MinerProfessionTierTwo2",
                Type = "Gathering",
                Region = "Area-Breca Mines",
                QuestArgs = 
                {
                    "Mining",
                    { "IronOre", "CopperOre", "GoldOre", "CobaltOre", "ObsidianOre" }
                },
                Count = 200,
                Name = "Ore Mined",
                NamePlural = "Ore Mined",
            },
        },

        {--3
            Name = "Mining Celador: Caverns",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            EndDialogue = {"[Miner II] Breca mine is massive.","That it is, a feat of modern engineering that is. If you're looking for something smaller, and not as well lighted, Mount Aris Cave in Eastern Frontier should be your next stop."},
            NextStep = 4,
            Requirements = {
                {"Skill", "MiningSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 4
            Name = "Mining Celador: Caverns",
            Instructions = "Mine [F2F5A9]200 ore[-] within Mount Aris Cave in Eastern Frontier.",
            MarkerLocs = {
                {2524, 0, 291},--Mount Aris Cave
            },
            NextStep = 5,
            Requirements = {
                {"Skill", "MiningSkill", 50},
            },
            QuestData = {
                Key = "MinerProfessionTierTwo4",
                Type = "Gathering",
                Region = "HelmMine4",
                QuestArgs = 
                {
                    "Mining",
                    { "IronOre", "CopperOre", "GoldOre", "CobaltOre", "ObsidianOre" }
                },
                Count = 200,
                Name = "Ore Mined",
                NamePlural = "Ore Mined",
            },
        },

        {--5
            Name = "Mining Celador: Caverns",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            EndDialogue = {"[Miner II] That cave was tiny.","Hah, yes caves come in all shapes and sizes. If you can find a medium sized on at Bootstrap Crevice in the Southern Rim."},
            NextStep = 6,
            Requirements = {
                {"Skill", "MiningSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 6
            Name = "Mining Celador: Caverns",
            Instructions = "Mine [F2F5A9]200 ore[-] within Bootsrap Crevice in the Southern Rim.",
            MarkerLocs = {
                {-1760, 0, -2372},--Bootstrap Crevice
            },
            NextStep = 7,
            Requirements = {
                {"Skill", "MiningSkill", 50},
            },
            QuestData = {
                Key = "MinerProfessionTierTwo6",
                Type = "Gathering",
                Region = "SouthernRimMine1",
                QuestArgs = 
                {
                    "Mining",
                    { "IronOre", "CopperOre", "GoldOre", "CobaltOre", "ObsidianOre" }
                },
                Count = 200,
                Name = "Ore Mined",
                NamePlural = "Ore Mined",
            },
        },

        {--7
            Name = "Mining Celador: Caverns",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            EndDialogue = {"[Miner II] My legs are getting tired.","No doubt, one last carvern to show you. You will need to do a bit of running to reach the Jhalir Mine in the Barren Lands."},
            NextStep = 8,
            Requirements = {
                {"Skill", "MiningSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 8
            Name = "Mining Celador: Caverns",
            Instructions = "Mine [F2F5A9]200 ore[-] from the Jhalir Mine in the Barren Lands.",
            MarkerLocs = {
                {-1952, 0, -1104},--Jhalir Mine
            },
            NextStep = 9,
            Requirements = {
                {"Skill", "MiningSkill", 50},
            },
            QuestData = {
                Key = "MinerProfessionTierTwo8",
                Type = "Gathering",
                Region = "BarrensMine2",
                QuestArgs = 
                {
                    "Mining",
                    { "IronOre", "CopperOre", "GoldOre", "CobaltOre", "ObsidianOre" }
                },
                Count = 200,
                Name = "Ore Mined",
                NamePlural = "Ore Mined",
            },
        },

        {--9
            Name = "Mining Celador: Caverns",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            EndDialogue = {"[Miner II] So many caves.","Yes and there are countless more some that aren't easily found on your maps. Caves can provide an ample source of ore but also attract unwanted attention. Use them when you can but don't rely on them solely."},
            NextStep = 8,
            Requirements = {
                {"Skill", "MiningSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },

    },
    MinerProfessionTierThree = {
        {--1
            Name = "Mining Celador: Outcrops",
            Instructions = "Speak to a blacksmith.",
            NextStep = 2,
            EndDialogue = {"[Miner III] Any good locations that aren't caves?","There are plenty of outcrops around New Celador, I've a few I could show you. First head to the Eastern Range Peninsula in Eastern Frontier."},
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            Requirements = {
                {"MaxTimesQuestDone", "MinerProfessionTierThree", 1},
                --{"QuestActive", "MinerProfessionTierThree", false},
                {"MinTimesQuestDone", "MinerProfessionTierTwo", 1},
                {"Skill", "MiningSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 2
            Name = "Mining Celador: Outcrops",
            Instructions = "Mine [F2F5A9]200 ore[-] from outcrops in Eastern Range Peninsula.",
            MarkerLocs = {
                {391, 0, -1574},--Breca Mines
            },
            NextStep = 3,
            Requirements = {
                {"Skill", "MiningSkill", 80},
            },
            QuestData = {
                Key = "MinerProfessionTierThree2",
                Type = "Gathering",
                Region = "Area-Eastern Range Peninsula",
                QuestArgs = 
                {
                    "Mining",
                    { "IronOre", "CopperOre", "GoldOre", "CobaltOre", "ObsidianOre" }
                },
                Count = 200,
                Name = "Ore Mined",
                NamePlural = "Ore Mined",
            },
        },

        {--3
            Name = "Mining Celador: Outcrops",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            EndDialogue = {"[Miner III] I found the outcrops.","Good, they usually offer decent mining with few distractions there is another in the Soldier's Rest region of Upper Plains."},
            NextStep = 4,
            Requirements = {
                {"Skill", "MiningSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 4
            Name = "Mining Celador: Outcrops",
            Instructions = "Mine [F2F5A9]200 ore[-] from outcrops in Soldier's Rest.",
            MarkerLocs = {
                {524, 0, 1456},--Breca Mines
            },
            NextStep = 5,
            Requirements = {
                {"Skill", "MiningSkill", 80},
            },
            QuestData = {
                Key = "MinerProfessionTierThree4",
                Type = "Gathering",
                Region = "Area-Soldier's Rest",
                QuestArgs = 
                {
                    "Mining",
                    { "IronOre", "CopperOre", "GoldOre", "CobaltOre", "ObsidianOre" }
                },
                Count = 200,
                Name = "Ore Mined",
                NamePlural = "Ore Mined",
            },
        },

        {-- 5
            Name = "Mining Celador: Outcrops",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            EndDialogue = {"[Miner III] A lot of running involved.","Aye, I would recommend getting a runebook from the scribes and creating a collection of outcrops. Next goto Spiritwood in the Black Forest."},
            NextStep = 6,
            Requirements = {
                {"Skill", "MiningSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 6
            Name = "Mining Celador: Outcrops",
            Instructions = "Mine [F2F5A9]200 ore[-] from outcrops in Spiritwood.",
            MarkerLocs = {
                {1396, 0, 12},--Spiritwood Outcrop
            },
            NextStep = 7,
            Requirements = {
                {"Skill", "MiningSkill", 80},
            },
            QuestData = {
                Key = "MinerProfessionTierThree6",
                Type = "Gathering",
                Region = "Area-Spiritwood",
                QuestArgs = 
                {
                    "Mining",
                    { "IronOre", "CopperOre", "GoldOre", "CobaltOre", "ObsidianOre" }
                },
                Count = 200,
                Name = "Ore Mined",
                NamePlural = "Ore Mined",
            },
        },

        {-- 7
            Name = "Mining Celador: Outcrops",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            EndDialogue = {"[Miner III] You didn't mention the spiders.","Oh the spiders, yeah they can be a problem. But you get the idea, just like with caves you want to pick outcrops that will be less frequented by others. Go and find some outcrops, master miner."},
            NextStep = 6,
            Requirements = {
                {"Skill", "MiningSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },

    },
    MinerProfessionTierFour = {
        
        {--1
            Name = "Mining Celador: Deposits",
            Instructions = "Speak to a blacksmith.",
            NextStep = 2,
            EndDialogue = {"[Miner IV] I'm ready to claim grandmaster.","Excellent! Using everything you've learned I want you to mine 300 obsidian ore then return to me."},
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            Requirements = {
                {"MaxTimesQuestDone", "MinerProfessionTierFour", 1},
                --{"QuestActive", "MinerProfessionTierFour", false},
                {"MinTimesQuestDone", "MinerProfessionTierThree", 1},
                {"Skill", "MiningSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        { -- 2
            Name = "Mining Celador: Deposits",
            Instructions = "Mine [F2F5A9]300 obsidian ore[-].",
            NextStep = 3,
            Requirements = {
                {"Skill", "MiningSkill", 100},
            },
            QuestData = {
                Key = "MinerProfessionTierTwo4",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Mining",
                    "ObsidianOre"
                },
                Count = 300,
                Name = "Ore Mined",
                NamePlural = "Ore Mined",
            },
        },

        {--3
            Name = "Mining Celador: Deposits",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithPyros,
                QuestMarkers.BlacksmithValus,
                QuestMarkers.BlacksmithEldeir,
                QuestMarkers.BlacksmithHelm,
                QuestMarkers.BlacksmithBlackForest,
            },
            EndDialogue = {"[Miner IV] Mined that obsidian ore.","Well done, you've learned all I have to teach and you've the tools to make you own way. I grant you the title of grandmaster miner."},
            NextStep = 6,
            Requirements = {
                {"Skill", "MiningSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },

    },
    FisherProfessionIntro = {
        {--1
            Name = "Learn to Fish",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            NextStep = 2,
            EndDialogue = {"[Fisher Intro] Can you teach me to fish?","Anyone with patience can do it. Obtain a fishing rod and speak with me again. I happen to sell some right here!"},
            Requirements = {
                {"MaxTimesQuestDone", "FisherProfessionIntro", 1},
                --{"QuestActive", "FisherProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn to Fish",
            Instructions = "Get a [F2F5A9]Fishing Rod[-]. You can buy them from fisher merchants.",
            NextStep = 3,
            StartDialogue = {"[Fisher Intro] Can you teach me to fish?","Anyone with patience can do it. Obtain a fishing rod and speak with me again. I happen to sell some right here!"},
            Requirements = {
                {"MaxTimesQuestDone", "FisherProfessionIntro", 1},
                {"QuestActive", "FisherProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill", "FishingSkill", 30},
            },
            Goals = {
                {"HasItemInBackpack", "tool_fishing_rod"},
            },
        },
        {--3
            Name = "Learn to Fish",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            NextStep = 4,
            EndDialogue = {"[Fisher Intro] I have a fishing rod.","Being the fishing expert I am, stick.. string.. reel.. yep! That is definitely a fishing rod. Go find some water and dangle this part here over it. After that... hmmm I can't remember what happens next."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Learn to Fish",
            Instructions = "Equip a [F2F5A9]Fishing Rod[-] by double clicking it in your backpack or dragging it onto yourself in the character screen.",
            NextStep = 5,
            Goals = {
                {"Custom", function(playerObj)
                    local handObjects = {}
                    table.insert(handObjects, playerObj:GetEquippedObject("LeftHand"))
                    table.insert(handObjects, playerObj:GetEquippedObject("RightHand"))
                    local pick = 0
                    for i = 1, #handObjects do
                        if ( handObjects[i]:HasModule("tool_fishing_rod") ) then
                            pick = 1
                        end
                    end
                    if ( pick == 0 ) then return false end
                    return true
                end},
            },
        },
        {--5
            Name = "Learn to Fish",
            Instructions = "Catch a [F2F5A9]fish[-].",
            NextStep = 6,
            Goals = {
                {"Custom", function(playerObj)
                    local lifetimePlayerStats = playerObj:GetObjVar("LifetimePlayerStats")
                    if ( lifetimePlayerStats
                    and lifetimePlayerStats["FishCaught"]
                    and lifetimePlayerStats["FishCaught"] >= 1 ) then return true else return false end
                end},
            },
        },
        {--6
            Name = "Learn to Fish",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            EndDialogue = {"[Fisher Intro] I caught a fish!","Yeah, well the waters are practically swimming with them. You might find some other surprises in the depths too. Fishing is relaxing, but it's also a great source of food and treasures!"},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    FisherProfessionTierOne = {
        {--1
            Name = "Fishing Celador",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            NextStep = 2,
            EndDialogue = {"[Fisher I] Are the fish biting?","They always are! Go see if you can catch 100 or so."},
            Requirements = {
                {"MaxTimesQuestDone", "FisherProfessionTierOne", 1},
                --{"QuestActive", "FisherProfessionTierOne", false},
                {"MinTimesQuestDone", "FisherProfessionIntro", 1},
                {"Skill", "FishingSkill", 30},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Fishing Celador",
            Instructions = "Catch [F2F5A9]100[-] fish.",
            NextStep = 3,
            Requirements = {
                {"Skill", "FishingSkill", 30},
            },
            QuestData = {
                Key = "FisherProfessionTierOne2",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Fishing",
                    true
                },
                Count = 100,
                Name = "Fish",
                NamePlural = "Fish",
            },
        },
        {--3
            Name = "Fishing Celador",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            EndDialogue = {"[Fisher I] I caught 100 fish.","Here, take this rod. It will hold up better than yours."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperUpperPlains,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","tool_fishing_rod_light"},
                },
            },
        },
    },
    FisherProfessionTierTwo = {
        {--1
            Name = "Spotting Tero Fish",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            MarkerLocs = {
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            EndDialogue = {"[Fisher II] How is the fishing today?","Amazing as always! If you're having trouble you should knuckle down and catch 100 Spotted Tero Fish you can find them eveywhere!"},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "FisherProfessionTierTwo", 1},
                --{"QuestActive", "FisherProfessionTierTwo", false},
                {"MinTimesQuestDone", "FisherProfessionTierOne", 1},
                {"Skill", "FishingSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Spotting Tero Fish",
            Instructions = "Catch [F2F5A9]100 Spotted Tero Fish[-].",
            NextStep = 3,
            Requirements = {
                {"Skill", "FishingSkill", 50},
            },
            QuestData = {
                Key = "FisherProfessionTierTwo2",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Fishing",
                    "resource_fish_spotted_tero"
                },
                Count = 100,
                Name = "Spotted Tero Fish",
                NamePlural = "Spotted Tero Fish",
            },
        },
        {--3
            Name = "Spotting Tero Fish",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            MarkerLocs = {
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Fisher II] What did you tell me to catch?", "100 Spotted Tero Fish is what you need to bring me."},
            EndDialogue = {"[Fisher II] I caught all those fish.","Good, Spotted Tero are my goto when I feel like I am in a slump they are always bitin."},
            Requirements = {
                {"Skill", "FishingSkill", 50},
                {"HasItemInBackpack", "resource_fish_spotted_tero", 100},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_fish_spotted_tero", 100},
                {"TakeItem", "resource_fish_spotted_tero", 100},
            },
        },
    },
    FisherProfessionTierThree = {
        {--1
            Name = "Reel'n Razors",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            MarkerLocs = {
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            EndDialogue = {"[Fisher III] I am looking for a challenge.","Oh are ye now. Well you won't find much more challenge'n then Razor-Fish. They're elusive and smart, you catch even 50 of them and you're quite a fisherman."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "FisherProfessionTierThree", 1},
                --{"QuestActive", "FisherProfessionTierThree", false},
                {"MinTimesQuestDone", "FisherProfessionTierTwo", 1},
                {"Skill", "FishingSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Reel'n Razors",
            Instructions = "Catch [F2F5A9]50 Razor-Fish[-].",
            NextStep = 3,
            Requirements = {
                {"Skill", "FishingSkill", 80},
            },
            QuestData = {
                Key = "FisherProfessionTierThree2",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Fishing",
                    "resource_fish_razor"
                },
                Count = 50,
                Name = "Spotted Tero Fish",
                NamePlural = "Spotted Tero Fish",
            },
        },
        {--3
            Name = "Spotting Tero Fish",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            MarkerLocs = {
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Fisher III] What did you tell me to catch?", "You need to catch 50 Razor-Fish and bring them to me as proof."},
            EndDialogue = {"[Fisher III] Finally got all those fish caught.","Bit'n of a challenge eh? They do look grand though I might mount a few on my walls, erm, for comparison obviously."},
            Requirements = {
                {"Skill", "FishingSkill", 80},
                {"HasItemInBackpack", "resource_fish_razor", 50},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_fish_razor", 50},
                {"TakeItem", "resource_fish_razor", 50},
            },
        },
    },
    FisherProfessionTierFour = {
        {--1
            Name = "Bountiful Waters",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            MarkerLocs = {
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            EndDialogue = {"[Fisher IV] I hear tales of treasure.","Ah you've discovered why their are no poor fishermen. All the waters of New Celador are littered with the lost bounties of countless ships. No doubt you've found some of their maps. Go fish up a Soaked Map and come back to me."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "FisherProfessionTierFour", 1},
                --{"QuestActive", "FisherProfessionTierFour", false},
                {"MinTimesQuestDone", "FisherProfessionTierThree", 1},
                {"Skill", "FishingSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Bountiful Waters",
            Instructions = "Catch [F2F5A9]1 soaked map[-].",
            NextStep = 3,
            Requirements = {
                {"Skill", "FishingSkill", 100},
            },
            QuestData = {
                Key = "FisherProfessionTierFour2",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Fishing",
                    "sos_map_1"
                },
                Count = 1,
                Name = "Soaked Map",
                NamePlural = "Soaked Map",
            },
        },
        {--3
            Name = "Spotting Tero Fish",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            NextStep = 4,
            MarkerLocs = {
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Fisher IV] What map was I looking for?", "You need to fish up a soaked map."},
            EndDialogue = {"[Fisher III] I found one!","That you did, this be excite'n. Follow the instructions it gives you and come back to me when you've fished up the bounty."},
            Requirements = {
                {"Skill", "FishingSkill", 100},
                {"HasItemInBackpack", "sos_map_1", 1},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 4
            Name = "Bountiful Waters",
            Instructions = "Discover the location of the [F2F5A9]soaked map[-] and retrieve the contents.",
            NextStep = 5,
            Requirements = {
                {"Skill", "FishingSkill", 100},
            },
            QuestData = {
                Key = "FisherProfessionTierFour4",
                Type = "Gathering",
                QuestArgs = 
                {
                    "SOS",
                    "sos_map_1"
                },
                Count = 1,
                Name = "Sunken Treasure",
                NamePlural = "Sunken Treasure",
            },
        },
        {--5
            Name = "Bountiful Waters",
            Instructions = "Speak to a [F2F5A9]fisher[-].",
            MarkerLocs = {
                QuestMarkers.FisherHelm,
                QuestMarkers.FisherPyros,
                QuestMarkers.FisherValus,
            },
            EndDialogue = {"[Fisher IV] I enjoyed that.","Good to hear. It is a reward'n part of our humble trade. You now have the skill and the tools you need to make you own way."},
            Requirements = {
                {"Skill", "FishingSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    BlacksmithProfessionIntro = {
        {--blac0.1
            Name = "Learn Blacksmithing",
            Instructions = "Speak to any [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            StartDialogue = {"[Blacksmith Intro] Can you teach me blacksmithing?","Interested in the art, eh? I'll start you off easy; show me 10 bars of Iron. Feel free to use my anvil over there."},
            Requirements = {
                {"MaxTimesQuestDone", "BlacksmithProfessionIntro", 1},
                {"CapStepCompletions", "BlacksmithProfessionIntro", 1, 1},
            },
            OnStart = {
                {"SetSkill", "MetalsmithSkill", 30},
            },
            NextStep = 2,
        },
        {--blac0.2
            Name = "Learn Blacksmithing",
            Instructions = "Make 10 [F2F5A9]Iron Bars[-] from [F2F5A9]Iron Ore[-] at an [F2F5A9]Anvil[-].",
            IneligibleDialogue = { "[Blacksmith Intro] How are these iron bars?","I don't see 10 iron bars in your bag. Did you forget some?"},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionIntro", 2},
                {"HasCraftedItem", "resource_iron", 10},
            },
            Goals = {
                {"StillEligible", "BlacksmithProfessionIntro", 2},
            },
            NextStep = 3,
        },
        {--blac0.3
            Name = "Learn Blacksmithing",
            Instructions = "Show the [F2F5A9]Iron Bars[-] to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith Intro] Here is my first attempt.","Let me see 'em! Hmm. Alright. Next, I want you to show me 5 Iron Daggers"},
            IneligibleDialogue = { "[Blacksmith Intro] How are these iron bars?","Unacceptable. I want to see 10 iron bars that you crafted, no less. And no pawning for them from a friend!"},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionIntro", 3},
                {"HasCraftedItem", "resource_iron", 10},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionIntro", 3},
            },
            NextStep = 4,
        },
        {--blac0.4
            Name = "Learn Blacksmithing",
            Instructions = "Make 5 [F2F5A9]Iron Daggers[-] at an [F2F5A9]Anvil[-].",
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionIntro", 4},
                {"HasCraftedItem", "weapon_dagger", 5, "Material", "Iron"},
            },
            Goals = {
                {"StillEligible", "BlacksmithProfessionIntro", 4},
            },
            NextStep = 5,
        },
        {--blac0.5
            Name = "Learn Blacksmithing",
            Instructions = "Show the [F2F5A9]Iron Daggers[-] to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith Intro] Check out these daggers.","These are kinda heavy, but their shapes formed pretty well! Alright, now I want to see you make some tools. Show me an Iron Hatchet, a Shovel, and a pack of Lockpicks."},
            IneligibleDialogue = { "[Blacksmith Intro] What do you think of these daggers?","Unacceptable. I want to see 10 Iron Daggers that you crafted, no less. And no pawning for them from a friend!"},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionIntro", 5},
                {"HasCraftedItem", "weapon_dagger", 5, "Material", "Iron"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionIntro", 5},
            },
            NextStep = 6,
        },
        {--blac0.6
            Name = "Learn Blacksmithing",
            Instructions = "Make an [F2F5A9]Iron Hatchet,[-] a [F2F5A9]Shovel,[-] and a pack of [F2F5A9]Lockpicks[-] at an [F2F5A9]Anvil[-].",
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionIntro", 6},
                {"HasCraftedItem", "tool_lockpick", 1},
                {"HasCraftedItem", "tool_shovel", 1},
                {"HasCraftedItem", "tool_hatchet", 1},
            },
            Goals = {
                {"StillEligible", "BlacksmithProfessionIntro", 6},
            },
            NextStep = 7,
        },
        {--blac0.7
            Name = "Learn Blacksmithing",
            Instructions = "Show the [F2F5A9]Iron Hatchet,[-] [F2F5A9]Shovel,[-] and [F2F5A9]Lockpicks[-] to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith Intro] I don't know how I carry all this.","Strong back muscles! Every good blacksmith needs them! You've got a nice number of crafts under your belt. Talk to me when you think you're up for fulfilling orders."},
            IneligibleDialogue = { "[Blacksmith Intro] How do these tools look?","Unacceptable. I want to see 3 specific tools that you crafted, no less. And no pawning for them from a friend!"},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionIntro", 7},
                {"HasCraftedItem", "tool_lockpick", 1},
                {"HasCraftedItem", "tool_shovel", 1},
                {"HasCraftedItem", "tool_hatchet", 1},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionIntro", 7},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have completed the Intro to Blacksmithing.","info")
                end},
            },
        },
    },
    BlacksmithProfessionTierOne = {
        {--blac1.1
            Name = "Blacksmith Commissions",
            Instructions = "Speak to any [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            StartDialogue = {"[Blacksmith I] Is it safe to inhale all that smoke?","Hasn't killed me! I take it you want to make some money. Ask me about Crafting Orders."},
            IneligibleDialogue = { "[Blacksmith I] Is there more to learn?","You never stop learning. But you need to improve a bit more before I can guide you again."},
            IneligibleCheck = {
                {"MinTimesQuestDone", "BlacksmithProfessionIntro", 1},
                {"MaxTimesQuestDone", "BlacksmithProfessionTierOne", 1},
                --{"BlacksmithProfessionTierOne", 1, false},
            },
            Requirements = {
                {"Skill", "MetalsmithSkill", 30},
                {"MaxTimesQuestDone", "BlacksmithProfessionTierOne", 1},
                {"MinTimesQuestDone", "BlacksmithProfessionIntro", 1},
                {"CapStepCompletions", "BlacksmithProfessionTierOne", 1, 1},
            },
            NextStep = 2,
        },
        {--blac1.2
            Name = "Blacksmith Commissions",
            Instructions = "Speak to a [F2F5A9]blacksmith[-] and inquire about [F2F5A9]Crafting Orders[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierOne", 2},
                {"HasSpecificItem", "crafting_order", "OrderSkill", "MetalsmithSkill"},
            },
            Goals = {
                {"StillEligible", "BlacksmithProfessionTierOne", 2},
            },
            NextStep = 3,
        },
        {--blac1.3
            Name = "Blacksmith Commissions",
            Instructions = "Fulfill the [F2F5A9]Crafting Order[-] then talk to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith I] Got my first completed order.","Good! Solid work, here is your payment. Keep them coming!"},
            IneligibleDialogue = { "[Blacksmith I] Is this order good?","Hmm. I don't see any completed blacksmith order signed for you. Hope you're not trying to pull a fast one on me!"},
            IneligibleCheck = {
                {"HasItemInBackpack", "crafting_order"},
            },
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierOne", 3},
                {"HasCompletedCraftingOrder", "MetalsmithSkill"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionTierOne", 3},
            },
            OnEnd = {
                {"AcceptCraftingOrder", "MetalsmithSkill"},
            },
            NextStep = 4,
        },
        {--blac1.4
            Name = "Blacksmith Commissions",
            Instructions = "Fulfill another Blacksmith [F2F5A9]Crafting Order[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith I] Got another order completed.","Let me see it! Wrap up one more for me and I think you'll be on the right track."},
            IneligibleDialogue = { "[Blacksmith I] Is this order good?","Hmm. I don't see any completed blacksmith order signed for you. Hope you're not trying to pull a fast one on me!"},
            IneligibleCheck = {
                {"HasItemInBackpack", "crafting_order"},
            },
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierOne", 4},
                {"HasCompletedCraftingOrder", "MetalsmithSkill"},  
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionTierOne", 4},
            },
            OnEnd = {
                {"AcceptCraftingOrder", "MetalsmithSkill"},
            },
            NextStep = 5,
        },
        {--blac1.5
            Name = "Blacksmith Commissions",
            Instructions = "Fulfill one last Blacksmith [F2F5A9]Crafting Order[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith I] Order number three.","Looks great! You have developed wonderfully as a blacksmith. My next request is to complete a harvesting job for a Stocker."},
            IneligibleDialogue = { "[Blacksmith I] Is this order good?","Hmm. I don't see any completed blacksmith order signed for you. Hope you're not trying to pull a fast one on me!"},
            IneligibleCheck = {
                {"HasItemInBackpack", "crafting_order"},
            },
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierOne", 5},
                {"HasCompletedCraftingOrder", "MetalsmithSkill"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionTierOne", 5},
            },
            OnEnd = {
                {"AcceptCraftingOrder", "MetalsmithSkill"},
            },
            NextStep = 6,
        },
        {--blac1.6
            Name = "Blacksmith Commissions",
            Instructions = "Accept a Mining [F2F5A9]Harvesting Mission[-] from a [F2F5A9]Stocker[-].",
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierOne", 6},
            },
            MarkerLocs = {
                QuestMarkerGroups.AllHarvestMissions,
            },
            Goals = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("Missions")
                    if not missions then return false end
                    for i=1, #missions do
                        if ( missions[i] and next(missions[i]) and missions[i]["Type"] == "Harvest" and missions[i]["Category"] and missions[i]["Category"] == "HarvestMining" ) then 
                            return true 
                        else 
                            return false 
                        end
                    end
                    return false
                end},
            },
            NextStep = 7,
        },
        {--blac1.7
            Name = "Blacksmith Commissions",
            Instructions = "Complete a Mining [F2F5A9]Harvesting Mission[-].",
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierOne", 7},
            },
            OnStart = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("LifetimePlayerStats")
                    if ( missions
                    and missions["HarvestMissionsCompleted"] ) then
                        Quests.SetQuestData( playerObj, "HarvestMissionQuests", "Count", missions["HarvestMissionsCompleted"] )
                    else
                        Quests.SetQuestData( playerObj, "HarvestMissionQuests", "Count", 0 )
                    end
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("LifetimePlayerStats") or {}
                    local startCount = Quests.GetQuestData( playerObj, "HarvestMissionQuests", "Count") or 0
                    if ( missions
                    and missions["HarvestMissionsCompleted"]
                    and missions["HarvestMissionsCompleted"] > startCount ) then
                        return true else return false
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "HarvestMissionQuests")
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "HarvestMissionQuests")
                end},
            },
            NextStep = 8,
        },
        {--blac1.8
            Name = "Blacksmith Commissions",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogue = {"[Blacksmith I] I've completed the Harvesting Mission.","Wonderful. With all this knowledge, you are now qualified to be an Apprentice Blacksmith."},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierOne", 8},
            },
            Goals = {
                {"TurnIn"},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have achieved the Blacksmith rank of Apprentice.","info")
                end},
            },
        },
    },
    BlacksmithProfessionTierTwo = {
        {--blac2.1
            Name = "What's Next?",
            Instructions = "Speak to a [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            StartDialogue = {"[Blacksmith II] I've practiced even more.","Oh yeah? Let's put those muscles to use, then. Make 2 sets of copper chain armor and drop them off at the blacksmith in Belhaven."},
            IneligibleDialogue = { "[Blacksmith II] Is there more to learn?","You never stop learning. But you need to improve a bit more before I can guide you again."},
            IneligibleCheck = { 
                {"MinTimesQuestDone", "BlacksmithProfessionTierOne", 1},
                {"MaxTimesQuestDone", "BlacksmithProfessionTierTwo", 1},
                --{"BlacksmithProfessionTierOne", 7, true},
                --{"BlacksmithProfessionTierTwo", 1, false},
            },
            Requirements = {
                {"Skill", "MetalsmithSkill", 50},
                {"MaxTimesQuestDone", "BlacksmithProfessionTierTwo", 1},
                {"MinTimesQuestDone", "BlacksmithProfessionTierOne", 1},
                {"CapStepCompletions", "BlacksmithProfessionTierTwo", 1, 1},
            },
            NextStep = 2,
        },
        {--blac2.2
            Name = "Carrier",
            Instructions = "Make 2 sets of [F2F5A9]Copper Chain Armor[-] (Helm, Leggings, and Torso).",
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierTwo", 2},
                {"HasCraftedItem", "armor_chain_helm", 2, "Material", "Copper"},
                {"HasCraftedItem", "armor_chain_tunic", 2, "Material", "Copper"},
                {"HasCraftedItem", "armor_chain_leggings", 2, "Material", "Copper"},
            },
            Goals = {
                {"StillEligible", "BlacksmithProfessionTierTwo", 2},
            },
            NextStep = 3,
        },
        {--blac2.3
            Name = "Carrier",
            Instructions = "Deliver the 2 sets of [F2F5A9]Copper Chain Armor[-] to [F2F5A9]Belhaven[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithBelhaven,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith II] I was asked to bring you this armor.","Thanks, hun. You heading up north soon? Could you bring some things to Eldeir for me?"},
            IneligibleDialogue = { "[Blacksmith II] I heard you were looking for copper armor?","I am, but I need two full sets."},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierTwo", 3},
                {"HasCraftedItem", "armor_chain_helm", 2, "Material", "Copper"},
                {"HasCraftedItem", "armor_chain_tunic", 2, "Material", "Copper"},
                {"HasCraftedItem", "armor_chain_leggings", 2, "Material", "Copper"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionTierTwo", 3},
            },
            OnEnd = {
                {"TakeCraftedItems", "armor_chain_helm", 2, "Material", "Copper"},
                {"TakeCraftedItems", "armor_chain_tunic", 2, "Material", "Copper"},
                {"TakeCraftedItems", "armor_chain_leggings", 2, "Material", "Copper"},
            },
            NextStep = 4,
        },
        {--blac2.4
            Name = "Carrier",
            Instructions = "The blacksmith of [F2F5A9]Belhaven[-] has a request for you.",
            MarkerLocs = {
                QuestMarkers.BlacksmithBelhaven,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith II] I can stop by Eldeir.","Wonderful! They are requesting 15 Golden Great Axes."},
            IneligibleDialogue = { "[Blacksmith II] What do you need delivered?","I have something in mind, but I'm not sure you are up to the task."},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierTwo", 4},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 5,
        },
        {--blac2.5
            Name = "Carrier",
            Instructions = "Make 15 [F2F5A9]Golden Great Axes[-].",
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierTwo", 5},
                {"HasCraftedItem", "weapon_greataxe", 15, "Material", "Gold"},
            },
            Goals = {
                {"StillEligible", "BlacksmithProfessionTierTwo", 5},
            },
            NextStep = 6,
        },
        {--blac2.6
            Name = "Carrier",
            Instructions = "Deliver the 15 [F2F5A9]Golden Great Axes[-] to [F2F5A9]Eldeir[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithEldeir,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith II] Belhaven sent me to deliver these.","Good timing, we need them for a ceremony later. While you're here, could you help me with something?"},
            IneligibleDialogue = { "[Blacksmith II] Were you expecting these?","What do you mean? I don't see any of my orders."},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierTwo", 6},
                {"HasCraftedItem", "weapon_greataxe", 15, "Material", "Gold"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionTierTwo", 6},
            },
            OnEnd = {
                {"TakeCraftedItems", "weapon_greataxe", 15, "Material", "Gold"},
            },
            NextStep = 7,
        },
        {--blac2.7
            Name = "Carrier",
            Instructions = "The blacksmith of [F2F5A9]Eldeir[-] has a request for you.",
            MarkerLocs = {
                QuestMarkers.BlacksmithEldeir,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith II] I can help you.","Great! Could you make and deliver 10 Iron Spears and 5 Gold Quarterstaffs to the Black Forest Outpost?"},
            IneligibleDialogue = { "[Blacksmith II] Can I help you?","I'm afraid not. I need someone with a bit more skill for this task."},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierTwo", 7},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 8,
        },
        {--blac2.8
            Name = "Carrier",
            Instructions = "Make 10 [F2F5A9]Iron Spears[-] and 5 [F2F5A9]Gold Quarterstaffs[-].",
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierTwo", 8},
                {"HasCraftedItem", "weapon_spear", 10, "Material", "Iron"},
                {"HasCraftedItem", "weapon_quarterstaff", 5, "Material", "Gold"},
            },
            Goals = {
                {"StillEligible", "BlacksmithProfessionTierTwo", 8},
            },
            NextStep = 9,
        },
        {--blac2.9
            Name = "Carrier",
            Instructions = "Deliver 10 [F2F5A9]Iron Spears[-] and 5 [F2F5A9]Gold Quarterstaffs[-] to the [F2F5A9]Black Forest Outpost[-].",
            MarkerLocs = {
                QuestMarkers.BlacksmithBlackForest,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith II] These are from Eldeir.","Thanks! These will work great for training."},
            IneligibleDialogue = { "[Blacksmith II] Is that what you wanted?","No, that's not quite right."},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierTwo", 9},
                {"HasCraftedItem", "weapon_spear", 10, "Material", "Iron"},
                {"HasCraftedItem", "weapon_quarterstaff", 5, "Material", "Gold"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionTierTwo", 9},
            },
            OnEnd = {
                {"TakeCraftedItems", "weapon_spear", 10, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_quarterstaff", 5, "Material", "Gold"},
            },
            NextStep = 10,
        },
        {--blac2.10
            Name = "Carrier",
            Instructions = "Speak to any [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith II] I have done enough deliveries.","You've been a great help. Depending on your ability, there may be more we can teach you."},
            IneligibleDialogue = { "[Blacksmith II] What's next?","There is higher ground to reach, but you are not ready for the climb."},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierTwo", 10},
            },
            Goals = {
                {"TurnIn"},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have achieved the Blacksmith rank of Journeyman.","info")
                end},
            },
        },
    },
    BlacksmithProfessionTierThree = {
        {--blac3.1
            Name = "Mass Production",
            Instructions = "Speak to any [F2F5A9]blacksmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            StartDialogue = {"[Blacksmith III] I want to be a Master Blacksmith.","I like your spirit! Show me that you can keep up with a Master's output."},
            IneligibleDialogue = { "[Blacksmith III] Is there more to learn?","You never stop learning. But you need to improve a bit more before I can guide you again."},
            IneligibleCheck = { 
                {"MinTimesQuestDone", "BlacksmithProfessionTierTwo", 1},
                {"MaxTimesQuestDone", "BlacksmithProfessionTierThree", 1},
                --{"BlacksmithProfessionTierTwo", 10, true},
                --{"BlacksmithProfessionTierThree", 1, false},
            },
            Requirements = {
                {"Skill", "MetalsmithSkill", 80},
                {"MaxTimesQuestDone", "BlacksmithProfessionTierThree", 1},
                {"MinTimesQuestDone", "BlacksmithProfessionTierTwo", 1},
                {"CapStepCompletions", "BlacksmithProfessionTierThree", 1, 1},
            },
            NextStep = 2,
        },
        {--blac3.2
            Name = "Mass Production",
            Instructions = "Make a set of every craftable weapon in [F2F5A9]Iron[-].",
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierThree", 2},

                -- Costs 44 iron, 116 weight, 20 items. Could do 2?
                {"HasCraftedItem", "weapon_longsword", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_broadsword", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_saber", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_katana", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_greataxe", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_largeaxe", 1, "Material", "Iron"},

                {"HasCraftedItem", "weapon_mace", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_hammer", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_maul", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_war_mace", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_quarterstaff", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_warhammer", 1, "Material", "Iron"},
                
                {"HasCraftedItem", "weapon_dagger", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_kryss", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_poniard", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_bone_dagger", 1, "Material", "Iron"},
                
                {"HasCraftedItem", "weapon_warfork", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_voulge", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_spear", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_halberd", 1, "Material", "Iron"},
            },
            Goals = {
                {"StillEligible", "BlacksmithProfessionTierThree", 2},
            },
            NextStep = 3,
        },
        {--blac3.3
            Name = "Mass Production",
            Instructions = "Give the [F2F5A9]Iron[-] weapon set to any blacksmith.",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith III] Here is your armory.","Looks good! Next, show me you know your way around New Celador."},
            IneligibleDialogue = { "[Blacksmith III] How are these?","That doesn't look like a full set of Iron weapons to me! Check again."},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierThree", 3},

                -- Costs 44 iron, 116 weight, 20 items. Could do 2?
                {"HasCraftedItem", "weapon_longsword", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_broadsword", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_saber", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_katana", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_greataxe", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_largeaxe", 1, "Material", "Iron"},

                {"HasCraftedItem", "weapon_mace", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_hammer", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_maul", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_war_mace", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_quarterstaff", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_warhammer", 1, "Material", "Iron"},
                
                {"HasCraftedItem", "weapon_dagger", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_kryss", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_poniard", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_bone_dagger", 1, "Material", "Iron"},
                
                {"HasCraftedItem", "weapon_warfork", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_voulge", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_spear", 1, "Material", "Iron"},
                {"HasCraftedItem", "weapon_halberd", 1, "Material", "Iron"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionTierThree", 3},
            },
            OnEnd = {
                {"TakeCraftedItems", "weapon_longsword", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_broadsword", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_saber", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_katana", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_greataxe", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_largeaxe", 1, "Material", "Iron"},

                {"TakeCraftedItems", "weapon_mace", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_hammer", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_maul", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_war_mace", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_quarterstaff", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_warhammer", 1, "Material", "Iron"},
                
                {"TakeCraftedItems", "weapon_dagger", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_kryss", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_poniard", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_bone_dagger", 1, "Material", "Iron"},
                
                {"TakeCraftedItems", "weapon_warfork", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_voulge", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_spear", 1, "Material", "Iron"},
                {"TakeCraftedItems", "weapon_halberd", 1, "Material", "Iron"},
            },
            NextStep = 4,
        },
        {--blac3.4
            Name = "Mass Production",
            Instructions = "Make 500 [F2F5A9]Cobalt Bars[-].",
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierThree", 4},
                
                {"HasCraftedItem", "resource_cobalt", 500},
            },
            Goals = {
                {"StillEligible", "BlacksmithProfessionTierThree", 4},
            },
            NextStep = 5,
        },
        {--blac3.5
            Name = "Mass Production",
            Instructions = "Give the 500 [F2F5A9]Cobalt Bars[-] to any blacksmith.",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith III] A collection of Cobalt for you.","Good haul, I hope you've found some good mining spots by now. Here is our last task for you before Mastery."},
            IneligibleDialogue = { "[Blacksmith III] How are these?","Not good enough, I don't see 500 Cobalt Bars made by you!"},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierThree", 5},
                
                {"HasCraftedItem", "resource_cobalt", 500},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionTierThree", 5},
            },
            OnEnd = {
                {"TakeCraftedItems", "resource_cobalt", 500},
            },
            NextStep = 6,
        },
        {--blac3.6
            Name = "Mass Production",
            Instructions = "Make a set of [F2F5A9]Obsidian Full Plate Armor[-] (Helm, Leggings, and Torso).",
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierThree", 6},

                --costs 13 obsidian, 22 weight, but high skill needed = higher chance to fail + obsidian is rarer?
                {"HasCraftedItem", "armor_fullplate_helm", 1, "Material", "Obsidian"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Obsidian"},
                {"HasCraftedItem", "armor_fullplate_leggings", 1, "Material", "Obsidian"},
            },
            Goals = {
                {"StillEligible", "BlacksmithProfessionTierThree", 6},
            },
            NextStep = 7,
        },
        {--blac3.7
            Name = "Mass Production",
            Instructions = "Give the set of [F2F5A9]Obsidian Full Plate Armor[-] to any blacksmith.",
            MarkerLocs = {
                QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith III] Finished the set.","Wonderful armor! You keep this one, actually. You have proven yourself in our art. Congratulations, Master Blacksmith."},
            IneligibleDialogue = { "[Blacksmith III] How is this?","That doesn't look like Obsidian Full Plate armor to me! Try again."},
            Requirements = {
                {"StepMustBeActive", "BlacksmithProfessionTierThree", 7},

                {"HasCraftedItem", "armor_fullplate_helm", 1, "Material", "Obsidian"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Obsidian"},
                {"HasCraftedItem", "armor_fullplate_leggings", 1, "Material", "Obsidian"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "BlacksmithProfessionTierThree", 7},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have achieved the Blacksmith rank of Master.","info")
                end},
            },
        },
    },
    BlacksmithProfessionTierFour = {
        {--blac4.1
            Name = "Sweat and Blood",
            Instructions = "Travel to the [F2F5A9]Southern Rim[-] and speak to [F2F5A9]Yewell Steeltounge[-] outside [F2F5A9]Iris Cave[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            StartDialogue = {"[Blacksmith IV] I want to be a Grandmaster.","The road to becoming a grandmaster is soaked in both sweat and blood.  You have shown you can put in the sweat, now can you sacrifice the blood? There is one who can guide you on these last steps of your journey. Seek out Yewell Steeltounge outside the Iris Cave in the Southern Rim."},
            IneligibleDialogue = { "[Blacksmith IV] Is there more to learn?","There is one final step but I need you to train even more, first."},
            IneligibleCheck = {
                {"MinTimesQuestDone", "BlacksmithProfessionTierThree", 1},
                {"MaxTimesQuestDone", "BlacksmithProfessionTierFour", 1},
                --{"BlacksmithProfessionTierThree", 7, true},
                --{"BlacksmithProfessionTierFour", 1, false},
            },
            Requirements = {
                --{"Disabled"},
                {"Skill", "MetalsmithSkill", 100},
                {"MaxTimesQuestDone", "BlacksmithProfessionTierFour", 1},
                {"MinTimesQuestDone", "BlacksmithProfessionTierThree", 1},
                {"CapStepCompletions", "BlacksmithProfessionTierFour", 1, 1},
            },
            NextStep = 2,
        },

        {--blac4.2
            Name = "Sweat and Blood",
            Instructions = "Travel to the [F2F5A9]Southern Rim[-] and speak to [F2F5A9]Yewell Steeltounge[-] outside [F2F5A9]Iris Cave[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogue = {"[Blacksmith IV] I want to be a Grandmaster.","Aren't you a sight to see. Let me guess, they had you make order after order and called you a master metalsmith?  Bah, I doubt you'd know a cobalt vien from a carrot crop the way you're looking at me. Smithing isn't about how much you can make, it is about feeling the metal give with each strike of the hammer and if you lucky giving a bit of yourself with each strike."},
            Requirements = {
                --{"Disabled"},
                {"Skill", "MetalsmithSkill", 100},
                {"StepMustBeActive", "BlacksmithProfessionTierFour", 2},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 3,
        },

        {--blac4.3
            Name = "Price You Pay",
            Instructions = "Speak to [F2F5A9]Yewell Steeltounge[-] outside [F2F5A9]Iris Cave[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogue = {"[Blacksmith IV] How can I prove myself?.","I'll make this brief, you show me a weapon you've crafted that meets the highest quality [50% attack bonus].  You do that, and we'll talk about you becoming a grandmaster metalsmith.  Don't bother bringing me scrap, those can go on the new fangled township boards everyone likes so much."},
            Requirements = {
                --{"Disabled"},
                {"Skill", "MetalsmithSkill", 100},
                {"StepMustBeActive", "BlacksmithProfessionTierFour", 3},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 4,
        },

        {--blac4.4
            Name = "Cost Worth Paying",
            Instructions = "Bring a 50% damage weapon you crafted to [F2F5A9]Yewell Steeltounge[-] outside [F2F5A9]Iris Cave[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith IV] How is this?.","This will do. You've done quite well actually."},
            IneligibleDialogue = { "[Blacksmith IV] What did you need?","I need to see weapon crafted by you that does maximum attack [50% bonus]."},
            Requirements = {
                {"Skill", "MetalsmithSkill", 100},
                {"StepMustBeActive", "BlacksmithProfessionTierFour", 4},
                {"Custom", function(playerObj)
                    local backpackObj = playerObj:GetEquippedObject("Backpack")
                    local hasItem = false
                    ForEachItemInContainerRecursive(backpackObj, function( item )
                        --DebugMessage("ItemName: " .. item:GetName())
                        if(
                            item:HasObjVar("WeaponType") -- is it a weapon?
                            and item:GetObjVar("Crafter") == playerObj -- did the player craft this?
                            and item:GetObjVar("AttackBonus") == 50 -- is it 50% attack bonus?
                         ) then
                            hasItem = true
                            return false
                         end
                         return true
                    end)

                    --DebugMessage("HasItems: " .. tostring(hasItem))
                    return hasItem
                end},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 5,
        },

        {--blac4.5
            Name = "Flip of a Coin",
            Instructions = "Speak to [F2F5A9]Yewell Steeltounge[-] outside [F2F5A9]Iris Cave[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogue = {"[Blacksmith IV] Am I done?.","To be honest with you, I wasn't sure you would stick to it. But you did, and there's something to be said for that. Now that I know you have the what it takes to put in the sweat and blood. Lets have you bring me an Artisan's Crafting Commission. It is a rare reward earned through completing crafting orders."},
            Requirements = {
                --{"Disabled"},
                {"Skill", "MetalsmithSkill", 100},
                {"StepMustBeActive", "BlacksmithProfessionTierFour", 5},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 6,
        },

        {--blac4.6
            Name = "Flip of a Coin",
            Instructions = "Bring an artisan's commission to [F2F5A9]Yewell Steeltounge[-] outside [F2F5A9]Iris Cave[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Blacksmith IV] I have the commission.","Would you looky there. I've not seen one of these in ages. I want to formally welcome you to the rank of grandmaster metalsmith."},
            IneligibleDialogue = { "[Blacksmith IV] What did you need?","I need you to bring me an Artisan's Crafting Commission. You can get them from crafitng orders."},
            Requirements = {
                --{"Disabled"},
                {"Skill", "MetalsmithSkill", 100},
                {"StepMustBeActive", "BlacksmithProfessionTierFour", 6},
                {"HasItemInBackpack", "artisan_crafting_commission_blacksmith", 1},
                -- TODO: REQUIRE An artisans token!
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "artisan_crafting_commission_blacksmith", 1},
                {"TakeItem", "artisan_crafting_commission_blacksmith", 1},
            },
        },
        
    },
    CarpenterProfessionIntro = {
        {--carp0.1
            Name = "Learn Carpentry",
            Instructions = "Speak to any [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            StartDialogue = {"[Carpenter Intro] Can you teach me carpentry?","I sure can! Let me see ya refine some wooden boards on that Carpentry Table over there."},
            Requirements = {
                {"MaxTimesQuestDone", "CarpenterProfessionIntro", 1},
                {"CapStepCompletions", "CarpenterProfessionIntro", 1, 1},
            },
            OnStart = {
                {"SetSkill", "WoodsmithSkill", 30},
            },
            NextStep = 2,
        },
        {--carp0.2
            Name = "Learn Carpentry",
            Instructions = "Make 10 [F2F5A9]Boards[-] from [F2F5A9]Wood[-] at a [F2F5A9]Carpentry Table[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionIntro", 2},
                {"HasCraftedItem", "resource_boards", 10},
            },
            Goals = {
                {"StillEligible", "CarpenterProfessionIntro", 2},
            },
            NextStep = 3,
        },
        {--carp0.3
            Name = "Learn Carpentry",
            Instructions = "Show the [F2F5A9]Wooden Boards[-] to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpenter Intro] I managed to craft some boards.","Not bad, now how do ya handle something finer? Try your hand at making some arrows."},
            IneligibleDialogue = { "[Carpenter Intro] How are these boards?","Not good enough. I want to see 10 of them made by you."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionIntro", 3},
                {"HasCraftedItem", "resource_boards", 10},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionIntro", 3},
            },
            NextStep = 4,
        },
        {--carp0.4
            Name = "Learn Carpentry",
            Instructions = "Craft 20 [F2F5A9]Arrows[-] at a [F2F5A9]Carpentry Table[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionIntro", 4},
                {"HasCraftedItem", "arrow", 20},
            },
            Goals = {
                {"StillEligible", "CarpenterProfessionIntro", 4},
            },
            NextStep = 5,
        },
        {--carp0.5
            Name = "Learn Carpentry",
            Instructions = "Show the [F2F5A9]Arrows[-] to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpenter Intro] Here are the arrows.","Solid! I'd maybe put one or two of these up for sale. Next, let's see some musical instruments."},
            IneligibleDialogue = { "[Carpenter Intro] Are these enough arrows?","No, I wanted 20 arrows made by you."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionIntro", 5},
                {"HasCraftedItem", "arrow", 20},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionIntro", 5},
            },
            NextStep = 6,
        },
        {--carp0.6
            Name = "Learn Carpentry",
            Instructions = "Craft a [F2F5A9]Lute[-], a [F2F5A9]Flute[-], and a [F2F5A9]Drum[-] at a [F2F5A9]Carpentry Table[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionIntro", 6},
                {"HasCraftedItem", "tool_lute", 1},
                {"HasCraftedItem", "tool_flute", 1},
                {"HasCraftedItem", "tool_drum", 1},
            },
            Goals = {
                {"StillEligible", "CarpenterProfessionIntro", 6},
            },
            NextStep = 7,
        },
        {--carp0.7
            Name = "Learn Carpentry",
            Instructions = "Show the [F2F5A9]Lute[-], [F2F5A9]Flute[-], and [F2F5A9]Drum[-] to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpenter Intro] Finished the instruments.","Getting the hang of this, it seems! Try playing them later and hear how they sound! Good work, pal. Hone your skills some more on your own time then come speak with me again."},
            IneligibleDialogue = { "[Carpenter Intro] Are these instruments good?","You're on you way, but I need a lute, flute, and drum all made by you."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionIntro", 7},
                {"HasCraftedItem", "tool_lute", 1},
                {"HasCraftedItem", "tool_flute", 1},
                {"HasCraftedItem", "tool_drum", 1},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionIntro", 7},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have completed the Intro to Carpentry.","info")
                end},
            },
        },
    },
    CarpenterProfessionTierOne = {
        {--carp1.1
            Name = "Taking Orders",
            Instructions = "Speak to any [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            StartDialogue = {"[Carpentry I] I'm getting good use of that table.","It's a start. When ya think you're ready, show me how you handle a Crafting Order. Ask me about 'em later."},
            IneligibleDialogue = { "[Carpentry I] Is there more to learn?","Not as you are right now. Refine your skill some more."},
            IneligibleCheck = {
                {"MinTimesQuestDone", "CarpenterProfessionIntro", 1},
                {"MaxTimesQuestDone", "CarpenterProfessionTierOne", 1},
                --{"CarpenterProfessionIntro", 7, true},
                --{"CarpenterProfessionTierOne", 1, false},
            },
            Requirements = {
                {"Skill", "WoodsmithSkill", 30},

                {"MaxTimesQuestDone", "CarpenterProfessionTierOne", 1},
                {"MinTimesQuestDone", "CarpenterProfessionIntro", 1},
                {"CapStepCompletions", "CarpenterProfessionTierOne", 1, 1},
            },
            NextStep = 2,
        },
        {--carp1.2
            Name = "Taking Orders",
            Instructions = "Speak to a [F2F5A9]woodsmith[-] and inquire about [F2F5A9]Crafting Orders[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierOne", 2},
                --{"HasCompletedCraftingOrder", "WoodsmithSkill"},
                {"HasSpecificItem", "crafting_order", "OrderSkill", "WoodsmithSkill"},
            },
            Goals = {
                {"StillEligible", "CarpenterProfessionTierOne", 2},
            },
            NextStep = 3,
        },
        {--carp1.3
            Name = "Taking Orders",
            Instructions = "Turn the [F2F5A9]Crafting Order[-] in to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry I] Finished an order.","Hey, not too shabby! Give me another one."},
            IneligibleDialogue = { "[Carpentry I] Is this order good?","I'm not seeing any completed Carpentry order done by you."},
            IneligibleCheck = {
                {"HasItemInBackpack", "crafting_order"},
            },
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierOne", 3},
                {"HasCompletedCraftingOrder", "WoodsmithSkill"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionTierOne", 3},
            },
            OnEnd = {
                {"AcceptCraftingOrder", "WoodsmithSkill"},
            },
            NextStep = 4,
        },
        {--carp1.4
            Name = "Taking Orders",
            Instructions = "Fulfill another Woodsmith [F2F5A9]Crafting Order[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry I] Here is your order.","I think you got potential. Give me one more and I may hire you for something bigger."},
            IneligibleDialogue = { "[Carpentry I] Is this order good?","I'm not seeing any completed Carpentry order done by you."},
            IneligibleCheck = {
                {"HasItemInBackpack", "crafting_order"},
            },
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierOne", 4},
                {"HasCompletedCraftingOrder", "WoodsmithSkill"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionTierOne", 4},
            },
            OnEnd = {
                {"AcceptCraftingOrder", "WoodsmithSkill"},
            },
            NextStep = 5,
        },
        {--carp1.5
            Name = "Taking Orders",
            Instructions = "Fulfill one more Woodsmith [F2F5A9]Crafting Order[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry I] Got your order here.","You are improving quickly! Instead of another Crafting Order, I want you to go talk to a Stocker and run a mission for the town."},
            IneligibleDialogue = { "[Carpentry I] Is this order good?","I'm not seeing any completed Carpentry order done by you."},
            IneligibleCheck = {
                {"HasItemInBackpack", "crafting_order"},
            },
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierOne", 5},
                {"HasCompletedCraftingOrder", "WoodsmithSkill"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionTierOne", 5},
            },
            OnEnd = {
                {"AcceptCraftingOrder", "WoodsmithSkill"},
            },
            NextStep = 6,
        },
        {--carp1.6
            Name = "Taking Orders",
            Instructions = "Accept a Lumber [F2F5A9]Harvesting Mission[-] from a [F2F5A9]Stocker[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierOne", 6},
            },
            MarkerLocs = {
                QuestMarkerGroups.AllHarvestMissions,
            },
            Goals = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("Missions")
                    if not missions then return false end
                    for i=1, #missions do
                        if ( missions[i] and next(missions[i]) and missions[i]["Type"] == "Harvest" and missions[i]["Category"] and missions[i]["Category"] == "HarvestLumberjack" ) then 
                            return true 
                        else 
                            return false 
                        end
                    end
                    return false
                end},
            },
            NextStep = 7,
        },
        {--carp1.7
            Name = "Taking Orders",
            Instructions = "Complete a Lumber [F2F5A9]Harvesting Mission[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierOne", 7},
            },
            OnStart = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("LifetimePlayerStats")
                    if ( missions
                    and missions["HarvestMissionsCompleted"] ) then
                        Quests.SetQuestData( playerObj, "HarvestMissionQuests", "Count", missions["HarvestMissionsCompleted"] )
                    else
                        Quests.SetQuestData( playerObj, "HarvestMissionQuests", "Count", 0 )
                    end
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("LifetimePlayerStats") or {}
                    local startCount = Quests.GetQuestData( playerObj, "HarvestMissionQuests", "Count") or 0
                    if ( missions
                    and missions["HarvestMissionsCompleted"]
                    and missions["HarvestMissionsCompleted"] > startCount ) then
                        return true else return false
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "HarvestMissionQuests")
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "HarvestMissionQuests")
                end},
            },
            NextStep = 8,
        },
        {--carp1.8
            Name = "Taking Orders",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogue = {"[Carpentry I] I helped the Stocker.","Knowing what you do now, I believe you could call yourself an Apprentice Carpenter."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierOne", 8},
            },
            Goals = {
                {"TurnIn"},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have achieved the Carpenter rank of Apprentice.","info")
                end},
            },
        },
    },
    CarpenterProfessionTierTwo = {
        {--carp2.1
            Name = "The Next Step",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            StartDialogue = {"[Carpentry II] Tell me the next step.","I think I can trust ya with delivering straight to my customers. A big shipment is going down to the docks of Pyros Landing, can you help us with it?"},
            IneligibleDialogue = { "[Carpentry II] Is there more to learn?","Not as you are right now. Refine your skill some more."},
            IneligibleCheck = {
                {"MinTimesQuestDone", "CarpenterProfessionTierOne", 1},
                {"MaxTimesQuestDone", "CarpenterProfessionTierTwo", 1},
                --{"CarpenterProfessionTierOne", 7, true},
                --{"CarpenterProfessionTierTwo", 1, false},
            },
            Requirements = {
                {"Skill", "WoodsmithSkill", 50},

                {"MaxTimesQuestDone", "CarpenterProfessionTierTwo", 1},
                {"MinTimesQuestDone", "CarpenterProfessionTierOne", 1},
                {"CapStepCompletions", "CarpenterProfessionTierTwo", 1, 1},
            },
            NextStep = 2,
        },
        {--carp2.2
            Name = "Delivery for Pyros",
            Instructions = "Make 15 [F2F5A9]Wooden Crates[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierTwo", 2},
                {"HasCraftedItem", "packed_object", 15, "UnpackedTemplate", "crate_empty"},
            },
            Goals = {
                {"StillEligible", "CarpenterProfessionTierTwo", 2},
            },
            NextStep = 3,
        },
        {--carp2.3
            Name = "Delivery for Pyros",
            Instructions = "Deliver the 15 [F2F5A9]Wooden Crates[-] to the [F2F5A9]woodsmith[-] in [F2F5A9]Pyros[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry II] I was asked to bring you these crates.","That's a pleasant surprise! Much appreciated. I may have a task for you, myself. Let me know if want in."},
            IneligibleDialogue = { "[Carpentry II] You requested some crates?","I did, but these are not good enough."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierTwo", 3},
                {"HasCraftedItem", "packed_object", 15, "UnpackedTemplate", "crate_empty"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionTierTwo", 3},
            },
            OnEnd = {
                {"TakeCraftedItems", "packed_object", 15, "UnpackedTemplate", "crate_empty"},
            },
            NextStep = 4,
        },
        {--carp2.4
            Name = "Formation of Trust",
            Instructions = "The [F2F5A9]woodsmith[-] of [F2F5A9]Pyros[-] has a request for you.",
            MarkerLocs = {
                QuestMarkers.CarpenterPyros,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry II] Let me hear this task.","Alright, we are expecting fruits from Valus. I need you to make their payment. You will notice that furniture is Packed into crates. You'd need a house of your own to get any real use out of 'em! "},
            IneligibleDialogue = { "[Carpentry II] Is there more to learn?","Not as you are right now. Refine your skill some more."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierTwo", 4},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 5,
        },
        {--carp2.5
            Name = "Delivery for Valus",
            Instructions = "Make 10 [F2F5A9]Round Tables[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierTwo", 5},
                {"HasCraftedItem", "packed_object", 10, "UnpackedTemplate", "furniture_table_round"},
            },
            Goals = {
                {"StillEligible", "CarpenterProfessionTierTwo", 5},
            },
            NextStep = 6,
        },
        {--carp2.6
            Name = "Delivery for Valus",
            Instructions = "Deliver the 10 [F2F5A9]Round Tables[-] to the [F2F5A9]woodsmith[-] in [F2F5A9]Valus[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterValus,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry II] Here is Pyros' payment for the crates","Appreciated. I've heard you are trustworthy, you think you can take one more order?"},
            IneligibleDialogue = { "[Carpentry II] May I offer these from Pyros?","No, you may not! These are not good enough."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierTwo", 6},
                {"HasCraftedItem", "packed_object", 10, "UnpackedTemplate", "furniture_table_round"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionTierTwo", 6},
            },
            OnEnd = {
                {"TakeCraftedItems", "packed_object", 10, "UnpackedTemplate", "furniture_table_round"},
            },
            NextStep = 7,
        },
        {--carp2.7
            Name = "Furthering Trust",
            Instructions = "The [F2F5A9]woodsmith[-] of [F2F5A9]Valus[-] has a request for you.",
            MarkerLocs = {
                QuestMarkers.CarpenterValus,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry II] I can do your delivery.","Oh, wonderful! I got an order for ash boards to be brought to Belhaven, please."},
            IneligibleDialogue = { "[Carpentry II] Is there more to learn?","Not as you are right now. Refine your skill some more."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierTwo", 7},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 8,
        },
        {--carp2.8
            Name = "Delivery for Belhaven",
            Instructions = "Make 40 [F2F5A9]Ash Boards[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierTwo", 8},
                {"HasCraftedItem", "resource_boards_ash", 40},
            },
            Goals = {
                {"StillEligible", "CarpenterProfessionTierTwo", 8},
            },
            NextStep = 9,
        },
        {--carp2.9
            Name = "Delivery for Belhaven",
            Instructions = "Deliver the 40 [F2F5A9]Ash Boards[-] to the [F2F5A9]woodsmith[-] in [F2F5A9]Belhaven[-].",
            MarkerLocs = {
                QuestMarkers.CarpenterBelhaven,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry II] I have brought ash boards from Valus","Perfect! You have done wonders."},
            IneligibleDialogue = { "[Carpentry II] You requested these ash boards?","I did, but these are not good enough."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierTwo", 9},
                {"HasCraftedItem", "resource_boards_ash", 40},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionTierTwo", 9},
            },
            OnEnd = {
                {"TakeCraftedItems", "resource_boards_ash", 40},
            },
            NextStep = 10,
        },
        {--carp2.10
            Name = "Established Trust",
            Instructions = "Speak to any [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry II] Have I proven myself trustworthy?","Yes, you are showing a lot of promise within the woodsmith circle. I believe you're ready for a bigger role."},
            IneligibleDialogue = { "[Carpentry II] Is there more to learn?","Not as you are right now. Refine your skill some more."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierTwo", 10},
            },
            Goals = {
                {"TurnIn"},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have achieved the Carpenter rank of Journeyman.","info")
                end},
            },
        },
    },
    CarpenterProfessionTierThree = {
        {--carp3.1
            Name = "Major Output",
            Instructions = "Speak to a [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            StartDialogue = {"[Carpentry III] Got anything for me?","Ya. We need bricks to reinforce the wall separating the Barren Lands from the lands beyond. Hand them in to any woodsmith, we trust ya."},
            IneligibleDialogue = { "[Carpentry III] Is there more to learn?","Not as you are right now. Refine your skill some more."},
            IneligibleCheck = {
                {"MinTimesQuestDone", "CarpenterProfessionTierTwo", 1},
                {"MaxTimesQuestDone", "CarpenterProfessionTierThree", 1},
                --{"CarpenterProfessionTierTwo", 10, true},
                --{"CarpenterProfessionTierThree", 1, false},
            },
            Requirements = {
                {"Skill", "WoodsmithSkill", 80},

                {"MaxTimesQuestDone", "CarpenterProfessionTierThree", 1},
                {"MinTimesQuestDone", "CarpenterProfessionTierTwo", 1},
                {"CapStepCompletions", "CarpenterProfessionTierThree", 1, 1},
            },
            NextStep = 2,
        },
        {--carp3.2
            Name = "Major Output",
            Instructions = "Make 700 [F2F5A9]Stone Bricks[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierThree", 2},
                {"HasCraftedItem", "resource_brick", 700},
            },
            Goals = {
                {"StillEligible", "CarpenterProfessionTierThree", 2},
            },
            NextStep = 3,
        },
        {--carp3.3
            Name = "Major Output",
            Instructions = "Give the 700 [F2F5A9]Stone Bricks[-] to any [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry III] These bricks are for the Barren Lands.","Ah, roger. Thanks!"},
            IneligibleDialogue = { "[Carpentry III] How is this pile of stone?","What am I supposed to do with these? I need more!"},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierThree", 3},
                {"HasCraftedItem", "resource_brick", 700},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionTierThree", 3},
            },
            OnEnd = {
                {"TakeCraftedItems", "resource_brick", 700},
            },
            NextStep = 4,
        },
        {--carp3.4
            Name = "Major Output",
            Instructions = "Speak to any [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry III] Got anything for me?","Ya. We got an order for a whole suite of Crafting Stations. We need you to make some solid and reliable builds."},
            IneligibleDialogue = { "[Carpentry III] Is there more to learn?","Not as you are right now. Refine your skill some more."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierThree", 4},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 5,
        },
        {--carp3.5
            Name = "Major Output",
            Instructions = "Make one of every craftable [F2F5A9]Crafting Station[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierThree", 5},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "tool_anvil"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "tool_carpentry_table"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "tool_loom"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "workbench_inscription"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "tool_forge_round"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "workbench_alchemy"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "stove"},
            },
            Goals = {
                {"StillEligible", "CarpenterProfessionTierThree", 5},
            },
            NextStep = 6,
        },
        {--carp3.6
            Name = "Major Output",
            Instructions = "Give the [F2F5A9]Crafting Stations[-] to any [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry III] Made a pretty sweet suite.","Fantastic, I'm sure the client will love this."},
            IneligibleDialogue = { "[Carpentry III] Is this enough?","Not quite. I need more and I need them made by your trusted hand."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierThree", 6},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "tool_anvil"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "tool_carpentry_table"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "tool_loom"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "workbench_inscription"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "tool_forge_round"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "workbench_alchemy"},
                {"HasCraftedItem", "packed_object", 1, "UnpackedTemplate", "stove"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionTierThree", 6},
            },
            OnEnd = {
                {"TakeCraftedItems", "packed_object", 1, "UnpackedTemplate", "tool_anvil"},
                {"TakeCraftedItems", "packed_object", 1, "UnpackedTemplate", "tool_carpentry_table"},
                {"TakeCraftedItems", "packed_object", 1, "UnpackedTemplate", "tool_loom"},
                {"TakeCraftedItems", "packed_object", 1, "UnpackedTemplate", "workbench_inscription"},
                {"TakeCraftedItems", "packed_object", 1, "UnpackedTemplate", "tool_forge_round"},
                {"TakeCraftedItems", "packed_object", 1, "UnpackedTemplate", "workbench_alchemy"},
                {"TakeCraftedItems", "packed_object", 1, "UnpackedTemplate", "stove"},
            },
            NextStep = 7,
        },
        {--carp3.7
            Name = "Major Output",
            Instructions = "Speak to any [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry III] Got anything for me?","Ya. Can you prepare some Blightwood War Bows and Arrows for me? We're splitting up an order from Valus, and Blightwood can be tricky to find."},
            IneligibleDialogue = { "[Carpentry III] Is there more to learn?","Not as you are right now. Refine your skill some more."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierThree", 7},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 8,
        },
        {--carp3.8
            Name = "Major Output",
            Instructions = "Make 4 [F2F5A9]Blightwood War Bows[-] and 40 [F2F5A9]Blightwood Arrows[-].",
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierThree", 8},
                {"HasCraftedItem", "arrow_blightwood", 40},
                {"HasCraftedItem", "weapon_warbow", 4, "Material", "BlightwoodBoards"},
            },
            Goals = {
                {"StillEligible", "CarpenterProfessionTierThree", 8},
            },
            NextStep = 9,
        },
        {--carp3.9
            Name = "Major Output",
            Instructions = "Give the 4 [F2F5A9]Blightwood War Bows[-] and 40 [F2F5A9]Blightwood Arrows[-] to any [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry III] 4 Bows, 40 Arrows.","Thanks so much, we wouldn't have made this order in time without ya! We actually went over the commissioned amount, so you keep a bow!"},
            IneligibleDialogue = { "[Carpentry III] Is this enough?","Not quite. I need more and I need them made by your trusted hand."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierThree", 9},
                {"HasCraftedItem", "arrow_blightwood", 40},
                {"HasCraftedItem", "weapon_warbow", 4, "Material", "BlightwoodBoards"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "CarpenterProfessionTierThree", 9},
            },
            OnEnd = {
                {"TakeCraftedItems", "arrow_blightwood", 40},
                {"TakeCraftedItems", "weapon_warbow", 3, "Material", "BlightwoodBoards"},
            },
            NextStep = 10,
        },
        {--carp3.10
            Name = "Major Output",
            Instructions = "Speak to any [F2F5A9]woodsmith[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllCarpenters,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry III] I think I have proven myself.","Agreed. However, if you want to reach further beyond, we will guide you."},
            IneligibleDialogue = { "[Carpentry III] Is there more to learn?","Not as you are right now. Refine your skill some more."},
            Requirements = {
                {"StepMustBeActive", "CarpenterProfessionTierThree", 10},
            },
            Goals = {
                {"TurnIn"},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have achieved the Carpenter rank of Master.","info")
                end},
            },
        },
    },
    CarpenterProfessionTierFour = {
        {--carp4.1
            Name = "With the Grain",
            Instructions = "Travel to the [F2F5A9]Black Forest[-] and speak to [F2F5A9]Kasey Alban[-] at the [F2F5A9]lumberyard[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            StartDialogue = {"[Carpentry IV] I want to be a Grandmaster.","You've shown dedication to your craft to make it this far. I would have you visit Kasey Alban at the lumberyard in the Black Forest, she can lead you on the last leg of your journey."},
            IneligibleDialogue = { "[Carpentry IV] Is there more to learn?","There is one final step but I need you to train even more, first."},
            IneligibleCheck = {
                {"MinTimesQuestDone", "CarpenterProfessionTierThree", 1},
                {"MaxTimesQuestDone", "CarpenterProfessionTierFour", 1},
                --{"CarpenterProfessionTierThree", 10, true},
                --{"CarpenterProfessionTierFour", 1, false},
            },
            Requirements = {
                --{"Disabled"},
                {"Skill", "WoodsmithSkill", 100},
                {"MaxTimesQuestDone", "CarpenterProfessionTierFour", 1},
                {"MinTimesQuestDone", "CarpenterProfessionTierThree", 1},
                {"CapStepCompletions", "CarpenterProfessionTierFour", 1, 1},
            },
            NextStep = 2,
        },

        {--carp4.2
            Name = "A Needful Death",
            Instructions = "Travel to the [F2F5A9]Black Forest[-] and speak to [F2F5A9]Kasey Alban[-] at the [F2F5A9]lumberyard[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogue = {"[Carpentry IV] I want to be a Grandmaster.","Look around us. There is a reason I have come to the Black Forest to live out my days. These woods are alive, even the very trees themselves will uproot and walk. With each log you hew a part of nature is lost, are your skillls worthy of such a cost?"},
            Requirements = {
                --{"Disabled"},
                {"Skill", "WoodsmithSkill", 100},
                {"StepMustBeActive", "CarpenterProfessionTierFour", 2},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 3,
        },

        {--carp4.3
            Name = "Worth the Cost",
            Instructions = "Speak to [F2F5A9]Kasey Alban[-] at the [F2F5A9]lumberyard[-] in the [F2F5A9]Black Forest[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogue = {"[Carpentry IV] Will you let me prove myself?","Indeed you can try. Prove to me that your skills are of the highest quality, bring me a bow you have crafted that has the maximum attack bonus. [+50% attack]"},
            Requirements = {
                --{"Disabled"},
                {"Skill", "WoodsmithSkill", 100},
                {"StepMustBeActive", "CarpenterProfessionTierFour", 3},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 4,
        },

        {--carp4.4
            Name = "Endless Death",
            Instructions = "Bring a 50% damage bow you crafted to [F2F5A9]Kasey Alban[-] at the [F2F5A9]lumberyard[-] in the [F2F5A9]Black Forest[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry IV] How is this?.","This is a worthy bow. It is truly that of a grandmaster in my art. Reflect on this bow for not only did it take a life to make it, but the bow itself will take many more lives. Come to me when you're ready to continue your trial."},
            IneligibleDialogue = { "[Carpentry IV] What did you need?","I need to see a bow you've crafted that does maximum attack [50% bonus]."},
            Requirements = {
                {"Skill", "WoodsmithSkill", 100},
                {"StepMustBeActive", "CarpenterProfessionTierFour", 4},
                {"Custom", function(playerObj)
                    local backpackObj = playerObj:GetEquippedObject("Backpack")
                    local hasItem = false
                    ForEachItemInContainerRecursive(backpackObj, function( item )
                        --DebugMessage("ItemName: " .. item:GetName())
                        if(
                            Weapon.IsRanged(item:GetObjVar("WeaponType")) -- is it a bow (ie. ranged)?
                            and item:GetObjVar("Crafter") == playerObj -- did the player craft this?
                            and item:GetObjVar("AttackBonus") == 50 -- is it 50% attack bonus?
                        ) then
                            hasItem = true
                            return false
                        end
                        return true
                    end)

                    --DebugMessage("HasItems: " .. tostring(hasItem))
                    return hasItem
                end},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 5,
        },

        {--carp4.5
            Name = "Flip of a Coin",
            Instructions = "Speak to [F2F5A9]Kasey Alban[-] at the [F2F5A9]lumberyard[-] in the [F2F5A9]Black Forest[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogue = {"[Carpentry IV] What awaits me next?","A simple task, bring me an Artisan's Crafting Commission. They can be earned by completing the highest quality crafting orders."},
            Requirements = {
                --{"Disabled"},
                {"Skill", "WoodsmithSkill", 100},
                {"StepMustBeActive", "CarpenterProfessionTierFour", 5},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 6,
        },

        {--carp4.6
            Name = "Flip of a Coin",
            Instructions = "Bring an artisan's commission to [F2F5A9]Kasey Alban[-] at the [F2F5A9]lumberyard[-] in the [F2F5A9]Black Forest[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Carpentry IV] I have the commission.","So you do. Very well then, you have earned my respect and with it the worthiness of your title -- Grandmaster Carpenter."},
            IneligibleDialogue = { "[Carpentry IV] What did you need?","I need you to bring me an Artisan's Crafting Commission. You can get them from crafitng orders."},
            Requirements = {
                --{"Disabled"},
                {"Skill", "WoodsmithSkill", 100},
                {"StepMustBeActive", "CarpenterProfessionTierFour", 6},
                {"HasItemInBackpack", "artisan_crafting_commission_carpentry", 1},
                -- TODO: REQUIRE An artisans token!
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "artisan_crafting_commission_carpentry", 1},
                {"TakeItem", "artisan_crafting_commission_carpentry", 1},
            },
        },
    },
    AlchemistProfessionIntro = {
        {--1
            Name = "Learn Alchemy",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            NextStep = 2,
            EndDialogue = {"[Alchemist Intro] I wish to practice Alchemy.","That is good news! Celador needs more alchemists! We will start with a simple recipe. First acquire 10 blood. Blood can be obtained from a selection of different creatures, or through trading."},
            Requirements = {
                {"MaxTimesQuestDone", "AlchemistProfessionIntro", 1},
                --{"QuestActive", "AlchemistProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn Alchemy",
            Instructions = "Get 10 [F2F5A9]Blood[-]. It can be commonly found on [F2F5A9]Zombies, Lizardmen[-], or traded for with other players.",
            NextStep = 3,
            StartDialogue = {"[Alchemist Intro] I wish to practice Alchemy.","That is good news! Celador needs more alchemists! We will start with a simple recipe. First acquire 10 blood. Blood can be obtained from a selection of different creatures, or through trading."},
            Requirements = {
                {"MaxTimesQuestDone", "AlchemistProfessionIntro", 1},
                {"QuestActive", "AlchemistProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill", "AlchemySkill", 30},
            },
            Goals = {
                {"HasItem", "animalparts_blood", 10},
            },
        },
        {--3
            Name = "Learn Alchemy",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            NextStep = 4,
            EndDialogue = {"[Alchemist Intro] I have collected blood.","I will not ask where you obtained this so quickly. Some people will do anything for alchemy ingredients. Take your blood and combine it with some Ginseng to create a Lesser Heal Potion."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Learn Alchemy",
            Instructions = "Craft a [F2F5A9]Lesser Heal Potion[-] at an [F2F5A9]Alchemy Table[-].",
            NextStep = 5,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "potion_lheal")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--5
            Name = "Learn Alchemy",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            EndDialogue = {"[Alchemist Intro] I made a potion.","I suppose pouring liquid into a bottle counts as \"making a potion\" these days. Potions can serve you well and raise your potential to new heights, so do not be afraid to use them. The materials can be difficult to come by, and sought after at market."},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    AlchemistProfessionTierOne = {
        {--1
            Name = "Muddle the Mix",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogue = {"[Alchemist I] What next, master?","Well, alchemy is a tricky trade. It's more of a matter of sourcing ingredients than any real skill or science. You managed to procure blood the last time we spoke. Let's see if you can round up three different ingredients and craft one of each standard strength potion. Good luck!"},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "AlchemistProfessionTierOne", 1},
                --{"QuestActive", "AlchemistProfessionTierOne", false},
                {"MinTimesQuestDone", "AlchemistProfessionIntro", 1},
                {"Skill", "AlchemySkill", 30},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Muddle the Mix",
            Instructions = "Craft all of the [F2F5A9]standard strength potions[-] at an [F2F5A9]Alchemy Table[-].",
            StartDialogue = {"[Alchemist I] What next, master?","Well, alchemy is a tricky trade. It's more of a matter of sourcing ingredients than any real skill or science. You managed to procure blood the last time we spoke. Let's see if you can round up three different ingredients and craft one of each standard strength potion. Good luck!"},
            NextStep = 3,
            Requirements = {
                {"MaxTimesQuestDone", "AlchemistProfessionTierOne", 1},
                {"QuestActive", "AlchemistProfessionTierOne", false},
                {"MinTimesQuestDone", "AlchemistProfessionIntro", 1},
                {"Skill", "AlchemySkill", 30},
            },
        },
        {--3
            Name = "Muddle the Mix",
            Instructions = "Craft a [F2F5A9]Heal Potion[-].",
            NextStep = 4,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "potion_heal")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--4
            Name = "Muddle the Mix",
            Instructions = "Craft a [F2F5A9]Mana Potion[-].",
            NextStep = 5,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "potion_mana")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--5
            Name = "Muddle the Mix",
            Instructions = "Craft a [F2F5A9]Stamina Potion[-].",
            NextStep = 6,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "potion_stamina")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--6
            Name = "Muddle the Mix",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogue = {"[Alchemist I] It wasn't that hard, really.","Ah yes, well just wait until you have to extract the brains of wyverns several hundred times a day to perfect your art. We will see if you can last, then. But seriously, well done. The biggest threat to us alchemists are those adventurers who enjoy collecting potions rather than actually drinking them! They can be quite strong!"},
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","animalparts_blood_beast",15},
                    {"Item","animalparts_bone_cursed",15},
                    {"Item","animalparts_eye_sickly",15},
                },
            },
        },
    },
    AlchemistProfessionTierTwo = {
        {--1
            Name = "Bubble Bubble!",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogue = {"[Alchemist II] I am ready for my next task.","Good, you've come at the right time too. The League of Explorers has comissioned the alchemist guild to procure stamina potions for their ranks. All of our order are required to pitch in, I would say 30 would do nicely."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "AlchemistProfessionTierTwo", 1},
                --{"QuestActive", "AlchemistProfessionTierTwo", false},
                {"MinTimesQuestDone", "AlchemistProfessionTierOne", 1},
                {"Skill", "AlchemySkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Bubble Bubble!",
            Instructions = "Craft [F2F5A9]30 stamina potions[-] for the alchemist guild.",
            NextStep = 3,
            Requirements = {
                {"Skill", "AlchemySkill", 50},
            },
            QuestData = {
                Key = "AlchemistProfessionTierTwo2",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Potions",
                    "potion_stamina"
                },
                Count = 30,
                Name = "Stamina Potion",
                NamePlural = "Stamina Potions",
            },
        },
        {--3
            Name = "Bubble Bubble!",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Alchemist II] What did you need?", "30 stamina potions and quickly if you can find the time!"},
            EndDialogue = {"[Alchemist II] I have the potions.","Very nice, I knew I could count on you."},
            NextStep = 4,
            Requirements = {
                {"Skill", "AlchemySkill", 50},
                {"HasItemInBackpack", "potion_stamina", 30},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "potion_stamina", 30},
                {"TakeItem", "potion_stamina", 30},
            },
        },
        {--4
            Name = "Bubble Bubble!",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogue = {"[Alchemist II] Any more work?","Well aren't you just a go getter! Sure, I've got some things that need doing. Why don't you craft 20 Greater Heal potions and get them to me."},
            NextStep = 5,
            Requirements = {
                {"Skill", "AlchemySkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 5
            Name = "Bubble Bubble!",
            Instructions = "Craft [F2F5A9]20 greater heal potions[-] for the alchemist guild.",
            NextStep = 6,
            Requirements = {
                {"Skill", "AlchemySkill", 50},
            },
            QuestData = {
                Key = "AlchemistProfessionTierTwo5",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Potions",
                    "potion_gheal"
                },
                Count = 20,
                Name = "Greater Heal Potion",
                NamePlural = "Greater Heal Potions",
            },
        },
        {--6
            Name = "Bubble Bubble!",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Alchemist II] What did you need?", "I needed 20 greater heal potions, you're a forgetful one."},
            EndDialogue = {"[Alchemist II] I have the potions.","It appears you do! Thank you muchly."},
            NextStep = 7,
            Requirements = {
                {"Skill", "AlchemySkill", 50},
                {"HasItemInBackpack", "potion_gheal", 20},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "potion_gheal", 20},
                {"TakeItem", "potion_gheal", 20},
            },
        },
        {--7
            Name = "Bubble Bubble!",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogue = {"[Alchemist II] I'd like my journeyman certification.","Aren't you just a charmer. Lucky for you I'm in a pickle and need 30 Greater Mana potions yesterday, get me those and I'll sign off on your journeyman certificate."},
            NextStep = 8,
            Requirements = {
                {"Skill", "AlchemySkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 8
            Name = "Bubble Bubble!",
            Instructions = "Craft [F2F5A9]30 greater mana potions[-] for the alchemist guild.",
            NextStep = 9,
            Requirements = {
                {"Skill", "AlchemySkill", 50},
            },
            QuestData = {
                Key = "AlchemistProfessionTierTwo8",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Potions",
                    "potion_gmana"
                },
                Count = 30,
                Name = "Greater Mana Potion",
                NamePlural = "Greater Mana Potions",
            },
        },
        {--9
            Name = "Bubble Bubble!",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Alchemist II] What did you need?", "I needed 30 greater mana potions."},
            EndDialogue = {"[Alchemist II] I have the potions.","These look good, congratulations journeyman."},
            --NextStep = 7,
            Requirements = {
                {"Skill", "AlchemySkill", 50},
                {"HasItemInBackpack", "potion_gmana", 30},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "potion_gmana", 30},
                {"TakeItem", "potion_gmana", 30},
            },
        },
    },
    AlchemistProfessionTierThree = {
        {--1
            Name = "Tons of Tonics",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogue = { "[Alchemist III] I'm getting pretty good at\nmaking these potions.","Lets hold on there, I'll be judge of who's good or not. You just worry about filling orders. With that in mind can you get my 30 greater stamina potions." },
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "AlchemistProfessionTierThree", 1},
                --{"QuestActive", "AlchemistProfessionTierThree", false},
                {"MinTimesQuestDone", "AlchemistProfessionTierTwo", 1},
                {"Skill", "AlchemySkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Tons of Tonics",
            Instructions = "Craft [F2F5A9]30 greather stamina potions[-] for the alchemist guild.",
            NextStep = 3,
            Requirements = {
                {"Skill", "AlchemySkill", 80},
            },
            QuestData = {
                Key = "AlchemistProfessionTierThree2",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Potions",
                    "potion_gstamina"
                },
                Count = 30,
                Name = "Greater Stamina Potion",
                NamePlural = "Greater Stamina Potions",
            },
        },
        {--3
            Name = "Tons of Tonics",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Alchemist III] What did you need?", "30 greater stamina potions if you don't mind."},
            EndDialogue = {"[Alchemist III] I have the potions.","These are quite well mixed, you skill is improving."},
            NextStep = 4,
            Requirements = {
                {"Skill", "AlchemySkill", 80},
                {"HasItemInBackpack", "potion_gstamina", 30},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "potion_gstamina", 30},
                {"TakeItem", "potion_gstamina", 30},
            },
        },
        {--4
            Name = "Tons of Tonics",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogue = {"[Alchemist III] We need to start discussing my master certificate.","Yeah yeah, I figured you wouldn't forget to mention that. You've got a bit more work to do before we get there though. Specifically I need 100 mending potions to fill a big order from the Valusian guards."},
            NextStep = 5,
            Requirements = {
                {"Skill", "AlchemySkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 5
            Name = "Tons of Tonics",
            Instructions = "Craft [F2F5A9]100 mending potions[-] for the alchemist guild.",
            NextStep = 6,
            Requirements = {
                {"Skill", "AlchemySkill", 80},
            },
            QuestData = {
                Key = "AlchemistProfessionTierThree5",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Potions",
                    "potion_mending"
                },
                Count = 100,
                Name = "Mending Potion",
                NamePlural = "Mending Potions",
            },
        },
        {--6
            Name = "Tons of Tonics",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Alchemist III] What did you need?", "I need 100 mending potions to fill that order out."},
            EndDialogue = {"[Alchemist III] I have the potions.","You doing okay? Collecting those cactus in the desert didn't melt you brain did it?. Yep, potions look good. Come talk to me again when you're free."},
            NextStep = 7,
            Requirements = {
                {"Skill", "AlchemySkill", 80},
                {"HasItemInBackpack", "potion_mending", 100},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "potion_mending", 100},
                {"TakeItem", "potion_mending", 100},
            },
        },
        {--7
            Name = "Tons of Tonics",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogue = {"[Alchemist III] I'm starting to dread talking to you.","Aww now I thought we were friends. Besides, I just wanted to let you know I'm signing off on your master certificate."},
            Requirements = {
                {"Skill", "AlchemySkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    AlchemistProfessionTierFour = {
        {--1
            Name = "Last Draught",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogue = { "[Alchemist IV] I'm not making 100 more potions.","Oh don't be like that. Plus we both know you'd make 100, 200, or 300 more if I asked you. But no, don't need that many. We have an order from wealthy militia patron that wants 50 Summon Mount potions." },
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "AlchemistProfessionTierFour", 1},
                --{"QuestActive", "AlchemistProfessionTierFour", false},
                {"MinTimesQuestDone", "AlchemistProfessionTierThree", 1},
                {"Skill", "AlchemySkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Last Draught",
            Instructions = "Craft [F2F5A9]50 summon mount potions[-] for the alchemist guild.",
            NextStep = 3,
            Requirements = {
                {"Skill", "AlchemySkill", 100},
            },
            QuestData = {
                Key = "AlchemistProfessionTierFour2",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Potions",
                    "potion_etherealize"
                },
                Count = 50,
                Name = "Summon Mount Potion",
                NamePlural = "Summon Mount Potions",
            },
        },
        {--3
            Name = "Last Draught",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Alchemist IV] What did you need?", "We need 50 summon mount potions."},
            EndDialogue = {"[Alchemist IV] I have the potions.","These will work nicely."},
            NextStep = 4,
            Requirements = {
                {"Skill", "AlchemySkill", 100},
                {"HasItemInBackpack", "potion_etherealize", 50},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "potion_etherealize", 50},
                {"TakeItem", "potion_etherealize", 50},
            },
        },
        {--4
            Name = "Last Draught",
            Instructions = "Speak to an [F2F5A9]alchemist[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.PyrosAlchemist,
                QuestMarkers.ValusAlchemist,
                QuestMarkers.BarrenLandsAlchemist,
                QuestMarkers.EldeirAlchemist,
                QuestMarkers.BlackForestAlchemist,
            },
            EndDialogue = {"[Alchemist IV] You need anything?","That's the spirit! I actually do not need a thing, plenty of fledglings to do the dirty work. Sounds like you'll need to make you own way, grandmaster."},
            Requirements = {
                {"Skill", "AlchemySkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    ScribeProfessionIntro = {
        {--1
            Name = "Learn Inscription",
            Instructions = "Speak to a [F2F5A9]scribe[-].",
            NextStep = 2,
            EndDialogue = {"[Scribe Intro] Teach me inscription.","Ah yes. Everyone wants to learn but few are willing to put in the effort. I just finished binding spellbooks; my wrists will be sore for days. Well, if you are determined, start by assembling the basic materials. Go find some lemongrass and blank scrolls."},
            Requirements = {
                {"MaxTimesQuestDone", "ScribeProfessionIntro", 1},
                --{"QuestActive", "ScribeProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn Inscription",
            Instructions = "Get 10 [F2F5A9]Lemongrass[-]. It can be purchased from alchemists and herbalists.",
            NextStep = 3,
            StartDialogue = {"[Scribe Intro] Teach me inscription.","Ah yes. Everyone wants to learn but few are willing to put in the effort. I just finished binding spellbooks; my wrists will be sore for days. Well, if you are determined, start by assembling the basic materials. Go find some lemongrass and blank scrolls."},
            Requirements = {
                {"MaxTimesQuestDone", "ScribeProfessionIntro", 1},
                {"QuestActive", "ScribeProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill", "InscriptionSkill", 30},
            },
            Goals = {
                {"HasItemInBackpack", "ingredient_lemongrass", 10},
            },
        },
        {--3
            Name = "Learn Inscription",
            Instructions = "Get 10 [F2F5A9]Blank Scrolls[-]. They can be bought from scribes.",
            NextStep = 4,
            Goals = {
                {"HasItemInBackpack", "resource_blankscroll", 10},
            },
        },
        {--4
            Name = "Learn Inscription",
            Instructions = "Speak to a [F2F5A9]scribe[-].",
            NextStep = 5,
            EndDialogue = {"[Scribe Intro] I brought what you requested.","Creating spells and passing on arcane knowledge, what could be better? How exciting! Once inscribed, a scroll can be placed in a spellbook for reference or used outright for the lesser skilled. You can start with a very simple spell scroll. Good luck."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--5
            Name = "Learn Inscription",
            Instructions = "Craft a [F2F5A9]Mana Missile Scroll[-] at an [F2F5A9]Inscription Table[-].",
            NextStep = 6,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_mana_missile")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--6
            Name = "Learn Inscription",
            Instructions = "Speak to a [F2F5A9]scribe[-].",
            EndDialogue = {"[Scribe Intro] Does this scroll look correct?","Honestly, mana missile is such a simple spell, as long as there is some writing there, it should work. We scribes provide essential services for mages. If we did not exist, mages wouldn't either!"},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    ScribeProfessionTierOne = {
        {--1
            Name = "Chapter One",
            Instructions = "Speak to a [F2F5A9]scribe[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.PyrosScribe,
                QuestMarkers.ValusScribe,
                QuestMarkers.EldeirScribe,
                QuestMarkers.HelmScribe,
            },
            EndDialogue = {"[Scribe I] Can you teach me more?","Of course. The question is... are you prepared? Are you ready to learn? Do you have what it takes? All of the characteristics of a fledgling scribe... a, um... scribe's table and some scrolls... AND REAGENTS! A scribe's duty is to curate knowledge, and pass it on. Sort and organize your materials, and bring all of those frayed sheets together into something truly useful. Here is a list of ALL of the basic spells. Better get started."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "ScribeProfessionTierOne", 1},
                --{"QuestActive", "ScribeProfessionTierOne", false},
                {"MinTimesQuestDone", "ScribeProfessionIntro", 1},
                {"Skill", "InscriptionSkill", 30},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Chapter One",
            Instructions = "Craft all [F2F5A9]scrolls[-] requiring a skill of 30 or lower.",
            StartDialogue = {"[Scribe I] Can you teach me more?","Of course. The question is... are you prepared? Are you ready to learn? Do you have what it takes? All of the characteristics of a fledgling scribe... a, um... scribe's table and some scrolls... AND REAGENTS! A scribe's duty is to curate knowledge, and pass it on. Sort and organize your materials, and bring all of those frayed sheets together into something truly useful. Here is a list of ALL of the basic spells. Better get started."},
            NextStep = 3,
            Requirements = {
                {"MaxTimesQuestDone", "ScribeProfessionTierOne", 1},
                {"QuestActive", "ScribeProfessionTierOne", false},
                {"MinTimesQuestDone", "ScribeProfessionIntro", 1},
                {"Skill", "InscriptionSkill", 30},
            },
        },
        {--3
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Heal Scroll[-].",
            NextStep = 4,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_heal")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--4
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Cure Scroll[-].",
            NextStep = 5,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_cure")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--5
            Name = "Chapter One",
            Instructions = "Craft an [F2F5A9]Infuse Scroll[-].",
            NextStep = 6,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_infuse")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--6
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Mana Missile Scroll[-].",
            NextStep = 7,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_mana_missile")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--7
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Refresh Scroll[-].",
            NextStep = 8,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_refresh")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--8
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Ruin Scroll[-]",
            NextStep = 9,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_ruin")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--9
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Poison Scroll[-].",
            NextStep = 10,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_poison")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--10
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Fireball Scroll[-].",
            NextStep = 11,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_fireball")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--11
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Teleport Scroll[-].",
            NextStep = 12,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_teleport")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--12
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Wall of Fire Scroll[-].",
            NextStep = 13,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_walloffire")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--13
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Greater Heal Scroll[-].",
            NextStep = 14,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_greater_heal")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--14
            Name = "Chapter One",
            Instructions = "Craft a [F2F5A9]Lightning Scroll[-].",
            NextStep = 15,
            Goals = {
                {"Custom", function(playerObj)
                    local item = GetItem(playerObj, "lscroll_lightning")
                    if ( item ) then
                        local crafter = item:GetObjVar("Crafter")
                        if ( crafter and crafter == playerObj ) then
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--15
            Name = "Chapter One",
            Instructions = "Speak to a [F2F5A9]scribe[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.PyrosScribe,
                QuestMarkers.ValusScribe,
                QuestMarkers.EldeirScribe,
                QuestMarkers.HelmScribe,
            },
            EndDialogue = {"[Scribe I] I think that's all of them.","Well, the basic ones at least. But even these basic scrolls can be very useful. In time you will call yourself one of the few that can create all the tools of magic, and your skills will be sought after, I'm sure of it."},
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","ingredient_frayed_scroll",50},
                },
            },
        },
    },
    ScribeProfessionTierTwo = {
        {--1
            Name = "Fire and Brimestone",
            Instructions = "Speak to an [F2F5A9]scribe[-].",
            MarkerLocs = {
                QuestMarkers.PyrosScribe,
                QuestMarkers.ValusScribe,
                QuestMarkers.EldeirScribe,
                QuestMarkers.HelmScribe,
            },
            EndDialogue = {"[Scribe II] What should I stock up on?","Scrolls aren't just good for filling spellbooks. Scribes can utilize scrolls with increased pontency so having a stockpile of offensive scrolls is always recommended. Why don't you make 100 fireball scrolls."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "ScribeProfessionTierTwo", 1},
                --{"QuestActive", "ScribeProfessionTierTwo", false},
                {"MinTimesQuestDone", "ScribeProfessionTierOne", 1},
                {"Skill", "InscriptionSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Fire and Brimestone",
            Instructions = "Craft [F2F5A9]100 fireball[-] scrolls.",
            NextStep = 3,
            Requirements = {
                {"Skill", "InscriptionSkill", 50},
            },
            QuestData = {
                Key = "ScribeProfessionTierTwo2",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Scrolls",
                    "lscroll_fireball"
                },
                Count = 100,
                Name = "Fireball Scroll",
                NamePlural = "Fireball Scrolls",
            },
        },
        {--3
            Name = "Fire and Brimestone",
            Instructions = "Speak to an [F2F5A9]scribe[-].",
            MarkerLocs = {
                QuestMarkers.PyrosScribe,
                QuestMarkers.ValusScribe,
                QuestMarkers.EldeirScribe,
                QuestMarkers.HelmScribe,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Scribe II] How many fireball scrolls?", "I'd start off by crafting 100 of them."},
            EndDialogue = {"[Scribe II] I crafted those scrolls.","Excellent, hang on to them as your skill grows they will only get more powerful. Next make 100 lightning scrolls."},
            NextStep = 4,
            Requirements = {
                {"Skill", "InscriptionSkill", 50},
                {"HasItemInBackpack", "lscroll_fireball", 100},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "lscroll_fireball", 100},
            },
        },
        { -- 4
            Name = "Fire and Brimestone",
            Instructions = "Craft [F2F5A9]100 lightning[-] scrolls.",
            NextStep = 5,
            Requirements = {
                {"Skill", "InscriptionSkill", 50},
            },
            QuestData = {
                Key = "ScribeProfessionTierTwo4",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Scrolls",
                    "lscroll_lightning"
                },
                Count = 100,
                Name = "Lightning Scroll",
                NamePlural = "Lightning Scrolls",
            },
        },
        {--5
            Name = "Fire and Brimestone",
            Instructions = "Speak to an [F2F5A9]scribe[-].",
            MarkerLocs = {
                QuestMarkers.PyrosScribe,
                QuestMarkers.ValusScribe,
                QuestMarkers.EldeirScribe,
                QuestMarkers.HelmScribe,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Scribe II] How many lightning scrolls?", "I'd start off by crafting 100 of them."},
            EndDialogue = {"[Scribe II] 100 lightning scrolls complete.","Good, you are well on your way journeyman."},
            Requirements = {
                {"Skill", "InscriptionSkill", 50},
                {"HasItemInBackpack", "lscroll_lightning", 100},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "lscroll_lightning", 100},
            },
        },
    },
    ScribeProfessionTierThree = {
        {--1
            Name = "Frosty Bolts",
            Instructions = "Speak to an [F2F5A9]scribe[-].",
            MarkerLocs = {
                QuestMarkers.PyrosScribe,
                QuestMarkers.ValusScribe,
                QuestMarkers.EldeirScribe,
                QuestMarkers.HelmScribe,
            },
            EndDialogue = {"[Scribe III] Are there any other scrolls to stock?","Aye, any that could be used in combat scenarios. I would suggest making 100 Energy Bolt scrolls."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "ScribeProfessionTierThree", 1},
                --{"QuestActive", "ScribeProfessionTierThree", false},
                {"MinTimesQuestDone", "ScribeProfessionTierTwo", 1},
                {"Skill", "InscriptionSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Frosty Bolts",
            Instructions = "Craft [F2F5A9]100 energy bolt[-] scrolls.",
            NextStep = 3,
            Requirements = {
                {"Skill", "InscriptionSkill", 80},
            },
            QuestData = {
                Key = "ScribeProfessionTierThree2",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Scrolls",
                    "lscroll_electricbolt"
                },
                Count = 100,
                Name = "Energy Bolt Scroll",
                NamePlural = "Energy Bolt Scrolls",
            },
        },
        {--3
            Name = "Frosty Bolts",
            Instructions = "Speak to an [F2F5A9]scribe[-].",
            MarkerLocs = {
                QuestMarkers.PyrosScribe,
                QuestMarkers.ValusScribe,
                QuestMarkers.EldeirScribe,
                QuestMarkers.HelmScribe,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Scribe III] How many energy bolt scrolls?", "You needed to craft 100 of them."},
            EndDialogue = {"[Scribe III] 100 energy bolt scrolls completed.","Well done, those will for sure come in mighty handy. I'd stock up on frost scrolls next, they are a bit tricky so I'd only make 25."},
            NextStep = 4,
            Requirements = {
                {"Skill", "InscriptionSkill", 80},
                {"HasItemInBackpack", "lscroll_electricbolt", 100},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "lscroll_electricbolt", 100},
            },
        },
        { -- 4
            Name = "Frosty Bolts",
            Instructions = "Craft [F2F5A9]25 frost[-] scrolls.",
            NextStep = 5,
            Requirements = {
                {"Skill", "InscriptionSkill", 80},
            },
            QuestData = {
                Key = "ScribeProfessionTierThree4",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Scrolls",
                    "lscroll_frost"
                },
                Count = 25,
                Name = "Frost Scroll",
                NamePlural = "Frost Scrolls",
            },
        },
        {--5
            Name = "Frosty Bolts",
            Instructions = "Speak to an [F2F5A9]scribe[-].",
            MarkerLocs = {
                QuestMarkers.PyrosScribe,
                QuestMarkers.ValusScribe,
                QuestMarkers.EldeirScribe,
                QuestMarkers.HelmScribe,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Scribe III] How many frost scrolls?", "I need you to show me 25 of them."},
            EndDialogue = {"[Scribe III] I made those frost scrolls.","Good deal, they can be used in combat or can even fetch a pretty penny if sold to the right buyer. You've proven yourself a master scribe."},
            Requirements = {
                {"Skill", "InscriptionSkill", 80},
                {"HasItemInBackpack", "lscroll_frost", 25},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "lscroll_frost", 25},
            },
        },
    },
    ScribeProfessionTierFour = {
        {--1
            Name = "Power Word: Passage",
            Instructions = "Speak to an [F2F5A9]scribe[-].",
            MarkerLocs = {
                QuestMarkers.PyrosScribe,
                QuestMarkers.ValusScribe,
                QuestMarkers.EldeirScribe,
                QuestMarkers.HelmScribe,
            },
            EndDialogue = {"[Scribe IV] I feel I've reached my peak.","You have, I can see in your the magic that has been imbued through the long hours and constant toil in perfecting this craft. No doubt you have realized the potential in your skill, craft 10 runebooks and return to me for your final instruction."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "ScribeProfessionTierFour", 1},
                --{"QuestActive", "ScribeProfessionTierFour", false},
                {"MinTimesQuestDone", "ScribeProfessionTierThree", 1},
                {"Skill", "InscriptionSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Power Word: Passage",
            Instructions = "Craft [F2F5A9]10 runebooks[-].",
            NextStep = 3,
            Requirements = {
                {"Skill", "InscriptionSkill", 100},
            },
            QuestData = {
                Key = "ScribeProfessionTierFour2",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Books",
                    "runebook"
                },
                Count = 10,
                Name = "Runebook",
                NamePlural = "Runebooks",
            },
        },
        {--3
            Name = "Power Word: Passage",
            Instructions = "Speak to an [F2F5A9]scribe[-].",
            MarkerLocs = {
                QuestMarkers.PyrosScribe,
                QuestMarkers.ValusScribe,
                QuestMarkers.EldeirScribe,
                QuestMarkers.HelmScribe,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Scribe IV] How many runebooks?", "You needed to craft 10 and return to me."},
            EndDialogue = {"[Scribe IV] The runebooks are complete","You alone have the power to craft such powerful tools of travel. These books represent the highest achievement of our order and are a testament to your skill. May I be the first to greet you as a grandmaster scribe."},
            Requirements = {
                {"Skill", "InscriptionSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    TailorProfessionIntro = {
        {--tail0.1
            Name = "Learn Fabrication",
            Instructions = "Speak to any [F2F5A9]tailor[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            StartDialogue = {"[Tailor Intro] I would like to learn Fabrication.","And I would like to teach you. Pop a squat by the Loom over here."},
            Requirements = {
                {"MaxTimesQuestDone", "TailorProfessionIntro", 1},
                {"CapStepCompletions", "TailorProfessionIntro", 1, 1},
            },
            OnStart = {
                {"SetSkill", "FabricationSkill", 30},
            },
            NextStep = 2,
        },
        {--tail0.2
            Name = "Learn Fabrication",
            Instructions = "Make 20 [F2F5A9]Bolts of Cloth[-].",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionIntro", 2},
                {"HasCraftedItem", "resource_bolt_of_cloth", 20},
            },
            Goals = {
                {"StillEligible", "TailorProfessionIntro", 2},
            },
            NextStep = 3,
        },
        {--tail0.3
            Name = "Learn Fabrication",
            Instructions = "Show the 20 [F2F5A9]Bolts of Cloth[-] to any [F2F5A9]tailor[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor Intro] I made these bolts of cloth.","I can see a bright future in this weaving. Here, try a Bandana, next. Ooh and show me you wearing it!"},
            IneligibleDialogue = { "[Tailor Intro] How do these look?","That isn't the bright future I saw, something is missing."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionIntro", 3},
                {"HasCraftedItem", "resource_bolt_of_cloth", 20},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionIntro", 3},
            },
            NextStep = 4,
        },
        {--tail0.4
            Name = "Learn Fabrication",
            Instructions = "Make a [F2F5A9]Bandana[-].",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionIntro", 4},
                {"HasCraftedItem", "clothing_bandana_helm", 1},
            },
            Goals = {
                {"StillEligible", "TailorProfessionIntro", 4},
            },
            NextStep = 5,
        },
        {--tail0.5
            Name = "Learn Fabrication",
            Instructions = "Wear the [F2F5A9]Bandana[-] and show it to the [F2F5A9]tailor[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor Intro] Check out the bandana!","Yes, good! Keeps your head's form well without stretching too thin. Make an apron next and model it for me."},
            IneligibleDialogue = { "[Tailor Intro] How is this?","That isn't the bright future I saw, something is missing."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionIntro", 5},
                {"HasEquippedCraftedItem", "clothing_bandana_helm", 1},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionIntro", 5},
            },
            NextStep = 6,
        },
        {--tail0.6
            Name = "Learn Fabrication",
            Instructions = "Make an [F2F5A9]Apron[-].",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionIntro", 6},
                {"HasCraftedItem", "clothing_apron_chest", 1},
            },
            Goals = {
                {"StillEligible", "TailorProfessionIntro", 6},
            },
            NextStep = 7,
        },
        {--tail0.7
            Name = "Learn Fabrication",
            Instructions = "Wear the [F2F5A9]Apron[-] and show it to the [F2F5A9]tailor[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor Intro] I'm ready, chef!","Wonderful! My new technique even fits your form, yes, good. You are showing promise, friend!"},
            IneligibleDialogue = { "[Tailor Intro] How is this?","That isn't the bright future I saw, something is missing."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionIntro", 7},
                {"HasEquippedCraftedItem", "clothing_apron_chest", 1},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionIntro", 7},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have completed the Intro to Tailoring.","info")
                end},
            },
        },
    },
    TailorProfessionTierOne = {
        {--tail1.1
            Name = "Suiting Their Needs",
            Instructions = "Speak to any [F2F5A9]tailor[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            StartDialogue = {"[Tailor I] I'm ready for something bigger.","Yes, it looks like it! A crucial step to being a tailor is developing for others, and a great way to start would be with Crafting Orders! Come talk with me about them when you want."},
            IneligibleDialogue = { "[Tailor I] Is there more to learn?","Of course, you think you are a master already? You would insult my livelihood like that? You think it's that easy, huh? Huh? You are not ready yet if you are asking me at your level."},
            IneligibleCheck = {
                {"MinTimesQuestDone", "TailorProfessionIntro", 1},
                {"MaxTimesQuestDone", "TailorProfessionTierOne", 1},
                --{"TailorProfessionIntro", 7, true},
                --{"TailorProfessionTierOne", 1, false},
            },
            Requirements = {
                {"Skill", "FabricationSkill", 30},
                {"MaxTimesQuestDone", "TailorProfessionTierOne", 1},
                {"MinTimesQuestDone", "TailorProfessionIntro", 1},
                {"CapStepCompletions", "TailorProfessionTierOne", 1, 1},
            },
            NextStep = 2,
        },
        {--tail1.2
            Name = "Suiting Their Needs",
            Instructions = "Speak to a [F2F5A9]tailor[-] and inquire about [F2F5A9]Crafting Orders[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierOne", 2},
                --{"HasCompletedCraftingOrder", "FabricationSkill"},
                {"HasSpecificItem", "crafting_order", "OrderSkill", "FabricationSkill"},
            },
            Goals = {
                {"StillEligible", "TailorProfessionTierOne", 2},
            },
            NextStep = 3,
        },
        {--tail1.3
            Name = "Suiting Their Needs",
            Instructions = "Turn the [F2F5A9]Crafting Order[-] in to a [F2F5A9]tailor[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor I] Finished this order.","Let's see it! Nice work, show me how you tackle another one."},
            IneligibleDialogue = { "[Tailor I] How is this order?","I'm not seeing any completed order I gave you. I thought you were brighter than this."},
            IneligibleCheck = {
                {"HasItemInBackpack", "crafting_order"},
            },
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierOne", 3},
                {"HasCompletedCraftingOrder", "FabricationSkill"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionTierOne", 3},
            },
            OnEnd = {
                {"AcceptCraftingOrder", "FabricationSkill"},
            },
            NextStep = 4,
        },
        {--tail1.4
            Name = "Suiting Their Needs",
            Instructions = "Fulfill another Tailor [F2F5A9]Crafting Order[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor I] Here's a Crafting Order.","Hey, alright. Love your enthusiasm! Give me one more order, would you?"},
            IneligibleDialogue = { "[Tailor I] How is this order?","I'm not seeing any completed order I gave you. I thought you were brighter than this."},
            IneligibleCheck = {
                {"HasItemInBackpack", "crafting_order"},
            },
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierOne", 4},
                {"HasCompletedCraftingOrder", "FabricationSkill"},  
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionTierOne", 4},
            },
            OnEnd = {
                {"AcceptCraftingOrder", "FabricationSkill"},
            },
            NextStep = 5,
        },
        {--tail1.5
            Name = "Suiting Their Needs",
            Instructions = "Fulfill one more Tailor [F2F5A9]Crafting Order[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor I] Another order down.","Quality stuff! Now that you've got Crafting Orders down, go do a harvesting mission from a Stocker."},
            IneligibleDialogue = { "[Tailor I] How is this order?","I'm not seeing any completed order I gave you. I thought you were brighter than this."},
            IneligibleCheck = {
                {"HasItemInBackpack", "crafting_order"},
            },
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierOne", 5},
                {"HasCompletedCraftingOrder", "FabricationSkill"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionTierOne", 5},
            },
            OnEnd = {
                {"AcceptCraftingOrder", "FabricationSkill"},
            },
            NextStep = 6,
        },
        {--tail1.6
            Name = "Suiting Their Needs",
            Instructions = "Accept a Cotton [F2F5A9]Harvesting Mission[-] from a [F2F5A9]Stocker[-].",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierOne", 6},
            },
            MarkerLocs = {
                QuestMarkerGroups.AllHarvestMissions,
            },
            Goals = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("Missions")
                    if not missions then return false end
                    for i=1, #missions do
                        if ( missions[i] and next(missions[i]) and missions[i]["Type"] == "Harvest" and missions[i]["Category"] and missions[i]["Category"] == "HarvestFabrication" ) then 
                            return true 
                        else 
                            return false 
                        end
                    end
                    return false
                end},
            },
            NextStep = 7,
        },
        {--tail1.7
            Name = "Suiting Their Needs",
            Instructions = "Complete a Cotton [F2F5A9]Harvesting Mission[-].",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierOne", 7},
            },
            OnStart = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("LifetimePlayerStats")
                    if ( missions
                    and missions["HarvestMissionsCompleted"] ) then
                        Quests.SetQuestData( playerObj, "HarvestMissionQuests", "Count", missions["HarvestMissionsCompleted"] )
                    else
                        Quests.SetQuestData( playerObj, "HarvestMissionQuests", "Count", 0 )
                    end
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local missions = playerObj:GetObjVar("LifetimePlayerStats") or {}
                    local startCount = Quests.GetQuestData( playerObj, "HarvestMissionQuests", "Count") or 0
                    if ( missions
                    and missions["HarvestMissionsCompleted"]
                    and missions["HarvestMissionsCompleted"] > startCount ) then
                        return true else return false
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "HarvestMissionQuests")
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "HarvestMissionQuests")
                end},
            },
            NextStep = 8,
        },
        {--tail1.8
            Name = "Suiting Their Needs",
            Instructions = "Speak to a [F2F5A9]tailor[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogue = {"[Tailor I] I finished my mission.","Wonderful job, my Apprentice Tailor!"},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierOne", 8},
            },
            Goals = {
                {"TurnIn"},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have achieved the Tailor rank of Apprentice.","info")
                end},
            },
        },
    },
    TailorProfessionTierTwo = {
        {--tail2.1
            Name = "Speedy Delivery",
            Instructions = "Speak to any [F2F5A9]tailor[-].",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            StartDialogue = {"[Tailor II] I can do more than orders.","You think so, too? Then sure, help me with safely delivering some products."},
            IneligibleDialogue = { "[Tailor II] Is there more to learn?","Of course, you think you are a master already? You would insult my livelihood like that? You are not ready yet if you are asking me at your level."},
            IneligibleCheck = {
                {"MinTimesQuestDone", "TailorProfessionTierOne", 1},
                {"MaxTimesQuestDone", "TailorProfessionTierTwo", 1},
                --{"TailorProfessionTierOne", 7, true},
                --{"TailorProfessionTierTwo", 1, false},
            },
            Requirements = {
                {"Skill", "FabricationSkill", 50},
                {"MaxTimesQuestDone", "TailorProfessionTierTwo", 1},
                {"MinTimesQuestDone", "TailorProfessionTierOne", 1},
                {"CapStepCompletions", "TailorProfessionTierTwo", 1, 1},
            },
            NextStep = 2,
        },
        {--tail2.2
            Name = "Speedy Delivery",
            Instructions = "Make 2 sets of [F2F5A9]Quilted Padded Armor[-] (Helm, Leggings, Chest).",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierTwo", 2},
                {"HasCraftedItem", "robe_padded_helm", 2, "Material", "QuiltedCloth"},
                {"HasCraftedItem", "robe_padded_leggings", 2, "Material", "QuiltedCloth"},
                {"HasCraftedItem", "robe_padded_tunic", 2, "Material", "QuiltedCloth"},
            },
            Goals = {
                {"StillEligible", "TailorProfessionTierTwo", 2},
            },
            NextStep = 3,
        },
        {--tail2.3
            Name = "Speedy Delivery",
            Instructions = "Deliver 1 set and Wear the second set of [F2F5A9]Quilted Padded Armor[-] (Helm, Leggings, Chest) to [F2F5A9]Helm[-].",
            MarkerLocs = {
                QuestMarkers.TailorHelm,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor II] I have a delivery, and a demo.","Let me take a look at you. Quilted, huh? And it stays together while travelling, not bad! I think I'd like to commission you, myself."},
            IneligibleDialogue = { "[Tailor II] What do you think of this armor?","It's missing something. I don't think I'm sold on it, just yet."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierTwo", 3},
                {"HasCraftedItem", "robe_padded_helm", 2, "Material", "QuiltedCloth"},
                {"HasCraftedItem", "robe_padded_leggings", 2, "Material", "QuiltedCloth"},
                {"HasCraftedItem", "robe_padded_tunic", 2, "Material", "QuiltedCloth"},
                {"HasEquippedCraftedItem", "robe_padded_helm", 1, "Material", "QuiltedCloth"},
                {"HasEquippedCraftedItem", "robe_padded_leggings", 1, "Material", "QuiltedCloth"},
                {"HasEquippedCraftedItem", "robe_padded_tunic", 1, "Material", "QuiltedCloth"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionTierTwo", 3},
            },
            OnEnd = {
                {"TakeCraftedItemFromBackpack", "robe_padded_helm", 1, "Material", "QuiltedCloth"},
                {"TakeCraftedItemFromBackpack", "robe_padded_leggings", 1, "Material", "QuiltedCloth"},
                {"TakeCraftedItemFromBackpack", "robe_padded_tunic", 1, "Material", "QuiltedCloth"},
            },
            NextStep = 4,
        },
        {--tail2.4
            Name = "Speedy Delivery",
            Instructions = "The [F2F5A9]tailor[-] of [F2F5A9]Helm[-] has a commission for you.",
            MarkerLocs = {
                QuestMarkers.TailorHelm,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor II] I can take your commission.","Great, can you bring 4 new pairs of shorts, 6 new skirts, 12 waterskins, and 12 pouches to Trinit? A group of schoolkids there are going to be visiting Valus and I want to help them look sharp!"},
            IneligibleDialogue = { "[Tailor II] What is your commission?","I'm sorry, I don't think you can currently help me."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierTwo", 4},
                {"Skill", "FabricationSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 5,
        },
        {--tail2.5
            Name = "Speedy Delivery",
            Instructions = "Make 4 [F2F5A9]Shorts[-], 6 [F2F5A9]Skirts[-], 12 [F2F5A9]Waterskins[-], and 12 [F2F5A9]Pouches.[-]",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierTwo", 5},
                {"HasCraftedItem", "clothing_shorts_legs", 4},
                {"HasCraftedItem", "clothing_skirt_legs", 6},
                {"HasCraftedItem", "waterskin", 12},
                {"HasCraftedItem", "pouch", 12},
            },
            Goals = {
                {"StillEligible", "TailorProfessionTierTwo", 5},
            },
            NextStep = 6,
        },
        {--tail2.6
            Name = "Speedy Delivery",
            Instructions = "Deliver the 4 [F2F5A9]Shorts[-], 6 [F2F5A9]Skirts[-], 12 [F2F5A9]Waterskins[-], and 12 [F2F5A9]Pouches[-] to [F2F5A9]Trinit.[-]",
            MarkerLocs = {
                QuestMarkers.TailorTrinit,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor II] Helm sent these for trip.","Wow, really? That is so nice of them. The kids will be so happy to wear these! Hey, this trip was going to make me miss a delivery. Could I ask you to help me?"},
            IneligibleDialogue = { "[Tailor II] Would these help your town's kids?","They could, yes, but my gut doesn't trust the math here. I cannot currently accept your gifts."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierTwo", 6},
                {"HasCraftedItem", "clothing_shorts_legs", 4},
                {"HasCraftedItem", "clothing_skirt_legs", 6},
                {"HasCraftedItem", "waterskin", 12},
                {"HasCraftedItem", "pouch", 12},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionTierTwo", 6},
            },
            OnEnd = {
                {"TakeCraftedItems", "clothing_shorts_legs", 4},
                {"TakeCraftedItems", "clothing_skirt_legs", 6},
                {"TakeCraftedItems", "waterskin", 12},
                {"TakeCraftedItems", "pouch", 12},
            },
            NextStep = 7,
        },
        {--tail2.7
            Name = "Speedy Delivery",
            Instructions = "The [F2F5A9]tailor[-] of [F2F5A9]Trinit[-] has a request for you.",
            MarkerLocs = {
                QuestMarkers.TailorTrinit,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor II] I could do your delivery.","Oh, wonderful! You are a beacon of goodness. I have already started, so you only need to finish the rest of this order!"},
            IneligibleDialogue = { "[Tailor II] Could I do your delivery?","No offense, but I don't think I could leave my reputations in your hands at your skill level."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierTwo", 7},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 8,
        },
        {--tail2.8
            Name = "Speedy Delivery",
            Instructions = "Make 20 [F2F5A9]Saddles[-] and 20 [F2F5A9]Saddle Bags.[-]",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierTwo", 8},
                {"HasCraftedItem", "saddlebags_packed", 20, "EquipmentTemplate", "saddlebags"},
                {"HasCraftedItem", "saddle_packed", 20, "EquipmentTemplate", "saddle"},
            },
            Goals = {
                {"StillEligible", "TailorProfessionTierTwo", 8},
            },
            NextStep = 9,
        },
        {--tail2.9
            Name = "Speedy Delivery",
            Instructions = "Deliver the 20 [F2F5A9]Saddles[-] and 20 [F2F5A9]Saddle Bags[-] to [F2F5A9]Oasis.[-]",
            MarkerLocs = {
                QuestMarkers.TailorOasis,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor II] Here is the remainder of Trinit's order.","Did you make these ones? They are wonderful quality! Good work!"},
            IneligibleDialogue = { "[Tailor II] Mind if I finish Trinit's order?","Yes, I do mind. This does not complete the order, and I will not accept this."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierTwo", 9},
                {"HasCraftedItem", "saddlebags_packed", 20, "EquipmentTemplate", "saddlebags"},
                {"HasCraftedItem", "saddle_packed", 20, "EquipmentTemplate", "saddle"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionTierTwo", 9},
            },
            OnEnd = {
                {"TakeCraftedItems", "saddlebags_packed", 20, "EquipmentTemplate", "saddlebags"},
                {"TakeCraftedItems", "saddle_packed", 20, "EquipmentTemplate", "saddle"},
            },
            NextStep = 10,
        },
        {--tail2.10
            Name = "Speedy Delivery",
            Instructions = "Speak to any [F2F5A9]tailor.[-]",
            MarkerLocs = {
                QuestMarkers.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor II] have proven myself able to deliver.","Yes, you have! Keep shining and come back to us later."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierTwo", 10},
            },
            Goals = {
                {"TurnIn"},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have achieved the Tailor rank of Journeyman.","info")
                end},
            },
        },
    },
    TailorProfessionTierThree = {
        {--tail3.1
            Name = "Assembly Line Of One",
            Instructions = "Speak to any [F2F5A9]tailor.[-]",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            StartDialogue = {"[Tailor III] I am sure feeling bright.","You do still shine, magnificent! Some lose their luster, but you may have what it takes to become a Grandmaster. Before you can, though, we need to test your production quality at high volume."},
            IneligibleDialogue = { "[Tailor III] Is there more to learn?","You think you are a master already? Well, you are close, but still! You are not ready yet."},
            IneligibleCheck = {
                {"MinTimesQuestDone", "TailorProfessionTierTwo", 1},
                {"MaxTimesQuestDone", "TailorProfessionTierThree", 1},
                --{"TailorProfessionTierTwo", 10, true},
                --{"TailorProfessionTierThree", 1, false},
            },
            Requirements = {
                {"Skill", "FabricationSkill", 80},
                {"MaxTimesQuestDone", "TailorProfessionTierThree", 1},
                {"MinTimesQuestDone", "TailorProfessionTierTwo", 1},
                {"CapStepCompletions", "TailorProfessionTierThree", 1, 1},
            },
            NextStep = 2,
        },
        {--tail3.2
            Name = "Assembly Line Of One",
            Instructions = "Make 300 [F2F5A9]Silk Cloth[-] and 300 [F2F5A9]Beast Leather.[-]",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierThree", 2},
                {"HasCraftedItem", "resource_silk_cloth", 300},
                {"HasCraftedItem", "resource_beast_leather", 300},
            },
            Goals = {
                {"StillEligible", "TailorProfessionTierThree", 2},
            },
            NextStep = 3,
        },
        {--tail3.3
            Name = "Assembly Line Of One",
            Instructions = "Give the 300 [F2F5A9]Silk Cloth[-] and 300 [F2F5A9]Beast Leather[-] to any [F2F5A9]tailor.[-]",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor III] Here you go.","Let's see! Pretty consistent quality, I am impressed!"},
            IneligibleDialogue = { "[Tailor III] How are these?","Hmmm. Feels like something is missing. Look over these another time."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierThree", 3},
                {"HasCraftedItem", "resource_silk_cloth", 300},
                {"HasCraftedItem", "resource_beast_leather", 300},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionTierThree", 3},
            },
            OnEnd = {
                {"TakeCraftedItems", "resource_silk_cloth", 300},
                {"TakeCraftedItems", "resource_beast_leather", 300},
            },
            NextStep = 4,
        },
        {--tail3.4
            Name = "Assembly Line Of One",
            Instructions = "Speak to any [F2F5A9]tailor.[-]",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor III] What do you need?","Help us with this bulk order for a mage academy, please!"},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierThree", 4},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 5,
        },
        {--tail3.5
            Name = "Assembly Line Of One",
            Instructions = "Make 20 [F2F5A9]Cloth Mage Robes[-], 5 [F2F5A9]Quilted Mage Robes[-], and 10 [F2F5A9]Silk Mage Robes.[-]",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierThree", 5},
                {"HasCraftedItem", "robe_robe", 20, "Material", "Cloth"},
                {"HasCraftedItem", "robe_robe", 5, "Material", "QuiltedCloth"},
                {"HasCraftedItem", "robe_robe", 10, "Material", "SilkCloth"},
            },
            Goals = {
                {"StillEligible", "TailorProfessionTierThree", 5},
            },
            NextStep = 6,
        },
        {--tail3.6
            Name = "Assembly Line Of One",
            Instructions = "Give the 20 [F2F5A9]Cloth[-], 5 [F2F5A9]Quilted[-], and 10 [F2F5A9]Silk Mage Robes[-] to any [F2F5A9]tailor.[-]",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor III] Off to Wizard School.","What consistent quality! I got one more challenge for you."},
            IneligibleDialogue = { "[Tailor III] How are these?","Do you want these Mage students bullied? That'll happen if you don't get this order right."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierThree", 6},
                {"HasCraftedItem", "robe_robe", 20, "Material", "Cloth"},
                {"HasCraftedItem", "robe_robe", 5, "Material", "QuiltedCloth"},
                {"HasCraftedItem", "robe_robe", 10, "Material", "SilkCloth"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionTierThree", 6},
            },
            OnEnd = {
                {"TakeCraftedItems", "robe_robe", 20, "Material", "Cloth"},
                {"TakeCraftedItems", "robe_robe", 5, "Material", "QuiltedCloth"},
                {"TakeCraftedItems", "robe_robe", 10, "Material", "SilkCloth"},
            },
            NextStep = 7,
        },
        {--tail3.7
            Name = "Assembly Line Of One",
            Instructions = "Make a full set of [F2F5A9]Vile Hardened Armor[-] (Helm, Leggings, Chest).",
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierThree", 7},
                {"HasCraftedItem", "armor_hardened_helm", 1, "Material", "VileLeather"},
                {"HasCraftedItem", "armor_hardened_chest", 1, "Material", "VileLeather"},
                {"HasCraftedItem", "armor_hardened_leggings", 1, "Material", "VileLeather"},
            },
            Goals = {
                {"StillEligible", "TailorProfessionTierThree", 7},
            },
            NextStep = 8,
        },
        {--tail3.8
            Name = "Assembly Line Of One",
            Instructions = "Give the set of [F2F5A9]Vile Hardened Armor[-] to any [F2F5A9]tailor.[-]",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor III] This proves my abilities.","Wow, you actually succeeded! I was never really going to take it, you keep the armor!"},
            IneligibleDialogue = { "[Tailor III] How are these?","Not Vile enough. I want it made with your touch on it."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierThree", 8},
                {"HasCraftedItem", "armor_hardened_helm", 1, "Material", "VileLeather"},
                {"HasCraftedItem", "armor_hardened_chest", 1, "Material", "VileLeather"},
                {"HasCraftedItem", "armor_hardened_leggings", 1, "Material", "VileLeather"},
            },
            Goals = {
                {"TurnIn"},
                {"StillEligible", "TailorProfessionTierThree", 8},
            },
            NextStep = 9,
        },
        {--tail3.9
            Name = "Assembly Line Of One",
            Instructions = "Speak to any [F2F5A9]tailor.[-]",
            MarkerLocs = {
                QuestMarkerGroups.AllTailors,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor III] See? I can keep up with you all.","You certainly can! You must shine event brighter; brighter until you think you can no more and then speak with us again. Good work, Master Tailor."},
            IneligibleDialogue = { "[Tailor III] Am I ready to be a Master?","Not as you are. You seem to have lost your shine."},
            Requirements = {
                {"StepMustBeActive", "TailorProfessionTierThree", 9},
            },
            Goals = {
                {"TurnIn"},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("You have achieved the Tailor rank of Master.","info")
                end},
            },
        },
    },
    TailorProfessionTierFour = {
        
        {--tail4.1
            Name = "High Cotton",
            Instructions = "Travel to the [F2F5A9]Eldeir[-] and speak to [F2F5A9]Ren Mezmer[-] near the [F2F5A9]cotton field[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            StartDialogue = {"[Tailor IV] I want to be a Grandmaster.","You have done very well.  I think you have what it takes to earn the title of Grandmaster Tailor.  Sadly, I do not get to make that decision. Travel to the Eldeir cotton fields and speak to Ren Mezmer, she will guide you in these final trials. May your baskets always overflow with cotton."},
            IneligibleDialogue = { "[Tailor IV] Is there more to learn?","There is one final step but I need you to train even more, first."},
            IneligibleCheck = {
                {"MinTimesQuestDone", "TailorProfessionTierThree", 1},
                {"MaxTimesQuestDone", "TailorProfessionTierFour", 1},
                --{"TailorProfessionTierThree", 9, true},
                --{"TailorProfessionTierFour", 1, false},
            },
            Requirements = {
                --{"Disabled"},
                {"Skill", "FabricationSkill", 100},
                {"MaxTimesQuestDone", "TailorProfessionTierFour", 1},
                {"MinTimesQuestDone", "TailorProfessionTierThree", 1},
                {"CapStepCompletions", "TailorProfessionTierFour", 1, 1},
            },
            NextStep = 2,
        },

        {--tail4.2
            Name = "Hand in Hand",
            Instructions = "Travel to the [F2F5A9]Eldeir[-] and speak to [F2F5A9]Ren Mezmer[-] near the [F2F5A9]cotton field[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogue = {"[Tailor IV] I want to be a Grandmaster.","You have come for the final stages of your training.  How can I tell?  It's your hands, the hands of a tailor can tell the story of their craft.  You have the hands of someone who has put in the work and toil required to master the trade.  But are they still deft?  Do they still have the percision to create that perfect seam which can be the difference between life and death for your patrons? You will need to prove this to me."},
            Requirements = {
                --{"Disabled"},
                {"Skill", "FabricationSkill", 100},
                {"StepMustBeActive", "TailorProfessionTierFour", 2},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 3,
        },

        {--tail4.3
            Name = "The Deft Proof",
            Instructions = "Speak to [F2F5A9]Ren Mezmer[-] near the [F2F5A9]cotton field[-] in [F2F5A9]Eldeir[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogue = {"[Tailor IV] How can I prove myself?.","Bring me a full set of Vile Hardened Leather armor (helm, chest, legs). Each piece must be of the highest quality [+3 defense each].  Do this and you will prove your worth to complete the next trial."},
            Requirements = {
                --{"Disabled"},
                {"Skill", "FabricationSkill", 100},
                {"StepMustBeActive", "TailorProfessionTierFour", 3},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 4,
        },

        {--tail4.4
            Name = "Seams Reasonable",
            Instructions = "Bring a Vile Hardened Leather helm, chest, and legs [+3 defense each] to [F2F5A9]Ren Mezmer[-] near the [F2F5A9]cotton field[-] in [F2F5A9]Eldeir[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor IV] How is this?.","The seams are nice and tight and you've managed to do it without weakening the leather. Well done, these will do nicely."},
            IneligibleDialogue = { "[Tailor IV] What did you need?","I need to see a Vile Hardened Leather helm, chest, and legs [+3 defense each]."},
            Requirements = {
                {"Skill", "FabricationSkill", 100},
                {"StepMustBeActive", "TailorProfessionTierFour", 4},
                {"Custom", function(playerObj)
                    local backpackObj = playerObj:GetEquippedObject("Backpack")
                    local hasHelm = false
                    local hasChest = false
                    local hasLegs = false

                    ForEachItemInContainerRecursive(backpackObj, function( item )
                        if(
                            item:GetObjVar("ArmorType") == "Hardened" -- is it the right armor type?
                            and item:GetObjVar("Crafter") == playerObj -- did the player craft this?
                            and item:GetObjVar("Material") == "VileLeather" -- is it VileLeather?
                            and item:GetObjVar("ArmorBonus") == 3 -- is it +3 armor bonus?
                        ) then
                            local template = item:GetCreationTemplateId()
                            if( template == "armor_hardened_chest" ) then
                                hasChest = true
                            elseif( template == "armor_hardened_helm" ) then
                                hasHelm = true
                            elseif( template == "armor_hardened_leggings" ) then
                                hasLegs = true
                            end
                        end
                        return true
                    end)

                    --DebugMessage("HasItems: " .. tostring(hasItem))
                    return ( hasHelm and hasChest and hasLegs )
                end},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 5,
        },

        {--tail4.5
            Name = "Flip of a Coin",
            Instructions = "Speak to [F2F5A9]Ren Mezmer[-] near the [F2F5A9]cotton field[-] in [F2F5A9]Eldeir[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogue = {"[Tailor IV] Is there anything else?","One last trial for you. Bring me an Artisan's Crafting Commission to show your merits in crafting the highest quality of crafting orders."},
            Requirements = {
                --{"Disabled"},
                {"Skill", "FabricationSkill", 100},
                {"StepMustBeActive", "TailorProfessionTierFour", 5},
            },
            Goals = {
                {"TurnIn"},
            },
            NextStep = 6,
        },

        {--tail4.6
            Name = "Flip of a Coin",
            Instructions = "Bring an artisan's commission to [F2F5A9]Ren Mezmer[-] near the [F2F5A9]cotton field[-] in [F2F5A9]Eldeir[-].",
            MarkerLocs = {
                --QuestMarkerGroups.AllBlacksmiths,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[Tailor IV] I have the commission.","You have done well and are worthy of the title Grandmaster Tailor."},
            IneligibleDialogue = { "[Tailor IV] What did you need?","I need you to bring me an Artisan's Crafting Commission. You can get them from crafting orders."},
            Requirements = {
                --{"Disabled"},
                {"Skill", "FabricationSkill", 100},
                {"StepMustBeActive", "TailorProfessionTierFour", 6},
                {"HasItemInBackpack", "artisan_crafting_commission_fabrication", 1},
                -- TODO: REQUIRE An artisans token!
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "artisan_crafting_commission_fabrication", 1},
                {"TakeItem", "artisan_crafting_commission_fabrication", 1},
            },
        },

    },
    ChefProfessionIntro = {
        {--1
            Name = "Learn Cooking",
            Instructions = "Speak to a [F2F5A9]chef.[-]",
            NextStep = 2,
            EndDialogue = {"[Chef Intro] Teach me to cook.","Of course! Everyone needs to eat, and so everyone should know how to cook too! There are all kinds of recipes. It all depends on what you put in the pot! Let's start with some low quality meat that you can skin from most of the critters around here. I can teach you how to skin also!"},
            Requirements = {
                {"MaxTimesQuestDone", "ChefProfessionIntro", 1},
                --{"QuestActive", "ChefProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn Cooking",
            Instructions = "Get 10 [F2F5A9]Stringy Meat[-] by skinning [F2F5A9]Chickens[-] in town.",
            NextStep = 3,
            StartDialogue = {"[Chef Intro] Teach me to cook.","Of course! Everyone needs to eat, and so everyone should know how to cook too! There are all kinds of recipes. It all depends on what you put in the pot! Let's start with some low quality meat that you can skin from most of the critters around here. I can teach you how to skin also!"},
            Requirements = {
                {"MaxTimesQuestDone", "ChefProfessionIntro", 1},
                {"QuestActive", "ChefProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill", "CookingSkill", 30},
                {"Custom", function(playerObj)
                    Quests.TryStart(playerObj, "Skinning", 1)
                end},
            },
            Goals = {
                {"HasItemInBackpack", "animalparts_stringy_meat", 10},
            },
        },
        {--3
            Name = "Learn Cooking",
            Instructions = "Get a [F2F5A9]Cooking Pot[-]. They can be purchased from tinkerers.",
            NextStep = 4,
            EndDialogue = {"[Chef Intro] Look how stringy this meat is.","All the better. Stringy meat is harder to ruin. It doesn't taste much different even if it's burnt. Go find a pot to cook it in."},
            Goals = {
                {"HasItemInBackpack", "tool_cookingpot"},
            },
        },
        {--4
            Name = "Learn Cooking",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            NextStep = 5,
            EndDialogue = {"[Chef Intro] Will this pot suffice?","Sure, they are all the same, really. Go find a source of heat and try your luck cooking a snack out of that meat."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--5
            Name = "Learn Cooking",
            Instructions = "Find a [F2F5A9]heat source[-], such as a [F2F5A9]Hearth[-] or a [F2F5A9]Campfire[-].",
            NextStep = 6,
            Goals = {
                {"Custom", function(playerObj)
                    local heatSource = FindObjects(SearchMulti(
                    {
                        SearchObjectInRange(5),
                        SearchObjVar("HeatSource", true),
                    }))
                    if ( next(heatSource) ) then return true else return false end
                end},
            },
        },
        {--6
            Name = "Learn Cooking",
            Instructions = "Put the [F2F5A9]Stringy Meat[-] into your [F2F5A9]Cooking Pot[-] by itself.",
            NextStep = 7,
            Goals = {
                {"Custom", function(playerObj)
                    local backpack = playerObj:GetEquippedObject("Backpack")
                    if ( backpack ) then
                        local pots = FindItemsInContainerByTemplateRecursive(playerObj, "tool_cookingpot")
                        if ( next(pots) ) then
                            for pot = 1, #pots do
                                local recipe = GetBestFoodThatCanBeCooked(pots[pot])
                                if ( recipe == "StringyMeatSnack" ) then
                                    return true
                                end
                            end
                        end
                    end
                    return false
                end},
            },
        },
        {--7
            Name = "Learn Cooking",
            Instructions = "Cook a [F2F5A9]Stringy Meat Snack[-]. Double click your [F2F5A9]Cooking Pot[-] in your backpack to open it. Press the cook button while standing near a heat source to cook the meat.",
            NextStep = 8,
            Goals = {
                {"HasItemInBackpack", "item_food_stringymeat_snack"},
            },
        },
        {--8
            Name = "Learn Cooking",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            EndDialogue = {"[Chef Intro] Would you like to try my meat?","That's... that's nice of you but no thank you. I get plenty of the stuff. Top quality. The better the quality, the better the quality, the better for you. You are what you eat!"},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    ChefProfessionTierOne = {
        {--1
            Name = "Suspect Salami",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogue = {"[Chef I] What can I cook?","You CAN cook anything small enough to fit in your pot! Chickens, very small rocks, maps, scrolls, torches, bandages, potions, moss, clothing, smaller cooking pots, metal tools, wooden tools, dead animals, live animals, live vegetables, dead vegetables, books, luggage, tents if you fold them properly, knives, bees, chainmail, churches, pillows, small children, the Pyros flag...\n\n\n\n\n\n\n\n\n...gold coins, platinum coins, rats, shoes, land deeds, arrows, George's horseshoes, Ol'Kalu if you are clever, bucklers. Hell, even raw meat, fruits, and vegetables! Go try some out for yourself. Experiment! Those are just a few ideas!"},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "ChefProfessionTierOne", 1},
                --{"QuestActive", "ChefProfessionTierOne", false},
                {"MinTimesQuestDone", "ChefProfessionIntro", 1},
                {"Skill", "CookingSkill", 30},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Suspect Salami",
            Instructions = "Craft some [F2F5A9]beginner recipes[-].",
            StartDialogue = {"[Chef I] What can I cook?","You CAN cook anything small enough to fit in your pot! Chickens, very small rocks, maps, scrolls, torches, bandages, potions, moss, clothing, smaller cooking pots, metal tools, wooden tools, dead animals, live animals, live vegetables, dead vegetables, books, luggage, tents if you fold them properly, knives, bees, chainmail, churches, pillows, small children, the Pyros flag...\n\n\n\n\n\n\n\n\n...gold coins, platinum coins, rats, shoes, land deeds, arrows, George's horseshoes, Ol'Kalu if you are clever, bucklers. Hell, even raw meat, fruits, and vegetables! Go try some out for yourself. Experiment! Those are just a few ideas!"},
            NextStep = 3,
            Requirements = {
                {"MaxTimesQuestDone", "ChefProfessionTierOne", 1},
                {"QuestActive", "ChefProfessionTierOne", false},
                {"MinTimesQuestDone", "ChefProfessionIntro", 1},
                {"Skill", "CookingSkill", 30},
            },
        },
        {--3
            Name = "Suspect Salami",
            Instructions = "Cook a [F2F5A9]Carrot[-].",
            NextStep = 4,
            Goals = {
                {"HasItemInBackpack", "item_food_carrot_snack"},
            },
        },
        {--4
            Name = "Suspect Salami",
            Instructions = "Cook an [F2F5A9]Onion[-].",
            NextStep = 5,
            Goals = {
                {"HasItemInBackpack", "item_food_onion_snack"},
            },
        },
        {--5
            Name = "Suspect Salami",
            Instructions = "Cook a [F2F5A9]Strawberry[-].",
            NextStep = 6,
            Goals = {
                {"HasItemInBackpack", "item_food_strawberry_snack"},
            },
        },
        {--6
            Name = "Suspect Salami",
            Instructions = "Cook a [F2F5A9]Melon[-].",
            NextStep = 7,
            Goals = {
                {"HasItemInBackpack", "item_food_melon_meal"},
            },
        },
        {--7
            Name = "Suspect Salami",
            Instructions = "Cook [F2F5A9]Mystery Meat[-].",
            NextStep = 8,
            Goals = {
                {"HasItemInBackpack", "item_food_mysterymeat_snack"},
            },
        },
        {--8
            Name = "Suspect Salami",
            Instructions = "Cook a [F2F5A9]Mystery Meat and Strawberry Snack[-].",
            NextStep = 9,
            Goals = {
                {"HasItemInBackpack", "item_food_mysterymeatstrawberry_snack"},
            },
        },
        {--9
            Name = "Suspect Salami",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperSouthernHills,
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogue = {"[Chef I] It's edible, I think.","Yes, yes... showoff. Look at you, very good. Anyone can manage to cook with those boring ingredients. There are many combinations of ingredients at our disposal, and different ingredients provide different health benefits for mind and body! The best chefs can create dishes which are quite desirable to adventurers. It's up to us chefs to share our knowledge with each other so that we can feed Celador!"},
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","animalparts_stringy_meat",50},
                },
            },
        },
    },
    ChefProfessionTierTwo = {
        {--1
            Name = "Leafy Greens?",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogue = {"[Chef II] I seek more training as a cook.","Very good, that is the attitude you will need as being a chef is not as desserts and whipped toppings. Lets start with the basics of a accomplished chef, cook me up 10 Red-leaf Lettuce meals."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "ChefProfessionTierTwo", 1},
                --{"QuestActive", "ChefProfessionTierTwo", false},
                {"MinTimesQuestDone", "ChefProfessionTierOne", 1},
                {"Skill", "CookingSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Leafy Greens?",
            Instructions = "Craft [F2F5A9]10 Red-leaf Lettuce meals[-] for the chef guild.",
            NextStep = 3,
            Requirements = {
                {"Skill", "CookingSkill", 50},
            },
            QuestData = {
                Key = "ChefProfessionTierTwo2",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Food",
                    "item_food_redleaflettuce_meal"
                },
                Count = 10,
                Name = "Red-leaf Lettuce Meal",
                NamePlural = "Red-leaf Lettuce Meals",
            },
        },
        {--3
            Name = "Leafy Greens?",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Chef II] What was I cooking?", "10 Red-leaf Lettuce meals."},
            EndDialogue = {"[Chef II] I have the meals.","Not too wilted and not too bitter, you did quite well."},
            NextStep = 4,
            Requirements = {
                {"Skill", "CookingSkill", 50},
                {"HasItemInBackpack", "item_food_redleaflettuce_meal", 10},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "item_food_redleaflettuce_meal", 10},
                {"TakeItem", "item_food_redleaflettuce_meal", 10},
            },
        },
        {--4
            Name = "Leafy Greens?",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogue = {"[Chef II] I'd like to test my cooking skills more.","Your lettuce was well cooked, we need something that is a bit more challening for you. Ah, I know, melons! Prepare for me 20 Melon meals, I'll feature them at a feast I am catering."},
            NextStep = 5,
            Requirements = {
                {"Skill", "CookingSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 5
            Name = "Leafy Greens?",
            Instructions = "Cook [F2F5A9]20 Melon meals[-] for the chef guild.",
            NextStep = 6,
            Requirements = {
                {"Skill", "CookingSkill", 50},
            },
            QuestData = {
                Key = "ChefProfessionTierTwo5",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Food",
                    "item_food_melon_meal"
                },
                Count = 20,
                Name = "Melon Meal",
                NamePlural = "Melon Meals",
            },
        },
        {--6
            Name = "Leafy Greens?",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Chef II] How many melons did you need?", "I needed 20 melon meals."},
            EndDialogue = {"[Chef II] I have prepared the melons.","Goodness, how long did you cook these? They are mushy in the middle, a good melon must be warm but still offer that crisp center. These will not do, make them again."},
            NextStep = 7,
            Requirements = {
                {"Skill", "CookingSkill", 50},
                {"HasItemInBackpack", "item_food_melon_meal", 20},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "item_food_melon_meal", 20},
                {"TakeItem", "item_food_melon_meal", 20},
            },
        },
        { -- 7
            Name = "Leafy Greens?",
            Instructions = "Cook another [F2F5A9]20 Melon meals[-] for the chef guild.",
            NextStep = 8,
            Requirements = {
                {"Skill", "CookingSkill", 50},
            },
            QuestData = {
                Key = "ChefProfessionTierTwo7",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Food",
                    "item_food_melon_meal"
                },
                Count = 20,
                Name = "Melon Meal",
                NamePlural = "Melon Meals",
            },
        },
        {--8
            Name = "Leafy Greens?",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Chef II] How many melons did you need?", "I needed 20 melon meals."},
            EndDialogue = {"[Chef II] See if these melons are better.","Yes! These are perfect the texture is crisp and delightful while the center is still warm and inviting. You are learning."},
            NextStep = 9,
            Requirements = {
                {"Skill", "CookingSkill", 50},
                {"HasItemInBackpack", "item_food_melon_meal", 20},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "item_food_melon_meal", 20},
                {"TakeItem", "item_food_melon_meal", 20},
            },
        },
        {--9
            Name = "Leafy Greens?",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogue = {"[Chef II] What else can you teach me?","For now you need to practice on your own, master the ingredients we have discussed and come back to me when you've grown in skill. I bestow upon you the title of journeyman."},
            Requirements = {
                {"Skill", "CookingSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    ChefProfessionTierThree = {
        {--1
            Name = "Sous Whew",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogue = {"[Chef III] I'm ready to take the next step.","This is good news, go get some cucumbers and return to me when you've prepared 20 cucumber banquets."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "ChefProfessionTierThree", 1},
                --{"QuestActive", "ChefProfessionTierThree", false},
                {"MinTimesQuestDone", "ChefProfessionTierTwo", 1},
                {"Skill", "CookingSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Sous Whew",
            Instructions = "Cook [F2F5A9]20 cucumber banquets[-] for the chef guild.",
            NextStep = 3,
            Requirements = {
                {"Skill", "CookingSkill", 80},
            },
            QuestData = {
                Key = "ChefProfessionTierThree2",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Food",
                    "item_food_cucumber_banquet"
                },
                Count = 20,
                Name = "Cucumber Banquet",
                NamePlural = "Cucumber Banquets",
            },
        },
        {--3
            Name = "Sous Whew",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Chef III] What was I cooking?", "I needed 20 cucumber banquets."},
            EndDialogue = {"[Chef III] I have prepared the banquets.","Excellent, you've improved much since that melon incident. Think you could bring me some gourmet meat with strawberries banquet to finish off this dish?"},
            NextStep = 4,
            Requirements = {
                {"Skill", "CookingSkill", 80},
                {"HasItemInBackpack", "item_food_cucumber_banquet", 20},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "item_food_cucumber_banquet", 20},
                {"TakeItem", "item_food_cucumber_banquet", 20},
            },
        },
        { -- 4
            Name = "Sous Whew",
            Instructions = "Prepare [F2F5A9]5 gourmet meat-strawberry banquests[-] for the chef guild.",
            NextStep = 5,
            Requirements = {
                {"Skill", "CookingSkill", 80},
            },
            QuestData = {
                Key = "ChefProfessionTierThree4",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Food",
                    "item_food_gourmetmeatstrawberry_banquet"
                },
                Count = 5,
                Name = "Gourmet Meat-Strawberry Banquest",
                NamePlural = "Gourmet Meat-Strawberry Banquests",
            },
        },
        {--5
            Name = "Sous Whew",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Chef III] What was I cooking?", "I needed 5 gourmet meat-strawberry banquests."},
            EndDialogue = {"[Chef III] Here are the banquets.","Your skill is improving in leaps and bounds and I'm glad you've started to master the mixing of ingredients. You are truly worth of the master chef title."},
            Requirements = {
                {"Skill", "CookingSkill", 80},
                {"HasItemInBackpack", "item_food_gourmetmeatstrawberry_banquet", 5},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "item_food_gourmetmeatstrawberry_banquet", 5},
                {"TakeItem", "item_food_gourmetmeatstrawberry_banquet", 5},
            },
        },
        
    },
    ChefProfessionTierFour = {
        {--1
            Name = "Cook's Combo",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogue = {"[Chef IV] I'd like to complete my last trial.","Very well, we are catering a banquet for an order of magi and they've requested Razor Fish and Eggplant you'll need about 40 of them."},
            NextStep = 2,
            Requirements = {
                {"MaxTimesQuestDone", "ChefProfessionTierFour", 1},
                --{"QuestActive", "ChefProfessionTierFour", false},
                {"MinTimesQuestDone", "ChefProfessionTierThree", 1},
                {"Skill", "CookingSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Cook's Combo",
            Instructions = "Cook [F2F5A9]40 Razor Fish-Egglant banquets[-] for the chef guild.",
            NextStep = 3,
            Requirements = {
                {"Skill", "CookingSkill", 100},
            },
            QuestData = {
                Key = "ChefProfessionTierThree2",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Food",
                    "item_food_fishfilletrazoreggplant_banquet"
                },
                Count = 40,
                Name = "Razor Fish-Egglant Banquet",
                NamePlural = "Razor Fish-Egglant Banquets",
            },
        },
        {--3
            Name = "Cook's Combo",
            Instructions = "Speak to a [F2F5A9]chef[-].",
            MarkerLocs = {
                QuestMarkers.HelmChef,
                QuestMarkers.EldeirChef,
            },
            EndDialogueHasEligibleCheck = true,
            IneligibleDialogue = { "[Chef IV] What was I cooking?", "I needed 40 Razor Fish-Egglant banquets."},
            EndDialogue = {"[Chef IV] I have prepared the banquets.","These are amazingly prepared, you have outdone yourself this time. The chef's guild is proud to welcome you as our newest grandmaster."},
            Requirements = {
                {"Skill", "CookingSkill", 100},
                {"HasItemInBackpack", "item_food_fishfilletrazoreggplant_banquet", 40},
            },
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "item_food_fishfilletrazoreggplant_banquet", 40},
                {"TakeItem", "item_food_fishfilletrazoreggplant_banquet", 40},
            },
        },
    },
    TamerProfessionIntro = {
        {--1
            Name = "Learn Taming",
            Instructions = "Speak to a [F2F5A9]stable master[-].",
            NextStep = 2,
            EndDialogue = {"[Tamer Intro] Can you teach me taming?","Taming is not just skill, but a way of life. Tamers are intertwined with their pets. Taming creatures is as rewarding as it is involved. I can show you the basics. First, you will need a crook in order to tame."},
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            Requirements = {
                {"MaxTimesQuestDone", "TamerProfessionIntro", 1},
                --{"QuestActive", "TamerProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn Taming",
            Instructions = "Get a [F2F5A9]Crook[-]. You can buy them from stable masters.",
            NextStep = 3,
            StartDialogue = {"[Tamer Intro] Can you teach me taming?","Taming is not just skill, but a way of life. Tamers are intertwined with their pets. Taming creatures is as rewarding as it is involved. I can show you the basics. First, you will need a crook in order to tame."},
            Requirements = {
                {"MaxTimesQuestDone", "TamerProfessionIntro", 1},
                {"QuestActive", "TamerProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill", "AnimalTamingSkill", 30},
                {"SetSkill", "AnimalLoreSkill", 30},
            },
            Goals = {
                {"HasItemInBackpack", "tool_crook"},
            },
        },
        {--3
            Name = "Learn Taming",
            Instructions = "Speak to a [F2F5A9]stable master[-].",
            NextStep = 4,
            EndDialogue = {"[Tamer Intro] I found a crook, but I think it's bent.","Uhhh, no that is how they are supposed to be. Right, yeah... you hold it by that end. See if one of these chickens will warm up to you."},
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Learn Taming",
            Instructions = "Equip a [F2F5A9]Crook[-] by double clicking it in your backpack or dragging it onto yourself in the character window.",
            NextStep = 5,
            Goals = {
                {"Custom", function(playerObj)
                    local handObjects = {}
                    table.insert(handObjects, playerObj:GetEquippedObject("LeftHand"))
                    table.insert(handObjects, playerObj:GetEquippedObject("RightHand"))
                    local weapons = 0
                    for i = 1, #handObjects do
                        local weaponType = handObjects[i]:GetObjVar("WeaponType")
                        if ( weaponType == "Crook"  ) then
                            weapons = 1
                        end
                    end
                    if ( weapons == 0 ) then return false end
                    return true
                end},
            },
        },
        {--5
            Name = "Learn Taming",
            Instructions = "Tame a [F2F5A9]Chicken[-] using your [F2F5A9]Crook[-].",
            NextStep = 6,
            Goals = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        if ( pets[pet]:GetCreationTemplateId() == "chicken" ) then
                            local owners = pets[pet]:GetObjVar("PreviousOwners")
                            if ( owners and next(owners) ) then
                                for owner = 1, #owners do
                                    if ( GameObj(tonumber(owners[owner])) ~= playerObj ) then
                                        return false
                                    end
                                end
                            end
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--6
            Name = "Learn Taming",
            Instructions = "Speak to a [F2F5A9]stable master[-].",
            NextStep = 7,
            EndDialogue = {"[Tamer Intro] LOOK! HE IS MY FRIEND NOW!","Awww, he looks delicious, I mean adorable! This chicken is now your responsibility. You had better be a good chicken parent. He will need a name."},
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            Goals = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        if ( pets[pet]:GetCreationTemplateId() == "chicken" ) then
                            return true
                        end
                    end
                    return false
                end},
                {"TurnIn"},
            },
        },
        {--7
            Name = "Learn Taming",
            Instructions = "Rename your [F2F5A9]Chicken[-] by right clicking it and choosing the [F2F5A9]Rename[-] option.",
            NextStep = 8,
            Goals = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        if ( pets[pet]:GetCreationTemplateId() == "chicken" ) then
                            local name = pets[pet]:GetObjVar("CommandName")
                            if ( name and name ~= "chicken" ) then
                                return true
                            end
                        end
                    end
                    return false
                end},
            },
        },
        {--8
            Name = "Learn Taming",
            Instructions = "Speak to a [F2F5A9]stable master[-].",
            NextStep = 9,
            EndDialogue = {"[Tamer Intro] Now he is a member of the family.","Naming them always makes you more attached, doesn't it? I guess that's why they tell you not to name farm animals... woops. Well I'm sure nothing bad will happen to him. Try giving him some verbal commands!"},
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--9
            Name = "Learn Taming",
            Instructions = "Say \"[F2F5A9]all follow me[-]\" in chat. You can also use the [F2F5A9]Command[-] ability from your Animal Taming skill in your skill book.",
            NextStep = 10,
            OnStart = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        if ( pets[pet]:IsValid() and pets[pet]:GetCreationTemplateId() == "chicken" ) then
                            if ( pets[pet]:HasObjVar("LastPetCommand") ) then
                                pets[pet]:DelObjVar("LastPetCommand")
                            end
                        end
                    end
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        if ( pets[pet]:IsValid() and pets[pet]:GetCreationTemplateId() == "chicken" ) then
                            local lastPetCommand = pets[pet]:GetObjVar("LastPetCommand")
                            if ( lastPetCommand and lastPetCommand == "follow" ) then
                                return true
                            end
                        end
                    end
                    return false
                end},
            },
        },
        {--10
            Name = "Learn Taming",
            Instructions = "Say \"[F2F5A9]all stay[-]\" in chat. You can also use the[F2F5A9] Command [-]ability from your Animal Taming skill in your skill book.",
            NextStep = 11,
            Goals = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        if ( pets[pet]:IsValid() and pets[pet]:GetCreationTemplateId() == "chicken" ) then
                            local lastPetCommand = pets[pet]:GetObjVar("LastPetCommand")
                            if ( lastPetCommand and lastPetCommand == "stay" ) then
                                return true
                            end
                        end
                    end
                    return false
                end},
            },
        },
        {--11
            Name = "Learn Taming",
            Instructions = "Say \"[F2F5A9]all guard me[-]\" in chat. You can also use the[F2F5A9] Command [-]ability from your Animal Taming skill in your skill book.",
            NextStep = 12,
            Goals = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        if ( pets[pet]:IsValid() and pets[pet]:GetCreationTemplateId() == "chicken" ) then
                            local lastPetCommand = pets[pet]:GetObjVar("LastPetCommand")
                            if ( lastPetCommand and lastPetCommand == "guard" ) then
                                return true
                            end
                        end
                    end
                    return false
                end},
            },
        },
        {--12
            Name = "Learn Taming",
            Instructions = "Say \"[F2F5A9]all kill[-]\" in chat and target yourself. You can also use the[F2F5A9] Command [-]ability from your Animal Taming skill in your skill book.",
            NextStep = 13,
            Goals = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        if ( pets[pet]:IsValid() and pets[pet]:GetCreationTemplateId() == "chicken" ) then
                            local lastPetCommand = pets[pet]:GetObjVar("LastPetCommand")
                            if ( lastPetCommand and lastPetCommand == "attack" ) then
                                return true
                            end
                        end
                    end
                    return false
                end},
            },
        },
        {--13
            Name = "Learn Taming",
            Instructions = "Say \"[F2F5A9]all stop[-]\" in chat. You can also use the[F2F5A9] Command [-]ability from your Animal Taming skill in your skill book.",
            NextStep = 14,
            Goals = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        if ( pets[pet]:IsValid() and pets[pet]:GetCreationTemplateId() == "chicken" ) then
                            local lastPetCommand = pets[pet]:GetObjVar("LastPetCommand")
                            if ( lastPetCommand and lastPetCommand == "stop" ) then
                                return true
                            end
                        end
                    end
                    return false
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        if ( pets[pet]:IsValid() and playerObj:IsValid() and pets[pet]:GetCreationTemplateId() == "chicken" ) then
                            pets[pet]:NpcSpeech("*gazes at "..playerObj:GetName().." obediently*")
                            pets[pet]:PlayObjectSound("event:/animals/chicken/chicken_environment")
                            CallFunctionDelayed(TimeSpan.FromSeconds(3),function()
                                if ( pets[pet]:IsValid() ) then
                                    local loc = pets[pet]:GetLoc()
                                    pets[pet]:PlayObjectSound("event:/animals/chicken/chicken_death", false, 0.0)
                                    pets[pet]:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact", false, 0.0)
                                    PlayEffectAtLoc("MeatExplosion", loc)
                                    PlayEffectAtLoc("FireballExplosionEffect", loc)
                                    pets[pet]:SetScale(0*pets[pet]:GetScale())
                                    CallFunctionDelayed(TimeSpan.FromSeconds(3),function()
                                        if ( pets[pet]:IsValid() ) then
                                            pets[pet]:Destroy()
                                        end
                                    end)
                                end
                            end)
                        end
                    end
                    return false
                end},
            },
        },
        {--14
            Name = "Learn Taming",
            Instructions = "Speak to a [F2F5A9]stable master[-].",
            EndDialogue = {"[Tamer Intro] *sobs*","Oh my... what happened? They usually last longer than that. Well, this is only the beginning of a long journey. Taming can be one of the most rewarding and powerful professions. Be patient and adventurous and I'm sure you will succeed. Go tame those critters!"},
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            Goals = {
                {"TurnIn"},
            },
        },
    },
    TamerProfessionTierOne = {
        {--1
            Name = "Friend or Fauna",
            Instructions = "Speak to a [F2F5A9]stable master[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            NextStep = 2,
            EndDialogue = {"[Tamer I] I seek guidance.","There are many creatures that roam Celador. Make it your goal to seek out and tame them all. This will get you started with the ones which are easier to tame."},
            Requirements = {
                {"MaxTimesQuestDone", "TamerProfessionTierOne", 1},
                --{"QuestActive", "TamerProfessionTierOne", false},
                {"MinTimesQuestDone", "TamerProfessionIntro", 1},
                {"Skill", "AnimalTamingSkill", 30},
                {"Skill", "AnimalLoreSkill", 30},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Friend or Fauna",
            Instructions = "Tame a [F2F5A9]Chicken[-].",
            NextStep = 3,
            StartDialogue = {"[Tamer I] I seek guidance.","There are many creatures that roam Celador. Make it your goal to seek out and tame them all. This will get you started with the ones which are easier to tame."},
            Requirements = {
                {"MaxTimesQuestDone", "TamerProfessionTierOne", 1},
                {"QuestActive", "TamerProfessionTierOne", false},
                {"MinTimesQuestDone", "TamerProfessionIntro", 1},
                {"Skill", "AnimalTamingSkill", 30},
                {"Skill", "AnimalLoreSkill", 30},
            },
            Goals = {
                {"Tamed","chicken"},
            },
        },
        {--3
            Name = "Friend or Fauna",
            Instructions = "Tame a [F2F5A9]Turkey[-].",
            NextStep = 5,
            Goals = {
                {"Tamed","turkey"},
            },
        },
        {--4 skip
            Name = "Friend or Fauna",
            Instructions = "Tame a [F2F5A9]Bat[-].",
            NextStep = 5,
            Goals = {
                {"Tamed","bat"},
            },
        },
        {--5
            Name = "Friend or Fauna",
            Instructions = "Tame a [F2F5A9]Giant Rat[-].",
            NextStep = 6,
            Goals = {
                {"Tamed","rat_giant"},
            },
        },
        {--6
            Name = "Friend or Fauna",
            Instructions = "Tame a [F2F5A9]Fox[-].",
            NextStep = 7,
            Goals = {
                {"Tamed","fox"},
            },
        },
        {--7
            Name = "Friend or Fauna",
            Instructions = "Tame a [F2F5A9]Hind[-].",
            NextStep = 8,
            Goals = {
                {"Tamed","hind"},
            },
        },
        {--8
            Name = "Friend or Fauna",
            Instructions = "Tame a [F2F5A9]Wolf[-].",
            NextStep = 9,
            Goals = {
                {"Tamed","wolf"},
            },
        },
        {--9
            Name = "Friend or Fauna",
            Instructions = "Tame a [F2F5A9]Horse[-].",
            NextStep = 10,
            Goals = {
                {"Custom", function(playerObj)
                    local pets = GetActivePets(playerObj)
                    for pet = 1, #pets do
                        local template = pets[pet]:GetCreationTemplateId()
                        if ( template == "horse" or template == "chestnut_horse" or template == "bay_horse" ) then
                            local owners = pets[pet]:GetObjVar("PreviousOwners")
                            if ( owners and next(owners) ) then
                                for owner = 1, #owners do
                                    if ( GameObj(tonumber(owners[owner])) ~= playerObj ) then
                                        return false
                                    end
                                end
                            end
                            return true
                        end
                    end
                    return false
                end},
            },
        },
        {--10
            Name = "Friend or Fauna",
            Instructions = "Speak to a [F2F5A9]stable master[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            EndDialogue = {"[Tamer I] I tamed many critters!","This is only the beginning of your journey! You must hone your skills to be able to control the best creatures. Some of them are really powerful!"},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    TamerProfessionTierTwo = {
        {--1
            Name = "Bears, Oh My!",
            Instructions = "Speak to a stable master.",
            MarkerLocs = {
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            NextStep = 2,
            EndDialogue = {"[Tamer II] What other creatures can I tame?","Bears, scorpions, and crabs, oh my! Congratulations for sticking with it. Go and see if you can find these creatures."},
            Requirements = {
                {"MaxTimesQuestDone", "TamerProfessionTierTwo", 1},
                --{"QuestActive", "TamerProfessionTierTwo", false},
                {"MinTimesQuestDone", "TamerProfessionTierOne", 1},
                {"Skill", "AnimalTamingSkill", 50},
                {"Skill", "AnimalLoreSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Bears, Oh My!",
            Instructions = "Tame a [F2F5A9]crab[-].",
            NextStep = 3,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 50},
                {"Skill", "AnimalLoreSkill", 50},
            },
            QuestData = {
                Key = "TamerProfessionTierTwo2",
                Type = "Taming",
                QuestArgs = 
                {
                    "crab"
                },
                Count = 1,
                Name = "Crab",
                NamePlural = "Crabs",
            },
        },
        { -- 3
            Name = "Bears, Oh My!",
            Instructions = "Tame a [F2F5A9]black bear[-].",
            NextStep = 4,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 50},
                {"Skill", "AnimalLoreSkill", 50},
            },
            QuestData = {
                Key = "TamerProfessionTierTwo3",
                Type = "Taming",
                QuestArgs = 
                {
                    "black_bear"
                },
                Count = 1,
                Name = "Black Bear",
                NamePlural = "Black Bears",
            },
        },
        { -- 4
            Name = "Bears, Oh My!",
            Instructions = "Tame a [F2F5A9]brown bear[-].",
            NextStep = 5,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 50},
                {"Skill", "AnimalLoreSkill", 50},
            },
            QuestData = {
                Key = "TamerProfessionTierTwo4",
                Type = "Taming",
                QuestArgs = 
                {
                    "brown_bear"
                },
                Count = 1,
                Name = "Brown Bear",
                NamePlural = "Brown Bears",
            },
        },
        { -- 5
            Name = "Bears, Oh My!",
            Instructions = "Tame a [F2F5A9]giant scorpion[-].",
            NextStep = 6,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 50},
                {"Skill", "AnimalLoreSkill", 50},
            },
            QuestData = {
                Key = "TamerProfessionTierTwo5",
                Type = "Taming",
                QuestArgs = 
                {
                    "scorpion"
                },
                Count = 1,
                Name = "Giant Scorpion",
                NamePlural = "Giant Scorpions",
            },
        },
        {--6
            Name = "Bears, Oh My!",
            Instructions = "Speak to a stable master.",
            Requirements = {
                {"Skill", "AnimalTamingSkill", 50},
                {"Skill", "AnimalLoreSkill", 50},
            },
            MarkerLocs = {
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            EndDialogue = {"[Tamer II] That was easy.","Well, there's more where that came from. The higher your skill, the more dangerous the creatures you will encounter in your journey to improve your talents."},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    TamerProfessionTierThree = {
        {--1
            Name = "Dire Situations",
            Instructions = "Speak to a stable master.",
            MarkerLocs = {
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            NextStep = 2,
            EndDialogue = {"[Tamer III] I want to tame them all.","This one's a doosey. Here is a list that should last you a while, and take you to some interesting places. You may notice your creatures are becoming quite a bit stronger!"},
            Requirements = {
                {"MaxTimesQuestDone", "TamerProfessionTierThree", 1},
                --{"QuestActive", "TamerProfessionTierThree", false},
                {"MinTimesQuestDone", "TamerProfessionTierTwo", 1},
                {"Skill", "AnimalTamingSkill", 80},
                {"Skill", "AnimalLoreSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Dire Situations",
            Instructions = "Tame a [F2F5A9]black wolf[-].",
            NextStep = 3,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 80},
                {"Skill", "AnimalLoreSkill", 80},
            },
            QuestData = {
                Key = "TamerProfessionTierThree2",
                Type = "Taming",
                QuestArgs = 
                {
                    "wolf_black"
                },
                Count = 1,
                Name = "Black Wolf",
                NamePlural = "Black Wolves",
            },
        },
        { -- 3
            Name = "Dire Situations",
            Instructions = "Tame a [F2F5A9]desert wolf[-].",
            NextStep = 4,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 80},
                {"Skill", "AnimalLoreSkill", 80},
            },
            QuestData = {
                Key = "TamerProfessionTierThree3",
                Type = "Taming",
                QuestArgs = 
                {
                    "wolf_desert"
                },
                Count = 1,
                Name = "Desert Wolf",
                NamePlural = "Desert Wolves",
            },
        },
        { -- 4
            Name = "Dire Situations",
            Instructions = "Tame a [F2F5A9]grey wolf[-].",
            NextStep = 5,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 80},
                {"Skill", "AnimalLoreSkill", 80},
            },
            QuestData = {
                Key = "TamerProfessionTierThree4",
                Type = "Taming",
                QuestArgs = 
                {
                    "wolf_grey"
                },
                Count = 1,
                Name = "Grey Wolf",
                NamePlural = "Grey Wolves",
            },
        },
        { -- 5
            Name = "Dire Situations",
            Instructions = "Tame a [F2F5A9]arctic wolf[-].",
            NextStep = 6,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 80},
                {"Skill", "AnimalLoreSkill", 80},
            },
            QuestData = {
                Key = "TamerProfessionTierThree5",
                Type = "Taming",
                QuestArgs = 
                {
                    "arctic_wolf"
                },
                Count = 1,
                Name = "Arctic Wolf",
                NamePlural = "Arctic Wolves",
            },
        },
        { -- 6
            Name = "Dire Situations",
            Instructions = "Tame a [F2F5A9]dire wolf[-].",
            NextStep = 7,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 80},
                {"Skill", "AnimalLoreSkill", 80},
            },
            QuestData = {
                Key = "TamerProfessionTierThree6",
                Type = "Taming",
                QuestArgs = 
                {
                    "dire_wolf"
                },
                Count = 1,
                Name = "Dire Wolf",
                NamePlural = "Dire Wolves",
            },
        },
        {--7
            Name = "Dire Situations",
            Instructions = "Speak to a stable master.",
            MarkerLocs = {
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            EndDialogue = {"[Tamer III] So many wolves!","Yes, I know. Even bigger beasties are just around the corner. I hope you're ready."},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    TamerProfessionTierFour = {
        {--1
            Name = "Deadly Commands",
            Instructions = "Speak to a stable master.",
            MarkerLocs = {
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            NextStep = 2,
            EndDialogue = {"[Tamer IV] I finally did it.","Congratulations! You are clearly an incredibly accomplished tamer. Tame these remaining creatures to prove your skill."},
            Requirements = {
                {"MaxTimesQuestDone", "TamerProfessionTierFour", 1},
                --{"QuestActive", "TamerProfessionTierFour", false},
                {"MinTimesQuestDone", "TamerProfessionTierThree", 1},
                {"Skill", "AnimalTamingSkill", 100},
                {"Skill", "AnimalLoreSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        { -- 2
            Name = "Deadly Commands",
            Instructions = "Tame a [F2F5A9]huntress spider[-].",
            NextStep = 3,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 100},
                {"Skill", "AnimalLoreSkill", 100},
            },
            QuestData = {
                Key = "TamerProfessionTierFour2",
                Type = "Taming",
                QuestArgs = 
                {
                    "spider_huntress"
                },
                Count = 1,
                Name = "Huntress Spider",
                NamePlural = "Huntress Spiders",
            },
        },
        { -- 3
            Name = "Deadly Commands",
            Instructions = "Tame a [F2F5A9]dire bear[-].",
            NextStep = 4,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 100},
                {"Skill", "AnimalLoreSkill", 100},
            },
            QuestData = {
                Key = "TamerProfessionTierFour3",
                Type = "Taming",
                QuestArgs = 
                {
                    "dire_bear"
                },
                Count = 1,
                Name = "Dire Bear",
                NamePlural = "Dire Bears",
            },
        },
        { -- 4
            Name = "Deadly Commands",
            Instructions = "Tame a [F2F5A9]wyvern[-].",
            NextStep = 5,
            Requirements = {
                {"Skill", "AnimalTamingSkill", 100},
                {"Skill", "AnimalLoreSkill", 100},
            },
            QuestData = {
                Key = "TamerProfessionTierFour4",
                Type = "Taming",
                QuestArgs = 
                {
                    "wyvern"
                },
                Count = 1,
                Name = "Wyvern",
                NamePlural = "Wyverns",
            },
        },
        {--5
            Name = "Deadly Commands",
            Instructions = "Speak to a stable master.",
            MarkerLocs = {
                QuestMarkers.StablePyros,
                QuestMarkers.StableValus,
                QuestMarkers.StableEldeir,
                QuestMarkers.StableHelm,
                QuestMarkers.StableBlackForest,
            },
            EndDialogue = {"[Tamer IV] I am the master now.","Look at you! Now you can enjoy the hard earned benefits of commanding some of the toughest creatures of Celador. Remember to treat your pets well."},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    BardProfessionIntro = {
        {--1
            Name = "Learn Musicianship",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 2,
            EndDialogue = {"[Bard Intro] Can you teach me to play?","I would be happy to! First you will need to equip an instrument."},
            Requirements = {
                {"MaxTimesQuestDone", "BardProfessionIntro", 1},
                --{"QuestActive", "BardProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn Musicianship",
            Instructions = "Equip an [F2F5A9]instrument[-] by double clicking it in your backpack or dragging it onto yourself in the character window.",
            NextStep = 3,
            StartDialogue = {"[Bard Intro] Can you teach me to play?","I would be happy to! First you will need to equip an instrument."},
            Requirements = {
                {"MaxTimesQuestDone", "BardProfessionIntro", 1},
                {"QuestActive", "BardProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill", "MusicianshipSkill", 30},
                {"SetSkill", "EntertainmentSkill", 30},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local handObjects = {}
                    table.insert(handObjects, playerObj:GetEquippedObject("LeftHand"))
                    table.insert(handObjects, playerObj:GetEquippedObject("RightHand"))
                    local instruments = 0
                    for i = 1, #handObjects do
                        local toolType = handObjects[i]:GetObjVar("ToolType")
                        if ( toolType and toolType == "Instrument" ) then
                            instruments = 1
                        end
                    end
                    if ( instruments == 0 ) then return false end
                    return true
                end},
            },
        },
        {--3
            Name = "Learn Musicianship",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 4,
            EndDialogue = {"[Bard Intro] How do I use this thing?","Bards can strengthen their allies, and weaken their foes. The most basic job of a bard, however is to play sweet music. You can do this by standing next to an inn hearth. Solos will revitalize the vitality of listeners!"},
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Learn Musicianship",
            Instructions = "Press [F2F5A9]Q[-] to begin playing your instrument.",
            NextStep = 5,
            Goals = {
                {"HasMobileEffect","Play",true},
            },
        },
        {--5
            Name = "Learn Musicianship",
            Instructions = "Press [F2F5A9]E[-] to do a solo while playing. Solos revitalize the vitality of listening players.",
            NextStep = 6,
            Goals = {
                {"HasMobileEffect","Solo",true},
            },
        },
        {--6
            Name = "Learn Musicianship",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 7,
            EndDialogue = {"[Bard Intro] I'm no good at this.","Of course you aren't! Nobody just picks up an instrument and can immediately play it well. You need to practice! You may consider doing it somewhere alone for a while, though... no offense. Musicianship goes beyond simple tunes. Speak to me once you have obtained a songbook."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--7
            Name = "Learn Musicianship",
            Instructions = "Get a [F2F5A9]Songbook[-].",
            NextStep = 8,
            Goals = {
                {"HasItemInBackpack","songbook"},
            },
        },
        {--8
            Name = "Learn Musicianship",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 9,
            EndDialogue = {"[Bard Intro] I have a songbook, now what?","Play a song, of course. Songs are powerful tools that can fill the area with emotion, affecting those around you. You can provide powerful boons to your allies and change the tide of battles. Try one out."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--9
            Name = "Learn Musicianship",
            Instructions = "Play [F2F5A9]Song of Thew[-] from your [F2F5A9]Songbook[-]. Right click your songbook in your inventory and open it to view the pages.",
            NextStep = 10,
            Goals = {
                {"HasMobileEffect","SongAOE"},
            },
        },
        {--10
            Name = "Learn Musicianship",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            Instructions = "Speak to a [F2F5A9]bard[-].",
            EndDialogue = {"[Bard Intro] What is this feeling?","That's the power of the bard! Bards affect those around them more than any other profession! Their music is not all happy and kind, either. Go forth and bring sound to Celador."},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    BardProfessionTierOne = {
        {--1
            Name = "Triple Fret",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 2,
            EndDialogue = {"[Bard I] I am improving already.","You will continue to, as long as you practice. What use is music if you can't share it with others? Music can rejuvenate and destroy. A really nice solo can fill listeners with vitality. Persuade 3 adventurers to listen to you play at once. I don't care how you do it. Bribe them if you have to."},
            Requirements = {
                {"MaxTimesQuestDone", "BardProfessionTierOne", 1},
                --{"QuestActive", "BardProfessionTierOne", false},
                {"MinTimesQuestDone", "BardProfessionIntro", 1},
                {"Skill", "MusicianshipSkill", 30},
                {"Skill", "EntertainmentSkill", 30},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Triple Fret",
            Instructions = "Get 2 adventurers to [F2F5A9]listen to you play[-] at once by right clicking you while you are playing and selecting listen. Make them dance!",
            StartDialogue = {"I am improving already.","You will continue to, as long as you practice. What use is music if you can't share it with others? Music can rejuvenate and destroy. A really nice solo can fill listeners with vitality. Persuade 3 adventurers to listen to you play at once. I don't care how you do it. Bribe them if you have to."},
            NextStep = 3,
            Requirements = {
                {"MaxTimesQuestDone", "BardProfessionTierOne", 1},
                {"QuestActive", "BardProfessionTierOne", false},
                {"MinTimesQuestDone", "BardProfessionIntro", 1},
                {"Skill", "MusicianshipSkill", 30},
                {"Skill", "EntertainmentSkill", 30},
            },
            Goals = {
                {"Custom", function(playerObj)
                    local mobiles = FindObjects(SearchMulti(
                    {
                        SearchPlayerInUpdateRange(true),
                        SearchHasObjVar("ListeningToBandID"),
                    }))
                    if ( mobiles and #mobiles >= 2 ) then return true else return false end
                end},
            },
        },
        {--3
            Name = "Triple Fret",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            EndDialogue = {"[Bard I] Some people will dance to anything.","Tell me about it! Mix in some alcohol for greatest effect. How else do you think a guy who looks like me got a wife?"},
            Goals = {
                {"TurnIn"},
            },
        },
    },
    BardProfessionTierTwo = {

        {--1
            Name = "Journey to Journeyman",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 2,
            EndDialogue = {"[Bard II] I am ready to improve.","Well then pack up your instruments. A bard is only as good as the stories he can tell. Go and visit Yewell Steeltounge outside Iris Cave."},
            Requirements = {
                {"MaxTimesQuestDone", "BardProfessionTierTwo", 1},
                {"MinTimesQuestDone", "BardProfessionTierOne", 1},
                {"Skill", "MusicianshipSkill", 50},
                {"Skill", "EntertainmentSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        {--2
            Name = "Journey to Journeyman",
            Instructions = "Listen to [F2F5A9]Yewell Steeltounge's[-] story.",
            MarkerLocs = {
                QuestMarkers.YewellSteeltounge,
            },
            NextStep = 3,
            EndDialogue = {"[Bard II] I was sent here for a story.","You want a tale do you? I've got plenty of those...\nA once I was diggin for rocks,\nwith naught on but my socks!\nAlong came a bandit a creepin,\nere not suspect'n what he be peepin!\nFor each rock that I sent slinging,\nweren't just my pick that was swinging."},
            Requirements = {
                {"Skill", "MusicianshipSkill", 50},
                {"Skill", "EntertainmentSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            }, 
        },

        {--3
            Name = "Journey to Journeyman",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 4,
            EndDialogue = {"[Bard II] I don't think nudist miner stories will cut it.","Hah, he told you the one about swinging, huh? Oh geez that one gets me every time. Fine fine, I'll tell you what. In the northern reaches of the Barren Lands is an infestation of reptiles. Venture there and help eradicate their nest."},
            Requirements = {
                {"Skill", "MusicianshipSkill", 50},
                {"Skill", "EntertainmentSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        {--4
            Name = "Journey to Journeyman",
            Instructions = "Discover the [F2F5A9]dragon awakening[-] in the Barren Lands.",
            NextStep = 5,
            Requirements = {
                {"Skill", "MusicianshipSkill", 50},
                {"Skill", "EntertainmentSkill", 50},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( playerObj:IsInRegion("AwakeningDragon") ) then
                        return true
                    end
                end},
            },
        },

        { -- 5
            Name = "Journey to Journeyman",
            NextStep = 6,
            Requirements = {
                {"Skill", "MusicianshipSkill", 50},
                {"Skill", "EntertainmentSkill", 50},
            },
            QuestData = {
                Key = "BardProfessionTierTwo5",
                Type = "Hunting",
                Region = "AwakeningDragon",
                QuestArgs = 
                {
                    "awakening_dragon_boss",
                    true
                },
                Count = 1,
                Name = "Vazguhn the Ancient",
                NamePlural = "Vazguhn the Ancient",
            },
            Instructions = "Kill [F2F5A9]Vazguhn the Ancient[-] in the Barren Lands.",
        },

        {--6
            Name = "Journey to Journeyman",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 7,
            EndDialogue = {"[Bard II] I helped slay Vazguhn.","Excellent, visit Yewell Steeltounge again, he has something for you."},
            Requirements = {
                {"Skill", "MusicianshipSkill", 50},
                {"Skill", "EntertainmentSkill", 50},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        {--7
            Name = "Journey to Journeyman",
            Instructions = "Speak to [F2F5A9]Yewell Steeltounge[-].",
            MarkerLocs = {
                {-1191, 0, -2103}
            },
            EndDialogue = {"[Bard II] You have something for me?","That I do, I hear you are making some stories of your own. Take this tome to help you on your journey."},
            NextStep = 8,
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item","prestige_journeyman_bard"},
                },
            },
        },

        {--8
            Name = "Journey to Journeyman",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            EndDialogue = {"[Bard II] I have the journeyman tome.","Very well, in exchange for it I will grant you the rank of Journeyman Bard."},
            Requirements = {
                {"Skill", "MusicianshipSkill", 50},
                {"Skill", "EntertainmentSkill", 50},
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_journeyman_bard"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Journeyman Bard abilities.","info")
                    end, "Journeyman Bard Abilities"},
                },                
            },
        },

    },
    BardProfessionTierThree = {
        {--1
            Name = "A Master's Tale",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 2,
            EndDialogue = {"[Bard III] I'm ready for another journey.","That is well, you will need to visit Kato the Fighter Guildmaster. He can usually be found at the arena just outside of Eldier."},
            Requirements = {
                {"MaxTimesQuestDone", "BardProfessionTierThree", 1},
                --{"QuestActive", "BardProfessionTierThree", false},
                {"MinTimesQuestDone", "BardProfessionTierTwo", 1},
                {"Skill", "MusicianshipSkill", 80},
                {"Skill", "EntertainmentSkill", 80},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        {--2
            Name = "A Master's Tale",
            Instructions = "Listen to [F2F5A9]Kato's[-] story.",
            MarkerLocs = {
                {-1191, 0, -2103}
            },
            NextStep = 3,
            EndDialogue = {"[Bard III] I was sent here for a story.","I'm not much on story-telling, but I'll see what I can do.\nI've fought in countless a battle,\nand took blows that made my teeth rattle.\nWith each clash of the sword and mace,\nI have grown and earned my place.\nYet the fighting is far from done,\nfor this war can never be won."},
            Requirements = {
                {"Skill", "MusicianshipSkill", 80},
                {"Skill", "EntertainmentSkill", 80},
                {"StepMustBeActive", "BardProfessionTierTwo", 2},
            },
            Goals = {
                {"TurnIn"},
            }, 
        },

        {--3
            Name = "A Master's Tale",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 4,
            EndDialogue = {"[Bard III] Remind me not to become a fighter.","It truly is a challenging profession. But the role of a bard is to brighten the spirits of those around them and to aid in the weariness of battle. With that in mind you are to travel once more to the Barren Lands and help fight the Cultist King."},
            Requirements = {
                {"Skill", "MusicianshipSkill", 80},
                {"Skill", "EntertainmentSkill", 80},
                {"StepMustBeActive", "BardProfessionTierThree", 3},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        {--4
            Name = "A Master's Tale",
            Instructions = "Discover the [F2F5A9]cultist ruins awakening[-] in the Barren Lands.",
            NextStep = 5,
            Requirements = {
                {"Skill", "MusicianshipSkill", 80},
                {"Skill", "EntertainmentSkill", 80},
                {"StepMustBeActive", "BardProfessionTierThree", 4},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( playerObj:IsInRegion("Area-Ruin of the Artificers") ) then
                        return true
                    end
                end},
            },
        },

        {--5
            Name = "A Master's Tale",
            Instructions = "Obtain a [F2F5A9]Tome: Master Bard[-] from killing the Cultist King.",
            NextStep = 6,
            Requirements = {
                {"Skill", "MusicianshipSkill", 80},
                {"Skill", "EntertainmentSkill", 80},
            },
            Goals = {
                {"HasItem", "prestige_master_bard"},
            },
        },

        {--6
            Name = "A Master's Tale",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            EndDialogue = {"[Bard III] I have the master tome.","You have done well, I grant you the title of Master Bard."},
            Requirements = {
                {"Skill", "MusicianshipSkill", 80},
                {"Skill", "EntertainmentSkill", 80},
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_master_bard"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Master Bard abilities.","info")
                    end, "Master Bard Abilities"},
                },                
            },
        },
    },

    BardProfessionTierFour = {
        {--1
            Name = "Darkness Uncovered",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 2,
            EndDialogue = {"[Bard IV] I wish to confirm my grandmaster status.","You have come a long way. I would have you seek out the Mage Grandmaster in Oasis to begin this last step in your journey."},
            Requirements = {
                {"MaxTimesQuestDone", "BardProfessionTierFour", 1},
                --{"QuestActive", "BardProfessionTierFour", false},
                {"MinTimesQuestDone", "BardProfessionTierThree", 1},
                {"Skill", "MusicianshipSkill", 100},
                {"Skill", "EntertainmentSkill", 100},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        {--2
            Name = "Darkness Uncovered",
            Instructions = "Listen to [F2F5A9]Magica's[-] story.",
            MarkerLocs = {
                {-1191, 0, -2103}
            },
            NextStep = 3,
            EndDialogue = {"[Bard IV] Do you have a story to tell?","A story? Yes, I have one to share.\nThis world is full of wonderous things,\nfrom trees that can walk to orcs that sting.\nBut of all these things I have a seen,\nwalking trees made my stomach turn green.\nFor though most trees can rock and sway,\nat least you can still get away."},
            Requirements = {
                {"Skill", "MusicianshipSkill", 100},
                {"Skill", "EntertainmentSkill", 100},
                {"StepMustBeActive", "BardProfessionTierFour", 2},
            },
            Goals = {
                {"TurnIn"},
            }, 
        },

        {--3
            Name = "Darkness Uncovered",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            NextStep = 4,
            EndDialogue = {"[Bard III] Please don't tell me I have to fight trees.","Not trees, just one tree! Specifically Lord Barkas who resides in swamp of the Black Forest."},
            Requirements = {
                {"Skill", "MusicianshipSkill", 100},
                {"Skill", "EntertainmentSkill", 100},
                {"StepMustBeActive", "BardProfessionTierFour", 3},
            },
            Goals = {
                {"TurnIn"},
            },
        },

        {--4
            Name = "Darkness Uncovered",
            Instructions = "Discover the [F2F5A9]ent awakening[-] in the Black Forest.",
            NextStep = 5,
            Requirements = {
                {"Skill", "MusicianshipSkill", 100},
                {"Skill", "EntertainmentSkill", 100},
                {"StepMustBeActive", "BardProfessionTierFour", 4},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( playerObj:IsInRegion("GreatTree") ) then
                        return true
                    end
                end},
            },
        },

        {--5
            Name = "Darkness Uncovered",
            Instructions = "Obtain a [F2F5A9]Tome: Grandmaster Bard[-] from killing Lord Barkas.",
            NextStep = 6,
            Requirements = {
                {"Skill", "MusicianshipSkill", 100},
                {"Skill", "EntertainmentSkill", 100},
            },
            Goals = {
                {"HasItem", "prestige_grandmaster_bard"},
            },
        },

        {--6
            Name = "Darkness Uncovered",
            Instructions = "Speak to a [F2F5A9]bard[-].",
            MarkerLocs = {
                QuestMarkers.BardPyros,
                QuestMarkers.BardValus,
                QuestMarkers.BardHelm,
                QuestMarkers.BardEldeir,
            },
            EndDialogue = {"[Bard IV] I have the grandmaster tome.","Congratulations grandmaster."},
            Requirements = {
                {"Skill", "MusicianshipSkill", 100},
                {"Skill", "EntertainmentSkill", 100},
            },
            Goals = {
                {"TurnIn"},
                {"TakeItem", "prestige_grandmaster_bard"},
            },
            Rewards = {
                {--choice 1
                    {"Custom", function(playerObj)
                        playerObj:SystemMessage("You are now able to train Grandmaster Bard abilities.","info")
                    end, "Grandmaster Bard Abilities"},
                },                
            },
        },
    },
    HealerProfessionIntro = {
        {--1
            Name = "Learn to Bandage",
            Instructions = "Speak to a [F2F5A9]healer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.HealerPyros,
                QuestMarkers.HealerHelm,
                QuestMarkers.HealerEldeir,
                QuestMarkers.HealerValus,
            },
            NextStep = 2,
            EndDialogue = {"Teach me to bandage.","Sure. Bring me a bandage and I will show you how to use it."},
            Requirements = {
                {"MaxTimesQuestDone", "HealerProfessionIntro", 1},
                --{"QuestActive", "HealerProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn to Bandage",
            Instructions = "Get a [F2F5A9]Bandage[-]. You may buy them from a merchant.",
            NextStep = 3,
            StartDialogue = {"Teach me to bandage.","Sure. Bring me a bandage and I will show you how to use it."},
            Requirements = {
                {"MaxTimesQuestDone", "HealerProfessionIntro", 1},
                {"QuestActive", "HealerProfessionIntro", false},
            },
            OnStart = {
                {"SetSkill", "HealingSkill", 30},
                {"SetSkill", "MeleeSkill", 30},
            },
            Goals = {
                {"HasItemInBackpack", "bandage"},
            },
        },
        {--3
            Name = "Learn to Bandage",
            Instructions = "Speak to a [F2F5A9]healer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.HealerPyros,
                QuestMarkers.HealerHelm,
                QuestMarkers.HealerEldeir,
                QuestMarkers.HealerValus,
            },
            NextStep = 4,
            EndDialogue = {"I have a bandage.","Use the bandage from your backpack, and then choose your patient. You must be standing closeby."},
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Learn to Bandage",
            Instructions = "Bandage a patient. Double click your [F2F5A9]Bandage(s)[-] in your backpack and [F2F5A9]target yourself or a closeby patient[-].",
            NextStep = 5,
            OnStart = {
                {"Custom", function(playerObj)
                    SetCurHealth(playerObj,math.min(GetCurHealth(playerObj),GetMaxHealth(playerObj)/2))
                end},
            },
            Goals = {
                {"HasMobileEffect", "NoBandage", true},
            },
        },
        {--5
            Name = "Learn to Bandage",
            Instructions = "Speak to a [F2F5A9]healer[-].",
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperBarrenLands,
                QuestMarkers.HealerPyros,
                QuestMarkers.HealerHelm,
                QuestMarkers.HealerEldeir,
                QuestMarkers.HealerValus,
            },
            EndDialogue = {"I bandaged my patient!","Well done! Keep in mind that bandages can only be used every so often. Bandages can cure poisons and even resurrect with enough healing skill. Here are some bandages to get you started."},
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item", "bandage", 150},
                },
            },
        },
    },
    HealerProfessionTierOne = {
        {--1
            Name = "Healer Part 1",
            Instructions = "Obtain the required skill.",
            Requirements = {
                {"Disabled"},
                {"MaxTimesQuestDone", "HealerProfessionTierOne", 1},
                --{"QuestActive", "HealerProfessionTierOne", false},
                {"MinTimesQuestDone", "HealerProfessionIntro", 1},
                {"Skill", "HealingSkill", 30},
                {"Skill", "MeleeSkill", 30},
            },
        },
    },
    HealerProfessionTierTwo = {
        {--1
            Name = "Healer Part 2",
            Instructions = "Obtain the required skill.",
            Requirements = {
                {"Disabled"},
                {"MaxTimesQuestDone", "HealerProfessionTierTwo", 1},
                --{"QuestActive", "HealerProfessionTierTwo", false},
                {"MinTimesQuestDone", "HealerProfessionTierOne", 1},
                {"Skill", "HealingSkill", 50},
                {"Skill", "MeleeSkill", 50},
            },
        },
    },
    HealerProfessionTierThree = {
        {--1
            Name = "Healer Part 3",
            Instructions = "Obtain the required skill.",
            Requirements = {
                {"Disabled"},
                {"MaxTimesQuestDone", "HealerProfessionTierThree", 1},
                --{"QuestActive", "HealerProfessionTierThree", false},
                {"MinTimesQuestDone", "HealerProfessionTierTwo", 1},
                {"Skill", "HealingSkill", 80},
                {"Skill", "MeleeSkill", 80},
            },
        },
    },
    HealerProfessionTierFour = {
        {--1
            Name = "Healer Part 4",
            Instructions = "Obtain the required skill.",
            Requirements = {
                {"Disabled"},
                {"MaxTimesQuestDone", "HealerProfessionTierFour", 1},
                --{"QuestActive", "HealerProfessionTierFour", false},
                {"MinTimesQuestDone", "HealerProfessionTierThree", 1},
                {"Skill", "HealingSkill", 100},
                {"Skill", "MeleeSkill", 100},
            },
        },
    },
    --Non-profession tutorials
    Skinning = {
        {--1
            Name = "Fuzzy Wuzzy",
            Instructions = "Speak to a [F2F5A9]chef or tailor[-] to learn how to skin dead creatures.",
            NextStep = 2,
            EndDialogue = {"How do I skin animals?","With a knife, obviously. Talk to me again when you have one. You can buy them from tinkerers."},
            Requirements = {
                {"MaxTimesQuestDone", "Skinning", 1},
                --{"QuestActive", "Skinning", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Fuzzy Wuzzy",
            Instructions = "Get a [F2F5A9]Hunting Knife[-]. You can buy them from tinkerers.",
            NextStep = 3,
            StartDialogue = {"How do I skin animals?","With a knife, obviously. Talk to me again when you have one. You can buy them from tinkerers."},
            Requirements = {
                {"MaxTimesQuestDone", "Skinning", 1},
                {"QuestActive", "Skinning", false},
            },
            Goals = {
                {"HasItemInBackpack", "tool_hunting_knife"},
            },
        },
        {--3
            Name = "Fuzzy Wuzzy",
            Instructions = "Speak to a [F2F5A9]tailor or chef[-].",
            NextStep = 4,
            EndDialogue = {"I have a knife!","That's not a knife! Well, I suppose it is, technically. Use this end on the critters. I have found all sorts of things on dead dinguses. Where do you think I got my underwear?"},
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Fuzzy Wuzzy",
            Instructions = "[F2F5A9]Skin a creature[-] by double clicking its corpse with a [F2F5A9]Hunting Knife[-] in your inventory.",
            Goals = {
                {"Custom", function(playerObj)
                    local skinned = FindObjects(SearchMulti(
                    {
                        SearchObjectInRange(10),
                        SearchObjVar("HarvestToolType", "Knife"),
                    }))
                    for mob = 1, #skinned do
                        local tag = skinned[mob]:GetObjVar("Tag")
                        if ( tag and next(tag) ) then
                            for looter, bool in pairs(tag) do
                                if ( looter == playerObj ) then
                                    local handlesHarvest = skinned[mob]:GetObjVar("HandlesHarvest")
                                    if ( not handlesHarvest and skinned[mob]:HasObjVar("AnimalParts") ) then
                                        return true
                                    end
                                end
                            end
                        end
                    end
                    return false
                end},
            },
        },
    },
    --Hand-out quests
    InitiateFreeHorseStatue = {
        {--1
            Name = "Free Horse",
            Instructions = "Get a free horse from the stable master.",
            Silent = true,
            StartDialogue = {"I wish I could afford a horse.","No adventurer new to Celador should have to go without a steed. Take this one!"},
            Requirements = {
                {"Custom", function(playerObj)
                    if ( playerObj:HasModule("npe_player") )
                    then return true else return false end
                end},
                {"MaxTimesQuestDone", "InitiateFreeHorseStatue", 1},
                --{"QuestActive", "InitiateFreeHorseStatue", false},
            },
            OnEnd = {
                {"GiveItem","item_statue_mount_horse"},
                {"Custom", function(playerObj)
                    playerObj:SystemMessage("Received a free horse statue!")
                    playerObj:SystemMessage("Received a free horse statue!", "info")
                end},
            }
        },
    },
    --[[ ManifesterProfessionIntro = {
        {--1
            Name = "Learn Manifestation",
            Instructions = "Speak to a mage trainer.",
            NextStep = 2,
            EndDialogue = {"Can you teach me magic?","I can teach you once you have gathered the required materials. First, obtain a spellbook. A spellbook keeps all of a mage's known spells safely within, and stays with the mage even through death."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Requirements = {
                {"Disabled"},
                {"MaxTimesQuestDone", "ManifesterProfessionIntro", 1},
                {"QuestActive", "ManifesterProfessionIntro", false},
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--2
            Name = "Learn Manifestation",
            Instructions = "Get a spellbook. You can buy them from scribes.",
            NextStep = 3,
            StartDialogue = {"Can you teach me magic?","I can teach you once you have gathered the required materials. First, obtain a spellbook. A spellbook keeps all of a mage's known spells safely within, and stays with the mage even through death."},
            Requirements = {
                {"Disabled"},
                {"MaxTimesQuestDone", "ManifesterProfessionIntro", 1},
                {"QuestActive", "ManifesterProfessionIntro", false},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if ( CountItemsInContainer(playerObj, "spellbook")
                    or CountItemsInContainer(playerObj, "spellbook_noob")
                    or CountItemsInContainer(playerObj, "spellbook_full") ) then
                        return true
                    end
                    return false
                end},
            },
        },
        {--3
            Name = "Learn Manifestation",
            Instructions = "Speak to a mage trainer.",
            NextStep = 4,
            EndDialogue = {"I have a spellbook.","It looks a bit light, but you can fill it with spell scrolls over time. Open it up and take a look at which spells are inside. When you are finished, bring me 10 of the reagents needed for Heal. They are sold by alchemists."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--4
            Name = "Learn Manifestation",
            Instructions = "Get 10 Heal reagents.",
            NextStep = 5,
            Goals = {
                {"HasItem","ingredient_ginsengroot"},
            },
        },
        {--5
            Name = "Learn Manifestation",
            Instructions = "Speak to a mage trainer.",
            NextStep = 6,
            EndDialogue = {"I come bearing ginseng.","You finally have all you need to cast a spell. Go into your spellbook and retrieve the Heal spell. Make sure you can easily access it. Use the spell to begin casting. Then unleash it on one of these chickens."},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Goals = {
                {"TurnIn"},
            },
        },
        {--6
            Name = "Learn Manifestation",
            Instructions = "Cast Heal.",
            NextStep = 7,
            Goals = {
                {"Custom", function(playerObj)
                    local primedSpell = playerObj:GetObjVar("PrimedSpell")
                    if ( not primedSpell or primedSpell ~= "Heal" ) then
                        return false
                    end
                    return true
                end},
            },
        },
        {--7
            Name = "Learn Manifestation",
            Instructions = "Target the spell.",
            NextStep = 8,
            Goals = {
                {"Custom", function(playerObj)
                    local primedSpell = playerObj:GetObjVar("PrimedSpell")
                    if ( primedSpell and primedSpell == "Heal" ) then
                        return false
                    end
                    return true
                end},
            },
        },
        {--8
            Name = "Learn Manifestation",
            Instructions = "Speak to a mage trainer.",
            EndDialogue = {"I did it! I think?","There are varying difficulties of spells, all requiring different skill levels to cast successfully. Spells can fizzle out if your technique falters. Each attempt consumes a spell reagent, so don't eat them! Magic is tough but it gets easier!"},
            MarkerLocs = {
                QuestMarkers.GatekeeperBlackForest,
                QuestMarkers.GatekeeperSouthernRim,
                QuestMarkers.GatekeeperCrossroads,
                QuestMarkers.GatekeeperEasternFrontier,
                QuestMarkers.MageBarrenLands,
                QuestMarkers.MageEldeir,
                QuestMarkers.MageValus,
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {--choice 1
                    {"Item", "ingredient_ginsengroot", 150},
                    {"SetSkill", "ChannelingSkill", 30},
                    {"SetSkill", "ManifestationSkill", 30},
                },
            },
        },
    },
    ManifesterProfessionTierOne = {
        {--1
            Name = "Manifester Part 1",
            Instructions = "Obtain the required skill.",
            Requirements = {
                {"Disabled"},
                {"MaxTimesQuestDone", "ManifesterProfessionTierOne", 1},
                {"QuestActive", "ManifesterProfessionTierOne", false},
                {"MinTimesQuestDone", "ManifesterProfessionIntro", 1},
                {"Skill", "ManifestationSkill", 30},
                {"Skill", "ChannelingSkill", 30},
            },
        },
    },
    ManifesterProfessionTierTwo = {
        {--1
            Name = "Manifester Part 2",
            Instructions = "Obtain the required skill.",
            Requirements = {
                {"Disabled"},
                {"MaxTimesQuestDone", "ManifesterProfessionTierTwo", 1},
                {"QuestActive", "ManifesterProfessionTierTwo", false},
                {"MinTimesQuestDone", "ManifesterProfessionTierOne", 1},
                {"Skill", "ManifestationSkill", 50},
                {"Skill", "ChannelingSkill", 50},
            },
        },
    },
    ManifesterProfessionTierThree = {
        {--1
            Name = "Manifester Part 3",
            Instructions = "Obtain the required skill.",
            Requirements = {
                {"Disabled"},
                {"MaxTimesQuestDone", "ManifesterProfessionTierThree", 1},
                {"QuestActive", "ManifesterProfessionTierThree", false},
                {"MinTimesQuestDone", "ManifesterProfessionTierTwo", 1},
                {"Skill", "ManifestationSkill", 80},
                {"Skill", "ChannelingSkill", 80},
            },
        },
    },
    ManifesterProfessionTierFour = {
        {--1
            Name = "Manifester Part 4",
            Instructions = "Obtain the required skill.",
            Requirements = {
                {"Disabled"},
                {"MaxTimesQuestDone", "ManifesterProfessionTierFour", 1},
                {"QuestActive", "ManifesterProfessionTierFour", false},
                {"MinTimesQuestDone", "ManifesterProfessionTierThree", 1},
                {"Skill", "ManifestationSkill", 100},
                {"Skill", "ChannelingSkill", 100},
            },
        },
    },]]


    DungoneerLeaguePR10 = 
    {
        { -- 1
            Name = "Silencing Shamans",
            Instructions = "Kill [F2F5A9]20 kobolds shamans[-] in [F2F5A9]Everfrost Cavern[-].",
            StartDialogue = {"Silencing Shamans?","Reports are coming in that a tribe of Kobolds have a stronghold in the Everfrost Cavern. Some of them seem to possess a basic mastered magery! This cannot be tolerated, take the light to them."},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "DungoneerLeaguePR10_1",
                Type = "Hunting",
                Region = "Area-Everfrost Cavern",
                QuestArgs = 
                {
                    { "kobold_shaman" },
                    true
                },
                Count = 10,
                Name = "Kobold Shaman",
                NamePlural = "Kobold Shamans",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        { -- 2
            Name = "Howling Caves",
            Instructions = "Kill [F2F5A9]5 yeti matriarchs[-] in [F2F5A9]Everfrost Cavern[-].",
            StartDialogue = {"Howling Caves?","The Everfrost Cavern seems to have an infestation of yetis in the eastern tunnels. I am told that a group of matriarchs sustain their presense. You know what to do."},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "DungoneerLeaguePR10_2",
                Type = "Hunting",
                Region = "Area-Everfrost Cavern",
                QuestArgs = 
                {
                    { "yeti_matriarch" },
                    true
                },
                Count = 5,
                Name = "Yeti Matriarch",
                NamePlural = "Yeti Matriarch",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        { -- 3
            Name = "Dust to Dust",
            Instructions = "Kill [F2F5A9]5 soul reapers[-] in [F2F5A9]The Ancient City[-].",
            StartDialogue = {"Dust to Dust?","To the west of the Fjolla lays a forgotten and forsaken city. The streets are haunted by the spirits of those long dead, they should be reminded that they must not walk within the light."},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "DungoneerLeaguePR10_3",
                Type = "Hunting",
                Region = "Area-The Ancient City",
                QuestArgs = 
                {
                    { "soul_reaper" },
                    true
                },
                Count = 5,
                Name = "Soul Reaper",
                NamePlural = "Soul Reapers",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },

        { -- 4
            Name = "Old Enemies",
            Instructions = "Kill [F2F5A9]5 lich lords[-] in [F2F5A9]The Asper Graveyard[-].",
            StartDialogue = {"Old Enemies?","The Asper Graveyard sits atop a hill to the northeast of Asper. It was one a tranquil place for the departed to spend eternity. Now it crawls with liches, drawn by the countless souls that rest. Ensure they are allowed to stay at rest."},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "DungoneerLeaguePR10_4",
                Type = "Hunting",
                Region = "Spawn-AsperGraveyard",
                QuestArgs = 
                {
                    { "ruin_lich_lord" },
                    true
                },
                Count = 5,
                Name = "Lich Lord",
                NamePlural = "Lich Lords",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },

        { -- 5
            Name = "Lukewarm Joy",
            Instructions = "Kill [F2F5A9]10 thawed beetles[-] in [F2F5A9]The Hot Springs[-].",
            StartDialogue = {"Lukewarm Joy?","I'm told the Hot Springs in the southwestern reaches of the tundra are quite enjoyable. I personally do not find pleasure in such fancies, but I do find pleasure in knowing that you will control the beetle population so that others may enjoy the hot springs."},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "DungoneerLeaguePR10_5",
                Type = "Hunting",
                Region = "Area-Frozen Tundra",
                QuestArgs = 
                {
                    { "beetle_giant_ice" },
                    true
                },
                Count = 10,
                Name = "Thawed Beetle",
                NamePlural = "Thawed Beetles",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        { -- 6
            Name = "Playing Human",
            Instructions = "Kill the [F2F5A9]Kobold Chieftain[-] in [F2F5A9]Abandoned Cabin[-].",
            StartDialogue = {"Playing Human?","The most horrific news arrived today. On the border between Everfrost and The Vald a Kobold Chieftain has taken up residence in the cabin of a slain family. This outrage demands a reponse!"},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "DungoneerLeaguePR10_6",
                Type = "Hunting",
                Region = "Area-Everfrost",
                QuestArgs = 
                {
                    { "kobold_chief" },
                    true
                },
                Count = 1,
                Name = "Kobold Chieftain",
                NamePlural = "Kobold Chieftain",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },
    },

    ExplorerLeaguePR10 = 
    {
        { -- 1
            Name = "Grave Tidings",
            Instructions = "Slay the [F2F5A9]Frozen Skeleton[-] at [F2F5A9]Grave Lake[-].",
            StartDialogue = {"Grave Tidings?","There is an abandoned chapel in eastern Everfrost. Rumor has it that the dead have begun to arise at a nearby lake and they are lead by a skeleton of immense power. Go and take care of it."},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "ExplorerLeaguePR10_1",
                Type = "Hunting",
                Region = "Area-Frozen Tundra",
                QuestArgs = 
                {
                    { "skeleton_ice_boss" },
                    true
                },
                Count = 1,
                Name = "Frozen Skeleton",
                NamePlural = "Frozen Skeleton",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        { -- 2
            Name = "Feeding Time",
            Instructions = "Slay the [F2F5A9]Overfed Drone[-] at the [F2F5A9]Hot Springs[-].",
            StartDialogue = {"Feeding Time?","Within the Frozen Tundra at the Belgae encampment are some isolated hot springs. I have it from a reliable source that a true abomonation resides there. Go find out."},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "ExplorerLeaguePR10_2",
                Type = "Hunting",
                Region = "Area-Frozen Tundra",
                QuestArgs = 
                {
                    { "beetle_flying_boss" },
                    true
                },
                Count = 1,
                Name = "Overfed Drone",
                NamePlural = "Overfed Drone",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        { -- 3
            Name = "Blood Rage",
            Instructions = "Slay the [F2F5A9]Enraged Yeti[-] in the [F2F5A9]Everfrost Cavern[-].",
            StartDialogue = {"Blood Rage?","The Everfrost Cavern is a unique place to visit. It seems that a tribe of Kobolds and a slaughter of Yetis coexist in the caves. My concern is with the yetis, it is my understanding that when provoked they can enter a blood rage. Care to go confirm for me?"},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "ExplorerLeaguePR10_3",
                Type = "Hunting",
                Region = "Area-Frozen Tundra",
                QuestArgs = 
                {
                    { "yeti_boss" },
                    true
                },
                Count = 1,
                Name = "Enraged Yeti",
                NamePlural = "Enraged Yeti",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        { -- 4
            Name = "Azura's Choosen",
            Instructions = "Slay [F2F5A9]Azura's High Priestess[-] at the [F2F5A9]Temple of Azura[-].",
            StartDialogue = {"Azura's Choosen?","There is an ancient city in the wilds of the Frozen Tundra that once housed a mighty civilzation. Left to the will of time it has been reduced to little but dust and rubble. However, there is a temple to the south of the ruins that I am told is still in remarkable condition but guarded by a spirit of old. This is no task to tackle alone."},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "ExplorerLeaguePR10_4",
                Type = "Hunting",
                Region = "Area-Frozen Tundra",
                QuestArgs = 
                {
                    { "azura_high_preist" },
                    true
                },
                Count = 1,
                Name = "Azura High Priestess",
                NamePlural = "Azura High Priestess",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },

        { -- 5
            Name = "Belgae War Plans",
            Instructions = "Slay the [F2F5A9]Belgae Warmaster[-] at the [F2F5A9]Barbarian Encampment[-].",
            StartDialogue = {"Belgae War Plans?","The Belgae of the Tundra are a harsh breed and find war a comfortable condition. Even now they makes plans to bring destruction upon others. Go to their encampment and seek out their warmaster, bring friends as he will not listen to reason."},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "ExplorerLeaguePR10_5",
                Type = "Hunting",
                Region = "Area-Frozen Tundra",
                QuestArgs = 
                {
                    { "belgae_warmaster" },
                    true
                },
                Count = 1,
                Name = "Belgae Warmaster",
                NamePlural = "Belgae Warmaster",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },

        { -- 6
            Name = "Unwelcomed Guests",
            Instructions = "Slay [F2F5A9]20 yetis[-] at the [F2F5A9]Valdheim Ruins[-].",
            StartDialogue = {"Unwelcomed Guests?","Valdheim was once the jewel of the north I am told. However some grave calamity caused the mountian above the village to collapse and seal Valdheim in a frozen tomb of ice and snow. Now yetis have taken up residence in the ruins, this place needs to be studied not plundered. Go and take care of the yeti problem."},
            Cooldown = TimeSpan.FromHours(1),
            QuestData = {
                Key = "ExplorerLeaguePR10_6",
                Type = "Hunting",
                Region = "Area-Frozen Tundra",
                QuestArgs = 
                {
                    {"yeti", "yeti_matriarch", "yeti_patriarch" },
                    true
                },
                Count = 1,
                Name = "Yeti",
                NamePlural = "Yetis",
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },
    },

    ArtisansLeaguePR10 = 
    {
        {-- 1
            Name = "Take a Bow",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]15 ash short bows[-] for Jig.",
            StartDialogue = {"Take a Bow?","We've a large order for ash short bows, can you contribute 15 to our stocks?"},
            NextStep = 2,
            Goals = {
                {"HasCraftedItem", "weapon_shortbow", 15, "Material", "AshBoards"},
            },
        },
        {-- 1.2
            Name = "Take a Bow",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Take a Bow","Well done."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "weapon_shortbow", 15, "Material", "AshBoards"},
                
            },
            OnEnd = {
                {"TakeCraftedItems", "weapon_shortbow", 15, "Material", "AshBoards"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 2
            Name = "Fluttering Flutes",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]20 flutes[-] for Jig.",
            StartDialogue = {"Fluttering Flutes?","Bards are everyhere these days! The Artisan League is here to profit off them too, bring me 20 flutes we can sell."},
            NextStep = 4,
            Goals = {
                {"HasCraftedItem", "tool_flute", 20},
            },
        },
        {-- 2.2
            Name = "Fluttering Flutes",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Fluttering Flutes","Ah bards, it's like taking money from a... well bard."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "tool_flute", 20},
                
            },
            OnEnd = {
                {"TakeCraftedItems", "tool_flute", 20},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 3
            Name = "Incoming Damage",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]20 wooden kite shields[-] for Jig.",
            StartDialogue = {"Incoming Damage?","Everyone is off to explore the Frozen Tundra, I hear. Not old Marlo here, I like my climates agreeable. But danger for others is profit for us, how bout you bring me 20 wooden kite shields."},
            NextStep = 6,
            Goals = {
                {"HasCraftedItem", "shield_kite", 20, "Material", "Boards"},
            },
        },
        {-- 3.2
            Name = "Incoming Damage",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Incoming Damage","Very nice, see we sell wooden ones because they will work well, but not too well. Need those return customers."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "shield_kite", 20, "Material", "Boards"},
                
            },
            OnEnd = {
                {"TakeCraftedItems", "shield_kite", 20, "Material", "Boards"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 4
            Name = "Reel Issues",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]20 fishing rods[-] for Jig.",
            StartDialogue = {"Reel Issues?","I've heard that fishing is all the craze now, some tall tales about pulling of chests full of booty. I don't believe such stories, but I'll profit off the cattle. Bring in 20 fishing rods."},
            NextStep = 8,
            Goals = {
                {"HasCraftedItem", "tool_fishing_rod", 20 },
            },
        },
        {-- 4.2
            Name = "Reel Issues",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Reel Issues","Yep, they look like fishing rods to me."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "tool_fishing_rod", 20 },
                
            },
            OnEnd = {
                {"TakeCraftedItems", "tool_fishing_rod", 20 },
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 5
            Name = "Planting Pots",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft 10 [F2F5A9]plant pots[-] for Jig.",
            StartDialogue = {"Planting Pots?","So gardening is a big deal now. Every time I visit Valus all I hear on the streets is how big someone's melons are. I prefer eggplants myself but that's another tale. Bring me 10 plant pots."},
            NextStep = 10,
            Goals = {
                {"HasCraftedItem", "garden_plant_pot", 10 },
            },
        },
        {-- 5.2
            Name = "Planting Pots",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Planting Pots","You know these sell like hotcakes too. I might have to give gardening a whirl."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "garden_plant_pot", 10 },
                
            },
            OnEnd = {
                {"TakeCraftedItems", "garden_plant_pot", 10 },
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },

        {-- 6
            Name = "Blight Flight",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft 50 [F2F5A9]blightwood arrows[-] for Jig.",
            StartDialogue = {"Blight Flight?","We've a guild that wants a large order of Blightwood Arrows delievered to them. I mean, we're talking retirement money for me. You can help by bringing in 50 blightwood arrows."},
            NextStep = 12,
            Goals = {
                {"HasCraftedItem", "arrow_blightwood", 50 },
            },
        },
        {-- 6.2
            Name = "Blight Flight",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Blight Flight","They do look deadly, glad I choose to push pens instead of cross swords."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "arrow_blightwood", 50 },
                
            },
            OnEnd = {
                {"TakeCraftedItems", "arrow_blightwood", 50 },
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },
    },

    ProcurersLeaguePR10 =
    {
        {-- 1
            Name = "Recipe Exchange",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Collect a [F2F5A9]crafting recipe[-] for Naldren.",
            StartDialogue = {"Recipe Exchange?","We are looking at doing a recipe exchange program with some of the artisan shops around Celador. I've been asked to pilot the program here in Valus. Figured I'd start with our supply chain participants, if you bring me a recipe I can give you a recipe for the same skill."},
            NextStep = 2,
            Goals = {
                {"Custom", function(playerObj)
                    local backpackObj = playerObj:GetEquippedObject("Backpack")
                    local hasItem = false
                    ForEachItemInContainerRecursive(backpackObj, function( item )
                        if(
                            item:HasObjVar("Recipe") -- is it recipe?
                            and item:HasModule("recipe") -- make sure!
                        ) then
                            hasItem = true
                            return false
                        end
                        return true
                    end)
                    return hasItem
                end},
            },
        },
        {-- 1.1
            Name = "Recipe Exchange",
            Instructions = "Give a [F2F5A9]crafting recipe[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Recipe Exchange","Nice doing business with you."},
            Goals = {
                {"TurnIn"},
                {"Custom", function(playerObj)
                    local backpackObj = playerObj:GetEquippedObject("Backpack")
                    local hasItem = false
                    ForEachItemInContainerRecursive(backpackObj, function( item )
                        if(
                            item:HasObjVar("Recipe") -- is it recipe?
                            and item:HasModule("recipe") -- make sure!
                        ) then
                            Quests.SetQuestData( playerObj, "ProcurersLeaguePR10_1", "Recipe", item:GetObjVar("Recipe") )
                            item:Destroy()
                            hasItem = true
                            return false
                        end
                        return true
                    end)
                    return hasItem
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.NormalFavor)

                        local recipeType = Quests.GetQuestData( playerObj, "ProcurersLeaguePR10_1", "Recipe" );
                        local skill = GetSkillForRecipe( recipeType )
                        local template = CraftingOrders.RecipeRewards[skill][ math.random( 1,#CraftingOrders.RecipeRewards[skill] ) ]
                        Create.InBackpack(template, playerObj)
                    end},
                },
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ProcurersLeaguePR10_1")
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ProcurersLeaguePR10_1")
                end},
            },
        },

        {-- 2
            Name = "And My Pick",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Collect [F2F5A9]100 obsidian ore[-] for Naldren.",
            StartDialogue = {"And My Pick?","The League of Procurers are wanting to show appreciation for all the hard working harvesters that help keep us in business. In that spirit I'm trading a Deft Mining Pick for 100 obsidian ore. Don't look at me like that! I'm still running a business here. Do you want it or not?"},
            NextStep = 4,
            Goals = {
                {"HasItemInBackpack", "resource_obsidian_ore", 100},
            },
        },
        {-- 2.1
            Name = "And My Pick",
            Instructions = "Give [F2F5A9]100 obsidian ore[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] And My Pick","The League of Procurers appreciate you. Well, your business at least."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_obsidian_ore", 100},
                {"TakeItem", "resource_obsidian_ore", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.GreaterFavor)
                    end},
                    {"Item","tool_mining_pick_deft"},
                },
            },
        },

        {-- 3
            Name = "And My Axe",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Collect [F2F5A9]100 ash wood[-] for Naldren.",
            StartDialogue = {"And My Axe?","The League of Procurers are wanting to show appreciation for all the hard working harvesters that help keep us in business. In that spirit I'm trading a Deft Hatchet for 100 ash wood. Don't look at me like that! I'm still running a business here. Do you want it or not?"},
            NextStep = 6,
            Goals = {
                {"HasItemInBackpack", "resource_ash", 100},
            },
        },
        {-- 3.1
            Name = "And My Axe",
            Instructions = "Give [F2F5A9]100 ash wood[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] And My Axe","The League of Procurers appreciate you. Well, your business at least."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_ash", 100},
                {"TakeItem", "resource_ash", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.GreaterFavor)
                    end},
                    {"Item","tool_hatchet_deft"},
                },
            },
        },

        {-- 4
            Name = "And My Rod",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Collect [F2F5A9]100 Razor-Fish[-] for Naldren.",
            StartDialogue = {"And My Rod?","The League of Procurers are wanting to show appreciation for all the hard working harvesters that help keep us in business. In that spirit I'm trading a Deft Fishing Rod for 100 Razor-Fish. Don't look at me like that! I'm still running a business here. Do you want it or not?"},
            NextStep = 8,
            Goals = {
                {"HasItemInBackpack", "resource_fish_razor", 100},
            },
        },
        {-- 4.1
            Name = "And My Rod",
            Instructions = "Give [F2F5A9]100 Razor-Fish[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] And My Rod","The League of Procurers appreciate you. Well, your business at least."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_fish_razor", 100},
                {"TakeItem", "resource_fish_razor", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.GreaterFavor)
                    end},
                    {"Item","tool_fishing_rod_deft"},
                },
            },
        },

        {-- 5
            Name = "More Essences",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Collect [F2F5A9]100 magical essences[-] for Naldren.",
            StartDialogue = {"More Essences?","Our research into magical essences has ramped up and we need a lot more. Can you secure me 100 samples?"},
            NextStep = 10,
            Goals = {
                {"HasItemInBackpack", "resource_essence", 100},
            },
        },
        {-- 5.1
            Name = "More Essences",
            Instructions = "Give [F2F5A9]100 magical essences[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] More Essences","Very nice, you're one of my top collectors."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_essence", 100},
                {"TakeItem", "resource_essence", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },

        {-- 6
            Name = "Jewelcraft Research",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Collect [F2F5A9]100 gold ore[-] for Naldren.",
            StartDialogue = {"Jewelcraft Research?","Now listen, I won't say we haven't had setbacks but we're getting closer everyday I tell you! Bring in 100 gold ore to help the cause."},
            NextStep = 12,
            Goals = {
                {"HasItemInBackpack", "resource_gold_ore", 100},
            },
        },
        {-- 6.1
            Name = "Jewelcraft Research",
            Instructions = "Give [F2F5A9]100 gold ore[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Jewelcraft Research","My advice, anyone planning on making jewelry -- I'd stockpile this stuff."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_gold_ore", 100},
                {"TakeItem", "resource_gold_ore", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },
    },

    DungeonRuin = {
        {--1
            Name = "Animated Bones",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Skeleton",
            Instructions = "Slay [F2F5A9]20 skeletons[-] within Ruin.",
            StartDialogue = {"Animated Bones?","Within the walls of Ruin a dark magic seeps into the bones of fallen adventurers. These skeletons reanimate and haunt the halls and passageways -- doomed to live in death. Release them from their bonds, and put them to rest."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "RuinDungeon1", {"skeleton","skeleton_archer","skeleton_mage"} )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( ServerSettings.WorldName == "Ruin" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "RuinDungeon1", {"skeleton","skeleton_archer","skeleton_mage"}, "Skeletons", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon1" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon1" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },
        {--2
            Name = "Lich, Do This",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Lich",
            Instructions = "Slay [F2F5A9]20 liches[-] within Ruin.",
            StartDialogue = {"Liche, Do This?","Ruin was once a peaceful resting place for those denizens of Celeador who passed into the next realm. Nearly a century ago the Valusian magus Ymar Stillheart sought to defy death though forbidden magically incantations.  This necromancy caused the reanimation of the undead that now haunt the halls of Ruin. The deadliest among these being those who possessed some magical affinity in life, doomed now to arise as liches.  Put these souls back to rest, kill any lich you find within."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "RuinDungeon2", "lich" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( ServerSettings.WorldName == "Ruin" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "RuinDungeon2", "lich", "Liches", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon2" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon2" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },
        {--3
            Name = "Tormented Bones",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Skeleton Watcher",
            Instructions = "Slay the [F2F5A9]skelton watcher[-] within Ruin.",
            StartDialogue = {"Tormented Bones?","The upper level of Ruin was reserved for the burials of stewards and magisters.  Those who dedicated their lives to the welfare of their fellow Celadorians.  Now in death they roam the crypts watching over the countless undead who have risen with them. Find one of the skeleton watchers and destroy what it has become."},
            OnStart = {
                {"Custom", function(playerObj)
                    QuestsLeague.AddForceStartDynamicSpawn( playerObj, "Ruin1DynamicSpawner" )
                    Quests.InitTrackMobileKillCount( playerObj, "RuinDungeon3", "skeleton_watcher" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( ServerSettings.WorldName == "Ruin" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "RuinDungeon3", "skeleton_watcher", "Skeleton Watcher", 1 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "Ruin1DynamicSpawner" )
                    Quests.ClearQuestData( playerObj, "RuinDungeon3" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "Ruin1DynamicSpawner" )
                    Quests.ClearQuestData( playerObj, "RuinDungeon3" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },
        {--4
            Name = "Rotting Flesh",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Corpse Walkers",
            Instructions = "Slay [F2F5A9]20 corpse walkers (zombies, ghouls, or mummies)[-] within Ruin.",
            StartDialogue = {"Rotting Flesh?","Tradition is a powerful drug.  Though the act of burying the dead within Ruin has been banned for nearly a century there are those who still seek to have their loved ones reside in these hallowed halls. Many of these bodies do not fully decay before the dark magic takes hold and they arise as deformed and rotting vestiges -- walking corpses. These zombies, ghouls, and mummies need to be destroyed."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "RuinDungeon4", {"zombie","ghoul","mummy"} )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( ServerSettings.WorldName == "Ruin" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "RuinDungeon4", {"zombie","ghoul","mummy"}, "Corpse Walkers", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon4" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon4" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },
        {--5
            Name = "Servants Released",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Lich Servant",
            Instructions = "Slay [F2F5A9]20 lich servants[-] within Ruin.",
            StartDialogue = {"Servants Released?","The liches of Ruin constantly endeavor to increase their magical prowess. The labor intensive tasks they delegate to an army of servants they have conjured from the severed skulls they've robbed from the crypts. These servants though mostly obedient still have moments of lucid rebellion. This cannot be allowed, extinguish the magic that binds these skulls and free the servants."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "RuinDungeon5", "ruin_servant" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( ServerSettings.WorldName == "Ruin" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "RuinDungeon5", "ruin_servant", "Lich Servants", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon5" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon5" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },
        {--6
            Name = "Lich, Go Again",
            Cooldown = TimeSpan.FromHours(3),
            MobileKillName = "Lich Lord",
            Instructions = "Slay [F2F5A9]10 lich lords[-] within Ruin.",
            StartDialogue = {"Lich, Go Again?","Deep within Ruin there has emerged a ruling class of ancient magi. Possessing both strength and intense magical abilities they rule over the lower halls. I fear that if their ranks grow too numerous they will seek to venture forth from Ruin and expand their hold. You must help us, slay some of these lich lords."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "RuinDungeon6", "lich_lord" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( ServerSettings.WorldName == "Ruin" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "RuinDungeon6", "lich_lord", "Lich Lords", 10 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon6" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon6" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },
        {--7
            Name = "Deathly Scales",
            Cooldown = TimeSpan.FromHours(3),
            MobileKillName = "Deathly Scales",
            Instructions = "Slay a [F2F5A9]Crypt Dragon[-] within Ruin.",
            StartDialogue = {"Deathly Scales?","The liches of ruin have turned their dark magic to the art of manipulating the corpses of dragons.  The offspring of these horrors have been housed in a makeshift cell block on the lower level. We must ensure that these experiments are destroyed, least the liches study their mistakes and unleash untold horrors into the world. Go forth and slay one of these crypt dragons."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "RuinDungeon7", "zombie_dragon" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( ServerSettings.WorldName == "Ruin" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "RuinDungeon7", "zombie_dragon", "Crypt Dragon", 1 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon7" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "RuinDungeon7" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },
        {--8
            Name = "Breaking the Bond",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Enraged Servant",
            Instructions = "Slay the [F2F5A9]enraged servant[-] within Ruin.",
            StartDialogue = {"Breaking the Bond?","We are getting reports that some of the lich servants have bonded with their master. This is horrible news! What has kept the liches of Ruin in-check thus far has been the constant struggle to control their underlings. Find one of these servants and kill it, beware however they are reported to be very loyal and prone to bouts of terrible anger when disturbed."},
            OnStart = {
                {"Custom", function(playerObj)
                    QuestsLeague.AddForceStartDynamicSpawn( playerObj, "Ruin2DynamicSpawner" )
                    Quests.InitTrackMobileKillCount( playerObj, "RuinDungeon8", "enraged_lich_servant" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( ServerSettings.WorldName == "Ruin" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "RuinDungeon8", "enraged_lich_servant", "Enraged Servant", 1 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "Ruin2DynamicSpawner" )
                    Quests.ClearQuestData( playerObj, "RuinDungeon8" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "Ruin2DynamicSpawner" )
                    Quests.ClearQuestData( playerObj, "RuinDungeon8" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },
    },

    CorruptionDungeon = {
        {--1
            Name = "Imps Abound",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Imp",
            Instructions = "Slay [F2F5A9]20 Imps[-] within Corruption.",
            StartDialogue = {"Imps Abound?","The imps have began to move into the upper halls of Corruption. This cannot be allowed, you must go and cull their population before they overrun us!"},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "CorruptionDungeon1", "imp" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in Corruption when completing the quest.
                    if( ServerSettings.WorldName == "Corruption" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "CorruptionDungeon1", "imp", "Imps", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon1" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon1" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", 100)
                    end},
                },
            },
        },
        {--2
            Name = "Cull the Hounds",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Slay [F2F5A9]20 Hell Hounds[-] within Corruption.",
            StartDialogue = {"Cull the Hounds?","Long ago the demons that lurk in the depths of Corruption turned their dark arts on the wolves of the surronding forests. Their tireless toiling produced the dreaded Hell Hounds found within Corruption.  These are abomonations and must be put the sword - at all costs!"},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "CorruptionDungeon2", "hell_hound" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in Corruption when completing the quest.
                    if( ServerSettings.WorldName == "Corruption" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "CorruptionDungeon2", "hell_hound", "Hell Hounds", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon2" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon2" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", 100)
                    end},
                },
            },
        },
        {--3
            Name = "Imps Abound Redux",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Slay [F2F5A9]20 Greater Imps[-] within Corruption.",
            StartDialogue = {"Imps Abound Redux?","The Imp incursions into the upper halls of Corruption have eluded to a more sinister breed of Imps residing in the depths.  Explore Corruption, find these Greater Imps and slay them."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "CorruptionDungeon3", "greater_imp" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    if( ServerSettings.WorldName == "Corruption" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "CorruptionDungeon3", "greater_imp", "Greater Imps", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon3" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon3" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", 100)
                    end},
                },
            },
        },
        {--4
            Name = "Gargoyle Hunting",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Slay [F2F5A9]20 Gargoyles[-] within Corruption.",
            StartDialogue = {"Gargoyle Hunting?","Stories of winged demons haunting the upper halls of Corruption have began to trickle out. Go, find these Gargoyles, and kill them enmass."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "CorruptionDungeon4", "gargoyle" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in Corruption when completing the quest.
                    if( ServerSettings.WorldName == "Corruption" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "CorruptionDungeon4", "gargoyle", "Gargoyles", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon4" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon4" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", 100)
                    end},
                },
            },
        },
        {--5
            Name = "Skull Dosing",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Slay [F2F5A9]20 Flaming Skulls[-] within Corruption.",
            StartDialogue = {"Skull Dosing?","Many would be adventurers have met their doom in Corruption.  Their bodies are consumed by the filth that resides within, all save their skulls.  These are taken into the darkest reaches and transformed into hideous monstrosities that serve the demons they once sought to defeat."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "CorruptionDungeon5", "death_skull" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in Corruption when completing the quest.
                    if( ServerSettings.WorldName == "Corruption" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "CorruptionDungeon5", "death_skull", "Flaming Skulls", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon5" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon5" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", 100)
                    end},
                },
            },
        },
        {--6
            Name = "The Gluttonous",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Slay [F2F5A9]20 Gluttons[-] within Corruption.",
            StartDialogue = {"The Gluttonous?","The corpses of fallen foes are quickly devoured by the demons in Corruption. However, the Gluttons devour more fervently than all the rest. They gorge themselves near to bursting and hunger for more. Let them now dine on fire and iron!"},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "CorruptionDungeon6", "glutton" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in Corruption when completing the quest.
                    if( ServerSettings.WorldName == "Corruption" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "CorruptionDungeon6", "glutton", "Gluttons", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon6" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon6" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", 100)
                    end},
                },
            },
        },
        {--7
            Name = "Eye for an Eye",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Slay [F2F5A9]20 Gazers[-] within Corruption.",
            StartDialogue = {"Eye for an Eye?","Of all the creatures that roam the cursed halls of Corruption, none strike more fear than the Gazers of the lower halls. Their many eyes see all that passes and some whisper that they pluck the eyes from their unwilling victims to add to their own."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "CorruptionDungeon7", "gazer" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in Corruption when completing the quest.
                    if( ServerSettings.WorldName == "Corruption" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "CorruptionDungeon7", "gazer", "Gazers", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon7" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon7" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", 100)
                    end},
                },
            },
        },
        {--8
            Name = "Bring the Light",
            Cooldown = TimeSpan.FromHours(4),
            Instructions = "Slay [F2F5A9]40 Demons[-] within Corruption.",
            StartDialogue = {"Bring the Light?","The demons of Corruption have grown too numerous, you are charged with slaying the beasts. Bright the light of truth to their darkened halls."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "CorruptionDungeon8", { "imp", "hell_hound", "greater_imp", "gargoyle", "death_skull", "glutton", "half_gazer", "gazer", "greater_demon" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in Corruption when completing the quest.
                    if( ServerSettings.WorldName == "Corruption" ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "CorruptionDungeon8", { "imp", "hell_hound", "greater_imp", "gargoyle", "death_skull", "glutton", "half_gazer", "gazer", "greater_demon" }, "Demons", 40 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon8" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "CorruptionDungeon8" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Dungeoneers", 100)
                    end},
                },
            },
        },
    },
    ContemptDungeon = {
        -- PLACEHOLDER
    },
    RuinDungeon = {
        -- PLACEHOLDER
    },
    DeceptionDungeon = {
        -- PLACEHOLDER
    },
    ExplorersLeagueValus = 
    {

        {--1 [ Rebel Camp: Southern Rim ]
            Name = "Rebelious Incursions",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Rebels",
            Instructions = "Slay [F2F5A9]20 Rebels[-] at the Rebel Camp in Southern Rim.",
            StartDialogue = {"Rebelious Incursions?","In the Southern Rim, just north of the Feet of Gods, lies the headquarters of a large rebel faction. They have been raiding shipments along the Old Fool's Road - this cannot be allowed."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeagueValus1", { "rebel", "rebel_female", "rebel_looter", "rebel_looter_female", "rebel_hq" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("RebelCampInner") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeagueValus1", { "rebel", "rebel_female", "rebel_looter", "rebel_looter_female", "rebel_hq" }, "Rebels", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus1" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus1" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", 100)
                    end},
                },
            },
        },

        {--2 [ Rebel Camp: Southern Rim (Dynamic Spawner) ]
            Name = "Rebel Reinforcements",
            Cooldown = TimeSpan.FromHours(4),
            MobileKillName = "Rebel High General",
            Instructions = "Slay the [F2F5A9]Rebel High General[-] at the Rebel Camp in Southern Rim.",
            StartDialogue = {"Rebel Reinforcements?","The Rebel Camp in the Soutern Rim has recently had an influx of fresh recruits.  They are lead by a charismatic general who must be stopped before he amasses enough troops to threaten Valus itself. Explore the camp, find the general, and kill the general and his reinforcements."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeagueValus2", "rebel_dynamic_boss" )
                    QuestsLeague.AddForceStartDynamicSpawn( playerObj, "DynamicRebelSpawner" ) -- force start the dynamic spawner
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("RebelCampInner") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeagueValus2", "rebel_dynamic_boss", "Rebel High General", 1 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus2" )
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "DynamicRebelSpawner" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus2" )
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "DynamicRebelSpawner" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", 250)
                    end},
                },
            },
        },

        {--3 [ Rock Spirte: Misty Caverns ]
            Name = "Water and Stone",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Rock Sprites",
            Instructions = "Slay [F2F5A9]20 Rock Sprites[-] in the Misty Caverns.",
            StartDialogue = {"Water and Stone?","In the northern folds of the Southern Rim resides an ancient cavern. Many a surveyors have ventured in seeking the wealth and riches of the caves, most never return. Stories of great bouldery beasts have made locals uneasy. Go to these caverns and kill these stone beasts."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeagueValus3", "rock_sprite" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Waterfall Mine to get kills
                    if( playerObj:IsInRegion("WaterfallMine") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeagueValus3", "rock_sprite", "Rock Sprite", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus3" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus3" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", 100)
                    end},
                },
            },
        },

        {--4 [ Gazer Island ]
            Name = "Watchful Eyes",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Gazers",
            Instructions = "Slay [F2F5A9]20 Gazers[-] on Gazer Island in the Barren Lands",
            StartDialogue = {"Watchful Eyes?","On an island in the southeast corner of the Barren Lands resides a Gazer den. These many-eyed demons are attracted to the power that resonates from the ruins found on the island. Travel there and end their watch."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeagueValus4", { "gazer", "half_gazer" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("Area-Gazer Isle") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeagueValus4", { "gazer", "half_gazer" }, "Gazers", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus4" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus4" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", 100)
                    end},
                },
            },
        },

        {--5 [ Abandoned Town ]
            Name = "Magic Bones",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Undead",
            Instructions = "Slay [F2F5A9]20 Undead[-] at the Destroyed Town in Denir.",
            StartDialogue = {"Magic Bones?","There is an abandoned town along the eastern face of the Kulled Crag in Denir. The town was once a thirving trade center until a deadly plague befell the population and all perished. There are reports that the skeletons of the dead have risen and wander the old streets. Go there, give the undead a lasting peace."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeagueValus5", { "lich", "skeleton_mage" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("EasternAbandoned") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeagueValus5", { "lich", "skeleton_mage" }, "Undead", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus5" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus5" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", 100)
                    end},
                },
            },
        },

        {--6 [ Abandonted Town: Dynamic Spawner ]
            Name = "Dead Uprising",
            Cooldown = TimeSpan.FromHours(4),
            MobileKillName = "Enraged Skeleton",
            Instructions = "Slay the [F2F5A9]Enraged Skeleton[-] at the Destroyed Town in Denir.",
            StartDialogue = {"Dead Uprising?","There are reports that a truly awful evil has taken up residence in the abandoned town near the Kulled Crag in Denir. It seems the dead are arising in great numbers. You must get there at once and stop this before it gets out of hand!"},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeagueValus6", "undead_hard_boss" )
                    QuestsLeague.AddForceStartDynamicSpawn( playerObj, "DynamicUndeadHardSpawner" ) -- force starts the dynamic spawner
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("EasternAbandoned") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeagueValus6", "undead_hard_boss", "Enraged Skeleton", 1 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus6" )
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "DynamicUndeadHardSpawner" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus6" )
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "DynamicUndeadHardSpawner" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", 250)
                    end},
                },
            },
        },

        {--7 [ Mushroom Grove: Ents ]
            Name = "Wooden Menace",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Forest Walkers",
            Instructions = "Slay [F2F5A9]20 Forest Walkers[-] within the Radiant Woodlands in Vilewood.",
            StartDialogue = {"Wooden Menace?","In the center of the Vilewood is a enchanted grove. Mushrooms there grow so large that they wilt under their own weight and give off a eerie glow. In this place Forest Walkers, the dark shepards, have made their home. If we are to expand our borders these creatures must go."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeagueValus7", "ent" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("Area-Radiant Woodlands") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeagueValus7", "ent", "Forest Walkers", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus7" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus7" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", 100)
                    end},
                },
            },
        },

        {--8 [ Bandit Camp: Upper Plains ]
            Name = "Bodeful Bandits",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Bandits",
            Instructions = "Slay [F2F5A9]20 Bandits[-] at their camp north of Lake Tethys.",
            StartDialogue = {"Bodeful Bandits?","There is a large bandit camp on the northern shore of Lake Tethys. These ruffians have been ambushing travelers along The Caravan Path and are responsible for the death of at least two very cute bunnies. Survey their camp and kill as many as you can."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeagueValus8", { "bandit", "bandit_female", "bandit_archer", "bandit_archer_female", "bandit_hq" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Bandit camp to complete the quest.
                    if( playerObj:IsInRegion("BanditCampEasy1") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeagueValus8", { "bandit", "bandit_female", "bandit_archer", "bandit_archer_female", "bandit_hq" }, "Bandits", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus8" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeagueValus8" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", 100)
                    end},
                },
            },
        },

    },
    ExplorersLeaguePR9 = 
    {
        {--1
            Name = "Treacherous Webs",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Spiders",
            Instructions = "Slay [F2F5A9]20 spiders[-] at the Spider's Nest in the Black Forest.",
            StartDialogue = {"Treacherous Webs?","Deep within the eastern wilds of the Black Forest lays the Spider's Nest.  Many an unexpecting adventurers have befallen a grime fate when they journey too near this horrid compound. Reports have trickled in that these fiends have begun to hunt ever closer to the Outpost stationed in the forest. Go to the nest, kill as many spiders as you dare. But do not venture too far in, dead men reap no rewards."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-1", { "spider_large", "spider", "spider_vile", "spider_huntress", "spider_hunter" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("BlackForrestSpiders") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-1", { "spider_large", "spider", "spider_vile", "spider_huntress", "spider_hunter" }, "Spiders", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-1" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-1" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {--2
            Name = "Disrupt The Shrine",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Shrine Guardian",
            Instructions = "Slay the [F2F5A9]Shrine Guardian[-] at the Undead Shrine in the Eastern Frontier.",
            StartDialogue = {"Disrupt The Shrine?","There is a deep magic that flows through the mountains and grasslands of Celador. Sometimes this magic can manifest at locations of great power. Like the ley lines of ancient magi they begin to pulsate with energy. Along the Easter Range Peninsula lays one of these locations.  Old ruins gathered upon a hilltop and a shrine that is trying to ignite. Go there, stop the shrine from activating."},
            OnStart = {
                {"Custom", function(playerObj)
                    QuestsLeague.AddForceStartDynamicSpawn( playerObj, "UndeadShrineDynamicSpawner" )
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-2", { "undead_shrine_boss" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("EasternShrine") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-2", { "undead_shrine_boss" }, "Shrine Guardian", 1 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "UndeadShrineDynamicSpawner" )
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-2" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "UndeadShrineDynamicSpawner" )
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-2" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {--3
            Name = "Harpy Eradication",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Harpies",
            Instructions = "Slay [F2F5A9]20 harpies[-] at the Harpy Nest in the Black Forest.",
            StartDialogue = {"Harpy Eradication?","In the northern reaches of the Vilewood region in the Black Forest a nest of harpies have taken up residence.  Harpies are extremely isolatory and to have so many in one location is extremely rare.  Go and explore their nest, kill what you need and keep a sharp eye out for anything unusual."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-3", { "harpy_fledgling", "harpy_adolescent", "harpy", "wild_harpy", "stone_harpy", "harpy_matriarch" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("BlackForrestHarpies2") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-3", { "harpy_fledgling", "harpy_adolescent", "harpy", "wild_harpy", "stone_harpy", "harpy_matriarch" }, "Harpies", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-3" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-3" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {--4
            Name = "Vanishing Valkyrie",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Valkyrie",
            Instructions = "Slay the [F2F5A9]Bandit Valkyrie[-] at the Bandit Camp in the Helm.",
            StartDialogue = {"Vanishing Valkyrie?","So, word is that the bandits in western Helm have found themselves a charismatic and capable leader who is calling herself “Valkyrie”.  This is exciting news!  Well, not so much for Helm but for us.  Go and investigate the bandit camp, you might need to cut a few throats to lure her out."},
            OnStart = {
                {"Custom", function(playerObj)
                    QuestsLeague.AddForceStartDynamicSpawn( playerObj, "BanditDynamicSpawner" )
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-4", "bandit_dynamic_boss" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("EasternCamp3") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-4", "bandit_dynamic_boss", "Valkyrie", 1 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "BanditDynamicSpawner" )
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-4" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    QuestsLeague.RemoveForceStartDynamicSpawn( playerObj, "BanditDynamicSpawner" )
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-4" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {--5
            Name = "Culling the Cultists",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Cultists",
            Instructions = "Slay [F2F5A9]20 cultists[-] at the Ruin of the Artificers in the Barren Lands.",
            StartDialogue = {"Culling the Cultists?","In the harsh deserts of the Barren Lands a fanatical cult has taken hold on the local nomadic inhabitants.  The epicenter of this movement seems to be centered on a collection of prehistoric structures known as -- The Ruins of the Artificer. I would like for you to go and investigate you will undoubtedly need to defend yourself as these cultists are rumored to kidnap and sacrifice non-believers."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-5", { "cultist_guard", "cultist_warrior_rookie", "cultist_mage_apprentice","cultist_warrior_rookie_female", "cultist_mage_apprentice_female","cultist_warrior_veteran", "cultist_warrior_veteran_female", "cultist_warrior_rookie_archer", "cultist_priest", "cultist_mage_devout", "cultist_priest" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("CultistRuins") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-5", { "cultist_guard", "cultist_warrior_rookie", "cultist_mage_apprentice","cultist_warrior_rookie_female", "cultist_mage_apprentice_female", "cultist_warrior_veteran", "cultist_warrior_veteran_female", "cultist_warrior_rookie_archer", "cultist_priest", "cultist_mage_devout", "cultist_priest" }, "Cultists", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-5" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-5" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {--6
            Name = "Snipping Stingers",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Scorpion",
            Instructions = "Slay [F2F5A9]20 scorpions[-] in the Arid Lake within the Barren Lands.",
            StartDialogue = {"Snipping Stingers?","The Arid Lake, located in the Barren Lands, one once a vibrant and thriving oasis. Sometime over the last 1,000 years the lake dried up and left a shattered basin where the lake once resided. Now the only inhabitants are the scorpions that scour the land for carrion. We would like to send some of our league to investigate the lake but need the scorpions removed, that's where you come in."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-6", { "scorpion", "scorpion_skull" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("Area-Arid Lake") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-6", { "scorpion", "scorpion_skull" }, "Scorpion", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-6" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-6" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {--7
            Name = "It's Dangerous Alone",
            Cooldown = TimeSpan.FromHours(3),
            MobileKillName = "World Boss",
            Instructions = "Slay a [F2F5A9]world boss[-].",
            StartDialogue = {"Snipping Stingers?","There are beings of immense power that reside in the world of Celador. They are beyond the might of any single adventurer and must be engaged enmasse.  They include; Icharis a monstrous spider who resides in Dark Web Woods, Lord Lycaon a shapeshifter who patrols north of the swamp in Black Forest, and King Rhitkis, Lord of the Scorpions, who resides in the Arid Lake of Barren Lands."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-7", { "world_boss_scorpion", "world_boss_spider", "world_boss_werewolf" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("Area-Arid Lake") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-7", { "world_boss_scorpion", "world_boss_spider", "world_boss_werewolf" }, "World Boss", 1 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-7" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-7" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },

        {--8
            Name = "Goblin Raiders",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Goblins",
            Instructions = "Slay [F2F5A9]20 goblins[-] at the Goblin Camp in the Soutern Rim.",
            StartDialogue = {"Goblin Raiders?","Just east of Pyros Land in the Southern Rim a encampment of goblins has been spotted. Goblins are incredibly social and great caution should be used when engaging them as they can quickly overpower even the most seasoned warrior with shear numbers. Pyros has commissioned the league to enact 'population control' on the camp."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-8", { "goblin", "goblin_mage", "goblin_hq" } )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                    -- We want to force the player to be in the Rebel camp to complete the quest.
                    if( playerObj:IsInRegion("OrcCampInner") ) then
                        return Quests.RunTrackMobileKillCount( playerObj, "ExplorersLeaguePR9-8", { "goblin", "goblin_mage", "goblin_hq" }, "Goblins", 20 )
                    end
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-8" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ExplorersLeaguePR9-8" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },
    },

    ArtisansLeaguePR9 = 
    {
        {-- 1
            Name = "Guard Chain Tunics",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]10 iron chain tunic[-] for Jig.",
            StartDialogue = {"Guard Chain Tunics?","The guards here in Helm are constantly putting in orders for new armor. I honestly don't see how they are damaging their old armor as I hardly ever see them leave their stations. But who am I to judge, money is money. Can you craft me 10 iron chain tunics?"},
            NextStep = 2,
            Goals = {
                {"HasCraftedItem", "armor_chain_tunic", 10, "Material", "Iron"},
            },
        },

        {-- 2
            Name = "Guard Chain Tunics",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Guard Chain Tunics","These will do. I'm sure another order will come in soon."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "armor_chain_tunic", 10, "Material", "Iron"},
                
            },
            OnEnd = {
                {"TakeCraftedItems", "armor_chain_tunic", 10, "Material", "Iron"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 3
            Name = "Chain Leggings",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]10 iron chain leggings[-] for Jig.",
            StartDialogue = {"Chain Leggings?","Have you ever worn chain leggings? Well I have and I can tell you it's not for the weak at heart. The chafing alone will drive you mad! I'd rather take an arrow to the knee then have to walk a mile in chain. For some reason people buy them though -- bring me 10 iron chain leggings when you have time?"},
            NextStep = 4,
            Goals = {
                {"HasCraftedItem", "armor_chain_leggings", 10, "Material", "Iron"},
            },
        },

        {-- 4
            Name = "Chain Leggings",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Chain Leggings","Just thinking about having to wear these is giving me anxiety. Move along, I'm going to need a moment."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "armor_chain_leggings", 10, "Material", "Iron"},
            },
            OnEnd = {
                {"TakeCraftedItems", "armor_chain_leggings", 10, "Material", "Iron"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 5
            Name = "Chain Helms",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]10 iron chain helms[-] for Jig.",
            StartDialogue = {"Chain Helms?","What is it with all the chain orders we are getting lately? Do they know something I don't… Anyway I need 10 iron chain helms can you get on that?"},
            NextStep = 6,
            Goals = {
                {"HasCraftedItem", "armor_chain_helm", 10, "Material", "Iron"},
            },
        },

        {-- 6
            Name = "Chain Helms",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Chain Helms","These will work, thank you."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "armor_chain_helm", 10, "Material", "Iron"},
            },
            OnEnd = {
                {"TakeCraftedItems", "armor_chain_helm", 10, "Material", "Iron"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 7
            Name = "Ceremonial Plate",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]1 set of gold plate armor[-] for Jig.",
            StartDialogue = {"Ceremonial Plate?","We've had an order come in for a full set of gold plate armor. No doubt some lordlings ceremonial armor to be used during a fancy soiree on their country estate. I wouldn't be caught dead in the stuff, one hit from a she goat and you'd never get it back off. Just be quick about getting it made."},
            NextStep = 8,
            Goals = {
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Gold"},
                {"HasCraftedItem", "armor_fullplate_leggings", 1, "Material", "Gold"},
                {"HasCraftedItem", "armor_fullplate_helm", 1, "Material", "Gold"},
            },
        },

        {-- 8
            Name = "Ceremonial Plate",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Ceremonial Plate","Fine quality, you have done quite well. I'll have it delivered."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Gold"},
                {"HasCraftedItem", "armor_fullplate_leggings", 1, "Material", "Gold"},
                {"HasCraftedItem", "armor_fullplate_helm", 1, "Material", "Gold"},  
            },
            OnEnd = {
                {"TakeCraftedItems", "armor_fullplate_tunic", 1, "Material", "Gold"},
                {"TakeCraftedItems", "armor_fullplate_leggings", 1, "Material", "Gold"},
                {"TakeCraftedItems", "armor_fullplate_helm", 1, "Material", "Gold"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 9
            Name = "Dark Blades",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Craft [F2F5A9]10 obsidian katanas[-] for Jig.",
            StartDialogue = {"Dark Blades?","Finally, an order worth filling! We've a request for 10 obsidian katanas to be crafted. There is something about obsidian that draws the eye and arrests the soul. A darkness that reaches out to consume you. Take your time with this order, make me proud."},
            NextStep = 10,
            Goals = {
                {"HasCraftedItem", "weapon_katana", 10, "Material", "Obsidian"},
            },
        },

        {-- 10
            Name = "Dark Blades",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Dark Blades","These are magnificent you have outdone yourself this time. The client should be overjoyed."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "weapon_katana", 10, "Material", "Obsidian"},
            },
            OnEnd = {
                {"TakeCraftedItems", "weapon_katana", 10, "Material", "Obsidian"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },

        {-- 11
            Name = "Prismatic Armor",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Craft [F2F5A9]1 full plate tunic of each ore type[-] for Jig.",
            StartDialogue = {"Prismatic Armor?","The League of Artisans have been conducting metallurgy tests on various armor types and we're looking for someone to craft a full plate tunic of each ore type. Quality is less of a concern, just get them to me quickly."},
            NextStep = 12,
            Goals = {
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Iron"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Copper"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Gold"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Cobalt"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Obsidian"},
            },
        },

        {-- 12
            Name = "Prismatic Armor",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Prismatic Armor","Ah yes, they will work well."},
            Goals = {
                {"TurnIn"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Iron"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Copper"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Gold"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Cobalt"},
                {"HasCraftedItem", "armor_fullplate_tunic", 1, "Material", "Obsidian"},
            },
            OnEnd = {
                {"TakeCraftedItems", "armor_fullplate_tunic", 1, "Material", "Iron"},
                {"TakeCraftedItems", "armor_fullplate_tunic", 1, "Material", "Copper"},
                {"TakeCraftedItems", "armor_fullplate_tunic", 1, "Material", "Gold"},
                {"TakeCraftedItems", "armor_fullplate_tunic", 1, "Material", "Cobalt"},
                {"TakeCraftedItems", "armor_fullplate_tunic", 1, "Material", "Obsidian"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },

    },

    ArtisansLeagueValus = 
    {
        {-- 1: A Wedding Proposal
            Name = "A Wedding Proposal",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]10 skirts[-] and [F2F5A9]10 long sleeve shirts[-] for Jig.",
            StartDialogue = {"A Wedding Proposal?","Love is in the air, can you smell it? I SAID CAN YOU SMELL IT?! You know what I smell? Money. With all these wedding orders I need some help could you make me some skirts and long sleeved shirts? They must be made by you personally!"},
            NextStep = 2,
            Goals = {
                {"HasItemInBackpack", "clothing_skirt_legs", 10, playerObj},
                {"HasItemInBackpack", "clothing_long_sleeve_shirt_chest", 10, playerObj},
            },
        },

        {-- 2: A Wedding Proposal [Turn In]
            Name = "A Wedding Proposal",
            Instructions = "Speak to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] A Wedding Proposal","Get those skirts and long sleeved shirts done? Great, hand them over!"},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "clothing_skirt_legs", 10, playerObj},
                {"HasItemInBackpack", "clothing_long_sleeve_shirt_chest", 10, playerObj},
                {"TakeItem", "clothing_skirt_legs", 10},
                {"TakeItem", "clothing_long_sleeve_shirt_chest", 10},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", 100)
                    end},
                },
            },
        },

        {-- 3: Supplying The Calvary
            Name = "Supplying the Calvary",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]20 leather saddles[-] for Jig.",
            StartDialogue = {"Supplying the Calvary?","The Valus city guard have requested 20 leather saddles. Apparently they are tired of having to chase vagrants on foot. Could you fill this order for me?"},
            NextStep = 4,
            Goals = {
                {"HasItemInBackpack", "saddle_packed", 20, playerObj},
            },
        },

        {-- 4: Supplying The Calvary [Turn In]
            Name = "Supplying The Calvary",
            Instructions = "Give [F2F5A9]20 leather saddles[-] to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Supplying The Calvary","You made 20 leather saddles right? Don't short me!"},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "saddle_packed", 20, playerObj},
                {"TakeItem", "saddle_packed", 20},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", 100)
                    end},
                },
            },
        },

        {-- 5: Needle and the Blade
            Name = "Needle and the Blade",
            Cooldown = TimeSpan.FromHours(1),        
            Instructions = "Craft [F2F5A9]5 sets of leather armor[-] for Jig.",
            StartDialogue = {"Needle and the Blade?","Have you heard the old sayin, 'The weapon makes the warrior?'. It's wrong, what makes a warrior is his armor, and no armor is better than a sturdy suit of leather. I've a client that needs 5 suits made, can you do that?"},
            NextStep = 6,
            Goals = {
                {"HasItemInBackpack", "armor_leather_helm", 5, playerObj},
                {"HasItemInBackpack", "armor_leather_leggings", 5, playerObj},
                {"HasItemInBackpack", "armor_leather_chest", 5, playerObj},
            },
        },

        {-- 6: Needle and the Blade [Turn In]
            Name = "Needle and the Blade",
            Instructions = "Give [F2F5A9]5 leather armor suits[-] to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Needle and the Blade","These aren't the best work I've seen, but I've also seen far worse. They'll do and the coins for them will spend the same."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "armor_leather_helm", 5, playerObj},
                {"HasItemInBackpack", "armor_leather_leggings", 5, playerObj},
                {"HasItemInBackpack", "armor_leather_chest", 5, playerObj},
                {"TakeItem", "armor_leather_helm", 5},
                {"TakeItem", "armor_leather_leggings", 5},
                {"TakeItem", "armor_leather_chest", 5},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", 100)
                    end},
                },
            },
        },

        {-- 7: The Drape Matchmaker
            Name = "The Drape Matchmaker",   
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]3 bolts of silk cloth[-] for Jig.",
            StartDialogue = {"The Drape Matchmaker?","I've an order for some new drapes by a wealthy patron. They requested I make the drapes myself, however you could supply the silk cloth I need."},
            NextStep = 8,
            Goals = {
                {"HasItemInBackpack", "resource_silk_cloth", 3},
            },
        },

        {-- 8: The Drape Matchmaker [Turn In]
            Name = "The Drape Matchmaker",   
            Instructions = "Give [F2F5A9]3 bolts of silk cloth[-] to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] The Drape Matchmaker","You have the silk bolts? Excellent!"},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_silk_cloth", 3},
                {"TakeItem", "resource_silk_cloth", 3},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", 100)
                    end},
                },
            },
        },

        {-- 9: Leather Go
            Name = "Leather Go",   
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Craft [F2F5A9]1 vile leather, 5 beast leather, and 20 leather[-] for Jig.",
            StartDialogue = {"Leather Go?","I lost my wife some time ago. I turned myself to my work, the constant distraction of a full day filled the emptiness in my heart. I enjoy working with leather, it is firm yet malleable, as I must be. Could you get me 1 vile leather, 5 beast leather, and 20 leather."},
            NextStep = 10,
            Goals = {
                {"HasItemInBackpack", "resource_vile_leather", 1},
                {"HasItemInBackpack", "resource_beast_leather", 5},
                {"HasItemInBackpack", "resource_leather", 20},
            },
        },

        {-- 10: Leather Go [Turn In]
            Name = "Leather Go",   
            Instructions = "Give [F2F5A9]1 vile, 5 beast, and 20 leather[-] to [F2F5A9]Jig[-] in [F2F5A9]Helm[-].",
            EndDialogue = {"[Turn In] Leather Go","Thank you, this should keep my busy."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_vile_leather", 1},
                {"HasItemInBackpack", "resource_beast_leather", 5},
                {"HasItemInBackpack", "resource_leather", 20},
                {"TakeItem", "resource_vile_leather", 1},
                {"TakeItem", "resource_beast_leather", 5},
                {"TakeItem", "resource_leather", 20},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Artisans", 100)
                    end},
                },
            },
        },
    },

    ProcurerLeaguePR9 = 
    {

        {-- 1
            Name = "Sacred Sap",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Collect [F2F5A9]a nectar[-] for Naldren.",
            StartDialogue = {"Sacred Sap?","The Forest Walkers which reside in the Black Forest are the only source for a highly valuable form of sap known as “nectar”.  The healers here in Valus will pay handsomely for all they can get. You think you can secure some for me?"},
            NextStep = 2,
            Goals = {
                {"HasItemInBackpack", "animalparts_nectar", 1},
            },
        },

        {-- 2
            Name = "Sacred Sap",
            Instructions = "Give [F2F5A9]a nectar[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Sacred Sap","This is a remarkable sample, I trust you didn't put yourself in too much danger to acquire it."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "animalparts_nectar", 1},
                {"TakeItem", "animalparts_nectar", 1},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.GreaterFavor)
                    end},
                },
            },
        },

        {-- 3
            Name = "Cooking With Cactus",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Collect [F2F5A9]5 sacred cactus[-] for Naldren.",
            StartDialogue = {"Cooking With Cactus?","Scattered throughout the Barren Lands is a rare form of cactus that is said to be a key ingredient in high end Valusian cooking recipes. I'm not much on cactus myself, too gummy if you ask me. But I don't judge as long as they pay. Get me five sacred cactus and I'll see what I can do for you."},
            NextStep = 4,
            Goals = {
                {"HasItemInBackpack", "ingredient_sacred_cactus", 5},
            },
        },

        {-- 4
            Name = "Cooking With Cactus",
            Instructions = "Give [F2F5A9]5 sacred cactus[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Cooking With Cactus","Ouch, how did you carry all these without looking like a pincushion. This is, oww, asburd."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "ingredient_sacred_cactus", 5},
                {"TakeItem", "ingredient_sacred_cactus", 5},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 5
            Name = "Essential Investigations",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Collect [F2F5A9]15 magical essences[-] for Naldren.",
            StartDialogue = {"Essential Investigations?","With the invention of runebooks we've had a large influx of orders for magical essences. Apparently they are used to power those arcane contraptions. I refuse to use them myself, you know they scramble you and put you back together, not me, no way. Can you bring me back 15 of those essences though, that should get us started."},
            NextStep = 6,
            Goals = {
                {"HasItemInBackpack", "resource_essence", 15},
            },
        },

        {-- 6
            Name = "Essential Investigations",
            Instructions = "Give [F2F5A9]15 magical essences[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Essential Investigations","Oh these are purple, I thought you'd bring back the orange ones. But these should do."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_essence", 15},
                {"TakeItem", "resource_essence", 15},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 7
            Name = "Help Ore Harm",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Collect [F2F5A9]100 copper ore[-] for Naldren.",
            StartDialogue = {"Help Ore Harm?","Seems like nowadays you don't find much copper being used by artisans. I mean we get hundreds of orders for iron, cobalt, and obsidian items, even a few gold from time to time. But copper, it's seen better days. That might be changing though the League of Artisans has put in a big request for copper ore, I'll have you fill some of the order if you want."},
            NextStep = 8,
            Goals = {
                {"HasItemInBackpack", "resource_copper_ore", 100},
            },
        },

        {-- 8
            Name = "Help Ore Harm",
            Instructions = "Give [F2F5A9]100 copper ore[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Help Ore Harm","It looks like copper ore to me, hopefully things will pick up and we'll see more orders for copper."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_copper_ore", 100},
                {"TakeItem", "resource_copper_ore", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 9
            Name = "Collecting Old Magic",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Collect a [F2F5A9]Misha Rest: Colossal Map[-] for Naldren.",
            StartDialogue = {"Collecting Old Magic?","I've been told there are scrolls that only the keenest of treasure hunters can decipher. These scrolls tell the locations of colossal beasts that roam the wilds. A man would pay a hefty price to get ahold of one of those, especially one that offered a chance at a big game hunting. You feel up to finding me one?"},
            NextStep = 10,
            Goals = {
                {"HasItemInBackpack", "treasure_map_colossal_misha", 1},
            },
        },

        {-- 10
            Name = "Collecting Old Magic",
            Instructions = "Give a [F2F5A9]Misha Rest: Colossal Map[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Collecting Old Magic","Very nice, this goes into my personal collection."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "treasure_map_colossal_misha", 1},
                {"TakeItem", "treasure_map_colossal_misha", 1},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 11
            Name = "Light as a Feather",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Collect [F2F5A9]30 wild feathers[-] for Naldren.",
            StartDialogue = {"Light as a Feather?","Valusian high society is always looking for a new craze to get into. The flavor of the week appears to be wild feather beds. They only drop of harpies and from what I know of harpies they can't smell that good. But what do I know, I only put my pants on one leg at a time. If you can manage to find any let me know."},
            NextStep = 12,
            Goals = {
                {"HasItemInBackpack", "animalparts_wild_feather", 30},
            },
        },

        {-- 12
            Name = "Light as a Feather",
            Instructions = "Give [F2F5A9]30 wild feathers[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Light as a Feather","Oh Tethys, they do smell horrible. I'll never understand the aristocracy."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "animalparts_wild_feather", 30},
                {"TakeItem", "animalparts_wild_feather", 30},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.NormalFavor)
                    end},
                },
            },
        },

        {-- 13
            Name = "Enchanted Research",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Collect [F2F5A9]10 damaged shards of vanquishing[-] for Naldren.",
            StartDialogue = {"Enchanted Research?","You cannot win them all and the league has recently taken a big loss on a large shipment of enchanting essences we purchased. I've decided to try and downgrade them to see if I can move this stock. If you can bring me 10 Damaged Shards of Vanquishing I'll give you a normal Shard of Vanquishing in return."},
            NextStep = 14,
            Goals = {
                {"HasItemInBackpack", "essence_vanquishing_lesser", 10},
            },
        },

        {-- 14
            Name = "Enchanted Research",
            Instructions = "Give [F2F5A9]10 damaged shards of vanquishing[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Enchanted Research","Appreciate it, I might have a contact with the League of Artisans that can purify these and extract their power."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "essence_vanquishing_lesser", 10},
                {"TakeItem", "essence_vanquishing_lesser", 10},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.GreaterFavor)
                    end},
                    {"Item","essence_vanquishing", 1},
                },
            },
        },

        {-- 15
            Name = "Recipe Exchange",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Collect a [F2F5A9]crafting recipe[-] for Naldren.",
            StartDialogue = {"Recipe Exchange?","We are looking at doing a recipe exchange program with some of the artisan shops around Celador. I've been asked to pilot the program here in Valus. Figured I'd start with our supply chain participants, if you bring me a recipe I can give you a recipe for the same skill."},
            NextStep = 16,
            Goals = {
                {"Custom", function(playerObj)
                    local backpackObj = playerObj:GetEquippedObject("Backpack")
                    local hasItem = false
                    ForEachItemInContainerRecursive(backpackObj, function( item )
                        if(
                            item:HasObjVar("Recipe") -- is it recipe?
                            and item:HasModule("recipe") -- make sure!
                         ) then
                            hasItem = true
                            return false
                         end
                         return true
                    end)
                    return hasItem
                end},
            },
        },

        {-- 16
            Name = "Recipe Exchange",
            Instructions = "Give a [F2F5A9]crafting recipe[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Recipe Exchange","Nice doing business with you."},
            Goals = {
                {"TurnIn"},
                {"Custom", function(playerObj)
                    local backpackObj = playerObj:GetEquippedObject("Backpack")
                    local hasItem = false
                    ForEachItemInContainerRecursive(backpackObj, function( item )
                        if(
                            item:HasObjVar("Recipe") -- is it recipe?
                            and item:HasModule("recipe") -- make sure!
                         ) then
                            Quests.SetQuestData( playerObj, "ProcurersPR9-16", "Recipe", item:GetObjVar("Recipe") )
                            item:Destroy()
                            hasItem = true
                            return false
                         end
                         return true
                    end)
                    return hasItem
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.NormalFavor)

                        local recipeType = Quests.GetQuestData( playerObj, "ProcurersPR9-16", "Recipe" );
                        local skill = GetSkillForRecipe( recipeType )
                        local template = CraftingOrders.RecipeRewards[skill][ math.random( 1,#CraftingOrders.RecipeRewards[skill] ) ]
                        Create.InBackpack(template, playerObj)
                    end},
                },
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ProcurersPR9-16")
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "ProcurersPR9-16")
                end},
            },
        },

        {-- 17
            Name = "And My Pick",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Collect [F2F5A9]100 obsidian ore[-] for Naldren.",
            StartDialogue = {"And My Pick?","The League of Procurers are wanting to show appreciation for all the hard working harvesters that help keep us in business. In that spirit I'm trading a Deft Mining Pick for 100 obsidian ore. Don't look at me like that! I'm still running a business here. Do you want it or not?"},
            NextStep = 18,
            Goals = {
                {"HasItemInBackpack", "resource_obsidian_ore", 100},
            },
        },

        {-- 18
            Name = "And My Pick",
            Instructions = "Give [F2F5A9]100 obsidian ore[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] And My Pick","The League of Procurers appreciate you. Well, your business at least."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_obsidian_ore", 100},
                {"TakeItem", "resource_obsidian_ore", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.GreaterFavor)
                    end},
                    {"Item","tool_mining_pick_deft"},
                },
            },
        },

        {-- 19
            Name = "And My Axe",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Collect [F2F5A9]100 ash wood[-] for Naldren.",
            StartDialogue = {"And My Axe?","The League of Procurers are wanting to show appreciation for all the hard working harvesters that help keep us in business. In that spirit I'm trading a Deft Hatchet for 100 ash wood. Don't look at me like that! I'm still running a business here. Do you want it or not?"},
            NextStep = 20,
            Goals = {
                {"HasItemInBackpack", "resource_ash", 100},
            },
        },

        {-- 20
            Name = "And My Axe",
            Instructions = "Give [F2F5A9]100 ash wood[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] And My Axe","The League of Procurers appreciate you. Well, your business at least."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_ash", 100},
                {"TakeItem", "resource_ash", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.GreaterFavor)
                    end},
                    {"Item","tool_hatchet_deft"},
                },
            },
        },

        {-- 21
            Name = "And My Rod",
            Cooldown = TimeSpan.FromHours(3),
            Instructions = "Collect [F2F5A9]100 Razor-Fish[-] for Naldren.",
            StartDialogue = {"And My Rod?","The League of Procurers are wanting to show appreciation for all the hard working harvesters that help keep us in business. In that spirit I'm trading a Deft Fishing Rod for 100 Razor-Fish. Don't look at me like that! I'm still running a business here. Do you want it or not?"},
            NextStep = 22,
            Goals = {
                {"HasItemInBackpack", "resource_fish_razor", 100},
            },
        },

        {-- 22
            Name = "And My Rod",
            Instructions = "Give [F2F5A9]100 Razor-Fish[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] And My Rod","The League of Procurers appreciate you. Well, your business at least."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_fish_razor", 100},
                {"TakeItem", "resource_fish_razor", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", QuestsLeague.GreaterFavor)
                    end},
                    {"Item","tool_fishing_rod_deft"},
                },
            },
        },
    },

    ProcurerLeagueValus = 
    {
        {-- 1: Hard as Stone
            Name = "Hard as Stone",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Gather [F2F5A9]100 stone[-] for Naldren.",
            StartDialogue = {"Hard as Stone?","Valus though a great city is in constant need of repairs. Unlike those fools in Pyros who built their city from kindling, Valus is built with stone and therefore Valus is hard, hard as stone. Go, gather me stone to help aide in the city repairs."},
            NextStep = 2,
            Goals = {
                {"HasItemInBackpack", "resource_stone", 100},
            },
        },

        {-- 2: Hard as Stone [Turn In]
            Name = "Hard as Stone",
            Instructions = "Give [F2F5A9]100 stone[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Hard as Stone","Thank you! This stone will be put to good use strengthening the city."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_stone", 100},
                {"TakeItem", "resource_stone", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", 100)
                    end},
                },
            },
        },

        {-- 3: Cotton-Eyed Joel
            Name = "Cotton-Eyed Joel",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Gather [F2F5A9]30 fluffy cotton[-] for Naldren.",
            StartDialogue = {"Cotton-Eyed Joel?","I've a client named Joel that is quite handy with textiles. He is in contant need of fluffy cotton and I cannot seem to keep it stocked. It'd be great if you could secure some for me."},
            NextStep = 4,
            Goals = {
                {"HasItemInBackpack", "resource_cotton_fluffy", 30},
            },
        },

        {-- 4: Cotton-Eyed Joel [Turn In]
            Name = "Cotton-Eyed Joel",
            Instructions = "Give [F2F5A9]30 fluffy cotton[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Cotton-Eyed Joel","Goodness, you really came through for me. I'm so happy I could dance, but I won't."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_cotton_fluffy", 30},
                {"TakeItem", "resource_cotton_fluffy", 30},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", 100)
                    end},
                },
            },
        },

        {-- 5: Fish, Fish, Maps!
            Name = "Fish, Fish, Maps!",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Gather [F2F5A9]10 barrel fish, 10 tero fish, and 3 soaked maps[-] for Naldren.",
            StartDialogue = {"Fish, Fish, Maps!?","Here's the deal -- I'm not a fisherman myself, love the taste, but it's nasty work if you ask me. I've no desire to slog around in the mud waiting for that perfect cast. You on the other hand look like someone who wouldn't mind. Care to collect some items for me?"},
            NextStep = 6,
            Goals = {
                {"HasItemInBackpack", "resource_fish_barrel", 10},
                {"HasItemInBackpack", "resource_fish_tero", 10},
                {"HasItemInBackpack", "sos_map_1", 3},
            },
        },

        {-- 6: Fish, Fish, Maps! [Turn In]
            Name = "Fish, Fish, Maps!",
            Instructions = "Give [F2F5A9]10 barrel fish, 10 tero fish, and 3 soaked maps[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Fish, Fish, Maps!","*wrinkles his nose* Well that's what I asked for, but the smell wasn't part of the bargin. Best if you moved along before I lose my appetite."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_fish_barrel", 10},
                {"HasItemInBackpack", "resource_fish_tero", 10},
                {"HasItemInBackpack", "sos_map_1", 3},
                {"TakeItem", "resource_fish_barrel", 10},
                {"TakeItem", "resource_fish_tero", 10},
                {"TakeItem", "sos_map_1", 3},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", 100)
                    end},
                },
            },
        },

        {-- 7: The Green Thumb
            Name = "The Green Thumb",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Gather [F2F5A9]20 onion and 10 squash[-] for Naldren.",
            StartDialogue = {"The Green Thumb?","Feeding a city the size of Valus is no easy task. The markets are constantly in need of ingredients. Take onions for example, you cannot keep them stocked and when you can they burn your eyes. It's a rare thing that makes you cry both when it's there and gone. Bring me some onions, might as well get some squash too."},
            NextStep = 8,
            Goals = {
                {"HasItemInBackpack", "ingredient_onion", 20},
                {"HasItemInBackpack", "ingredient_squash", 10},
            },
        },

        {-- 8: The Green Thumb [Turn In]
            Name = "The Green Thumb",
            Instructions = "Give [F2F5A9]20 onion and 10 squash[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] The Green Thumb","*tears up* You done good champ. Now leave me be, I need a moment."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "ingredient_onion", 20},
                {"HasItemInBackpack", "ingredient_squash", 10},
                {"TakeItem", "ingredient_onion", 20},
                {"TakeItem", "ingredient_squash", 10},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", 100)
                    end},
                },
            },
        },

        {-- 9: A Seed to Plow
            Name = "A Seed to Plow",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Gather [F2F5A9]3 green pepper, 3 cucumber, and 3 eggplant seeds[-] for Naldren.",
            StartDialogue = {"A Seed to Plow?","The city has decided to start stocking up on seeds for the next planting season. If you run across any green pepper, cucumber, or eggplant seeds I'll take them off you hands."},
            NextStep = 10,
            Goals = {
                {"HasItemInBackpack", "seed_greenpepper", 3},
                {"HasItemInBackpack", "seed_cucumber", 3},
                {"HasItemInBackpack", "seed_eggplant", 3},
            },
        },

        {-- 10: A Seed to Plow [Turn In]
            Name = "A Seed to Plow",
            Instructions = "Give [F2F5A9]3 green pepper, 3 cucumber, and 3 eggplant seeds[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] A Seed to Plow","These will do well. I hope they weren't too difficult for you to obtain."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "seed_greenpepper", 3},
                {"HasItemInBackpack", "seed_cucumber", 3},
                {"HasItemInBackpack", "seed_eggplant", 3},
                {"TakeItem", "seed_greenpepper", 3},
                {"TakeItem", "seed_cucumber", 3},
                {"TakeItem", "seed_eggplant", 3},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", 100)
                    end},
                },
            },
        },

        {-- 11: Bones to Bread
            Name = "Bones to Bread",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Gather [F2F5A9]30 bones[-] for Naldren.",
            StartDialogue = {"Bones to Bread?","Valusian cuisine is a delightful and sometimes horrifying treat. Take the mages of Valus, they make a special bread for their ceremonies that has ground bonemeal as a primary ingredient. Not my cup of tea, I'll tell you that. But they pay and that's what matters?"},
            NextStep = 12,
            Goals = {
                {"HasItemInBackpack", "animalparts_bone", 30},
            },
        },

        {-- 12: Bones to Bread [Turn In]
            Name = "Bones to Bread",
            Instructions = "Give [F2F5A9]30 bones[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Bones to Bread","Listen, the less we talk about where you got these the better off we'll both be."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "animalparts_bone", 30},
                {"TakeItem", "animalparts_bone", 30},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", 100)                        
                    end},
                },
            },
        },

        {-- 13: Marvelous Melons
            Name = "Marvelous Melons",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Gather [F2F5A9]20 melon meals[-] for Naldren.",
            StartDialogue = {"Marvelous Melons?","I love melons! They're round, they're juicey, and they don't judge me... As far as I'm concerned their a miracle food that everyone should have handy. Actually, you care to get me some melon meals? All this talk has made me hungry."},
            NextStep = 14,
            Goals = {
                {"HasItemInBackpack", "item_food_melon_meal", 20},
            },
        },

        {-- 14: Marvelous Melons [Turn In]
            Name = "Marvelous Melons",
            Instructions = "Give [F2F5A9]20 melon meals[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Marvelous Melons","*takes a bite of melon* Now this is life, am I right? I'd offer you some but we're not that close."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "item_food_melon_meal", 20},
                {"TakeItem", "item_food_melon_meal", 20},

            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", 100)
                    end},
                },
            },
        },

        {-- 15: Wood You Mind
            Name = "Wood You Mind",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Gather [F2F5A9]200 ash and 100 blightwood[-] for Naldren.",
            StartDialogue = {"Wood You Mind?","The Valus guards are commissioning a large quantity of ash and blightwood bows for craftsmen around the city. This would be a good time to stock up on ash and blightwood, you can help me with that, right?"},
            NextStep = 16,
            Goals = {
                {"HasItemInBackpack", "resource_ash", 200},
                {"HasItemInBackpack", "resource_blightwood", 100},
            },
        },

        {-- 16: Wood You Mind [Turn In]
            Name = "Wood You Mind",
            Instructions = "Give [F2F5A9]200 ash and 100 blightwood[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Wood You Mind","Good, this batch of wood will go fast. I appreciate you help."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "resource_ash", 200},
                {"HasItemInBackpack", "resource_blightwood", 100},
                {"TakeItem", "resource_ash", 200},
                {"TakeItem", "resource_blightwood", 100},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", 100)
                    end},
                },
            },
        },

        {-- 17: Gems to Craft
            Name = "Gems to Craft",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Gather [F2F5A9]10 topaz gems and 20 gold bars[-] for Naldren.",
            StartDialogue = {"Gems to Craft?","Innovation, that's how to get ahead in this world, well that and a lot of luck. The League of Procurers is researching the posibility of crafting jewelery as opposed to bartering with the dungeoneers. We have artisans hard at work perfecting the craft but we need materials. Can you get some topaz and gold bars?"},
            NextStep = 18,
            Goals = {
                {"HasItemInBackpack", "topaz_gem", 10},
                {"HasItemInBackpack", "resource_gold", 20},
            },
        },

        {-- 18: Gems to Craft [Turn In]
            Name = "Gems to Craft",
            Instructions = "Give [F2F5A9]10 topaz gems and 20 gold bars[-] to [F2F5A9]Naldren[-] in [F2F5A9]Valus[-].",
            EndDialogue = {"[Turn In] Gems to Craft","These should do fine. I'll let you know if they make any progress, keep a sharp eye for other gems we might need more soon."},
            Goals = {
                {"TurnIn"},
                {"HasItemInBackpack", "topaz_gem", 10},
                {"HasItemInBackpack", "resource_gold", 20},
                {"TakeItem", "topaz_gem", 10},
                {"TakeItem", "resource_gold", 20},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Procurers", 100)
                    end},
                },
            },
        },
    },
    -- [[ ELDEIR CITIZENSHIP QUESTS ]]
    JoinTownshipEldeir = 
    {
        {-- 1: Eldeir Citizenship
            Name = "Eldeir Citizenship",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Claim [F2F5A9]Eldeir[-] citizenship from [F2F5A9]Mayor Orville[-].",
            StartDialogue = {"Eldeir Citizenship?","New Celador is changing and not always for the better, there is safety in numbers. Eldeir is always in need of fine folks, such as yourself, to help our village grow'n prosper. Think it over at least will you?"},
            EndDialogue = {"[Turn In] Eldeir Citizenship","You've made the right decision! Be sure to head over to our local trade board in front of the inn and see how you can help out."},
            
            Requirements = {
                {"Custom", function(playerObj)
                    if ( 
                        not playerObj:HasObjVar(TownshipsHelper.TownshipObjVar) 
                        and Militia.GetId(playerObj) == nil 
                        and not IsMurderer(playerObj) 
                    )
                    then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
                --{"InRegion", "EldeirVillage", true},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        TownshipsHelper.AddPlayerToTownship( playerObj, "Eldeir" )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                    end},
                },
            },
        },
    },
    LeaveTownshipEldeir = 
    {
        {-- 1: Renounce Eldeir Citizenship
            Name = "Renounce Eldeir Citizenship",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Renounce [F2F5A9]Eldeir[-] citizenship with [F2F5A9]Mayor Orville[-].",
            StartDialogue = {"Renounce Eldeir Citizenship?","This is horrible news! Take some time to think it over."},
            EndDialogue = {"[Turn In] Renounce Eldeir Citizenship","I am sad to see you go, you are always welcome to come back."},


            Requirements = {
                {"Custom", function(playerObj)
                    local leaveTime = playerObj:GetObjVar("CanLeaveTownship") or DateTime.MinValue
                    if ( 
                        playerObj:GetObjVar(TownshipsHelper.TownshipObjVar) == "Eldeir" 
                        and Militia.GetId(playerObj) == nil
                        and ( DateTime.Compare(DateTime.UtcNow, leaveTime) > 0 ) 
                    )
                    then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        TownshipsHelper.RemovePlayerFromTownship( playerObj )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                        --QuestsLeague.SetLeagueCurrecy( playerObj, "Eldeir", 0 )
                    end},
                },
            },
        },
    },

    -- [[ HELM CITIZENSHIP QUESTS ]]
    JoinTownshipHelm = 
    {
        {-- 1: Helm Citizenship
            Name = "Helm Citizenship",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Claim [F2F5A9]Helm[-] citizenship from [F2F5A9]Mayor Hameka[-].",
            StartDialogue = {"Helm Citizenship?","We are always in need of strong arms and able sword here in Helm. Unless I've missed my mark, I'd say you would fit that bill perfectly. What do you say, mull it over and get back to me?"},
            EndDialogue = {"[Turn In] Helm Citizenship","I knew you'd come to the right decision!"},
            Requirements = {
                {"Custom", function(playerObj)
                    if ( not playerObj:HasObjVar(TownshipsHelper.TownshipObjVar) and Militia.GetId(playerObj) == nil  and not IsMurderer(playerObj) )
                    then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        TownshipsHelper.AddPlayerToTownship( playerObj, "Helm" )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                    end},
                },
            },
        },
    },
    LeaveTownshipHelm = 
    {
        {-- 1: Renounce Helm Citizenship
            Name = "Renounce Helm Citizenship",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Renounce [F2F5A9]Helm[-] citizenship with [F2F5A9]Mayor Hameka[-].",
            StartDialogue = {"Renounce Helm Citizenship?","This is horrible news! Take some time to think it over."},
            EndDialogue = {"[Turn In] Renounce Helm Citizenship","You'll be sorely missed, don't hesitate to let me know if you change your mind!"},
            Requirements = {
                {"Custom", function(playerObj)
                    local leaveTime = playerObj:GetObjVar("CanLeaveTownship") or DateTime.MinValue
                    if ( 
                        playerObj:GetObjVar(TownshipsHelper.TownshipObjVar) == "Helm" 
                        and Militia.GetId(playerObj) == nil
                        and ( DateTime.Compare(DateTime.UtcNow, leaveTime) > 0 ) 
                    )
                    then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        TownshipsHelper.RemovePlayerFromTownship( playerObj )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                        --QuestsLeague.SetLeagueCurrecy( playerObj, "Helm", 0 )
                    end},
                },
            },
        },
    },

    -- [[ PYROS CITIZENSHIP QUESTS ]]
    JoinTownshipPyros = 
    {
        {-- 1: Pyros Citizenship
            Name = "Pyros Citizenship",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Claim [F2F5A9]Pyros[-] citizenship from [F2F5A9]Mayor Rose[-].",
            StartDialogue = {"Pyros Citizenship?","To the east we have the wilds of the Southern Rim and to the north the dunes of Jhalir.  I tell you, there is no better place to hang your fishing pole than right here! We're always in need of fresh blood and able explorers to help the interested of Pyro's. Give it some thought, and get back to me?"},
            EndDialogue = {"[Turn In] Pyros Citizenship","You won't be disappointed with your decision! Chalk another deckhand up for Pyro's Landing."},
            Requirements = {
                {"Custom", function(playerObj)
                    if ( not playerObj:HasObjVar(TownshipsHelper.TownshipObjVar) and Militia.GetId(playerObj) == nil  and not IsMurderer(playerObj) )
                    then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        TownshipsHelper.AddPlayerToTownship( playerObj, "Pyros" )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                    end},
                },
            },
        },
    },
    LeaveTownshipPyros = 
    {
        {-- 1: Renounce Pyros Citizenship
            Name = "Renounce Pyros Citizenship",
            Cooldown = TimeSpan.FromHours(1),
            Instructions = "Renounce [F2F5A9]Pyros[-] citizenship with [F2F5A9]Mayor Rose[-].",
            StartDialogue = {"Renounce Pyros Citizenship?","This is horrible news! Take some time to think it over."},
            EndDialogue = {"[Turn In] Renounce Pyros Citizenship","I am sad it has come to this. Pyro's Landing will endure and you will always be welcome here."},
            Requirements = {
                {"Custom", function(playerObj)
                    local leaveTime = playerObj:GetObjVar("CanLeaveTownship") or DateTime.MinValue
                    if ( 
                        playerObj:GetObjVar(TownshipsHelper.TownshipObjVar) == "Pyros" 
                        and Militia.GetId(playerObj) == nil
                        and ( DateTime.Compare(DateTime.UtcNow, leaveTime) > 0 ) 
                    )
                    then return true else return false end
                end},
            },
            Goals = {
                {"TurnIn"},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        TownshipsHelper.RemovePlayerFromTownship( playerObj )
                        playerObj:SendMessage("MilitiaApplyBuffs")
                        --QuestsLeague.SetLeagueCurrecy( playerObj, "Pyros", 0 )
                    end},
                },
            },
        },
    },
    MonolithEntranceQuests = 
    {
        {--1
            Name = "Risen Cultists",
            Cooldown = TimeSpan.FromHours(1),
            MobileKillName = "Cultists",
            Instructions = "Slay [F2F5A9]20 Risen Cultist[-] outside the sealed entrance to the Monolith.",
            StartDialogue = {"Risen Cultists?",""},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "MonolithEntranceQuests-1", "monolith_cultist_risen" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                        return Quests.RunTrackMobileKillCount( playerObj, "MonolithEntranceQuests-1", "monolith_cultist_risen", "Risen Cultists", 20 )
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "MonolithEntranceQuests-1" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "MonolithEntranceQuests-1" )
                end},
            },
            Rewards = {
                {
                    {"Custom", function(playerObj)
                        QuestsLeague.OnCompleteQuest(playerObj,"Explorers", QuestsLeague.NormalFavor)
                    end},
                    {"Item","monolith_schematic_resource"},
                },
            },
        },
    },

    MonolithQuests = 
    {
        {--1
            Name = "Bringing War",
            Cooldown = TimeSpan.FromHours(3),
            MobileKillName = "Warbringers",
            Instructions = "Slay [F2F5A9]10 Cultist Warbringers[-] on the 2nd floor of the Monolith.",
            StartDialogue = {"Bringing War [Quest]","Armsmaster Finkle prides himself on the training of his elite order.  Cultist Warbringers are his prized students and capable of commanding their allies in battle. Kill them, kill them all!"},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "MonolithQuests-1", "monolith_cultist_advanced_tank" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                        return Quests.RunTrackMobileKillCount( playerObj, "MonolithQuests-1", "monolith_cultist_advanced_tank", "Cultist Warbringer", 10 )
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "MonolithQuests-1" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "MonolithQuests-1" )
                end},
            },
            Rewards = {
                {
                    {"Item","monolith_keeper_reward_box"},
                },
            },
        },
        {--2
            Name = "A Single Spark",
            Cooldown = TimeSpan.FromHours(3),
            MobileKillName = "Sparkmasters",
            Instructions = "Slay [F2F5A9]5 Cultist Sparkmasters[-] on the 2nd floor of the Monolith.",
            StartDialogue = {"A Single Spark [Quest]","Sparks are raw Echo energy that has no medium to flow through. Somehow Jianna's order found a way to control these sparks for their own ends. Any Sparkmasters you find should be dealt with harshly."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "MonolithQuests-2", "monolith_cultist_sparkmaster" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                        return Quests.RunTrackMobileKillCount( playerObj, "MonolithQuests-2", "monolith_cultist_sparkmaster", "Cultist Sparkmaster", 5 )
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "MonolithQuests-2" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "MonolithQuests-2" )
                end},
            },
            Rewards = {
                {
                    {"Item","monolith_keeper_reward_box"},
                },
            },
        },
        {--3
            Name = "Deconstruction",
            Cooldown = TimeSpan.FromHours(3),
            MobileKillName = "Unstable Construct",
            Instructions = "Slay [F2F5A9]1 Unstable Construct[-] on the 2nd floor of the Monolith.",
            StartDialogue = {"Deconstruction [Quest]","A vast army of constructs was left by the Xor to help me maintain order in the Monolith. Sadly they have slowly drained of power or been 'repurposed' by the cultists. Some of these constructs have grown unstable and pose a great risk to the stability of the Monolith itself."},
            OnStart = {
                {"Custom", function(playerObj)
                    Quests.InitTrackMobileKillCount( playerObj, "MonolithQuests-3", "construct_unstable" )
                end},
            },
            Goals = {
                {"Custom", function(playerObj)
                        return Quests.RunTrackMobileKillCount( playerObj, "MonolithQuests-3", "construct_unstable", "Unstable Construct", 1 )
                end},
            },
            OnEnd = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "MonolithQuests-3" )
                end},
            },
            OnFail = {
                {"Custom", function(playerObj)
                    Quests.ClearQuestData( playerObj, "MonolithQuests-3" )
                end},
            },
            Rewards = {
                {
                    {"Item","monolith_keeper_reward_box"},
                },
            },
        },
    },
    TestQuests = 
    {
        {-- Test If Pouch is Empty
            Name = "Empty Pouch",
            Instructions = "Deliver 1 [F2F5A9]Pouches[-] to [F2F5A9]Quest Test.[-]",
            MarkerLocs = {
                QuestMarkers.TailorTrinit,
            },
            EndDialogueHasEligibleCheck = true,
            EndDialogue = {"[QUEST] Just give me the pouch.", "Yeah yeah yeah, we're done."},
            IneligibleDialogue = { "[QUEST] Just give me the pouch.", "Either you don't have the pouch or it's not empty."},
            Requirements = {
                {"HasCraftedItem", "pouch", 1},
            },
            Goals = {
                {"TurnIn"},
            },
            OnEnd = {
                {"TakeCraftedItems", "pouch", 1},
            },
        },
    },

    -- Hunting Challenges
    HuntingChallenges = 
    { 
        { -- 1
            Name = "Messy Business",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 50,
                Key = "HuntingChallenges1",
                Type = "DynamicSpawn",
                QuestArgs = 
                {
                    { "KilledMonster" }
                },
                Count = 40,
                Name = "Enemies Slain",
                NamePlural = "Enemies Slain",
            },
            Instructions = "Slay [F2F5A9]40 enemies[-] at a [F2F5A9]dynamic spawn[-] event.",
        },

        { -- 2
            Name = "Get the Big One!",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 50,
                Key = "HuntingChallenges2",
                Type = "DynamicSpawn",
                QuestArgs = 
                {
                    { "KilledBoss" }
                },
                Count = 1,
                Name = "Bosses Slain",
                NamePlural = "Bosses Slain",
            },
            Instructions = "Slay [F2F5A9]1 boss[-] at a [F2F5A9]dynamic spawn[-] event.",
        },

        { -- 3
            Name = "Swords, Boards, and Bandits",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "HuntingChallenges3",
                Type = "Hunting",
                QuestArgs = 
                {
                    true,
                    "Bandit"
                },
                Count = 20,
                Name = "Bandit",
                NamePlural = "Bandits",
            },
            Instructions = "Kill [F2F5A9]20 bandits[-] throughout [F2F5A9]New Celeador[-].",
        },

        { -- 4
            Name = "The Weight of Scales",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "HuntingChallenges4",
                Type = "Hunting",
                QuestArgs = 
                {
                    true,
                    "Reptile"
                },
                Count = 20,
                Name = "Reptile",
                NamePlural = "Reptiles",
            },
            Instructions = "Kill [F2F5A9]20 reptiles[-] throughout [F2F5A9]New Celeador[-].",
        },

        { -- 5
            Name = "They Do, in the Woods.",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "HuntingChallenges5",
                Type = "Hunting",
                QuestArgs = 
                {
                    { "dire_bear", "grizzly_bear", "brown_bear", "black_bear", "great_bear", "rabid_bear" },
                    true
                },
                Count = 20,
                Name = "Bear",
                NamePlural = "Bears",
            },
            Instructions = "Kill [F2F5A9]20 bears[-] (excluding arctic bears) throughout [F2F5A9]New Celeador[-].",
        },

        { -- 6
            Name = "Chilly Hunters",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "HuntingChallenges6",
                Type = "Hunting",
                QuestArgs = 
                {
                    true,
                    "TundraPredator"
                },
                Count = 20,
                Name = "Predator",
                NamePlural = "Predators",
            },
            Instructions = "Kill [F2F5A9]predators[-] in [F2F5A9]The Frozen Tundra[-].",
        },

        { -- 7
            Name = "Little Green Menace",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "HuntingChallenges7",
                Type = "Hunting",
                QuestArgs = 
                {
                    true,
                    "Goblin"
                },
                Count = 20,
                Name = "Goblin",
                NamePlural = "Goblins",
            },
            Instructions = "Kill [F2F5A9]20 goblins[-] throughout [F2F5A9]New Celeador[-].",
        },
        
    },

    PVPChallenges = 
    {
        { -- 1
            Name = "[800000]Crushing Sprites[-]",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                PVPTokens = 25,
                Key = "PVPChallenges1",
                Type = "Hunting",
                Region = "WaterfallMine",
                QuestArgs = 
                {
                    { "rock_sprite" },
                    true
                },
                Count = 20,
                Name = "Rock Sprite",
                NamePlural = "Rock Sprites",
            },
            Instructions = "Kill [F2F5A9]20 Rock Sprites[-] in the [F2F5A9]Misty Caverns[-].",
        },

        { -- 2
            Name = "[800000]Grinding Golems[-]",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                PVPTokens = 25,
                Key = "PVPChallenges2",
                Type = "Hunting",
                Region = "WaterfallMine",
                QuestArgs = 
                {
                    { "rock_golem" },
                    true
                },
                Count = 10,
                Name = "Rock Golem",
                NamePlural = "Rock Golems",
            },
            Instructions = "Kill [F2F5A9]10 Rock Golems[-] in the [F2F5A9]Misty Caverns[-].",
        },

        { -- 3
            Name = "[800000]Dousing Golems[-]",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                PVPTokens = 25,
                Key = "PVPChallenges3",
                Type = "Hunting",
                Region = "WaterfallMine",
                QuestArgs = 
                {
                    { "lava_golem" },
                    true
                },
                Count = 1,
                Name = "Lava Golem",
                NamePlural = "Lava Golems",
            },
            Instructions = "Kill a[F2F5A9]Lava Golem[-] in the [F2F5A9]Misty Caverns[-].",
        },

        { -- 4
            Name = "[800000]Rocks to Dust[-]",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                PVPTokens = 75,
                Key = "PVPChallenges4",
                Type = "Hunting",
                Region = "WaterfallMine",
                QuestArgs = 
                {
                    true,
                    "Golem"
                },
                Count = 40,
                Name = "Golem",
                NamePlural = "Golems",
            },
            Instructions = "Kill [F2F5A9]40 golems[-] in the [F2F5A9]Misty Caverns[-].",
        },

        { -- 5
            Name = "[800000]A Mighty Harvest[-]",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                PVPTokens = 50,
                Key = "PVPChallenges5",
                Type = "Gathering",
                Region = "WaterfallMine",
                QuestArgs = 
                {
                    "Mining",
                    { "IronOre", "CopperOre", "GoldOre", "CobaltOre", "ObsidianOre" }
                },
                Count = 100,
                Name = "Ore (PVP)",
                NamePlural = "Ore (PVP)",
            },
            Instructions = "Mine [F2F5A9]100 ore[-] from the [F2F5A9]Misty Caverns[-].",
        },

        { -- 6
            Name = "[800000]Not In My House![-]",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                PVPTokens = 50,
                Key = "PVPChallenges6",
                Type = "DynamicSpawn",
                Region = "WaterfallMine",
                QuestArgs = 
                {
                    { "KilledMonster" }
                },
                Count = 40,
                Name = "Enemies Slain (PVP)",
                NamePlural = "Enemies Slain (PVP)",
            },
            Instructions = "Slay [F2F5A9]20 enemies[-] at a [F2F5A9]dynamic spawn[-] event in the [F2F5A9]Misty Caverns[-].",
        },

        { -- 7
            Name = "[800000]RUUUNN![-]",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                PVPTokens = 75,
                Key = "PVPChallenges7",
                Type = "DynamicSpawn",
                Region = "WaterfallMine",
                QuestArgs = 
                {
                    { "KilledBoss" }
                },
                Count = 1,
                Name = "Bosses Slain (PVP)",
                NamePlural = "Bosses Slain (PVP)",
            },
            Instructions = "Slay a [F2F5A9]boss[-] at a [F2F5A9]dynamic spawn[-] event in the [F2F5A9]Misty Caverns[-].",
        },
    },

    CraftingChallenges = 
    {
        { -- 1
            Name = "Ingots",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "CraftingChallenges1",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Resources",
                    { "resource_iron", "resource_copper", "resource_gold", "resource_cobalt", "resource_obsidian" }
                },
                Count = 100,
                Name = "Ingot",
                NamePlural = "Ingots",
            },
            Instructions = "Craft [F2F5A9]100 ingots[-] at an anvil.",
        },

        { -- 2
            Name = "Boards",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "CraftingChallenges2",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Resources",
                    { "resource_boards", "resource_boards_ash", "resource_blightwood_boards" }
                },
                Count = 100,
                Name = "Board",
                NamePlural = "Boards",
            },
            Instructions = "Craft [F2F5A9]100 boards[-] at an carpentry table.",
        },

        { -- 3
            Name = "Textiles",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "CraftingChallenges3",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Resources",
                    { "resource_bolt_of_cloth", "resource_bolt_of_cloth_quilted", "resource_silk_cloth", "resource_leather", "resource_beast_leather", "resource_vile_leather" }
                },
                Count = 500,
                Name = "Textile",
                NamePlural = "Textiles",
            },
            Instructions = "Craft [F2F5A9]100 cloth or leather[-] at a loom.",
        },

        { -- 4
            Name = "Potions",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "CraftingChallenges4",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Potions",
                    true
                },
                Count = 20,
                Name = "Potion",
                NamePlural = "Potions",
            },
            Instructions = "Craft [F2F5A9]20 potions[-] at an alchemy table.",
        },

        { -- 5
            Name = "Scrolls",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "CraftingChallenges5",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Scrolls",
                    true
                },
                Count = 20,
                Name = "Scroll",
                NamePlural = "Scrolls",
            },
            Instructions = "Craft [F2F5A9]20 scrolls[-] at an inscription table.",
        },

        { -- 6
            Name = "Meals",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "CraftingChallenges6",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Food",
                    true
                },
                Count = 20,
                Name = "Meal",
                NamePlural = "Meals",
            },
            Instructions = "Cook [F2F5A9]20 meals[-] using a cooking pot.",
        },

        { -- 7
            Name = "Weapons",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "CraftingChallenges7",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Weapons",
                    true,
                    true,
                },
                Count = 20,
                Name = "Weapon",
                NamePlural = "Weapons",
            },
            Instructions = "Craft [F2F5A9]20 weapons[-].",
        },

        { -- 8
            Name = "Armor",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "CraftingChallenges8",
                Type = "Crafting",
                QuestArgs = 
                {
                    "Armor",
                    true,
                    true,
                },
                Count = 20,
                Name = "Armor",
                NamePlural = "Armor",
            },
            Instructions = "Craft [F2F5A9]20 armor[-].",
        },

    },

    GatheringChallenges = 
    {
        { -- 1
            Name = "Lumberjacking",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "GatheringChallenges1",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Lumberjacking",
                    true
                },
                Count = 100,
                Name = "Wood",
                NamePlural = "Wood",
            },
            Instructions = "Chop [F2F5A9]100 wood[-].",
        },

        { -- 2
            Name = "Mining",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "GatheringChallenges2",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Mining",
                    { "IronOre", "CopperOre", "GoldOre", "CobaltOre", "ObsidianOre" }
                },
                Count = 100,
                Name = "Ore",
                NamePlural = "Ore",
            },
            Instructions = "Mine [F2F5A9]100 ore[-].",
        },

        { -- 3
            Name = "Hides & Silk",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "GatheringChallenges3",
                Type = "Harvesting",
                QuestArgs = 
                {
                    {"Skinning", "BareHands"},
                    { "animalparts_leather_hide", "animalparts_beast_leather_hide", "animalparts_vile_leather_hide", "animalparts_spider_silk", "LeatherHide", "BeastLeatherHide", "VileLeatherHide", "Silk" }
                },
                Count = 25,
                Name = "Hide/Silk",
                NamePlural = "Hides/Silk",
            },
            Instructions = "Skin [F2F5A9]25 hides or silk[-] from corpses.",
        },

        { -- 4
            Name = "Meat",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "GatheringChallenges4",
                Type = "Harvesting",
                QuestArgs = 
                {
                    "Skinning",
                    { "animalparts_mystery_meat", "animalparts_stringy_meat", "animalparts_tough_meat", "animalparts_tender_meat", "animalparts_gourmet_meat" }
                },
                Count = 50,
                Name = "Meat",
                NamePlural = "Meat",
            },
            Instructions = "Skin [F2F5A9]50 meat[-] from corpses.",
        },

        { -- 5
            Name = "Cotton",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "GatheringChallenges5",
                Type = "Harvesting",
                QuestArgs = 
                {
                    "BareHands",
                    { "Cotton", "CottonFluffy", "FieldCotton", "BarrensCotton", "BarrensCottonFluffy", "FieldCottonFluffy" }
                },
                Count = 50,
                Name = "Cotton",
                NamePlural = "Cotton",
            },
            Instructions = "Pick [F2F5A9]50 cotton or fluffy cotton[-].",
        },

        { -- 6
            Name = "Fishing",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "GatheringChallenges6",
                Type = "Gathering",
                QuestArgs = 
                {
                    "Fishing",
                    { "resource_fish_barrel", "resource_fish_tero", "resource_fish_spotted_tero", "resource_fish_razor", "resource_fish_aether" }
                },
                Count = 20,
                Name = "Fish",
                NamePlural = "Fish",
            },
            Instructions = "Catch [F2F5A9]20 fish[-].",
        },

        { -- 7
            Name = "Seeds",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "GatheringChallenges7",
                Type = "Harvesting",
                QuestArgs = 
                {
                    "Skinning",
                    { "seed_onion", "seed_carrot", "seed_strawberry", "seed_corn", "seed_squash", "seed_greenleaflettuce", "seed_redleaflettuce", "seed_buttonmushroom", "seed_melon", "seed_greenpepper", "seed_cucumber", "seed_eggplant" }
                },
                Count = 20,
                Name = "Seed",
                NamePlural = "Seeds",
            },
            Instructions = "Skin [F2F5A9]20 seeds[-] from corpses.",
        },

        { -- 8
            Name = "Reagents",
            Cooldown = TimeSpan.FromHours(1),
            HideTracking = true,
            QuestData = {
                Tokens = 25,
                Key = "GatheringChallenges8",
                Type = "Harvesting",
                QuestArgs = 
                {
                    "BareHands",
                    { "Ginseng", "LemonGrass", "Mushrooms", "Moss" }
                },
                Count = 50,
                Name = "Reagent",
                NamePlural = "Reagents",
            },
            Instructions = "Pick [F2F5A9]50 reagents (lemon grass, moss, ginseng, or mushrooms)[-].",
        },

    },
}