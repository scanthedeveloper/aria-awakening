--For poi reference, check map_locations.lua

MissionData =
{
	--Dispatchers distribute categories of missions
	MissionCategories = 
	{
		Combat = 
		{
			DisplayName = "Combat",

			--The min skill required before a mission can be offered
			DifficultyLevels =
			{
				0, -- 1
				40, -- 2
				60, -- 3
				80, -- 4
				90, -- 5
			},
		},
		Taming = 
		{
			DisplayName = "Animal Taming",
			DifficultyLevels =
			{
				0, -- 1
				40, -- 2
				60, -- 3
				80, -- 4
				90, -- 5
			},
		},
		HarvestMining = 
		{
			DisplayName = "Mining",
			DifficultyLevels =
			{
				0, --  1
				25, -- 2
				40, -- 3
				60, -- 4
				80, -- 5
			},

		},
		HarvestLumberjack = 
		{
			DisplayName = "Logging",
			DifficultyLevels =
			{
				0, -- 1
				40, -- 2
				80, -- 3
			},
		},
		HarvestFabrication = 
		{
			DisplayName = "Gathering",
			DifficultyLevels =
			{
				0, -- 1
				40, -- 2
				80, -- 3
			},
		},
		Fishing = 
		{
			DisplayName = "Fishing",
			DifficultyLevels =
			{
				0, -- 1
				25, -- 2
				45, -- 3
				65, -- 4
				85, -- 5
			},
		},
	},

	Missions = 
	{
		AllSubregions =
		{
			--If you don't want prefab to spawn with boss, use PrefabName = ""
			Wolves_1 =
			{
				Difficulty = 1,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "WolfLair_1",
				Title = "Wolves on the prowl",
				Description = "A wolfpack has moved in and has been laying waste to honest folks' livestock. I can offer you some coin if you wouldn't mind running them out?",
				RewardMin = 100,
				RewardMax = 200,
			},
			Bears_1 =
			{
				Difficulty = 1,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "BearLair_1",
				Title = "Bears Sighted",
				Description = "A couple of amatuer bandits have been giving people trouble. Head on over to their little camp and give 'em a good smackdown, would you?",
				RewardMin = 100,
				RewardMax = 200,
			},
			Spiders_2 =
			{
				Difficulty = 2,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "SpiderLair_2",
				Title = "Spider Infestation",
				Description = "Bloody spiders are everywhere! Talking about them gives me the creeps, I'll pay you good money to take care of it.",
				RewardMin = 200,
				RewardMax = 400,
			},
			Bandits_2 =
			{
				Difficulty = 2,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "BanditLair_2",
				Title = "Bandit Bounty",
				Description = "Bandits have been reported robbing travellers on the surrounding roads. Take out their encampment and you'll be rewarded handsomely.",
				RewardMin = 200,
				RewardMax = 400,
			},
			Undead_3 =
			{
				Difficulty = 3,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "UndeadLair_3",
				Title = "Living Dead",
				Description = "Stories have reached us of ancient burial grounds raising from the ground, protected by the living dead. We must repel this scorge!",
				RewardMin = 400,
				RewardMax = 800,
			},
			Orks_4 =
			{
				Difficulty = 4,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "OrkLair_4",
				Title = "Ork Raiders",
				Description = "The orks have been getting gutsy lately. They've been ambushing caravans and robbing adventurers. We found their camp though. Get over there and teach them a thing or two.",
				RewardMin = 800,
				RewardMax = 1200,
			},
			Giants_5 =
			{
				Difficulty = 5,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "GiantLair_5",
				Title = "The Ground Shakes!",
				Description = "Giant ogres have descended upon the land, oh won't you please do something! We'll reward you greatly.",
				RewardMin = 1200,
				RewardMax = 1500,
			},	

			-- Taming Missions
			TameChicken_1 =
			{
				Difficulty = 1,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "mission_chicken",
				Title = "Tame Willful Chickens",
				Description = "Go tame 5 Willful Chickens.",
				RewardMin = 60,
				RewardMax = 120,
			},
			TameRat_1 =
			{
				Difficulty = 1,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "mission_rat_giant",
				Title = "Tame Willful Rats",
				Description = "Go tame 5 Willful Rats.",
				RewardMin = 60,
				RewardMax = 120,
			},
			TameHind_2 =
			{
				Difficulty = 2,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "mission_hind",
				Title = "Tame Headstrong Hinds",
				Description = "Go tame 5 Headstrong Hinds.",
				RewardMin = 120,
				RewardMax = 240,
			},
			TameBears_2 =
			{
				Difficulty = 2,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "mission_black_bear",
				Title = "Tame Headstrong Bears",
				Description = "Go tame 5 Headstrong Bears.",
				RewardMin = 120,
				RewardMax = 240,
			},
			TameWolves_3 =
			{
				Difficulty = 3,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "mission_wolf_grey",
				Title = "Tame Defiant Wolves",
				Description = "Go tame 5 Defiant Wolves.",
				RewardMin = 240,
				RewardMax = 480,
			},
			TameGrizzlies_3 =
			{
				Difficulty = 3,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "mission_grizzly_bear",
				Title = "Tame Defiant Bears",
				Description = "Go tame 5 Defiant Bears.",
				RewardMin = 240,
				RewardMax = 480,
			},
			TameCoyotes_4 =
			{
				Difficulty = 4,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "mission_coyote",
				Title = "Tame Unruly Coyotes",
				Description = "Go tame 5 Unruly Coyotes.",
				RewardMin = 480,
				RewardMax = 720,
			},
			TameElk_4 =
			{
				Difficulty = 4,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "mission_whitetail",
				Title = "Tame Unruly Elk",
				Description = "Go tame 5 Unruly Elk.",
				RewardMin = 480,
				RewardMax = 720,
			},
			TameHuntresses_5 =
			{
				Difficulty = 5,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "mission_spider_huntress",
				Title = "Tame Ornery Huntresses",
				Description = "Go tame 5 Ornery Huntresses.",
				RewardMin = 720,
				RewardMax = 900,
			},
			TameCrocodiles_5 =
			{
				Difficulty = 5,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "mission_crocodile_ancient",
				Title = "Tame Ornery Crocodiles",
				Description = "Go tame 5 Ornery Crocodiles.",
				RewardMin = 720,
				RewardMax = 900,
			},

			HarvestWood_1 =
			{
				Difficulty = 1,
				MissionType = "Harvest",
				MissionCategory = "HarvestLumberjack",
				Title = "Clear the fallen tree",
				Description = "Townsfolk are complaining about a tree that went down nearby. Says it's making the place look sloppy. Go out there and clear it would ya? You can keep whatever you get from it.",
				NodeTemplate = "lumberjack_node_wood",
				ResourceId = "Wood",
				ResourceAmount = 76,
				RewardMin = 60,
				RewardMax = 120,
			},
			HarvestAshWood_2 =
			{
				Difficulty = 2,
				MissionType = "Harvest",
				MissionCategory = "HarvestLumberjack",
				Title = "Clear the high quality tree",
				Description = "A real sturdy tree just went down nearby. A good opportunity if you're looking to make good materials.",
				NodeTemplate = "lumberjack_node_wood",
				ResourceId = "Ash",
				ResourceAmount = 51,
				RewardMin = 120,
				RewardMax = 240,
			},
			HarvestBlightWood_3 =
			{
				Difficulty = 3,
				MissionType = "Harvest",
				MissionCategory = "HarvestLumberjack",
				Title = "Clear the ancient tree",
				Description = "This tree has been around for as long as I can remember, and she just went down. I'd split it with ya, but I need to stay here. So go split some logs!",
				NodeTemplate = "lumberjack_node_wood",
				ResourceId = "Blightwood",
				ResourceAmount = 26,
				RewardMin = 240,
				RewardMax = 480,
				RegionLimits = 
				{
					Helm = { "Area-Eastern Range Peninsula", "Area-Mer Beach" },
					Oasis = { "Area-Arid Lake", "Area-Gold Coast", "Area-Withering Sands" },
					BlackForest = { "Area-Nosca Grove", "Area-The Brushwoods", "Area-Isle of Spears" },
					Belhaven = { "Area-Yoln's Bluff", "Area-The Brooks", "Area-Steel Coast" },
					["City of Valus"] = { "Area-Yoln's Bluff", "Area-The Brooks", "Area-Steel Coast" },
					["Pyros Landing"] = { "Area-Serpent's Pass" },
					Trinit = {"Area-Black Grove", "Area-Fremen Quarry", "Area-The Rambles"},
					["Eldeir Village"] = {"Area-Black Grove", "Area-Fremen Quarry", "Area-The Rambles"},
				}
			},

			HarvestIron_1 =
			{
				Difficulty = 1,
				MissionType = "Harvest",
				MissionCategory = "HarvestMining",
				Title = "Mine ore vein",
				Description = "A buddy of mine discovered a good spot for mining nearby. Might be worth looking into.",
				NodeTemplate = "mining_node_iron",
				ResourceId = "IronOre",
				ResourceAmount = 76,
				RewardMin = 60,
				RewardMax = 120,
			},

			HarvestCopper_2 =
			{
				Difficulty = 2,
				MissionType = "Harvest",
				MissionCategory = "HarvestMining",
				Title = "Mine ore vein",
				Description = "A buddy of mine discovered a good spot for mining nearby. Might be worth looking into.",
				NodeTemplate = "mining_node_iron",
				ResourceId = "CopperOre",
				ResourceAmount = 61,
				RewardMin = 60,
				RewardMax = 120,
			},

			HarvestGold_3 =
			{
				Difficulty = 3,
				MissionType = "Harvest",
				MissionCategory = "HarvestMining",
				Title = "Mine exotic ore vein",
				Description = "A guy came here saying he found a great spot for finding some more exotic materials. You'd be missing out if you didn't at least check it out.",
				NodeTemplate = "mining_node_iron",
				ResourceId = "GoldOre",
				ResourceAmount = 51,
				RewardMin = 120,
				RewardMax = 240,
			},

			HarvestCobalt_4 =
			{
				Difficulty = 4,
				MissionType = "Harvest",
				MissionCategory = "HarvestMining",
				Title = "Mine exotic ore vein",
				Description = "A guy came here saying he found a great spot for finding some more exotic materials. You'd be missing out if you didn't at least check it out.",
				NodeTemplate = "mining_node_iron",
				ResourceId = "CobaltOre",
				ResourceAmount = 36,
				RewardMin = 120,
				RewardMax = 240,
				RegionLimits = 
				{
					Helm = { "Area-Eastern Range Peninsula", "Area-Mer Beach" },
					Oasis = { "Area-Arid Lake", "Area-Gold Coast", "Area-Withering Sands" },
					BlackForest = { "Area-Nosca Grove", "Area-The Brushwoods", "Area-Isle of Spears" },
					Belhaven = { "Area-Yoln's Bluff", "Area-The Brooks", "Area-Steel Coast" },
					["City of Valus"] = { "Area-Yoln's Bluff", "Area-The Brooks", "Area-Steel Coast" },
					["Pyros Landing"] = { "Area-Serpent's Pass" },
					Trinit = {"Area-Black Grove", "Area-Fremen Quarry", "Area-The Rambles"},
					["Eldeir Village"] = {"Area-Black Grove", "Area-Fremen Quarry", "Area-The Rambles"},
				}
			},

			HarvestObsidian_5 =
			{
				Difficulty = 5,
				MissionType = "Harvest",
				MissionCategory = "HarvestMining",
				Title = "Tap the rare ore vein",
				Description = "Some drunkard at the inn was shouting about hitting the motherlode and bought drinks for everyone the other night. Got him to tell me where it is before he passed out.",
				NodeTemplate = "mining_node_iron",
				ResourceId = "ObsidianOre",
				ResourceAmount = 26,
				RewardMin = 240,
				RewardMax = 480,
				RegionLimits = 
				{
					Helm = { "Area-Eastern Range Peninsula", "Area-Mer Beach" },
					Oasis = { "Area-Arid Lake", "Area-Gold Coast", "Area-Withering Sands" },
					BlackForest = { "Area-Nosca Grove", "Area-The Brushwoods", "Area-Isle of Spears" },
					Belhaven = { "Area-Yoln's Bluff", "Area-The Brooks", "Area-Steel Coast" },
					["City of Valus"] = { "Area-Yoln's Bluff", "Area-The Brooks", "Area-Steel Coast" },
					["Pyros Landing"] = { "Area-Serpent's Pass" },
					Trinit = {"Area-Black Grove", "Area-Fremen Quarry", "Area-The Rambles"},
					["Eldeir Village"] = {"Area-Black Grove", "Area-Fremen Quarry", "Area-The Rambles"},
				}
			},

			HarvestCotton_1 =
			{
				Difficulty = 1,
				MissionType = "Harvest",
				MissionCategory = "HarvestFabrication",
				Title = "Pick up cotton shipment",
				Description = "A merchant camp nearby just packed up and moved away. In their rush, they left behind a shipment of cotton. You're more then welcome to help yourself to it.",
				NodeTemplate = "cotton_node",
				ResourceId = "CottonNode",
				ResourceAmount = 16,
				RewardMin = 60,
				RewardMax = 120,
			},
			HarvestFluffyCotton_2 =
			{
				Difficulty = 2,
				MissionType = "Harvest",
				MissionCategory = "HarvestFabrication",
				Title = "Find lost fluffy cotton",
				Description = "I heard a caravan had lost a crate of Fluffy Cotton. Go check it out.",
				NodeTemplate = "cotton_node",
				ResourceId = "FluffyCottonNode",
				ResourceAmount = 11,
				RewardMin = 60,
				RewardMax = 120,
			},
			HarvestSilk_3 =
			{
				Difficulty = 3,
				MissionType = "Harvest",
				MissionCategory = "HarvestFabrication",
				Title = "Regain stolen silk",
				Description = "A crate of spider silk was recently stolen. Could you go steal it back? We don't want thieves thinking they can do this without consequences.",
				NodeTemplate = "silk_node",
				ResourceId = "Silk",
				ResourceAmount = 6,
				RewardMin = 60,
				RewardMax = 120,
				RegionLimits = 
				{
					Helm = { "Area-Eastern Range Peninsula", "Area-Mer Beach" },
					Oasis = { "Area-Arid Lake", "Area-Gold Coast", "Area-Withering Sands" },
					BlackForest = { "Area-Nosca Grove", "Area-The Brushwoods", "Area-Isle of Spears" },
					Belhaven = { "Area-Yoln's Bluff", "Area-The Brooks", "Area-Steel Coast" },
					["City of Valus"] = { "Area-Yoln's Bluff", "Area-The Brooks", "Area-Steel Coast" },
					["Pyros Landing"] = { "Area-Serpent's Pass" },
					Trinit = {"Area-Black Grove", "Area-Fremen Quarry", "Area-The Rambles"},
					["Eldeir Village"] = {"Area-Black Grove", "Area-Fremen Quarry", "Area-The Rambles"},
				}
			},
			HarvestLeather_1 =
			{
				Difficulty = 1,
				MissionType = "Harvest",
				MissionCategory = "HarvestFabrication",
				Title = "Guide to leather hides",
				Description = "Some travelers had to lower their gear weight while traveling, so they left behind some leather hides.",
				NodeTemplate = "leather_node",
				ResourceId = "LeatherHide",
				ResourceAmount = 11,
				RewardMin = 60,
				RewardMax = 120,
			},
			HarvestBeastLeather_2 =
			{
				Difficulty = 2,
				MissionType = "Harvest",
				MissionCategory = "HarvestFabrication",
				Title = "Sun-dried beast leather",
				Description = "Some beast leather was forgotten outside while drying. Go pick it up before someone nefarious does.",
				NodeTemplate = "leather_node",
				ResourceId = "BeastLeatherHide",
				ResourceAmount = 8,
				RewardMin = 60,
				RewardMax = 120,
			},
			HarvestVileLeather_3 =
			{
				Difficulty = 3,
				MissionType = "Harvest",
				MissionCategory = "HarvestFabrication",
				Title = "Vile leather in bad weather",
				Description = "Shipments of vile leather had gotten soaked the other day and were considered useless. They can actually still be saved, so go grab 'em.",
				NodeTemplate = "leather_node",
				ResourceId = "VileLeatherHide",
				ResourceAmount = 6,
				RewardMin = 60,
				RewardMax = 120,
				RegionLimits = 
				{
					Helm = { "Area-Eastern Range Peninsula", "Area-Mer Beach" },
					Oasis = { "Area-Arid Lake", "Area-Gold Coast", "Area-Withering Sands" },
					BlackForest = { "Area-Nosca Grove", "Area-The Brushwoods", "Area-Isle of Spears" },
					Belhaven = { "Area-Yoln's Bluff", "Area-The Brooks", "Area-Steel Coast" },
					["City of Valus"] = { "Area-Yoln's Bluff", "Area-The Brooks", "Area-Steel Coast" },
					["Pyros Landing"] = { "Area-Serpent's Pass" },
					Trinit = {"Area-Black Grove", "Area-Fremen Quarry", "Area-The Rambles"},
					["Eldeir Village"] = {"Area-Black Grove", "Area-Fremen Quarry", "Area-The Rambles"},
				}
			},


			HarvestBarrelfish_1 =
			{
				Difficulty = 1,
				MissionType = "Fishing",
				MissionCategory = "Fishing",
				Title = "Catch Barrelfish",
				Description = "We could always use more barrelfish. Why not go out and fill up our stock, yea?",
				Fish = "Barrelfish",
				NodeTemplate = "fish_node",
				ResourceAmount = 16,
				RewardMin = 60,
				RewardMax = 80,
			},
			HarvestTerofish_2 =
			{
				Difficulty = 2,
				MissionType = "Fishing",
				MissionCategory = "Fishing",
				Title = "Catch Terofish",
				Description = "We could always use more terofish. Why not go out and fill up our stock, yea?",
				NodeTemplate = "fish_node",
				Fish = "Terofish",
				ResourceAmount = 13,
				RewardMin = 80,
				RewardMax = 160,
			},
			HarvestSpottedTerofish =
			{
				Difficulty = 3,
				MissionType = "Fishing",
				MissionCategory = "Fishing",
				Title = "Catch Spotted Terofish",
				Description = "We could always use more spotted terofish. Why not go out and fill up our stock, yea?",
				NodeTemplate = "fish_node",
				Fish = "SpottedTerofish",
				ResourceAmount = 11,
				RewardMin = 120,
				RewardMax = 240,
			},
			HarvestFourEyeSalar_4 =
			{
				Difficulty = 4,
				MissionType = "Fishing",
				MissionCategory = "Fishing",
				Title = "Catch Four-Eyed Salar",
				Description = "We could always use more four-eyed salar. Why not go out and fill up our stock, yea?",
				NodeTemplate = "fish_node",
				Fish = "FourEyeSalar",
				ResourceAmount = 8,
				RewardMin = 160,
				RewardMax = 240,
			},
			HarvestRazorFish_5 =
			{
				Difficulty = 5,
				MissionType = "Fishing",
				MissionCategory = "Fishing",
				Title = "Catch Razor-Fish",
				Description = "We could always use more razor-fish. Why not go out and fill up our stock, yea?",
				NodeTemplate = "fish_node",
				Fish = "RazorFish",
				ResourceAmount = 6,
				RewardMin = 240,
				RewardMax = 480,
			},
		},

		FrozenTundra =
		{
			Wolves_1 =
			{
				Difficulty = 1,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "ArcticWolfLair_1",
				Title = "Wolf Over-Population",
				Description = "The Wolves have been over-populating and over-hunting the arctic hares. Please help us revive the food chain.",
				RewardMin = 100,
				RewardMax = 200,
			},
			PolarBears_3 =
			{
				Difficulty = 3,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "PolarBearLair_3",
				Title = "Bear Caved",
				Description = "I heard someone might have fallen into a Bear Den out in the Tundra. Please go investigate it for me.",
				RewardMin = 800,
				RewardMax = 1500,
			},
			SnowLeopards_4 =
			{
				Difficulty = 4,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "SnowLeopardLair_4",
				Title = "Head of the Pack",
				Description = "I need you to hunt down this pack of Snow Leopards that have harassing travelers and merchants.",
				RewardMin = 1500,
				RewardMax = 2000,
			},
			Kobold_5 =
			{
				Difficulty = 5,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "KoboldLair_5",
				Title = "Hunt the Gatherers",
				Description = "Kobolds have been building a large hub of tents. You need to stop them before their forces grow too large.",
				RewardMin = 2000,
				RewardMax = 4000,
			},

			HarvestBarrelfish_1 =
			{
				Difficulty = 1,
				MissionType = "Fishing",
				MissionCategory = "Fishing",
				Title = "Catch Barrelfish",
				Description = "We could always use more barrelfish. Why not go out and fill up our stock, yea?",
				Fish = "Barrelfish",
				NodeTemplate = "fish_node",
				ResourceAmount = 16,
				RewardMin = 60,
				RewardMax = 80,
			},
			HarvestTerofish_2 =
			{
				Difficulty = 2,
				MissionType = "Fishing",
				MissionCategory = "Fishing",
				Title = "Catch Terofish",
				Description = "We could always use more terofish. Why not go out and fill up our stock, yea?",
				NodeTemplate = "fish_node",
				Fish = "Terofish",
				ResourceAmount = 13,
				RewardMin = 80,
				RewardMax = 160,
			},
			HarvestSpottedTerofish =
			{
				Difficulty = 3,
				MissionType = "Fishing",
				MissionCategory = "Fishing",
				Title = "Catch Spotted Terofish",
				Description = "We could always use more spotted terofish. Why not go out and fill up our stock, yea?",
				NodeTemplate = "fish_node",
				Fish = "SpottedTerofish",
				ResourceAmount = 11,
				RewardMin = 120,
				RewardMax = 240,
			},
			HarvestFourEyeSalar_4 =
			{
				Difficulty = 4,
				MissionType = "Fishing",
				MissionCategory = "Fishing",
				Title = "Catch Four-Eyed Salar",
				Description = "We could always use more four-eyed salar. Why not go out and fill up our stock, yea?",
				NodeTemplate = "fish_node",
				Fish = "FourEyeSalar",
				ResourceAmount = 8,
				RewardMin = 160,
				RewardMax = 240,
			},
			HarvestRazorFish_5 =
			{
				Difficulty = 5,
				MissionType = "Fishing",
				MissionCategory = "Fishing",
				Title = "Catch Razor-Fish",
				Description = "We could always use more razor-fish. Why not go out and fill up our stock, yea?",
				NodeTemplate = "fish_node",
				Fish = "RazorFish",
				ResourceAmount = 6,
				RewardMin = 240,
				RewardMax = 480,
			},
			TameWalrus_4 =
			{
				Difficulty = 4,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "walrus",
				Title = "Tame Walruses",
				Description = "Got a herder around here who wants to get into the Walrus business, whatever that means. They're offering some good coin regardless. You in?",
				RewardMin = 480,
				RewardMax = 720,
			},
			TameWerecrab_5 =
			{
				Difficulty = 5,
				MissionType = "Tame",
				MissionCategory = "Taming",
				TameCount = 5,
				MobTemplate = "werecrab",
				Title = "Tame Werecrabs",
				Description = "See those giant hulking crabs? They usually stick to the beaches, but not always. Go herd them back to the beach before they freeze or something. They make a great natural defence against barbarians, assuming they don't chop you to pieces.",
				RewardMin = 720,
				RewardMax = 900,
			},
		},

		SouthernHills = 
		{
			
		},

		UpperPlains =
		{

		},

		SouthernRim =
		{

		},

		BarrenLands =
		{
			--[[Beetles_2 =
			{
				Difficulty = 2,
				MissionType = "Lair",
				PrefabName = "BeetleLair_2",
				Title = "Bug Problem",
				Description = "A nest of giant beetles sprouted up and is giving the local farmers trouble. Get over there and start stomping.",
				RewardMin = 200,
				RewardMax = 300,
			},]]
			Cultists_3 =
			{
				Difficulty = 3,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "CultistLair_3",
				Title = "Cultist Encampment",
				Description = "A group of cultists was spotted nearby. Possibly scouting for an upcoming raid. Clearly they need a reminder of who's boss around here. Go trash their camp, and take out those cultists.",
				RewardMin = 800,
				RewardMax = 1500,
			},
			Gazers_5 =
			{
				Difficulty = 5,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "GazerLair_5",
				Title = "Purge Abominations",
				Description = "Some strange cult assembled a gateway for creatures from beyond our realm to arrive. The gate needs to be crushed before those things get serious.",
				RewardMin = 2000,
				RewardMax = 4000,
			},
			Scorpions_4 =
			{
				Difficulty = 4,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "ScorpionLair_4",
				Title = "Scorpion Nest",
				Description = "A bunch of scorpions have started building a nest nearby, and are terrorizing anyone that comes by. Get over there and crush those bugs before they do some real damage to someone.",
				RewardMin = 1500,
				RewardMax = 2000,
			},
		},

		BlackForest =
		{
			Ents_4 =
			{
				Difficulty = 4,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "EntLair_4",
				Title = "Walking Trees",
				Description = "Some crazy druid has been working up the trees into a frenzy. Now they're chasing caravans, and crushing homes. The gaurds are offering a reward to the person who puts them down if you're interested.",
				RewardMin = 1500,
				RewardMax = 2000,
			},	
			Harpy_3 =
			{
				Difficulty = 3,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "HarpyLair_3",
				Title = "Harpy Nest",
				Description = "A flock of harpies flew in and built some nests recently. Go knock them down before they get too comfortable.",
				RewardMin = 800,
				RewardMax = 1500,
			},
			Spiders_5 =
			{
				Difficulty = 5,
				MissionType = "Lair",
				MissionCategory = "Combat",
				PrefabName = "SpiderLair_5",
				Title = "Spider Infestation",
				Description = "Bloody spiders are everywhere! Talking about them gives me the creeps, I'll pay you good money to take care of it.",
				RewardMin = 2000,
				RewardMax = 4000,
			},
		},

		EasternFrontier =
		{

		},
	}
}