ProfessionSkillTiers = {30,50,80,100}

SpellSkills = {"MagerySkill"}
WeaponSkills = {"BashingSkill", "SlashingSkill", "ArcherySkill", "LancingSkill", "PiercingSkill", "BrawlingSkill" }

--see related quests configurations: scripts\globals\static_data\quests.lua
Professions = {    

    Fighter = {
        DisplayName = "Fighter",
        IntroQuest = "FighterProfessionIntro",
        Quests = {"FighterProfessionTierOne", "FighterProfessionTierTwo", "FighterProfessionTierThree", "FighterProfessionTierFour"},
        Skills = { "Weapon", "MartialProwess", "Melee" },
        CanResumeLaterStep = true,
    },

    Mage = {
        DisplayName = "Mage",
        IntroQuest = "MageProfessionIntro",
        Quests = {"MageProfessionTierOne", "MageProfessionTierTwo", "MageProfessionTierThree", "MageProfessionTierFour"},
        Skills = { "Magery", "MagicAffinity", "Channeling" },
        CanResumeLaterStep = true,
    },
    
    --[[Evocator = {
        DisplayName = "Evocator",
        IntroQuest = "EvocatorProfessionIntro",
        Quests = {"EvocatorProfessionTierOne", "EvocatorProfessionTierTwo", "EvocatorProfessionTierThree", "EvocatorProfessionTierFour"},
        RequiredProfession = "Mage",
        Skills = { "Evocation" },
    },]]

    Tamer = {
        DisplayName = "Tamer",
        IntroQuest = "TamerProfessionIntro",
        Quests = {"TamerProfessionTierOne","TamerProfessionTierTwo", "TamerProfessionTierThree", "TamerProfessionTierFour"},
        Skills = { "AnimalTaming", "AnimalLore" },
        CanResumeLaterStep = true,
    },

    --[[Healer = {
        DisplayName = "Healer",
        IntroQuest = "HealerProfessionIntro",
        Quests = {"HealerProfessionTierOne"},-- "HealerProfessionTierTwo", "HealerProfessionTierThree", "HealerProfessionTierFour"},
        Skills = { "Healing" },
    },

    Manifester = {
        DisplayName = "Manifester",
        IntroQuest = "ManifesterProfessionIntro",
        Quests = {"ManifesterProfessionTierOne", "ManifesterProfessionTierTwo", "ManifesterProfessionTierThree", "ManifesterProfessionTierFour"},
        Skills = { "Manifestation", "Channeling" },
    },]]

    Rogue = {
        DisplayName = "Rogue",
        IntroQuest = "RogueProfessionIntro",
        Quests = {"RogueProfessionTierOne", "RogueProfessionTierTwo", "RogueProfessionTierThree", "RogueProfessionTierFour"},
        Skills = { "Hiding", "Stealth" },
        CanResumeLaterStep = true,
    },

    Bard = {
        DisplayName = "Bard",
        IntroQuest = "BardProfessionIntro",
        Quests = {"BardProfessionTierOne", "BardProfessionTierTwo", "BardProfessionTierThree", "BardProfessionTierFour"},
        Skills = { "Musicianship", "Entertainment" },
        CanResumeLaterStep = true,
    },
    
    Blacksmith = {
        DisplayName = "Blacksmith",
        IntroQuest = "BlacksmithProfessionIntro",
        Quests = {"BlacksmithProfessionTierOne", "BlacksmithProfessionTierTwo", "BlacksmithProfessionTierThree", "BlacksmithProfessionTierFour"},
        Skills = { "Metalsmith" },
        CanResumeLaterStep = true,
    },

    Carpenter = {
        DisplayName = "Carpenter",
        IntroQuest = "CarpenterProfessionIntro",
        Quests = {"CarpenterProfessionTierOne", "CarpenterProfessionTierTwo", "CarpenterProfessionTierThree", "CarpenterProfessionTierFour"},
        Skills = { "Woodsmith" },
        CanResumeLaterStep = true,
    },

    Tailor = {
        DisplayName = "Tailor",
        IntroQuest = "TailorProfessionIntro",
        Quests = {"TailorProfessionTierOne", "TailorProfessionTierTwo", "TailorProfessionTierThree", "TailorProfessionTierFour"},
        Skills = { "Fabrication" },
        CanResumeLaterStep = true,
    },

    Alchemist = {
        DisplayName = "Alchemist",
        IntroQuest = "AlchemistProfessionIntro",
        Quests = {"AlchemistProfessionTierOne", "AlchemistProfessionTierTwo", "AlchemistProfessionTierThree", "AlchemistProfessionTierFour"},
        Skills = { "Alchemy" },
        CanResumeLaterStep = true,
    },

    Scribe = {
        DisplayName = "Scribe",
        IntroQuest = "ScribeProfessionIntro",
        Quests = {"ScribeProfessionTierOne", "ScribeProfessionTierTwo", "ScribeProfessionTierThree", "ScribeProfessionTierFour"},
        Skills = { "Inscription" },
        CanResumeLaterStep = true,
    },

    Chef = {
        DisplayName = "Chef",
        IntroQuest = "ChefProfessionIntro",
        Quests = {"ChefProfessionTierOne", "ChefProfessionTierTwo", "ChefProfessionTierThree", "ChefProfessionTierFour"},
        Skills = { "Cooking" },
        CanResumeLaterStep = true,
    },

    Miner = {
        DisplayName = "Miner",
        IntroQuest = "MinerProfessionIntro",
        Quests = {"MinerProfessionTierOne", "MinerProfessionTierTwo", "MinerProfessionTierThree", "MinerProfessionTierFour"},
        Skills = { "Mining" },
        CanResumeLaterStep = true,
    },

    Fisher = {
        DisplayName = "Fisher",
        IntroQuest = "FisherProfessionIntro",
        Quests = {"FisherProfessionTierOne", "FisherProfessionTierTwo", "FisherProfessionTierThree", "FisherProfessionTierFour"},
        Skills = { "Fishing" },
        CanResumeLaterStep = true,
    },

    Lumberjack = {
        DisplayName = "Lumberjack",
        IntroQuest = "LumberjackProfessionIntro",
        Quests = {"LumberjackProfessionTierOne", "LumberjackProfessionTierTwo", "LumberjackProfessionTierThree", "LumberjackProfessionTierFour"},
        Skills = { "Lumberjack" },
        CanResumeLaterStep = true,
    },

    --[[TreasureHunter = {
        DisplayName = "Treasure Hunter",
        IntroQuest = "TreasureHunterProfessionIntro",
        Quests = {"TreasureHunterProfessionTierOne", "TreasureHunterProfessionTierTwo", "TreasureHunterProfessionTierThree", "TreasureHunterProfessionTierFour"},
        Skills = { "TreasureHunting" },
    },

    Lockpicker = {
        DisplayName = "Lockpicker",
        IntroQuest = "LockpickerProfessionIntro",
        Quests = {"LockpickerProfessionTierOne", "LockpickerProfessionTierTwo", "LockpickerProfessionTierThree", "LockpickerProfessionTierFour"},
        Skills = { "Lockpicking" },
    },]]

}

TaskIndexTitles = {
    "Apprentice",
    "Journeyman",
    "Master",
    "Grandmaster"
}
