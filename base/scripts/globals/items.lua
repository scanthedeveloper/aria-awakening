-- This affects the price merchants will buy/sell items with durability for
-- and the amount of resources required to craft and the amount salvaged
DURABILTY_VALUE_MULTIPLIER = 5

OBJECT_INTERACTION_RANGE = 5

DEFAULT_DURABILITY = 5

containerDropAreas = 
{ 
	Backpack = { min = Loc(-0.5,0.0,-0.31),
                 max = Loc(0.5,2.0,-0.31) },

	Chest =    { min = Loc(-0.5,0.1,-0.8),
                 max = Loc(0.5,0.1,-0.3) },

    BoneChest ={ min = Loc(-0.6,0.3,-0.8),
                 max = Loc(0.6,0.3,-0.2) },

    Crate =    { min = Loc(-0.3,0.1,-0.6),
				 max = Loc(0.35,0.1,0.4) },

	Lockbox =  { min = Loc(-0.3,0,-0.6),
				 max = Loc(0.4,0,-0.2) },

	Pouch =    { min = Loc(-0.4,1.1,-0.5),
				 max = Loc(0.5,-0.1,0.4) },

	Coffin =  { min = Loc(-0.4,1.2,-0.4),
                max = Loc(0.4,1.2,0.6) },

    Trade =   { min = Loc(-0.5,0.0,-0.31),
                max = Loc(0.5,2.0,-0.31) }
}


function GetRandomInRange(min, max)
	return min + (math.random() * (max - min))
end

function CanHaveDurability(item)
	if (item == nil) then LuaDebugCallStack("nil item provided to CanHaveDurability") end
	if not((item:HasObjVar("ArmorClass")) or (item:HasObjVar("ShieldClass")) or (item:HasObjVar("WeaponType")) or (item:HasModule("tool_base")) or (item:HasModule("harvest_tool_base"))) then
		--DebugMessage(item:GetName() .. " Cannot Have Durability")
		return false
	end
	return true
end

--[[function MoveEquipmentToGround(target)
	local backpackObj = this:GetEquippedObject("Backpack")
    local leftHand = this:GetEquippedObject("LeftHand")
    local rightHand = this:GetEquippedObject("RightHand")
    local chest = this:GetEquippedObject("Chest")
    local legs = this:GetEquippedObject("Legs")
    local head = this:GetEquippedObject("Head")
    leftHand:SetWorldPosition(this:GetLoc()+Loc(math.random()-0.5,0,math.random()-0.5))
    rightHand:SetWorldPosition(this:GetLoc()+Loc(math.random()-0.5,0,math.random()-0.5))
    chest:SetWorldPosition(this:GetLoc()+Loc(math.random()-0.5,0,math.random()-0.5))
    legs:SetWorldPosition(this:GetLoc()+Loc(math.random()-0.5,0,math.random()-0.5))
    head:SetWorldPosition(this:GetLoc()+Loc(math.random()-0.5,0,math.random()-0.5))
	local lootObjects = backpackObj:GetContainedObjects()
	for i,j in pairs(lootObjects) do 
		--DebugMessage("Yes it's happening")
		j:SetWorldPosition(this:GetLoc()+Loc(math.random()-0.5,0,math.random()-0.5))
	end
end--]]

function GetRandomDropPositionForContainerType(containerType)
	local dropArea = containerDropAreas[containerType]
	if( dropArea == nil ) then
		--DebugMessage("GetRandomDropPosition returning 0")
		return Loc(0,0,0)
	end

	local dropLoc = Loc(GetRandomInRange(dropArea.min.X,dropArea.max.X),
			   GetRandomInRange(dropArea.min.Y,dropArea.max.Y),
		       GetRandomInRange(dropArea.min.Z,dropArea.max.Z))

	return dropLoc
end

-- return a random valid location inside the given container
function GetRandomDropPosition(containerObj)
	-- default to backpack
	if not(containerObj) then
		LuaDebugCallStack("ERROR: no containerObj specified")
	end
	local containerType = containerObj:GetSharedObjectProperty("ContainerWindowType") or "Backpack"

	return GetRandomDropPositionForContainerType(containerType)
end

function IsLockedDown(targetObj)
	return targetObj:HasObjVar("LockedDown")
end

function CreateObjInCarrySlot(target,template,eventId,...)
	CreateObjInContainer(template, target, Loc(0,0,0), eventId, ...)
end

function CreateObjInBackpack(target,template,eventId,...)
	local backpackObj = target:GetEquippedObject("Backpack")
	-- TODO: Verify creation success and refund money on failure
	if( target ~= nil and backpackObj ~= nil and template ~= nil ) then
		local randomLoc = GetRandomDropPosition(backpackObj)
		backpackObj:SendOpenContainer(target)
		CreateObjInContainer(template, backpackObj, randomLoc, eventId, ...)	
		return true
	end

	return false
end

function CreateStackInBackpack(target,template,amount,eventId,...)
	local backpackObj = target:GetEquippedObject("Backpack")
	
	if( target ~= nil and backpackObj ~= nil and template ~= nil ) then
		local templateData = GetTemplateData(template)
		if(templateData) then
			local randomLoc = GetRandomDropPosition(backpackObj)
			backpackObj:SendOpenContainer(target)
			
			if not(templateData.ObjectVariables) then templateData.ObjectVariables = {} end
			templateData.ObjectVariables.StackCount = amount

			CreateCustomObjInContainer(template, templateData, backpackObj, randomLoc, eventId, ...)	
			return true
		end
	end

	return false
end

function CreateStackInBank(target,template,amount,eventId,...)
	local bankObj = target:GetEquippedObject("Bank")
	
	if( target ~= nil and bankObj ~= nil and template ~= nil ) then
		local templateData = GetTemplateData(template)
		if(templateData) then
			local randomLoc = GetRandomDropPosition(bankObj)
			--bankObj:SendOpenContainer(target)
			
			if not(templateData.ObjectVariables) then templateData.ObjectVariables = {} end
			templateData.ObjectVariables.StackCount = amount
			CreateCustomObjInContainer(template, templateData, bankObj, randomLoc, eventId, ...)	
			return true
		end
		
	end

	return false
end

function CreateCoinStackInBank(target,template,amount,eventId,...)
	local bankObj = target:GetEquippedObject("Bank")
	
	if( target ~= nil and bankObj ~= nil and template ~= nil ) then
		local existingStackable = FindItemInContainer(bankObj, function(foundItem)
			local templateID = foundItem:GetCreationTemplateId()
			if IsStackable(foundItem) and templateID == template then
				return foundItem
			else
				--DebugMessage("Could not find",template,"in bank, found", templateID)
			end
		end)

		if existingStackable then
			local oldCount = existingStackable:GetObjVar("StackCount")
			existingStackable:SendMessage("AdjustCoinStack", existingStackable, amount)
			DebugMessage("Adding",amount,"to existing coin purse.\nBalance is now",DumpTable(existingStackable:GetObjVar("Amounts")))
		else
			DebugMessage("New Money Stack created as none were found")
			local templateData = GetTemplateData(template)
			if(templateData) then
				local randomLoc = GetRandomDropPosition(bankObj)
				
				if not(templateData.ObjectVariables) then templateData.ObjectVariables = {} end
				templateData.ObjectVariables.StackCount = amount
				CreateCustomObjInContainer(template, templateData, bankObj, randomLoc, eventId, ...)	
				return true
			end
		end
	end
	return false
end

function CountItemsInContainer(container, template)
	if ( not container or not container:IsValid() ) then return false end
	local items = FindItemsInContainerByTemplateRecursive(container, template)
	if ( not items or not items[1] ) then return 0 end
	local count = 0
	for i = 1, #items do
		local stack = items[i]:GetObjVar("StackCount") or 1
		count = count + stack
	end
	return count
end

function CountCraftedItemsInContainer(container, template, playerObj)
	if ( not container or not container:IsValid() ) then return false end
	local items = FindItemsInContainerByTemplateRecursive(container, template)
	if ( not items or not items[1] ) then return 0 end
	local count = 0
	for i = 1, #items do
		stack = 0
		if( playerObj and items[i]:GetObjVar("Crafter") == player ) then
			stack = items[i]:GetObjVar("StackCount") or 1
		end

		count = count + stack
	end
	return count
end

function RemoveItemsInBackpack(mobileObj, template, count)
	if ( not mobileObj or not mobileObj:IsValid() ) then return false end
	local remainingToDestroy = count or 1
	local mobileBackpack = mobileObj:GetEquippedObject("Backpack")
	if ( not mobileBackpack ) then return false end
	local items = FindItemsInContainerByTemplateRecursive(mobileBackpack, template)
	if ( not items or not items[1] ) then return false end
	for i = 1, #items do
		if ( not items[i]:IsValid() ) then return false end
		local itemStackCount = items[i]:GetObjVar("StackCount") or 1
		local countToDestroy = math.min(itemStackCount, remainingToDestroy)
		if ( countToDestroy >= itemStackCount ) then
			items[i]:Destroy()
			remainingToDestroy = remainingToDestroy - countToDestroy
		else
			items[i]:SendMessage("AdjustStack", countToDestroy * -1)
			remainingToDestroy = remainingToDestroy - countToDestroy
		end
		if ( remainingToDestroy < 1 ) then return true end
	end
	return false
end

function HasItemInBackpack(target,searchTemplate)
	local backpackObj = target:GetEquippedObject("Backpack")
	if( target == nil or backpackObj == nil or searchTemplate == nil ) then return false end
	local lootObjects = backpackObj:GetContainedObjects()
	for index, lootObj in pairs(lootObjects) do	    		
   		if(lootObj:GetCreationTemplateId() == searchTemplate ) then
   			return true
   		end
   	end
   	return false
end

function HasItem(target, searchTemplate)
	if( target == nil or searchTemplate == nil ) then return false end
	local equippedObjects = target:GetAllEquippedObjects()
	for index,equippedItem in pairs(equippedObjects) do
		if (equippedItem:GetCreationTemplateId() == searchTemplate) then
			return true
		end
	end

	if (HasItemInBackpack(target,searchTemplate)) then return true end

   	return false
end

-- [Fabrication] to check that player is wearing something
function HasEquippedCraftedItem(playerObj, searchTemplate, count, objVarName, objVarValue)
	local isSpecific = objVarName and objVarValue
	local meetsCriteria = false
	local hasEquipped = false
	local equippedObjects = playerObj:GetAllEquippedObjects()

	for index,equippedItem in pairs(equippedObjects) do
		if ( isSpecific ) then
			matchesCriteria = equippedItem:GetCreationTemplateId() == searchTemplate and ( equippedItem:GetObjVar(objVarName) == objVarValue or objVarValue == "empty" )
		else
			matchesCriteria = equippedItem:GetCreationTemplateId() == searchTemplate
		end

		if ( matchesCriteria) then
			hasEquipped = true
		end
	end 
	return hasEquipped
end

function HasCraftedItems(playerObj, searchTemplate, count, objVarName, objVarValue)
	local craftedItems = GetCraftedItems(playerObj, searchTemplate, count, objVarName, objVarValue)
	if not craftedItems then return false end
	local craftCount = 0
	for i=1, #craftedItems do
		if not( craftedItems[i]:IsContainer() and CountObjectsInContainer(craftedItems[i]) > 0 ) then
			craftCount = craftCount + (craftedItems[i]:GetObjVar("StackCount") or 1) 
		end
	end
	if craftCount >= count then return true else return false end
end

-- [Fabrication] so it doesn't take armor worn on player, only armor in backpack
function TakeCraftedItemFromBackpack(target,searchTemplate, count, objVarName, objVarValue)
	local items = GetItemsInBackpack(target,searchTemplate, objVarName, objVarValue)
	local foundAndDestroyed = false
	local countTaken = 0
	for i = 1, #items do
		local crafter = items[i]:GetObjVar("Crafter")
		if items[i] and crafter and countTaken < count then 
			items[i]:Destroy()
			countTaken = countTaken + 1
			foundAndDestroyed = true
		end
	end
	if not foundAndDestroyed then return false end
end

function RemoveCraftedItems(playerObj, searchTemplate, count, objVarName, objVarValue)
	local craftedItems = GetCraftedItems(playerObj, searchTemplate, count, objVarName, objVarValue)
	local remainingToDestroy = count or 1
	for i=1, #craftedItems do
		-- Is it valid?
		if ( not craftedItems[i]:IsValid() ) then return false end
		-- If this is a container and it has items in it, we need to return false.
		if( craftedItems[i]:IsContainer() and CountObjectsInContainer(craftedItems[i]) > 0 ) then return false end
		
		local itemStackCount = craftedItems[i]:GetObjVar("StackCount") or 1
		local countToDestroy = math.min(itemStackCount, remainingToDestroy)
		local templateName = craftedItems[i]:GetCreationTemplateId()

		if ( countToDestroy >= itemStackCount ) then
			craftedItems[i]:Destroy()
			remainingToDestroy = remainingToDestroy - countToDestroy
		else
			craftedItems[i]:SendMessage("AdjustStack", countToDestroy * -1)
			remainingToDestroy = remainingToDestroy - countToDestroy
		end

		if ( remainingToDestroy < 1 ) then return true end
	end
	return false
end

function GetCraftedItem(playerObj, searchTemplate, count, objVarName, objVarValue)
	local craftedItems = GetCraftedItems(playerObj, searchTemplate, count, objVarName, objVarValue)
	if craftedItems then return craftedItems[1] else return nil end
end

function GetCraftedItems(playerObj, searchTemplate, count, objVarName, objVarValue)
	local items = GetItems(playerObj, searchTemplate, objVarName, objVarValue)
	if not items then return false end
	local craftedItems = {}

	for i = 1, #items do
		if ( items[i] ) then
            local crafter = items[i]:GetObjVar("Crafter")
            if ( crafter and crafter == playerObj ) then
                table.insert(craftedItems, items[i])
            end
        end
	end
	return craftedItems or nil
end

function HasCompletedCraftingOrder(playerObj, desiredSkill)
	local order = GetFirstCompletedCraftingOrder(playerObj, desiredSkill)
	if order then return true else return false end
end

function TakeCraftingOrderGiveReward(playerObj, desiredSkill)
	local order = GetFirstCompletedCraftingOrder(playerObj, desiredSkill)
	if not order then return end
	local orderInfo = order:GetObjVar("OrderInfo")
	CraftingOrderReward( playerObj, order, desiredSkill, orderInfo)
end

function GetFirstCompletedCraftingOrder(playerObj, desiredSkill)
    local orders = GetCompletedCraftingOrders(playerObj, desiredSkill)
	if orders then return orders[1] else return nil end
end

function GetCompletedCraftingOrders(playerObj, desiredSkill)
    local orders = GetItems(playerObj, "crafting_order", "OrderSkill", desiredSkill)
    if ( not orders ) then return false end
    local desiredOrders = {}
    for i = 1, #orders do
        local complete = orders[i]:GetObjVar("OrderComplete")
        if ( complete ) then
            local name = GetOrderOwner(playerObj,orders[i])
            if ( name and name == playerObj ) then
                table.insert(desiredOrders, orders[i])
            end
        end
    end
	return desiredOrders or nil
end

function GetItemSpecific(target,searchTemplate, objVarName, objVarValue)
	local items = GetItems(target,searchTemplate, objVarName, objVarValue)
	if items then return items[1] else return nil end
end

function GetItems(target,searchTemplate, objVarName, objVarValue)
	if( target == nil or searchTemplate == nil ) then return nil end
	local isSpecific = objVarName and objVarValue
	local items = {}
	local matchesCriteria = false

	local equippedObjects = target:GetAllEquippedObjects()
	for index,equippedItem in pairs(equippedObjects) do

		if ( isSpecific ) then
			matchesCriteria = equippedItem:GetCreationTemplateId() == searchTemplate and ( equippedItem:GetObjVar(objVarName) == objVarValue or objVarValue == "empty" )
		else
			matchesCriteria = equippedItem:GetCreationTemplateId() == searchTemplate
		end

		if ( matchesCriteria) then
			table.insert(items, equippedItem)
		end
	end

	local backpackItems = GetItemsInBackpack(target,searchTemplate, objVarName, objVarValue) or nil

	if backpackItems then
		for i=1, #backpackItems do table.insert(items, backpackItems[i]) end
	end

   	return items or nil
end

function GetItemInBackpack(target,searchTemplate, objVarName, objVarValue)
	local items = GetItemsInBackpack(target,searchTemplate, objVarName, objVarValue)
	if items then return items[1] else return nil end
end

function GetItemsInBackpack(target,searchTemplate, objVarName, objVarValue)
	local backpackObj = target:GetEquippedObject("Backpack")
	if( backpackObj == nil ) then return nil end
	local isSpecific = objVarName and objVarValue
	local items = {}
	local matchesCriteria = false

	local lootObjects = FindItemsInContainerByTemplateRecursive(backpackObj, searchTemplate)
		for index, lootObj in pairs(lootObjects) do	 
			if ( isSpecific ) then
				matchesCriteria = lootObj:GetCreationTemplateId() == searchTemplate and lootObj:GetObjVar(objVarName) == objVarValue
			else
				matchesCriteria = lootObj:GetCreationTemplateId() == searchTemplate
			end  		
	   		if( matchesCriteria ) then
   				table.insert(items, lootObj)
	   		end
   		end

	return items or nil
end

-- OLD VERSION, not used in new crafter professions. Used elsewhere.
function GetItem(target,searchTemplate)
	
	if( target == nil or searchTemplate == nil ) then
		return nil
	end

	local equippedObjects = target:GetAllEquippedObjects()
	for index,equippedItem in pairs(equippedObjects) do
		if (equippedItem:GetCreationTemplateId() == searchTemplate) then
			return equippedItem
		end
	end

	local backpackObj = target:GetEquippedObject("Backpack")

	if( backpackObj == nil ) then
		return nil
	end

	lootObjects = backpackObj:GetContainedObjects()
   	for index, lootObj in pairs(lootObjects) do	    		
   		if(lootObj:GetCreationTemplateId() == searchTemplate ) then
   			return lootObj
   		end
   	end

   	return nil
end


function GetItemUserAction(target,user)
	local resourceType = target:GetObjVar("ResourceType")
	if(resourceType ~= nil) then
		local itemName = target:GetObjVar("SingularName") or target:GetName()

		return {
			ID=resourceType,
			ActionType="Resource",
			DisplayName=itemName,
			Icon="LighterSlotIcon",
			IconObject=target:GetIconId(),
			IconObjectColor=target:GetColor(),
			IconObjectHue=target:GetHue(),
			Enabled=true,
			ServerCommand="useresource " .. resourceType,
		}
	else
		local actionType = "Object"
		local serverCommand = "use " .. target.Id

		local equipSlot = GetEquipSlot(target)
		if( GetEquipSlot(target) ~= nil 
				and GetEquipSlot(target) ~= "Backpack" 
				and GetEquipSlot(target) ~= "TempPack" 
				and GetEquipSlot(target) ~= "Bank") then
			actionType="Equipment"
			serverCommand="equip " .. target.Id
		end

		return {
			ID=tostring(target.Id),
			ActionType=actionType,
			DisplayName=target:GetName(),
			Icon="LighterSlotIcon",
			IconObject=target:GetIconId(),
			IconObjectColor=target:GetColor(),
			IconObjectHue=target:GetHue(),
			Enabled=true,
			ServerCommand=serverCommand
		}
	end
end

-- Key functions that must be global since they are used by the engine callback (Use cases)

function GetKeyRing(user)
	return user:GetObjVar("KeyRing")
end

function KeyMatches(key,lockUniqueId)
	--Check if key has same lock id and if key actually is a key instead of box
	return ( lockUniqueId ~= nil and key:GetObjVar("lockUniqueId") == lockUniqueId and key:HasModule("key"))
end

function GetKey(user,lockObject)
    if(user ~= nil and lockObject ~= nil and user:IsValid() and lockObject:IsValid()) then
    	local lockUniqueId = lockObject:GetObjVar("lockUniqueId")
    	
    	-- first check keyring
    	local keyRing = GetKeyRing(user)
    	if(keyRing ~= nil and keyRing:IsValid()) then
    		local keyObj = FindItemInContainerRecursive(keyRing,function(item)
	    				return KeyMatches(item,lockUniqueId)
					end)
    		
	    	if(keyObj) then
	    		return keyObj
	    	end
	    end

	    local backpackObj = user:GetEquippedObject("Backpack")
	    if(backpackObj ~= nil and backpackObj:IsValid()) then
	    	local keyObj = FindItemInContainerRecursive(backpackObj,function(item)
	    				return KeyMatches(item,lockUniqueId)
					end)
	    		
	    	if(keyObj) then
	    		return keyObj
	    	end	    	
	    end
	end
	return nil
end

function GetCreationWeight(template,amount)
	amount = amount or 1
	local templateWeight = GetTemplateObjectProperty(template,"Weight") or -1
	if(templateWeight ~= -1 and amount > 1) then
		local unitWeight = GetTemplateObjVar(template,"UnitWeight") or 1
		templateWeight = math.ceil(math.max(1,amount*unitWeight))
	end

	return templateWeight
end

-- get the weight of a single unity of this object (intended for stackables)
function GetUnitWeight(targetObj,amount)
	if(targetObj == nil or not targetObj:IsValid()) then
		return 0
	end
	
	if(not(IsStackable(targetObj))) then
		return GetWeight(targetObj) * amount
	else
		if(targetObj:GetCreationTemplateId() == "coin_purse") then
			return ServerSettings.Misc.CoinWeight * amount
		else
			local stackCount = GetStackCount(targetObj)
			-- DAB TODO: Should we assume the unitweight is 1?
			local singleWeight = targetObj:GetObjVar("UnitWeight") or 1
			return singleWeight * amount
		end
	end
end

function GetRandomLootItemIndex(availableItems)
	-- get total weight
	local totalWeight = 0
	for index,item in pairs(availableItems) do
		local weight = item.Weight or 1
		totalWeight = totalWeight + weight
	end

	local roll = math.random() * (totalWeight);--uses float now instead of int only
	--local roll = math.random(1,totalWeight)
	--DebugMessage("Total Chance: "..totalChance.." Roll: "..roll)
	local curCount = 0
	for index,item in pairs(availableItems) do
		local weight = item.Weight or 1
		--DebugMessage("Cur Chance: "..(tostring(curCount + item.Chance)))
		if( roll <= curCount + weight ) then
			return index
		end
		curCount = curCount + weight
	end
end

-- rolls chance for each item to get a filtered list of
-- items to perform the weight roll
function FilterLootItemsByChance(lootItems,multiplier)
	local multiplier = multiplier or 1
	local availableItems = {}
	for index, lootEntry in pairs(lootItems) do
		if( lootEntry.Template ~= nil or lootEntry.Templates ~= nil ) then
			local chance = lootEntry.Chance or 100
			local roll = (math.random(1,10000)) * multiplier
			if( roll <= (chance * 100) ) then
				table.insert(availableItems, lootEntry)
			end
		end
	end

	return availableItems
end


--- Hook function to set the tooltip of an item, should be called on creation ( like loot and crafting n stuff )
-- @param item gameObj
-- @param noUseCases boolean By default, use cases are applied. Set this to true to prevent that.
function SetItemTooltip(item, noUseCases)
	local tooltipInfo = {}
	local itemTemplate = item:GetCreationTemplateId()

	-- Arena Rewards
	RankedArena.SetArenaAwardTooltip( item, tooltipInfo )

	-- Heirlooms
	if( HeirloomsHelper and HeirloomsHelper.IsHeirloom( item ) ) then
		tooltipInfo = HeirloomsHelper.UpdateTooltip( item , tooltipInfo)
	end

	-- Masks
	if( 
		itemTemplate == "mask_pve"
		or itemTemplate == "mask_pvp"
		or itemTemplate == "founders_mask"
		or itemTemplate == "reward_mask"
		or itemTemplate == "reward_markeedragon_head"
	 ) then
		tooltipInfo.MaskTooltip = {
            TooltipString = "Wearable over armor.",
            Priority = -100,
        }
	 end
	
	-- Monolith Schematic
	tooltipInfo = MonolithSchematic.SetTooltip( item, tooltipInfo )

	-- Sorcery Wisps
	local wispCount = item:GetObjVar("WispCount")
	if( wispCount and wispCount > 0 ) then
		tooltipInfo.WispCount = {
            TooltipString = "Maximum Wisps: " .. wispCount,
            Priority = -8455,
        }
	end

	-- Gardening
	if(  Gardening.IsPlant(item) ) then
		tooltipInfo = Gardening.GeneratePlantTooltip(item, tooltipInfo)
	end

	--[[
	-- add Militia rank requirement
	local rankRequired = item:GetObjVar("MilitiaRankRequired")
	if ( rankRequired ) then
		tooltipInfo.MilitiaRankRequired = {
            TooltipString = "Requires Militia rank "..rankRequired,
            Priority = 999996,
        }
	end
	]]

	-- Crafting Order
	if( item:HasObjVar("OrderInfo") ) then
		tooltipInfo = UpdateCraftOrderTooltip( item, tooltipInfo )
	end

	-- add the blessed/cursed item property
	if ( item:HasObjVar("Blessed") ) then
		if ( item:HasObjVar("Godly") ) then
			tooltipInfo.Blessed = {
				TooltipString = "Godly",
				Priority = 999995,
		    }
		elseif ( item:HasObjVar("Cursed") ) then
			tooltipInfo.Blessed = {
				TooltipString = "Cursed",
				Priority = 999994,
			}
		else
			tooltipInfo.Blessed = {
				TooltipString = "Blessed",
				Priority = 999993,
			}
		end
	end

	-- add Executioner info
	local executioner = item:GetObjVar("Executioner")
	if ( executioner ~= nil ) then
		tooltipInfo.Executioner = {
			TooltipString = string.format(ServerSettings.Executioner.LevelString[item:GetObjVar("ExecutionerLevel") or 1], executioner),
			Priority = 999992,
		}
	end

	-- add durable (double durability) item property
	if ( item:HasObjVar("Durable") ) then
		tooltipInfo.Durable = {
			TooltipString = "Durable",
			Priority = 999990,
		}
	end

	-- enchants
	local enchants = item:HasObjVar("Enchants")
	if ( enchants == true ) then
		tooltipInfo.Enchants = {
			TooltipString = EnchantingHelper.GetEnchantTooltips(item),
			Priority = -8500,
		}
	end

	-- Attunement
    local attuned = item:HasObjVar("Attunement")
    if ( attuned == true ) then
        local attuneVal = item:GetObjVar("Attunement")
        local weaponType = Weapon.GetType(item) or nil
        local tipStr = ""
		
		--DebugMessage("WeaponType: " .. weaponType)

		-- Wand
		if( weaponType == "MagicWand" or 
		weaponType == "SorcererMagicWand" or 
		--SCAN ADDED
		weaponType == "LightningWand" or
		weaponType == "FireWand" or
		weaponType == "HealWand" ) then
			if( attuneVal == 25 ) then
				tipStr = "Powerful Magical Attunement"
			elseif( attuneVal == 33.3 ) then
				tipStr = "Greater Magical Attunement"
			elseif( attuneVal == 50 ) then
				tipStr = "Some Magical Attunement"
			elseif( attuneVal == 100 ) then
				tipStr = "Lesser Magic Attunement"
			end
        -- Staff
		elseif( weaponType == "MagicStaff" or 
		weaponType == "SorcererMagicStaff" or
		--SCAN ADDED
		weaponType == "LightningStaff" or
		weaponType == "FireStaff" or
		weaponType == "HealStaff" ) then
			if( attuneVal == 12.5 ) then
				tipStr = "Powerful Magical Attunement"
			elseif( attuneVal ==16.6 ) then
				tipStr = "Greater Magical Attunement"
			elseif( attuneVal == 25 ) then
				tipStr = "Some Magical Attunement"
			elseif( attuneVal == 50 ) then
				tipStr = "Lesser Magic Attunement"
			end
        end
        
        
        tooltipInfo.Attuned = {
            TooltipString = tipStr,
            Priority = -8450,
        }
    end

	-- Durability
	if( CanHaveDurability(item) and not(item:HasObjVar("Blessed")) ) then
		local tipStr = GetDurabilityString(item)
		--DebugMessage( "TipStr: " .. tipStr )
		tooltipInfo.Durability = {
			TooltipString = tipStir,
			Priority = -99999,
		}
	end
	
	-- add the maker's mark
	if ( ServerSettings.Crafting.MakersMark.Enabled and item:HasObjVar("CraftedBy") ) then
		tooltipInfo.MakersMark = {
			TooltipString = "\n" .. string.format(ServerSettings.Crafting.MakersMark.MakersMark, item:GetObjVar("CraftedBy")),
			Priority = -8888,
		}
	end

	-- Discoverer Mark
	if ( item:HasObjVar("Discoverer") ) then
		tooltipInfo.DiscovererMark = {
			TooltipString = "Discovered by: " .. item:GetObjVar("Discoverer"),
			Priority = -8889,
		}
	end

	--SCAN ADDED
	-- add the DuraBonus
	if ( ServerSettings.Crafting.MagicDurability.Enabled and item:HasObjVar("DuraBonus") ) then
		tooltipInfo.MakersMark = {
			TooltipString = "\n" .. string.format(ServerSettings.Crafting.MagicDurability.DuraBonus, item:GetObjVar("DuraBonus")),
			Priority = -8890,
		}
	end

	-- add player merchant prices
	local price = item:GetObjVar("itemPrice")
	if ( price and price > 0 ) then
		tooltipInfo.item_price = {
			TooltipString = "Price: "..ValueToAmountStr(price).."\n",
			Priority = 100,
		}
	end

	if ( item:HasObjVar("locked") ) then
		tooltipInfo.lock = {
			TooltipString = "[FF0000]*Locked*[-]",
			Priority = -1,
		}
	end

	if ( item:HasObjVar("LockedDown") ) then
		tooltipInfo.locked_down = {
			TooltipString = "Locked Down",
			Priority = 98,
		}
	end

	-- required account age
	local requiredAge = item:GetObjVar("RequiredAccountAge")
	if ( requiredAge ) then
		tooltipInfo.required_age = {
			TooltipString = "\nRequires "..requiredAge.." month Premium Account Age",
			Priority = -9999,	
		}
	end
	
	-- add equipment tooltips
	tooltipInfo = GetEquipmentTooltipTable(item, tooltipInfo)

	-- default weapons/armor to double click to equip
	local slot = item:GetSharedObjectProperty("EquipSlot")
	if ( slot ~= nil and slot ~= "TempPack" and slot ~= "Bank" and slot ~= "Familiar" and slot ~= "Mount" and not item:IsContainer() ) then
		item:SetSharedObjectProperty("DefaultInteraction", "Equip")
	end

	local resourceType = item:GetObjVar("ResourceType")
	if ( resourceType ) then
		-- add resource tooltips
		tooltipInfo = GetResourceTooltipTable(resourceType, tooltipInfo, item)
		
		-- add food tooltips
		tooltipInfo = GetFoodTooltipTable(resourceType, tooltipInfo)

		-- by default add the use cases
		if ( noUseCases ~= true ) then
			ApplyResourceUsecases(item, resourceType)
		end

		CallResourceInitFunc(item, resourceType)
	end
	
	if ( tooltipInfo ) then
		for id,data in pairs(tooltipInfo) do -- ensure there's at least one entry.
			SetTooltip(item, tooltipInfo)
			return
		end
	end
end