require 'dynamic_spawn_controller'
mDynamicSpawnKey = "DynamicUndeadShrineSpawner"

mDynamicHUDVars.Title = "Unholy Rituals"
mDynamicHUDVars.Description = "Kill undead to stop the shrine from activating."

mEffects = {}

local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        PlayEffectAtLoc("FlameAuraEffect",this:GetLoc(),10)
        msg = "The ritual has started."
    elseif( progress == 2 ) then 
        PlayEffectAtLoc("RadiationAuraEffect",this:GetLoc(),10)
        msg = "You hear a low hum coming from the shrine."
    elseif( progress == 3 ) then 
        PlayEffectAtLoc("GrimAuraEffect",this:GetLoc(),10)
        msg = "The shrine goes silent."
    elseif( progress == 999 ) then 
        msg = "You failed to stop the shrine from activating."
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end