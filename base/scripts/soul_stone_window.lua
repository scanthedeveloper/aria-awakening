mSoulStoneObj = nil
mSelectedSkill = nil
mExpandedCombat = true
mExpandedTrade = false

function ValidateSoulStone()
	if not(mSoulStoneObj) or not(mSoulStoneObj:IsValid()) then
		return false
	end

	if( mSoulStoneObj:TopmostContainer() ~= this) then
		this:SystemMessage("The soul stone must be in your backpack.","info")
		return false
	end

	return true
end

function CloseSoulStoneWindow()
	this:CloseDynamicWindow("SoulStone")
	this:DelModule("soul_stone_window")
end
RegisterEventHandler(EventType.Message,"CloseSoulStone",CloseSoulStoneWindow)

function ShowSoulStoneWindow()
	if not(ValidateSoulStone()) then
		CloseSoulStoneWindow()
		return
	end	

	local skillCount = 0
	local skillTable = {}
	for i,j in pairs(SkillData.AllSkills) do 
		if(GetSkillLevel(this,i) > 30) then
			table.insert(skillTable,{Id=i,Text=GetSkillDisplayName(i)})
			skillCount = skillCount + 1
		end
	end

	if(skillCount == 0) then
		this:SystemMessage("No skill eligible for Soul Stone transfer.","info")
		CloseSoulStoneWindow()
		return
	end

	table.sort(skillTable,function (a,b)
		return a.Text < b.Text
	end)
			
	ButtonMenu.Show{
    	TargetUser = this,
        DialogId = "SoulStone",
        TitleStr = "Soul Stone",
        SubtitleStr = "Select a Skill",
        ResponseType = "id",
        Buttons = skillTable,
        ResponseFunc = function(users,buttonResult)
        	if(buttonResult and buttonResult ~= "") then
	        	ConfirmTransfer(buttonResult)
	        else
	        	CloseSoulStoneWindow()
				return
	        end
        end,
    }
end

function ConfirmTransfer(skillName)
	if not(ValidateSoulStone()) then
		CloseSoulStoneWindow()
		return
	end	

	ClientDialog.Show{
	        TargetUser = this,
	        ResponseObj = this,
	        DialogId = "ConfirmSoulStoneTransfer",
	        TitleStr = "Confirm Skill Transfer",
	        DescStr = "You are about to transfer your "..GetSkillDisplayName(skillName).." skill into this soul stone. This soul stone will be consumed when the skill is retrieved. Skills can only be retrieved from the same account. Are you sure you wish to do this?",   
	        ResponseFunc = function (user,buttonId)
	        	if(buttonId == 0) then
		        	if not(ValidateSoulStone()) then
						CloseSoulStoneWindow()
						return
					end
					local storedLevel = GetSkillLevel(this,skillName)
					mSoulStoneObj:SetObjVar("StoredSkill",skillName)
					mSoulStoneObj:SetObjVar("StoredSkillLevel",storedLevel)
					mSoulStoneObj:SetObjVar("Owner",this)
					mSoulStoneObj:SetObjVar("UserId",this:GetAttachedUserId())
					SetSkillLevel(this,skillName,0)
					this:SystemMessage("You have successfully transferred your skill into the stone.","info")
					mSoulStoneObj:SetSharedObjectProperty("Variation","Activated")
					SetTooltipEntry(mSoulStoneObj,"Tip1","Activate this stone to retrieve the skill stored within.\n\nSkill Stored\n"..GetSkillDisplayName(skillName)..": "..tostring(storedLevel).."\n\nOwner: " .. this:GetName() .. "\n")
					CloseSoulStoneWindow()
				else
					CloseSoulStoneWindow()
				end
	        end
	    }
end

RegisterEventHandler(EventType.LoadedFromBackup,"",
	function ()
		this:DelModule("soul_stone_window")
	end)

RegisterEventHandler(EventType.ModuleAttached,"soul_stone_window",
	function ( ... )
		mSoulStoneObj = initializer.SoulStoneObj
		
		ShowSoulStoneWindow()
	end)