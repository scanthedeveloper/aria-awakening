local keepDoor, militiaId

militiaInRangeCount = 0

maxHealth = ServerSettings.Militia.Events[3].RamMaxHP
this:SetStatMaxValue("Health", math.floor(maxHealth))
this:SetStatVisibility("Health","Global")

function UpdateTooltip(curHP)
	curHP = curHP or this:GetStatValue("Health")
	SetTooltipEntry(this,"amount","HP: "..curHP .." / ".. maxHealth)
end

function GetKeepDoor()
	if not(keepDoor) then
		keepDoor = FindObject(SearchTemplate("keep_wood_door",20))
	end
	return keepDoor
end

function GetMilitiaId()
    if not(militiaId) then
        militiaId = this:GetObjVar("MilitiaId")
    end
	return militiaId
end

function DoSwing()	
	local curAnimState = this:GetSharedObjectProperty("Animation")
	if(curAnimState == "hit1") then
		this:SetSharedObjectProperty("Animation","hit2")
	else
		this:SetSharedObjectProperty("Animation","hit1")
	end

	if(keepDoor) then			
		CallFunctionDelayed(TimeSpan.FromSeconds(0.4),function ( ... )
			keepDoor:PlayObjectSound("event:/character/combat_abilities/shockwave",false)
		end)
		CallFunctionDelayed(TimeSpan.FromSeconds(0.8),function ( ... )
			keepDoor:SendMessage("DoDamage",ServerSettings.Militia.Events[3].RamBaseDamage)
		end)
	end	
end

function DoDamage(amount)
	local oldHP = this:GetStatValue("Health")
    local curHP = math.max(oldHP - amount, 0)

    if(curHP ~= oldHP) then
        this:SetStatValue("Health",curHP)
        UpdateTooltip(curHP)
        if(curHP == 0) then
        	militiaInRangeCount = 0
        	this:SetSharedObjectProperty("Animation","destroyed")
        	this:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact",false)
            CallFunctionDelayed(TimeSpan.FromSeconds(3),
            	function ()
            		this:Destroy()
            	end)
        end
    end
end

RegisterEventHandler(EventType.Message,"Swing",
	function ( ... )
		DoSwing()
	end)


RegisterEventHandler(EventType.Timer,"SwingTimer",
	function ( ... )		
		this:ScheduleTimerDelay(ServerSettings.Militia.Events[3].RamAttackSpeed,"SwingTimer")

		local damageAmt = ServerSettings.Militia.Events[3].RamDamagePerSecond * 5
		
		local keepDoor = GetKeepDoor()
		if(not(keepDoor) or keepDoor:GetStatValue("Health") <= 0) then
			damageAmt = maxHealth / 5
			DoDamage(damageAmt)
		elseif(GetMilitiaInRange() >= ServerSettings.Militia.Events[3].RamAttackNumPlayers) then
			DoSwing()
		else
			if( not(this:HasTimer("RamDamageWarn")) ) then
				local keepTownship = keepDoor:GetObjVar("Township")
	            Militia.EventMessageMilitia("The battering ram at the ".. keepTownship .." keep needs more operators.",GetMilitiaId())
	            this:ScheduleTimerDelay(TimeSpan.FromMinutes(5), "RamDamageWarn")
			end

			DoDamage(damageAmt)
		end		
	end)

function GetMilitiaInRange()
	local militiaInRangeCount = 0
	for i,playerObj in pairs(FindObjects(SearchPlayerInRange(12))) do
		if(GetMilitiaId() == Militia.GetId(playerObj) and not(IsDead(playerObj))) then
			militiaInRangeCount = militiaInRangeCount + 1
		end
	end

	--DebugMessage("InRange: "..militiaInRangeCount)
	return militiaInRangeCount
end

function OnInit()	
	this:ScheduleTimerDelay(ServerSettings.Militia.Events[3].RamAttackSpeed,"SwingTimer")
end

RegisterEventHandler(EventType.ModuleAttached,"militia_battering_ram",
	function ( ... )
		CallFunctionDelayed(TimeSpan.FromSeconds(0.3),function ( ... )
			this:PlayObjectSound("event:/character/combat_abilities/shield_bash",false)
		end)		

		this:SetStatValue("Health", maxHealth)

		UpdateTooltip(maxHealth)

		OnInit()
	end)

RegisterEventHandler(EventType.LoadedFromBackup,"",
	function ( ... )
		OnInit()
	end)

