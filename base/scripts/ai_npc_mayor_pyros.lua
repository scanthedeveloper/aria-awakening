require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false
AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true


--[[ QUEST ]]
AI.QuestsWelcomeText ="Greetings traveler, do you have a moment to talk about the benefits of becoming a citizen of Pyros?"

    AI.QuestAvailableMessages = {"I have a matter I wish to talk over with you."}
    
    AI.QuestStepsInvolvedIn = {
        {"JoinTownshipPyros", 1},
        {"LeaveTownshipPyros", 1},
    }
    


function Dialog.OpenGreetingDialog(user)
        local text = "Hello there, the sea air is crisp and the fish are plentiful, what more could we want! Is there something you need?"
        local response = {}

        response[1] = {}
        response[1].text = "Who are you?"
        response[1].handle = "Who"
        
        response[2] = {}
        response[2].text = "Goodbye."
        response[2].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
end


function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,"I am the mayor here in Pyros. My main focus is helping to build this great city of Pyros into a powerhouse of commerce and arms.")
end