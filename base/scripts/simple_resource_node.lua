--A spawnable resource node for resources harvesters

RegisterEventHandler(EventType.Message, "RequestResource", 
	function(harvester, user, depletionAmount)
		
		local resourceSourceId = this:GetObjVar("ResourceSourceId")
		if (resourceSourceId == nil) then
			return
		end

		local remaining = (this:GetObjVar("RemainingResources") or 0) - depletionAmount
		if (remaining > 0) then
			--Harvest
			this:SetObjVar("RemainingResources", remaining)
			harvester:SendMessage("RequestResourceResponse", true, user, resourceSourceId, this, remaining, nil, true)
		elseif (remaining == 0) then

			--Depleted
			this:SetSharedObjectProperty("VisualState", "Depleted")
			this:SetObjVar("RemainingResources", 0)
			this:SendMessage("NodeDepleted")
			harvester:SendMessage("RequestResourceResponse", false, user, resourceSourceId, this, remaining, "Depleted")

			local decayTimer = this:GetObjVar("DecayTime")
			if (decayTimer) then
				Decay(this, decayTimer)
			end

			--Add to lifetime count
			local lifetimePlayerStats = user:GetObjVar("LifetimePlayerStats")
			if not(lifetimePlayerStats) then lifetimePlayerStats = {} end
			if not(lifetimePlayerStats.NodesHarvested) then lifetimePlayerStats.NodesHarvested = {} end
			
			if not(lifetimePlayerStats.NodesHarvested[resourceSourceId]) then 
				lifetimePlayerStats.NodesHarvested[resourceSourceId] = 1
			else
				lifetimePlayerStats.NodesHarvested[resourceSourceId] = lifetimePlayerStats.NodesHarvested[resourceSourceId] + 1
			end
			user:SetObjVar("LifetimePlayerStats", lifetimePlayerStats)


			--Respawn node
			if not(this:HasTimer("ResourceRegen")) then
				local regenMins = this:GetObjVar("RegenMins")
				if (regenMins ~= nil) then
					this:ScheduleTimerDelay(TimeSpan.FromMinutes(regenMins),"ResourceRegen")
				end
			end
		else
			--Attempting to harvest depleted node
			harvester:SendMessage("RequestResourceResponse", false, user, resourceSourceId, this, remaining, "Depleted")
		end
	end)

function ResetNode()
	local resourceCount = this:GetObjVar("MaxResources") or 1
	if (resourceCount ~= nil) then
		this:SetObjVar("RemainingResources", resourceCount)
	end

	local resourceSourceId = this:GetObjVar("ResourceSourceId")
	local hue = OreVeins[resourceSourceId] or FabricationResourceNodes[resourceSourceId]
	if (hue) then
		this:SetHue(hue)
	else
		this:SetHue(0)
	end

	this:SetSharedObjectProperty("VisualState", "Default")
end

RegisterEventHandler(EventType.Timer, "ResourceRegen",
	function()
		ResetNode()
	end)

RegisterEventHandler(EventType.Message, "ResetNode",
	function()
		ResetNode()
	end)

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(),
	function()
		ResetNode()
	end)

