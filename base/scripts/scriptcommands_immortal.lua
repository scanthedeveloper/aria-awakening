require 'scriptcommands_UI_edit'
require 'scriptcommands_UI_goto'
require 'scriptcommands_UI_create'
require 'scriptcommands_UI_info'
require 'scriptcommands_UI_search'
require 'scriptcommands_UI_setquest'

RegisterEventHandler(EventType.ClientTargetLocResponse, "jump", 
	function(success,targetLoc)
		local commandInfo = GetCommandInfo("jump")

		if not(LuaCheckAccessLevel(this,commandInfo.AccessLevel)) then return end	

		if( success ) then
			this:RequestClientTargetLoc(this, "jump")
			if( IsPassable(targetLoc) ) then
				this:SetWorldPosition(targetLoc)
				this:PlayEffect("TeleportToEffect")
			end
		end	
	end)

RegisterEventHandler(EventType.Timer,"Clock",
	function()
		local clockWindow = DynamicWindow("ClockWindow","Clock",100,100)
		clockWindow:AddLabel(100,30,GetGameTimeOfDayString())

		local label = "Broken"
		local isDaytime = not IsNightTime()
		if (isDaytime) then
			label = "Day"
		else
			label = "Night"
		end

		clockWindow:AddLabel(100,50,label)
		this:OpenDynamicWindow(clockWindow)

		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"Clock")
	end)

RegisterEventHandler(EventType.DynamicWindowResponse,"ClockWindow",
	function()
		this:RemoveTimer("Clock")
	end)

UserListPage = 1

function ShowUserList(selectedUser, keyword)
	if (selectedUser == nil) then selectedUser = this end
	local newWindow = DynamicWindow("UserList","Player List",450,530) 
	local allPlayers = FindPlayersInRegion()
	
	if(#allPlayers == 0) then
		table.insert(allPlayers,{Name="Center",Loc=Loc(0,0,0)})
	end

	local scrollWindow = ScrollWindow(20,40,380,375,25)
	for i,player in pairs(allPlayers) do
		local name = player:GetName()
		if ( not keyword or string.lower(name):match(keyword) ) then
			local scrollElement = ScrollElement()	

			if((i-1) % 2 == 1) then
				scrollElement:AddImage(0,0,"Blank",360,25,"Sliced","1A1C2B")
			end
			
			scrollElement:AddLabel(5, 3, name,0,0,18)

			local selState = ""
			if(player.Id == selectedUser.Id) then
				selState = "pressed"
			end
			scrollElement:AddButton(340, 3, "select|"..player.Id, "", 0, 18, "", "", false, "Selection",selState)
			scrollWindow:Add(scrollElement)
		end
	end
	newWindow:AddScrollWindow(scrollWindow)

	-- Goto
	newWindow:AddButton(15, 420, "teleport|"..selectedUser.Id, "Tele To", 100, 23, "", "", false,"",buttonState)
	newWindow:AddButton(115, 420, "teleportToMe|"..selectedUser.Id, "Tele Here", 100, 23, "", "", false,"",buttonState)
	newWindow:AddButton(215, 420, "heal|"..selectedUser.Id, "Heal", 100, 23, "", "", false,"",buttonState)
	newWindow:AddButton(315, 420, "resurrect|"..selectedUser.Id, "Resurrect", 100, 23, "", "", false,"",buttonState)
	
	this:OpenDynamicWindow(newWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"UserList",
	function (user,returnId)
		if(returnId ~= nil) then
			action = StringSplit(returnId,"|")[1]
			playerId = StringSplit(returnId,"|")[2]
			--DebugMessage("action is "..tostring(action)) 
			--DebugMessage("playerId is "..tostring(playerId))
			if (playerId ~= nil) then
				local playerObj = GetPlayerByNameOrId(playerId)
				if(action== "teleport") then
					if( playerObj ~= nil ) then
						this:SetWorldPosition(playerObj:GetLoc())
						this:PlayEffect("TeleportToEffect")
					end
				elseif(action== "teleportToMe") then
					if( playerObj ~= nil ) then
						playerObj:PlayEffect("TeleportFromEffect")
						playerObj:SetWorldPosition(this:GetLoc())
						playerObj:PlayEffect("TeleportToEffect")
					end
				elseif(action== "select") then
					local playerObj = GetPlayerByNameOrId(playerId)
					ShowUserList(playerObj)
				elseif(action=="heal") then
					local curHealth = math.floor(GetCurHealth(playerObj))
					local healAmount = GetMaxHealth(playerObj) - curHealth
					SetCurHealth(playerObj,curHealth + healAmount)
					playerObj:PlayEffect("HealEffect")
					this:SystemMessage("Healed "..playerObj:GetName().." for " .. healAmount,"event")
				elseif(action== "resurrect") then
					if( playerObj ~= nil ) then
						playerObj:SendMessage("Resurrect",1.0,this)
					end
				end
			end
		end	
	end)

isFollowing = false
RegisterEventHandler(EventType.ClientTargetGameObjResponse,"follow",
	function (target,user)
		if(target ~= nil) then
			if(IsImmortal(user) or ShareGroup(target,user) ) then
				local runspeed = ServerSettings.Stats.RunSpeedModifier
				if ( IsMounted(this) ) then
					runspeed = ServerSettings.Stats.MountSpeedModifier
				end
				user:PathToTarget(target,1,runspeed)			
				isFollowing = true	
			end
		end
	end)

--ToggleInvuln scriptcommand
function DoToggleInvuln(targetObj)
	if(targetObj == nil or not(targetObj:IsValid())) then return end

	if(targetObj:IsPlayer() and not IsImmortal(targetObj)) then
		this:SystemMessage("You cannot set a mortal player to invulnerable.")
	elseif(targetObj:HasObjVar("Invulnerable")) then
		targetObj:DelObjVar("Invulnerable")
		this:SystemMessage(targetObj:GetName().."("..tostring(targetObj.Id)..") Invulnerable Off")
	else
		targetObj:SetObjVar("Invulnerable",true)
		this:SystemMessage(targetObj:GetName().."("..tostring(targetObj.Id)..") Invulnerable On")
	end
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse,"invuln",
	function(target,user)
		DoToggleInvuln(target)
	end)

--Freeze scriptcommand
function DoFreeze(target)	
	if( target:IsValid() ) then
		if ( HasMobileEffect(target, "GodFreeze") ) then
			target:SendMessage("EndGodFreezeEffect")
		else
			target:SendMessage("StartMobileEffect", "GodFreeze")
		end
	end
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "freezeTarget", 
	function(target,user)
		if( target == nil ) then
			return
		end

		DoFreeze(target)
	end
)

--Taming scriptcomamnd
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "tamecmd",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end
		SetCreatureAsPet(target, this)
	end)

--OpenContainer scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "container",
	function(target,user)
		if not(IsImmortal(this)) then return end

		if( target == nil ) then
			return
		end

		target:SendOpenContainer(user)
	end)

ImmortalCommandFuncs = {

	Clock = function()
		this:FireTimer("Clock")
	end,

	WhoDialog =	function(keyword)
		ShowUserList(this, keyword)
	end,

	Cloak = function()	
		local targetObj = this	

		if( targetObj ~= nil ) then
			local isCloaked = targetObj:IsCloaked()
			targetObj:SetCloak(not(isCloaked))
		end
	end,

	TeleportTo = function(nameOrId)
		if( nameOrId ~= nil) then	
			if(nameOrId:sub(1,1) == "$" or nameOrId:sub(1,1) == "$") then
				local permObj = PermanentObj(tonumber(nameOrId:sub(2)))
				if( permObj ~= nil ) then
					this:SetWorldPosition(permObj:GetLoc())
					this:PlayEffect("TeleportToEffect")
				end
			else
				local playerObj = GetPlayerByNameOrId(nameOrId)
				if( playerObj ~= nil ) then
					playerObj = playerObj:TopmostContainer() or playerObj
					this:SetWorldPosition(playerObj:GetLoc())
					this:PlayEffect("TeleportToEffect")
				end
			end
		else
			Usage("teleportto")
		end
	end,

	Jump = function(id, force)
		if ( id ) then
			local player = GameObj(tonumber(id))
			if ( force == "force" or GlobalVarReadKey("User.Online", player) ) then
				RequestPlayerLocation(player, this, function(loc, address)
					if ( loc and address ) then
						TeleportUser(GameObj(0), this, loc, address, nil, true, true, true)
					else
						this:SystemMessage("Failed to jump to user.", "info")
					end
				end)
			else
				this:SystemMessage("Did not find that player online. Add force to skip check.", "info")
			end
		else
			this:RequestClientTargetLoc(this, "jump")
		end
	end,

	Goto = function(...)	
		local args = table.pack(...)
		if( #args < 1 ) then
			ShowGoToList()
			return
		end

		local x, y, z = 0, 0, 0
		if( #args == 2 ) then
			x = tonumber(args[1])
			z = tonumber(args[2])
		else
			x = tonumber(args[1])
			y = tonumber(args[2])
			z = tonumber(args[3])
		end
		if (type(x) ~= "number" or type(y) ~= "number" or type(z) ~= "number") then return end
		this:SetWorldPosition(Loc(x,y,z))
	end,

	JoinGuild = function(guildId,guildName,guildTag)
		local isNew = guildId == nil or guildId == "" or guildId == "new"

		if(GuildHelpers.Get(this)) then
			this:SystemMessage("You must leave your guild first")
			return
		end

		if(isNew) then 
			guildId = uuid() 
		end

		if not(GuildHelpers.GetGuildRecord(guildId)) then
			if(guildName == nil) then
				this:SystemMessage("Guild does not exist, specify a guild name!")
				return
			end
			npGuildRecord = Guild.Create(nil,guildName,guildId,guildTag)
		end

		CallFunctionDelayed(TimeSpan.FromSeconds(1),function ( ... )
			Guild.AddToGuild(guildId)
		end)
		
		CallFunctionDelayed(TimeSpan.FromSeconds(2),function ( ... )
			local guildRecord = GuildHelpers.GetGuildRecord(guildId)
			local guildRank = isNew and "Guildmaster" or "Officer"
			Guild.PromoteMember(this,guildRecord,guildRank,true)
		end)	
	end,

	Follow = function ()
		if(isFollowing) then
			this:ClearPathTarget()
			isFollowing = false
		else
			this:SystemMessage("Select the group member you wish to follow.","info")
			this:RequestClientTargetGameObj(this,"follow")
		end
	end,

	TestMortal = function ()
		if ( this:HasObjVar("TestMortal") ) then
			this:DelObjVar("TestMortal")
			this:SystemMessage("TestMortal Removed.","info")
		else
			this:SystemMessage("TestMortal Applied.","info")
			this:SetObjVar("TestMortal", true)
		end
		this:SendMessage("UpdateName")
	end,

	SearchDialog =	function(arg)
		ShowNewSearch(arg)
	end,

	CreateCamp = function (campname)		
		if( campname ~= nil ) then
			campCreate = campname
			this:RequestClientTargetLoc(this, "camp")
		end
	end,

	Tame = function(targetObj)
		if(targetObj == nil) then			
			this:RequestClientTargetGameObj(this, "tamecmd")
		else
			SetCreatureAsPet(GameObj(tonumber(targetObj)), this)
		end
	end,

	Info = function(targetObjId)
		DebugMessage("Getting Info")
		if(targetObjId ~= nil) then
			gameObj = GameObj(tonumber(targetObjId))
			if(gameObj:IsValid()) then
				DoInfo(gameObj)
				return
			else
				this:SystemMessage(tostring(targetObjId).." is not a valid id. Object does not exist.")
			end
		end
		this:RequestClientTargetGameObj(this, "info")		
	end,

	Freeze = function()
		if(targetObjId == nil) then
			this:RequestClientTargetGameObj(this, "freezeTarget")
			return
		end

		local gameObj = this
		if( targetObjId ~= "self" ) then
			gameObj = GameObj(tonumber(targetObjId))
		end

		DoFreeze(gameObj)
	end,	

	TeleportPlayer = function(nameOrId)
		if( nameOrId ~= nil) then		
			local playerObj = GetPlayerByNameOrId(nameOrId)
			if( playerObj ~= nil ) then
				playerObj:SetWorldPosition(this:GetLoc())
				playerObj:PlayEffect("TeleportToEffect")
			end
		else
			Usage("teleportplayer")
		end
	end,

	OpenContainer = function(nameOrId,equipSlot)
		if( nameOrId ~= nil) then
			if( equipSlot == nil ) then equipSlot = "Backpack" end

			local objRef = GetPlayerByNameOrId(nameOrId)
			if( objRef ~= nil ) then
				if(equipSlot == "Self" or equipSlot == "self") then
					objRef:SendOpenContainer(this)
				elseif( objRef:IsPlayer() ) then
					local contObj = objRef:GetEquippedObject(equipSlot)
					if( contObj ~= nil and contObj:IsContainer() ) then
						contObj:SendOpenContainer(this)
					end
				elseif( objRef:IsContainer() ) then
					objRef:SendOpenContainer(this)
				end
			end
		else
			this:RequestClientTargetGameObj(this, "container")	
		end
	end,

	ContainerInfo = function (id,arg)
		if(id == "all") then
			local containerSizes = {}
			for i,containerObj in pairs(FindObjects(SearchSharedObjectProperty("Capacity",nil))) do
				local count = containerObj:GetItemCount()
				table.insert(containerSizes,{Obj=containerObj,Count = count})
			end

			table.sort(containerSizes,function(a,b)
					return a.Count > b.Count
				end)

			for i=1,50 do
				if(#containerSizes >= i) then
					local objRef = containerSizes[i].Obj
					this:SystemMessage("Container "..objRef:GetName().." ("..objRef.Id..") contains "..containerSizes[i].Count.." items.")
				end
			end
			this:SystemMessage("Total World Containers: "..#containerSizes)
		else
			local objRef = GameObj(tonumber(id))
			if(objRef ~= nil and objRef:IsValid()) then
				local count = objRef:GetItemCount()
				this:SystemMessage("Container "..objRef:GetName().." contains "..count.." items.")
			else
				this:SystemMessage("Container not found. Must pass id!")
			end
		end
	end,

	--Immortal player can only toggle it's own invulnerability
	ToggleInvuln = function()
		DoToggleInvuln(this)
	end,
	
	MilitiaRemove = function(id)
		if not( id ) then
			Usage("removemilitia")
			return
		end
		local targetObj = GameObj(tonumber(id))
		if ( targetObj and targetObj:IsValid() ) then
			Militia.RemovePlayer(targetObj, true)
		end
	end,

	Who = function(keyword)
		--[[if(not(IsDemiGod(this))) then
			this:SystemMessage("Who command has been temporarily disabled.")
			return
		end]]
		if ( keyword ) then
			keyword = string.lower(keyword)
		end
		local max = 64
		local total = 0
		local isGod = IsGod(this)
		local online = GlobalVarRead("User.Online")
		for user,y in pairs(online) do
			local name = user:GetCharacterName()
			if ( name and
				(keyword and string.lower(name):match(keyword))
				or
				(not keyword and total < max)
			) then
			    this:SystemMessage(string.format("[FFBF00]%s (%s)[-]", name, user.Id))
			end
			total = total + 1
		end

		local suffix = ""
		if ( total > max ) then
			suffix = " (List Truncated)"
		end
		if ( isGod ) then
			this:SystemMessage(string.format("[FFBF00]%d players online.%s[-]", total, suffix))
		end
	end,

	IsPassable = function()
		local isPassable = IsPassable(this:GetLoc())
		this:SystemMessage("Passable: "..tostring(isPassable))
		DebugMessage("Passable: "..tostring(isPassable))
		if not(isPassable) then
			local collisionInfo = GetCollisionInfoAtLoc(this:GetLoc())
			for i,infoStr in pairs(collisionInfo) do
				this:SystemMessage(infoStr)
				DebugMessage(infoStr)
			end
		end
	end,

	ListPlots = function(userid, gotoId)
		
		if not( userid ) then return end
		
		-- userid
		if ( userid ) then
			userid = string.lower(userid)
		end

		if( gotoId ) then
			gotoId = tonumber(gotoId)
		end
		
		-- get plots
		local plots = GlobalVarRead("User.Plots."..userid) or {}
		local total = 0
		local remoteTotal = 0

		-- loop through plots
		for i, plot in pairs( plots ) do
			if(plot:IsValid()) then
				local loc = plot:GetLoc()
				this:SystemMessage(string.format("[FFBF00]Plot "..i.." @ %s[-]", loc))
				total = total + 1
				if( gotoId and gotoId == i ) then 
					this:SystemMessage(string.format("[FFBF00]Moving you to plot %d.[-]", i))
					this:SetWorldPosition( GetNearbyPassableLocFromLoc(loc,0,3) ) 
				end
			else
				local plotInfo = GlobalVarRead("Plot."..plot.Id)
				if(plotInfo) then
					remoteTotal = remoteTotal + 1
					this:SystemMessage("Plot Region: "..plotInfo.Region.." Controller: "..plotInfo.Controller.Id)
				end
			end
		end

		this:SystemMessage( string.format("[FFBF00]%d local plots found for user %s.[-]", total, userid) )
		this:SystemMessage( string.format("[FFBF00]%d remote plots found for user %s.[-]", remoteTotal, userid) )
	end,

	FilterWord = function( _word )
		if( _word ) then
			local words = ChatHelper.GetFilters() or {}
			_word = tostring(_word)
			for i=1, #words do 
				if( words[i] == _word ) then
					this:SystemMessage(string.format("%s is already a filtered word.", _word))
					return true 
				end
			end

			table.insert( words, _word )

			SetGlobalVar("Chat", function(record)
				record.Filters = words
				return true
			end, function()end)

			this:SystemMessage(string.format("'%s' added to filtered words.", _word))
			return true
		end
		
		this:SystemMessage("You must provide a word to be filtered.")
		return false
	end,

	RemoveFilterWord = function( _word )
		if( _word ) then
			local newWords = {}
			local words = ChatHelper.GetFilters() or {}
			
			_word = tostring(_word)
			for i=1, #words do 
				if( words[i] ~= _word ) then
					table.insert( newWords, words[i] )
				end
			end

			SetGlobalVar("Chat", function(record)
				record.Filters = newWords
				return true
			end, function()end)

			this:SystemMessage(string.format("'%s' removed from filtered words.", _word))
			return true
		end
		
		this:SystemMessage("You must provide a word to be filtered.")
		return false
	end,

	Filters = function()
		
		this:SystemMessage(" ----------- Filtered Words ----------- ")
		local words = ChatHelper.GetFilters()
		for i=1, #words do 
			this:SystemMessage("    "..tostring(i).. ") " .. "'"..words[i].."'")
		end
		this:SystemMessage(" -----------  End of List   ----------- ")

	end,
}


RegisterCommand{ Command="clock", 					Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Clock, Desc="Opens a window that shows the time" }
RegisterCommand{ Command="follow", 					Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Follow, Desc="Automatically follow a mob."}
RegisterCommand{ Command="cloak", 					Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Cloak, Usage="[<name|id>]", Desc="[$2476]" }
RegisterCommand{ Command="reveal", 					Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=function() DoReveal(this) end, Desc="Reveal yourself in a cool cool way." }
RegisterCommand{ Command="jump", 					Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Jump, Desc="Get a cursor for a location to jump to. Optionally provide a player's id to jump cross region to them." }
RegisterCommand{ Command="gotolocation", 			Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Goto, Usage="[<x>] [<y>] [<z>]", Desc="[$2477]", Aliases={"goto"}}
RegisterCommand{ Command="teleportto", 				Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.TeleportTo, Usage="<name|id>", Desc="Teleport to a person by name or an object by id", Aliases={"tpto"}}
RegisterCommand{ Command="whod", 					Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.WhoDialog, Desc="List players on server in a dialog window" }			
RegisterCommand{ Command="invuln", 					Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.ToggleInvuln, Usage="[<target_id|self>]", Desc="Toggle invulnerability", Aliases={"inv"} }
RegisterCommand{ Command="freeze", 					Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Freeze, Desc="Freeze/Unfreeze a mobile in place."}
RegisterCommand{ Command="teleportplayer",			Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.TeleportPlayer, Usage="<name|id>", Desc="Teleport a player to your location", Aliases={"tp"}}	
RegisterCommand{ Command="who", 					Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Who, Desc="Lists players on the server" }
RegisterCommand{ Command="listplots", 				Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.ListPlots, Usage="<userid> [plotnum]", Desc="Lists plots for user-id. Passing a plotnum moves to that plot." }
RegisterCommand{ Command="addwordfilter", 			Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.FilterWord, Usage="[<word>]", Desc="Given a word will filter it in chat." }
RegisterCommand{ Command="removewordfilter", 		Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.RemoveFilterWord, Usage="[<word>]", Desc="Given a word will remove it from the chat filter." }
RegisterCommand{ Command="filters", 				Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Filters, Desc="Lists all the filtered words." }

--RegisterCommand{ Command="portal", Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Portal, Usage="[<x>] [<y>] [<z>]", Desc="Open a two way portal to a location on the map" }
--RegisterCommand{ Command="joinguild", Category = "God Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.JoinGuild, Usage="[<guild_id>]", Desc="[$2483]" }

RegisterCommand{ Command="info", 					Category = "Dev Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Info, Desc="Get information about an object. Gives cursor" }	
RegisterCommand{ Command="search", 					Category = "Dev Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.SearchDialog, Usage = "<name>", Desc="[$2486]" }		
RegisterCommand{ Command="tame", 					Category = "Dev Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.Tame, Desc="Tame a mobile." }
RegisterCommand{ Command="opencontainer", 			Category = "Dev Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.OpenContainer, Usage="<name|id> [<equipslot>]", Desc="[$2495]", Aliases={"opencont"}}	
RegisterCommand{ Command="containerinfo", 			Category = "Dev Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.ContainerInfo, Usage="<id> [detailed]", Desc="[$2496]"}	
RegisterCommand{ Command="removemilitia", 			Category = "Dev Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.MilitiaRemove, Usage="<id>", Desc="Remove a player by id from their militia, if they are in one." }
RegisterCommand{ Command="testmortal", 				Category = "Dev Power", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.TestMortal, Desc="Toggle Yourself as a Test Mortal." }

RegisterCommand{ Command="ispassable", 				Category = "Debug", AccessLevel = AccessLevel.Immortal, Func=ImmortalCommandFuncs.IsPassable, Desc="[$2511]"}
