require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false
AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

local currencyName = QuestsLeague.LeagueCurrencyName.Procurers
local hasRewards = this:HasObjVar("HasRewards")

-- Hue 827

function Dialog.OpenGreetingDialog(user)
        local text = "An honest man can get you want you need, but I'm the type of man that can get you want you want. Don't confuse the two, one takes guts and the other, well, that's a trade secret."
        local response = {}
        local key = 1

        if( hasRewards ) then
            text = "Hello there, is there something you'd like to purchase from the League of Procurers? The table here showcases our stocks."
            
            response[key] = {}
            response[key].text = "What can I purchase?"
            response[key].handle = "PurchaseRewards"
            key = key + 1

            --response[key] = {}
            --response[key].text = "What is in the supply box?"
            --response[key].handle = "LotteryBoxInfo"
            --key = key + 1
        end

        --[[
        response[key] = {}
        response[key].text = "Who are you?"
        response[key].handle = "Who"
        key = key + 1
        ]]


        response[key] = {}
        response[key].text = "Goodbye."
        response[key].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
end

function MakeGetRewardsTable(user)
    local Rewards = {
        HairDye = {
            Name = "Explorer Hair Dye",
            Cost = 8000,
            Template = "hair_dye_procurer", -- 958
        },
        ClothDye = {
            Name = "Explorer Cloth Dye",
            Cost = 8000,
            Template = "procurer_clothing_dye",
        },
        LeatherDye = {
            Name = "Explorer Leather Dye",
            Cost = 8000,
            Template = "procurer_leather_dye",
        },
        Cloak = {
            Name = "Procurement Cloak",
            Cost = 18000,
            Template = "procurement_cloak",
        },
        Necklace = {
            Name = "Treasure Hunter's Shovel",
            Cost = 36000,
            Template = "pr10_league_tool_shovel",
        },
        Ring = {
            Name = "Lucky Tamer's Crook",
            Cost = 36000,
            Template = "pr10_league_tool_crook",
        },
        TitleScrollOne = {
            Name = "Title Scroll: The Heretic",
            Cost = 24000,
            Template = "pr10_league_procurer_titleonescroll",
        },
        TitleScrollTwo = {
            Name = "Title Scroll: The Broken",
            Cost = 24000,
            Template = "pr10_league_procurer_titletwoscroll",
        },
        TitleScrollThree = {
            Name = "Title Scroll: The Humble",
            Cost = 24000,
            Template = "pr10_league_procurer_titlethreescroll",
        },
    }

    return Rewards
end

-- Callback to alter items if needed
function AfterPurchase(item)
    return
end

function Purchase(user, itemKey, Rewards)
    
    -- See if the item is still for sale
    if ( Rewards[itemKey] == nil or Rewards[itemKey].Cost == nil or Rewards[itemKey].Template == nil ) then
        QuickDialogMessage(this,user,"That item is not available right now, unfortunately.")
        return
    end
    
    -- Check to see if the player has enough currency
    if( QuestsLeague.AdjustLeagueCurrency( user, "Procurers", -Rewards[itemKey].Cost ) ) then
        Create.InBackpack(Rewards[itemKey].Template, user, nil, function(item)
            if ( item ) then
                AfterPurchase(item)
                Dialog.OpenPurchaseRewardsDialog(user)
            else
                QuickDialogMessage(this,user,"I'm sorry something went wrong, please try that again.")
            end
        end)
    else
        QuickDialogMessage(this,user,"You do not have enough "..currencyName.." to purchase that.")
    end

end

function SetupPurchaseDialogHandles(user, Rewards)
    -- setup the dialog handles for each item purchase
    for key,val in pairs(Rewards) do
        Dialog[string.format("Open%sConfirmDialog", key)] = function(user)
            Purchase(user, key, Rewards)
        end
        Dialog[string.format("Open%sDialog", key)] = function(user)
            local text = string.format("I can sell you %s for %s "..currencyName..".", val.Name, val.Cost)
            local response = {
                {
                    text = "I accept your offer.",
                    handle = key.."Confirm",
                },
                {
                    text = "What else do you have to sell?",
                    handle = "PurchaseRewards"
                },
                {
                    text = "Goodbye.",
                    handle = ""
                },
            }
            NPCInteractionLongButton(text,this,user,"Responses",response)
        end
    end
end

function Dialog.OpenPurchaseRewardsDialog(user)
    local text = "Which item would you like to purchase?\n\nYou currently have [483D8B]"..QuestsLeague.GetLeagueCurrecy(user,"Procurers").."[-] favor with the League of Procurers."
    local response = {}
    local Rewards = MakeGetRewardsTable(user)

    SetupPurchaseDialogHandles(user, Rewards)

    for x, y in sortpairs(Rewards, function(Rewards, a, b) return Rewards[a]["Name"] < Rewards[b]["Name"] end) do
        table.insert(response, {text = y.Name..": "..y.Cost.." "..currencyName, handle = x})
    end

    table.insert( response, { text = "Goodbye.", handle = "" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenLotteryBoxInfoDialog(user)
    local text = "The supply box has..."
    local response = {}

    table.insert( response, { text = "Thank you.", handle = "Greeting" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

if( not hasRewards ) then

    -- [[ QUEST STUFF ]]
    function AI.QuestStepsInvolvedInFunc()
        local quests = { 1, 3, 5, 7, 9, 11 }
        return 
        { 
            {"ProcurersLeaguePR10", quests[math.random(#quests)]} ,
            {"ProcurersLeaguePR10", quests[math.random(#quests)]} ,
            {"ProcurersLeaguePR10", quests[math.random(#quests)]} ,
            {"ProcurerLeagueValus", 2 },
            {"ProcurerLeagueValus", 4 },
            {"ProcurerLeagueValus", 6 },
            {"ProcurerLeagueValus", 8 },
            {"ProcurerLeagueValus", 10 },
            {"ProcurerLeagueValus", 12 },
            {"ProcurerLeagueValus", 14 },
            {"ProcurerLeagueValus", 16 },
            {"ProcurerLeagueValus", 18 },
            {"ProcurerLeaguePR9", 2 },
            {"ProcurerLeaguePR9", 4 },
            {"ProcurerLeaguePR9", 6 },
            {"ProcurerLeaguePR9", 8 },
            {"ProcurerLeaguePR9", 10 },
            {"ProcurerLeaguePR9", 12 },
            {"ProcurerLeaguePR9", 14 },
            {"ProcurerLeaguePR9", 16 },
            {"ProcurerLeaguePR9", 18 },
            {"ProcurerLeaguePR9", 20 },
            {"ProcurerLeaguePR9", 22 },
            {"ProcurersLeaguePR10", 2 },
            {"ProcurersLeaguePR10", 4 },
            {"ProcurersLeaguePR10", 6 },
            {"ProcurersLeaguePR10", 8 },
            {"ProcurersLeaguePR10", 10 },
            {"ProcurersLeaguePR10", 12 },
        }
    end

    AI.QuestsWelcomeText ="Psst, listen I'm in need of someone with a knack for finding things and keeping quiet about it. You interested?"

    AI.QuestAvailableMessages = 
    {
        "I've a job for you to do.",
    }

    function Dialog.OpenWhoDialog(user)
        QuickDialogMessage(this,user,"I think it's best we not share too much about ourselves, in this line of business that's not the wisest choice. I'll just say, if you've thought of it I've bought and sold it -- twice.")
    end

end