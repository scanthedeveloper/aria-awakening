require 'base_ai_npc'

AI.Settings.StationedLeash = true
--AI.Settings.CanConverse = false
AI.Settings.SpeechTable = "Pirate"
AI.Settings.FallbackSpeechTable = "Pirate"

AI.GreetingMessages = 
{
    "Hmm?",
}

function Dialog.OpenTalkDialog(user)
    --QuickDialogMessage(this,user,"Will we be ready?")
end

function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,
    	"If you are asking, you are not who I am looking for. Now shh! Go away."
    	)
    --QuickDialogMessage(this,user,"REPLACED -> I am a quest guy!")
end

function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)] or AI.GreetingMessages[1]

    response = {}

    response[1] = {}
    response[1].text = "Who are you?"
    response[1].handle = "Who" 
    
    response[2] = {}
    response[2].text = "Goodbye."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)

