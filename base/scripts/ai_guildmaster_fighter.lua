require 'base_ai_npc'

AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = true
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

function GetPrestigeClass()
    return this:GetObjVar("PrestigeClass") or ""
end
local prestigeClass = GetPrestigeClass()
if ( prestigeClass ) then
        AI.QuestStepsInvolvedIn = {
            {"FighterProfessionTierTwo", 7},
            {"FighterProfessionTierThree", 13},
            {"FighterProfessionTierFour", 10},
            {"BardProfessionTierThree", 2},
        }
end

function Dialog.OpenGreetingDialog(user)
    local text = nil    
    
    text = "Greetings traveler. Are you here to train in the ways of the Mage..?"

    local response = {
        {
            text = "Yes, I'd like to learn some basics.",
            handle = "SkillTrain"
        },
        {
            text = "Goodbye.",
            handle = ""
        }
    }
    
    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenSkillTrainDialog(user)
        SkillTrainer.ShowTrainContextMenu(user)
end