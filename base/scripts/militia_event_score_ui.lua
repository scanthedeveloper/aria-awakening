local _controller

if ( initializer and initializer.Controller ) then
    _controller = initializer.Controller
    this:SetObjVar("MilitiaEventController", _controller)
end

if ( _controller == nil ) then
    _controller = this:GetObjVar("MilitiaEventController")
end

local delay = TimeSpan.FromSeconds(1)
function CheckController()
    if ( not _controller or not _controller:IsValid() ) then return end
    local active = _controller:GetObjVar("Active")
    local radius = _controller:GetObjVar("Radius") or _controller:GetObjVar("spawnRadius") or 10
    local distance = this:DistanceFrom(_controller)
    if ( _controller:IsValid()
    and active and distance
    and distance < radius
    and active == 1 ) then
        MilitiaEventScore(_controller, this)
    else
        this:CloseDynamicWindow("MilitiaEventScore")
        this:DelModule(GetCurrentModule())
        return
    end
    this:ScheduleTimerDelay(delay, "CheckController")
end

function MilitiaEventScore(controller, this)
    local radius = controller:GetObjVar("spawnRadius") or controller:GetObjVar("Radius") or 150
    local type = controller:GetObjVar("Type") or 0
    local endTime = controller:GetObjVar("EndTime")
    local score = controller:GetObjVar("Score") or {}
    UpdateMilitiaScore(this,type,endTime,score)
end

RegisterEventHandler(EventType.Timer, "CheckController", CheckController)
this:ScheduleTimerDelay(delay, "CheckController")