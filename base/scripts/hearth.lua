local bindLocation = this:GetObjVar("BindLocation")

this:ScheduleTimerDelay(TimeSpan.FromSeconds(0.5),"ReloadFireplaceSound")

RegisterSingleEventHandler(EventType.Timer, "ReloadFireplaceSound", function()
	this:StopObjectSound("event:/objects/furniture/fireplace", false, 0.5)
	this:PlayObjectSound("event:/objects/furniture/fireplace", false)
end)


AddView("NearbyPlayer", SearchPlayerInRange((ServerSettings.Vitality.Hearth.MaxRange - 3)))

this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"HearthPulse")

RegisterEventHandler(EventType.Timer, "HearthPulse", function()
	HearthPulse()
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"HearthPulse")
end)

RegisterEventHandler(EventType.Message, "UseObject", 
	function(user,usedType)
		if(usedType ~= "Use" and usedType ~= "Bind To Location") then return end

		if not(ValidateUse(user) ) then
			return
		end

		user:SendMessage("BindToLocation",bindLocation or user:GetLoc())
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "hearth", function()
	AddUseCase(this,"Bind To Location")
	SetTooltipEntry(this, "hearth", "Warms your soul and allows you to bind to this location.")
end)



function ValidateUse(user)
	if (IsDead(user)) then 
		return false
	end
	if (this:DistanceFrom(user) > OBJECT_INTERACTION_RANGE) then
		return false
	end

	return true
end

function CheckBardsPlaying()
	local players = GetViewObjects("NearbyPlayer")
	local bardPlaying = false

	for i,player in pairs(players) do 
		if( player:HasObjVar("BandID") ) then
			bardPlaying = true
		end
	end

	this:SetObjVar("BardPlaying", bardPlaying)
	return bardPlaying

end

function HearthPulse()
	local players = GetViewObjects("NearbyPlayer")
	for i,player in pairs(players) do 

		if not( HasMobileEffect(player, "Hearth") ) then
			player:SendMessage("StartMobileEffect", "Hearth", this)
		end
		
		if( 
			ServerSettings.Vitality.Hearth.EnableHearthVitalityRegen 
			and not( HasMobileEffect(player,"VitalityHearth") ) 
			and not CheckBardsPlaying()
			and GetCurVitality(player) < GetMaxVitality(player)
		) then
			player:SendMessage("StartMobileEffect", "VitalityHearth", this, {})
		end
	end
end



