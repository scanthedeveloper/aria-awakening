RegisterEventHandler(EventType.Message, "EndMission", 
	function(missionId, completed)
		local missionKey = GetMissionKeyFromId(this, missionId)
		if (missionKey	== nil) then return end

		if (completed == true) then
			--Mission completed
			if not(IsDead(this)) then
				this:SystemMessage("Completed Mission", "info")
				this:PlayEffect("HealEffect")

				--Reward
				local amount = math.random(GetMissionTable(missionKey).RewardMin, GetMissionTable(missionKey).RewardMax) or 100
				this:SystemMessage(amount.." coins have been added to your bank for completing the mission.","info")
				CreateCoinStackInBank(this, "coin_purse", amount)

				local playerStats = this:GetObjVar("LifetimePlayerStats") or {}

				if GetMissionTable(missionKey).MissionType == "Harvest" then
					playerStats.HarvestMissionsCompleted = (playerStats.HarvestMissionsCompleted or 0) + 1
				end
				local numberMissionCompleted = playerStats.NumberMissionCompleted or 0
				playerStats.NumberMissionCompleted = numberMissionCompleted + 1
				this:SetObjVar("LifetimePlayerStats",playerStats)

				CheckAchievementStatus(this, "Activity", "BountiesNumber", numberMissionCompleted + 1)
				this:SendMessage("OnDailyRewardTaskComplete","mission")
			else
				this:SystemMessage("Mission Ended", "info")
			end
		else
			this:SystemMessage("Failed Mission \"[ff6347]"..GetMissionTable(missionKey).Title.."[-]\"", "info")
		end
		RemoveMission(this, missionId)
		UpdateMissionUi()
	end)

RegisterEventHandler(EventType.DynamicWindowResponse, "TaskUI",
	function(user, buttonId, fieldData)
		if (buttonId:match("MissionButton")) then
			local missionID = StringSplit(buttonId,"|")[2]
			if (user:IsValid()) then
				local missions = this:GetObjVar("Missions")
				if (missions) then
					for i, j in pairs(missions) do
						if (j.ID == missionID) then
							ToggleMissionMapMarker(j)
							UpdateMissionUi()
						end
					end
				end
			end

		elseif(buttonId:match("HideMissionUI")) then
			this:SetObjVar("HideMissionUI", true)
			UpdateMissionUi()
			return
		elseif(buttonId:match("ShowMissionUI")) then
			this:DelObjVar("HideMissionUI")
			UpdateMissionUi()
			return
		elseif(buttonId:match("QuestButton")) then

			local buttonQuestName = StringSplit(buttonId,"|")[2]
			local mapMarkers = this:GetObjVar("MapMarkers")
			local hasActiveQuestMarkers = Quests.HasActiveQuestMarkers(this,buttonQuestName)
			if (hasActiveQuestMarkers) then
				Quests.RemoveQuestMarkers(this,buttonQuestName)
			else
				Quests.UpdateQuestMarkers(this, buttonQuestName)
			end
			UpdateMissionUi()

		elseif (buttonId:match("RemoveMission")) then
			local missionID = StringSplit(buttonId,"|")[2]
			if (user:IsValid()) then
				if ( string.match(missionID, "#queststep#" ) ) then
					local quest, step = missionID:match("(.+)#queststep#(.+)")
					step = tonumber(step)
					local instructions = AllQuests[quest][step].Instructions
					ClientDialog.Show
					{
						TargetUser = user,
						DialogId = "RemoveMissionDialog"..missionID,
						TitleStr = "Remove Task",
						DescStr = "Are you sure you want to cancel this task?\n\nInstructions:\n"..instructions,
						Button1Str = "Yes",
						Button2Str = "No",
						ResponseObj= this,
						ResponseFunc= 
						function(dialogUser, dialogButtonId)
							local dialogButtonId = tonumber(dialogButtonId)
							if (dialogUser == nil) then return end

							if (dialogButtonId == 0) then
								Quests.Fail(user, quest, step)
								this:SendMessage("UpdateProfessionGuideWindow")
								return
							end
						end,
					}
				else
					local missions = this:GetObjVar("Missions")
					for i, j in pairs(missions) do
						if (j.ID == missionID) then

							ClientDialog.Show
							{
								TargetUser = user,
								DialogId = "RemoveMissionDialog"..missionID,
								TitleStr = "Remove Mission",
								DescStr = "Are you sure you want to cancel this mission?",
								Button1Str = "Yes",
								Button2Str = "No",
								ResponseObj= this,
								ResponseFunc= 
								function(dialogUser, dialogButtonId)
									local dialogButtonId = tonumber(dialogButtonId)
									if (dialogUser == nil) then return end

									if (dialogButtonId == 0) then
										RemoveMission(this, missionID)
										UpdateMissionUi()
										return
									end
								end,
							}
						end
					end
				end
			end
		end
	end)

function ToggleMissionMapMarker(missionData)
	if (customMapFunctions[ServerSettings.WorldName]() == nil) then
		RemoveDynamicMapMarker(this,"Mission|"..missionData.ID)
		return 
	end

	local missionMapMarker = GetMissionMapMarker(this, missionData.ID)
	if (missionMapMarker == nil) then
		local mapMarker = {Icon="marker_mission", Location=missionData.SpawnLoc, Tooltip=GetMissionTable(missionData.Key).Title.."\n"..(GetRegionalName(missionData.SpawnLoc) or ServerSettings.SubregionName), RemoveDistance=5}
		AddDynamicMapMarker(this,mapMarker,"Mission|"..missionData.ID)
		this:SystemMessage(GetMissionTable(missionData.Key).Title.." marker enabled.")
	else
		this:SystemMessage(GetMissionTable(missionData.Key).Title.." marker disabled.")
		RemoveDynamicMapMarker(this,"Mission|"..missionData.ID)
	end
end

--Checks if player is close enough to mission controller to activate it 
RegisterEventHandler(EventType.Timer, "MissionCheck", function()
	CheckForNearbyMissions(this)
	UpdateMissionUi()
	end)
this:ScheduleTimerDelay(TimeSpan.FromSeconds(MISSION_CHECK_TIME), "MissionCheck")

RegisterEventHandler(EventType.Message, "UpdateMissionUI",
	function()
		UpdateMissionUi()
	end)

RegisterEventHandler(EventType.Message, "UpdateAwakeningUI",
	function()
		UpdateAwakeningUI()
	end)

RegisterEventHandler(EventType.CreatedObject, "createdController", 
	function(success, objRef, missionData)
		missionData.ControllerObj = objRef

		local missions = this:GetObjVar("Missions")
		for i, j in pairs(missions) do
			if (j.ID == missionData.ID) then
				j.ControllerObj = objRef
			end
		end
		this:SetObjVar("Missions", missions)
		
		UpdateMissionUi()

		objRef:SetObjVar("MissionData", missionData)
		objRef:SetObjVar("MissionOwner", this)
	end)


--Each mission shows a title, a button to display a waypoint on the minimap, and progress(?)
function UpdateMissionUi()
	--this:CloseDynamicWindow("TaskUI")
	local missions = this:GetObjVar("Missions") or {}
	local quests = this:GetObjVar("Quests") or {}
	local skillTrackerOffset = #(this:GetObjVar("TrackedSkills") or {}) * SKILL_TRACKER_SIZE + 20
	local missionOffset = skillTrackerOffset
	local missionWindow = DynamicWindow("TaskUI","",200,180,-265,425,"TransparentDraggable","TopRight")

	if( this:HasObjVar("HideMissionUI") ) then
		missionWindow:AddImage(0,10,"TextFieldChatUnsliced",220,45,"Sliced")
		missionWindow:AddImage(190,20,"PlusButton_Default",24,24)
		missionWindow:AddButton(190,20,"ShowMissionUI", "", 24, 24, "Display missions and quests.","",false, "Invisible")
	else
		missionWindow:AddImage(190,20,"MinusButton_Default",24,24)
		missionWindow:AddButton(190,20,"HideMissionUI", "", 24, 24, "Hide missions and quests.","",false, "Invisible")
		missionWindow:AddImage(0,10,"TextFieldChatUnsliced",220,350,"Sliced")

		local scrollWindow = ScrollWindow(10,50,190,300,60)
		
		if (#missions >= 1) then

			--If mission is past deadline, remove it from list
			for i = #missions, 1, -1 do
				if (DateTime.UtcNow > missions[i].EndTime) then
					RemoveMission(this, missions[i].ID)
					UpdateMissionUi()
					return
				end
			end

			--Add mission ui elements
			for i, j in pairs(missions) do
				local missionTable = (j.Key) and GetMissionTable(j.Key)
				if (missionTable) then
					local missionTitle = missionTable.Title

					local buttonState = "disabled"
					if (GetMissionMapMarker(this, j.ID) ~= nil) then
						buttonState = "pressed"
					end

					if (missionTitle ~= nil) then
						local scrollElement = ScrollElement()
						local labelText = "[ff6347]"..missionTitle.."\n[-]"..(j.RegionalName or j.Subregion)
						scrollElement:AddImage(0,0,"TextFieldChatUnsliced",180,60,"Sliced")

						scrollElement:AddLabel(5,5,labelText,0,0,18,"left",false,true)
						scrollElement:AddButton(150,0,"MissionButton|"..j.ID, "", 24, 24, "Toggle Markers", "", false, "Selection2", buttonState)
						scrollElement:AddButton(5,0,"RemoveMission|"..j.ID, "", 140, 0, missionTable.Description,"",false, "Invisible")

						scrollElement:AddLabel(5,40,GetTimerLabelString(DateTime.UtcNow - j.EndTime),0,0,18,"left",false,true)
						scrollWindow:Add(scrollElement)
						missionOffset = ((i) * 60) + skillTrackerOffset
					end
				end
			end
		end

		if ( quests and next(quests) ) then
			--Add/remove quest ui elements
			local counter = 1
			for questName, quest in pairs(quests) do
				for stepIndex, step in pairs(quest) do
					
					-- Ensure the questName and stepIndex are valid!
					if( AllQuests[questName] and AllQuests[questName][stepIndex] and not( AllQuests[questName][stepIndex].HideTracking ) ) then
						local active = step["Active"]
						local Id = questName.."#queststep#"..stepIndex
						local title = AllQuests[questName][stepIndex].Name
						local instructions = AllQuests[questName][stepIndex].Instructions
						--local timeLimit = AllQuests[questName][stepIndex].TimeLimit
						--local endTime = nil
						local silent = AllQuests[questName][stepIndex].Silent
						--[[if ( timeLimit ) then
							local startTime = step[1]
							if ( startTime ) then
								endTime = startTime:Add(timeLimit)
							end
						end]]

						if ( active and active == 1 and (not silent or silent ~= true) ) then
							local scrollElement = ScrollElement()
							local buttonState = "disabled"
							if (Quests.HasActiveQuestMarkers(this,questName) or AllQuests[questName][stepIndex].MarkerLocs==nil) then
								buttonState = "pressed"
							else
								buttonState = "disabled"
							end

							if (title ~= nil) then
								local labelText = "[ff6347]"..title.."\n[-]"..(instructions)
								scrollElement:AddImage(0,0,"TextFieldChatUnsliced",180,60,"Sliced")

								scrollElement:AddLabel(5,5,labelText,170,70,18,"left",false,true)

								if (AllQuests[questName][stepIndex].MarkerLocs) then
									scrollElement:AddButton(150,2,"QuestButton|"..questName, "", 24, 24, "Toggle Markers", "", false, "Selection2", buttonState)
								end
								
								scrollElement:AddButton(5,0,"RemoveMission|"..Id, "", 140, 50, instructions,"",false, "Invisible")

								--[[if ( endTime ) then
									missionWindow:AddLabel(5,35+missionOffset,GetTimerLabelString(DateTime.UtcNow - endTime),0,0,18,"left",false,true)
								end]]
								--missionOffset = ((#missions + counter) * 60) + skillTrackerOffset	
								--counter = counter + 1
								scrollWindow:Add(scrollElement)
							end
						else
							--RemoveMission(this, Id)
							--2/12/2019 AB: 'Missions' ObjVar will never have Quest 'Id' and MapMarkers are refreshed in Quests.TryEnd so this call is not needed for inactive quests (Large 'Quests' table would tank performance)
						end
					else

						-- The quest is valid, but the step no longer exists. Remove it!
						if( AllQuests[questName] and not AllQuests[questName][stepIndex] ) then
							Quests.RemoveQuestStep( this, questName, stepIndex );
						end

					end
				end
			end
		end

		--local showAwakening = Awakening.AddAwakeningUI(this,missionWindow,missionOffset)

		if ( #quests < 1 and #missions < 1 and not(showAwakening) ) then
			this:CloseDynamicWindow("TaskUI")
		end

		missionWindow:AddScrollWindow(scrollWindow)

	end

	missionWindow:AddLabel(10,22,"[FFFFFF]Missions & Quests[-]",0,0,24,"", false, false, "SpectralSC-SemiBold")
	this:OpenDynamicWindow(missionWindow)
end

function UpdateAwakeningUI()
	if( Awakening.IsInAwakening(this) ) then
		Awakening.AddAwakeningUI(this,missionWindow,20)
	else
		CloseDynamicEventHUD(this)
	end
end

--[[
function UpdateMissionData(missionData)
	local missions = this:GetObjVar("Missions") or {}
	for i = #missions, 1, -1 do
		if (missions[i].ID == missionData.ID) then
			missions[i] = missionData
		end
	end

	this:SetObjVar("Missions", missions)
	DebugMessage(DumpTable(missions))
end
]]--