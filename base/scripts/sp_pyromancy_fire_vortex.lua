--require 'base_magic_sys'
local FIREVORTEX_DAMAGE_RANGE = 6
local mTargetLoc = nil
local mRainActive = false
local mPulse = 0
--[[ SCAN WROTE FOR STUNNING
local mDisable = 0
]]
local function ValidatePyroFireVortex(targetLoc)
	--DebugMessage("Debuggery Deh Yah")
	if( not(IsPassable(targetLoc)) ) then
		this:SystemMessage("[$2612]","info")
		return false
	end

	if not(this:HasLineOfSightToLoc(targetLoc,ServerSettings.Combat.LOSEyeLevel)) then
		this:SystemMessage("[$2613]","info")
		return false
	end
	return true
end

RegisterEventHandler(EventType.Message,"PyroFireVortexSpellTargetResult",
	function (targetLoc)
		-- validate FlamePillar
--DebugMessage("Debuggery Here2")
		if not(ValidatePyroFireVortex(targetLoc)) then
			EndEffect()
			return
		end
		--DebugMessage("TARGETED!!")
	--DebugMessage("Debuggery Here")
		if(mRainActive == false) then
			mTargetLoc = targetLoc
		end
		mPulse = 0
		--[[ SCAN WROTE FOR STUNNING
		mDisable = 0
		]]

		mRainActive = true
		this:FireTimer("PyroFireVortexPulse")
		--this:PlayObjectSound("event:/magic/fire/magic_fire_pillar_of_fire")
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(8),"PyroFireVortexRemove",mTargetLoc)
	end)



RegisterEventHandler(EventType.Timer, "PyroFireVortexPulse",
	function()
		if(mRainActive == false) then
			EndEffect()
			return
		end
		--DebugMessage("PULSE!!")
		if(math.fmod(mPulse,3)== 0 ) then
				PlayEffectAtLoc("ShoutEffect",mTargetLoc, 5)
				PlayEffectAtLoc("FirePillarEffect",mTargetLoc, 3)
				PlayEffectAtLoc("FireballExplosionEffect",mTargetLoc, 3)
				PlayEffectAtLoc("GroundCircleRed2",mTargetLoc, 5)
				PlayEffectAtLoc("WarriorAoETaunt",mTargetLoc, 2)
				PlayEffectAtLoc("WarriorDemoShout",mTargetLoc, 2)
				CreateTempObj("spell_aoe",mTargetLoc,"aoe_created")
			end
	local mobiles = FindObjects(SearchMulti({
					SearchRange(mTargetLoc,FIREVORTEX_DAMAGE_RANGE),
					SearchMobile()}), GameObj(0))
	for i,v in pairs(mobiles) do
		--v:NpcSpeech("Burning Burning burning")
		if(not IsDead(v)) then
			this:SendMessage("RequestMagicalAttack", "PyroFireVortex", v, this, true)
			--this:SendMessage("RequestMagicalAttack", "ImmolationIgnite",v, this, true)
			--[[ SCAN WROTE FOR STUNNING
			if(mDisable < 1 ) then
				for i=1,#mobiles do
					SetMobileModExpire(mobiles[i], "Freeze", "PyroFireVortex", true, TimeSpan.FromSeconds(1.5))
					mDisable = mDisable + 1
				end
			elseif false then
				end
			]]

			--if(not v:HasModule("sp_burn_effect")) then
				--v:SetObjVar("sp_burn_effectSource" , this)
				--v:AddModule("sp_burn_effect")
			--end
		end
	end
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(1), "PyroFireVortexPulse")
end)

RegisterEventHandler(EventType.CreatedObject,"aoe_created",function (success,objRef)
	if (success) then
		objRef:SetObjVar("DecayTime",3)
	end
end)

RegisterEventHandler(EventType.Timer,"PyroFireVortexRemove",
	function()
		EndEffect()
	end)


function EndEffect()
	--DebugMessage("Ending!!")
	if(this:HasTimer("PyroFireVortexRemove")) then this:RemoveTimer("PyroFireVortexRemove") end
	if(this:HasTimer("PyroFireVortexPulse")) then this:RemoveTimer("PyroFireVortexPulse") end
	this:StopObjectSound("event:/magic/fire/magic_fire_pillar_of_fire",false,0.0)
	this:DelModule("sp_pyromancy_fire_vortex")
end