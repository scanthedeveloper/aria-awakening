MAX_SPIDERS = 2
MIN_SPIDERS = 1
MIN_DELAY = 4
MAX_DELAY = 8

local target = nil
spiderBoss = FindObject(SearchTemplate("world_boss_spider",50))

-- Conrols what should happen when the nest is destroyed.
-- @param: user - the user that destroyed the nest if nil the nest was allowed to mature.
function Destroy(user)
    if( spiderBoss == nil ) then this:Destroy() return end 
    if (this:GetSharedObjectProperty("IsDestroyed") == true) then return end 
    this:SetSharedObjectProperty("IsDestroyed",true)
    this:SetName("Destroyed Spider Nest")

    -- The nest was allowed to mature.
    if( user == nil ) then
        CreateObj("world_boss_spider_minion_vile",this:GetLoc():Project(math.random(0,360), math.random(2,4)),"InitSpawn")
    -- the nest was destroyed by a user
    else
        local i = 1
        local spawnAmount = math.random(MIN_SPIDERS,MAX_SPIDERS)
        while i <= spawnAmount do 
            target = user
            CreateObj("world_boss_spider_minion",this:GetLoc():Project(math.random(0,360), math.random(2,4)),"InitSpawn")
            i = i + 1
        end
    end

    

    CallFunctionDelayed(TimeSpan.FromSeconds( math.random( 45, 60 ) ),function() this:Destroy() end)

end

RegisterEventHandler(EventType.Message, "UseObject", 
    function (user,usedType)
        --DebugMessage(1)
        if(usedType ~= "Use" and usedType ~= "Break") then return end
        --DebugMessage(2)
        user:PlayAnimation("attack")
        FaceObject(user,this)
        Destroy(user)
    end
)

RegisterEventHandler(EventType.Message,"DamageInflicted",
    function(attacker,damageAmt)
        if(damageAmt > 0 and this:GetSharedObjectProperty("IsDestroyed") == false) then
            Destroy(attacker)
        end
    end
)

--for mobs with custom ages
RegisterEventHandler(EventType.CreatedObject,"InitSpawn",
    function(success,objRef)

        objRef:SetObjVar("MobileLeader", spiderBoss)

        if( target ) then
            -- Set target to nest destroyer! 
            objRef:SendMessage("AttackEnemy",target)
        else
            -- Get bosses target!
            --DebugMessage("Attacking: " .. tostring( spiderBoss:GetObjVar("CurrentTarget") ))
            objRef:SendMessage("AttackEnemy",spiderBoss:GetObjVar("CurrentTarget"))
        end

    end
)

RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), 
    function()
        AddUseCase(this,"Break",true)
        CallFunctionDelayed(TimeSpan.FromSeconds( math.random( MIN_DELAY, MAX_DELAY ) ),function() Destroy(nil) end)
    end
)