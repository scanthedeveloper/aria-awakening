-- Since this is the object that does the destroy all, it does not destroy itself so we need to get rid of the extra one the spawns
local otherControllers = FindObjectsWithTag("CatacombsController")
for i,j in pairs(otherControllers) do
	if (j ~= this) then
		this:Destroy()
	end
end

local purgeComplete = false

function OnLoad()
	--DebugMessage("[catacombs_controller:OnLoad]")
	this:SetObjectTag("CatacombsController")
end

RegisterEventHandler(EventType.LoadedFromBackup,"",
	function ()
		OnLoad()
	end)

RegisterEventHandler(EventType.Message,"Reset",
	function()
		local isActive = this:GetObjVar("IsActive")
		--DebugMessage("[catacombs_controller:Reset]  "..tostring(isActive))
		-- This allows for multiple catacombs_portal_controllers
		if not(this:GetObjVar("IsActive")) then
			DebugMessage("[catacombs_controller] ---- Reset and Active")

			this:SetObjVar("IsActive",true)

			DestroyAllObjects(false,"WorldReset")
		end
	end)

function PurgePlayers()
	DebugMessage("[catacombs_controller:PurgePlayers] Execute")

	local sendto = GetRegionAddressesForName("SouthernHills")
    for i = 1, #sendto do
        SendRemoteMessage(sendto[i], MapLocations.NewCelador["Southern Hills: Catacombs Portal Controller"], 5, "close_catacombs_portal")
    end

    purgeComplete = true
    local loggedOnUsers = FindPlayersInRegion()
    for index,object in pairs(loggedOnUsers) do
    	object:PlayEffectWithArgs("ScreenShakeEffect", 2.0,"Magnitude=5")
        TeleportUser(object,object,MapLocations.NewCelador["Southern Hills: Catacombs Portal"],sendto[1], 0, true, nil, true)
    end

    --DebugMessage("[catacombs_controller:PurgePlayers] Active false")
    this:SetObjVar("IsActive",false)
end
RegisterEventHandler(EventType.Timer,"PurgePlayers",PurgePlayers)

RegisterEventHandler(EventType.Message,"PurgePlayers", function() 
		DebugMessage("[catacombs_controller:PurgePlayers] Purge Players command acknowledged")
		purgeComplete = false
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(15),"shake")
		this:ScheduleTimerDelay(TimeSpan.FromMinutes(3),"PurgePlayers")
    end)

RegisterEventHandler(EventType.DestroyAllComplete,"WorldReset", function ()
	--DebugMessage("[catacombs_controller:PurgePlayers] World Reset Complete")

	LoadSeeds()
	ResetPermanentObjectStates()
end)


RegisterEventHandler(EventType.Timer,"shake",
	function ( ... )		
		--DebugMessage("[catacombs_controller:shake]")

		local loggedOnUsers = FindPlayersInRegion()
        for index,object in pairs(loggedOnUsers) do
        	object:SystemMessage("Death's curse descends as he is vanquished from this realm for a time.. Hurry and exit this cursed place!","event")
        	object:PlayEffectWithArgs("ScreenShakeEffect", math.random(2,5),"Magnitude=10")
        end

		if(purgeComplete == false) then
			--DebugMessage("[catacombs_controller:shake] Schedule Timer")
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(math.random(8,25)),"shake")
		end
	end)

RegisterEventHandler(EventType.Message,"portal_state", function( state )
	this:SetObjVar("PortalState", state)
end)