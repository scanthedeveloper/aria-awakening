require 'globals.lua.extensions'
local radius = this:GetObjVar("Radius") or 5
local pointsLimit = ServerSettings.Militia.Events[2].PointsLimit
local minutesLimit = ServerSettings.Militia.Events[2].MinutesLimit
local registerPulseInterval = TimeSpan.FromSeconds(30)
local pulseInterval = TimeSpan.FromSeconds(5)
local score = this:GetObjVar("Score") or {0,0,0}
RegisterEventHandler(EventType.LoadedFromBackup,"",function()OnLoad()end)
RegisterEventHandler(EventType.Message,"StartEvent",function()ResetAndStartEvent()end)
RegisterEventHandler(EventType.Message,"StopEvent",function()StopEvent()end)
RegisterEventHandler(EventType.Timer, "EventPulse",function()EventPulse()end)
RegisterEventHandler(EventType.Timer,"RegisterControllerToGlobalTable",function()MilitiaEvent.RegisterControllerToGlobalTable(this, registerPulseInterval)end)
this:ScheduleTimerDelay(registerPulseInterval,"RegisterControllerToGlobalTable")
this:ScheduleTimerDelay(pulseInterval,"EventPulse")

function ResetAndStartEvent()
    DebugMessage("Militia koth event activated: "..(this:GetObjVar("Label") or "Unknown"))
    this:SetObjVar("Active", 1)
    this:SetObjVar("Score", {0,0,0})
    this:SetObjVar("EndTime", DateTime.UtcNow:Add(minutesLimit))
    local message = (this:GetObjVar("Label") or "Unknown").." is now an active Militia "..ServerSettings.Militia.Events[2].Name.." event location."
    Militia.EventMessagePlayers(message)
    MilitiaEvent.UpdateGlobalTable(this)
end

function StopEvent()
    this:SetObjVar("Active", 0)
    local cooldown = this:GetObjVar("Cooldown") or ServerSettings.Militia.EventDefaultCooldown or DateTime.UtcNow
    local unlockTime = DateTime.UtcNow:Add(cooldown)
    this:SetObjVar("UnlockTime", unlockTime)
    this:SetObjVar("EndTime", DateTime.UtcNow)
    local winner = this:GetObjVar("Winner")
    local militiaData = Militia.GetDataById(winner)
    if ( militiaData and militiaData.Name ) then
        local label = this:GetObjVar("Label") or "Unknown"
        local message = militiaData.Name.." has won "..(this:GetObjVar("Label") or "Unknown").."!"
        Militia.EventMessagePlayers(message)
    end
    MilitiaEvent.UpdateGlobalTable(this)
    Militia.BuffAllPlayers()
    local saltRecipients = SearchForSaltRecipients(winner)
    if ( saltRecipients and next(saltRecipients) ) then
        for i = 1, #saltRecipients do
            MilitiaEvent.FavorReward(saltRecipients[i])
        end
    end
end

function EventPulse()
    local endTime = this:GetObjVar("EndTime")
    local active = this:GetObjVar("Active")
    local cooldown = nil
    if ( endTime and active and active == 0 and DateTime.Compare(DateTime.UtcNow, endTime:Add(ServerSettings.Militia.EventDefaultCooldown)) < 0 ) then
        cooldown = endTime:Add(ServerSettings.Militia.EventDefaultCooldown):Subtract(DateTime.UtcNow)
        SetTooltipEntry(this,"vulnerability","Vulnerable in "..TimeSpanToWords(cooldown)..".",98)
    else
        SetTooltipEntry(this,"vulnerability","Vulnerable now!",98)
    end
    
    local occupants = SearchForPointOccupants()
    local teamsContesting = 0
    local kingTeam = 0
    for i = 1, 3 do
        if ( #occupants[i] > 0 ) then
            kingTeam = i
            teamsContesting = teamsContesting + 1 
        end
    end
    
    if ( active ~= nil and active == 1 ) then
        this:PlayEffect("MagicRingEffect")
        --if only one Militia is contesting
        if ( teamsContesting == 1 ) then
            local winner = this:GetObjVar("Winner") or 0
            local score = this:GetObjVar("Score") or {0,0,0}
            
            --increment score based on number of players
            score[1] = math.max(0, score[1] - 5)
            score[2] = math.max(0, score[2] - 5)
            score[3] = math.max(0, score[3] - 5)
            score[kingTeam] = score[kingTeam] + 10
            this:SetObjVar("Score", score)
            this:PlayEffect("HolyEffect")
            --check for change in winning Militia, do changes
            MilitiaEvent.FlagToWinner(this, winner, score)
        end
        --check win condition and end event
        local endTime = this:GetObjVar("EndTime")
        if ( endTime ~= nil and DateTime.UtcNow > endTime 
        or score ~= nil and score[kingTeam] ~= nil and score[kingTeam] >= pointsLimit ) then
            StopEvent()
        end
    else
        if ( teamsContesting == 1 and not cooldown ) then
            this:SendMessage("StartEvent")
        end
    end
    Militia.FlagForConflict(this, 100)
    this:ScheduleTimerDelay(pulseInterval,"EventPulse")
end

function SearchForPointOccupants()
    local allOccupants = {}
    for i = 1, 3 do
        local search = FindObjects(SearchMulti(
        {
            SearchPlayerInRange(radius, false),
            SearchObjVar("Militia", i),
        }))
        local occupants = {}
        for ii = 1, #search do
            if ( not(IsDead(search[ii])) ) then table.insert(occupants, search[ii]) end
        end
        table.insert(allOccupants, occupants)
    end
    return allOccupants
end

function SearchForSaltRecipients(militiaId)
    local search = FindObjects(SearchMulti(
    {
        SearchPlayerInRange(100, true),
        SearchObjVar("Militia", militiaId),
    }))
    local kingTeamOccupants = {}
    for i = 1, #search do
        if ( Militia.GetRankNumber(search[i]) > 1 and not(IsDead(search[i])) ) then table.insert(kingTeamOccupants, search[i]) end
    end
    return kingTeamOccupants
end

function OnLoad()
    this:ScheduleTimerDelay(pulseInterval, "EventPulse")
end