

-- required for fast identification within require modules
_IS_BRAIN = true

require 'combat'
require 'brain_inc' -- must come after combat, before mobile
require 'base_mobile_advanced'

local fsm

function Start()
    fsm = FSM(this, {
        States.Death,
        States.SummonCompanion,
        States.Leash,
        States.Aggro,
        States.AttackAggroList,
        States.Attack,
        States.FightDamage,
        States.Idle,
    })

    if( this:HasObjVar("MyPath") ) then
        fsm.ReplaceState(States.Idle, States.Patrol)
    elseif( this:GetObjVar("AI-CanWander") == true ) then
        fsm.ReplaceState(States.Idle, States.Wander)
    end

    fsm.Start()
end

local _OnMobileLoad = OnMobileLoad
function OnMobileLoad()
    this:SetObjVar("GroupAI", true)
    _OnMobileLoad()
    Start()
end

RegisterEventHandler(EventType.Message,"GroupAIMessage",function ( _sender, _message, _args )
    
    --DebugMessage("ListenForMessage : " .. _message)
    for key, message in pairs( GroupAI.MessageTags ) do 
        if( _message == message ) then

            -- If the tank wants us to target this mobile, we need to SetTarget
            -- and give us an aggro boost on the target to prevent us from swapping.
            if( key == "UpdateTarget" ) then
                this:SendMessage("SetCurrentTarget", _args.Target)
                this:SendMessage("AddAggro", _args.Target, 400)
            end

        end
    end
end)