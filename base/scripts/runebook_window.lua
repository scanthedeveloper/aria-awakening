BOOK_WIDTH = 789
BOOK_HEIGHT = 455

runebookObj = nil

mRunebookPageIndex = 1

function CleanUp()
    this:CloseDynamicWindow("Runebook")
    this:DelModule("runebook_window")
end

function UpdateWindow()
	local dynamicWindow = DynamicWindow(
		"Runebook", --(string) Window ID used to uniquely identify the window. It is returned in the DynamicWindowResponse event.
		"", --(string) Title of the window for the client UI
		BOOK_WIDTH, --(number) Width of the window
		BOOK_HEIGHT, --(number) Height of the window
		-BOOK_WIDTH/2, --startX, --(number) Starting X position of the window (chosen by client if not specified)
		-BOOK_HEIGHT/2, --startY, --(number) Starting Y position of the window (chosen by client if not specified)
		"TransparentDraggable",--windowType, --(string) Window type (optional)
		"Center" --windowAnchor --(string) Window anchor (default "TopLeft")
	)

	dynamicWindow:AddImage(
		0, --(number) x position in pixels on the window
		0, --(number) y position in pixels on the window
		"Skillbook", --(string) sprite name
		BOOK_WIDTH, --(number) width of the image
		BOOK_HEIGHT --(number) height of the image
		--spriteType, --(string) sprite type Simple, Sliced or Object (defaults to Simple)
		--spriteHue, --(string) sprite hue (defaults to white)
		--opacity --(number) (default 1.0)		
	)

	--[[dynamicWindow:AddImage(
		400, --(number) x position in pixels on the window
		384, --(number) y position in pixels on the window
		"BookTabLower", --(string) sprite name
		0, --(number) width of the image
		0 --(number) height of the image
		--spriteType, --(string) sprite type Simple, Sliced or Object (defaults to Simple)
		--spriteHue, --(string) sprite hue (defaults to white)
		--opacity --(number) (default 1.0)		
	)]]

	dynamicWindow:AddButton(
			700, --(number) x position in pixels on the window
			22, --(number) y position in pixels on the window
			"", --(string) return id used in the DynamicWindowResponse event
			"", --(string) text in the button (defaults to empty string)
			0, --(number) width of the button (defaults to width of text)
			0,--(number) height of the button (default decided by type of button)
			"", --(string) mouseover tooltip for the button (default blank)
			"", --(string) server command to send on button click (default to none)
			true, --(boolean) should the window close when this button is clicked? (default true)
			"CloseSquare" --(string) button type (default "Default")
			--buttonState --(string) button state (optional, valid options are default,pressed,disabled)
			--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
		)

	local hasPrevPage = mRunebookPageIndex > 1
	if (hasPrevPage) then
		local pageStr = tostring(mRunebookPageIndex - 1)
		dynamicWindow:AddButton(
			90, --(number) x position in pixels on the window
			38, --(number) y position in pixels on the window
			"Page|"..pageStr, --(string) return id used in the DynamicWindowResponse event
			"", --(string) text in the button (defaults to empty string)
			154, --(number) width of the button (defaults to width of text)
			93,--(number) height of the button (default decided by type of button)
			"", --(string) mouseover tooltip for the button (default blank)
			"", --(string) server command to send on button click (default to none)
			false, --(boolean) should the window close when this button is clicked? (default true)
			"BookPageDown" --(string) button type (default "Default")
			--(string) button state (optional, valid options are default,pressed,disabled)
			--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
		)
	end

	local runeInfo = runebookObj:GetObjVar("RuneInfo") or {}
	local hasNextPage = mRunebookPageIndex < (1 + math.ceil(#runeInfo/2))
	if (hasNextPage) then
		local pageStr = tostring(mRunebookPageIndex + 1)
		dynamicWindow:AddButton(
			550, --(number) x position in pixels on the window
			38, --(number) y position in pixels on the window
			"Page|"..pageStr, --(string) return id used in the DynamicWindowResponse event
			"", --(string) text in the button (defaults to empty string)
			154, --(number) width of the button (defaults to width of text)
			93,--(number) height of the button (default decided by type of button)
			"", --(string) mouseover tooltip for the button (default blank)
			"", --(string) server command to send on button click (default to none)
			false, --(boolean) should the window close when this button is clicked? (default true)
			"BookPageUp" --(string) button type (default "Default")
			--(string) button state (optional, valid options are default,pressed,disabled)
			--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
		)
	end

	local buttonState = ""
	if(mRunebookPageIndex == 1) then
		buttonState = "pressed"
	end
	dynamicWindow:AddButton(
		32, --(number) x position in pixels on the window
		70, --(number) y position in pixels on the window
		"Page|1", --(string) return id used in the DynamicWindowResponse event
		"", --(string) text in the button (defaults to empty string)
		78, --(number) width of the button (defaults to width of text)
		58,--(number) height of the button (default decided by type of button)
		"", --(string) mouseover tooltip for the button (default blank)
		"", --(string) server command to send on button click (default to none)
		false, --(boolean) should the window close when this button is clicked? (default true)
		"RuneTab", --(string) button type (default "Default")
		--"SongsTab", --(string) button type (default "Default")
		buttonState --(string) button state (optional, valid options are default,pressed,disabled)
		--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
	)

	if(mRunebookPageIndex == 1) then
		AddRunebookIndexPage(dynamicWindow)
	else
		AddRunebookDetailPage(dynamicWindow)
	end

	local charges, maxCharges = GetRunebookCharges(runebookObj)
	dynamicWindow:AddLabel(565,415, "Recall Charges: "..tostring(charges) .. "/" .. tostring(maxCharges),160,20,20,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
	dynamicWindow:AddButton(506,406,"RuneDrop","",140,38,"Use the Charge Runebook manifestation spell to add charges.","",false,"Invisible")

	this:OpenDynamicWindow(dynamicWindow)
end

function AddRunebookIndexPage(dynamicWindow)

	dynamicWindow:AddLabel(
		236, --(number) x position in pixels on the window
		44, --(number) y position in pixels on the window
		"[43240f]"..StripColorFromString(runebookObj:GetName()).."[-]", --(string) text in the label
		0, --(number) width of the text for wrapping purposes (defaults to width of text)
		0, --(number) height of the label (defaults to unlimited, text is not clipped)
		46, --(number) font size (default specific to client)
		"center", --(string) alignment "left", "center", or "right" (default "left")
		false, --(boolean) scrollable (default false)
		false, --(boolean) outline (defaults to false)
		"Kingthings_Calligraphica_Dynamic" --(string) name of font on client (optional)
	)

	dynamicWindow:AddImage(
		110, --(number) x position in pixels on the window
		80, --(number) y position in pixels on the window
		"SpellIndexInfo_Divider", --(string) sprite name
		250, --(number) width of the image
		0, --(number) height of the image
		"Sliced" --(string) sprite type Simple, Sliced or Object (defaults to Simple)
		--spriteHue, --(string) sprite hue (defaults to white)
		--opacity --(number) (default 1.0)
	)		

	local runeInfo = runebookObj:GetObjVar("RuneInfo") or {}

	local xOffset = 120
	local yOffset = 90
	local runeIndex = 1
	for i,runeEntry in pairs(runeInfo) do
		local pageStr = tostring(1 + math.ceil(runeIndex/2))
		dynamicWindow:AddButton(
			xOffset, --(number) x position in pixels on the window
			yOffset, --(number) y position in pixels on the window
			"Page|"..pageStr, --(string) return id used in the DynamicWindowResponse event
			runeEntry.Name or "Blank Rune", --(string) text in the button (defaults to empty string)
			250, --(number) width of the button (defaults to width of text)
			34,--(number) height of the button (default decided by type of button)
			"", --(string) mouseover tooltip for the button (default blank)
			"", --(string) server command to send on button click (default to none)
			false, --(boolean) should the window close when this button is clicked? (default true)
			"BookListSingle" --(string) button type (default "Default")
			--(string) button state (optional, valid options are default,pressed,disabled)
			--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
		)
		
		yOffset = yOffset + 36
		runeIndex = runeIndex + 1
		if(runeIndex == 9) then
			yOffset = 48
			xOffset = xOffset + 320
		end
	end

	if(runeIndex <= ServerSettings.Teleport.RunebookMaxLocations) then
		dynamicWindow:AddButton(
				xOffset, --(number) x position in pixels on the window
				yOffset, --(number) y position in pixels on the window
				"DropMarked", --(string) return id used in the DynamicWindowResponse event
				"Drop marked rune here", --(string) text in the button (defaults to empty string)
				250, --(number) width of the button (defaults to width of text)
				34,--(number) height of the button (default decided by type of button)
				"", --(string) mouseover tooltip for the button (default blank)
				"", --(string) server command to send on button click (default to none)
				false, --(boolean) should the window close when this button is clicked? (default true)
				"BookListSingle", --(string) button type (default "Default")
				"faded"--(string) button state (optional, valid options are default,pressed,disabled)
				--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
			)

		for i=runeIndex+1,ServerSettings.Teleport.RunebookMaxLocations do
			runeIndex = runeIndex + 1
			yOffset = yOffset + 36
			if(runeIndex == 9) then
				yOffset = 90
				xOffset = xOffset + 300
			end

			dynamicWindow:AddButton(
				xOffset, --(number) x position in pixels on the window
				yOffset, --(number) y position in pixels on the window
				"", --(string) return id used in the DynamicWindowResponse event
				"", --(string) text in the button (defaults to empty string)
				250, --(number) width of the button (defaults to width of text)
				34,--(number) height of the button (default decided by type of button)
				"", --(string) mouseover tooltip for the button (default blank)
				"", --(string) server command to send on button click (default to none)
				false, --(boolean) should the window close when this button is clicked? (default true)
				"BookListSingle", --(string) button type (default "Default")
				"faded"--(string) button state (optional, valid options are default,pressed,disabled)
				--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
			)
		end

	end
end

function AddRunebookDetailPage(dynamicWindow)
	local runeInfo = runebookObj:GetObjVar("RuneInfo") or {}

	local curX = 0

	for runeIndex = (mRunebookPageIndex-2)*2  + 1, (mRunebookPageIndex-2)*2 + 2 do
		--DebugMessage("runeIndex",tostring(runeIndex))
		if(#runeInfo >= runeIndex) then
			local runeEntry = runeInfo[runeIndex]
			dynamicWindow:AddLabel(curX + 240,42,"[43240f]- "..tostring(runeIndex) .." -[-]",150,0,24,"center",false,false,"Kingthings_Dynamic")

			dynamicWindow:AddLabel(
				curX+245, --(number) x position in pixels on the window
				76, --(number) y position in pixels on the window
				"[43240f]"..StripColorFromString(runeEntry.Name).."[-]", --(string) text in the label
				0, --(number) width of the text for wrapping purposes (defaults to width of text)
				0, --(number) height of the label (defaults to unlimited, text is not clipped)
				46, --(number) font size (default specific to client)
				"center", --(string) alignment "left", "center", or "right" (default "left")
				false, --(boolean) scrollable (default false)
				false, --(boolean) outline (defaults to false)
				"Kingthings_Calligraphica_Dynamic" --(string) name of font on client (optional)
			)

			dynamicWindow:AddLabel(
				curX+236, --(number) x position in pixels on the window
				116, --(number) y position in pixels on the window
				"[43240f]"..runeEntry.RegionalName.."[-]", --(string) text in the label
				0, --(number) width of the text for wrapping purposes (defaults to width of text)
				0, --(number) height of the label (defaults to unlimited, text is not clipped)
				28, --(number) font size (default specific to client)
				"center", --(string) alignment "left", "center", or "right" (default "left")
				false, --(boolean) scrollable (default false)
				false, --(boolean) outline (defaults to false)
				"Kingthings_Calligraphica_Dynamic" --(string) name of font on client (optional)
			)

			if(runeEntry.Universe) then
				dynamicWindow:AddLabel(
					curX+236, --(number) x position in pixels on the window
					142, --(number) y position in pixels on the window
					"[43240f]"..runeEntry.Universe.." Universe[-]", --(string) text in the label
					0, --(number) width of the text for wrapping purposes (defaults to width of text)
					0, --(number) height of the label (defaults to unlimited, text is not clipped)
					28, --(number) font size (default specific to client)
					"center", --(string) alignment "left", "center", or "right" (default "left")
					false, --(boolean) scrollable (default false)
					false, --(boolean) outline (defaults to false)
					"Kingthings_Calligraphica_Dynamic" --(string) name of font on client (optional)
				)
			end

			dynamicWindow:AddButton(curX+115,182,"Remove|"..tostring(runeIndex),"[43240f]Remove[-]",250,34,"","",false,"BookListSingle")
			dynamicWindow:AddButton(curX+115,216,"Rename|"..tostring(runeIndex),"[43240f]Rename[-]",250,34,"","",false,"BookListSingle")
			dynamicWindow:AddButton(curX+115,250,"Recall|"..tostring(runeIndex),"[43240f]Recall (1 Charge)[-]",250,34,"","",false,"BookListSingle")
			dynamicWindow:AddButton(curX+115,284,"Portal|"..tostring(runeIndex),"[43240f]Cast Portal[-]",250,34,"","",false,"BookListSingle")

		end

		curX = curX + 312	
	end
end

RegisterEventHandler(EventType.DynamicWindowResponse, "Runebook",
	function (user,buttonId)
		if(buttonId == "DropMarked") then
			-- must be in backpack or have appropriate security
			if not (IsInBackpack(runebookObj,user)) then
				if( not(Plot.CheckUseObjectSecurity(user,runebookObj)) ) then
					return
				else
					user:SystemMessage("That must be in your backpack to do that.","info")	
					return 
				end
			end

			local carriedObject = user:CarriedObject()			
			if(carriedObject ) then	
				if not(IsRune(carriedObject)) then
					user:SystemMessage("You can not add that to this book.","info")
				else
					local destRegionAddress, destination = GetRuneDestination(carriedObject)				
					if(destination) then
						AddRuneLocationToRunebook(runebookObj,carriedObject,destination,destRegionAddress)
						carriedObject:Destroy()
						user:SystemMessage("Location added to runebook.","info")
						UpdateWindow()
					else
						user:SystemMessage("That rune has no inscription.","info")
					end
				end
			end		
		else
			local buttonId,arg = buttonId:match("(%a+)|(.+)")
			if(buttonId == "Page") then
				mRunebookPageIndex = tonumber(arg)
				UpdateWindow()
			elseif(buttonId == "Remove") then
				-- must be in backpack or have appropriate security
				if not (IsInBackpack(runebookObj,user)) then
					if( not(Plot.CheckUseObjectSecurity(user,runebookObj)) ) then
						return
					else
						user:SystemMessage("That must be in your backpack to do that.","info")	
   						return 
   					end
				end

				local runeIndex = tonumber(arg)
				RemoveRune(runeIndex)
			elseif(buttonId == "Rename") then
				-- must be in backpack or have appropriate security
				if not (IsInBackpack(runebookObj,user)) then
					if( not(Plot.CheckUseObjectSecurity(user,runebookObj)) ) then
						return
					else
						user:SystemMessage("That must be in your backpack to do that.","info")	
   						return 
   					end
				end

				local runeIndex = tonumber(arg)
				RenameRuneEntry(runeIndex)				
			elseif(buttonId == "Recall") then
				if( not(Plot.CheckUseObjectSecurity(user,runebookObj)) ) then
					return
				end

				if( InNoPortalExitRegion( user ) ) then
					user:SystemMessage("Cannot recall out of this area.", "info")
					return
				end

				local runeIndex = tonumber(arg)
				RecallToRune(runeIndex)
			elseif(buttonId == "Portal") then

				if( InNoPortalExitRegion( user ) ) then
					user:SystemMessage("Cannot create portal inside this area.", "info")
					return
				end

				local runeIndex =  tonumber(arg)
				PortalToRune(runeIndex)
			else
				CleanUp()
			end
		end
	end)

function RemoveRune(runeIndex)
	local runeEntry, runeInfo = GetRuneEntryAtIndex(runeIndex)

	if(runeEntry) then
		Create.InBackpack("rune_blank",this,nil,function (runeObj)
			-- fix transferred bugged runes
			local currentUniverse = GetUniverseName()
	        local universeName = GetUniverseName(runeEntry.RegionAddress)
	        if(universeName and currentUniverse and universeName ~= currentUniverse) then
	            runeEntry.RegionAddress = string.gsub(runeEntry.RegionAddress, universeName..".", currentUniverse..".")
	        end

			MarkRune(runeObj,runeEntry.Destination,runeEntry.RegionAddress,runeEntry.Facing,runeEntry.Appearance,runeEntry.RegionalName,runeEntry.Universe)
			runeObj:SetName(runeEntry.Name)
			backpackObj = this:GetEquippedObject("Backpack")
  			backpackObj:SendOpenContainer(this)

  			this:SystemMessage("You remove the rune from the book.","info")

  			table.remove(runeInfo,runeIndex)
  			runebookObj:SetObjVar("RuneInfo",runeInfo)

  			mRunebookPageIndex = 1

  			UpdateWindow()
		end)
	end
end

function RenameRuneEntry(runeIndex)
	local runeEntry = GetRuneEntryAtIndex(runeIndex)
	if(runeEntry) then
		TextFieldDialog.Show{
	        TargetUser = this,
	        Title = "Rename Rune",
	        DialogId = "RenameRune",
	        Description = "Enter the name of the rune.",
	        ResponseFunc = function(user,newName)
	        	if(newName ~= nil and newName ~= "") then
	        		if (string.len(newName) > 20) then
				 		user:SystemMessage("The rune name must be less than 26 characters.","info")
				 		return
				 	end

	        		if(ServerSettings.Misc.EnforceBadWordFilter and HasBadWords(newName)) then
	    		 		user:SystemMessage("Rune name can not contain any foul language!","info")
	    		 		return
	    		 	end

	    		 	local newName = StripColorFromString(newName)
					local runeInfo = runebookObj:GetObjVar("RuneInfo")
					if(runeIndex and #runeInfo >= runeIndex) then
						runeInfo[runeIndex].Name = newName
						runebookObj:SetObjVar("RuneInfo",runeInfo)
						user:SystemMessage("Rune renamed.","info")

						UpdateWindow()
					end
		        end
	        end
	    }    
	end
end

function RecallToRune(runeIndex)
	local runeEntry = GetRuneEntryAtIndex(runeIndex)

	if(runeEntry) then
		local charges = runebookObj:GetObjVar("RuneCount") or 0
		if(this:IsMoving()) then
			this:SystemMessage("Can not be used while moving.","info")
		elseif(charges > 0) then
			this:CloseDynamicWindow("Runebook")
			this:SendMessage("StartMobileEffect", "RunebookRecall", this, { RuneEntry = runeEntry, RunebookObj = runebookObj })
		else
			this:SystemMessage("Runebook has no charges.","info")
		end
	end
end

function PortalToRune(runeIndex)
	local runeEntry = GetRuneEntryAtIndex(runeIndex)

	if(runeEntry) then
		this:SetObjVar("TeleportTarget",runeEntry)
		this:SendMessage("CastSpellMessage","Portal",this,nil,nil,true)
		CallFunctionDelayed(TimeSpan.FromMilliseconds(50), 
		function()
			this:SendClientMessage("CancelSpellCast")
		end)
	end
end

function GetRuneEntryAtIndex(runeIndex)
	local runeInfo = runebookObj:GetObjVar("RuneInfo")
	if(runeIndex and #runeInfo >= runeIndex) then
		return runeInfo[runeIndex], runeInfo
	end
end

function Init(bookTarget)
	runebookObj = bookTarget
    UpdateWindow()
end


RegisterEventHandler(EventType.Message, "Init", Init)

RegisterEventHandler(EventType.Message, "UpdateRunebookWindow", function ( ... )
	UpdateWindow()
end)

RegisterEventHandler(EventType.ModuleAttached,"runebook_window",
	function ( ... )
		if(initializer and initializer.BookTarget) then
			runebookObj = initializer.BookTarget
			UpdateWindow()
		else
			CleanUp()
		end
	end)

RegisterEventHandler(EventType.LoadedFromBackup,"",
	function ()
		CleanUp()
	end)