CloudHeight = 3 -- seconds
CloudDuration = 4 -- seconds
LightningDelay = 1.5 -- seconds

local function ValidateSummonStorm(targetLoc)
	--DebugMessage("Debuggery Deh Yah")
	if( not(IsPassable(targetLoc)) ) then
		this:SystemMessage("[$2614]","info")
		return false
	end

	if not(this:HasLineOfSightToLoc(targetLoc,ServerSettings.Combat.LOSEyeLevel)) then
		this:SystemMessage("[$2615]","info")
		return false
	end
	return true
end

local function SummonStorm( targetLoc )
	this:PlayObjectSound("event:/magic/earth/magic_earth_cast_meteor", false)
	local mobiles = FindObjects(SearchMulti({SearchRange(targetLoc,5), SearchMobile()}), GameObj(0))
	for i=1,#mobiles do
		if ( mobiles[i]:HasLineOfSightToLoc(targetLoc,ServerSettings.Combat.LOSEyeLevel) and ValidCombatTarget(this, mobiles[i], true) ) then
			local loc = mobiles[i]:GetLoc()
			loc.Y = loc.Y + CloudHeight
			PlayEffectAtLoc("ChainLightningExplosionEffect", loc, CloudDuration)
			CallFunctionDelayed(TimeSpan.FromSeconds(LightningDelay),function()
				loc.Y = loc.Y - CloudHeight
				this:SendMessage("RequestMagicalAttack", "SummonStorm", mobiles[i], this, true, loc)
				mobiles[i]:PlayEffect("LightningCloudEffect")
			end)
			
		end
	end
	
end

RegisterEventHandler(EventType.Message,"SummonStormSpellTargetResult", function (targetLoc, skipRemove)
		if not(ValidateSummonStorm(targetLoc)) then
			EndEffect()
			return
		end
		local mTargetLoc = targetLoc
		-- create the cloud 
		mTargetLoc.Y = mTargetLoc.Y + CloudHeight

		PlayEffectAtLoc("ChainLightningExplosionEffect",mTargetLoc, CloudDuration)

		local stormEventTimer = "SummonStorm_"..uuid()

		RegisterSingleEventHandler(EventType.Timer, stormEventTimer,
		function( targetLoc )
			SummonStorm( targetLoc )
		end)

		this:ScheduleTimerDelay(TimeSpan.FromSeconds(0.25),stormEventTimer,mTargetLoc)
		
		-- If we are by-passing the this we will need to end it on our own!
		if not( skipRemove ) then
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(3),"StormRemove",mTargetLoc)
		end
		
end)

RegisterEventHandler(EventType.Timer,"StormRemove", function()
	EndEffect()
end)

RegisterEventHandler(EventType.Message,"StormRemove", function()
	EndEffect()
end)

function EndEffect()
	if(this:HasTimer("StormRemove")) then this:RemoveTimer("StormRemove") end
	this:DelModule("sp_summon_storm_effect")
end