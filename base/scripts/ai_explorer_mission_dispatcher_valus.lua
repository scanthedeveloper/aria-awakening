require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false
AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

local currencyName = QuestsLeague.LeagueCurrencyName.Explorers
local hasRewards = this:HasObjVar("HasRewards")

function Dialog.OpenGreetingDialog(user)
        local text = "The world of New Celador is filled with amazing and treacherous places. The more we explore these sites the more we can decipher the great tapestry of New Celador."
        local response = {}
        local key = 1

        if( hasRewards ) then
            text = "Hello there, is there something you'd like to purchase from the League of Explorers? The table here showcases our stocks."
            
            response[key] = {}
            response[key].text = "What can I purchase?"
            response[key].handle = "PurchaseRewards"
            key = key + 1

            --response[key] = {}
            --response[key].text = "What is in the supply box?"
            --response[key].handle = "LotteryBoxInfo"
            --key = key + 1
        end

        --[[
        response[key] = {}
        response[key].text = "Who are you?"
        response[key].handle = "Who"
        key = key + 1
        ]]


        response[key] = {}
        response[key].text = "Goodbye."
        response[key].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
end

function MakeGetRewardsTable(user)
    local Rewards = {
        HairDye = {
            Name = "Explorer Hair Dye",
            Cost = 8000,
            Template = "hair_dye_explorer", -- 958
        },
        ClothDye = {
            Name = "Explorer Cloth Dye",
            Cost = 8000,
            Template = "explorer_clothing_dye",
        },
        LeatherDye = {
            Name = "Explorer Leather Dye",
            Cost = 8000,
            Template = "explorer_leather_dye",
        },
        Dagger = {
            Name = "Explorer's Ice-covered Shield",
            Cost = 36000,
            Template = "pr10_league_caster_shield",
        },
        Cloak = {
            Name = "Explorer League Cloak",
            Cost = 18000,
            Template = "explorer_cloak",
        },
        Ring = {
            Name = "Explorers's Ring",
            Cost = 36000,
            Template = "pr10_league_melee_ring",
        },
        LotteryBox = {
            Name = "Title Scroll: 'The Pioneer'",
            Cost = 24000,
            Template = "pr10_league_explorer_titlescroll",
        },
    }

    return Rewards
end

-- Callback to alter items if needed
function AfterPurchase(item)
    return
end

function Purchase(user, itemKey, Rewards)
    
    -- See if the item is still for sale
    if ( Rewards[itemKey] == nil or Rewards[itemKey].Cost == nil or Rewards[itemKey].Template == nil ) then
        QuickDialogMessage(this,user,"That item is not available right now, unfortunately.")
        return
    end
    
    -- Check to see if the player has enough currency
    if( QuestsLeague.AdjustLeagueCurrency( user, "Explorers", -Rewards[itemKey].Cost ) ) then
        Create.InBackpack(Rewards[itemKey].Template, user, nil, function(item)
            if ( item ) then
                AfterPurchase(item)
                Dialog.OpenPurchaseRewardsDialog(user)
            else
                QuickDialogMessage(this,user,"I'm sorry something went wrong, please try that again.")
            end
        end)
    else
        QuickDialogMessage(this,user,"You do not have enough "..currencyName.." to purchase that.")
    end

end

function SetupPurchaseDialogHandles(user, Rewards)
    -- setup the dialog handles for each item purchase
    for key,val in pairs(Rewards) do
        Dialog[string.format("Open%sConfirmDialog", key)] = function(user)
            Purchase(user, key, Rewards)
        end
        Dialog[string.format("Open%sDialog", key)] = function(user)
            local text = string.format("I can sell you %s for %s "..currencyName..".", val.Name, val.Cost)
            local response = {
                {
                    text = "I accept your offer.",
                    handle = key.."Confirm",
                },
                {
                    text = "What else do you have to sell?",
                    handle = "PurchaseRewards"
                },
                {
                    text = "Goodbye.",
                    handle = ""
                },
            }
            NPCInteractionLongButton(text,this,user,"Responses",response)
        end
    end
end

function Dialog.OpenPurchaseRewardsDialog(user)
    local text = "Which item would you like to purchase?\n\nYou currently have [483D8B]"..QuestsLeague.GetLeagueCurrecy(user,"Explorers").."[-] favor with the League of Explorers."
    local response = {}
    local Rewards = MakeGetRewardsTable(user)

    SetupPurchaseDialogHandles(user, Rewards)

    for x, y in sortpairs(Rewards, function(Rewards, a, b) return Rewards[a]["Name"] < Rewards[b]["Name"] end) do
        table.insert(response, {text = y.Name..": "..y.Cost.." "..currencyName, handle = x})
    end

    table.insert( response, { text = "Goodbye.", handle = "" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenLotteryBoxInfoDialog(user)
    local text = "The supply box..."
    local response = {}

    table.insert( response, { text = "Thank you.", handle = "Greeting" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

if( not hasRewards ) then

    -- [[ QUEST STUFF ]]
    function AI.QuestStepsInvolvedInFunc()
        return 
        { 
            {"ExplorerLeaguePR10", math.random(1, #AllQuests["ExplorerLeaguePR10"])} ,
            {"ExplorerLeaguePR10", math.random(1, #AllQuests["ExplorerLeaguePR10"])} ,
            {"ExplorerLeaguePR10", math.random(1, #AllQuests["ExplorerLeaguePR10"])} 
        }
    end

    AI.QuestsWelcomeText ="Greetings. I am looking for a handy adventurer to do some leg work. You seem stout enough. What can you help me with..."

    AI.QuestAvailableMessages = 
    {
        "I've a job for you to do.",
    }

    function Dialog.OpenWhoDialog(user)
        QuickDialogMessage(this,user,"I am Jonus. I was a member of the city guard before I was recruited by the League to help organize their exploration efforts. It's honest work and I don't have to chase snoopers around the city.")
    end

end