plotStorage, activeTab, tabState, userId = nil

-- this is set to true any time there is a pending action to prevent funny stuff
actionsLocked = false

plotContainers = {}

function CanPerformAction()
    return not(HasMobileEffect(this, "MovingCrate") or HasMobileEffect(this, "MovingLandDeed") or HasMobileEffect(this, "LandDeed") )
end

function CleanUp()
    this:CloseDynamicWindow("StoredItemWindow")
    this:CloseDynamicWindow("ItemStorage")
    this:DelModule("stored_item_manager")
end

function UpdateWindow()
    if(not plotStorage) then
        CleanUp()
        return
    end

    local dynamicWindow = DynamicWindow("StoredItemWindow", "Item Storage Management",550,504,0,0,"")

    local tabs = {}
    for i, plotInfo in pairs(plotStorage) do
        table.insert(tabs,{ Text = plotInfo.PlotSize.X .. " X " .. plotInfo.PlotSize.Z, TabId = tostring(i) })        
    end    

    activeTab = activeTab or "1"
    tabState = tabState or "Main"

    AddTabMenu(dynamicWindow,
    {
        ActiveTab = activeTab, 
        Buttons = tabs,
        ButtonWidth = 103,
        Width = 510,
    })    

    HandleTabs(dynamicWindow)

    this:OpenDynamicWindow(dynamicWindow, this)
end

function HandleTabs(dynamicWindow)
    dynamicWindow:AddImage(8,32,"BasicWindow_Panel",514,414,"Sliced")

    local plotIndex = tonumber(activeTab)
    if(plotIndex <= #plotStorage) then
        AddPlotTab(dynamicWindow,plotIndex)        
    end
end

function AddPlotTab(dynamicWindow, plotIndex)
    local plotInfo = plotStorage[plotIndex]
    dynamicWindow:AddLabel(20,44,"Original Item Count: "..plotInfo.ItemCount,0,0,18)
    local houseName = "No House"
    if(plotInfo.HouseTemplate) then
        local directions = { "North", "South", "East", "West"}
        houseName = (GetTemplateObjectName(plotInfo.HouseTemplate) or "") .." ("..directions[plotInfo.HouseDirection]..")"
    end
    dynamicWindow:AddLabel(20,64,"House: "..houseName,0,0,18)

    dynamicWindow:AddLabel(20,104,"Choose this option if you wish to restore your old plot exactly as it was.",0,0,18)    
    dynamicWindow:AddButton(20,134,"Automatic","Automatic Restore ("..plotInfo.PlotSize.X .. " X " .. plotInfo.PlotSize.Z ..")",490,26,"Allows you claim land at exactly the size of the old plot (No resize needed)","",false,"List")

    dynamicWindow:AddLabel(20,190,"Choose this option if you wish to claim a 12 x 12 plot freely resizable up to your old plot size. All of your items including blueprint, merchant and sale items can be reclaimed from a Moving Crate on the plot. You have one week to claim the items before the Moving Crate is destroyed.",480,90,18) 
    dynamicWindow:AddButton(20,270,"Advanced","Advanced Restore",490,26,"Allows you claim a land deed for the default size (12x12). This deed will be freely resizable up to the old plot size.","",false,"List")    

    if(plotInfo.StorageKey) then
        dynamicWindow:AddLabel(20,314,"Choose this option if you can not place any more plots and just want the items from this plot. A Moving Crate will be placed at the location you specify on another plot that you own. You have one week to claim the items before the Moving Crate is destroyed.",480,90,18) 
        dynamicWindow:AddButton(20,394,"Crate","Crate Only",490,26,"","",false,"List")    
    end

    if(IsGod(this)) then
        dynamicWindow:AddButton(20,424,"Transfer","Transfer to User (GOD)",490,26,"","",false,"List")    
    end
end

RegisterEventHandler(EventType.DynamicWindowResponse, "StoredItemWindow", function (user,returnId)
    if(not plotStorage) then
        CleanUp()
        return
    end

    local newTab = HandleTabMenuResponse(returnId)
    if ( newTab ) then
        activeTab = newTab
        UpdateWindow()
        return
    end

    local plotIndex = tonumber(activeTab)
    local plotInfo = nil
    if(plotIndex <= #plotStorage) then
        plotInfo = plotStorage[plotIndex]
    end

    if(returnId and returnId ~= "") then
        local override = this:GetObjVar("AllowUnpack")
        if(override or (ServerSettings.ClusterId ~= ServerSettings.Clusters["Crimson Sea"].Id)) then 
            if(returnId == "Automatic") then
                if not(CanPerformAction()) then
                    this:SystemMessage("You must cancel the current action before you can do this.","info")
                    return
                end

                this:SendMessage("StartMobileEffect", "MovingLandDeed", this, {PlotX = plotInfo.PlotSize.X,PlotZ = plotInfo.PlotSize.Z, StorageKey=plotInfo.StorageKey, MovingType="Automatic", UserId=userId})
                return
            elseif(returnId == "Advanced") then        
                if not(CanPerformAction()) then
                    this:SystemMessage("You must cancel the current action before you can do this.","info")
                    return
                end

                this:SendMessage("StartMobileEffect", "MovingLandDeed", this, {FreeResize = plotInfo.PlotSize, StorageKey = plotInfo.StorageKey, MovingType="Manual", UserId=userId})
                return
            elseif(returnId == "Crate") then
                if not(plotInfo.StorageKey) then
                    return
                end

                if not(CanPerformAction()) then
                    this:SystemMessage("You must cancel the current action before you can do this.","info")
                    return
                end

                this:SendMessage("StartMobileEffect", "MovingCrate", this, {FreeResize = plotInfo.PlotSize, StorageKey = plotInfo.StorageKey, UserId = userId})
                return
            elseif(returnId == "Transfer" and IsGod(this)) then
                local plotIndex = tonumber(activeTab)

                TextFieldDialog.Show{
                    TargetUser = this,
                    ResponseObj = this,
                    Title = "Transfer Packed Plot",
                    Description = "Enter the UserId (XX-XXXX) of the user to transfer to.",
                    ResponseFunc = function(user,userId)
                        SetGlobalVar("PackedPlots."..userId, 
                            function(record)
                                table.insert(record,plotStorage[plotIndex])
                                return true
                            end,
                            function(success)
                                SetGlobalVar("PackedPlots."..this:GetAttachedUserId(), 
                                    function(record)
                                        table.remove(record,plotIndex)
                                        return true
                                    end,
                                    function (success)
                                        this:SendMessage("RefreshStoredItemManager")
                                        this:SystemMessage("Transfer complete, Success: " .. tostring(success))
                                    end)
                            end)
                    end
                }

                local plotInfo = plotStorage[plotIndex]

            end
        else
            this:SystemMessage("Plots can not be unpacked on Crimson Sea. You must transfer to Ethereal Moon before you can unpack. If this is an error, please contact a GM.","info")
            return
        end
    end
end)

function OnLoad()
    userId = (initializer and initializer.UserId) or this:GetAttachedUserId()

    -- check for houses in temp pack 
    local tempPack = this:GetEquippedObject("TempPack")
    if(tempPack) then
        for i,tempObj in pairs(tempPack:GetContainedObjects()) do
            if(tempObj:HasObjVar("PlotStorage")) then
                Plot.SendToStorage(tempObj:GetObjVar("ObjectHeader"),userId,tempObj)
            end
        end
    end

    CallFunctionDelayed(TimeSpan.FromSeconds(1),function ( ... )        
        plotStorage = GlobalVarRead("PackedPlots."..userId)
        if(plotStorage) then
            if(initializer and initializer.UserId) then
                UpdateWindow()
            else            
                OpenStorageButton()
            end
        else
            CleanUp()
        end
    end)
end

RegisterEventHandler(EventType.ModuleAttached, "stored_item_manager", function ( ... )
    OnLoad()
end)

RegisterEventHandler(EventType.LoadedFromBackup,"",function ( ... )
    OnLoad()
end)

RegisterEventHandler(EventType.Message,"RefreshStoredItemManager",function ( ... )
    plotStorage = GlobalVarRead("PackedPlots."..userId)
    if(plotStorage and #plotStorage > 0) then
        UpdateWindow()
    else
        if(#plotStorage == 0) then
            DelGlobalVar("PackedPlots."..userId)
        end
        CleanUp()
    end
end)

function OpenStorageButton() 
    local testWindow = DynamicWindow("ItemStorage","",200,24,-470,20,"Transparent","TopRight")
    testWindow:AddButton(0,0,"Control","Item Storage Management",0,24,"","",false,"")
    this:OpenDynamicWindow(testWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse, "ItemStorage", function (user,returnId)
    if(returnId == "Control") then
        UpdateWindow()
    end
end)