BOOK_WIDTH = 789
BOOK_HEIGHT = 455

mCatalogObj = nil
mPageTurns = nil
mMaxPageTurns = 1

mCollectionPageIndices = {}

mOpen = false

--[[
	DynamicWindow(ID, Title, WindowWidth, WindowHeight, xOffset, yOffset, WindowType, WindowAnchor)

	AddLabel(xOffset, yOffset, LabelText, TextWidth, LabelHeight, FontSize, Alignment, Scrollable, Outline, Font)

	AddButton(xOffset, yOffset, ID, ButtonText, ButtonWidth, ButtonHeight, Tooltip, ServerCommand, ShouldCloseOnPress, ButtonType, ButtonState, CustomSprite)

	AddImage(xOffset, yOffset, SpriteName, ImageWidth, ImageHeight, SpriteType, SpriteHue, Opacity)
]]

function CleanUp()
    this:CloseDynamicWindow("Catalog")
    this:DelModule(GetCurrentModule())
end

function DrawCatalogWindow()
	if not (mCatalogObj) then DebugMessage("Draw Catalog_Window failed.",mCatalogObj) return end

	local dynamicWindow = DynamicWindow("Catalog", "", BOOK_WIDTH, BOOK_HEIGHT, -BOOK_WIDTH/2, -BOOK_HEIGHT/2, "TransparentDraggable", "Center")

	dynamicWindow:AddImage(0, 0, "Skillbook", BOOK_WIDTH, BOOK_HEIGHT)

	local hasPrevPage = mPageTurns >= 1
	if (hasPrevPage) then
		local pageStr = tostring(mPageTurns * 2 - 1)
		dynamicWindow:AddButton(70, 25, "Page|"..pageStr, "", 154, 93, "", "", false, "BookPageDown")
	end

	local runeInfo = {}
	local hasNextPage = mPageTurns < mMaxPageTurns
	if (hasNextPage) then
		local pageStr = tostring(mPageTurns * 2 + 3)
		dynamicWindow:AddButton(575, 25, "Page|"..pageStr, "", 154, 93, "", "", false, "BookPageUp")
	end
	
	dynamicWindow:AddButton(720, 20, "", "", 0, 0, "", "", true, "CloseSquare")

	local buttonState = (mPageTurns == 0 and "pressed") or ""
	dynamicWindow:AddButton(6, 70, "Page|1", "", 78, 58, "", "", false, "RuneTab", buttonState)

	AddCollectionPage(dynamicWindow)

	this:OpenDynamicWindow(dynamicWindow)
	mOpen = true
end

function AddCollectionPage(dynamicWindow)
	local args = {}

	local leftPageIndex = mPageTurns * 2 + 1
	local leftPageCollectionIndex = 1
	local rightPageCollectionIndex = 1

	for i=1, #mCollectionPageIndices do
		local currentCollectionPageIndex = mCollectionPageIndices[i]
		local nextCollectionPageIndex = mCollectionPageIndices[i+1] or 999
		if leftPageIndex >= currentCollectionPageIndex and leftPageIndex ~= 0 then
			if leftPageIndex < nextCollectionPageIndex then
				leftPageCollectionIndex = leftPageIndex - currentCollectionPageIndex + 1
				--DebugMessage("Left Page Collection Index:",leftPageCollectionIndex)
				args.CollectionIndex = i
				args.Collection = AllTomeCollections[args.CollectionIndex]

				rightPageCollectionIndex = leftPageIndex + 1 == nextCollectionPageIndex and 1 or leftPageCollectionIndex + 1
				--DebugMessage("|",currentCollectionPageIndex,"<=",leftPageIndex,"<",nextCollectionPageIndex,"| collIndex:",leftPageCollectionIndex,"| rightNew?",rightPageCollectionIndex)
			end
		end
	end

	if not args.Collection then DebugMessage("CatalogCollectionPage() Failed! No collection.") return end

	args.DynamicWindow = dynamicWindow

	args.CollectionPageIndex = leftPageCollectionIndex
	args.PageIndex = leftPageIndex
	args.xOffset = 0
	--DebugMessage("LEFT <--")
	DrawTomeList(args)

	args.CollectionPageIndex = rightPageCollectionIndex
	args.PageIndex = leftPageIndex + 1
	args.xOffset = 310
	if rightPageCollectionIndex == 1 then
		args.CollectionIndex = args.CollectionIndex + 1
		args.Collection = AllTomeCollections[args.CollectionIndex]
	end
	--DebugMessage("--> RIGHT")
	DrawTomeList(args)
end

function DrawTomeList(args)
	if not (args.Collection) then return end
	local maxTomesOnPage = 6

	if args.CollectionPageIndex == 1 then
		args.StartIndex = 1
		args.yOffset = 90
		args.DynamicWindow:AddLabel(245 + args.xOffset, 44, "[43240f]"..args.Collection[1].."[-]", 0, 0, 46, "center", false, false, "Kingthings_Calligraphica_Dynamic")
	else
		args.StartIndex = (args.CollectionPageIndex - 2) * 8 + 8
		--maxTomesOnPage = 7
		args.yOffset = 90
	end

	if args.Collection.Locked then
		args.DynamicWindow:AddLabel(245, args.yOffset + 10,"[43240fdd] Currently unavailable. [-]",200,34,20,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
		args.DynamicWindow:AddLabel(245 + args.xOffset,365,"[43240f]"..tostring(args.PageIndex).."[-]",260,340,15,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
		return
	end

	args.EndIndex = math.min(#args.Collection[2], args.StartIndex + maxTomesOnPage)
	local tomes = mCatalogObj:GetObjVar("Tomes") or {}
	
	--[[local frameX = args.xOffset + 120
	local frameWidth = 250
	local nameX = args.xOffset + 150
	local nameWidth = 175
	local progressX = args.xOffset + 320
	local progressWidth = 70]]
	local frameX = args.xOffset + 100
	local frameWidth = 280
	local nameX = args.xOffset + 130
	local nameWidth = 200
	local progressX = args.xOffset + 330
	local progressWidth = 70

	local isAdmin = IsGod(this) and not TestMortal(this)
	local incompleteTooltip = "Collect all Entries to be able to transcribe this knowledge."
	local godTooltip = "GOD: Click to change entry to "

	--DebugMessage("for i=",args.StartIndex,",",args.EndIndex,"do")
	for i = args.StartIndex, args.EndIndex do
		local tomeId = args.Collection[2][i] or ""
		local tome = tomes[tomeId] or {}

		if next(tome) then
			local name = AllTomes[tomeId].Name or ""

			local entryIntSum = 0
			local completedEntryCount = 0
			local entryCount = #tome
			for j=1, entryCount do
				entryIntSum = entryIntSum + tome[j]
				completedEntryCount = tome[j] > 0 and (completedEntryCount + 1) or completedEntryCount
			end

			local isFilledInt = completedEntryCount < entryCount and 0 or entryIntSum == 2 * entryCount and 2 or 1
			local progress = tostring(completedEntryCount).."/"..tostring(entryCount)

			name = completedEntryCount == 0 and "???" or name
			if isFilledInt == 0 then
				args.DynamicWindow:AddButton(frameX, args.yOffset, "", "", frameWidth, 34, incompleteTooltip, "", false, "BookListSingle", "faded")
				args.DynamicWindow:AddLabel(nameX, args.yOffset + 10,"[43240fdd]"..name.."[-]",nameWidth,34,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
				args.DynamicWindow:AddLabel(progressX, args.yOffset + 10,"[43240f]"..progress.."[-]",progressWidth,34,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")

				if isAdmin then
					args.DynamicWindow:AddButton(frameX, args.yOffset, "DBG|1:"..tostring(i)..":"..tostring(args.CollectionIndex), "", frameWidth, 34, godTooltip.."RECORDED.", "", false, "BookListSingle", "faded")
				end
			elseif isFilledInt == 1 then
				args.DynamicWindow:AddButton(frameX, args.yOffset, "SET|"..tostring(i)..":"..tostring(args.CollectionIndex), name, frameWidth, 34, "", "", false, "BookListSingle")
				args.DynamicWindow:AddLabel(progressX, args.yOffset + 10,"[43240f]"..progress.."[-]",progressWidth,34,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")

				if isAdmin then
					args.DynamicWindow:AddButton(frameX, args.yOffset, "DBG|2:"..tostring(i)..":"..tostring(args.CollectionIndex), "", frameWidth, 34, godTooltip.."TRANSCRIBED.", "", false, "BookListSingle", "faded")
				end
			elseif isFilledInt == 2 then
				if isAdmin then
					args.DynamicWindow:AddButton(frameX, args.yOffset, "DBG|0:"..tostring(i)..":"..tostring(args.CollectionIndex), "", frameWidth, 34, godTooltip.."EMPTY.", "", false, "Invisible")
				end

				args.DynamicWindow:AddLabel(nameX, args.yOffset + 9,"[43240f]"..name.."[-]",nameWidth,34,22,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
				args.DynamicWindow:AddLabel(progressX, args.yOffset + 9,"[43240f]"..progress.."[-]",progressWidth,34,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
			else
				DebugMessage("[tome_entry_catalog_window] 'isFilledInt'",isFilledInt,"is not valid.")
			end

			args.yOffset = args.yOffset + 36
		end
	end
	args.DynamicWindow:AddLabel(245 + args.xOffset,365,"[43240f]"..tostring(args.PageIndex).."[-]",260,340,15,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
end

function UpdateCatalogTomeEntries(newValue, collectionIndex, tomeIndex, entryIndex)
	local tomes = mCatalogObj:GetObjVar("Tomes") or {}
	if AllTomeCollections[collectionIndex] then
		local id = AllTomeCollections[collectionIndex][2][tomeIndex]
		local tome = tomes[id]
		if tome then
			if entryIndex then 
				tome[entryIndex] = newValue
			else
				for i=1, #tome do
					tome[i] = newValue
				end
			end
		end
	end
	mCatalogObj:SetObjVar("Tomes", tomes)
	Lore.GetCatalogProgress(mCatalogObj)
end

RegisterEventHandler(EventType.DynamicWindowResponse, "Catalog", function (user,buttonId)
	--DebugMessage("Button Hit:",buttonId)
	local page, arg = buttonId:match("(%a+)|(.+)")
	if page == "Page" then
		mPageTurns = math.ceil(tonumber(arg) / 2 - 1)
		--DebugMessage("Page",arg,"=",mPageTurns,"page turns.")
		DrawCatalogWindow()
	elseif page == "" then
		CleanUp()
	elseif page == "DBG" then
		if not IsGod(this) then return end
		local argSplits = StringSplit(arg, ":")
		local newValue = tonumber(argSplits[1]) or 0
		local tomeIndex = tonumber(argSplits[2]) or 1
		local collectionIndex = tonumber(argSplits[3])

		local newStatus = newValue == 2 and "TRANSCRIBED" or newValue == 1 and "RECORDED" or "EMPTY"
		ClientDialog.Show{
            TargetUser = user,
            TitleStr = "GOD Catalog Set",
            DescStr = "Please confirm you wish to set '"..(AllTomes[AllTomeCollections[collectionIndex][2][tomeIndex]].Name or "[ERROR]").."' to "..newStatus.."?",
            Button1Str = "Confirm",
            Button2Str = "Cancel",
            ResponseObj = this,
            ResponseFunc = function (user,buttonId)
                if (buttonId == 0) then
                    UpdateCatalogTomeEntries(newValue, collectionIndex, tomeIndex)
					--DebugMessage("Collection",collectionIndex,"Index",tomeIndex,"is now value",newValue)
					DrawCatalogWindow()
                end
            end,
        }
		
	elseif page == "SET" then
		local ownerObj = mCatalogObj:GetObjVar("Owner")
		if ownerObj and ownerObj ~= user then
			ClientDialog.Show
			{
			    TargetUser = user,
			    TitleStr = "Already-Claimed Catalog",
			    DescStr = "This catalog is currently signed to "..ownerObj:GetName()..". You cannot transcribe their entries for them unless they unclaim the Catalog.",
			    --Button1Str = "Confirm",
			    Button2Str = "Cancel",
			    ResponseObj = this,
			    ResponseFunc = function (user,buttonId)

				end,
			}
			return
		end

		if IsLockedDown(mCatalogObj) then
			local plotController = Plot.GetAtLoc(mCatalogObj:GetLoc())
			if not plotController or Plot.IsStranger(plotController, user) then
				user:SystemMessage("That catalog is someone else's property.", "info") 
				return
			end
		end

		local argSplits = StringSplit(arg, ":")
		local tomeIndex = tonumber(argSplits[1])
		local collectionIndex = tonumber(argSplits[2])
		local collectionData = AllTomeCollections[collectionIndex]

		user:SystemMessage("Select an empty Tomebook to transcribe knowledge into.", "info")
		user:RequestClientTargetGameObj(this, "ToTomebook")

        RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse,"ToTomebook", function(targetTomebook, user)
        	if not targetTomebook or not (targetTomebook:GetCreationTemplateId() == "lore_tomebook" and collectionData) then return end
			if IsLockedDown(targetTomebook) then
				local plotController = Plot.GetAtLoc(targetTomebook:GetLoc())
    			if not plotController or Plot.IsStranger(plotController, user) then
					user:SystemMessage("That tomebook is someone else's property.", "info") 
					return
				end
			end

        	if targetTomebook and false then
				UpdateCatalogTomeEntries(2, collectionIndex, tomeIndex)
				DrawCatalogWindow()
        		return
        	end
			local tomeIds = collectionData[2]
			local tomeId = tomeIds[tomeIndex]
			--local tomebookEntries = targetTomebook:GetObjVar("Entries") or {}
			local catalogTomes = mCatalogObj:GetObjVar("Tomes") or {}
			local tomebookTomeId = targetTomebook:GetObjVar("TomeId")
			-- TomeId is primary identifier. TomeId's don't match = fail. If collectionIndex or tomeIndex fail, just update them to AllTomeCollections' records.
    		
        	if not targetTomebook:HasObjVar("CollectionIndex") or tomebookTomeId == tomeId then
    			
    			local targetEntries = targetTomebook:GetObjVar("Entries") or {}
				if #tomeIds == #targetEntries then
					user:SystemMessage("That tomebook is already complete.", "info")
				else
	    			local catalogTomeEntries = catalogTomes[tomeId]
	    			local entryIndicesString = ""
	    			for i=1, #catalogTomeEntries do
	    				if catalogTomeEntries[i] > 0 then
	    					local comma = entryIndicesString ~= "" and "," or ""
	    					entryIndicesString = entryIndicesString..comma..tostring(i)
	    				else
	    					DebugMessage("[lore_catalog_window] Error: Catalog has incomplete tome being transcribed.")
							user:SystemMessage("That Catalog entry is incomplete, it cannot be currently transcribed.", "info")
	    					return
	    				end
	    			end
	    			catalogTomes[tomeId].TranscribedTo = targetTomebook
	    			mCatalogObj:SetObjVar("Tomes", catalogTomes)

	    			targetTomebook:SetObjVar("CollectionIndex", collectionIndex)
    				targetTomebook:SetObjVar("TomeIndex", argSplits[1])
    				targetTomebook:SetObjVar("TranscribedBy", user)
	    			--DebugMessage("AddEntries()",tomeId,entryIndicesString)
	    			local entryIndices = Dialogue.ProcessNumberRange(entryIndicesString)
	    			targetTomebook:SendMessage("AddEntries", user, tomeId, entryIndices)
					Lore.RecordPlayerLoreTranscribing(user, mCatalogObj, tomeId, entryIndices)

	    			if HasMobileEffect(user, "Tomebook") then
						user:SendMessage("EndTomebookEffect")
					end

					UpdateCatalogTomeEntries(2, collectionIndex, tomeIndex)
					DrawCatalogWindow()

					targetTomebook:SetHue(collectionData.BookHue or 0)
					targetTomebook:SetSharedObjectProperty("Text", tostring(argSplits[1]))
				end
    		else
				user:SystemMessage("That tomebook is already used for a different topic.", "info")
    		end
	    end)
	else
		CleanUp()
	end
end)

RegisterEventHandler(EventType.Message, "OpenCatalog", function(catalog)
	mCatalogObj = catalog
	mPageTurns = mPageTurns or 0
	mCollectionPageIndices = {}

	local pageCount = 0
    for i=1, #AllTomeCollections do
        mCollectionPageIndices[i] = pageCount + 1
        pageCount = pageCount + math.ceil(#AllTomeCollections[i][2] / 8)
    end
	--mMaxPageTurns = math.ceil(pageCount / 2) - 2
	mMaxPageTurns = math.floor((pageCount - 1) / 2)
	--DebugMessage("OPEN","max page turns:",mMaxPageTurns,"coll page indices:",DumpTable(mCollectionPageIndices))

	DrawCatalogWindow()
end)

RegisterEventHandler(EventType.LoadedFromBackup, "", function ()
	CleanUp()
end)

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(5), "TWABNOCleanup")
end)
-- Catalog Window Attached But Not Open Cleanup
RegisterEventHandler(EventType.Timer, "TWABNOCleanup", function()
	if not( mOpen ) then
		this:DelModule(GetCurrentModule())
	end
end)