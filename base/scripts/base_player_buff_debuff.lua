--Basic buff layout, similar to map markers
--[[
{
	Identifier = "NameOfBuff"
	Icon = "SpriteImage",	
	Tooltip = "Tool tip describing the buff here."
	DisplayName = "Name of Buff"
	IsDebuff = true or false if it's a debuff
}
--]]
BUFF_COLOR = "[00D700]"
DEBUFF_COLOR = "[D70000]"
BUFFS_PER_ROW = 10
BUFF_SIZE = 24

mBuffs = {}

function HasBuff(buff)
    for i=1,#mBuffs do
		if ( mBuffs[i].Identifier == buff.Identifier ) then
		 	return true, i
		end
	end
	return false, nil
end

function AddBuff(buff, duration)

	--DebugTable(buff)

	if ( buff == nil ) then
		LuaDebugCallStack("[AddBuff] nil buff provided.")
		return
    end

    if ( type(duration) == "number" ) then
        -- legacy support
        duration = TimeSpan.FromSeconds(duration)
    end

	local hasBuff, buffPos = HasBuff(buff)
	if not( hasBuff ) then 
		mBuffs[#mBuffs+1] = buff 
	else
		mBuffs[buffPos] = buff 
	end
    
    if ( duration ~= nil ) then
        local timerId = "RemoveBuffIcon" .. tostring(buff.Identifier)
		RegisterSingleEventHandler(EventType.Timer, timerId, function()
			RemoveBuff(buff.Identifier)
		end)
		this:ScheduleTimerDelay(duration, timerId)
    end
    
	ShowBuffWindow()
end

function RemoveBuff(identifier)
    for i=1,#mBuffs do
        if ( mBuffs[i].Identifier == identifier ) then
            table.remove(mBuffs, i)
            ShowBuffWindow()
            break
        end
    end
end

function UpdateWindow()
	local taskWindow = DynamicWindow("BuffBar","",0,0,0,0,"Transparent"--[["Transparent"--]],"Top", 1000)
	--for each buff we have
    --but wait if there's no buffs then just close the window
    if (#mBuffs == 0) then
        -- when trying to close this at the same time another one is removed can cause it to become stuck -KH
        OnNextFrame(function()
            this:CloseDynamicWindow("BuffBar")
        end)
		return
	end

	local BUFF_OFFSET = BUFF_SIZE + 2
	local numBuffs = #mBuffs
	local startX = -math.min(numBuffs,BUFFS_PER_ROW) * BUFF_OFFSET / 2
    
    local buff
    for index=1,#mBuffs do
        buff = mBuffs[index]
        
		--create a buff that shows something when you mouse over it
        local displayString = (buff.IsDebuff and DEBUFF_COLOR or BUFF_COLOR) .. buff.DisplayName .. "[-]\n" .. buff.Tooltip

		local timeRemaining = this:GetTimerDelay("RemoveBuffIcon"..tostring(buff.Identifier))
		if ( timeRemaining ) then
			displayString = displayString .. "\n\nRemaining: " .. GetTimerLabelString(timeRemaining,true)
		end
		
		taskWindow:AddButton(startX + (((index-1) % BUFFS_PER_ROW) * BUFF_OFFSET),math.floor((index-1)/BUFFS_PER_ROW)*BUFF_OFFSET,"","",BUFF_SIZE,BUFF_SIZE,displayString,"",false,"Invisible")
		taskWindow:AddImage(startX + (((index-1) % BUFFS_PER_ROW) * BUFF_OFFSET),math.floor((index-1)/BUFFS_PER_ROW)*BUFF_OFFSET,buff.Icon,BUFF_SIZE,BUFF_SIZE)
		--add the buff
	end
	--open the window
	this:OpenDynamicWindow(taskWindow)
end

function ShowBuffWindow()
    this:ScheduleTimerDelay(TimeSpan.FromSeconds(0.2), "UpdateWindow")
end
RegisterEventHandler(EventType.Timer, "UpdateWindow", function()
    UpdateWindow()
end)

--Show the buff window on login and create.
RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(),function ( ... )
	ShowBuffWindow()
end)
RegisterEventHandler(EventType.LoadedFromBackup,"",function ( ... )
	ShowBuffWindow()
end)

RegisterEventHandler(EventType.Message,"AddBuffIcon",function (buff,timeSecs)
	AddBuff(buff,timeSecs)
end)

RegisterEventHandler(EventType.Message,"RemoveBuffIcon",function (identifier)
	RemoveBuff(identifier)
	-- body
end)