--Handed out by base_merchant
--To enable crafting orders, add the CraftOrderSkill objVar to a merchant template, and assign to the skill according to AllRecipes
--/create crafting_order picks a random crafting order from CraftingOrderDefines, regardless of skill


AddUseCase(this,"Add item to order",true,"UseObject")
AddUseCase(this, "Read order", true, "UseObject")
SetDefaultInteraction(this,"Add item to order")

function InitCraftingOrder()
	local orderInfo = this:GetObjVar("OrderInfo")

	local recipe = GetRecipeFromEntryName(orderInfo.Recipe)

	if not ( this:HasObjVar("CurrentAmount") ) then
		this:SetObjVar("CurrentAmount", 0)
	end

	SetItemTooltip(this)
	this:SetName("Craft Order: "..orderInfo.Amount.." "..GetItemNameFromOrderInfo(recipe))
end

function GetRecipeTemplateObjectName(recipe,material)
	local materialName = ""
	if ( material and Materials[material] and ResourceData.ResourceInfo[material].CraftedItemPrefix ~= nil ) then
		materialName = ResourceData.ResourceInfo[material].CraftedItemPrefix .. " "
	end
	local itemName = GetItemNameFromRecipe(recipe) or "Unknown"
	return materialName .. itemName
end

function GetItemNameFromOrderInfo(recipe)
	if (this:GetObjVar("OrderInfo") ~= nil) then
		local orderInfo = this:GetObjVar("OrderInfo")
		return GetRecipeTemplateObjectName(orderInfo.Recipe, orderInfo.Material)
	else 
		return this:GetObjVar("OrderInfo").Recipe
	end
end

--Check if item is a stack. If so, compare GetItemNameFromOrderInfo and singular name
function HandleSelectedTaskItem(target, user)
	
	if (target == nil) then return end	
	--if (target:GetObjVar("CraftedBy") ~= user:GetName() or GetItemNameFromOrderInfo()  ~= target:GetName()) then return end
	local orderInfo = this:GetObjVar("OrderInfo")

	if not (VerifyTarget(target, user)) then return end


	AddToOrder(target, user)
	local currentAmount = this:GetObjVar("CurrentAmount")
	if (currentAmount >= orderInfo.Amount) then 
		if not (this:GetObjVar("OrderComplete")) then 
			CompleteOrder(user)
		else
			user:SystemMessage("This order is already complete. Turn it in to a craftsman for your reward!","info")
			return
		end
	end

	user:RequestClientTargetGameObj(this, "select_task_item")
	SetItemTooltip(this)
end

function VerifyTarget(target, user)

	local orderInfo = this:GetObjVar("OrderInfo")
	local orderSkill = this:GetObjVar("OrderSkill")
	local orderRecipe = AllRecipes[orderSkill][orderInfo.Recipe]
	local crafter = target:GetObjVar("Crafter")

	if (target:TopmostContainer() ~= user) then
		user:SystemMessage("Item must be in your backpack.","info")
		user:RequestClientTargetGameObj(this, "select_task_item")
		return false
	end	
	
	if (orderInfo == nil) then return end

	if (crafter ~= user) then
		user:SystemMessage("This item was not created by you.","info")
		user:RequestClientTargetGameObj(this, "select_task_item")
	 	return false
	end
	
	local targetMaterial = target:GetObjVar("Material")
	local targetTemplate = target:GetCreationTemplateId()

	--If object is packed, get the unpacked template
	local weight = GetTemplateObjectProperty(orderRecipe.CraftingTemplateFile,"Weight")
	if (weight == -1) then
		if (target:HasObjVar("UnpackedTemplate")) then 
			targetTemplate = target:GetObjVar("UnpackedTemplate")
		else
			return false
		end
	end

	--If target container has objects, return false
	if (target:IsContainer()) then
		
		-- Lockboxes are allowed to have their key, and only their key in them!
		if ( (
				orderInfo.Recipe == "Lockbox" and 
				(
					CountObjectsInContainer(target, true) > 1 or
					(
						CountObjectsInContainer(target, true) == 1 and 
						FindItemInContainerByTemplate( target, "key" ) == nil
					)
				)				
			) or 
			(orderInfo.Recipe ~= "Lockbox" and CountObjectsInContainer(target, true) > 0 ) ) then
			user:SystemMessage("Contents of container must be removed before adding to craft order.","info")
			return false
		end
	end

	if ((targetTemplate ~= orderRecipe.CraftingTemplateFile or targetMaterial ~= orderInfo.Material)) then
		user:SystemMessage("That is not the correct item for this order.","info")
		user:RequestClientTargetGameObj(this, "select_task_item")
		return false
	end

	if (user ~= GetOrderOwner(user,this)) then
		user:SystemMessage("This crafting order was not issued to you.","info")
		user:RequestClientTargetGameObj(this, "select_task_item")
	 	return false
	end

	return true
end

function AddToOrder(target, user)
	local stackCount = target:GetObjVar("StackCount")
	local currentAmount = this:GetObjVar("CurrentAmount")
	local amountToAdd = 0

	local orderInfo = this:GetObjVar("OrderInfo")

	if (currentAmount == orderInfo.Amount) then return end

	if (stackCount == nil) then
		amountToAdd = 1
	else
		amountToAdd = orderInfo.Amount - currentAmount
		if (amountToAdd > stackCount) then
			amountToAdd = stackCount
		end
	end

	if (stackCount ~= nil) then
		user:SystemMessage("Added "..amountToAdd.." "..target:GetName().." to crafting order.","info")
		RequestConsumeResource(user,target:GetObjVar("ResourceType"), amountToAdd ,"ConsumeResourceResponse",this, target)
	else
		user:SystemMessage("Added "..target:GetName().." to crafting order.","info")
		target:Destroy()
	end

	if (target:IsContainer()) then
		target:SendCloseContainer(user)
	end

	this:SetObjVar("CurrentAmount", (currentAmount + amountToAdd))
end

function CompleteOrder(user)
	local orderInfo = this:GetObjVar("OrderInfo")
	if ( orderInfo and orderInfo.Amount ) then
		this:SetObjVar("CurrentAmount", orderInfo.Amount)
		SetItemTooltip(this)
	end
	RemoveUseCase(this, "Add item to order")
	SetDefaultInteraction(this, "Read order")
	this:SetObjVar("OrderComplete", true)
	user:PlayObjectSound("event:/ui/skill_gain", false)
	user:SystemMessage("Crafting order complete. Turn in to craftsman for reward.", "info")
end

RegisterEventHandler(EventType.Message,"UseObject",function (user,useType)
    if (not useType == "Add item to order" or not useType == "Read order") then return end

    	local orderInfo = this:GetObjVar("OrderInfo")

    	if (useType == "Add item to order") then

    		if (this:TopmostContainer() ~= user) then
				user:SystemMessage("Crafting order must be in your backpack.","info")
				return
			end

    		user:RequestClientTargetGameObj(this, "select_task_item")
    	end

    	if (useType == "Read order") then
    		local orderSkill = this:GetObjVar("OrderSkill")
    		local orderRecipe = AllRecipes[orderSkill][orderInfo.Recipe]
    		local orderOwner = GetOrderOwner(user,this)
    		if (orderOwner ~= nil) then
    			user:SystemMessage("The order was issued to "..orderOwner:GetName().. " and calls for "..orderInfo.Amount.." "..GetItemNameFromOrderInfo(orderRecipe),"info")
    		else
    			user:SystemMessage("This order calls for "..orderInfo.Amount.." "..GetItemNameFromOrderInfo(orderRecipe),"info")
    		end
    	end
end)

RegisterEventHandler(EventType.Message, "ConsumeResourceResponse", 
    function (success,transactionId,user, target)
    	if (success) then
    	end
	end)
	
RegisterEventHandler(EventType.Message, "CompleteOrder", 
    function(user)
    	CompleteOrder(user)
    end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "select_task_item", HandleSelectedTaskItem)

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()
	InitCraftingOrder()
	end)

--MJT: Moved all information about the craft order to the OrderInfo objVar
--Updates pre-patch orders
RegisterEventHandler(EventType.LoadedFromBackup, "", function()
	PatchCraftOrder()
	InitCraftingOrder()
	end)

function PatchCraftOrder()
	local orderInfo = {}
	local craftOrderSkill = this:GetObjVar("OrderSkill")
	local oldOrderInfo = this:GetObjVar("OrderInfo") or {}
	local oldOrderRecipeTable = this:GetObjVar("OrderRecipe")

	if (this:HasObjVar("OrderAmount")) then
		orderInfo.Amount = this:GetObjVar("OrderAmount")
	elseif ( oldOrderInfo.Amount ) then
		orderInfo.Amount = oldOrderInfo.Amount
	end

	if ( oldOrderInfo.Recipe ) then
		orderInfo.Recipe = oldOrderInfo.Recipe
	elseif (this:HasObjVar("OrderRecipe")) then
		for recipeKey, recipeTable in pairs(AllRecipes[craftOrderSkill]) do
			if (recipeTable.CraftingTemplateFile == oldOrderRecipeTable.CraftingTemplateFile) then
				orderInfo.Recipe = recipeKey
			end
		end
	end

	if ( oldOrderInfo.Material ) then
		orderInfo.Material = oldOrderInfo.Material
	end

	--DebugMessage(DumpTable(orderInfo))
	this:DelObjVar("OrderRecipe")
	this:SetObjVar("OrderInfo", orderInfo)
end