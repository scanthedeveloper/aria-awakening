
_DoMobileDeath = DoMobileDeath
function DoMobileDeath(damager)
	Verbose("PlayerDeath", "DoMobileDeath", damager, this)
    if ( this:HasTimer("TeleportDelay") ) then
        SetCurHealth(this,0)
        return -- prevent death midst region transfer
    end
    -- mark us as dead right away
    this:ScheduleTimerDelay(TimeSpan.FromSeconds(5), "RecentlyDied")

    -- perform normal mobile death
	_DoMobileDeath(damager)

	if ( damager ~= nil and damager ~= this and damager:IsPlayer() ) then
		damager:SystemMessage("You have vanquished " .. this:GetName() .. "!", "info")
	end
    
    -- perform player mobile death
	PlayerDeathStart(this)
end

RegisterEventHandler(EventType.LoadedFromBackup, "", function ( ... )
	if ( IsDead(this) and not this:HasObjVar("OverrideDeath") ) then
		if ( not this:HasObjVar("DeathShrineClear") and PlayerResurrectFromBackup(this) ) then return end

		if ( this:HasObjVar("IsGhost") ) then
			AstralPlane.Join(this, true)
		end

		PlayerCorpseReleaseDialog(this)
		if ( this:GetSharedObjectProperty("IsDead") == true ) then
			this:SetMobileFrozen(true,true)
		end
		CallFunctionDelayed(TimeSpan.FromSeconds(0.5), function ()
			this:PlayLocalEffect(this,"GrayScreenEffect")
			this:PlayMusic("Death")
		end)
	end
end)

RegisterEventHandler(EventType.Timer, "AutoCorpseRelease", function()
	PlayerReleaseCorpse(this)
end)

--- in the event a player releases and ends up in corpse range, re-show the self res dialog
RegisterEventHandler(EventType.Timer, "RecentlyReleased", function()
	local corpse = this:GetObjVar("CorpseObject")
	if ( corpse and corpse:IsValid() ) then
		corpse:SendMessage("SelfResDialog")
	end
end)

--- handle resurrecting a player that's in a remote region
RegisterEventHandler(EventType.Message, "RemoteResurrect", function(address, location, resurrector, force)
	if ( not address or not location ) then return end

	CorpseResurrectDialog(this, resurrector, force, function()
		-- mark res on backup load
		SetPlayerResurrectFromBackup(this, resurrector)
		-- transfer regions
		this:TransferRegionRequest(address, location)
	end)

end)

-- force them to transfer to closest shrine if they crashed/forced closed client on transfer
RegisterEventHandler(EventType.UserLogin,"", function(loginType)
	if ( loginType == "Connect" ) then
		if ( this:HasObjVar("IsGhost") and this:HasObjVar("DeathShrineClear") ) then
			TeleportToClosestShrine(this)
		end
    end
end)