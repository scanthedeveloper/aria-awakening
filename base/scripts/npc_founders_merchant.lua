require 'npc_founders_vip_merchant'

CanBuyItem = function (buyer,item)
    return IsFounder(buyer) or HasPremiumSubscription(buyer)
end
CanUseNPC = CanBuyItem
