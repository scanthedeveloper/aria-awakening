--- these variables are used by the summoning table system
local SummonCategoryKey = "DemonologySkill"
local SummonMobTableKey = "Daemon"
local SummonDurationBase = 45

--- these table entries are the base level of the summoned skeleton
local SummonedTemplates = {}
SummonedTemplates[1] = "summoned_daemon" -- rank 1
SummonedTemplates[2] = "summoned_daemon" -- rank 2
SummonedTemplates[3] = "summoned_daemon" -- rank 3
SummonedTemplates[4] = "summoned_daemon" -- rank 4
SummonedTemplates[5] = "summoned_daemon" -- rank 5

--- standard citadel check for validation of the target
-- @pararm targetLoc - the location where the player targeted to summon this skeleton
local function ValidateSpellTarget(targetLoc)
	-- is this location passable
	if( not(IsPassable(targetLoc)) ) then
		this:SystemMessage("[$2614]","info")
		return false
	end
	-- do we have line of sight
	if not(this:HasLineOfSightToLoc(targetLoc,ServerSettings.Combat.LOSEyeLevel)) then
		this:SystemMessage("[$2615]","info")
		return false
	end
	return true
end

--- this function ends the summoning process and removes this module
function EndEffect()
	this:DelModule("sp_summon_skeleton_archer")
end

--- this handles the event message sent to this player to handle the target result
-- @param targetLoc - the location the player targeted sent by the engine
-- @param skipRemove - unused in this function
function HandleSpellTargetResult(targetLoc, skipRemove)
	-- validate the spell target location
	if not(ValidateSpellTarget(targetLoc)) then
		EndEffect()
		return
	end
	-- apply a void pillar effect at the summon target location
	PlayEffectAtLoc("PrimedVoid2",targetLoc, 3)
	PlayEffectAtLoc("CastVoid2",targetLoc, 5)
	PlayEffectAtLoc("CastVoid2",targetLoc, 5)
	PlayEffectAtLoc("CastVoid2",targetLoc, 5)
	PlayEffectAtLoc("CastVoid2",targetLoc, 5)
	PlayEffectAtLoc("CastVoid2",targetLoc, 5)
	PlayEffectAtLoc("CastVoid2",targetLoc, 5)
	PlayEffectAtLoc("PinkFlowersWindEffect",targetLoc, 5)
	-- if we can create a summon, let us
	if(CanAddSummon(this,SummonCategoryKey,SummonMobTableKey)) then
		local i = 1
        local spawnAmount = 1
		while i <= spawnAmount do
			CreateObj(SelectTamplate(), targetLoc, "DaemonCreated")
			i = i + 1
		end
	end
end

--- this function will determine the template to spawn based on the
--- players necromancy skill and summoning skill
function SelectTamplate()
	local necromancySkill = GetSkillLevel(this,"DemonologySkill")	
	local summoningSkill = GetSkillLevel(this,"SummoningSkill")

	local necroSkillCheck = GetSkillLevel(this,"DemonologySkill") or 0
	local summoningSkillCheck = GetSkillLevel(this,"SummoningSkill") or 0

	CheckSkillChance(user,"DemonologySkill", (100 - necroSkillCheck) / 100)
	CheckSkillChance(user,"SummoningSkill", (100 - summoningSkillCheck) / 100)
	
	-- choice starts as rank 1
	local templateChoice = 1
	-- set the rank to 2 if we meet the requirements
	if(necromancySkill > 49.9) then templateChoice = 2 end
	-- set the rank to 3 if we meet the requirements	
	if(necromancySkill > 79.9) then templateChoice = 3 end
	-- the summoning skill can add up to 2 in rank at 50 and 100 skill
	templateChoice = templateChoice + math.floor(summoningSkill/50)
	-- lets cap the template choice at 5 since our table only contains 5 entries
	if(templateChoice > 5) then templateChoice = 5 end
	-- return the correct template to spawn
	return SummonedTemplates[templateChoice]
end

--- this handles the event creation of the summoned object
-- @param success - true if the monster is created, false if not
-- @param objref - the phantom object that was created
function DaemonCreated(success, objref)
	if(success) then
		local necromancySkill = GetSkillLevel(this,"DemonologySkill")
		-- lets add addition duration to the summoned based on skill level
		-- duration bonus is between 0-50 based on necromancy skill
		local durationBonus = math.floor(necromancySkill/10)
		local duration = TimeSpan.FromSeconds(SummonDurationBase+durationBonus)
		
		-- add summon information to object
		objref:SetObjVar("Summoned",true)
		objref:SetObjVar("SummonMaster",this)
		-- attach the delete timer for summoned objects
		objref:AddModule("summoning.summon_delete_timer",{Duration=duration})
		-- key information for the summoning system
		objref:SetObjVar("CategoryKey",SummonCategoryKey)
		objref:SetObjVar("MobileTableKey",SummonMobTableKey)
		-- insert this mobile in to the players summon table object variable
		PlayerAddSummon(this,SummonCategoryKey,SummonMobTableKey,objref)
		-- play sound effect when summoned
		this:PlayObjectSound("event:/magic/air/magic_air_cast_air",false,0.7)		
		-- now that everything is done, lets end this effect
		--SetCreatureAsPet(objref, this)
		EndEffect()		
	end
end

-- fires when the spell target result message is sent to this module
RegisterEventHandler(EventType.Message,"SummonDaemonSpellTargetResult", HandleSpellTargetResult)

-- fires when the create object function is finished
RegisterEventHandler(EventType.CreatedObject, "DaemonCreated", DaemonCreated)
