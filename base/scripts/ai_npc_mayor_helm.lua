require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false
AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true


--[[ QUEST ]]
AI.QuestsWelcomeText ="Greetings traveler, do you have a moment to talk about the benefits of becoming a citizen of Helm?"

    AI.QuestAvailableMessages = {"I have a matter I wish to talk over with you."}
    
    AI.QuestStepsInvolvedIn = {
        {"JoinTownshipHelm", 1},
        {"LeaveTownshipHelm", 1},
    }
    


function Dialog.OpenGreetingDialog(user)
        local text = "Greetings, it is a fine day today. What can I do for you?"
        local response = {}

        response[1] = {}
        response[1].text = "Who are you?"
        response[1].handle = "Who"
        
        response[2] = {}
        response[2].text = "Goodbye."
        response[2].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
end


function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,"I am the mayor here in Helm. I also am the proprietor of the tinker shop -- Oh, and I own the blacksmith too. But mainly, I'm the mayor.")
end