require 'incl_fishing'

MIN_FISHING_TIME = 6
MAX_FISHING_TIME = 30
MIN_REELING_TIME = 3
MAX_REELING_TIME = 20
FISHING_RANGE = 25
FISH_SCHOOL_DIFFICULTY_BONUS = 20
mTargetLoc = nil
mFoundSosTreasureMap = nil
mFishSchool = nil

--[[
CHANCE_TO_FISH_SOS = { -- percent based, ordered lowest to highest chance
    0.005, --sos_map
    0.010, --sos_map_1
    0.015, --sos_map_2
    0.020, --sos_map_3
    0.025 --sos_map_4
}
]]

SOS_CHANCE = {
    NumItems = 1,
    LootItems = {
        { Chance = 2, Template = "sos_map", Unique = true, Skill = 20, Level = 1 },
        { Chance = 1, Template = "sos_map_1", Unique = true, Skill = 40, Level = 2 },
        { Chance = 0.5, Template = "sos_map_2", Unique = true, Skill = 60, Level = 3 },
        { Chance = 0.25, Template = "sos_map_3", Unique = true, Skill = 70, Level = 4 },
        { Chance = 0.125, Template = "sos_map_4", Unique = true, Skill = 90, Level = 5 },
    }
}

-- NOTE: this assumes the fish controller is on the map controller object!
fishController = nil
function GetFishController()
    if( fishController == nil or not fishController:IsValid() ) then
        fishController = FindObjectWithTag("FishController")
    end

    return fishController
end

function RemoveAllTimers()
    this:RemoveTimer("ReelTimer")
    this:RemoveTimer("ReelSuccessTimer")
    this:RemoveTimer("fishingTimer")
    this:RemoveTimer("CheckFishTimer")
end

function CleanUp()
    this:PlayAnimation("idle")
    this:StopObjectSound("event:/character/skills/gathering_skills/fishing/fishing_reel", false)
    RemoveAllTimers()
    this:DelModule(GetCurrentModule())
end

function ValidateUse(user)
    if( user == nil or not(user:IsValid()) ) then
        return false
    end

    local rod = this:GetEquippedObject("RightHand")
    if( not(rod) or rod:GetObjVar("WeaponType") ~= "FishingRod" ) then
        user:SystemMessage("[FA0C0C] You need to equip a fishing rod to fish!","info")
        return false
    end

    if (user:HasTimer("fishingTimer")) then
        user:SystemMessage("You are already fishing.","info")
        return false
    end

    if (user:HasTimer("ReelSuccessTimer")) then
        user:SystemMessage("You are reeling in a fish!","info")
        return false
    end

    if ( Plot.GetAtLoc(user:GetLoc()) ) then
        user:SystemMessage("You don't have a permit to fish on this property.","info")
        return false
    end

    return true
end

function GetFishSizeDisplayString(size)
    if (size == 5) then
        return "[$1641]"    
    elseif (size == 4) then
        return "It's gigantic! It's a great catch!"
    elseif (size == 3) then
        return "It's huge! It's a good catch!"
    elseif (size == 2) then
        return "It's healthy looking!"
    elseif (size == 1) then
        return "It's not very big..."    
    end
end

function GetFishTitleString(size,player)
    --DebugMessage(size,this)
    if (size == 5) then
        return "[D4CC74]Legendary"
    elseif (size == 4) then        
        return "[D68A3E]Gigantic"
    elseif (size == 3) then
        return "[D6623E]Huge"
    elseif (size == 2) then
        return "[8B95E0]Large"
    elseif (size == 1) then
        return "[C8CBE6]Small"
    end
end

function CheckFindSos(player)
    local fishingLevel = GetSkillLevel(player,"FishingSkill")
    
    local availableItems = FilterLootItemsByChance(SOS_CHANCE.LootItems)
    if( availableItems[1] ) then
        local item = availableItems[1]
        if( fishingLevel >= item.Skill ) then
            return item.Level
        end
    end
    return false
end

function CheckFindSosTreasure(player)
    local backpackObj = player:GetEquippedObject("Backpack")
    if( backpackObj == nil ) then return false end

    local packObjects = backpackObj:GetContainedObjects()  
    for index, packObj in pairs(packObjects) do 
        if( packObj:HasModule("sos_map") ) then
            local mapLocation = packObj:GetObjVar("MapLocation")
            if( mapLocation ~= nil and mapLocation:Distance(mTargetLoc) < 80 ) then
                return packObj
            end
        end
    end
    return nil
end

function DoFish(targetLoc)        
    if(targetLoc == nil) then CleanUp() return end

    if not( ValidateUse(this) ) then CleanUp() return end

    local wellItem = FindItemInContainerRecursive(backpackObj,
        function(item)            
            return item:GetObjVar("UnpackedTemplate") == "well"
        end)

    if( this:GetLoc():Distance(targetLoc) >= FISHING_RANGE ) then
        this:SystemMessage("That location is too far away.","info")
        CleanUp()
        return
    end

    --if (not this:HasLineOfSightToLoc(targetLoc,ServerSettings.Combat.LOSEyeLevel)) then
    --    this:SystemMessage("You can't see that location.","info")
     --   return
    --end

    if (not IsLocInRegion(targetLoc,"Water")) then
        this:SystemMessage("[FF0000]You need to cast into water to fish![-]","info")
        CleanUp()
        return
    end

    mTargetLoc = targetLoc
    this:SetFacing(this:GetLoc():YAngleTo(targetLoc))
    this:PlayAnimation("fishcast")
    this:PlayObjectSound("event:/character/skills/gathering_skills/fishing/fishing_cast")
    CallFunctionDelayed(TimeSpan.FromSeconds(2.7),function ( ... )
            PlayEffectAtLoc("WaterSplashEffect",targetLoc)
        end)

    -- first see if we fished on a fish school    
    mFishSchool = GetNearbySchoolOfFish(targetLoc)
    if(mFishSchool) then
        local fishName = mFishSchool:GetObjVar("SchoolOfFish")
        -- DAB TODO REQUEST FISH FROM SCHOOL
        mFish = {
            Template = AllFish[fishName].Template,
            Size = GetFishSize(),
            DisplayName = AllFish[fishName].DisplayName,
            MinSkill = math.max(0,AllFish[fishName].MinSkill - FISH_SCHOOL_DIFFICULTY_BONUS),
        }
        if not( mFishSchool:HasModule("simple_fish_node")) then
            StartFishingTimer()
            return
        end
    end

    -- next check to see if we have a treasure map for this location
    mFoundSosTreasureMap = CheckFindSosTreasure(this)
    if(mFoundSosTreasureMap) then
        StartFishingTimer()
        return
    end

    -- next see if we fish up a treasure map
    mFoundSos = CheckFindSos(this)
    if(mFoundSos) then
        StartFishingTimer()
        return
    end

    -- finally try to get a fish from the fish resource controller
    local fishController = GetFishController()
    if (fishController and mFishSchool == nil) then
        -- request a fish from the fish controller
        GetFishController():SendMessage("RequestFishResource",this,targetLoc)
        return
    end

    -- DAB TODO: We could have a default fish table for maps with no fish controller
    StartFishingTimer()    
end

RegisterEventHandler(EventType.Message,"RequestFishResourceResponse",
    function (fish)
        mFish = fish
        StartFishingTimer()
    end)

function StartFishingTimer()    
        --DebugMessage("--StartFishingTimer",DumpTable(mFish))
        -- we check to see if we catch the fish here so its one single skill check
        -- then we set the fish and reel timers based on the difficult and weight of the fish

        RequestMobileMod( this, this, {"HarvestDelayReductionTimes", "FishingDelayReductionPlus", "FishingDelayReductionTimes"},
        function(MobileMod) 

            local bonusHarvestDelay = fishingRod:GetObjVar("BonusHarvestDelay") or 0

            MIN_FISHING_TIME = math.max(0.5, (bonusHarvestDelay + MIN_FISHING_TIME + GetMobileMod(MobileMod.FishingDelayReductionPlus)) * ( 1 - GetMobileMod(MobileMod.HarvestDelayReductionTimes, 0) / 100 ) * ( 1 - GetMobileMod(MobileMod.FishingDelayReductionTimes, 0) / 100 ) )
            MAX_FISHING_TIME = math.max(0.5, (bonusHarvestDelay + MAX_FISHING_TIME + GetMobileMod(MobileMod.FishingDelayReductionPlus)) * ( 1 - GetMobileMod(MobileMod.HarvestDelayReductionTimes, 0) / 100 ) * ( 1 - GetMobileMod(MobileMod.FishingDelayReductionTimes, 0) / 100 ) )
            MIN_REELING_TIME = math.max(0.5, (bonusHarvestDelay + MIN_REELING_TIME + GetMobileMod(MobileMod.FishingDelayReductionPlus)) * ( 1 - GetMobileMod(MobileMod.HarvestDelayReductionTimes, 0) / 100 ) * ( 1 - GetMobileMod(MobileMod.FishingDelayReductionTimes, 0) / 100 ) )
            MAX_REELING_TIME = math.max(0.5, (bonusHarvestDelay + MAX_REELING_TIME + GetMobileMod(MobileMod.FishingDelayReductionPlus)) * ( 1 - GetMobileMod(MobileMod.HarvestDelayReductionTimes, 0) / 100 ) * ( 1 - GetMobileMod(MobileMod.FishingDelayReductionTimes, 0) / 100 ) )
            
            local fishHookTime = MIN_FISHING_TIME
            --DebugMessage("1 FishHookTime: " .. fishHookTime)

            mFishHookSuccess = false
            mFishCatchSuccess = false
            if(mFish) then
                if( GetFishSuccessRoll(this,mFish) ) then
                    mFishHookSuccess = true
                    mFishCatchSuccess = true
                end 
                -- additional hook time is a factor of MAX TIME - MIN TIME based on fish difficulty
                fishHookTime = MIN_FISHING_TIME + (mFish.MinSkill / 100 * (MAX_FISHING_TIME - MIN_FISHING_TIME))
            elseif(mFoundSos or mFoundSosTreasureMap) then
                mFishHookSuccess = true
                mFishCatchSuccess = true
                fishHookTime = MIN_FISHING_TIME + (50 / 100 * (MAX_FISHING_TIME - MIN_FISHING_TIME))
            end

            --DebugMessage("2 FishHookTime: " .. fishHookTime)

            CallFunctionDelayed(TimeSpan.FromSeconds(3),function() this:PlayAnimation("fishidle") end)

            this:ScheduleTimerDelay(TimeSpan.FromSeconds(fishHookTime),"fishingTimer") 

        end) 

end

RegisterEventHandler(EventType.Timer,"fishingTimer",
    function()
        --DebugMessage("--fishingTimer")
        if not(mFish) and not(mFoundSos) and not(mFoundSosTreasureMap) then
            this:SystemMessage("There doesn't seem to be any fish nearby.","info")
            CleanUp()
        elseif(mFishHookSuccess) then
            StartReeling()
        else
            this:SystemMessage("You haven't found any fish.","info")
            CheckSkillChance(this,"FishingSkill")
            CleanUp()
        end
    end)

function GetRod()
    local rodObj = this:GetEquippedObject("RightHand")
    local isRod = rodObj and GetWeaponType(rodObj) == "FishingRod" 

    if(isRod) then
        return rodObj
    end
end
fishingRod = GetRod()

function StartReeling()
    RemoveAllTimers()

    local reelTime = MIN_REELING_TIME
    if(mFish) then
        local maxSize = #ServerSettings.Resources.Fish.SizeChance
        reelTime = MIN_REELING_TIME + (mFish.Size / maxSize * (MAX_REELING_TIME - MIN_REELING_TIME))
    end

    if not(fishingRod) then
        this:SystemMessage("You quietly slip your fishing rod back into your pack.","info")
        CleanUp()
    else
        this:NpcSpeechToUser("Something is hooked on the line!",this,"info")
        this:PlayObjectSound("event:/character/skills/gathering_skills/fishing/fishing_reel")
        this:PlayObjectSound("event:/character/skills/gathering_skills/fishing/fishing_jump")    
        this:PlayAnimation("fishhook")
        
        this:ScheduleTimerDelay(TimeSpan.FromSeconds(reelTime),"ReelTimer")
    end
end

RegisterEventHandler(EventType.Timer,"ReelTimer",
    function()
        if not(fishingRod) then
            this:SystemMessage("You quietly slip your fishing rod back into your pack.","info")
            CleanUp()
        else
            if ( Success(ServerSettings.Durability.Chance.OnToolUse) ) then
                AdjustDurability(fishingRod, -1)
            end

            if(mFoundSos) then
                local mapLevel = mFoundSos - 1
                local mapTemplate = "sos_map"
                if (mapLevel > 0) then
                    mapTemplate = "sos_map_"..mapLevel
                end
                CreateObjInBackpackOrAtLocation(this, mapTemplate)
                this:SystemMessage("You found a treasure map!", "info")
                Quests.SendQuestEventMessage( this, "Gathering", { "Fishing", mapTemplate }, 1 )
                CleanUp() 
                return
            end

            if(mFoundSosTreasureMap and mFoundSosTreasureMap:IsValid()) then
                mFoundSosTreasureMap:SendMessage("FoundSosTreasure", this)
                CleanUp() 
                return
            end

            if (not(mFish) or not(mFishCatchSuccess)) then
                this:SystemMessage("The fish got away!","info")
                CheckSkillChance(this,"FishingSkill")
                CleanUp()
                return
            else
                --deplete from specific resource node instead of fish resource controller
                if (mFishSchool and mFishSchool:HasModule("simple_fish_node")) then
                    mFishSchool:SendMessage("RequestFishNodeResource", this, 1)
                end
            end            

            local backpackObj = this:GetEquippedObject("Backpack")
            if( backpackObj == nil ) then 
                this:SystemMessage("[D7D700]You have no backpack. You release the fish back into the water.[-]","info")
                CleanUp() 
                return 
            end

            this:RemoveTimer("FishingAnimationTimer")
            sizeString = GetFishSizeDisplayString(mFish.Size)
            local messageStr = "You caught a "..mFish.DisplayName.."! "..sizeString ..""
            this:NpcSpeechToUser(messageStr,this,"info")
            Quests.SendQuestEventMessage( this, "Gathering", { "Fishing", mFish.Template }, 1 )

            CheckSkillChance(this,"FishingSkill")
            --DebugMessage(fish.Size.." is the fish size")
            
            IncrementFishCaught(mFish.Size)

            local templateData = GetTemplateData(mFish.Template)
            local templateWeight = templateData.SharedObjectProperties.Weight
            local templateScale = templateData.ScaleModifier
            local templateResourceType = templateData.ObjectVariables.ResourceType or "Fish"

            local fishResourceType = templateResourceType .. tostring(mFish.Size)

            local stackSuccess, stackObj = TryAddToStack(fishResourceType,backpackObj,1)
            if not( stackSuccess ) then
                local fishWeight = ServerSettings.Resources.Fish.SizeWeights[mFish.Size]*templateWeight
                local fishScale = templateScale * ServerSettings.Resources.Fish.SizeScales[mFish.Size]

                local dropPosition = GetRandomDropPosition(backpackObj)
                if (not CanAddWeightToContainer(backpackObj,fishWeight)) then
                    this:SystemMessage("[D7D700]Your backpack is full. You release the fish back into the water.[-]","info")
                    CleanUp()
                else
                    CreateObjExtended(mFish.Template,backpackObj,dropPosition,Loc(0,0,0),Loc(fishScale,fishScale,fishScale),"created_fish",fishWeight,mFish.Size,fishResourceType)
                end
            else
                CleanUp()
            end

        end
    end)

RegisterEventHandler(EventType.CreatedObject,"created_fish",function (success,objRef,fishWeight,fishSize,fishResourceType)
    --DebugMessage(1)
    if (success) then
        objRef:SetSharedObjectProperty("Weight",fishWeight)
        objRef:SetObjVar("ResourceType",fishResourceType)
        objRef:SetName(GetFishTitleString(fishSize).." "..StripColorFromString(objRef:GetName()).."[-]")
        if (objRef:TopmostContainer() == nil) then
            Decay(objRef)
        end
        SetItemTooltip(objRef)
    end
    CleanUp()
end)

function IncrementFishCaught(fishSize)
    local lifetimeStats = this:GetObjVar("LifetimePlayerStats")
    lifetimeStats.FishCaught = (lifetimeStats.FishCaught or 0) + 1
    this:SetObjVar("LifetimePlayerStats",lifetimeStats)

    CheckAchievementStatus(this, "Fishing", "FishingNumber", lifetimeStats.FishCaught)
    CheckAchievementStatus(this, "Fishing", "FishingSize", fishSize)
    CheckAchievementStatus(this, "Fishing", fishResourceType, 1)
end

RegisterEventHandler(EventType.ModuleAttached,"base_fishing_controller",
    function ()
        if(initializer ~= nil) then
            DoFish(initializer.TargetLoc)
            --TestFishAttempts( 1000 )
        end
    end)

RegisterEventHandler(EventType.StartMoving,"",function ( ... )
    this:NpcSpeechToUser("The line snaps!",this,"info")
    CleanUp()
end)

RegisterEventHandler(EventType.Message,"DamageInflicted",function ( ... )
    this:NpcSpeechToUser("The line snaps!",this,"info")
    CleanUp()
end)

RegisterEventHandler(EventType.Message, "RequestFishNodeResourceResponse", 
    function(success,user,resourceType,objRef,failReason)
        -- if successfully got a resource from the object
        if not ( success ) then
            if ( failReason == "Depleted" ) then
                user:SystemMessage("All resources depleted.", "info")
                CleanUp()
                return
            end
        end
        StartFishingTimer()
end)

function TestFishAttempts( numCasts )

    DebugMessage("Total Casts : ", numCasts)
    local totalSOS = 0
    local SOSFound = { 0,0,0,0,0 }

    for i=1, numCasts do 
        local index = CheckFindSos(this)
        if( index ) then
            SOSFound[index] = SOSFound[index] + 1
            totalSOS = totalSOS + 1
        end
        
        --DebugMessage( "itemIndex", itemIndex )

    end

    DebugMessage("SOS-TOTAL : ", totalSOS)
    DebugTable(SOSFound)
end