require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

function IntroDialog(user)
    Dialog.OpenGreetingDialog(user)
end

function Dialog.OpenGreetingDialog(user)
    text = "Traveler *crackle* you have arrived at dire times! The Monolith has become unstable ".. 
    "and the cultists of Jianna the Red have begun to awaken. I fear the Xor are losing ".. 
    "their control over the Monolith.  I am the one *crackle* who caused the Echo energy of ".. 
    "the Monolith to overflow in hopes of summoning a being such as yourself. Your assistance is needed."

    local int = 0
    response = {}

    int = #response + 1
    response[int] = {}
    response[int].text = "What are you?"
    response[int].handle = "WhatYou"

    int = #response + 1
    response[int] = {}
    response[int].text = "Echo energy?"
    response[int].handle = "WhatEcho"

    int = #response + 1
    response[int] = {}
    response[int].text = "How can I assist?"
    response[int].handle = "HowAssist"

    int = #response + 1
    response[int] = {}
    response[int].text = "Goodbye."
    response[int].handle = ""

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenWhatYouDialog(user)

    text = "I am The Keeper, one of the constructs created by the Xor, I was tasked with overseeing the " .. 
    "Monolith and ensuring the Echo is untainted by the filth of Jianna and her ilk.  A task I have faithfully " .. 
    "fulfilled until recently. But my strength is fading and the cultist have begun to reclaim the Monolith."

    local int = 0
    response = {}

    int = #response + 1
    response[int] = {}
    response[int].text = "I see."
    response[int].handle = "Greeting" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenWhatEchoDialog(user)

    text = "The Monolith was created by Jianna the Red to act as a condiut between this world and the Echo.  " .. 
    "The Echo is a ancient magic that can grant its wielder great power.  Unfortunately Jianna sought to harness the Echo " .. 
    "in an attempt to conquer the shattered worlds. For that reason the Xor overthrew her and the Monolith was sealed -- least " .. 
    "it be used by another with sinister intents."

    local int = 0
    response = {}

    int = #response + 1
    response[int] = {}
    response[int].text = "I see."
    response[int].handle = "Greeting" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenHowAssistDialog(user)

    text = "I am not longer able to keep the Monolith safe, I require a traveler such as yourself to help me " ..
    "secure it once more.  Navigate the Monolith's first floor, learn it's secrets and I shall be waiting for you " ..
    "on the second floor."

    local int = 0
    response = {}

    int = #response + 1
    response[int] = {}
    response[int].text = "I will meet you there."
    response[int].handle = ""

    NPCInteractionLongButton(text,this,user,"Responses",response)
end


OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)