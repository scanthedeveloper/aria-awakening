require 'teleporter'

-- store these so we can destroy them on teleport
skulls = {}

base_ActivateTeleporter = ActivateTeleporter
function ActivateTeleporter(user)	
	skulls = {}

	-- look for the skulls
	local backpack = user:GetEquippedObject("Backpack")
	local found = false
	if ( backpack ~= nil ) then		
		ForEachItemInContainerRecursive(backpack, function(item)
			if(item:GetCreationTemplateId() == "tree_skull") then
				skulls.Forest = item
			elseif(item:GetCreationTemplateId() == "cultist_skull") then
				skulls.Cultist = item
			elseif(item:GetCreationTemplateId() == "dragon_skull") then
				skulls.Fire = item
			end

			if( skulls.Forest ~= nil and skulls.Fire ~= nil and skulls.Cultist ~= nil ) then
				found = true
			end
			return not(found) -- keep
		end)
	end

	if not(found) then
		user:SystemMessage("[$3388]","info")
	end

	base_ActivateTeleporter(user)
end

base_DoTeleport = DoTeleport
function DoTeleport(user,targetLoc,destRegionAddress)
	-- if this is not set something wierd happened
	if( skulls.Forest ~= nil and skulls.Fire ~= nil and skulls.Cultist ~= nil ) then
		this:PlayEffect("DeathwaveEffect")
		skulls.Forest:Destroy()
		skulls.Fire:Destroy()
		skulls.Cultist:Destroy()
		base_DoTeleport(user,targetLoc,destRegionAddress)
	end
end