require 'incl_magic_sys'

PYROIMMOLATE_DAMAGE_RANGE = 3.5
--local ignoreView = true
PULSE_COST = -1
PULSE_SPEED = .1
mPulseID = 0
mTargetsIR = {}
--DebugMessage("PyroImmolate ATTACHY")
mUpkeep = .5
mActive = false
FLAME_AURA_MS_MOD = .8

function StartPyroImmolateEffect()
	--DebugMessage("Starting")
		--this:SetObjVar("Dangerous",true)
		if(mActive == false) then
			mActive = true
			this:SetObjVar("PyroImmolateBonusCastingOffset", -.5)
		else
			EndEffect()
			--DebugMessage("Ending It")
			return
		end
		if not(HasView("PyroImmolateView")) then AddView("PyroImmolateView",SearchMobileInRange(PYROIMMOLATE_DAMAGE_RANGE, true)) end
		local upkeepCost = GetSpellInformation("PyroImmolate", "upkeepCost")
		if(upkeepCost ~= nil) and (upkeepCost ~= 0) then
			mUpkeep = upkeepCost
			SetMobileMod(this, "ManaRegenPlus", "PyroImmolateUpkeep", upkeepCost)
		end
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"PyroImmolatePulse")
		--this:SetObjVar("PyroImmolateBonusCastingOffset", 1.2)
		--this:PlayObjectSound("event:/magic/fire/magic_fire_wall_of_fire", false)
			
		AddBuffIcon(this,"PyroImmolate","PyroImmolate","PyroImmolate","[$2606]",true,30)
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(25),"PyroImmolateRemove")
		--this:SendMessage("AddMoveSpeedEffectMessage", "sp_pyromancy_immolate", "Multiplier", FLAME_AURA_MS_MOD)
		this:PlayEffect("FlameAuraEffect", 5)
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(2), "FlameEffectLoop")
end

RegisterEventHandler(EventType.Timer, "FlameEffectLoop",
	function()

			this:PlayEffect("FlameAuraEffect", 5)
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(4), "FlameEffectLoop")
	
	end)
RegisterEventHandler(EventType.Message, "CompletionEffectsp_pyromancy_immolate",
	function()
		--DebugMessage("LAUNCHE MESSAGE)")
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1), "PyroImmolateLaunchTimer")
		StartPyroImmolateEffect()
	end)

function EndEffect()
		this:DelObjVar("PyroImmolateBonusCastingOffset")
		this:RemoveTimer("PyroImmolatePulse")
		this:RemoveTimer("PyroImmolateRemove")
		this:RemoveTimer("FlameEffectLoop")
		RemoveBuffIcon(this,"PyroImmolate")
		SetMobileMod(this, "ManaRegenPlus", "PyroImmolateUpkeep", nil)
		--this:SendMessage("RemoveMoveSpeedEffectMessage", "sp_pyromancy_immolate")
		DelView("PyroImmolateView")
		this:FireTimer("PyroImmolateCleanUpTimer")
		this:StopEffect("FlameAuraEffect")
end

RegisterEventHandler(EventType.EnterView, "PyroImmolateView", 
	function(damTarget)
		--DebugMessage("Enter Aura View:" .. damTarget:GetName())
			local myPulseId = mPulseID + 1
			if(myPulseId > 10) then myPulseId = 0 end
			mTargetsIR[damTarget] = myPulseId
	end)

RegisterEventHandler(EventType.Timer, "PyroImmolatePulse",
	function()
		--DebugMessage("Pulse " ..mPulseID)
		this:SendMessage("BreakInvisEffect", "Casting")
		local curMana = GetCurMana(this)
		if(curMana < (-1* PULSE_COST)) then
			EndEffect()
			return
		end
		local charge =false
		local hit = false
		mPulseID = mPulseID + 1
		if(mPulseID > 10) then mPulseID = 0 end
		hit = false
	for i,v in pairs(mTargetsIR) do
		--DebugMessage(i:GetName())
		if(mTargetsIR[i] == mPulseID) then
			if (not IsDead(i)) and (i:ContainedBy() == nil) then
				hit = true
				this:PlayEffect("PrimedFire2", 2)
				--DebugMessage("Hit")
				this:SendMessage("RequestMagicalAttack", "PyroImmolate",i, this, true)
				this:SendMessage("RequestMagicalAttack", "ImmolationIgnite",i, this, true)
			end
		end
	end
		if(hit) then
			--this:PlayObjectSound("event:/magic/fire/magic_fire_wall_of_fire", false)
			AdjustCurMana(this,PULSE_COST)
		end
	
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(PULSE_SPEED), "PyroImmolatePulse")
end)

RegisterEventHandler(EventType.LeaveView, "PyroImmolateView",
	function(objLeavin)
		mTargetsIR[objLeavin] = nil
end)

RegisterEventHandler(EventType.Message,"HasDiedMessage",
	function()
		EndEffect()
		this:StopEffect("FlameAuraEffect")
	end)
RegisterEventHandler(EventType.Timer,"PyroImmolateRemove",
	function()
		EndEffect()
	end)



RegisterEventHandler(EventType.Timer, "PyroImmolateCleanUpTimer",
	function()
		--this:DelObjVar("Dangerous")
		this:DelModule("sp_pyromancy_immolate")
		this:StopEffect("FlameAuraEffect")
		end)