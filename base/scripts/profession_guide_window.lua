function Init()
    UpdateWindow()
end

function CleanUp()
    this:CloseDynamicWindow("ProfessionGuideWindow")
    this:DelModule("profession_guide_window")
end

function UpdateWindow()
    local dynWindow = DynamicWindow("ProfessionGuideWindow", "Profession Guide",974,746,0,0,"Parchment")

    local professionList = {}
    for profName,profInfo in pairs(Professions) do
        table.insert(professionList,{Name = profName, Info = profInfo})
    end

    table.sort(professionList,function(a,b) 
            return (a.Info.DisplayName or a.Name) < (b.Info.DisplayName or b.Name)
        end)

    local scrollWindow = ScrollWindow(0,10,950,224*3,224)
    local showDivider = false
    local lineIndex = 1
    local lockedProfs = {}
    for i,item in pairs(professionList) do
        if(ProfessionsHelpers.IsProfessionUnlocked(this,item.Name)) then
            AddProfessionElement(lineIndex,item.Name,item.Info,scrollWindow)        
            showDivider = true
            lineIndex = lineIndex + 1
        else
            table.insert(lockedProfs,item)
        end 
    end

    local rowIndex = 1
    local scrollElement = nil
    for i,item in pairs(lockedProfs) do        
        if not(ProfessionsHelpers.IsProfessionUnlocked(this,item.Name)) then
            if(rowIndex == 1) then
                scrollElement = ScrollElement()
                if(i == 1 and showDivider) then
                    scrollElement:AddImage(20, 12, "FancyDivider",880,0,"Sliced")
                end
            end

            AddLockedProfession(188*(rowIndex-1)+40,item.Name,item.Info,scrollElement)

            if(rowIndex == 5) then
                rowIndex = 1
                scrollWindow:Add(scrollElement)
            else
                rowIndex = rowIndex + 1
            end
        end        
    end

    -- add the last set 
    if(rowIndex ~= 1) then
        scrollWindow:Add(scrollElement)
    end

    dynWindow:AddScrollWindow(scrollWindow)
    this:OpenDynamicWindow(dynWindow, this)
end

function BuildLockedTooltip(profName,taskIndex)
    local skillAmount = ProfessionSkillTiers[taskIndex]
    local profInfo = Professions[profName]


    if not(ProfessionsHelpers.IsTaskAvailable(profName,taskIndex)) then
        return "Coming Soon"
    end

    tooltip = "Requires:\n\n"
    
    if(profInfo.RequiredProfession ) then--and not(ProfessionsHelpers.IsTaskCompleted(this,profInfo.RequiredProfession,taskIndex))
        local reqProfInfo = Professions[profInfo.RequiredProfession]
        tooltip = tooltip .. TaskIndexTitles[taskIndex] .. " " .. (reqProfInfo.DisplayName or profInfo.RequiredProfession) .. "\n"
    end

    --if not(ProfessionsHelpers.IsPreviousTaskComplete(this,profName,taskIndex)) then
    local previousTaskIndex = taskIndex - 1
    if ( previousTaskIndex and previousTaskIndex > 0 ) then
        tooltip = tooltip .. TaskIndexTitles[previousTaskIndex] .. " " .. (profInfo.DisplayName or profName) .. "\n"
    end
    --end

    for i, skillReq in pairs(profInfo.Skills) do        
        if(skillReq == "Weapon") then
            --[[
            local found = false
            for i,skill in pairs(WeaponSkills) do
                local skillLevel = GetSkillLevel(this,skill)
                if(skillLevel >= skillAmount) then
                    found = true
                end
            end
            if not(found) then
            ]]
                tooltip = tooltip .. tostring(skillAmount) .. " Weapon Skill\n"
            --end
        elseif(skillReq == "Spell") then
            --[[
            local found = false
            for i,skill in pairs(SpellSkills) do
                local skillLevel = GetSkillLevel(this,skill)
                if(skillLevel >= skillAmount) then
                    found = true
                end
            end
            if not(found) then
            ]]
                tooltip = tooltip .. tostring(skillAmount) .. " Spell Skill\n"
            --end
        else
            local skill = skillReq .. "Skill"
            --[[
            local skillLevel = GetSkillLevel(this,skill)
            if(skillLevel < skillAmount) then
                ]]
                tooltip = tooltip .. tostring(skillAmount) .. " " .. GetSkillDisplayName(skill) .. "\n"
            --end
        end
    end

    return StripTrailingNewline(tooltip)
end

function AddLockedProfession(xOffset,profName,profInfo,scrollElement)
    scrollElement:AddLabel(xOffset+50,28,"[43240f]"..(profInfo.DisplayName or profName).."[-]",0,0,33,"center",false,false,"Kingthings_Dynamic")        
    local iconName = "Profession_"..profName
    scrollElement:AddImage(xOffset , 60, iconName,100,100)
    if(ProfessionsHelpers.IsTaskActive(this,profName,0)) then
        scrollElement:AddButton(xOffset-24,170,"Stop|"..profName.."|0","Stop Tracking",157,27,"","",false,"ParchmentButton")
    elseif (ProfessionsHelpers.IsTaskCancelled(this, profName, 0) and ProfessionsHelpers.CanResumeLaterStep(this, profName)) then
        scrollElement:AddButton(xOffset-24,170,"Resume|"..profName.."|0","Resume Tracking", 157,27,"","",false,"ParchmentButton")
    else
        scrollElement:AddButton(xOffset-24,170,"Train|"..profName,"Train Profession",157,27,"","",false,"ParchmentButton")
    end
    scrollElement:AddImage(xOffset+68, 130, "lock_large",25,29)
  

    scrollElement:AddButton(xOffset,60,"","",100,100,BuildLockedTooltip(profName,1),"",false,"Invisible") 

    if(ProfessionsHelpers.HasSkillRequirement(this,profName,1)) then
        scrollElement:AddButton(xOffset-24,200,"Skip|"..profName,"Skip Introduction",157,27,"","",false,"ParchmentButton")
    end

end

function AddProfTask(taskIndex,profName,profInfo,profElement)
    local iconOffsets = {18,18,18,18}
    local taskIcons = {"Apprentice_Icon","Journeyman_Icon","Master_Icon","Grandmaster_Icon"}
    local taskLabels = {"[d8a54d]Apprentice[-]","[d8a54d]Journeyman[-]","[d8a54d]Master[-]","[d8a54d]Grandmaster[-]"}
    local xOffset = 228 + (taskIndex-1)*176

    profElement:AddImage(xOffset, 52, "BasicWindow_BG_thin",160,160,"Sliced")
    profElement:AddImage(xOffset+49, iconOffsets[taskIndex], taskIcons[taskIndex],63,63)
    profElement:AddLabel(xOffset+80,82,taskLabels[taskIndex],150,60,24,"center",false,false,"Kingthings_Dynamic")

    if(ProfessionsHelpers.IsTaskLocked(this,profName,taskIndex)) then
        profElement:AddImage(xOffset+57, 128, "lock_large",47,54)
        profElement:AddButton(xOffset+30,82,"","",100,100,BuildLockedTooltip(profName,taskIndex),"",false,"Invisible") 
    elseif(ProfessionsHelpers.IsTaskCompleted(this,profName,taskIndex)) then
        
        -- Determine what a completed task should display
        xOffset = ProfessionsHelpers.GetTaskCompleteDisplay( profName, taskIndex, profElement, xOffset )
        
    else
        --local rewardList = ProfessionsHelpers.GetTaskRewards(this,profName,taskIndex)
        --local yOffset = 120
        --for i,reward in pairs(rewardList) do
        --    profElement:AddImage(xOffset+18+reward.IconOffset,yOffset+reward.IconOffset,reward.Icon,reward.IconSize,reward.IconSize,reward.IconType)
        --    profElement:AddLabel(xOffset+50,yOffset+14,reward.Desc,110,60,16,"vcenter",false,false)
        --    yOffset = yOffset + 34
        --end
        profElement:AddLabel(xOffset+80,120,"[FFFFFF]"..ProfessionsHelpers.GetTaskName(profName,taskIndex).."[-]",130,80,32,"center",false,false,"Kingthings_Dynamic")

        if(ProfessionsHelpers.IsTaskActive(this,profName,taskIndex)) then            
            profElement:AddButton(xOffset+20,190,"Stop|"..profName.."|"..taskIndex,"Abandon Task",118,32,"","",false,"GoldRedButton")
        elseif (ProfessionsHelpers.IsTaskCancelled(this, profName, taskIndex) and ProfessionsHelpers.CanResumeLaterStep(this, profName)) then
            profElement:AddButton(xOffset+20,190,"Resume|"..profName.."|"..taskIndex,"Resume Task", 118,32,"","",false,"GoldRedButton")
        else
            profElement:AddButton(xOffset+20,190,"Begin|"..profName.."|"..taskIndex,"Begin Task",118,32,"","",false,"GoldRedButton")
        end
    end
end

function AddProfessionElement(lineIndex,profName,profInfo,scrollWindow)
    local profElement = ScrollElement()
    
    local iconName = "Profession_"..profName
    profElement:AddImage(48, 42, iconName,128,128)

    profElement:AddImage(16,176,"Prestige_TitleHeader")
    profElement:AddLabel(110,176,"[43240f]"..(profInfo.DisplayName or profName).."[-]",0,0,34,"center",false,false,"Kingthings_Dynamic")

    AddProfTask(1,profName,profInfo,profElement)
    AddProfTask(2,profName,profInfo,profElement)
    AddProfTask(3,profName,profInfo,profElement)
    AddProfTask(4,profName,profInfo,profElement)

    scrollWindow:Add(profElement)
end


-- Events

RegisterEventHandler(EventType.DynamicWindowResponse, "ProfessionGuideWindow", function (user,buttonId)
    if not(buttonId) or buttonId == "" then CleanUp() return end

    local args = StringSplit(buttonId, "|")
    local buttonId = args[1]

    if(buttonId == "Train" and #args > 1) then
        local profName = args[2]
        local profInfo = Professions[profName]

        if(profInfo) then
            local introQuest = profInfo.IntroQuest
            --Quests.TryStart(this,introQuest)
            Quests.SetQuest(this, nil, profName, "0.1")

            UpdateWindow()
        end
    elseif(buttonId == "Skip" and #args > 1) then
        local profName = args[2]
        local profInfo = Professions[profName]
        if(profInfo and ProfessionsHelpers.HasSkillRequirement(this,profName,1)) then
            local introQuest = profInfo.IntroQuest
            Quests.ForceComplete(this,introQuest)

            UpdateWindow()
        end
    elseif(buttonId == "Begin" and #args > 2) then
        local profName = args[2]
        local taskIndex = tonumber(args[3])
        if not(ProfessionsHelpers.IsProfessionUnlocked(this,profName)) or ProfessionsHelpers.IsTaskLocked(this,profName,taskIndex) then return end
        local profInfo = Professions[profName]
        if(profInfo) then
            local questId = profInfo.Quests[taskIndex]
            --Quests.TryStart(this,questId)
            Quests.SetQuest(this, nil, profName, tostring(taskIndex)..".1")

            UpdateWindow()
        end
    elseif(buttonId == "Resume" and #args > 2) then
        local profName = args[2]
        local taskIndex = tonumber(args[3])
        local profInfo = Professions[profName]
        if(profInfo) then
            local questId = profInfo.Quests[taskIndex]
            if taskIndex == 0 then questId = profInfo.IntroQuest end
            --Quests.ResumeFailedStep(this, questId)
            local failedStep = Quests.GetFailedStep(this, questId)
            Quests.SetQuest(this, nil, profName, tostring(taskIndex).."."..tostring(failedStep))
            this:SystemMessage("[F2F5A9]"..tostring(AllQuests[questId][failedStep].Name).." resumed.[-]", "info")
            --DebugMessage("RESUME",profName,tostring(taskIndex).."."..tostring(failedStep), AllQuests[questId][failedStep].Name)
            UpdateWindow()
        end
    elseif(buttonId == "Stop" and #args > 2) then
        local profName = args[2]
        local taskIndex = tonumber(args[3])

        local profInfo = Professions[profName]
        if(profInfo) then
            local questId = ""
            if(tonumber(taskIndex) == 0) then
                questId = profInfo.IntroQuest
            else
                questId = profInfo.Quests[taskIndex]
            end
            Quests.Fail(this,questId)

            UpdateWindow()
        end
    end
end)

RegisterEventHandler(EventType.Message, "UpdateProfessionGuideWindow", UpdateWindow)
RegisterEventHandler(EventType.Message, "CloseProfessionGuideWindow", CleanUp)
RegisterEventHandler(EventType.Message, "InitProfessionGuideWindow", Init)

UpdateWindow()
