function OpenAuctioneerWindow()	
	local dynWindow = DynamicWindow("AuctioneerWindow","Auctioneer",560,400)

	local curItem = this:GetObjVar("AuctionItem")
	local bidHistory = this:GetObjVar("BidHistory") or {}

	local curY = 10
	dynWindow:AddLabel(20,curY,"Current Auction: ".. (curItem and curItem:GetName() or "None"),0,0,16)
	dynWindow:AddButton(228, curY-4, "SelectItem", "Select",80,26,"","",false,"")

	curY = curY + 30
	if(curItem) then
		dynWindow:AddButton(20, curY, "AddBid", "Add Bid",80,26,"","",false,"")
		dynWindow:AddButton(120, curY, "EndAuction", "End Auction",120,26,"","",false,"")
	end

	curY = curY + 30
	local scrollWindow = ScrollWindow(24,curY,500,390,26)
	for i,bidInfo in pairs(bidHistory) do
		local element = ScrollElement()
		element:AddLabel(24,3,bidInfo.Bidder:GetAttachedUserId(),0,0,18)
		element:AddLabel(100,3,bidInfo.Bidder:GetCharacterName() or "N/A",0,0,18,"left")
		element:AddLabel(400,3,tostring(bidInfo.BidAmount),1000,0,18,"left")
		scrollWindow:Add(element)
	end
	dynWindow:AddScrollWindow(scrollWindow)
	
	this:OpenDynamicWindow(dynWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"AuctioneerWindow",
	function (user,buttonId,fieldData)
		if not(IsGod(user)) then return end

		if(buttonId == "SelectItem") then
			this:SystemMessage("Select the item you wish to put up for auction.","info")
			this:RequestClientTargetGameObj(this, "target")
		elseif(buttonId == "AddBid") then
			this:SystemMessage("Select the bidder.","info")
			this:RequestClientTargetGameObj(this, "bidder")
		elseif(buttonId == "EndAuction") then
			local curItem = this:GetObjVar("AuctionItem")
			local backpackObj = this:GetEquippedObject("Backpack")
			curItem:MoveToContainer(backpackObj,GetRandomDropPosition(backpackObj))
			curItem:SetSharedObjectProperty("DenyPickup",false)

			this:DelObjVar("AuctionItem")
			OpenAuctioneerWindow()
		end
	end)

RegisterEventHandler(EventType.ModuleAttached,"auctioneer",
	function ( ... )
		AddUseCase(this,"Auctioneer")
	end)

RegisterEventHandler(EventType.Message,"UseObject",
	function (user,usedType)
		if(usedType == "Auctioneer") then
			OpenAuctioneerWindow()		
		end
	end)

local curAuctionItem = nil
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "target", 
    function(targ)
    	if(targ) then
    		this:SystemMessage("Select where you wish to place the item.","info")
    		curAuctionItem = targ
    		this:RequestClientTargetLoc(this, "place")
    	else
    		this:SystemMessage("Cancelled","info")
    	end
    end)

local bidder
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "bidder", 
    function(targ)
    	if(targ) then
    		bidder = targ
    		TextFieldDialog.Show{
		        TargetUser = this,
		        ResponseObj = this,
		        Title = "Bid Amount",
		        Description = "",
		        ResponseFunc = function(user,bidAmount)
		        	if(bidAmount ~= nil and tonumber(bidAmount)) then
			        	local bidHistory = this:GetObjVar("BidHistory") or {}
			        	table.insert(bidHistory,{Bidder=bidder,BidAmount=bidAmount})
			        	this:SetObjVar("BidHistory",bidHistory)
			        	OpenAuctioneerWindow()
			        end
		        end
		    }
    	else
    		this:SystemMessage("Cancelled","info")
    	end
    end)

RegisterEventHandler(EventType.ClientTargetLocResponse, "place", 
    function(success,targetLoc,targetObj,user)
    	if(success) then
    		curAuctionItem:SetWorldPosition(targetLoc)
    		curAuctionItem:SetSharedObjectProperty("DenyPickup",true)
    		this:SetObjVar("AuctionItem",curAuctionItem)
    		this:DelObjVar("BidHistory")
    		OpenAuctioneerWindow()
    	else
    		this:SystemMessage("Cancelled","info")
    	end
    end)

function UpdateAuctionHUD(playerObj)		
	local curItem = this:GetObjVar("AuctionItem")
	
	local title = "Celadorian New Year Festival Auction"

	local dynWindow = DynamicWindow("DynamicEventHUD","",0,0,0,0,"Transparent","Top")
	local yPos = 30
	dynWindow:AddLabel(0,yPos,title,0,0,24,"center",false,true,"SpectralSC-SemiBold")
	yPos = yPos + 20

	if(curItem) then
		local item = "Item For Auction: "..(curItem and curItem:GetName() or "None")
				
		dynWindow:AddLabel(0,yPos,item,0,0,18,"center",false,true,"SpectralSC-SemiBold")
		yPos= yPos + 15

		local bidHistory = this:GetObjVar("BidHistory")
		if(bidHistory and #bidHistory > 0) then
			local highBidderInfo = bidHistory[#bidHistory]
			dynWindow:AddLabel(0,yPos,"["..COLORS.FloralWhite.."]".."High Bidder: [-]"..(highBidderInfo.Bidder:GetCharacterName() or "N/A"),0,0,18,"center",false,true,"SpectralSC-SemiBold")
			yPos= yPos + 15
			dynWindow:AddLabel(0,yPos,"["..COLORS.FloralWhite.."]".." Amount: [-]"..highBidderInfo.BidAmount.."p",0,0,18,"center",false,true,"SpectralSC-SemiBold")			
		end		
	else
		dynWindow:AddLabel(0,yPos,"Beginning Shortly",0,0,18,"center",false,true,"SpectralSC-SemiBold")
	end

	if(playerObj) then
		playerObj:OpenDynamicWindow(dynWindow)
	else	
		local players = GetViewObjects("PlayersInRange")
		for i,player in pairs(players) do
			player:OpenDynamicWindow(dynWindow)
		end
	end
end

function CloseAuctionHUD( playerObj )
	if(playerObj) then
		playerObj:CloseDynamicWindow("DynamicEventHUD")
	else
		local players = GetViewObjects("PlayersInRange")
		for i,player in pairs(players) do
			player:OpenDynamicWindow(dynWindow)
		end
	end
end

AddView("PlayersInRange",SearchPlayerInRange(30,true))

RegisterEventHandler(EventType.LeaveView,"PlayersInRange",
	function ( playerObj)
		CloseAuctionHUD(playerObj)
	end)

RegisterEventHandler(EventType.Timer,"UpdateEvent",
	function ( ... )
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(5),"UpdateEvent")
		UpdateAuctionHUD()
		UpdateAuctionHUD(this)
	end)

this:FireTimer("UpdateEvent")