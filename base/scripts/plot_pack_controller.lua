
--- this is a module that is attached to a temporary object when cluster controller is told to pack all plots into item storage


local _Controllers = {}
local _I,_Frame_I = 0,0
local _FrameDelay = TimeSpan.FromSeconds(1)
local _PlotsPerSecond = 10

local _Done = 0

function CleanUp()
    DebugMessage("[Sale] Done. Total Plots Packed: "..#_Controllers)
    
    this:Destroy()
end

function PackAll()
    DebugMessage("[Pack] Started.")

    RegisterSingleEventHandler(EventType.DestroyAllComplete,"Prepack",
        function ( ... )
            local controllers = FindObjects(SearchTemplate("plot_controller"))
            for i=1,#controllers do
                local controller = controllers[i]
                if ( controller and controller:IsValid() ) then
                    _Controllers[#_Controllers+1] = controller
                end
            end
            if ( #_Controllers < 1 ) then
                CleanUp()
                return
            end
            PackNext()
        end)

    -- first destroy everything except noreset objects
    DestroyAllObjects(false,"Prepack")
end

function CheckDone()
    _Done = _Done + 1
    if ( _Done >= #_Controllers ) then
        CleanUp()
    end
end

function PackStart(controller,pack_index)
    local plotOwner = Plot.GetOwner(controller)
    if not(plotOwner) then
        CheckDone()
        return
    end

    Plot.Pack(controller,plotOwner,CheckDone)
end

function PackNext()
    _I = _I + 1
    -- done
    if ( _I > #_Controllers ) then return end

    xpcall(function()
        PackStart(_Controllers[_I],_I)        
    end, function(e)
        LuaDebugCallStack("[PLOT PACK CONTROLLER]" .. e)
    end)

    -- do next
    _Frame_I = _Frame_I + 1
    if ( _Frame_I > _PlotsPerSecond ) then
        _Frame_I = 0
        -- next frame
        this:ScheduleTimerDelay(_FrameDelay, "PackNext")
    else
        -- same frame
        PackNext()
    end
end

RegisterEventHandler(EventType.Timer, "PackNext", PackNext)

RegisterEventHandler(EventType.ModuleAttached, "plot_pack_controller", function()
    -- put it into the next frame so we can detach
    CallFunctionDelayed(_FrameDelay, PackAll)
end)