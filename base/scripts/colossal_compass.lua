require 'treasure_map'

DEFAULT_ACCURACY = 30
TARGET_TYPE = "colossal"


function OnStudySuccess(user, mapLocation)
    --DebugMessage("SUCCESS")
    local spawnData = {
        Prefab = this:GetObjVar("ColossalPrefabTemplate"),
        Loc = this:GetObjVar("MapLocation"),
        Region = this:GetObjVar("RegionalName"),
    }
    
    -- Check for houses
    local prefabExtents = GetPrefabExtents(spawnData.Prefab)
    local relBounds = GetRelativePrefabExtents(spawnData.Prefab, spawnData.Loc, prefabExtents)
    
    -- If the location is good, lets do this!
    if(CheckBounds(relBounds,false)) then
        user:SystemMessage("The map disintegrate as the colossal is summoned.","info")
        CreateObj("prefab_camp_controller",spawnData.Loc,"controller_spawned",spawnData)
        this:Destroy()
    -- The location was blocked
    else
        user:SystemMessage("The colossal has moved, you must consult the map once more.","info")
        -- Find a new location
        FindTreasureLocation()
        -- Reset durability
        AdjustDurability(this, 0, 10, 10)
    end


    -- IF house found then change map location
    -- Spawn camp here
    
    -- MAKE MONEY!!!

    --DebugMessage("PREFAB MADE")
end

function DecipherMap(user)
	local difficulty = this:GetObjVar("Difficulty")
	if not(CanRead(user,difficulty)) then
		user:SystemMessage("You have no chance of deciphering this map.","info")
		return
	end

	local skillLevel = GetSkillLevel(user,"TreasureHuntingSkill")
	local decipherChance = SkillValueMinMax(skillLevel, difficulty - 25, difficulty + 25 )
	if(IsPrecise() or CheckSkillChance(user,"TreasureHuntingSkill", skillLevel, decipherChance)) then
		local regionalName = this:GetObjVar("RegionalName")
		this:SetObjVar("Deciphered",true)
		user:SystemMessage("You decipher the map and find it to be in "..regionalName[2]..".","info")
		SetTooltipEntry(this,"decipher",regionalName[2])
		RemoveUseCase(this,"Decipher")
		AddUseCase(this,"Study",true,"HasObject")		
	elseif(Success(ServerSettings.Durability.Chance.OnMapDecipher)) then
		user:SystemMessage("You fail to decipher the map and damage it in the process.","info")
		AdjustDurability(this, ServerSettings.Durability.FailHit.OnMapDecipher)
	else
		user:SystemMessage("You fail to decipher the map.","info")
	end
end

function FindTreasureLocation()
    --DebugMessage("BOOM 2")
	if (not this:HasObjVar("RegionalName")) then
		DebugMessage("[treasure_map] Had a map with no map regions.")
		this:Destroy()
		return
	end
	--DebugMessage(ServerSettings.WorldName,this:GetObjVar("Shard"),ServerSettings.WorldName == this:GetObjVar("Shard"))
	if( ServerSettings.WorldName == "NewCelador" ) then
		local region = GetRegion(this:GetObjVar("RegionalName")[1])
        if (region ~= nil) then
            
		    local prefabExtents = GetPrefabExtents(this:GetObjVar("ColossalPrefabTemplate"))

		    local try = 0
		    local mapLocation = nil
		    while(try < 10) do
			    -- try to find an area that is in a camp region and all 4 corners are not overlapping another area
			    local spawnLoc = GetRandomPassableLocationFromRegion(region,true)
			    --DebugMessage("GetRandomSpawnLocation "..tostring(entry.Prefab)..", "..DumpTable(entry))
			    local relBounds = GetRelativePrefabExtents(this:GetObjVar("ColossalPrefabTemplate"), spawnLoc, prefabExtents)

			    --Check if player is in camp boundary and if a player exists, do not build camp here
			    local nearbyPlayer = FindObjects(SearchMulti({SearchRect(relBounds),SearchUser()}),GameObj(0))
			    if (#nearbyPlayer ~= 0) then
			        return nil
			    end

			    if(CheckBounds(relBounds,false)) then
			        --Move every mobiles in camp boundary
			        MoveMobiles(relBounds, spawnLoc)
			    	mapLocation = spawnLoc
			    	break
			    end
			    try = try + 1
			end

			if(mapLocation) then
				this:SetObjVar("MapLocation",mapLocation)
			else
				-- DAB NOTE: If a player gets a map for a different region, it will silently destroy itself when he enters that region if it fails to find a location
				-- we need a better solution for this
				this:Destroy()
				return
			end
		end
	end
end

RegisterSingleEventHandler(EventType.ModuleAttached,"colossal_compass",
	function()
		this:SetObjVar("ColossalPrefabTemplate",initializer.ColossalPrefabTemplate[1])
		--this:SetObjVar("MapRegions",initializer.MapRegions)		

		--DebugMessage(DumpTable(MapRegions))
		AddUseCase(this,"Decipher",true,"HasObject")

		if (initializer.RegionalNames and not (this:HasObjVar("RegionalName"))) then
            local mapRegion = GetRandomKeyInTable(initializer.RegionalNames)
			--DebugTable(initializer.RegionalNames[mapRegion])
            this:SetObjVar("RegionalName",initializer.RegionalNames[mapRegion])
        end
		FindTreasureLocation()
    end)
    
RegisterEventHandler(EventType.CreatedObject,"controller_spawned",
function(success,objRef,callbackData)        
    --spawn the object and save it
    if success then
        objRef:SetObjVar("PrefabName",callbackData.Prefab)
        objRef:SendMessage("Reset")
    end
end)

studyLoc = nil