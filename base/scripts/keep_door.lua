local townshipId
local militiaId
local ramObj

local maxHealth = ServerSettings.Militia.Events[3].DoorMaxHP
this:SetStatMaxValue("Health", math.floor(maxHealth))
this:SetStatVisibility("Health","Global")

local enemyTeleportLoc = this:GetLoc():Project(90,6.5)

function GetRam()
    if(ramObj and not(ramObj:IsValid())) then
        ramObj = nil
    end

    if not(ramObj) then
        ramObj = FindObject(SearchTemplate("battering_ram",20))
    end
    return ramObj
end

function UpdateTooltip(curHP)    
    curHP = curHP or this:GetStatValue("Health")
    SetTooltipEntry(this,"amount","HP: ".. curHP .." / ".. maxHealth)    
end

function GetTownship()
    if not(townshipId) then
        townshipId = this:GetObjVar("Township")
    end
	return townshipId
end

function GetMilitaId()
    return TownshipsHelper.GetMilitaId(GetTownship())
end

function DoRepair(amount,useSpiritwood)
    local oldHP = this:GetStatValue("Health")
    local curHP = math.min(oldHP + amount, maxHealth)

    if(curHP ~= oldHP) then
        if(useSpiritwood) then
            local spiritwoodCost = math.ceil((curHP - oldHP) * ServerSettings.Militia.Events[3].DoorRepairSpiritwoodPerHP)
            if not(Militia.ConsumeGlobalResource(GetMilitaId(),"Spiritwood",spiritwoodCost)) then
                --DebugMessage("NOT ENOUGH SPIRITWOOD TO REPAIR "..GetMilitaId())
                if( not(this:HasTimer("RepairSpiritwoodWarn")) ) then
                    Militia.EventMessageMilitia("Your militia requires more spiritwood to repair the keep door.",GetMilitaId())
                    this:ScheduleTimerDelay(TimeSpan.FromMinutes(15), "RepairSpiritwoodWarn")
                end
                return                
            end
        end

        this:SetStatValue("Health",curHP)        

        UpdateTooltip(curHP)
        if(curHP == ServerSettings.Militia.Events[3].DoorMaxHP) then
            if(this:GetSharedObjectProperty("DoorState") == "Destroy") then
                this:PlayObjectSound("event:/objects/doors/wooden_door/wooden_door_close",false)
                this:SetSharedObjectProperty("DoorState","Repair")
                this:SetCollisionBoundsFromTemplate(this:GetCreationTemplateId())
            end
        elseif(curHP < oldHP) then
            -- TODO: Show repair?            
        end
    end
end

function DoDamage(amount)
    local oldHP = this:GetStatValue("Health")
    local curHP = math.max(oldHP - amount, 0)

    if(curHP ~= oldHP) then
        this:SetStatValue("Health",curHP)
        UpdateTooltip(curHP)
        
        if( not(this:HasTimer("KeepAttackWarn")) ) then
            local ramObj = GetRam()
            if(ramObj) then
                local militiaId = ramObj:GetObjVar("MilitiaId")
                local militiaData = Militia.GetDataById(militiaId)
                if ( militiaData ) then
                    Militia.EventMessageMilitia("Your keep is under attack by the "..militiaData.Name.." militia!",GetMilitaId())
                end
            else
                Militia.EventMessageMilitia("Your keep is under attack!",GetMilitaId())
            end
            
            this:ScheduleTimerDelay(TimeSpan.FromMinutes(5), "KeepAttackWarn")
        end

        if(curHP == 0) then
            this:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact",false)
            
            this:SetSharedObjectProperty("DoorState","Destroy")
            this:ClearCollisionBounds()
        elseif(curHP < oldHP) then
            local curState = this:GetSharedObjectProperty("DoorState")
            -- messy way to play impact animation multiple times
            if(curState == "Impact1") then
                this:SetSharedObjectProperty("DoorState","Impact2")
            else
                this:SetSharedObjectProperty("DoorState","Impact1")
            end            
        end

        if not(this:HasTimer("RepairTimer")) then
            this:ScheduleTimerDelay(TimeSpan.FromSeconds(5),"RepairTimer")
        end
    end
end
RegisterEventHandler(EventType.Message,"DoDamage",DoDamage)

function RepairTick()
    --DebugMessage("REPAIR TICK")

    local curHealth = this:GetStatValue("Health")
    local maxHealth = ServerSettings.Militia.Events[3].DoorMaxHP
    if(curHealth >= maxHealth) then return end

    this:ScheduleTimerDelay(TimeSpan.FromSeconds(5),"RepairTimer")

    local ramObj = GetRam()
    if(ramObj) then
        --DebugMessage("DEBUG: REPAIR BLOCKED: RAM EXISTS")
        return
    end

    local enemyMilitiaInKeep = false
    for i,playerObj in pairs(GetViewObjects("PlayersInKeep")) do
        local militiaId = Militia.GetId(playerObj)
        if militiaId and (militiaId ~= GetMilitaId() and not(IsDead(playerObj))) then
            enemyMilitiaInKeep = true
            break
        end
    end

    if(enemyMilitiaInKeep) then
        --DebugMessage("DEBUG: REPAIR BLOCKED: ENEMIES IN KEEP")
        if( not(this:HasTimer("RepairEnemyWarn")) ) then
            Militia.EventMessageMilitia("Keep door can not be repaired while enemies are inside.",GetMilitaId())
            this:ScheduleTimerDelay(TimeSpan.FromMinutes(5), "RepairEnemyWarn")
        end
        return
    end

    DoRepair(ServerSettings.Militia.Events[3].DoorRepairPerSecond, true)
end

RegisterEventHandler(EventType.Message, "UseObject", 
    function(user,usedType)
        if(usedType == "Admin" and IsGod(user)) then
            OpenAdminWindow(user)
    	elseif(usedType == "Enter/Exit" or usedType == "Enter" or usedType == "Exit" or usedType == "Use") then 
    		local userTownship = TownshipsHelper.GetPlayerTownship(user)
    		if(userTownship == GetTownship()) then
    			local chosenPos, reason = Door.GetPassThroughLoc(this,user)

                if chosenPos then

                    ForeachMobileAndPet( user, function(mobile) 
                        mobile:SetWorldPosition(chosenPos)
                        mobile:PlayEffect("TeleportToEffect")    
                    end)

                    
                else
                    if reason == "distance" then 
                        user:SystemMessage("I am too far to pass through there.", "info")
                        return
                    else                            
                        user:SystemMessage("I cannot pass through there.", "info")
                        return
                    end
                end
    		else
    			user:SystemMessage("You must be a member of the local township to enter.", "info")
                return
    		end
    	end
    end)

RegisterEventHandler(EventType.Timer,"RepairTimer",function ( ... )
    RepairTick()
end)

RegisterEventHandler(EventType.LoadedFromBackup,"",
    function ()
        RepairTick()
    end)

RegisterEventHandler(EventType.ModuleAttached,"keep_door",
	function ()
		AddUseCase(this,"Enter/Exit",true)		
        AddUseCase(this,"Admin",false,"IsGod")     

        this:SetStatValue("Health", maxHealth)
	end)

function OpenAdminWindow(user)
    local dynWindow = DynamicWindow("AdminWindow","Door Debug",560,200)

    local doorState = this:GetSharedObjectProperty("DoorState")

    local curY = 20
    
    dynWindow:AddButton(20,curY,"Impact1","Impact1",80,23,"","",false,"",GetButtonState(doorState,"Impact1")) 
    dynWindow:AddButton(100,curY,"Impact2","Impact2",80,23,"","",false,"",GetButtonState(doorState,"Impact2"))
    dynWindow:AddButton(180,curY,"Destroy","Destroy",80,23,"","",false,"",GetButtonState(doorState,"Destroy"))  
    dynWindow:AddButton(260,curY,"Repair","Repair",80,23,"","",false,"",GetButtonState(doorState,"Repair"))   
    dynWindow:AddButton(340,curY,"Default","Default",80,23,"","",false,"",GetButtonState(doorState,"Default"))

    local curY = 50
    dynWindow:AddLabel(24,curY+13,"[A1ADCC]Damage[-]",0,35,18,"left")
    dynWindow:AddTextField(120,curY+10,200,20,"damage")
    dynWindow:AddButton(300,curY+8,"Damage","Damage",80,23,"","",false,"")      
    curY = curY + 34
    dynWindow:AddLabel(24,curY+13,"[A1ADCC]Repair[-]",0,35,18,"left")
    dynWindow:AddTextField(120,curY+10,200,20,"repair")
    dynWindow:AddButton(300,curY+8,"RepairAmt","Repair",80,23,"","",false,"")  
    
    user:OpenDynamicWindow(dynWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"AdminWindow",
    function (user,buttonId,fieldData)
        if not(IsGod(user)) then return end

        if(buttonId == "RepairAmt" and fieldData.repair) then
            DoRepair(tonumber(fieldData.repair), false)
        elseif(buttonId == "Damage" and fieldData.damage) then
            DoDamage(tonumber(fieldData.damage))
        elseif(buttonId and buttonId ~= "") then
            this:SetSharedObjectProperty("DoorState",buttonId)
            OpenAdminWindow(user)                
        end
    end)


RegisterEventHandler(EventType.EnterView,"PlayersInKeep",
    function (targetObj)
        local militiaId = GetMilitaId()
        local playerMilitia = Militia.GetId(targetObj)
        if(playerMilitia and militiaId ~= playerMilitia) then
            if(this:GetSharedObjectProperty("DoorState") ~= "Destroy") then
                targetObj:SystemMessage("A powerful magic prevents you from entering the keep.","info")
                local teleportLoc = enemyTeleportLoc
                if not(IsPassable(teleportLoc)) then
                    teleportLoc = GetNearbyPassableLocFromLoc(teleportLoc,0,6)    
                end
                TeleportUser(this,targetObj,teleportLoc)            
            end
        end
    end)

AddView("PlayersInKeep",SearchMulti{SearchRegion(GetTownship().."Keep",true),SearchUser()})