require 'incl_humanloot'

if(initializer.EquipTable) then
	EquipMobile(initializer.EquipTable,initializer.LootTables,false, true)
end

CallFunctionDelayed(TimeSpan.FromSeconds(2),function ( ... )
	this:DelModule("autoequip_mobile")
end)