require 'dynamic_spawn_controller'

mDynamicSpawnKey = "DynamicFlatWildsSpawner"

mDynamicHUDVars.Title = "Tough Carapaces"
mDynamicHUDVars.Description = "Kill the beetles surrounding the Flickering Portal."

local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        msg = "Giant Beetles begin to appear."
    elseif( progress == 2 ) then 
        msg = "Scorched Beetles begin to appear."
    elseif( progress == 3 ) then 
        msg = "Venomous Beetles begin to appear."
    elseif( progress == 4 ) then 
        msg = "A Gargantuan Beetle approaches."
    elseif( progress == 999 ) then 
        msg = "The flame is extinguished."
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end