--A spawnable resource node for fish

RegisterEventHandler(EventType.Message, "RequestFishNodeResource",
	function(user, depletionAmount)
		local fishType = this:GetObjVar("SchoolOfFish")
		if not(fishType) then
			return
		end

		local remaining = (this:GetObjVar("RemainingResources") or 0) - depletionAmount
		if (remaining > 0) then
			--Harvest
			this:SetObjVar("RemainingResources", remaining)
			user:SendMessage("RequestFishNodeResourceResponse", true, user, nil, this, remaining)

		elseif (remaining == 0) then
			--Depleted
			this:SetObjVar("RemainingResources", 0)
			this:SendMessage("NodeDepleted")
			user:SendMessage("RequestFishNodeResourceResponse", false, user, nil, this, remaining, "Depleted")

			--Decay/Destroy
			local decayTimer = this:GetObjVar("DecayTime")
			if (decayTimer) then
				Decay(this, decayTimer)
			else
				this:Destroy()
				return
			end

			--Respawn node timer
			if not(this:HasTimer("ResourceRegen")) then
				local regenMins = this:GetObjVar("RegenMins")
				if (regenMins ~= nil) then
					this:ScheduleTimerDelay(TimeSpan.FromMinutes(regenMins),"ResourceRegen")
				end
			end
		else
			--Attempting to harvest depleted node
			user:SendMessage("RequestFishNodeResourceResponse", false, user, nil, this, remaining, "Depleted")
		end
	end)

function ResetNode()
	local resourceCount = this:GetObjVar("MaxResources") or 1
	if (resourceCount ~= nil) then
		this:SetObjVar("RemainingResources", resourceCount)
	end
end

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(),
	function()
		local max = this:GetObjVar("MaxResources")
		if (max) then
			this:SetObjVar("RemainingResources", max)
		end
	end)