require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

AI.IntroMessages =
{
	"I'm busy, move along please.",
} 

function Dialog.OpenGreetingDialog(user)
    local text = nil    
    
    text = "Hmm."

    local response = {
        {
            text = "Sorry to bother you.",
            handle = ""
        }
    }
    
    NPCInteractionLongButton(text,this,user,"Responses",response)
end
