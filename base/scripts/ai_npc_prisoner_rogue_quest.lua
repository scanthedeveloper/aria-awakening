require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

AI.GreetingMessages = 
{
    "...",
}

function Dialog.OpenWhoDialog(user)
    --QuickDialogMessage(this,user,"REPLACED -> I am a quest guy!")
end

function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)] or AI.GreetingMessages[1]

    response = {}

    response[2] = {}
    response[2].text = "... ... ..."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)