require 'dynamic_spawn_controller'

mDynamicSpawnKey = "DynamicFlatWildsSpawner"

mDynamicHUDVars.Title = "Dragonkin Rising"
mDynamicHUDVars.Description = "Kill the dragonkin surrounding the Enduring Portal."

local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        msg = "Drakes begin to appear."
    elseif( progress == 2 ) then 
        msg = "Dragons begin to appear."
    elseif( progress == 3 ) then 
        msg = "A Mature Wyvern approaches."
    elseif( progress == 999 ) then 
        msg = "The flame is extinguished."
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end