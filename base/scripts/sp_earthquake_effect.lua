mPulsesLeft = 0
mLoc = nil
EARTHQUAKE_DAMAGE_RANGE = 6
RegisterEventHandler(EventType.Timer, "EarthquakePulseTimer",
function ()
	--this:PlayEffect("ScreenShakeEffect", 2)
	--if(math.fmod(mPulsesLeft,2) == 0) then

	--end		
	mPulsesLeft = mPulsesLeft - 1
	local mobiles = FindObjects(SearchMulti({
		SearchRange(mLoc,EARTHQUAKE_DAMAGE_RANGE),
		SearchMobile()}), GameObj(0))
	for i=1,#mobiles do
		if ( this ~= mobiles[i] and mobiles[i]:HasLineOfSightToLoc(mLoc,ServerSettings.Combat.LOSEyeLevel) and ValidCombatTarget(this, mobiles[i], true) ) then
			this:SendMessage("RequestMagicalAttack", "Earthquake", mobiles[i], this, true)
		end
	end
	--this:PlayEffectWithArgs("ScreenShakeEffect", 10.0,"Magnitude=10")

  if(mPulsesLeft > 0) then 
  	this:ScheduleTimerDelay(TimeSpan.FromSeconds(.6), "EarthquakePulseTimer")
  else
  	this:FireTimer("EndEarthquakeTimer")
  end

end)


RegisterEventHandler(EventType.CreatedObject,"aoe_created_with_sound",function (success,objRef)
	if (success) then
		objRef:SetObjVar("DecayTime",3)
		objRef:SetObjVar("SoundOnCreation", "event:/magic/fire/magic_fire_cast_fire")
		objRef:AddModule("object_sound_holder")
	end
end)

RegisterEventHandler(EventType.Message, "CompletionEffectsp_earthquake_effect",
	function ()
		if(mLoc == nil) then 
			mLoc = this:GetLoc()
		end
		PlayEffectAtLoc("EarthquakeDustEffect", mLoc, 5.0)
		CreateTempObj("spell_aoe",mLoc,"aoe_created_with_sound")
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1), "EarthquakePulseTimer")
		mPulsesLeft = 5
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(4), "EndEarthquakeTimer")
	end)



RegisterEventHandler(EventType.Timer, "EndEarthquakeTimer",
function() 
	this:DelModule("sp_earthquake_effect")
end)