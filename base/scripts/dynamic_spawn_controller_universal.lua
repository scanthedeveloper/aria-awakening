require 'dynamic_spawn_controller'
mDynamicSpawnKey =  this:GetObjVar("SpawnKey") or "UniversalDynamicSpawner"

local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    ProgressMessages = this:GetObjVar("ProgressMessages") or {}
    local msg = ""
    msg = ProgressMessages[progress] or nil

    if( progress == 999 ) then 
        msg = "You failed to stop the shrine from activating."
    end

    if( msg ) then
        -- Broadcast message to players
        HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
            player:SystemMessage(msg)
            player:SystemMessage(msg,"info")
        end)
    end

end