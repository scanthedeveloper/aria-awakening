
function ToggleBook(isVisible)
	--DebugMessage("Toggle Catalog:",isVisible and "ON" or "OFF")
	this:SetSharedObjectProperty("Variation", isVisible and "YesBook" or "NoBook")

	if isVisible then
		AddUseCase(this,"Remove Catalog")
	else
		RemoveUseCase(this, "Remove Catalog")
		RemoveTooltipEntry(this,"progress")
	end
end

function PickCatalog(target, user)
	if target == nil or target:GetCreationTemplateId() ~= "lore_catalog" then return end
	if Lore.IsCatalogOwner(target, user) then
		this:SetObjVar("Owner", target:GetObjVar("Owner"))
		this:SetObjVar("Tomes", target:GetObjVar("Tomes"))
		this:SetObjVar("Accreditee", target:GetObjVar("Accreditee"))

        target:Destroy()
        ToggleBook(true)
    else
    	user:SystemMessage("You do not own that Catalog.", "info")
	end
end

function RemoveCatalog(user)
	if Lore.IsCatalogOwner(this, user) or not this:HasObjVar("Owner") then
		Create.InBackpack("lore_catalog", user, nil, function(obj)
	        if obj then
	            obj:SetObjVar("Owner", this:GetObjVar("Owner"))
				obj:SetObjVar("Tomes", this:GetObjVar("Tomes"))
				obj:SetObjVar("Accreditee", this:GetObjVar("Accreditee"))

	            this:DelObjVar("Owner")
				this:DelObjVar("Tomes")
				this:DelObjVar("Accreditee")
		        ToggleBook(false)
	        end
	    end)
    else
    	user:SystemMessage("You do not own that Catalog.", "info")
	end
end

RegisterEventHandler(EventType.LoadedFromBackup,"", function()
	Lore.GetCatalogProgress(this)
	ToggleBook(this:GetObjVar("Tomes"))
end)

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()
	ToggleBook(this:GetObjVar("Tomes"))
	AddUseCase(this,"Use",true)
end)

RegisterEventHandler(EventType.Message, "ChangeUser", function(newOwner)
	if newOwner:IsPlayer() then
		this:SetObjVar("Owner", newOwner)
	end
end)

RegisterEventHandler(EventType.Message, "UseObject", function(user,usedType)
	if usedType == "Use" then
		if this:GetLoc():Distance2(user:GetLoc()) > 5 then user:SystemMessage("Too far away.", "info") return end

		this:PlayObjectSound("Use", true)

		local catalogTomeData = this:GetObjVar("Tomes")

		if catalogTomeData then
			Lore.ReadCatalog(user, this)
		else
			if this:GetObjVar("LockedDown") then
				local controller = Plot.GetAtLoc(this:GetLoc())
				if not controller or Plot.IsStranger(controller, user) then
					user:SystemMessage("You cannot place a Catalog on a Stranger's bookstand.", "info")
					return
				end
			end
			user:SystemMessage("Select a Catalog to display.", "info")
			user:RequestClientTargetGameObj(this, "pickCatalog")
		end
	elseif usedType == "Remove Catalog" then
		RemoveCatalog(user)
	end
end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "pickCatalog", PickCatalog)