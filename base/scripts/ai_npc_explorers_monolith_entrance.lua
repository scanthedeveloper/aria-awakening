require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

function IntroDialog(user)
    Dialog.OpenGreetingDialog(user)
end

function Dialog.OpenGreetingDialog(user)
    text = user:GetName() .. ", thank goodness you've arrived. We've been " ..
    "investigating this location for a long time now, but this is unprecedented. " ..
    "We are detecting a strong resonance of magical energies pouring out from the rock " .. 
    "wall behind me and last night cultists began to rise from the graveyard just to the south."

    response = {}

    response[1] = {}
    response[1].text = "What sort of energy?"
    response[1].handle = "WhatEnergy"

    response[2] = {}
    response[2].text = "Cultist?"
    response[2].handle = "WhatCultists"

    response[3] = {}
    response[3].text = "How can I help?"
    response[3].handle = "HowHelp"
    
    response[4] = {}
    response[4].text = "Goodbye."
    response[4].handle = ""

    NPCInteractionLongButton(text,this,user,"Responses",response)
end



function Dialog.OpenWhatEnergyDialog(user)
    text = "I am not sure yet, but I have been able to distill some of it. "..
    "It appears to be very old magic from before the shattering. I fear that if  " .. 
    "left unchecked this could herald a terrible calamity."

    response = {}
    
    response[1] = {}
    response[1].text = "Okay."
    response[1].handle = "Greeting"

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenWhatCultistsDialog(user)
    text = "Yes " .. user:GetName() ..", *clears throat* the ones just to the west. " .. 
    "At first I called the League of Dungeoneers to come and put them down, but these creatures " .. 
    "seem to have regenative powers. I am afraid we will need your help to thin their numbers."

    response = {}
    
    response[1] = {}
    response[1].text = "Okay."
    response[1].handle = "Greeting"

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenHowHelpDialog(user)
    text = "Kill the Risen Cultists and release their spirits. " .. 
    "They don't appear to be hostile, so you will simply need to inflict a deadly wound " .. 
    "before they are able to fully regenerate. Return 20 of them to their rest and I shall reward you " .. 
    "with something that might be useful as we learn more about this dark magic."

    response = {}
    
    response[1] = {}
    response[1].text = "I'll help."
    response[1].handle = "WillHelp"

    response[2] = {}
    response[2].text = "Nevermind."
    response[2].handle = "Greeting"

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenWillHelpDialog(user)
    
    --[[
        this:PlayObjectSound("event:/magic/bard/riddle_of_docility")
        this:PlayAnimation("cast_heal")
        local eventObj = FindObject(SearchHasObjVar("MonolithEventCenter",40))
        user:SendMessage("StartMobileEffect", "MonolithEntranceEvent", eventObj)
        PlayEffectAtLoc("WispSummonEffect", user:GetLoc(), 3)
    ]]
    
    Quests.TryStart( user, "MonolithEntranceQuests", 1 )

    text = "Goodspeed, " .. user:GetName() .. " I fear dark times are ahead of us. Have the courage to " .. 
    "to stand firm where others would cower.\n\nCome back to me in an hour, I might have more work for you."

    response = {}

    response[1] = {}
    response[1].text = "I will."
    response[1].handle = ""

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)