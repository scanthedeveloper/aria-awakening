require 'teleporter_map'

RegisterSingleEventHandler(EventType.ModuleAttached,"limbo_char_teleporter",
	function ( ... )
		SetTooltipEntry(this,"limbo","Requires that your acccount has less the 5 characters.")
	end)

local base_ActivateTeleporter = ActivateTeleporter
function ActivateTeleporter(user)
	if(user:GetUsedCharacterSlots() > ServerSettings.AllowedCharacterSlots + 1) then -- (+1 for premium)
		user:SystemMessage("You have too many characters to leave limbo.","info")

		ClientDialog.Show{
	        TargetUser = user,
	        ResponseObj = this,
	        DialogId = "TooManyChars",
	        TitleStr = "Note",
	        DescStr = "You can not leave limbo until you have 5 or less characters on your account.",
	        Button1Str = "Ok.",
	    }
		return
	end

	base_ActivateTeleporter(user)
end