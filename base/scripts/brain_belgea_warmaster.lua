

-- required for fast identification within require modules
_IS_BRAIN = true

require 'combat'
require 'brain_inc' -- must come after combat, before mobile
require 'base_mobile_advanced'

local fsm

function Start()
    fsm = FSM(this, {
        States.BelgaeWarmasterDeath,
        States.Leash,
        States.Aggro,
        States.AttackAggroList,
        States.BelgaeWarmasterFight,
        States.BelgaeWarmasterIdle,
    })

    fsm.Start()
end

local _OnMobileLoad = OnMobileLoad
function OnMobileLoad()
    this:SetObjVar("GroupAI", true)
    _OnMobileLoad()
    Start()
end