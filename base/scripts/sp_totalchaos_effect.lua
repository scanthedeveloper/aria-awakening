--SCAN ADDED

--require 'base_magic_sys'
local METOR_IMPACT_RANGE = 5
local mTargetLoc = nil
local function ValidateMeteor(targetLoc)
	--DebugMessage("Debuggery Deh Yah")
	if( not(IsPassable(targetLoc)) ) then
		this:SystemMessage("[$2614]","info")
		return false
	end

	if not(this:HasLineOfSightToLoc(targetLoc,ServerSettings.Combat.LOSEyeLevel)) then
		this:SystemMessage("[$2615]","info")
		return false
	end
	return true
end


local function MeteorImpact( targetLoc )
	--DebugMessage("Impact Function!")
	this:PlayObjectSound("event:/magic/earth/magic_earth_cast_meteor", false)
	local mobiles = FindObjects(SearchMulti({
					SearchRange(targetLoc,METOR_IMPACT_RANGE),
					SearchMobile()}), GameObj(0))
	for i=1,#mobiles do
		if ( mobiles[i]:HasLineOfSightToLoc(targetLoc,ServerSettings.Combat.LOSEyeLevel) and ValidCombatTarget(this, mobiles[i], true) ) then
			this:SendMessage("RequestMagicalAttack", "Lightning", mobiles[i], this, true, targetLoc)
		end
	end
	PlayEffectAtLoc("bsSmoke", targetLoc, 5)
end

RegisterEventHandler(EventType.Message,"MeteorSpellTargetResult",
	function (targetLoc, skipRemove)
		if not(ValidateMeteor(targetLoc)) then
			EndEffect()
			return
		end
		mTargetLoc = targetLoc
		PlayEffectAtLoc("LightningCloudEffect",mTargetLoc, 5)
        PlayEffectAtLoc("BardExplosionCyan",mTargetLoc, 5)

		local impactEventTimer = "MeteorImpact_"..uuid()

		RegisterSingleEventHandler(EventType.Timer, impactEventTimer,
		function( targetLoc )
			MeteorImpact( targetLoc )
		end)

		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1.5),impactEventTimer,mTargetLoc)
		
		-- If we are by-passing the this we will need to end it on our own!
		if not( skipRemove ) then
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(3),"MeteorRemove",mTargetLoc)
		end
		
	end)

RegisterEventHandler(EventType.Timer,"MeteorRemove",
	function()
		EndEffect()
	end)

	RegisterEventHandler(EventType.Message,"MeteorRemove",
	function()
		EndEffect()
	end)

function EndEffect()
	if(this:HasTimer("MeteorRemove")) then this:RemoveTimer("MeteorRemove") end
	this:DelModule("sp_totalchaos_effect")
end