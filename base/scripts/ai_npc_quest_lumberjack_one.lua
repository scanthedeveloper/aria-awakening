require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

AI.QuestStepsInvolvedIn = {
    {"LumberjackProfessionTierTwo", 3},	-- Ayanda, east of Pyros Landing
}

AI.GreetingMessages = 
{
    "Chopping lumber keeps me limber!",
}

function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,
    	"I am a Lumberjack, by trade. I am not fast enough for fighting, so this is the next best use of my slow strength."
    	)
    --QuickDialogMessage(this,user,"REPLACED -> I am a quest guy!")
end

function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)] or AI.GreetingMessages[1]

    response = {}

    response[1] = {}
    response[1].text = "Who are you?"
    response[1].handle = "Who" 
    
    response[2] = {}
    response[2].text = "Goodbye."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)