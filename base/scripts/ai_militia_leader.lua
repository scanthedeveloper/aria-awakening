require 'base_ai_npc'

AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

this:SetObjVar("Invulnerable", true)
Militia.CheckAndCreateRosterController(this)

__Militia_Data = nil
function MilitiaData()
    if ( __Militia_Data == nil ) then
        __Militia_Data = Militia.GetDataById(this:GetObjVar("Militia") or 1)
    end
    return __Militia_Data
end

function Dialog.OpenGreetingDialog(user)

    local militiaData = MilitiaData()

    local userMilitiaId = Militia.GetId(user)
    local townshipId = TownshipsHelper.GetPlayerTownship( user )

    if ( userMilitiaId ) then
        local recruiterMilitiaId = this:GetObjVar("Militia")
        if ( recruiterMilitiaId ~= userMilitiaId ) then
            InteractionOpposingMilitia(user)
        else
            InteractionSameMilitia(user)
        end
    elseif ( townshipId and townshipId ~= militiaData.Town ) then
        InteractionEnemyTownship(user)
    elseif ( not Militia.ValidateJoin(user) ) then
        InteractionTooWeak(user)
    else
        InteractionNoMilitia(user)
    end
end

function InteractionTooWeak(user)
    local militiaData = MilitiaData()
    local response = {}
    local text = "Greetings. I am a recruiter for the militia of "..militiaData.Town..". Come see me when you are stronger to enlist!\n\n*Joining a Militia requires a total of "..(ServerSettings.Stats.TotalPlayerStatsCap - 20).." stat points. You currently have "..GetTotalBaseStats(user)..".*"
    
    response[1] = {}
    response[1].text = "Ok."
    response[1].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function InteractionEnemyTownship(user)
    local response = {}
    local text = "The nerve you have! Citizens of enemy Townships may not join our ranks."
    
    response[1] = {}
    response[1].text = "Ok."
    response[1].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function InteractionNoMilitia(user)
    
    local response = {}
    local text = "Are you here to join the militia of "..MilitiaData().Name.."?"

    response[1] = {}
    response[1].text = "Yes."
    response[1].handle = "JoinMilitiaConfirm"
    
    response[2] = {}
    response[2].text = "No."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)

end

function InteractionSameMilitia(user)

    local militiaResign = user:GetObjVar("MilitiaResign")

    if ( militiaResign ) then
        InteractionResignMilitia(user, militiaResign)
        return
    end
    
    local response = {}
    local text = "Hello there warrior, keep up the good fight.\n\n"

    text = "Incase you're curious, here are militia rules:\n"
    text = MilitiaRules(text)
    
    response[1] = {}
    response[1].text = "Ok."
    response[1].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)

end

function InteractionResignMilitia(user, militiaResign)

    if ( militiaResign ~= true and militiaResign > DateTime.UtcNow ) then
    
        local response = {}
        local text = "I see you would like to resign, however militia rules dictate you wait a bit longer still."
        
        response[1] = {}
        response[1].text = "Fine."
        response[1].handle = "" 

        NPCInteractionLongButton(text,this,user,"Responses",response)
    else
    
        local response = {}
        local text = "Resign and renouce "..MilitiaData().Name.."? \n\nAll progress with this militia will be gone forever. You will remain a citizen of " .. MilitiaData().Town.."."
    
        response[1] = {}
        response[1].text = "Yes."
        response[1].handle = "LeaveMilitia"
        
        response[2] = {}
        response[2].text = "Not Yet."
        response[2].handle = "" 
    
        NPCInteractionLongButton(text,this,user,"Responses",response)

    end

end

function InteractionOpposingMilitia(user)
    
    local response = {}
    local text = "I don't deal with the likes of you."
    
    response[1] = {}
    response[1].text = "Goodbye."
    response[1].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)

end

function Dialog.OpenLeaveMilitiaDialog(user)

    local militiaResign = user:GetObjVar("MilitiaResign")

    if ( militiaResign ~= true ) then
        if ( militiaResign == nil or militiaResign > DateTime.UtcNow ) then
            Dialog.OpenGreetingDialog(user)
            return
        end
    end

    Militia.RemovePlayer(user, MilitiaData().Id)
    local text = "Thank you for your service, now be gone!"
    
    local response = {}
    response[1] = {}
    response[1].text = "Goodbye."
    response[1].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function MilitiaRules(text)
    text = text .. "- Minimum mandatory service of "..TimeSpanToWords(ServerSettings.Militia.ResignTime)..".\n"
    text = text .. "- Actions between opposing militias are never criminal.\n"
    text = text .. "- Guards do not interfere with conflicts between opposing militias.\n"
    text = text .. "- Beneficial actions on you are prohibited save for fellow militia members.\n"
    text = text .. "- If you are not currently a citizen of "..MilitiaData().Town.." you will be made one.\n"
    return text
end

function Dialog.OpenJoinMilitiaConfirmDialog(user)

    local text = "Are you ready to swear your undying fealty to "..MilitiaData().Name.."?\n\n"

    text = text .. "By joining you are bound to militia rules:\n"
    text = MilitiaRules(text)
    
    local response = {}
    response[1] = {}
    response[1].text = "I'm ready to fight."
    response[1].handle = "ConfirmJoin" 

    response[2] = {}
    response[2].text = "Not Yet."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenConfirmJoinDialog(user)
    ClientDialog.Show{
        TargetUser = user,
        ResponseObj = this,
        DialogId = "JoinMilitiaWindow",
        TitleStr = "Join Militia",
        DescStr = "Upon joining militia, you will not be able to leave the militia for 7 days. Do you wish to join "..MilitiaData().Name.."?",
        Button1Str = "Ok.",
        Button2Str = "Cancel."
    }
end

RegisterEventHandler(EventType.DynamicWindowResponse, "JoinMilitiaWindow", function(user,buttonId)
    local buttonId = tonumber(buttonId)
    if (user == nil) then return end
    if (buttonId == nil) then return end
    if ( buttonId == 0 ) then
        Dialog.OpenJoinMilitiaDialog(user)
    end
end)

function Dialog.OpenJoinMilitiaDialog(user)

    Militia.AddPlayer(user, MilitiaData().Id)
    TownshipsHelper.AddPlayerToTownship( user, MilitiaData().Town )
    local text = "Welcome to the militia of "..MilitiaData().Name..". Make us proud."
    
    local response = {}
    response[1] = {}
    response[1].text = "Ok."
    response[1].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

if ( MilitiaData().MilitiaLeaderTitle ) then
    this:SetSharedObjectProperty("Title", MilitiaData().MilitiaLeaderTitle)
end

-- set the militia icon
--this:SetSharedObjectProperty("Faction", MilitiaData().Icon)