

-- required for fast identification within require modules
_IS_BRAIN = true

require 'combat'
require 'brain_inc' -- must come after combat, before mobile
require 'base_mobile_advanced'

local fsm

function Start()
    fsm = FSM(this, {
        States.Death,
        States.SummonCompanion,
        States.Leash,
        States.Aggro,
        States.AttackAggroList,
        States.Attack,
        States.FightTank,
        States.Idle,
    })

    if( this:HasObjVar("MyPath") ) then
        fsm.ReplaceState(States.Idle, States.Patrol)
    elseif( this:GetObjVar("AI-CanWander") == true ) then
        fsm.ReplaceState(States.Idle, States.Wander)
    end

    fsm.Start()
end

local _OnMobileLoad = OnMobileLoad
function OnMobileLoad()
    this:SetObjVar("GroupAI", true)
    _OnMobileLoad()
    Start()
end