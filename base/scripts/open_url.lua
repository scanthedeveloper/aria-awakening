RegisterEventHandler(EventType.ModuleAttached,"open_url",
	function ( ... )
		AddUseCase(this,"Set URL",false,"IsImmortal")
	end)

RegisterEventHandler(EventType.Message,"UseObject",
	function(user,usedType)
		local URLCase = this:GetObjVar("URLCase")
		if(usedType == "Set URL" and IsImmortal(user)) then			
			TextFieldDialog.Show{
		        TargetUser = user,
		        ResponseObj = this,
		        Title = "Context Menu Text",
		        Description = "",
		        ResponseFunc = function(user,text)
		        	if(text and text ~= "") then
			        	AddUseCase(this,text,true)
			        	this:SetObjVar("URLCase",text)
			        	TextFieldDialog.Show{
					        TargetUser = user,
					        ResponseObj = this,
					        Title = "URL",
					        Description = "",
					        ResponseFunc = function(user,text)
					        	if(text and text ~= "") then
						        	this:SetObjVar("TargetURL",text)
						        end
					        end
					    }
			        end
		        end
		    }
		elseif(URLCase and usedType == URLCase) then
			local target = this:GetObjVar("TargetURL")
			ClientDialog.Show{
			    TargetUser = user,
			    TitleStr = "Confirm Open URL",
			    DescStr = "Do you wish to open a browser window for the following URL: "..target,
			    Button1Str = "Confirm",
			    Button2Str = "Cancel",
			    ResponseObj = this,
			    ResponseFunc = function (user,buttonId)
						if(buttonId == 0) then
							user:SendClientMessage("OpenURL",target)
						end
					end,
			}			
		end
	end)

