

require 'use_object'


local _player
function Player()
    if ( _player == nil ) then
        local playerId = this:GetObjVar("PlayerId")
        if(playerId) then
            _player = GameObj(playerId)
        else
            DebugMessage("ERROR: PlayerId on player corpse is nil!")
        end
    end
    return _player
end

-- we create a name object to allow corpse names to show from CTRL but not hover or entering screen
local _nameObject = this:GetObjVar("NameObject")
local _creatingNameObj = false
function ShowNameObject()
    if ( _creatingNameObj ) then return end
    local player = Player()
    if not( player ) then return end

    if ( _nameObject == nil ) then
        _nameObject = this:GetObjVar("NameObject")
    end
    if ( _nameObject == nil ) then
        _creatingNameObj = true
        local templateData = GetTemplateData("empty")
        templateData.ClientId = 293
        templateData.Name = ColorizePlayerName(this, EnglishPossessive(player:GetCharacterName()) .. " Corpse")
        templateData.ScaleModifier = 0.0001
        templateData.LuaModules = {
            player_corpse_name_object = { CorpseId = this.Id }
        }
        Create.Custom.AtLoc("empty", templateData, this:GetLoc(), function(nameObj)
            this:SetObjVar("NameObject", nameObj)
            _nameObject = nameObj
            _creatingNameObj = false
        end)
        return
    end
    if ( _nameObject:GetName() == "" ) then
        local name = _nameObject:GetObjVar("Name") or ColorizePlayerName(this, EnglishPossessive(player:GetCharacterName()) .. " Corpse")
        _nameObject:SetName(name)
    end
end

function HideNameObject()
    if ( _nameObject ) then
        _nameObject:SetObjVar("Name", _nameObject:GetName())
        _nameObject:SetName("")
    end
end

function SelfResDialog(player)
    PlayerResurrectDialog(player, {
        tooltip = "Corpse will be resurrected and moved to where you are standing.",
        cb = function(accept)
            if ( accept and player:DistanceFrom(this) <= ServerSettings.Death.SelfResurrectDistance ) then
                PlayerResurrect( player, player, this, ServerSettings.Death.SelfResurrectStatPercent )
            end
        end,
        response = this,
    })
end

RegisterEventHandler(EventType.Message, "Resurrect", function( statPercent, resurrector, force )

    if ( this:HasObjVar("BeenResurrected") ) then
        if ( resurrector and resurrector:IsValid() and resurrector:IsPlayer() ) then
            resurrector:SystemMessage("They have already been resurrected.", "info")
        end
        return
    end

    -- look for the player
    local player = Player()
    if ( player ) then

        -- play some effects on corpse so everyone else knows what's going on
        this:PlayEffect("HolyEffect")
        CallFunctionDelayed(TimeSpan.FromSeconds(1), function()
            this:PlayEffect("HolyEffect")
        end)

        -- local region resurrect
        if ( player:IsValid() ) then
            statPercent = statPercent or ServerSettings.Death.SelfResurrectStatPercent
            CorpseResurrectDialog(player, resurrector, force, function()
                PlayerResurrect( player, resurrector, this, statPercent)
            end, this)
        -- remote region resurrect
        else
            if ( GlobalVarReadKey("User.Online", player) ) then
                -- player is online, send a global message to them                        ternary
                player:SendMessageGlobal("RemoteResurrect", ServerSettings.RegionAddress, (resurrector and resurrector:IsValid()) and resurrector:GetLoc() or this:GetLoc(), resurrector, force)
            else
                resurrector:SystemMessage("That player is offline.", "info")
            end
        end
    end
end)

local playerSelfResInRange = nil
RegisterEventHandler(EventType.EnterView, "PlayerCorpseView", function(mobile)
    if ( 
        Player().Id ~= mobile.Id
        or mobile:GetObjVar("CorpseObject") ~= this
        or mobile:HasTimer("RecentlyReleased")
    ) then return end

    if ( mobile:GetSharedObjectProperty("IsDead") == true ) then
        -- player entered the scene, clear the corpse name so it doesn't show
        -- (real player is scaled down out of sight but their name still shows)
        HideNameObject()
    elseif (
        IsDead(mobile)
        and not mobile:HasObjVar("DeathShrineClear")
        and not mobile:HasObjVar("ResurrectLoadedFromBackup")
    ) then
        -- player's ghost entered the scene, offer them a self resurrect
        playerSelfResInRange = mobile
        SelfResDialog(mobile)
    end
end)
RegisterEventHandler(EventType.LeaveView, "PlayerCorpseView", function(mobile)
    if ( mobile == playerSelfResInRange ) then
        -- player's ghost left scene
        playerSelfResInRange = nil
        CloseResurrectDialog(mobile)
    elseif ( Player().Id == mobile.Id ) then
        -- player left the scene (logged out/released to shrine) show name object
        ShowNameObject()
    end
end)
AddView("PlayerCorpseView", SearchPlayerInRange(ServerSettings.Death.SelfResurrectDistance,true), 0.5)

RegisterEventHandler(EventType.Message, "Resurrected", function()
    UpdateCorpseOnPlayerResurrected(this)
    DelView("PlayerCorpseView")
end)

-- so we can re-pop the self res dialog when a player cancel's an optional resurrect while within corpse range.
RegisterEventHandler(EventType.Message, "SelfResDialog", function()
    local player = Player()
    if ( player ) then
        if ( player and player:IsValid() and player:DistanceFrom(this) <= ServerSettings.Death.SelfResurrectDistance ) then
            SelfResDialog(player)
        end
    end
end)