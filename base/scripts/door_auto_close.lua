

function Close()
    -- because Door.Close also detaches this module, put it in next frame just in case
    OnNextFrame(function()
        Door.Close(this)
    end)
end

-- after the delay close the door
RegisterEventHandler(EventType.Timer, "AutoCloseTimer", Close)
-- loading from backup close the door
RegisterEventHandler(EventType.LoadedFromBackup, "door_auto_close", Close)

-- on attach we schedule the timer out
RegisterEventHandler(EventType.ModuleAttached, "door_auto_close", function()
    this:ScheduleTimerDelay(Door.AutoCloseDelay, "AutoCloseTimer")
end)