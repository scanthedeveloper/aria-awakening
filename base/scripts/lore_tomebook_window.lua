BOOK_WIDTH = 789
BOOK_HEIGHT = 455

mTomebookObj = nil
mPageTurns = 0
mMaxPageTurns = 1
mBookEntries = {}

mCompleteTomeData = nil
--mCompleteEntryTitlePapes = {}

mOpen = false

--[[
	DynamicWindow(ID, Title, WindowWidth, WindowHeight, xOffset, yOffset, WindowType, WindowAnchor)

	AddLabel(xOffset, yOffset, LabelText, TextWidth, LabelHeight, FontSize, Alignment, Scrollable, Outline, Font)

	AddButton(xOffset, yOffset, ID, ButtonText, ButtonWidth, ButtonHeight, Tooltip, ServerCommand, ShouldCloseOnPress, ButtonType, ButtonState, CustomSprite)

	AddImage(xOffset, yOffset, SpriteName, ImageWidth, ImageHeight, SpriteType, SpriteHue, Opacity)
]]

function CleanUp()
    this:CloseDynamicWindow("Tomebook")
    this:DelModule(GetCurrentModule())
end

function DrawTomebookWindow()

	local dynamicWindow = DynamicWindow("Tomebook", "", BOOK_WIDTH, BOOK_HEIGHT, -BOOK_WIDTH/2, -BOOK_HEIGHT/2, "TransparentDraggable", "Center")

	dynamicWindow:AddImage(0, 0, "Spellbook", BOOK_WIDTH, BOOK_HEIGHT)

	dynamicWindow:AddButton(726, 21, "", "", 0, 0, "", "", true, "CloseSquare")
	
	if not (mTomebookObj and mCompleteTomeData) then
		this:OpenDynamicWindow(dynamicWindow)
		DebugMessage("Draw Tomebook_Window failed.",mTomebookObj,mCompleteTomeData) 
		return 
	end

	local hasPrevPage = mPageTurns >= 1
	if (hasPrevPage) then
		local pageStr = tostring(mPageTurns * 2 - 1)
		dynamicWindow:AddButton(70, 25, "Page|"..pageStr, "", 154, 93, "", "", false, "BookPageDown")
	end

	local runeInfo = {}
	local hasNextPage = mPageTurns < mMaxPageTurns
	if (hasNextPage) then
		local pageStr = tostring(mPageTurns * 2 + 3)
		dynamicWindow:AddButton(570, 25, "Page|"..pageStr, "", 154, 93, "", "", false, "BookPageUp")
	end

	local buttonState = (mPageTurns == 0 and "pressed") or ""
	dynamicWindow:AddButton(6, 70, "Page|1", "", 78, 58, "", "", false, "RuneTab", buttonState)

	if mPageTurns == 0 then
		AddTomebookIndexPage(dynamicWindow)
	else
		AddTomebookDetailPage(dynamicWindow)
	end

	this:OpenDynamicWindow(dynamicWindow)
	mOpen = true
end

function AddTomebookIndexPage(dynamicWindow)

	dynamicWindow:AddLabel(236, 44, "[43240f]"..StripColorFromString(mCompleteTomeData.Name).."[-]", 0, 0, 46, "center", false, false, "Kingthings_Calligraphica_Dynamic")

	dynamicWindow:AddImage(110, 80, "SpellIndexInfo_Divider", 250, 0, "Sliced")

	local entries = mCompleteTomeData.Entries or {}

	local xOffset = 120
	local yOffset = 90
	for entryIndex = 1, #entries do
		--local isFilled = IsInTableArray(bookEntries, entryIndex)
		local isFilled = mBookEntries[entryIndex].LocalIndex ~= 0

		if isFilled then
			--[[local page = 1
			if entryIndex > 1 then
				for i = 1, entryIndex - 1 do
					page = page + #entries[i].Pages
				end
				--page = 1 + math.ceil(page/2)
			else
				page = 2
			end]]
			local page = mBookEntries[entryIndex].LocalIndex or 1
			dynamicWindow:AddButton(xOffset, yOffset, "Page|"..tostring(page), tostring(entryIndex)..". "..(entries[entryIndex].Title or ""), 250, 34, "", "", false, "BookListSingle")		
		else	
			dynamicWindow:AddButton(xOffset, yOffset, "", tostring(entryIndex)..". ", 250, 34, "", "", false, "BookListSingle", "faded")
		end
		yOffset = yOffset + 36
		if(entryIndex == 8) then
			yOffset = 48
			xOffset = xOffset + 320
		end
	end
end

function AddTomebookDetailPage(dynamicWindow)
	local entryData
	local entryIndex = 0
	local isRightPageANewEntry = false
	local rightPageIndex
	local leftPageIndex = mPageTurns * 2 + 1
	local leftPageEntryIndex = 1

	--DebugMessage("Pre-Loop.", "page turns:", mPageTurns, "left page index:",leftPageIndex)

	for i=1, #mBookEntries do
		local s = "Possible EntryIndex: "..tostring(i)
		local currentEntryTitlePageIndex = mBookEntries[i].LocalIndex
		local nextEntryTitlePageIndex = 0

		if leftPageIndex >= currentEntryTitlePageIndex and currentEntryTitlePageIndex ~= 0 then
			for j=i+1, #mBookEntries do
				if mBookEntries[j].LocalIndex ~= 0 and nextEntryTitlePageIndex == 0 then
					nextEntryTitlePageIndex = mBookEntries[j].LocalIndex
					rightPageIndex = j
					s = s.."\nFound Next Entry:"..tostring(nextEntryTitlePageIndex)
				end
			end
			nextEntryTitlePageIndex = (nextEntryTitlePageIndex == 0) and 999 or nextEntryTitlePageIndex
			if leftPageIndex < nextEntryTitlePageIndex then
				leftPageEntryIndex = leftPageIndex - currentEntryTitlePageIndex + 1
				entryIndex = i
				entryData = mCompleteTomeData.Entries[i]
				isRightPageANewEntry = not (leftPageIndex + 1 > currentEntryTitlePageIndex and leftPageIndex + 1 < nextEntryTitlePageIndex)
				s = s.." was chosen!"
				s = s.."\n"..string.format("Left Page Entry Index: %s"..
					"\nTitle Page Index: %s"..
					"\nNext Entry Title Page Index: %s",
					tostring(leftPageEntryIndex), tostring(currentEntryTitlePageIndex), tostring(nextEntryTitlePageIndex)
					)
			else
				s = s.." was skipped."
			end
		else
			s = s.." was skipped."
		end
		--DebugMessage(s)
	end

	if not entryData then DebugMessage("TomebookDetailPage() Failed! No entryData.",entryData) return end

	local title = entryData.Title
	local pages = entryData.Pages

	-- left page ----
	if pages[leftPageEntryIndex] then
		if leftPageEntryIndex == 1 then
			--dynamicWindow:AddImage(110, 75, "ThinFrameBackgroundExpand", 260, 40, "Sliced")
			dynamicWindow:AddLabel(230, 70,"[43240f]"..tostring(entryIndex)..". "..tostring(title).."[-]",150,0,40,"center",false,false,"Kingthings_Dynamic")
			--dynamicWindow:AddImage(110, 110, "ThinFrameBackgroundExpand", 260, 270, "Sliced")
			dynamicWindow:AddLabel(110, 110,"[43240f]"..pages[leftPageEntryIndex].."[-]",260,270,19,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
		else
			--dynamicWindow:AddImage(110, 75, "ThinFrameBackgroundExpand", 260, 305, "Sliced")
			dynamicWindow:AddLabel(110, 75,"[43240f]"..pages[leftPageEntryIndex].."[-]",260,305,19,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
			dynamicWindow:AddLabel(230,50,"[43240f]"..title.."[-]",260,305,15,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
		end
	end
	dynamicWindow:AddLabel(230,365,"[43240f]"..tostring(leftPageIndex).."[-]",260,340,15,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
	-- end left page ----

	-- right page ----
	if isRightPageANewEntry then
		if not rightPageIndex then rightPageIndex = entryIndex + 1 end
		entryData = mCompleteTomeData.Entries[rightPageIndex]
		title = entryData.Title
		pages = entryData.Pages
		--dynamicWindow:AddImage(420, 75, "ThinFrameBackgroundExpand", 260, 40, "Sliced")
		dynamicWindow:AddLabel(555, 70,"[43240f]"..tostring(rightPageIndex)..". "..tostring(title).."[-]",150,0,40,"center",false,false,"Kingthings_Dynamic")
		--dynamicWindow:AddImage(420, 110, "ThinFrameBackgroundExpand", 260, 270, "Sliced")
		dynamicWindow:AddLabel(420, 110,"[43240f]"..pages[1].."[-]",260,280,19,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
		--DebugMessage("Right Page is new Entry")
	elseif pages[leftPageEntryIndex + 1] then
		--dynamicWindow:AddImage(420, 75, "ThinFrameBackgroundExpand", 260, 305, "Sliced")
		dynamicWindow:AddLabel(420, 75,"[43240f]"..pages[leftPageEntryIndex + 1].."[-]",260,305,19,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
		dynamicWindow:AddLabel(555,50,"[43240f]"..title.."[-]",260,305,15,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
		--DebugMessage("Right Page continues current Entry")
	end
	dynamicWindow:AddLabel(555,365,"[43240f]"..tostring(leftPageIndex + 1).."[-]",260,340,15,"center",false,false,"PermianSlabSerif_Dynamic_Bold")
	-- end right page ----
end

RegisterEventHandler(EventType.DynamicWindowResponse, "Tomebook", function (user,buttonId)
	--DebugMessage("Button Hit:",buttonId)
	local page, arg = buttonId:match("(%a+)|(.+)")
	if page == "Page" then
		mPageTurns = math.ceil(tonumber(arg) / 2 - 1)
		--DebugMessage("Page",arg,"=",mPageTurns,"page turns.")
		--DebugMessage("New mPageTurns:",mPageTurns)
		--DebugMessage("Page Change",arg,buttonId)
		DrawTomebookWindow()
	else
		CleanUp()
	end
end)


RegisterEventHandler(EventType.Message, "OpenTomebook", function(tomebook)
	mTomebookObj = tomebook or mTomebookObj
	mCompleteTomeData = AllTomes[mTomebookObj:GetObjVar("TomeId")]
	mPageTurns = 0
	--mCompleteEntryTitlePapes = {}
	if not (mTomebookObj and mCompleteTomeData) then
		DrawTomebookWindow()
		DebugMessage("Get Tomebook & TomeData failed.",mTomebookObj,mCompleteTomeData)
		return
	end
	mBookEntries = mTomebookObj:GetObjVar("Entries")
	if not mBookEntries then DebugMessage("OpenTomebook() failure: No Entries") end

	local pageCount = 3
	mBookEntries[1].CompleteIndex = pageCount
	--table.insert(mCompleteEntryTitlePapes, pageCount)
	for i=2, #mCompleteTomeData.Entries do
		local entry = mCompleteTomeData.Entries[i] or {}
		pageCount = pageCount + #entry.Pages
		mBookEntries[i].CompleteIndex = pageCount
		--table.insert(mCompleteEntryTitlePapes, pageCount)
	end

	pageCount = 0
	for i=1, #mBookEntries do
		local entry = mCompleteTomeData.Entries[i] or {}
		if mBookEntries[i].LocalIndex ~= 0 then
			pageCount = pageCount + #entry.Pages
		end
	end
	mMaxPageTurns = math.ceil(pageCount / 2)

	--DebugMessage("Opened.",mMaxPageTurns,"Max Page Turns. Entry Start Pages recorded:",DumpTable(mBookEntries))

	DrawTomebookWindow()
end)

RegisterEventHandler(EventType.LoadedFromBackup, "", function ()
	CleanUp()
end)

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(5), "TWABNOCleanup")
end)

-- Spell Window Attached But Not Open Cleanup
RegisterEventHandler(EventType.Timer, "TWABNOCleanup", function()
	if not( mOpen ) then
		this:DelModule(GetCurrentModule())
	end
end)