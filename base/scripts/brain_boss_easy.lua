

-- required for fast identification within require modules
_IS_BRAIN = true

require 'combat'
require 'brain_inc' -- must come after combat, before mobile
require 'base_mobile_advanced'

local fsm

function Start()
    fsm = FSM(this, {
        States.EasyBossDeath,
        --States.Flee,
        States.Attack,
        States.Leash,
        States.Aggro,
        States.AttackAggroList,
        States.FightDamage,
        States.EasyBossWander,
    })

    --fsm.ReplaceState(States.Fight, States.EasyBossPhaseOne)
    fsm.Start()
end

local _OnMobileLoad = OnMobileLoad
function OnMobileLoad()
    _OnMobileLoad()
    Start()
end