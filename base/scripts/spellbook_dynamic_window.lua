require 'incl_magic_sys'

--[[

Seperate module that attaches directly to the user to allow for capturing EventType.DynamicWindowResponse

]]

SPELLBOOK_X_OFFSET = 0.77
SPELLBOOK_Y_OFFSET = 0.77
SPELLBOOK_WIDTH = 789
SPELLBOOK_HEIGHT = 455

SPELLBOOK_DIALOG = "SpellBook"

SPELLBOOK_CALC_WIDTH = SPELLBOOK_WIDTH * SPELLBOOK_X_OFFSET
SPELLBOOK_CALC_HEIGHT = SPELLBOOK_HEIGHT * SPELLBOOK_Y_OFFSET

mSpellBook = nil

-- page types are ManifestationIndex, EvocationIndex, ManifestationSpellDetail, EvocationSpellIndex
mPageType = "EvocationIndex"
mPageNumber = 1

function ShowSpellDetail(dynamicWindow,spellEntry,xOffset)

	local actionData = GetSpellUserAction(spellEntry.Name)

	--DebugTable( actionData )

	-- add spell user action
	dynamicWindow:AddUserAction(
		xOffset+208, --(number) x position in pixels of the window
		60, --(number) y position in pixels of the window
		actionData, --(table) table containing the user action data
		128, --(number) width in pixels of the window (default decided by client)
		128 --(number) height in pixels of the window (default decided by client)
	)


	dynamicWindow:AddImage(xOffset + 146,138,"Prestige_TitleHeader")
	dynamicWindow:AddLabel(xOffset + 240,141,"[43240f]"..(spellEntry.Data.SpellDisplayName or spellEntry.Name).."[-]",150,0,28,"center",false,false,"Kingthings_Dynamic")
	dynamicWindow:AddImage(xOffset + 114,174,"Prestige_Divider",250,0,"Sliced")

	local regStr = GetReagentStr(spellEntry.Data.Reagents)
	local descStr = spellEntry.Data.SpellTooltipString .. "\n\n" .. "Difficulty: "..ToRomanNumerals(spellEntry.Data.Circle or 1) .. "\n\nReagents:\n" .. regStr
	dynamicWindow:AddLabel(xOffset + 114,192,"[43240f]"..descStr.."[-]",220,300,18,"left",false,false,"PermianSlabSerif_Dynamic_Bold")

	--[[
	dynamicWindow:AddLabel(
		xOffset+236, --(number) x position in pixels on the window
		44, --(number) y position in pixels on the window
		"[412A08]"..(spellEntry.Data.SpellDisplayName or spellEntry.Name).."[-]", --(string) text in the label
		0, --(number) width of the text for wrapping purposes (defaults to width of text)
		0, --(number) height of the label (defaults to unlimited, text is not clipped)
		46, --(number) font size (default specific to client)
		"center", --(string) alignment "left", "center", or "right" (default "left")
		false, --(boolean) scrollable (default false)
		false, --(boolean) outline (defaults to false)
		"Kingthings_Calligraphica_Dynamic" --(string) name of font on client (optional)
	)

	dynamicWindow:AddImage(
		xOffset+110, --(number) x position in pixels on the window
		80, --(number) y position in pixels on the window
		"SpellIndexInfo_Divider", --(string) sprite name
		250, --(number) width of the image
		0, --(number) height of the image
		"Sliced" --(string) sprite type Simple, Sliced or Object (defaults to Simple)
		--spriteHue, --(string) sprite hue (defaults to white)
		--opacity --(number) (default 1.0)
	)

	local actionData = GetSpellUserAction(spellEntry.Name)
	-- add spell user action
	dynamicWindow:AddUserAction(
		xOffset+110, --(number) x position in pixels of the window
		100, --(number) y position in pixels of the window
		actionData, --(table) table containing the user action data
		128, --(number) width in pixels of the window (default decided by client)
		128 --(number) height in pixels of the window (default decided by client)
	)

	dynamicWindow:AddLabel(
		xOffset+250, --(number) x position in pixels on the window
		118, --(number) y position in pixels on the window
		"[412A08]Difficulty: "..(spellEntry.Data.Circle or 1).."[-]", --(string) text in the label
		0, --(number) width of the text for wrapping purposes (defaults to width of text)
		0, --(number) height of the label (defaults to unlimited, text is not clipped)
		32, --(number) font size (default specific to client)
		"center", --(string) alignment "left", "center", or "right" (default "left")
		false, --(boolean) scrollable (default false)
		false, --(boolean) outline (defaults to false)
		"Kingthings_Calligraphica_Dynamic" --(string) name of font on client (optional)
	)

	dynamicWindow:AddImage(
		xOffset+110, --(number) x position in pixels on the window
		180, --(number) y position in pixels on the window
		"SpellInfo_ReagentFrame", --(string) sprite name
		250, --(number) width of the image
		190, --(number) height of the image
		"Sliced" --(string) sprite type Simple, Sliced or Object (defaults to Simple)
		--spriteHue, --(string) sprite hue (defaults to white)
		--opacity --(number) (default 1.0)
	)
	
	local regStr = GetReagentStr(spellEntry.Data.Reagents)
	if(regStr ~= "") then
		dynamicWindow:AddLabel(
			xOffset+128, --(number) x position in pixels on the window
			194, --(number) y position in pixels on the window
			"[412A08]"..regStr.."[-]", --(string) text in the label
			0, --(number) width of the text for wrapping purposes (defaults to width of text)
			0, --(number) height of the label (defaults to unlimited, text is not clipped)
			32, --(number) font size (default specific to client)
			"left", --(string) alignment "left", "center", or "right" (default "left")
			false, --(boolean) scrollable (default false)
			false, --(boolean) outline (defaults to false)
			"Kingthings_Calligraphica_Dynamic" --(string) name of font on client (optional)
		)
	end]]
end

function ShowSpellBookDialog()
	if ( circle == nil ) then circle = 1 end

	local castingSkill = "EvocationSkill"
	if(mPageType == "ManifestationIndex" or mPageType == "ManifestationSpellDetail") then
		castingSkill = "ManifestationSkill"
	end

	local detailPageStr = (mPageType == "ManifestationIndex" or mPageType == "ManifestationSpellDetail") and "ManifestationSpellDetail" or "EvocationSpellDetail"
	local isDetailPage = (mPageType == "ManifestationSpellDetail" or mPageType == "EvocationSpellDetail")

	local bookSpells = mSpellBook:GetObjVar("SpellList") or nil
	local spellsSorted = {}
	if(bookSpells) then
		for k,v in pairs(SpellData.AllSpells) do
			if(bookSpells[k] and v.Skill == castingSkill) then
				table.insert(spellsSorted,{Name = k, Data = v})
			end
		end

		table.sort(spellsSorted,
			function(a,b) 
				local circleA = (a.Data.Circle or 0)
				local circleB = (b.Data.Circle or 0)
				if(circleA == circleB) then
					return a.Name < b.Name
				else
					return circleA < circleB
				end
			end)
	end

	local dynamicWindow = DynamicWindow(
		SPELLBOOK_DIALOG, --(string) Window ID used to uniquely identify the window. It is returned in the DynamicWindowResponse event.
		"", --(string) Title of the window for the client UI
		SPELLBOOK_WIDTH, --(number) Width of the window
		SPELLBOOK_HEIGHT, --(number) Height of the window
		-SPELLBOOK_WIDTH/2, --startX, --(number) Starting X position of the window (chosen by client if not specified)
		-SPELLBOOK_HEIGHT/2, --startY, --(number) Starting Y position of the window (chosen by client if not specified)
		"TransparentDraggable",--windowType, --(string) Window type (optional)
		"Center" --windowAnchor --(string) Window anchor (default "TopLeft")
	)

	dynamicWindow:AddImage(
		0, --(number) x position in pixels on the window
		0, --(number) y position in pixels on the window
		"Spellbook", --(string) sprite name
		SPELLBOOK_WIDTH, --(number) width of the image
		SPELLBOOK_HEIGHT --(number) height of the image
		--spriteType, --(string) sprite type Simple, Sliced or Object (defaults to Simple)
		--spriteHue, --(string) sprite hue (defaults to white)
		--opacity --(number) (default 1.0)		
	)

	if(isDetailPage) then		
		local isFirstPage = (mPageNumber == 1)
		local isLastPage = (mPageNumber == math.ceil(#spellsSorted/2))

		if not(isFirstPage) then
			local pageStr = tostring(mPageNumber - 1)
			dynamicWindow:AddButton(
				60, --(number) x position in pixels on the window
				24, --(number) y position in pixels on the window
				"ChangePage|"..detailPageStr.."|"..pageStr, --(string) return id used in the DynamicWindowResponse event
				"", --(string) text in the button (defaults to empty string)
				154, --(number) width of the button (defaults to width of text)
				93,--(number) height of the button (default decided by type of button)
				"", --(string) mouseover tooltip for the button (default blank)
				"", --(string) server command to send on button click (default to none)
				false, --(boolean) should the window close when this button is clicked? (default true)
				"BookPageDown" --(string) button type (default "Default")
				--(string) button state (optional, valid options are default,pressed,disabled)
				--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
			)
		end

		if not(isLastPage) then
			local pageStr = tostring(mPageNumber + 1)
			dynamicWindow:AddButton(
				574, --(number) x position in pixels on the window
				24, --(number) y position in pixels on the window
				"ChangePage|"..detailPageStr.."|"..pageStr, --(string) return id used in the DynamicWindowResponse event
				"", --(string) text in the button (defaults to empty string)
				154, --(number) width of the button (defaults to width of text)
				93,--(number) height of the button (default decided by type of button)
				"", --(string) mouseover tooltip for the button (default blank)
				"", --(string) server command to send on button click (default to none)
				false, --(boolean) should the window close when this button is clicked? (default true)
				"BookPageUp" --(string) button type (default "Default")
				--(string) button state (optional, valid options are default,pressed,disabled)
				--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
			)
		end
	end

	dynamicWindow:AddButton(
			726, --(number) x position in pixels on the window
			22, --(number) y position in pixels on the window
			"", --(string) return id used in the DynamicWindowResponse event
			"", --(string) text in the button (defaults to empty string)
			0, --(number) width of the button (defaults to width of text)
			0,--(number) height of the button (default decided by type of button)
			"", --(string) mouseover tooltip for the button (default blank)
			"", --(string) server command to send on button click (default to none)
			true, --(boolean) should the window close when this button is clicked? (default true)
			"CloseSquare", --(string) button type (default "Default")
			buttonState --(string) button state (optional, valid options are default,pressed,disabled)
			--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
		)

	local buttonState = ""
	if(mPageType == "EvocationIndex" or mPageType == "EvocationSpellDetail") then
		buttonState = "pressed"
	end

	dynamicWindow:AddButton(
			3, --(number) x position in pixels on the window
			70, --(number) y position in pixels on the window
			"ChangePage|EvocationIndex|1", --(string) return id used in the DynamicWindowResponse event
			"", --(string) text in the button (defaults to empty string)
			78, --(number) width of the button (defaults to width of text)
			58,--(number) height of the button (default decided by type of button)
			"", --(string) mouseover tooltip for the button (default blank)
			"", --(string) server command to send on button click (default to none)
			false, --(boolean) should the window close when this button is clicked? (default true)
			"EvocationTab", --(string) button type (default "Default")
			buttonState --(string) button state (optional, valid options are default,pressed,disabled)
			--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
		)

	buttonState = ""
	if(mPageType == "ManifestationIndex" or mPageType == "ManifestationSpellDetail") then
		buttonState = "pressed"
	end

	dynamicWindow:AddButton(
			3, --(number) x position in pixels on the window
			126, --(number) y position in pixels on the window
			"ChangePage|ManifestationIndex|1", --(string) return id used in the DynamicWindowResponse event
			"", --(string) text in the button (defaults to empty string)
			76, --(number) width of the button (defaults to width of text)
			58,--(number) height of the button (default decided by type of button)
			"", --(string) mouseover tooltip for the button (default blank)
			"", --(string) server command to send on button click (default to none)
			false, --(boolean) should the window close when this button is clicked? (default true)
			"ManifestationTab", --(string) button type (default "Default")
			buttonState --(string) button state (optional, valid options are default,pressed,disabled)
			--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
		)	

	if(mPageType == "ManifestationIndex" or mPageType == "EvocationIndex") then
		local labelStr = (mPageType == "ManifestationIndex") and "[412A08]Manifestation[-]" or "[412A08]Evocation[-]"

		dynamicWindow:AddLabel(
			236, --(number) x position in pixels on the window
			44, --(number) y position in pixels on the window
			labelStr, --(string) text in the label
			0, --(number) width of the text for wrapping purposes (defaults to width of text)
			0, --(number) height of the label (defaults to unlimited, text is not clipped)
			46, --(number) font size (default specific to client)
			"center", --(string) alignment "left", "center", or "right" (default "left")
			false, --(boolean) scrollable (default false)
			false, --(boolean) outline (defaults to false)
			"Kingthings_Calligraphica_Dynamic" --(string) name of font on client (optional)
		)

		dynamicWindow:AddImage(
			110, --(number) x position in pixels on the window
			80, --(number) y position in pixels on the window
			"SpellIndexInfo_Divider", --(string) sprite name
			250, --(number) width of the image
			0, --(number) height of the image
			"Sliced" --(string) sprite type Simple, Sliced or Object (defaults to Simple)
			--spriteHue, --(string) sprite hue (defaults to white)
			--opacity --(number) (default 1.0)
		)		

		local xOffset = 110
		local yOffset = 90
		local spellIndex = 1
		for i,spellEntry in pairs(spellsSorted) do
			local pageStr = tostring(math.ceil(spellIndex/2))
			dynamicWindow:AddButton(
				xOffset, --(number) x position in pixels on the window
				yOffset, --(number) y position in pixels on the window
				"ChangePage|"..detailPageStr.."|"..pageStr, --(string) return id used in the DynamicWindowResponse event
				ToRomanNumerals(spellEntry.Data.Circle or 1).."|"..(spellEntry.Data.SpellDisplayName or spellEntry.Name), --(string) text in the button (defaults to empty string)
				250, --(number) width of the button (defaults to width of text)
				34,--(number) height of the button (default decided by type of button)
				"", --(string) mouseover tooltip for the button (default blank)
				"", --(string) server command to send on button click (default to none)
				false, --(boolean) should the window close when this button is clicked? (default true)
				"BookList" --(string) button type (default "Default")
				--(string) button state (optional, valid options are default,pressed,disabled)
				--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
			)

			yOffset = yOffset + 36
			spellIndex = spellIndex + 1
			if(spellIndex == 9) then
				yOffset = 48
				xOffset = xOffset + 320
			end
		end

		dynamicWindow:AddButton(
				xOffset, --(number) x position in pixels on the window
				yOffset, --(number) y position in pixels on the window
				"Drop", --(string) return id used in the DynamicWindowResponse event
				"+|Drop spell scroll here", --(string) text in the button (defaults to empty string)
				250, --(number) width of the button (defaults to width of text)
				34,--(number) height of the button (default decided by type of button)
				"", --(string) mouseover tooltip for the button (default blank)
				"", --(string) server command to send on button click (default to none)
				false, --(boolean) should the window close when this button is clicked? (default true)
				"BookList", --(string) button type (default "Default")
				"faded"--(string) button state (optional, valid options are default,pressed,disabled)
				--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
			)
	else		
		local xOffset = 0
		local castingSkill = (mPageType == "ManifestationSpellDetail") and "ManifestationSkill" or "EvocationSkill"
		local spellIndex = 1
		local spellStart = ((mPageNumber-1) * 2) + 1
		for i,spellEntry in pairs(spellsSorted) do
			if(spellIndex >= spellStart and spellIndex <= spellStart + 1) then
				ShowSpellDetail(dynamicWindow,spellEntry,xOffset)
				xOffset = 320
			end
			spellIndex = spellIndex + 1
		end			
	end

	this:OpenDynamicWindow(dynamicWindow)
	mOpen = true

end

function ShowSongRiddleDetail(dynamicWindow,songEntry,xOffset,abilityType)

	--DebugMessage( "Song: " .. songEntry .. " and " .. abilityType  )

	local actionData = GetPrestigeAbilityUserAction(this, nil,"Barding",songEntry)
	local abilityData = PrestigeData.Barding.Abilities[songEntry]
	--DebugTable( actionData )

	-- add spell user action
	dynamicWindow:AddUserAction(
		xOffset+208, --(number) x position in pixels of the window
		60, --(number) y position in pixels of the window
		actionData, --(table) table containing the user action data
		128, --(number) width in pixels of the window (default decided by client)
		128 --(number) height in pixels of the window (default decided by client)
	)

	dynamicWindow:AddImage(xOffset + 146,138,"Prestige_TitleHeader")
	dynamicWindow:AddLabel(xOffset + 240,141,"[43240f]"..(abilityData.Action.DisplayName).."[-]",150,0,28,"center",false,false,"Kingthings_Dynamic")
	dynamicWindow:AddImage(xOffset + 114,174,"Prestige_Divider",250,0,"Sliced")

	local descStr = abilityData.Tooltip .. "\n\n".. "Difficulty: "..abilityData.Rank or 1
	dynamicWindow:AddLabel(xOffset + 114,192,"[43240f]"..descStr.."[-]",220,300,18,"left",false,false,"PermianSlabSerif_Dynamic_Bold")


end

function ShowSongBookDialog()

	local bardSongType = "Songs"
	local detailPageStr = "SongsDetail"
	if(mPageType == "RiddlesIndex" or mPageType == "RiddlesDetail") then
		bardSongType = "Riddles"
		detailPageStr = "RiddlesDetail"
	end

	if( mPageType == "EvocationIndex" ) then mPageType = "SongsIndex" end

	local isDetailPage = (mPageType == "RiddlesDetail" or mPageType == "SongsDetail")
	local counts = { SongsIndex = 0, RiddlesIndex = 0 }

	-- Get song and riddle counts
	local songCount = 0
	local riddleCount = 0
	for i,entry in pairs(PrestigeData.Barding.Abilities) do
		if( entry.AbilityType == "Song" ) then
			songCount = songCount + 1
		elseif( entry.AbilityType == "Riddle" ) then
			riddleCount = riddleCount + 1
		end
	end

	counts.SongsIndex = songCount
	counts.SongsDetail = songCount
	counts.RiddlesIndex = riddleCount
	counts.RiddlesDetail = riddleCount

	-- BASE WINDOW
	local dynamicWindow = DynamicWindow(
		SPELLBOOK_DIALOG, --(string) Window ID used to uniquely identify the window. It is returned in the DynamicWindowResponse event.
		"", --(string) Title of the window for the client UI
		SPELLBOOK_WIDTH, --(number) Width of the window
		SPELLBOOK_HEIGHT, --(number) Height of the window
		-SPELLBOOK_WIDTH/2, --startX, --(number) Starting X position of the window (chosen by client if not specified)
		-SPELLBOOK_HEIGHT/2, --startY, --(number) Starting Y position of the window (chosen by client if not specified)
		"TransparentDraggable",--windowType, --(string) Window type (optional)
		"Center" --windowAnchor --(string) Window anchor (default "TopLeft")
	)

	-- SPELLBOOK BACKGROUND
	dynamicWindow:AddImage(
		0, --(number) x position in pixels on the window
		0, --(number) y position in pixels on the window
		"Spellbook", --(string) sprite name
		SPELLBOOK_WIDTH, --(number) width of the image
		SPELLBOOK_HEIGHT --(number) height of the image
		--spriteType, --(string) sprite type Simple, Sliced or Object (defaults to Simple)
		--spriteHue, --(string) sprite hue (defaults to white)
		--opacity --(number) (default 1.0)		
	)

	if(isDetailPage) then		
		local isFirstPage = (mPageNumber == 1)
		local isLastPage = (mPageNumber == math.ceil(counts[mPageType]/2))

		if not(isFirstPage) then
			local pageStr = tostring(mPageNumber - 1)
			dynamicWindow:AddButton(
				60, --(number) x position in pixels on the window
				24, --(number) y position in pixels on the window
				"ChangePage|"..detailPageStr.."|"..pageStr, --(string) return id used in the DynamicWindowResponse event
				"", --(string) text in the button (defaults to empty string)
				154, --(number) width of the button (defaults to width of text)
				93,--(number) height of the button (default decided by type of button)
				"", --(string) mouseover tooltip for the button (default blank)
				"", --(string) server command to send on button click (default to none)
				false, --(boolean) should the window close when this button is clicked? (default true)
				"BookPageDown" --(string) button type (default "Default")
				--(string) button state (optional, valid options are default,pressed,disabled)
				--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
			)
		end

		if not(isLastPage) then
			local pageStr = tostring(mPageNumber + 1)
			dynamicWindow:AddButton(
				574, --(number) x position in pixels on the window
				24, --(number) y position in pixels on the window
				"ChangePage|"..detailPageStr.."|"..pageStr, --(string) return id used in the DynamicWindowResponse event
				"", --(string) text in the button (defaults to empty string)
				154, --(number) width of the button (defaults to width of text)
				93,--(number) height of the button (default decided by type of button)
				"", --(string) mouseover tooltip for the button (default blank)
				"", --(string) server command to send on button click (default to none)
				false, --(boolean) should the window close when this button is clicked? (default true)
				"BookPageUp" --(string) button type (default "Default")
				--(string) button state (optional, valid options are default,pressed,disabled)
				--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
			)
		end
	end

	-- CLOSE BUTTON
	dynamicWindow:AddButton(
		726, --(number) x position in pixels on the window
		22, --(number) y position in pixels on the window
		"", --(string) return id used in the DynamicWindowResponse event
		"", --(string) text in the button (defaults to empty string)
		0, --(number) width of the button (defaults to width of text)
		0,--(number) height of the button (default decided by type of button)
		"", --(string) mouseover tooltip for the button (default blank)
		"", --(string) server command to send on button click (default to none)
		true, --(boolean) should the window close when this button is clicked? (default true)
		"CloseSquare", --(string) button type (default "Default")
		buttonState --(string) button state (optional, valid options are default,pressed,disabled)
		--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
	)

	-- SONG TAB
	local buttonState = ""
	if(bardSongType == "Songs" or mPageType == "SongsIndex") then
		buttonState = "pressed"
	end
	dynamicWindow:AddButton(
		3, --(number) x position in pixels on the window
		70, --(number) y position in pixels on the window
		"ChangePage|SongsIndex|1", --(string) return id used in the DynamicWindowResponse event
		"", --(string) text in the button (defaults to empty string)
		78, --(number) width of the button (defaults to width of text)
		58,--(number) height of the button (default decided by type of button)
		"", --(string) mouseover tooltip for the button (default blank)
		"", --(string) server command to send on button click (default to none)
		false, --(boolean) should the window close when this button is clicked? (default true)
		"SongTab", --(string) button type (default "Default")
		--"SongsTab", --(string) button type (default "Default")
		buttonState --(string) button state (optional, valid options are default,pressed,disabled)
		--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
	)

	-- RIDDLES TAB
	buttonState = ""
	if(bardSongType == "Riddles" or mPageType == "RiddlesIndex") then
		buttonState = "pressed"
	end
	dynamicWindow:AddButton(
		3, --(number) x position in pixels on the window
		126, --(number) y position in pixels on the window
		"ChangePage|RiddlesIndex|1", --(string) return id used in the DynamicWindowResponse event
		"", --(string) text in the button (defaults to empty string)
		76, --(number) width of the button (defaults to width of text)
		58,--(number) height of the button (default decided by type of button)
		"", --(string) mouseover tooltip for the button (default blank)
		"", --(string) server command to send on button click (default to none)
		false, --(boolean) should the window close when this button is clicked? (default true)
		"RiddleTab", --(string) button type (default "Default")
		--"RiddlesTab", --(string) button type (default "Default")
		buttonState --(string) button state (optional, valid options are default,pressed,disabled)
		--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
	)

	-- TAB LABEL
	--DebugMessage("mPageType = " .. mPageType)
	if(mPageType == "RiddlesIndex" or mPageType == "SongsIndex") then
		local labelStr = (bardSongType == "Riddles") and "[412A08]Riddles[-]" or "[412A08]Songs[-]"

		dynamicWindow:AddLabel(
			236, --(number) x position in pixels on the window
			44, --(number) y position in pixels on the window
			labelStr, --(string) text in the label
			0, --(number) width of the text for wrapping purposes (defaults to width of text)
			0, --(number) height of the label (defaults to unlimited, text is not clipped)
			46, --(number) font size (default specific to client)
			"center", --(string) alignment "left", "center", or "right" (default "left")
			false, --(boolean) scrollable (default false)
			false, --(boolean) outline (defaults to false)
			"Kingthings_Calligraphica_Dynamic" --(string) name of font on client (optional)
		)

		dynamicWindow:AddImage(
			110, --(number) x position in pixels on the window
			80, --(number) y position in pixels on the window
			"SpellIndexInfo_Divider", --(string) sprite name
			250, --(number) width of the image
			0, --(number) height of the image
			"Sliced" --(string) sprite type Simple, Sliced or Object (defaults to Simple)
			--spriteHue, --(string) sprite hue (defaults to white)
			--opacity --(number) (default 1.0)
		)	

		-- LIST ALL SONGS/RIDDLES
		local xOffset = 110
		local yOffset = 90
		local itemIndex = 1
		local myList = PrestigeData.Barding.Abilities

		local myListSorted = {}
		if( myList ) then

			for k,v in pairs(PrestigeData.Barding.Abilities) do
				table.insert(myListSorted,{Name = k, Data = v})
			end

			table.sort(myListSorted,
			function(a,b) 
				local RankA = (a.Data.Rank or 0)
				local RankB = (b.Data.Rank or 0)
				if(RankA == RankB) then
					return a.Data.Action.DisplayName < b.Data.Action.DisplayName
				else
					return RankA < RankB
				end
			end)

		end

		for i,entry in pairs(myListSorted) do
			entry = entry.Data
			if(detailPageStr == "SongsDetail" and entry.AbilityType == "Song" or detailPageStr == "RiddlesDetail" and entry.AbilityType == "Riddle" )  then 

				local pageStr = tostring(math.ceil(itemIndex/2))
				dynamicWindow:AddButton(
					xOffset, --(number) x position in pixels on the window
					yOffset, --(number) y position in pixels on the window
					"ChangePage|"..detailPageStr.."|"..pageStr, --(string) return id used in the DynamicWindowResponse event
					(entry.Rank or 1).."|"..(entry.Action.DisplayName), --(string) text in the button (defaults to empty string)
					250, --(number) width of the button (defaults to width of text)
					34,--(number) height of the button (default decided by type of button)
					"", --(string) mouseover tooltip for the button (default blank)
					"", --(string) server command to send on button click (default to none)
					false, --(boolean) should the window close when this button is clicked? (default true)
					"BookList" --(string) button type (default "Default")
					--(string) button state (optional, valid options are default,pressed,disabled)
					--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
				)

				yOffset = yOffset + 36
				itemIndex = itemIndex + 1
				if(itemIndex == 9) then
					yOffset = 48
					xOffset = xOffset + 320
				end
			end
		end

	else

		local xOffset = 0
		local bardSongType = (mPageType == "RiddlesDetail") and "Riddles" or "Songs"
		local songIndex = 1
		local songStart = ((mPageNumber-1) * 2) + 1
		local myList = PrestigeData.Barding.Abilities

		local myListSorted = {}
		if( myList ) then

			for k,v in pairs(PrestigeData.Barding.Abilities) do
				table.insert(myListSorted,{Name = k, Data = v})
			end

			table.sort(myListSorted,
			function(a,b) 
				local RankA = (a.Data.Rank or 0)
				local RankB = (b.Data.Rank or 0)
				if(RankA == RankB) then
					return a.Data.Action.DisplayName < b.Data.Action.DisplayName
				else
					return RankA < RankB
				end
			end)

		end

		for k,entry in pairs(myListSorted) do
			local abilityName = entry.Name
			entry = entry.Data
			if(mPageType == "SongsDetail" and entry.AbilityType == "Song" or mPageType == "RiddlesDetail" and entry.AbilityType == "Riddle" )  then 
				if(songIndex >= songStart and songIndex <= songStart + 1) then
					ShowSongRiddleDetail(dynamicWindow,abilityName,xOffset,bardSongType)
					xOffset = 320
				end
				songIndex = songIndex + 1
			end
		end	

	end

	this:OpenDynamicWindow(dynamicWindow)
	mOpen = true
end

function ShowMartialAbilityDetail(dynamicWindow,martialEntry,xOffset,abilityType)
	local actionData = GetPrestigeAbilityUserAction(this, nil,"Martial",martialEntry)
	local abilityData = PrestigeData.Martial.Abilities[martialEntry]

	-- add spell user action
	dynamicWindow:AddUserAction(
		xOffset+208, --(number) x position in pixels of the window
		60, --(number) y position in pixels of the window
		actionData, --(table) table containing the user action data
		128, --(number) width in pixels of the window (default decided by client)
		128 --(number) height in pixels of the window (default decided by client)
	)

	dynamicWindow:AddImage(xOffset + 146,138,"Prestige_TitleHeader")
	dynamicWindow:AddLabel(xOffset + 240,141,"[43240f]"..(abilityData.Action.DisplayName).."[-]",150,0,28,"center",false,false,"Kingthings_Dynamic")
	dynamicWindow:AddImage(xOffset + 114,174,"Prestige_Divider",250,0,"Sliced")

	local descStr = abilityData.Tooltip .. "\n\n".. "Difficulty: "..abilityData.Rank or 1
	dynamicWindow:AddLabel(xOffset + 114,192,"[43240f]"..descStr.."[-]",220,300,18,"left",false,false,"PermianSlabSerif_Dynamic_Bold")


end

function ShowMartialBookDialog()

	local martialAbilityType = "Martials"
	local detailPageStr = "MartialsDetail"
	if(mPageType == "MartialsDetail" or mPageType == "MartialsDetail") then
		martialAbilityType = "Martials"
		detailPageStr = "MartialsDetail"
	end

	if( mPageType == "EvocationIndex" ) then mPageType = "MartialsIndex" end

	local isDetailPage = (mPageType == "MartialsDetail")
	local counts = { MartialsIndex = 0 }

	-- Get martial count
	local martialCount = 0
	for i,entry in pairs(PrestigeData.Martial.Abilities) do
		if ( entry.AbilityType == "Martial" ) then
			martialCount = martialCount + 1
		elseif ( entry.AbilityType == "Martial" ) then
			martialCount = martialCount + 1
		end
	end

	counts.MartialsIndex = martialCount
	counts.MartialsDetail = martialCount

	-- BASE WINDOW
	local dynamicWindow = DynamicWindow(
		SPELLBOOK_DIALOG, --(string) Window ID used to uniquely identify the window. It is returned in the DynamicWindowResponse event.
		"", --(string) Title of the window for the client UI
		SPELLBOOK_WIDTH, --(number) Width of the window
		SPELLBOOK_HEIGHT, --(number) Height of the window
		-SPELLBOOK_WIDTH/2, --startX, --(number) Starting X position of the window (chosen by client if not specified)
		-SPELLBOOK_HEIGHT/2, --startY, --(number) Starting Y position of the window (chosen by client if not specified)
		"TransparentDraggable",--windowType, --(string) Window type (optional)
		"Center" --windowAnchor --(string) Window anchor (default "TopLeft")
	)

	-- SPELLBOOK BACKGROUND
	dynamicWindow:AddImage(
		0, --(number) x position in pixels on the window
		0, --(number) y position in pixels on the window
		"Spellbook", --(string) sprite name
		SPELLBOOK_WIDTH, --(number) width of the image
		SPELLBOOK_HEIGHT --(number) height of the image
		--spriteType, --(string) sprite type Simple, Sliced or Object (defaults to Simple)
		--spriteHue, --(string) sprite hue (defaults to white)
		--opacity --(number) (default 1.0)		
	)

	if(isDetailPage) then		
		local isFirstPage = (mPageNumber == 1)
		local isLastPage = (mPageNumber == math.ceil(counts[mPageType]/2))

		if not(isFirstPage) then
			local pageStr = tostring(mPageNumber - 1)
			dynamicWindow:AddButton(
				60, --(number) x position in pixels on the window
				24, --(number) y position in pixels on the window
				"ChangePage|"..detailPageStr.."|"..pageStr, --(string) return id used in the DynamicWindowResponse event
				"", --(string) text in the button (defaults to empty string)
				154, --(number) width of the button (defaults to width of text)
				93,--(number) height of the button (default decided by type of button)
				"", --(string) mouseover tooltip for the button (default blank)
				"", --(string) server command to send on button click (default to none)
				false, --(boolean) should the window close when this button is clicked? (default true)
				"BookPageDown" --(string) button type (default "Default")
				--(string) button state (optional, valid options are default,pressed,disabled)
				--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
			)
		end

		if not(isLastPage) then
			local pageStr = tostring(mPageNumber + 1)
			dynamicWindow:AddButton(
				574, --(number) x position in pixels on the window
				24, --(number) y position in pixels on the window
				"ChangePage|"..detailPageStr.."|"..pageStr, --(string) return id used in the DynamicWindowResponse event
				"", --(string) text in the button (defaults to empty string)
				154, --(number) width of the button (defaults to width of text)
				93,--(number) height of the button (default decided by type of button)
				"", --(string) mouseover tooltip for the button (default blank)
				"", --(string) server command to send on button click (default to none)
				false, --(boolean) should the window close when this button is clicked? (default true)
				"BookPageUp" --(string) button type (default "Default")
				--(string) button state (optional, valid options are default,pressed,disabled)
				--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
			)
		end
	end

	-- CLOSE BUTTON
	dynamicWindow:AddButton(
		726, --(number) x position in pixels on the window
		22, --(number) y position in pixels on the window
		"", --(string) return id used in the DynamicWindowResponse event
		"", --(string) text in the button (defaults to empty string)
		0, --(number) width of the button (defaults to width of text)
		0,--(number) height of the button (default decided by type of button)
		"", --(string) mouseover tooltip for the button (default blank)
		"", --(string) server command to send on button click (default to none)
		true, --(boolean) should the window close when this button is clicked? (default true)
		"CloseSquare", --(string) button type (default "Default")
		buttonState --(string) button state (optional, valid options are default,pressed,disabled)
		--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
	)

	-- MARTIALS TAB
	local buttonState = ""
	if(martialAbilityType == "Martials" or mPageType == "MartialsIndex") then
		buttonState = "pressed"
	end
	dynamicWindow:AddButton(
		3, --(number) x position in pixels on the window
		70, --(number) y position in pixels on the window
		"ChangePage|MartialsIndex|1", --(string) return id used in the DynamicWindowResponse event
		"", --(string) text in the button (defaults to empty string)
		78, --(number) width of the button (defaults to width of text)
		58,--(number) height of the button (default decided by type of button)
		"", --(string) mouseover tooltip for the button (default blank)
		"", --(string) server command to send on button click (default to none)
		false, --(boolean) should the window close when this button is clicked? (default true)
		--"Default", --(string) button type (default "Default")
		"EvocationTab", --(string) button type (default "Default")
		buttonState --(string) button state (optional, valid options are default,pressed,disabled)
		--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
	)

	-- TAB LABEL
	--DebugMessage("mPageType = " .. mPageType)
	if (mPageType == "MartialsIndex") then
		local labelStr = (martialAbilityType == "Martials") and "[412A08]Martials[-]"

		dynamicWindow:AddLabel(
			236, --(number) x position in pixels on the window
			44, --(number) y position in pixels on the window
			labelStr, --(string) text in the label
			0, --(number) width of the text for wrapping purposes (defaults to width of text)
			0, --(number) height of the label (defaults to unlimited, text is not clipped)
			46, --(number) font size (default specific to client)
			"center", --(string) alignment "left", "center", or "right" (default "left")
			false, --(boolean) scrollable (default false)
			false, --(boolean) outline (defaults to false)
			"Kingthings_Calligraphica_Dynamic" --(string) name of font on client (optional)
		)

		dynamicWindow:AddImage(
			110, --(number) x position in pixels on the window
			80, --(number) y position in pixels on the window
			"SpellIndexInfo_Divider", --(string) sprite name
			250, --(number) width of the image
			0, --(number) height of the image
			"Sliced" --(string) sprite type Simple, Sliced or Object (defaults to Simple)
			--spriteHue, --(string) sprite hue (defaults to white)
			--opacity --(number) (default 1.0)
		)	

		-- LIST ALL SONGS/RIDDLES
		local xOffset = 110
		local yOffset = 90
		local itemIndex = 1
		local myList = PrestigeData.Martial.Abilities

		local myListSorted = {}
		if( myList ) then

			for k,v in pairs(PrestigeData.Martial.Abilities) do
				table.insert(myListSorted,{Name = k, Data = v})
			end

			table.sort(myListSorted,
			function(a,b) 
				local RankA = (a.Data.Rank or 0)
				local RankB = (b.Data.Rank or 0)
				if(RankA == RankB) then
					return a.Data.Action.DisplayName < b.Data.Action.DisplayName
				else
					return RankA < RankB
				end
			end)

		end

		for i,entry in pairs(myListSorted) do
			entry = entry.Data
			if(detailPageStr == "MartialsDetail" and entry.AbilityType == "Martial" )  then 

				local pageStr = tostring(math.ceil(itemIndex/2))
				dynamicWindow:AddButton(
					xOffset, --(number) x position in pixels on the window
					yOffset, --(number) y position in pixels on the window
					"ChangePage|"..detailPageStr.."|"..pageStr, --(string) return id used in the DynamicWindowResponse event
					(entry.Rank or 1).."|"..(entry.Action.DisplayName), --(string) text in the button (defaults to empty string)
					250, --(number) width of the button (defaults to width of text)
					34,--(number) height of the button (default decided by type of button)
					"", --(string) mouseover tooltip for the button (default blank)
					"", --(string) server command to send on button click (default to none)
					false, --(boolean) should the window close when this button is clicked? (default true)
					"BookList" --(string) button type (default "Default")
					--(string) button state (optional, valid options are default,pressed,disabled)
					--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
				)

				yOffset = yOffset + 36
				itemIndex = itemIndex + 1
				if(itemIndex == 9) then
					yOffset = 48
					xOffset = xOffset + 320
				end
			end
		end

	else

		local xOffset = 0
		local martialAbilityType = (mPageType == "MartialsDetail") and "Martials"
		local martialIndex = 1
		local martialStart = ((mPageNumber-1) * 2) + 1
		local myList = PrestigeData.Martial.Abilities

		local myListSorted = {}
		if( myList ) then

			for k,v in pairs(PrestigeData.Martial.Abilities) do
				table.insert(myListSorted,{Name = k, Data = v})
			end

			table.sort(myListSorted,
			function(a,b) 
				local RankA = (a.Data.Rank or 0)
				local RankB = (b.Data.Rank or 0)
				if(RankA == RankB) then
					return a.Data.Action.DisplayName < b.Data.Action.DisplayName
				else
					return RankA < RankB
				end
			end)

		end

		for k,entry in pairs(myListSorted) do
			local abilityName = entry.Name
			entry = entry.Data
			if(mPageType == "MartialsDetail" and entry.AbilityType == "Martial" )  then 
				if(martialIndex >= martialStart and martialIndex <= martialStart + 1) then
					ShowMartialAbilityDetail(dynamicWindow,abilityName,xOffset,martialAbilityType)
					xOffset = 320
				end
				martialIndex = martialIndex + 1
			end
		end	

	end

	this:OpenDynamicWindow(dynamicWindow)
	mOpen = true
end

function OpenSpellbook(user)
	-- make the dynamic window transparent and dragable, and center it on the screen
	local dynamicWindow = DynamicWindow("customSpellBookHandler","", 790, 486, -395, -243, "TransparentDraggable","Center")
	-- add the spellbook artwork
	dynamicWindow:AddImage(0, 0, "Spellbook", 789, 486)

	-- show the spellbook user interface
	-- close button
	dynamicWindow:AddButton(726, 22, "", "", 0, 0, "", "", true, "CloseSquare")
	
	-- show the spells inside the book
	local x = 120
	local y = 45
	local spellListCount = 0
	local spellList = this:GetObjVar("SpellList") or {}	
	local sortedList = {}

	-- set up sorting table to alphabatize the spell list by spell name
	for key,value in pairs(spellList) do
		table.insert(sortedList,{Name=SpellData.AllSpells[key].SpellDisplayName,Key=key})
	end

	-- sort the spells alphabetically to display in the book
	table.sort(sortedList, function(a,b) 
		return a.Name < b.Name 
	end)

	-- loop through the spells and add their UserAction buttons
	for key,value in pairs(sortedList) do
		spellListCount = spellListCount + 1
		-- only 7 spells per page so switch the y value back to the top
		if(spellListCount == 8) then y = 45 end
		if(spellListCount <= 7) then
			AddUserAction(dynamicWindow,x,y,value.Key)
			dynamicWindow:AddLabel(x+50,y+14,"[565656]"..SpellData.AllSpells[value.Key].SpellDisplayName,200,30,26,"left")
			y = y + 50
		else
			AddUserAction(dynamicWindow,x+310,y,value.Key)
			dynamicWindow:AddLabel(x+360,y+14,"[565656]"..SpellData.AllSpells[value.Key].SpellDisplayName,200,30,26,"left")
			y = y + 50			
		end		
	end
	
	-- show the spellbook to the player and setting this spellbook as the event handler for the dynamic window
	user:OpenDynamicWindow(dynamicWindow,this)
end



RegisterEventHandler(EventType.Message, "OpenSpellBook", 
	function(spellBook,pageType)
		if ( spellBook == nil ) then
			DebugMessage("[spellbook_dynamic_window|Message:OpenSpellBook] ERROR: spellBook is nil")
			return
		end
		if(pageType) then
			mPageType = pageType
			mPageNumber = 1
		end
		mSpellBook = spellBook
		local bookType = getBookType(mSpellBook)
		if ( bookType ) then
			if ( bookType == "Spellbook" ) then
				ShowSpellBookDialog()
			elseif ( bookType == "Songbook" ) then
				ShowSongBookDialog()
			elseif ( bookType == "Martialbook" ) then
				ShowMartialBookDialog()
				--SCAN ADDED
			elseif ( bookType == "PyromancyBook" ) then
				OpenSpellbook()
			elseif ( bookType == "NatureBook" ) then
				OpenSpellbook()
			elseif ( bookType == "DemonologyBook" ) then
				OpenSpellbook()
			end
		end

	end)

RegisterEventHandler(EventType.DynamicWindowResponse, SPELLBOOK_DIALOG, function(user, returnId)
	if(returnId ~= nil) then
		--DebugMessage(returnId)
		local result = StringSplit(returnId, "|")
		if(result[1] == "ChangePage") then			
			mPageType = result[2]
			mPageNumber = tonumber(result[3])
			local bookType = getBookType(mSpellBook)
			if ( bookType ) then
				if ( bookType == "Spellbook" ) then
					ShowSpellBookDialog()
				elseif ( bookType == "Songbook" ) then
					ShowSongBookDialog()
				elseif ( bookType == "Martialbook" ) then
					ShowMartialBookDialog()
					--SCAN ADDED
				elseif ( bookType == "PyromancyBook" ) then
					OpenSpellbook()
				elseif ( bookType == "NatureBook" ) then
					OpenSpellbook()
				elseif ( bookType == "DemonologyBook" ) then
					OpenSpellbook()
				end
			end
			return
		elseif(returnId == "Drop") then
			if(getBookType(mSpellBook) ~= "Spellbook") then return end
			local carriedObject = user:CarriedObject()			
			if(carriedObject ) then
				local spell = carriedObject:GetObjVar("Spell")
				if(spell and SpellData.AllSpells[spell]) then	
					TryAddSpellToSpellbook(mSpellBook,carriedObject,user)
				end
			end
			return
		end
	end

	this:DelModule("spellbook_dynamic_window")
end)

RegisterEventHandler(EventType.ClientObjectCommand, "dropAction",
	function (user,sourceId,actionType,actionId,slot)
		--DebugMessage(1)
		--DebugMessage(user,sourceId,actionType,actionId,slot)
		if((sourceId == SPELLBOOK_DIALOG) and slot ~= nil) then
			local hotbarAction = nil

			if(getBookType(mSpellBook) == "Spellbook" ) then
				hotbarAction = GetSpellUserAction(actionId)
			else
				local prestigeClass, prestigeName = actionId:match("pa_(%a+)_(%a+)")
				hotbarAction = GetPrestigeAbilityUserAction(this, nil,prestigeClass,prestigeName)
			end
			
			hotbarAction.Slot = tonumber(slot)
			RequestAddUserActionToSlot(user,hotbarAction)
		end
	end)

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), 
	function()
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(5), "SWABNOCleanup")
	end)

-- Spell Window Attached But Not Open Cleanup
RegisterEventHandler(EventType.Timer, "SWABNOCleanup",
	function()
		if not( mOpen ) then
			this:DelModule("spellbook_dynamic_window")
		end
	end)