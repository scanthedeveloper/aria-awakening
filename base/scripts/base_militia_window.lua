
MILITIA_WINDOW_WIDTH = 400
MILITIA_WINDOW_HEIGHT = 760

ClaimCustomSprites = {"Lockbox","Lockbox_Hover","Lockbox_Hover","Lockbox_Disabled"}

local mCurrentTab = "Militia"
local redeeming = false

function GetRankRewards()
	local militiaRankRewards = this:GetObjVar("MilitiaRankRewards") or {}
		
	if(militiaRankRewards.Season and militiaRankRewards.Season ~= ServerSettings.Militia.CurSeason) then
		militiaRankRewards = { Season=ServerSettings.Militia.CurSeason }
		this:SetObjVar("MilitiaRankRewards")
	end

	return militiaRankRewards
end

function ReputationInfo()
	local dynamicWindow = DynamicWindow(
		"Militia", --(string) Window ID used to uniquely identify the window. It is returned in the DynamicWindowResponse event.
		"Militia & Favor", --(string) Title of the window for the client UI
		MILITIA_WINDOW_WIDTH, --(number) Width of the window
		MILITIA_WINDOW_HEIGHT --(number) Height of the window
		--nil, --startX, --(number) Starting X position of the window (chosen by client if not specified)
		--nil, --startY, --(number) Starting Y position of the window (chosen by client if not specified)
		--"TransparentDraggable",--windowType, --(string) Window type (optional)
		--"Center" --windowAnchor --(string) Window anchor (default "TopLeft")
	)

	AddTabMenu(dynamicWindow,
	{
        ActiveTab = mCurrentTab, 
        Buttons = {{Text="Militia"}, {Text="Favor"}}
    })	

	dynamicWindow:AddImage(10,32,"DropHeaderBackground",362,670,"Sliced")

	if(mCurrentTab == "Militia") then
		MilitiaInfo( dynamicWindow )
	else
		local curY = 46

		dynamicWindow:AddLabel(190,curY,"[F2F5A9]Leagues[-]",500,32,24,"center")
		curY = curY + 30

		local curX = 34

		dynamicWindow:AddImage(curX,curY,"BasicWindow_Panel",156,186,"Sliced")
		dynamicWindow:AddImage(curX+40,curY+20,QuestsLeague.LeagueIcon.Dungeoneers,76,106)
		dynamicWindow:AddLabel(curX+80,curY+134,"League of Dungeoneers",500,32,16,"center")
		dynamicWindow:AddLabel(curX+80,curY+158,"[F2F5A9]"..tostring(QuestsLeague.GetLeagueCurrecy(this, "Dungeoneers")).." / " .. QuestsLeague.MaximumBankedFavor .."[-]",100,32,20,"center")

		curX = curX + 160

		dynamicWindow:AddImage(curX,curY,"BasicWindow_Panel",156,186,"Sliced")
		dynamicWindow:AddImage(curX+40,curY+20,QuestsLeague.LeagueIcon.Artisans,76,106)
		dynamicWindow:AddLabel(curX+80,curY+134,"League of Artisans",500,32,16,"center")
		dynamicWindow:AddLabel(curX+80,curY+158,"[F2F5A9]"..tostring(QuestsLeague.GetLeagueCurrecy(this, "Artisans")).." / " .. QuestsLeague.MaximumBankedFavor .."[-]",100,32,20,"center")
		
		curY = curY + 190
		curX = 34

		dynamicWindow:AddImage(curX,curY,"BasicWindow_Panel",156,186,"Sliced")
		dynamicWindow:AddImage(curX+40,curY+20,QuestsLeague.LeagueIcon.Procurers,76,106)
		dynamicWindow:AddLabel(curX+80,curY+134,"League of Procurers",500,32,16,"center")
		dynamicWindow:AddLabel(curX+80,curY+158,"[F2F5A9]"..tostring(QuestsLeague.GetLeagueCurrecy(this, "Procurers")).." / " .. QuestsLeague.MaximumBankedFavor .. "[-]",140,32,20,"center")

		curX = curX + 160

		dynamicWindow:AddImage(curX,curY,"BasicWindow_Panel",156,186,"Sliced")
		dynamicWindow:AddImage(curX+40,curY+20,QuestsLeague.LeagueIcon.Explorers,76,106)
		dynamicWindow:AddLabel(curX+80,curY+134,"League of Explorers",500,32,16,"center")
		dynamicWindow:AddLabel(curX+80,curY+158,"[F2F5A9]"..tostring(QuestsLeague.GetLeagueCurrecy(this, "Explorers")).." / " .. QuestsLeague.MaximumBankedFavor .."[-]",100,32,20,"center")

		curY = curY + 200
		curX = 114

		dynamicWindow:AddLabel(190,curY,"[F2F5A9]Township[-]",500,32,24,"center")

		curY = curY + 30

		dynamicWindow:AddImage(curX,curY,"BasicWindow_Panel",156,186,"Sliced")
		dynamicWindow:AddImage(curX+25,curY+20,"icon_Valus",106,106)
		dynamicWindow:AddLabel(curX+80,curY+134,"Township Boards",500,32,16,"center")
		dynamicWindow:AddLabel(curX+80,curY+158,"[F2F5A9]"..tostring(QuestsLeague.GetLeagueCurrecy(this, "Helm")) .. "[-]",140,32,20,"center")

		--[[for league,currency in pairs(QuestsLeague.LeagueNameToCurrency) do 
			curY = curY + 30
			dynamicWindow:AddLabel(20,curY,"League of "..league,500,32,18,"")
			dynamicWindow:AddButton(20, curY, "tooltip", "", 350, 32, QuestsLeague.LeagueNameDescription[league], "",false,"Invisible")
			dynamicWindow:AddLabel(350,curY,"[F2F5A9]"..tostring(QuestsLeague.GetLeagueCurrecy(this, league)).."[-]",100,32,20,"right")
		end

		curY = curY + 330

		dynamicWindow:AddLabel(20,curY,"Township",500,32,18,"")
		dynamicWindow:AddButton(20, curY, "tooltip", "", 350, 32, QuestsLeague.LeagueNameDescription["Helm"], "",false,"Invisible")
		dynamicWindow:AddLabel(350,curY,"[F2F5A9]"..tostring(QuestsLeague.GetLeagueCurrecy(this, "Helm")).."[-]",100,32,20,"right")]]
	end

	mMilitiaWindowOpen = true
	this:OpenDynamicWindow(dynamicWindow)
end

function MilitiaInfo( dynamicWindow )
	local militiaResign = this:GetObjVar("MilitiaResign")
	if (militiaResign) then
		local text = "You can speak to your militia leader to fully renounce your militia now."

		local now = DateTime.UtcNow
		if ( now < militiaResign ) then
			text = "You are currently in the militia resignation waiting period. Speak to your militia leader in "..TimeSpanToWords(militiaResign:Subtract(now)).." to complete the resignation."
		end

		ClientDialog.Show{
	        TargetUser = this,
	        ResponseObj = this,
	        DialogId = "MilitiaResignInfo",
	        TitleStr = "Resignation Pending",
	        DescStr = text,
	        Button1Str = "Ok",
		}
	end
		local militiaId = Militia.GetId(this)
		local curY = 0
		local rank,tier 

		if ( militiaId == nil ) then 
			dynamicWindow:AddLabel(190,50,"Join a Militia" ,500,200,24,"center")
			dynamicWindow:AddImage(40,84,"Divider",300,0,"Sliced")

			dynamicWindow:AddLabel(30,110,"The citizens of Eldeir Village, Helm and Pyros wage an endless war for control of the three towns. You can enlist with the local militia to participate in this conflict free of any criminal punishment. Not only will you be able to communicate globally with your fellow militia members, but you will gain access to militia specific items and rewards. Are you ready to join the battle?\n\nTo join a militia you first join a township by speaking with the mayor one of the three towns (Eldeir Village, Helm or Pyros). Next, you need to visit the militia leader in the keep of that town to enlist." ,324,400,18,"left")

			rank = 0
			curY = 416
		else
			local militiaData = Militia.GetDataById(militiaId)
			if ( militiaData == nil ) then return end

			curY = curY + 36
			dynamicWindow:AddImage(152,curY,militiaData.IconLarge,76,106)
			dynamicWindow:AddLabel(190,curY+110,militiaData.Name,500,32,24,"center")
			dynamicWindow:AddLabel(190,curY+135,"Current Leader: "..Militia.GetHighestRankedPlayer(militiaId),500,32,15,"center")
			dynamicWindow:AddImage(40,curY+155,"Divider",300,0,"Sliced")

			rank,tier = Militia.GetRankNumber(this)

			Militia.UpdatePlayerVars(this)
			dynamicWindow:AddLabel(190,curY+169,Militia.GetRankName(this,rank,tier),500,32,18,"center")
			dynamicWindow:AddLabel(190,curY+187,"(Rank Points: "..math.floor(Militia.GetFavor(this))..")",500,32,15,"center")
			dynamicWindow:AddLabel(24,curY+220,"Standing: Rated # "..Militia.GetStanding(this).." in "..Militia.GetMilitiaName(this),500,32,15,"")
			dynamicWindow:AddLabel(24,curY+235,"Percentile: Rated higher than "..(Militia.GetPercentile(this)*100).."% of "..Militia.GetMilitiaName(this),500,32,15,"")
			local season = GlobalVarRead("Militia.CurrentSeason")
			if ( season and season.EndDate and season.Name ) then
				local date = season.EndDate.Month.."/"..season.EndDate.Day
				dynamicWindow:AddLabel(24,curY+260,"Season of "..season.Name.." ends on "..date,500,32,15,"")
			else
				dynamicWindow:AddLabel(24,curY+260,"Off Season",500,32,15,"")
			end
			local militiaNextEventTime = GlobalVarReadKey("Militia.NextEvent", "Time")
			if ( militiaNextEventTime ) then
				local timeUntilEvent = militiaNextEventTime:Subtract(DateTime.UtcNow)
				if ( DateTime.Compare(DateTime.UtcNow, militiaNextEventTime) > 0 ) then
					dynamicWindow:AddLabel(20,curY+275,"Event is active!",500,32,15,"")
				else
					dynamicWindow:AddLabel(20,curY+275,"Event starts in "..TimeSpanToWords(timeUntilEvent),500,32,15,"")
				end
			end

			dynamicWindow:AddLabel(190,curY+296,"Militia Resources",500,32,18,"center")
			dynamicWindow:AddImage(40,curY+320,"Divider",300,0,"Sliced")

			local spiritWoodCount = GlobalVarReadKey("Militia.GlobalResources."..militiaId, "Spiritwood") or 0
			dynamicWindow:AddLabel(24,curY+340,"Spiritwood: "..spiritWoodCount.." / "..ServerSettings.Militia.GlobalResources.MaximumValue,500,32,15,"")
			dynamicWindow:AddButton(160,curY+338,"","",12,12,"Spiritwood is used to repair keep doors and construct battering rams.\n\nIt can be obtained by controlling the Spiritwood Lumber Mill in the Black Forest.","",false,"Help")

			curY = curY + 380
		end

		dynamicWindow:AddLabel(190,curY,"Militia Abilities",500,32,18,"center")
		dynamicWindow:AddImage(40,curY+24,"Divider",300,0,"Sliced")

		curY = curY + 40

		summonAbility = GetPrestigeAbilityUserAction(this, nil, "Militia", "SummonRam")
		dynamicWindow:AddUserAction(40,curY,summonAbility,52)

		summonAbility = GetPrestigeAbilityUserAction(this, nil, "Militia", "EquipCloak")
		dynamicWindow:AddUserAction(120,curY,summonAbility,52)

		summonAbility = GetPrestigeAbilityUserAction(this, nil, "Militia", "SummonMount")
		dynamicWindow:AddUserAction(200,curY,summonAbility,52)

		curY = curY + 74

		dynamicWindow:AddLabel(190,curY,"Rank Rewards",500,32,18,"center")
		dynamicWindow:AddImage(40,curY+24,"Divider",300,0,"Sliced")

		curY = curY + 42

		local militiaRankRewards = GetRankRewards()

		local curX = 30
		for i=2,5 do
			local buttonState = ""
			if not(militiaRankRewards[i]) and rank < i then 
				buttonState = "disabled" 
				dynamicWindow:AddButton(curX + (i-2)*85,curY,"","",64,48,"Rank requirement not met this season.","",false,"Invisible")
			end

			dynamicWindow:AddButton(curX + (i-2)*85,curY,"Claim|"..i,"",64,48,"","",false,"",buttonState,ClaimCustomSprites)
			if(militiaRankRewards[i]) then
				dynamicWindow:AddImage(curX + (i-2)*85+46,curY+2,"Checkmark",16,16,"Sliced")		
			end
			dynamicWindow:AddLabel(curX + 30 + (i-2)*85,curY+52,ServerSettings.Militia.RankNames[i].."",0,0,14,"center")		
		end

		curY = curY + 96
		dynamicWindow:AddButton(
			70, --(number) x position in pixels on the window
			curY, --(number) y position in pixels on the window
			"Leave", --(string) return id used in the DynamicWindowResponse event
			"Resign", --(string) text in the button (defaults to empty string)
			240, --(number) width of the button (defaults to width of text)
			26,--(number) height of the button (default decided by type of button)
			"", --(string) mouseover tooltip for the button (default blank)
			"", --(string) server command to send on button click (default to none)
			true --(boolean) should the window close when this button is clicked? (default true)
			--"CloseSquare", --(string) button type (default "Default")
			--buttonState --(string) button state (optional, valid options are default,pressed,disabled)
			--customSprites --(table) Table of custom button sprites (normal, hover, pressed. disabled)
		)

		mMilitiaWindowOpen = true

		this:OpenDynamicWindow(dynamicWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse, "Militia", function(user, buttonId)
	local newTab = HandleTabMenuResponse(buttonId)
	if(newTab) then
		mCurrentTab = newTab
		ReputationInfo()
		return
	end

	if ( buttonId == "Leave" ) then
		local militiaResign = this:GetObjVar("MilitiaResign")
		if (militiaResign) then
			ReputationInfo()
			return
		end
		ClientDialog.Show{
			TargetUser = this,
			ResponseObj = this,
			DialogId = "MilitiaResign",
			TitleStr = "Confirm Resignation",
			DescStr = "Are you sure you wish to leave your militia? There is a waiting period of ".. TimeSpanToWords(ServerSettings.Militia.ResignTime) ..".",
			Button1Str = "Confirm",
			Button2Str = "Cancel",
			ResponseFunc=function(user,buttonId)
				if(buttonId == 0) then
					Militia.BeginResignation(this)
				end
			end,
		}
	elseif(buttonId == "" or buttonId == nil) then
		mMilitiaWindowOpen = false
	else
		buttonId,arg = string.match(buttonId, "(%a+)|(.+)")
		if(buttonId == "Claim") then
			local rewardRank = tonumber(arg)
			local rank,tier = Militia.GetRankNumber(this)
			if(not(redeeming) and rank >= rewardRank) then
				local militiaRankRewards = GetRankRewards()
				if not(militiaRankRewards[rewardRank]) then 
					redeeming = true
					Create.InBackpack(ServerSettings.Militia.Militias[Militia.GetId(this)].RankRewardBoxes[rewardRank],this,nil,function (rewardBox)
						militiaRankRewards[rewardRank] = true
						this:SetObjVar("MilitiaRankRewards",militiaRankRewards)
						redeeming = false
						this:SystemMessage("You have claimed your "..ServerSettings.Militia.RankNames[rewardRank].." reward!","info")
						ReputationInfo()
					end)
				else
					this:SystemMessage("That reward has already been claimed.","info")
				end
			end
		end
	end
end)

RegisterEventHandler(EventType.ClientObjectCommand, "dropAction",
	function (user,sourceId,actionType,actionId,slot)
		--DebugMessage(1)
		--DebugMessage(user,sourceId,actionType,actionId,slot)
		if((sourceId == "Militia") and slot ~= nil) then
			--DebugMessage(2)
			local prestigeClass, prestigeName = actionId:match("pa_(%a+)_(%a+)")
			local hotbarAction = GetPrestigeAbilityUserAction(this, nil,prestigeClass,prestigeName)
			hotbarAction.Slot = tonumber(slot)
			
			AddUserActionToSlot(hotbarAction)
		end
	end)

function ToggleMilitiaWindow()
	if(mMilitiaWindowOpen) then
		mMilitiaWindowOpen = false
		this:CloseDynamicWindow("Militia")
	else
		ReputationInfo()
	end
end