require 'base_ai_mob'
AI.Settings.Leash = true
AI.Settings.LeashSpeed = 0.5
AI.Settings.LeashDistance = 40
AI.Settings.CanWander = true
AI.Settings.WanderChance = 1
AI.Settings.CanUseCombatAbilities = false
AI.Settings.ShouldAggro = false
AI.Settings.AggroRange = 0.0
AI.Settings.AggroChanceAnimals = 0
AI.Settings.FleeSpeed = 1
AI.Settings.FleeChance = 100
AI.Settings.FleeUnlessInCombat = true

RegisterEventHandler(EventType.Message, "HasDiedMessage",
    function(killer)
        this:PlayEffect("IgnitedEffect",7)
        this:PlayObjectSound("event:/monsters/phantom/phantom_death")
        this:ScheduleTimerDelay(TimeSpan.FromSeconds(3.8),"PlayVanishEffect")
    end)

RegisterEventHandler(EventType.Timer, "PlayVanishEffect", 
    function()
        PlayEffectAtLoc("VoidTeleportToEffect",this:GetLoc(),4)
        this:Destroy()
    end)
