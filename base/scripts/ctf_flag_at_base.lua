local registerPulseInterval = TimeSpan.FromSeconds(math.random(25,35))
local pulseInterval = TimeSpan.FromSeconds(3)
RegisterEventHandler(EventType.Message,"RemoveOwnership",function()RemoveOwnership()end)
RegisterEventHandler(EventType.Timer, "EventPulse",function()EventPulse()end)
RegisterEventHandler(EventType.Timer,"RegisterControllerToGlobalTable",function()MilitiaEvent.RegisterControllerToGlobalTable(this, TimeSpan.FromSeconds(math.random(29,31)))end)
this:ScheduleTimerDelay(registerPulseInterval,"RegisterControllerToGlobalTable")
this:ScheduleTimerDelay(pulseInterval,"EventPulse")

function OnLoad()
    SetObjVarsFromGlobalEvents()
    SetFlagState()
    AddUseCase(this, "Take Flag", true)
end

function HandleUseObject(user,useType)
	if(useType ~= "Take Flag" or not HasUseCase(this,"Take Flag") ) then return end	

	if( user == nil or not(user:IsValid()) ) then
		return
    end
    
    local active = this:GetObjVar("Active")
    local winner = this:GetObjVar("Winner")
    if ( active and active == 1 or winner and winner == 0 ) then
        user:SystemMessage("The flag appears to be missing!","info")
        return
    end

    -- You can only take the flag of the town you are capturing
    local standMilitia = this:GetObjVar("StandMilitia")
    local flagMilitia = this:GetObjVar("FlagMilitia")
    if(standMilitia ~= flagMilitia) then
        user:SystemMessage("You can not pick up this flag.","info")
        return
    end

    if ( MilitiaEvent.EligibleFlagbearer(user, this, false, true) ) then
        user:SendMessage("StartMobileEffect", "MilitiaFlagPickup", user, { Flag = this })        
    end
end

RegisterEventHandler(EventType.Message,"CompletePickup",
    function (flagbearer)
        if ( MilitiaEvent.EligibleFlagbearer(flagbearer, this, false, true) ) then
            RemoveUseCase(this, "Take Flag")
            ResetAndStartEvent(flagbearer)
            SetFlagState()
            MilitiaEvent.UpdateGlobalTable(this)
        end
    end)

function SetObjVarsFromGlobalEvents()
    local standMilitia = this:GetObjVar("StandMilitia")
    local flagMilitia = this:GetObjVar("FlagMilitia")
    local events = GlobalVarRead("Militia.Events")
    if ( events and next(events) ) then
        for controller, data in pairs(events) do
            if ( data[5] and data[5] == 3--event type is ctf
            and data[4] == flagMilitia
            and data[1] == this:GetLoc() ) then--record coincides with this flag pole
                this:SetObjVar("Winner", data[2])
                this:SetObjVar("Active", data[3])
                this:SetObjVar("Label", data[4])
            end
        end
    end
end

function SetFlagState()
    local flagMilitiaId = this:GetObjVar("FlagMilitia")
    if ( flagMilitiaId and flagMilitiaId ~= 0 ) then
        local active = this:GetObjVar("Active")
        if ( active and active == 0 ) then
            AddUseCase(this, "Take Flag", true)
        end
        local winner = this:GetObjVar("Winner")
        this:SetName(ServerSettings.Militia.Militias[flagMilitiaId].TextColor..ServerSettings.Militia.Militias[flagMilitiaId].Name.." Flag[-]")
        if ( active and active == 1 or winner and winner == 0 ) then
            this:SetScale(Loc(0,0,0))
        else
            if ( flagMilitiaId ) then
                this:SetSharedObjectProperty("Variation", ServerSettings.Militia.Militias[flagMilitiaId].Town)
                this:SetScale(Loc(1,1,1))
            end
        end
    end
end

local homeFlag = nil
function GetHomeFlag()
    if not(homeFlag) or not(homeFlag:IsValid()) then
        homeFlag = FindObject(SearchMulti(
            {
                SearchRange(this:GetLoc(), 10),
                SearchTemplate("ctf_flag_at_base"),                
                SearchObjVar("FlagMilitia", this:GetObjVar("StandMilitia")),                
            }))
    end

    return homeFlag
end

function EventPulse()
    Militia.FlagForConflict(this, ServerSettings.Militia.FlagAtBaseVulnerabilityRadius)
    
    -- if we are not the home flag check if we need to go home
    local standMilitia = this:GetObjVar("StandMilitia")
    local flagMilitia = this:GetObjVar("FlagMilitia")
    if(standMilitia ~= flagMilitia and this:GetObjVar("Active") == 0 and this:GetObjVar("Winner") == standMilitia) then
        GetHomeFlag()
        if(homeFlag and (homeFlag:GetObjVar("Active") == 1 or homeFlag:GetObjVar("Winner") ~= standMilitia) ) then
            RemoveOwnership()
            SetFlagState()
            local events = GlobalVarRead("Militia.Events")
            for controller, data in pairs(events) do
                if ( data[5] and data[5] == 3 ) then -- if ctf controller
                    controller:SendMessageGlobal("ReturnHome",flagMilitia)
                end
            end
        end
    end

    this:ScheduleTimerDelay(pulseInterval,"EventPulse")
end

function ResetAndStartEvent(user)
    this:SetObjVar("Active", 1)
    local standMilitia = this:GetObjVar("StandMilitia")
    local flagMilitia = this:GetObjVar("FlagMilitia")
    user:SendMessage("StartMobileEffect", "Flagbearer", user, { Flag = this, Duration = ServerSettings.Militia.Events[3].FlagbearerDuration })
    local name = user:GetName() or "Unknown"
    local message = name.." has stolen the "..ServerSettings.Militia.Militias[flagMilitia].TextColor..ServerSettings.Militia.Militias[flagMilitia].Name.."[-] flag!"
    Militia.EventMessagePlayers(message)
    MilitiaEvent.UpdateGlobalTable(this)
end

function ReturnFlag(returner)
    local standMilitia = this:GetObjVar("StandMilitia")
    local flagMilitia = this:GetObjVar("FlagMilitia")

    this:SetObjVar("Active", 0)
    
    MilitiaEvent.UpdateGlobalTable(this)
    local message = "The "..ServerSettings.Militia.Militias[flagMilitia].TextColor..ServerSettings.Militia.Militias[flagMilitia].Name.."[-] flag was returned to "..ServerSettings.Militia.Militias[standMilitia].TextColor..ServerSettings.Militia.Militias[standMilitia].Name.."[-]!"
    if ( returner ) then
        local name = returner:GetName() or "Unknown"
        message = name.." returned the "..ServerSettings.Militia.Militias[flagMilitia].TextColor..ServerSettings.Militia.Militias[flagMilitia].Name.."[-] flag to "..ServerSettings.Militia.Militias[standMilitia].TextColor..ServerSettings.Militia.Militias[standMilitia].Name.."[-]!"
    end
    Militia.EventMessagePlayers(message)
    SetFlagState()
end

function CaptureFlag(capturer)
    this:SetObjVar("Active", 0)
    this:SetObjVar("EndTime", DateTime.UtcNow)
    local standMilitia = this:GetObjVar("StandMilitia")
    local flagMilitia = this:GetObjVar("FlagMilitia")
    local flagsOwned, flagsOwnedGlobally = MilitiaEvent.GetNumberFlagsOwned(standMilitia)
    local MaxCooldown = ServerSettings.Militia.Events[3].MaxCooldown or ServerSettings.Militia.EventDefaultCooldown or DateTime.UtcNow
    local cooldown = math.clamp(TimeSpan.FromTicks(MaxCooldown.Ticks * (flagsOwnedGlobally-flagsOwned-1) / (flagsOwnedGlobally - 1)), ServerSettings.Militia.Events[3].MinCooldown, ServerSettings.Militia.Events[3].MaxCooldown)

    this:SetObjVar("Cooldown", cooldown)
    this:SetObjVar("Winner", standMilitia)
    --capping effect
    if ( flagMilitia == 1 ) then
        this:PlayEffect("FireTornadoEffect",5)
        this:PlayObjectSound("event:/magic/fire/magic_fire_flame_wave")
    elseif ( flagMilitia == 2 ) then
        this:PlayEffect("LightningEffect")
        this:PlayEffect("IceRainEffect")
        this:PlayObjectSound("event:/magic/water/magic_water_ice_rain")
        this:PlayObjectSound("event:/magic/air/magic_air_lightning_impact")
    elseif ( flagMilitia == 3 ) then
        this:PlayEffect("RockDropEffect")
        this:PlayEffect("DustTrailEffect")
        this:PlayObjectSound("event:/magic/earth/magic_earth_bombardment_throw")
        CallFunctionDelayed(TimeSpan.FromSeconds(0.5),function()this:PlayObjectSound("event:/magic/earth/magic_earth_bombardment_impact")end)
    end
    SetFlagState()
    if ( capturer ) then
        local name = capturer:GetName() or "Unknown"
        local message = name.." has captured the "..ServerSettings.Militia.Militias[flagMilitia].TextColor..ServerSettings.Militia.Militias[flagMilitia].Name.."[-] flag for "..ServerSettings.Militia.Militias[standMilitia].TextColor..ServerSettings.Militia.Militias[standMilitia].Name.."[-]!"
        Militia.EventMessagePlayers(message)
    else
        local message = "The "..ServerSettings.Militia.Militias[flagMilitia].TextColor..ServerSettings.Militia.Militias[flagMilitia].Name.."[-] flag has returned home!"
        Militia.EventMessagePlayers(message)
    end

    local events = GlobalVarRead("Militia.Events")
    for controller, data in pairs(events) do
        if ( data[5] and data[5] == 3--event type is ctf
        and tonumber(data[4]) == flagMilitia ) then--flag stand holds this flag
            if ( data[3] == 1 ) then--flag is gone 
                if ( controller:IsValid() ) then
                    controller:SendMessage("RemoveOwnership")
                else
                    controller:SendMessageGlobal("RemoveOwnership")
                end
            end
            --message flag stands that hold this flag to change township banner visual states
            if ( controller:IsValid() ) then
                controller:SendMessage("ChangeTownshipVisuals", standMilitia)
            else
                controller:SendMessageGlobal("ChangeTownshipVisuals", standMilitia)
            end
        end
    end
    MilitiaEvent.UpdateGlobalTable(this)
    Militia.BuffAllPlayers()
end

function RemoveOwnership()
    this:SetObjVar("Active", 0)
    this:SetObjVar("Winner", 0)
    MilitiaEvent.UpdateGlobalTable(this)
end

RegisterEventHandler(EventType.Message, "UseObject", HandleUseObject)
RegisterEventHandler(EventType.Message, "SetFlagState", SetFlagState)
RegisterEventHandler(EventType.Message, "ReturnFlag", ReturnFlag)
RegisterEventHandler(EventType.Message, "CaptureFlag",function (capturer)CaptureFlag(capturer)end)
RegisterEventHandler(EventType.Message, "ChangeTownshipVisuals",
    function(militiaId)
        local standMilitia = this:GetObjVar("StandMilitia")
        local flagMilitia = this:GetObjVar("FlagMilitia")
        if ( standMilitia and flagMilitia
        and standMilitia == flagMilitia ) then
            MilitiaEvent.SetTownshipFlags(militiaId)
        end
    end)
RegisterEventHandler(EventType.ModuleAttached, "ctf_flag_at_base",function()OnLoad()end)
RegisterEventHandler(EventType.LoadedFromBackup,"",OnLoad)

RegisterEventHandler(EventType.Message,"ReturnHome",function (militiaId)
    if(this:GetObjVar("StandMilitia") == militiaId and this:GetObjVar("FlagMilitia") == militiaId) then
        CaptureFlag()
    end
end)