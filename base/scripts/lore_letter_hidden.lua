function OnInteract(user, usedType)
    if (usedType ~= "Interact") then return end

    if this:GetLoc():Distance2(user:GetLoc()) > 3 then
    	user:SystemMessage("Too far to reach that.", "info")
    	return
    end

    local tomeId = this:GetObjVar("TomeId")
    local entryIndices = Dialogue.ProcessNumberRange(this:GetObjVar("EntryIndicesNumberRange"))
    if not (tomeId and entryIndices and next(entryIndices)) then return end
    
    local canSee = false
    local wardHidden = user:GetObjVar("HiddenWard")
    if wardHidden then
        local wardHiddenLoc = wardHidden[1]:GetLoc()

        if this:GetLoc():Distance2(wardHiddenLoc) < 9 then

            if Lore.KnowsLore(user, tomeId, entryIndices) then
                user:SystemMessage("You've already recorded this information.","info")
                return
            end

            canSee = true
        end
    else
        local nearbyWardHiddens = FindObjects(SearchMulti(
        {
            SearchModule("ward_hidden"),
            SearchObjectInRange(9),
        }))
        canSee = #nearbyWardHiddens > 0
    end

    if canSee then
        ClientDialog.Show{
            TargetUser = user,
            TitleStr = "Interesting Find",
            DescStr = "The contents of this letter can now be seen. Want to record this in your Catalog?",
            Button1Str = "Confirm",
            Button2Str = "Cancel",
            ResponseObj = this,
            ResponseFunc = function (user,buttonId)
                if (buttonId == 0) then
                    Lore.EarnLoreIfNew(user, tomeId, entryIndices)
                end
            end,
        }
    else
        ClientDialog.Show{
            TargetUser = user,
            TitleStr = "Mysterious Find",
            DescStr = "You suspect the contents of this letter are Hidden. You must reveal them, somehow.",
            Button1Str = "Cancel",
        }
    end
    
end

RegisterEventHandler(EventType.Message, "UseObject", OnInteract)

RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(), function()
    AddUseCase(this,"Interact",true)
end)