require 'dynamic_spawn_controller'

mDynamicSpawnKey = "DynamicFlatWildsSpawner"

mDynamicHUDVars.Title = "Easter Competition"
mDynamicHUDVars.Description = "[00FF00]Kill the Egg-Hunters & Bandits surrounding the portal[-]"

local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        msg = "The Malevolent Empire is coming..."
    elseif( progress == 2 ) then 
        msg = "The Malevolent Empire are sending ranged reinforcements!"
    elseif( progress == 3 ) then 
        msg = "A Malevolent Empire Commander approaches."
    elseif( progress == 999 ) then 
        msg = "You have eliminated the competition."
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end