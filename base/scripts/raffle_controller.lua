ticketData = nil
winners = nil

function CountTotal()
	local total = 0
	for userId,ticketInfo in pairs(ticketData) do
		total = total + ticketInfo.TicketCount
	end

	return total
end

function OpenRaffleWindow(user)
	ticketData = ticketData or this:GetObjVar("TicketData") or {}
	winnerData = this:GetObjVar("Winners")

    local dynWindow = DynamicWindow("RaffleWindow","Raffle",420,226)

    dynWindow:AddLabel(20,10,"Current Raffle: ".. (this:GetObjVar("RaffleName") or "None"),0,0,16)

    local raffleEndDate = this:GetObjVar("RaffleEndDate")
    local purchaseEnabled = false
    if(raffleEndDate) then
    	local now = DateTime.UtcNow
    	if(winnerData) then
	    	dynWindow:AddLabel(20,30,"The raffle has ended.",0,0,16)
    	elseif(raffleEndDate > now) then
	    	local timespanStr = GetTimerLabelString(raffleEndDate:Subtract(now))
	    	dynWindow:AddLabel(20,30,"Raffle Ends "..raffleEndDate:ToString().." UTC - "..timespanStr,0,0,16)
	    	purchaseEnabled = true	    
	    else
	    	dynWindow:AddLabel(20,30,"No longer selling tickets for this raffle.",0,0,16)
	    end	    
	else
		dynWindow:AddLabel(20,30,"This raffle has not started yet.",0,0,16)
	end

	local ticketPrice = this:GetObjVar("TicketPrice") or 20
	dynWindow:AddLabel(20,50,"Tickets are "..ticketPrice.."g each.",0,0,16)

	local limit = this:GetObjVar("Limit") 
	if(limit) then
		dynWindow:AddLabel(20,70,"Limit "..limit.." tickets per person.",0,0,16)		
	end

	local ticketCount = 0
	local raffleId = ""
	local userId = user:GetAttachedUserId()
	if(ticketData[userId]) then
		ticketCount = ticketData[userId].TicketCount or 0
		raffleId = ticketData[userId].RaffleId
	end
	dynWindow:AddLabel(20,90,"Your Tickets: "..math.min(ticketCount,(limit or 999999999999)),0,0,16)
	dynWindow:AddLabel(20,110,"Raffle Id: "..raffleId .. " (This is what we will use to identify the winner)",0,0,16)

	if(winnerData) then
		local winLabel = ""
		for i,info in pairs(winnerData) do
			winLabel = winLabel .. " " .. info.RaffleId .. ","
		end
		winLabel = StripTrailingComma(winLabel)
		dynWindow:AddLabel(20,140,"Winner:"..winLabel,0,0,24)
	else
	    dynWindow:AddTextField(18,140,200,20,"Amount","Enter Amount")

	    local buttonState = not(purchaseEnabled) and "disabled" or ""
	    dynWindow:AddButton(228, 137, "Purchase", "Purchase",80,26,"","",false,"",buttonState)
	end

    if(IsGod(user)) then
    	dynWindow:AddButton(300, 0, "Admin", "Admin",80,26,"","",false,"")
    end


    user:OpenDynamicWindow(dynWindow)
end

function OpenAdminWindow(user)
	ticketData = ticketData or this:GetObjVar("TicketData") or {}

	local dynWindow = DynamicWindow("AdminWindow","Raffle Admin",560,954)

	local raffleName = this:GetObjVar("RaffleName") or "None"
	local raffleEndDate = this:GetObjVar("RaffleEndDate")
	local raffleEnd = (raffleEndDate and raffleEndDate:ToString('yyyy-MM-dd HH:mm:ss')) or "2020-01-01 00:00:00"
	local ticketPrice = this:GetObjVar("TicketPrice") or 20
	local allowFree = tostring(this:GetObjVar("AllowFree")) or "True"
	local numWinners = this:GetObjVar("NumWinners") or 1

	local curY = 20
	dynWindow:AddLabel(24,curY+13,"[A1ADCC]Name[-]",0,35,18,"left")
	dynWindow:AddTextField(120,curY+10,200,20,"name",raffleName)
	dynWindow:AddButton(300,curY+8,"UpdateName","Update",80,23,"","",false,"")		
	curY = curY + 34
	dynWindow:AddLabel(24,curY+13,"[A1ADCC]End Date[-]",0,35,18,"left")
	dynWindow:AddTextField(120,curY+10,200,20,"date",raffleEnd)
	dynWindow:AddButton(300,curY+8,"UpdateDate","Update",80,23,"","",false,"")		
	curY = curY + 34
	dynWindow:AddLabel(24,curY+13,"[A1ADCC]Ticket Price[-]",0,35,18,"left")
	dynWindow:AddTextField(120,curY+10,200,20,"ticketprice",tostring(ticketPrice))
	dynWindow:AddButton(300,curY+8,"UpdateTicketPrice","Update",80,23,"","",false,"")		
	curY = curY + 34
	dynWindow:AddLabel(24,curY+13,"[A1ADCC]Allow Free[-]",0,35,18,"left")
	dynWindow:AddTextField(120,curY+10,200,20,"allowfree",tostring(allowFree))
	dynWindow:AddButton(300,curY+8,"UpdateAllowFree","Update",80,23,"","",false,"")		
	curY = curY + 34
	dynWindow:AddLabel(24,curY+13,"[A1ADCC]Num Winners[-]",0,35,18,"left")
	dynWindow:AddTextField(120,curY+10,200,20,"numwinners",tostring(numWinners))
	dynWindow:AddButton(300,curY+8,"UpdateNumWinners","Update",80,23,"","",false,"")		
	curY = curY + 54

	local scrollWindow = ScrollWindow(24,curY,500,390,26)
	for userId,ticketInfo in pairs(ticketData) do
		local element = ScrollElement()
		element:AddLabel(24,3,userId,0,0,18)
		element:AddLabel(100,3,ticketInfo.RaffleId,0,0,18,"left")

		local name = ticketInfo.Player:GetCharacterName()
		if( not(name) or name == "" ) then
			name = ticketInfo.CharacterName or "N/A"
		end
		element:AddLabel(200,3,name,0,0,18)
		element:AddLabel(400,3,tostring(ticketInfo.TicketCount),1000,0,18,"left")
		scrollWindow:Add(element)
	end
	dynWindow:AddScrollWindow(scrollWindow)
	curY = curY + 410

	dynWindow:AddLabel(200,curY,"Winners",0,0,22)
	curY = curY + 30
	local winnersInfo = winners or this:GetObjVar("Winners")
	if(winnersInfo) then
		for i,info in pairs(winnersInfo) do
			dynWindow:AddLabel(24,curY+((i-1)*20),info.UserId,0,0,18)
			dynWindow:AddLabel(100,curY+((i-1)*20),info.RaffleId,0,0,18,"left")
		end
	end
	curY = curY + 220


	dynWindow:AddLabel(24,curY,"Ticket Total: "..tostring(CountTotal()),0,0,18)
	dynWindow:AddButton(228, curY, "Reset", "Reset Data",80,26,"","",false,"")
	dynWindow:AddButton(228+90, curY, "Roll", "Roll",80,26,"","",false,"")
	if(winners) then
		dynWindow:AddButton(228+180, curY, "End", "End Raffle",80,26,"","",false,"")
	end

	user:OpenDynamicWindow(dynWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"AdminWindow",
	function (user,buttonId,fieldData)
		if not(IsGod(user)) then return end

		if(buttonId == "UpdateName" and fieldData.name) then
			this:SetObjVar("RaffleName",fieldData.name)
			OpenAdminWindow(user)
		elseif(buttonId == "UpdateDate" and fieldData.date) then
			local date = Convert.ToDateTime(fieldData.date)
			if(date) then
				this:SetObjVar("RaffleEndDate",date)
			end
			OpenAdminWindow(user)
		elseif(buttonId == "UpdateTicketPrice" and fieldData.ticketprice) then
			local price = tonumber(fieldData.ticketprice)
			if(price) then
				this:SetObjVar("TicketPrice",fieldData.ticketprice)
			end
			OpenAdminWindow(user)
		elseif(buttonId == "UpdateAllowFree" and fieldData.allowfree) then
			this:SetObjVar("AllowFree",fieldData.allowfree == "True" or fieldData.allowfree == "true")
			OpenAdminWindow(user)
		elseif(buttonId == "UpdateNumWinners" and fieldData.numwinners) then
			local numWinners = tonumber(fieldData.numwinners)
			if(numWinners) then
				this:SetObjVar("NumWinners",fieldData.numwinners)
			end
			OpenAdminWindow(user)
		elseif(buttonId == "Reset") then
			ClientDialog.Show{
		        TargetUser = user,
		        ResponseObj = this,
		        TitleStr = "Reset",
		        DescStr = "Are you sure you wish to reset?",
		        Button1Str = "Ok",
		        Button2Str = "Cancel",
		        ResponseFunc = function (user,buttonId)
		        	if(buttonId == 0) then
		        		user:SystemMessage("Reset complete.","info")
		        		this:DelObjVar("TicketData")
		        		this:DelObjVar("Winners")
		        		ticketData = {}		        		
		        		OpenAdminWindow(user)
		        	else
		        		user:SystemMessage("Reset Cancelled.","info")
		        	end
		        end
		    }
		elseif(buttonId == "Roll") then
			if(this:HasObjVar("Winners")) then
				ClientDialog.Show{
			        TargetUser = user,
			        ResponseObj = this,
			        TitleStr = "Reset",
			        DescStr = "Winner has already been chosen. Are you sure you wish to roll again?",
			        Button1Str = "Ok",
			        Button2Str = "Cancel",
			        ResponseFunc = function (user,buttonId)
			        	if(buttonId == 0) then
			        		DoRoll(user)
			        	else
			        		user:SystemMessage("Roll Cancelled.","info")
			        	end
			        end
			    }
			else
				DoRoll(user)
			end
		elseif(buttonId == "End" and winners ~= nil and #winners > 0) then
			this:SetObjVar("Winners",winners)			
			winners = nil
			
			OpenAdminWindow(user)
		end
	end)


pendingTransactions = {}

function DoRoll(user)
	local limit = this:GetObjVar("Limit") or 999999999999999
	numResults = this:GetObjVar("NumWinners") or 1
	winners = {}
	for i=1,numResults do
		if not(IsTableEmpty(ticketData)) then
			local total = 0
			for userId,info in pairs(ticketData) do
				total = total + math.min(info.TicketCount,limit)
			end

			local roll = math.random(1,total)
			local curTicket = 0
			local winner = nil
			for userId,info in pairs(ticketData) do
				curTicket = curTicket + math.min(info.TicketCount,limit)
				if(roll <= curTicket) then
					winner = userId
					break							
				end
			end

			if(winner) then
				table.insert(winners,{UserId = winner, RaffleId = ticketData[winner].RaffleId, CharacterName = ticketData[winner].CharacterName })
				ticketData[winner] = nil
			end
		end
	end
	ticketData = this:GetObjVar("TicketData")
	
	OpenAdminWindow(user)
end

function ShowPurchaseConfirm(user,amount)
	local userId = user:GetAttachedUserId()
	local ticketPrice = this:GetObjVar("TicketPrice") or 20
	local total = amount * ticketPrice

	if not(pendingTransactions[userId]) then
		TextFieldDialog.Show{
	        TargetUser = user,
	        Width = 500,
	        Title = "Purchase Tickets",
	        Description = "Type CONFIRM in all capital letters to confirm your purchase of "..amount.." tickets for "..(total).." gold.",
	        ResponseObj = this,
	        ResponseFunc = function(user,newValue)
	            if(newValue == "CONFIRM") then
	                local backpackCanBuy = ( CountCoins(user) >= total )
					local bankCanBuy = ( CountCoinsInBank(user) >= total )

					if not(CheckLimit(user,amount)) then
						return
					end

					if( not backpackCanBuy and not bankCanBuy ) then
						user:SystemMessage("You can not afford that many tickets.","info")
						return
					else					
						
						pendingTransactions[userId] = amount
						-- Can our backpack afford the purchase, if not take from the bank!
						if( backpackCanBuy ) then
							RequestConsumeResource(user,"coins",total,"Raffle",this)
						else
							RequestConsumeResourceFromBank(user,"coins",total,"Raffle",this)
						end
					end
	            else
	                user:SystemMessage("Ticket purchase cancelled.","info")
	            end
	        end
	    }
	end
end

function HandleConsumeResourceResponse(success,transactionId,user)			
	local userId = user:GetAttachedUserId()

	if not(pendingTransactions[userId]) then return end

	-- something went wrong taking the coins
	if( not(success) ) then
		DebugMessage("ERROR: HandleConsumeResourceResponse failed",tostring(transactionId),tostring(user))
		this:SystemMessage("Ticket purchase cancelled.","info")
	-- we have taken the coins complete the transaction
	else
		ticketData = ticketData or this:GetObjVar("TicketData") or {}
		if not(ticketData[userId]) then ticketData[userId] = {} end

		ticketData[userId].TicketCount = (ticketData[userId].TicketCount or 0) + pendingTransactions[userId]
		ticketData[userId].Player = user
		ticketData[userId].CharacterName = StripColorFromString(user:GetName())
		ticketData[userId].RaffleId = GenerateRaffleId()

		this:SetObjVar("TicketData",ticketData)

		user:SystemMessage(pendingTransactions[userId] .. " tickets purchased.","info")

		OpenRaffleWindow(user)
	end

	pendingTransactions[userId] = nil
end

function CheckLimit(user,amount)
	local limit = this:GetObjVar("Limit")
	if(limit) then
		local ticketData = this:GetObjVar("TicketData") or {}
		local userId = user:GetAttachedUserId()
		local ticketInfo = ticketData[userId]
		local ticketCount = (ticketInfo and ticketInfo.TicketCount) or 0
		if(amount + ticketCount > limit) then
			user:SystemMessage("Can not purchase more than "..tonumber(limit).." tickets for this raffle.","info")		
			return false
		end
	end

	return true
end

RegisterEventHandler(EventType.Message, "ConsumeResourceResponse", HandleConsumeResourceResponse)

RegisterEventHandler(EventType.DynamicWindowResponse,"RaffleWindow",
	function (user,buttonId,fieldData)
		if(buttonId == "Admin" and IsGod(user)) then
			OpenAdminWindow(user)
		elseif(buttonId == "Purchase") then
			if(this:GetObjVar("AllowFree") ~= true and IsFreeAccount(user)) then
				user:SystemMessage("Free accounts are not permitted to participate in this raffle. Please purchase a Premium Subscription or Citizen's Pass.","info")
				return
			end

			local amount = tonumber(fieldData.Amount)
			if(amount and amount > 0 and amount <= 1000000) then
				if not(CheckLimit(user,amount)) then
					return
				end

				ShowPurchaseConfirm(user,amount)
			else
				user:SystemMessage("Please enter a valid amount.","info")
			end
		end
	end)


RegisterEventHandler(EventType.Message,"UseObject",
    function ( user,usedType)
    	if(usedType == "Buy Tickets") then
	        OpenRaffleWindow(user)
	    end
    end)


RegisterEventHandler(EventType.ModuleAttached,"raffle_controller",
	function()

		AddUseCase(this,"Buy Tickets",true)

		if not(this:HasObjVar("AllowFree")) then
			this:SetObjVar("AllowFree",false)
		end
	end)

function GenerateRaffleId() 
    local unique = false
    local shortId = nil
    while not(unique) do
        unique = true
        local uuid = uuid()        
        shortId = uuid:sub(1,5)
        DebugMessage("shortId",tostring(shortId))

        for userId,info in pairs(ticketData) do
	        if(info.RaffleId == shortId) then
	            unique = false
            	break
            end
        end
    end

    DebugMessage("found",tostring(shortId))
    return shortId
end
