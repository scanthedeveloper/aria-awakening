require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false
AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

-- Arena
Arena = {}
Arena.MaxDistance = 35
Arena.MatchInProgress = false
Arena.MatchData = {}
Arena.Queue = {}
Arena.RankedQueue = {}
Arena.Debug = this:GetObjVar("EnableDebug") or false

function CleanUpRankedQueue()
    local newQueue = {}
    for key, data in pairs( Arena.RankedQueue ) do 
        table.insert( newQueue, data )
    end
    Arena.RankedQueue = newQueue
end


function DebugArena( var )
    if( Arena.Debug ) then
        if( type(var) == "table" ) then
            DebugTable( var )
        else
            DebugMessage( tostring( var ) )
        end
    end
end

RegisterEventHandler(EventType.Message, "EndMatch",  
function( args )
    Arena.MatchData.Winner = 'Draw'
end)

RegisterEventHandler(EventType.Message, "ClearQueue",  
function( args )
    for key, data in pairs( Arena.RankedQueue ) do 
        data.Player:SendMessageGlobal("RankedArenaMessage", { Action = "OutOfRange" })
    end
    Arena.RankedQueue = {}
end)

RegisterEventHandler(EventType.Message, "RankedArenaMessage",  
    function ( args )
        -- The match organizer has pressed the "Start" button
        if( args.Action == "EnterQueue" ) then
            
            if( args.Player and args.Player:IsValid() and args.GroupSize and args.GroupSize > 0 and args.GroupMembers and CanQueue( args.GroupMembers ) ) then
                table.insert( Arena.RankedQueue, { Player = args.Player, GroupSize = args.GroupSize, GroupMembers = args.GroupMembers } )
                DebugArena( "Added to Queue!" )
                DebugArena( Arena.RankedQueue )
                this:NpcSpeech(args.Player:GetName() .. " you are queued. You and any group members will need to stay near the arena.")
                args.Player:SendMessage("RankedArenaMessage", { Action = "QueueSuccessful" })
            else
                args.Player:SendMessage("RankedArenaMessage", { Action = "AlreadyQueued" })
            end

        -- Team needs to be removed from the queue
        elseif( args.Action == "LeaveQueue" ) then
            for key, team in pairs( Arena.RankedQueue ) do 
                if( team.Player == args.Player ) then
                    RemovePlayerFromQueue( Arena.RankedQueue[key].Player )
                    CleanUpRankedQueue()
                end
            end
    
        -- Player has indicated they are ready
        elseif( args.Action == "Ready" ) then
            table.insert(Arena.MatchData.PlayersReady , args.Player)

        -- A player in the match has died.
        elseif( args.Action == "PlayerDied" ) then
            
            -- Are we in an active match?
            if( Arena.MatchInProgress and Arena.MatchData.Countdown == nil ) then
                if( RankedArena.IsPlayerOnTeam( Arena.MatchData.TeamA, args.Player ) ) then
                    table.insert( Arena.MatchData.TeamADead, args.Player );
                    if( #Arena.MatchData.TeamADead >= #Arena.MatchData.TeamA ) then
                        Arena.MatchData.Winner = "Enemy"
                    end
                elseif( RankedArena.IsPlayerOnTeam( Arena.MatchData.TeamB, args.Player ) ) then
                    table.insert( Arena.MatchData.TeamBDead, args.Player );
                    if( #Arena.MatchData.TeamBDead >= #Arena.MatchData.TeamB ) then
                        Arena.MatchData.Winner = "Friendly"
                    end
                end
            end

        elseif( args.Action == "GetData" ) then
            --DebugMessage("GotMessageForData")
            args.Player:SendMessage("GetArenaDataAdmin", Arena)
        end

    end
)

function CanQueue( _groupMembers )
    for k,teamA in pairs( Arena.RankedQueue ) do 
        for i,memberB in pairs( _groupMembers ) do 
            for j,memberA in pairs( teamA.GroupMembers ) do 

                if( not memberA:IsValid() or memberB == memberA ) then return false end

            end
        end
    end
    return true
end

function RemovePlayerFromQueue( _playerObj )
    for k,data in pairs( Arena.RankedQueue ) do 
        if( data.Player == _playerObj ) then
            Arena.RankedQueue[k] = nil
        end
    end
end

function ProcessRankedArenaQueue()    
    DebugArena( " --- Processing Arena Queue --- " )
    DebugArena( "RankedQueue . . . " )
    DebugArena( Arena.RankedQueue )

    for k,team in pairs( Arena.RankedQueue ) do 
        if( CheckGroupMembersInRange( team ) == false ) then
            team.Player:SendMessageGlobal("RankedArenaMessage", { Action = "OutOfRange" })
            Arena.RankedQueue[k] = nil
        end

        if( CheckGroupMembersHavePets( team ) == false ) then
            team.Player:SendMessageGlobal("RankedArenaMessage", { Action = "NoPetsAllowed" })
            Arena.RankedQueue[k] = nil
        end
    end

    local queueCount = CountTable(Arena.RankedQueue)

    -- We need to start a new match
    if( Arena.MatchInProgress == false and queueCount >= 2 ) then
        DebugArena( "Trying to start match . . . " )
        local lookingForMatch = true

        -- We need to take the first person and queue and try to match them, if that falls
        -- we move to the next. Until we run out of tries.
        for k,teamA in pairs( Arena.RankedQueue ) do 
            if( lookingForMatch ) then
                local bestChoice = nil
                local bestDelta = 3000
                local title = teamA.GroupSize.."v"..teamA.GroupSize
                local TeamARating = RankedArena.GetTeamAvgRating( teamA.GroupMembers, title, this )

                for i,teamB in pairs( Arena.RankedQueue ) do 
                    if( lookingForMatch and (teamA.Player and teamB.Player) and (teamA.Player ~= teamB.Player) and (teamA.GroupSize == teamB.GroupSize) ) then
                        local TeamBRating = RankedArena.GetTeamAvgRating( teamB.GroupMembers, title, this )
                        local delta = math.abs( TeamARating - TeamBRating )
                        --DebugMessage( "Delta:", tostring(delta), tostring(TeamARating), tostring(TeamBRating)  )
                        if( delta < bestDelta ) then
                            bestDelta = delta
                            bestChoice = teamB
                        end
                    end
                end

                if( bestChoice ) then
                    local teamB = bestChoice
                    lookingForMatch = false
                    
                    -- Set the match data and start the match!
                    Arena.MatchInProgress = true
                    Arena.MatchData.Ranked = this:GetObjVar("RankedMatches") or false
                    Arena.MatchData.LeaderA = teamA.Player
                    Arena.MatchData.LeaderB = teamB.Player
                    Arena.MatchData.TeamA = deepcopy(teamA.GroupMembers)
                    Arena.MatchData.TeamB = deepcopy(teamB.GroupMembers)
                    Arena.MatchData.Countdown = Arena.MatchData.Countdown or RankedArena.DefaultMatchCoutdown
                    Arena.MatchData.TeamADead = {}
                    Arena.MatchData.TeamBDead = {}
                    Arena.MatchData.Title = title
                    Arena.MatchData.Winner = nil
                    DebugArena( "Removing players from queue ("..tostring(teamA.Player)..") ("..tostring(teamB.Player)..")" )
                    RemovePlayerFromQueue( teamA.Player )
                    RemovePlayerFromQueue( teamB.Player )
                    CleanUpRankedQueue()
                end
            end
        end

    elseif( Arena.MatchInProgress and Arena.MatchData.Countdown and Arena.MatchData.Countdown > 0 ) then
        DebugArena( "Performing match coutdown . . . " )
        Arena.MatchData.Countdown = (Arena.MatchData.Countdown - 1)
        Arena.MatchData.LeaderA:SendMessage("RankedArenaMessage", { Action = "CountdownPulse", Countdown = Arena.MatchData.Countdown })
        Arena.MatchData.LeaderB:SendMessage("RankedArenaMessage", { Action = "CountdownPulse", Countdown = Arena.MatchData.Countdown })

    -- Match needs to start!
    elseif( Arena.MatchInProgress and Arena.MatchData.Countdown == 0 ) then
        DebugArena( "Starting match . . . " )
        Arena.MatchData.Countdown = nil
        Arena.MatchData.StartTime = os.time(os.date("!*t")) -- UNIX EPOCH TIMESTAMP
        Arena.MatchData.EndTime = Arena.MatchData.StartTime + RankedArena.MatchLengthSeconds -- UNIX TIMESTAMP + SECONDS TO LAST

        local HUDArgs = 
        {
            Title = "Ranked Arena Match",
            Description = "Kill all opponents in the arena.",
            EndTime = DateTime.UtcNow:Add(TimeSpan.FromMinutes(math.floor(RankedArena.MatchLengthSeconds/60)))
        }

        Arena.MatchData.HUDArgs = HUDArgs

        Arena.MatchData.LeaderA:SendMessage("RankedArenaMessage", { Action = "MatchStarted", ArenaMaster = this, MatchData = Arena.MatchData })
        Arena.MatchData.LeaderB:SendMessage("RankedArenaMessage", { Action = "MatchStarted", ArenaMaster = this, MatchData = Arena.MatchData })

    elseif( Arena.MatchInProgress and Arena.MatchData.Winner == nil ) then
        DebugArena( "Match in progress . . . " )
        local currentUnixTS = os.time(os.date("!*t"))
        
        -- We have ran out of time!
        if( currentUnixTS > Arena.MatchData.EndTime ) then
            -- Did more people on TeamA die?
            if( #Arena.MatchData.TeamADead > #Arena.MatchData.TeamBDead ) then
                Arena.MatchData.Winner = "Enemy"
            -- Did more people on TeamB die?
            elseif( #Arena.MatchData.TeamADead > #Arena.MatchData.TeamBDead ) then
                Arena.MatchData.Winner = "Friendly"
            -- Did an equal amount on both teams die?
            else
                Arena.MatchData.Winner = "Draw"
            end
        end
    
    elseif( Arena.MatchInProgress and Arena.MatchData.Winner ~= nil ) then
        DebugArena( "Match ended . . . " )

        if( Arena.MatchData.Winner ~= 'Draw' ) then
            local TeamARating = RankedArena.GetTeamAvgRating( Arena.MatchData.TeamA, Arena.MatchData.Title, this )
            local TeamBRating = RankedArena.GetTeamAvgRating( Arena.MatchData.TeamB, Arena.MatchData.Title, this )
            local newTeamARating, newTeamBRating = RankedArena.CalculateEloRating( TeamARating, TeamBRating, ( Arena.MatchData.Winner == "Friendly" ) )

            -- Update the friendly team score
            for i=1, #Arena.MatchData.TeamA do 
                RankedArena.SavePlayerName( Arena.MatchData.TeamA[i], this )
                if( Arena.MatchData.Ranked ) then
                    local delta = newTeamARating - TeamARating 
                    RankedArena.AdjustPlayerRating( Arena.MatchData.TeamA[i], delta, this, Arena.MatchData.Title, true )
                    ResetAllPrestigeCooldowns( Arena.MatchData.TeamA[i] )
                end
            end

            -- Update the enemy team score
            for i=1, #Arena.MatchData.TeamB do 
                RankedArena.SavePlayerName( Arena.MatchData.TeamB[i], this )
                if( Arena.MatchData.Ranked ) then 
                    local delta = newTeamBRating - TeamBRating
                    RankedArena.AdjustPlayerRating( Arena.MatchData.TeamB[i], delta, this, Arena.MatchData.Title, true )
                    ResetAllPrestigeCooldowns( Arena.MatchData.TeamB[i] )
                end
            end



            if( Arena.MatchData.Winner == 'Friendly' ) then
                this:NpcSpeech("[0000FF]Blue Team[-] has won the match!")
            elseif( Arena.MatchData.Winner == 'Enemy' ) then
                this:NpcSpeech("[FF0000]Red Team[-] has won the match!")
            end
        else
            this:NpcSpeech("The match has ended in a [FFFF00]draw[-]!")
        end
        
        RankedArena.SendMessageToPlayers( Arena.MatchData.TeamA, Arena.MatchData.TeamB, 
        { 
            Action = "MatchEnded", 
            ArenaMaster = this,
        })

        -- End the match
        Arena.MatchInProgress = false
        
        local arenaMatchLog = this:GetObjVar("ArenaMatchLog") or {}
        Arena.MatchData.LeaderA = nil
        Arena.MatchData.LeaderB = nil
        Arena.MatchData.TeamADead = nil
        Arena.MatchData.TeamBDead = nil
        Arena.MatchData.Countdown = nil
        Arena.MatchData.MatchInProgress = nil
        Arena.MatchData.HUDArgs = nil
        table.insert( arenaMatchLog, Arena.MatchData );
        this:SetObjVar("ArenaMatchLog", arenaMatchLog);

        DestroyWardsInArena()

    end
    
    CallFunctionDelayed(TimeSpan.FromSeconds(1), ProcessRankedArenaQueue)
end

function DestroyWardsInArena()
    local wards = FindObjects(SearchTemplate("blank",30))
    for k,ward in pairs(wards) do
        if( ward:HasModule("ward_evil") or ward:HasModule("ward_hidden") ) then
            if( ward:IsInRegion("Arena") ) then
                ward:Destroy()
            end
        end
    end
end

function CheckGroupMembersInRange( _ArenaQueueEntry )
    local allMembersInRange = true
    if( _ArenaQueueEntry and _ArenaQueueEntry.GroupMembers ) then

        for key, member in pairs( _ArenaQueueEntry.GroupMembers ) do 
            if( not member:IsValid() or this:DistanceFrom(member) > Arena.MaxDistance ) then
                allMembersInRange = false
            end
        end
    else
        allMembersInRange = false
    end

    return allMembersInRange
end

function CheckGroupMembersHavePets( _ArenaQueueEntry )
    local noPetsExist = true
    if( _ArenaQueueEntry and _ArenaQueueEntry.GroupMembers ) then

        for key, member in pairs( _ArenaQueueEntry.GroupMembers ) do 
            if( member:IsValid() ) then
                local pets = GetActivePets(member, nil, false)
                if( #pets > 0 ) then noPetsExist = false end 
            end
        end
    else
        noPetsExist = false
    end

    return noPetsExist
end

function Dialog.OpenGreetingDialog(user)
        local text = "I am not conducting ranked matches at this time, but you are welcome to practice in the arena, just sign up."
        local response = {}
        local key = 1
        local allowRanked = this:GetObjVar("RankedMatches")

        if( allowRanked ) then

            text = "Looking to test your mettle versus the finest in New Celador? Sign up for a ranked match!"

        end

        response[key] = {}
        response[key].text = "Sign me up!"
        response[key].handle = "DoOrganize"
        key = key + 1

        response[key] = {}
        response[key].text = "What is my ranking?"
        response[key].handle = "ShowRating"
        key = key + 1

        local foundReward = false
        local playerRewards = this:GetObjVar("PlayerRewards") or {}
        for i=1, #playerRewards do
            local reward = playerRewards[i]
            if( reward.PlayerId == tostring(user.Id) and reward.Claimed ~= true and foundReward == false ) then
                response[key] = {}
                response[key].text = "I would like my rewards?"
                response[key].handle = "ClaimRewards"
                key = key + 1
                foundReward = true
            end
        end

        response[key] = {}
        response[key].text = "Goodbye."
        response[key].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenDoOrganizeDialog(user)
    user:SendMessage("StartMobileEffect", "RankedArenaSignup", this )
    user:CloseDynamicWindow("Responses")
end

function Dialog.OpenShowRatingDialog(user)
    user:SendMessage("StartMobileEffect", "RankedArenaStandings", this )
    user:CloseDynamicWindow("Responses")
end

function Dialog.OpenClaimRewardsDialog(user)

    local playerRewards = this:GetObjVar("PlayerRewards") or {}
    for i=1, #playerRewards do
        local reward = playerRewards[i]
        if( reward.PlayerId == tostring(user.Id) and reward.Claimed ~= true and reward.Rewards ) then
            playerRewards[i].Claimed = true

            for j=1, #reward.Rewards do
                local mReward = reward.Rewards[j]
                Create.InBackpack( mReward, user, nil, function(createdObj) 
                    local rewardInfo = { AwardedTo = tostring(user:GetName()), Bracket = tostring(reward.Bracket), Rank = tostring(reward.Rank) }
                    createdObj:SetObjVar("RewardInfo", rewardInfo)
                    CallFunctionDelayed(TimeSpan.FromMilliseconds(30), function()
                        SetItemTooltip(createdObj)
                    end)
                end, true)
            end
        end
    end

    this:SetObjVar("PlayerRewards", playerRewards )
    user:CloseDynamicWindow("Responses")
end

CallFunctionDelayed(TimeSpan.FromSeconds(1), ProcessRankedArenaQueue)

-- schedule next ranked event
-- note: if the server is down when the end event fires

function SetRankEnabled(isEnabled)
    this:SetObjVar("RankedMatches",isEnabled)
end

for index,info in pairs(ServerSettings.Arena.RankedArenaEvents) do
    local eventStart = NextDayOfWeek(info.Day):Add(info.StartTime)
    if( DateTime.Compare(DateTime.UtcNow,eventStart) < 0 ) then
        this:ScheduleTimerAtUTC(eventStart,"EnableRanked-"..index)
        RegisterSingleEventHandler(EventType.Timer,"EnableRanked-"..index,
            function ()
                SetRankEnabled(true)
                DebugMessage("Server Event: Ranked arena matches have begun at the Pyros Arena!")
                ServerBroadcast("Ranked arena matches have begun at the Pyros Arena!",true)
            end)

        -- try to schedule an announcement
        local announcementTime = eventStart:Subtract(TimeSpan.FromMinutes(5))
        if( DateTime.Compare(DateTime.UtcNow,announcementTime) < 0 ) then
            this:ScheduleTimerAtUTC(announcementTime,"AnnounceRanked-"..index)
            RegisterSingleEventHandler(EventType.Timer,"AnnounceRanked-"..index,
                function ()
                    DebugMessage("Server Event: Ranked arena matches will begin in 5 minutes at the Pyros Arena!")
                    ServerBroadcast("Ranked arena matches will begin in 5 minutes at the Pyros Arena!",true)
                end)
        end
    end

    --
    local eventEnd = NextDayOfWeek(info.Day):Add(info.EndTime)
    if( DateTime.Compare(DateTime.UtcNow,eventEnd) < 0 ) then
        this:ScheduleTimerAtUTC(eventEnd,"DisableRanked-"..index)
        RegisterSingleEventHandler(EventType.Timer,"DisableRanked-"..index,
            function ()                
                SetRankEnabled(false)
                DebugMessage("Server Event: Ranked arena matches at the Pyros arena are now over. Thank you for participating.")
                LocalBroadcast("Ranked arena matches at the Pyros arena are now over. Thank you for participating.",true)
            end)

        -- try to schedule an announcement
        local announcementTime = eventEnd:Subtract(TimeSpan.FromMinutes(5))
        if( DateTime.Compare(DateTime.UtcNow,announcementTime) < 0 ) then
            this:ScheduleTimerAtUTC(announcementTime,"AnnounceEndRanked-"..index)
            RegisterSingleEventHandler(EventType.Timer,"AnnounceEndRanked-"..index,
                function ()
                    DebugMessage("Server Event: Ranked arena matches will end in 5 minutes.")
                    LocalBroadcast("Ranked arena matches will end in 5 minutes.",true)
                end)
        end
    end
end