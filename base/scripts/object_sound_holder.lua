function SoundOnCreation()
	if this:HasObjVar("SoundOnCreation") then
		local _SoundOnCreation = this:GetObjVar("SoundOnCreation")
		if _SoundOnCreation ~= nil then
			this:SendMessage("StartCreationSound");
		end
	end
end

RegisterSingleEventHandler(EventType.Message, "StartCreationSound", 
	function()
		this:PlayObjectSound(this:GetObjVar("SoundOnCreation"), false)
	end)

RegisterSingleEventHandler(EventType.ModuleAttached, "object_sound_holder", 
	function()
		SoundOnCreation()
	end)