--Spawned in the world. Spawns objects for assigned mission.
--This is created once a player has accepted a mission from a dispatcher

MissionController = MissionController or {}

--Spawns instance of mob that must be killed
MissionController.Boss = {}

--Spawns mobs that must be tamed
MissionController.Tame = {}

--Spawns mob lair that must be destroyed
MissionController.Lair = {}

--Spawns resource node that must be harvested
MissionController.Harvest = {}

--Spawn fish school that must be fished
--Note: Fishing does not use resource harvester system, hence why it's different from harvesting missions
MissionController.Fishing = {}

--Spawn prefab and set up mission when player get's nearby
--Depending on the type of mission, run variant of MissionControllerInit afterwards
function InitMissionController()
	local missionData = this:GetObjVar("MissionData")
	local missionInfo = GetMissionTable(missionData.Key)

	if (missionInfo.PrefabName ~= nil) then
		local spawnLoc = GetNearbyRandomMissionSpawnLoc(missionInfo.PrefabName)

		if (spawnLoc == nil) then
			DebugMessage("Mission controller "..tostring(this).." failed to spawn prefab "..missionInfo.PrefabName)
			DestroyMissionController()
		end
		this:SetObjVar("PrefabSpawnLoc", spawnLoc)

		if (missionInfo.PrefabName ~= nil) then
			CreatePrefab(missionInfo.PrefabName, spawnLoc, Loc(0, 0, 0), "prefab_object_spawned")
		end
	end
	this:SetObjVar("ObjectivesCompleted", 0)

	local missionType = missionInfo.MissionType
	if (MissionController[missionType].Init) then
		MissionController[missionType].Init()
	end
end

function CheckStatus()
	--Check status of mission
	local missionData = this:GetObjVar("MissionData")
	local missionInfo = GetMissionTable(missionData.Key)
	local missionType = missionInfo.MissionType
	local missionOwner = this:GetObjVar("MissionOwner")
	if (MissionController[missionType].CheckStatus) then
		if not (MissionController[missionType].CheckStatus()) then
			--Mission failed
			missionOwner:SendMessage("EndMission", missionData.ID, false)
			DestroyMissionController()
		end
	end
end

function MissionController.Boss.Init()
	local missionData = this:GetObjVar("MissionData")
	local missionInfo = GetMissionTable(missionData.Key)

	for i = #missionInfo.BossTemplate, 1, -1 do
		local bossSpawnLoc = GetNearbyPassableLocFromLoc(this:GetObjVar("PrefabSpawnLoc"))
		CreateObj(missionInfo.BossTemplate[i], bossSpawnLoc, "BossSpawned")
	end
	this:SetObjVar("ObjectiveCount", #missionInfo.BossTemplate)
end

RegisterEventHandler(EventType.CreatedObject, "BossSpawned", 
	function(success, objRef)
		if (success) then
			local missionData = this:GetObjVar("MissionData")
			objRef:SetObjVar("MissionSource", this)
			objRef:SetObjVar("MissionData", missionData)
			objRef:AddModule("mission_objective")
			objRef:SetObjVar("MissionKey", missionData.Key)

			local prefabObjects = this:GetObjVar("PrefabObjects") or {}
			table.insert(prefabObjects, objRef)
			this:SetObjVar("PrefabObjects", prefabObjects)
		else
			DebugMessage(tostring(this).." INVALID BOSS MOB TEMPLATE. ENDING MISSION.")
			DestroyMissionController()
		end
	end)

function MissionController.Lair.Init()
	local missionData = this:GetObjVar("MissionData")
	local missionInfo = GetMissionTable(missionData.Key)

	local prefabObjects = this:GetObjVar("PrefabObjects")

	this:SetObjVar("ObjectivesCompleted", 0)
	local objectiveCount = 0
	for i, j in pairs(prefabObjects) do
		if (j:HasModule("destructable_mob_spawner") or j:HasModule("destroyable_object")) then
			j:SetObjVar("MissionSource", this)
			j:SetObjVar("MissionData", missionData)
			j:AddModule("mission_objective", {Key = missionData.Key})
			j:SetObjVar("MissionKey", missionData.Key)
			objectiveCount = objectiveCount + 1
		end
	end

	if (objectiveCount == 0) then
		DebugMessage(tostring(this).." NO destructable_mob_spawner FOUNDS. ENDING MISSION.")
		DestroyMissionController()
	else
		this:SetObjVar("ObjectiveCount", objectiveCount)
	end
end

function MissionController.Tame.Init()
	local missionData = this:GetObjVar("MissionData")
	local missionInfo = GetMissionTable(missionData.Key)		
	this:SetObjVar("ObjectivesCompleted", 0)
	this:SetObjVar("ObjectiveCount", missionInfo.TameCount)

	local prefabObjects = this:GetObjVar("PrefabObjects") or {}
	for i = 0,missionInfo.TameCount + 1,1 do
		--Spawn mobs with objective module
		Create.AtLoc(missionInfo.MobTemplate, GetRandomPassableLocationInRadius(this:GetLoc(), 24, true),
		function(mobile)
			mobile:SetSharedObjectProperty("Title", "[ff6347]"..missionInfo.Title)
			mobile:AddModule("mission_objective")
			mobile:SetObjVar("AI-Leash", true)
			mobile:SetObjVar("MissionKey", missionData.Key)
			mobile:SetObjVar("MissionSource", this)
			table.insert(prefabObjects, mobile)
			this:SetObjVar("PrefabObjects", prefabObjects)
		end)
	end

	--Checks if mission can be completed every X seconds
	--This catches cases where tamed creatures are killed at the same time
	RegisterEventHandler(EventType.Timer, "CheckStatusTimer", 
	function()
		CheckStatus()
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(3),"CheckStatusTimer")		
	end)
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(3),"CheckStatusTimer")
end

function MissionController.Fishing.Init()
	local missionData = this:GetObjVar("MissionData")
	local missionInfo = GetMissionTable(missionData.Key)
	this:SetObjVar("ObjectivesCompleted", 0)
	this:SetObjVar("ObjectiveCount", 1)

	local waterLoc = missionData.WaterLoc
	Create.AtLoc(missionInfo.NodeTemplate, waterLoc,
	function(resourceNode)
		resourceNode:SetName(missionInfo.Title)
		resourceNode:AddModule("mission_objective")
		resourceNode:SetObjVar("MissionKey", missionData.Key)
		resourceNode:SetObjVar("MissionSource", this)
		resourceNode:SetObjVar("SchoolOfFish", missionInfo.Fish)
		resourceNode:SetObjVar("MaxResources", missionInfo.ResourceAmount)
		this:SetObjVar("PrefabObjects", resourceNode)
		
		resourceNode:SendMessage("ResetNode")
	end)
end

--Check if mission can still be completed with remaining mobs
function MissionController.Tame.CheckStatus()
	local missionData = this:GetObjVar("MissionData")
	local missionInfo = GetMissionTable(missionData.Key)

	local prefabObjects = this:GetObjVar("PrefabObjects")


	--Get available mobs
	local livingMobs = 0
	for i, prefabObj in pairs(prefabObjects) do
		if (prefabObj:IsValid() and IsDead(PrefabObj) and prefabObj:HasModule("mission_objective")) then
			livingMobs = livingMobs + 1
		end
	end

	if (missionInfo.TameCount - this:GetObjVar("ObjectivesCompleted") -1 >= livingMobs) then
		return false
	end
	return true

end

function MissionController.Harvest.Init()
	local missionData = this:GetObjVar("MissionData")
	local missionInfo = GetMissionTable(missionData.Key)		
	this:SetObjVar("ObjectivesCompleted", 0)
	this:SetObjVar("ObjectiveCount", 1)

	Create.AtLoc(missionInfo.NodeTemplate, GetRandomPassableLocationInRadius(this:GetLoc(), 24, true),
		function(resourceNode)
			resourceNode:AddModule("mission_objective")
			resourceNode:SetObjVar("MissionKey", missionData.Key)
			resourceNode:SetObjVar("MissionSource", this)

			resourceNode:SetObjVar("MaxResources", missionInfo.ResourceAmount)

			--Set hue on node
			resourceNode:SetObjVar("ResourceSourceId", missionInfo.ResourceId)
			local resourceInfoTemplate = GetTemplateData(ResourceData.ResourceInfo[missionInfo.ResourceId].Template)
			if (resourceInfoTemplate and resourceInfoTemplate.Hue) then
				resourceNode:SetHue(resourceInfoTemplate.Hue)
			end

			this:SetObjVar("PrefabObjects", resourceNode)
			
			resourceNode:SendMessage("ResetNode")
		end)
end

function HasCompletedObjectives()
	local objectivesCompleted = this:GetObjVar("ObjectivesCompleted") or 0
	if (objectivesCompleted >= (this:GetObjVar("ObjectiveCount") or 0)) then
		return true
	end
	return false
end

--Notifies player of victory and destroys itself
function CompleteMission()
	local missionData = this:GetObjVar("MissionData")
	local missionOwner = this:GetObjVar("MissionOwner")
	missionOwner:SendMessage("EndMission", missionData.ID, true)
	DestroyMissionController()
end

--Adds a short decay to prefab objects before destroying this controller
--If the object is a dead mob, add a default 
function MissionController.Lair.Cleanup()
	local prefabObjects = this:GetObjVar("PrefabObjects") or {}
	if (prefabObjects ~= nil) then
		for i, j in pairs(prefabObjects) do
			if (j:IsValid()) then
				if (j:HasModule("mission_objective")) then
					j:DelModule("mission_objective")
				end
				if (j:IsMobile()) then
					if (IsDead(j)) then
						Decay(j)
					end
				else
					Decay(j, 150)
				end
			end
		end
	end
	this:Destroy()
end

function MissionController.Tame.Cleanup()
	local prefabObjects = this:GetObjVar("PrefabObjects") or {}
	if (prefabObjects ~= nil) then
		for i, j in pairs(prefabObjects) do
			if (j:IsValid()) then
				j:Destroy()
			end
		end
	end
	this:Destroy()
end

function MissionController.Harvest.Cleanup()
	local resourceNode = this:GetObjVar("PrefabObjects")
	if (resourceNode) then
		Decay(resourceNode, 150)
	end
	this:Destroy()
end

function DestroyMissionController()
	RegisterSingleEventHandler(EventType.Timer, "DestroyMissionController",
		function()
			local missionData = this:GetObjVar("MissionData")
			local missionInfo = GetMissionTable(missionData.Key)
			local missionType = missionInfo.MissionType
			if (MissionController[missionType].Cleanup) then
				MissionController[missionType].Cleanup()
			else
				this:Destroy()
			end
		end)
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(1), "DestroyMissionController")
end

function GetNearbyRandomMissionSpawnLoc(prefabName)
	local prefabExtents = GetPrefabExtents(prefabName)
    local spawnLoc = GetNearbyPassableLoc(this, 360, 10, 25)
    local relBounds = GetRelativePrefabExtents(prefabName, spawnLoc, prefabExtents)

    local i = 0
    while (i < 40) do
    	local doesPass, reason = CheckBounds(relBounds,false)
	    if doesPass then
	    	return spawnLoc
	    else
	    	spawnLoc = GetNearbyPassableLoc(this, 180, 1, 25)
    		relBounds = GetRelativePrefabExtents(prefabName, spawnLoc, prefabExtents)
    	end
	    i = i+1
    end
	-- If loop fails, spawn node on top of Controller (which is hopefully at a valid loc)
    return this:GetLoc()
end

--Adds spawned objects to a collection
RegisterEventHandler(EventType.CreatedObject, "prefab_object_spawned", 
	function(success, objRef)
		if (success) then
			local prefabObjects = this:GetObjVar("PrefabObjects") or {}
			table.insert(prefabObjects, objRef)
			this:SetObjVar("PrefabObjects", prefabObjects)
		end
	end)

RegisterEventHandler(EventType.Message, "ObjectiveCompleted", 
	function()

		--Add to objective counter
		local objectivesCompleted = this:GetObjVar("ObjectivesCompleted") or 0
		objectivesCompleted = objectivesCompleted + 1
		this:SetObjVar("ObjectivesCompleted", objectivesCompleted)
		
		--If enough objectives are completed, complete mission
		if (HasCompletedObjectives()) then
			CompleteMission()
			return
		end

		CheckStatus()
	end)

RegisterEventHandler(EventType.Message, "CheckStatus",
	function()
		CheckStatus()
	end)

--Schedules the destruction of the mission when created
RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(),
	function ()
		local missionData = this:GetObjVar("MissionData")
		if not missionData then this:Destroy() return end
		if (DateTime.UtcNow > missionData.EndTime) then
			DestroyMissionController()
		end

		this:ScheduleTimerDelay(missionData.EndTime - missionData.StartTime, "DestroyMissionController")
		InitMissionController()
	end)

--If loaded from a backup, verify that the mission hasn't expired
RegisterEventHandler(EventType.LoadedFromBackup, "", 
	function()
		local missionData = this:GetObjVar("MissionData")
		if (missionData and DateTime.UtcNow > missionData.EndTime) then
			DestroyMissionController()
		end
	end)

RegisterEventHandler(EventType.Message, "DestroyMissionController", DestroyMissionController)