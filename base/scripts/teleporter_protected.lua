require 'teleporter'

-- store the mobiles around this teleporter
mobiles = {}
ignoredTeamType = this:GetObjVar("IgnoreMobileTeamType") or nil

base_ActivateTeleporter = ActivateTeleporter
function ActivateTeleporter(user)

	-- If this is a God character or a dead character, let them pass.
	if( 
		IsGod(user) 
		or IsDead(user) 
		or this:HasObjVar("ForceOpen") 
	) then base_ActivateTeleporter(user) return end

	local region = this:GetObjVar("InRegion") or nil
	local mobiles = {}
	local canTeleport = true

	--If we have a region we need to get our mobile count
	if( region ) then
		mobiles = FindObjects(SearchMulti{SearchRegion(region),SearchHasObjVar("MobileTeamType")})
	end
	
	-- If mobiles are not dead we need to stop teleportation.
	for i=1, #mobiles do 
		if( not IsDead(mobiles[i]) and mobiles[i]:GetObjVar("MobileTeamType") ~= ignoredTeamType and not IsPet(mobiles[i]) ) then
			--DebugMessage("Cannot teleport because of: " .. mobiles[i]:GetName())
			canTeleport = false
		end
	end

	if( not canTeleport ) then
		user:SystemMessage("That portal will not respond with enemies nearby.","info")
		return
	else
		base_ActivateTeleporter(user)
	end

end

