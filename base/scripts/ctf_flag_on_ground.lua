local pulseInterval = TimeSpan.FromSeconds(3)
RegisterEventHandler(EventType.Timer, "Pulse",function()Pulse()end)
this:ScheduleTimerDelay(pulseInterval,"Pulse")
local timeDropped = nil

function OnLoad()
	local flagMilitiaId = this:GetObjVar("FlagMilitia")
	if ( flagMilitiaId ) then
		CallFunctionDelayed(TimeSpan.FromMilliseconds(1),function()
			this:SetName(ServerSettings.Militia.Militias[flagMilitiaId].TextColor..ServerSettings.Militia.Militias[flagMilitiaId].Name.." Flag[-]")
			this:SetSharedObjectProperty("Variation", ServerSettings.Militia.Militias[flagMilitiaId].Town)
			this:SetScale(Loc(1,1,1))
		end)
	end
	timeDropped = DateTime.UtcNow
	AddUseCase(this, "Take Flag", true)
	SetTooltipEntry(this,"ctf_flag_desc","Bring it home!\n")		
end

function HandleUseObject(user,useType)
	if(useType ~= "Take Flag" or not HasUseCase(this,"Take Flag") ) then return end	

	if( user == nil or not(user:IsValid()) ) then
		return
    end

	if ( MilitiaEvent.EligibleFlagbearer(user, this, false, true) ) then
		user:SendMessage("StartMobileEffect", "MilitiaFlagPickup", user, { Flag = this })        		
    end
end

RegisterEventHandler(EventType.Message,"CompletePickup",
    function (flagbearer)
        if ( MilitiaEvent.EligibleFlagbearer(flagbearer, this, false, true) ) then
            RemoveUseCase(this, "Take Flag")
            flagbearer:SendMessage("StartMobileEffect", "Flagbearer", flagbearer, { Flag = this, Duration = ServerSettings.Militia.Events[3].FlagbearerDuration })            
        end
    end)

function Pulse()
	local loc = this:GetLoc()
	if ( Plot.GetAtLoc(loc) or 
	DateTime.Compare(DateTime.UtcNow, timeDropped:Add(ServerSettings.Militia.Events[3].GroundDuration)) > 0 ) then
		local flagMilitia = this:GetObjVar("FlagMilitia")
		Militia.SetVar("MissingFlag"..flagMilitia, function(record)
			record[1] = nil
			record[2] = nil
			record[3] = nil
			return true
		end,
		function(success)
			if ( success ) then
				local events = GlobalVarRead("Militia.Events")
				if ( events and next(events) ) then
					for controller, data in pairs(events) do
						--find the event controller that is CTF, active(flag stolen) and holds the flag we are returning
						if ( data[5] and data[5] == 3 and data[3] == 1 and tonumber(data[4]) == tonumber(flagMilitia) ) then--
							local scaleToDistance = 600
							local distanceFromStand = scaleToDistance
							local flagMilitia = this:GetObjVar("FlagMilitia")
							if ( controller:IsValid() ) then
								controller:SendMessage("ReturnFlag")
								distanceFromStand = controller:GetLoc():Distance(loc)
							else
								controller:SendMessageGlobal("ReturnFlag")
							end
							
							this:Destroy()
						end
					end
				end
			end
		end)
	end
	Militia.FlagForConflict(this, ServerSettings.Militia.VulnerabilityRadius)
	this:ScheduleTimerDelay(pulseInterval,"Pulse")
end

RegisterEventHandler(EventType.Message, "UseObject", HandleUseObject)
RegisterEventHandler(EventType.ModuleAttached, "ctf_flag_on_ground",function()OnLoad()end)
RegisterEventHandler(EventType.LoadedFromBackup,"",OnLoad)

RegisterEventHandler(EventType.Message, "ForceReturn", function ( ... )
	timeDropped = DateTime.MinValue
	Pulse()
end)