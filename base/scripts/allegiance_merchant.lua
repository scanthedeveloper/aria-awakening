require 'base_ai_npc'

AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true
AI.Settings.CanConverse = false
local militiaCurrencyName = "talents"

function Dialog.OpenGreetingDialog(user)
    if ( MilitiaEvent.AmWinningTerritoryCount(user) == true ) then
        local text = "I am at peace. I had a good life."
        local response = {}
        
        response[1] = {}
        response[1].text = "What are you selling?"
        response[1].handle = "PurchaseRewards"

        response[2] = {}
        response[2].text = "Tell me about godly weapons."
        response[2].handle = "GodlyWeaponInfo"

        --response[3] = {}
        --response[3].text = "I'd like to change my sinful ways."
        --response[3].handle = "ReprieveInfo"
        
        response[4] = {}
        response[4].text = "Goodbye."
        response[4].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
    else
        local text = "Hello there. Sorry, but I only sell wares to the current controlling Militia."
        local response = {}

        --response[1] = {}
        --response[1].text = "I'd like to change my sinful ways."
        --response[1].handle = "ReprieveInfo"
        
        response[2] = {}
        response[2].text = "Goodbye."
        response[2].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
    end
end

function Dialog.OpenPurchaseRewardsDialog(user)
    local text = "Which item would you like to purchase?"
    local response = {}
    local Rewards = MakeGetRewardsTable(user)

    SetupPurchaseDialogHandles(user, Rewards)

    for x, y in sortpairs(Rewards, function(Rewards, a, b) return Rewards[a]["Rank"] < Rewards[b]["Rank"] end) do
        table.insert(response, {text = "(Rank "..y.Rank..")"..y.Name..": "..y.Cost.." "..militiaCurrencyName, handle = x})
    end

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenGodlyWeaponInfoDialog(user)
    local Rewards = {
    GodlyWeaponScroll = {
        Name = "Godly Weapon Scroll",
        Cost = 50000,
        Rank = 7,
        Template = "godly_weapon_scroll",
        }
    }
    local text = "I know of a way to imbue a weapon with godly power. If you ever reach rank "..Rewards.GodlyWeaponScroll.Rank..", bring me "..Rewards.GodlyWeaponScroll.Cost.." "..militiaCurrencyName.." and I will share my knowledge."
    local response = {}

    SetupPurchaseDialogHandles(user, Rewards)
    for reward, property in pairs(Rewards) do
        table.insert(response, {text = "(Rank "..property.Rank..")"..property.Name..": "..property.Cost.." "..militiaCurrencyName, handle = reward})
    end
    table.insert(response, {text = "Fascinating...", handle = "Greeting"})

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

--[[function Dialog.OpenReprieveInfoDialog(user)
    local text = "For a limited time, the gods are offering a chance to return to the path of servitude. You may only ask forgiveness once, and any negative behavior will be punished."
    
    local response = {}

    response[1] = {}
    response[1].text = "I promise to change my ways."
    response[1].handle = "TryReprieve"

    response[2] = {}
    response[2].text = "I'm not interested in changing."
    response[2].handle = "Greeting"

    NPCInteractionLongButton(text,this,user,"Responses",response)
end]]

function Dialog.OpenTryReprieveDialog(user)
    local text = "Good luck to you."
    local response = {}
    StartMobileEffect(user, "Reprieve")

    response[1] = {}
    response[1].text = "Thank you."
    response[1].handle = "Greeting"

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function MakeGetRewardsTable(user)
    local Rewards = {
    CursedHearthstone = {
        Name = "Cursed Hearthstone",
        Cost = 50,
        Rank = 1,
        Template = "cursed_hearthstone",
    },
    LotteryBox = {
        Name = "Militia Lottery Box",
        Cost = 3000,
        Rank = 2,
        Template = "lottery_box_militia",
    },
    FlaskOfMending = {
        Name = "Flask Of Mending",
        Cost = 1000,
        Rank = 3,
        Template = "flask_of_mending",
    },
    ElixiOfHolding = {
        Name = "Elixir of Holding",
        Cost = 500,
        Rank = 4,
        Template = "elixir_of_holding",
    },
    }
    local OtherRewards = {
        Cloak = {
            Cost = 30000,
            Rank = 5,
        },
        Mount = {
            Cost = 18000,
            Rank = 6,
        },
    }
    local militiaId = Militia.GetId(user)
    if ( militiaId ) then
        if ( militiaId == 1 ) then
            Rewards.WarriorsOfPyros = {Name = "Warriors of Pyros Cloak", Cost = OtherRewards.Cloak.Cost, Rank = OtherRewards.Cloak.Rank, Template = "warriors_of_pyros_cloak"}
            Rewards.PyrosHorseStatue = {Name = "Pyros Horse Statue", Cost = OtherRewards.Mount.Cost, Rank = OtherRewards.Mount.Rank, Template = "item_statue_mount_pyros_horse"}
        elseif ( militiaId == 2 ) then
            Rewards.OrderOfHelm = {Name = "Order of Helm Cloak", Cost = OtherRewards.Cloak.Cost, Rank = OtherRewards.Cloak.Rank, Template = "order_of_helm_cloak"}
            Rewards.TethysHorseStatue = {Name = "Tethys Horse Statue", Cost = OtherRewards.Mount.Cost, Rank = OtherRewards.Mount.Rank, Template = "item_statue_mount_tethys_horse"}
        elseif ( militiaId == 3 ) then
            Rewards.GuardiansOfEldeir = {Name = "Guardians of Eldeir Cloak", Cost = OtherRewards.Cloak.Cost, Rank = OtherRewards.Cloak.Rank, Template = "guardians_of_eldeir_cloak"}
            Rewards.EbrisHorseStatue = {Name = "Ebris Horse Statue", Cost = OtherRewards.Mount.Cost, Rank = OtherRewards.Mount.Rank, Template = "item_statue_mount_ebris_horse"}
        end
    end
    return Rewards
end

function Purchase(user, itemKey, Rewards)
    if ( Rewards[itemKey] == nil or Rewards[itemKey].Cost == nil or Rewards[itemKey].Rank == nil or Rewards[itemKey].Template == nil ) then
        QuickDialogMessage(this,user,"That item is not available right now, unfortunately.")
        return
    end

    local backpack = user:GetEquippedObject("Backpack")
    if ( backpack == nil ) then return false end
    local bank = user:GetEquippedObject("Bank")
    if ( bank == nil ) then return false end
    if ( ConsumeResourceContainer(backpack, "AllegianceIchor", Rewards[itemKey].Cost) ) then
        Create.InBackpack(Rewards[itemKey].Template, user, nil, function(item)
            if ( item ) then
                AfterPurchase(item)
                Dialog.OpenPurchaseRewardsDialog(user)
            else
                QuickDialogMessage(this,user,"I'm sorry something went wrong, please try that again.")
            end
        end)
        return
    elseif ( ConsumeResourceContainer(bank, "AllegianceIchor", Rewards[itemKey].Cost) ) then
        Create.InBackpack(Rewards[itemKey].Template, user, nil, function(item)
            if ( item ) then
                AfterPurchase(item)
                Dialog.OpenPurchaseRewardsDialog(user)
            else
                QuickDialogMessage(this,user,"I'm sorry something went wrong, please try that again.")
            end
        end)
        return
    end
    
    QuickDialogMessage(this,user,"You do not have enough "..militiaCurrencyName.." to purchase that.")
end

function SetupPurchaseDialogHandles(user, Rewards)
    -- setup the dialog handles for each item purchase
    for key,val in pairs(Rewards) do
        Dialog[string.format("Open%sConfirmDialog", key)] = function(user)
            Purchase(user, key, Rewards)
        end
        Dialog[string.format("Open%sDialog", key)] = function(user)
            local text = string.format("I can sell you %s for %s "..militiaCurrencyName..".", val.Name, val.Cost)
            local response = {
                {
                    text = "I accept your offer.",
                    handle = key.."Confirm",
                },
                {
                    text = "What else do you have to sell?",
                    handle = "PurchaseRewards"
                },
                {
                    text = "Goodbye.",
                    handle = ""
                },
            }
            NPCInteractionLongButton(text,this,user,"Responses",response)
        end
    end
end

local _OnMobileCreated = OnMobileCreated
function OnMobileCreated()
    _OnMobileCreated()
    Create.Equipped("robe_linen_tunic", this, function(item)
        item:SetColor("0xC100FFFF")
    end)
    Create.Equipped("robe_linen_leggings", this, function(item)
        item:SetColor("0xC100FFFF")
    end)
    Create.Equipped("robe_necromancer_helm", this, function(item)
        item:SetColor("0xC100FFFF")
    end)
    Create.Equipped("facial_hair_beard_long", this, function(item)
        item:SetColor("0xC100FFFF")
    end)

    this:SetSharedObjectProperty("Title", "Militia Merchant")
end

function AfterPurchase(item)
    if ( item:IsValid() ) then
        local itemName = item:GetName()
        if ( string.find(itemName, "Cloak") ) then
            local seasonName = GlobalVarReadKey("Militia.CurrentSeason", "Name")
            if ( seasonName ) then
                itemName = itemName:gsub("Cloak", "Cloak ("..seasonName..")")
                item:SetName(itemName)
            end
        end
    end
end