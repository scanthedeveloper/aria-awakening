

function HandleUseObject(user,usedType)
	Verbose("Mobile", "HandleUseObject", user,usedType)
	-- TODO: Check for guard protection (if you loot there it should alert the guards)
	if( usedType == "Open Pack" ) then
		if(IsDead(this)) then
			if(this:GetLoc():Distance(user:GetLoc()) > OBJECT_INTERACTION_RANGE ) then    
        		user:SystemMessage("You cannot reach that.","info")  
        		return false
    		end
	    	if not(user:HasLineOfSightToObj(this,ServerSettings.Combat.LOSEyeLevel)) then 
	    		user:SystemMessage("[FA0C0C]You cannot see that![-]","info")
	    		return false
	    	end

			if( this:HasObjVar("guardKilled") ) then
				user:SystemMessage("[$1673]")
			elseif( not(this:HasObjVar("lootable") or this:HasObjVar("HasPetPack")) ) then
				user:SystemMessage("You find there is nothing of value on that corpse.","info")
			else				
		    	local backpackObj = this:GetEquippedObject("Backpack")
			    if ( backpackObj == nil ) then
					this:SendOpenContainer(user)
				else
					if ( #backpackObj:GetContainedObjects() > 0 ) then
						backpackObj:SendOpenContainer(user)
					else
						user:SystemMessage("You find there is nothing of value on that corpse.","info")
					end
				end
			end
		elseif(this:HasObjVar("HasPetPack")) then
			if(IsController(user,this) or IsDemiGod(user)) then
				local backpackObj = this:GetEquippedObject("Backpack")
			    if( backpackObj ~= nil ) then
		    		backpackObj:SendMessage("OpenPack",user)
			    end
			else
				user:SystemMessage("You can't do that.","info")
			end
		end
	elseif( (usedType == "Loot All" or usedType == "Loot Gold") and IsDead(this) ) then

    	local lootContainer = this:GetEquippedObject("Backpack")
	    if( lootContainer == nil ) then
			lootContainer = this
		end
		if ( #containerObj:GetContainedObjects() > 0 ) then
			if( usedType == "Loot All" ) then
				user:SendMessage("LootAll", this)
			else
				user:SendMessage("LootAll", this, true)
			end
			
		else
			user:SystemMessage("You find nothing worth looting on this corpse.","info")
			return
		end
    elseif(usedType == "Cut Off Head") then
        user:SendMessage("StartMobileEffect", "CutOffHead", this)
	elseif(usedType == "Dismount" and user == this) then
		local mountObj = this:GetEquippedObject("Mount")
		if(mountObj ~= nil) then
			if ( DismountMobile(this, mountObj) and IsPet(mountObj) ) then
				SendPetCommandTo(mountObj, "follow", this)
			end
		end
	end
end

RegisterEventHandler(EventType.Message, "UseObject", HandleUseObject)