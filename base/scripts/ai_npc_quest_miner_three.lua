require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

AI.QuestStepsInvolvedIn = {
    {"MinerProfessionTierFour", 3},	-- Olufunke at Misty Caverns (mouth of Silus River near Southern Rim)
}

AI.GreetingMessages = 
{
    "Will there always be more to Mine? Yes, of course there will.",
}

function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,
    	"I am a Grandmaster Miner. If you're approaching that rank, you will be called to come see me."
    	)
    --QuickDialogMessage(this,user,"REPLACED -> I am a quest guy!")
end

function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)] or AI.GreetingMessages[1]

    response = {}

    response[1] = {}
    response[1].text = "Who are you?"
    response[1].handle = "Who" 
    
    response[2] = {}
    response[2].text = "Goodbye."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)