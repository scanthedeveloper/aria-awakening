
local MOLTENEXPLOSION_DAMAGE_RANGE = 5
local mTargetLoc = nil
local mPillarActive = false
local function ValidatePyroMoltenExplosion(targetLoc)

	if( not(IsPassable(targetLoc)) ) then
		this:SystemMessage("[$2616]","info")
		return false
	end

	if not(this:HasLineOfSightToLoc(targetLoc,ServerSettings.Combat.LOSEyeLevel)) then
		this:SystemMessage("[$2617]","info")
		return false
	end
	return true
end

RegisterEventHandler(EventType.Message,"PyroMoltenExplosionSpellTargetResult",
	function (targetLoc)
		if not(ValidatePyroMoltenExplosion(targetLoc)) then
			EndEffect()
			return
		end
		if(mPillarActive == false) then
			mTargetLoc = targetLoc
		end
		this:PlayObjectSound("event:/magic/air/magic_air_lightning", false)
		PlayEffectAtLoc("FlameAuraEffect",mTargetLoc, 5)
		PlayEffectAtLoc("FirePillarEffect",mTargetLoc, 3)
		PlayEffectAtLoc("FireballExplosionEffect",mTargetLoc, 3)
		PlayEffectAtLoc("PrimedFire2",mTargetLoc, 3)
		local mobiles = FindObjects(SearchMulti({
						SearchRange(mTargetLoc,MOLTENEXPLOSION_DAMAGE_RANGE),
						SearchMobile()}), GameObj(0))
		for i,v in pairs(mobiles) do
			if(not IsDead(v)) then
				PlayEffectAtLoc("FlameAuraEffect",mTargetLoc, 4)
				PlayEffectAtLoc("FireballExplosionEffect",mTargetLoc, 3)
				this:SendMessage("RequestMagicalAttack", "PyroMoltenExplosion", v, this, true)
				this:SendMessage("RequestMagicalAttack", "ImmolationIgnite", v, this, true)
			end
		end

		EndEffect()

	end)



function EndEffect()
	this:DelModule("sp_pyromancy_molten_explosion")
end