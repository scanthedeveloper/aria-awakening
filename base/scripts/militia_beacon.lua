-- flags everyone for militia combat within a radius

local pulseInterval = TimeSpan.FromSeconds(3)
RegisterEventHandler(EventType.Timer, "Pulse",function()Pulse()end)
this:ScheduleTimerDelay(pulseInterval,"Pulse")

function Pulse()
	Militia.FlagForConflict(this, this:GetObjVar("militiaBeaconRadius") or ServerSettings.Militia.VulnerabilityRadius)
	this:ScheduleTimerDelay(pulseInterval,"Pulse")
end
