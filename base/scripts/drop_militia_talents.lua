eventController = nil
function GetEventController( ... )
	if not(eventController) then
		eventController = FindObject(SearchModule("militia_king_of_the_hill_controller"))
	end

	return eventController
end

RegisterEventHandler(EventType.Message,"HasDiedMessage",
	function (killer)
		GetEventController()

		if(eventController) then
			local winner = eventController:GetObjVar("Winner")
			if(winner and winner ~= 0) then
				local damagers = this:GetObjVar("Damagers") or {killer}
		        local numIndexedTable = {}
		        local counter = 1
		        for player, damageAmount in pairs(damagers) do
		            if (player:IsValid() and not IsDead(player) and Militia.GetId(player) == winner) then
		                numIndexedTable[counter] = player
		                counter = counter + 1
		            end
		        end

		        local numMilitia = #numIndexedTable
		        if(#numIndexedTable > 0) then		        	
			        local talentAmount = math.ceil(ServerSettings.Militia.Events[2].MilitiaMobTalentPool / numMilitia)
			        talentAmount = math.clamp(talentAmount, ServerSettings.Militia.Events[2].MilitiaMobMinReward, ServerSettings.Militia.Events[2].MilitiaMobMaxReward)

			        for i,playerObj in pairs(numIndexedTable) do
			        	Militia.RewardMilitiaCurrency(playerObj,talentAmount)			        	
				    end
			    end
		    end
	    end
	end)

RegisterEventHandler(EventType.ModuleAttached,"drop_militia_talents",
	function ( ... )
		this:SetSharedObjectProperty("Title","Militia Event")
	end)

local pulseInterval = TimeSpan.FromSeconds(6)
RegisterEventHandler(EventType.Timer, "Pulse",function()Pulse()end)
this:ScheduleTimerDelay(pulseInterval,"Pulse")

function Pulse()
	if not(IsDead(this)) then
		this:PlayEffectWithArgs("BuffEffect_F",3.0,"Bone=Ground")
		this:ScheduleTimerDelay(pulseInterval,"Pulse")
	end
end
