MIN_RELICS = 1000

local relicTemplates = {
	"relic_pirate",
	"relic_orc",
	"relic_outlaw",
	"relic_undead",
	"relic_giant",
	"relic_cultist"
}

function GetTotals()
	relicCounts = this:GetObjVar("RelicCounts") or {}

	local count = 0
	local total = 0
	for i,relicTemplate in pairs(relicTemplates) do
		count = count + math.min(MIN_RELICS,(relicCounts[relicTemplate] or 0))
		total = total + MIN_RELICS
	end

	return count,total
end

if(HasMultipleUniverses()) then
	local otherControllers = FindObjects(SearchModule("dead_gate_controller"))
	for i,j in pairs(otherControllers) do
		if (j ~= this) then
			this:Destroy()
		end
	end
else
	-- dont need this if there is only one Universe running
	-- NOTE: add relic count check to make sure we dont destroy the actual gate by accident
	local count = GetTotals()
	if(count == 0) then
		this:Destroy()
	end
end

require 'teleporter_map'

BUFF_TIME_HOURS = 1
BUFF_INCREASE = 0.2

deadGateObj = nil

AddView("NearbyPlayer", SearchPlayerInRange(10), 1.0)

local relicNames = {}
for i,relicTemplate in pairs(relicTemplates) do
	relicNames[relicTemplate] = GetTemplateObjectName(relicTemplate)
end

this:SetObjectTag("DeadGateController")

RegisterEventHandler(EventType.EnterView, "NearbyPlayer",
	function(playerObj)
		--only players
		if (not playerObj:IsPlayer()) then
			return
		end

		if (this:HasObjVar("IsActivated")) then
			AttunePlayer(playerObj)
		else
			--get the backpack object
			local backpackObj = playerObj:GetEquippedObject("Backpack")
			
			local relicItems = FindItemsInContainerRecursive(backpackObj,
	            function(item)           
	            	local templateId = item:GetCreationTemplateId()

	            	for i,relicTemplate in pairs(relicTemplates) do
	                	if(templateId == relicTemplate) then
	                		return true
	                	end
	                end
	                return false
	            end)
			
			if(#relicItems) then
				--add buffs for each relic
				local gateRelicCountData = this:GetObjVar("RelicCounts") or {}
				local playerRelicCountData = playerObj:GetObjVar("RelicCounts") or {}

				for i,item in pairs(relicItems) do
					local templateId = item:GetCreationTemplateId()
					
					playerObj:PlayEffect("LightningCloudEffect",0.5)
					playerObj:SendMessage("AddSkillBuff",playerObj,"All",BUFF_INCREASE,BUFF_TIME_HOURS*60*60,templateId,true)				
					
					local count = gateRelicCountData[templateId] or 0
					local playerCount = playerRelicCountData[templateId] or 0
					if(count < MIN_RELICS) then
						local stackCount = GetStackCount(item)
						gateRelicCountData[templateId] = count + stackCount
						playerRelicCountData[templateId] = playerCount + stackCount				
					end

					item:Destroy()
				end		

				this:SetObjVar("RelicCounts",gateRelicCountData)
				playerObj:SetObjVar("RelicCounts",playerRelicCountData)

				UpdateGate()
			end
		end
	end)

function AttunePlayer(playerObj)
	if not(playerObj:HasObjVar("IsAttuned")) then
		playerObj:SetObjVar("IsAttuned",true)
		playerObj:SystemMessage("You have been attuned for cross universe travel.","info")
		playerObj:PlayEffect("HolyEffect")
		playerObj:PlayObjectSound("event:/magic/misc/magic_water_greater_heal",false)
	end
end
RegisterEventHandler(EventType.Message,"Attune",
	function (id)
		AttunePlayer(GameObj(tonumber(id)))
	end)

function ActivateGate()
	this:SetObjVar("IsActivated",true)
	this:SetObjVar("Destination",this:GetLoc())
	this:SetObjVar("DestinationMap","NewCelador")
	this:SetObjVar("DestinationSubregion","UpperPlains")
	this:SetObjVar("ChooseUniverse",true)

	AddUseCase(this,"Activate",true)

	for i,playerObj in pairs(GetViewObjects("NearbyPlayer")) do
		AttunePlayer(playerObj)
		playerObj:PlayLocalEffect(this,"ScreenShakeEffect", 0.3,"Magnitude=10")
	end
end

function UpdateGate()
	local count,total = GetTotals()
	local pctComplete = count/total
	
	if(pctComplete >= 1) then
		deadGateObj:SetVisualState("Stage4")
		if not(this:HasObjVar("IsActivated")) then
			ActivateGate()
		end
	elseif(pctComplete >= 0.66) then
		deadGateObj:SetVisualState("Stage3")
	elseif(pctComplete >= 0.33) then
		deadGateObj:SetVisualState("Stage2")
	else
		deadGateObj:SetVisualState("Stage1")
	end

	local tooltipStr = nil
	if(IsActive()) then
		tooltipStr = "Activated"
	else
		tooltipStr = "Inactive\n\n"

		relicCounts = this:GetObjVar("RelicCounts") or {}
		for i,relicTemplate in pairs(relicTemplates) do
			tooltipStr = tooltipStr .. relicNames[relicTemplate] .. ": "..(relicCounts[relicTemplate] or 0).." / ".. MIN_RELICS .."\n"
		end
	end

	SetTooltipEntry(this,"info",tooltipStr)
end

function IsActive()
	return this:GetObjVar("IsActivated") == true
end

function OnLoad( ... )
	results = FindPermanentObjects(PermanentObjSearchHasObjectTag("DeadGate"))
	if(#results > 0) then
		deadGateObj = results[1]
	end

	UpdateGate()
end

RegisterEventHandler(EventType.ModuleAttached,"dead_gate_controller",function ( ... )
	OnLoad()	
end)

RegisterEventHandler(EventType.LoadedFromBackup,"",function ( ... )
	OnLoad()
end)

RegisterEventHandler(EventType.Message,"AddRelic",function (relicTemplate,count)
	local count = tonumber(count) or 1
	if not(IsInTableArray(relicTemplates,relicTemplate)) then 
		return
	end

	local gateRelicCountData = this:GetObjVar("RelicCounts") or {}
	local curCount = gateRelicCountData[relicTemplate] or 0
	if(curCount < MIN_RELICS) then
		gateRelicCountData[relicTemplate] = math.min(MIN_RELICS,curCount + count)
	end

	this:SetObjVar("RelicCounts",gateRelicCountData)

	UpdateGate()		
end)