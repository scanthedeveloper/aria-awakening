NUM_STAGES = 12
PULSE_FREQUENCY = TimeSpan.FromMinutes(0.25)

function UpdateVisualState()
	this:ScheduleTimerDelay(PULSE_FREQUENCY,"UpdateVisualState")

	local militiaId = this:GetObjVar("MilitiaId")
	if(militiaId) then
		local stage = 0
		local spiritWoodCount = GlobalVarReadKey("Militia.GlobalResources."..tostring(militiaId), "Spiritwood") or 0
		if(spiritWoodCount > 0) then
			stage = math.max(1,math.floor(spiritWoodCount / ServerSettings.Militia.GlobalResources.MaximumValue * 12))
		end

		if(stage == 0) then
			this:SetSharedObjectProperty("VisualState","Stage1")
			this:SetScale(Loc(0.001,0.001,0.001))
		else
			this:SetScale(Loc(1,1,1))
			this:SetSharedObjectProperty("VisualState","Stage"..tostring(stage))
		end

		SetTooltipEntry(this,"amount","Spiritwood: "..spiritWoodCount .." / ".. ServerSettings.Militia.GlobalResources.MaximumValue)
	end	
end

RegisterSingleEventHandler(EventType.ModuleAttached,"spiritwood_lumber_pile",
	function ( ... )
		UpdateVisualState()
	end)

RegisterSingleEventHandler(EventType.LoadedFromBackup,"",
	function ( ... )
		UpdateVisualState()
	end)

RegisterEventHandler(EventType.Message,"UpdateVisualState",
	function ( ... )
		UpdateVisualState()
	end)

RegisterEventHandler(EventType.Timer,"UpdateVisualState",
	function ( ... )
		UpdateVisualState()		
	end)

RegisterEventHandler(EventType.Message,"AddWood",
	function (amount)
		Militia.AddGlobalResource(this:GetObjVar("MilitiaId"),"Spiritwood",amount)
	end)