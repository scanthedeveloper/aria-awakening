require 'incl_magic_sys'

--TODO
-- Mana Take on Cast
-- Mana Refund on cancel

mEqBonusDict = {}
SPV_MAX_EFFECTIVENESS_MOD = 20
SPV_MIN_EFFECTIVENESS_MOD = 15

mCastingDisplayName = ""
mCurSpell = nil
mPrimedSpell = nil
mQueuedTarget = nil
mQueuedTargetLoc = nil
mCombatStance = nil
mConSize = 0
mConsumed = {}
mConSuccess = {}
mFreeSpell = false

--Messages
--ObjVars

--Spells
mySpellsFlyingDict = {}
myBonusDict = {}
mSpellSource = nil
mAutoTarg = nil
mAutoTargLoc = nil
mNoTarget = nil

--scroll casting support
mScrollObj = nil


--
function GetSpellTargetType(spellName)
	Verbose("Magic", "GetSpellTargetType", spellName)
	local myTarget = GetSpellInformation(spellName, "TargetType")
	--DebugMessage("MyTarget return " .. tostring(myTarget))
	if(myTarget == "TargetSelf") then return this end
	if(myTarget == nil) then return nil end
	if(myTarget == "targetMobile") then return "RequestTarget" end
	if(myTarget == "targetObject") then return "RequestTarget" end
	if(myTarget == "LeftHand") then 
		return this:GetEquippedObject("LeftHand")
	end
	if(myTarget == "RightHand") then 
		return this:GetEquippedObject("RightHand")
	end
	if(myTarget == "targetLocation") then return "RequestLocation" end
	return nil

end

function ValidateSpellCastTarget(spellName,spellTarget,spellSource)
	Verbose("Magic", "ValidateSpellCastTarget", spellName,spellTarget,spellSource)
	local targetType = GetSpellInformation(spellName,"TargetType")

	if ( not IsInSpellRange(spellName, spellTarget, this)) then
		this:SystemMessage("Not in range.", "info")
		return false
	elseif ( SpellData.AllSpells[spellName].TargetValidate and not SpellData.AllSpells[spellName].TargetValidate(this, spellTarget) ) then
		return false
	elseif ( spellName == "Resurrect" and not ValidResurrectTarget(this, spellTarget) ) then
		return false
	elseif ( spellTarget ~= nil and targetType == "targetMobile" and not(spellTarget:IsMobile())) then
		if not (spellTarget:HasObjVar("Attackable")) then
			return false
		end
	elseif( not LineOfSightCheck(spellName, spellTarget)) then
		this:SystemMessage("Cannot see that.", "info")
		return false
	elseif (not TargetDeadCheck(spellName,spellTarget)) then
		local mustBeDead = GetSpellInformation(spellName, "TargetMustBeDead") 
		local mustBeAlive = GetSpellInformation(spellName, "TargetMustBeAlive")
		if (mustBeDead) then
			this:SystemMessage(spellTarget:GetName() .. " is alive. Target must be dead. " .. spellName)
			return false
		end
		if (mustBeAlive) then
			this:SystemMessage(spellTarget:GetName() .. " is dead. Target must be alive. " .. spellName)
			return false
		end
	elseif spellTarget:IsCloaked() and (not ShouldSeeCloakedObject(this, spellTarget)) then
		if not (this:IsPlayer()) then 
			this:SendMessage("CannotSeeTarget", spellTarget) 
			return false
		else
			this:SystemMessage("Cannot see that.", "info")
			return false
		end
	elseif IsAttackTypeSpell(spellName) and not ValidCombatTarget(this, spellTarget) then
		return false
	elseif (SpellData.AllSpells[spellName].BeneficialSpellType == true and SpellData.AllSpells[spellName].SkipCriminalCheck ~= true) and not AllowFriendlyActions(this, spellTarget, true) then
		return false
	elseif (GetSpellInformation(spellName, "TargetResource")) then
		if (spellTarget:GetObjVar("ResourceType") ~= GetSpellInformation(spellName, "TargetResource")) then
			return false		
		end
	end

	return true
end

--- Decides whether a caster is able to resurrect a target and displays fail sysmsg for caster.
-- @param caster
-- @param target
-- @return true if success
function ValidResurrectTarget(caster, target)
	if ( not IsDead(target) ) then
		caster:SystemMessage("Cannot resurrect that which is not dead.","info")
		return false
	else
		if ( caster:IsPlayer() ) then
			if ( IsPet(target) ) then
				if ( GetSkillLevel(caster, "AnimalLoreSkill") < 80 ) then
					caster:SystemMessage("Not skilled enough in the lore of animals to resurrect this pet.","info")
					return false
				end
			elseif ( not IsPlayerObject(target) ) then
				caster:SystemMessage("Can only resurrect dead players or pets.","info")
				return false
			end
		end
	end
	
	return true
end

function UpdateSpellTarget(newTarget)
	if (this:GetObjVar("AutotargetEnabled") and this:IsPlayer() and this ~= newTarget) then
		mAutoTarg = newTarget
	end
end

function PrimeSpell(spellName, spellSource)
	Verbose("Magic", "PrimeSpell", spellName, spellSource)
	if(spellSource == nil) then spellSource = this end
	if(spellName == nil) then return false end

	CancelCurrentSpellEffects()

	if ( HasMobileEffect(this, "Silence") ) then
		if(this:IsPlayer()) then
			this:SystemMessage("You are silenced.", "info")
		end
		return false
	end

	--[[ REMOVE PER JEFFERY - 11/26/2019
		if ( HasMobileEffect(this, "FireArrow") ) then
			if(this:IsPlayer()) then
				this:SystemMessage("Cannot cast right now.", "info")
			end
			return false
		end
	]]

	if( IsMobileDisabled(this) ) then
		if ( this:IsPlayer() ) then
			this:SystemMessage("Cannot cast right now.", "info")
		end
		return false
	end

	if ( this:HasTimer("SpellGlobalCooldownTimer") ) then
		if ( this:IsPlayer() ) then
			this:SystemMessage("You are still recovering.", "info")
		end
		return false
	end

	if(spellName == "Reflectivearmor") then
		if(this:HasModule("sp_reflective_armor")) then
			this:SystemMessage("You are already protected.", "info")
			return
		end
		if(this:HasTimer("ReflectiveArmorTimer")) then
			this:SystemMessage("Cannot cast that spell again yet.", "info")
			return
		end
	end

	--DebugMessage("Priming..")
	local myTable = GetSpellSchoolTable(spellName)
	if(myTable == nil) then
		if(this:IsPlayer()) then	
			--LuaDebugCallStack(spellName)
			this:SystemMessage(spellName.. " is not a valid spell.","info")
		end
		return false
	end

	spellSource:SendMessage("BreakInvisEffect", "Casting", "PrimeSpell")

	if not( CheckMana(spellName, spellSource, mScrollObj) ) then 
		if ( this:IsPlayer() ) then
			this:SystemMessage("Not enough mana.", "info")
		end
		return
	end
	
	local myTargType = GetSpellTargetType(spellName)
	if ( myTargType == "RequestTarget" and not(mNoTarget) and ShouldAutoTarget(spellName) ) then
		if(not ValidateSpellCastTarget(spellName,mAutoTarg,spellSource)) then
			return false
		end
		--FaceObject(this,mAutoTarg)
	end

	local myCastTime = GetSpellCastTime(spellName, spellSource, mScrollObj)

	if (myCastTime == nil) then 
		--DebugMessage("[ERROR] Invalid Spell Casttime")
		return false 
	elseif( mScrollObj ) then
		myCastTime = myCastTime * GetCombatMod(CombatMod.ScrollCastPenaltyTimes, 3)
	end

	this:PlayAnimation("cast")

	if ( this:IsPlayer() and SpellData.AllSpells[spellName].PowerWords ~= nil ) then
		this:NpcSpeech(SpellData.AllSpells[spellName].PowerWords, "combat")
	end

	local castingTime
	if(myCastTime > 0) then
	--D*ebugMessage("Cast Time:" .. tostring(myCastTime))
	--D*ebugMessage("spellName: " ..spellName)

		local castFX = GetSpellInformation(spellName, "SpellPrimeFXName")
		if (castFX ~= nil) then
			local mySpEffectArgs = GetSpellInformation(spellName, "SpellPrimeFXArgs")
		--D*ebugMessage("CFX: " ..tostring(castFX) .. " CTIM: " ..tostring(myCastTime) .. this:GetName())
			if(mySpEffectArgs) then
				this:PlayEffectWithArgs(castFX,myCastTime+1.0,mySpEffectArgs)
			else
				this:PlayEffect(castFX,myCastTime+1.0)--,"Bone=Ground")
			end			
		end
		local castFX2 = GetSpellInformation(spellName, "SpellPrimeFX2Name")
		if (castFX2 ~= nil) then
			local mySpEffectArgs2 = GetSpellInformation(spellName, "SpellPrimeFX2Args")
		--D*ebugMessage("CFX: " ..tostring(castFX2) .. " CTIM: " ..tostring(myCastTime) .. this:GetName())
			if(mySpEffectArgs2) then
				this:PlayEffectWithArgs(castFX2,myCastTime+0.5,mySpEffectArgs2)
			else
				this:PlayEffect(castFX2,myCastTime+0.5)--,"Bone=Ground")
			end			
		end

		local mySound = GetSpellInformation(spellName, "SpellPrimeSFX")
		if not (mySound == nil) then
			--DebugMessage("SpellSound: " .. mySound)
			this:PlayObjectSound(mySound,false,myCastTime)
		end
		castingTime = TimeSpan.FromMilliseconds( myCastTime * 1000 )
		this:ScheduleTimerDelay(castingTime, "SpellPrimeTimer", spellName, spellSource)

		if ( IsPlayerCharacter(this) ) then	
			local spellDisplayName = SpellData.AllSpells[spellName].SpellDisplayName

			this:SendClientMessage("StartCasting",myCastTime)
			mCastingDisplayName = tostring(spellDisplayName)
			this:SetObjVar("LastSpell",spellDisplayName)

			if(myTargType == "RequestTarget") or (myTargType == "RequestLocation") then
				RequestSpellTarget(spellName)
			end

			ProgressBar.Show(
			{
				TargetUser = spellSource,
				Label="Casting "..spellDisplayName,
				Duration=myCastTime,
				PresetLocation="UnderPlayer",
			})
		end

		if not(CanMoveWhileCasting(spellName)) then
			mSwingReady.RightHand = false
			mSwingReady.LeftHand = false
			SetMobileMod(this, "Disable", "CastFreeze", true)
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(myCastTime), "CastFreezeTimer")
		end
		return true
	else
		--AdjustCurMana(this,-manaCost)
		this:FireTimer("SpellPrimeTimer", spellName, spellSource)
		return true
	end
end

RegisterEventHandler(EventType.Timer, "CastFreezeTimer", function()
	DelaySwingTimer(ServerSettings.Combat.CastSwingDelay, "All")
	SetMobileMod(this, "Disable", "CastFreeze", nil)
end)

function IsInstantHitSpell(spellName)
	local mySpInfo = GetSpellInformation(spellName, "InstantHitSpell")
	if mySpInfo == nil then mySpInfo = false end
	return mySpInfo
end

function IsHitTypeSpell(spellName)
	local myHitType = GetSpellInformation(spellName, "SpellType")
	if(myHitType == "MagicAttackTypeSpell") then return true end
	return false
end

function IsAttackTypeSpell(spellName)
	return ( 
		SpellData.AllSpells[spellName]
		and
		SpellData.AllSpells[spellName].AttackSpellType == true
	)
end

function IsBeneTypeSpell(spellName)
	return ( 
		SpellData.AllSpells[spellName]
		and
		SpellData.AllSpells[spellName].BeneficialSpellType == true
	)
end

function SetSpellTravelTime(spellName, spTarget, spellSource)
	Verbose("Magic", "SetSpellTravelTime", spellName, spTarget, spellSource)
	
	if (spellSource == nil) then spellSource = this end

	spellSource:SendMessage("BreakInvisEffect", "Action", "SpellTravelTime")

	if not ( mFreeSpell == true ) then

		if not( CheckMana(spellName, spellSource, mScrollObj) ) then 
			return
		end

		if not ( CheckReagents(spellName, spellSource, mScrollObj) ) then
			return
		end

		if not( CheckSpellCastSuccess(spellName, spellSource, mScrollObj) ) then
			return
		end
	end
	
	-- dismount them
	local noDismount = GetSpellInformation(spellName, "NoDismount")
	if not(noDismount) then
		DismountMobile(this)
	end

	mFreeSpell = false

	local manaCost = GetManaCost(spellName, this)

	local mySpEffectType = GetSpellInformation(spellName, "effectType")
	local mySpEffect = GetSpellInformation(spellName, "SpellFXName")
	local overrideRate = 10
	local isRaySpell = false
	local timer = 0

	local spellFireAnim = GetSpellInformation(spellName, "SpellFireAnim")
	if( spellFireAnim ~= nil ) then
		this:PlayAnimation(spellFireAnim)
	end

	if(mySpEffectType == "RayProjectile") then
		overrideRate = 0.01
		local mySpEffectArgs = GetSpellInformation(spellName, "SpellFXArgs")
		this:PlayProjectileEffectTo(mySpEffect,spTarget,overrideRate,1,mySpEffectArgs)
	elseif(mySpEffectType == "Projectile") then 
		local mySpEffectDespawnDelay = GetSpellInformation(spellName, "SpellFXDespawnDelay") or 0
		overrideRate = GetSpellInformation(spellName, "SpellTravelRate")
		isReverseProjectile = GetSpellInformation(spellName, "IsReverseProjectile")
		if (overrideRate == nil ) then overrideRate = 10 end
		local bodyOffset = .5
		local castLoc = this:GetLoc()
		local targLoc = spTarget:GetLoc()
		local dist = targLoc:Distance(castLoc)
		timer = (dist - .5) / overrideRate	
		local indirect = GetSpellInformation(spellName, "IndirectProjectile")
		local spellLaunchFX = GetSpellInformation(spellName, "SpellLaunchFX")
		--DebugMessage("spellLaunch:" ..tostring(spellLaunchFX))
		if(indirect == true and spellLaunchFX ~= nil) then
			this:PlayEffect(spellLaunchFX)
		elseif( mySpEffect ~= nil ) then
			--DebugMessage ("Projectile")
			local mySpEffectArgs = GetSpellInformation(spellName, "SpellFXArgs")
			local mySpEffectDelay = GetSpellInformation(spellName, "SpellFXDelay")
			local mySpFlightSound = GetSpellInformation(spellName, "SpellFlightSFX")
			if(mySpEffectDelay) then				
				CallFunctionDelayed(TimeSpan.FromSeconds(mySpEffectDelay),
					function()
						if (isReverseProjectile) then
							if (mySpFlightSound) then
								spTarget:PlayProjectileEffectToWithSound(mySpEffect,this,mySpFlightSound,overrideRate,timer,mySpEffectArgs, mySpEffectDespawnDelay)
							else
								spTarget:PlayProjectileEffectTo(mySpEffect,this,overrideRate,timer,mySpEffectArgs, mySpEffectDespawnDelay)
							end
						else
							if (mySpFlightSound) then
								this:PlayProjectileEffectToWithSound(mySpEffect,spTarget,mySpFlightSound,overrideRate,timer,mySpEffectArgs, mySpEffectDespawnDelay)
							else
								this:PlayProjectileEffectTo(mySpEffect,spTarget,overrideRate,timer,mySpEffectArgs, mySpEffectDespawnDelay)
							end
						end
					end)
			else
				if (isReverseProjectile) then
					if (mySpFlightSound) then
						spTarget:PlayProjectileEffectToWithSound(mySpEffect,this,mySpFlightSound,overrideRate,timer,mySpEffectArgs)
					else
						spTarget:PlayProjectileEffectTo(mySpEffect,this,overrideRate,timer,mySpEffectArgs)
					end
				else
					if (mySpFlightSound) then
						this:PlayProjectileEffectToWithSound(mySpEffect,spTarget,mySpFlightSound,overrideRate,timer,mySpEffectArgs)
					else
						this:PlayProjectileEffectTo(mySpEffect,spTarget,overrideRate,timer,mySpEffectArgs)
					end
				end
			end
		end
	else
		if( mySpEffect ~= nil ) then
			local mySpEffectArgs = GetSpellInformation(spellName, "SpellFXArgs") or ""
			local mySpEffectDelay = GetSpellInformation(spellName, "SpellFXDelay") 
			local mySpEffectDuration = GetSpellInformation(spellName, "SpellFXDuration") or 0
			if(mySpEffectDelay) then				
				CallFunctionDelayed(TimeSpan.FromSeconds(mySpEffectDelay),
					function()
						if(mySpEffectArgs ~= nil) then
							spTarget:PlayEffectWithArgs(mySpEffect,mySpEffectDuration, mySpEffectArgs)
						else
							spTarget:PlayEffect(mySpEffect,mySpEffectDuration)
						end
					end)
			else
				if(mySpEffectArgs ~= nil) then
					spTarget:PlayEffectWithArgs(mySpEffect,mySpEffectDuration, mySpEffectArgs)
				else
					spTarget:PlayEffect(mySpEffect,mySpEffectDuration)
				end
			end
		end
	end

	local myLauchSFX = GetSpellInformation(spellName, "SpellLaunchSFX")
	if( myLauchSFX ~= nil ) then
		spellSource:PlayObjectSound(myLauchSFX,false)
	end

	local myDict = mySpellsFlyingDict or {}
	local minTravTime = GetSpellInformation(spellName, "MinTravelTime")
	if(minTravTime ~= nil) then timer = math.max(timer,minTravTime) end
	--if(this:HasObjVar("SpellsInFlight")) then myDict = this:GetObjVar("SpellsInFlight") end	
	
	local myBaseTimer = nil
	local mySpellTime = timer * 1000
	local myTime = ServerTimeMs() + (timer * 1000)

	SendAdjustManaRequest( this, -manaCost, false, mScrollObj )
	ConsumeScroll(spellSource, mScrollObj)
	--mScrollObj = nil
	--AdjustCurMana(this,-manaCost)
	-- Added as part of enchanting system - bphelps
	TryExecuteCastEnchant(this)
	TryDamageAttunedWeapon( spellName )
	
	ApplyReleaseEffects(spellName, spTarget, spellSource, nil)
	local myArgs = {
		spellName,
		spTarget,
		myTime,
		spellSource }
	--DebugMessage(" Vect5: " .. tostring(myArgs[1]) .. " " .. tostring(myArgs[2]) .. " " .. tostring(myArgs[3]) .. " " .. tostring(myArgs[4]))

	if(this:HasObjVar("AutotargetEnabled")) then
		SetCurrentTarget(spTarget)
	end

	local myKey = tostring(spellName) .. tostring(myTime)
	myDict[myKey] = myArgs
	mySpellsFlyingDict = myDict

	if(mySpellTime > 0) then

		CallFunctionDelayed(TimeSpan.FromMilliseconds(mySpellTime), 
			function()

				HandleSpellTravelled(spellName, spTarget, spSource, myKey)
			end)
	else
		HandleSpellTravelled(spellName, spTarget, spSource, myKey)
	end
	
end

function SendAdjustManaRequest( mobile, manaCost , skipMods, scrollObj )
	if( mobile == nil or manaCost == nil or scrollObj ~= nil ) then return end
	mobile:SendMessage("ManaAdjustRequest", manaCost, skipMods)
end

function ApplyReleaseEffects(spellName, spTarget, spellSource, targLoc)
	Verbose("Magic", "ApplyReleaseEffects", spellName, spTarget, spellSource, targLoc)

	if (spTarget ~= nil and not(IsBeneTypeSpell(spellName))) then
		spTarget:SendMessage("AttackedBySpell",this,spellName)
	end
	--DebugMessage("ApplyingReleaseEffect")
	local userReleaseEffect = GetSpellInformation(spellName, "SpellReleaseUserScript")
	if (userReleaseEffect ~= nil) and not this:HasModule(userReleaseEffect) then
		this:AddModule(userReleaseEffect)
		this:SendMessage(spellName .. "SelfEffectApplying")
		--DebugMessage("AddingModule On User: " .. userReleaseEffect)
		if(targLoc ~= nil) then this:SendMessage(spellName .. "SpellTargetLoc",targLoc) end
	end
	if(spTarget == nil) then return end
	local targetReleaseEffect = GetSpellInformation(spellName, "SpellReleaseTargetScript")
	if (targetReleaseEffect ~= nil) and not (spTarget:HasModule(targetReleaseEffect)) then
		spTarget:AddModule(targetReleaseEffect)
		spTarget:SendMessage("TargetReleaseEffect" .. targetReleaseEffect,spellSource,spTarget,targLoc)
	end
end

function ApplySpellCompletionEffects(spellName, spTarget, spellSource)
	Verbose("Magic", "ApplySpellCompletionEffects", spellName, spTarget, spellSource)
	local mobileEffect = GetSpellInformation(spellName, "MobileEffect")
	if ( mobileEffect ~= nil ) then
		local args = GetSpellInformation(spellName, "MobileEffectArgs") or {}
		-- TODO: Add a bonus of sorts here from spell power or whatever
		StartMobileEffect(this, mobileEffect, this, args)
	end

	local spCasterEffectScript = GetSpellInformation(spellName, "completionEffectUserScript")
	if not(spCasterEffectScript == nil) and not(this:HasModule(spCasterEffectScript)) then
		this:AddModule(spCasterEffectScript)
	end
	--DebugMessage("SendingSpellCompletionMessage")
		if(spCasterEffectScript ~= nil) then this:SendMessage("CompletionEffect"..spCasterEffectScript) end

	local spTargetEffectScript = GetSpellInformation(spellName, "completionEffectTargetScript")
	if not(spTargetEffectScript == nil) and not(spTarget == nil) and not(spTarget:HasModule(spTargetEffectScript)) then
		spTarget:SetObjVar(spTargetEffectScript .. "Source", this)
		spTarget:AddModule(spTargetEffectScript)
		spTarget:SendMessage("SpellEffect" .. spTargetEffectScript,spellSource,spTarget)
	end
end

function ApplySpellEffects(spellName, spTarget, spellSource)
	Verbose("Magic", "ApplySpellEffects", spellName, spTarget, spellSource)
	--DebugMessage("ApplySpellEffects")
	if(spellSource == nil) then spellSource = this end
	local spellType = GetSpellInformation(spellName, "SpellType")
	if spellType == nil then 
		--DebugMessage("[ERROR] **Invalid Spell Type")
		return 
	end
	if (spTarget ~= nil and not(IsBeneTypeSpell(spellName))) then
		spTarget:SendMessage("AttackedBySpell",this,spellName)
	end
	--DebugMessage("**SpellType: " .. spellType)
	if(spellType == "HealTypeSpell") then
		PerformSpellHeal(spellName, spTarget, spellSource)
	end

	if (
		SpellData.AllSpells[spellName] ~= nil 
		and
		SpellData.AllSpells[spellName].BeneficialSpellType == true
		and
		SpellData.AllSpells[spellName].SkipCriminalCheck ~= true
	) then
		if ( spTarget ~= this and IsPlayerCharacter(this) ) then
			CheckCriminalBeneficialAction(this, spTarget)
		end
	end

	if ( spTarget ~= nil or mNoTarget ) then
		local spTargetHitEffectScript = GetSpellInformation(spellName, "spellHitEffectTargetScript")
		if not( spTargetHitEffectScript == nil ) then
			if not(spTarget:HasModule(spTargetHitEffectScript)) then
				spTarget:SetObjVar(spTargetHitEffectScript .. "Source", this)
				spTarget:AddModule(spTargetHitEffectScript)
			end
			spTarget:SendMessage("SpellHitEffect" .. spTargetHitEffectScript, this)
		else
			local targetMobileEffect = GetSpellInformation(spellName, "TargetMobileEffect")
			if ( targetMobileEffect ~= nil and spTarget ) then
				local args = GetSpellInformation(spellName, "TargetMobileEffectArgs") or {}
				-- TODO: Add a bonus of sorts here from spell power or whatever
				spTarget:SendMessage("StartMobileEffect", targetMobileEffect, this, args)
			end

			local mobileEffect = GetSpellInformation(spellName, "MobileEffect")
			if ( mobileEffect ~= nil ) then
				local args = GetSpellInformation(spellName, "MobileEffectArgs") or {}
				-- TODO: Add a bonus of sorts here from spell power or whatever
				StartMobileEffect(this, mobileEffect, spTarget, args)
			end
		end

		local spellHitFX = GetSpellInformation(spellName, "SpellHitFX")
		if( spellHitFX ~= nil ) then
			spTarget:PlayEffect(spellHitFX)
		end

	local mySourceSFX = GetSpellInformation(spellName, "SpellSourceSFX")
	if (mySourceSFX ~= nil) then
		this:PlayObjectSound(mySourceSFX)
	end

		local mySound = GetSpellInformation(spellName, "SpellHitSFX")
		if (mySound ~= nil) then
			--DebugMessage("SpellHitSound: " .. mySound)
			spTarget:PlayObjectSound(mySound)
		end

	end
	spellName = nil
	mNoTarget = nil
end

function TryExecuteCastEnchant( mobileObj )
	
	local enchants = EnchantingHelper.GetUniqueEquipmentEnchants(_Weapon, _Armor)

	--DebugTable( CombatMod )

	for enchant,data in pairs(enchants) do 

		-- If this isn't swing proc'd, or we don't have a mobile mod then we need to leave!
		if( data.CastProc and CombatMod[data.MobileMod] ) then

			local successChance = (math.min(GetCombatMod(CombatMod[data.MobileMod], 0), data.EffectRangeMax)/100)
			
			--DebugMessage("Success: " .. tostring(successChance))
			local success = Success(successChance)

			-- Proc mobile effect on swing!
			if( success ) then
				-- Mobile Effect
				if( data.MobileEffect and HasMobileEffect(this, data.MobileEffect) == false or data.MobieEffectCanStack == true ) then
					StartMobileEffect(
						this, 
						data.MobileEffect, 
						nil, 
						{ 
							Duration = data.MobileEffectDuration or nil, 
						} 
					)
				end

			end

		end

	end
	
	return true
end

-- Attemps to damage the weapon in the player's right hand if it is attuned.
-- @param spellName: the name of the spell cast with the weapon
function TryDamageAttunedWeapon( spellName, ignoreSpell )
	
	local skill = GetSpellInformation(spellName, "Skill") or "MagerySkill"

	if( (skill == "MagerySkill" or ignoreSpell) -- Are they casting an Evocation spell? Or are we ignoring spell check?
		and _Weapon.RightHand.Object -- Is their weapon in their primary hand?
		and Weapon.IsAttuned( _Weapon.RightHand.Object ) -- Is it attuned?
		and Success(ServerSettings.Durability.Chance.AttunedWeapon)  -- Did we successfully damage the weapon?
	) then
		AdjustDurability(_Weapon.RightHand.Object, -1)
	end

end


function PerformSpellLocationActions(spellName,spellTarget, targetLoc, spellSource)
	Verbose("Magic", "PerformSpellLocationActions", spellName,spellTarget, targetLoc, spellSource)

	spellSource:SendMessage("BreakInvisEffect", "Action", "SpellLocationAction")

	if not( CheckMana(spellName, spellSource, mScrollObj) ) then
		return
	end

	if not( CheckReagents(spellName, spellSource, mScrollObj) ) then
		return
	end

	if not( CheckSpellCastSuccess(spellName, spellSource, mScrollObj) )then
		return
	end
	
	-- dismount them
	DismountMobile(this)

	

	local spellFireAnim = GetSpellInformation(spellName, "SpellFireAnim")
	if( spellFireAnim ~= nil ) then
		this:PlayAnimation(spellFireAnim)
	end

	local manaCost = GetManaCost(spellName, this)

	SendAdjustManaRequest( this, -manaCost, false, mScrollObj )
	ConsumeScroll(spellSource, mScrollObj)
	mScrollObj = nil
	--AdjustCurMana(this,-manaCost)
	-- Added as part of enchanting system - bphelps
	TryExecuteCastEnchant(this)

	TryDamageAttunedWeapon( spellName )

	if(spellTarget == nil) then spellTarget = this end

	-- DAB NOTE: This has to happen before SpellTargetResult message
	ApplyReleaseEffects(spellName, spellTarget, spellSource, targetLoc)

	local spCasterEffectScript = GetSpellInformation(spellName, "completionEffectUserScript")
	if not(spCasterEffectScript == nil) and not(this:HasModule(spCasterEffectScript)) then
		this:AddModule(spCasterEffectScript)
	end
	this:SendMessage(spellName .. "SpellTargetResult",targetLoc)

	local mobileEffect = GetSpellInformation(spellName, "MobileEffect")
	if ( mobileEffect ~= nil ) then
		local args = GetSpellInformation(spellName, "MobileEffectArgs") or {}
		StartMobileEffect(this, mobileEffect, targetLoc, args)
	end

	local spellHitFX = GetSpellInformation(spellName, "SpellHitFX")
	if( spellHitFX ~= nil ) then
		PlayEffectAtLoc(spellHitFX,targetLoc)
	end

	local mySound = GetSpellInformation(spellName, "SpellHitSFX")
	if (mySound ~= nil) then
		--DebugMessage("SpellHitSound: " .. mySound)
		this:PlayObjectSound(mySound)
	end


	local myLauchSFX = GetSpellInformation(spellName, "SpellLaunchSFX")
	if( myLauchSFX ~= nil ) then
		spellSource:PlayObjectSound(myLauchSFX,false)
	end
end

--SCAN ADDED
function GetSpellHealAmount(spellName, spellSource)
	Verbose("Magic", "GetSpellHealAmount", spellName, spellSource)
    if(spellSource == nil) then spellSource = this end

	-- If they are using a scroll we want to aleast give them half the benefits of the scroll 
	-- if they don't have higher magery skill.
	if( mScrollObj ) then
		magicSkill = math.max( magicSkill, ServerSettings.Combat.ScrollBaseMagerySkill )
	end
	
	local healAmount = 0
	local magicSkill = GetSkillLevel(spellSource,"ManifestationSkill")
	if(spellName == "Heal") then
		healAmount = (magicSkill / 7.5) + (math.floor(math.random(1, 3) + 0.5))
	elseif(spellName == "Greaterheal") then
		healAmount = (magicSkill * 0.4) + (math.floor(math.random(1, 10) + 0.5))
	
	-- OVERRIDE START
	-- add nature healing touch
	elseif(spellName == "HealingTouch") then
		magicSkill = GetSkillLevel(spellSource,"NatureMagicSkill")
		healAmount = (magicSkill * 0.25) + (math.floor(math.random(1, 6) + 0.5))
	-- OVERRIDE END
	
	else
		DebugMessage("Unknown healing spell "..spellName)
		return 0
	end
	--round to nearest int
	healAmount = math.floor(healAmount + 0.5)

	return healAmount * 4
end

--[[ SCAN DISABLED
function GetSpellHealAmount(spellName, spellSource)
	Verbose("Magic", "GetSpellHealAmount", spellName, spellSource)
    if(spellSource == nil) then spellSource = this end
	
	--DebugMessage( "scrollObj", mScrollObj )

	local healAmount = 0
	local magicSkill = GetSkillLevel(spellSource,"MagerySkill")

	-- If they are using a scroll we want to aleast give them half the benefits of the scroll 
	-- if they don't have higher magery skill.
	if( mScrollObj ) then
		magicSkill = math.max( magicSkill, ServerSettings.Combat.ScrollBaseMagerySkill )
	end

	if(spellName == "Heal") then
		healAmount = (magicSkill / 7.5) + (math.floor(math.random(1, 3) + 0.5))
	elseif(spellName == "Greaterheal") then
		healAmount = (magicSkill * 0.4) + (math.floor(math.random(1, 10) + 0.5))
	else
		DebugMessage("Unknown healing spell "..spellName)
		return 0
	end
	--round to nearest int
	healAmount = math.floor(healAmount + 0.5)

	return healAmount * 4
end
]]

function IsInSpellRange(spellName, spellTarget, spellSource)
	Verbose("Magic", "IsInSpellRange", spellName, spellTarget, spellSource)
	local myLoc = this:GetLoc()

	local topmostObj = spellTarget:TopmostContainer() or spellTarget
	local theirLoc = topmostObj:GetLoc()
	
	local bodyOffset = GetBodySize(this) or ServerSettings.Combat.DefaultBodySize
	local theirOffset = GetBodySize(spellTarget) or ServerSettings.Combat.DefaultBodySize

	local dist = theirLoc:Distance(myLoc) 
	local spellRange = GetSpellInformation(spellName, "SpellRange") 
	if (spellRange == nil) then spellRange = DEFAULT_SPELL_RANGE end
	if((spellRange + bodyOffset + theirOffset) >= dist) then return true end
	return false
end

function IsLocInSpellRange(spellName, targetLoc, spellSource)
	Verbose("Magic", "IsLocInSpellRange", spellName, targetLoc, spellSource)
	local myLoc = this:GetLoc()
	local theirLoc = targetLoc
	local dist = theirLoc:Distance(myLoc) 
	local spellRange = GetSpellInformation(spellName, "SpellRange") 
	if (spellRange == nil) then spellRange = DEFAULT_SPELL_RANGE end
	if((spellRange) >= dist) then return true end
	return false

end

function LineOfSightCheck(spellName, spellTarget)
	Verbose("Magic", "LineOfSightCheck", spellName, spellTarget)
	local reqLOS = GetSpellInformation(spellName, "requireLineOfSight") 
	local targetType = GetSpellInformation(spellName,"TargetType")
	if( reqLOS ~= nil and reqLOS ) then
		if(targetType == "targetObject" or targetType == "targetMobile") then
			local topmostObj = spellTarget
			if(targetType == "targetObject") then
				topmostObj = spellTarget:TopmostContainer() or spellTarget
			end
			
			if( not(this:HasLineOfSightToObj(topmostObj,ServerSettings.Combat.LOSEyeLevel)) ) then
				return false
			end
		elseif( targetType == "targetLocation" and not(this:HasLineOfSightToLoc(spellTarget,ServerSettings.Combat.LOSEyeLevel)) ) then
			return false
		end
	end

	return true
end

function TargetDeadCheck(spellName, spellTarget)
	Verbose("Magic", "TargetDeadCheck", spellName, spellTarget)
	local mustBeDead = GetSpellInformation(spellName, "TargetMustBeDead") 
	local mustBeAlive = GetSpellInformation(spellName, "TargetMustBeAlive")
	if (mustBeDead) then
		if (not IsDead(spellTarget)) then
			return false
		end
	end
	if (mustBeAlive) then
		if (IsDead(spellTarget)) then
			return false
		end
	end
	return true
end

function PerformSpellHeal(spellName, spellTarget, spellSource)
	Verbose("Magic", "PerformSpellHeal", spellName, spellTarget, spellSource)
	if ( spellSource == nil ) then spellSource = this end
	if ( IsDead(spellTarget) ) then
		spellSource:SystemMessage("Your spell fails to stir the corpse.", "info")
		return
	end

	if ( HasMobileEffect(spellTarget, "MortalStruck") ) then
		this:SystemMessage("Your target is mortally wounded.", "info")
		return false
	end

	--[[ SCAN DISABLED ENRAGED HEALING DISABLEMENT
	if ( HasMobileEffect(spellTarget, "Enrage") ) then
		this:SystemMessage("Your target is enraged.", "info")
		return false
	end
	]]

	--DebugMessage("ShouldCriminalProtect: " .. tostring( ShouldCriminalProtect(this, spellTarget, true) ) )
	if ( ShouldCriminalProtect(this, spellTarget, true) ) then
		--this:SystemMessage("Your magic seems to have no effect.", "info")
		return false
    end
    
	local healAmount = GetSpellHealAmount(spellName, spellSource)
	-- variance
	healAmount = randomGaussian(healAmount, healAmount * 0.05)
	spellTarget:SendMessage("HealRequest", healAmount, this)
	
	if ( HasMobileEffect(this, "Empower") ) then
		local effects = this:GetObjVar("MobileEffects")
		if ( effects and effects.Empower and effects.Empower[2] and effects.Empower[2].Modifier ) then
			spellTarget:SendMessage("StartMobileEffect", "EmpowerAoE", this, {Heal=(healAmount * effects.Empower[2].Modifier)})
		end
	end

	return
end



function CanMoveWhileCasting(spellName)
	local myRet = GetSpellInformation(spellName, "CanMoveWhileCasting")
	if (myRet == true) then return true end
	return false
end

function IsSpellEnabled(spellName, spellSourceObj)
	if ( not SpellData.AllSpells[spellName] or not SpellData.AllSpells[spellName].SpellEnabled ) then
		if ( spellSourceObj and IsPlayerCharacter(spellSourceObj) ) then
			spellSourceObj:SystemMessage("Use of "..spellName.." is disabled.", "info")
		end
		return false
	end
	return true
end

--Casting Activity


function HandleSpellCastCommand(spellName, spellTargetObj, spellSourceObj)
	Verbose("Magic", "HandleSpellCastCommand", spellName, spellTargetObj, spellSourceObj)
	local spellTarget = nil
	local spellSource = this 
	if(spellSourceObj ~= nil) then
		local spellSourceReq = GameObj(tonumber(spellSourceObj))
		if(not spellSourceReq:IsValid()) then spellSource = this end
		if(spellSourceReq:TopmostContainer() == this) or (spellSourceReq:GetObjVar("controller") == this) then
			spellSource = spellSourceReq
		else
			this:SystemMessage("Invalid spell source: Reverting to self.")
		end
	end
	if(spellTargetObj ~= nil) then
		spellTarget = GameObj(tonumber(spellTargetObj))
	end

	--If this has autotarget enabled, change auto target if this is not set to be a target
	if (this:HasObjVar("AutotargetEnabled")) then
		--If this is a target, do nothing
		if (this ~= mCurrentTarget) then
			mAutoTarg = mCurrentTarget
		end
	else
		mAutoTarg = nil
	end

	mScrollObj = nil
	mNoTarget = nil
	CastSpell(spellName, spellSource, spellTarget)
end

function HandleSongCastCommand(songName, songTargetObj, songSourceObj)
	Verbose("Magic", "HandleSongCastCommand", songName, songTargetObj, songSourceObj)
	local songTarget = nil
	local songSource = this 
	if(songSourceObj ~= nil) then
		local songSourceReq = GameObj(tonumber(songSourceObj))
		if(not songSourceReq:IsValid()) then songSource = this end
		if(songSourceReq:TopmostContainer() == this) or (songSourceReq:GetObjVar("controller") == this) then
			songSource = songSourceReq
		else
			this:SystemMessage("Invalid spell source: Reverting to self.")
		end
	end
	if(songTargetObj ~= nil) then
		songTarget = GameObj(tonumber(songTargetObj))
	end

	--If this has autotarget enabled, change auto target if this is not set to be a target
	if (this:HasObjVar("AutotargetEnabled")) then
		--If this is a target, do nothing
		if (this ~= mCurrentTarget) then
			mAutoTarg = mCurrentTarget
		end
	else
		mAutoTarg = nil
	end

	mScrollObj = nil
	--DebugMessage( "Use song: " .. songName )
	CastSong(songName, songSource, songTarget)
end

function HandleScrollCastRequest(spellName, scrollObj)
	Verbose("Magic", "HandleScrollCastRequest", spellName, scrollObj)
	if(spellName == nil or scrollObj == nil) then return end

	mScrollObj = scrollObj
	mNoTarget = nil
	CastSpell(spellName, this)
end

-- if noTarget is true, a spell that normally requires a target will still fire without requesting one
function HandleSpellCastRequest(spellName,spellSource,preDefTarg,targetLoc,noTarget)
	Verbose("Magic", "HandleSpellCastRequest", spellName,spellSource,preDefTarg,targetLoc)
	if(spellName == nil) then return end
	if(spellSource == nil) then spellSource = this end

	if not(preDefTarg == nil) and not (preDefTarg:IsValid()) and targetLoc == nil then
		--DebugMessage("Error targetloc is nil and no target")
		preDefTarg = nil
		return
	end
	mAutoTarg = preDefTarg
	mAutoTargLoc = targetLoc
	mNoTarget = noTarget
	if mScrollObj then
		CancelSpellCast()
		mScrollObj = nil
	end

	--DebugMessage("Sp Name:" .. tostring(spellName) .. " Targ: " .. mAutoTarg:GetName())
	CastSpell(spellName, spellSource)
end

function CastSpell(spellName, spellSource, spellTarget)
	Verbose("Magic", "CastSpell", spellName, spellSource, spellTarget)
	if  not( IsSpellEnabled(spellName, spellSource) ) then return end
	local player = spellSource:IsPlayer()
	if( mPrimedSpell ~= nil ) then		
		if( player ) then
			spellSource:SendClientMessage("ClearPrimed")			
			spellSource:PlayAnimation("idle")
		end
	end


	if( spellTarget ~= nil ) then
		local targetType = GetSpellInformation(spellName,"TargetType")
		if(targetType == "targetMobile" and not(spellTarget:IsMobile())) then
			return
		end
	end

	if not( HasSpell(spellName, this, mScrollObj) ) then
		if ( player ) then
			spellSource:SystemMessage("You do not have that spell.", "info")
		end
		return
	end

	if( IsDead(spellSource) ) then
		if ( player ) then
			spellSource:SystemMessage("OooOoOOooOOoOoOooooO", "info")
		end
		return
	end

	if( IsAsleep(spellSource) ) then
		if ( player ) then
			spellSource:SystemMessage("ZZZzzzz....", "info")
		end
		return
	end

	-- put us into combat
	if(IsAttackTypeSpell(spellName)) then
		BeginCombat()
	end

	mCurSpell = nil
	--mPrimedSpell = nil
	mQueuedTarget = nil
	mQueuedTargetLoc = nil

	mCurSpell = spellName
	if(spellTarget ~= nil) then mQueuedTarget = spellTarget end
	PrimeSpell(spellName, spellSource)
end

function RequestSpellTarget(spellName)
	Verbose("Magic", "RequestSpellTarget", spellName)
	if(mAutoTarg == nil) and not(noTarget) and this:IsPlayer() then
		local myTargType = GetSpellTargetType(spellName)
		local spellDisplayName = SpellData.AllSpells[spellName].SpellDisplayName or spellName
	--DefensivebugMessage("Target Type Set to " .. tostring(myTargType))
		if (myTargType == "RequestTarget") and (mQueuedTarget == nil) then
				this:RequestClientTargetGameObj(this, "QueueSpellTarget")
			return
		end
		if(myTargType == "RequestLocation") then
				this:RequestClientTargetLoc(this, "QueueSpellLoc")
			return
		end
	else
		return
	end
end

function ShouldAutoTarget(spellName)
	Verbose("Magic", "ShouldAutoTarget", spellName)
	--if not( this:IsPlayer() ) then return false end
	if ( SpellData.AllSpells[spellName] == nil or mAutoTarg == nil or not mAutoTarg:IsValid() ) then return false end

	return ( 
		( SpellData.AllSpells[spellName].BeneficialSpellType ~= true )
		or
		( SpellData.AllSpells[spellName].BeneficialSpellType == true and AllowFriendlyActions(this,mAutoTarg) )
	)
end

function HandleSuccessfulSpellPrime(spellName, spellSource, free)
	Verbose("Magic", "HandleSuccessfulSpellPrime", spellName, spellSource, free)

	if ( SpellData.AllSpells[spellName].PreventTownCast == true and GetGuardProtection(this) == "Town" ) then
		this:SystemMessage("Cannot cast that spell in town.", "info")
		CancelSpellCast()
		return
	end

	mFreeSpell = false
	if ( free == true ) then
		mFreeSpell = true
	end
		CancelCurrentSpellEffects()
	this:PlayAnimation("idle")	
	this:DelObjVar("LastSpell")
	if (spellName == nil) then spellName = mCurSpell end
	if (spellName == nil) then LuaDebugCallStack("NIL SPELL") end

	local spellDisplayName = SpellData.AllSpells[spellName].SpellDisplayName or spellName
	mCastingDisplayName = spellName
	mSpellSource = spellSource or this
	mCurSpell = nil

	mSpellSource:SendMessage("BreakInvisEffect", "Action", "SpellSuccess")

	--Trying it off
	--this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(500), "SpellGlobalCooldownTimer")

	local myTargType = GetSpellTargetType(spellName)
	--DebugMessage("Target Type Set to " .. tostring(myTargType))

	if (myTargType == "RequestTarget") then
		-- disallow 'utility' spells
		if ( SpellData.AllSpells[spellName].SpellType ~= "BuffTypeSpell" ) then
			-- Handle chambering spells
			-- can't check for HasMobileEffect for SpellChamber since it works through stacking.
			local spellChamberLevel = spellSource:GetObjVar("SpellChamberLevel")
			if ( spellChamberLevel ~= nil and (SpellData.AllSpells[spellName].Circle or 8) <= spellChamberLevel ) then
				if not( CheckMana(spellName, spellSource, mScrollObj) ) then 
					return
				end

				if not ( CheckReagents(spellName, spellSource, mScrollObj) ) then
					return
				end

				if not( CheckSpellCastSuccess(spellName, spellSource, mScrollObj) ) then
					return
				end

				spellSource:SendMessage("ChamberSpell", spellName, spellDisplayName)
				CancelSpellCast()
				return
			end
		end

		mPrimedSpell = spellName
		this:SetObjVar("PrimedSpell", spellName)
		
		if not(this:IsPlayer()) then 
			if(mAutoTarg == nil and mNoTarget == nil) then return end
			--DebugMessage("FiringSpell: " ..spellName .. " at " .. mAutoTarg:GetName())
			HandleSpellTargeted(mAutoTarg)
			mAutoTarg = nil
		else
			if( mNoTarget or ShouldAutoTarget(spellName) ) then
				HandleSpellTargeted(mAutoTarg) 
			else
				if(mQueuedTarget ~= nil) then 
					HandleSpellTargeted(mQueuedTarget)
					mQueuedTarget = nil
				else
					this:RequestClientTargetGameObj(this, "SelectSpellTarget")
				end
			end
		end
	elseif(myTargType == "RequestLocation") then
		if ( ShouldAutoTarget(spellName) ) then
			mPrimedSpell = spellName
			this:SetObjVar("PrimedSpell", spellName)
			if ( mAutoTargLoc == nil ) then
				mAutoTargLoc = mAutoTarg:GetLoc()
			end
			HandleSpellLocTargeted(true, mAutoTargLoc)	
			mAutoTargLoc = nil
			--DebugMessage("Handling a requested targetloc")
		else
			mPrimedSpell = spellName
			this:SetObjVar("PrimedSpell", spellName)
			if(mQueuedTargetLoc ~= nil) then
				HandleSpellLocTargeted(true, mQueuedTargetLoc)
				mQueuedTargetLoc = nil
			else
				local spellDisplayName = SpellData.AllSpells[spellName].SpellDisplayName
				this:SystemMessage("Select Location for " .. spellDisplayName)
				this:RequestClientTargetLoc(this, "SelectSpellLoc")
			end
		end
	else
		CancelCurrentSpellEffects()
		mPrimedSpell = nil
		this:DelObjVar("PrimedSpell")

		if not ( CheckMana(spellName, spellSource, mScrollObj) ) then
			return
		end

		if not ( CheckReagents(spellName, spellSource, mScrollObj) ) then
			return
		end

		if not( CheckSpellCastSuccess(spellName, spellSource, mScrollObj) ) then
			return
		end
	
		-- dismount them
		DismountMobile(this)

		local manaCost = GetManaCost(spellName, spellSource)

		SendAdjustManaRequest( spellSource, -manaCost, false, mScrollObj )
		ConsumeScroll(spellSource, mScrollObj)
		mScrollObj = nil
		--AdjustCurMana(spellSource,-manaCost)
		-- Added as part of enchanting system - bphelps
		TryExecuteCastEnchant(this)
		TryDamageAttunedWeapon( spellName ) 
		ApplySpellCompletionEffects(spellName, myTarg, mSpellSource)	
		mSpellSource = nil	
	end
	
	if( mPrimedSpell ~= nil ) then
		if( this:IsPlayer() ) then
			local spellRange = GetSpellInformation(spellName, "SpellRange") or 0

			-- DAB TODO: If it is area effect, send the area effect radius
			clientInfo = {
				spellName,
				myTargType,
				spellRange 
			}
			--DebugMessage(" Vect7: " .. tostring(clientInfo[1]) .. " " .. tostring(clientInfo[2]) .. " " .. tostring(clientInfo[3]))

			this:SendClientMessage("SpellPrimed",clientInfo)
		end

		local primedFX = GetSpellInformation(spellName, "SpellPrimedFXName") 	
		if( primedFX ~= nil ) then
			local mySpEffectArgs = GetSpellInformation(spellName, "SpellPrimedFXArgs")
		    --D*ebugMessage("CFX: " ..tostring(primedFX) .. " " .. this:GetName())
			if(mySpEffectArgs) then
				this:PlayEffectWithArgs(primedFX,60,mySpEffectArgs)
			else
				this:PlayEffect(primedFX,60)--,"Bone=Ground")
			end	
		end
		local primedFX2 = GetSpellInformation(spellName, "SpellPrimedFX2Name") 	
		if( primedFX2 ~= nil ) then
			local mySpEffectArgs2 = GetSpellInformation(spellName, "SpellPrimedFX2Args")
			--D*ebugMessage("CFX: " ..tostring(primedFX2) .. " " .. this:GetName())
			if(mySpEffectArgs2) then
				this:PlayEffectWithArgs(primedFX2,60,mySpEffectArgs2)
			else
				this:PlayEffect(primedFX2,60)--,"Bone=Ground")
			end	
		end
	end
end


function HandleSpellTargeted(spellTarget)
	Verbose("Magic", "HandleSpellTargeted", spellTarget)
	--DebugMessage("SpellTargeted")

	if mPrimedSpell == nil then return end
		--DebugMessage("[HandleScriptCommandTargetObject] ".. tostring(spellTarget))

	local spellName = mPrimedSpell
	--DebugMessage("Reloaded")
	if( not(mNoTarget) and spellTarget == nil ) then
		CancelCurrentSpellEffects()
		mPrimedSpell = nil
		this:DelObjVar("PrimedSpell")
	elseif( not(mNoTarget) and not(spellTarget:IsValid()) ) then
		CancelCurrentSpellEffects()
		mPrimedSpell = nil
		this:DelObjVar("PrimedSpell")
	elseif (not(mNoTarget) and not ValidateSpellCastTarget(spellName,spellTarget,this)) then		
		this:RequestClientTargetGameObj(this, "SelectSpellTarget")
		return
	elseif not(this:HasTimer("SpellPrimeTimer")) then
		CancelCurrentSpellEffects()
		if not (mSpellSource:IsPlayer()) then 
			mSpellSource:SendMessage("SpellFired", spellTarget) 
		end
		SetSpellTravelTime(mPrimedSpell, spellTarget, mSpellSource)
		mPrimedSpell = nil
		this:DelObjVar("PrimedSpell")
		if( IsPlayerCharacter(this) and Weapon.IsRanged( Weapon.GetType( _Weapon.RightHand.Object ) ) ) then
			StartMobileEffect(this, "FireArrow", this, { JustNotch = true })
		end
	else
		CancelCurrentSpellEffects()
		mPrimedSpell = nil
		this:DelObjVar("PrimedSpell")
		this:SystemMessage("You are already casting.", "info")
		return
	end

	if( mPrimedSpell == nil ) then		
		if( this:IsPlayer() ) then
			this:SendClientMessage("ClearPrimed")
		end
			
		local primedFX = GetSpellInformation(spellName, "SpellPrimedFXName") 	
		if( primedFX ~= nil ) then
			this:StopEffect(primedFX,1.0)
		end
		local primedFX2 = GetSpellInformation(mPrimedSpell, "SpellPrimedFX2Name") 	
		if( primedFX2 ~= nil ) then
			this:StopEffect(primedFX2,1.0)
		end

		if(spellTarget ~= this and spellTarget ~= nil) then this:SetFacing(this:GetLoc():YAngleTo(spellTarget:GetLoc())) end
	end
end
				

function HandleSpellLocTargeted(success, targetLoc)
	Verbose("Magic", "HandleSpellLocTargeted", success, targetLoc)
	--DebugMessage("Loc Targeted")


	if mPrimedSpell == nil then DebugMessage("mPrimedSpell is nil") return end

	local spellName = mPrimedSpell

		--DebugMessage("[HandleScriptCommandTargetObject] ".. tostring(spellTarget))
	if not(success) then
		CancelCurrentSpellEffects()
		mPrimedSpell = nil
		this:DelObjVar("PrimedSpell")
		--DebugMessageA(this,"target cleared")
	elseif not(IsLocInSpellRange(mPrimedSpell, targetLoc, mSpellSource)) then
		this:SystemMessage("Not in range.", "info")
		this:RequestClientTargetLoc(this, "SelectSpellLoc")
		--DebugMessageA(this,"not in range")
		return
	elseif not(LineOfSightCheck(mPrimedSpell, targetLoc)) then
		this:SendMessage("CannotSeeTarget", spellTarget) 
		--DebugMessageA(this,"Can't see target")
		this:RequestClientTargetLoc(this, "SelectSpellLoc")
	elseif not(this:HasTimer("SpellPrimeTimer")) then
		CancelCurrentSpellEffects()
		PerformSpellLocationActions(mPrimedSpell, this,  targetLoc, mSpellSource)
		--DebugMessageA(this,"No spell prime timer")
		mSpellSource = nil
		mPrimedSpell = nil
		this:DelObjVar("PrimedSpell")
		if( IsPlayerCharacter(this) and Weapon.IsRanged( Weapon.GetType( _Weapon.RightHand.Object ) ) ) then
			StartMobileEffect(this, "FireArrow", this, { JustNotch = true })
		end
	else
		CancelCurrentSpellEffects()
		mPrimedSpell = nil
		this:DelObjVar("PrimedSpell")
		mSpellSource = nil
		--DebugMessageA(this,"already casting")
		this:SystemMessage("Already casting.", "info")
		return
	end
	--DebugMessage("Reached end")
	if( mPrimedSpell == nil ) then		
		if( this:IsPlayer() ) then
			this:SendClientMessage("ClearPrimed")
		end
			
		local primedFX = GetSpellInformation(spellName, "SpellPrimedFXName") 	
		if( primedFX ~= nil ) then
			this:StopEffect(primedFX,1.0)
		end
		local primedFX2 = GetSpellInformation(mPrimedSpell, "SpellPrimedFX2Name") 	
		if( primedFX2 ~= nil ) then
			this:StopEffect(primedFX2,1.0)
		end
		if(targetLoc ~= this:GetLoc() and targetLoc ~= nil) then this:SetFacing(this:GetLoc():YAngleTo(targetLoc)) end
	end

end

function HandleSpellTravelled(spellName, spTarget, spellSource, spellID)
	Verbose("Magic", "HandleSpellTravelled", spellName, spTarget, spellSource, spellID)
	if(spellSource == nil) then spellSource = this end
	if(IsHitTypeSpell(spellName)) then
		local overrideTarg = GetSpellInformation(spellName, "DoNotReplaceTarget")
		if (overrideTarg == nil) then overrideTarg = false end					
			if( mScrollObj ) then
				this:SendMessage("RequestMagicalAttack", spellName,spTarget,spellSource,overrideTarg,true,GetCombatMod(CombatMod.ScrollDamageTimes, 1), true)
			else
				this:SendMessage("RequestMagicalAttack", spellName,spTarget,spellSource,overrideTarg,true)
			end
		
			--DebugMessage(" Vect2: " .. tostring(mySend[1]) .. " " .. tostring(mySend[2]) .. " " .. tostring(mySend[3]) .. " " .. tostring(mySend[4]))
	else
		ApplySpellEffects(spellName, spTarget, spellSource)
	end 

	mScrollObj = nil
	
end

function CancelCurrentSpellEffects()
	if (mPrimedSpell ~= nil) then
		local primedFX = GetSpellInformation(mPrimedSpell, "SpellPrimedFXName") 	
		if( primedFX ~= nil ) then
		--DebugMessage("RemovingEffect")
			this:StopEffect(primedFX,1.0)
		end
		local primedFX2 = GetSpellInformation(mPrimedSpell, "SpellPrimedFX2Name") 	
		if( primedFX2 ~= nil ) then
		--DebugMessage("RemovingEffect")
			this:StopEffect(primedFX2,1.0)
		end
	end
end

function CancelSpellCast()
	Verbose("Magic", "CancelSpellCast")

	if ( this:HasTimer("CastFreezeTimer") ) then
		this:FireTimer("CastFreezeTimer")
	end
	--DebugMessage("MagicDeathCleanup")
	CancelCurrentSpellEffects()
	
	if(this:GetTimerDelay("SpellPrimeTimer") ~= nil) then
		--DebugMessage("Removing Timer")
		this:RemoveTimer("SpellPrimeTimer")
	end
	if ( IsPlayerCharacter(this) ) then
		this:SendClientMessage("CancelSpellCast")
	 	if ( this:HasObjVar("LastSpell") ) then
			ProgressBar.Cancel("Casting " ..this:GetObjVar("LastSpell"),this)
		end
	end
	mCastingDisplayName = ""
	mCurSpell = nil
	mPrimedSpell = nil
	this:DelObjVar("PrimedSpell")

	this:PlayAnimation("idle")
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "QueueSpellTarget", 
	function(target)
		if(target == nil) then return end
		if(target:IsValid()) then
			mQueuedTarget = target
		end

		end)
RegisterEventHandler(EventType.ClientTargetLocResponse, "QueueSpellLoc", 
	function(success,targetLoc)
			if(success) then
				mQueuedTargetLoc = targetLoc
			else
				mQueuedTargetLoc = nil
			end
	end)

function HandleRequestCombatMods( modTable, requester, requestId )

	if( modTable == nil ) then return end
	local mMobileMod = {}
	
	-- Build the table with just the CombatMods we need.
	for i=1,#modTable do 
		mMobileMod[ modTable[i] ] = CombatMod[ modTable[i] ]
	end

	-- Return the mods
	requester = requester or this
	requester:SendMessage(requestId, mMobileMod)
end

RegisterEventHandler(EventType.Message, "RequestCombatMods", HandleRequestCombatMods)
RegisterEventHandler(EventType.Message, "ScrollCastSpell", HandleScrollCastRequest)
RegisterEventHandler(EventType.ClientUserCommand, "cancelspellcast", CancelSpellCast)
RegisterEventHandler(EventType.Message, "CastSpellMessage", HandleSpellCastRequest)
RegisterEventHandler(EventType.ClientUserCommand, "sp", HandleSpellCastCommand)
RegisterEventHandler(EventType.ClientUserCommand, "ba", HandleSongCastCommand)
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "SelectSpellTarget", HandleSpellTargeted)
RegisterEventHandler(EventType.ClientTargetLocResponse, "SelectSpellLoc", HandleSpellLocTargeted)
RegisterEventHandler(EventType.Timer, "SpellPrimeTimer", HandleSuccessfulSpellPrime)
RegisterEventHandler(EventType.Message, "HasDiedMessage", CancelSpellCast)
RegisterEventHandler(EventType.Message, "CancelSpellCast", CancelSpellCast)