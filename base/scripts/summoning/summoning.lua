Summoning = {}

-- a table containing the skill categories
Summoning.CategoryTypes = 
{
	"DemonologySkill",
	"NecromancySkill",
	"PyromancyMagicSkill",
	"ManifestationSkill",
	"NatureMagicSkill",
}

-- a table containing the skill category definitions
-- the Max table will contain your maximum for each summon type
-- the keys in each Max table will help generate the correct tables
Summoning.CategoryTypesDefinitions =
{
	NecromancySkill = 
	{
		Max = {Skeletons = 1,Liches = 1,SkeletonArcher = 1,}
	},
	DemonologySkill = 
	{
		Max = {Hounds = 1,Familiar = 1,Daemon = 1,}
	},
	ManifestationSkill =
	{
		Max = {Phantoms = 2,}
	},
	PyromancyMagicSkill =
	{
		Max = {Elemental = 1,}
	},
	NatureMagicSkill =
	{
		Max = {Ent = 1,}
	}
}

--- this will fire in the player.lua override in the loadfrombackup event
--- it sets up the table entries for summoning.
-- @param playerObject - the player to setup 
function InitializePlayerSummonTable(playerObject)
	local tempTable = {}
	for key,value in pairs(Summoning.CategoryTypes) do
		-- create the initial table
		tempTable[value] = {}
		-- add an entry into each table for the summon types
		for k,v in pairs(Summoning.CategoryTypesDefinitions[value].Max) do
			tempTable[value][k] = {}
		end
	end
	-- update the player summon table
	playerObject:SetObjVar("SummonTable",tempTable)
end

--- returns true if you can summon this type of mobile
-- @param playerObject - the player to get the summoning info from
-- @param skillName -
-- @param mobileCategory -
function CanAddSummon(playerObject,skillName,mobileCategory)
	local summonTable = playerObject:GetObjVar("SummonTable") or nil
	if(summonTable) then
		-- can we summon another of this type?
		if(#summonTable[skillName][mobileCategory] < GetMaxSummons(skillName,mobileCategory)) then
			return true
		else
			-- if we are over our limit, then remove the first instance in the table
			local mobile = summonTable[skillName][mobileCategory][1] or nil
			if(mobile) then
				table.remove(summonTable[skillName][mobileCategory],1)
				PlayEffectAtLoc("CloakEffect",mobile:GetLoc())
				mobile:Destroy()
				playerObject:SetObjVar("SummonTable",summonTable)
				return true
			end
		end
	end
	return false
end

--- adds the summon into the correct table for the use of the summoning system
-- @param playerObject - the player to get the summoning info from
-- @param skillName - category type
-- @param mobileCategory - mob table type
-- @param summonedMobile - the summoned object reference
function PlayerAddSummon(playerObject,skillName,mobileCategory,summonedMobile)
	local summonTable = playerObject:GetObjVar("SummonTable")
	if(summonTable) then
		table.insert(summonTable[skillName][mobileCategory],summonedMobile)
		playerObject:SetObjVar("SummonTable",summonTable)
	end
end


--- removes the summon from the correct table for the use of the summoning system
-- @param playerObject - the player to get the summoning info from
-- @param skillName - category type
-- @param mobileCategory - mob table type
-- @param summonedMobile - the summoned object reference
function PlayerRemoveSummon(playerObject,skillName,mobileCategory,summonedMobile)
	local summonTable = playerObject:GetObjVar("SummonTable")
	if(summonTable) then
		for key,value in pairs(summonTable[skillName][mobileCategory]) do
			if(value == summonedMobile) then
				table.remove(summonTable[skillName][mobileCategory],key)
				playerObject:SetObjVar("SummonTable",summonTable)
				return
			end
		end
	end
end

--- returns the max you can summon from a given category and summon type
-- @param skillName -
-- @param mobileCategory -
function GetMaxSummons(skillName,mobileCategory)
	return (Summoning.CategoryTypesDefinitions[skillName].Max[mobileCategory] or 0)
end

--- destroys all current summoned minions the player has
--- this function is called from the player.lua logout event
-- @param playerObject - the player object you wish to remove the summons for
function DestroyAllPlayerSummons(playerObject)
	local summonTable = playerObject:GetObjVar("SummonTable")
	if(summonTable) then
		for skillName,value1 in pairs(summonTable) do
			for mobType,value2 in pairs(summonTable[skillName]) do
				for index,value3 in pairs(summonTable[skillName][mobType]) do
					if(summonTable[skillName][mobType][index]) then
						summonTable[skillName][mobType][index]:Destroy()
					end
				end
			end
		end
	end
end