--- this returns a timespan for when to delete this summons
function GetSpawnPulse()
    local duration = TimeSpan.FromSeconds(30)
    if(initializer) then
        if(initializer.Duration) then
            duration = initializer.Duration
        end
    end
    return duration
end

--- this handles the timer event trigger
function HandleDeleteTimer()
    local summonMaster = this:GetObjVar("SummonMaster")
    local categoryKey = this:GetObjVar("CategoryKey")
    local mobileTableKey = this:GetObjVar("MobileTableKey")
    if(summonMaster and categoryKey and mobileTableKey) then
        PlayerRemoveSummon(summonMaster,categoryKey,mobileTableKey,this)
    end
    this:Destroy()
    PlayEffectAtLoc("CloakEffect",this:GetLoc())
end

--- fires when the delete timer expires
RegisterEventHandler(EventType.Timer, "deleteTimer", HandleDeleteTimer)

--- fire off the delete timer
this:ScheduleTimerDelay(GetSpawnPulse(), "deleteTimer")    