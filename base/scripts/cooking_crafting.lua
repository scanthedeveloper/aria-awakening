require 'container'

function CanOpen(user)
    if (IsImmortal(user)) then
        return true
    end 
    local topmostObj = this:TopmostContainer() or this

    if not (IsInBackpack(this,user)) then
    	user:SystemMessage("Cooking pot must be in your backpack to use.","info")
    	return false
    end

    --Make sure we can reach object
    if(topmostObj:GetLoc():Distance(user:GetLoc()) > OBJECT_INTERACTION_RANGE ) then    
        user:SystemMessage("You cannot reach that.","info")  
        return false
    end

    if not(user:HasLineOfSightToObj(topmostObj,ServerSettings.Combat.LOSEyeLevel)) then 
        user:SystemMessage("[FA0C0C]You cannot see that![-]","info")
        return false
    end
    
    --if so return true
    return true
end

OverrideEventHandler("container",EventType.Message, "UseObject", 
	function (user,usedType)
		if(usedType == "Open" or usedType == "Use") then  
			if( CanOpen(user) ) then
				ShowCookingWindow(user)
			else
				this:SendOpenContainer(user)
            end
		end
	end)

function ShowCookingWindow(user)
	local dynWindow = DynamicWindow("CookingWindow","",256,296,-240,-100,"TransparentDraggable","Center")
	
	dynWindow:AddImage(0,45,"TextFieldChatUnsliced",247,256,"Sliced")
	dynWindow:AddButton(16,265 ,"CookButton","Cook All",96,30,"","",false,"")
	dynWindow:AddButton(134,265 ,"CookOneButton","Cook One",96,30,"","",false,"")
	dynWindow:AddContainerScene(0,0,256,256,this)
	dynWindow:AddButton(224, 50,"CloseButton","",18,18,"","",true,"CloseSquare")

	user:OpenDynamicWindow(dynWindow)
end


RegisterSingleEventHandler(EventType.ModuleAttached, "cooking_crafting",
	function()
		SetTooltipEntry(this,"cooking","Used to cook a variety of different recipes.")
		--AddUseCase(this,"Cook")
	end)

RegisterEventHandler(EventType.DynamicWindowResponse,"CookingWindow",
    function (user,buttonId)
	    if( buttonId == 0 ) then return end
	    if( user == nil or not(user:IsValid())) then return end
		local args = {}
	    if (buttonId == "CloseButton") then
	    	user:CloseDynamicWindow("CookingWindow")
	    elseif(buttonId == "CookButton") then
			if not (HasMobileEffect(user, "CookingPot") ) then
				args.CookOne = false
	    		user:SendMessage("StartMobileEffect", "CookingPot", this, args)
			end
		elseif(buttonId == "CookOneButton") then
			if not (HasMobileEffect(user, "CookingPot") ) then
				args.CookOne = true
	    		user:SendMessage("StartMobileEffect", "CookingPot", this, args)
	    	end
	    end
	end)