require 'base_ai_npc'

AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true
AI.Settings.CanConverse = false
local militiaCurrencyName = "talents"

function Dialog.OpenGreetingDialog(user)
    if ( 1 == 1 ) then
        local text = "I am at peace. I had a good life."
        local response = {}
        
        response[1] = {}
        response[1].text = "What can I buy with talents?"
        response[1].handle = "PurchaseRewards"

        --response[2] = {}
        --response[2].text = "What can I buy with gold?"
        --response[2].handle = "PurchaseRewardsGold"

        --response[3] = {}
        --response[3].text = "I'd like to change my sinful ways."
        --response[3].handle = "ReprieveInfo"
        
        response[3] = {}
        response[3].text = "Goodbye."
        response[3].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
    else
        local text = "Hello there. Sorry, but I only sell wares to Township Militia members."
        local response = {}

        --response[1] = {}
        --response[1].text = "I'd like to change my sinful ways."
        --response[1].handle = "ReprieveInfo"
        
        response[2] = {}
        response[2].text = "Goodbye."
        response[2].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
    end
end

function Dialog.OpenPurchaseRewardsDialog(user)
    local text = "Which item would you like to purchase?"
    local response = {}
    local Rewards = MakeGetRewardsTable(user)

    SetupPurchaseDialogHandles(user, Rewards)

    for x, y in sortpairs(Rewards, function(Rewards, a, b) return Rewards[a]["Rank"] < Rewards[b]["Rank"] end) do
        table.insert(response, {text = "(Rank "..y.Rank..") "..y.Name..": "..y.Cost.." "..militiaCurrencyName, handle = x})
    end

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenPurchaseRewardsGoldDialog(user)
    local text = "Which item would you like to purchase?"
    local response = {}
    local Rewards = MakeGetRewardsTable(user)

    SetupPurchaseGoldDialogHandles(user, Rewards)

    for x, y in sortpairs(Rewards, function(Rewards, a, b) return Rewards[a]["Rank"] < Rewards[b]["Rank"] end) do
        table.insert(response, {text = "(Rank "..y.Rank..") "..y.Name..": " .. GetAmountStr( ValueToAmounts(y.CostGold), nil, true ), handle = x})
    end

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

--[[function Dialog.OpenReprieveInfoDialog(user)
    local text = "For a limited time, the gods are offering a chance to return to the path of servitude. You may only ask forgiveness once, and any negative behavior will be punished."
    
    local response = {}

    response[1] = {}
    response[1].text = "I promise to change my ways."
    response[1].handle = "TryReprieve"

    response[2] = {}
    response[2].text = "I'm not interested in changing."
    response[2].handle = "Greeting"

    NPCInteractionLongButton(text,this,user,"Responses",response)
end]]

function Dialog.OpenTryReprieveDialog(user)
    local text = "Good luck to you."
    local response = {}
    StartMobileEffect(user, "Reprieve")

    response[1] = {}
    response[1].text = "Thank you."
    response[1].handle = "Greeting"

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function MakeGetRewardsTable(user)
    local Rewards = {
        MilitiaRing = {
            Name = "Band of Attack",
            Cost = 25000,
            CostGold = 25000,
            Rank = 1,
            Template = "pr9_militia_ring",
        },
        MilitiaNecklace = {
            Name = "Mark of Defense",
            Cost = 25000,
            CostGold = 25000,
            Rank = 1,
            Template = "pr9_militia_necklace",
        },
        MilitiaPlateHelm = {
                Name = "Knight's Breastplate",
                Cost = 50000,
                CostGold = 50000,
                Rank = 1,
                Template = "pr9_militia_plate_chest"
        },
        MilitiaRobeHelm = {
                Name = "Wizard's Hat",
                Cost = 50000,
                CostGold = 50000,
                Rank = 1,
                Template = "pr9_militia_robe_helm"
        },
        --[[MilitiaRobeChest = {
                Name = "Arcane Tunic",
                Cost = 5000,
                Rank = 1,
                Template = "pr9_militia_robe_chest"
        },]]
        MilitiaLeatherChest = {
                Name = "Battle-Hardened Tunic",
                Cost = 50000,
                CostGold = 50000,
                Rank = 1,
                Template = "pr9_militia_leather_chest"
        },
    }
    return Rewards
end

function Purchase(user, itemKey, Rewards)
    if ( Rewards[itemKey] == nil or Rewards[itemKey].Cost == nil or Rewards[itemKey].Rank == nil or Rewards[itemKey].Template == nil ) then
        QuickDialogMessage(this,user,"That item is not available right now, unfortunately.")
        return
    end

    local backpack = user:GetEquippedObject("Backpack")
    if ( backpack == nil ) then return false end
    local bank = user:GetEquippedObject("Bank")
    if ( bank == nil ) then return false end
    if ( ConsumeResourceContainer(backpack, "AllegianceIchor", Rewards[itemKey].Cost) ) then
        Create.InBackpack(Rewards[itemKey].Template, user, nil, function(item)
            if ( item ) then
                --AfterPurchase(item)
                Dialog.OpenPurchaseRewardsDialog(user)
            else
                QuickDialogMessage(this,user,"I'm sorry something went wrong, please try that again.")
            end
        end)
        return
    elseif ( ConsumeResourceContainer(bank, "AllegianceIchor", Rewards[itemKey].Cost) ) then
        Create.InBackpack(Rewards[itemKey].Template, user, nil, function(item)
            if ( item ) then
                --AfterPurchase(item)
                Dialog.OpenPurchaseRewardsDialog(user)
            else
                QuickDialogMessage(this,user,"I'm sorry something went wrong, please try that again.")
            end
        end)
        return
    end
    
    QuickDialogMessage(this,user,"You do not have enough "..militiaCurrencyName.." to purchase that.")
end

function PurchaseGold(user, itemKey, Rewards)
    if ( Rewards[itemKey] == nil or Rewards[itemKey].Cost == nil or Rewards[itemKey].Rank == nil or Rewards[itemKey].Template == nil ) then
        QuickDialogMessage(this,user,"That item is not available right now, unfortunately.")
        return
    end

    local backpack = user:GetEquippedObject("Backpack")
    if ( backpack == nil ) then return false end
    local bank = user:GetEquippedObject("Bank")
    if ( bank == nil ) then return false end
    local consumedCoins = false


    if( CountCoins(user) >= Rewards[itemKey].CostGold ) then
        consumedCoins = ConsumeResourceContainer( backpack, "coins", Rewards[itemKey].CostGold )
    elseif( CountCoinsInBank(user) >= Rewards[itemKey].CostGold ) then
        consumedCoins = ConsumeResourceContainer( bank, "coins", Rewards[itemKey].CostGold )
    else
        QuickDialogMessage(this,user,"You do not have enough gold to purchase that.")
    end

    if( consumedCoins ) then
        Create.InBackpack(Rewards[itemKey].Template, user, nil, function(item)
            if ( item ) then
                --AfterPurchase(item)
                Dialog.OpenPurchaseRewardsGoldDialog(user)
            else
                QuickDialogMessage(this,user,"I'm sorry something went wrong, please try that again.")
            end
        end)
        return
    end
end

function SetupPurchaseDialogHandles(user, Rewards)
    -- setup the dialog handles for each item purchase
    for key,val in pairs(Rewards) do
        Dialog[string.format("Open%sConfirmDialog", key)] = function(user)
            Purchase(user, key, Rewards)
        end
        Dialog[string.format("Open%sDialog", key)] = function(user)
            local text = string.format("I can sell you %s for %s "..militiaCurrencyName..".", val.Name, val.Cost)
            local response = {
                {
                    text = "I accept your offer.",
                    handle = key.."Confirm",
                },
                {
                    text = "What else do you have to sell?",
                    handle = "PurchaseRewards"
                },
                {
                    text = "Goodbye.",
                    handle = ""
                },
            }
            NPCInteractionLongButton(text,this,user,"Responses",response)
        end
    end
end

function SetupPurchaseGoldDialogHandles(user, Rewards)
    -- setup the dialog handles for each item purchase
    for key,val in pairs(Rewards) do
        Dialog[string.format("Open%sConfirmDialog", key)] = function(user)
            PurchaseGold(user, key, Rewards)
        end
        Dialog[string.format("Open%sDialog", key)] = function(user)
            local text = string.format("I can sell you %s for %s.", val.Name, GetAmountStr( ValueToAmounts(val.CostGold), nil, true ))
            local response = {
                {
                    text = "I accept your offer.",
                    handle = key.."Confirm",
                },
                {
                    text = "What else do you have to sell?",
                    handle = "PurchaseRewardsGold"
                },
                {
                    text = "Goodbye.",
                    handle = ""
                },
            }
            NPCInteractionLongButton(text,this,user,"Responses",response)
        end
    end
end

local _OnMobileCreated = OnMobileCreated
function OnMobileCreated()
    _OnMobileCreated()
    Create.Equipped("robe_linen_tunic", this, function(item)
        item:SetColor("0xC100FFFF")
    end)
    Create.Equipped("robe_linen_leggings", this, function(item)
        item:SetColor("0xC100FFFF")
    end)
    Create.Equipped("robe_necromancer_helm", this, function(item)
        item:SetColor("0xC100FFFF")
    end)
    Create.Equipped("facial_hair_beard_long", this, function(item)
        item:SetColor("0xC100FFFF")
    end)

    this:SetSharedObjectProperty("Title", "Militia Merchant")
end

function AfterPurchase(item)
    if ( item:IsValid() ) then
        local itemName = item:GetName()
        if ( string.find(itemName, "Cloak") ) then
            local seasonName = GlobalVarReadKey("Militia.CurrentSeason", "Name")
            if ( seasonName ) then
                itemName = itemName:gsub("Cloak", "Cloak ("..seasonName..")")
                item:SetName(itemName)
            end
        end
    end
end