local spawningBoss = false

RegisterEventHandler(EventType.Message,"Activate",
function ( ... )
	if (
		this:HasObjVar("Timer") -- we have a spawn timer?
		and not this:HasTimer("ResetTimer") -- do we have the ResetTimer objvar, or the timer currently set?
	) then
		--DebugMessage("SpawnBoss 1 : " .. tostring(spawningBoss))
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(this:GetObjVar("Timer")),"SpawnBoss") 
	elseif( not this:HasTimer("ResetTimer") ) then
		--DebugMessage("SpawnBoss 2 : " .. tostring(spawningBoss))
		this:FireTimer("SpawnBoss")
	end
end)

RegisterEventHandler(EventType.Timer,"SpawnBoss",function()
		if ((not this:HasObjVar("Boss") or this:GetObjVar("Boss"):IsValid() == false) and spawningBoss == false) then
			spawningBoss = true
			--DebugMessage("SpawnBoss 3 : " .. tostring(spawningBoss))
			CreateObj(this:GetObjVar("BossTemplate"),this:GetLoc(),"boss_created")
			if (this:HasObjVar("Effect")) then
				PlayEffectAtLoc(this:GetObjVar("Effect"),this:GetLoc())
			end
		end
	end)

RegisterEventHandler(EventType.CreatedObject,"boss_created",
function (success,objRef)
	--DebugMessage("SpawnBoss 4 : " .. tostring(spawningBoss))
	if (success) then
		this:SetObjVar("Boss",objRef)
		objRef:SendMessage(this:GetObjVar("BossSpawnMessage"))
	end
	spawningBoss = false
end)