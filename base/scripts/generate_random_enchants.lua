-- When attached to an item it will fill the remaining enchant 
-- slots with item appropriate enchants as well as attaching a 
-- prefix and suffix to it.

RegisterEventHandler(EventType.ModuleAttached,"generate_random_enchants",
	function ()
		CallFunctionDelayed(TimeSpan.FromMilliseconds(200),function()
			EnchantingHelper.ApplyRandomEnchantsToItem( this, nil, nil, true )
			this:DelModule("generate_random_enchants")
		end)
	end)

EnchantingHelper.ApplyRandomEnchantsToItem( 
  this, -- item to be enchanted
  nil, -- crafter who made it?
  nil,  -- material it is made from, if crafted.
  true, -- is the item noncrafted?
  3, -- non-crafted item level [1-3]
  2 -- number of maximum enchants to apply
)