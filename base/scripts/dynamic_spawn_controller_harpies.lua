require 'dynamic_spawn_controller'

mDynamicSpawnKey = "DynamicHarpySpawner"

mDynamicHUDVars.Title = "Harpy Culling"
mDynamicHUDVars.Description = "Kill the Harpy Matriarch and her nestlings."

local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        msg = "Fledgling harpies begin to spawn."
    elseif( progress == 2 ) then 
        msg = "Adolescent harpies begin to spawn."
    elseif( progress == 3 ) then 
        msg = "Harpies begin to spawn."
    elseif( progress == 4 ) then 
        msg = "Wild harpies begin to spawn."
    elseif( progress == 5 ) then 
        msg = "Stone harpies begin to spawn."
    elseif( progress == 6 ) then 
        msg = "The Harpy Matriarch has spawned!"
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end