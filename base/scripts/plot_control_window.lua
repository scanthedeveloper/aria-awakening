
controller = nil
plotController = nil
isHouse = false
tab = nil
contextCoOwner = nil

function Init(controllerObj, plotControllerObj, defaultTab)        
    controller = controllerObj
    plotController = plotControllerObj

    if ( controller == nil or plotController == nil ) then
        CleanUp()
        return
    end

    -- when controller isn't plot controller, it's a house.
    isHouse = controller.Id ~= plotController.Id
    -- each time the owner visits the plot control window, lets update the subscription status
    if(not(isHouse) and Plot.IsOwner(this,plotControllerObj)) then
        Plot.UpdateSubscriptionStatus(plotController,this)
    end

    tab = defaultTab or ((isHouse and "Security") or "Taxes")
    UpdateWindow()
end


function CleanUp()
    this:CloseDynamicWindow("PlotControlWindow")
    this:DelModule("plot_control_window")
end

function UpdateWindow()
    if (
        not controller
        or
        (isHouse and not Plot.HasHouseControl(this, plotController, controller))
        or
        (not isHouse and not Plot.IsOwner(this, controller))
    ) then
        CleanUp()
        return
    end

    local dynamicWindow = DynamicWindow("PlotControlWindow", (isHouse and "House" or "Plot") .. " Control",350,354,0,0,"")

    local tabs = {}
    if ( isHouse ) then
        table.insert(tabs,{ Text = "Security" })
    else
        table.insert(tabs,{ Text = "Taxes" })
    end
    table.insert(tabs,{ Text = "Decorate" })
    table.insert(tabs,{ Text = "Manage" })    


    AddTabMenu(dynamicWindow,
    {
        ActiveTab = tab, 
        Buttons = tabs,
        ButtonWidth = 103,
        Width = 310,
    })    

    HandleTabs(dynamicWindow)

    this:OpenDynamicWindow(dynamicWindow, this)
end

function OpenDecorateMailbox()
    local dynamicWindow = DynamicWindow("MailboxDecoWindow", "Customize Mailbox",250,190,0,0,"")

    local mailboxType = plotController:GetObjVar("MailboxTemplate") or "mailbox_wooden"

    dynamicWindow:AddLabel(20,10,"Mailbox Style",0,0,20)

    dynamicWindow:AddButton(25,35,"mailbox_wooden","Wooden",110,30,"","",false,"Selection2",GetButtonState(mailboxType,"mailbox_wooden"))
    dynamicWindow:AddButton(25,60,"mailbox_stone","Stone",110,30,"","",false,"Selection2",GetButtonState(mailboxType,"mailbox_stone"))
    dynamicWindow:AddButton(25,85,"mailbox_tudor","Fancy",110,30,"","",false,"Selection2",GetButtonState(mailboxType,"mailbox_tudor"))

    dynamicWindow:AddButton(170,100,"","OK",0,0,"","",true)

    this:OpenDynamicWindow(dynamicWindow)
end

function HandleDecorateMailboxResponse(user,buttonId)
    if(buttonId == nil or buttonId == "") then
        return
    end

    local mailboxType = plotController:GetObjVar("MailboxTemplate") or "mailbox_wooden"
    if((buttonId == "mailbox_wooden" or buttonId == "mailbox_stone" or buttonId == "mailbox_tudor") and mailboxType ~= buttonId) then
        plotController:SetObjVar("MailboxTemplate",buttonId)
        plotController:SetAppearanceFromTemplate(buttonId)
        OpenDecorateMailbox()
    end
end
RegisterEventHandler(EventType.DynamicWindowResponse,"MailboxDecoWindow",function(...) HandleDecorateMailboxResponse(...) end)

function HandleTabs(dynamicWindow)
    if ( tab == "Manage" ) then
        ManageTab(dynamicWindow)
    elseif ( tab == "Decorate" ) then
        DecorateTab(dynamicWindow)
    else
        if ( isHouse ) then
            if ( tab == "Security" ) then
                CoOwnerTab(dynamicWindow)
                FriendTab(dynamicWindow)
            end
        else
            if ( tab == "Taxes" ) then
                TaxTab(dynamicWindow)
            end
        end
    end
end

function HandleDecorateWindowResponse(returnId)

    if ( returnId == "LockDownItem" ) then
        this:SendMessage("StartMobileEffect", "PlotLockdown", plotController)
    elseif ( returnId == "ReleaseItem" ) then
        this:SendMessage("StartMobileEffect", "PlotRelease", plotController)
    elseif ( returnId == "MoveItem" ) then
        this:SendMessage("StartMobileEffect", "PlotMoveObject", plotController)
    elseif ( returnId == "Mailbox" ) then
        OpenDecorateMailbox()
    else
        -- nothing matched
        return false            
    end

    -- something matched
    return true
end

function HandleSecurityTabResponse(returnId, user)
    local option, arg = string.match(returnId, "(.+)|(.+)")
    if ( option == "SelectCoOwner" ) then
        contextArg = arg
        user:OpenCustomContextMenu("CoOwnerOptions","Action",{"Remove"})
    elseif ( option == "SelectFriend" ) then
        contextArg = arg
        user:OpenCustomContextMenu("FriendOptions","Action",{"Remove"})
    else
        -- nothing matched
        return false
    end
    -- something matched
    return true
end

function DecorateTab(dynamicWindow)
    dynamicWindow:AddImage(8,32,"BasicWindow_Panel",314,264,"Sliced")

    local lockdownStr = string.format("%d / %d", controller:GetObjVar("LockCount") or 0, controller:GetObjVar("LockLimit") or 10)
    dynamicWindow:AddLabel(20,44,"Lockdowns:",0,0,18)
    dynamicWindow:AddLabel(180,44,lockdownStr,0,0,18)

    local buttonStartY = 94

    -- add the container count if we are controlling a house
    if ( isHouse ) then
        local containerStr = string.format("%d / %d", controller:GetObjVar("ContainerCount") or 0, controller:GetObjVar("ContainerLimit") or 1)
        dynamicWindow:AddLabel(20,64,"Containers:",0,0,18)
        dynamicWindow:AddLabel(180,64,containerStr,0,0,18)
    else
        -- Get the sale totals for plots
        local itemsaleStr = string.format("%d / %d", controller:GetObjVar("SaleCount") or 0, controller:GetObjVar("SaleLimit") or 100)
        dynamicWindow:AddLabel(20,64,"Items For Sale:",0,0,18)
        dynamicWindow:AddLabel(180,64,itemsaleStr,0,0,18)    
    end

    dynamicWindow:AddButton(20,buttonStartY,"MoveItem","Decorate",290,26,"Adjust the location and orientation of a locked down item.","",false,"List")
    dynamicWindow:AddButton(20,buttonStartY+26,"LockDownItem","Lock Down",290,26,"[$1828]","",false,"List")
    dynamicWindow:AddButton(20,buttonStartY+52,"ReleaseItem","Release/Pack",290,26,"Select an item to release from lock down.","",false,"List") 
    dynamicWindow:AddButton(20,buttonStartY+78,"Mailbox","Customize Mailbox",290,26,"Customize your mailbox appearance.","",false,"List") 
end 

function ManageTab(dynamicWindow)
    
    dynamicWindow:AddImage(8,32,"BasicWindow_Panel",314,264,"Sliced")
    
    -- shared buttons
    dynamicWindow:AddButton(20,40,"Rename","Rename",290,26,"Max 20 characters","",false,"List")
    local kickStrangersTooltip = "Removes all players who are not:\n"..
    "Friends / in a Guild / in a Group with the Homeowner\n"..
    "or are not Co-Owners / Friends of a house on the Plot."
    dynamicWindow:AddButton(20,66,"KickStrangers","Kick Strangers From House",290,26,kickStrangersTooltip,"",false,"List")
    dynamicWindow:AddButton(20,92,"Kick","Kick",290,26,"Remove a target from your plot.","",false,"List")

    if ( isHouse ) then
        dynamicWindow:AddButton(20,118,"Destroy","Destroy",290,26,"Destroy this house.","",false,"List")
    else
        dynamicWindow:AddButton(20,118,"Resize","Resize Plot",290,26,"Expand or Shrink your plot, altering the required tax payment.","",false,"List")
        dynamicWindow:AddButton(20,144,"Transfer","Transfer Ownership",290,26,"Opens a secure trade transaction containing the deed to the plot. Cannot transfer with active merchants on land. Target must be standing near mailbox.","",false,"List")
        dynamicWindow:AddButton(20,170,"Destroy","Relinquish Control",290,26,"Relinquish control of the land. Plot cannot have any houses or locked down items. Will refund a land deed.","",false,"List")

        if(ServerSettings.Plot.AllowPackPlot or IsGod(this)) then
            dynamicWindow:AddButton(20,196,"Pack","Pack Plot",290,26,"Pack up this plot and any houses into a moving crate. You will be issued a land deed that is linked to this moving crate. When placing this land deed, the moving crate will appear.","",false,"List")
        end
    end
end

-- house only
function CoOwnerTab(dynamicWindow)
    dynamicWindow:AddImage(8,32,"BasicWindow_Panel",314,132,"Sliced")

    dynamicWindow:AddButton(11,135,"AddCoOwner","Add Co-Owner",308,26,"Co-Owners can Lock/Unlock the door, use secure containers, and lock down/release items. Only in this house.","",false,"List")
    
    local coOwners = controller:GetObjVar("HouseCoOwners") or {}
    local scrollWindow = ScrollWindow(18,38,280,92,23)
    local any = false

    for coowner,v in pairs(coOwners) do
        any = true
        local scrollElement = ScrollElement()
        scrollElement:AddButton(0,0,string.format("SelectCoOwner|%s", coowner.Id),"",260,23,"","",false,"ThinFrameHover","")
        scrollElement:AddLabel(
            6, --(number) x position in pixels on the window
            6, --(number) y position in pixels on the window
            coowner:GetCharacterName(), --(string) text in the label
            0, --(number) width of the text for wrapping purposes (defaults to width of text)
            0, --(number) height of the label (defaults to unlimited, text is not clipped)
            16 --(number) font size (default specific to client)
            --"center", --(string) alignment "left", "center", or "right" (default "left")
            --false, --(boolean) scrollable (default false)
            --false, --(boolean) outline (defaults to false)
            --"" --(string) name of font on client (optional)
        )
        scrollWindow:Add(scrollElement)
    end

    if not( any ) then
        local scrollElement = ScrollElement()
        scrollElement:AddLabel(
            6, --(number) x position in pixels on the window
            6, --(number) y position in pixels on the window
            "No Co-Owners Added.", --(string) text in the label
            0, --(number) width of the text for wrapping purposes (defaults to width of text)
            0, --(number) height of the label (defaults to unlimited, text is not clipped)
            16 --(number) font size (default specific to client)
            --"center", --(string) alignment "left", "center", or "right" (default "left")
            --false, --(boolean) scrollable (default false)
            --false, --(boolean) outline (defaults to false)
            --"" --(string) name of font on client (optional)
        )
        scrollWindow:Add(scrollElement)
    end

    dynamicWindow:AddScrollWindow(scrollWindow)
end
-- house only
function FriendTab(dynamicWindow)
    dynamicWindow:AddImage(8,162,"BasicWindow_Panel",314,132,"Sliced")

    dynamicWindow:AddButton(11,265,"AddFriend","Add Friend",308,26,"Friends can open locked doors and use assigned secure containers. Only in this house.","",false,"List")
    
    local friends = controller:GetObjVar("HouseFriends") or {}
    local scrollWindow = ScrollWindow(18,168,280,92,23)
    local any = false

    for friend,v in pairs(friends) do
        any = true
        local scrollElement = ScrollElement()
        scrollElement:AddButton(0,0,string.format("SelectFriend|%s", friend.Id),"",260,23,"","",false,"ThinFrameHover","")
        scrollElement:AddLabel(
            6, --(number) x position in pixels on the window
            6, --(number) y position in pixels on the window
            friend:GetCharacterName(), --(string) text in the label
            0, --(number) width of the text for wrapping purposes (defaults to width of text)
            0, --(number) height of the label (defaults to unlimited, text is not clipped)
            16 --(number) font size (default specific to client)
            --"center", --(string) alignment "left", "center", or "right" (default "left")
            --false, --(boolean) scrollable (default false)
            --false, --(boolean) outline (defaults to false)
            --"" --(string) name of font on client (optional)
        )
        scrollWindow:Add(scrollElement)
    end

    if not( any ) then
        local scrollElement = ScrollElement()
        scrollElement:AddLabel(
            6, --(number) x position in pixels on the window
            6, --(number) y position in pixels on the window
            "No Friends Added.", --(string) text in the label
            0, --(number) width of the text for wrapping purposes (defaults to width of text)
            0, --(number) height of the label (defaults to unlimited, text is not clipped)
            16 --(number) font size (default specific to client)
            --"center", --(string) alignment "left", "center", or "right" (default "left")
            --false, --(boolean) scrollable (default false)
            --false, --(boolean) outline (defaults to false)
            --"" --(string) name of font on client (optional)
        )
        scrollWindow:Add(scrollElement)
    end

    dynamicWindow:AddScrollWindow(scrollWindow)
end

-- plot only
function TaxTab(dynamicWindow)
    dynamicWindow:AddImage(8,32,"BasicWindow_Panel",314,264,"Sliced")

    local isOwner = Plot.IsOwner(this, plotController)
    local plotBounds = plotController:GetObjVar("PlotBounds")
    -- this will need to be updated to support L shaped plots
    local plotSize = Plot.GetSize(plotController,plotBounds)
    local balance = Plot.GetBalance(plotController)
    local taxesBalanceStr = "0"
    if ( balance < 0 ) then
        taxesBalanceStr = "[FF0000]-" .. GetAmountStrShort(ValueToAmounts(math.abs(balance))) .. "[-]"
    elseif ( balance > 0 ) then
        taxesBalanceStr = GetAmountStrShort(ValueToAmounts(balance))
    end

    local isPrimary = Plot.IsPrimaryPlot(plotController)
    local plotStatusStr = "Primary"
    if not(isPrimary) then
        plotStatusStr = "Secondary"
    end

    local plotSizeStr = plotSize.X .. "x" .. plotSize.Z
    local rate = Plot.CalculateRate(plotBounds)        

    dynamicWindow:AddLabel(20,44,"Plot Status:",0,0,18)
    dynamicWindow:AddLabel(122,44,plotStatusStr,0,0,18)
    if not(isPrimary) then
        dynamicWindow:AddButton(210,40,"MakePrimary","Make Primary",100,20,"[$3387]","",false,"List")
    end

    dynamicWindow:AddLabel(20,64,"Plot Size:",0,0,18)
    dynamicWindow:AddLabel(122,64,plotSizeStr,0,0,18)
    if ( balance < 0 ) then
        dynamicWindow:AddLabel(20,84,"Forecloses on:",0,0,18)
        dynamicWindow:AddLabel(122,84,Plot.GetNegativeTaxDueString(),0,0,18)
    else
        dynamicWindow:AddLabel(20,84,"Taxes Due:",0,0,18)
        dynamicWindow:AddLabel(122,84,Plot.GetTaxDueString(),0,0,18)
    end
    dynamicWindow:AddLabel(20,104,"Weekly Tax:",0,0,18)

    local taxesAmountStr = GetAmountStrShort(ValueToAmounts(rate))
    if(isOwner and not(Plot.ShouldPayTaxes(this))) then
        dynamicWindow:AddLabel(122,104,"FREE ("..taxesAmountStr..")",0,0,18)        
    else        
        dynamicWindow:AddLabel(122,104,taxesAmountStr,0,0,18)
    end

    dynamicWindow:AddLabel(20,124,"Tax Balance:",0,0,18)
    dynamicWindow:AddLabel(122,124,taxesBalanceStr,0,0,18)

    if ( balance < 0 ) then
        dynamicWindow:AddLabel(20,210,"WARNING: This property is currently in foreclosure. Please pay your taxes before the foreclosure date.",290,200,16)
    end

    if ( plotController:HasObjVar("PlotTaxReturn") and isOwner ) then
        local taxreturn = plotController:GetObjVar("PlotTaxReturn") or 0
        if ( taxreturn > 0 ) then
            dynamicWindow:AddButton(14,264,"Return","Claim Tax Return",303,26,"Claim tax return of "..ValueToAmountStr(taxreturn),"",false,"List")
        end
    else
        local max = Plot.CalculateTaxMax(rate)
        if ( balance < max ) then
            dynamicWindow:AddButton(14,264,"Pay","Add to Tax Balance",303,26,"Deposit money into the tax balance for this plot.","",false,"List")
        end
    end
end

-- Events

RegisterEventHandler(EventType.ContextMenuResponse,"CoOwnerOptions", function(user,optionStr)
    if ( contextArg ) then
        if ( optionStr == "Remove" ) then
            Plot.RemoveHouseCoOwner(user, GameObj(tonumber(contextArg)), controller)
            UpdateWindow()
        end
    end
end)

RegisterEventHandler(EventType.ContextMenuResponse,"FriendOptions", function(user,optionStr)
    if ( contextArg ) then
        if ( optionStr == "Remove" ) then
            Plot.RemoveHouseFriend(user, GameObj(tonumber(contextArg)), controller)
            UpdateWindow()
        end
    end
end)

RegisterEventHandler(EventType.DynamicWindowResponse, "PlotControlWindow", function (user,returnId)

    if (
        not controller
        or
        (isHouse and not Plot.HasHouseControl(this, plotController, controller))
        or
        (not isHouse and not Plot.IsOwner(this, controller))
    ) then
        CleanUp()
        return
    end
    
    if ( user ~= this or HandleDecorateWindowResponse(returnId) or HandleSecurityTabResponse(returnId, user) ) then return end

    -- handle tabs
    local newTab = HandleTabMenuResponse(returnId)
    if ( newTab ) then
        tab = newTab
        UpdateWindow()
        return
    end

    if ( returnId == "Kick" ) then
        this:SendMessage("StartMobileEffect", "PlotKick", plotController, isHouse and controller or nil)
        return -- don't cleanup
    end

    if ( returnId == "KickStrangers" ) then
        this:SendMessage("StartMobileEffect", "PlotKickStrangers", plotController, isHouse and controller or nil)
        return -- don't cleanup
    end

    if ( isHouse ) then
        -- handle individual button responses (all tabs) for houses
        if ( returnId == "Rename" ) then
            this:SendMessage("StartMobileEffect", "HouseRename", controller)
            return -- don't cleanup
        elseif ( returnId == "Destroy" ) then
            this:SendMessage("StartMobileEffect", "HouseDestroy", controller)
            return -- don't cleanup        
        elseif ( returnId == "AddCoOwner" ) then
            this:SendMessage("StartMobileEffect", "HouseAddCoOwner", controller)
            return -- don't cleanup
        elseif ( returnId == "AddFriend" ) then
            this:SendMessage("StartMobileEffect", "HouseAddFriend", controller)
            return -- don't cleanup
        end
    else
        -- handle individual button responses (all tabs) for plots
        if ( returnId == "Rename" ) then
            this:SendMessage("StartMobileEffect", "PlotRename", controller)
            return -- don't cleanup
        elseif ( returnId == "Destroy" ) then
            this:SendMessage("StartMobileEffect", "PlotDestroy", controller)
            return -- don't cleanup
        elseif ( returnId == "Pack" and (ServerSettings.Plot.AllowPackPlot or IsGod(this))) then
            ClientDialog.Show{
                TargetUser = this,
                TitleStr = "Confirm Pack Plot",
                DescStr = "Are you sure you wish to pack up your plot? You must transfer to Ethereal Moon before you can unpack this plot. This can not be undone!",
                Button1Str = "Confirm",
                Button2Str = "Cancel",
                ResponseFunc = function (user,buttonId)
                    DebugMessage("buttonId "..tostring(buttonId))
                        if(buttonId == 0) then
                            -- verify owner before allowing to complete
                            if(Plot.IsOwner(this, controller)) then
                                Plot.Pack(controller,user:GetAttachedUserId(),function ()
                                    user:SystemMessage("Your plot has been packed into storage and will be accessible on login.","info")
                                end)
                            end
                        end
                    end,
            }           
            return 
        elseif ( returnId == "Transfer" ) then
            this:SendMessage("StartMobileEffect", "PlotTransferOwnership", controller)
            return -- don't cleanup
        elseif ( returnId == "Resize" ) then
            if not( this:HasModule("plot_resize_window") ) then
                this:AddModule("plot_resize_window")
            end
            this:SendMessage("InitResize", plotController)
            return -- don't cleanup
        elseif ( returnId == "Pay" ) then
            if not( this:HasModule("plot_tax_window") ) then
                this:AddModule("plot_tax_window")
            end
            this:SendMessage("InitTaxWindow", plotController)
            return
        elseif ( returnId == "Return" ) then
            local taxreturn = plotController:GetObjVar("PlotTaxReturn") or 0
            if ( taxreturn > 0 ) then
                Create.Coins.InBackpack(this, taxreturn, function(coins)
                    if ( coins ) then
                        this:SystemMessage(ValueToAmountStr(taxreturn) .. " has been returned.", "info")
                        plotController:DelObjVar("PlotTaxReturn")
                    end
                    UpdateWindow()
                end)
            else
                plotController:DelObjVar("PlotTaxReturn")
            end
            return
        elseif ( returnId == "MakePrimary" ) then
            Plot.MakePrimary(plotController,this)
            return
        end
    end
    
    CleanUp()
end)

RegisterEventHandler(EventType.Message, "UpdatePlotControlWindow", UpdateWindow)
RegisterEventHandler(EventType.Message, "ClosePlotControlWindow", CleanUp)
RegisterEventHandler(EventType.Message, "InitControlWindow", Init)

