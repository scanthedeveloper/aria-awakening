require 'ai_cultist'

SnakeCooldown = true

table.insert(AI.CombatStateTable,{StateName = "SpawnLords",Type = "rangedattack",Range = 15})

function HandleCreated(success,objRef,snakeList)
	if( success ) then
		objRef:SendMessage("AttackEnemy",AI.MainTarget)
        table.insert(snakeList,objRef)
        this:SetObjVar("SnakeList",snakeList)
	end
end

--make little ones
AI.StateMachine.AllStates.SpawnLords = {
        GetPulseFrequencyMS = function() return math.random(400) end,

        OnEnterState = function()
            if( not(AI.IsValidTarget(AI.MainTarget)) ) then
                AI.StateMachine.ChangeState("Idle")
                return
            end
            --DebugMessage("Attempting Imp Spawn")
            FaceTarget()
            if (not SnakeCooldown) then
                AI.StateMachine.ChangeState("Chase")                
                return
            end

            this:StopMoving()
            this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(400), "cooldownLords")
            this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(200), "SpawnLords")
			this:PlayAnimation("cast")
            this:PlayEffect("CastAir",0.5)
            this:PlayObjectSound("event:/magic/air/magic_air_cast_air",false,0.7)
			
            SnakeCooldown = false
        end,

        AiPulse = function()
            DecideCombatState()
        end,
        
        OnExitState = function()
            this:SendMessage("CancelSpellCast")
        end,
    }

RegisterEventHandler(EventType.Timer,"cooldownLords", function()
	SnakeCooldown = true
	end)

RegisterEventHandler(EventType.Timer,"SpawnLords", function()

        local SnakeList = {}
        if (this:HasObjVar("SnakeList")) then            
            SnakeList = this:GetObjVar("SnakeList")
        end
        --DebugMessage("SnakeList is size "..#SnakeList)
		if (#SnakeList >= 1) then return end

		this:PlayAnimation("cast")
		this:PlayEffect("CastAir",0.5)
		this:PlayObjectSound("event:/magic/air/magic_air_cast_air",false,0.7)
        SpawnSnake(SnakeList)
        
        this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(200), "SpawnLords")
	end)
		--gameObj:SendMessage("Resurrect",1.0)

function SpawnSnake(snakeList)
		this:PlayAnimation("cast")
		local spawnLoc = this:GetLoc():Project(this:GetFacing(), math.random(5,5))
		local snake = CreateObj("demonborn_familiar", spawnLoc, "orksSpawned", snakeList)
end

AI.Settings.Debug = false
AI.Settings.AggroRange = 8.0
AI.Settings.ChaseRange = 8.0
AI.Settings.LeashDistance = 20
AI.Settings.CanConverse = true
AI.Settings.ScaleToAge = false
AI.Settings.CanWander = false
AI.Settings.CanUseCombatAbilities = true
AI.Settings.CanCast = true
AI.Settings.ChanceToNotAttackOnAlert = 2

RegisterEventHandler(EventType.CreatedObject, "orksSpawned", HandleCreated)

RegisterEventHandler(EventType.Message, "HasDiedMessage",
    function(killer)
        if (IsGuard(killer)) then return end
        -- prevent res killing for more phat lewts
        if ( this:HasObjVar("lootable") ) then return end
        
        local loottable = nil
        local titleReward = nil
        --This is the auto-group loot table
        if (this:GetCreationTemplateId() == "contempt_sirius_warlord") then
            loottable = {TemplateDefines.LootTable.ContemptLootBox}
        end

        local damagers = this:GetObjVar("Damagers") or {}
        local numIndexedTable = {}
        local counter = 1
        for player, damageAmount in pairs(damagers) do
            if (player:IsValid() and not IsDead(player)) then
                numIndexedTable[counter] = player
                counter = counter + 1
            end
        end
        --they took part in killing the demon, they deserve credit
        DistributeBossRewards(numIndexedTable, loottable, titleReward, false, GetGuardProtection(this))
    end)
    
RegisterEventHandler(EventType.CreatedObject, "AwakeningRewardCreated", function(success,objRef,amount)
    if ( success ) then
        SetItemTooltip(objRef)
    end
end)
