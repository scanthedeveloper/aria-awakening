
function Reset()
	this:SetCollisionBoundsFromTemplate(this:GetCreationTemplateId())
end

function Clear()
	this:ClearCollisionBounds()
end

RegisterEventHandler(EventType.Message,"Clear",Clear)

RegisterEventHandler(EventType.Message,"Reset",Reset)