DEFAULT_DECAYTIME = 180

function ScheduleDecay(decayTime)
	--If the creature is pet, set decay time to be 10 times longer
	if (this:HasModule("pet_controller")) then
		decayTime = DEFAULT_DECAYTIME * 10
	end

	local template = this:GetCreationTemplateId()
	if ( template and (template == "buried_treasure"
	or template == "buried_treasure_1"
	or template == "buried_treasure_2"
	or template == "buried_treasure_3"
	or template == "buried_treasure_4"
	--SCAN ADDED DECAY EXTENSION TO SUNKEN CHESTS
	or template == "sunken_treasure"
	or template == "sunken_treasure_1"
	or template == "sunken_treasure_2"
	or template == "sunken_treasure_3"
	or template == "sunken_treasure_4") ) then
		decayTime = DEFAULT_DECAYTIME * 10
	end

	if(decayTime == nil) then
		decayTime = this:GetObjVar("DecayTime")		
	end

	if( decayTime == nil ) then
		decayTime = DEFAULT_DECAYTIME
	end

	---SCAN ADDED DECAY TABLE
	if(template == "evil_easter_bunny_spawner") then
		decayTime = 10
	elseif (template == "easter_rabbit_evil_red") then
		decayTime = 30
	end

	this:ScheduleTimerDelay(TimeSpan.FromSeconds(decayTime), "decay")
end

RegisterSingleEventHandler(EventType.ModuleAttached, "decay", 
	function()
		ScheduleDecay()
	end)

RegisterEventHandler(EventType.Message, "UpdateDecay", 
	function(decayTime)
		ScheduleDecay(decayTime)
	end)

RegisterEventHandler(EventType.Timer, "decay", 
	function()
		this:Destroy()
	end)