-----------------------------------------------------------------
-- Set Variables
-----------------------------------------------------------------
local __MaximumMobsSpawnable = 10
local __TotalMobsSpawned = 0


function StartEvent()
    DoEventPulse()
end

function DoEventPulse()
    --DebugMessage( "Event Pulse Fired" )
    local monolithMobiles = FindObjects(SearchMulti({ SearchMobileInRange(30), SearchObjVar("MobileTeamType","Monolith")}))
    
    -- Count our mobs
    __TotalMobsSpawned = 0
    for index,mobile in pairs(monolithMobiles) do 
        __TotalMobsSpawned = __TotalMobsSpawned + 1
    end

    --DebugMessage( "__TotalMobsSpawned : ", tostring(__TotalMobsSpawned), tostring(__MaximumMobsSpawnable) )

    for i=1, 5 do
        if( __TotalMobsSpawned < __MaximumMobsSpawnable ) then
            --DebugMessage("Spawning a Risen Cultist!")
            __TotalMobsSpawned = __TotalMobsSpawned + 1
            CallFunctionDelayed(TimeSpan.FromMilliseconds(math.random( 1000,4000 )), function() 
                local spawnLocation = SorceryHelper.GetSummonSpawnLocation( this, 25 )
                PlayEffectAtLoc("DigDirtParticle",spawnLocation)
                Create.AtLoc( "monolith_cultist_risen", spawnLocation,nil)
            end)
        end
    end

    CallFunctionDelayed(TimeSpan.FromSeconds(5),function ( ... ) DoEventPulse() end) 
end



-----------------------------------------------------------------
-- Register Events
-----------------------------------------------------------------
-- On Backup
RegisterEventHandler(EventType.LoadedFromBackup,"",function()StartEvent()end)
-- On Load
RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), 
    function()
        StartEvent()
    end
)