function UpdateOwnerName()
	local ownerObj = this:GetObjVar("Owner")
	this:SetName(ownerObj and "[D2BE47]"..ownerObj:GetName().."'s Catalog[-]" or "Catalog") -- DEC741
	--DebugMessage("Name: "..this:GetName())
end

function SetAllTomeEntryRecords(chance)
	local tomes = this:GetObjVar("Tomes") or {}

	for i=1, #AllTomeCollections do

		local collection = AllTomeCollections[i]
		local collectionTomeIds = collection[2] or {}
		
		for j=1, #collectionTomeIds do
			local id = collectionTomeIds[j]
			local tomeData = AllTomes[id] or {}
			local staticEntryCount = #tomeData.Entries

			local tome = {}
			for k=1, staticEntryCount do
				tome[k] = (Success(chance or 0) and 1) or 0
			end
			tomes[id] = tome
		end
	end
	this:SetObjVar("Tomes", tomes)
end

function AddEntries(user, tomeId, entryIndices)
	local tomes = this:GetObjVar("Tomes") or {}
	if not AllTomes[tomeId] then DebugMessage("[tome_catalog] Error. TomeId '",tomeId,"' is not valid.") return end
	
	local tome = tomes[tomeId]
	if tome then
		for i=1, #entryIndices do
			if tome[entryIndices[i]] > 0 then
				--user:SystemMessage("This Catalog already has '"..AllTomes[tomeId].Name.."' Entry #"..tostring(entryIndices[i]).." transcribed.","info")
				user:SystemMessage("Catalog already has this '"..AllTomes[tomeId].Name.."' Entry recorded.","info")
				return
			end
		end
	else
		tome = {}
		for i=1, #AllTomes[tomeId].Entries do
			tome[i] = 0
		end
	end
	tome[entryIndices[1]] = 1
	local entryRangeString = entryIndices[1]
	for i=2, #entryIndices do
		tome[entryIndices[i]] = 1
		entryRangeString = entryRangeString..", "..tostring(entryIndices[i])
	end
	--local entryString = #entryIndices > 1 and "Entries "..entryRangeString.." have" or "Entry "..tostring(entryIndices[1]).." has"
	--user:SystemMessage("'"..AllTomes[tomeId].Name.."' "..entryString.."been transcribed to a Catalog.","info")
	tomes[tomeId] = tome
	this:SetObjVar("Tomes", tomes)
	UpdateOwnerName()
	Lore.GetCatalogProgress(this)
	Lore.RecordPlayerLoreRecording(user, this, tomeId, entryIndices)
end

RegisterEventHandler(EventType.LoadedFromBackup,"", function()
	Lore.GetCatalogProgress(this)
end)

RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()
	RemoveUseCase(this, "Use")
	AddUseCase(this,"Read",true)
	AddUseCase(this,"Change Owner")
	AddUseCase(this,"Disown")
	if not this:GetObjVar("Tomes") then
		SetAllTomeEntryRecords(0)
	end
	UpdateOwnerName()
	Lore.GetCatalogProgress(this)
end)

--RegisterEventHandler(EventType.Message, "AddEntry", AddEntry)
RegisterEventHandler(EventType.Message, "AddEntries", AddEntries)

RegisterEventHandler(EventType.Message, "Fill", function()
	SetAllTomeEntryRecords(0.5)
end)

RegisterEventHandler(EventType.Message, "ChangeUser", function(newOwner)
	if newOwner:IsPlayer() then
		this:SetObjVar("Owner", newOwner)
		UpdateOwnerName()
	end
end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "ChangeUser", function(target, user)
	if not target then return end
		
	if target:IsPlayer() then
		this:SetObjVar("Owner", target)
		UpdateOwnerName()
	else
		user:SystemMessage("Chosen target must be a Player.", "info")
	end
end)

RegisterEventHandler(EventType.Message, "UseObject", function(user,usedType)
	if usedType == "Read" then
		this:PlayObjectSound("Use", true)
		UpdateOwnerName()
		Lore.ReadCatalog(user, this)
	elseif usedType == "Change Owner" then
		local ownerObj = this:GetObjVar("Owner")
		if ownerObj then
			if ownerObj ~= user then 
				user:SystemMessage("You are not the current Owner of this catalog.", "info")
				return
			else
				user:SystemMessage("Choose a new Owner to sign this catalog.", "info")
			end
		elseif IsLockedDown(this) then
			local plotController = Plot.GetAtLoc(this:GetLoc())
			if not plotController or Plot.IsStranger(plotController, user) then
				user:SystemMessage("This catalog is someone else's property.", "info")
				return
			end
		else
			user:SystemMessage("Choose an Owner to sign this catalog.", "info")
		end

		user:RequestClientTargetGameObj(this, "ChangeUser")
	elseif usedType == "Disown" then
		local ownerObj = this:GetObjVar("Owner")
		if ownerObj then
			if ownerObj ~= user then 
				user:SystemMessage("You are not the current Owner of this catalog.", "info")
				return
			else
				ClientDialog.Show
				{
				    TargetUser = user,
				    TitleStr = "Disown Catalog?",
				    DescStr = "If disowned, other players can Transcribe your entries into their own Tomebooks and prevent you from achieving a full collection of Tomebooks!",
				    Button1Str = "Confirm",
				    Button2Str = "Cancel",
				    ResponseObj = this,
				    ResponseFunc = function (user,buttonId)
				    	if (buttonId == 0) then
							this:DelObjVar("Owner")
							UpdateOwnerName()
						end
					end,
				}
			end
		else
			user:SystemMessage("This catalog has no Owner.", "info")
		end
	end
end)