require 'globals.lua.extensions'
local playerPointsTable = this:GetObjVar("PlayerEventPoints") or {}
local score = {0,0,0}
local pointsLimit = ServerSettings.Militia.Events[1].PointsLimit
local minutesLimit = ServerSettings.Militia.Events[1].MinutesLimit
local pulseInterval = TimeSpan.FromSeconds(5)
local registerPulseInterval = TimeSpan.FromSeconds(30)
RegisterEventHandler(EventType.LoadedFromBackup,"",function()OnLoad()end)
RegisterEventHandler(EventType.Message,"StartEvent",function()ResetAndStartEvent()end)
RegisterEventHandler(EventType.Message,"StopEvent",function()StopEvent()end)
RegisterEventHandler(EventType.Timer,"RegisterControllerToGlobalTable",function()MilitiaEvent.RegisterControllerToGlobalTable(this, registerPulseInterval)end)
this:ScheduleTimerDelay(registerPulseInterval,"RegisterControllerToGlobalTable")

RegisterEventHandler(EventType.Message, "AddEventPoints",	
function (playerObj, addAmount, militiaId)
    --update player table
    local oldAmount = playerPointsTable[playerObj] or 0
    playerPointsTable[playerObj] = oldAmount + addAmount
    --update militia table
    local oldAmount = score[militiaId] or 0
    score[militiaId] = oldAmount + addAmount

    CheckWinCondition()
end)

RegisterEventHandler(EventType.Timer, "EventPulse", 
function()
    local active = this:GetObjVar("Active")
    if ( active ~= nil and active == 1 ) then
        this:ScheduleTimerDelay(pulseInterval, "EventPulse")
    else
        this:RemoveTimer("EventPulse")
        return
    end

    WriteEventPointsToObjVars()
    CheckWinCondition()
end)

function ResetAndStartEvent()
    DebugMessage("Militia scavenge event activated: "..(this:GetObjVar("Label") or "Unknown"))
    --reset points
    playerPointsTable = {}
    score = {0,0,0}
    WriteEventPointsToObjVars()
    this:ScheduleTimerDelay(pulseInterval, "EventPulse")
    --start spawning mobs
    this:SendMessage("Activate")
    --stamp time of start
    this:SetObjVar("EndTime", DateTime.UtcNow:Add(minutesLimit))
    MilitiaEvent.UpdateGlobalTable(this)
    local message = (this:GetObjVar("Label") or "Unknown").." is now an active Militia "..ServerSettings.Militia.Events[1].Name.." event location."
    Militia.EventMessagePlayers(message)
end

function StopEvent()
    this:RemoveTimer("EventPulse")
    WriteEventPointsToObjVars()
    this:SendMessage("Deactivate")
    this:SendMessage("DestroyAllSpawns")
    DecideWinner()
    OnEndOfEvent()
    MilitiaEvent.UpdateGlobalTable(this)
end

function WriteEventPointsToObjVars()
    this:SetObjVar("PlayerEventPoints", playerPointsTable)
    this:SetObjVar("Score", score)
end

function CheckWinCondition()
    --a militia has reached the point limit
    if ( score ~= nil ) then
        for team, points in pairs(score) do
            if ( points ~= nil and points >= pointsLimit ) then
                StopEvent()
                return
            end
        end
    end
    --time limit has been reached
    local endTime = this:GetObjVar("EndTime")
    if ( endTime ~= nil and DateTime.UtcNow > endTime ) then
        StopEvent()
        return
    end
end

function DecideWinner()
    local eventPoints = this:GetObjVar("Score")
    for k, v in sortpairs(eventPoints, function(t,a,b) return t[b] < t[a] end) do
        if ( v > 0 ) then
            this:SetObjVar("Winner", k)
            --[[
            local militiaData = Militia.GetDataById(k)
            if ( militiaData == nil or militiaData.Name == nil ) then return false end
            local message = militiaData.Name.." has won "..(this:GetObjVar("Label") or "Unknown").." and has control of Belhaven!"
            Militia.EventMessagePlayers(message)
            Militia.SetVar("Belhaven", function(record)
                record["Controller"] = k
                return true
            end,
            function(success)
                if ( success ) then
                end
            end)
            ]]
        else
            this:SetObjVar("Winner", 0)
        end
        return
    end
end

function OnEndOfEvent()
    local t = playerPointsTable
    local length = CountTable(t)
    local count = 1
    for x, y in sortpairs(t, function(t, a, b) return a[1] < b[1] end) do
        local percentile = math.round(count/length, 2)
        count = count + 1
        if ( percentile > 0.5 ) then MilitiaEvent.FavorReward(x) end
    end
    return t
end

function OnLoad()
    this:ScheduleTimerDelay(pulseInterval, "EventPulse")
end