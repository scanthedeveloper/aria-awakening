RegisterSingleEventHandler(EventType.ModuleAttached, "owned_item", 
function()
	--this:SetSharedObjectProperty("Weight",-1)
	this:SetSharedObjectProperty("DenyPickup", true)
	this:SetObjVar("handlesPickup",true)
end)

RegisterEventHandler(EventType.Timer, "check_valid", 
function ()
	if( this:HasObjVar("itemOwner") ) then
		local owner = this:GetObjVar("itemOwner")
		if( owner and not(owner:IsValid())) then
			--DebugMessage("Destroying at A")
			this:Destroy()
		else
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(4 + math.random()),"check_valid")
		end
	end
end)

this:ScheduleTimerDelay(TimeSpan.FromSeconds(4 + math.random()),"check_valid")
