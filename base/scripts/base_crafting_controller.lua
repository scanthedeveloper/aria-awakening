require 'incl_resource_source'
require 'incl_magic_sys'
	
USAGE_DISTANCE = 5
RESOURCE_DIVISOR = 3

--this:ScheduleTimerDelay(TimeSpan.FromSeconds(1.5),"RemoveCraftingModule")
mCurUserTable = {}
mImprovementBias = 100
mCurImprovements = 1
mConsumeScheduled = false
mResourcesConsumed = false
mCraftingStage = 0
mCraftedItem = nil
mImprovingState = false
mItemToCraft = nil
mCrafting = false
mCraftingTemp = nil
mSkillLevel = 0
mBonusConsumed = false
mImprovementGuarantees = 0
mSkNum = 0
BASE_IMPROVEMENT_CHANCE = 45
BASE_IMPROVEMENT_REDUCTION_FACTOR = 3.5
mTool = nil
mAwaitingDialog = false
mCurPage = 0
mSkill = nil
skillName = nil
mResourceTable = nil
mDifficulty = nil
QualitySelectedTable = {}
mAutoCraft = false
mAutoCraftQuantity = nil
mAutoCraftCompleted = 1
mCurrentMaterial = nil
mCraftingDuration = BASE_CRAFTING_DURATION

lastCraftRequest = nil

function GetResourceTemplateId(resource)
	if(resource == nil) then
		LuaDebugCallStack("ERROR: GetResourceTemplateId got nil")
		return
	elseif not(ResourceData.ResourceInfo[resource]) then
		LuaDebugCallStack("ERROR: GetResourceTemplateId no resource info for "..tostring(resource))
		return
	end

	return ResourceData.ResourceInfo[resource].Template
end

function GetPrimaryResource(recipeTable,quality)
	local highestValue = 0
	local highestResource = nil
	-- get the highest value resource used to make the item
	local resourceTable = GetQualityResourceTable(recipeTable.Resources,quality)
	if (resourceTable == nil) then return {} end
	for resourceType, count in pairs(resourceTable) do
		if(ResourceData.ResourceInfo[resourceType]~= nil) then
			local value = CustomItemValues[ResourceData.ResourceInfo[resourceType].Template]
			if(value ~=nil and value*count > highestValue) then
				highestValue = value*count 
				highestResource = resourceType
			end
		end
	end

	return highestResource
end

function HandleConsumeResourceResponse(success,transactionId)	

	--this:PlayAnimation("anvil_strike")
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(1.5),"AnimationTimer")
	this:SendMessage("EndCombatMessage")
	if (mTool ~= nil) then
		FaceObject(this,mTool)
	else
		CleanUp()
		return
	end
	-- DAB TODO: refund resources on failure
	if not(success) then
			CleanUp()
		return
	end
	if(transactionId ~= "crafting_controller") then
		--DebugMessage("Invalid Consume")
		CleanUp()
		return
	end
	if(mTool ~= nil) then
		local toolAnim = mTool:GetObjVar("ToolAnimation")
		if(toolAnim ~= nil) then
			this:PlayAnimation(toolAnim)
		end

		local toolSound = mTool:GetObjVar("ToolSound")
		if(toolSound ~= nil) then
			if toolAnim ~= "blacksmith" then
				this:PlayObjectSound(toolSound,false,mCraftingDuration)
			end 
		end
	end
	if (mCraftedItem == nil) then
		--DebugMessage("Crafting")
		
		SetMobileModExpire(this, "Disable", "Crafting", true, TimeSpan.FromSeconds(mCraftingDuration))
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(mCraftingDuration), "CraftingTimer")
		return
	end
	if not(mCraftedItem == nil) then
		--DebugMessage("Improving")
		mBonusConsumed = true

		SetMobileModExpire(this, "Disable", "Improving", true, TimeSpan.FromSeconds(mCraftingDuration))
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(mCraftingDuration), "CraftingTimer")
		return
	end
	CleanUp()	
end

function HandleCraftingTimer()
		if(mCraftedItem == nil) then mCraftingStage = 0 end
		mCraftingStage = mCraftingStage + 1
		if (not isImproving) then
			--DebugMessage("Completed: ("..mAutoCraftCompleted.."/"..mAutoCraftQuantity..")")
			if (mAutoCraft and mAutoCraftCompleted < mAutoCraftQuantity ) then
				mAutoCraftCompleted = mAutoCraftCompleted + 1
				this:ScheduleTimerDelay(TimeSpan.FromSeconds(mCraftingDuration),"AutoCraftItem",mCurrentMaterial)
			else
				this:PlayAnimation("idle")
			end
		end
		if(mCraftingStage == 1) then
			CreateCraftedItem(mItemToCraft)
			--this:ScheduleTimerDelay(TimeSpan.FromSeconds(CRAFTING_BUFFER_DURATION), "CraftingTimer")
			return
		end
		--[[
		if not(mImprovingState) then
			CleanUp()
			return
		end
		]]--
end

function CreateCraftedItem(item)

	local itemTemplate = GetCraftTemplate(item)

	--DebugMessage( "Crafting : ", itemTemplate, mCurrentMaterial )
	
	if(itemTemplate == nil)then
		--DebugMessage("CRAFT:MS: No template found for item :" .. tostring(item))
		CleanUp()
		return
	end

	if (mResourceTable) == nil then
		mResourceTable = {}
	end

	if ( mSuccess ) then
		local recp = GetRecipeTableFromSkill(mSkill)
		local recipeTable = recp[mItemToCraft]

		local backpackObj = this:GetEquippedObject("Backpack")
	 	local randomLoc = GetRandomDropPosition(this)
	 	mCraftingTemp = itemTemplate 

		local creationTemplateData=GetTemplateData(itemTemplate)
		local resourceType=nil

		-- Get resource type for items that are resources
		if (creationTemplateData ~= nil) then
			if creationTemplateData.ObjectVariables~=nil then
				resourceType=creationTemplateData.ObjectVariables.ResourceType
			end
		end

		-- Fire Quest Event
		Quests.SendQuestEventMessage( this, "Crafting", { recipeTable.Category, mCraftingTemp, mCurrentMaterial }, recipeTable.StackCount or 1 )

		-- Check if the item is a stackable resource
		if (resourceType ~= nil) and (creationTemplateData.LuaModules ~= nil) and (creationTemplateData.LuaModules.stackable ~= nil) then
			-- try to add to the stack in the players pack		
			local stackCount = recipeTable.StackCount or 1
			local stackSuccess, stackObj = TryAddToStack(resourceType,backpackObj,stackCount)
		    if not( stackSuccess ) then
			   	-- no stack in players pack so create an object in the pack
			   	CreateObjInBackpackOrAtLocation(this,itemTemplate,"crafted_item",false,stackCount)
		    	--DebugMessage(4)
		    else
		    	mResourceTable = nil
		    	HandleItemCreation(true,stackObj,true)
			end
			
		    return -- Leave - we're done here
		end	

		weight = GetTemplateObjectProperty(itemTemplate,"Weight")
			 
		if(weight == -1) then
		    CreatePackedObjectInBackpack(this,itemTemplate,false,"crafted_item")    
		    --DebugMessage(2)        
		else
			CreateObjInBackpackOrAtLocation(this, itemTemplate,"crafted_item") 
			--DebugMessage(3)             
		end
	else
		this:SystemMessage("You fail and lose some materials.","info")
		ShowCraftingMenu()
	end

   	--CreateObjInContainer(itemTemplate, backpackObj, randomLoc, "crafted_item")
end

function GetCraftTemplate(item)
	--DebugMessage("GetCraftTemplate",mSkill,item)
	if (item == nil) then
		CleanUp()
		return
	end
	
	local recipeTable = GetRecipeTableFromSkill(mSkill)
	if(recipeTable == nil) then
		CleanUp()
		return
	end

	local tTable = recipeTable[item]

	if (tTable == nil) then
		CleanUp()
		return
	end
	--D*ebugTable(tTable)

	local defTemp = tTable.CraftingTemplateFile
	--DebugMessage(" : CraftingTemplateFile")
	if(defTemp == nil) then
		--DebugMessage("CRAFT:MS: No template found for item :" .. tostring(item))
		CleanUp()
		return
	end
	return defTemp
end
----------------------------------------------------------------------------


function AllItems(user)

	local craftingSkill = GetSkillLevel(user,mSkill)
	if(craftingSkill == nil) then 
		user:SystemMessage("[F7CC0A]You have not yet learned the basics of "..skillName..".[-]","info")
		CleanUp()
		return 
	end
	--DebugMessage("Skill:" .. tostring(craftingSkill))
	local canCraftTable = {}
	local craftTableIndex = 0
	local craftAdded = false
	local skillTable = GetRecipeTableFromSkill(mSkill)
	--DebugMessage("CanCraft Creating")
	
	for i, v in pairs(skillTable) do
		--DebugMessage(">> " .. tostring(i))
		local curReqSkill = 0
	--	for k,inf in pairs (v) do
	--		if(k ~= "Resources") then
	--			DebugTable(inf)
	--			DebugMessage("Skill Pres/Req: " ..tostring(craftingSkill) .. "/" .. tostring(inf.MinLevelToCraft))
				--if(craftingSkill >= v.MinLevelToCraft) then
					craftTableIndex = craftTableIndex + 1
					canCraftTable[i] = v
				--end
		--	end
	--	end
	end

	return canCraftTable
end

function UncraftableItems(user)
	--DebugMessage("CraftableItems:")

	local craftingSkill = GetSkillLevel(user,mSkill)
	if(craftingSkill == nil) then 
		user:SystemMessage("[F7CC0A]You have not yet learned the basics of "..skillName..".[-]","info")
		CleanUp()
		return 
	end
	--DebugMessage("Skill:" .. tostring(craftingSkill))
	local canCraftTable = {}
	local craftTableIndex = 0
	local craftAdded = false
	local skillTable = GetRecipeTableFromSkill(mSkill)
	--DebugMessage("CanCraft Creating")
	
	for i, v in pairs(skillTable) do
		--DebugMessage(">> " .. tostring(i))
		local curReqSkill = 0
	--	for k,inf in pairs (v) do
	--		if(k ~= "Resources") then
	--			DebugTable(inf)
	--			DebugMessage("Skill Pres/Req: " ..tostring(craftingSkill) .. "/" .. tostring(inf.MinLevelToCraft))
				if(not HasRecipe(this,i,mSkill)) then
					craftTableIndex = craftTableIndex + 1
					canCraftTable[i] = v
				end
		--	end
	--	end
	end

	return canCraftTable
end


function CraftableItems(user)
	--DebugMessage("CraftableItems:")

	local craftingSkill = GetSkillLevel(user,mSkill)
	if(craftingSkill == nil) then 
		user:SystemMessage("[F7CC0A]You have not yet learned the basics of "..skillName..".[-]","info")
		CleanUp()
		return 
	end
	--DebugMessage("Skill:" .. tostring(craftingSkill))
	local canCraftTable = {}
	local craftTableIndex = 0
	local craftAdded = false
	local skillTable = GetRecipeTableFromSkill(mSkill)
	--DebugMessage("CanCraft Creating")
	
	for i, v in pairs(skillTable) do
		--DebugMessage(">> " .. tostring(i))
		local curReqSkill = 0
	--	for k,inf in pairs (v) do
	--		if(k ~= "Resources") then
	--			DebugTable(inf)
	--			DebugMessage("Skill Pres/Req: " ..tostring(craftingSkill) .. "/" .. tostring(inf.MinLevelToCraft))
				if(craftingSkill >= v.MinLevelToCraft and HasRecipe(this,i,mSkill)) then
					craftTableIndex = craftTableIndex + 1
					canCraftTable[i] = v
				end
		--	end
	--	end
	end

	return canCraftTable
end


function TryCraftItem(user, userRequest,skill,noWindow, quality, tool, openWindow)

	--DebugMessage("TryCraftItem [BASE_CRAFTING_CONTROLLER.LUA]", tostring(user), tostring(recipe), tostring(skill), tostring(noWindow), tostring(quality), tostring(tool))

	quality = mQuality or quality
	tool = mTool or tool
	
	mItemToCraft = userRequest
	mImprovingState = false
	mCraftedItem = nil
	mImprovementGuarantees = 0
	mCurImprovements = 1

	local args = {
		Recipe = userRequest,
		Quality = quality,
		Skill = skill,
		Tool = tool or mTool,
		AutoCraftQuantity = mAutoCraftQuantity,
		AutoCraftCompleted = mAutoCraftCompleted,
		AutoCraft = mAutoCraft,
		OpenWindow = openWindow
	}

	this:SendMessage("StartMobileEffect", "CraftItem", nil, args)

end

---------------------------------------------------------------

function CannotCraftReason(item)
	local resourceTable = GetRecipeTableFromSkill(mSkill)[item].Resources	

	if (not HasRecipe(this,item,mSkill)) then
		return "Recipe not learned."
	end

	if not HasRequiredCraftingSkill(this,item,mSkill) and not this:GetObjVar("CanCraftOutOfThinAir") then
		return "Not enough skill."
	end

	if not(HasResources(resourceTable, this, mCurrentMaterial)) then
		return "Not enough resources."
	end

	--DebugMessage("[base_crafting_controller|CannotCraftReason] ERROR: called when player can craft item")

	return "I am the walrus."
end

function CanCraftItem(item)
	local resourceTable = GetRecipeTableFromSkill(mSkill)[item].Resources	

	if not(HasResources(resourceTable, this, mCurrentMaterial) )and not this:GetObjVar("CanCraftOutOfThinAir") then
		return false
	end

	if (not HasRecipe(this,item,mSkill)) then
		return false
	end

	if (not HasRequiredCraftingSkill(this,item,mSkill) ) then
		return false
	end
	return true
end

-- you can pass either the base or bonus stat name in here
function GetCraftingStat(template,statName)
	local strippedName = string.gsub(statName, "Base", "")
	strippedName = string.gsub(strippedName, "Bonus", "")
	
	--DFB TODO: Crafting bonus stats?
	weaponType = GetTemplateObjVar(template,"WeaponType") 
	armorType = GetTemplateObjVar(template,"ArmorClass")
	--DebugMessage("weaponType = " ..tostring(weaponType))
	--DebugMessage("armorType = "..tostring(armorType))

	if( weaponType ~= nil and EquipmentStats.BaseWeaponStats[weaponType] ~= nil ) then
		return EquipmentStats.BaseWeaponStats[weaponType][statName]
	end

	if (armorType ~= nil and EquipmentStats.BaseArmorStats[armorType] ~= nil) then
		return EquipmentStats.BaseArmorStats[armorType][statName]
	end
end

function GetClassBaseStat(class,statName)
	if (class ~= nil and statName ~= nil) then
		return EquipmentStats.BaseWeaponClass[class][statName]
	end
end

STAMINA_DEFAULT = 28
function GetCraftingDescription(template,recipe,object)
	local weaponType = GetTemplateObjVar(template,"WeaponType") 
	local armorType = GetTemplateObjVar(template,"ArmorClass")
	--DebugMessage("_-----------------------_")
	--DebugMessage("TEMPLATE IS "..tostring(template))
	--DebugMessage("item weaponType = " ..tostring(weaponType))
	--DebugMessage("item armorType = "..tostring(armorType))

	return recipe.Description
end

function GetRecipeItemName(recipeTable,material)
	if (recipeTable == nil) then
		LuaDebugCallStack("ERROR: recipeTable is nil") 
	end
	local materialName = ""
	if ( material and Materials[material] and ResourceData.ResourceInfo[material].CraftedItemPrefix ~= nil ) then
		materialName = ResourceData.ResourceInfo[material].CraftedItemPrefix .. " "
	end
	return materialName .. ( recipeTable.DisplayName or GetTemplateObjectName(recipeTable.CraftingTemplateFile) )
end

function GetRecipeSelectedQuality(recipeID)
	if (mQuality ~= nil) then return mQuality end
	return QualitySelectedTable[recipeID] or "Flimsy"
end

function GetImprovementGuarantees(qualityLevel)
	return ImprovementGuarantees[qualityLevel]
end

function ConsumeBonusResources(mItemToCraft)
--DebugMessage("Here " .. mItemToCraft)
	local recipeTable = GetRecipeTableFromSkill(mSkill)[mItemToCraft]
	local resourceTable = GetQualityResourceTable(recipeTable.Resources,mCurrentMaterial)
	local cost = 0
	local tempTable = {}
	local tableSize = 0
	for resourceType, count in pairs(resourceTable) do
		--DebugMessage("RT: " .. resourceType .. " CT: " .. count)
		cost = math.max(count / 5, 1)
		if(cost > 0) then
			tableSize = tableSize + 1
			tempTable[resourceType] = math.floor(cost)
		end
	end
	--D*ebugTable(tempTable)
	if(tableSize > 0) then
	--DebugMessage("Attempted Bonus Consume")

		ConsumeResources(tempTable, this, "crafting_controller", mCurrentMaterial)
	end

end

function CleanUp()
    this:PlayAnimation("idle")
	--if(this:HasTimer("CraftingTimer")) then this:RemoveTimer("CraftingTimer") end
	this:CloseDynamicWindow("CraftingWindow")
	--DebugMessage(GetCurrentModule())
	this:DelModule(GetCurrentModule())
end

function InterruptCrafting()
	if(this:HasTimer("CraftingTimer")) then
	    this:PlayAnimation("idle")
		--refund resources here
		SetMobileMod(this, "Disable", "Crafting", nil)
		this:RemoveTimer("CraftingTimer")
		--DebugMessage(DumpTable(mResourceTable))
		--DFB NOTE: Removed resource refunding
		--[[if (mResourceTable ~= nil) and (not mImprovingState) and ResourceData.ResourceInfo ~= nil then
			local backpackObj = this:GetEquippedObject("Backpack") 
			for i,j in pairs(mResourceTable) do
				if(ResourceData.ResourceInfo[i]) then
					local creationTemplate = ResourceData.ResourceInfo[i].Template
					--DebugMessage(j)
					--LuaDebugCallStack("Refunding here...")
					CreateObjInBackpack(this,creationTemplate,"RefundResources",j) 
				else
					DebugMessage("ERROR: Attempted to refund invalid resource "..tostring(i))
				end
		    end
		    mResourceTable = nil
		else
			--DebugMessage("[base_crafting_controller|InterruptCrafting] ERROR: Interrupted crafting without refunding resources!")
		end	
		]]--	
	end
	CallFunctionDelayed(TimeSpan.FromSeconds(0.1),function() CleanUp() end)
end

function ShowImprovementClientDialogString(moreImprovement,overrideString)
	local string = "Improve this item?"
	if (moreImprovement) then
		string = "Continue improving this item?"
	end


	ShowCraftingMenu(mCraftedItem,true,true,overrideString,string)
	--[[Old, we have a window for this now.
	ClientDialog.Show{
			TargetUser = this,
			DialogId = "ItemCrafted",
			TitleStr = "Continue Crafting?",
			DescStr = string,
			Button1Str = "Confirm",
			Button2Str = "Cancel",
			ResponseFunc = HandleDialogResult,
		}--]]
end

function HandleDialogResult(user, buttonId)
	buttonId = tonumber(buttonId)
	--DebugMessage("Dialog Received")
	--DebugMessage("DL: " ..dialogId)
	if( user ~= this ) then
		return
	end
	--DebugMessage(" Button :" .. buttonId .. " dialogId :" .. dialogId)
	if (buttonId == 0 and user == this) then

		local toolAnim = mTool:GetObjVar("ToolAnimation")
		if(toolAnim ~= nil) then
			this:PlayAnimation(toolAnim)
		end

		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1.5),"AnimationTimer")
		if (mTool ~= nil) then
			FaceObject(this,mTool)
		else
			CleanUp()
			return
		end		

		--mDialog = false
		mImprovingState = true
		this:RemoveTimer("RemoveCraftingModule")
		
		if not (this:HasTimer("CraftingTimer")) then
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(IMPROVEMENT_DURATION), "CraftingTimer")
			-- DAB NOTE: Do not consume resources for improvements!
		    --DebugMessage("Improving item")
			--if(mBonusConsumed == false) then
			--	ConsumeBonusResources(mItemToCraft)	
			--end
		end
			return
	end
	
		this:SystemMessage("[57FA0C]You have stopped trying to improve the item. [-]","info")
		mImprovingState = false
		--DebugMessage("Reset Improvment guarantees 2")
		mCraftedItem = nil
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(250),"ShowCraftingMenu")
		mImprovementGuarantees = 0
end

function HandleCraftingMenuResponse(user,id,fieldData)
	--DebugMessage("Getting here")
	local skill = mSkill
	--DebugMessage(id)
	if not (user == this) then 
		CleanUp()
		return
	end
	local result = StringSplit(id,"|")
	local action = result[1]
	local arg = result[2]

	local newTab = HandleTabMenuResponse(id)
	if(newTab) then
		mCurrentTab = newTab
		mCurrentCategory = nil
		mRecipe = nil
		ShowCraftingMenu()
		return
	end

	if (action == "ChangeSubcategory") then
		mCurrentCategory = arg
		mRecipe = nil
		ShowCraftingMenu()
		return
	end
	if (action == "ChangeRecipe") then
		mRecipe = arg
		ShowCraftingMenu()
		return
	end
	if (action == "OK") then
		ShowCraftingMenu()
		return
	end
	if (action == "Improve") then
		HandleDialogResult(user, 0)
		return
	end
	if (action == "Cancel") then
		HandleDialogResult(user, 1)
		return
	end
	--[[if id == "All" then
		mCurrentTab = id
		ShowCraftingMenu()
		return
	end
	if id == "Known" then
		mCurrentTab = id
		ShowCraftingMenu()
		return
	end
	if id == "Not Learned" then
		mCurrentTab = id
		ShowCraftingMenu()
		return
	end--]]
	if (id == "CraftAll") then
		mAutoCraftQuantity = tonumber(fieldData.quantity) or 1 
		mAutoCraftCompleted = 1
		mAutoCraft = true
		TryCraftItem(this, mRecipe, skill,false, mCurrentMaterial, mTool, true)
		return
	end
	if(id == "") then
		return
	end
	if (id == nil) then
		CleanUp()
		return
	end
	--TryCraftItem(this, id, skill,false)
end

function ShowCraftingMenu(createdObject,isImproving,canImprove,improveResultString,improveString)
	isImproving = false
	if (this:HasTimer("CraftingTimer")) then return end
	if (this:HasTimer("AutoCraftItem")) then return end
	if (mAutoCraft == true) then mAutoCraft = false end
	if not(RecipeCategories[mSkill]) then
		--DebugMessage("ERROR: Crafting menu for skill with no recipes! "..tostring(mSkill) )
		return
	end

	this:RemoveTimer("RemoveCraftingModule")
	--save it to an obj var so players can come back and not have to select the quality again.
	mCurrentTab = mCurrentTab or "Materials"
	skillName = mSkill
	local subCategoryTable = {}
	for i,j in pairs(RecipeCategories[skillName]) do
		if (j.Name == mCurrentTab) then
			subCategoryTable = j.Subcategories
			break
		end
	end
	--DebugMessage(mCurrentTab)
	--DebugMessage(mCurrentCategory)
	--DebugMessage(DumpTable(subCategoryTable))
	if (mCurrentCategory == nil) then
		for i,j in pairs(subCategoryTable) do
			mCurrentCategory = j[1]
			break
		end
	end
	--DebugMessage(mCurrentCategory)
	--DebugMessage("mCurrentCategory is "..tostring(mCurrentCategory))
	--DebugMessage("mCurrentTab is " .. tostring(mCurrentTab))	
	local mainWindow = DynamicWindow("CraftingWindow",StripColorFromString(mTool:GetName()),805,510,-410,-280,"","Center")
	local numCategories = CountTable(RecipeCategories[skillName])

	local buttons = {}
	for i,j in pairs(RecipeCategories[skillName]) do
		table.insert(buttons,{Text = j.Name})
	end

	AddTabMenu(mainWindow,
	{
        ActiveTab = mCurrentTab, 
        Buttons = buttons
    })	
		
	--Add the images for each sub window
	mainWindow:AddImage(0,37-9,"BasicWindow_Panel",94,437,"Sliced")
	mainWindow:AddImage(95,37-9,"BasicWindow_Panel",210,437,"Sliced")
	mainWindow:AddImage(306,37-9,"BasicWindow_Panel",478,437,"Sliced")
	--Next create 2 scroll windows.

	--One for subcategories
	local subCategories = ScrollWindow(3,43-9,78,426,69) --423 not 430?
	--add all the sub categories for that category
	--DebugMessage(DumpTable(RecipeCategories[skillName][mCurrentTab]))
	local categories = {}
	for i,j in pairs(subCategoryTable) do 
		categories[i] = j
	end
	for i,j in pairs(categories) do
		newElement = ScrollElement()
		local selected = "default"
		if (mCurrentCategory == j[1]) then
			selected = "pressed"
		end
		local subCatDisplayName = j[2] or j[1]
		local subCatIconName = j[3] or j[1]
		newElement:AddButton(3,3,"ChangeSubcategory|"..tostring(j[1]),"",64,64,tostring(subCatDisplayName),"",false,"List",selected)
		newElement:AddImage(3,3,"CraftingCategory_"..tostring(subCatIconName),0,0)
		
		subCategories:Add(newElement)
	end
	--One for actual recipes
	local recipeList = ScrollWindow(94,36,196,426,26)
		--add all the recipes for that category

	--first sort the recipes
	local recipes = {}
	for i,j in pairs(AllRecipes[skillName]) do 
		if (j.Category == mCurrentTab and j.Subcategory == mCurrentCategory) then
			j.Name = i
			table.insert(recipes,j)
		end
	end
	table.sort(recipes,function (a,b)
		if (a.MinLevelToCraft < b.MinLevelToCraft) then
			return true
		else
			return false
		end
	end)

	if (mRecipe == nil and #recipes > 0) then
		mRecipe = recipes[1].Name		
	end
	--DebugMessage("mRecipe is " .. tostring(mRecipe))

	--add the recipes
	local hasRecipes = false
	for i,j in pairs(recipes) do
		newElement = ScrollElement()
		local selected = "default"
		if (mRecipe == j.Name) then selected = "pressed" end
		--DebugMessage(j.Category," is category")
		--Checking for guild faction recipes

		local showRecipe = true
		if (showRecipe) then
			if (HasRecipe(this,j.Name)) then
				newElement:AddButton(6,0,"ChangeRecipe|"..tostring(j.Name),"",178,26,"Recipe for a "..j.DisplayName,"",false,"List",selected)
				newElement:AddLabel(22,6,j.DisplayName,155,30,16,"",false,false,"")
			else
				newElement:AddButton(6,0,"ChangeRecipe|"..tostring(j.Name),"",178,26,"Recipe for a "..j.DisplayName.."\n[D70000]You have not learned this recipe[-]","",false,"List",selected)
				newElement:AddLabel(22,6,"[999999]"..j.DisplayName.."[-]",155,30,16,"",false,false,"")
			end
			hasRecipes = true
			--DebugMessage("it should be added")
			recipeList:Add(newElement)
		end
	end
	mainWindow:AddScrollWindow(subCategories)
	if (hasRecipes) then
		mainWindow:AddScrollWindow(recipeList)
	end
	newElement = nil
	--create the craft window
	if (mRecipe ~= nil and mRecipe ~= "None" and skillName ~= nil) then
		--(skillName,mRecipe)
		--DebugMessage("mRecipe is "..mRecipe)
		local recipeTable = AllRecipes[skillName][mRecipe]
		if(recipeTable == nil) then
			DebugMessage("ERROR: Attempted to display recipe that is missing recipe table entry. " .. tostring(mRecipe))
		else

			-- verify the material for the given recipe
			if ( recipeTable.CanImprove ) then
				local first = nil
				local found = false
				for i=1,#MaterialIndex[skillName] do
					if ( recipeTable.Resources[MaterialIndex[skillName][i]] ~= nil ) then
						--DebugMessage(recipeTable.Resources[MaterialIndex[skillName][i]])
						if ( first == nil ) then first = MaterialIndex[skillName][i] end
						if ( mCurrentMaterial == MaterialIndex[skillName][i] ) then
							found = true
						end
					end
				end
				if not( found ) then
					mCurrentMaterial = first
				end
			else
				mCurrentMaterial = nil
			end

			--DebugMessage("Current Material ", mCurrentMaterial)

			--DebugMessage("Getting here")
		    --Add the portrait
			mainWindow:AddImage(314,38,"DropHeaderBackground",100,100,"Sliced")
			--DebugMessage(Materials[mCurrentMaterial])
			local templateData = GetTemplateData(recipeTable.CraftingTemplateFile)
			mainWindow:AddImage(319,43,tostring(templateData.ClientId),90,90,"Object",nil,Materials[mCurrentMaterial] or templateData.Hue or 0)
			if(recipeTable.StackCount) then
				mainWindow:AddLabel(385,105,tostring(recipeTable.StackCount),350,26,26)
			end

			local Description = GetCraftingDescription(recipeTable.CraftingTemplateFile,recipeTable)

		    --add the craft button
		    local reason = ""
			local craftText = "Craft"
			local craftAllText = "Craft All"
			local enableCraft = "default"
			if (not CanCraftItem(mRecipe)) then
				craftText = "[555555]Craft[-]"
				craftAllText = "[555555]Craft All[-]"
				enableCraft = "disabled"
				reason = "[D70000]"..CannotCraftReason(mRecipe).."[-]"
			end
			if (not HasRequiredCraftingSkill(this,mRecipe,mSkill) ) then
				skillColor = "[D70000]"
			else
				skillColor = ""
			end
			--DebugMessage("i is "..tostring(i))		
			local skillReq, maxSkill = GetRecipeSkillRequired(mRecipe,mCurrentMaterial)
			local minSkillLabel = skillColor.."Minimum "..GetSkillDisplayName(skillName).." Skill: " ..tostring(math.max(0, skillReq)).."[-]"

			--mainWindow:AddButton(489,422-9,mRecipe,craftText,120,0,"Craft this item.","",true,"",enableCraft)
			mainWindow:AddButton(489,422-9,"CraftAll","Craft",120,0,"Craft this item until you run out of resources.","",true,"",enableCraft)
			mainWindow:AddLabel(489-150,420,"[A1ADCC]How many: [-]",460,70,18)
			mainWindow:AddTextField(489-75,418,60,20,"quantity", "1")

			--
			--add the cannot craft message if you can't craft it=
			local recipeTitle = GetRecipeItemName(recipeTable,mCurrentMaterial) or (recipeTable.DisplayName)
			if(recipeTable.StackCount) then
				recipeTitle = recipeTitle .. " x "..tostring(recipeTable.StackCount)
			end
			mainWindow:AddLabel(424,46-9,recipeTitle,350,26,26)
			--Add the label
			mainWindow:AddLabel(633,421-9,reason,150,0,16,"",false,false,"PermianSlabSerif_16_Dynamic")
			--Add the min skill
			mainWindow:AddLabel(420-16+20,68-12+10+6-9,minSkillLabel,200,20,16,"",false,false,"PermianSlabSerif_16_Dynamic")
			
			local reqSkillLev, maxSkillLev = GetRecipeSkillRequired(mRecipe,mCurrentMaterial)
			mSkillLevel = GetSkillLevel(this,mSkill)
			local chance = SkillValueMinMax(mSkillLevel, reqSkillLev, maxSkillLev)
			chance = math.floor(math.max(0, math.min(100,chance*100)))
			if( chance < 1 and chance > 0 ) then chance = 1 end
			mainWindow:AddLabel(314,158-9,"[A1ADCC]Success Chance: [-]"..tostring(chance).."%",460,70,18)

			if(Description) then
				if (type(Description) == "string") then
					mainWindow:AddLabel(314,158-9+20,"[A1ADCC]Description: [-]"..Description,460,70,18)
				--scrollElement:AddLabel(215,30,"Description: "..Description,330,60,15
				--Add the description)
				elseif type(Description) == "table" then
					--DebugMessage(DumpTable(Description))
					mainWindow:AddLabel(314,158-9+20,"[A1ADCC]Description: [-]"..Description[1],460,70,18)
					--Add the bonuses if applicable
					mainWindow:AddLabel(403,248-6-9+20,Description[2],200,70,16)
					mainWindow:AddLabel(564,248-6-9+20,Description[3],200,70,16)
					--mainWindow:AddLabel(420-16,82-12+10,"Select Quality Type:",150,20,16)
					--scrollElement:AddLabel(215,30,"Stats:",110,60,15)
					--scrollElement:AddLabel(265,30,Description[1],110,60,15)
					--scrollElement:AddLabel(340,30,Description[2],110,60,15)
					--scrollElement:AddLabel(445,30,Description[3],110,60,15)
					mainWindow:AddImage(309+6,228-9+20,"Divider",475-16,0,"Sliced") --
				else
					DebugMessage("[base_crafting_controller|DisplayItem] ERROR: Crafting description is not string or table. This should never happen.")
				end	
			end
			--add the section bars
			mainWindow:AddImage(419+7,145-9,"Divider",360-14,0,"Sliced")
			mainWindow:AddImage(309+6,323-9,"Divider",470-12,0,"Sliced") --
			--Add the quality levels

			if ( recipeTable.CanImprove ) then
				local index = 0
				local count = 0
				for i=1,#MaterialIndex[skillName] do
					if ( recipeTable.Resources[MaterialIndex[skillName][i]] ~= nil ) then
						if ( MaterialIndex[skillName][i] == mCurrentMaterial ) then
							index = count
						end
						count = count + 1
						local resourceTable = GetQualityResourceTable(recipeTable.Resources, MaterialIndex[skillName][i])
						if ( resourceTable ~= nil ) then
							--DebugMessage(resourceTable)
							userActionData = GetDisplayItemActionData(mRecipe,recipeTable,MaterialIndex[skillName][i])
							--DebugMessage(userActionData)
							mainWindow:AddUserAction(374+(50*count),88,userActionData,40)
						end
					end
				end
				mainWindow:AddImage(420+(50*index),85,"CraftingWindowWeaponTypeHighlight",47,47)
			else
				local userActionData = GetDisplayItemActionData(mRecipe,recipeTable)
				mainWindow:AddUserAction(424,88,userActionData,40)
			end

			-- Add the trivial tag if necesary
			if(GetSkillLevel(this,mSkill) >= maxSkill) then
				mainWindow:AddImage(670,40,"DropHeaderBackground",100,25,"Sliced")
				mainWindow:AddLabel(720,46,"[DAA520]Trivial[-]",50,0,18,"center",false,false)
				mainWindow:AddButton(670,40,"","",100,25,"[$1632]","",false,"Invisible")
			end

			--Add the resources required
			-----------------------------------------------------------------------------------------------------------
			local qualityResourceTable = GetQualityResourceTable(recipeTable.Resources,mCurrentMaterial)

			if ( qualityResourceTable == nil ) then
				LuaDebugCallStack("Nil qualityResource Table for recipe "..recipeTable.DisplayName.." current material: "..mCurrentMaterial)
				return
			end

			--calculate the total size of the resources required section
			local SIZE_PER_RESOURCE = 50
			local resourceSectionSize = CountTable(qualityResourceTable)
			--set the starting position to be the size/2
			local resourceStartLocation = 530 - 8 - ((resourceSectionSize*SIZE_PER_RESOURCE)/2)+10
			local position = resourceStartLocation
			local count = 0
			--for each resource required
			for i,j in pairs(qualityResourceTable) do
				local myResources = CountResourcesInContainer(this:GetEquippedObject("Backpack"), i)
				local resourcesRequired = j
				local resultString =  myResources.. " / " .. resourcesRequired
				--get the resources required
				--count the resources the player has
				local resourceTemplate = GetResourceTemplateId(i)
				if(resourceTemplate ~= nil) then
					local tooltipString = "You need " ..tostring(resourcesRequired - myResources).." more "..GetResourceDisplayName(i)
					if (myResources >= resourcesRequired) then
						tooltipString = "You'll need to use "..tostring(resourcesRequired).. " "..GetResourceDisplayName(i).." to craft this. You have "..tostring(myResources).."."
					end
					--add the image highlighted if you have the resource
					local resourceHue = nil
					if (HasResources(recipeTable.Resources, this, mCurrentMaterial)) then
						mainWindow:AddImage(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+20,345-9,"CraftingItemsFrame",38,38)
					else
						mainWindow:AddImage(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+20,345-9,"CraftingNoItemsFrame",38,38)
						resourceHue = "AAAAAA"
					end
					--invisible button that does a tooltip
					mainWindow:AddButton(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+20,345-9,"","",38,38,tooltipString,"",false,"Invisible")
					--add the icon
					mainWindow:AddImage(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+23,348-9,tostring(GetTemplateIconId(resourceTemplate)),32,32,"Object",resourceHue,Materials[mCurrentMaterial] or 0)
					--display it red if they don't have it
					if (myResources < resourcesRequired) then
						resultString = "[D70000]" .. resultString .. "[-]"
					end
					--add the label
					mainWindow:AddLabel(resourceStartLocation+(SIZE_PER_RESOURCE)*(count)+42,390-9,resultString,50,20,16,"center",false,false,"PermianSlabSerif_16_Dynamic")
					count = count + 1
				else
					DebugMessage("ERROR: Recipe has invalid ingredient resource. "..tostring(mRecipe))
				end
			end
		end
	----------------IMPROVEMENT WINDOW---------------------------
	elseif (createdObject ~= nil or isImproving) then
		local recipeTable = AllRecipes[skillName][mRecipe]
		--Add the label 
		--DebugMessage("mQuality is " ..tostring(mQuality))
		local countString = ""
		if (mCount) > 1 then
			countString = mCount .. " "
		end
		local resultLabel = improveResultString or ("You have crafted: "..countString..tostring(GetRecipeItemName(recipeTable,mQuality)))
			--if the label isn't specified

		--Add the bars
		--add the description (if it's not destroyed)
		local Description = GetCraftingDescription(recipeTable.CraftingTemplateFile,recipeTable,createdObject)
		
		mainWindow:AddImage(498,46+25-9,"ObjectPictureFrame",100,100,"Sliced")
		mainWindow:AddImage(520,61+25-9,tostring(GetTemplateIconId(recipeTable.CraftingTemplateFile)),64,64,"Object")
		mainWindow:AddLabel(550,181-9,resultLabel,490,40,24,"center")
		mainWindow:AddImage(309+6,214-9,"Divider",470-12,0,"Sliced") --
		if (canImprove) then
			mainWindow:AddImage(309+6,309-9,"Divider",470-12,0,"Sliced") --
		end

		if (canImprove and Description) then
			if (type(Description) == "string") then
				mainWindow:AddLabel(309,158-9,Description,460,70,16)
			--scrollElement:AddLabel(215,30,"Description: "..Description,330,60,15
			--Add the description)
			elseif type(Description) == "table" then
				--DebugMessage(DumpTable(Description))
				--Add the bonuses if applicable
				mainWindow:AddLabel(403,248-6-9,Description[2],200,70,16)
				mainWindow:AddLabel(564,248-6-9,Description[3],200,70,16)
			else
				DebugMessage("[base_crafting_controller|DisplayItem] ERROR: Crafting description is not string or table. This should never happen.")
			end	
		end
		if (createdObject ~= nil and recipeTable.CanImprove) then
		--if it's improving 
			local improveLabel = improveString
			mainWindow:AddLabel(545,335-9,improveLabel,500,30,22,"center")
			--add the chance to break or improve
			local improvementString = ""
			if (mImprovementGuarantees > 0) then
				improvementString = "\n[F7CC0A]"..tostring(mImprovementGuarantees).." Improvements Remaining[-]"		 
			else		
				improvementString = improvementString .."\n[009715]"..math.floor(GetImprovementChance()).."% Chance to Improve Item[-]"
				improvementString = improvementString .."\n[FA0C0C]"..math.floor(GetBreakChance()).."% Chance to Break Item[-]"
			end
			mainWindow:AddLabel(545,360-15-9,improvementString,200,50,16,"center")
			--add the improve button
			mainWindow:AddButton(416,425-9,"Improve","Improve",120,30,"[$1633]","",true)
			--add the cancel button
			mainWindow:AddButton(565,425-9,"Cancel","Cancel",120,30,"Don't improve the item.","",false)
		--otherwise 
		else
			mainWindow:AddButton(489,422-9,"OK","OK",120,30,"Return to the crafting menu.","",false)
		end
			--label--display the OK button
	else
		--This should never happen
		mainWindow:AddLabel(560,50-9,"Select a recipe for details.",150,20,16,"center")
	end
	mQuality = nil
	this:OpenDynamicWindow(mainWindow,this)

	--this:ScheduleTimerDelay(TimeSpan.FromSeconds(60),"RemoveCraftingModule")
end

function GetDisplayItemActionData(recipeName,recipeTable,material)
	local displayName = GetRecipeItemName(recipeTable,material)
	local tooltip = "[$1634]"

	local templateName = nil
	local template = nil

	if ( recipeTable.CanImprove and material ) then
		templateName = AllRecipes[mSkill][material].CraftingTemplateFile
	else
		templateName = recipeTable.CraftingTemplateFile
	end

	-- load the template data
	template = GetTemplateData(templateName)
	if not(template) then
		DebugMessage("ERROR: Template not found: "..tostring(templateName).. " Recipe: "..tostring(recipeName))
		return nil
	end

	--DebugMessage(tostring(icon))
	return {
		ID = ( material or "" ).."="..recipeName,
		ActionType = "Crafted",
		DisplayName = displayName,
		Tooltip = tooltip,
		IconObject = template.ClientId,
		IconHue = Materials[material] or template.Hue or 0,
		Enabled = true,
		--Requirements = {
		--	{mSkill,GetRecipeSkillRequired(this,recipeName)}
		--},
		--ServerCommand = "ActivateQualityVariation "..recipeName.. " "..quality
		ServerCommand = Materials[material] ~= nil and "ChangeMaterial "..material or nil
	}
end

RegisterEventHandler(EventType.Timer,"ShowCraftingMenu",function ( ... )
	ShowCraftingMenu(...)
end)

RegisterEventHandler(EventType.ClientUserCommand, "ChangeMaterial",
function (material)
	if ( this:HasTimer("CraftingTimer") or not Materials[material] ) then return end
	--DebugMessage(material)
	mCurrentMaterial = material
	ShowCraftingMenu()
end)

RegisterEventHandler(EventType.Message, "InitiateCrafting",
function (fabTool,skill,initialTab)
	if(fabTool == nil) then CleanUp() end
	mTool = fabTool
	mSkill = skill
	if not(mCurrentTab) then
		mCurrentTab = initialTab
		mCurrentCategory = nil
	end

	local fabCont = mTool:TopmostContainer() or mTool
	local fabLoc = fabCont:GetLoc()
	if(this:GetLoc():Distance(fabLoc) > USAGE_DISTANCE ) then
		--DebugMessage("Too Far to Fabricate")
		this:SystemMessage("[FA0C0C]You are too far to use that![-]")
		CleanUp()
		return
	end

	ShowCraftingMenu()
end)

--This fires when the client object command is droppe
RegisterEventHandler(EventType.ClientObjectCommand,"dropAction",
	function (user,sourceId,actionType,actionId,slot)
		if(sourceId == "CraftingWindow" and slot ~= nil) then
			
			local result = StringSplit(actionId,"=")
			--DebugMessage(actionId.." is actionId")
			recipe = result[2] or result[1]
			quality = result[1]

			--DebugMessage ("quality is "..tostring(quality) .. " recipe is "..tostring(recipe))
			local recipeTable = GetRecipeTableFromSkill(mSkill)[recipe]
			local itemName = GetRecipeItemName(recipeTable,quality)
			local itemName,color = StripColorFromString(itemName)
			local displayName = "Craft "..itemName
			if(color ~= nil) then
				displayName = color.."Craft "..itemName.."[-]"
			end

			--DebugMessage("recipeTable is "..tostring(recipeTable))
			local hotbarAction = {
				ID = actionId,
				ActionType = "CraftItem",
				DisplayName = displayName,
				Tooltip = "Craft this item.", --DFBTODO ADD RESOURCES ETC
				IconObject = GetTemplateIconId(recipeTable.CraftingTemplateFile),
				Enabled = true,
				Requirements = {
					{mSkill,GetRecipeSkillRequired(recipe,mCurrentMaterial)}
				},
				ServerCommand = "CraftItem "..recipe.." "..quality.." "..mSkill,
			}
			hotbarAction.Slot = tonumber(slot)
			RequestAddUserActionToSlot(user,hotbarAction)

			-- Is this being dropped on a macro slot?

		end
	end)


local canCraftItem = true
function HandleCraftItem(user,recipe,quality,skill,tool)
	
	mAutoCraft = false
	mAutoCraftCompleted = 1
	mAutoCraftQuantity = nil
	mSkill = skill
	mQuality = quality
	mTool = tool
	mRecipe = recipe
	mCurrentMaterial = quality
	TryCraftItem( this, recipe, skill, true, quality, tool )
end

RegisterEventHandler(EventType.CreatedObject,"RefundResources",
	function(success,objRef,amount)
		if ( amount ~= nil and amount > 1) then 
			RequestSetStack(objRef,amount)
		end
		--finish cleaning up
		this:PlayAnimation("idle")
		if(this:HasTimer("CraftingTimer")) then this:RemoveTimer("CraftingTimer") end	
		CleanUp()
	end)

RegisterEventHandler(EventType.Message,"CraftItem",HandleCraftItem)
RegisterEventHandler(EventType.Message,"ShowCraftingMenu",ShowCraftingMenu)
RegisterEventHandler(EventType.Timer,"RemoveCraftingModule", CleanUp)
--RegisterEventHandler(EventType.Timer,"CraftingTimer", HandleCraftingTimer)
--RegisterEventHandler(EventType.CreatedObject,"crafted_item", HandleItemCreation)
--RegisterEventHandler(EventType.Message, "ConsumeResourceResponse", HandleConsumeResourceResponse)
RegisterEventHandler(EventType.DynamicWindowResponse, "CraftingWindow", HandleCraftingMenuResponse)
RegisterEventHandler(EventType.StartMoving, "" , InterruptCrafting)
