require 'dynamic_spawn_controller'
mDynamicSpawnKey = "DynamicUndeadEasySpawner"

mDynamicHUDVars.Title = "Undead Rising"
mDynamicHUDVars.Description = "Defeat waves of the undead along with the Crypt Stalker."

local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        msg = "Skeletons begin to appear."
    elseif( progress == 2 ) then 
        msg = "Zombies begin to appear."
    elseif( progress == 3 ) then 
        msg = "Bone magi begin to appear."
    elseif( progress == 4 ) then 
        msg = "Ghouls begin to appear."
    elseif( progress == 5 ) then 
        msg = "The Crypt Stalker approaches."
    elseif( progress == 999 ) then 
        msg = "The undead return to their graves."
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end