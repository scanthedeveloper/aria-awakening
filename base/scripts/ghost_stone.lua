
RegisterSingleEventHandler(EventType.ModuleAttached, "ghost_stone", 
    function ()
         SetTooltipEntry(this,"ghost_stone", "Perhaps you should inspect this item further")
    end)

RegisterEventHandler(EventType.Message, "UseObject", 
    function(user,usedType)
        if(usedType ~= "Use" and usedType ~= "Examine") then return end
        local cont = this:TopmostContainer()
        if(cont ~= user) then
            user:SystemMessage("[$1816]","info")
            return
        end

        user:SystemMessage("Whatever power was in this stone has long since dissipated. It dissolves in your hands.")

        this:Destroy()
    end)