require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

--[[AI.QuestStepsInvolvedIn = {
    {"FighterProfessionTierThree", 18}, -- step 18 doesnt exist, this throws an error
}]]

AI.GreetingMessages = 
{
    "My name's Saurial.",
}

function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,
    	"Just an adventurer, like yourself. Excuse me, I'm busy at the moment."
    	)
    --QuickDialogMessage(this,user,"REPLACED -> I am a quest guy!")
end

function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)] or AI.GreetingMessages[1]

    response = {}

    response[1] = {}
    response[1].text = "Who are you?"
    response[1].handle = "Who" 
    
    response[2] = {}
    response[2].text = "Goodbye."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)