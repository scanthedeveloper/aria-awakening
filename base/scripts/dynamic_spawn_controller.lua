require 'simple_mob_spawner'

--[[

    Spawn controller designed to scale spawn strength based on players in spawn radius.

    Init Variables:
        spawnRadius -- how far around the controller can mobs spawn.
        spawnEffect -- what visual effect should play when mobs spawn?
        spawnMode   -- how should the spawn advance? [ Random | Progressive ]
            -- Random will pull mobs randomly from the spawn table if they fill the power vacuum.
            -- Progressive will pull from the spawn table based on the value of mSpawnProgress.

]]

mSpawnRateIncrease = 1.03 -- This number determines how fast the spawns ramp up. The higher it is the faster things will spawn.
mCurrentSpawnPower = 0 -- the current power of the spawner
mCurrentPlayerPower = 0 -- the current power of the players around the spawner
mAdjustdPlayerPower = 0 -- the power of players adjusted over time
mSpawnKillScore = 0 -- the score total of all mobs killed since this spawner started
--mPlayerPowerIncrease = 0
mSpawnTimeLimitSeconds = this:GetObjVar("timeLimitInSeconds") or 1200 -- 15 minutes default
mSpawnDelayRangeMSMin = 1000
mSpawnDelayRangeMSMax = 15000
mDynamicHUDVars = {
    Title = "Dynamic Event",
    Description = "A dynamic event is running.",
    EndTime =  DateTime.UtcNow:Add(TimeSpan.FromMinutes(math.floor(mSpawnTimeLimitSeconds/60))),
    Stage = 1,
    MaxStage = 1,
    Progress = 0
}
mDisplayHUD = false
mPlayersInView = {} -- players in range of the dynamic spawner
mSpawnTable = {} -- mobs this spawner can spawn
mobSpawnTable = {} -- table of monsters that have been spawned ( true=alive/false=dead )
mPlayerContributions = {} -- table which holds the total damage each player did
mPlayerCount = 0 -- how many unique players have entered the event area?
mTotalDamageDone = 0 -- total damage done to all mobs created by this spawner
mSpawnProgress = 1 -- what stage is the spawn at ?
mProgressModifier = this:GetObjVar("progressModifier") or 2.5 -- default progress modifier
mSpawnRadius = this:GetObjVar("spawnRadius") or 10 -- default spawnRadius
mStartTime = nil -- when did this event start?
mEndTime = nil -- when should this event end?
mPauseSpawning = false -- is spawning paused? (ie. nothing should spawn)
mBossDead = false -- is the boss dead?
mBossObject = nil -- reference to the boss mobile object
mMaxSpawnLimit = 20 -- number of mobs that can spawned and alive at any given time
mIsPersistant = true -- does this spawner stay after the event ends?
mTriggerChance = nil
mDynamicSpawnKey = nil
mDebugMode = false
mAutoStart = false
mScaleBoss = false

-- On Backup
RegisterEventHandler(EventType.LoadedFromBackup,"",function()FireEngines()end)
-- On Load
RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), 
    function()
        this:SetObjVar("initializer", initializer)
        FireEngines()
    end
)

RegisterEventHandler(EventType.Message, "DamageApplied", 
    function(mob, player, damageAmount)
        damageAmount = tonumber( damageAmount )
        if( 
            mob and mob:IsValid() and  -- make sure mob is good
            player and player:IsValid() and -- make sure player is good
            damageAmount > 0 -- make sure damage was done
        ) then
            mTotalDamageDone = mTotalDamageDone + damageAmount
            local playerID = player:ToString()
            local damage = (mPlayerContributions[playerID] or 0) + damageAmount
            
            -- If we are adding a new player lets up our count
            if( mPlayerContributions[playerID] == nil ) then
                mPlayerCount = mPlayerCount + 1
            end

            mPlayerContributions[playerID] = damage
        end
    end)
    

RegisterEventHandler(EventType.Message, "Start", 
function()
    ResetSpawner()
    StartSpawning()
end)

RegisterEventHandler(EventType.Message, "Stop", 
function()
    ResetSpawner()
    EndSpawnController(true)
end)

RegisterEventHandler(EventType.Message, "LeaveSpawnArea", 
function( playerObj )
    mPlayersInView[playerObj] = nil
end)

RegisterEventHandler(EventType.Message, "GetSpawnData", function( playerObj, admin )
    --DebugMessage( "Admin: " .. tostring( admin ) )
    
    if( admin == true ) then

        playerObj:SendMessage( "GetSpawnDataAdmin",{
            Title = mDynamicHUDVars.Title,
            Stage = mSpawnProgress,
            MaxStage = mDynamicHUDVars.MaxStage,
            PlayersInRange = CountPlayersInView(),
            TotalMonstersSpawned = #mobSpawnTable,
            UniquePlayerCount = mPlayerCount,
            IsSpawning = ( mStartTime ~= nil ),
            IsPaused = mPauseSpawning,
            SpawnRadius = mSpawnRadius,
            DynamicHUD = mDynamicHUDVars,
            SpawnPower = mCurrentSpawnPower,
            KillScore = mSpawnKillScore,
            PlayerPower = mCurrentPlayerPower,
            AdjustPlayerPower = mAdjustdPlayerPower,


        })
    else
        playerObj:SendMessage( "GetSpawnData",{
            IsSpawning = ( mStartTime ~= nil ),
            SpawnRadius = mSpawnRadius,
            DynamicHUD = mDynamicHUDVars,
        })
    end
end)

function FireEngines()
    local initializer = this:GetObjVar("initializer")
    initializer.TeamType = initializer.TeamType or "Undead" -- default

        -- Gets the spawnTable set in [ static/dynamic_spawn_tables.lua ]
        if(initializer and initializer.TeamType) then
            this:SetObjVar("spawnTable",DynamicSpawnTables.MobileTeamType[initializer.TeamType] or {})
        end

        -- Determine if the dynamic spawn should autostart
        if(initializer and initializer.AutoStart) then
            mAutoStart = true
        end

        -- Determine if the dynamic spawn should be destroyed
        if(initializer and initializer.DestroyOnEnd) then
            mIsPersistant = false
        end

        -- Determine if the dynamic spawn boss should scale
        if(initializer and initializer.ScaleBoss) then
            mScaleBoss = true
        end

        -- Get HUDVars
        if(initializer and initializer.SpawnKey) then
            this:SetObjVar("SpawnKey", initializer.SpawnKey)
        end

        -- Get HUDVars
        if(initializer and initializer.HUDVars) then
            mDynamicHUDVars.Title = initializer.HUDVars.Title or "Universal Event Started"
            mDynamicHUDVars.Description = initializer.HUDVars.Description or "A universal dynamic event is running."
        end

        -- Get ProgressMessages
        if(initializer and initializer.ProgressMessages) then
            this:SetObjVar("ProgressMessages", initializer.ProgressMessages)
        end

        -- This determines if the spawner is triggered by the death of a specific TeamType
        if(initializer and initializer.TriggerSpawnTeamType) then
            local triggerRadius = initializer.TriggerDeathRadius or 30
            mTriggerChance = initializer.TriggerChance or 0.01
            LoadTriggerView( initializer.TriggerSpawnTeamType, triggerRadius )
        else
            mStartTime = os.time(os.date("!*t")) -- UNIX EPOCH TIMESTAMP
            mEndTime = mStartTime + mSpawnTimeLimitSeconds -- UNIX TIMESTAMP + SECONDS TO LAST
            --DebugMessage( "StartTime: " .. mStartTime " and EndTime: " .. mEndTime )
        end

        -- Sets the ObjVars for mobiles that are spawned with this spawner
        if(initializer and initializer.SpawnObjVars) then
            this:SetObjVar("spawnObjVars",initializer.SpawnObjVars)
        end

        ResetSpawner()
        CheckSpawn()

end


-- We need to build the view that will watch for TriggerTeamTypes if needed.
function LoadTriggerView( teamType, triggerRadius )
    
    -- Watch for mobiles entering the trigger radius
    RegisterEventHandler(EventType.EnterView, "TriggerSpawnView",
    function (mobileObj)
        mobileObj:SetObjVar("TriggerController", this)
    end)

    -- Watch for players to enter the trigger radius
    RegisterEventHandler(EventType.EnterView, "DynamicSpawnPlayers",
    function (playerObj)
        --DebugMessage("PlayerEnteredView: " .. tostring( playerObj ))
        mPlayersInView[playerObj] = true
        playerObj:SendMessage("StartMobileEffect", "DynamicSpawn", this, mDynamicHUDVars)
        
        -- If the player can force the spawn to start, and the spawn isn't already started!
        if( ( mAutoStart or playerObj:HasObjVar("ForceStartSpawner_"..mDynamicSpawnKey)) and mStartTime == nil ) then
            StartSpawning()
        end
    end)


    -- A triggerMob was slain we need to determine if the event should start!
    RegisterEventHandler(EventType.Message, "TriggerMobSlain", 
    function()
        --DebugMessage( "mTriggerChance = " .. tostring(mTriggerChance) .. " | mStartTime = " .. tostring(mStartTime) )
        -- We are not currently spawning; but we passed the spawn success!
        if( mStartTime == nil and mTriggerChance ~= nil and Success( mTriggerChance ) ) then
            StartSpawning()
        end

    end)

    AddView("DynamicSpawnPlayers", SearchPlayerInRange(mSpawnRadius,true),1.0)
    AddView("TriggerSpawnView",SearchMulti({SearchObjVar("MobileTeamType",teamType),SearchObjectInRange(triggerRadius)}), 0.5)

end

function CountPlayersInView()
    local count = 0
    for k,v in pairs(mPlayersInView) do 
        count = count + 1
    end
    return count
end

--[[
    ++++++++++++++++++++++++++++    OVERRIDE FUNCTIONS  ++++++++++++++++++++++++++++++
    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
]]

function StartSpawning()
    ResetSpawner() -- Reset spawner to fresh

    -- Get spawn table
    mSpawnTable = this:GetObjVar("spawnTable")

    mDynamicHUDVars.MaxStage = #mSpawnTable or 0
    mDynamicHUDVars.EndTime =  DateTime.UtcNow:Add(TimeSpan.FromMinutes(math.floor(mSpawnTimeLimitSeconds/60)))

    mStartTime = os.time(os.date("!*t")) -- UNIX EPOCH TIMESTAMP
    mEndTime = mStartTime + mSpawnTimeLimitSeconds -- UNIX TIMESTAMP + SECONDS TO LAST
    ProgressAdjusted( 1, nil )
end

function GetSpawnPulse()
    return TimeSpan.FromSeconds(3)
end

function GetSpawnDelay()
    return TimeSpan.FromSeconds(1)
end

function DestroySelf()
    local prefabController = this:GetObjVar("PrefabController")
    if( prefabController ) then
        prefabController:SendMessage("Destroy")
    else
        this:Destroy()
    end
end

function ShouldSpawn(spawnData, spawnIndex)
    return true
end

function MobSpawned(success, objref, power, spawnEntry)
    if( success) then
        objref:SetObjVar("DynamicSpawner",this)
        --objref:SetObjVar("MobileTeamType","DynamicSpawn")
        if(objref:IsMobile()) then
            if(this:HasObjVar("spawnRadius")) then
                objref:SetFacing(math.random() * 360)
            else
                objref:SetFacing(this:GetFacing())
            end
        end

        local spawnObjVars = this:GetObjVar("spawnObjVars")
        if(spawnObjVars) then
            for varName,varData in pairs(spawnObjVars) do
                objref:SetObjVar(varName,varData)
            end
        end

        -- If we don't want spawns to be lootable lets clear their loot tables
        if ( spawnEntry.NoLoot == true ) then
            --DebugMessage("Clear Loot Table")
            objref:SetObjVar("noloot", true)
        end

        -- If this is the boss we want to track it.
        if( spawnEntry.Boss == true ) then
            objref:SetObjVar("DynamicSpawnerBoss",true)
            mBossObject = objref
            
            -- If the boss should be scaled, lets do that!
            if( mScaleBoss ) then
                DynamicSpawnTables.SetScaledHealth( mCurrentPlayerPower, mBossObject )
                DynamicSpawnTables.SetScaledAttack( mCurrentPlayerPower, mBossObject )

                CallFunctionDelayed(TimeSpan.FromMilliseconds(100),function() 
                    DynamicSpawnTables.SetScaledLootLevel( mBossObject )
                end)
            end

        end

        mobSpawnTable[#mobSpawnTable+1] = { State = true, Mobile = objref, Power = power }

    end
end

--[[
    ++++++++++++++++++++++++++++++++++    END  +++++++++++++++++++++++++++++++++++++++
    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
]]

-- Resets the spawner to the default "pre-spawn" conditions
function ResetSpawner(  )
    --DebugMessage("Spawner Reset!")
    mStartTime = nil
    mEndTime = nil
    mDisplayHUD = true
    mCurrentSpawnPower = 0
    mCurrentPlayerPower = 0
    mAdjustdPlayerPower = 0
    mSpawnKillScore = 0
    mPlayerContributions = {}
    mPlayerCount = 0
    mTotalDamageDone = 0
    mSpawnProgress = 1
    mBossDead = false
    mBossObject = nil

    -- For each mob, if it's valid and not dead lets destroy it!
    for i=1, #mobSpawnTable do 
        if( mobSpawnTable[i].Mobile:IsValid() and not IsDead( mobSpawnTable[i].Mobile )  ) then
            mobSpawnTable[i].Mobile:Destroy()
        end
    end

    mobSpawnTable = {}
    mPauseSpawning = false

end

-- Check Spawn Override
function CheckSpawn()

    -- Are we debugging this spawner?
    if( this:GetObjVar("Debug") == true ) then
        mDebugMode = true
    else
        mDebugMode = false
    end

    DoDebug("------------------ Checking Spawns ------------------")
    -- add some randomness so there arent all spawning at the same time
    this:ScheduleTimerDelay(GetSpawnPulse(), "spawnTimer")

    -- If the spawner is disabled then we don't do anything!
    if (this:HasObjVar("Disable")) then return end

    -- Determine if the spawn can or already has started!
    DoDebug("StartCheck: " .. tostring(StartCheckPassed()))
    if not ( StartCheckPassed() ) then return end

    -- Determine the power of all players in range
    local previousPlayerPower = mCurrentPlayerPower

    -- We want to use the maximum player power achieved always.
    mCurrentPlayerPower = math.max(mCurrentPlayerPower, (HelperSpawns.GetPlayersPowerInSpawnRegionOrRange(this, mPlayersInView) / 5)) -- TWEAKING THIS CHANGES INITIAL SPAWN
    
    DoDebug( "Players Power: " .. mCurrentPlayerPower )

    -- If more players come we need to make sure we update the adjusted power
    if( mCurrentPlayerPower > mAdjustdPlayerPower ) then
        mAdjustdPlayerPower = math.floor(mCurrentPlayerPower)
    end

    -- We need to base the spawn rate off this adjusted player power
    mSpawnRateIncrease = this:GetObjVar("SpawnRateIncrease") or mSpawnRateIncrease
    mAdjustdPlayerPower = math.floor(mAdjustdPlayerPower * mSpawnRateIncrease)

    -- Check kills and progress spawn if needed
    local fillQueue = CheckKills()

    -- If spawning is paused we don't do anything.
    if( mPauseSpawning == true ) then return end

    DoDebug( "Adjusted Power: " .. mAdjustdPlayerPower )
    DoDebug( "SpawnKill Score: " .. mSpawnKillScore )
    DoDebug( "Spawn Power: " .. mCurrentSpawnPower )

    -- Determine the power vacuum based on spawn/player power
    local powerVacuum = math.floor(HelperSpawns.CalculatePowerVacuum( mAdjustdPlayerPower, mCurrentSpawnPower ))
    
    -- Add monsters to spawn queue based on power vacuum
    if( fillQueue ) then
        for i=1, 10 do 

            if( powerVacuum > 0 and not mPauseSpawning ) then

                -- What type of spawner do we have?
                local spawnMode = this:GetObjVar("spawnMode") or "Random"
                local spawnEntry = nil

                -- We want to just randomly pull mobs from the spawn tables
                if( spawnMode == "Random" ) then
                    spawnEntry = mSpawnTable[math.random( 1,#mSpawnTable )]
                elseif( spawnMode == "Progressive" ) then
                    spawnEntry = mSpawnTable[mSpawnProgress]
                else
                    spawnEntry = mSpawnTable[1]
                end

                -- Get spawnEntry stats
                local spawnEntryLevel = spawnEntry["Level"]
                local spawnEntryTemplate = nil
                
                -- Get the template based on variable type
                if( type(spawnEntry["Template"]) == "table" ) then
                    spawnEntryTemplate = spawnEntry["Template"][math.random( 1,#spawnEntry["Template"] )]
                else
                    spawnEntryTemplate = spawnEntry["Template"]
                end

                local adjustedPower = powerVacuum - HelperSpawns.GetPowerFromLevel(spawnEntryLevel)
                --DebugMessage("AdjustedPower: " .. adjustedPower)

                -- Spawn monsters, # of concurrent spawns based on power vacuum
                if( adjustedPower >= 0 ) then
                    local spawnLoc = GetSpawnLoc()
                    if(spawnLoc) then
                        --DebugMessage("Spawning: " .. spawnEntryTemplate )
                        mCurrentSpawnPower = math.floor(mCurrentSpawnPower + HelperSpawns.GetPowerFromLevel(spawnEntryLevel))
                        powerVacuum = math.floor(HelperSpawns.CalculatePowerVacuum( mAdjustdPlayerPower, mCurrentSpawnPower ))
                        
                        -- Do we need to pause spawning?
                        if( spawnEntry["Boss"] ) then
                            mPauseSpawning = true
                        end

                        -- We have the monsters spawn on a random delay, so they don't all show up at once :)
                        CallFunctionDelayed(TimeSpan.FromMilliseconds(math.random( mSpawnDelayRangeMSMin, mSpawnDelayRangeMSMax )), 
                            function()
                                -- If spawn effect exists, play it.
                                if( this:HasObjVar("SpawnEffect") ) then
                                    -- TODO cause an effect to occur where a mob spawns
                                end

                                -- Spawn this puppy!
                                CreateObj(spawnEntryTemplate, spawnLoc, "mobSpawned", HelperSpawns.GetPowerFromLevel(spawnEntryLevel), spawnEntry)
                            end
                        )
                        
                    end
                else
                    fillQueue = false
                    --DebugMessage("Stop Spawning")
                    break
                end
            end

        end
    end
end

-- Determines of the spawner has started and if so, check to make sure it hasn't ran out of time yet!
-- Additionally, either destroys or resets the timer if need be.
-- @return: true if spawner is good to go; false if it should be stopped
function StartCheckPassed()

    -- if the start time is nil, then we haven't started yet!
    local currentUnixTS = os.time(os.date("!*t"))
    if( mStartTime == nil ) then
        --DebugMessage("StartTime: NIL")
        return false 
    elseif( mEndTime and mEndTime < currentUnixTS ) then
        --DebugMessage("Event Timedout: " .. mEndTime)
        
        -- Event timed out
        ProgressAdjusted( 999, nil )

        -- If we are presistant just reset the spawner
        if( mIsPersistant ) then
            ResetSpawner()
        -- Else we need to reset the spawner (removes spawns) and destroy it!
        else
            ResetSpawner()
            DestroySelf()
        end

        return false -- return here to stop executing code below!
    end

    return true

end

-- Checks to see if mobs were killed in the spawn radius and helps update the progressModifier
-- to helper determine when the spawn should escalate.
function CheckKills()
    
    -- track if mobs are still alive at the spawner
    local mobsStillAlive = false
    local aliveCount = 0 
    -- Check if boss is still alive?
    if( mBossObject ~= nil and IsDead(mBossObject) ) then
        mBossDead = true
    end

    -- Loop through our mobs and 
    for i=1, #mobSpawnTable do 
        -- This mob was previously alive
        if( mobSpawnTable[i].State == true ) then
            mobsStillAlive = true
            aliveCount = aliveCount + 1
            -- If the mob is dead or decayed then lets count it's power toward kills
            if( not mobSpawnTable[i].Mobile:IsValid() or IsDead(mobSpawnTable[i].Mobile)  ) then
                mobSpawnTable[i].State = false
                mSpawnKillScore = mSpawnKillScore + mobSpawnTable[i].Power
            end
        end
    end

    mDynamicHUDVars.Progress = math.floor((mSpawnKillScore / ((mSpawnProgress * mProgressModifier) * mCurrentPlayerPower)) * 100)

    --DebugMessage( "KillScore = " .. mSpawnKillScore .. " | KillProgress = " .. ((mSpawnProgress * mProgressModifier) * mCurrentPlayerPower) )

    -- We need to check and see if our progress needs to advance
    if( (mSpawnTable and #mSpawnTable >= mSpawnProgress) and  ((mSpawnProgress * mProgressModifier) * mCurrentPlayerPower) <= mSpawnKillScore and mSpawnKillScore > 0 ) then
        mSpawnProgress = mSpawnProgress + 1

        -- If we have progressed passed our range of mobs, then the event needs to end.
        if( #mSpawnTable < mSpawnProgress and mPauseSpawning ~= true ) then
            --DebugMessage("Calling EndSpawnController 1")
            EndSpawnController() -- end event
            return false
        end

        ProgressAdjusted( mSpawnProgress, mobsStillAlive ) -- called each time progress adjusts
    
    -- If the boss and all mobs are dead and spawning is paused, we are done!
    elseif( mBossDead == true and mobsStillAlive == false and mPauseSpawning == true ) then
        ResetSpawner()
        if( mIsPersistant ) then
            EndSpawnController() -- end event
        else
            DestroySelf()
        end
        return false
    end

    --DebugMessage("Alive: " .. aliveCount .. " | Maximum: " .. mMaxSpawnLimit .. " CanSpawn: " .. tostring( (aliveCount < mMaxSpawnLimit) ))

    return (aliveCount < mMaxSpawnLimit)

end

-- Function to hand out loot based on event variables
function DistributeLoot()
    local lootMode = this:GetObjVar("lootMode") or "Weighted"
    if( lootMode == "Weighted" ) then
        
        for playerID,damageDone in pairs( mPlayerContributions ) do 

            -- What percent did this player contribute to their total share?
            local percentDamage = damageDone / (mTotalDamageDone / mPlayerCount)
            
            --TODO HAND OUT LOOT BASED ON CONTRIBUTIONS

        end
        
    end
end

-- Progress increased fuction
-- PLACEHOLDER NEEDS TO BE OVERRIDDEN
function ProgressAdjusted( progress, mobsStillAlive )
    --DebugMessage( "ProgressAdjusted: " .. progress .. " | " .. tostring(mobsStillAlive) )
    -- OVERRIDE THIS
    mDynamicHUDVars.Stage = math.min(progress, #mSpawnTable)
end

-- IF YOU OVERRIDE THIS FUNCTION BE SURE TO CALL THIS FUNCTION INSIDE THE OVERRIDE FUCTION
-- AS IT DESTROYS THE SPAWNER.
-- Event has ended
function EndSpawnController(skipDelay)
    -- Pause spawning
    mPauseSpawning = true
end

function DoDebug( value ) 
    if( mDebugMode == true ) then
        DebugMessage( value )
    end
end