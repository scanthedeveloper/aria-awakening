require 'catacombs_boss_spawner'

AddView("FireInProximity",SearchPlayerInRange(this:GetObjVar("Range")))

RegisterEventHandler(EventType.EnterView,"FireInProximity",
	function (mob)
		if (this:HasObjVar("Disabled")) then return end

		local bossObj = this:GetObjVar("Boss")
		if (this:HasTimer("ResetTimer")) and (not bossObj or bossObj:IsValid() == false) then
			mob:SystemMessage("You must wait before Death can re-materialize into his physical form.","info")
		end
	    
	    local traps = FindObjects(SearchObjVar("TrapKey",this:GetObjVar("TrapKey")),GameObj(0))
	    for i,trap in pairs(traps) do
	    	trap:SendMessage("Activate")
	    end
	    -- Removed by bphelps, was causing issue with spawn resetting
	    --DebugMessage(1)
	    --this:ScheduleTimerDelay(TimeSpan.FromSeconds(this:GetObjVar("ResetTime")),"ResetTimer")
	end)

RegisterEventHandler(EventType.Message,"ResetDelay",
	function ( ... )
		--DebugMessage("Resetting activate delay:" .. this:GetObjVar("ResetTime"))
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(this:GetObjVar("ResetTime")),"ResetTimer")
	end)