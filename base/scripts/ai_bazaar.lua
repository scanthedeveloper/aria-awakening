require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false
AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

local currencyName = QuestsLeague.LeagueCurrencyName.Explorers
local hasRewards = this:HasObjVar("HasRewards")

function Dialog.OpenGreetingDialog(user)
        local text = "Welcome! I am representative of the Celador Trading Company. We keep updated records for all items being sold by Celadorians around the world. Would you like to browse the bazaar?"
        local response = {}
        local key = 1

        response[key] = {}
        response[key].text = "Browse Bazaar"
        response[key].handle = "BrowseBazaar"
        key = key + 1

        --response[key] = {}
        --response[key].text = "What is in the supply box?"
        --response[key].handle = "LotteryBoxInfo"
        --key = key + 1

        --[[
        response[key] = {}
        response[key].text = "Who are you?"
        response[key].handle = "Who"
        key = key + 1
        ]]


        response[key] = {}
        response[key].text = "Goodbye."
        response[key].handle = ""

        NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenBrowseBazaarDialog(user)
    user:SendMessage("StartMobileEffect", "BazaarBoard", this )
end