
--- this is a module that cluster control attaches to its self when told to tax all the houses in a region


local _Controllers = {}
local _I,_Frame_I = 0,0
local _FrameDelay = TimeSpan.FromSeconds(1)
local _PlantsPerSecond = 10

local _Done = 0

function CleanUp()
    --DebugMessage("[Plant Growth] Done. Total Plants: "..#_Controllers)
    this:Destroy()
end

function AddTempleteTo_Controllers( template )
    local controllers = FindObjects(SearchTemplate(template))
    for i=1,#controllers do
        local controller = controllers[i]
        if ( controller and controller:IsValid() ) then
            _Controllers[#_Controllers+1] = controller
        end
    end
end

function GrowAll()
    --DebugMessage("[Plant Growth] Started.")
    -- Plant Pots
    AddTempleteTo_Controllers("garden_plant_pot")
    -- Small Raised Beds
    AddTempleteTo_Controllers("garden_raised_bed_small")
    -- Medium Raised Beds
    AddTempleteTo_Controllers("garden_raised_bed_medium")
    -- Large Raised Beds
    AddTempleteTo_Controllers("garden_raised_bed_large")

    if ( #_Controllers < 1 ) then
        CleanUp()
        return
    end
    GrowNext()
end

function CheckDone()
    _Done = _Done + 1
    if ( _Done >= #_Controllers ) then
        CleanUp()
    end
end

function GrowNext()
    _I = _I + 1
    -- no more
    if ( _I > #_Controllers ) then return end

    xpcall(function()
        -- grow plant via controller
        Gardening.DoGrowth(_Controllers[_I])
        CheckDone()
    end, function(e)
        LuaDebugCallStack("[PLANT GROWTH CONTROLLER]" .. e)
    end)

    -- do next
    _Frame_I = _Frame_I + 1
    if ( _Frame_I > _PlantsPerSecond ) then
        _Frame_I = 0
        -- next frame
        this:ScheduleTimerDelay(_FrameDelay, "GrowNext")
    else
        -- same frame
        GrowNext()
    end
end

RegisterEventHandler(EventType.Timer, "GrowNext", GrowNext)

RegisterEventHandler(EventType.ModuleAttached, "plant_growth_controller", function()
    -- put it into the next frame so we can detach
    CallFunctionDelayed(_FrameDelay, GrowAll)
end)

