require 'dynamic_spawn_controller'
mDynamicSpawnKey = "DynamicUndeadInsaneSpawner"


local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        msg = "Undead: Stage 1"
    elseif( progress == 2 ) then 
        msg = "Undead: Stage 2"
    elseif( progress == 3 ) then 
        msg = "Undead: Stage 3"
    elseif( progress == 4 ) then 
        msg = "Undead: Stage 4"
    elseif( progress == 999 ) then 
        msg = "The undead event has timed out."
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end