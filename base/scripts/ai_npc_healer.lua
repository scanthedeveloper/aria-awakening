require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

AI.QuestStepsInvolvedIn = {
    {"HealerProfessionIntro", 1},
    {"HealerProfessionIntro", 2},
    {"HealerProfessionIntro", 3},
    {"HealerProfessionIntro", 5},
}

AI.GreetingMessages = 
{
    "Are you interested in Healing?",
}

function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,
    	"I am a Healer. I want to help those hurt by this dark, dangerous world. If you feel the same, I could teach you!"
    	)
    --QuickDialogMessage(this,user,"REPLACED -> I am a quest guy!")
end

function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)] or AI.GreetingMessages[1]

    response = {}

    response[1] = {}
    response[1].text = "Who are you?"
    response[1].handle = "Who" 
    
    response[2] = {}
    response[2].text = "Goodbye."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)