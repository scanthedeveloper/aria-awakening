require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

--[[ QUEST ]]
AI.QuestsWelcomeText = "Traveler! I knew you could decipher the secrets of the Monolith. I have some tasks for you to aide me with."
    
AI.QuestStepsInvolvedIn = {
    {"MonolithQuests", 1},
    {"MonolithQuests", 2},
    {"MonolithQuests", 3},
}

function IntroDialog(user)
    Dialog.OpenGreetingDialog(user)
end

function Dialog.OpenGreetingDialog(user)
    text = "Traveler! I knew you could decipher the secrets of the Monolith. Help me to end Jianna's terror once and for all."

    local int = 0
    response = {}

    int = #response + 1
    response[int] = {}
    response[int].text = "Goodbye."
    response[int].handle = ""

    NPCInteractionLongButton(text,this,user,"Responses",response)
end


OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)