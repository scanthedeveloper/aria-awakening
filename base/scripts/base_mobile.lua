-- most basic mobile script for animals that do not have loot

require 'equipment_cache'
-- this module handles derived stats and adding and removing stat modifiers
require 'base_mobilestats'
-- allows us to add temporary modifiers to this mobile.
require 'base_mobile_mods'
-- allows this item to be used.
require 'use_object'

mInvisibilityEffects = {}
mInvisEffectsCount = 0

--[[ Damage / Heal tracking for mobs (used in the killboard / event record system)]]--

-- clear damagers/healers list on start
mDamagers = {}
mHealers = {}
this:DelObjVar("Damagers")
this:DelObjVar("Healers")

function UpdateDamagersList(attacker, damage)
	-- reasign to owner if applicable
	attacker = attacker:GetObjVar("controller") or attacker

	if(mDamagers[attacker]) then
		mDamagers[attacker].Amount = mDamagers[attacker].Amount + damage
	else
		mDamagers[attacker] = { Amount=damage }
	end

	this:SetObjVar("Damagers",mDamagers)
	
	-- set a timeout to clear their list after a 'battle'
	this:ScheduleTimerDelay(ServerSettings.Conflict.RelationDuration, "ClearDamagersHealers")
end

function UpdateHealersList(healer, amt)
	if(mHealers[healer]) then
		mHealers[healer].Amount = mHealers[healer].Amount + amt
	else
		mHealers[healer] = { Amount=amt }
	end

	this:SetObjVar("Healers",mHealers)

	ForeachConflict( this, function( conflictObj )  
		if( healer ~= this and conflictObj:IsValid() and not IsDead(conflictObj) and conflictObj:HasObjVar("MobileTeamType") ) then
			if( IsPet( this ) ) then
				conflictObj:SendMessage("AddAggro", healer, 1)
			else
				conflictObj:SendMessage("AddAggro", healer, math.floor( amt / 3 ))
			end
		end
	end)

	-- set a timeout to clear their list after a 'battle'
	this:ScheduleTimerDelay(ServerSettings.Conflict.RelationDuration, "ClearDamagersHealers")
end

RegisterEventHandler(EventType.Timer, "ClearDamagersHealers", function()
    	Verbose("Mobile", "ClearDamagersHealers")
		mDamagers = {}
		mHealers = {}
		this:DelObjVar("Damagers")
		this:DelObjVar("Healers")
	end)

RegisterEventHandler(EventType.Message, "Kill", function()
    	DoMobileDeath()
    	--this:Destroy()
	end)

function Speak(entry)
    if( entry.Text ~= nil ) then
        this:NpcSpeech(entry.Text)
    end
    if( entry.Audio ~= nil ) then
        this:PlayObjectSound(entry.Audio)
    end
end

function DoMobileDeath(damager, damageSource)
	Verbose("Mobile", "DoMobileDeath", damager)
	SetCurHealth(this,0)

	EndMobileEffectsOnDeath(this)
	
	-- When a mobile dies we need to award them the kill if they had it tagged.
	if( this:HasObjVar("Conflicts") ) then

		ForeachConflict(this, function( aggressor ) 
			aggressor:SendMessage("VictimKilled", this, nil, nil, nil) 
		end)

	elseif not(damager == this) and (damager ~= nil) then		
		damager:SendMessage("VictimKilled", this, damageAmount, damageType, damageSource)
	end

	-- mounts are the only mobiles that can be equipped
	if(this:IsEquipped()) then
		DismountMobile(this:ContainedBy(), this)
	end

	-- If we have a TriggerController we need to tell them we died!
	local triggerController = this:GetObjVar("TriggerController")
	if( triggerController ~= nil ) then
		triggerController:SendMessage("TriggerMobSlain")
	end

	this:SendMessage("HasDiedMessage", damager)
	this:PlayObjectSound("Death", true)
	
	this:StopMoving()
	this:ClearCollisionBounds()

	RemoveAllInvisibilityEffects()

	-- these should be calculated via MarkStatsDirty after IsDead returns true, but this works for now.

	SetMobileMod(this, "HealthRegenPlus", "Death", -1000)
	SetMobileMod(this, "ManaRegenPlus","Death", -1000)
	SetMobileMod(this, "StaminaRegenPlus","Death", -1000)
	SetMobileMod(this, "BloodlustRegenPlus","Death", -1000)

	if ( this:IsPlayer() and not IsPossessed(this)) then
		if not( IsCriminal(this) ) then
			PunishAllAggressorsForMurder(this)
		end
		Militia.RewardKill(this)
		GuildHelpers.RewardWarKill(this,damager)
	else
		-- if it's a pet
        if ( _MyOwner ~= nil ) then
            --remove durability from tamed pets
			AdjustDurability(this, ServerSettings.Durability.Pets.OnDeath)
        else
			HandleMobileDeathRewards(this)
            FreezeConflictTable(this)
		end

		this:SetMobileFrozen(true,true)
		this:PlayAnimation("die")
		-- set as corpse
		this:SetSharedObjectProperty("IsDead", true)

		UpdateName(this:GetName() .. " Corpse")

		-- freeze conflict table on the mobile

		local spawnerObj = this:GetObjVar("Spawner")
		if(spawnerObj) then
			spawnerObj:SendMessage("MobHasDied",this)
		end		
		
		if (not this:HasObjVar("DoNotDecay") and not(this:DecayScheduled())) then
			Decay(this, 600)
		end

		local oldUseCaseList = this:GetObjVar("UseCases")
		if(oldUseCaseList ~= nil) then
			this:SetObjVar("LivingUseCases",oldUseCaseList)
		end

		this:SetObjVar("UseCases",{})

		--Bloodlust.TryApplyToCorpse(this) disabled for now, probably will be a necro thing
	end
	
	-- record the event of death
	EventTracking.RecordDeathEvent(this,damager)
	-- clear damagers/healers list
	this:FireTimer("ClearDamagersHealers")
end

-- isReflected is not used in here, instead it's used in other things sending this message to determine if the damage should be reflected or not, 
	-- this is to prevent reflected damage from reflecting back and forth. It's not pretty when this happens.
--Damage source is being passed in to check for players kill achievement
function HandleApplyDamage(damager, damageAmount, damageType, isCrit, wasBlocked, isReflected, damageSource)
	Verbose("Mobile", "HandleApplyDamage", damager, damageAmount, damageType, isCrit, wasBlocked, isReflected, damageSource)
	if( IsDead(this) or damageAmount == nil ) then
		return
	end

	if ( this:HasObjVar("Invulnerable") ) then
		this:NpcSpeech("[C0C0C0]Invulnerable[-]", "combat")
		return
	end

	this:SendMessage("BreakInvisEffect", "Damage", "Combat")	

	if ( IsMobileImmune(this) ) then
		DoMobileImmune(this)
		return
	end

	damageType = damageType or "Bashing"
	local typeData = CombatDamageType[damageType]

	--DebugMessage( "DamageType", damageType )
	
	local hasMageArmor = HasMobileEffect(this, "MageArmor")
	if ( not hasMageArmor and IsPlayerCharacter(damager) ) then
		-- If the player has wisp armor and is casting it can prevent interrupts
		if( HasMobileEffect(this, "WispArmor") and this:HasTimer("SpellPrimeTimer") ) then
			this:NpcSpeech("[ffffff]Absorbed Interrupt[-]", "combat")
			this:SendMessage("ConsumeWispShield")
		else
			if( not wasBlocked ) then
				if( not typeData.SkipCastInterrupt ) then
					CheckSpellCastInterrupt(this)
				end
			end
		end
	end

	-- If the players is in town and being attacked by a mob, call for guard help!
	if( 
		GetGuardProtection(this) == "Town" -- In town
		and (not IsPlayerCharacter(damager) and not IsPet(damager))  -- Not Player/Pet
		and IsPlayerCharacter(this) -- Defender is player!
	) then
		
		local nearestGuard = GetNearestGuard( this )
		if( nearestGuard and nearestGuard:IsValid() ) then
			nearestGuard:SendMessage("RequestHelp", damager, nil, this ) --help me
		end
		
	end

	
	-- not a real combat damage type, stop here.
	if not( typeData ) then
		LuaDebugCallStack("[HandleApplyDamage] invalid type: "..damageType)
		return
	end

	-- Increase sorcery damage
	if( damageType == "Sorcery" ) then
		local damageBoost = GetMobileMod(MobileMod.SorceryDamageTakenTimes, 1)
		--DebugMessage("Sorcery Damage BEFORE : ", damageAmount, damageBoost)
		damageAmount = damageAmount * damageBoost
		--DebugMessage("Sorcery Damage AFTER : ", damageAmount, damageBoost)
	end

	local mod = {plus = 0,times = 1}
	if ( typeData.Magic ) then
		if ( not isReflected and hasMageArmor ) then
			this:NpcSpeech("[ffffff]Reflected[-]", "combat")
			return
		end

		damageAmount = TrySpellTriggers( damageAmount )

		-- Sorcery should not benefit from magic damage increases
		if( damageType ~= "Sorcery" ) then
			mod.plus = GetMobileMod(MobileMod.MagicDamageTakenPlus)
			mod.times = GetMobileMod(MobileMod.MagicDamageTakenTimes, 1)
		end

		-- Bloodlust
		if ( HasMobileEffect(this, "Bloodlust") ) then
			local random = math.random(1, 100)
			if ( random > 50 ) then
				bloodlustGain = math.round(damageAmount/20) + 1
				AdjustCurBloodlust(this, bloodlustGain)
				UpdateBloodlust(this)
				this:NpcSpeech("+ "..bloodlustGain.." Bloodlust", "combat")
			end
		end
		
	end

	if ( typeData.Physical ) then
		mod.plus = GetMobileMod(MobileMod.PhysicalDamageTakenPlus)
		mod.times = GetMobileMod(MobileMod.PhysicalDamageTakenTimes, 1)

		-- individual mob damage interactions ie shield wall, nemesis
		-- nemesis
		if ( HasMobileEffect(this, "Nemesis") ) then
			local mobileKind = damager:GetObjVar("MobileKind")
			if ( mobileKind ) then
				local damagerNemesis = this:GetObjVar("Nemesis")
				if ( damagerNemesis and damagerNemesis ~= mobileKind ) then
					damageAmount = damageAmount * 1.5
				end
			end
		end
		if ( HasMobileEffect(damager, "Nemesis") ) then
			local mobileKind = this:GetObjVar("MobileKind")
			if ( mobileKind ) then
				local damagerNemesis = damager:GetObjVar("Nemesis")
				if ( damagerNemesis and damagerNemesis == mobileKind ) then
					damageAmount = damageAmount * 1.5
				end
			end
		end
		-- shield wall
		if ( HasMobileEffect(this, "ShieldWall") ) then
			local targetOfTarget = this:GetObjVar("CurrentTarget")
			if ( targetOfTarget and targetOfTarget == damager ) then
				local blockingSkill = GetSkillLevel(this, "BlockingSkill")
				damageAmount = damageAmount * ( 1 - (blockingSkill / 100 / 2) )
			end
		end
		-- Bloodlust
		if ( HasMobileEffect(this, "Bloodlust") ) then
			local random = math.random(1, 100)
			if ( random > 50 ) then
				bloodlustGain = math.round(damageAmount/20) + 1
				AdjustCurBloodlust(this, bloodlustGain)
				UpdateBloodlust(this)
				this:NpcSpeech("+ "..bloodlustGain.." Bloodlust", "combat")
			end
		end
		if ( HasMobileEffect(damager, "Bloodlust") ) then
			local random = math.random(1, 100)
			if ( random > 50 ) then
				bloodlustGain = math.round(math.min(damageAmount/10 + 1, 20))
				AdjustCurBloodlust(damager, bloodlustGain)
				UpdateBloodlust(damager)
				damager:NpcSpeech("+ "..bloodlustGain.." Bloodlust", "combat")
			end
		end
	end

	local modNames = {
		string.format("%sDamageTakenPlus", damageType),
		string.format("%sDamageTakenTimes", damageType)
	}
	if ( MobileMod[modNames[1]] ~= nil and MobileMod[modNames[2]] ~= nil ) then
		--DebugMessage(modNames[1], modNames[2], GetMobileMod(MobileMod[modNames[1]], mod.plus), GetMobileMod(MobileMod[modNames[2]], mod.times))
		damageAmount = ( damageAmount + GetMobileMod(MobileMod[modNames[1]], mod.plus) ) * GetMobileMod(MobileMod[modNames[2]], mod.times)
	end
	
	-- Magic, Piercing, Bashing, and Slashing reduction is PVP only.
	if( damager:IsPlayer() and this:IsPlayer() ) then
		-- Reduced damage based on the damage type
		damageAmount = TryReduceDamageFromDamageType(damageType, damageAmount)
	else
		-- Reduce damage based on damage reduction mobile mods
		damageAmount = TryReduceDamageFromMobileKind(damager, damageAmount)
	end

	--to account for more absorbing then damage and prevent 0 damage done (or NaN or Infinity)
	if ( damageAmount < 0.5 or damageAmount ~= damageAmount or damageAmount > 9999999999 ) then
		damageAmount = 1
	end
	
	damageAmount = math.round(damageAmount)

	if(isCrit) then
		this:NpcSpeech("[FF0000]"..damageAmount.."[-]", "combat")
	else
		this:NpcSpeech("[FCF914]"..damageAmount.."[-]", "combat")
	end

	-- Determine if the damage should be blocked by Wisp Armor
	if( HasMobileEffect(this, "WispArmor") ) then
		
		local wispArmor = this:GetObjVar("WispArmor") or 0
		this:SendMessage("StartMobileEffect", "WispArmor", this, { Damage = damageAmount })
		
		-- If the wisp armor completely negated the damage then return out
		if( wispArmor >= damageAmount ) then
			this:NpcSpeech("[ffffff]Fully Absorbed[-]", "combat")
			return 
		-- Otherwise subtract what armor was left from the total damage delt
		else
			this:NpcSpeech("[ffffff]Partially Absorbed[-]", "combat")
			damageAmount = damageAmount - wispArmor
		end
	end
	
	local curHealth = GetCurHealth(this) or 1
	if ( damageAmount > curHealth ) then damageAmount = curHealth end

	UpdateDamagersList(damager, damageAmount)
    -- advance conflict
    if not( isReflected ) then
        AdvanceConflictRelation(damager, this, typeData.NoGuards)
    end
	
	-- Handle dynamic spawner damage contribution calculations
	if( this:HasObjVar("DynamicSpawner") ) then
		local spawner = this:GetObjVar("DynamicSpawner")
		spawner:SendMessage("DamageApplied", this, damager, damageAmount)
	end

	----------------------------------------------------------------------------------------------
	-- START Process OnMeleeStruck
	----------------------------------------------------------------------------------------------
	
	if( Success( GetMobileMod(MobileMod.ChanceToCastEchofallOnMeleeStruck) / 100 ) ) then
		StartMobileEffect(this, "OnMeleeStruck", damager, { StruckTag = "TriggerEchofall" })
	end

	if( Success( GetMobileMod(MobileMod.ChanceToGainEchoHealOnMeleeStruck) / 100 ) ) then
		StartMobileEffect(this, "OnMeleeStruck", damager, { StruckTag = "TriggerEchoHeal" })
	end

	if( Success( GetMobileMod(MobileMod.ChanceToGainWispOnMeleeStruck) / 100 ) ) then
		StartMobileEffect(this, "OnMeleeStruck", damager, { StruckTag = "TriggerGainWisp" })
	end

	----------------------------------------------------------------------------------------------
	-- END Process OnMeleeStruck
	----------------------------------------------------------------------------------------------

	local newHealth = curHealth - damageAmount
	if (newHealth <= 0) then
		DoMobileDeath(damager, damageSource)
	else
		SetCurHealth(this,newHealth)
		if(damageAmount>=2) then 
			if not(wasBlocked) then
				this:PlayAnimation("was_hit")
			end

			if ( damageAmount > 3 ) then
				this:PlayObjectSound("Pain", true)
			end
			if ( damageAmount > 10 ) then
				this:PlayEffect("BloodEffect_A")

				if(IsMounted(this) and ServerSettings.Combat.DazedOnDamageWhileMounted) then
					if (ServerSettings.Combat.DismountWhileDazed and HasMobileEffect(this, "Dazed") ) then
						local dismountRoll = math.random(0,100)
						local dismountChance = ServerSettings.Combat.DismountWhileDazedChance or 50
						if(dismountRoll < dismountChance) then
							DismountMobile(this, nil)
						end
					else
						local dazeRoll = math.random(0,100)
						local dazeChance = ServerSettings.Combat.DazeChance or 50
						if(dazeRoll < dazeChance) then
							this:SendMessage("StartMobileEffect", "Dazed", this, 3)
						end						
					end
				end
			end			
		end
	end

	return newHealth
end



--[[ MOBILE MOD FUNCTIONS ]]

function TrySpellTriggers(damageAmount)

	local enchants = EnchantingHelper.GetUniqueEquipmentEnchants(_Weapon, _Armor)

	for enchant,data in pairs(enchants) do 

		-- If this isn't spell triggered, or we don't have a mobile mod then we need to leave!
		if( data.SpellTriggered and MobileMod[data.MobileMod] ) then

			local successChance = (math.min(GetMobileMod(MobileMod[data.MobileMod], 0), data.EffectRangeMax)/100)
			--DebugMessage("Success: " .. tostring(successChance))
			local success = Success(successChance)

			-- Block Spell
			if( success ) then

				-- Stop damage!
				if( data.PreventMagicDamage ) then
					this:NpcSpeech("[C0C0C0]Magic Damage Reduced[-]", "combat")
					damageAmount = 0
				end

				-- Mobile Effect
				if( data.MobileEffect and HasMobileEffect(this, data.MobileEffect) == false or data.MobieEffectCanStack == true ) then
					StartMobileEffect(this, data.MobileEffect, nil, { Duration = data.MobileEffectDuration or nil } )
				end

			end

		end

	end
	
	return damageAmount

end

function TryReduceDamageFromMobileKind( mobile, damageAmount )
	
	local mobileKind = mobile:GetObjVar("MobileKind")
	local modName = string.format("%sDamageReductionTimes", mobileKind) or nil
	
	-- If we don't have a mobilekind or our mobilemod doesn't exist, just return the damage
	if( mobileKind == nil or modName == nil or MobileMod[modName] == nil ) then return damageAmount end

	-- We are reducing damage instead of increasing it
	local modifer = 1 - (GetMobileMod(MobileMod[modName], 0) / 100)
	
	-- We want to return the damage adjusted by the mobilekind modifiers
	return damageAmount * modifer

end

function TryReduceDamageFromDamageType( damageType, damageAmount )
	
	-- This is used to make all magic damage sources present as magic damage for reduction purposes.
	if( CombatDamageType[damageType].Magic and CombatDamageType[damageType].Magic == true ) then
		damageType = "Magic"
	end

	local modName = string.format("%sDamageReductionTimes", damageType) or nil
	--DebugMessage( modName )
	--DebugMessage( 1 - (GetMobileMod(MobileMod[modName], 0) / 100) )
	
	-- If we don't have a mobilekind or our mobilemod doesn't exist, just return the damage
	if( damageType == nil or modName == nil or MobileMod[modName] == nil ) then return damageAmount end

	-- We are reducing damage instead of increasing it
	local modifer = 1 - (GetMobileMod(MobileMod[modName], 0) / 100)
	
	-- We want to return the damage adjusted by the mobilekind modifiers
	return damageAmount * modifer
	
end

function HandleManaAdjustRequest(amount, skipMods)
	
	if( IsDead(this) ) then return end
	
	if( amount == nil or amount ~= amount or amount > 9999999999 ) then return end

	--DebugMessage("Amount 1: " .. amount )

	if ( skipMods ~= true ) then
		local baseAmount = amount

		-- apply mods
		amount = ( amount - GetMobileMod(MobileMod.ManaReductionMinus) ) * ( 1 - (GetMobileMod(MobileMod.ManaReductionTimes, 0) / 100))
		--DebugMessage("Amount 1.1: " .. amount )
		--DebugMessage("Amount 1.2: " .. (baseAmount * ServerSettings.Combat.MaxLimits.ManaReductionTimes) )
		-- Cap how low mods can reduce mana cost
		amount = math.floor(math.min(amount, (baseAmount * ServerSettings.Combat.MaxLimits.ManaReductionTimes) ))

	end

	--DebugMessage("Amount 2: " .. amount )

	-- do the mana adjustment
	AdjustCurMana(this, amount)

end


function HandleHealRequest(amount, healer, skipMods)
	if( IsDead(this) ) then return end

	-- Sorcerers cannot be healed by any outside source
	if not( SorceryHelper.CanTargetBeHealed(healer, this) ) then
		return
	end

	if( amount == nil or amount <= 0 or healer == nil or amount ~= amount or amount > 9999999999 ) then return end

	if ( potionObj ~= nil ) then
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(10), "PotionHealCooldownTimer")
		-- consume potion
		potionObj:SendMessage("AdjustStack", -1)
		healer = potionObj
	end

	if ( skipMods ~= true ) then
		-- apply mods
		amount = ( amount + GetMobileMod(MobileMod.HealingReceivedPlus) ) * GetMobileMod(MobileMod.HealingReceivedTimes, 1)
	end

	-- ensure we don't over heal
	local myHealth = GetCurHealth(this)
	amount = math.floor(math.max(0,math.min(GetMaxHealth(this), myHealth + amount) - myHealth))

	-- do the health adjust and combat text
	AdjustCurHealth(this, amount)
	this:NpcSpeech("[00FF00]"..amount.."[-]", "combat")

	-- update the healers list (used for statistics and the like)
	if ( healer ) then
		UpdateHealersList(healer, amount)
	end
end

--[[ VISIBILITY CONTROL ]]--

function AddInvisibilityEffect(effectName)
	--DebugMessage(this:GetName() .. " Added Move Speed Mod: " .. effectName)

	if(mInvisibilityEffects[effectName] == nil) then
		mInvisibilityEffects[effectName] = effectName
		mInvisEffectsCount = mInvisEffectsCount + 1
	end
	--DebugMessage(this:GetName() .. " shound be cloaked.")
	this:SetCloak(true)
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(15), "ValidateInvisStatus")
end

function RemoveInvisibilityEffect(effectName)
	if(mInvisibilityEffects[effectName]) then
		mInvisibilityEffects[effectName] = nil
		mInvisEffectsCount = mInvisEffectsCount - 1
		if(mInvisEffectsCount < 1) then
			this:SetCloak(false)
			ClearCanSeeGroup(this)
			this:RemoveTimer("ValidateInvisStatus")
		end
	end
end

function RemoveAllInvisibilityEffects()
	if(mInvisEffectsCount > 0) then
		mInvisibilityEffects = {}
		mInvisEffectsCount = 0
		this:SetCloak(false)
		ClearCanSeeGroup(this)
	end
end
		
function ValidateInvisStatus()
	if (this:ContainedBy()) then return end
	if (this:HasObjVar("IsGhost")) then return end 
	if (this:HasObjVar("VisibleToDeadOnly")) then return end

	if(mInvisibilityEffects ~= nil) then
		for i,v in pairs(mInvisibilityEffects) do
			-- DAB HACK: Hiding does not use a module!
			if not (this:HasModule(i)) and i ~= "Hiding" and not(this:GetObjVar("Invulnerable") == true) then
				RemoveInvisibilityEffect(i)
			end
		end
	end
	mInvisEffectsCount = CountTable(mInvisibilityEffects)

	if (mInvisEffectsCount < 1) then				
		this:SetCloak(false)
	else
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(15),"ValidateInvisStatus")
	end
end
RegisterEventHandler(EventType.Timer, "ValidateInvisStatus",ValidateInvisStatus)

-- [[ Stats ]]--

function SetStartingStats(statTable,quiet)	
	-- begin with base stats
	--HACK for new character creation, fix later.
	local initialStats = statTable.Stats or { Str=20, Agi=20, Int=20, Wis=20, Will=20, Con=20 }

	if not (quiet) then
		if (GetStr(this) < initialStats.Str) then
			this:SystemMessage("[F7CC0A]Your Strength has increased, now "..initialStats.Str.." (+" .. tostring(-GetStr(this) + initialStats.Str) .. ")","info")
		elseif (GetStr(this) > initialStats.Str) then
			this:SystemMessage("[F7CC0A]Your Strength has decreased, now "..initialStats.Str.." (-" .. tostring(GetStr(this) - initialStats.Str) .. ")","info")
		end
	end
	SetStr(this,initialStats.Str or 10)
	if not (quiet) then
		if (GetAgi(this) < initialStats.Agi) then
			this:SystemMessage("[F7CC0A]Your Agility has increased, now "..initialStats.Agi.." (+" .. tostring(-GetAgi(this) + initialStats.Agi) .. ")","info")
		elseif (GetAgi(this) > initialStats.Agi) then
			this:SystemMessage("[F7CC0A]Your Agility has decreased, now "..initialStats.Agi.." (-" .. tostring(GetAgi(this) - initialStats.Agi) .. ")","info")
		end
	end
	SetAgi(this,initialStats.Agi or 10)
	if not (quiet) then
		if (GetInt(this) < initialStats.Int) then
			this:SystemMessage("[F7CC0A]Your Intelligence has increased, now "..initialStats.Int.." (+" .. tostring(-GetInt(this) + initialStats.Int) .. ")","info")
		elseif (GetInt(this) > initialStats.Int) then
			this:SystemMessage("[F7CC0A]Your Intelligence has decreased, now "..initialStats.Int.." (-" .. tostring(GetInt(this) - initialStats.Int) .. ")","info")
		end
	end
	SetInt(this,initialStats.Int or 10)	
	if not (quiet) then
		if (GetCon(this) < initialStats.Con) then
			this:SystemMessage("[F7CC0A]Your Constitution has increased, now "..initialStats.Con.." (+" .. tostring(-GetCon(this) + initialStats.Con) .. ")","info")
		elseif (GetCon(this) > initialStats.Con) then
			this:SystemMessage("[F7CC0A]Your Constitution has decreased, now "..initialStats.Con.." (-" .. tostring(GetCon(this) - initialStats.Con) .. ")","info")
		end
	end
	SetCon(this,initialStats.Con or 10)	
	if not (quiet) then
		if (GetWis(this) < initialStats.Wis) then
			this:SystemMessage("[F7CC0A]Your Wisdom has increased, now "..initialStats.Wis.." (+" .. tostring(-GetWis(this) + initialStats.Wis) .. ")","info")
		elseif (GetWis(this) > initialStats.Wis) then
			this:SystemMessage("[F7CC0A]Your Wisdom has decreased, now "..initialStats.Wis.." (-" .. tostring(GetWis(this) - initialStats.Wis) .. ")","info")
		end
	end
	SetWis(this,initialStats.Wis or 10)	
	if not (quiet) then
		if (GetWill(this) < initialStats.Will) then
			this:SystemMessage("[F7CC0A]Your Will has increased, now "..initialStats.Will.." (+" .. tostring(-GetWill(this) + initialStats.Will) .. ")","info")
		elseif (GetWill(this) > initialStats.Will) then
			this:SystemMessage("[F7CC0A]Your Will has decreased, now "..initialStats.Will.." (-" .. tostring(GetWill(this) - initialStats.Will) .. ")","info")
		end
	end
	SetWill(this,initialStats.Will)	

	DoRecalculateStats()

	-- next do health / mana / stamina
	if not (quiet) then
		if (GetMaxHealth(this) > GetCurHealth(this))  then
			this:SystemMessage("[F7CC0A]Your Health has increased, now "..GetMaxHealth(this).." (+" .. tostring(-GetMaxHealth(this) + GetCurHealth(this)) .. ")","info")
		elseif (GetMaxHealth(this) < GetCurHealth(this)) then
			this:SystemMessage("[F7CC0A]Your Health has decreased, now "..GetMaxHealth(this).." (-" .. tostring(GetMaxHealth(this) - GetCurHealth(this)) .. ")","info")
		end
	end
	SetCurHealth(this,GetMaxHealth(this))

	if not (quiet) then
		if (GetMaxMana(this) > GetCurMana(this))  then
			this:SystemMessage("[F7CC0A]Your Mana has increased, now "..GetMaxMana(this).." (+" .. tostring(-GetMaxMana(this) + GetCurMana(this)) .. ")","info")
		elseif (GetMaxMana(this) < GetCurMana(this)) then
			this:SystemMessage("[F7CC0A]Your Mana has decreased, now "..GetMaxMana(this).." (-" .. tostring(GetMaxMana(this) - GetCurMana(this)) .. ")","info")
		end
	end
	SetCurMana(this,GetMaxMana(this))

	if not (quiet) then
		if (GetMaxStamina(this) > GetCurStamina(this))  then
			this:SystemMessage("[F7CC0A]Your Mana has increased, now "..GetMaxStamina(this).." (+" .. tostring(-GetMaxStamina(this) + GetCurStamina(this)) .. ")","info")
		elseif (GetMaxStamina(this) < GetCurStamina(this)) then
			this:SystemMessage("[F7CC0A]Your Health has decreased, now "..GetMaxStamina(this).." (-" .. tostring(GetMaxStamina(this) - GetCurStamina(this)) .. ")","info")
		end
	end
	SetCurStamina(this,GetMaxStamina(this))

	-- DAB TODO: Shouldn't this go in player.lua?
	if(this:IsPlayer()) then
		SetCurVitality(this,GetMaxVitality(this))
	end

	-- set skills
	if( statTable.Skills ~= nil ) then
		for name, value in pairs(statTable.Skills) do
			
			if( name == 'Manifestation' or name == 'Evocation' ) then
				name = 'Magery'
			end

			local skillName = name .. "Skill"
			if (SkillData.AllSkills[skillName]) then			
				if not (quiet) then
					if GetSkillLevel(this,skillName) > value then
						this:SystemMessage("[F7CC0A]Your knowledge of the art of " .. name .. " has decreased, now "..value.." (-" .. tostring(GetSkillLevel(this,skillName) - value) .. ")","info")	
					elseif GetSkillLevel(this,skillName) < value then
						this:SystemMessage("[F7CC0A]Your knowledge of the art of " .. name .. " has increased, now "..value.." (+" .. tostring(-GetSkillLevel(this,skillName) + value) .. ")","info")	
					end
				end
				SetSkillLevel(this, skillName, value, false)
			else
				DebugMessage("ERROR: Template specifies invalid skill Template: " .. (this:GetObjVar("FormTemplate") or this:GetCreationTemplateId()) .. " Name: "..name)
			end
		end
	end

	if not (quiet) then
		this:SystemMessage("Your equipment has changed.","info")
	end	
end

function DoResurrect(statPercent, resurrector)
	if not( IsDead(this) ) then return end

	local controller = this:GetObjVar("controller")
	if ( controller and not controller:IsValid() ) then
		if ( resurrector and IsPlayerCharacter(resurrector) ) then
			resurrector:SystemMessage("Their owner is no where to be found, cannot resurrect.", "info")
		end
		return
	end

	--AddView("alert", SearchMobileInRange(GetSetting("AlertRange")))

	if (this:DecayScheduled()) then
		this:RemoveDecay()
	end

	this:SetCollisionBoundsFromTemplate(this:GetObjVar("FormTemplate") or this:GetCreationTemplateId())
	
	this:SetSharedObjectProperty("IsDead", false)
	
	this:SendMessage("OnResurrect")

	local livingUseCases = this:GetObjVar("LivingUseCases")
	if(livingUseCases ~= nil) then
		this:SetObjVar("UseCases",livingUseCases)
		this:DelObjVar("LivingUseCases")
	end
			
	local mobileType = this:GetMobileType()
	-- this order matters
	if(mobileType == "Friendly") then
		if IsMount(this) then
			if not( this:IsEquipped() ) then	
				SetDefaultInteraction(this,"Mount")
			end
		elseif ( HasUseCase(this,"Interact") ) then
			this:SetSharedObjectProperty("DefaultInteraction","Interact")
		else
			this:SetSharedObjectProperty("DefaultInteraction","Use")
		end
	else
		--if you're stupid enough to res an enemy mob.
		this:SetSharedObjectProperty("DefaultInteraction","Attack")
	end

	-- needed to clear sparkles on a resurrect
	local backpack = this:GetEquippedObject("Backpack")
	if ( backpack ~= nil and backpack:HasModule("tagged_mob") ) then
		backpack:SendMessage("ClearMobTag")
	end

	-- clear the conflict table on the mobile if they were resurrected for whatever reason
	ClearConflictTable(this)

	DeathEndAll(this, statPercent)
	
	ApplyMobEffects()

	UpdateName()
end

RegisterEventHandler(EventType.Message, "AddXPLevel", 
    function (skillName,amount)
        AddXPLevel(this,skillName,amount)
    end)

--On resurrection
RegisterEventHandler(EventType.Message, "Resurrect", function (statPercent, resurrector)
	if( not(IsDead(this)) ) then return end
		-- non player resurrects.
	DoResurrect(statPercent, resurrector)		
end)


--- I was swug on (They missed a swing at me)
function HandleSwungOn(attacker)
    -- advance conflict
    AdvanceConflictRelation(attacker, this)
end

RegisterEventHandler(EventType.Message, "SwungOn", function(...) HandleSwungOn(...) end)
RegisterEventHandler(EventType.Message, "DamageInflicted", function(...) HandleApplyDamage(...) end)
RegisterEventHandler(EventType.Message, "HealRequest", HandleHealRequest)
RegisterEventHandler(EventType.Message, "ManaAdjustRequest", HandleManaAdjustRequest)

RegisterEventHandler(EventType.Message, "RemoveInvisEffect", RemoveInvisibilityEffect)
RegisterEventHandler(EventType.Message, "AddInvisEffect", AddInvisibilityEffect)

RegisterSingleEventHandler(EventType.Timer,"mobile_delayed_init",
	function()
		if not(this:IsPlayer()) then
			-- give other scritps like AI a chance to set the name, then colorize it
			UpdateName()
		end
	end)

function LoadMobTraits()
	local mobTraits = nil
	if(initializer and initializer.MobTraits) then
		mobTraits = initializer.MobTraits
		this:SetObjVar("MobTraits", initializer.MobTraits)
	else
		mobTraits = this:GetObjVar("MobTraits")
	end

	if(mobTraits) then
		for trait,level in pairs(mobTraits) do 
			local traitData = MobTraits[trait]
			if( traitData and #traitData.Levels >= level and MobileMod[traitData.MobileMod] ) then
				if ( traitData.MobileMod == "MaxHealthTimes" or traitData.MobileMod == "MaxHealthPlus" ) then
					-- need to delay this since marking a stat dirty (HandleMobileMod) has a tiny delay within.
					RegisterSingleEventHandler(EventType.Timer, "DoRecalculateStats", function()
						SetCurHealth(this, GetMaxHealth(this))
					end)
				end
				HandleMobileMod(traitData.MobileMod, "MobTrait"..trait, traitData.Levels[level])
			else
				DebugMessage("ERROR: Invalid mob trait: "..tostring(trait).." "..level.. " Template: "..this:GetCreationTemplateId())
			end
		end
	end
end

function OnMobileLoad()
	-- clear disabled (remove obj var and clear mobile frozen)
	this:DelObjVar("Disabled")
	this:SetMobileFrozen(false, false)
	if not IsImmortal(this) and not IsDead(this) then
		this:SetCloak(false)
	end
	-- mobile mods are not persistent so we load them every time
	LoadMobTraits()
end

function OnMobileCreated()
	-- all mobiles have the ability to defend themselves
	if ( mSkipAddCombatModule ~= true ) then
		this:AddModule("combat")
	end
	--this:AddModule("mobile_footprints")

	if ( initializer.AvailableSpells ) then
		local availSpells = {}
		for i=1,#initializer.AvailableSpells do
			availSpells[initializer.AvailableSpells[i]] = true
		end
		this:SetObjVar("AvailableSpellsDictionary", availSpells)
	end

	if ( initializer.SelfMobileEffects ) then
		local mobileEffects = {}
		for i=1,#initializer.SelfMobileEffects do
			mobileEffects[initializer.SelfMobileEffects[i]] = true
		end
		this:SetObjVar("SelfMobileEffectsDictionary", mobileEffects)
	end

	if( initializer.DistrbutedLootTable ) then
		this:SetObjVar("DistrbutedLootTable", initializer.DistrbutedLootTable)
	end

	if ( initializer.TargetMobileEffects ) then
		local mobileEffects = {}
		for i=1,#initializer.TargetMobileEffects do
			mobileEffects[initializer.TargetMobileEffects[i]] = true
		end
		this:SetObjVar("TargetMobileEffectsDictionary", mobileEffects)
	end

	SetStartingStats(initializer,true)

	-- dynamic combat abilities
	if ( initializer.CombatAbilities ~= nil ) then
		SetInitializerCombatAbilities(this, initializer.CombatAbilities)
	end

	-- dynamic weapon abilities
	if ( initializer.WeaponAbilities ~= nil ) then
		this:SetObjVar("WeaponAbilities", initializer.WeaponAbilities)
		this:SendMessage("UpdateFixedAbilitySlots")
	end

	-- all players can see this mobs health
	this:SetStatVisibility("Health","Global")
	this:SetStatVisibility("Mana","Restricted")
	this:SetStatVisibility("Stamina","Restricted")

    if initializer.StatMods ~= nil then
		LuaDebugCallStack("initializer StatMods have been removed, use traits instead.")
	end
	
	if ( initializer.MobileEffects ~= nil ) then
		this:SetObjVar("InitMobEffects", initializer.MobileEffects)
	end
    
	-- wait one frame to color the name
	this:FireTimer("mobile_delayed_init")			

	OnMobileLoad()

	ApplyMobEffects(initializer.MobileEffects)
end

function ApplyMobEffects(effects)
	effects = effects or this:GetObjVar("InitMobEffects")
	if ( effects == nil ) then return end
	for i=1,#effects do
		StartMobileEffect(this, effects[i][1], nil, effects[i][2])
	end
end

function UpdateName(displayName)
	displayName = StripColorFromString(displayName or this:GetName())
	this:SetSharedObjectProperty("DisplayName", ColorizeMobileName(this, displayName))
end
RegisterEventHandler(EventType.Message, "UpdateName", UpdateName)

RegisterSingleEventHandler(EventType.ModuleAttached,GetCurrentModule(),
	function()
		if(not(this:HasObjVar("mobileInitialized"))) then
			this:SetObjVar("mobileInitialized",true)
			OnMobileCreated()
		end
	end)

RegisterSingleEventHandler(EventType.LoadedFromBackup,"",
	function ()
		if ( mSkipAddCombatModule ~= true and not this:HasModule("combat") ) then
			this:AddModule("combat")
		end

		OnMobileLoad()

		ApplyPersistentMobileEffects(this)
	
		ApplyMobEffects()
	end)

RegisterEventHandler(EventType.Message,"StartMobileEffect",
	function(effectName,target,args)
		StartMobileEffect(this,effectName,target,args)
	end)
	
function HandleRequestMobileMods( modTable, requester, requestId )
	if( modTable == nil ) then return end
	local mMobileMod = {}

	-- Build the table with just the CombatMods we need.
	for i=1,#modTable do 
		mMobileMod[ modTable[i] ] = MobileMod[ modTable[i] ]
	end
	
	-- Return the mods
	requester:SendMessage(requestId, mMobileMod)
end

RegisterEventHandler(EventType.Message, "RequestMobileMods", HandleRequestMobileMods)