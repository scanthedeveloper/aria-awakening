

function DoPack(controller)
    local plotOwner = Plot.GetOwner(controller)

    local hasHouse = false
    local houseDirection = 0
    local houseTemplate = ""    
    Plot.ForeachHouse(controller, function(house)        
            local houseData = Plot.GetHouseDataFromHouse(house)
            hasHouse = true
            houseDirection = houseData.Direction
            houseTemplate = Plot.GetBlueprintFromHouse(house)
        end)

    Create.AtLoc("crate_empty",Loc(0,0,0),function (crateObj)   
        crateObj:SetObjVar("locked",true)
        
        local itemCount = 0     

        local taxBalance = controller:GetObjVar("PlotTaxBalance") or 0
        if(taxBalance > 0) then
            Create.Stack.InContainer( "coin_purse", crateObj, taxBalance, Loc(0,0,0), function (objRef)
                objRef:SetObjVar("RentRefund",true)
            end )
            itemCount = 1
        end

        local plotBounds = controller:GetObjVar("PlotBounds")                

        local plotMarkers = Plot.GetMarkers(controller)            
        local offsetLocX, offsetLocZ
        for i,marker in pairs(plotMarkers) do
            local markerLoc = marker:GetLoc()
            if not(offsetLocX) or markerLoc.X < offsetLocX then
                offsetLocX = markerLoc.X
            end

            if not(offsetLocZ) or markerLoc.Z < offsetLocZ then
                offsetLocZ = markerLoc.Z
            end
        end

        Plot.ForeachHouse(controller, function(plotObject)        
            local packItemLoc = plotObject:GetLoc()
            local relativeLoc = Loc(packItemLoc.X - offsetLocX,packItemLoc.Y,packItemLoc.Z - offsetLocZ)
            plotObject:MoveToContainer(crateObj,relativeLoc)

            local houseDoor = plotObject:GetObjVar("HouseDoor")
            if(houseDoor) then
                local packItemLoc = houseDoor:GetLoc()
                local relativeLoc = Loc(packItemLoc.X - offsetLocX,packItemLoc.Y,packItemLoc.Z - offsetLocZ)
                houseDoor:MoveToContainer(crateObj,relativeLoc)
            end

            local houseSign = plotObject:GetObjVar("HouseSign")
            if(houseSign) then
                local packItemLoc = houseSign:GetLoc()
                local relativeLoc = Loc(packItemLoc.X - offsetLocX,packItemLoc.Y,packItemLoc.Z - offsetLocZ)
                houseSign:MoveToContainer(crateObj,relativeLoc)
            end
        end)

        Plot.ForeachLockdown(controller, function (plotObject)
            if (not(IsInTableArray(plotMarkers,plotObject)) and not(plotObject == controller) and not(plotObject:IsPlayer()) ) then
                local packItemLoc = plotObject:GetLoc()
                local relativeLoc = Loc(packItemLoc.X - offsetLocX,packItemLoc.Y,packItemLoc.Z - offsetLocZ)
                plotObject:MoveToContainer(crateObj,relativeLoc)
                if not( plotObject:HasObjVar("IsPlotObject") ) then
                    -- if its a container count the items inside
                    if(plotObject:IsContainer()) then
                        ForEachItemInContainerRecursive(plotObject, function (contObj)
                            itemCount = itemCount + 1
                            return true
                        end)
                    end
                    itemCount = itemCount + 1
                elseif(plotObject:HasObjVar("IsHouse")) then
                    -- make sure we keep the crate if we have a house in it!
                    itemCount = itemCount + 1
                end
            end
        end, true, 4)

        if(itemCount == 0) then
            crateObj:Destroy()
        end

        Plot.RemoveOwner(controller, function()    
            -- destroy all markers
            Plot.ForeachMarker(controller, function(marker)
                marker:Destroy()
            end)

            Plot.DestroyPlot(controller, function ()
                local objectHeader = {
                    -- plot size
                    PlotSize = Plot.GetSize(controller, plotBounds),
                    -- number of items
                    ItemCount = itemCount
                }
                if(hasHouse) then
                    objectHeader.HouseDirection = houseDirection
                    objectHeader.HouseTemplate = houseTemplate
                end                

                if(itemCount > 0) then
                    local storageKey = plotOwner.."-"..uuid()
                    objectHeader.StorageKey = storageKey

                    local id = uuid()
                    RegisterSingleEventHandler(EventType.SendItemToStorageResult,id,function (success)
                        if not(success) then
                            DebugMessage("PACK ERROR: Failed to send items to storage.")
                        end

                        SetGlobalVar("PackedPlots."..plotOwner,function (record)
                            table.insert(record,objectHeader)
                            return true
                        end,
                        function ( success)
                            if not(success) then
                                DebugMessage("PACK ERROR: Failed to update globalvar PackedPlots.")
                                return
                            end

                            DebugMessage("PACK SUCCESS: "..storageKey..", "..itemCount)
                        end)
                    end)

                    crateObj:SetObjVar("PlotStorage",storageKey)
                    SendItemToStorage(id,storageKey,nil,crateObj)
                else
                    objectHeader.StorageKey = nil
                    SetGlobalVar("PackedPlots."..plotOwner,function (record)
                            table.insert(record,objectHeader)
                            return true
                        end,
                        function ( success)
                            if not(success) then
                                DebugMessage("PACK ERROR: Failed to update globalvar PackedPlots.")
                                return
                            end

                            DebugMessage("PACK SUCCESS: "..plotOwner..", NONE")
                        end)
                end
            end)
        end)        
    end)
end

DoPack(GameObj(1340445936))