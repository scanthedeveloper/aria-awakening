require 'base_ai_npc'

AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = true
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true

function GetPrestigeClass()
    return this:GetObjVar("PrestigeClass") or ""
end
local prestigeClass = GetPrestigeClass()
if ( prestigeClass ) then
    if ( prestigeClass == "Mage" ) then
        AI.QuestStepsInvolvedIn = {
            {"MageProfessionIntro", 1},
            {"MageProfessionIntro", 2},
            {"MageProfessionIntro", 3},
            {"MageProfessionIntro", 5},
            {"MageProfessionIntro", 8},
            {"MageProfessionTierOne", 1},
            {"MageProfessionTierOne", 2},
            {"MageProfessionTierOne", 4},
            {"MageProfessionTierOne", 7},
            {"MageProfessionTierTwo", 1},
            {"MageProfessionTierTwo", 2},
            {"MageProfessionTierTwo", 4},
            {"MageProfessionTierTwo", 6},
            {"MageProfessionTierThree", 1},
            {"MageProfessionTierThree", 2},
            {"MageProfessionTierThree", 10},
            {"MageProfessionTierFour", 1},
            {"MageProfessionTierFour", 2},
        }
    elseif ( prestigeClass == "Fighter" ) then
        AI.QuestStepsInvolvedIn = {
            {"FighterProfessionIntro", 1},
            {"FighterProfessionIntro", 2},
            {"FighterProfessionIntro", 3},
            {"FighterProfessionIntro", 6},
            {"FighterProfessionIntro", 10},
            {"FighterProfessionTierOne", 1},
            {"FighterProfessionTierOne", 2},
            {"FighterProfessionTierOne", 4},
            {"FighterProfessionTierOne", 7},
            {"FighterProfessionTierTwo", 1},
            {"FighterProfessionTierTwo", 2},
            {"FighterProfessionTierTwo", 4},
            {"FighterProfessionTierTwo", 6},
            {"FighterProfessionTierThree", 1},
            {"FighterProfessionTierThree", 2},
            {"FighterProfessionTierThree", 11},
            {"FighterProfessionTierFour", 1},
            {"FighterProfessionTierFour", 2},
        }
    elseif ( prestigeClass == "Rogue" ) then
        AI.QuestStepsInvolvedIn = {
            {"RogueProfessionIntro", 1},
            {"RogueProfessionIntro", 2},
            {"RogueProfessionIntro", 3},
            {"RogueProfessionTierOne", 1},
            {"RogueProfessionTierOne", 2},
            {"RogueProfessionTierOne", 5},
            {"RogueProfessionTierTwo", 1},
            {"RogueProfessionTierTwo", 2},
            {"RogueProfessionTierTwo", 4},
            {"RogueProfessionTierThree", 1},
            {"RogueProfessionTierThree", 2},
            {"RogueProfessionTierThree", 7},
            {"RogueProfessionTierFour", 1},
            {"RogueProfessionTierFour", 2},
            {"RogueProfessionTierFour", 5},
        }
    end
end

function Dialog.OpenGreetingDialog(user)
    local text = nil    
    
    text = "Greetings traveler. Are you here to train in the ways of the "..GetPrestigeDisplayName(GetPrestigeClass()).."?"

    local response = {
        {
            text = "Yes, I'd like to learn some basics.",
            handle = "SkillTrain"
        },
        {
            text = "Goodbye.",
            handle = ""
        }
    }
    
    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenSkillTrainDialog(user)
        SkillTrainer.ShowTrainContextMenu(user)
end