-- These stats are reset and recalculated on login/logout 
-- start with all stats dirty
statsToRecalculate = {
	HealthRegen = true, 
	ManaRegen = true,
	StaminaRegen = true,
	BloodlustRegen = true,
	Strength = true,
	Agility = true,
	Intelligence = true,
	Constitution = true,
	Wisdom = true,
	Will = true,
	Accuracy = true,
	Evasion = true,
	Attack = true,
	Power = true,
	Force = true,
	Defense = true,
	AttackSpeed = true,
	MoveSpeed = true,
	MountMoveSpeed = true,
	MaxWeight = true,
	MaxBloodlust = true,
}

if(IsPlayerCharacter(this)) then
	statsToRecalculate.MaxVitality = true
	statsToRecalculate.VitalityRegen = true
end

function RecalculateHealthRegen()
	-- checks to see if disabled before calculating regen
	if(this:HasObjVar("NoRegen") or IsDead(this)) then
		this:SetObjVar("HealthRegen",0)
		this:SetStatRegenRate("Health",0)
	else
		this:SetStatRegenRate("Health", (ServerSettings.Stats.BaseHealthRegenRate + GetMobileMod(MobileMod.HealthRegenPlus)) *GetMobileMod(MobileMod.HealthRegenTimes, 1) )
	end

	--D*ebugMessage("[base_mobilestats::RecalculateHealthRegen] For "..this:GetName()..": "..healthRegen)
end

function RecalculateManaRegen()

	local manaRegen = this:GetObjVar("ManaRegen")
	if not( manaRegen ) then
		if ( IsPlayerCharacter(this) ) then
			
			local channeling = GetSkillLevel(this, "ChannelingSkill")
			local skillMod = 1
			if ( channeling >= 100 ) then
				skillMod = skillMod + 0.1
			end

			-- get the lowest armor mana regen mod from equipped armor
			local armorModifier = 1 -- start at the max mod possible
			for i,slot in pairs(ARMORSLOTS) do
				local item = this:GetEquippedObject(slot)
				if ( item ) then
					local classRegen = GetArmorClassManaRegenModifier(item, slot)
					if ( classRegen < armorModifier ) then armorModifier = classRegen end
				end
			end

			--DebugMessage("ArmorManaRegenMod: "..armorModifier)
		
			-- double the modifier for active focusing
			if ( HasMobileEffect(this, "Focus") ) then
				armorModifier = armorModifier * 2
			end
			
			manaRegen = (skillMod * (channeling*4/400+GetInt(this)/400) * armorModifier)

			--[[ TODO: Re-enabled per item mana regen bonus
			for i,slot in pairs(ARMORSLOTS) do
				local item = this:GetEquippedObject(slot)
				if ( item ) then
					manaRegen = manaRegen + GetArmorTypeManaRegenModifier(item, slot)
				end
			end
			]]
			--DebugMessage("MANA REGEN RATE: "..manaRegen)
		else
			manaRegen = ServerSettings.Stats.DefaultMobManaRegen
		end
	end

	--DebugMessage("manaRegen: "..manaRegen)

	this:SetStatRegenRate("Mana", (ServerSettings.Stats.BaseManaRegenRate + manaRegen + GetMobileMod(MobileMod.ManaRegenPlus)) * GetMobileMod(MobileMod.ManaRegenTimes, 1) )
end

function RecalculateStaminaRegen()

	local staminaRegen = this:GetObjVar("StaminaRegen")
	if not( staminaRegen ) then
		if ( this:IsPlayer() ) then
			staminaRegen = 0
			-- players regenerate stamina dufferently dependant on the armor worn
			for i,slot in pairs(ARMORSLOTS) do
				local item = this:GetEquippedObject(slot)
				if ( item ) then
					staminaRegen = staminaRegen + GetArmorStaRegenModifier(item, slot)
				end
			end
		else
			staminaRegen = ServerSettings.Stats.DefaultMobStamRegen
		end
	end
	
	this:SetStatRegenRate("Stamina", (ServerSettings.Stats.BaseStaminaRegenRate + staminaRegen + GetMobileMod(MobileMod.StaminaRegenPlus)) * GetMobileMod(MobileMod.StaminaRegenTimes, 1) )
end

function RecalculateBloodlustRegen()
	local bloodlustRegen = this:GetObjVar("BloodlustRegen")
	if not( bloodlustRegen ) then
		if ( this:IsPlayer() ) then
			bloodlustRegen = 0
		else
			bloodlustRegen = ServerSettings.Stats.DefaultMobBloodlustRegen
		end
	end
	
	this:SetStatRegenRate("Bloodlust", (ServerSettings.Stats.BaseBloodlustRegenRate + bloodlustRegen + GetMobileMod(MobileMod.BloodlustRegenPlus)) * GetMobileMod(MobileMod.BloodlustRegenTimes, 1) )
end

function RecalculateVitalityRegen()
	if ( this:HasObjVar("NoRegen") or IsDead(this) ) then
		this:SetObjVar("VitalityRegen",0)
		this:SetStatRegenRate("Vitality",0)
	else
		this:SetStatRegenRate("Vitality", (ServerSettings.Stats.BaseVitalityRegenRate + GetMobileMod(MobileMod.VitalityRegenPlus)) *GetMobileMod(MobileMod.VitalityRegenTimes, 1))
	end
end

function RecalculateMaxHealth()
	local isPlayer = IsPlayerCharacter(this)
	local baseHealth = this:GetObjVar("BaseHealth")
	if(baseHealth == nil) then
		if(isPlayer) then
			baseHealth = ServerSettings.Stats.PlayerBaseHealth
		else
			baseHealth = ServerSettings.Stats.NPCBaseHealth
		end
	end
	
	local strPlus = 0
	if (isPlayer) then
		strPlus = ServerSettings.Stats.HpBonusFunc(GetStr(this))
	end

	local jewelPlus = 0
	for i=1,#JEWELRYSLOTS do
		local item = this:GetEquippedObject(JEWELRYSLOTS[i])
		if ( item ) then
			local jewelryType = item:GetObjVar("JewelryType")
			if ( jewelryType ) then
				local jewelryData = JewelryTypeData[jewelryType]
				if ( jewelryData and jewelryData.Health ) then
					jewelPlus = jewelPlus + jewelryData.Health
				end
			end
		end
	end

	local maxHealth = (baseHealth + strPlus + jewelPlus + GetMobileMod(MobileMod.MaxHealthPlus)) * GetMobileMod(MobileMod.MaxHealthTimes, 1)
	--this:NpcSpeech("MaxHP: "..tostring(maxHealth).." : ("..tostring(baseHealth).."*"..tostring(GetMobileMod(MobileMod.MaxHealthTimes, 1))..")")
	this:SetStatMaxValue("Health", math.floor(maxHealth))
end

function RecalculateMaxMana()
	this:SetStatMaxValue("Mana", math.floor(((GetInt(this)*2.5) + GetMobileMod(MobileMod.MaxManaPlus)) *GetMobileMod(MobileMod.MaxManaTimes, 1)) )
end

function RecalculateMaxStamina()
	this:SetStatMaxValue("Stamina", math.floor(((ServerSettings.Stats.BaseStamina + (GetAgi(this)  * 2) + GetMobileMod(MobileMod.MaxStaminaPlus)) *GetMobileMod(MobileMod.MaxStaminaTimes, 1)) ) )
end

function RecalculateMaxVitality()
	this:SetStatMaxValue("Vitality", math.floor((ServerSettings.Stats.BaseVitality + GetMobileMod(MobileMod.MaxVitalityPlus)) *GetMobileMod(MobileMod.MaxVitalityTimes, 1)) )
end

function RecalculateMaxBloodlust()
	this:SetStatMaxValue("Bloodlust", math.floor((ServerSettings.Stats.BaseBloodlust + GetMobileMod(MobileMod.MaxBloodlustPlus)) *GetMobileMod(MobileMod.MaxBloodlustTimes, 1)) )
end

function RecalculateDerivedStr()
	this:SetStatValue("Str", math.floor(( GetBaseStr(this) + GetMobileMod(MobileMod.StrengthPlus) ) *GetMobileMod(MobileMod.StrengthTimes, 1)) )

	RecalculateMaxHealth()
	RecalculateAttack()
	RecalculateMaxWeight()

	--D*ebugMessage("[base_statmod::RecalculateDerivedStr] For "..this:GetName())
end

function RecalculateDerivedAgi()

	local agi = GetBaseAgi(this)
	local armorType = nil
	local armorClass = nil
	local agiBonus = 0

	if ( IsPlayerCharacter(this) ) then
		local skillDictionary = GetSkillDictionary(this)
		for i,slot in pairs(ARMORSLOTS) do
			local item = this:GetEquippedObject(slot)
			if ( item ) then								
				agiBonus = agiBonus + GetArmorAgiBonus(slot,item)
			else
				armorType = "Natural"
				armorClass = "Cloth"
			end
		end
	end

	agi = math.clamp(agi, ServerSettings.Stats.IndividualStatMin, ServerSettings.Stats.IndividualPlayerStatCap)
	agi = math.floor(( ((agi + GetMobileMod(MobileMod.AgilityPlus)) *GetMobileMod(MobileMod.AgilityTimes, 1))) + agiBonus)

	this:SetStatValue("Agi", agi )

	RecalculateMaxStamina()
	RecalculateAttackSpeed()

	--D*ebugMessage("[base_statmod::RecalculateDerivedAgi] For "..this:GetName())
end

function RecalculateDerivedInt()
	this:SetStatValue("Int", math.floor(( GetBaseInt(this) + GetMobileMod(MobileMod.IntelligencePlus) ) *GetMobileMod(MobileMod.IntelligenceTimes, 1)) )

	RecalculateMaxMana()
	RecalculatePower()
	RecalculateForce()

	--D*ebugMessage("[base_statmod::RecalculateDerivedInt] For "..this:GetName())
end

function RecalculateDerivedCon()
	local conBonus = 0
	for i=1,#JEWELRYSLOTS do
		local item = this:GetEquippedObject(JEWELRYSLOTS[i])
		if ( item ) then
			local jewelryType = item:GetObjVar("JewelryType")
			if ( jewelryType ) then
				local jewelryData = JewelryTypeData[jewelryType]
				if ( jewelryData and jewelryData.Con ) then
					conBonus = conBonus + jewelryData.Con
				end
			end
		end
	end
	this:SetStatValue("Con", math.floor(( GetBaseCon(this) + conBonus + GetMobileMod(MobileMod.ConstitutionPlus) ) *GetMobileMod(MobileMod.ConstitutionTimes, 1)))
	--D*ebugMessage("[base_statmod::RecalculateDerivedCon] For "..this:GetName())
end

--This works as a type of magic resistance.
function RecalculateDerivedWis()
	local wisBonus = 0
	for i=1,#JEWELRYSLOTS do
		local item = this:GetEquippedObject(JEWELRYSLOTS[i])
		if ( item ) then
			local jewelryType = item:GetObjVar("JewelryType")
			if ( jewelryType ) then
				local jewelryData = JewelryTypeData[jewelryType]
				if ( jewelryData and jewelryData.Wis ) then
					wisBonus = wisBonus + jewelryData.Wis
				end
			end
		end
	end
	this:SetStatValue("Wis", math.floor((GetBaseWis(this) + wisBonus + GetMobileMod(MobileMod.WisdomPlus)) *GetMobileMod(MobileMod.WisdomTimes, 1)))
	
	--SCAN ADDED
	local isPlayer = IsPlayerCharacter(this)
	local totalWisdom = GetBaseWis(this)

		--SCAN ADDED Will Stat Bonus
		if ( IsPlayerCharacter(this) ) then
			if(totalWisdom > 10 ) and (totalWisdom < 21 ) then 
				SetMobileMod(this, "PowerHourBuffPlus", "WisdomBonus", .10)
				--this:SystemMessage("[00FF00]Will Bonus Applied - .1%[-] ", "info")
				AddBuffIcon(this, "WisdomBonus", "Wisdom Bonus", "Cold Mastery", "Chance to gain skills increased by 10%")
			end
			if(totalWisdom > 20 ) and (totalWisdom < 31 ) then 
				SetMobileMod(this, "PowerHourBuffPlus", "WisdomBonus", .20)
				--this:SystemMessage("[00FF00]Will Bonus Applied - .2%[-] ", "info")
				AddBuffIcon(this, "WisdomBonus", "Wisdom Bonus", "Cold Mastery", "Chance to gain skills increased by 20%")
			end
			if(totalWisdom > 30 ) and (totalWisdom < 41 ) then 
				SetMobileMod(this, "PowerHourBuffPlus", "WisdomBonus", .30)
				--this:SystemMessage("[00FF00]Will Bonus Applied - .3%[-] ", "info")
				AddBuffIcon(this, "WisdomBonus", "Wisdom Bonus", "Cold Mastery", "Chance to gain skills increased by 30%")
			end
			if(totalWisdom > 40 ) and (totalWisdom < 51 ) then 
				SetMobileMod(this, "PowerHourBuffPlus", "WisdomBonus", .40)
				--this:SystemMessage("[00FF00]Will Bonus Applied - .4%[-] ", "info")
				AddBuffIcon(this, "WisdomBonus", "Wisdom Bonus", "Cold Mastery", "Chance to gain skills increased by 40%")
			end
			if(totalWisdom > 50 ) and (totalWisdom < 61 ) then 
				SetMobileMod(this, "PowerHourBuffPlus", "WisdomBonus", .50)
				--this:SystemMessage("[00FF00]Will Bonus Applied - .5%[-] ", "info")
				AddBuffIcon(this, "WisdomBonus", "Wisdom Bonus", "Cold Mastery", "Chance to gain skills increased by 50%")
			end
		end	
end

--This works as a type of crowd control resistance.
function RecalculateDerivedWill()
	local wilBonus = 0
	for i=1,#JEWELRYSLOTS do
		local item = this:GetEquippedObject(JEWELRYSLOTS[i])
		if ( item ) then
			local jewelryType = item:GetObjVar("JewelryType")
			if ( jewelryType ) then
				local jewelryData = JewelryTypeData[jewelryType]
				if ( jewelryData and jewelryData.Will ) then
					wilBonus = wilBonus + jewelryData.Will
				end
			end
		end
	end

	this:SetStatValue("Will", math.floor((GetBaseWill(this) + wilBonus + GetMobileMod(MobileMod.WillPlus)) *GetMobileMod(MobileMod.WillTimes, 1)))
	--SCAN ADDED
	local isPlayer = IsPlayerCharacter(this)
	local totalWill = GetBaseWill(this)

		--SCAN ADDED Will Stat Bonus
		if ( IsPlayerCharacter(this) ) then
			if(totalWill > 10 ) and (totalWill < 21 ) then 
				SetMobileMod(this, "ChanceToGainEchoHealOnMeleeStruck", "WillBonus", .10)
				--this:SystemMessage("[00FF00]Will Bonus Applied - .1%[-] ", "info")
				AddBuffIcon(this, "WillBonus", "Will Bonus", "Holy Mastery", "Chance to heal when struck, increased by 1%")
			end
			if(totalWill > 20 ) and (totalWill < 31 ) then 
				SetMobileMod(this, "ChanceToGainEchoHealOnMeleeStruck", "WillBonus", .20)
				--this:SystemMessage("[00FF00]Will Bonus Applied - .2%[-] ", "info")
				AddBuffIcon(this, "WillBonus", "Will Bonus", "Holy Mastery", "Chance to heal when struck, increased by 2%")
			end
			if(totalWill > 30 ) and (totalWill < 41 ) then 
				SetMobileMod(this, "ChanceToGainEchoHealOnMeleeStruck", "WillBonus", .30)
				--this:SystemMessage("[00FF00]Will Bonus Applied - .3%[-] ", "info")
				AddBuffIcon(this, "WillBonus", "Will Bonus", "Holy Mastery", "Chance to heal when struck, increased by 3%")
			end
			if(totalWill > 40 ) and (totalWill < 51 ) then 
				SetMobileMod(this, "ChanceToGainEchoHealOnMeleeStruck", "WillBonus", .40)
				--this:SystemMessage("[00FF00]Will Bonus Applied - .4%[-] ", "info")
				AddBuffIcon(this, "WillBonus", "Will Bonus", "Holy Mastery", "Chance to heal when struck, increased by 4%")
			end
			if(totalWill > 50 ) and (totalWill < 61 ) then 
				SetMobileMod(this, "ChanceToGainEchoHealOnMeleeStruck", "WillBonus", .50)
				--this:SystemMessage("[00FF00]Will Bonus Applied - .5%[-] ", "info")
				AddBuffIcon(this, "WillBonus", "Will Bonus", "Holy Mastery", "Chance to heal when struck, increased by 5%")
			end
		end	
end

function RecalculateAccuracy()
	local isPlayer = IsPlayerCharacter(this)

	local weaponAccuracy = EquipmentStats.BaseWeaponClass[_Weapon.RightHand.Class].Accuracy
	local weaponAccuracyBonus = 0
	
	--[[
	-- REMOVE PER JEFFERY ( LL-2051 )
	if ( _Weapon and _Weapon.RightHand.Object ~= nil ) then
		weaponAccuracyBonus = _Weapon.RightHand.Object:GetObjVar("AccuracyBonus") or 0
	end
	]]

	-- We need to limit the maximum accuracy bonus
	weaponAccuracyBonus = math.min(weaponAccuracyBonus, ServerSettings.Combat.MaxLimits.AccuracyPlus)

	--DebugMessage( "weaponAccuracyBonus:" .. weaponAccuracyBonus )

	--local skillAccuracyBonus = GetSkillLevel(this,EquipmentStats.BaseWeaponClass[_Weapon.RightHand.Class].WeaponSkill) + 50
	local skillAccuracyBonus = GetSkillLevel(this,GetPrimaryWeaponSkill(this)) + 50
	if(EquipmentStats.BaseWeaponClass[_Weapon.RightHand.Class].SecondaryWeaponSkill) then
		local secondaryAccuracyBonus = GetSkillLevel(this,EquipmentStats.BaseWeaponClass[_Weapon.RightHand.Class].SecondaryWeaponSkill, skillDictionary) + 50
		skillAccuracyBonus = math.max(skillAccuracyBonus,secondaryAccuracyBonus)
	end

	local accuracy = (weaponAccuracy + weaponAccuracyBonus + skillAccuracyBonus + GetMobileMod(MobileMod.AccuracyPlus)) *GetMobileMod(MobileMod.AccuracyTimes, 1)
	local hitchance = math.min((0 + GetMobileMod(MobileMod.HitChancePlus)) * GetMobileMod(MobileMod.HitChanceTimes, 1), ServerSettings.Combat.MaxLimits.HitChancePlus)

	--DebugMessage("[ACCURACY]["..this:GetName().."]",tostring(accuracy),tostring(baseAccuracy),tostring(weaponAccuracy),tostring(weaponAccuracyBonus),tostring(skillAccuracyBonus),tostring(GetMobileMod(MobileMod.AccuracyTimes, 1)),tostring(GetMobileMod(MobileMod.AccuracyPlus)))
	


	this:SetStatValue("HitChance", math.floor(hitchance))
	this:SetStatValue("Accuracy", math.floor(accuracy))
end

function RecalculateEvasion()
	local evasionPlusMod = GetMobileMod(MobileMod.EvasionPlus)
	local evasionTimesMod = GetMobileMod(MobileMod.EvasionTimes, 1)
	local isPlayer = IsPlayerCharacter(this)

	-- We dont call the GetArmorProficiencyType helper function to avoid iterating through the armor twice
	local evasionArmorBonus = 0
	local canGetLightArmorBonus = isPlayer
	for i,slot in pairs(ARMORSLOTS) do
		local armorClass = nil

		local item = this:GetEquippedObject(slot)		
		if(item) then
			evasionArmorBonus = evasionArmorBonus + (item:GetObjVar("EvasionBonus") or 0)
			if(canGetLightArmorBonus) then
				armorClass = GetArmorClass(item)
			end
		end

		if(armorClass ~= "Light") then
			canGetLightArmorBonus = false
		end
	end



	local skillDictionary = GetSkillDictionary(this)

	local lightArmorEvasionBonus = 0
	
	--[[
	if(isPlayer and canGetLightArmorBonus) then
		lightArmorEvasionBonus = GetSkillLevel(this,"LightArmorSkill", skillDictionary) * ServerSettings.Stats.LightArmorEvasionBonus
	end
	]]

	local skillEvasionBonus = GetSkillLevel(this,GetPrimaryWeaponSkill(this), skillDictionary) + 50
	if(EquipmentStats.BaseWeaponClass[_Weapon.RightHand.Class].SecondaryWeaponSkill) then
		local secondaryEvasionBonus = GetSkillLevel(this,EquipmentStats.BaseWeaponClass[_Weapon.RightHand.Class].SecondaryWeaponSkill, skillDictionary) + 50
		skillEvasionBonus = math.max(skillEvasionBonus,secondaryEvasionBonus)
	end

	--DebugMessage( "skillEvasionBonus: " .. skillEvasionBonus )

	local shieldEvasionPenalty = 0
	local thisShieldClass = nil
	if(_Weapon and _Weapon.LeftHand.Object) then
		thisShieldClass = GetShieldType(_Weapon.LeftHand.Object)
	end

	if(thisShieldClass ~= nil) then
		shieldEvasionPenalty = ServerSettings.Stats.ShieldEvasionPenalty
	end

    local baseEvasion = evasionArmorBonus+skillEvasionBonus-shieldEvasionPenalty+lightArmorEvasionBonus
	local evasion = (baseEvasion + evasionPlusMod) *evasionTimesMod
	--DebugMessage( "baseEvasion:" .. baseEvasion )
	--DebugMessage( "evasion:" .. evasion )
	evasion = math.min(evasion, (baseEvasion + ServerSettings.Combat.MaxLimits.EvasionTimes))
	--DebugMessage( "evasion final:" .. evasion )

	local defensechance = math.min((0 + GetMobileMod(MobileMod.DefenseChancePlus)) * GetMobileMod(MobileMod.DefenseChanceTimes, 1), ServerSettings.Combat.MaxLimits.DefenseChancePlus)


	--DebugMessage("[EVASION]["..this:GetName().."]",tostring(evasion),tostring(baseEvasion),tostring(evasionArmorBonus),tostring(skillEvasionBonus),tostring(shieldEvasionPenalty),tostring(evasionTimesMod),tostring(evasionPlusMod))
	this:SetStatValue("DefenseChance",math.floor(defensechance))   
	this:SetStatValue("Evasion",math.floor(evasion))
end

function RecalculateMaxWeight()
	local backpack = this:GetEquippedObject("Backpack")
	if ( backpack ~= nil ) then
		local base = ServerSettings.Misc.BackpackBaseWeightLimit
		if ( IsPlayerCharacter(this) ) then
			local weight = (base + GetStr(this) * 4 + GetMobileMod(MobileMod.MaxWeightPlus)) * GetMobileMod(MobileMod.MaxWeightTimes, 1)
			backpack:SetSharedObjectProperty("MaxWeight", weight)
		else
			if ( backpack:HasObjectTag("HorseBackpack") ) then
				backpack:SetSharedObjectProperty("MaxWeight", base * 2)
			end
		end
	end
end

function RecalculateAttack()
	local isPlayer = IsPlayerCharacter(this)

	--start with attack from objvar if it exists
	local attack = 0
	if(isPlayer) then
		attack = this:GetObjVar("Attack") or 0
        --if unarmed, use brawling attack
        if not (_Weapon and _Weapon.RightHand.Object) then
            attack = 10 + ( (GetSkillLevel(this, "BrawlingSkill") or 0) / 100 ) * 6
        --start with equipped weapon's base attack for that weapon TYPE if available
        else
            attack = EquipmentStats.BaseWeaponStats[_Weapon.RightHand.Type].Attack or 0
        end
        --adjust attack for stats, skills, weapon attack bonus, mobile mod
        local itemAttackModifier = math.min(GetMobileMod(MobileMod.AttackBonus)*100 + (_Weapon.RightHand.AttackBonus or 0), ServerSettings.Combat.MaxLimits.AttackBonus) / 100
        local strModifier = ServerSettings.Stats.StrModifierFunc(GetStr(this))
        local vigorBonus = (GetSkillLevel(this, "MeleeSkill") or 0) / 100
        local martialBonus = (GetSkillLevel(this, "MartialProwessSkill") or 0) / 100
        attack = attack * (strModifier + vigorBonus + martialBonus + itemAttackModifier)
    else
		attack = this:GetObjVar("Attack") or ServerSettings.Stats.DefaultMobAttack or 0
    end

	attack = math.floor((attack + GetMobileMod(MobileMod.AttackPlus)) * GetMobileMod(MobileMod.AttackTimes, 1))
	this:SetStatValue("Attack", attack)
end

-- power is magic cast damage modifier
function RecalculatePower()
	local power = this:GetObjVar("Power") -- for mobs or overrides on players
	local powerBonus = 0
	local isPlayer = IsPlayerCharacter(this)
	if not( power ) then
		if ( isPlayer ) then
			power = _Weapon.RightHand.Power or 0
			--powerBonus = GetMagicItemDamageBonus(_Weapon.RightHand.Object)
		else
			power = ServerSettings.Stats.DefaultMobPower
		end
	end

	if ( isPlayer ) then
		local affinityBonus = math.max( ((GetSkillLevel(this, "MagicAffinitySkill") or 0) / 4), ((GetSkillLevel(this, "InscriptionSkill") or 0) / 10) )
		if affinityBonus < 1 then
			affinityBonus = 1
		end
		--local inscriptionBonus = (GetSkillLevel(this, "InscriptionSkill") or 0) / 20
		power = ( power + powerBonus + affinityBonus ) * ServerSettings.Stats.IntMod(GetInt(this))
	end

	--this:NpcSpeech("power: "..power)
	--this:NpcSpeech("powerBonus: "..powerBonus)
	--this:NpcSpeech("channelingBonus: "..channelingBonus)
	--this:NpcSpeech("intmod: "..ServerSettings.Stats.IntMod(GetInt(this)))

	-- Spell PowerPlus need to be capped to the MaxLimits value
	local powerPlus = math.min( GetMobileMod(MobileMod.PowerPlus), ServerSettings.Combat.MaxLimits.PowerPlus )
	--DebugMessage("PowerPlus", powerPlus)


	this:SetStatValue("Power", math.floor((power + powerPlus) * GetMobileMod(MobileMod.PowerTimes, 1)))

end

-- force is magic cast beneficial plus
function RecalculateForce()
	local force = this:GetObjVar("Force") -- for mobs or overrides on players
	local forceBonus = 0
	local isPlayer = IsPlayerCharacter(this)
	if not( force ) then
		if ( isPlayer ) then
			force = EquipmentStats.BaseWeaponStats[_Weapon.RightHand.Type].Force or 0
			--forceBonus = GetMagicItemDamageBonus(_Weapon.RightHand.Object) --TODO for when we implement weapon bonuses
		else
			force = ServerSettings.Stats.DefaultMobForce
		end
	end

	if ( isPlayer ) then
		force = ( force + forceBonus) * ServerSettings.Stats.IntMod(GetInt(this))
	end

	this:SetStatValue("Force", math.floor((force + GetMobileMod(MobileMod.ForcePlus)) * GetMobileMod(MobileMod.ForceTimes, 1)))
end

function RecalculateDefense()
	local isPlayer = IsPlayerCharacter(this)

	local thisArmorRating = 0
	local thisArmorBonus = 0
	local armorType = nil
	local armorClass = nil

	if ( isPlayer ) then
		local skillDictionary = GetSkillDictionary(this)
		for i,slot in pairs(ARMORSLOTS) do
			local item = this:GetEquippedObject(slot)
			if ( item ) then
				armorType = GetArmorType(item)
				armorClass = GetArmorClassFromType(armorType)
				thisArmorBonus = thisArmorBonus + ( item:GetObjVar("ArmorBonus") or 0 )
			else
				armorType = "Natural"
				armorClass = "Cloth"
			end

			local armorRating = EquipmentStats.BaseArmorStats[armorType][slot].ArmorRating
			armorRating = (EquipmentStats.BaseArmorStats.Natural[slot].ArmorRating + (((armorRating) - EquipmentStats.BaseArmorStats.Natural[slot].ArmorRating))) -- difference between the rating and lowest possible rating
			thisArmorRating = thisArmorRating + armorRating
		end		
	else
		thisArmorRating = this:GetObjVar("Armor") or ServerSettings.Stats.DefaultMobArmorRating
	end

	--SCAN ADDED HEAVY ARMOR BONUS +10 Defense
	if( armorClass == "Heavy" )  then
	    thisArmorRating = thisArmorRating + 10
		AddBuffIcon(this, "HeavyArmorBonus", "Heavy Armor Bonus", "Lightning Mastery", "+10 Defense")
		RemoveBuffIcon(this, "LightArmorBonus")
		RemoveBuffIcon(this, "ClothArmorBonus")
	end

	--SCAN ADDED LIGHT ARMOR BONUS +10 Agility
	if( armorClass == "Light" )  then
		local thisAgi = GetAgi(this) + 10
		this:SetStatValue("Agi", thisAgi )
		AddBuffIcon(this, "LightArmorBonus", "Light Armor Bonus", "Lightning Mastery", "+10 Agility")
		RemoveBuffIcon(this, "HeavyArmorBonus")
		RemoveBuffIcon(this, "ClothArmorBonus")
	end

	--SCAN, ADDED CLOTH ARMOR BONUS +10 Intelligence
	if( armorClass == "Cloth" )  then
		local thisInt = GetInt(this) + 10
		this:SetStatValue("Int", thisInt )
		AddBuffIcon(this, "ClothArmorBonus", "Cloth Armor Bonus", "Lightning Mastery", "+10 Intelligence")
		RemoveBuffIcon(this, "HeavyArmorBonus")
		RemoveBuffIcon(this, "LightArmorBonus")
	end
 
	--Defense = (4+Armor Def. * Buff 1)+Buff 2+Armor Bonus
	local defense = thisArmorRating + thisArmorBonus
	defense = math.floor( (defense + GetMobileMod(MobileMod.DefensePlus)) * GetMobileMod(MobileMod.DefenseTimes, 1) )
	this:SetStatValue("Defense", math.max(defense, 35))
end

function RecalculateAttackSpeed()
	local isPlayer = IsPlayerCharacter(this)

	local speed = this:GetObjVar("AttackSpeed")
	local speedMod = 0
	local attackSpeedPlusMod = GetMobileMod(MobileMod.AttackSpeedPlus)
	local attackSpeedTimesMod = 1 - (GetMobileMod(MobileMod.AttackSpeedTimes, 0) / 100) -- we are reducing speed
	--attackSpeedTimesMod = math.max(attackSpeedTimesMod, ServerSettings.Combat.MaxLimits.SwingSpeedReduction)
	local bowSpeedModifier = nil
	if not(speed) then
		if(isPlayer) then
			speed = _Weapon.RightHand.Speed or 5
			--speedMod = GetMagicItemSpeedModifier(_Weapon.RightHand.Object)
		else
			-- DAB COMBAT CHANGES: MAKE THIS CONFIGURABLE
			speed = ServerSettings.Stats.DefaultMobWeaponSpeed
    	end
    end

	local lightArmorAttackSpeedMultiplier = 1
	
	--[[
	if(isPlayer) then
		if(GetArmorProficiencyType(this) == "Light") then
			lightArmorAttackSpeedMultiplier = 1 + (GetSkillLevel(this,"LightArmorSkill") * ServerSettings.Stats.LightArmorAttackSpeedMultiplier)
		end
	end
	]]

	local baseAttackSpeed = ((speed * 4) - math.floor(GetMaxStamina(this)/30)) * (100/(100 + speedMod))
	--DebugMessage("GetAgi(this)" .. math.floor(GetMaxStamina(this))/30)
	
	local attacksPerSecond = 1.25
	
	-- weapons with a bow speed modifier have a different speed calculation
	--if(bowSpeedModifier) then
	--	attacksPerSecond = (baseAttackSpeed/1000) * (1500/bowSpeedModifier)
	--else
		--DebugMessage( "AttackTimes: " .. attackSpeedTimesMod )
		attacksPerSecond = ((baseAttackSpeed/4) - attackSpeedPlusMod) * attackSpeedTimesMod
	--end

	--DebugMessage("GetSwingSpeed",tostring(1/(baseAttackSpeed/500)),tostring(ServerSettings.Stats.AgiModifierFunc(GetAgi(this))),tostring(speed),tostring(GetMagicItemSpeedModifier(weapon)),tostring(lightArmorAttackSpeedMultiplier))
	--local attackSpeedSecs = attacksPerSecond

	-- Our maximum attack speed reduction is limited
	attacksPerSecond = math.max(attacksPerSecond, ((baseAttackSpeed/4) * ServerSettings.Combat.MaxLimits.AttackSpeedTimes)  )

	-- Our minimum attack speed is limited
	attacksPerSecond = math.max( attacksPerSecond, ServerSettings.Combat.MaxLimits.AttackSpeed )

	this:SetStatValue("AttackSpeed",math.round(attacksPerSecond,2))
end

function RecalculateMoveSpeed()
    if ( IsMounted(this) ) then
      	this:SetBaseMoveSpeed(math.max(0.1, (((ServerSettings.Stats.MountMoveSpeed or ServerSettings.Stats.BaseMoveSpeed) + GetMobileMod(MobileMod.MountMoveSpeedPlus))) * GetMobileMod(MobileMod.MountMoveSpeedTimes, 1)))
    else
		if ( IsPlayerCharacter(this) ) then
			this:SetBaseMoveSpeed(math.max(0.1, (ServerSettings.Stats.BaseMoveSpeed + GetMobileMod(MobileMod.MoveSpeedPlus)) * GetMobileMod(MobileMod.MoveSpeedTimes, 1)))
		end
	end
end

recalcFuncs = 
{
	{Name="Strength", Func=RecalculateDerivedStr},
	{Name="Intelligence", Func=RecalculateDerivedInt},
	{Name="Agility", Func=RecalculateDerivedAgi},
	{Name="Constitution", Func=RecalculateDerivedCon},
	{Name="Wisdom", Func=RecalculateDerivedWis},
	{Name="Will", Func=RecalculateDerivedWill},
	{Name="HealthRegen", Func=RecalculateHealthRegen},
	{Name="ManaRegen", Func=RecalculateManaRegen},
	{Name="StaminaRegen", Func=RecalculateStaminaRegen},
	{Name="BloodlustRegen", Func=RecalculateBloodlustRegen},
	{Name="VitalityRegen", Func=RecalculateVitalityRegen},
	{Name="Accuracy", Func=RecalculateAccuracy},
	{Name="Evasion", Func=RecalculateEvasion},
	{Name="Attack", Func=RecalculateAttack},
	{Name="Power", Func=RecalculatePower},
	{Name="Force", Func=RecalculateForce},
	{Name="Defense", Func=RecalculateDefense},
	{Name="AttackSpeed", Func=RecalculateAttackSpeed},
	{Name="MoveSpeed", Func=RecalculateMoveSpeed},
	{Name="MountMoveSpeed", Func=RecalculateMoveSpeed},
	{Name="MaxHealth", Func=RecalculateMaxHealth},
	{Name="MaxVitality", Func=RecalculateMaxVitality},
	{Name="MaxBloodlust", Func=RecalculateMaxBloodlust},
	{Name="MaxStamina", Func=RecalculateMaxStamina},
	{Name="MaxMana", Func=RecalculateMaxMana},
	{Name="MaxWeight", Func=RecalculateMaxWeight},
}

-- this is the delayed function intended to reduced duplicate calls to recalc functions
function DoRecalculateStats()
	-- these functions must be called in order
	-- as some derived stats depend on others
	for key,value in pairs(recalcFuncs) do
		--DebugMessage("CHECKING",tostring(value.Name),tostring(statsToRecalculate[value.Name]))
		if( statsToRecalculate[value.Name] == true ) then			
			value.Func()
		end
	end

	--this:SendMessage("UpdateCharacterWindow")

	statsToRecalculate = {}
end

-- this is the message handler that marks derived stats as dirty to be recalculated
function MarkStatsDirty(recalculateStatsDict)
	--DebugMessage("DIRTY",DumpTable(recalculateStatsDict))
	for key, value in pairs(recalculateStatsDict) do		
		statsToRecalculate[key] = true
	end

	-- schedule client update
	-- 100ms buffer to reduce spam
	this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(100),"DoRecalculateStats")
end

-- Recalculate on load
this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(100),"DoRecalculateStats")

RegisterEventHandler(EventType.Timer, "DoRecalculateStats", DoRecalculateStats)
RegisterEventHandler(EventType.Message, "RecalculateStats", MarkStatsDirty)

-- This is to be included by mobiles for the ability to add and remove stat modifiers

-- this defines the derived stats that need to be recalculated when this skill changes
skillGainEffects = {
	MagicAffinitySkill = { ManaRegen = true, Power = true },
	LightArmorSkill = { Defense = true, },
	HeavyArmorSkill = { Defense = true, Agility = true, },
	SlashingSkill = { Evasion = true, Accuracy = true },
	PiercingSkill = { Evasion = true, Accuracy = true },
	BashingSkill = { Evasion = true, Accuracy = true },
	BrawlingSkill = { Evasion = true, Accuracy = true },
	ArcherySkill = { Evasion = true, Accuracy = true },
	LancingSkill = { Evasion = true, Accuracy = true },
	ChannelingSkill = { Power = true },
	RestorationSkill = { Force = true },
}

-- Args
--     statModName: the name of the stat to modify (right now we old support regen modifiers)
--     statModIdentifier: the identifier of this particular mod (so it can be removed later)
--     statModType: the type of modifier (bonus or multiplier)
--     statModValue: the actual modifier value
function HandleAddStatMod(statModName,statModIdentifier,statModType,statModValue,statModTime)
	LuaDebugCallStack("[DEPRECIATED] AddStatMod has been replaced with SetMobileMod.")
end

-- Args
--     statModName: the name of the stat to remove (right now we old support regen modifiers)
--     statModIdentifier: the identifier the mod to remove
function HandleRemoveStatMod(statModName, statModIdentifier)
	LuaDebugCallStack("[DEPRECIATED] AddStatMod has been replaced with SetMobileMod.")
end

function StatsHandleEquipmentChanged(item)
	local itemSlot = GetEquipSlot(item)

	local dirtyTable = {}

	if( itemSlot == "RightHand" ) then
		dirtyTable = {Accuracy=true,Evasion=true,Attack=true,AttackSpeed=true,Force=true,Power=true}
	elseif( itemSlot == "LeftHand" and GetShieldType(item) ) then
		dirtyTable.Evasion = true
	elseif( IsArmorSlot(itemSlot) ) then
		dirtyTable = {Agility=true, Defense=true, ManaRegen=true, StaminaRegen=true}
		-- this would be better to figure out a client side handle for the (+/-#) next to stats in paperdoll.
		if ( GetArmorClass(item) == "Heavy" ) then
			CallFunctionDelayed(TimeSpan.FromMilliseconds(250), function()
				this:SendMessage("UpdateCharacterWindow")
			end)
		end
	elseif (IsJewelrySlot(itemSlot)) then
			dirtyTable = {Will=true, Constitution=true, Wisdom=true}
	end

	MarkStatsDirty(dirtyTable)
end

function StatsHandleSkillChanged(skillName)
	if(skillGainEffects[skillName]) then
		MarkStatsDirty(skillGainEffects[skillName])
	end
end

RegisterEventHandler(EventType.Message, "AddStatMod", HandleAddStatMod)
RegisterEventHandler(EventType.Message, "RemoveStatMod", HandleRemoveStatMod)

RegisterEventHandler(EventType.Message, "OnSkillLevelChanged", StatsHandleSkillChanged)