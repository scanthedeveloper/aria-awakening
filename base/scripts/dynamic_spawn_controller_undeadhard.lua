require 'dynamic_spawn_controller'
mDynamicSpawnKey = "DynamicUndeadHardSpawner"

mDynamicHUDVars.Title = "Restless Spirits"
mDynamicHUDVars.Description = "Defeat waves of undead and the Enraged Skeleton."


local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        msg = "The Undead have been disturbed."
    elseif( progress == 2 ) then 
        msg = "The dead grow more agitated."
    elseif( progress == 3 ) then 
        msg = "Your feel the air around you chill."
    elseif( progress == 4 ) then 
        msg = "A screech pierces the air. Doom approaches."
    elseif( progress == 999 ) then 
        msg = "The undead event has timed out."
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end