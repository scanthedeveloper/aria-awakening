_PlayersNearby = {}

RegisterEventHandler(EventType.LoadedFromBackup,"",function()StartFence()end)
RegisterEventHandler(EventType.Message,"StartEvent",function()StartFence()end)
RegisterEventHandler(EventType.Message,"StopEvent",function()StopFence()end)
RegisterEventHandler(EventType.EnterView, "MobileNearbyEvent", function(mobileObj)
	AttachEventModule(mobileObj)
end)
RegisterEventHandler(EventType.LeaveView, "MobileNearbyEvent", function(mobileObj)
    -- remove the player from the list
    for i,player in pairs(_PlayersNearby) do
        if ( player == mobileObj ) then
            _PlayersNearby[i] = nil
        end
    end
end)

function StartFence()
    local active = this:GetObjVar("Active")
    if ( active and active == 1 ) then
        local radius = this:GetObjVar("Radius") or this:GetObjVar("spawnRadius") or 10
        AddView("MobileNearbyEvent", SearchPlayerInRange(radius, true), 1.0)
    end
end

function StopFence()
    DelView("MobileNearbyEvent")
end

function AttachEventModule(mobileObj)
    table.insert(_PlayersNearby, mobileObj)
    if ( mobileObj:IsValid() and not mobileObj:HasModule("militia_event_score_ui") ) then
        mobileObj:AddModule("militia_event_score_ui", {Controller = this})
    end
end