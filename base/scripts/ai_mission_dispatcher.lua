require 'base_ai_npc'

AI.Settings.EnableMissions = true

DEFAULT_AVAILABLE_MISSIONS = 3
MAX_MISSIONS = 3

mCurLoc = nil
mSelectedCategory = {}
mSelectedMissions = {}
--Prevents user from accepting same mission multiple times
mHasPickedMission = false

defaultText = "Looking for some work? I take jobs from all over and offer them to freelancers like yourself."
campRegion = GetRegion("DynamicCamp")
if (initializer) then
	this:SetObjVar("MissionCategories", initializer.MissionCategories)
end

AI.TooColdMessages = 
{
	"This outta be a cakewalk for you.",
	"This one shouldn't give you much trouble. I'd like to see you try and mess this one up."

}

AI.TooHotMessages = 
{
	"Take this one with caution friend, we've lost a lot of good mercs to this one.",
	"I'm not so sure you're up to this task, but we're running low on bodies to throw at this one.",
	"This one? Hey, it's your funeral pal."
}

AI.JustRightMessages =
{
	"I think this one will suit you.",
	""
}

function IntroDialog(user)
    Dialog.OpenGreetingDialog(user)
end

function Dialog.OpenGreetingDialog(user)
	Dialog.OpenMissionCategorySelectDialog(user)
  	--Dialog.OpenListMissionsDialog(user)
end

function Dialog.OpenMissionCategorySelectDialog(user)
	local missionCategories = this:GetObjVar("MissionCategories")

	if (#missionCategories == 1) then
		mSelectedCategory[user] = missionCategories[1]
		Dialog.OpenListMissionsDialog(user, missionCategories[1])
		return
	end

	text = defaultText

	response = {}
	for i, j in pairs(missionCategories) do
			--response[i] = "Category|"..missionCategories[i]
			local canAddCategory = true

			if (missionCategories[i] == "Fishing") then
				--Can't give fishing missions if there's no water
				if not(GetRegion("Water")) then
					canAddCategory = false
				end
			end

			if (canAddCategory) then
		    	response[i] = {}
			    response[i].text = MissionData.MissionCategories[missionCategories[i]].DisplayName
			    response[i].handle = tostring("MissionCategory|"..missionCategories[i])
			end
	end

	local index = #missionCategories + 1
	response[index] = {}
    response[index].text = "Goodbye"
    response[index].handle = ""

	NPCInteractionLongButton(text, this, user, "Responses", response)
end

function Dialog.OpenListMissionsDialog(user, missionCategory)

	mHasPickedMission = false

	local userDifficulty = GetUserDifficulty(user, missionCategory)
	local missionKeys = PickMissions(userDifficulty, missionCategory)
	mSelectedMissions[user] = {}
	for i, j in pairs(missionKeys) do
		local loc, waterLoc = PickMissionSpawnLoc(j)
		if(loc) then
			mSelectedMissions[user][i] = {Key = j, Loc = loc}
			if (waterLoc) then
				mSelectedMissions[user][i].WaterLoc = waterLoc
			end
		end
	end

	if (missionCategory == "Taming") then
		text = "Consider yourself pretty savvy with animals? Maybe you can help me out with some odd jobs."
	else
		text = "Looking for some work? I take jobs from all over and offer them to freelancers like yourself."
	end
    response = {}
    local responseIndex = 1

    local userMissions = user:GetObjVar("Missions") or {}
	if (#userMissions >= MAX_MISSIONS) then
		text = "I'm not going to give you any more leads until you tie up your loose ends. Go complete the jobs, and I'll have something when you come back."

		response[responseIndex] = {}
	    response[responseIndex].text = "Okay"
	    response[responseIndex].handle = ""
    	responseIndex = responseIndex + 1
	else

		if (mSelectedMissions[user] ~= nil) then
		    for i, j in pairs(mSelectedMissions[user]) do
		    	local missionTable = GetMissionTable(mSelectedMissions[user][i].Key)
		    	local descriptionText = missionTable.Title
		    	response[responseIndex] = {}
			    response[responseIndex].text = tostring(descriptionText).." at "..GetRegionalName(mSelectedMissions[user][i].Loc) or ServerSettings.SubregionName or ServerSettings.WorldName
			    response[responseIndex].handle = tostring("MissionKey|"..i)
		    	responseIndex = responseIndex + 1
		    end
		else
    		text = "I don't have any missions for you pal. Sorry."
		end

		local missionCategories = this:GetObjVar("MissionCategories")
		if (missionCategories and #missionCategories > 1) then
		    responseIndex = responseIndex + 1
		    response[responseIndex] = {}
		    response[responseIndex].text = "Actually..."
		    response[responseIndex].handle = "Back"
		else
		    responseIndex = responseIndex + 1
		    response[responseIndex] = {}
		    response[responseIndex].text = "Goodbye"
		    response[responseIndex].handle = ""
		end
	end

    NPCInteractionLongButton(text,this,user,"Responses",response)
    GetAttention(user)
end

function OpenMissionDetailsDialog(user, selectedMissionIndex)
	if not(user) or not(selectedMissionIndex) or not(mSelectedMissions[user]) or not(mSelectedMissions[user][selectedMissionIndex]) then
		DebugMessage("ERROR: Invalid arguments to OpenMissionDetailsDialog",tostring(user),tostring(selectedMissionIndex))
		return
	end

	local missionTable = GetMissionTable(mSelectedMissions[user][selectedMissionIndex].Key)
	local descriptionText = missionTable.Description
	local difficultyText = GetDifficultyDescription(user, missionTable.Difficulty, missionTable.MissionCategory)
	local directionDescription = GetDirectionDescription(mSelectedMissions[user][selectedMissionIndex].Loc) or ""
	text = difficultyText.." "..descriptionText.." "..directionDescription

	response = {}

	response[1] = {}
	response[1].text = "Accept"
	response[1].handle = "Accept|"..selectedMissionIndex.."|"..uuid()

	response[2] = {}
	response[2].text = "Decline"
	response[2].handle = "MissionCategory|"..mSelectedCategory[user]

	NPCInteractionLongButton(text,this,user,"Responses",response)
    GetAttention(user)
end

OverrideEventHandler("base_ai_npc", EventType.DynamicWindowResponse,"Responses", 
	function (user,buttonId)
		if ( Dialog["Open"..buttonId.."Dialog"] ~= nil) then 
        	Dialog["Open"..buttonId.."Dialog"](user)
		elseif (buttonId:match("MissionKey")) then
			local args = StringSplit(buttonId, "|")
			local missionIndex = args[2]
			if (user:IsValid()) then
				OpenMissionDetailsDialog(user, tonumber(missionIndex))
			end
		elseif(buttonId:match("Accept")) then
			local args = StringSplit(buttonId, "|")
			local missionIndex = tonumber(args[2])
			local acceptUUID = args[3]
			local lastAcceptUUID = user:GetObjVar("LastMissionAccepted") or ""
			-- If they try to accept the same mission more than once, we ignore the duplicate attempts
			--DebugMessage(lastAcceptUUID .."  :: " .. acceptUUID)
			if( lastAcceptUUID ~= acceptUUID ) then
				user:SetObjVar("LastMissionAccepted", acceptUUID)
				local missions = user:GetObjVar("Missions") or {}
				if (#missions < MAX_MISSIONS and not mHasPickedMission) then
					AddMission(user, mSelectedMissions[user][missionIndex].Key, mSelectedMissions[user][missionIndex].Loc, mSelectedMissions[user][missionIndex].WaterLoc or nil)
					mHasPickedMission = false
				end
			end
			user:CloseDynamicWindow("Responses")
		elseif (buttonId:match("MissionCategory")) then
			local args = StringSplit(buttonId, "|")
			mSelectedCategory[user] = args[2]
			Dialog.OpenListMissionsDialog(user, mSelectedCategory[user])
		elseif(buttonId == "Back") then
			Dialog.OpenMissionCategorySelectDialog(user)
		elseif(buttonId == "") then
			user:CloseDynamicWindow("Responses")
		end
		--user:CloseDynamicWindow("Responses")
	end)


function PickMissions(difficulty, missionCategory)
	local missionKeys = {}
	local availableMissions = {}
	local missionTable = GetSubregionMissions()
	local curDifficulty = difficulty

	while (missionKeys == nil or #missionKeys == 0 and curDifficulty >= 1) do
		--Get universal missions
		for i, j in pairs(MissionData.Missions.AllSubregions) do
			local difficultyCheck = false;
			if( missionCategory == "Taming" or missionCategory == "Combat" ) then
				difficultyCheck = ( j.Difficulty >= (curDifficulty -1) ) and j.Difficulty <= curDifficulty
			else
				difficultyCheck = ( j.Difficulty <= curDifficulty )
			end


			if (difficultyCheck and missionCategory == j.MissionCategory) then
				--DebugTable( j )
				table.insert(missionKeys, i)
			end
		end

		local tableCount = 0
		for i, j in pairs(missionTable) do
			tableCount = tableCount + 1
		end

		if (missionTable ~= nil or tableCount > 0) then
			--Get subregion specific missions
			for i, j in pairs(missionTable) do
				if (difficultyCheck and missionCategory == j.MissionCategory) then
					table.insert(missionKeys, i)
				end
			end
		end

		curDifficulty = curDifficulty - 1
	end

	for i = DEFAULT_AVAILABLE_MISSIONS, 1, -1 do
		local key = missionKeys[math.random(1, #missionKeys)]
		availableMissions[i] = key
	end
	return availableMissions
end

function GetUserDifficulty(user, missionCategory)
	local weaponSkill, weaponLevel = GetHighestWeaponSkill(user)

	local levels = {}
	if (missionCategory == "Combat") then
		levels = 
		{
			weaponLevel,
			GetSkillLevel(user, "MagerySkill"),
			GetSkillLevel(user, "AnimalTamingSkill"),
		}

	elseif (missionCategory == "Taming") then
		levels = 
		{
			(GetSkillLevel(user, "AnimalTamingSkill") + GetSkillLevel(user, "AnimalLoreSkill"))/2,
		}
	elseif (missionCategory == "HarvestLumberjack") then
		levels =
		{
			GetSkillLevel(user, "LumberjackSkill"),
		}
	elseif (missionCategory == "HarvestMining") then
		levels =
		{
			GetSkillLevel(user, "MiningSkill"),
		}
	elseif (missionCategory == "HarvestFabrication") then
		levels =
		{
			GetSkillLevel(user, "FabricationSkill"),
		}
	elseif(missionCategory == "Fishing") then
		levels =
		{
			GetSkillLevel(user, "FishingSkill"),
		}
	end
	local curHighestSkillLevel = levels[1]
	for i = 2, #levels do
		if ( levels[i] > curHighestSkillLevel) then
			curHighestSkillLevel = levels[i]
		end
	end

	local score = curHighestSkillLevel
	
	local difficulty = 1
	for lev,minSkill in pairs(MissionData.MissionCategories[missionCategory].DifficultyLevels) do		
		if(score < minSkill) then
			break
		end
		difficulty = lev
	end

	--DebugMessage(missionCategory .. " : " .. score .. " : " .. difficulty)

	return difficulty
end

function GetMaxDifficulty()
	local currDifficulty = 1
	for i, j in pairs(GetSubregionMissions()) do
		if (j.Difficulty >= currDifficulty) then
			currDifficulty = j.Difficulty
		end
	end

	for i, j in pairs(MissionData.Missions.AllSubregions) do
		if (j.Difficulty >= currDifficulty) then
			currDifficulty = j.Difficulty
		end
	end

	return currDifficulty
end

function GetDifficultyDescription(user, missionDifficulty, missionCategory)
	local userDifficulty = GetUserDifficulty(user, missionCategory)
	
	--TooCold
	if (userDifficulty > missionDifficulty) then
		return AI.TooColdMessages[math.random(1, #AI.TooColdMessages)]
	--TooHot
	elseif(userDifficulty < missionDifficulty) then
		return AI.TooHotMessages[math.random(1, #AI.TooHotMessages)]
	--JustRight
	elseif(userDifficulty == missionDifficulty) then
		return AI.JustRightMessages[math.random(1, #AI.JustRightMessages)]
	end
end

function GetDirectionDescription(spawnLoc)

	local destRegionalName = GetRegionalName(spawnLoc) or ServerSettings.SubregionName or ServerSettings.WorldName

	local angleTo = this:GetLoc():YAngleTo(spawnLoc)

		local direction = nil
		if( angleTo > 337 or angleTo <= 22 ) then
			direction = "North"
		elseif( angleTo > 22 and angleTo <= 67 ) then
			direction = "Northeast"
		elseif( angleTo > 67 and angleTo <= 112 ) then
			direction = "East"	
		elseif( angleTo > 112 and angleTo <= 157 ) then
			direction = "Southeast"			
		elseif( angleTo > 157 and angleTo <= 202 ) then
			direction = "South"
		elseif( angleTo > 202 and angleTo <= 247 ) then
			direction = "Southwest"
		elseif( angleTo > 247 and angleTo <= 292 ) then
			direction = "West"
		else
			direction = "Northwest"
		end

		if (destRegionalName == nil) then
			return "Head "..direction
		end

	return "Head "..direction.." towards "..destRegionalName.."."

end

function PickMissionSpawnLoc(missionKey)

	local spawnLoc = nil
	local waterLoc = nil
	local missionTable = GetMissionTable(missionKey)
	if (missionTable["Poi"] ~= nil) then
		if (PointsOfInterest[missionTable.Poi] ~= nil) then
			if (ServerSettings.WorldName == "NewCelador") then
				spawnLoc = PointsOfInterest[missionTable.Poi]
			end
		end
	end

	if (spawnLoc == nil and missionTable.PrefabName) then
		--Get loc while considering prefab bounds
		spawnLoc = GetRandomMissionSpawnLoc(missionTable.PrefabName)
	elseif (MissionData.Missions.AllSubregions[missionKey].MissionCategory == "Fishing") then
		--Get coastal loc
		spawnLoc, waterLoc = GetRandomCoastalLoc()
	else
		--Get random loc
		if( MissionData.Missions.AllSubregions[missionKey].RegionLimits ) then
			spawnLoc = GetRandomMissionLocation(campRegion, true, {"NoHousing"},MissionData.Missions.AllSubregions[missionKey].RegionLimits[GetRegionalName(this:GetLoc())])
		else
			spawnLoc = GetRandomMissionLocation(campRegion, true, {"NoHousing"})
		end
		
	end

	return spawnLoc, waterLoc
end

function IsInCampRegion(spawnLoc)
	return campRegion and campRegion:Contains(spawnLoc)
end

function GetRandomMissionSpawnLoc(prefabName)
	local prefabExtents = GetPrefabExtents(prefabName)

    local spawnLoc = GetRandomPassableLocationFromRegion(campRegion,true, {"NoHousing"})
    local relBounds = GetRelativePrefabExtents(prefabName, spawnLoc, prefabExtents)

    local i = 0
    while (i < 30) do
		if(CheckBounds(relBounds,false)) then
	        --Move every mobiles in camp boundary
	        MoveMobiles(relBounds, spawnLoc)
	    	return spawnLoc
	    else
		    spawnLoc = GetRandomPassableLocationFromRegion(campRegion,true, {"NoHousing"})
		    relBounds = GetRelativePrefabExtents(prefabName, spawnLoc, prefabExtents)
	    end
	    i = i+1
    end
    return spawnLoc
end

RegisterEventHandler(EventType.LoadedFromBackup,"",function()
	mSelectedMissions = {}
end)