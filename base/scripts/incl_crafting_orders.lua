function GetOrderOwner(user,order)
	local owner = order:GetObjVar("OrderOwner")
	if not(owner) then
		order:SetObjVar("OrderOwner",user)
		return user
	end

	return owner
end

function AddCommission(user, orderInfo)
	--If player is already commissioned, pull up corresponding order and set new order info
	if (GetCommission(user) ~= nil) then return end
	local commissions = this:GetObjVar("Commissions") or {}

	--Commission includes the order info, and wether it's been accepted by the player
	commissions[user] = {orderInfo, false}
	this:SetObjVar("Commissions", commissions)
end

function GetCommission(user)
	local commissions = this:GetObjVar("Commissions")
	if (commissions ~= nil) then
		if (commissions[user] ~= nil) then return commissions[user] end
	end
	return nil
end

function CommissionAccepted(user)
	local commissions = this:GetObjVar("Commissions")
	if (commissions	== nil) then return nil end
	commissions[user][2] = true
	this:SetObjVar("Commissions", commissions)
end

function HandleOrderSubmission(target, user)
	if (target == nil) then return end
	local isGod = IsGod(user)

    text = ""
    response = {}

    response[1] = {}
    response[1].text = ""
    response[1].handle = "Close" 
    
    local orderInfo = target:GetObjVar("OrderInfo")
    local orderOwner = GetOrderOwner(user,this)
    local craftOrderSkill = this:GetObjVar("CraftOrderSkill")
    
    --If there's no owner, continue anyways (useful for orders created using /create).
    if (orderOwner ~= nil) then
    	--If the order was not issued to the current player. 
	    if (orderOwner ~= user and not isGod ) then
	    	text = "This order doesn't belong to you! I will only reward a crafting order that's been issued directly to you."
	        response[1].text = "Sorry. My mistake."
	        NPCInteractionLongButton(text,this,user,"Responses",response)
	        return
	    end

		--If the order CraftOrderSkill doesn't match up with the skill of the recipe
		if (GetRecipeTableFromSkill(this:GetObjVar("CraftOrderSkill"))[orderInfo.Recipe] == nil) then
			text = "I'm not qualified to reward this crafting order. Heck, I don't even make these! I can't help you here pal. Go find another trader."
	        response[1].text = "Sorry. My mistake."
	        NPCInteractionLongButton(text,this,user,"Responses",response)
	        return
		end
	end

	--If the order is complete, issue reward and destroy the order.
    if (target:GetObjVar("OrderComplete") or isGod ) then

        --target:Destroy()

        user:SystemMessage("You attempt to turn in a craft order...", "info")
		--SCAN MODIFIED EFFECT
		user:PlayEffect("HelmFlagEffect",5)
		user:PlayObjectSound("event:/ui/quest_complete",false)

        text = "Everything checks out. Here's your payment. If you need more work, feel free to check in with me. I might have something more fruitful for you."
        response[1].text = "You're welcome"
        
        --Reset commission
        local commissions = this:GetObjVar("Commissions") or {}
        commissions[user] = nil
		this:SetObjVar("Commissions", commissions)
		
		CraftingOrderReward(user, target, craftOrderSkill, orderInfo)

        --[[local rewardInfo = PickCraftOrderReward(craftOrderSkill, orderInfo.Score)
        local rewardCoins = PickCraftOrderCoinReward(target,user)
        local rewardRecipe = CraftingOrderDefines[craftOrderSkill].RecipeRewards[math.random(1, #CraftingOrderDefines[craftOrderSkill].RecipeRewards)]
    	local recipeRewardTemplate = GetTemplateData(rewardRecipe)

        --DebugMessage(rewardCoins)
        --DebugMessage(rewardRecipe)
        --DebugMessage(rewardInfo.Template)

        --Generate reward message
        local recipeMessage = recipeRewardTemplate.Name or rewardRecipe

        --Issue coin reward
        if (rewardCoins) then
    		Create.Coins.InBackpack(user, rewardCoins)
    	end

    	--Issue recipe reward
    	if (recipeRewardTemplate) then
    		Create.InBackpack(rewardRecipe,user, nil, function(obj)end)
    	end

    	--Issue item reward
    	if (rewardInfo and rewardInfo.Template) then
    		local rewardTemplate = GetTemplateData(rewardInfo.Template)
	        local stackAmount = 1
			local rewardMessage = rewardTemplate.Name
	        if (rewardInfo.StackMax) then
	        	--Build stacked item reward
	        	local stackMin = 1
	        	if (rewardInfo.StackMin) then
	        		stackMin = rewardInfo.StackMin
	        	end
	        	stackAmount = math.random(stackMin, rewardInfo.StackMax)

	        	--Build reward string
    			local rewardTemplate = GetTemplateData(rewardInfo.Template)
    			if (rewardTemplate) then
		        	if (stackAmount > 0) then
		        		rewardMessage = tostring(stackAmount.." "..rewardTemplate.Name..", ")
		        	else
		        		rewardMessage = ""
		        	end
		        end

	        	if (rewardInfo and rewardInfo.Template) then
	        		Create.Stack.InBackpack(rewardInfo.Template,user, stackAmount,nil, function(obj)end)
	        	end

	        elseif (rewardInfo.Packed) then
	        	--Build packed item reward
	        	Create.PackedObject.InBackpack(rewardInfo.Template,user, nil, function(obj)end)
	        else
	        	Create.InBackpack(rewardInfo.Template,user,nil,function(obj)end)
	        end
			user:SystemMessage("You have earned "..rewardMessage..", "..rewardCoins.." coins, and a "..recipeMessage)

	    else
			user:SystemMessage("You have earned "..rewardCoins.." coins, and a "..recipeMessage)
	    end]]
    else
        text = "That order is not yet complete!"
        response[1].text = "Sorry!"
    end

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function CreateCraftingOrder(user, orderInfo)
	backpackObj = user:GetEquippedObject("Backpack") 
    backpackObj:SendOpenContainer(user) 
    local dropPos = GetRandomDropPosition(backpackObj)
	CreateObjInContainer("crafting_order", backpackObj, dropPos, "CreatedCraftingOrder", orderInfo, user)
end