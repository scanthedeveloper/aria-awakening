-- The login region uses a custom player script (for character creation and starting region selection)
require 'player.handle_item_properties'

if(ServerSettings.WorldName=="Login") then
	require 'login_player'
	return
end

require 'base_mobile_advanced'
require 'base_mobile_restregen'
require 'base_skill_sys'
require 'base_mission_sys'
require 'base_player_hotbar'
require 'base_skill_tracker'
require 'base_player_charwindow'
require 'base_skill_window'
require 'base_prestige_window'
require 'base_militia_window'
require 'base_player_titles'
require 'base_player_achievement'
require 'base_player_quest_window'
require 'base_player_godwindow'
require 'base_player_mapwindow'
require 'base_player_keyring'
require 'base_player_death'
require 'base_player_buff_debuff'
require 'base_player_weather'
require 'base_player_guild'
require 'base_player_guild_UI'
require 'scriptcommands'
require 'incl_player_group'
require 'incl_gametime'
require 'incl_mobile_helpers'
require 'incl_resource_source'
require 'incl_player_names'
require 'incl_player_friend'

require 'player_say_commands'
require 'player_starting_screen'

if(IsGod(this)) then
	require 'base_player_mobedit'
end

-- WEIGHT SYSTEM NOT REALLY IMPLEMENTED YET
maxCarryWeight = 1000000
maxTotalWeight = 1000000

carriedObjectSource = nil
carriedObjectSourceLoc = nil
carriedObjectSourceEquipSlot = nil
AUTOLOOT_DELAY = 1.0

currentRegionalName = nil

friendList = nil

lastCraftRequest = nil

-- Overriding the base_mobile apply damage to check for pvp rules
local BaseHandleApplyDamage = HandleApplyDamage
function HandleApplyDamage(damager, damageAmount, damageType, isCrit, wasBlocked, isReflected)
	Verbose("Player", "HandleApplyDamage", damager, damageAmount, damageType, isCrit, wasBlocked, isReflected)

	-- if we are being attacked by a player
	if ( IsPlayerObject(damager) ) then
		if ( ServerSettings.PlayerInteractions.PlayerVsPlayerEnabled ~= true and damager ~= this and not IsGod(damager) ) then
			-- if pvp enabled, not damaging ourselves, and the damager is not a god, stop here (disabled PVP)
			return true
		end
	end

	local newHealth = BaseHandleApplyDamage(damager, damageAmount, damageType, isCrit, wasBlocked, isReflected)
	if(newHealth and newHealth > 0) then
		local magnitude = math.clamp(damageAmount/3, 1, 4)		
		
		--DebugMessage("Choice = "..tostring(choice))
		local doNotShakeScreenOnHit = this:GetObjVar("doNotShakeScreenOnHit")
		
		if(doNotShakeScreenOnHit) then

		else
			--this:PlayLocalEffect(this,"ScreenShakeEffect", 0.3,"Magnitude="..magnitude)
		end
        
        if ( not this:HasObjVar("DisableHitFlash") and not this:HasTimer("RecentHitFlash") ) then
            local healthRatio = (newHealth/GetMaxHealth(this))
            local choice = 1
            if(healthRatio <= 0.2) then
                choice = 2
            end

            this:PlayLocalEffect(this,"BloodSplatter"..choice.."Effect", 1)	
            this:ScheduleTimerDelay(TimeSpan.FromSeconds(2.5), "RecentHitFlash")
        end			
	end

	return newHealth
end

--On resurrection
OverrideEventHandler("default:base_mobile", EventType.Message, "Resurrect", function (statPercent, resurrector, force)
    if not( IsDead(this) ) then return end

	if ( this:GetSharedObjectProperty("IsDead") ) then
		local corpseObj = this:GetObjVar("CorpseObject")
		if ( corpseObj ) then
			corpseObj:SendMessage("Resurrect", statPercent, resurrector, force)
			return
		end
    end

	this:PlayEffect("HolyEffect")
	CallFunctionDelayed(TimeSpan.FromSeconds(1), function()
		this:PlayEffect("HolyEffect")
	end)

    if ( force ) then
		PlayerResurrect(this, resurrector, nil, statPercent)
		return
	end

	-- confirm they want to be resurrected from their ghost
	local extraInfo = "You will be given a new body"
	if ( ServerSettings.PlayerInteractions.FullItemDropOnDeath ) then
		extraInfo = extraInfo .. " with none of your previous items less they be blessed." -- naked af, birthday suit tho
	else
		extraInfo = extraInfo .. " with what you were wearing on death."
	end

	this:SetMobileFrozen(true,true)
	
	PlayerResurrectDialog(this, {
		text = "Resurrect",
		tooltip = extraInfo,
		cancelable = true,
		cb = function(accept)
			if ( accept ) then
				PlayerResurrect(this, resurrector, nil, statPercent)
			end
			this:SetMobileFrozen(false,false)
		end,
	})

end)


-- this function checks range of the topmost object from this player
function IsInRange(targetObj)
	Verbose("Player", "IsInRange", targetObj)
	local userPos = this:GetLoc()
	local dropLoc = nil

	local topmostObj = targetObj:TopmostContainer() or targetObj

	local dropRange = topmostObj:GetObjVar("DropRange")
	if(topmostObj:GetLoc():Distance(userPos) > (dropRange or OBJECT_INTERACTION_RANGE) ) then		
		return false
	end

	return true
end

function CanPickUp(targetObj,quiet,isQuickLoot)
	Verbose("Player", "CanPickUp", targetObj,quiet,isQuickLoot)
	--DebugMessage("It is calling this function")	
	if (IsDead(this)) then return false end

	if not(IsInRange(targetObj)) then
		if(IsImmortal(this)) then
			this:SystemMessage("Your godly powers allow you to reach that.","info")
		else
			if not(quiet) then
				this:SystemMessage("You cannot reach that.","info")
			end
			return false
		end
	end

	-- dont even let gods pick these up since it can mess up the object
	if( IsLockedDown(targetObj) or targetObj:GetSharedObjectProperty("DenyPickup") == true) then
		if not(quiet) then
			this:SystemMessage("You cannot pick that up.","info")
		end
		return false
	end	

	if (mapWindowOpen) then
		if (targetObj == this:GetObjVar("ActiveMap")) then
			CloseMap()
		end
	end

	-- and we can pick it up
	local weight = targetObj:GetSharedObjectProperty("Weight")

	-- note: even gods should not be able to pick up -1 weight items normally	
	if( weight == nil or weight == -1 ) then
		if not(quiet) then
			this:SystemMessage("You cannot pick that up.","info")
		end
		return false
	end

	local topCont = targetObj:TopmostContainer() or this
	--if it's a mailbox and it's locked down then
	--DebugMessage(1)
	if (topCont:HasObjVar("IsMailbox") and topCont:GetObjVar("LockedDown")) then
	--if I'm not the owner
		--DebugMessage(2)
		if (not Plot.IsOwnerForLoc(this,topCont:GetLoc())) then
			--DebugMessage(3)
			this:SystemMessage("You can't pick up someone else's mail.","info")
			return false
		end
	end 

	--Used for stealing skill
	if (targetObj:HasObjVar("StealingDifficulty")) then
		this:SystemMessage("This object does not belong to you.","info")
		return false
	end
	--DebugMessage("topCont is "..tostring(topCont:GetName()))
	--if (not this:HasModule("ska_pickpocket")) then
	--DebugMessage("Really big check:",topCont ~= nil,topCont:IsMobile(),(topCont:IsPlayer() or not(IsDead(topCont))),topCont ~= this,not(IsDemiGod(this)))
	if(topCont ~= nil and topCont:IsMobile()
			and (topCont:IsPlayer() or not(IsDead(topCont)))
			and topCont ~= this and not(IsDemiGod(this))) then

		local isOwner = IsController(this,topCont)		
		if( not(isOwner) or GetHirelingOwner(topCont) == this) then
			isOwner = IsHiredMerchant(this,topCont)
		end

		if not(isOwner) then
			if not(quiet) then
					--DebugMessage("Yep that's it.")
				this:SystemMessage("You cannot pick that up.","info")
			end		
			return false
		end
	end
	--elseif (this:HasModule("ska_pickpocket") and topCont:IsMobile() and (not IsDead(topCont)) and (not (topCont == this))) then
	--	this:SendMessage("PickPocketRoll")
		--DebugMessage("Pick pocket roll")
	--end
	--DebugMessage("Result: ",this:HasModule("ska_pickpocket"),topCont:IsMobile(),(not IsDead(topCont)),(topCont == this))
	if ( targetObj:HasObjVar("noloot") ) then
		if IsGod(this) == false then
			this:SystemMessage("You cannot pick that up.","info")
			return false
		end
		this:SystemMessage("Your godly powers allow you to loot a noloot.","info")
	end

	if ( ( topCont:HasObjVar("noloot") or topCont:HasObjVar("guardKilled") ) and not(IsHiredMerchant(this,topCont)) ) then
		this:SystemMessage("You can't pick that up.","info")
		return false
	end

	--DebugMessage("topCont is "..tostring(topCont))
	if (not this:HasLineOfSightToObj(topCont,ServerSettings.Combat.LOSEyeLevel)) then
		this:SystemMessage("Cannot see that.","info")
		return false
	end

	-- make sure this item is not in a container that is for sale
	local inSaleContainer = false
	local lockedContainer = nil
	ForEachParentContainerRecursive(targetObj,false,
        function (parentObj)
            if(parentObj:HasModule("hireling_merchant_sale_item")) then                
                inSaleContainer = true
                return false
			end
			
			if( parentObj:HasObjVar("locked") ) then                
                lockedContainer = parentObj
                return false
			end
            return true
        end)

    if(inSaleContainer) then
        this:SystemMessage("[$2402]","info")
        return false
    end

    -- make sure it's not in a locked container
    if( lockedContainer ~= nil ) then
    	if(lockedContainer:HasObjVar("SecureContainer") and not Plot.HasObjectControl(this, lockedContainer, lockedContainer:HasObjVar("FriendContainer"))) then
	        this:SystemMessage("That is in a locked container.","info")
	        return false
        end
	end

    -- only block items inside containers inside the trade window
    local isTrade, depth = IsInTradeContainer(targetObj)
    if(isTrade and depth > 0) then
    	return false
    end

    if(IsInEquippedContainer("Bank",targetObj,this,true) and not(IsBankerInRange(this))) then						
    	this:SystemMessage("You can not access your bank from here.","info")
		return false
	end

	if ( IsInActiveTrade(this)
	and not IsInBackpack(targetObj, this)
	and not IsInTradingPouch(targetObj) 
	and topCont and topCont ~= this ) then
		this:SystemMessage("Cannot pick that up while trading.","info")
		return false
	end

	-- Is it inside a mounted object?
	local sourceContainer = targetObj:ContainedBy()
	if( sourceContainer ~= nil and IsMount(sourceContainer) ) then
		DebugMessage("Attempted to grab item from mount directly!")
		return false
	end

	return true
end

function UpdateName()
	Verbose("Player", "UpdateName")
	local charName = ColorizePlayerName(this, this:GetName() .. GetNameSuffix())
	this:SetSharedObjectProperty("DisplayName", charName)
end

function UpdateTitle()
	Verbose("Player", "UpdateTitle")
	-- DAB NOTE: THIS WILL CHANGE YOUR TITLE IF YOU GET A NEW ACCOUNT BASED TITLE
	local titleIndex = this:GetObjVar("titleIndex") or 1
	local oldTitle = GetActiveTitle(playerObj)
	local allTitles = GetAllAchievementsObtained(playerObj, true)

	if(titleIndex == nil) then titleIndex = #allTitles end

	titleIndex = math.min(titleIndex,#allTitles)
	--DebugMessage("Alphamaan")
	if( titleIndex ~= 0 ) then
		--DebugMessage("Betamaan")
		local newTitle = allTitles[titleIndex].Title
		if( oldTitle ~= newTitle ) then
			--DebugMessage("Cetamaan")
			this:SetObjVar("titleIndex",titleIndex)
			this:SetSharedObjectProperty("Title", newTitle)
			this:SystemMessage("Your title has been set to "..newTitle,"info")
			SetTooltipEntry(this,"Title",newTitle,100)
		end		
	elseif(oldTitle ~= "") then
		this:SetObjVar("titleIndex",titleIndex)		
		this:SetSharedObjectProperty("Title", "")
		RemoveTooltipEntry(this,"Title")
		this:SystemMessage("Your title has been cleared.","info")
	end

	this:SendMessage("UpdateCharacterWindow")
end

--SCAN ADDED
--- SUMMONING RULES
--- in order to make it simple, when a player logs out their summons will instantly destroy
--- this will be handled by "Connect" and "Disconnect" message be passed to the event handlers
--- when the player logs in, their summon table object variable is refreshed and empty.

--- this handles the event for when this player changes worlds or connects
-- @param loginType - (string)the type of log out "Connect" or "ChangeWorld"
function HandleUserLoginEvent(loginType)
	-- if the player is logging in and not changing regions
	-- then we want to cleanup and initialize a new summons table
	if(loginType=="Connect") then
		-- if we don't have the SummonTable then lets set up the table objvar
		-- this will take care of setting up the summontable
		if not(this:HasObjVar("SummonTable")) then
			this:SetObjVar("SummonTable",{})
		end
		-- refresh the summon tables
		InitializePlayerSummonTable(this)
	end
end

--- this handles the event for when this player changes worlds or disconnects
-- @param logoutType - (string)the type of log out "Disconnect" or "ChangeWorld"
function HandleUserLogoutEvent(logoutType)
	-- if the player is logging out and not changing regeions
	-- then we want to destroy all their pesky summons.
	if(logoutType =="Disconnect") then
		-- destroys all active summons from this player
		-- then initializes a new summon table
		DestroyAllPlayerSummons(this)
	end
end

function UpdateChatChannels()
	-- each entry in the chat channels is the name and command in array form
	--SCAN ADDED WORLD CHAT 03/27
	local chatChannels = { {"Say","say"}, {"World","world"} } 
	local guild = GuildHelpers.Get(this)
	if(guild ~= nil) then
		table.insert(chatChannels,{ "Guild", "g"})
	end
	local groupId = GetGroupId(this)
	if ( groupId ~= nil and GetGroupVar(groupId, "Leader") ~= nil ) then
		table.insert(chatChannels,{ "Group", "group" })
	end
	if ( Militia.GetId(this) ) then
		table.insert(chatChannels,{ "Militia", "m" })
	end

	local friendChatChannel = this:GetObjVar("FriendChatChannel")

	--Check friend chat. Add friend to chat channel if chat is valid; remove if not
	if (friendChatChannel ~= nil) then
		local friendList = this:GetObjVar("FriendList")

		for i=1,#friendChatChannel,1 do
			local friend = friendChatChannel[i]

			if (FriendValidForChat(this, friend, friendList)) then
				chatChannels[#chatChannels+1] = {friend:GetCharacterName(), "tell "..friend:GetCharacterName()}
			else
				RemoveFriendFromChatChannel(this, friend)
			end
		end
	end


	--DebugMessage("UpdateChatChannels: "..DumpTable(chatChannels))
	this:SendClientMessage("UpdateChatChannels",chatChannels)

	--DAB HACK: Fix to make sure players have the correct chat channels until we can fix it correctly
	-- The bug was the ChatChannelSelector on the client was not initialized right when the client loads in
	-- We need to delay client messages until Unity is fully loaded
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(60 + math.random()),"UpdateChatChannels")
end

function TryHarvestItem(objRef)
	if( not(objRef) or not(objRef:IsValid()) ) then return end

	if (IsMobileDisabled(this)) then 
		this:SystemMessage("Cannot harvest now", "info")
		return 
	end

	local requiredTool = GetRequiredTool(objRef)
	if( requiredTool ~= nil ) then
		local toolObj = nil
		if( requiredTool == "BareHands" ) then
			toolObj = this		
		else			
			local weapon = this:GetEquippedObject("RightHand")
			
			if( weapon ~= nil and weapon:GetObjVar("ToolType") == requiredTool) then
				toolObj = weapon
			elseif( requiredTool == "Knife" ) then
				toolObj = GetHarvestToolInBackpack(this,requiredTool)
			end
		end

		if( toolObj ~= nil ) then
			-- bare hands are also a tool and attached to player
			toolObj:SendMessage("HarvestObject",objRef,this)
			return true
		else
			this:SystemMessage("You need a "..requiredTool.." to harvest that.","info")
		end
	end

	return false
end

function HandleRequestPickUp(pickedUpObject, silent)
	--DebugMessage("Tried to pick up "..pickedUpObject:GetName())
	local carriedObject = this:CarriedObject()
	if( carriedObject ~= nil and carriedObject:IsValid() and pickedUpObject ~= carriedObject) then
		this:SystemMessage("You are already carrying something.","info")
		this:SendPickupFailed(pickedUpObject)
		return
	end
		
	-- DAB NOTE: This could be optimized to not iterate through parent containers twice (TopmostContainer and IsInBackpack both iterate through parents)
	local sourceTopmost = pickedUpObject:TopmostContainer()
	if(sourceTopmost) then
		local snoopTarget = this:GetObjVar("SnoopTarget")
		if(snoopTarget and snoopTarget == sourceTopmost and IsInBackpack(pickedUpObject,snoopTarget)) then
			TryStealItemFromMob(pickedUpObject,sourceTopmost)
			return
		end
	end

	if not(CanPickUp(pickedUpObject, silent)) then
		this:SendPickupFailed(pickedUpObject)
		return
	end
	--DebugMessage("Pick up here")
	this:SendMessage("BreakInvisEffect", "Pickup")

	if ( pickedUpObject:IsContainer() ) then
		CloseContainerRecursive(this, pickedUpObject)
	end

	-- keep track of the source location so we can undo the pickup
	local sourceContainer = pickedUpObject:ContainedBy()
	local sourceLoc = pickedUpObject:GetLoc()
	local sourceEquipSlot = nil
	local equipSlot = GetEquipSlot(pickedUpObject)
	--check to see if the containers have noloot on them
	if (sourceContainer ~= nil) then 
		if (sourceContainer:HasObjVar("noloot") and not(IsHiredMerchant(this,sourceContainer)) and (IsDemiGod(this) == false)) then			
			this:SystemMessage("You can't pick that up.","info")
			this:SendPickupFailed(pickedUpObject)
			return
		end
	end
	if sourceTopmost ~= nil then
		if sourceTopmost:HasObjVar("noloot") and not(IsHiredMerchant(this,sourceContainer)) and (IsDemiGod(this) == false) then			
			this:SystemMessage("You can't pick that up.","info")
			this:SendPickupFailed(pickedUpObject)
			return
		end
		if ( CheckCriminalLoot(this, sourceTopmost) == false ) then
			this:SendPickupFailed(pickedUpObject)
			return
		end
	end
	
	--GW we may have just died instantly due to insta whack guards, and the karma check above
	if( IsDead(this)) then
		this:SendPickupFailed(pickedUpObject)
		return
	end
	if (sourceContainer == nil or sourceContainer ~= this:GetEquippedObject("Backpack")) then
		local weight = pickedUpObject:GetSharedObjectProperty("Weight")
		local canAdd,weightCont,maxWeight = CanAddWeightToContainer(this:GetEquippedObject("Backpack"),weight)
		if ( not canAdd) then
			if not (IsImmortal(this)) then
				this:SystemMessage("You are carrying too much (Max: " .. tostring(maxWeight) .. " stones)","info")
				SetMobileMod(this, "Disable", "OverweightPickup", true)
				AddBuffIcon(this,"Overweight","Overweight","steal","Cannot move again until item is dropped.")
				this:SendMessage("Overweight")
			end
		end
	end

	if(pickedUpObject:HasObjVar("Summoned")) then
		this:SystemMessage("Your summoned ".. StripColorFromString(pickedUpObject:GetName()).." vanishes.","info")
		pickedUpObject:Destroy()
		return
	end
	
	if(equipSlot ~= nil and this:GetEquippedObject(equipSlot) == pickedUpObject) then
		sourceEquipSlot = equipSlot
	end

	if( pickedUpObject:MoveToContainer(this,Loc(0,0,0)) ) then
		if(pickedUpObject:DecayScheduled()) then
			pickedUpObject:RemoveDecay()
		end

		carriedObjectSource = sourceContainer
		carriedObjectSourceTopmost = sourceTopmost
		carriedObjectSourceLoc = sourceLoc
		carriedObjectSourceEquipSlot = sourceEquipSlot
		if(sourceEquipSlot ~= nil) then
			pickedUpObject:SendMessage("WasUnequipped", this)
		end
	end
end

function TryStealItemFromMob(pickedUpObject,sourceTopmost)
	-- Cannot steal blessed or cursed items
	if( pickedUpObject:HasObjVar("Blessed") or pickedUpObject:HasObjVar("Cursed") ) then
		this:SystemMessage("You cannot steal that item.", "info")        
		this:SendPickupFailed(pickedUpObject)
		return
	end

	-- Cannot steal from gods
	if(IsImmortal(sourceTopmost) and not(TestMortal(sourceTopmost))) then
		this:SystemMessage("You cannot steal from the gods!", "info")        
		this:SendPickupFailed(pickedUpObject)
		return
	end
	
	-- Cannot steal from protected players
	if( IsProtected(sourceTopmost, this, nil, true) ) then
		self.ParentObj:SystemMessage("The gods will not allow that.", "info")
		EndMobileEffect(root)
		return false 
	end

	-- block stealing from someone you are in an active conflict with
	if(sourceTopmost:HasTimer("RecentlyStolenFrom")) then
		this:SystemMessage("That person is too aware of your presence to steal from them.", "info")        
        this:SendPickupFailed(pickedUpObject)
        return
	end	

	local skillDict = GetSkillDictionary(this)
    local stealingSkill = GetSkillLevel(this,"StealingSkill",skillDict)	

	-- check weight
	local itemWeight = GetWeight(pickedUpObject)
	local maxWeight = math.max(1,(10 * (stealingSkill / 100)))
	if(itemWeight > maxWeight) then
        this:SystemMessage("That item is too heavy for you to steal.", "info")        
        this:SendPickupFailed(pickedUpObject)
        return
    end    

	-- perform skill check
	CheckSkillChance( this, "StealingSkill", stealingSkill )

	sourceTopmost:ScheduleTimerDelay(TimeSpan.FromMinutes(5),"RecentlyStolenFrom")

	local stealChance =  (0.5 * (stealingSkill / 100))	
    if not(Success(stealChance)) then
        this:SystemMessage("You fail to steal the item, revealing yourself!", "info")
        this:SendMessage("BreakInvisEffect", "Action", "Steal")
        sourceTopmost:SendMessage("You catch "..StripColorFromString(this:GetName()).." trying to steal something from your pack!","info")
        this:SendPickupFailed(pickedUpObject)
        return
    end

    this:SystemMessage("You deftly steal the item.", "info")
    pickedUpObject:MoveToContainer(this,Loc(0,0,0))
end

-- will attempt to return the carried object back to its source container and location
function UndoPickup()
	local carriedObject = this:CarriedObject()
	if( carriedObject ~= nil and carriedObject:IsValid() ) then
		if(carriedObjectSource ~= nil and carriedObjectSource:IsValid()) then
			if(carriedObjectSource == this and carriedObjectSourceEquipSlot ~= nil) then
				DoEquip(carriedObject,this)
			else
				local destLoc = carriedObjectSourceLoc or GetRandomDropPosition(carriedObjectSource)
				TryPutObjectInContainer(carriedObject, carriedObjectSource, destLoc)
			end
		elseif(carriedObjectSourceLoc ~= nil) then
			carriedObject:SetWorldPosition(carriedObjectSourceLoc)
		end
	end
end

function DropInWorld(droppedObject,dropLocation)
	droppedObject:SetWorldPosition(dropLocation)
	Decay(droppedObject, ServerSettings.Misc.ObjectDecayTimeSecs)
end

function HandleRequestDrop(droppedObject, dropLocation, dropObject, dropLocationSpecified, wasDroppedInto)
	if( not droppedObject:IsBeingCarriedBy(this) ) then
		-- something really bad happened
		DebugMessage("LUA ERROR: Tried to drop object that is not that players carried object!")
		this:SendPickupFailed(droppedObject)
		return
	end

	if( dropObject ~= nil and dropObject:IsValid() ) then
		-- if we are dropping this on ourself put it in the backpack
		if( dropObject == this ) then
			local backpackObj = this:GetEquippedObject("Backpack")
			if( backpackObj ~= nil ) then
				dropObject = backpackObj
			else
				return
			end
		-- confirm that the drop object is not too far away
		elseif not(IsInRange(dropObject)) then
			this:SystemMessage("Too far away.","info")
			return
		end

		if ( dropObject:HasObjVar("HandlesDrop") ) then
			local resourceType = dropObject:GetObjVar("ResourceType")
			if ( resourceType ~= nil and ResourceEffectData[resourceType] and ResourceEffectData[resourceType].MobileEffectDropHandle ) then
				StartMobileEffect(this, ResourceEffectData[resourceType].MobileEffectDropHandle, dropObject, droppedObject)
			else
				dropObject:SendMessage("HandleDrop",this,droppedObject)
			end
		elseif( dropObject:IsContainer() ) then
			-- if we got a nil drop pos we pick a random position in the container
			if not(dropLocationSpecified) then
				-- didnt stack so put in random location
				dropLocation = GetRandomDropPosition(dropObject)
			end
			local canHold, reason = TryPutObjectInContainer(droppedObject, dropObject, dropLocation, IsDemiGod(this) and not TestMortal(this), not(wasDroppedInto),false,carriedObjectSourceTopmost,this)
			if( not canHold ) then
				-- DAB TODO: Distinguish between full container and not a container
				this:SystemMessage("You cannot drop that there. "..(reason or ""),"info")
				return
			end
		else
			local dropContainer = dropObject:ContainedBy()
			if( dropContainer ~= nil ) then
				if(not dropLocationSpecified) then
					dropLocation = dropObject:GetLoc()
				end
				if (dropObject ~= nil) then
					local canHold, reason = TryPutObjectInContainer(droppedObject, dropContainer, dropLocation, IsDemiGod(this) and not TestMortal(this), false, true,carriedObjectSourceTopmost,this)
					if( not canHold ) then
						this:SystemMessage("You cannot drop that there. "..(reason or ""),"info")
						return
					end
					if( CanStack(dropObject,droppedObject) ) then
						RequestStackOnto(dropObject,droppedObject)
					else
						TryPutObjectInContainer(droppedObject, dropContainer, dropLocation, IsDemiGod(this) and not TestMortal(this), false,false,carriedObjectSourceTopmost,this)
					end
				end
			else
				if( CanStack(dropObject,droppedObject) ) then
					RequestStackOnto(dropObject,droppedObject)
				else
					dropLocation = dropObject:GetLoc()
					DropInWorld(droppedObject,dropLocation)
				end
			end
		end
	elseif( dropLocation ~= nil ) then
		if(this:GetLoc():Distance(dropLocation) > OBJECT_INTERACTION_RANGE ) then		
			this:SystemMessage("You cannot reach that.","info")
			return
		end

		DropInWorld(droppedObject,dropLocation)
	end	
	RemoveBuffIcon(this, "Overweight")
	SetMobileMod(this, "Disable", "OverweightPickup", nil)
end

function HandleRequestEquip(equipObject, equippedOn)
	local topmostObj = equipObject:TopmostContainer()
	if(topmostObj ~= this) then
		this:SystemMessage("You can only equip things you are already carrying","info")
		return
	end
	DoEquip(equipObject,equippedOn,this)
end

-- DAB TODO: We should make locking other objects stats as a separate function since it should
-- be for GOD characters only
function HandleLockStatRequest(myStat,targetObjId)	
	local myTarg = this
	if not (targetObjId == nil) then
		local targObj = GameObj(tonumber(targetObjId))
		if(targObj == nil) or not(targObj:IsValid()) then
			this:SystemMessage("Invalid set target","info")
			return
		end
		myTarg = targObj
	end
	local mySname =string.sub(myStat,2,3)
	local mySstart = string.sub(myStat,1,1)
	myStat = string.upper(mySstart) .. string.lower(mySname)
	myTarg:SetObjVar(myStat .. "Lock", true)
	if(myTarg:IsPlayer()) then
		myTarg:SystemMessage(myTarg:GetName() .. " locked ".. myStat,"info") 
	end
end

function HandleUnlockStatRequest(myStat,targetObjId)	
	local myTarg = this
	if not (targetObjId == nil) then
		local targObj = GameObj(tonumber(targetObjId))
		if(targObj == nil) or not(targObj:IsValid()) then
			this:SystemMessage("Invalid set target","info")
			return
		end
		myTarg = targObj
	end
	local mySname =string.sub(myStat,2,3)
	local mySstart = string.sub(myStat,1,1)
	myStat = string.upper(mySstart) .. string.lower(mySname)
	if(myTarg:HasObjVar(myStat .. "Lock")) then
		myTarg:DelObjVar(myStat .. "Lock")
	end
	if(myTarg:IsPlayer()) then
		myTarg:SystemMessage(myTarg:GetName() .. " unlocked ".. myStat,"info") 
	end
end	

function DoUse(usedObject,usedType)
	if (usedObject == nil or not usedObject:IsValid()) then return end
	
	if (not(IsInCombat(this)) and usedObject:HasModule("merchant_sale_item") == true and usedType == "Attack") then
		usedType = usedObject:GetSharedObjectProperty("DefaultInteraction")
	end

	--If the object is in a locked container, players cannot use it
	if (usedType ~= "God Info" and usedObject:IsInContainer()) then
		local container = usedObject:ContainedBy()
		local key = GetKey(this,container)
		if (container:GetObjVar("locked")) then
			-- Secure Containers allow a user to open and view contents without unlocking and rendering the container vulnerable
			if ( not container:HasObjVar("SecureContainer") or not Plot.HasObjectControl(this, container, container:HasObjVar("FriendContainer")) ) then
				this:SystemMessage("That is in a locked container.","info")
				return
			end
		end
	end

	--Load the use case ranges from use_cases.lua	
	local usedCaseRange = OBJECT_INTERACTION_RANGE
	local topmost = usedObject

	if not(usedObject:IsPermanent()) then
		if( IsDead(this) and not usedObject:HasObjVar("UseableWhileDead")) then
			return
		end

		topmost = usedObject:TopmostContainer() or usedObject
		if (AllUseCases[usedType] ~= nil) then
			usedCaseRange = AllUseCases[usedType].Range or OBJECT_INTERACTION_RANGE
			if (AllUseCases[usedType].Restriction == "God" and not IsGod(this)) then
				DebugMessage("WARNING: Player "..this:GetName().." attempted to use a use case restricted to gods! usedType is "..tostring(usedType))
				return
			end
			if (AllUseCases[usedType].Restriction == "Immortal" and not IsGod(this)) then
				DebugMessage("WARNING: Player "..this:GetName().." attempted to use a use case restricted to immortals! usedType is "..tostring(usedType))
				return
			end
			if (AllUseCases[usedType].Restriction == "DemiGod" and not IsGod(this)) then
				DebugMessage("WARNING: Player "..this:GetName().." attempted to use a use case restricted to demigods! usedType is "..tostring(usedType))
				return
			end
		end

		if(IsInTradeContainer(usedObject) and (not(usedObject:IsContainer()) or usedType ~= "Open")) then
			return
		end
	else
		if( IsDead(this) ) then
			return
		end
	end

	if( HelperHiredMerchants.IsHiredMerchant(usedObject) and usedType == 'Interact' ) then
		usedCaseRange = AllUseCases["Inspect"].Range
	end

	if (this:GetLoc():Distance2(topmost:GetLoc()) > usedCaseRange and not IsGod(this)) then
		this:SystemMessage("You are too far to do that.","info")
		return 
	end

	--DebugMessage("DoUse: "..tostring(usedObject)..", "..tostring(usedType))
	if(usedObject:IsValid()) then
		if(usedObject:IsPermanent()) then			
			--DebugMessage("DoUsePermanent",this:GetName(),tostring(usedObject),tostring(usedObject:GetSharedObjectProperty("ResourceSourceId")),tostring(usedType))

			-- right now the only thing you can do with permanents is harvest
			TryHarvestItem(usedObject)
		else
			if(usedType == nil or usedType == "") then
				usedType = usedObject:GetSharedObjectProperty("DefaultInteraction") or "Use"
			end

			--DebugMessage("DoUse",this:GetName(),tostring(usedObject),usedObject:GetName(),tostring(usedType))
			if(usedType == "Character") then
				this:SendMessage("OpenCharacterWindow")
			elseif(usedType == "Stand") then
				if (IsSitting(this)) then
					this:SendMessage("StopSitting")
					this:SetWorldPosition(this:GetObjVar("PositionBeforeUsing"))
				end
				RemoveUseCase(this, "Stand")
			elseif(usedType == "Wake Up") then
				this:SendMessage("WakeUp")
				this:SetWorldPosition(this:GetObjVar("PositionBeforeUsing"))
				RemoveUseCase(this, "Wake Up")
			elseif(usedType == "Inspect" or usedType == "Disrobe") then
				OpenInspectWindow(usedObject)
			elseif (usedType == "Kick from Group") then
				local groupId = GetGroupId(usedObject)
				if ( groupId ~= nil and GetGroupVar(groupId, "Leader") == this ) then
					GroupRemoveMember(groupId, usedObject)
				end
			elseif (usedType == "Invite to Group") then
				GroupInvite(this, usedObject)
			elseif (usedType == "Group") then
				if ( this:HasModule("group_ui") ) then
					this:SendMessage("GroupUpdate")
				else
					this:AddModule("group_ui")
				end
			elseif (usedType == "Invite to Guild") then
				Guild.Invite(this, usedObject)				
			elseif (usedType == "Send Friend Request") then
				SendFriendRequest(this, usedObject)
			elseif (usedType == "Trade") then
				if(IsInActiveTrade(this)) then
					this:SystemMessage("You already have an active trade in progress.","info")
				elseif(IsInActiveTrade(usedObject)) then
					this:SystemMessage("The target already has an active trade in progress.","info")
				elseif(IsInCombat(this)) then
					this:SystemMessage("Cannot trade while in combat.","info")
				elseif(IsInCombat(usedObject)) then
					this:SystemMessage("The target is in combat.","info")
				else
					this:AddModule("trading_controller",{TradeTarget=usedObject})
				end
			-- Runebooks
			elseif (usedType == "Rename Runebook") then
				StartMobileEffect(usedObject, "RunebookRename", this)
			-- Gardening Use Types
			elseif (usedType == "Water") then
				StartMobileEffect(usedObject, "WaterPlant", this)
			elseif (usedType == "Gather") then
				StartMobileEffect(usedObject, "GatherPlant", this)
			elseif (usedType == "Empty") then
				StartMobileEffect(usedObject, "EmptyPlant", this)
			elseif (usedType == "Force Grow") then
				if (IsGod(this)) then
					Gardening.DoGrowth(usedObject, true)
				end
			-- A player wants to listen to a band
			elseif (usedType == "Listen to Music") then
				StartMobileEffect(this, "ListenToBand", usedObject)
			elseif (usedType == "Stop Listening") then
				this:DelObjVar("ListeningToBandID")
			elseif(usedType == "Give Tip") then
				StartMobileEffect(this, "TipBard", usedObject)
			elseif (usedType == "Duel") then
				Duel.Challenge(this, usedObject)
			elseif (usedType == "Loot All" or usedType == "Loot Gold") then
				local targetContainer = usedObject
				if (targetContainer:IsMobile()) then	
					local backpackObj = targetContainer:GetEquippedObject("Backpack")    
					if(backpackObj) then
						targetContainer = backpackObj
					end
				elseif (targetContainer:IsContainer()) then
					if(targetContainer:HasObjVar("locked")) then
						if ( not targetContainer:HasObjVar("SecureContainer") or not Plot.HasObjectControl(this, targetContainer, targetContainer:HasObjVar("FriendContainer")) ) then
				    	   this:SystemMessage("That appears to be locked.","info")
				    	   return
				        end
				    end
				end
				if( usedType == "Loot All" ) then
					StartLootAll(targetContainer)
				else
					StartLootAll(targetContainer, true)
				end
			elseif (usedType == "God Info") then
				if (IsGod(this)) then
					DoInfo(usedObject)
				else
					DebugMessage("WARNING: Player "..this:GetName().." attempted to open a god info window for "..usedObject:GetName()..", player is not a God character.")
				end
			elseif (usedType == "Manage Arena") then
				if (IsGod(this)) then
					this:SendMessage("StartMobileEffect", "GodManageRankedArena", usedObject)
				else
					DebugMessage("WARNING: Player "..this:GetName().." attempted to open a manage arena window for "..usedObject:GetName()..", player is not a God character.")
				end
			elseif (usedType == "Manage Dynamic Spawn") then
				if (IsGod(this)) then
					this:SendMessage("StartMobileEffect", "GodManageDynamicSpawn", usedObject)
				end
			elseif (usedType == "Edit Mob") then
				if(IsGod(this)) then
					DoMobEdit(usedObject)
				end
			elseif(usedType == "Pick Up" or usedType == "God Pick Up") then
				HandleRequestPickUp(usedObject)
			elseif(usedType == "Quick Loot") then
				ProgressBar.Show{TargetUser=this,Label="Looting",Duration=AUTOLOOT_DELAY}
				this:ScheduleTimerDelay(TimeSpan.FromSeconds(AUTOLOOT_DELAY),"autolootitem",usedObject,usedObject:ContainedBy())
			elseif(usedType == "Harvest" or usedType == "Chop" or usedType == "Mine" or usedType == "Skin" or usedType == "Forage") then
				TryHarvestItem(usedObject)
			-- DAB TODO: Handle gods and subordinates
			elseif(usedType == "Equip") then
				if(usedObject:TopmostContainer() ~= this) then
					this:SystemMessage("[$2404]","info")
				else
					DoEquip(usedObject,this)
				end
			elseif (usedType == "Tame") then
				if not( HasMobileEffect(this, "Tame") ) then
					StartMobileEffect(this, "Tame", usedObject)
				end
			elseif (usedType == "Attack") then
				--DebugMessage("Getting here.")
				this:SendMessage("AttackTarget",usedObject)
			elseif(usedType == "Unequip") then
				if(usedObject:IsEquippedOn(this)) then
					user:SystemMessage("You can only unequip items from yourself.","info")
				else
					DoUnequip(usedObject,this)
				end
			-- always try to harvest objects with a resource source id
			elseif(usedObject:HasObjVar("ResourceSourceId") and usedType == "Use") then
				if not(TryHarvestItem(usedObject)) then
					usedObject:SendMessage("UseObject",this,usedType)
				end
			elseif( ValidResourceUseCase(usedObject, usedType) ) then
				TryUseResource(this, usedObject, usedType)
			else
				usedObject:SendMessage("UseObject",this,usedType)
			end
		end		
	end
end

function HandleUseCommand(usedObjectId,...)
	if( usedObjectId == nil ) then return end
	local usedObject = GameObj(tonumber(usedObjectId))
	-- the use type can contain spaces so combine it since its the last argument
	local usedType = CombineArgs(...)
	DoUse(usedObject,usedType)
end

function HandleUsePermanentCommand(permanentId,usedType)
	if( permanentId == nil ) then return end
	local usedObject = PermanentObj(tonumber(permanentId))
	
	DoUse(usedObject,usedType)
end

function HandleUseResourceCommand(resourceType)
	if ( resourceType == nil or resourceType == "" or IsDead(this) ) then return end

	local backpackObj = this:GetEquippedObject("Backpack")
	if ( backpackObj ~= nil ) then
		local resourceObj = FindItemInContainerRecursive(backpackObj, function(item)
			--Check if resource is in a locked container
			if (item:GetObjVar("ResourceType") == resourceType) then            
				if (item:IsInContainer()) then
					local container = item:ContainedBy()
					if (container:GetObjVar("locked")) then
						this:SystemMessage("That is in a locked container.","info")
						return false
					end
				end
				return true
			end
			return false
           --return item:GetObjVar("ResourceType") == resourceType
        end)

		if ( resourceObj ~= nil ) then TryUseResource(this, resourceObj) end
	end
end

RegisterEventHandler(EventType.CreatedObject,"NewLootStack",function (success,objRef,amount)
	if (success) then
		RequestSetStack(objRef,amount)
	end
end)

function AutolootItem(objRef,quiet)
	if not(CanPickUp(objRef,quiet)) then
		return
	end

	local sourceContainer = objRef:TopmostContainer() or objRef
	if ( CheckCriminalLoot(this, sourceContainer) == false ) then
		return
	end

	--GW we may be dead from insta whack guards and the karma check
	if (IsDead(this)) then
		return
	end

	-- and we are wearing a backpack
	local backpackObj = this:GetEquippedObject("Backpack")
	if( backpackObj == nil ) then		
		return
	end	
	
	local topmostContainer = objRef:TopmostContainer()
	if (topmostContainer ~= nil) then
		if (not this:HasLineOfSightToObj(topmostContainer,ServerSettings.Combat.LOSEyeLevel)) then
			this:SystemMessage("Cannot see that.","info")
			return
		end
	end

	-- look for lootbags
	local lootBag = FindItemInContainer(backpackObj,function(containedObj)
			return containedObj:HasObjVar("LootBag") and not this:HasObjVar("locked")
		end)

	local targetContainer = lootBag or backpackObj
	--DebugMessage(1)
	if not(CanAddWeightToContainer(targetContainer,GetWeight(objRef))) then
		--DebugMessage(2)
		this:SystemMessage("Your backpack cannot hold any more weight!","info")			
		return
	end
	-- try to autostack
	if( TryStack(objRef, targetContainer) ) then
		local resourceType = objRef:GetObjVar("ResourceType")
		local num = 1
		if ( resourceType == "coins" ) then
			num = GetAmountStr(objRef:GetObjVar("Amounts"),false,true)
		elseif ( IsStackable(objRef) ) then
			num = GetStackCount(objRef)
		end
		this:NpcSpeech("[F4FA58]+"..num.." "..StripColorFromString(GetResourceDisplayName(resourceType)).."[-]","combat")
		return
	end

	local canHold, reason = TryPutObjectInContainer(objRef, targetContainer, objRef:GetLoc(), IsDemiGod(this) and not TestMortal(this), false)
	if( not canHold ) then
		if not(quiet) then
			this:SystemMessage("You cannot drop that there. "..(reason or ""),"info")
		end
		return
	else
		this:NpcSpeech("[F4FA58]Looted "..StripColorFromString(objRef:GetName()).."[-]","combat")
		if(objRef:DecayScheduled()) then
			objRef:RemoveDecay()
		end
	end
end

RegisterEventHandler(EventType.Timer,"autolootitem",
	function (objRef, containerObj)
		if( objRef:ContainedBy() == containerObj and CanPickUp(objRef)) then 
			AutolootItem(objRef)
		end
	end)

function HandleAutoLootCommand(usedObjectId)
	if( usedObjectId == nil ) then return end

	local usedObject = GameObj(tonumber(usedObjectId))
	if(usedObject:IsValid()) then
		-- if the item is in a corpse
		local topCont = usedObject:TopmostContainer() or usedObject
		if( topCont ~= this and CanPickUp(usedObject)) then
			ProgressBar.Show{TargetUser=this,Label="Looting",Duration=AUTOLOOT_DELAY}
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(AUTOLOOT_DELAY),"autolootitem",usedObject,usedObject:ContainedBy())
		-- otherwise if it is in one of the player's containers
		elseif( topCont == this and usedObject:ContainedBy() ~= this) then
			-- and its equippable
			if( GetEquipSlot(usedObject) ~= nil and GetEquipSlot(usedObject) ~= "Backpack") then
				DoEquip(usedObject,this)
			end
		end
	end
end

RegisterEventHandler(EventType.Timer,"autolootall",
	function (lootItems, containerObj)		
		local newLootItems = {}

		local looted = false
		for i,lootItem in pairs(lootItems) do
			if( lootItem:ContainedBy() == containerObj ) then
				if not(looted) then 
					AutolootItem(lootItem)
					looted = true
				else
					table.insert(newLootItems,lootItem)
				end
			end
		end

		if(#newLootItems > 0) then
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(AUTOLOOT_DELAY),"autolootall",newLootItems,containerObj)
		else
			ProgressBar.Cancel("Looting",this)
		end
	end)

RegisterEventHandler(EventType.Timer,"autolootall_timeout",
	function (args)
		this:RemoveTimer("autolootall")
	end)

function StartLootAll(targetContainer, onlyGold)

	if(targetContainer == nil) then
		return
	end

	if not(targetContainer:IsValid()) then
		return 
	end

	if not(targetContainer:IsContainer()) then 
		if( not onlyGold or ( onlyGold and targetContainer:GetObjVar("ResourceType") == "coins") ) then
			AutolootItem(targetContainer,false)
			return
		end
	end	

	local topmostContainer = targetContainer:TopmostContainer()
	if (topmostContainer ~= nil) then
		if (not this:HasLineOfSightToObj(topmostContainer,ServerSettings.Combat.LOSEyeLevel)) then
			this:SystemMessage("Cannot see that.","info")
			return
		end
	end

	local topmostContainer = targetContainer:TopmostContainer() or targetContainer
	if ( topmostContainer:HasObjVar("noloot")
	or topmostContainer:HasObjVar("guardKilled")
	or topmostContainer:HasObjVar("locked") ) then
		this:SystemMessage("You can't loot that.","info")
		return
	end

	if((targetContainer:HasObjVar("noloot")
			or targetContainer:HasObjVar("guardKilled")
			or targetContainer:HasObjVar("locked"))and (IsDemiGod(this) == false)) then
			this:SystemMessage("You can't loot that.","info")
		return
	end	

	if(topmostContainer:IsMobile() and not(IsDead(topmostContainer)))	 then
		this:SystemMessage("That's still alive.","info")
		return	
	end

	local lootItems = {}

	for i,objRef in pairs(targetContainer:GetContainedObjects()) do
		if(CanPickUp(objRef,true)) then
			if( not onlyGold or ( onlyGold and objRef:GetObjVar("ResourceType") == "coins") ) then
				table.insert(lootItems,objRef)
			end
		end
	end

	if(#lootItems == 0) then
		this:SystemMessage("You find nothing of value in that container.","info")
	else
		this:SendMessage("BreakInvisEffect", "Pickup")
		local lootDuration = AUTOLOOT_DELAY * #lootItems
		ProgressBar.Show({TargetUser=this,Label="Looting",Duration=TimeSpan.FromSeconds(lootDuration)})
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(AUTOLOOT_DELAY),"autolootall",lootItems,targetContainer)
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(lootDuration+AUTOLOOT_DELAY),"autolootall_timeout")
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(AUTOLOOT_DELAY),"ReturnToIdle", true, this:GetLoc(), targetContainer:GetLoc(), IsInCombat(this))

		if this:GetLoc():Distance2(targetContainer:GetLoc()) < 2 then
			if (IsInCombat(this)) then
				this:SendMessage("toggleCombat")
			end
			this:PlayAnimation("forage")
		end
	end
end

function HandleLootAllCommand(targetContainer)
	local targetContainerObj = GameObj(tonumber(targetContainer))
	if (not targetContainerObj:IsValid()) then return end

	if ( targetContainerObj:IsMobile() ) then
		local backpackObj = targetContainerObj:GetEquippedObject("Backpack")
		if ( backpackObj ~= nil ) then
			targetContainerObj = backpackObj
		end
	end

	StartLootAll(targetContainerObj)
end

function HandleEquipCommand(equippedObj)
	local equippedObj = GameObj(tonumber(equippedObj))
	local backpackObj = this:GetEquippedObject("Backpack")
	local equipSlot = GetEquipSlot(equippedObj)
	--DebugMessage("----- " .. tostring(topCont) .. " " .. tostring(equipSlot))
	-- first see if we are carrying it
	if( equipSlot ~= nil and equipSlot ~= "Backpack") then
		-- if its in our body then we have it equipped
		if( equippedObj:ContainedBy() == this ) then
			DoUnequip(equippedObj,this)
		elseif( equippedObj:ContainedBy() == backpackObj ) then
			DoEquip(equippedObj,this)
		end
	end
end

function HandleStuckCommand()
	if(( IsDead(this) and not this:HasObjVar("IsGhost") )
			or (HasMobileEffect(this, "GodFreeze"))) then
		return
	end
	
	this:SendMessage("BreakInvisEffect", "Action", "Stuck")

	local loc = this:GetLoc()
	local controller = Plot.GetAtLoc(loc)
	if ( controller ~= nil or IsGod(this) ) then
		UnstickPlayer(this)
	else
		this:SystemMessage("[$2409]","event")
		this:SetObjVar("stuckLoc", loc)
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(15),"StuckTimer")
	end
end

function HandleStuckTimer(args)	
	local stuckLoc = this:GetObjVar("stuckLoc")
	if ( this:GetLoc() == stuckLoc ) then
		UnstickPlayer(this)
	else
		this:SystemMessage("You have moved. Unstuck Canceled.", "info")
	end
	this:DelObjVar("stuckLoc")
end

-- This variable stops HandleCraftItem() commands in the same frame from being processed
local canCraftItem = true

function HandleCraftItem(recipe, variation, skill)
	--DebugMessage("HandleCraftItem [PLAYER.LUA]")
	USAGE_DISTANCE = 5

	if (this:HasTimer("CraftingTimer") or canCraftItem ~= true ) then
		this:SystemMessage("You must wait to craft that.","info")
		return
	end

	local nearbyTool = nil

	local backpack = this:GetEquippedObject("Backpack")
	if ( backpack ) then
		nearbyTool = FindItemInContainerRecursive(backpack, function(fobj)
			if fobj:HasModule("tool_base") and fobj:HasObjVar("ToolSkill") then
				local skilltype = fobj:GetObjVar("ToolSkill")
				if(skilltype == skill or skilltype == "All") then
					return fobj;
				end
			end
		end)
	end

	if(nearbyTool == nil) then 
		local tools = FindObjects(SearchMulti({
		SearchModule("tool_base"),
		SearchHasObjVar("ToolSkill"),
		SearchRange(this:GetLoc(),USAGE_DISTANCE),
		}))

		for i,j in pairs(tools) do
			if (j:GetObjVar("ToolSkill") == skill or j:GetObjVar("ToolSkill") == "All") then
				nearbyTool = j
			end
		end
	end

	if (nearbyTool ~= nil) then
		--check here to see if tool is nearby
		--DebugMessage("Recipe is "..tostring(recipe).." Variation is " .. tostring(variation).." Skill is "..tostring(skill))
		if( not this:HasModule("base_crafting_controller") ) then
			this:AddModule("base_crafting_controller")
		end
		canCraftItem = false
		this:SendMessage("CraftItem", this,recipe,variation,skill,nearbyTool)
		CallFunctionDelayed(TimeSpan.FromMilliseconds(250),function() canCraftItem = true end)
	else
		this:SystemMessage("[$2411]","info")
	end
end

function CheckKillAchievements(victim, damageAmount, damageType, damageSource)
	if (victim == nil or not victim:IsValid()) then
		return
	end

	local killTable = this:GetObjVar("LifetimePlayerStats")
	if (killTable == nil) then
		killTable = {}
		killTable.Players = 0
		killTable.TotalMonsterKills = 0
	end

	--First, check for PvP achievements
	if (IsPlayerCharacter(victim) == true) then

		if not( IsCriminal(victim) ) then
			if (killTable.BluePlayers == nil) then
				killTable.BluePlayers = 0
			end

			killTable.BluePlayers = killTable.BluePlayers + 1
			Quests.SendQuestEventMessage( this, "PVP", { "BluePlayer" }, 1 )

			CheckAchievementStatus(this, "PvP", "VersusBlue", killTable.BluePlayers, nil, "PvP")
		elseif ( IsMurderer(victim) ) then
			if (killTable.RedPlayers == nil) then
				killTable.RedPlayers = 0
			end

			killTable.RedPlayers = killTable.RedPlayers + 1
			Quests.SendQuestEventMessage( this, "PVP", { "RedPlayer" }, 1 )

			CheckAchievementStatus(this, "PvP", "VersusRed", killTable.RedPlayers, nil, "PvP")
		end

		killTable.Players = killTable.Players + 1
		Quests.SendQuestEventMessage( this, "PVP", { "Player" }, 1 )
		CheckAchievementStatus(this, "PvP", "VersusAny", killTable.Players, nil, "PvP")
		this:SetObjVar("LifetimePlayerStats",killTable)
	--Then check for executioner achievements
	else
		if( victim:HasObjVar("DynamicSpawner") ) then
			if( victim:HasObjVar("DynamicSpawnerBoss") ) then
				Quests.SendQuestEventMessage( this, "DynamicSpawn", { "KilledBoss" }, 1 ) 
			end
			Quests.SendQuestEventMessage( this, "DynamicSpawn", { "KilledMonster" }, 1 )
		end

		local conflicts = GetConflictTable( victim )
		local hitByGuard = false
		for mobileObj,data in pairs(conflicts) do
			if( mobileObj:IsValid() and mobileObj:HasObjVar("IsGuard") ) then
				hitByGuard = true
			end
		end

		if( not hitByGuard ) then
			local type = victim:GetObjVar("MobileTeamType")
			local kind = victim:GetObjVar("MobileKind")
			local template = victim:GetCreationTemplateId()
			Quests.SendQuestEventMessage( this, "Hunting", { template, type }, 1 )
			if ( not killTable["CreatureKills"] ) then killTable["CreatureKills"] = {} end
			if ( not killTable["CreatureKills"]["TeamType"] ) then killTable["CreatureKills"]["TeamType"] = {} end
			if ( not killTable["CreatureKills"]["Kind"] ) then killTable["CreatureKills"]["Kind"] = {} end
			if ( not killTable["CreatureKills"]["Template"] ) then killTable["CreatureKills"]["Template"] = {} end
			if ( type ) then killTable["CreatureKills"]["TeamType"][type] = (killTable["CreatureKills"]["TeamType"][type] or 0) + 1 end
			if ( kind ) then killTable["CreatureKills"]["Kind"][kind] = (killTable["CreatureKills"]["Kind"][kind] or 0) + 1 end
			if ( template ) then killTable["CreatureKills"]["Template"][template] = (killTable["CreatureKills"]["Template"][template] or 0) + 1 end
			this:SetObjVar("LifetimePlayerStats",killTable)
		end
		
	end
end

--SCAN DISABLED MACRO SYSTEM
function HandlePlayMacro( macroId ) 
	--DebugMessage(" Play Macro " .. macroId)
	--StartMobileEffect(this, "PlayMacro", this, { MacroID = macroId })
	this:SystemMessage("The Macro system has been disabled on this community server.")
end

function HandleChallengeCommand()
	this:SendMessage("StartMobileEffect", "ChallegeSystem")
end

RegisterEventHandler(EventType.RequestPickUp, "", HandleRequestPickUp)
RegisterEventHandler(EventType.RequestDrop, "", HandleRequestDrop)
RegisterEventHandler(EventType.RequestEquip, "", HandleRequestEquip)

RegisterEventHandler(EventType.ClientUserCommand,"",function (eventId, ... )
	if( this:HasObjVar("LogCommands") ) then
		local commandString = {}
		local args = table.pack(...)
    	
		table.insert(commandString, "ObjectID : " .. tostring(this.Id) )
		table.insert(commandString, "UnixTS : " .. tostring( os.time(os.date("!*t")) ))
		table.insert(commandString, "EventID : " .. tostring( eventId ) )

		for i=1,args.n do
			table.insert(commandString, tostring(args[i]) )
		end

		outfile = io.open("logs/ClientUserCommandLog.txt", "a")
		outfile:write(CombineString(commandString, " "))
		outfile:write("\n")
		outfile:close()
		--DebugMessage( CombineString(commandString, " ") )
	end
	
	

end)

RegisterEventHandler(EventType.ClientUserCommand, "CraftItem", HandleCraftItem)
--RegisterEventHandler(EventType.Message, "CraftItemPlayer", HandleCraftItem)

RegisterEventHandler(EventType.ClientUserCommand, "unlockstat", HandleUnlockStatRequest)
RegisterEventHandler(EventType.ClientUserCommand, "lockstat", HandleLockStatRequest)
RegisterEventHandler(EventType.ClientUserCommand, "use", HandleUseCommand)
RegisterEventHandler(EventType.ClientUserCommand, "usepermanent", HandleUsePermanentCommand)
RegisterEventHandler(EventType.ClientUserCommand, "useresource", HandleUseResourceCommand)
RegisterEventHandler(EventType.ClientUserCommand, "autoloot", HandleLootAllCommand)
RegisterEventHandler(EventType.ClientUserCommand, "lootall", HandleLootAllCommand)
RegisterEventHandler(EventType.ClientUserCommand, "equip", HandleEquipCommand)
RegisterEventHandler(EventType.ClientUserCommand, "stuck", HandleStuckCommand)
RegisterEventHandler(EventType.ClientUserCommand, "playmacro", HandlePlayMacro)
RegisterEventHandler(EventType.ClientUserCommand, "challenge", HandleChallengeCommand)

OverrideEventHandler("base_mobile", EventType.Message, "UpdateName", UpdateName)
RegisterEventHandler(EventType.Message, "PickupObject", HandleRequestPickUp)
RegisterEventHandler(EventType.Message, "UpdateTitle", UpdateTitle)
RegisterEventHandler(EventType.Message, "VictimKilled", CheckKillAchievements)
RegisterEventHandler(EventType.Message, "LootAll", StartLootAll)

RegisterEventHandler(EventType.Timer, "StuckTimer", HandleStuckTimer)
RegisterEventHandler(EventType.Message,"SystemMessage",function (message,type)
	this:SystemMessage(message,type)
	if(type=="event") then
		this:SystemMessage(message)
	end
end)

RegisterEventHandler(EventType.Message, "TryHarvest",
	function(objRef)
		TryHarvestItem(objRef)
	end)

RegisterEventHandler(EventType.StartMoving,"",
	function (speedModifier)
		if( this:GetObjVar("IsHarvesting") ) then		
			-- this is a messy hack since the tool itself does the gathering right now
			local weapon = this:GetEquippedObject("RightHand")
			if( weapon ~= nil and weapon:HasObjVar("ToolType") ) then
				weapon:SendMessage("CancelHarvesting",this)
			end
			this:SendMessage("CancelHarvesting",this)
		end

		if( this:HasTimer("autolootitem")) then
			this:RemoveTimer("autolootitem")
			ProgressBar.Cancel("Looting",this)
		end
		if( this:HasTimer("autolootall")) then
			this:RemoveTimer("autolootall")
			this:RemoveTimer("autolootall_timeout")
			ProgressBar.Cancel("Looting",this)
		end		
	end)

--If the player stands near a camfire, they will set down after a delay
RegisterEventHandler(EventType.StopMoving,"",
	function ()
		if (HasMobileEffect(this, "Campfire")) then
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(5), "SitCampfire")
		end
	end)

RegisterEventHandler(EventType.Timer, "SitCampfire", 
	function()
		if (HasMobileEffect(this, "Campfire") and this:IsMoving() == false and not IsMounted(this)) then
			this:PlayAnimation("sit_ground")
		end
	end)

RegisterEventHandler(EventType.Timer, "ReturnToIdle", 
	function(wasKneeling, previousLoc, interactableLoc, previousCombatStance)
		local playerLoc = this:GetLoc()
		local distanceMoved = previousLoc:Distance2(playerLoc)
		local distanceFromInteractable = 0
		if interactableLoc then distanceFromInteractable = interactableLoc:Distance2(playerLoc) end
		local stillKneeling = wasKneeling and not (distanceMoved > 0) and not (distanceFromInteractable > 2) and not IsInCombat(this)

		if (previousCombatStance == true and not IsInCombat(this)) then
			this:SendMessage("toggleCombat")
		end
		
		if not stillKneeling then return else this:PlayAnimation("kneel_standup") end
	end)

-- Initialization Code

RegisterEventHandler(EventType.Message,"DamageInflicted",function(damager, damageAmount, damageType, isCrit, wasBlocked, isReflected)
	if ( this:HasTimer("StuckTimer") ) then
		this:RemoveTimer("StuckTimer")
		this:SystemMessage("Damage taken, Unstuck Canceled.", "info")
	end
end)
----------------------------------------------------------------------

function AddDynamicWindowRangeCheck(targetObj,windowHandle,maxDistance)
	--DebugMessage("AddDynamicWindowRangeCheck",tostring(targetObj),tostring(windowHandle),tostring(maxDistance))
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(2),"CheckWindow-"..windowHandle)

	RegisterEventHandler(EventType.Timer, "CheckWindow-"..windowHandle, 
		function ()
			--DebugMessage("CheckWindow-"..windowHandle.." fired.")
			maxDistance = maxDistance or OBJECT_INTERACTION_RANGE
			
			if (targetObj == nil or not(targetObj:IsValid())) then	
				this:CloseDynamicWindow(windowHandle)
				UnregisterEventHandler("player",EventType.Timer,"CheckWindow-"..windowHandle)
			else
				if (targetObj:DistanceFrom(this) < (maxDistance)) then
					this:ScheduleTimerDelay(TimeSpan.FromSeconds(2),"CheckWindow-"..windowHandle)
				else
					this:CloseDynamicWindow(windowHandle)
					UnregisterEventHandler("player",EventType.Timer,"CheckWindow-"..windowHandle)
				end
			end
		 end)
end

RegisterEventHandler(EventType.Message,"DynamicWindowRangeCheck",
	function(...)
		AddDynamicWindowRangeCheck(...)
	end)

RegisterEventHandler(EventType.Message,"OpenBank",
	function (bankSource)
		local bankObj = this:GetEquippedObject("Bank")
		this:SetObjVar("BankSource",bankSource)
		if( bankObj ~= nil ) then
			WarnContainerOverflow(bankObj, this)
			bankObj:SendOpenContainer(this)					
		end		

		this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"BankCheck")
	end)

RegisterEventHandler(EventType.Timer,"BankCheck",
	function ( ... )
		if not(IsBankerInRange(this)) then
			local bankObj = this:GetEquippedObject("Bank")
			if( bankObj ~= nil ) then
				--DebugMessage("Bank closed")
				CloseContainerRecursive(this,bankObj)
				CloseMap()
			end
		else
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"BankCheck")
		end
	end)

RegisterEventHandler(EventType.Message,"BindToLocation",
	function(targetLoc,quiet)
		this:SetObjVar("SpawnPosition",{Region=ServerSettings.RegionAddress,Loc=targetLoc})
		if not(quiet) then
			this:SystemMessage("Your spirit has become bound to this location.","event")
			this:PlayEffect("TeleportFromEffect")
		end
	end)


-- Move to globals
-----



function CreateStartingItems(playerObj)
	-- build the list first so we don't create duplicates
	local itemsToCreate = {}
	for i, skillItemInfo in pairs(CharacterCustomization.StartingItems) do
		if( skillItemInfo.Skill == "" or GetSkillLevel(playerObj,skillItemInfo.Skill) > 0) then
			for i, itemInfo in pairs(skillItemInfo.Items) do
				local template = nil
				local stackCount = 1
				if(type(itemInfo) == "table") then
					template = itemInfo[1]
					stackCount = itemInfo[2]
				else
					template = itemInfo
				end

				itemsToCreate[template] = stackCount
			end
		end
	end

	if not(initializer) then initializer = {} end
	if not(initializer.HotbarActions) then initializer.HotbarActions = {} end

	local nextHotbarItemSlot = 21

	-- DAB TODO: Place items intelligently
	-- only equip one item per slot
	local slotsEquipped = {}
	for itemTemplate, stackCount in pairs(itemsToCreate) do
		--DebugMessage("CreateStartingItems",tostring(itemTemplate),tostring(stackCount))
		if(stackCount > 1) then
			--DebugMessage("CreateStack",tostring(itemTemplate),tostring(stackCount))
			Create.Stack.InBackpack(itemTemplate, playerObj, stackCount, nil, function(stackObj)
				if ( stackObj ) then stackObj:SetObjVar("Worthless",true) end
			end)

			local templateData = GetTemplateData(itemTemplate)            
			if(itemTemplate ~= "arrow") then
				table.insert(initializer.HotbarActions,{ Slot=nextHotbarItemSlot, Type="Object", Name=itemTemplate} )
				nextHotbarItemSlot = nextHotbarItemSlot + 1
			end
		else
			local templateData = GetTemplateData(itemTemplate)
            local equipSlot = templateData.SharedObjectProperties.EquipSlot
            
            -- exclude hunting knife
            if(equipSlot and not(equipSlot == "Trade") and itemTemplate ~= "tool_hunting_knife" and not(slotsEquipped[equipSlot])) then
            	local equippedObj = this:GetEquippedObject(equipSlot)
            	if(equippedObj ~= nil) then
            		DoUnequip(equippedObj, this)
            	end

				Create.Equipped(itemTemplate, playerObj, function(stackObj)
					if ( stackObj ) then stackObj:SetObjVar("Worthless",true) end
				end)
            	slotsEquipped[equipSlot] = true
			else
				Create.InBackpack(itemTemplate, playerObj, nil, function(stackObj)
					if ( stackObj ) then
						stackObj:SetObjVar("Worthless",true)
						if ( stackObj:IsContainer() ) then
							CallFunctionDelayed(TimeSpan.FromSeconds(1),function()
								ForEachItemInContainerRecursive(stackObj, function(insideObj)
									if ( insideObj ) then
										insideObj:SetObjVar("Worthless",true)
										return true
									end
								end, 10)
							end)
						end
					end
				end)
			end

			-- arrows and armor are the only item you dont really need on your hotbar
			local armorType = templateData.ObjectVariables and templateData.ObjectVariables.ArmorType
			if(not(armorType) and itemTemplate ~= "tool_hunting_knife" ) then
				table.insert(initializer.HotbarActions,{ Slot=nextHotbarItemSlot, Type="Object", Name=itemTemplate} )
				nextHotbarItemSlot = nextHotbarItemSlot + 1
			end

			if(itemTemplate == "spellbook_noob") then
				table.insert(initializer.HotbarActions,{ Slot=1, Type="Spell", Name="Heal"})
            	table.insert(initializer.HotbarActions,{ Slot=2, Type="Spell", Name="Ruin"})
            	table.insert(initializer.HotbarActions,{ Slot=3, Type="Spell", Name="ManaMissile"})
			end
		end
	end
end

-----

-- On clusters that run the login region, this should not happen until they have entered the world (after character creation)
function InitializePlayer()
	--DebugMessage("Initialize Player")

	--this:AddModule("guard_protect")

	-- new character, past auto fixes need not apply
	this:SetObjVar("LastAutoFix", #AutoFixes)

	if not(IsImmortal(this)) then
		this:AddModule("temp_afkkick")	

		ShowTutorialUI(this)

		if(ServerSettings.NewPlayer.InitiateSystemEnabled) then		
			this:SetObjVar("InitiateMinutes", ServerSettings.NewPlayer.InitiateDurationMinutes)
			this:AddModule("npe_player")
		end

		AddSubMapByName(this, ServerSettings.SubregionName)

	else
		this:SetObjVar("Invulnerable",true)
	    -- show the welcome dialog here since 
		-- mortals get it when they complete the starting quest
		--ShowWelcomeDialog()			
	end

	-- starting template gets set from the login region				
	if(this:HasObjVar("CharCreateNew")) then
		CreateStartingItems(this)
		this:DelObjVar("CharCreateNew")		
	-- if it's not set let them pick their appearance
	else
		this:AddModule("custom_char_window")
	end

    this:AddModule("tool_barehands")
    
    -- default new characters to have their criminal protection toggled to true
    this:SetObjVar("CriminalProtect", true)

	-- bind their hearth for the first time
	BindLocationIfNot(this)

	if not( this:HasObjVar("CreationDate") ) then 
		this:SetObjVar("CreationDate", DateTime.UtcNow)
	end

	--Check for achievement for every skills on character creation
	CheckAchievementStatusAllSkills(this)

	-- Check Challenge System
	ChallengeSystemHelper.DoUpkeep( this )
end

-- This gets called on both creation and loading from backup
function OnLoad(isPossessed)
	if not(isPossessed) then
		if(not(this:HasObjVar("playerInitialized"))) then
			InitializePlayer()
			this:SetObjVar("playerInitialized",true)
		end

		--DAB/DFB hack: If the position is not valid send them to the outcast spawn position.
		if (not this:GetLoc():IsValid()) then
			local spawnPosition = FindObjectWithTag("OutcastSpawnPosition")
			if (spawnPosition == nil) then
				this:SetWorldPosition(GetPlayerSpawnPosition(this))
			else
				this:SetWorldPosition(spawnPosition:GetLoc())
			end
		end

		-- Check if player will be logged-in to a Stranger's House and kick them off plot, if so.
		Plot.KickIfStranger(nil, this)

		this:SetObjVar("LoginTime",DateTime.UtcNow)
		bankBox = this:GetEquippedObject("Bank")
		if( bankBox == nil ) then
			CreateEquippedObj("bank_box", this)
		end

		tempPack = this:GetEquippedObject("TempPack")
		if( tempPack == nil ) then
			CreateEquippedObj("crate_empty", this, "temppack_created")
			-- create the key ring inside the temp pack
			RegisterSingleEventHandler(EventType.CreatedObject,"temppack_created",
				function (success,objRef)
					objRef:SetSharedObjectProperty("Weight",-1)
					objRef:SetName("Internal Temp Pack")
					CheckKeyRing(objRef)
				end)
		else
			CheckKeyRing()
		end	
	
		ShowSkillTracker()
		if not this:HasObjVar("LifetimePlayerStats") then
			
			local newLifetimeStats = 
			{
			Players = 0,
			TotalMonsterKills = 0,
			}

			this:SetObjVar("LifetimePlayerStats",newLifetimeStats)
		end

		this:SendClientMessage("PlayerRunSpeeds",
			{
				ServerSettings.Stats.WalkSpeedModifier,
				ServerSettings.Stats.RunSpeedModifier
			})
		this:SetMaxMoveSpeedModifier(ServerSettings.Stats.RunSpeedModifier)

		--UpdateFactions()

		this:SetStatValue("PrestigeXPMax",ServerSettings.Prestige.PrestigePointXP)

		if ( this:GetSharedObjectProperty("IsSneaking") ) then
			StartMobileEffect(this, "Hide", nil, {Force = true})
		end

	else
		this:SendClientMessage("PlayerRunSpeeds",
			{
				1.0,
				this:GetObjVar("AI-ChargeSpeed") or 2.0
			})
	end

	this:SetObjectTag("AttachedUser")

	-- send skill values to player
	SendSkillList()

	UpdateClientSkill(this,this)

	--refresh the quest UI
	--this:SendMessage("RefreshQuestUI")	

	SendTimeUpdate(this)	

	InitializeHotbar()

	if(UpdateWeatherViews) then
		UpdateWeatherViews()
	end

    this:SendMessage("LoggedIn")
	
	-- DAB TODO: Sometimes EnterView will fire on another object (teleporter) before this fires
	-- some scripts might not want to act on someone who has just entered the world
	this:ScheduleTimerDelay(TimeSpan.FromSeconds(5),"EnteringWorld")

	this:DelObjVar("BuffIcons")

	--ApplyPersistentMobileEffects(this)

	-- Delayed OnLoadStuff
	CallFunctionDelayed(TimeSpan.FromSeconds(0.5),
		function()		
			if(initializer and initializer.HotbarActions) then
				BuildHotbar(initializer.HotbarActions)
			end						

			-- These functions are found in globals/dynamic_window/hud
			UpdateHotbar(this)
			UpdateSpellBar(this)
			UpdateItemBar(this)
			UpdateBloodlust(this)
			ShowStatusElement(this,{IsSelf=true,ScreenX=10,ScreenY=10})

			-- this technically does not need to be called every time you come from the backup
			UpdateFixedAbilitySlots()

			if not( isPossessed ) then
				InitializeClientConflicts(this)
				UpdateName()

				if ( IsMounted(this) and this:IsInRegion("NoMount") ) then
					DismountMobile(this)
				end			
			end
		end)

	-- GW TODO we need better handling of client messages in general, for ones that expect client objects to be constructed and started by time they are fired.
	CallFunctionDelayed(TimeSpan.FromSeconds(10.0),
		function()		
			if this:HasObjVar("AchievementWaiting") then
				this:SendClientMessage("SetAchievementNotification",true)
			end

			--start newbie quest
			Quests.TryStart(this, "StartAProfQuest")
		end)

	local equippedObjects = this:GetAllEquippedObjects()
	for i=1,#equippedObjects do
		if not(equippedObjects[i]:GetCreationTemplateId()) then
			equippedObjects[i]:Destroy()
			DebugMessage("ERROR: Destroying invalid equipped item. "..equippedObjects[i].Id)
		end
	end

	this:ScheduleTimerDelay(TimeSpan.FromSeconds(5 + math.random()),"UpdateChatChannels")

	Militia.UpdatePlayerVars(this)
	Militia.UpdateTitle(this)

	UpdatePlayerProtection(this,true)

	--update quest map markers
	local activeQuests = Quests.GetActive(this)
	for questName, quest in pairs(activeQuests) do
		Quests.UpdateQuestMarkers(this, questName, true)
	end

	this:DelObjVar("HomeUniverse")
end
RegisterEventHandler(EventType.Message,"OnLoad",function(...) OnLoad(...) end)

function CheckStartingQuest()
	local worldName = ServerSettings.WorldName
	if(worldName == "Limbo" or worldName == "Celador") then
		--this:SendMessage("StartQuest","StartingQuest")
	end

	-- if we didn't start in limbo, skip right to find the mayor
	if(worldName == "Celador") then
		this:SetObjVar("ChoseClass","Warrior")
		CallFunctionDelayed(TimeSpan.FromSeconds(1),function() this:SendMessage("AdvanceQuest","StartingQuest","FindMayor") end)
	end
end	

RegisterSingleEventHandler(EventType.ModuleAttached,"player",
	function()
		local attachType = initializer.AttachType or "Player"

		if(ServerSettings.WorldName ~= "Login") then
			OnLoad(attachType == "Possess")
		end		
	end)

RegisterSingleEventHandler(EventType.LoadedFromBackup,"",
	function()
		local isPossessed = IsPossessed(this)
		if(isPossessed and not(this:GetAttachedUserId())) then
			EndPossess(this)
		end

		OnLoad(isPossessed)
		
		if not(isPossessed) then
			-- do auto fix.
			CallFunctionDelayed(TimeSpan.FromSeconds(1), function()
				-- in new frame
				DoPlayerAutoFix(this)
			end)

			--If we have any items in our trade pouch for some reason then move them back into our backpack.
			local tradePouch = this:GetEquippedObject("Trade")
			if (tradePouch ~= nil) then			
			    local myPackItems = tradePouch:GetContainedObjects()
				local backpackObj = this:GetEquippedObject("Backpack")
			    if (myPackItems ~= nil) then
					for i,j in pairs(myPackItems) do
				  		local randomLoc = GetRandomDropPosition(backpackObj)
				  		j:MoveToContainer(backpackObj,randomLoc)
					end
				end
				tradePouch:Destroy()
			end
		end
	end)


function DoLogout(logoutType)
	this:SendMessage("BreakInvisEffect", "Action", "Logout")

    -- body
	this:DelObjVar("StatMods")
	--this:DelObjVar("MapMarkers")

	if(logoutType == "Transfer") then 
		this:SetObjVar("TransferTime",DateTime.UtcNow)
		this:SetObjVar("TransferSource",ServerSettings.RegionAddress)
	elseif (logoutType == "Disconnect") then

		if not( IsPossessed(this) ) then
			local clusterController = GetClusterController()
			if ( clusterController ) then
				clusterController:SendMessage("UserLogout", this)
			end
		end

		local isOnTheRun = HasMobileEffect(this, "OnTheRun")
		local inConflict = HasAnyActiveConflictRecords(this)

		local logoutTimer = nil
		--gods log out instantly
		if (IsImmortal(this)) then
			logoutTimer = TimeSpan.FromSeconds(2)
		-- if out of combat and in an in log out instantly
		elseif ( (this:IsInRegion("WorldInns") or Plot.IsInHouse(this,true)) and not(inConflict) ) then
			logoutTimer = TimeSpan.FromSeconds(2)
		-- else if not in conflict or on the run
		elseif not(inConflict) and not(isOnTheRun) then
			logoutTimer = TimeSpan.FromSeconds(30)
		-- else if in conflict but not on the run
		elseif not(isOnTheRun) then
			logoutTimer = TimeSpan.FromMinutes(1)
		end
		-- else on the run then wait the full timer (5 minutes)

		if(logoutTimer) then
			CallFunctionDelayed(logoutTimer,function() this:CompleteLogout() end)
		end
	end

	if ( logoutType ~= "Transfer" ) then
		EventTracking.UpdatePlayerRecord(this)
	end
end
RegisterEventHandler(EventType.Message,"DoLogout",function (...) DoLogout(...) end)

RegisterEventHandler(EventType.UserLogout,"", 
    function (logoutType)
        if ( logoutType == "Transfer" and not IsImmortal(this) ) then
            this:SetObjVar("Invulnerable", true)
        end

		-- if holding something
		local carriedObject = this:CarriedObject()
		if ( carriedObject ~= nil and carriedObject:IsValid() ) then
			-- try to put into backpack
			local backpack = this:GetEquippedObject("Backpack")
			if ( backpack and CanAddWeightToContainer(backpack, carriedObject:GetSharedObjectProperty("Weight")) ) then
				carriedObject:MoveToContainer(backpack,GetRandomDropPosition(backpack))
			else
				-- or drop to ground if backpack cannot hold
				carriedObject:SetWorldPosition(this:GetLoc())
			end
		end

		-- DAB TODO: Allow players to take possessed objects across server lines
		-- if we are possessed we need to restore the AI on this creature and forward the logout message
		-- to the actual owner
		local possessee = GetPossessee(this)
		if(possessee) then
			EndPossess(possessee)
		end

		DoLogout(logoutType)
	end)

RegisterEventHandler(EventType.UserLogin,"",
	function(loginType)
		

        if not( IsImmortal(this) ) then
			this:DelObjVar("Invulnerable")
			this:DelObjVar("NameColorOverride")
		end
		
		UpdateName()

		if not( IsPossessed(this) ) then
			local clusterController = GetClusterController()
			if ( clusterController ) then
				clusterController:SendMessage("UserLogin",this,loginType)			
			end
			if ( loginType == "Connect" ) then
				Guild.Initialize()
				-- warn about their plot taxes
				Plot.DailyTaxWarn(this)
                -- force them to peaceful
                if not( IsMurderer(this) ) then
                    ForcePeaceful(this)
                end
			end
		end

		if(loginType == "ChangeWorld") then
			if (ServerSettings.WorldName == "Catacombs") then
				CheckAchievementStatus(this, "Activity", "Dungeon", 1)
			end

			-- Close their BANK if they change worlds
			local bankObj = this:GetEquippedObject("Bank")
			if( bankObj ~= nil ) then
				--DebugMessage("Bank closed")
				CloseContainerRecursive(this,bankObj)
				CloseMap()
			end

			return
		end

		if(ServerSettings.WorldName == "Catacombs") then
			local sendto = GetRegionAddressesForName("SouthernHills")
			if not(#sendto == 0 or not IsClusterRegionOnline(sendto[1])) then
				if(GetKarma(this) > 0) then
					local sendto = GetRegionAddressesForName("UpperPlains")
		    		TeleportUser(this,this,MapLocations.NewCelador["Upper Plains: The Dead Gate"],sendto[1], 0, true, false, true)
		    	else		    		
			        TeleportUser(this,this,MapLocations.NewCelador["Southern Hills: Catacombs Portal"],sendto[1], 0, true, false, true)
			    end
			end			
		end
	end)

RegisterEventHandler(EventType.Message,"UpdateChatChannels",
	function()
		UpdateChatChannels()
	end)
RegisterEventHandler(EventType.Timer,"UpdateChatChannels",
	function()
		UpdateChatChannels()
	end)

-- update all players on login, kept running into missing player record problems, it's worth it to prevent missing data.
--- the event a player would login, kill some stuff (thus being a part of death events) and server crashed there would be no
-- player record and since we need to parse chronologically we can't continue without the player record.
EventTracking.UpdatePlayerRecord(this)

RegisterEventHandler(EventType.Message,"UpdateMilitiaObjVars",
	function()
		Militia.UpdatePlayerVars(this)
	end)

RegisterEventHandler(EventType.Message,"ApplyMilitiaBuffs",
	function()
		Militia.ApplyBuffs(this)
	end)

RegisterEventHandler(EventType.Message,"MilitiaEventMessage",
	function(message)
		this:SystemMessage(message, "event")
	end)
RegisterEventHandler(EventType.Message,"Militia.Chat",
	function(name,broadcast,rankString,line)
		if(name and rankString ) then
			if ( broadcast and broadcast == true ) then
				this:SystemMessage( "[ff9900][Militia] "..name.." "..rankString..": " .. line.."[-]","event")
			else
				this:SystemMessage( "[ff9900][Militia] " .. name .." "..rankString..": " .. line.."[-]","custom")
			end
		else
			this:SystemMessage( "[ff9900][Militia] " .. line.."[-]","custom")
		end
	end)

--MALEVOLENT MOD 
-- SCAN, ADDED
RegisterEventHandler(EventType.Message,"World.Chat",
	function(name,title,msg)
			this:SystemMessage( "[57FFBC][World] " .. name .. " : " .. msg .."[-]","custom")  
	end)


	function UpdateFixedAbilitySlots( _primaryAction, _secondaryAction )
	-- setup initial weapon abilites.
	local curAction = _primaryAction or GetWeaponAbilityUserAction(this, true)
	AddUserActionToSlot(curAction)
	curAction = _secondaryAction or GetWeaponAbilityUserAction(this, false)
	AddUserActionToSlot(curAction)

	UpdateAllPrestigeAbilityActions(this)
end
RegisterEventHandler(EventType.Message,"MilitiaApplyBuffs",
	function()
		Militia.ApplyBuffs(this)
	end)
RegisterEventHandler(EventType.Message,"AcceptDuelChallenge",
	function(user)
		Duel.Summon(this, user)
	end)
RegisterEventHandler(EventType.Message,"UpdateFixedAbilitySlots",UpdateFixedAbilitySlots)


-- Conflict stuff
RegisterEventHandler(EventType.Message, "ClearClientConflict", function(mobile)
	this:SendClientMessage("UpdateMobileConflictStatus", { { mobile, "" } })
end)

-- Prestige stuff.
RegisterEventHandler(EventType.Timer, "CastPrestigeAbility", CompleteCastPrestigeAbility)
RegisterEventHandler(EventType.Message, "AddPrestigeXP", function(amount)
	AddPrestigeXP(this,tonumber(amount))
end)

-- This is the player tick, it's performed once per minute.
-- It's the alternative to having multiple systems all updating under their own timers.
function PerformPlayerTick(notFirst, playMinutes)
	ProfessionsHelpers.ApplyTraits( this )
	
	-- prevent logins and reloads taking minutes away from initiate status
	if ( notFirst ) then
		-- check initiate
		CheckInitiate(this)
	end

	VitalityCheck(this)

	CheckBidRefund()

	CheckGmMessage(this)

	TryPenitentExpire( this )

	UpdateMurderCountExpire(this, playMinutes)

	-- Checks every minute if challenge quests can be re-started
	ChallengeSystemHelper.DoUpkeep( this )

end

function PerformPlayerShortTick()
	UpdatePlayerProtection(this)

	local newRegionalName = GetRegionalName(this:GetLoc())
	if(newRegionalName ~= nil and newRegionalName ~= currentRegionalName) then
		currentRegionalName = newRegionalName
		this:SystemMessage("You have entered "..currentRegionalName,"event")
		this:SendMessage("EnteredRegionalName", currentRegionalName)
		UpdateRegionStatus(this,currentRegionalName)
	end

	ChaosZoneHelper.ChaosZoneCheck( this )

	Quests.UpdateActive(this)
end

local IsInAwakening = false
function PerformPlayerFourTick()	
	CheckCorpseValid(this)

	friendList = this:GetObjVar("FriendList")

	local friendDifferent = false

	for i = 1, #friendList, 1 do
		local friend = friendList[i].Friend
		local friendOnline = friendList[i].Online
		local friendInBothList = friendList[i].InBothList
		local onlineStatus = GlobalVarReadKey("User.Online", friend)
		local appearOffline = GlobalVarReadKey("User.AppearOffline", friend)

		if not (friendOnline) and onlineStatus and friendInBothList and not appearOffline then
			--If friend is in a same region and friend deleted me from their friend list
			if (friend:IsValid() and not IsInFriendList(friend, this)) then
				friendList[i].InBothList = false
				friendDifferent = true
			elseif not (friend:IsValid()) then
				--This global message should be only sent if friend logs in and not in the same region
				friend:SendMessageGlobal("CheckPlayerInFriendList", this)
			else
				friendList[i].Online = true
				friendDifferent = true
			end
		elseif (friendOnline) and (not onlineStatus or not friendInBothList or appearOffline) then
			friendList[i].Online = false
			RemoveFriendFromChatChannel(this, friend)
			friendDifferent = true
		end
	end

	if (friendDifferent) then
		this:SetObjVar("FriendList", friendList)
		if (this:HasModule("friend_ui")) then
			this:SendMessage("FriendUpdate")
		end
	end

	if(not(IsInAwakening) and Awakening.IsInAwakening(this)) then
		--this:SendMessage("UpdateMissionUI")
		this:SendMessage("UpdateAwakeningUI")
		this:SystemMessage("Entering Awakening Area", "info")
		IsInAwakening = true
	elseif(IsInAwakening and not(Awakening.IsInAwakening(this))) then
		this:SendMessage("UpdateAwakeningUI")
		this:SystemMessage("Leaving Awakening Area", "info")
		IsInAwakening = false
	end
end

local checkingBidRefund = false
function CheckBidRefund()
	if not( checkingBidRefund ) then
		checkingBidRefund = true
		-- see if there are any auctions I didn't win
		Plot.CheckBidRefund(this, function()
			checkingBidRefund = false
		end)
	end
end

-- using an event handler so we can mark local checkingBidRefund and prevent a double whammy
RegisterEventHandler(EventType.Message, "CheckBidRefund", CheckBidRefund)

RegisterEventHandler(EventType.ClientUserCommand, "karmastate", function(alignment)

	-- If the player is an initiate we want to warn them if they
	-- try to go criminal.
    if( alignment == "Unlawful" ) then
        if ( IsMurderer(this) ) then
            this:DelObjVar("CriminalProtect")
            return
        end
        this:SendClientMessage("SetKarmaState", "Peaceful")
        local desc = "You will not be protected from becoming a criminal!"
        local isInitiate = IsInitiate(this)
        if ( isInitiate ) then
            desc = desc .. " Performing this action will remove your initiate status."
        end
        desc = desc .. "\nAre you sure you wish to do this?"
		ClientDialog.Show{
			TargetUser = this,
			ResponseObj = this,
			DialogId = "ToggleUnlawful",
			TitleStr = "Allow Criminal Actions",
			DescStr =  desc,
			Button1Str = "Yes.",
			Button2Str = "No.",
			ResponseFunc = function(user, buttonId)
				if( buttonId == 0 ) then
                    this:DelObjVar("CriminalProtect")
                    this:SendClientMessage("SetKarmaState", "Unlawful")
                    if ( isInitiate ) then
                        EndInitiate(this)
                    end
				end
			end
		}
	else
        this:SetObjVar("CriminalProtect", true)
	end

end)

RegisterEventHandler(EventType.Timer, "PlayerTick", function()
	this:ScheduleTimerDelay(TimeSpan.FromMinutes(1), "PlayerTick")
	local minutes = this:GetObjVar("PlayMinutes") or 0
	this:SetObjVar("PlayMinutes", minutes+1)
	PerformPlayerTick(true, minutes)
end)
if not( this:HasTimer("PlayerTick") ) then
	this:ScheduleTimerDelay(TimeSpan.FromMinutes(1), "PlayerTick")
end
CallFunctionDelayed(TimeSpan.FromSeconds(4), PerformPlayerTick)
-- end player tick

-- player guard stuff
--[[
RegisterEventHandler(EventType.CreatedObject, "super_guard", function(success,objRef,target)
	if not( success ) then return end
	if ( objRef ~= nil and objRef:IsValid() ) then
		objRef:SendMessage("AttackEnemy",target)
		--CreateObj("spawn_portal",objRef:GetLoc(),"spawn_portal")
		objRef:NpcSpeech(SuperGuardThingsToSay[math.random(1,#SuperGuardThingsToSay)])
	end
end)]]

RegisterEventHandler(EventType.Timer, "CriminalTimer", function ()
    CriminalTimerExpire(this)
end)

this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"PlayerShortTick")
RegisterEventHandler(EventType.Timer,"PlayerShortTick", function ( ... )
	PerformPlayerShortTick()

	this:ScheduleTimerDelay(TimeSpan.FromSeconds(1),"PlayerShortTick")
end)

this:ScheduleTimerDelay(TimeSpan.FromSeconds(4),"PlayerFourTick")
RegisterEventHandler(EventType.Timer,"PlayerFourTick", function ( ... )
	PerformPlayerFourTick()

	this:ScheduleTimerDelay(TimeSpan.FromSeconds(4),"PlayerFourTick")
end)
-- end player guard stuff

RegisterEventHandler(EventType.StartMoving,"",function (success)
	if (this:HasObjVar("IsHarvesting")) then
		local harvestingTool = this:GetObjVar("HarvestingTool")
		harvestingTool:SendMessage("CancelHarvesting",this)
	end
end)

RegisterEventHandler(EventType.Message,"ShowTutorialUI",
	function ( ... )
		ShowTutorialUI(this)
	end)

-- allow players to be summoned from any region (god command for example)
RegisterEventHandler(EventType.Message, "GlobalSummon", function(loc, address)
	TeleportUser(GameObj(0), this, loc, address, nil, true, true, true)
end)
-- allow gods to get the region a player is on (so they can jump there etc)
RegisterEventHandler(EventType.Message, "PlayerLocationRequest", function(responseObj)
	if ( responseObj ) then
		responseObj:SendMessageGlobal("PlayerLocationResponse", this:GetLoc(), ServerSettings.RegionAddress)
	end
end)

function HandleOpenStoreCommand( ... )
	--SCAN UPDATED BUTTON FUNCTION TO OPEN YOUTUBE
	--this:SystemMessage("The store is not enabled for community servers.","info")
		this:SendClientMessage("OpenURL", "https://www.youtube.com/channel/UCHG96k0l5KWHh_Limn0AOwA")
end

RegisterEventHandler(EventType.ClientUserCommand,"OpenStore",function ( ... )
		HandleOpenStoreCommand()
	end)

RegisterEventHandler(EventType.Message,"AdjustVitality",function (amount)
	local amount = tonumber(amount)
	if(amount) then
		SafeAdjustCurVitality(amount,this)

	end
end)

RegisterEventHandler(EventType.Message,"QuestEventFired",function (_questType, _args, _quantity)
	Quests.ProcessQuestEventMessage( this, _questType, _args, _quantity )
end)

RegisterEventHandler(EventType.Message,"VitalityAdjusted",function (_vitality)
	StartMobileEffect( this, "Vitality", this, { Vitality = _vitality } )
end)

--SCAN ADDED
-- fires when a player logs in to your world
RegisterEventHandler(EventType.UserLogin,"",HandleUserLoginEvent)

--SCAN ADDED
-- fires when a player logs out of your world
RegisterEventHandler(EventType.UserLogout,"",HandleUserLogoutEvent)