local plotController = this:GetObjVar("PlotController")

if( plotController ) then

    local merchantSaleItem = {}
    
    Plot.ForeachLockdown(plotController, 
    function(item) 
        local price = item:HasObjVar("itemPrice")
        if( item:IsContainer() and not price ) then
            local contObjects = item:GetContainedObjects()
            for index, contObj in pairs(contObjects) do
                price = contObj:HasObjVar("itemPrice")
                contObj:SetObjVar("merchantOwner", this)
                table.insert( merchantSaleItem, contObj )
            end
        else
            if( price ) then
                item:SetObjVar("merchantOwner", this)
                table.insert( merchantSaleItem, item )
            end
        end
    end, false) --not strict

    this:SetObjVar("merchantSaleItem", merchantSaleItem)

end