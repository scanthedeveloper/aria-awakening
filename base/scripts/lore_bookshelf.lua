
-- 1,115,1;5,40,5;10,90,10;15,0,10;15,0,15;20,0,20;25,0,25
-- 1,0,0-2,0,0-3,0,0-4,0,0-5,0,0-6,0,0-7,0,0-8,0,0-9,0,0-10,0,0-11,0,0-12,0,0-13,0,0-14,0,0-15,0,0-16,0,0-17,0,0-18,0,0-19,0,0-20,0,0-21,0,0-22,0,0-23,0,0-24,0,0-25,0,0-26,0,0-27,0,0-28,0,0-29,0,0

function InitShelfData()
	--DebugMessage("InitShelfData()")
	local shelfData = {}

	local shelfCount = this:GetObjVar("ShelfCount") or 0
	local booksPerShelf = this:GetObjVar("BooksPerShelf") or 0

	for i=1, shelfCount do
		for j=1, booksPerShelf do
			local index = (i-1) * booksPerShelf + j
			shelfData[index] = {ShelfIndex = index, Hue = 0, TomeIndex = -1}
		end
	end
	--DebugMessage("Init ShelfData: ", #shelfData, DumpTable(shelfData))
	this:SetObjVar("ShelfData", shelfData)

	return shelfData
	--this:SetSharedObjectProperty("AdjustShelf", commandString)
end

function GetShelfData()
	--DebugMessage("GetShelfData()")
	return this:GetObjVar("ShelfData") or InitShelfData()
end

function RebuildShelf(shelfData)
	--DebugMessage("RebuildShelf()")

	local commandString = ""
	local suffix = ""
	local tomeIndexString
	local max = #shelfData
	for i=1, max do
        suffix = i < max and ";" or ""
        tomeIndexString = shelfData[i].TomeId and tostring(shelfData[i].TomeIndex or -1) or "-1"
        --DebugMessage(i,"is kill?", (not shelfData[i].TomeId), tomeIndexString)
		commandString = commandString..tostring(shelfData[i].ShelfIndex)..","..tostring(shelfData[i].Hue or 0)..","..tomeIndexString..suffix
	end

	this:SetSharedObjectProperty("Variation", commandString)
end

function AdjustShelf(shelfDataChanges)
	--DebugMessage("AdjustShelf()")
	local shelfData = GetShelfData()

	for i=1, #shelfDataChanges do
		local shelfIndex, hue, tomeIndex = shelfDataChanges[i].ShelfIndex, shelfDataChanges[i].Hue, shelfDataChanges[i].TomeIndex

		if shelfIndex and hue and tomeIndex then
			shelfData[shelfIndex].Hue = hue
			shelfData[shelfIndex].TomeIndex = tomeIndex
		end
	end

	this:SetObjVar("ShelfData", shelfData)
end

function Random()
	--DebugMessage("Random()")
	local randomChanges = {}
	local rand = math.random(1,4)

	for i=1, rand do
		local index = math.random(1,28)
		randomChanges[i] = {ShelfIndex = index, Hue = math.random(1,100), TomeIndex = index}
	end

	AdjustShelf(randomChanges)

	RebuildShelf(GetShelfData())
end

function Fill()
	--DebugMessage("Random()")

	local shelfData = GetShelfData()

	for i=1, #shelfData do
		shelfData[i] = {ShelfIndex = i, Hue = math.random(1,100), TomeIndex = i}
	end

	AdjustShelf(shelfData)

	RebuildShelf(shelfData)
end

function Empty()
	local shelfData = GetShelfData()

	for i=1, #shelfData do
		shelfData[i] = {ShelfIndex = i, Hue = 0, TomeIndex = -1}
	end

	AdjustShelf(shelfData)

	RebuildShelf(shelfData)
end
--[[
	on CloseWindow or Obj Destroy / Logout / disable OR a couple min timer, then RebuildShelf()
		check if Rebuild is necessary

]]

RegisterEventHandler(EventType.LoadedFromBackup,"", function()
	RebuildShelf(GetShelfData())
end)

RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()
	--RemoveUseCase(this, "Use")
	--AddUseCase(this,"Use",true)

	this:SetObjVar("ShelfCount", initializer and initializer.ShelfCount or 0)
	this:SetObjVar("BooksPerShelf", initializer and initializer.BooksPerShelf or 0)

	RebuildShelf(GetShelfData())
end)

RegisterEventHandler(EventType.Message, "UseObject", function(user,usedType)
	if usedType == "Use" then
		this:PlayObjectSound("Use", true)
		user:SendMessage("StartMobileEffect", "Bookshelf", this )
	end
end)

RegisterEventHandler(EventType.Message, "RebuildShelf", RebuildShelf)

RegisterEventHandler(EventType.Message, "r", Random)
RegisterEventHandler(EventType.Message, "f", Fill)
RegisterEventHandler(EventType.Message, "e", Empty)

RegisterEventHandler(EventType.Message, "h", function()
	this:SetHue(14)
end)
