

-- required for fast identification within require modules
_IS_BRAIN = true

require 'combat'
require 'brain_inc' -- must come after combat, before mobile
require 'base_mobile_advanced'

local fsm

function Start()
    fsm = FSM(this, {
        States.MilitaChampionIdle,
        States.MilitaChampionDeath,
        States.Aggro,
        States.AttackAggroList,
        States.MilitaChampionFight,
    })

    fsm.Start()
end

local _OnMobileLoad = OnMobileLoad
function OnMobileLoad()
    _OnMobileLoad()
    Start()
end