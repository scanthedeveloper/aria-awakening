_Controller = this:GetObjVar("PlotController")
if ( _Controller == nil ) then
    _Controller = Plot.AtLoc(this:GetLoc())
    if ( _Controller ~= nil ) then
        this:SetObjVar("PlotController", _Controller)
    end
end

function OnLoad()    
    local decayDate = this:GetObjVar("DecayDate")
    if(decayDate) then
        local now = DateTime.UtcNow
        if(DateTime.Compare(now,decayDate) >= 0) then
            DebugMessage("[moving_crate] Decay time passed. Destroying.")
            this:Destroy()
        end
    end

    CallFunctionDelayed(TimeSpan.FromSeconds(1), function ()        
        if(#this:GetContainedObjects() == 0) then
            this:Destroy()
        end
    end)

    -- unlock secure containers
    for i,crateObj in pairs(this:GetContainedObjects()) do
        if(crateObj:HasObjVar("SecureContainer")) then
            crateObj:DelObjVar("SecureContainer")
            crateObj:DelObjVar("FriendContainer")
            crateObj:DelObjVar("locked")
            SetItemTooltip(crateObj)
        end
    end
end

curPage = 1
itemsPerPage = 50
allItems = nil

function DoRetrieveWindow(user)
    if not(allItems) then
        allItems = this:GetContainedObjects()
    end

    local itemCount = #allItems

    if(itemCount == 0) then
        user:SystemMessage("This crate is empty and will be destroyed.","info")
        user:CloseDynamicWindow("RetrieveWindow")
        this:Destroy()
        return
    end
    
    local startIndex = itemsPerPage * (curPage-1)    

    local dynWindow = DynamicWindow("RetrieveWindow","Moving Crate",482,630)

    dynWindow:AddImage(40,4,"TitleBackground",390,20,"Sliced")
    dynWindow:AddButton(40,8,"PageDown","",0,0,"","",false,"Previous")
    dynWindow:AddLabel(230,7,"Page "..curPage,463,0,18,"center")
    dynWindow:AddButton(400,8,"PageUp","",0,0,"","",false,"Next")

    dynWindow:AddImage(8,56,"DropHeaderBackground",442,488,"Sliced")

    dynWindow:AddLabel(28,40,"Name",190,20,18)
    dynWindow:AddLabel(228,40,"Weight",190,20,18)
    local scrollWindow = ScrollWindow(18,66,415,468,26)

    for i=1,itemsPerPage do
        local curIndex = i + startIndex
        if(curIndex <= itemCount) then
            local scrollElement = ScrollElement()

            scrollElement:AddLabel(10,5,allItems[curIndex]:GetName(),190,20,18)
            scrollElement:AddLabel(210,5,""..GetWeight(allItems[curIndex]),40,0,18)
            
            
            if(allItems[curIndex]:HasModule("container")) then
                scrollElement:AddButton(280,0,"Open|"..tostring(curIndex),"Open",60,22,"","",false,"List")
            end    
            scrollElement:AddButton(340,0,"Take|"..tostring(curIndex),"Take",60,22,"","",false,"List")

            scrollWindow:Add(scrollElement)
        end
    end

    dynWindow:AddScrollWindow(scrollWindow)

    local decayDate = this:GetObjVar("DecayDate")
    if(DateTime.Compare(DateTime.UtcNow,decayDate) >= 0) then
        dynWindow:AddLabel(28,554,"Scheduled for decay (Next server restart)",0,20,18)
    else
        dynWindow:AddLabel(28,554,"Decays at: "..this:GetObjVar("DecayDate"):ToString().." UTC",0,20,18)
    end

    user:OpenDynamicWindow(dynWindow)
end

RegisterEventHandler(EventType.LeaveView,"PlayerInPlot",
    function (playerObj)
        playerObj:CloseDynamicWindow("RetrieveWindow")
    end)

RegisterEventHandler(EventType.DynamicWindowResponse,"RetrieveWindow",
    function (user,buttonId)
        if not(this:HasObjVar("Initialized")) then
            user:SystemMessage("You can't do that just yet.","info")
            return
        end

        if(buttonId == "PageUp") then
            local newPage = curPage + 1
            if((newPage - 1) * itemsPerPage < #allItems) then
                curPage = newPage
                DoRetrieveWindow(user)
            end
            return
        elseif(buttonId == "PageDown") then
            local newPage = curPage -1
            if(newPage > 0) then
                curPage = newPage
                DoRetrieveWindow(user)
            end
            return
        end

        local command,index = string.match(buttonId, "(%a+)|(.+)")

        index = tonumber(index)

        if(not(index) or not(allItems) or index < 1 or index > #allItems) then
            return
        end

        local item = allItems[index]
        if(item:IsValid() and item:TopmostContainer() == this) then
            if(command == "Open") then
                item:SendOpenContainer(user)   
            elseif(command == "Take") then
                local carriedObject = user:CarriedObject()
                if( carriedObject ~= nil and carriedObject:IsValid() and item ~= carriedObject) then
                    user:SystemMessage("You are already carrying something.","info")
                    return
                end

                local backpack = user:GetEquippedObject("Backpack")
                local randomLoc = GetRandomDropPosition(backpack)
                if (TryPutObjectInContainer(item, backpack, randomLoc, false, false, true, user)) then
                    item:MoveToContainer(user,Loc(0,0,0))                    

                    this:SetSharedObjectProperty("NumItems", this:GetItemCount())

                    table.remove(allItems,index)
                    DoRetrieveWindow(user)
                else
                    if(IsStackable(item)) then  
                        local maxWeight = GetContainerMaxWeight(backpack)
                        local curWeight = GetContentsWeight(backpack)
                        local availableWeight = math.max(0,math.floor(maxWeight - curWeight))
                        if(availableWeight > 0) then
                            local unitWeight = GetUnitWeight(item,1)
                            if(item:GetCreationTemplateId() == "coin_purse") then
                                unitWeight = ServerSettings.Misc.CoinWeight
                            end
                            local carryCount = math.floor(availableWeight / unitWeight)
                            if(carryCount > 0) then
                                item:SendMessage("StackSplit",user,carryCount,user)
                                CallFunctionDelayed(TimeSpan.FromSeconds(0.5),function() DoRetrieveWindow(user) end)
                            end
                            return
                        end
                    end
                    user:SystemMessage("You can't carry that much weight.","info")
                    return
                end
            end
        end
    end)

RegisterEventHandler(EventType.Message,"UseObject",
    function (user,usedType)
        if(this:HasTimer("MovingCrateDelay")) then
            user:SystemMessage("You must wait before you can do that again.","info")
            return
        end
        this:ScheduleTimerDelay(TimeSpan.FromSeconds(5),"MovingCrateDelay")

        local plot = Plot.GetForObject(user)
        if not(plot) then 
            user:SystemMessage("You must be on your plot to do this.","info")
            return
        end

        if( not(Plot.HasControl(user,plot)) ) then
            user:SystemMessage("You do not own this crate.","info")
            return
        end

        if(usedType == "Retrieve Items" or usedType == "Open") then
            local plotBounds = plot:GetObjVar("PlotBounds")
            local bounds = AABox2(plotBounds[1][1].X-2,plotBounds[1][1].Z-2,plotBounds[1][2].X+4,plotBounds[1][2].Z+4)
            AddView("PlayerInPlot",SearchSingleObject(user,SearchAARect(bounds)),1)

            allItems = nil
            DoRetrieveWindow(user)
        elseif(usedType == "Discard") then
            TextFieldDialog.Show{
                TargetUser = user,
                Width = 600,
                Title = "Discard Moving Crate",
                Description = "Are you sure you wish to discard this moving crate and all associated items? To confirm type DISCARD in all capital letters.",
                ResponseFunc = function(user,newValue)
                    if(newValue == "DISCARD") then
                        if(this:GetContainedObjects() == 0) then
                            this:Destroy()
                            user:SystemMessage("Moving create discarded")
                        else
                            this:SetObjVar("SaveDecayDate",this:GetObjVar("DecayDate"))
                            this:SetObjVar("DecayDate",DateTime.UtcNow)
                            AddUseCase(this,"Undo Discard",false)
                            RemoveUseCase(this,"Discard")
                            user:SystemMessage("Moving crate will be discarded on the next server restart.","info")
                        end
                    else                        
                        user:SystemMessage("Discard moving crate cancelled.","info")
                    end
                end
            }
        elseif(usedType == "Undo Discard") then
            AddUseCase(this,"Discard",false)
            RemoveUseCase(this,"Undo Discard")
            user:SystemMessage("Moving crate discard cancelled.","info")
            this:SetObjVar("DecayDate",this:GetObjVar("SaveDecayDate"))
        end
    end)

RegisterEventHandler(EventType.ModuleAttached,"moving_crate",function ( ... )
    this:SetObjVar("HandlesDrop",true)

    RemoveUseCase(this,"Open")
    RemoveUseCase(this,"Lock")
    RemoveUseCase(this,"Unlock") 

    AddUseCase(this,"Retrieve Items",true)
    AddUseCase(this,"Discard",false)

    this:SetName("Moving Crate")

    OnLoad()

    CallFunctionDelayed(TimeSpan.FromSeconds(1),function ( ... )        
        for i, crateObj in pairs(this:GetContainedObjects()) do
            if(crateObj:GetObjVar("IsPlotObject")) then
                if(crateObj:HasObjVar("IsHouse")) then
                    local blueprintTemplate = Plot.GetBlueprintFromHouse(crateObj)
                    if(blueprintTemplate) then
                        Create.InContainer(blueprintTemplate,this,nil,function (objRef)
                            objRef:SetObjVar("Autocomplete",true)
                        end)
                    end
                end
                crateObj:Destroy()
            elseif(crateObj:IsMobile()) then
                -- get the gear off him
                for i,slot in pairs(ITEM_SLOTS) do
                    local item = crateObj:GetEquippedObject(slot)
                    if(item) then
                        item:MoveToContainer(this,Loc(0,0,0))
                    end
                end
                -- get the money
                local bankObj = crateObj:GetEquippedObject("Bank")
                local coinsObj = FindItemInContainerByTemplate(bankObj,"coin_purse")                
                if(coinsObj) then
                    coinsObj:MoveToContainer(this,Loc(0,0,0))
                end
                crateObj:Destroy()
            elseif(crateObj:IsContainer()) then
                -- remove from sale
                local saleItem = FindItemsInContainerRecursive(crateObj,  function(item)
                    return ( item:HasModule("hireling_merchant_sale_item") )
                end)

                for key,value in pairs(saleItem) do
                    if ( value:HasModule("hireling_merchant_sale_item") ) then
                        value:SendMessage("RemoveFromSale")
                    end
                end

                if(crateObj:HasObjVar("IsPackedObject")) then
                    for i, contObj in pairs(crateObj:GetContainedObjects()) do
                        contObj:MoveToContainer(this,Loc(0,0,0))
                    end
                end
            end

            if(crateObj:HasModule("hireling_merchant_sale_item")) then
                crateObj:SendMessage("RemoveFromSale")
            end

            crateObj:DelObjVar("LockedDown")
            crateObj:DelObjVar("NoReset")
            crateObj:DelObjVar("PlotController")
            RemoveTooltipEntry(crateObj,"locked_down")
        end
    end)

    CallFunctionDelayed(TimeSpan.FromSeconds(2),function ( ... )
        local stackables = {}

        for i, crateObj in pairs(this:GetContainedObjects()) do
            local isContainer = crateObj:IsContainer()
            local containerObjs = isContainer and crateObj:GetContainedObjects()
            local hasObjects = containerObjs and #containerObjs > 0
            
            if(IsStackable(crateObj)) then
                table.insert(stackables,crateObj)
            elseif(crateObj:HasObjVar("IsPackedObject") and not(hasObjects) ) then
                PackObject(this, crateObj)            
            end
        end
            
        for i, stackable in pairs(stackables) do
            TryStack(stackable, this, false, true)
        end

        this:SetObjVar("Initialized",true)
    end)
end)

RegisterEventHandler(EventType.LoadedFromBackup,"",function ()
    OnLoad()
end)

this:RemoveTimer("DecayTimer")