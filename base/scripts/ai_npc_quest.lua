require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

AI.QuestStepsInvolvedIn = {
    {"HealerProfessionIntro", 1},
    {"HealerProfessionIntro", 2},
    {"HealerProfessionIntro", 3},
    {"HealerProfessionIntro", 5},
}

function Dialog.OpenTalkDialog(user)
    QuickDialogMessage(this,user,"Will we be ready?")
end

function Dialog.OpenWhoDialog(user)
    QuickDialogMessage(this,user,"I am a quest guy!")
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)

