require 'base_ai_npc'

AI.Settings.MerchantEnabled = false
AI.Settings.EnableTrain = false
AI.Settings.EnableBuy = false
AI.Settings.SkipIntroDialog = true
AI.Settings.CanConverse = false
AI.Settings.StationedLeash = true

AI.GreetingMessages = 
{
	"Mortal, you have come to visit me. What is your name?",
}

-- PRIMARY DIALOG
function Dialog.OpenGreetingDialog(user)
    text = AI.GreetingMessages[math.random(1,#AI.GreetingMessages)]

    response = {}
    table.insert(response, { text = user:GetName(), handle = "Name" } )
    table.insert(response, { text = "I'd rather not say.", handle = "NoName" } )
    table.insert(response, { text = "Please send me to the PRZM Lounge.", handle = "Teleport" } )
    table.insert(response, { text = "Goodbye.", handle = "" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

-- Gave name.
function Dialog.OpenNameDialog(user)
    text = "You are brave child. I see a fierceness in you. Too often your kind is crushed and broken amoung the turmoils of this world. I think this is not to be your fate. Go with the blessing of the Pedesii."

    response = {}
    table.insert(response, { text = "Thank you.", handle = "Greeting" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

-- Gave name.
function Dialog.OpenNoNameDialog(user)
    text = user:GetName() .. ", I know of you child, whether you'd seek it or not. Summon your courage you will need it with the dawning of each day."

    response = {}
    table.insert(response, { text = "I will.", handle = "Greeting" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

-- TELEPORT OUT
function Dialog.OpenTeleportDialog(user)
    text = "This journey is not for the weak, are you ready?"

    response = {}
    table.insert(response, { text = "Yes, please.", handle = "ConfirmTeleport" } )
    table.insert(response, { text = "No, I'll stay!", handle = "Information" } )

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

-- CONFIRM TELEPORT OUT
function Dialog.OpenConfirmTeleportDialog(user)
    -- Teleport player back to the mage tower
    this:NpcSpeech("Please come again soon.")
    TeleportUser(user, user, Loc(30.67,0.00,60.88), "Founders")
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)

