require 'dynamic_spawn_controller'

mDynamicSpawnKey = "DynamicHarpySpawner"

mDynamicHUDVars.Title = "A Cave of Horrors"
mDynamicHUDVars.Description = "Kill the golems that infest the Misty Caverns."

local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        msg = "A sound of rocks grinding fills the air."
    elseif( progress == 2 ) then 
        msg = "A low rumble echoes throughout the caverns."
    elseif( progress == 3 ) then 
        msg = "The very earth beneath you groans in pain."
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end