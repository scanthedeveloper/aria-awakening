require 'dynamic_spawn_controller'
mDynamicSpawnKey = "DynamicBanditSpawner"

mDynamicHUDVars.Title = "Banditry Supression"
mDynamicHUDVars.Description = "Kill bandits at the Bandit Camp."


local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        msg = "Bandits have been alerted to your presence."
    elseif( progress == 2 ) then 
        msg = "More bandits begin to arrive."
    elseif( progress == 3 ) then 
        msg = "You hear a mighty warcry."
    elseif( progress == 999 ) then 
        msg = "The bandits fall back."
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end