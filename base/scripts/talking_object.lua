------- Variation of New NPC Interaction System (April 2020) -------
--

NPCDialogue = {}

if initializer and initializer.NPCDialogue then
    NPCDialogue = initializer.NPCDialogue
    this:SetObjVar("NPCDialogue", NPCDialogue)

    local loreItems = initializer.LoreItems or {}
    local createdItems = {}
    for i=1, #loreItems do
        local itemLoc = this:GetLoc():Add(ParseLoc(loreItems[i].RelativeLoc))
        local itemRot = ParseLoc(loreItems[i].Rotation)

        Create.AtLoc(loreItems[i].Template, itemLoc, function(item)
            createdItems[i] = item
            item:SetRotation(itemRot)
            item:SendMessage("Init", loreItems[i].TomeId, loreItems[i].EntryIndex)
        end)
    end
    this:SetObjVar("LoreItems",createdItems)
end

function GetNPCDialogue()
    NPCDialogue = next(NPCDialogue) and NPCDialogue or this:GetObjVar("NPCDialogue")
    return NPCDialogue
end

-- Checks that player "user" is valid to interact "usedType" with and then draws its root dialogue.
function InitializeNPCInteraction(user, usedType)
    if(usedType ~= "Interact") then return end
    if (IsAsleep(this)) then return end
    
    --DebugMessage("---- FRESH INTERACT ----")
    Dialogue.GoTo(this, user, GetNPCDialogue(), {}, 1, nil)
end

-- Processes string "clickId" sent from DynamicWindowResponse to find new instruction for traversal along Dialogue Trees.
function ProcessInstruction(user, clickId)
    local npc = this
    local player = user

    local commandInfo = Dialogue.ProcessCommandString(npc, player, NPCDialogue, clickId)
    local success = Dialogue.GlobalProcessInstruction(npc, player, NPCDialogue, commandInfo)
    if not success then
        -- No special behavior needed, continue on to next Depth and call logic from static_data/npc_dialogue.lua.
        Dialogue.GoTo(npc, player, NPCDialogue, commandInfo.Indices, commandInfo.Depth + 1, commandInfo.Instruction)
    end
end

function HandleObjectSelection(target, user)
    --DebugMessage("HandleObjectSelection()", target, user)
    local temp = Dialogue.GetTempInteractionRecord(this, user)
    local isValid = temp.Indices and temp.Depth and temp.Instruction
    if target == nil then return end
    if not temp.NeedContext then DebugMessage("Context was lost") return end
    if not isValid then DebugMessage("TempRecord not valid", temp.Indices, temp.Depth, temp.Instruction) return end

    local selectionAccepted = true
    local newInstruction = ""

    if temp.Instruction == "GIVE" then
        --selectionAccepted = DoGiveLogic(stuff)
        newInstruction = selectionAccepted and "SELLSUCCESS" or "SELLFAIL"
    else
        selectionAccepted = false
        DebugMessage("No valid SelectionId")
    end

    Dialogue.GoTo(this, user, NPCDialogue, temp.Indices, temp.Depth + 1, newInstruction)
end

--
------- End Variation of NPC Interaction block -------

RegisterEventHandler(EventType.Message, "UseObject", InitializeNPCInteraction)
RegisterEventHandler(EventType.DynamicWindowResponse, "NPCInteraction", ProcessInstruction)
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "DialogueObjectSelection", HandleObjectSelection)

RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(), function()
    AddUseCase(this,"Interact",true)
    this:SetObjVar("LockedDown",true)
    this:SetSharedObjectProperty("DenyPickup", true)
end)
