require 'base_tool_resourceharvester'

DEPLETION_AVOIDANCE_SKILL_THRESHOLD = 40
DEPLETION_INCREASE_SKILL_THRESHOLD = 30
ResourceHarvester.ToolType = "Pick"
ResourceHarvester.HarvestAnimation = "minerock"
ResourceHarvester.HarvestAnimationDurationSecs = 1.2
ResourceHarvester.DefaultHarvestDelaySecs = 6
ResourceHarvester.UseRange = 10

RegisterSingleEventHandler(EventType.ModuleAttached,"tool_miningpick",
	function ()
		this:ScheduleTimerDelay(TimeSpan.FromSeconds(2), "DelayedTooltipUpdate")
		ResourceHarvester.Initialize()
	end)

toolBonuses ={ 
	BonusHarvestDelay = { DisplayString = " Harvest Delay", reverseStat = true },
	BonusHarvestEfficiency = { DisplayString = " Increased Harvesting Efficiency", reverseStat = false },
	BonusHarvestYield = { DisplayString = "% Increased Harvesting Yield", reverseStat = false },
}

function GetModifierString(bonusName)
	local modStr = "" 
	local bonusValue = 0
	if(this:HasObjVar(bonusName)) then
		bonusValue = this:GetObjVar(bonusName)
	end
	if( bonusValue == 0 ) then
		return ""
	elseif( bonusValue > 0 ) then
		modStr = "+" .. tostring(bonusValue) .. " " .. toolBonuses[bonusName].DisplayString
	else
		modStr = tostring(bonusValue) .. " " .. toolBonuses[bonusName].DisplayString
	end

	return ColorizeStatString(modStr,bonusValue, toolBonuses[bonusName].reverseStat) .. "\n"
end

function UpdateToolTooltipString()
	local tooltipString = ""
	--DebugMessage("UpdateToolTooltipString")
	local myTooltipString = ""
	for i, j in pairs(toolBonuses) do
		myTooltipString = myTooltipString .. GetModifierString(i)
	end
	if( myTooltipString ~= "" ) then
		SetTooltipEntry(this,"tool_miningpick",myTooltipString)
	end
end

mHarvestedStackCount = 1

ResourceHarvester.CollectResource = function(user,resourceType,ignoreHarvetBonus)
	local backpackObj = user:GetEquippedObject("Backpack")  
	if( backpackObj ~= nil ) then

		local resSkill = ResourceData.ResourceInfo[resourceType].HarvestBonusSkill		
		local skillVal = 0
		if(resSkill) then
			skillVal = GetSkillLevel(user,resSkill)
		end

		--SCAN ADDED DEFAULT VALUE
		--mHarvestedStackCount = 1
		mHarvestedStackCount = math.random(1,2)

		if( not ignoreHarvetBonus ) then
			local doubleResourceChance = 0

			-- max of .50 chance from skill
			local doubleResourceSkillRange = ResourceData.ResourceInfo[resourceType].Difficulty.Max - ResourceData.ResourceInfo[resourceType].Difficulty.Min
			local doubleResourceSkillValue = math.clamp(skillVal - ResourceData.ResourceInfo[resourceType].Difficulty.Min,0,doubleResourceSkillRange)
			local doubleResourceSkillModifier = (doubleResourceSkillValue / doubleResourceSkillRange) * 0.50

			--DebugMessage("SkillMod",tostring(doubleResourceSkillValue), tostring(doubleResourceSkillRange), doubleResourceSkillModifier)

			doubleResourceChance = doubleResourceChance + doubleResourceSkillModifier

			if(GetGuardProtection(user) == "None") then
				doubleResourceChance = doubleResourceChance * ServerSettings.Misc.WildernessModifiers.Resource
			end		
			
			if( Success(doubleResourceChance) ) then
				--SCAN ADDED LUMBERJACKING BONUS LOGIC
				-----------------------------------
				--get the players skill level:
				local minersSkill = GetSkillLevel(user,"MiningSkill")	
				local miningBonus = 0
				--local displayName = GetResourceDisplayName(resourceType)

				--APPRENTICE
				if(minersSkill > 29.99 ) and (minersSkill < 50.0 ) then 
					miningBonus = math.random(1,4)
					mHarvestedStackCount = mHarvestedStackCount + miningBonus
					--user:SystemMessage("[FF9900]Apprentice Bonus - [-] "..mHarvestedStackCount.."", "info")
					--user:NpcSpeech("[FFFFFF]+"..mHarvestedStackCount.."[-] "..displayName.."[-]","combat")
					user:NpcSpeech("[00FF00] +"..miningBonus.." Apprentice Bonus[-]","combat")
				end
				--JOURNEYMAN
				if(minersSkill > 49.9 ) and (minersSkill < 80.0 ) then 
					miningBonus = math.random(2,5)
					mHarvestedStackCount = mHarvestedStackCount + miningBonus
					--user:SystemMessage("[FF9900]Journeyman Bonus - [-] "..mHarvestedStackCount.."", "info")
					--user:NpcSpeech("[FFFFFF]+"..mHarvestedStackCount.."[-] "..displayName.."[-]","combat")
					user:NpcSpeech("[00FF00] +"..miningBonus.." Journeyman Bonus[-]","combat")
				end
				--MASTER
				if(minersSkill > 79.9) and (minersSkill < 100.0 ) then 
					miningBonus = math.random(3,6)
					mHarvestedStackCount = mHarvestedStackCount + miningBonus
					--user:SystemMessage("[FF9900]Master Bonus - [-] "..mHarvestedStackCount.."", "info")
					--user:NpcSpeech("[FFFFFF]+"..mHarvestedStackCount.."[-] "..displayName.."[-]","combat")
					user:NpcSpeech("[00FF00] +"..miningBonus.." Master Bonus[-]","combat")
				end
				--GRANDMASTER
				if(minersSkill == 100.00) then 
					miningBonus = math.random(4,10)
					mHarvestedStackCount = mHarvestedStackCount + miningBonus 
					--user:SystemMessage("[FF9900]Grandmaster Bonus - [-] "..mHarvestedStackCount.."", "info")
					--user:NpcSpeech("[FFFFFF]+"..mHarvestedStackCount.."[-] "..displayName.."[-]","combat")
					user:NpcSpeech("[00FF00] +"..miningBonus.." Grandmaster Bonus[-]","combat")
				end
				--GOD 100 HARVESTING
				if(minersSkill > 100.00) then 
					miningBonus = 100
					mHarvestedStackCount = mHarvestedStackCount + miningBonus 
					--user:SystemMessage("[FF9900]Grandmaster Bonus - [-] "..mHarvestedStackCount.."", "info")
					--user:NpcSpeech("[FFFFFF]+"..mHarvestedStackCount.."[-] "..displayName.."[-]","combat")
					user:NpcSpeech("[00FF00] +"..miningBonus.." GOD Bonus[-]","combat")
				end
			end
		end

		

		-- failed skill check chance, lose the resource
		local difficulty = 1
		if ( ResourceData.ResourceInfo[resourceType].Difficulty ) then
			difficulty = SkillValueMinMax( skillVal, ResourceData.ResourceInfo[resourceType].Difficulty.Min or 100, ResourceData.ResourceInfo[resourceType].Difficulty.Max or 100)
		end

		--user:NpcSpeech("Difficulty: "..difficulty)
		if not CheckSkillChance( user, resSkill, skillVal, difficulty ) then	
	    	user:SystemMessage("Failed to harvest any usable resources.", "info")
			return
		end
		
		-- see if the user gets an upgraded version
		resourceType = GetHarvestResourceType(user,resourceType,resSkill)
		if (resourceType == nil) then return end

		-- try to add to the stack in the players pack		
	    if not( TryAddToStack(resourceType,backpackObj,mHarvestedStackCount) ) then
	    	-- no stack in players pack so create an object in the pack
	        local templateId = ResourceData.ResourceInfo[resourceType].Template

			--SCAN ADDED RARE INGRIEDIENTS
			--THIS IS WHERE WE ARE GOING TO CREATE OUR RARE INGRIEDIENT / HARVEST ON CHANCE

	    	CreateObjInBackpackOrAtLocation(user,templateId, "create_miningpick_harvest", mHarvestedStackCount)	    
	    end

	    local displayName = GetResourceDisplayName(resourceType)
	    user:SystemMessage("Harvested "..mHarvestedStackCount.." "..displayName..".", "info")
		user:NpcSpeech("[FFFFFF]+"..mHarvestedStackCount.." "..displayName.."[-]", "combat")
		Quests.SendQuestEventMessage( user, "Gathering", { "Mining", resourceType }, mHarvestedStackCount )
	end	

end

base_CompleteHarvest = ResourceHarvester.CompleteHarvest
ResourceHarvester.CompleteHarvest = function(objRef,user)
	base_CompleteHarvest(objRef,user,1)
end

RegisterEventHandler(EventType.CreatedObject, "create_miningpick_harvest", 
	function(success, objRef, amount)
		--DebugMessage("Created Object")
		if(success == true) then
			SetItemTooltip(objRef)
			local resName = objRef:GetObjVar("ResourceType")
			if(resName == nil) then return end
			if(amount < 2) then return end
		--local backpackObj = this:GetEquippedObject("Backpack")
			RequestSetStack(objRef,amount)
		end
	end)


RegisterEventHandler(EventType.Timer, "DelayedTooltipUpdate", 
	function()
		UpdateToolTooltipString()
	end)

RegisterEventHandler(EventType.Message, "UpdateTooltip", 
	function()
		UpdateToolTooltipString()
	end)