require 'scriptcommands_UI_globalvar'
require 'scriptcommands_UI_createcustom'
require 'scriptcommands_immortal'

newScale = nil
newColor = nil
newHue = nil

nameToSet = nil
mMobToCopy = nil
bodyTemplateId = nil
prefabCreate = nil

RegisterEventHandler(EventType.ClientTargetLocResponse, "prefab", 
	function(success,targetLoc)
		local commandInfo = GetCommandInfo("createprefab")

		if not(LuaCheckAccessLevel(this,commandInfo.AccessLevel)) then return end	

		if( success and prefabCreate) then
			CreatePrefab(prefabCreate,targetLoc,Loc(0,0,0))
			for i,point in pairs(GetRelativePrefabExtents(prefabCreate,targetLoc).Points) do
				CreateObj("house_plot_marker",Loc(point))
			end
			
		end	
	end)

--CreateCamp scriptcommand
campCreate = nil
RegisterEventHandler(EventType.ClientTargetLocResponse, "camp", 
	function(success,targetLoc)
		local commandInfo = GetCommandInfo("createcamp")

		if not(LuaCheckAccessLevel(this,commandInfo.AccessLevel)) then return end	

		if( success and campCreate) then
        	CreateObj("prefab_camp_controller",targetLoc,"controller_spawned", campCreate)
		end	
	end)

RegisterEventHandler(EventType.CreatedObject,"controller_spawned",
    function(success,objRef,callbackData)        
        --spawn the object and save it
        if success then
            objRef:SetObjVar("PrefabName",callbackData)
            objRef:SendMessage("Reset")
        end
    end)

--Destroy scriptcommand
local destroyTargetObj = nil
function DoDestroy(target)
	if(target:IsPlayer()) then
		this:SystemMessage("You cannot destroy players")
	elseif(target:HasObjVar("NoReset")) then
		destroyTargetObj = target
		ClientDialog.Show{
			TargetUser = this,
		    DialogId = "DestroyConfirm",
		    TitleStr = "Warning",
		    DescStr = "[$2456]",
		    Button1Str = "Yes",
		    Button2Str = "No",
		    ResponseFunc = function ( user, buttonId )
				buttonId = tonumber(buttonId)
				if( buttonId == 0 and destroyTargetObj ~= nil) then
					if(destroyTargetObj:HasEventHandler(EventType.Message,"Destroy")) then
						destroyTargetObj:SendMessage("Destroy")
					else
						destroyTargetObj:Destroy()
					end					
				end
			end
		}
	else
		this:SystemMessage("Destroying object "..tostring(target))
		if(target:HasEventHandler(EventType.Message,"Destroy")) then
			target:SendMessage("Destroy")
		else
			target:Destroy()
		end		
	end
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "destroyTarget",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		DoDestroy(target)
	end)	

--Resurrect scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "resTarget",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		target:SendMessage("Resurrect",1.0,this)
	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "resTargetForce",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		target:SendMessage("Resurrect",1.0,this,true)
	end)

--Heal scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "healTarget",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		local curHealth = math.floor(GetCurHealth(target))
		local healAmount = GetMaxHealth(target) - curHealth

		SetCurHealth(target,curHealth + healAmount)

		this:SystemMessage("Healed "..target:GetName().." for " .. healAmount,"event")
		this:SystemMessage("Health is now "..tostring(GetCurHealth(target)))
	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "craftOrderedItems",
	function(target,user)
		if not(IsDemiGod(this)) then return end
		--if( target == nil or not target:GetCreationTemplateId() == "crafting_order" ) then return end
		if( target == nil or not target:IsMobile() ) then return end
					
		Create.ItemsForCraftOrder(target)

	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "craftsNeededForActiveQuests",
	function(target,user)
		if not(IsDemiGod(this)) then return end
		if( target == nil ) then return end

		local activeQuests = Quests.GetActive(target)
		if not activeQuests then return end
		local itemsToMake = {}
		local orderSkill = nil

		for questName, quest in pairs(activeQuests) do
			local steps = AllQuests[questName]
			if steps then
				for i=1, #steps do
					local reqs = steps[i].Requirements
					local previousReqs = nil
					if i > 1 then
						previousReqs = steps[(i-1)].Requirements
					end
					if reqs then
						for j=1, #reqs do
							if reqs[j][1] and ( reqs[j][1] == "HasCraftedItem" or reqs[j][1] == "HasEquippedCraftedItem" ) then
								local generalMisMatch = true
								local specificMisMatch = true
								if previousReqs[j] then
									generalMisMatch = reqs[j][2] and reqs[j][3] and ( reqs[j][2] ~= previousReqs[j][2] or reqs[j][3] ~= previousReqs[j][3] )
									specificMisMatch = reqs[j][4] and reqs[j][5] and ( reqs[j][4] ~= previousReqs[j][4] or reqs[j][5] ~= previousReqs[j][5] )
								end
								local isSpecific = reqs[j][4] and reqs[j][5]
								local isDifferent = false
								local log = ""
								if not isSpecific then 
									isDifferent = generalMisMatch
									log = tostring(questName).." Step "..tostring(i).." Req "..tostring(j).." needs "..tostring(reqs[j][3]).." "..tostring(reqs[j][2])
								else
									isDifferent = specificMisMatch 
									log = tostring(questName).." Step "..tostring(i).." Req "..tostring(j).." needs "..tostring(reqs[j][3]).." "..tostring(reqs[j][2]).." with "..tostring(reqs[j][4]).." of "..tostring(reqs[j][5])
								end

								if previousReqs and isDifferent then
									table.insert(itemsToMake, reqs[j])
									DebugMessage(log)
								elseif not previousReqs then
									table.insert(itemsToMake, reqs[j])
									DebugMessage(log)
								end
							end
						end
					end
				end
			end
		end
			
		if #itemsToMake > 0 then
			Create.InBackpack("lockbox", target, nil, 
				function(obj) 
					Create.ItemsForCraftingProfession(target, itemsToMake, obj)
				end)
		else
			target:SystemMessage("No items needed to craft.", "info")
		end
	end)

--DoSlay scriptcommand
function DoSlay(target)	
    if( target:IsValid() ) then		
    	local damage = GetCurHealth(target)
    	if ( not damage or damage == 0) then damage = 100000 end
		target:SendMessage("ProcessTrueDamage", this, damage, true)
		target:PlayEffect("LightningCloudEffect")
	end
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "slayTarget", 
	function(target,user)
		if( target == nil ) then
			return
		end

		DoSlay(target)
	end)

--CreateCoins scriptcommand
function ShowCreateCoins()		
	local width = (#Denominations * 80) + 160
	local curX = 20

	local newWindow = DynamicWindow("CreateCoins","Create Coins",width,100,-50,-50,"","Center")

	local i = #Denominations
	while(i > 0) do
		local denomInfo = Denominations[i]
		newWindow:AddTextField(curX,20,50,20,denomInfo.Name,"0")
		newWindow:AddLabel(curX+52,25,denomInfo.Abbrev,60,0,18)
		curX = curX + 80
		i = i - 1
	end

	newWindow:AddButton(curX+10,15,"Create","Create",0,0,"","",true)

	this:OpenDynamicWindow(newWindow)
end

RegisterEventHandler(EventType.DynamicWindowResponse,"CreateCoins",
	function (user,buttonId,fieldData)
		if(buttonId == "Create") then
			amounts = {}
			for denomName,denomValStr in pairs(fieldData) do
				local denomVal = tonumber(denomValStr)
				if(denomVal ~= nil and denomVal > 0) then
					amounts[denomName] = denomVal
				end
			end
			if(CountTable(amounts) > 0) then
				CreateObjInBackpack(user,"coin_purse","coins_created",amounts)
			end
		end
	end)

RegisterEventHandler(EventType.CreatedObject,"coins_created",
	function(success,objref,amounts)
		if(success) then
			objref:SendMessage("SetCoins",amounts)
		end
	end)

--Push scriptcommand
function StartPush(pushObj)
	if not(IsDemiGod(this)) then return end

	this:SendClientMessage("EditObjectTransform",{pushObj,this,"push_edit"})
end

RegisterEventHandler(EventType.ClientObjectCommand, "transform",
	function (user,targetId,identifier,command,...)
		if(IsDemiGod(user) and identifier == "push_edit") then
			if(command == "confirm") then
				local targetObj = GameObj(tonumber(targetId))
				if(targetObj:IsValid()) then
					local commandArgs = table.pack(...)
					--GW Fixes locale problems where push command gets , vs .
					for i=1, #commandArgs do
                        commandArgs[i] = string.gsub(commandArgs[i], ",", ".")
                    end
                    
					local newPos = Loc(tonumber(commandArgs[1]),tonumber(commandArgs[2]),tonumber(commandArgs[3]))
					local newRot = Loc(tonumber(commandArgs[4]),tonumber(commandArgs[5]),tonumber(commandArgs[6]))
					local newScale = Loc(tonumber(commandArgs[7]),tonumber(commandArgs[8]),tonumber(commandArgs[9]))

					targetObj:SetWorldPosition(newPos)
					targetObj:SetRotation(newRot)
					targetObj:SetScale(newScale)
				end
			end
		end		
	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse,"setpushtarget",
	function(targetObj,user)
		if(targetObj ~= nil) then			
			StartPush(targetObj)
		end
	end)

RegisterEventHandler(EventType.Message,"PushObject",
	function (target)
		StartPush(target)
	end)

--Kickuser scriptcommand
function DoKick(arg)
	if( #arg >= 1) then
		local reason = ""
		if( #arg >= 2 ) then	
			-- DAB TODO: Only supports one word		
			reason = arg[2]
		end

		local playerObj = GetPlayerByNameOrId(arg[1])
		if( playerObj ~= nil ) then
			playerObj:KickUser(reason)
			
			this:SystemMessage(playerObj:GetName().." kicked from server.")
			
		end
	end	
end

--Ban scriptcommand
function DoUserBan(arg)
	if( #arg >= 1) then
		local hours = "0"
		if( #arg >= 2 ) then	
			hours = arg[2]
		end

		local userid = arg[1]

		if( userid ~= nil and userid ~= "") then
			BanUser(userid, hours)
		end
	end	
end

--Unban scriptcommand
function UnBanUser(userid)
	UnbanUser(userid)
end

--IPban scriptcommand
function DoIPBan(arg)
	local ip = arg[1]

	if( ip ~= nil and ip ~= "") then
		BanIP(ip)
	end
end

--IPUnban scriptcommand
function UnBanIP(ip)
	UnbanIP(ip)
end

--Copyform scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse,"copyformselect",
	function(target,user)
		if (not IsDemiGod(this)) then return end

		if (target == nil) then return end

		mMobToCopy = target
		user:SystemMessage("Which mob do you wish to change?")

		this:RequestClientTargetGameObj(this, "copyform")
	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "copyform",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		if not( target:IsMobile() ) then
			this:SystemMessage("Error: /changeform only works on mobiles")
		end

		target:SendMessage("CopyOtherMobile",mMobToCopy)
	end)

--Possess scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "possess",
	function(target,user)
		if not(IsDemiGod(this)) then return end
		if ( target == nil ) then return end
		TryPossess(user, target)
	end)

--Editchar scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "editchar",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end
		
		OpenCharEditWindow(target)
	end)

--Setname scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "setname",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		target:SetName(nameToSet)
		target:SendMessage("UpdateName")
	end)

--Setbody scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "body",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		if not( target:IsMobile() ) then
			this:SystemMessage("Error: /setbody only works on mobiles")
		end

		target:SetAppearanceFromTemplate(bodyTemplateId)
		target:SetObjVar("FormTemplate",bodyTemplateId)
	end)

--Changeform scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "form",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		if not( target:IsMobile() ) then
			this:SystemMessage("Error: /changeform only works on mobiles")
		end

		target:SendMessage("ChangeMobileToTemplate",bodyTemplateId,{LoadLoot=false})
	end)

--Setscale scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "scale",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		target:SetScale(Loc(newScale,newScale,newScale))
	end)

--Color scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "color",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		target:SetColor(newColor)
	end)

--Hue scriptcommand
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "hue",
	function(target,user)
		if not(IsDemiGod(this)) then return end

		if( target == nil ) then
			return
		end

		target:SetHue(newHue)
	end)

visualStateName = nil
RegisterEventHandler(EventType.ClientTargetAnyObjResponse,"SetVisualState",
	function(target,user)
		if(visualStateName and target and target:IsPermanent()) then
			target:SetVisualState(visualStateName)
		end
	end)

function DoCreateFromFile(filename)
	io.open(filename,"r")
	local count = 1
	local depth = 1
	local parent, curItem	
	local lines = {}
	for line in io.lines(filename) do table.insert(lines,line) end
	local args = StringSplit(lines[1]," ")
	table.remove(lines,1,1)
    CreateObj(args[1],this:GetLoc(),"fromfile",lines,args[2])
end

RegisterEventHandler(EventType.CreatedObject,"fromfile",
	function (success,objRef,entries,count)
		if(count ~= nil and tonumber(count) > 1) then
			RequestSetStackCount(objRef,tonumber(count))
		end

		if(#entries > 0) then
			local line = entries[1]
			local depth = entries.depth or 1
			local _, newDepth = string.gsub(line, "%\t", "")
			
			local args = StringSplit(StringTrim(line)," ")
			table.remove(entries,1,1)

			if(newDepth <= depth) then
				if(newDepth < depth) then
					entries.depth = newDepth
					entries.parent = entries.superParent
				end

				if(entries.parent) then
					DebugMessage("Creating ",args[1]," inside ",entries.parent:GetCreationTemplateId())
					local randomLoc = GetRandomDropPosition(entries.parent)
					CreateObjInContainer(args[1], entries.parent, randomLoc, "fromfile", entries, args[2])
				else
					DebugMessage("Creating ",args[1]," inside ","Ground")
					CreateObj(args[1],this:GetLoc(),"fromfile",entries,args[2])
				end
			elseif(newDepth > depth) then
				local randomLoc = GetRandomDropPosition(objRef)
				if(entries.parent ~= nil) then
					entries.superParent = entries.parent
				end
				entries.parent = objRef
				entries.depth = newDepth
				DebugMessage("Creating ",args[1]," inside ",objRef:GetCreationTemplateId())
				CreateObjInContainer(args[1], objRef, randomLoc, "fromfile", entries, args[2])		
			end
		end
	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse,"npcsit_chair",
	function (target,user)
		if(target and target:HasModule("chair")) then
			target:SendMessage("ForceSit",npcToSit)
		else
			this:SystemMessage("Request cancelled","info")
		end
	end)

npcToSit = nil
RegisterEventHandler(EventType.ClientTargetGameObjResponse,"npcsit",
	function (target,user)
		if(target and target:IsMobile()) then
			npcToSit = target
			this:SystemMessage("Select chair","info")
			this:RequestClientTargetGameObj(this,"npcsit_chair")
		else
			this:SystemMessage("Request cancelled","info")
		end
	end)

createSpawnTemplate = nil
RegisterEventHandler(EventType.ClientTargetLocResponse,"CreateSpawn",
	function (success,targetLoc,targetObj,user)
		if(targetLoc) then
			DebugMessage( "Success!" )
			DebugMessage( "Template : " .. createSpawnTemplate )
			DebugMessage( "TargetLoc : " .. tostring( targetLoc ) )

			local MyFile = io.open("essence_generation/spawn.txt", "a")
			io.output(MyFile)
			local out = 
				"<DynamicObject>\n" .. 
					"\t<Name>GroupA</Name>\n" ..
					"\t<Id>1</Id>\n" .. 
					"\t<ObjectCreationParams>simple_mob_spawner " .. string.format("%.2f",targetLoc.X) .. " 0 " .. string.format("%.2f",targetLoc.Z) .. " 0 0 0 1 1 1</ObjectCreationParams>\n" .. 
					"\t<ObjVarOverrides>\n" .. 
						"\t\t<StringVariable Name=\"spawnTemplate\">"..createSpawnTemplate.."</StringVariable>\n" .. 
						"\t\t<DoubleVariable Name=\"spawnDelay\">240</DoubleVariable>\n" .. 
					"\t</ObjVarOverrides>\n" .. 
				"</DynamicObject>\n"

			io.write( out )
			io.flush()
			io.close(MyFile)
		else
			this:SystemMessage("Create Spawn Cancelled","info")
		end
	end)

RegisterEventHandler(EventType.CreatedObject,"create_all_items",
	function (success,objRef,category,unique)
		local createCount = 0
		objRef:SetName(category)
		local templatesListTable = GetAllTemplateNames(category)
		for i, templateName in pairs(templatesListTable) do
			local templateData = GetTemplateData(templateName)
			if(not(unique) or not(createAllIds[templateData.ClientId])) then				
				local weight = templateData.SharedObjectProperties.Weight
				if(weight ~= -1) then
					if(unique) then createAllIds[templateData.ClientId] = true end
					local randomLoc = GetRandomDropPosition(objRef)
					CreateObjInContainer(templateName, objRef, randomLoc)		
					createCount = createCount + 1
				end
			end
		end

		if(createCount == 0) then
			objRef:Destroy()
		end
	end)

relpos1Target = nil
RegisterEventHandler(EventType.ClientTargetAnyObjResponse,"relpos1",
	function (target,user)
		if(target ~= nil) then
			this:SystemMessage("[$2458]")
			relpos1Target = target
			this:RequestClientTargetAnyObj(this,"relpos2")
		end
	end)

RegisterEventHandler(EventType.ClientTargetAnyObjResponse,"relpos2",
	function (target,user)
		if(target ~= nil) then
			local target1Loc = relpos1Target:GetLoc()			
			local target1Rot = Loc(0,0,0)
			if not(relpos1Target:IsPermanent()) then
				target1Rot = relpos1Target:GetRotation()
			end
			local relPos = target1Loc - target:GetLoc()
			ClientDialog.Show{
					TargetUser = this,
				    TitleStr = "Relative Position",
				    DescStr = "Relative Position: " .. string.format("%.2f",relPos.X) .. "," .. string.format("%.2f",target1Loc.Y) .. "," .. string.format("%.2f",relPos.Z)
				    			.. "\nRotation: " .. string.format("%.2f",target1Rot.X) .. "," .. string.format("%.2f",target1Rot.Y) .. "," .. string.format("%.2f",target1Rot.Z)
				    			.. "\nDistance: ".. target1Loc:Distance2(target:GetLoc()),
				    Button1Str = "Ok",					
			}		
		end
	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse,"clearCommissions",
	function (target,user)
		if(target ~= nil) then
			local comms = target:GetObjVar("Commissions")
			if comms then
				DebugMessage("user "..tostring(user))
				DebugMessage("target "..tostring(target).." count: "..tostring(#comms))
				for obj, data in pairs(comms) do
					if obj == user then
						comms[obj] = nil
					else
						DebugMessage("not match: "..tostring(obj))
					end
				end
				target:SetObjVar("Commissions", comms)
			end
		end
	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse,"SetQuestWindow",
	function (target,user)
		if not(IsDemiGod(this)) then return end

		if(target ~= nil) then
			OpenSetQuestWindow(target)
		end
	end)

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "SetAppearanceTest",
	function(target,user)
		if not(IsDemiGod(this)) then return end
		--if( target == nil or not target:GetCreationTemplateId() == "crafting_order" ) then return end
		if( target == nil or not target:IsMobile() ) then return end
		
		TextFieldDialog.Show{
            TargetUser = this,
            Title = "Transform Mobile",
            Description = "Type Template:",
            ResponseFunc = function(user,newValue)
            	local scale = GetTemplateObjectScale(newValue)
            	if scale then
            		target:SetAppearanceFromTemplate(newValue)
        			target:SetScale(scale)
        			DebugMessage("Transformed to", newValue)
        		else
        			DebugMessage(newValue, "was not recognized.")
            	end
            end
        }

	end)


DemigodCommandFuncs = {

	SAT = function()
		this:RequestClientTargetGameObj(this, "SetAppearanceTest")
	end,

	Create = function(templateName,amount)		
		if( templateName ~= nil ) then
			templateId = GetTemplateMatch(templateName)
			if( templateId ~= nil ) then
				amount = tonumber(amount) or 0
				if( amount > 1 ) then
					if( GetTemplateObjVar(templateId,"ResourceType") ~= nil ) then
						createAmount = amount
						CreateObj(templateId, this:GetLoc(), "createamount")
						PlayEffectAtLoc("TeleportFromEffect",this:GetLoc())
						return
					end
				end

				CreateObj(templateId, this:GetLoc(), "CreateCommand")
				PlayEffectAtLoc("TeleportFromEffect",this:GetLoc())
			else
				templateListCategory = "All"
				templateListCategoryIndex = 1
				templateListFilter = templateName
				ShowPlacableTemplates(amount)
			end
		else
			templateListFilter = ""
			ShowPlacableTemplates(amount)
		end
	end,

	CreateEquippedObject = function(templateName, targetId)
		local target = this
		if ( targetId ~= nil ) then
			target = GameObj(tonumber(targetId))
			if ( target == nil or not target:IsValid() ) then
				target = this
			end
		end
		if ( target ~= nil and templateName ~= nil and target:IsValid() ) then
			templateId = GetTemplateMatch(templateName)
			if ( templateId ~= nil ) then
				CreateEquippedObj(templateId, target, "CreateCommand")
			end
		end
	end,

	CreatePrefab = function (prefabname)		
		if( prefabname ~= nil ) then
			prefabCreate = prefabname
			this:RequestClientTargetLoc(this, "prefab")
		end
	end,

	CreateCamp = function (campname)		
		if( campname ~= nil ) then
			campCreate = campname
			this:RequestClientTargetLoc(this, "camp")
		end
	end,

	CraftNeeded = function()
		this:RequestClientTargetGameObj(this, "craftsNeededForActiveQuests")
	end,

	CraftOrdered = function()
		this:RequestClientTargetGameObj(this, "craftOrderedItems")
	end,

	Destroy = function(objId)
		if( objId ~= nil ) then
			local target = GameObj(tonumber(objId))
			DoDestroy(target)
		else
			this:RequestClientTargetGameObj(this, "destroyTarget")
		end
	end,

	Resurrect = function(forceOrTargetId, targetObjId)
		local force = false
		local targetId = forceOrTargetId
		if ( forceOrTargetId == "force" ) then
			force = true
			targetId = targetObjId
		end

		if ( targetId == nil ) then
			if ( force ) then
				this:RequestClientTargetGameObj(this, "resTargetForce")
			else
				this:RequestClientTargetGameObj(this, "resTarget")
			end
			return
		end

		local gameObj = this
		if( targetId ~= "self" ) then
			gameObj = GameObj(tonumber(targetId))
		end

		if( gameObj == nil or not(gameObj:IsValid()) ) then
			return
		end

		gameObj:SendMessage("Resurrect",1.0,this,force)
	end,

	ResurrectAll = function (force)
		for i,playerObj in pairs(FindObjects(SearchPlayerInRange(25))) do
			playerObj:SendMessage("Resurrect",1.0,this,force=="force")
		end
	end,

	Heal = function()				
		this:RequestClientTargetGameObj(this, "healTarget")
	end,

	Slay = function(targetObjId)
		if(targetObjId == nil) then
			this:RequestClientTargetGameObj(this, "slayTarget")
			return
		end

		local gameObj = this
		if( targetObjId ~= "self" ) then
			gameObj = GameObj(tonumber(targetObjId))
		end

		DoSlay(gameObj)
	end,

	Nuke = function(targets,userRadius)		
		local includePlayers = false
		local radius = 10
		
		if( targets == "all" ) then
			includePlayers = true
		end
					
		if( userRadius ~= nil ) then
			radius = tonumber(userRadius)
		end				

		local killObjects = FindObjects(SearchMobileInRange(radius))
		if( killObjects ~= nil ) then			
			for index, obj in pairs(killObjects) do
				if( not(obj:IsPlayer() or obj:GetCreationTemplateId() == "wave_target" or (obj:GetObjVar("controller") ~= nil and obj:GetObjVar("controller"):IsPlayer())) or includePlayers) then										
					obj:SendMessage("ProcessTrueDamage", this, 9999, true)
					obj:PlayEffect("LightningCloudEffect")
				end
			end
		end
	end,

	CreateCoins = function(...)
		local arg = table.pack(...)

		if(#arg > 0) then
			local coinAmount = tonumber(arg[1])
			if (coinAmount == nil) then return end
			if(coinAmount <= 0 ) then return end			

			CreateStackInBackpack(this,"coin_purse",coinAmount)
		else
			ShowCreateCoins()
		end
	end,

	Portal = function(...)
		local args = table.pack(...)
		if( #args < 1 ) then
			Usage("portal")
			return
		end

		local x, y, z = 0, 0, 0
		if( #args == 2 ) then
			x = tonumber(args[1])
			z = tonumber(args[2])
		else
			x = tonumber(args[1])
			y = tonumber(args[2])
			z = tonumber(args[3])
		end

		OpenTwoWayPortal(this:GetLoc(),Loc(x,y,z),20)
	end,

	Broadcast = function(...)
		local line = CombineArgs(...)
		if(line ~= nil) then
			ServerBroadcast(line,true)
		else
			Usage("broadcast")
		end
	end,

	LocalBroadcast = function(...)
		local line = CombineArgs(...)
		if(line ~= nil) then
			LocalBroadcast(line,true)
		else
			Usage("localbroadcast")
		end
	end,

	PushObject = function(id)
		if(id ~= nil) then
			local pushObj = GameObj(tonumber(id))
			StartPush(pushObj)
		end
		this:RequestClientTargetGameObj(this, "setpushtarget")
	end,

	SetTime = function(newTime)
		local timeController = GetTimeController()
		if( timeController == nil ) then return end
		--DebugMessage("Time Controller is not nil")
		if( newTime == nil or not timeController:IsValid() ) then 
			DebugMessage("[SetTime] ERROR: NEW TIME IS NIL")
			return 
		end

		local colonLoc = newTime:find(":")
		if(colonLoc ~= nil ) then
			--DebugMessage("Sending time update message")
			local hours = newTime:sub(1,colonLoc-1)
			local minutes = newTime:sub(colonLoc+1,#newTime)
			timeController:SendMessage("SetTime",hours,minutes)
		end
	end,

	AddTitle = function(userNameOrId,...)
		local lineData = table.pack(...)
		if (userNameOrId ~= nil and #lineData > 0) then
			local playerObj = GetPlayerByNameOrId(userNameOrId)
			if (playerObj ~= nil) then
				local line = ""
				for i = 1,#lineData do line = line .. tostring(lineData[i]) .. " " end
				if (line ~= "") then
					CheckAchievementStatus(playerObj, "Other", line, nil, {Description = "User Title", CustomAchievement = line, Reward = {Title = line}})
				end
			end
		end
	end,

	JoinGuild = function(guildId,guildName,guildTag)
		local isNew = guildId == nil or guildId == "" or guildId == "new"

		if(GuildHelpers.Get(this)) then
			this:SystemMessage("You must leave your guild first")
			return
		end

		if(isNew) then 
			guildId = uuid() 
		end

		if not(GuildHelpers.GetGuildRecord(guildId)) then
			if(guildName == nil) then
				this:SystemMessage("Guild does not exist, specify a guild name!")
				return
			end
			npGuildRecord = Guild.Create(nil,guildName,guildId,guildTag)
		end

		CallFunctionDelayed(TimeSpan.FromSeconds(1),function ( ... )
			Guild.AddToGuild(guildId)
		end)
		
		CallFunctionDelayed(TimeSpan.FromSeconds(2),function ( ... )
			local guildRecord = GuildHelpers.GetGuildRecord(guildId)
			local guildRank = isNew and "Guildmaster" or "Officer"
			Guild.PromoteMember(this,guildRecord,guildRank,true)
		end)	
	end,

	KickUser = function(...)
		local arg = table.pack(...)
		if( #arg == 0 ) then Usage("kickuser") return end

		DoKick(arg)		
	end,

	KickAll = function(...)
		local arg = table.pack(...)
		local reason = ""
		if( #arg >= 1 ) then
			-- DAB TODO: Only supports one word
			reason = arg[1]
		end

		local loggedOnUsers =  FindPlayersInRegion()
		for index,object in pairs(loggedOnUsers) do
			if not( IsImmortal(object) ) then
				this:SystemMessage(object:GetName().." kicked from server.")
				object:KickUser(reason)
			end
		end
	end,

	LuaBanUser = function(...)
		local arg = table.pack(...)
		if( #arg == 0 ) then Usage("banuser") return end

		DoUserBan(arg)		
	end,

	LuaUnbanUser = function(userId)
		if(userId ~= nil and userId ~= "") then
			UnBanUser(userId)
		else
			Usage("unbanuser")	
		end
	end,

	LuaBanIP = function(...)
		local arg = table.pack(...)
		if( #arg == 0 ) then Usage("banip") return end

		DoIPBan(arg)		
	end,

	LuaUnbanIP = function(ip)
		if(ip ~= nil and ip ~= "") then
			UnBanIP(ip)
		else
			Usage("unbanip")	
		end
	end,


	LuaClearBans = function()
		ClearBans()
	end,

	LuaGetBans = function()
		GetBans("BanListResults");
	end,

	GlobalVar = function(recordPath)
		local results = GlobalVarListRecords(recordPath)
		if(#results == 1) then
			ShowGlobalVar(results[1])
		else
			ListGlobalVars(results)
		end
	end,

	DeleteGlobalVar = function (recordPath)
		recordPath = recordPath or ""
		local results = GlobalVarListRecords(recordPath)
		if(#results > 0) then
			local varStr = ""
			for i, varName in pairs(results) do 
				varStr = varStr .. varName .. "\n"
			end

			ClientDialog.Show{
				    TargetUser = this,
				    DialogId = "DeleteVarsDialog",
				    TitleStr = "Are you sure?",
				    DescStr = "[$2460]"..varStr,
				    Button1Str = "Yes",
				    Button2Str = "No",
				    ResponseFunc = function ( user, buttonId )
						buttonId = tonumber(buttonId)
						if( buttonId == 0) then				
							for i, varName in pairs(results) do 
								GlobalVarDelete(varName,nil)
							end
							this:SystemMessage("Deleted "..#results.." records.")
						else
							this:SystemMessage("Cancelled")
						end
					end
				}
		else
			this:SystemMessage("No matching records")
		end
	end,

	Debug = function(targetObjId)
		if(args ~= nil) then
			gameObj = GameObj(tonumber(targetObjId))
			if(gameObj:IsValid()) then
				gameObj:SetObjVar("Debug",true)
				DebugMessage(gameObj:GetName().."-----------------------------------")
				return
			end
		end
		this:RequestClientTargetGameObj(this, "debug")		
	end,

	AIState = function(targetObjId)
		if(args ~= nil) then
			gameObj = GameObj(tonumber(targetObjId))
			if(gameObj:IsValid()) then
				DebugMessage("State is "..gameObj:GetObjVar("CurrentState"))
				this:SystemMessage("State is "..gameObj:GetObjVar("CurrentState"))
				return
			end
		end
		this:RequestClientTargetGameObj(this, "aistate")		
	end,

	CopyForm = function()
		this:SystemMessage("Which mob do you wish to copy?")
		this:RequestClientTargetGameObj(this, "copyformselect")
	end,

	Possess = function (targetObj)
		this:RequestClientTargetGameObj(this, "possess")
	end,

	EditChar = function(targetObj)
		if(targetObj == nil) then			
			this:RequestClientTargetGameObj(this, "editchar")
		else
			OpenCharEditWindow(targetObj)
		end
	end,

	SetQuest = function(input)
		local inp
		if input then
			inp = GetPlayerByNameOrId(input)

			if inp then OpenSetQuestWindow(inp) 
			else this:RequestClientTargetGameObj(this, "SetQuestWindow")
			end
		else
			this:RequestClientTargetGameObj(this, "SetQuestWindow")
		end
	end,

	ClearCommissions = function()
		this:RequestClientTargetGameObj(this, "clearCommissions")
	end,

	SetName = function(...)
		local arg = table.pack(...)
		if( #arg == 0 ) then
			Usage("setname")
			return
		end

		local name = ""
		for i = 1,#arg do name = name .. tostring(arg[i]) .. " " end
		nameToSet = name:sub(1, -2)

		if( nameToSet ~= nil ) then
			this:RequestClientTargetGameObj(this, "setname")
		end
	end,

	Body = function(templateName)
		if( templateName == nil ) then
			Usage("setbody")
			return
		end

		bodyTemplateId = GetTemplateMatch(templateName)

		if( bodyTemplateId ~= nil ) then
			this:RequestClientTargetGameObj(this, "body")
		end
	end,

	ChangeForm = function(templateName)
		if(templateName ~= nil) then
			bodyTemplateId = GetTemplateMatch(templateName)

			if( bodyTemplateId ~= nil ) then
				this:RequestClientTargetGameObj(this, "form")
			end
		else
			this:AddModule("change_form_window")
		end
	end,

	Scale = function(scaleValue)
		if( scaleValue == nil ) then
			Usage("setscale")
			return
		end

		newScale = tonumber(scaleValue)

		if( newScale ~= nil ) then
			this:RequestClientTargetGameObj(this, "scale")
		end
	end,

	Color = function(colorcode)
		if( colorcode == nil ) then
			Usage("SetColor")
			return
		end

		newColor = colorcode

		this:RequestClientTargetGameObj(this, "color")
	end,

	Hue = function(hueindex)
		if( hueindex == nil ) then
			Usage("SetHue")
			return
		end

		newHue = tonumber(hueindex)

		this:RequestClientTargetGameObj(this, "hue")
	end,

	SetStat = function(...)
		local arg = table.pack(...)
		if(#arg < 2) then 
			Usage("setstat")
			return
		end
		local myStat = arg[1]
		local myVal = tonumber(arg[2])
		local myTarg = this
		if not (arg[3] == nil) then
			local objId = arg[3]
			local targObj = GameObj(tonumber(objId))
			if(targObj == nil) or not(targObj:IsValid()) then
				this:SystemMessage("[F7CC0A] Invalid Set Target")
				return
			end
			myTarg = targObj
		end
		
		if( string.lower(myStat) == "hp" ) then
			--DebugMessage("SETTING TO "..myVal)
			SetCurHealth(myTarg,myVal)
		elseif(string.lower(myStat) == "mana" ) then
			SetCurMana(myTarg,myVal)
		elseif( string.lower(myStat) == "sta") then
			SetCurStamina(myTarg,myVal)
		elseif( string.lower(myStat) == "vit" ) then
			SetCurVitality(myTarg,myVal)
		-- else its a base stat
		else
			local mySname =string.sub(myStat,2)
			local mySstart = string.sub(myStat,1,1)
			myStat = string.upper(mySstart) .. string.lower(mySname)

			if(myVal <= ServerSettings.Stats.IndividualPlayerStatCap) and (myVal >= ServerSettings.Stats.IndividualStatMin) then
				local myMaxVal = GetStatCap(myTarg) - (GetTotalStats(myTarg) - myTarg:GetStatValue(myStat))
				DebugMessage(myStat,myVal,myMaxVal)
				SetStatByName(myTarg,myStat, math.min(myVal,myMaxVal))
				if (myVal > myMaxVal) then
					this:SystemMessage("[FA0C0C]Entered value exceeds maximum stat cap.[-]")
				end
				this:SystemMessage("[F7CC0A] Set " ..myTarg:GetName() .. " "..myStat.. " to " ..math.min(myVal,myMaxVal))
			else
				this:SystemMessage("Stat Range Must be between "..ServerSettings.Stats.IndividualStatMin.." and "..ServerSettings.Stats.IndividualPlayerStatCap)
			end
		end
	end,

	SetSkill = function(...)		
		local arg = table.pack(...)
		if(#arg < 2) then 
			Usage("setskill")
			return
		end
		local mySkill = arg[1]
		local myVal = tonumber(arg[2])
		local myTarg = this

		if not (arg[3] == nil) then
			local objId = tonumber(arg[3])
			if (type(objId) ~= "number") then return end
			local targObj = GameObj(objId)
			if(targObj == nil) or not(targObj:IsValid()) then
				this:SystemMessage("[F7CC0A]Invalid Set Target[-]")
				return
			end
			myTarg = targObj
		end
		local mySname =string.sub(mySkill,2)
		local mySstart = string.sub(mySkill,1,1)
		skillName = string.upper(mySstart) .. mySname .. "Skill"
		if(IsValidSkill(skillName)) then
			if(tostring(myVal) == "?") then
				local myLev = GetSkillLevel(myTarg,skillName)
				this:SystemMessage(myTarg:GetName() .. " " .. skillName .. " : " .. myLev.."[-]")
				return
			end
			SetSkillLevel(myTarg, skillName, myVal, false)
			if not (myTarg == nil) then
				this:SystemMessage("[F7CC0A]Set ".. myTarg:GetName().. " " .. skillName .. " to " .. myVal.."[-]")
			end
		else
			this:SystemMessage("[F7CC0A]Invalid Skill Set Request : (" .. skillName .. ")[-]")
		end
	end,

	SetAllSkills = function(...)		
		local arg = table.pack(...)
		if(#arg < 1) then 
			Usage("setallskills")
			return
		end
		local myVal = tonumber(arg[1])
		local myTarg = this
		if ( arg[2] ) then
			local objId = tonumber(arg[2])
			if (type(objId) ~= "number") then return end
			local targObj = GameObj(objId)
			if(targObj == nil) or not(targObj:IsValid()) then
				this:SystemMessage("[F7CC0A]Invalid Set Target[-]")
				return
			end
			myTarg = targObj
		end
		local skillDictionary = {}
		for name,data in pairs(SkillData.AllSkills) do
			skillDictionary[name] = {
				SkillLevel = myVal
			}
		end
		SetSkillDictionary(myTarg, skillDictionary)
		myTarg:SystemMessage("All skills set to "..myVal, "event")
	end,

	CreateFromFile = function(filename)
		DoCreateFromFile(filename)
	end,

	Nearby = function()
		local nearbyObjects = FindObjects(SearchObjectInRange(20))
		for i,nearbyObject in pairs(nearbyObjects) do
			this:SystemMessage(nearbyObject:GetCreationTemplateId() .. ": " .. nearbyObject.Id)
		end
	end,

	CreateCustom = function()
		OpenCreateCustomWindow()
	end,

	JumpMilitia = function()
		local players = Militia.GetOnlinePlayers()
		if ( players and #players > 0 ) then
			local random = math.random(#players)
			RequestPlayerLocation(players[random], this, function(loc, address)
				if ( loc and address ) then
					TeleportUser(GameObj(0), this, loc, address, nil, true, true, true)
				else
					this:SystemMessage("Failed to jump to Militia player.", "info")
				end
			end)
		else
			this:SystemMessage("No online Militia players found.", "info")
		end
	end,

	AddMilitiaResource = function (militiaId,resource,amount)
		militiaId = tonumber(militiaId)
		amount = tonumber(amount)

		Militia.AddGlobalResource(militiaId,resource,amount) 
		this:SystemMessage(amount.." Spiritwood added.","info")
	end,


	GlobalSummon = function(id, force)
		if not( id ) then
			Usage("summon")
			return
		end

		local player = GameObj(tonumber(id))
		if ( force == "force" or GlobalVarReadKey("User.Online", player) ) then
			player:SendMessageGlobal("GlobalSummon", this:GetLoc(), ServerSettings.RegionAddress)
			this:SystemMessage("Summoning player with id "..id, "info")
		else
			this:SystemMessage("Did not find that player online. Add force to skip check.", "info")
		end
	end,

	TeleportAll = function()
		local found = FindPlayersInRegion()
		for index, obj in pairs(found) do
			obj:SetWorldPosition(this:GetLoc())
		end
	end,

	CalculateRanks = function()
		this:SystemMessage("Calculating all ranks.")
		Militia.CalculateAllRanks()
	end,

	ShowProps = function(id)
		local obj = GameObj(tonumber(id))
		this:SystemMessage("Showing Properties for "..id)
		this:SystemMessage("----------------------------")
		for propName,propValue in pairs(obj:GetAllSharedObjectProperties()) do 
			this:SystemMessage(propName..": "..tostring(propValue))
		end
	end,

	SetObjProp = function(...)
		local arg = table.pack(...)	
		if(#arg < 3) then
			Usage("setobjprop")
			return
		end

		local gameObj = this
		if( arg[1] ~= "self" ) then
			gameObj = GameObj(tonumber(arg[1]))
		end

		local curProp = gameObj:GetSharedObjectProperty(arg[2])
		if(curProp == nil) then
			this:SystemMessage("Invalid object property: "..arg[2])
		end

		if(arg[3]:sub(0,3) == "Str") then
			local str = ""
			for i = 4,#arg do str = str .. tostring(arg[i]) .. " " end
			gameObj:SetSharedObjectProperty(arg[2],str)
		elseif(arg[3]:sub(0,4) == "Bool") then
			local val = false;
			if(arg[4]:lower() == "true") then
				val = true
			end
			gameObj:SetSharedObjectProperty(arg[2],val)
		elseif( tonumber(arg[3]) ~= nil ) then
			gameObj:SetSharedObjectProperty(arg[2],tonumber(arg[3]))
		else
			gameObj:SetSharedObjectProperty(arg[2],arg[3])
		end
	end,	

	SetFacing = function(...)				
		local arg = table.pack(...)
		if( #arg < 1) then
			Usage("setfacing")
			return
		end

		local targetObj = GameObj(tonumber(arg[1]))
		if( targetObj:IsValid() ) then
			if( #arg > 1 ) then
				targetObj:SetFacing(arg[2])
			else
				targetObj:SetFacing(this:GetFacing())
			end
		end
	end,

	PlayEffect = function(effectName,effectArgs)		
		if( effectName ~= nil and effectName ~= "" ) then
			this:PlayEffectWithArgs(effectName,0.0,"Bone=Ground")
		else
			Usage("playeffect")
		end
	end,

	PlaySound = function(soundName)				
		if( soundName ~= nil) then
			this:PlayObjectSound(soundName,false)
		else
			Usage("playsound")
		end
	end,

	PlayMusic = function(soundName)				
		if( soundName ~= nil) then
			this:PlayMusic(soundName)
		else
			Usage("playmusic")
		end
	end,

	PlayAnim = function(...)				
		local arg = table.pack(...)
		if( #arg >= 2) then
			local targetObj = GameObj(tonumber(arg[1]))
			if( targetObj:IsValid() ) then
				targetObj:PlayAnimation(arg[2])
			end
		elseif( #arg >= 1) then
			this:PlayAnimation(arg[1])
		else
			Usage("playanim")
		end
	end,

	SaveHotbar = function(filename)
		-- defined in base_player_hotbar
		SaveHotbarToXML(filename)
	end,

	LoadHotbar = function (filename)
		LoadHotbarFromXML(filename)		
	end,

	LuaDebug = function(level)
		if( level == nil ) then
			Usage("luadebug")
			return
		end

		SetLuaDebugLevel(level)
		this:SystemMessage("New Log Level: "..level)
		DebugMessage("New Log Level: "..level)
	end,

	GmMessage = function()
		if not(this:HasModule("gm_message_window")) then
            this:AddModule("gm_message_window")        
        end
	end,

	HasTimer = function(...)
		local arg = table.pack(...)
		if(#arg ~= 2) then
			Usage("hastimer")
			return
		end

		local gameObj = GameObj(tonumber(arg[1]))
		this:SystemMessage("HAS SWING" .. tostring(arg[2]) .. tostring(gameObj:HasTimer(arg[2])))
	end,	

	NewSeason = function(days)
		local durationDays
		if ( days ~= nil and type(days) == "Number") then	
			durationDays = days
		else
			durationDays = ServerSettings.Militia.SeasonDurationDays or 90
		end

		ClientDialog.Show{
		TargetUser = this,
		DialogId = "NewSeasonDialogue",
		TitleStr = "Are you sure?",
		DescStr = "Reset all rankings and start a new season?",
		Button1Str = "Yes",
		Button2Str = "No",
		ResponseFunc = function ( user, buttonId )
			buttonId = tonumber(buttonId)
			if ( buttonId == 0 ) then			
				this:SystemMessage("Starting a new season with duration of "..tostring(durationDays).." days.")	
				Militia.DoNewSeason(durationDays)
			else
				this:SystemMessage("Season reset cancelled.")
			end
		end
		}
	end,

	SeasonEnd = function(days)
		local durationDays
		if ( days ~= nil ) then	
			durationDays = days
		else
			durationDays = ServerSettings.Militia.SeasonDurationDays or 90
		end

		ClientDialog.Show{
		TargetUser = this,
		DialogId = "NewSeasonDialogue",
		TitleStr = "Are you sure?",
		DescStr = "Change days left in current Militia season?",
		Button1Str = "Yes",
		Button2Str = "No",
		ResponseFunc = function ( user, buttonId )
			buttonId = tonumber(buttonId)
			if ( buttonId == 0 ) then			
				this:SystemMessage("Setting season to end in "..tostring(durationDays).." days.")	
				Militia.SetDaysUntilEnd(durationDays)
			else
				this:SystemMessage("Season ending adjustment cancelled.")
			end
		end
		}
	end,

	UpdateRange = function(range)
		if( range == nil ) then Usage("updaterange") return end

		this:SetUpdateRange(tonumber(range))

		rangeStr = range or "[Default]"
		this:SystemMessage("Update range set to "..rangeStr)
	end,	

	ReloadBehavior = function(behaviorName)
		if( behaviorName == nil ) then
			Usage("reloadbehavior")
		end

		ReloadModule(behaviorName)
	end,

	TransferAll = function (targetRegion)		
		for i,userEntry in pairs(FindGlobalUsers()) do
			if(userEntry.RegionAddress ~= targetRegion) then
				userEntry.Obj:SendMessageGlobal("transfer",targetRegion)			
			end
		end
	end,

	CompleteAchievement = function(AchievementCategory,AchievementType, AchievementLevel)
		if (AchievementCategory ~= nil and AchievementType ~= nil) then
			local achievementCategory =string.sub(AchievementCategory,2)
			local achievementCStart = string.sub(AchievementCategory,1,1)
			AchievementCategory = string.upper(achievementCStart) .. achievementCategory

			local achievementType = string.sub(AchievementType,2)
			local achievementTStart = string.sub(AchievementType,1,1)
			AchievementType = string.upper(achievementTStart) .. achievementType
			local reference = {}
			if (AchievementCategory == "Skill") then
				AchievementType = AchievementType.."Skill"
				reference.TitleCheck = "Skill"
			end

			CheckAchievementStatus(this, AchievementCategory, AchievementType, tonumber(AchievementLevel), reference)
		end
	end,

	CompleteAllAchievements = function(AchievementCategory)
		if (AchievementCategory == nil) then
			return
		end
		local achievementCategory = string.sub(AchievementCategory,2)
		local achievementCStart = string.sub(AchievementCategory,1,1)
		AchievementCategory = string.upper(achievementCStart)..achievementCategory

		local achievementTab = AchievementCategory.."Achievements"
		if (AllAchievements[achievementTab] ~= nil) then
			local reference = {}
			reference.NoRestriction = true
			if (AchievementCategory == "Skill") then
				reference.TitleCheck = "Skill"
			end

			for key,value in pairs(AllAchievements[achievementTab]) do
				CheckAchievementStatus(this, AchievementCategory, key, 1, reference)
			end
		end
	end,

	SitNpc = function ()
		this:SystemMessage("Select mobile","info")
		this:RequestClientTargetGameObj(this, "npcsit")
	end,

	NpcCommand = function (command,...)
		for i,nearbyNpc in pairs(FindObjects(SearchMobileInRange(50))) do
			if not(nearbyNpc:IsPlayer()) then
				nearbyNpc:SendMessage(command,...)
			end
		end
	end,	

	ListMobs = function()
		for i,mobObj in pairs(FindObjects(SearchMobileInRange(9999))) do
			DebugMessage(i .. ": ".. mobObj:GetName() .. " | ".. mobObj:GetCreationTemplateId())
		end
	end,

	CreateAllItems = function (unique)
		local templateCategories = GetTemplateCategories()
		local xPos = this:GetLoc().X
		local zPos = this:GetLoc().Z

		if(unique) then
			createAllIds = {}
		end

		for i, category in pairs(templateCategories) do
			CreateObj("treasurechest",Loc(xPos,0,zPos),"create_all_items",category,unique)
			xPos = xPos + 1
		end
	end,

	RelativePos = function()
		this:SystemMessage("[$2469]")
		this:RequestClientTargetAnyObj(this,"relpos1")
	end,

	SetVisualState = function (stateName)
		visualStateName = stateName
		this:RequestClientTargetAnyObj(this,"SetVisualState")
	end,

	ReloadTemplates = function()
		ReloadTemplates()
	end,

	ToggleInvuln = function(targetId)
		if(targetId ~= nil) then
			local targetObj = nil
			if(targetId == 'self') then
				targetObj = this
			else
				targetObj = GameObj(tonumber(targetId))
			end

			DoToggleInvuln(targetObj)
		else
			this:RequestClientTargetGameObj(this,"invuln")
		end
	end,

	Cloak = function(nameOrId)	
		local targetObj = this	

		if(nameOrId ~= nil) then		
			targetObj = GetPlayerByNameOrId(nameOrId)
		end
			
		if( targetObj ~= nil ) then
			local isCloaked = targetObj:IsCloaked()
			targetObj:SetCloak(not(isCloaked))
		end
	end,

	DistributeRewards = function (loottable)
		local loottable = TemplateDefines.LootTable[loottable]
		if not(loottable) then
			this:SystemMessage("Loot table not found.","info")
		else
			local playerCount = 0
			local rewardedCount = 0
			DistributeBossRewards(FindObjects(SearchPlayerInRange(50)),{loottable},achievementName, isQuiet, guardProtection, 9999, function(playerObj,itemsSpawned)
				playerCount = playerCount + 1
				if(itemsSpawned and #itemsSpawned > 0) then
					rewardedCount = rewardedCount + 1

					playerObj:SystemMessage("You have been rewarded.","info")
					playerObj:PlayEffect("RedCoreImpactWaveEffect")	
					playerObj:PlayEffect("DeathwaveEffect")	
					playerObj:PlayObjectSound("event:/magic/fire/magic_fire_fireball_impact",false)
					playerObj:PlayLocalEffect(this,"ScreenShakeEffect", 0.3,"Magnitude=10")
				end
			end)

			this:SystemMessage("Players included: "..playerCount.." Earned a reward: "..rewardedCount)
		end
	end,

	TestCOs = function (craftingSkill, count, highestOfThree)
		local craftOrderSkill = nil
		if ( craftingSkill ) then
			if ( craftingSkill == "blacksmithing" or craftingSkill == "Blacksmithing" ) then
				craftOrderSkill = "MetalsmithSkill"
			elseif ( craftingSkill == "carpentry" or craftingSkill == "Carpentry" ) then
				craftOrderSkill = "WoodsmithSkill"
			elseif ( craftingSkill == "fabrication" or craftingSkill == "Fabrication" ) then
				craftOrderSkill = "FabricationSkill"
			else
				this:SystemMessage("Invalid crafting skill. Must be blacksmithing, carpentry, or fabrication.")
				return false
			end
		end
		SimulateCOs(this, craftOrderSkill, count, highestOfThree)
	end,

	PackPlot = function(plotObjId)
		local controller = GameObj(tonumber(plotObjId))
		Plot.Pack(controller,this:GetAttachedUserId(),function ()
                this:SystemMessage("Your plot has been packed into storage and will be accessible on login.","info")
            end)
	end,

	GiveLeagueFavor = function(league, amount, playerObjId)
		
		local playerObj = this
		if( playerObjId ) then playerObj = GameObj(tonumber(playerObjId)) end
		if( playerObj:IsValid() ) then
			QuestsLeague.AdjustLeagueCurrency( playerObj, league, tonumber(amount) )
		end
		
	end,

	ClearSeasonData = function( playerObjId )
		local playerObj = this
		if( playerObjId ) then playerObj = GameObj(tonumber(playerObjId)) end
		ChallengeSystemHelper.SetGlobalVar(playerObj, ServerSettings.ChallengeSystem.SeasonObjVar, tostring(1000) )
		ChallengeSystemHelper.DoUpkeep( playerObj )
	end,

	SetSeasonPVETokens = function( _amount, _playerObjId )
		if( not _amount ) then return end
		local playerObj = this
		if( _playerObjId ) then playerObj = GameObj(tonumber(_playerObjId)) end
		ChallengeSystemHelper.SetTokenAmount( playerObj, _amount )
	end,

	SetSeasonPVPTokens = function( _amount, _playerObjId )
		if( not _amount ) then return end
		local playerObj = this
		if( _playerObjId ) then playerObj = GameObj(tonumber(_playerObjId)) end
		ChallengeSystemHelper.SetTokenAmount( playerObj, _amount, true )
	end,

	GrowPlant = function()
		RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse, "GrowPlant", 
		function(target)
			Gardening.DoGrowth( target )
		end)
		this:RequestClientTargetGameObj(this, "GrowPlant")
	end,

	CreateSpawn = function ( _template )
		if( _template ) then
			createSpawnTemplate = _template
			this:SystemMessage("Select Location","info")
			this:RequestClientTargetLoc(this, "CreateSpawn")
		end
		this:SystemMessage("Template Not Defined!","info")
	end,

	RefreshTooltip = function()
		RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse,"refreshtooltip",
		function(target,user)
			SetItemTooltip(target)
		end)

		this:RequestClientTargetGameObj(this,"refreshtooltip")
	end,

	SetHirelingOwner = function( _playerObjId )
		this:SystemMessage("Choose the hireling to set the owner for.","info")
		RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse, "SetHirelingOwner", 
		function(target,user)
			if( target ) then
				this:SystemMessage("Choose the player to set as owner.", "info")
				RegisterSingleEventHandler(EventType.ClientTargetGameObjResponse, "SetOwnerForHireling", 
				function(owner)
					if( owner:IsPlayer() ) then
						target:SetObjVar("HirelingOwner", owner)
					end
				end)
				
				this:RequestClientTargetGameObj(this, "SetOwnerForHireling")
			end
		end)
		this:RequestClientTargetGameObj(this, "SetHirelingOwner")
	end,

}

RegisterCommand{ Command="sat", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SAT, Usage="[<template>]", Desc="[$2478]" }

RegisterCommand{ Command="create", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Create, Usage="[<template>]", Desc="[$2478]" }
RegisterCommand{ Command="cotest", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.TestCOs, Usage="<blacksmithing/carpentry/fabrication> [count] [highestOfThree(true)]", Desc="Simulate crafting order generation and reward." }
RegisterCommand{ Command="createequippedobject", 	Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CreateEquippedObject, Usage="<template> [<targetid]", Desc="Created the object as an equipped object on the target." }
RegisterCommand{ Command="createprefab", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CreatePrefab, Usage="<prefabname>", Desc="Create a prefab object of the specified name", Aliases={"createp"}}
RegisterCommand{ Command="craftneeded", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CraftNeeded, Desc="Craft all needed items for currently active quests."}
RegisterCommand{ Command="setquest", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetQuest, Usage="[<name|id>]", Desc="Manual control over active quests."}
RegisterCommand{ Command="clearcomm", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.ClearCommissions, Desc="Clear Crafter's Commissions Table"}
RegisterCommand{ Command="craftordered", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CraftOrdered, Desc="Craft all ordered items for held Crafting Orders."}
RegisterCommand{ Command="createcamp", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CreateCamp, Usage="<campname>", Desc="Create a camp prefab of the specified name", Aliases={"createc"}}
RegisterCommand{ Command="destroy", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Destroy, Usage="[<target_id>]", Desc="[$2479]" }
RegisterCommand{ Command="resurrect", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Resurrect, Desc="[$2480]", Aliases={"res"} }
RegisterCommand{ Command="resall", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.ResurrectAll, Usage="[force]", Desc="[$2481]" }
RegisterCommand{ Command="heal",					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Heal, Desc="Completely heal an object. Gives cursor" }
RegisterCommand{ Command="slay", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Slay, Desc="Instantly kill any mobile. Gives cursor" }	
RegisterCommand{ Command="nuke", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Nuke, Usage="[<all|npc>] [<radius>]", Desc="Instantly kill all mobiles in a radius around you." }
RegisterCommand{ Command="createcoins", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CreateCoins, Usage="<amount>", Desc="Create a bag of coins in your backpack" }
RegisterCommand{ Command="portal", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Portal, Usage="[<x>] [<y>] [<z>]", Desc="Open a two way portal to a location on the map" }
RegisterCommand{ Command="broadcast", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Broadcast, Usage="<message>", Desc="Send a message to every player on the server", Aliases={"bcast"}}	
RegisterCommand{ Command="localbroadcast", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.LocalBroadcast, Usage="<message>", Desc="Send a message to every player on this server region", Aliases={"localbcast"}}	
RegisterCommand{ Command="push", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.PushObject, Desc="[$2482]"}	
RegisterCommand{ Command="settime", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetTime, Usage="<newtime>", Desc="Sets the current game time (24 hour format)" }
RegisterCommand{ Command="addtitle", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.AddTitle, Usage="<name|id> <title>", Desc="Grant a title to a player" }
RegisterCommand{ Command="joinguild",				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.JoinGuild, Usage="[<guild_id>]", Desc="[$2483]" }
RegisterCommand{ Command="kickuser", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.KickUser, Usage="<name|id> [<reason>]", Desc="Kick the user off the shard" }
RegisterCommand{ Command="kickall", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.KickAll, Usage="[<reason>]", Desc="Kick every user off the shard" }
RegisterCommand{ Command="banuser", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.LuaBanUser, Usage="<userid> [<hours>]", Desc="Bans the user off the shard. Specify hours for temporary bans." }
RegisterCommand{ Command="unbanuser", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.LuaUnbanUser, Usage="<userid>", Desc="Unbans a user from the shard" }
RegisterCommand{ Command="banip", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.LuaBanIP, Usage="<ip address> [<hours>]", Desc="Bans an ip address from the shard, support * wildcards. Specify hours for temporary bans" }
RegisterCommand{ Command="unbanip", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.LuaUnbanIP, Usage="<ip address>", Desc="Unbans an ip address from the shard, must match exactly a previously banned ip address" }
RegisterCommand{ Command="clearbans", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.LuaClearBans, Desc="Clears all bans from the server." }
RegisterCommand{ Command="getbans", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.LuaGetBans, Desc="Returns a list of all active server bans, and their durations." }
RegisterCommand{ Command="jumpmilitia", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.JumpMilitia, Desc="Jump to a random Militia member." }
RegisterCommand{ Command="addmilitiaresource", 		Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.AddMilitiaResource, Usage="<militiaid> <resource> <amount>", Desc="Adds to the global resource for a militia." }
RegisterCommand{ Command="summon", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.GlobalSummon, Usage="<id>",  Desc="Will pull any mobile with this ID to your location." }	
RegisterCommand{ Command="teleportall", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.TeleportAll, Desc="[$2484]" }
RegisterCommand{ Command="newseason", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.NewSeason, Usage="<days>", Desc="Forces the start of a new Militia season." }
RegisterCommand{ Command="setseasonend", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SeasonEnd, Usage="<days>", Desc="Change the amount of days left in current Militia season." }
RegisterCommand{ Command="setspeed", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=function(speed) SetMobileMod(this, "MoveSpeedPlus", "GodCommand", tonumber(speed))  end, Desc="Add the number given to your movement speed. If no speed is provided, it will remove the modifer." }
RegisterCommand{ Command="makebook", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=function(a,b) PrestigeCommandMakeBook(this,a,b) end, Desc="Make a prestige book.", Usage="<PrestigeAbility>" }
RegisterCommand{ Command="prestige", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=function(a,b,c,d) PrestigeAbilityWindow(this,a,b,c,d) end, Desc="Handy dandy prestige window." }
RegisterCommand{ Command="learnrecipes", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=function(user)LearnAllRecipes(this) end, Desc="Learn all crafting recipes." }
RegisterCommand{ Command="createmission", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=function(missionKey)AddMission(this, missionKey, this:GetLoc()) end, Desc="Add a mission to the player and spawn the mission controller at their feet." }
RegisterCommand{ Command="completeachievement", 	Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CompleteAchievement, Desc="Complete one achievement.", Usage="<AchievementCategory> <AchievementType> <AchievementLevel>"}
RegisterCommand{ Command="completeallachievements",	Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CompleteAllAchievements, Desc="Complete all achievements in one tab.", Usage="<AchievementCategory>"}
RegisterCommand{ Command="sitnpc", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SitNpc, Desc="Force an NPC to sit in a chair."}
RegisterCommand{ Command="npccommand", 				Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.NpcCommand, Desc="Send a message to all nearby NPCs"}
RegisterCommand{ Command="createmapmarker", 		Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=function(tooltip,icon)CreateMapMarker(this:GetLoc(), tooltip, icon) end, Desc="Create a static map marker that can be discovered by the player." }
RegisterCommand{ Command="closewindow", 			Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=function(window) this:CloseDynamicWindow(window) end, Desc="Close a Dynamic Window by ID." }
RegisterCommand{ Command="msg", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.GmMessage, Desc="Send GM message to user"}
RegisterCommand{ Command="forcedisconnect", 		Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=function(objectId)ForceDisconnect(objectId) end, Desc="Forces a player character to be immediately disconnected and logged out of the server, not waiting for a character save." }
RegisterCommand{ Command="invuln", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.ToggleInvuln, Usage="[<target_id|self>]", Desc="Toggle invulnerability", Aliases={"inv"} }
RegisterCommand{ Command="cloak", 					Category = "God Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Cloak, Usage="[<name|id>]", Desc="[$2476]" }


RegisterCommand{ Command="globalvar", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.GlobalVar, Usage = "<globalvarpath>", Desc="[$2485]" }
RegisterCommand{ Command="deleteglobalvar", 		Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.DeleteGlobalVar, Usage = "<globalvarpath>", Desc="Deletes any global variable that matches the path." }	
RegisterCommand{ Command="debug", 					Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Debug, Desc="Sets an object to debug. Gives cursor" }	
RegisterCommand{ Command="aistate", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.AIState, Desc="Gets the current state of an object. Gives cursor" }
RegisterCommand{ Command="copyform", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CopyForm, Usage="", Desc="[$2487]" }
RegisterCommand{ Command="possess", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Possess, Desc="Possess a target mobile." }
RegisterCommand{ Command="editchar", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.EditChar, "Opens a window that allows you to alter skill/stat values" }
RegisterCommand{ Command="setname", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetName, Usage="<name>", Desc="[$2488]" }
RegisterCommand{ Command="setbody", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Body, Usage="<template>", Desc="[$2489]" }
RegisterCommand{ Command="changeform", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.ChangeForm, Usage="<template>", Desc="[$2490]" }
RegisterCommand{ Command="setscale", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Scale, Usage="<scale_multiplier>", Desc="Set the scale of a mobile. Gives you a cursor" }
RegisterCommand{ Command="setcolor", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Color, Usage="<colorcode>", Desc="[$2491]" }	
RegisterCommand{ Command="sethue", 					Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Hue, Usage="<hueindex>", Desc="Set an object's hue via color replacement from the client hue table." }	
RegisterCommand{ Command="setstat", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetStat, Usage="<hp|mana|sta|str|agi|int> <value> [<target_id>]", Desc="[$2492]" }
RegisterCommand{ Command="setskill", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetSkill, Usage="<skill_name> <value> [<target_id>]", "[$2494]" }
RegisterCommand{ Command="setallskills", 			Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetAllSkills, Usage="<value> [<target_id>]", "Set all skills to a level." }
RegisterCommand{ Command="createfromfile", 			Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CreateFromFile, Usage="filenamee", Desc="[$2499]"}	
RegisterCommand{ Command="nearby",					Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.Nearby, Desc="[$2503]" }
RegisterCommand{ Command="createcustom", 			Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CreateCustom, Desc="[DEBUG COMMAND] Create a custom object by it's art asset ID (Note: this should not be use to create game items since the game relies on the template name)" }
RegisterCommand{ Command="calculateranks", 			Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CalculateRanks, Desc="Calculate Militia rankings, standings, and percentiles from roster controllers." }
RegisterCommand{ Command="props", 					Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.ShowProps, Usage="<target_id|self>", Desc="List all object properties on an object." }
RegisterCommand{ Command="setobjprop", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetObjProp, Usage="[$2501]", Desc="Set an object property (see TagDefinitions.xml) for the specified object. Can use 'self' in place of id. If no type specified, defaults to number or one word string." }
RegisterCommand{ Command="setfacing", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetFacing, Usage="<id> [<direction>]", Desc="[$2502]" }
RegisterCommand{ Command="playeffect", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.PlayEffect, Usage="<effect_name>", Desc="Play a particle effect at your location." }
RegisterCommand{ Command="playsound", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.PlaySound, Usage="<sound_name>", Desc="Play a sound at your location" }
RegisterCommand{ Command="playmusic", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.PlayMusic, Usage="<sound_name>", Desc="Play music (only yourself)" }
RegisterCommand{ Command="playanim", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.PlayAnim, Usage="[<target_id>] <anim_name>", Desc="Play an animation on your character" }
RegisterCommand{ Command="listmobs", 				Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.ListMobs, Desc="List all mobs on the server region." }
RegisterCommand{ Command="distributerewards",      	Category = "Dev Power", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.DistributeRewards, Usage="<loottable>", Desc="Distributes a loot table to every nearby player." }
RegisterCommand{ Command="growplant",		 		Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.GrowPlant, Usage="[<target_id>]", Desc="Allow you to target a plant you wish to grow."}


RegisterCommand{ Command="savehotbar", 				Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SaveHotbar, Usage="<filename>", Desc="[DEBUG COMMAND] Save hotbar to xml file." }
RegisterCommand{ Command="loadhotbar", 				Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.LoadHotbar, Usage="<filename>", Desc="[DEBUG COMMAND] Load hotbar from xml file." }
RegisterCommand{ Command="setvisualstate", 			Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetVisualState, Desc="[DEBUG COMMAND] Manually set the visual state of a permanent object. For development only!" }
RegisterCommand{ Command="luadebug", 				Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.LuaDebug, Usage="<Off|Error|Warning|Information|Verbose>", Desc="[$2506]" }
RegisterCommand{ Command="hastimer", 				Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.HasTimer, Usage="<target_id> <timer_name>", Desc="[$2512]" }
RegisterCommand{ Command="updaterange", 			Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.UpdateRange, Usage="<range>", Desc="[$2510]" } 
RegisterCommand{ Command="reloadbehavior", 			Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.ReloadBehavior, Usage="<behavior>", Desc="[DEBUG COMMAND] Reload the behavior in memory.", Aliases={"reload"}}
RegisterCommand{ Command="transferall", 			Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.TransferAll, Usage="<transferregion>", Desc="[$2513]"}
RegisterCommand{ Command="reloadtemplates", 		Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.ReloadTemplates, Desc="[DEBUG COMMAND] Reload all templates in memory." }
RegisterCommand{ Command="createallitems", 			Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CreateAllItems, Usage="<unique>", Desc="Create every carryable item inside containers. If passed 'unique' then only one of each client id is created" }

RegisterCommand{ Command="relativepos", 			Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.RelativePos, Desc="[$2515]"}
RegisterCommand{ Command="packplot", 			    Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.PackPlot, Usage="<target_plot>", Desc="Packs a house into a crate."}
RegisterCommand{ Command="giveleaguefavor", 		Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.GiveLeagueFavor, Usage="<league> <amount> <playerId|self> ", Desc="Grants league favor to specified player."}
RegisterCommand{ Command="clearseasondata", 		Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.ClearSeasonData, Usage="<playerId|self> ", Desc="Clears all the seasonal tokens and claimed rewards."}
RegisterCommand{ Command="setseasonpvetokens", 		Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetSeasonPVETokens, Usage="<amount> [<playerId|self>]", Desc="Sets the amount of season PVE tokens. Optional: <playerId>."}
RegisterCommand{ Command="setseasonpvptokens", 		Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetSeasonPVPTokens, Usage="<amount> [<playerId|self>]", Desc="Sets the amount of season PVP tokens. Optional: <playerId>."}
RegisterCommand{ Command="createspawn",		 		Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.CreateSpawn, Usage="<template>", Desc="Creates a SeedObject entry in create_spawn.txt at server root."}
RegisterCommand{ Command="refreshtooltip",		 	Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.RefreshTooltip, Usage="<target_object>", Desc="Refreshes the tooltip on the targeted item."}
RegisterCommand{ Command="sethirelingowner",		Category = "Debug", AccessLevel = AccessLevel.DemiGod, Func=DemigodCommandFuncs.SetHirelingOwner, Usage="<hireling_object> <player_object>", Desc="Sets the owner of a targeted hireling to the targeted player."}
