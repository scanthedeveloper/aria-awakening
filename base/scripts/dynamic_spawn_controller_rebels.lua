require 'dynamic_spawn_controller'
mDynamicSpawnKey = "DynamicRebelSpawner"

mDynamicHUDVars.Title = "Rebel Reinforcements"
mDynamicHUDVars.Description = "Defeat the Rebel High General and his reinforcements."


local oldProgressAdjusted = ProgressAdjusted
function ProgressAdjusted( progress, mobsStillAlive )
    oldProgressAdjusted(progress, mobsStillAlive)
    local msg = ""
    if( progress == 1 ) then
        msg = "You have found the rebel reinforcements."
    elseif( progress == 2 ) then 
        msg = "More reinforcements have been called."
    elseif( progress == 3 ) then 
        msg = "More reinforcements have been called."
    elseif( progress == 4 ) then 
        msg = "The Rebel High General approaches."
    elseif( progress == 999 ) then 
        msg = "The rebel event has timed out."
    end

    -- Broadcast message to players
    HelperSpawns.ForeachPlayerInSpawnRegion( this, function(player) 
        player:SystemMessage(msg)
        player:SystemMessage(msg,"info")
    end)

end