require 'base_ai_npc'

AI.Settings.StationedLeash = true
AI.Settings.CanConverse = false

function IntroDialog(user)
    Dialog.OpenGreetingDialog(user)
end

function Dialog.OpenGreetingDialog(user)
    text = "Should this be a quest?"

    response = {}

    response[1] = {}
    response[1].text = "Who are you?"
    response[1].handle = "Who" 
    
    response[2] = {}
    response[2].text = "Goodbye."
    response[2].handle = "" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end

function Dialog.OpenTalkDialog(user)

    text = AI.TalkMessages[math.random(1,#AI.TalkMessages)]

    response = {}
    local responseCount = 0


    -- Celador
    responseCount = #response + 1
    response[responseCount] = {}
    response[responseCount].text = "What is this place?"
    response[responseCount].handle = "Celador" 

    -- Nevermind
    responseCount = #response + 1
    response[responseCount] = {}
    response[responseCount].text = "Nevermind."
    response[responseCount].handle = "Nevermind" 

    NPCInteractionLongButton(text,this,user,"Responses",response)
end


function Dialog.OpenCeladorDialog(user)
	DialogReturnMessage(this,user,"Message goes here!","Right, Thank you.")
end

OverrideEventHandler("base_ai_npc",EventType.DynamicWindowResponse, "Responses",ResponsesDialog)