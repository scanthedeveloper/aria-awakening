local chance = math.random(1, 100 )

if( chance < 70 ) then
    require 'brain_group_damage'
else
    require 'brain_group_tank'
end

if (initializer ~= nil) then
    if( initializer.PirateNames ~= nil ) then    
        local name = initializer.PirateNames[math.random(#initializer.PirateNames)]
        local job = initializer.PirateJobs[math.random(#initializer.PirateJobs)]
        this:SetName(name.." the "..job)
    end
end


