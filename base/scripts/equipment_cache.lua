

-- caching some weapon information to local memory space since this only changes when weapons are changed, 
	--- but the data is read from a lot.
_Weapon = {
	LeftHand = {
		Object = nil,
		Class = "Fist",
		Type = "BareHand",
		DamageType = "Bashing",
		SkillName = "BrawlingSkill",
		IsRanged = false,
		ShieldType = nil,
		Range = ServerSettings.Stats.DefaultWeaponRange,
		Speed = 5,
		Enchants = {},
	},
	RightHand = {
		Object = nil,
		Class = "Fist",
		Type = "BareHand",
		DamageType = "Bashing",
		SkillName = "BrawlingSkill",
		IsRanged = false,
		Range = ServerSettings.Stats.DefaultWeaponRange,
		Speed = 5,
		Enchants = {},
	}
}

_Armor = {
	Chest = {
		Object = nil,
		Type = "Natural",
		Class= nil,
		ProficiencyType = nil,
		Enchants = {},
	},
	Legs = {
		Object = nil,
		Type = "Natural",
		Class= nil,
		ProficiencyType = nil,
		Enchants = {},
	},
	Head = {
		Object = nil,
		Type = "Natural",
		Class= nil,
		ProficiencyType = nil,
		Enchants = {},
	},
	Ring = {
		Object = nil,
		Type = "Natural",
		Class= nil,
		ProficiencyType = nil,
		Enchants = {},
	},
	Necklace = 
	{
		Object = nil,
		Type = "Natural",
		Class= nil,
		ProficiencyType = nil,
		Enchants = {},
	}
}


function UpdateEquipmentCache(slot, equipObj)
	Verbose("Mobile", "UpdateEquipmentCache", slot, equipObj)

	-- See if we have an equipment object; if it's explictly 
	-- false we can skip getting what's equipped
	if ( equipObj == false ) then
		equipObj = nil
	else
		equipObj = equipObj or this:GetEquippedObject(slot)
	end

	-- Handle weapons
	if( slot == "RightHand" or slot == "LeftHand" ) then
		UpdateWeaponCache(slot, equipObj)
	-- Handle armor (neck and ring not used yet)
	elseif( slot == "Head" or slot == "Legs" or slot == "Chest" or slot == "Ring" or slot == "Necklace" ) then
		--DebugMessage("Updating Slot: " .. slot)
		UpdateArmorCache(slot, equipObj)
	end
	
	-- Apply mobile mods from enchants
	local mobileMods = GetEnchantMobileMods( equipObj )
	if( mobileMods ~= nil ) then
		for i=1, #mobileMods do
			--DebugMessage( "equipment_"..slot.." : ".. mobileMods[i].MobileMod)
			SetMobileMod(this, mobileMods[i].MobileMod, "equipment_"..slot, mobileMods[i].Amount)
		end
	end

	-- Apply combat mods
	local mobileMods = GetEnchantCombatMods( equipObj )
	if( mobileMods ~= nil ) then
		for i=1, #mobileMods do
			--DebugMessage( "equipment_"..slot.." : ".. mobileMods[i].MobileMod)
			SetCombatMod(this, mobileMods[i].MobileMod, "equipment_"..slot, mobileMods[i].Amount)
		end
	end


end

-- Function to update the cached armor
function UpdateArmorCache(slot, armorObj)
	Verbose("Mobile", "UpdateArmorCache", slot, armorObj)

	-- Remove existing mobile mods
	EnchantingHelper.RemoveMobileMods( this, _Armor[slot].Enchants, "equipment_"..slot  )

	-- Remove existing combat mods
	EnchantingHelper.RemoveCombatMods( this, _Armor[slot].Enchants, "equipment_"..slot  )

	_Armor[slot] = {
		Object = armorObj,
		Type = GetArmorType(armorObj) or "Natural",
		Class = GetArmorClass(armorObj),
		ProficiencyType = GetArmorProficiencyType(armorObj),
		Enchants = Weapon.GetEnchants(armorObj)
	}

	-- Apply mobile mods
	EnchantingHelper.ApplyMobileMods( this, _Armor[slot].Enchants, "equipment_"..slot  )

	--DebugTable(_Armor[slot])
end



-- function to update the cached weapon
function UpdateWeaponCache(slot, weaponObj)
	Verbose("Mobile", "UpdateWeaponCache", slot, weaponObj)

	if( slot == "RightHand" ) then
		SorceryHelper.ToggleSorceryEffect( this, weaponObj )
	end

	-- Remove existing mobile mods
	EnchantingHelper.RemoveMobileMods( this, _Weapon[slot].Enchants, "equipment_"..slot  )

	-- Remove existing combat mods
	EnchantingHelper.RemoveCombatMods( this, _Weapon[slot].Enchants, "equipment_"..slot  )
	
	-- Handle Left hand
	if ( slot == "LeftHand" and weaponObj ) then
		_Weapon.LeftHand = {
			Object = weaponObj,
			ShieldType = GetShieldType(weaponObj),
			Enchants = Weapon.GetEnchants(weaponObj)
		}
		-- shields need nothing more
		if ( _Weapon.LeftHand.ShieldType ) then
			-- Apply mobile mods
			EnchantingHelper.ApplyMobileMods( this, _Weapon[slot].Enchants, "equipment_"..slot  )
			return 
			end
	end

	local weaponType = nil
	if ( IsPlayerCharacter(this) ) then
		weaponType = Weapon.GetType(weaponObj)
	else
		-- mobs are balanced to default to BareHand
		weaponType = this:GetObjVar("AI-WeaponType") or "BareHand"
	end

	-- some 'weapons', torch for example, are non-combat.
	if ( 
		(
			EquipmentStats.BaseWeaponStats[weaponType]
			and
			EquipmentStats.BaseWeaponStats[weaponType].NoCombat == true
		)
		or
		slot == "LeftHand"
	) then
		_Weapon[slot] = { NoCombat = true, Enchants = {} }
		return
	end

	_Weapon[slot] = {
		Object = weaponObj,
		Type = weaponType,
		Power = Weapon.GetPower(weaponObj, this),
		Class = Weapon.GetClass(weaponType),
		DamageType = Weapon.GetDamageType(weaponType),
		SkillName = Weapon.GetSkill(weaponType),
		AttackBonus = Weapon.GetAttackBonus(weaponObj),
		AccuracyBonus = Weapon.GetAccuracyBonus(weaponObj),
		IsRanged = Weapon.IsRanged(weaponType),
		ToolType = Weapon.GetToolType(weaponObj),
		Range = this:GetObjVar("WeaponRange") or Weapon.GetRange(weaponType),
		Speed = Weapon.GetSpeed(weaponType),
		Enchants = Weapon.GetEnchants(weaponObj)
	}

	-- Apply mobile mods
	EnchantingHelper.ApplyMobileMods( this, _Weapon[slot].Enchants, "equipment_"..slot  )

	if not( EquipmentStats.BaseWeaponClass[_Weapon[slot].Class] ) then
		DebugMessage(string.format("[WARNING][UpdateWeaponCache] Invalid WeaponClass '%s' (WeaponType '%s') setting default.", _Weapon[slot].Class, _Weapon[slot].Type))
		_Weapon[slot].Class = "Fist"
	end

	

	--DebugTable(_Weapon[slot])
end