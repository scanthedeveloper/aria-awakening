--- handles the event message "UseObject"
-- @param user - user of the scroll object
-- @param usedType - message string to handle from the context menu
function HandleUseObject(user,usedType)
	if ( not IsInBackpack(this, user) ) then
		user:SystemMessage("The scroll must be in your backpack.")
			
	elseif(usedType=="Add Scroll To Spellbook") then
			user:RequestClientTargetGameObj(this, "select_spellbook_targetObject")
		end
	end

--- handles the event ModuleAttached
function HandleModuleAttached()
	AddUseCase(this,"Add Scroll To Spellbook",false)
end

--- handles the event for ClientTargetGameObjResponse request
-- @param targetObject - the target custom_spellbook to attempt to add the scroll too
-- @param user - user of the scroll object
function HandleClientTargetGameObjResponse(targetObject, user)
	local addScroll = true 
	-- make sure its a custom_spellbook
	if(targetObject:HasObjVar("BookType") and targetObject:HasObjVar("SpellSchool")) then
		-- if spellschools do not match then we can't add it
		if(this:GetObjVar("SpellSchool") ~= targetObject:GetObjVar("SpellSchool")) then
			user:SystemMessage("this spell may not be added to this spellbook")
			return
		end
		for key,value in pairs(targetObject:GetObjVar("SpellList")) do
			-- if the spell is already in the book then dont add it
			if(key == this:GetObjVar("Spell")) then addScroll = false end
		end
	end
	if(addScroll == true) then
		-- tell the spellbook its okay to add the scroll to the book
		targetObject:SendMessage("AddScroll",this,user)
	else
		user:SystemMessage("this spell may not be added to this spellbook")
	end
end
----------------------------------------------------------
--EVENTTYPE - Client Target Game Object Response 
----------------------------------------------------------
RegisterEventHandler(EventType.ClientTargetGameObjResponse,"select_spellbook_targetObject",HandleClientTargetGameObjResponse)

----------------------------------------------------------
--EVENTTYPE - ModuleAttached 
----------------------------------------------------------
RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), HandleModuleAttached)

----------------------------------------------------------
--EVENTTYPE - Message "UseObject" 
----------------------------------------------------------
RegisterEventHandler(EventType.Message, "UseObject", HandleUseObject)