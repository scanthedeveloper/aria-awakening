--- helper function to generate the UserAction button for the spell list page
-- @param dynamicWindow - dynamic window to attach the user action button
-- @param x - the x location on the dynamic window
-- @param y - the y location on the dynamic window
-- @param spell - the spell key value in the SpellData.AllSpells table
function AddUserAction(dynamicWindow,x,y,spell)
	-- add spell user action
	dynamicWindow:AddUserAction(x,y,GetSpellUserAction(spell),48,48)
end

--- helper function to get the reagent list for an individual spell
-- @param reagentTable - SpellData.AllSpells.SpellKey.Reagents table
function GetReagentStr(reagentTable)
	local reagentStr = ""
	if(reagentTable and #reagentTable > 0) then
		for i,reg in pairs(reagentTable) do 
			if(reagentStr ~= "") then
				reagentStr = reagentStr .. "\n"
			end
			reagentStr = reagentStr .. reg
		end
	end

	return reagentStr
end

--- this function generates the spell information
--- this function was created by citadel in their spellbook and modified to fit this custom_spellbook system
-- @param spell - spell index in the SpellData.AllSpells table
function GetSpellUserAction(spell)
	local spellData = SpellData.AllSpells[spell]
	local icon = spellData.Icon or spell:lower()
	if(spellData.Icon == nil and spellData.IconText ~= nil) then
		icon = "LighterSlotIcon"
	end
	local displayName = spellData.SpellDisplayName or spell
	local tooltip = spellData.SpellTooltipString
	local reagentStr = GetReagentStr(spellData.Reagents)
	local minSkill = SpellData.CircleSkills[spellData.Circle + 2][1]
	local castTime = SpellData.CastTimes[spellData.Circle]
	if(reagentStr ~= "") then
		tooltip = tooltip .. "\n[FFFF88]Reagents:\n[FFFFFF]"..reagentStr
	end
	tooltip = tooltip .. "\n[FFFF88]Minimum Skill: [FFFFFF]" .. minSkill
	tooltip = tooltip .. "\n[FFFF88]Cast Time: [FFFFFF]" .. castTime .. "s"

	local requirements = {}
	local manaCost = spellData.manaCost
	if(manaCost>0) then
		table.insert(requirements,{"Mana",manaCost})
	end

	local actionData = 
	{
		ID = spell,
		ActionType = "Spell",
		DisplayName = displayName,
		Tooltip = tooltip,
		Icon = icon,
		IconText = spellData.IconText,
		Enabled = true,
		ServerCommand = "sp "..spell,
		Requirements = requirements
	}
	return actionData
end

--- this handles the message from the spell scroll when you try to add it to the custom_spellbook
-- @param scrollObject - the scroll object that targeted this spellbook
-- @param user - the user who attempted to add a scroll to this custom_spellbook
function HandleAddScroll(scrollObject,user)
	local spellList = this:GetObjVar("SpellList")
	spellList[scrollObject:GetObjVar("Spell")] = true
	this:SetObjVar("SpellList",spellList)
	user:SystemMessage("you add this spell to the spellbook")
	scrollObject:Destroy()
	UpdateTooltip()
end

--- handles the event for when you use the command to fill a custom spellbook with all spells
-- @param target - the custom spell book targeted
-- @param user - the user who used the /fillspellbook command
function HandleFillCustomSpellbook(target,user)
	if(target) then
		if(target:HasModule("custom_spellbook")) then
			local typeOfBook = target:GetObjVar("SpellSchool")
			local spellList = {}
			for key,value in pairs(SpellData.AllSpells) do
				if(value.SpellSchool) then
					-- if both spell schools match, add spell to spell list
					if(value.SpellSchool == typeOfBook) then
						spellList[key] = true
					end
				end
			end
			this:SetObjVar("SpellList",spellList)
			user:SystemMessage("the spellbook has been filled with ".. typeOfBook .. " spells")
			UpdateTooltip()
		end
	end
end

--- handles the event fire for this mod being attached to the object
function HandleModuleAttached()
	this:SetObjVar("SpellList",{})
	AddUseCase(this,"Open Book",true)
	UpdateTooltip()
end

--- handles when this spellbook is used or double clicked
function HandleUseObject(user,usedType)
	if(usedType=="Open Book") then OpenSpellbook(user) end
end

--- handles the drop action of the UserAction button to the hotbar
-- @param user - the user attempting to drag a user action to the hotbar
-- @param sourceId - the dynamic window handle sending this event
-- @param actionType -
-- @param actionId - 
-- @param slot - the hotbar slot
function HandleUserActionButtonDropToHotBar(user,sourceId,actionType,actionId,slot)
	if((sourceId == "customSpellBookHandler") and slot ~= nil) then
		local hotbarAction = nil

		if(this:GetObjVar("BookType") == "Spellbook" ) then
			hotbarAction = GetSpellUserAction(actionId)
			hotbarAction.Slot = tonumber(slot)
			RequestAddUserActionToSlot(user,hotbarAction)
		end
	end
end

--- this function opens up the spellbook for the user
-- @param user - the user attempting to open the custom_spellbook
function OpenSpellbook(user)
	-- make the dynamic window transparent and dragable, and center it on the screen
	local dynamicWindow = DynamicWindow("customSpellBookHandler","", 790, 486, -395, -243, "TransparentDraggable","Center")
	-- add the spellbook artwork
	dynamicWindow:AddImage(0, 0, "Spellbook", 789, 486)

	-- show the spellbook user interface
	-- close button
	dynamicWindow:AddButton(726, 22, "", "", 0, 0, "", "", true, "CloseSquare")
	
	-- show the spells inside the book
	local x = 120
	local y = 45
	local spellListCount = 0
	local spellList = this:GetObjVar("SpellList") or {}	
	local sortedList = {}

	-- set up sorting table to alphabatize the spell list by spell name
	for key,value in pairs(spellList) do
		table.insert(sortedList,{Name=SpellData.AllSpells[key].SpellDisplayName,Key=key})
	end

	-- sort the spells alphabetically to display in the book
	table.sort(sortedList, function(a,b) 
		return a.Name < b.Name 
	end)

	-- loop through the spells and add their UserAction buttons
	for key,value in pairs(sortedList) do
		spellListCount = spellListCount + 1
		-- only 7 spells per page so switch the y value back to the top
		if(spellListCount == 8) then y = 45 end
		if(spellListCount <= 7) then
			AddUserAction(dynamicWindow,x,y,value.Key)
			dynamicWindow:AddLabel(x+50,y+14,"[565656]"..SpellData.AllSpells[value.Key].SpellDisplayName,200,30,26,"left")
			y = y + 50
		else
			AddUserAction(dynamicWindow,x+310,y,value.Key)
			dynamicWindow:AddLabel(x+360,y+14,"[565656]"..SpellData.AllSpells[value.Key].SpellDisplayName,200,30,26,"left")
			y = y + 50			
		end		
	end
	
	-- show the spellbook to the player and setting this spellbook as the event handler for the dynamic window
	user:OpenDynamicWindow(dynamicWindow,this)
end

--- this updates the tooltip information on the spellbook
function UpdateTooltip()
	local count = 0
	for k,v in pairs(this:GetObjVar("SpellList")) do
		count = count + 1
	end
	SetTooltipEntry(this,"spell_spellListCount", "Contains ".. count .." spells")
	SetTooltipEntry(this,"spell_school", this:GetObjVar("SpellSchool"))
end

----------------------------------------------------------
--EVENTTYPE - ModuleAttached 
----------------------------------------------------------
RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), HandleModuleAttached)

----------------------------------------------------------
--EVENTTYPE - Message "UseObject" 
----------------------------------------------------------
RegisterEventHandler(EventType.Message, "UseObject", HandleUseObject)

----------------------------------------------------------
--EVENTTYPE - Message "AddScroll" 
----------------------------------------------------------
RegisterEventHandler(EventType.Message,"AddScroll", HandleAddScroll)

----------------------------------------------------------
--EVENTTYPE - Message "FillCustomSpellbook" 
----------------------------------------------------------
RegisterEventHandler(EventType.Message,"FillCustomSpellbook", HandleFillCustomSpellbook)

----------------------------------------------------------
--EVENTTYPE - Message "ClientObjectCommand" 
----------------------------------------------------------
RegisterEventHandler(EventType.ClientObjectCommand, "dropAction", HandleUserActionButtonDropToHotBar)