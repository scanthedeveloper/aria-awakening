--- 
-- @param user -
-- @param dialogTree -
-- @param name - 
function ExportDialog(user,dialogTree,name)
	local fileName = ""
	local fileStream = nil
	local directory = "mods/"..GetModName().."/scripts/"

	-- format the filename to lower case and build the string
	name = string.lower(name)
	name = string.gsub(name," ","_")
	name = string.gsub(name,"'","")
	fileName = "ai_" .. name
	
	fileStream = io.open(directory .. fileName .. ".lua","w")
	
	WriteRequirements(fileStream,dialogTree)
	WriteDialogFunctions(fileStream,dialogTree)
	if(this:HasObjVar("CanStable")) then
		WriteStableEventHandler(fileStream)
	end
	
	fileStream:close()	
	
end

--- 
-- @param fileStream -
function WriteRequirements(fileStream,dialogTree)
	fileStream:write("-- grab the Dialog code from the base_ai_npc file\n")
	fileStream:write("require 'base_ai_npc'\n\n")
	fileStream:write("-- set some general npc flags\n")
	fileStream:write("AI.Settings.EnableTrain = false\n")
	fileStream:write("AI.Settings.MerchantEnabled = false\n")
	fileStream:write("AI.Settings.SkipIntroDialog = true\n")
	if(IsBanker(dialogTree)) then
		fileStream:write("AI.Settings.EnableBank = true\n")
		fileStream:write("this:SetObjVar(\"AI-EnableBank\",true)\n")
	end
	if(this:HasObjVar("CanStable")) then
		WriteStableFunctions(fileStream)
	end
end

--- 
-- @param fileStream -
-- @param dialogTree -
function WriteDialogFunctions(fileStream,dialogTree)
	-- loop through each dialog node and generate the scripted functions
	for key,value in pairs(dialogTree) do
		fileStream:write("\n")
		
		--function declaration
		fileStream:write("---------------------------------------------------------------------------\n")
		fileStream:write("-- "..key.. " Node\n")
		fileStream:write("---------------------------------------------------------------------------\n")
		fileStream:write("--- this opens the " .. key .. " part of the dialog\n")
		fileStream:write("--@ param user - the user object to display the dialog node too\n")
		fileStream:write("function Dialog.Open" .. key .. "Dialog(user)\n")
		
		-- function body
		fileStream:write("\t-- the npc text dialog to display to the player\n")
		fileStream:write("\tlocal text = \"" .. value.NpcDialog .. "\"\n")
		fileStream:write("\t-- the table to hold the player response info\n")
		fileStream:write("\tlocal response = {}\n\n")

		WriteRequiredActions(fileStream,dialogTree,key)

		-- write the player response code here
		WritePlayerResponses(fileStream,dialogTree,key)

		-- write citadels dialog interaction code
		fileStream:write("\t-- load up the new npc interaction dynamic window for the user\n")
		fileStream:write("\tNPCInteraction(text,this,user,\"Responses\",response)\n\n")
		
		-- face npc towards user
		fileStream:write("\t-- set the attention of the npc to the user who is interacting with it\n")
		fileStream:write("\tGetAttention(user)\n")
		
		--end function
		fileStream:write("end\n")
	end
end

function WriteRequiredActions(fileStream,dialogTree,GotoHook)
	for key,value in pairs(dialogTree) do
		for k,v in pairs(value.Player.Responses) do
			local writeInfo = false
			for i,z in pairs(v.Actions) do
				local split = StringSplit(z.Values,"|")
				if(z.Type == "GotoNode" and split[1] == GotoHook) then writeInfo = true end
			end
			if(writeInfo == true) then
				fileStream:write("\t-- set or delete object variables\n")
				for i, z in pairs(v.Actions) do
					if(z.Type=="SetObjVar") then
						local split = StringSplit(z.Values,"|")
						fileStream:write("\tuser:SetObjVar(\"" .. split[1] .. "\"," .. split[2] ..")\n")
					end
					if(z.Type=="DelObjVar") then
						fileStream:write("\tuser:DelObjVar(\"" .. z.Values .. "\")\n")
					end
				end
				fileStream:write("\n")
			end
		end
	end
end

--- 
-- @param fileStream -
-- @param dialogTree -
function WritePlayerResponses(fileStream,dialogTree,node)
	for key,value in pairs(dialogTree[node].Player.Responses) do
  		if(#dialogTree[node].Player.Responses[key].Requirements > 0) then
  			-- set up condtions
  			fileStream:write("\t-- does the player meet the requirements for this response\n")
  			-- write the if statement
  			WriteConditions(fileStream,dialogTree,node,key)
 			-- end the if statement
 			fileStream:write(") then\n")
			fileStream:write("\t\tresponse[" .. key .. "]={}\n")
			fileStream:write("\t\tresponse[" .. key .. "].text = \"" .. value.Text .. "\"\n")
			fileStream:write("\t\tresponse[" .. key .. "].handle =\"" .. GetResponseHandle(dialogTree,node,key) .. "\"\n")
			-- end the if block
			fileStream:write("\tend\n\n") 			
  		else
  			fileStream:write("\tresponse[" .. key .. "]={}\n")
  			fileStream:write("\tresponse[" .. key .. "].text = \"" .. value.Text .. "\"\n")
  			fileStream:write("\tresponse[" .. key .. "].handle =\"" .. GetResponseHandle(dialogTree,node,key) .. "\"\n")
  			fileStream:write("\n")
  		end
	end
end

--- 
-- @param fileStream -
function WriteStableFunctions(fileStream)
		--function declaration
		fileStream:write("\n---------------------------------------------------------------------------\n")
		fileStream:write("-- Stable Pet functions\n")
		fileStream:write("---------------------------------------------------------------------------\n")
		fileStream:write("--- this will stable the pet\n")
		fileStream:write("--@ param user - the user object to display the dialog node too\n")
		fileStream:write("function Dialog.OpenStableDialog(user)\n")

		-- function body
		fileStream:write("\t-- have npc give targeter\n")
		fileStream:write("\tthis:NpcSpeech(\"Show me the Animal\")\n")
		fileStream:write("\t-- give the player a targeting cursor\n")
		fileStream:write("\tuser:RequestClientTargetGameObj(this, \"TargetStableAnimal\")\n")

		-- face npc towards user
		fileStream:write("\t-- set the attention of the npc to the user who is interacting with it\n")
		fileStream:write("\tGetAttention(user)\n")
		
		--end function
		fileStream:write("end\n\n")

		--function declaration
		fileStream:write("---------------------------------------------------------------------------\n")
		fileStream:write("-- Request Pet From Stable functions\n")
		fileStream:write("---------------------------------------------------------------------------\n")
		fileStream:write("--- this will retrieve your pet list\n")
		fileStream:write("--@ param user - the user object to display the dialog node too\n")
		fileStream:write("function Dialog.OpenPetDialog(user)\n")

		-- function body
		fileStream:write("\tlocal pets, slots = GetStabledPets(user)\n")
		fileStream:write("\tif ( #pets > 0 ) then\n")
		fileStream:write("\t\tShowSelectStabledPetsWindow(user, pets)\n")
		fileStream:write("\telse\n")
		fileStream:write("\t\tthis:NpcSpeech(\"I ain't got a pet for you buddy.\")\n")
		fileStream:write("\tend\n")
		-- face npc towards user
		fileStream:write("\t-- set the attention of the npc to the user who is interacting with it\n")
		fileStream:write("\tGetAttention(user)\n")
		
		--end function
		fileStream:write("end\n\n")

		--function declaration
		fileStream:write("--- \n")
		fileStream:write("--@ param user - the user object to display the dialog node too\n")
		fileStream:write("function ShowSelectStabledPetsWindow(user, pets)\n")
		fileStream:write("\tbuttonList = {}\n")
		fileStream:write("\tfor i,pet in pairs(pets) do\n")
		fileStream:write("\t\ttable.insert(buttonList,{Id=tostring(pet.Id),Text=StripColorFromString(pet:GetName())})\n")
		fileStream:write("\tend\n")
		
		-- function body
		fileStream:write("\tButtonMenu.Show{\n")
		fileStream:write("\t\tTargetUser = user,\n")
		fileStream:write("\t\tDialogId = \"unstable\",\n")
		fileStream:write("\t\tTitleStr = \"Unstable Pet\",\n")
		fileStream:write("\t\tResponseType = \"id\",\n")
		fileStream:write("\t\tButtons = buttonList,\n")
		fileStream:write("\t\tResponseObj = this,\n")
		fileStream:write("\t\tResponseFunc = function(user,buttonId)\n")
		fileStream:write("\t\t\tif(buttonId ~= nil) then\n")
		fileStream:write("\t\t\t\tlocal pet = GetStabledPetById(user, tonumber(buttonId))\n")
		fileStream:write("\t\t\t\tif ( pet ~= nil ) then\n")
		fileStream:write("\t\t\t\t\tif ( (pet:GetObjVar(\"PetSlots\") or MaxActivePetSlots(user)) > GetRemainingActivePetSlots(user) ) then\n")
		fileStream:write("\t\t\t\t\t\tthis:NpcSpeech(\"Methinks you could not control yet another pet.\")\n")
		fileStream:write("\t\t\t\t\telseif( this:DistanceFrom(user) > ServerSettings.Pets.Taming.Distance ) then\n")
		fileStream:write("\t\t\t\t\t\tuser:SystemMessage(\"You are too far away from the stable master.\", \"info\")\n")
		fileStream:write("\t\t\t\t\telse\n")
		fileStream:write("\t\t\t\t\t\tpet:SendMessage(\"Unstable\")\n")
		fileStream:write("\t\t\t\t\t\tthis:NpcSpeech(\"Here you go!\")\n")
		fileStream:write("\t\t\t\t\tend\n")
		fileStream:write("\t\t\t\tend\n")
		fileStream:write("\t\t\tend\n")
		fileStream:write("\t\tend,\n")
		fileStream:write("\t}\n")

		fileStream:write("end\n")	
end

---
-- @param fileStream -
function WriteStableEventHandler(fileStream)
	fileStream:write("\n---------------------------------------------------------------------------\n")
	fileStream:write("-- Stable Pet Event Handler\n")
	fileStream:write("---------------------------------------------------------------------------\n")
	fileStream:write("RegisterEventHandler(EventType.ClientTargetGameObjResponse, \"TargetStableAnimal\", function(target, user)\n")
	fileStream:write("\tif ( not(IsTamable(target)) ) then\n")
	fileStream:write("\t\tthis:NpcSpeech(\"That's not something I can take care of.\")\n")
	fileStream:write("\telse\n")
	fileStream:write("\t\tif ( this:GetLoc():Distance(target:GetLoc()) > 30 ) then\n")
	fileStream:write("\t\t\tuser:SystemMessage(\"That animal is too far away from the stable master.\", \"info\")\n")
	fileStream:write("\t\telse\n")
	fileStream:write("\t\t\tlocal pet = GetActivePetById(user, target.Id)\n")
	fileStream:write("\t\t\tif ( pet == nil ) then\n")
	fileStream:write("\t\t\t\tuser:SystemMessage(\"Hey, what are you trying to pull!\", \"info\")\n")
	fileStream:write("\t\t\telse\n")
	fileStream:write("\t\t\t\tlocal stabledPets, stabledSlots = GetStabledPets(user)\n")
	fileStream:write("\t\t\t\tif ( stabledSlots + 1 >= MaxStabledPetSlots(user) ) then\n")
	fileStream:write("\t\t\t\t\tthis:NpcSpeech(\"I cannot take anymore of your pets.\")\n")
	fileStream:write("\t\t\t\telse\n")
	fileStream:write("\t\t\t\t\tlocal backpack = target:GetEquippedObject(\"Backpack\")\n")
	fileStream:write("\t\t\t\t\tif ( backpack and #backpack:GetContainedObjects() > 0 ) then\n")
	fileStream:write("\t\t\t\t\t\tthis:NpcSpeech(\"That pet is carrying stuff, I'm not a bank!\")\n")
	fileStream:write("\t\t\t\t\telse\n")
	fileStream:write("\t\t\t\t\t\t-- consume 50 coins\n")
	fileStream:write("\t\t\t\t\t\tRequestConsumeResource(user, \"coins\", 50, \"StablePet\", this, pet)\n")
	fileStream:write("\t\t\t\t\tend\n")
	fileStream:write("\t\t\t\tend\n")
	fileStream:write("\t\t\tend\n")
	fileStream:write("\t\tend\n")
	fileStream:write("\tend\n")
	fileStream:write("end)\n\n")
	fileStream:write("\n---------------------------------------------------------------------------\n")
	fileStream:write("-- Stable Pet Consume Resource Event Handler\n")
	fileStream:write("---------------------------------------------------------------------------\n")
	fileStream:write("RegisterEventHandler(EventType.Message, \"ConsumeResourceResponse\", function (success,transactionId,user,pet)\n")
	fileStream:write("\tif ( pet ~= nil and transactionId == \"StablePet\" ) then\n")
	fileStream:write("\t\tif not(success) then")
	fileStream:write("\t\t\tuser:SystemMessage(\"Not enough coins to stable your pet.\", \"info\")\n")
	fileStream:write("\t\telse\n")
	fileStream:write("\t\t\tuser:SystemMessage(\"50 gold paid to stable your pet.\", \"info\")\n")
	fileStream:write("\t\t\tpet:SendMessage(\"Stable\")\n")
	fileStream:write("\t\t\tthis:NpcSpeech(\"Your pet is safe with me.\")\n")
	fileStream:write("\t\tend\n")
	fileStream:write("\tend\n")
	fileStream:write("end)\n")	

end

--- 
-- @param dialogTree -
-- @param node -
-- @param responseIndex -
function GetResponseHandle(dialogTree,node,responseIndex)
	for key,value in pairs(dialogTree[node].Player.Responses[responseIndex].Actions) do
	    if(value.Type == "GotoNode") then return value.Values end
	    if(value.Type == "OpenBank") then return "Bank" end
	    if(value.Type == "StablePet") then return "Stable" end
	    if(value.Type == "ShowPets") then return "Pet" end
	end 
	return ""
end

--- 
-- @param dialogTree -
function IsBanker(dialogTree)
	for key,value in pairs(dialogTree) do
		for index,response in pairs(value.Player.Responses) do
			for i, t in pairs(response.Actions) do
				if(t.Type == "OpenBank") then return true end
			end
		end
	end
	return false
end

---
-- @param fileStream -
-- @param dialogTree -
-- @param node -
-- @param responseIndex -
function WriteConditions(fileStream,dialogTree,node,responseIndex)
	for key,value in pairs(dialogTree[node].Player.Responses[responseIndex].Requirements) do
		if(key == 1) then
			if(value.Type == "HasObjVar") then
				fileStream:write("\tif(user:HasObjVar(\"" .. value.Values .. "\")")
			end
			if(value.Type == "HasObjVarGreaterThan") then
				local split = StringSplit(value.Values,"|")
				fileStream:write("\tif((user:GetObjVar(\"" .. split[1] .. "\") or 0) > " .. split[2])
			end		
			if(value.Type == "HasObjVarLessThan") then
				local split = StringSplit(value.Values,"|")
				fileStream:write("\tif((user:GetObjVar(\"" .. split[1] .. "\") or 0) < " .. split[2])
			end	
			if(value.Type == "NotHasObjVar") then
				fileStream:write("\tif(not(user:HasObjVar(\"" .. value.Values .. "\"))")
			end				
		else

			if(value.Type == "HasObjVar") then
				fileStream:write(" and user:HasObjVar(\"" .. value.Values .. "\")")
			end
			if(value.Type == "HasObjVarGreaterThan") then
				local split = StringSplit(value.Values,"|")
				fileStream:write(" and (user:GetObjVar(\"" .. split[1] .. "\") or 0) > " .. split[2])
			end
			if(value.Type == "HasObjVarLessThan") then
				local split = StringSplit(value.Values,"|")
				fileStream:write(" and (user:GetObjVar(\"" .. split[1] .. "\") or 0) < " .. split[2])
			end	
			if(value.Type == "NotHasObjVar") then
				fileStream:write(" and not(user:HasObjVar(\"" .. value.Values .. "\"))")
			end					
		end
	end	
end
