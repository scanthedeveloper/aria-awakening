
--------------------------------------------------------------------------------------------------------
--								       SPAWNER SAVING FUNCTIONS
--------------------------------------------------------------------------------------------------------
--- this will save all spawners in the current admins region to file
-- @param admin - admin to send verification output to on the chat console
function SaveSpawnerData(admin)
	admin = admin or this
	local objectList = FindObjects(SearchTemplate("spawner"))
	local filename = ExtendedSpawnerSaveDirectory..ServerSettings.RegionAddress..".SpawnerData.xml" or false
	if(#objectList > 0 and objectList ~= nil) then
		local xmlRoot = xml.new("CustomSeeds")
		for k,v in pairs(objectList) do
			local entry = xml.new("SpawnerData")
			local dataTable = v:GetObjVar("Data.SpawnerTable")
			for l,m in pairs(dataTable) do
				entry:append(l)[1] = m.Value
			end
			entry:append("Location")[1] = v:GetLoc().X .. "," .. v:GetLoc().Y .. "," .. v:GetLoc().Z
			entry:append("Rotation")[1] = v:GetRotation().X.. "," .. v:GetRotation().Y .. "," .. v:GetRotation().Z
			xmlRoot:append(entry)
		end
		--[FILE ERROR 1]
		if(filename == false) then
			admin:SystemMessage("[FF4444]FILE ERROR [1]: check scriptcommands_extended_spawner.lua")
			return false
		end
		xmlRoot:save(filename)
		admin:SystemMessage(#objectList.." objects being saved","event")
		admin:SystemMessage("save success!!")
	end
end

--- this will load all spawners from the regions save file back into the world.
-- @param admin - admin to send verification output to on the chat console
function LoadSpawnerData(admin)
	local filename = ExtendedSpawnerSaveDirectory..ServerSettings.RegionAddress..".SpawnerData.xml" or false
	admin = admin or this
		--[FILE ERROR 2]
		if(filename == false) then
			admin:SystemMessage("[FF4444]FILE ERROR [2]: check scriptcommands_extended_spawner.lua")
			return false
		end
	local xmlRoot = xml.load(filename) -- <==== TODO: In a cluster you can read the region, in standalone you cant find better way to choose file name for area/region.
	local spawnChance = 1
	local spawnDelay = 180
	local spawnMax = 1
	local spawnPulse = 10
	local spawnRadius = 5
	local spawnTemplate = ""
	local timeOfDay = "Any"
	local location = Loc(0,0,0)
	local rotation = Loc(0,0,0)
	local count = 0
	for i=1,#xmlRoot do
		for k,v in pairs(xmlRoot[i]) do
			if(k ~= 0) then
				if (string.find(v[0], "Location")) then 
					local data = StringSplit(v[1],",")
					location = Loc(tonumber(data[1]),tonumber(data[2]),tonumber(data[3])) 
				end
				if (string.find(v[0], "Rotation")) then 
					local data = StringSplit(v[1],",")
					rotation = Loc(tonumber(data[1]),tonumber(data[2]),tonumber(data[3])) 
				end
				if (string.find(v[0], "SpawnChance")) then spawnChance = v[1] end
				if (string.find(v[0], "SpawnDelay")) then spawnDelay = v[1] end
				if (string.find(v[0], "SpawnMax")) then spawnMax = v[1] end
				if (string.find(v[0], "SpawnPulse")) then spawnPulse = v[1] end
				if (string.find(v[0], "SpawnRadius")) then spawnRadius = v[1] end
				if (string.find(v[0], "SpawnTemplate")) then spawnTemplate = v[1] end
				if (string.find(v[0], "TimeOfDay")) then timeOfDay = v[1] end
			end
		end

		local data = 
		{			        
			SpawnChance = {Label="Spawn Chance", Value=spawnChance},				
			SpawnDelay = {Label="Spawn Delay", Value=spawnDelay},				
			SpawnMax = {Label="Spawn Max", Value=spawnMax},					
			SpawnPulse = {Label="Spawn Pulse", Value=spawnPulse},				
			SpawnRadius = {Label="Spawn Radius", Value=spawnRadius},					      		
			SpawnTemplate = {Label="Spawn Template", Value=spawnTemplate},         
			TimeOfDay = {Label="Time Of Day", Value = timeOfDay},			
		}	
		count = count + 1
		CreateObj("spawner",location,"persistant_spawner_loaded",data,rotation)
	end
	admin:SystemMessage(count.." spawners loaded")
end

--when the objects are created, lets make sure we change all the values back to their natural in game state
RegisterEventHandler(EventType.CreatedObject,"persistant_spawner_loaded",function (success,objRef,data,rotation)
    if (success) then
    	CallFunctionDelayed(TimeSpan.FromMilliseconds(350),function()
    		objRef:SetRotation(rotation)
    		objRef:SetObjVar("Data.SpawnerTable",data)
    		objRef:SetObjVar("spawnTemplate",data.SpawnTemplate.Value)
    	end)
    end
end)