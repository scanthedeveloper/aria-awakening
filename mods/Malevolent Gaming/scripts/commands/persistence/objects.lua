
--------------------------------------------------------------------------------------------------------
--									PERMANENT OBJECT SAVING FUNCTIONS
--------------------------------------------------------------------------------------------------------

--- toggle or untoggle a item in game and save it to a backup file for reloading
function TogglePermanent()
    this:RequestClientTargetGameObj(this, "togglepermanent")
end

--- save the essentials of the world objects to your xml data file
-- @param admin - admin to send output on successful save
function SaveWorldData(admin)
	local objectList = FindObjects(SearchObjVar("PermanentWorldObject",true))
	local filename = DecorationSaveLocation..ServerSettings.RegionAddress..".DecorationData.xml" or false
	if(#objectList > 0 and objectList ~= nil) then
		local xmlRoot = xml.new("CustomSeeds")
		for k,v in pairs(objectList) do
			local entry = xml.new("ObjectData")
			entry:append("Template")[1] = v:GetCreationTemplateId()
			entry:append("Location")[1] = v:GetLoc().X .. "," .. v:GetLoc().Y .. "," .. v:GetLoc().Z
			entry:append("Rotation")[1] = v:GetRotation().X.. "," .. v:GetRotation().Y .. "," .. v:GetRotation().Z
			entry:append("Name")[1] = v:GetName()
			entry:append("Hue")[1] = v:GetHue()
			entry:append("Color")[1] = v:GetColor()
			entry:append("Scale")[1] = v:GetScale().X.. "," .. v:GetScale().Y .. "," .. v:GetScale().Z
			xmlRoot:append(entry)
		end
		if(filename == false) then
			admin:SystemMessage("[FF4444]Failed to Save bad filename")
			return false
		end
		xmlRoot:save(filename)
		admin:SystemMessage(#objectList.." objects being saved","event")
		admin:SystemMessage("save success!!")
	end
end

--load the essentials of the world objects from your data file
-- @param admin - admin to send output on successful load
function LoadWorldData(admin)
	local filename = DecorationSaveLocation..ServerSettings.RegionAddress..".DecorationData.xml" or false
	if(filename == false) then
		admin:SystemMessage("[FF4444]Failed to Load bad filename")
		return false
	end
	local xmlRoot = xml.load(filename) -- <==== TODO: In a cluster you can read the region, in standalone you cant find better way to choose file name for area/region.
	local template = ""
	local location = Loc(0,0,0)
	local rotation = Loc(0,0,0)
	local name = ""
	local hue = 0
	local color = "FFFFFF"
	local scale = Loc(0,0,0)
	for i=1,#xmlRoot do
		for k,v in pairs(xmlRoot[i]) do
			if(k ~= 0) then
				if (string.find(v[0], "Template")) then template = v[1] end
				if (string.find(v[0], "Location")) then 
					local data = StringSplit(v[1],",")
					location = Loc(tonumber(data[1]),tonumber(data[2]),tonumber(data[3])) 
				end
				if (string.find(v[0], "Rotation")) then 
					local data = StringSplit(v[1],",")
					rotation = Loc(tonumber(data[1]),tonumber(data[2]),tonumber(data[3])) 
				end
				if (string.find(v[0], "Name")) then name = v[1] end
				if (string.find(v[0], "Hue")) then hue = tonumber(v[1]) end
				if (string.find(v[0], "Color")) then color = v[1] end
				if (string.find(v[0], "Scale")) then 
					local data = StringSplit(v[1],",")
					scale = Loc(tonumber(data[1]),tonumber(data[2]),tonumber(data[3])) 
				end
			end
		end
		CreateObj(template,location,"persistant_object_loaded",{Rotation=rotation,Name=name,Hue=hue,Color=color,Scale=scale})
	end
end

--when the objects are created, lets make sure we change all the values back to their natural in game state
RegisterEventHandler(EventType.CreatedObject,"persistant_object_loaded",function (success,objRef,data)
    if (success) then
    	CallFunctionDelayed(TimeSpan.FromMilliseconds(50),function()
    		objRef:SetName(data.Name)
    		objRef:SetRotation(data.Rotation)
    		objRef:SetHue(data.Hue)
    		objRef:SetColor(data.Color)
    		objRef:SetObjVar("NoReset",true)
    		objRef:SetObjVar("PermanentWorldObject",true)
    		objRef:SetScale(data.Scale)
    	end)
    end
end)

-- event handler for the TogglePermanent function
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "togglepermanent",  function(target)
	if(target:IsMobile()== true) then 
		this:SystemMessage("Cant target mobiles...")
		return false
	end
	if not(target:HasObjVar("PermanentWorldObject")) then
    	target:SetObjVar("NoReset",true)
    	target:SetObjVar("PermanentWorldObject",true)
    	this:SystemMessage("You make this "..target:GetName().. " a permanent part of this world.")
    	SaveWorldData(this)
    else
    	target:DelObjVar("NoReset")
    	target:DelObjVar("PermanentWorldObject")
    	this:SystemMessage("You removed this "..target:GetName().. " from the worlds clutches.")
    	SaveWorldData(this)
    end
end)

