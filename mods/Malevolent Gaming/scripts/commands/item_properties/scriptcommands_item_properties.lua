----------------------------------------------------------
--					EditItemProperties()
----------------------------------------------------------
function EditItemProperties()
	this:CloseDynamicWindow("editItemProperties")
  	this:RequestClientTargetGameObj(this, "editItemProperties")
  	this:SystemMessage("target a item you wish to examine...")
end

--#########################################################
--				 DYNAMIC WINDOW FUNCTIONS
--#########################################################
--- this will draw the close button on a dynamic window
-- @param x - this is the x location to draw the close button at
-- @param y - this is the y location to draw the close button at
-- @param dynamicWindow - this is the dynamic window to attach the close button too
function DrawCloseButton(x,y,dynamicWindow)
    --border background color
    dynamicWindow:AddImage(x,y,"HueSwatch",18,18,"Sliced","555555")
    --background color
    dynamicWindow:AddImage(x+2,y+2,"HueSwatch",14,14,"Sliced","252525")
    --invisible button
    dynamicWindow:AddButton(x,y,"close_button","",18,18,"[4488CC]Close Button\n[959555]clicking this will close the item property window.","",true,"Invisible")
    --draw the "X" string
    dynamicWindow:AddLabel(x+5,y+3,"[959555]X",18,18,18,"left")
end

----------------------------------------------------------
--			ShowItemPropertyWindow(user,item,page)
----------------------------------------------------------
--- this shows the item property dynamic window
-- @param user - player to open the dynamic window for
-- @param item - object which has the item_properties table objvar
-- @param page - the page of properties to view
function ShowItemPropertyWindow(user,item,page)
	if(page == nil) then page = "Primary" end
	local properties = item:GetObjVar("MagicProperties")
	if(properties == nil) then
		properties = ItemProperties.PropertyTable
		item:SetObjVar("MagicProperties",properties)
	end
	local dynamicWindow = DynamicWindow("ItemPropertyWindow","[BBBB44]Gizmo's Equipment Editor",500,360,-250,-180,"TransparentDraggable","Center")

 	--Create Background
    dynamicWindow:AddImage(1,1,"HueSwatch",500,310,"Simple","050505")
    --Create Header Background
    dynamicWindow:AddImage(1,1,"HueSwatch",500,34,"Simple","353535")
    DrawCloseButton(471,9,dynamicWindow,"MerchantDialog")
    --Create Header Panel
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",506,40,"Sliced","757575")

	if(page == "Standard") then
		dynamicWindow:AddLabel(15,45,"[BBBB44]Name",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(107,45,365,20,"text_field_name",item:GetName())

		dynamicWindow:AddLabel(15,75,"[BBBB44]Hue",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(107,75,365,20,"text_field_hue",tostring(item:GetHue()))

		dynamicWindow:AddLabel(15,105,"[BBBB44]Color",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(107,105,365,20,"text_field_color",item:GetColor())

		dynamicWindow:AddLabel(15,135,"[BBBB44]Weight",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(107,135,365,20,"text_field_weight",tostring(GetWeight(item)))

		dynamicWindow:AddLabel(15,165,"[BBBB44]Durability",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(107,165,365,20,"text_field_maxdurability",tostring(item:GetObjVar("MaxDurability")))

		dynamicWindow:AddLabel(15,195,"[BBBB44]Lore",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(107,195,365,80,"text_field_lore",tostring(item:GetObjVar("LoreString") or ""))

		dynamicWindow:AddButton(15,275,"ScriptIt:"..item.Id..":"..page,"",130,28,"[4488CC]Script This Item\n[FFFFBB]create this item in xml format to make a permanent object out of it",nil,true,"Invisible")
		dynamicWindow:AddLabel(37,280,"[4488CC]Create Me",130,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")

		dynamicWindow:AddButton(340,275,"ApplyChangesStandard:"..item.Id..":"..page,"",130,28,"[4488CC]Apply Changes\n[FFFFBB]switch to standard item properties",nil,false,"Invisible")
		dynamicWindow:AddLabel(356,280,"[BBBB44]Apply Changes",130,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")		
	end
	if(page == "Primary") then
		dynamicWindow:AddLabel(18,65,"[BBBB44]Strength",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,63,100,20,"text_field_strength_plus",tostring(properties["StrengthPlus"]))
		dynamicWindow:AddTextField(285,63,100,20,"text_field_strength_times",tostring(properties["StrengthTimes"]))

		dynamicWindow:AddLabel(18,95,"[BBBB44]Agility",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,93,100,20,"text_field_agility_plus",tostring(properties["AgilityPlus"]))	
		dynamicWindow:AddTextField(285,93,100,20,"text_field_agility_times",tostring(properties["AgilityTimes"]))	

		dynamicWindow:AddLabel(18,125,"[BBBB44]Constitution",112,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,123,100,20,"text_field_constitution_plus",tostring(properties["ConstitutionPlus"]))
		dynamicWindow:AddTextField(285,123,100,20,"text_field_constitution_times",tostring(properties["ConstitutionTimes"]))

		dynamicWindow:AddLabel(18,155,"[BBBB44]Intelligence",112,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,153,100,20,"text_field_intelligence_plus",tostring(properties["IntelligencePlus"]))
		dynamicWindow:AddTextField(285,153,100,20,"text_field_intelligence_times",tostring(properties["IntelligenceTimes"]))

		dynamicWindow:AddLabel(18,185,"[BBBB44]Wisdom",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,183,100,20,"text_field_wisdom_plus",tostring(properties["WisdomPlus"]))
		dynamicWindow:AddTextField(285,183,100,20,"text_field_wisdom_times",tostring(properties["WisdomTimes"]))

		dynamicWindow:AddLabel(18,215,"[BBBB44]Will",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,213,100,20,"text_field_will_plus",tostring(properties["WillPlus"]))
		dynamicWindow:AddTextField(285,213,100,20,"text_field_will_times",tostring(properties["WillTimes"]))

		dynamicWindow:AddButton(340,269,"ApplyChangesPrimary:"..item.Id..":"..page,"",130,28,"[FF7700]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible")
		dynamicWindow:AddLabel(356,275,"[BBBB44]Apply Changes",130,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")		
							
		dynamicWindow:AddLabel(16,42,"Property",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddLabel(165,42,"Bonus",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddLabel(290,42,"Multiplier",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
	end

	if(page == "Combat") then
		dynamicWindow:AddLabel(18,65,"[BBBB44]Accuracy",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,63,100,20,"text_field_accuracy_plus",tostring(properties["AccuracyPlus"]))
		dynamicWindow:AddTextField(285,63,100,20,"text_field_accuracy_times",tostring(properties["AccuracyTimes"]))

		dynamicWindow:AddLabel(18,95,"[BBBB44]Defense",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,93,100,20,"text_field_defense_plus",tostring(properties["DefensePlus"]))
		dynamicWindow:AddTextField(285,93,100,20,"text_field_defense_times",tostring(properties["DefenseTimes"]))

		dynamicWindow:AddLabel(18,125,"[BBBB44]Evasion",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,123,100,20,"text_field_evasion_plus",tostring(properties["EvasionPlus"]))
		dynamicWindow:AddTextField(285,123,100,20,"text_field_evasion_times",tostring(properties["EvasionTimes"]))

		dynamicWindow:AddLabel(18,155,"[BBBB44]Movement",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,153,100,20,"text_field_movespeed_plus",tostring(properties["MoveSpeedPlus"]))
		dynamicWindow:AddTextField(285,153,100,20,"text_field_movespeed_times",tostring(properties["MoveSpeedTimes"]))

		dynamicWindow:AddButton(340,269,"ApplyChangesCombat:"..item.Id..":"..page,"",130,28,"[FF7700]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible")
		dynamicWindow:AddLabel(356,275,"[BBBB44]Apply Changes",130,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")		
							
		dynamicWindow:AddLabel(16,42,"Property",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddLabel(165,42,"Bonus",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddLabel(290,42,"Multiplier",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
	end
	if(page == "Damage") then
		dynamicWindow:AddLabel(18,65,"[BBBB44]Attack",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,63,100,20,"text_field_attack_plus",tostring(properties["AttackPlus"]))
		dynamicWindow:AddTextField(285,63,100,20,"text_field_attack_times",tostring(properties["AttackTimes"]))

		dynamicWindow:AddLabel(18,95,"[BBBB44]Force",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,93,100,20,"text_field_force_plus",tostring(properties["ForcePlus"]))	
		dynamicWindow:AddTextField(285,93,100,20,"text_field_force_times",tostring(properties["ForceTimes"]))	

		dynamicWindow:AddLabel(18,125,"[BBBB44]Power",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,123,100,20,"text_field_power_plus",tostring(properties["PowerPlus"]))
		dynamicWindow:AddTextField(285,123,100,20,"text_field_power_times",tostring(properties["PowerTimes"]))

		dynamicWindow:AddButton(340,269,"ApplyChangesDamage:"..item.Id..":"..page,"",130,28,"[FF7700]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible")
		dynamicWindow:AddLabel(356,275,"[BBBB44]Apply Changes",130,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")			
							
		dynamicWindow:AddLabel(16,42,"Property",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddLabel(165,42,"Bonus",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddLabel(290,42,"Multiplier",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
	end	

	if(page == "Resource") then
		dynamicWindow:AddLabel(18,65,"[BBBB44]Max Health",122,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,63,100,20,"text_field_maxhealth_plus",tostring(properties["MaxHealthPlus"]))
		dynamicWindow:AddTextField(285,63,100,20,"text_field_maxhealth_times",tostring(properties["MaxHealthTimes"]))

		dynamicWindow:AddLabel(18,95,"[BBBB44]Max Mana",122,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,93,100,20,"text_field_maxmana_plus",tostring(properties["MaxManaPlus"]))	
		dynamicWindow:AddTextField(285,93,100,20,"text_field_maxmana_times",tostring(properties["MaxManaTimes"]))	

		dynamicWindow:AddLabel(18,125,"[BBBB44]Max Stamina",122,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,123,100,20,"text_field_maxstamina_plus",tostring(properties["MaxStaminaPlus"]))
		dynamicWindow:AddTextField(285,123,100,20,"text_field_maxstamina_times",tostring(properties["MaxStaminaTimes"]))

		dynamicWindow:AddLabel(18,155,"[BBBB44]Max Vitality",122,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,153,100,20,"text_field_maxvitality_plus",tostring(properties["MaxVitalityPlus"]))
		dynamicWindow:AddTextField(285,153,100,20,"text_field_maxvitality_times",tostring(properties["MaxVitalityTimes"]))

		dynamicWindow:AddLabel(18,185,"[BBBB44]Health Regen",122,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,183,100,20,"text_field_healthregen_plus",tostring(properties["HealthRegenPlus"]))
		dynamicWindow:AddTextField(285,183,100,20,"text_field_healthregen_times",tostring(properties["HealthRegenTimes"]))

		dynamicWindow:AddLabel(18,215,"[BBBB44]Mana Regen",122,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,213,100,20,"text_field_manaregen_plus",tostring(properties["ManaRegenPlus"]))
		dynamicWindow:AddTextField(285,213,100,20,"text_field_manaregen_times",tostring(properties["ManaRegenTimes"]))

		dynamicWindow:AddLabel(18,245,"[BBBB44]Stamina Regen",122,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddTextField(160,243,100,20,"text_field_staminaregen_plus",tostring(properties["StaminaRegenPlus"]))
		dynamicWindow:AddTextField(285,243,100,20,"text_field_staminaregen_times",tostring(properties["StaminaRegenTimes"]))

		dynamicWindow:AddButton(340,269,"ApplyChangesResource:"..item.Id..":"..page,"",130,28,"[FF7700]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible")
		dynamicWindow:AddLabel(356,275,"[BBBB44]Apply Changes",130,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")		
							
		dynamicWindow:AddLabel(16,42,"Property",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddLabel(165,42,"Bonus",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
		dynamicWindow:AddLabel(290,42,"Multiplier",92,18,18,"left",false,false,"PermianSlabSerif_16_Dynamic")
	end

	if(page == "Standard") then
		dynamicWindow:AddButton(8,4,"ShowStandard:"..item.Id,"",92,28,"[4488CC]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible","pressed")
		dynamicWindow:AddLabel(24,10, "[4488CC]Standard",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
	else
		dynamicWindow:AddButton(8,4,"ShowStandard:"..item.Id,"",92,28,"[4488CC]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible")
		dynamicWindow:AddLabel(24,10, "[BBBB44]Standard",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")	
	end
	if(page == "Primary") then
		dynamicWindow:AddButton(101,4,"ShowPrimary:"..item.Id,"",92,28,"[4488CC]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible","pressed")
		dynamicWindow:AddLabel(118,10,"[4488CC]Primary",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
	else
		dynamicWindow:AddButton(101,4,"ShowPrimary:"..item.Id,"",92,28,"[4488CC]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible")
		dynamicWindow:AddLabel(118,10,"[BBBB44]Primary",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
	end
	if(page == "Combat") then
		dynamicWindow:AddButton(193,4,"ShowCombat:"..item.Id,"",92,28,"[4488CC]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible","pressed")
		dynamicWindow:AddLabel(214,10,"[4488CC]Combat",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
	else
		dynamicWindow:AddButton(193,4,"ShowCombat:"..item.Id,"",92,28,"[4488CC]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible")
		dynamicWindow:AddLabel(214,10,"[BBBB44]Combat",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
	end
	if(page == "Damage") then
		dynamicWindow:AddButton(287,4,"ShowDamage:"..item.Id,"",92,28,"[4488CC]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible","pressed")
		dynamicWindow:AddLabel(304,10,"[4488CC]Damage",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")	
	else
		dynamicWindow:AddButton(287,4,"ShowDamage:"..item.Id,"",92,28,"[4488CC]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible")
		dynamicWindow:AddLabel(304,10,"[BBBB44]Damage",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")	
	end
	if(page == "Resource") then
		dynamicWindow:AddButton(380,4,"ShowResource:"..item.Id,"",92,28,"[4488CC]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible","pressed")
		dynamicWindow:AddLabel(396,10,"[4488CC]Resource",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
	else
		dynamicWindow:AddButton(380,4,"ShowResource:"..item.Id,"",92,28,"[4488CC]Switch to Standard\n[FFFFBB]switch to standard item properties",nil,false,"Invisible")
		dynamicWindow:AddLabel(396,10,"[BBBB44]Resource",92,20,20,"left",false,false,"PermianSlabSerif_16_Dynamic")
	end

    --Create Body Panel
    dynamicWindow:AddImage(-2,30,"CraftingItemsFrame",506,285,"Sliced","757575")

	user:OpenDynamicWindow(dynamicWindow,this)	
end
----------------------------------------------------------
--				ExportToXml(fullpath,user,item)
----------------------------------------------------------
--- export the item to a xml file
-- @param fullpath - the full filepath to save too
-- @param user - admin to send verification string to the chat console
-- @param item - the item to get the save data from
function ExportToXml(fullpath,user,item)
	local file = io.open(fullpath,"w")

	file:write("<ObjectTemplate>\n")

	file:write("\t<Name>" .. item:GetName() .. "</Name>\n")
	file:write("\t<ClientId>" .. tostring(item:GetIconId()) .. "</ClientId>\n")
	file:write("\t<Hue>" .. item:GetHue() .. "</Hue>\n")
	file:write("\t<Color>".. item:GetColor() .. "</Color>\n")
	--LORE HERE
	local lore = item:GetObjVar("LoreString") or false
	if(lore) then
		file:write("\t<SharedStateEntry name=\"TooltipString\" type=\"string\" value=\"" .. lore .. "\"/>\n")
	end
	file:write("\t<SharedStateEntry name=\"Weight\" type=\"int\" value=\"" .. GetWeight(item).. "\"/>\n")
	file:write("\t<ObjectVariableComponent>\n")

	if(item:HasObjVar("WeaponType")) then
		file:write("\t\t<StringVariable Name=\"WeaponType\">" .. item:GetObjVar("WeaponType") .. "</StringVariable>\n")
	elseif(item:HasObjVar("ArmorType")) then
		file:write("\t\t<StringVariable Name=\"ArmorType\">" .. item:GetObjVar("ArmorType") .. "</StringVariable>\n")
	elseif(item:HasObjVar("ShieldType")) then
		file:write("\t\t<StringVariable Name=\"ShieldType\">" .. item:GetObjVar("ShieldType") .. "</StringVariable>\n")
	end

	if(item:HasObjVar("MaxDurability")) then
		if(item:GetObjVar("MaxDurability") ~= nil) then
			file:write("\t\t<DoubleVariable Name=\"MaxDurability\">" .. item:GetObjVar("MaxDurability") .. "</DoubleVariable>\n")
		end
	end

	file:write("\t</ObjectVariableComponent>\n")
	file:write("\t<ScriptEngineComponent>\n")

	if(item:HasModule("armor_base")) then
		file:write("\t\t<LuaModule Name=\"armor_base\"/>\n")
	end

	file:write("\t\t<LuaModule Name=\"item_properties.item_properties\">\n")
	file:write("\t\t\t<Initializer>\n")
	file:write("\t\t\t{\n")
	print("Writing Item Properties....")
	WriteItemProperties(file,item)
	file:write("\t\t\t}\n")
	file:write("\t\t\t</Initializer>\n")
	file:write("\t\t</LuaModule>\n")
	file:write("\t</ScriptEngineComponent>\n")
	file:write("</ObjectTemplate>")
	file:close()

	user:SystemMessage("[FF7700]"..fullpath.."[FFFFBB]... scripted and saved!")
end

----------------------------------------------------------
--				WriteItemProperties(file)
----------------------------------------------------------
--- export the item to a xml file
-- @param file - filestream to write too
-- @param item - the item to get the save data from
function WriteItemProperties(file,item)
	for prop,value in pairs(item:GetObjVar("MagicProperties")) do
		if(tonumber(value) > 0) then
			file:write("\t\t\t\t"..prop.." = " .. value .. ",\n")
		end
	end
end

----------------------------------------------------------
--EVENTTYPE - ClientTargetGameObjResponse - "editItemProperties"
----------------------------------------------------------
RegisterEventHandler(EventType.ClientTargetGameObjResponse, "editItemProperties", 
function(target)
	if not (target:HasObjVar("MagicProperties")) then
		target:SetObjVar("MagicProperties",propertyBonusValues)
		ShowItemPropertyWindow(this,target)
		return
	end
	ShowItemPropertyWindow(this,target)
end)

----------------------------------------------------------
--EVENTTYPE - DynamicWindowResponse - "ItemPropertyWindow"
----------------------------------------------------------
--this is the item property dynamicWindow dialog event handler
RegisterEventHandler(EventType.DynamicWindowResponse,"ItemPropertyWindow", 
	function (user,buttonId,textField)
		local buttonData = StringSplit(buttonId,":")
		local button = buttonData[1]
		local objectId = buttonData[2]

		if(button=="ShowStandard") then
			ShowItemPropertyWindow(user,GameObj(tonumber(objectId)),"Standard")
		end
		if(button=="ShowPrimary") then
			ShowItemPropertyWindow(user,GameObj(tonumber(objectId)),"Primary")
		end	
		if(button=="ShowCombat") then
			ShowItemPropertyWindow(user,GameObj(tonumber(objectId)),"Combat")
		end
		if(button=="ShowDamage") then
			ShowItemPropertyWindow(user,GameObj(tonumber(objectId)),"Damage")
		end
		if(button=="ShowResource") then
			ShowItemPropertyWindow(user,GameObj(tonumber(objectId)),"Resource")
		end
		if(button=="ApplyChangesPrimary") then
			local gameObject = GameObj(tonumber(objectId)) or nil
			if(gameObject ~= nil) then
				local magicProperties = gameObject:GetObjVar("MagicProperties")
				if(tonumber(textField.text_field_strength_plus) ~= 0) then
					magicProperties["StrengthPlus"] = tonumber(textField.text_field_strength_plus)
				else
					magicProperties["StrengthPlus"] = 0
				end
				if(tonumber(textField.text_field_strength_times) ~= 0) then
					magicProperties["StrengthTimes"] = tonumber(textField.text_field_strength_times)
				else
					magicProperties["StrengthTimes"] = 0
				end
				if(tonumber(textField.text_field_agility_plus) ~= 0) then
					magicProperties["AgilityPlus"] = tonumber(textField.text_field_agility_plus)
				else
					magicProperties["AgilityPlus"] = 0
				end
				if(tonumber(textField.text_field_agility_times) ~= 0) then
					magicProperties["AgilityTimes"] = tonumber(textField.text_field_agility_times)
				else
					magicProperties["AgilityTimes"] = 0
				end
				if(tonumber(textField.text_field_constitution_plus) ~= 0) then
					magicProperties["ConstitutionPlus"] = tonumber(textField.text_field_constitution_plus)
				else
					magicProperties["ConstitutionPlus"] = 0
				end
				if(tonumber(textField.text_field_constitution_times) ~= 0) then
					magicProperties["ConstitutionTimes"] = tonumber(textField.text_field_constitution_times)
				else
					magicProperties["ConstitutionTimes"] = 0
				end
				if(tonumber(textField.text_field_intelligence_plus) ~= 0) then
					magicProperties["IntelligencePlus"] = tonumber(textField.text_field_intelligence_plus)
				else
					magicProperties["IntelligencePlus"] = 0
				end
				if(tonumber(textField.text_field_intelligence_times) ~= 0) then
					magicProperties["IntelligenceTimes"] = tonumber(textField.text_field_intelligence_times)
				else
					magicProperties["IntelligenceTimes"] = 0
				end
				if(tonumber(textField.text_field_wisdom_plus) ~= 0) then
					magicProperties["WisdomPlus"] = tonumber(textField.text_field_wisdom_plus)
				else
					magicProperties["WisdomPlus"] = 0
				end
				if(tonumber(textField.text_field_wisdom_times) ~= 0) then
					magicProperties["WisdomTimes"] = tonumber(textField.text_field_wisdom_times)
				else
					magicProperties["WisdomTimes"] = 0
				end
				if(tonumber(textField.text_field_will_plus) ~= 0) then
					magicProperties["WillPlus"] = tonumber(textField.text_field_will_plus)
				else
					magicProperties["WillPlus"] = 0
				end
				if(tonumber(textField.text_field_will_times) ~= 0) then
					magicProperties["WillTimes"] = tonumber(textField.text_field_will_times)
				else
					magicProperties["WillTimes"] = 0
				end
				gameObject:SetObjVar("MagicProperties",magicProperties)
				CallFunctionDelayed(TimeSpan.FromSeconds(1),function()ShowItemPropertyWindow(user,gameObject,buttonData[3])end)
				
				ItemProperties.UpdateItemTooltip(gameObject)
			else
				user:SystemMessage("Error Object Invalid")
			end
		end

		if(button=="ApplyChangesCombat") then
			local gameObject = GameObj(tonumber(objectId)) or nil
			if(gameObject ~= nil) then
				local magicProperties = gameObject:GetObjVar("MagicProperties")

				if(tonumber(textField.text_field_accuracy_plus) ~= 0) then
					magicProperties["AccuracyPlus"] = tonumber(textField.text_field_accuracy_plus)
				else
					magicProperties["AccuracyPlus"] = 0
				end
				if(tonumber(textField.text_field_accuracy_times) ~= 0) then
					magicProperties["AccuracyTimes"] = tonumber(textField.text_field_accuracy_times)
				else
					magicProperties["AccuracyTimes"] = 0
				end				
				if(tonumber(textField.text_field_defense_plus) ~= 0) then
					magicProperties["DefensePlus"] = tonumber(textField.text_field_defense_plus)
				else
					magicProperties["DefensePlus"] = 0
				end
				if(tonumber(textField.text_field_defense_times) ~= 0) then
					magicProperties["DefenseTimes"] = tonumber(textField.text_field_defense_times)
				else
					magicProperties["DefenseTimes"] = 0
				end			
				if(tonumber(textField.text_field_evasion_plus) ~= 0) then
					magicProperties["EvasionPlus"] = tonumber(textField.text_field_evasion_plus)
				else
					magicProperties["EvasionPlus"] = 0
				end
				if(tonumber(textField.text_field_evasion_times) ~= 0) then
					magicProperties["EvasionTimes"] = tonumber(textField.text_field_evasion_times)
				else
					magicProperties["EvasionTimes"] = 0
				end
				if(tonumber(textField.text_field_movespeed_plus) ~= 0) then
					magicProperties["MoveSpeedPlus"] = tonumber(textField.text_field_movespeed_plus)
				else
					magicProperties["MoveSpeedPlus"] = 0
				end
				if(tonumber(textField.text_field_movespeed_times) ~= 0) then
					magicProperties["MoveSpeedTimes"] = tonumber(textField.text_field_movespeed_times)
				else
					magicProperties["MoveSpeedTimes"] = 0
				end

				gameObject:SetObjVar("MagicProperties",magicProperties)
				CallFunctionDelayed(TimeSpan.FromSeconds(1),function()ShowItemPropertyWindow(user,gameObject,buttonData[3])end)
				
				ItemProperties.UpdateItemTooltip(gameObject)
			else
				user:SystemMessage("Error Object Invalid")
			end
		end

		if(button=="ApplyChangesDamage") then
			local gameObject = GameObj(tonumber(objectId)) or nil
			if(gameObject ~= nil) then
				local magicProperties = gameObject:GetObjVar("MagicProperties")

				if(tonumber(textField.text_field_attack_plus) ~= 0) then
					magicProperties["AttackPlus"] = tonumber(textField.text_field_attack_plus)
				else
					magicProperties["AttackPlus"] = 0
				end
				if(tonumber(textField.text_field_attack_times) ~= 0) then
					magicProperties["AttackTimes"] = tonumber(textField.text_field_attack_times)
				else
					magicProperties["AttackTimes"] = 0
				end				
				if(tonumber(textField.text_field_force_plus) ~= 0) then
					magicProperties["ForcePlus"] = tonumber(textField.text_field_force_plus)
				else
					magicProperties["ForcePlus"] = 0
				end
				if(tonumber(textField.text_field_force_times) ~= 0) then
					magicProperties["ForceTimes"] = tonumber(textField.text_field_force_times)
				else
					magicProperties["ForceTimes"] = 0
				end
				if(tonumber(textField.text_field_power_plus) ~= 0) then
					magicProperties["PowerPlus"] = tonumber(textField.text_field_power_plus)
				else
					magicProperties["PowerPlus"] = 0
				end
				if(tonumber(textField.text_field_power_times) ~= 0) then
					magicProperties["PowerTimes"] = tonumber(textField.text_field_power_times)
				else
					magicProperties["PowerTimes"] = 0
				end			

				gameObject:SetObjVar("MagicProperties",magicProperties)
				CallFunctionDelayed(TimeSpan.FromSeconds(1),function()ShowItemPropertyWindow(user,gameObject,buttonData[3])end)
				
				ItemProperties.UpdateItemTooltip(gameObject)
			else
				user:SystemMessage("Error Object Invalid")
			end
		end

		if(button=="ApplyChangesResource") then
			local gameObject = GameObj(tonumber(objectId)) or nil
			if(gameObject ~= nil) then
				local magicProperties = gameObject:GetObjVar("MagicProperties")

				if(tonumber(textField.text_field_maxhealth_plus) ~= 0) then
					magicProperties["MaxHealthPlus"] = tonumber(textField.text_field_maxhealth_plus)
				else
					magicProperties["MaxHealthPlus"] = 0
				end
				if(tonumber(textField.text_field_maxhealth_times) ~= 0) then
					magicProperties["MaxHealthTimes"] = tonumber(textField.text_field_maxhealth_times)
				else
					magicProperties["MaxHealthTimes"] = 0
				end
				if(tonumber(textField.text_field_maxmana_plus) ~= 0) then
					magicProperties["MaxManaPlus"] = tonumber(textField.text_field_maxmana_plus)
				else
					magicProperties["MaxManaPlus"] = 0
				end
				if(tonumber(textField.text_field_maxmana_times) ~= 0) then
					magicProperties["MaxManaTimes"] = tonumber(textField.text_field_maxmana_times)
				else
					magicProperties["MaxManaTimes"] = 0
				end
				if(tonumber(textField.text_field_maxstamina_plus) ~= 0) then
					magicProperties["MaxStaminaPlus"] = tonumber(textField.text_field_maxstamina_plus)
				else
					magicProperties["MaxStaminaPlus"] = 0
				end
				if(tonumber(textField.text_field_maxstamina_times) ~= 0) then
					magicProperties["MaxStaminaTimes"] = tonumber(textField.text_field_maxstamina_times)
				else
					magicProperties["MaxStaminaTimes"] = 0
				end
				if(tonumber(textField.text_field_maxvitality_plus) ~= 0) then
					magicProperties["MaxVitalityPlus"] = tonumber(textField.text_field_maxvitality_plus)
				else
					magicProperties["MaxVitalityPlus"] = 0
				end
				if(tonumber(textField.text_field_maxvitality_times) ~= 0) then
					magicProperties["MaxVitalityTimes"] = tonumber(textField.text_field_maxvitality_times)
				else
					magicProperties["MaxVitalityTimes"] = 0
				end
				if(tonumber(textField.text_field_healthregen_plus) ~= 0) then
					magicProperties["HealthRegenPlus"] = tonumber(textField.text_field_healthregen_plus)
				else
					magicProperties["HealthRegenPlus"] = 0
				end
				if(tonumber(textField.text_field_healthregen_times) ~= 0) then
					magicProperties["HealthRegenTimes"] = tonumber(textField.text_field_healthregen_times)
				else
					magicProperties["HealthRegenTimes"] = 0
				end
				if(tonumber(textField.text_field_manaregen_plus) ~= 0) then
					magicProperties["ManaRegenPlus"] = tonumber(textField.text_field_manaregen_plus)
				else
					magicProperties["ManaRegenPlus"] = 0
				end
				if(tonumber(textField.text_field_manaregen_times) ~= 0) then
					magicProperties["ManaRegenTimes"] = tonumber(textField.text_field_manaregen_times)
				else
					magicProperties["ManaRegenTimes"] = 0
				end
				if(tonumber(textField.text_field_staminaregen_plus) ~= 0) then
					magicProperties["StaminaRegenPlus"] = tonumber(textField.text_field_staminaregen_plus)
				else
					magicProperties["StaminaRegenPlus"] = 0
				end
				if(tonumber(textField.text_field_staminaregen_times) ~= 0) then
					magicProperties["StaminaRegenTimes"] = tonumber(textField.text_field_staminaregen_times)
				else
					magicProperties["StaminaRegenTimes"] = 0
				end

				gameObject:SetObjVar("MagicProperties",magicProperties)
				CallFunctionDelayed(TimeSpan.FromSeconds(1),function()ShowItemPropertyWindow(user,gameObject,buttonData[3])end)
				
				ItemProperties.UpdateItemTooltip(gameObject)
			else
				user:SystemMessage("Error Object Invalid")
			end
		end	

		if(button=="ApplyChangesStandard") then	
			local gameObject = GameObj(tonumber(objectId)) or nil
			if(gameObject ~= nil) then		
				gameObject:SetName(textField.text_field_name)
				gameObject:SetHue(tonumber(textField.text_field_hue))
				gameObject:SetColor(textField.text_field_color)
				gameObject:SetSharedObjectProperty("Weight",textField.text_field_weight)
				gameObject:SetObjVar("MaxDurability", textField.text_field_maxdurability)
				if(textField.text_field_lore ~= "") then
					gameObject:SetObjVar("LoreString",textField.text_field_lore)
				else
					if(gameObject:HasObjVar("ItemLore") == true) then gameObject:DelObjVar("ItemLore") end
				end				
				CallFunctionDelayed(TimeSpan.FromSeconds(1),function()ShowItemPropertyWindow(user,gameObject,buttonData[3])end)
				ItemProperties.UpdateItemTooltip(gameObject)
			end			
		end

		if(button=="ScriptIt") then
			local gameObject = GameObj(tonumber(objectId)) or nil
			if(gameObject ~= nil) then		
				local name, color = StripColorFromString(gameObject:GetName())
				local name = name:lower()
				local filename = string.gsub(name," ","_")
				filename = string.gsub(filename,"'","")
				local fullpath = EditedInGameItemSaveDirectory .. filename .. ".xml"
				ExportToXml(fullpath,user,gameObject)
				--load the editor back up
				CallFunctionDelayed(TimeSpan.FromSeconds(0.5),function()ShowItemPropertyWindow(user,gameObject,buttonData[3])end)
				--update the new item properties on the saved object
				ItemProperties.UpdateItemTooltip(gameObject)
			end
		end
	end)
