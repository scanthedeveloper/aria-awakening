require 'default:scriptcommands'

--require the xml saving/loading functions
--TODO: Add a better way to control the location of where to spawn permanent object data
DecorationSaveLocation = "data/decoration/"
ExtendedSpawnerSaveDirectory = "data/spawners/"
EditedInGameItemSaveDirectory = "data/items/custom/"


require 'admin.tools.elements_data'
require 'admin.tools.elements_window'
require 'admin.tools.templates_window'
require 'commands.item_properties.scriptcommands_item_properties'

require('LuaXML')

require 'commands.persistence.objects'
require 'commands.spawning.spawner'

--view provided by Teiravon *hugs*
RegisterCommand{ Command="loremaster", AccessLevel=AccessLevel.Mortal, Func=LoremasterPermission, Usage="<command_name>", Desc="Opens the Elements Window" }
RegisterCommand{ Command="tlw", AccessLevel=AccessLevel.God, Func=TemplateListWindow, Usage="<command_name>", Desc="Opens the Templates Window" }

--item properties
RegisterCommand{ Command="properties", AccessLevel=AccessLevel.God, Func=EditItemProperties, Desc="Edit item property"}

--persistence
RegisterCommand{ Command="perm", AccessLevel = AccessLevel.God, Func=TogglePermanent, Desc="Make a item permanent and save it to the world" }
RegisterCommand{ Command="saveperm", AccessLevel = AccessLevel.God, Func=SaveWorldData, Desc="save the worlds permanent objects" }
RegisterCommand{ Command="loadperm", AccessLevel = AccessLevel.God, Func=LoadWorldData, Desc="load the worlds permanent objects" }
RegisterCommand{ Command="savespawners", AccessLevel = AccessLevel.God, Func=SaveSpawnerData, Desc="save the worlds permanent objects" }
RegisterCommand{ Command="loadspawners", AccessLevel = AccessLevel.God, Func=LoadSpawnerData, Desc="load the worlds permanent objects" }

--------

require 'default:scriptcommands'
require 'default:scriptcommands_mortal'
require 'default:scriptcommands_immortal'
require 'default:scriptcommands_demigod'
require 'default:scriptcommands_immortal'
require 'default:scriptcommands_god'
require 'editors.equipment_editor'
--require 'editors.equipment_editor2'
--- this opens up the equipment editor
function EditItem()
	this:RequestClientTargetGameObj(this, "requestTargetObject")
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse,"requestTargetObject",function(targetObj)
    if(targetObj == nil) then return end
    -- is this a valid piece of equipment such as armor, shields or weapons then display the editor
    if(targetObj:HasObjVar("ArmorType") or targetObj:HasObjVar("ShieldType") or targetObj:HasObjVar("WeaponType")) then
    	if not (targetObj:HasModule("editors.equipment_editor")) then
    		targetObj:AddModule("editors.equipment_editor")
    	end
    	targetObj:SendMessage("OpenForUser",this)
    	this:CloseDynamicWindow("ColorEditorDynamicResponse")
    	this:CloseDynamicWindow("EssenceEditorDynamicResponse")
	else
		this:SystemMessage("[FF7700]This is not a valid item to edit, try targeting a piece of equipment")
		EditItem()
	end
end)

RegisterCommand{ Command="edititem", AccessLevel = AccessLevel.God, Func=EditItem, Usage="", Desc="opens up a item editor for that type of item" }

--[[
function TransmogPermission()
	this:RequestClientTargetGameObj(this, "requestTargetObject")
end


RegisterEventHandler(EventType.ClientTargetGameObjResponse,"requestTargetObject",function(targetObj)
    if(targetObj == nil) then return end
    -- is this a valid piece of equipment such as armor, shields or weapons then display the editor
    if(targetObj:HasObjVar("ArmorType") or targetObj:HasObjVar("ShieldType") or targetObj:HasObjVar("WeaponType")) then
    	if not (targetObj:HasModule("editors.equipment_editor2")) then
    		targetObj:AddModule("editors.equipment_editor2")
    	end
    	targetObj:SendMessage("OpenForUser",this)
    	this:CloseDynamicWindow("ColorEditorDynamicResponse")
    	this:CloseDynamicWindow("EssenceEditorDynamicResponse")
	else
		this:SystemMessage("[FF7700]This is not a valid item to edit, try targeting a piece of equipment")
		TransmogPermission()
	end
end)

RegisterCommand{ Command="transmog", AccessLevel = AccessLevel.Mortal, Func=TransmogPermission, Usage="", Desc="opens up the transmog tool" }
]]

---------
--[[
--- required script files
require 'default:scriptcommands'
require 'editors.dialog_editor'

--- this opens up the dialog editor
function CreateDialog()
    this:RequestClientTargetGameObj(this, "requestTargetNpc")
end

--- handles the targeting of the object, and applys the dialog editor script to it
RegisterEventHandler(EventType.ClientTargetGameObjResponse,"requestTargetNpc",function(targetObj)
    if(targetObj == nil) then return end
    -- is this a valid piece of equipment such as armor, shields,jewelry or weapons then display the editor
    if(targetObj:GetCreationTemplateId() == "dialog_test_guy") then
        if not (targetObj:HasModule("editors.dialog_editor")) then
            targetObj:AddModule("editors.dialog_editor")
        end
        --ShowDialogEditor(user,"Greeting",false)
        -- if this is a new object, make sure we remove the pickers for the old object
    else
        this:SystemMessage("[FF7700]create a dialog_test_guy to work with")
        EditItem()
    end
end)

--- the command to create dialogs 
RegisterCommand{ Command="createdialog", AccessLevel = AccessLevel.God, Func=CreateDialog, Usage="", Desc="opens up a dialog editor" }
]]

function GetClipTarget()
	this:SystemMessage("target a object you want to clip to the clip board")
	this:RequestClientTargetGameObj(this, "targetToClip")
end

function HandleClipTarget(target,user)
	if(target==nil) then return end

	local clipString = ""
	local finalString = ""
	local template = curInfoObj:GetCreationTemplateId()
	local location = curInfoObj:GetLoc()
	local rotation = curInfoObj:GetRotation()
	local scale = curInfoObj:GetScale()
	local dynamicObjectElement = xml.new("DynamicObject")
    local creationParams = template .. " " .. 
                           location.X .. " " ..
                           location.Y .. " " ..
                           location.Z .. " " ..
                           rotation.X .. " " ..
                           rotation.Y .. " " ..
                           rotation.Z .. " " ..
                           scale.X .. " " ..
                           scale.Y .. " " ..
                           scale.Z
    -- add the objectcreationparams element and element data
    dynamicObjectElement:append("ObjectCreationParams")[1] = creationParams
    if(template == "simple_mob_spawner") then
    	local objVarOverridesElement = xml.new("ObjVarOverrides")
    	local spawnTemplateElement = xml.new("StringVariable")
    	local spawnDelayElement = xml.new("DoubleVariable")
    	local spawnCountElement = xml.new("DoubleVariable")
    	local spawnRadiusElement = xml.new("DoubleVariable") 
		
		-- spawnTemplate
		spawnTemplateElement[1] = "template"
		spawnTemplateElement["Name"] = "spawnTemplate"
		objVarOverridesElement:append(spawnTemplateElement)

		-- spawnDelay
		spawnDelayElement[1] = 300 -- default 5 minutes
		spawnDelayElement["Name"] = "spawnDelay"
		objVarOverridesElement:append(spawnDelayElement)

		--spawnCount
		spawnCountElement[1] = 1 -- default spawn of 1
		spawnCountElement["Name"] = "spawnCount"
		objVarOverridesElement:append(spawnCountElement)

		--spawnRadius
		spawnRadiusElement[1] = 0 -- default to spawn at spawner location
		spawnRadiusElement["Name"] = "spawnRadius"
		objVarOverridesElement:append(spawnRadiusElement)

		dynamicObjectElement:append(objVarOverridesElement)
	end    

    clipString = string.gsub(tostring(dynamicObjectElement),"&quot;","\"")
    finalString = AddIndentX2(clipString)
	io.popen('clip','w'):write(finalString):close()
	user:SystemMessage("Object Cliped CTRL+V To Paste")
end

function AddIndentX2(str)
  local result = {}
  local finalResult = ""
  for line in str:gmatch '[^\n]+' do
    table.insert(result, line)
  end
  for k,v in pairs(result) do
  	finalResult = finalResult .. "\t\t"..v.."\n"
  end
  return finalResult
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse, "targetToClip", HandleClipTarget)

RegisterCommand { Command="clip",Category="GizmosTools",AccessLevel=AccessLevel.God,Func=GetClipTarget,Usage="<clientids>", Desc="Copies the objects data and formats it then pastes it to the clip board. This allows you to paste in your <dynamicobject> element block in your seedobjects.xml file." }