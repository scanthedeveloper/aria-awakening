require 'incl_player_titles'
require 'incl_gametime'
require 'scriptcommands_UI_customcommand'
require 'scriptcommands_possessee'
require 'base_militia_window'
require 'base_player_emotes'
require 'base_bug_report'

MortalCommandFuncs = {
	-- Mortal Commands

	Help = function(commandName)
		if( commandName ~= nil ) then
			if(commandName == "actions") then
				local emotesStr = ""
				for commandName, animName in pairs(Emotes) do 
					emotesStr = emotesStr .. "/" .. commandName .. ", "
				end
				emotesStr = StripTrailingComma(emotesStr)
				this:SystemMessage("Emotes: "..emotesStr)
			else
				local commandInfo = GetCommandInfo(commandName)
				if( commandInfo == nil ) then
					this:SystemMessage("Invalid command")
				elseif( not( LuaCheckAccessLevel(this,commandInfo.AccessLevel) or this:HasObjVar("IsGod")) ) then
					this:SystemMessage("You do not have the power to use that command.")
				else
					local usageStr = "Usage: /"..commandName
					if( commandInfo.Usage ~= nil ) then
						usageStr = usageStr.." "..commandInfo.Usage
					end
					this:SystemMessage(usageStr)
					if(commandInfo.Desc ~= nil ) then
						this:SystemMessage(commandInfo.Desc)
					end
				end
			end
		else
			ClientDialog.Show{
			    TargetUser = this,
			    DialogId = "WikiHelp",
			    TitleStr = "Legends of Aria Help",
			    DescStr = "Would you like to open the official Legends of Aria wiki?",
			    Button1Str = "Ok",
			    Button2Str = "Cancel",
			    ResponseObj = this,
			    ResponseFunc = function (user,buttonId)
						if( buttonId == 0) then
							this:SendClientMessage("OpenURL","http://wiki.legendsofaria.com")
						end
					end,
			}

			local outStr = "Available Commands: "
			for i,commandInfo in pairs(CommandList) do
				if( LuaCheckAccessLevel(this,commandInfo.AccessLevel) or this:HasObjVar("IsGod") ) then
					outStr = outStr .. commandInfo.Command .. ", "
				end
			end
			this:SystemMessage(outStr)
			this:SystemMessage("Type /help <command> to get more info.")
			this:SystemMessage("For a list of emotes type /help actions")			
		end
	end,

	Title = function()
		ToggleAchievementWindow(this)
	end,
	Achievement = function()
		ToggleAchievementWindow(this)
	end,

	Profession = function()
		if not( this:HasModule("profession_guide_window") ) then
            this:AddModule("profession_guide_window")
        else
            this:SendMessage("CloseProfessionGuideWindow")
        end
	end,

	Friend = function()
		if not( this:HasModule("friend_ui") ) then
            this:AddModule("friend_ui")
        else
            this:SendMessage("FriendWindowClose")
        end
	end,

	Autoharvesting = function ()
		if (this:HasObjVar("NoQueueHarvest")) then
			this:SystemMessage("Autoharvesting of resources enabled.")
			this:DelObjVar("NoQueueHarvest")
		else
			this:SystemMessage("Autoharvesting of resources disabled.")
			this:SetObjVar("NoQueueHarvest",true)
		end
	end,

	Say = function(...)
		local line = CombineArgs(...)

		-- The line is required to pass the chat filter before being posted.
		if( ChatHelper.PassFilter( line ) ) then
			this:LogChat("say", json.encode(line))
			this:PlayerSpeech(line,30)
		else
			this:SystemMessage("[FFFF00]Chat message blocked by anti-spam.[-]")
		end
		
	end,

	Roll = function(...)
		if ( this:HasTimer("AntiSpamTimer") ) then return end
		this:ScheduleTimerDelay(TimeSpan.FromMilliseconds(500), "AntiSpamTimer")

		local args = table.pack(...)
		local lower = math.max(1, tonumber(args[1]) or 1)
		local upper = math.min(1000, tonumber(args[2]) or 100)
		if ( lower > upper ) then
			local temp = upper
			upper = lower
			lower = temp
		end
		local name = StripColorFromString(this:GetName())
		local roll = math.random(lower,upper)
		local message = string.format("%s rolls %d (%d-%d)", name, roll, lower, upper)
		this:SystemMessage(message)
		local nearbyPlayers = FindObjects(SearchPlayerInRange(30))
		for i=1,#nearbyPlayers do
			nearbyPlayers[i]:SystemMessage(message)
		end
	end,

	Stats = function()
		this:SystemMessage("Str:" ..GetStr(this) .. ",  Agi:" .. GetAgi(this) .. ",  Int:" ..GetInt(this))
		this:SystemMessage("Health:".. GetCurHealth(this).. "/".. GetMaxHealth(this).. " Regen:".. math.floor(GetHealthRegen(this) * 10)/10)
		this:SystemMessage("Stam:".. GetCurStamina(this).. "/".. GetMaxStamina(this).. " Regen:".. math.floor(GetStaminaRegen(this) * 10)/10)
		this:SystemMessage("Mana:".. GetCurMana(this).. "/".. GetMaxMana(this).. " Regen:".. math.floor(GetManaRegen(this) * 10) /10)
	end,

	Where = function()
		local loc = this:GetLoc()
		local locX = string.format("%.2f", loc.X)
		local locY = string.format("%.2f", loc.Y)
		local locZ = string.format("%.2f", loc.Z)
		local facing = string.format("%.0f", this:GetFacing())
		local regions = GetRegionsAtLoc(loc)
		local regionStr = ""
		for i,regionName in pairs(regions) do
			regionStr = regionStr .. regionName .. ", "
		end

		local regionAddress = ServerSettings.RegionAddress
		if(regionAddress ~= nil and regionAddress ~= "") then
			this:SystemMessage("Region Address: "..regionAddress)
		end

		if(IsGod(this)) then
			local ClusterId = ServerSettings.ClusterId
			if(ClusterId ~= nil and ClusterId ~= "") then
				this:SystemMessage("ClusterId: "..ServerSettings.ClusterId)
			end
			this:SystemMessage("World: "..ServerSettings.WorldName)
			this:SystemMessage("Subregions: "..regionStr)

			--DebugMessage( ""..locX.." "..locY.." "..locZ.." 0 "..tostring(facing) )
			--DebugMessage( "("..locX..","..locY..","..locZ..")" )

		end
		
		this:SystemMessage("Loc: "..locX..", "..locY..", "..locZ..", Facing: "..tostring(facing))
	end,

	GroupLeave = function(...)
		local groupId = GetGroupId(this)

		if ( groupId == nil ) then
			this:SystemMessage("You are not in a group.", "info")
			return
		end

		ClientDialog.Show{
			TargetUser = this,
			DialogId = uuid(),
			TitleStr = "Leave Group",
			DescStr = "Are you sure you wish to leave your group?",
			Button1Str = "Confirm",
			Button2Str = "Cancel",
			ResponseFunc=function(user,buttonId)
				if ( user == nil or buttonId ~= 0 ) then return end

				GroupRemoveMember(groupId, this)
			end,
		}		
	end,

	GuildMenu = function()		
		ToggleGuildWindow()
	end,

	MilitiaMenu = function ()
		ToggleMilitiaWindow()
	end,

	Time = function()
		this:SystemMessage("It is "..GetGameTimeOfDayString())
	end,

	--[[
	Hunger = function()
		if ( this:HasTimer("CheckedHunger") ) then return end
		local hunger = this:GetObjVar("Hunger") or 0
		if ( hunger < ServerSettings.Hunger.Threshold ) then
			local tillHungry = TimeSpan.FromSeconds(((ServerSettings.Hunger.Threshold - hunger) / ServerSettings.Hunger.Rate) * 60)
			
			this:SystemMessage("You will be hungry in "..TimeSpanToWords(tillHungry))
			this:ScheduleTimerDelay(TimeSpan.FromSeconds(60), "CheckedHunger")
		else
			this:SystemMessage("You are hungry.")
		end
	end,
	]]

	Resethotbar = function()
		WipeHotbarData()
		this:SystemMessage("Your hotbars have been reset, please log out and then back in.")
	end,

	CustomCommand = function()
		ShowCustomCommandWindow()
	end,

	DeleteChar = function ()
        local createdAt = this:GetObjVar("CreationDate")
        if ( createdAt ) then
            local totalTime = DateTime.UtcNow - createdAt
            local overrideExists = (this:GetObjVar("AllowCharDelete") ~= nil)
            if ( totalTime.TotalDays < ServerSettings.NewPlayer.MinimumDeleteDays and overrideExists == false) then
                this:SystemMessage("Your character must be at least "..ServerSettings.NewPlayer.MinimumDeleteDays.." days old before it can be deleted. This character has existed for "..TimeSpanToWords(totalTime)..".")
                return
            end
        end
        TextFieldDialog.Show{
            TargetUser = this,
            Title = "Delete Character",
            Description = "[$2467]",
            ResponseFunc = function(user,newValue)
                if(newValue == "DELETE") then
                    -- remove from guild (function does nothing if not in a guild)
                    GuildHelpers.Remove(this)

                    local clusterController = GetClusterController()
                    clusterController:SendMessage("UserLogout", this, true);

                    this:DeleteCharacter()
                    
                else
                    this:SystemMessage("Delete character cancelled")
                end
            end
        }
    end,

	PlayTime = function()
		this:SystemMessage("You have played for a total of "..TimeSpanToWords(TimeSpan.FromMinutes(this:GetObjVar("PlayMinutes") or 1)))
	end,

	BugReport = function()
		OpenBugReportDialog(this)
	end,

	--SCAN DISABLED MACRO SYSTEM
	Macro = function()
		--this:SendMessage("StartMobileEffect", "MacroCreator", nil, nil)
		this:SystemMessage("The Macro system has been disabled on this community server.")
	end,

	HelpReport = function()
		if not( this:HasModule("help_report_window") ) then
            this:AddModule("help_report_window")        
        end
	end,

	ResetWindowPos = function ()
		this:SendClientMessage("ClearCachedPanelPositions")
		ShowStatusElement(this,{IsSelf=true,ScreenX=10,ScreenY=10})
	end,

	MilitiaRank = function ()
		if ( Militia.GetId(this) ) then
			local cooldown = this:GetObjVar("MilitiaChatCooldown")
			if ( cooldown ~= nil and DateTime.UtcNow < cooldown ) then
				this:SystemMessage("Must wait a bit to do that.")
				return
			end
			this:SetObjVar("MilitiaChatCooldown", DateTime.UtcNow:Add(ServerSettings.Militia.ChatCooldown))
			this:NpcSpeech("Percentile: > "..(Militia.GetPercentile(this)*100).."% of "..Militia.GetMilitiaName(this))
		else
			this:SystemMessage("Not in a Militia.")
		end
    end,
    
    ToggleHitFlash = function()
        if ( this:HasObjVar("DisableHitFlash") ) then
            this:DelObjVar("DisableHitFlash")
            this:SystemMessage("Combat Damage Hit Flash Enabled.")
        else
            this:SetObjVar("DisableHitFlash", true)
            this:SystemMessage("Combat Damage Hit Flash Disabled.")
        end
	end,

	Lockdown = function()
		local loc = this:GetLoc()
		local controller = Plot.GetAtLoc(loc)
		if(controller ~= nil and controller:IsValid() and Plot.ContainsLoc(controller, loc)) then
			this:SendMessage("StartMobileEffect", "PlotLockdown", controller)
		else
			this:SystemMessage("You must be on a plot you own to lock something down.")
		end
	end,

	Release = function()
		local loc = this:GetLoc()
		local controller = Plot.GetAtLoc(loc)
		if(controller ~= nil and controller:IsValid() and Plot.ContainsLoc(controller, loc)) then
			this:SendMessage("StartMobileEffect", "PlotRelease", controller)
		else
			this:SystemMessage("You must be on a plot you own to release an item.")
		end
	end,

	Decorate = function()
		local plots = Plot.GetPlayerPlots(this)
		local loc = this:GetLoc()
		for i,plotController in pairs(plots) do	
			if(plotController ~= nil and plotController:IsValid() and Plot.ContainsLoc(plotController, loc)) then
				this:SendMessage("StartMobileEffect", "PlotMoveObject", plotController)
			end
		end
		this:SystemMessage("You must be on a plot you own to decorate.")
	end,

	KickStrangers = function()
		local loc = this:GetLoc()
		local controller = Plot.GetAtLoc(loc)
		if(controller ~= nil and controller:IsValid() and Plot.ContainsLoc(controller, loc)) then
			this:SendMessage("StartMobileEffect", "PlotKickStrangers", controller)
		else
			this:SystemMessage("You must be on a plot you own to kick something.")
		end
	end,

	Kick = function()
		local loc = this:GetLoc()
		local controller = Plot.GetAtLoc(loc)
		if(controller ~= nil and controller:IsValid() and Plot.ContainsLoc(controller, loc)) then
			this:SendMessage("StartMobileEffect", "PlotKick", controller)
		else
			this:SystemMessage("You must be on a plot you own to kick something.")
		end	
	end,

	Sell = function( price )
		if( tonumber(price) == nil or not(tonumber(price) > 0) ) then 
			this:SystemMessage("The price must be a valid number.")
			return
		end
		
		local plots = Plot.GetPlayerPlots(this)
		local loc = this:GetLoc()
		local foundPlot = false
		local foundMerchant = false
		for i,plotController in pairs(plots) do	
			if(plotController ~= nil and plotController:IsValid() and Plot.ContainsLoc(plotController, loc)) then
				foundPlot = true
				Plot.ForeachMerchant( plotController, function( merchant, index ) 
					-- Always use the first merchant we find
					if( index == 1 ) then
						merchant:SendMessage("ListItem", this, price)
						foundMerchant = true
					end
				end)
			end
		end

		if( not foundPlot ) then
			this:SystemMessage("You must be on a plot you own to sell items.")
		elseif( not foundMerchant ) then
			this:SystemMessage("Your plot must have a active merchant to sell items.")
		end
	end,

	Discord = function()
		local isSteamPlayer = this:GetAccountProp("SteamId")
		local isFreePlayer = IsFreeAccount(this)

		if not isSteamPlayer then this:SystemMessage("'/discord' is only necessary for Steam players. Please visit your profile dashboard at http://legendsofaria.com/dashboard for your Discord Link Code.") end
		if isFreePlayer then this:SystemMessage("'/discord' is only available for players who have purchased the Citizen's Pass or a Premium Subscription.") end
	end,
}


RegisterCommand{ Command="sell", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Sell, Desc="Sell an item on your plot. <Price> optional." }
RegisterCommand{ Command="kick", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Kick, Desc="Kick target from your plot." }
RegisterCommand{ Command="kickstrangers", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.KickStrangers, Desc="Kick all strangers from your plot." }
RegisterCommand{ Command="decorate", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Decorate, Desc="Move and rotate item on plot or in a house." }
RegisterCommand{ Command="release", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Release, Desc="Release item on plot or in a house." }
RegisterCommand{ Command="lockdown", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Lockdown, Desc="Lockdown item on plot or in a house." }
--MALEVOLENT MOD SCAN ADDED
--RegisterCommand{ Command="help", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Help, Usage="<command_name>", Desc="[$2471]" }
RegisterCommand{ Command="title", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Title, Desc="Show title window" }
RegisterCommand{ Command="achievement", Accesslevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Achievement, Desc = "Show achievement window" }
RegisterCommand{ Command="profession", Accesslevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Profession, Desc = "Show profession window" }
--MALEVOLENT MOD SCAN ADDED
--RegisterCommand{ Command="bugreport", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.BugReport, Desc="Send a bug report" }
RegisterCommand{ Command="macro", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Macro, Desc="Make a macro" }
--RegisterCommand{ Command="helpreport", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.HelpReport, Desc="Send a help report" }
RegisterCommand{ Command="say", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Say, Desc="Say something."}
RegisterCommand{ Command="roll", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Roll, Desc="Roll between 0.0 and 100.0"}
RegisterCommand{ Command="stats", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Stats, Desc="Prints out your stats" }
RegisterCommand{ Command="where", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Where, Desc="Prints location on the map" }
--RegisterCommand{ Command="militiamessage", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.MilitiaMessage, Desc="[$2472]" }
RegisterCommand{ Command="guild", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.GuildMenu, Desc="Open guild window" }
RegisterCommand{ Command="allegiance", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.MilitiaMenu, Desc="Open militia window" }
RegisterCommand{ Command="friend", Accesslevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Friend, Desc = "Open friend list" }
RegisterCommand{ Command="leavegroup", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.GroupLeave, Desc="Leaves your group." }
RegisterCommand{ Command="time", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Time, Desc="Gets the current game time" }
RegisterCommand{ Command="autoharvest", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Autoharvesting, Desc="Toggles autoharvesting on and off." }
RegisterCommand{ Command="custom", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.CustomCommand, Desc="[$2474]" }
RegisterCommand{ Command="deletechar", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.DeleteChar, Desc="Permanently deletes your player character.", Aliases={"delchar", "deletecharacter", "delcharacter"}}
RegisterCommand{ Command="playtime", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.PlayTime, Desc="Tells you your total ingame time.", Aliases={"timeplayed", "totaltime"}}
RegisterCommand{ Command="resethotbar", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Resethotbar, Usage="<command_name>", Desc="Resets your hotbars removing all items." }
--RegisterCommand{ Command="hunger", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Hunger, Desc="Displays your current hunger status" }
RegisterCommand{ Command="resetwindowpos", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.ResetWindowPos, Desc="Resets the saved window positions on the client. Used if one of your windows gets stuck off screen." }
RegisterCommand{ Command="militiarank", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.MilitiaRank, Desc="Prints out Militia rank statistics overhead.", Aliases={"showscore", "punkte", "rank", "standing"}}
RegisterCommand{ Command="togglehitflash", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.ToggleHitFlash, Desc="Disable/Enable the Combat Hit Flash Effect when taking damage." }
--MALEVOLENT MOD SCAN ADDED
--RegisterCommand{ Command="discord", AccessLevel = AccessLevel.Mortal, Func=MortalCommandFuncs.Discord, Usage="<name|id>", Desc="Retrieves your discord link code." }