local mWidth = 780
local mHeight = 760
local squareSize = 126
local tTable = GetAllTemplateNames() --Table of all templates
local x = 1 -- Initial item
local y = 65 -- Rows per page (too many and scrolling becomes too steep!)
local z = math.round(mWidth/(squareSize*1.2))-1 -- Columns per page (4)
local pages = math.ceil(#tTable/(y*z))

-- Button responses:
RegisterEventHandler(EventType.DynamicWindowResponse,"tlWindow",function(user,buttonId,fieldData)
    if tonumber(buttonId) then
        local templName = tostring(tTable[tonumber(buttonId)])
        -- user:SystemMessage(buttonId) --debug
        -- user:SystemMessage(templName) --debug
        CreateObj((templName),user:GetLoc())
        user:PlayEffect("BlacksmithSpark")
        user:PlayObjectSound("DeathCannon")
        user:SystemMessage("[EEEE66]"..templName.." created at your feet")
    elseif buttonId == "buttonPrev" then
        if x>1 then x=x-1 end
    elseif buttonId == "buttonNext" then
        if x < pages then x=x+1 end
    elseif buttonId == "pageNumButton" then
        if (tonumber(fieldData.pageNumField) < 1) or (tonumber(fieldData.pageNumField) > pages) then --Check page valid page
            user:SystemMessage("[BBBBBB]Invalid page.[-]")
        else
            x = tonumber(math.floor(fieldData.pageNumField))
        end
    else
        return -- Without this there is no way to close the window
    end
    TemplateListWindow()
end)

function TemplateListWindow()
    local scrollWindow = ScrollWindow(0,mHeight*0.02,mWidth*0.95,mHeight*0.92,squareSize*0.9) --Scrollable area
    local mainWindow = DynamicWindow("tlWindow","Template List",mWidth,mHeight+38) --Main
    mainWindow:AddButton(mWidth*0.68-32,-28,"buttonPrev","",36,18,"Previous section",nil,false,"Previous")
    mainWindow:AddButton(mWidth*0.68+50,-28,"buttonNext","",36,18,"Next section",nil,false,"Next")
    mainWindow:AddTextField(mWidth*0.68,-28,50,16,"pageNumField",tostring(x))
    mainWindow:AddButton(mWidth*0.68-20,0,"pageNumButton","Goto page",90,16,"Last page: "..pages,nil,false)
    mainWindow:AddScrollWindow(scrollWindow)
    for i=x, y*z, z do
        scrollEle = ScrollElement()
        scrollEle:AddButton(0,0,"scrollBackground",nil,mWidth*0.93,mHeight/5,nil,nil,false,"Invisible")--,"Invisible") --Workaround to improve scrolling - mousewheel currently only works over buttons.
        for j=0, z-1, 1 do
            local tempNum = (x-1)*y*z+i+j -- Table sort number
            local tempName = tostring(tTable[tempNum]) -- i.e: "tv_dagger_dagger"
            -- print(tempName) --debug
            local tempIcon = tostring(GetTemplateIconId(tempName)) -- Template icon, only shows for templates with icons...
            
            scrollEle:AddImage(j*mWidth/z,0,tempIcon,squareSize,squareSize,"Object")
            scrollEle:AddButton(j*mWidth/z,squareSize*0.7,tostring(tempNum),tempName.." ("..tempNum..")",squareSize*1.2,27,tempName,nil,false,"Release")
        end
        scrollWindow:Add(scrollEle)
    end

    this:OpenDynamicWindow(mainWindow)
end


--Teiravon--[JBir]--