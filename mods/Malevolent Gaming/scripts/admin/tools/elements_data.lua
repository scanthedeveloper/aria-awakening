AllElements = {
	AnimationEffects = {
		--[[ False entries, leaving them here to avoid mistaken reentry in the future [JBir]
		"alarmed",
		"anvil_strike",
		"dance",
		"Dance_GuitarSolo",
		"dismiss_one",
		"excited_one",
		"fistbump",
		"flamethrower",
		"no_one",
		"washit",
		"PlayAnim",
		"revive",
		"shakefist",
		"showingoff",
		"yes_one",
		]]
		"angry",
		"anvil",
		"attack",
		"attack_jump",
		"bashful",
		"block",
		"bow",
		"buttslap",
		"carve",
		"cast",
		"cast_heal",
		"cast_lightning",
		"cheer",
		"clapping",
		"dance_bellydance",
		"dance_cabbagepatch",
		"dance_chickendance",
		"dance_gangamstyle",
		"dance_guitarsolo",
		"dance_headbang",
		"dance_hiphop",
		"dance_macarena",
		"dance_raisetheroof",
		"dance_robot",
		"dance_runningman",
		"dance_twist",
		"dance_wave",
		"die",
		"dismiss",
		"dismiss_two",
		"dodge",
		"draw_bow",
		"embarassed",
		"end_draw_bow",
		"excited",
		"excited_two",
		"fabrication",
		"fidget",
		"fishcast",
		"fishhook",
		"fishidle",		
		"fistpump",
		"flipthebird",
		"followthrough",
		"happy",
		"holdcrook",
		"howl",
		"idle",
		"impale",
		"jump_attack",
		"kneel",
		"kneel_standup",
		"lay",
		"lay_ground",
		"loser",
		"no",
		"no_two",
		"point",
		"pummel",
		"really",
		"rend",
		"rise",
		"roar",
		"sad",
		"salute",
		"shakeFist",
		"shield_bash",
		"showingOff",
		"sit",
		"sit_ground",
		"sit_standing_armstothesky",
		"spell_fire",
		"stand",
		"stretch",
		"sunder",
		"sword_in_ground",
		"thinking",
		"throatslash",
		"throatslash_one",
		"throatslash_two",
		"tired",
		"transaction",
		"was_hit",
		"wave",
		"weapon_spin",
		"whatever",
		"wideswept_attack",
		"yes",
		"yes_two",
	},
	
	SoundEffects = {
		--[[	
		"Attack",
		"Death",
		"Drop",	--SFX
		"Hand",
		"MAGIC",	--SFX
		"Pain",	--SFX
		"Use",
		]]	
		"Aaah",	--PC Dialogue
		"AdrenalineRush",	--SFX
		"Alchemy",	--SFX
		"AxeDraw",	--Combat
		"AxeImpactLeather",	--Combat
		"AxeImpactPlate",	--Combat
		"AxeMiss",	--Combat
		"AxeSheathe",	--Combat
		"BackpackClose",	--SFX
		"BackpackDrop",	--SFX
		"BackpackOpen",	--SFX
		"BackpackPickup",	--SFX
		"BandageUse",	--SFX
		"BansheeAttack",	--SFX
		"BansheeDeath",	--SFX
		"BansheePain",	--SFX
		"BatAttack",	--SFX
		"BatDeath",	--SFX
		"BatPain",	--SFX
		"BearAttack",	--SFX
		"BearBodyfall",	--SFX
		"BearDeath",	--SFX
		"BearEnvironment",	--Environment
		"BearImpact",	--SFX
		"BearMiss",	--SFX
		"BearPain",	--SFX
		"BearWalkGrass",	--Footsteps
		"BindPortal",	--SFX
		"BirdWingFlap",	--SFX
		"BlackForestDayAmbience",	--Ambience
		"BlackForestNightAmbience",	--Ambience
		"BlacksmithEnvironment",	--Environment
		"BodyfallLeather",	--SFX
		"BodyfallPlate",	--SFX
		"BodyHit",	--SFX
		"BombardmentImpact",	--SFX
		"BombardmentThrow",	--SFX
		"BookDrop",	--SFX
		"BookPickup",	--SFX
		"BookUse",	--SFX
		"BountyHeadDrop",	--SFX
		"BountyHeadPickup",	--SFX
		"BroadswordDraw",	--Combat
		"BroadswordImpactLeather",	--Combat
		"BroadswordImpactPlate",	--Combat
		"BroadswordMiss",	--Combat
		"BroadswordSheathe",	--Combat
		"Campfire",	--Environment
		"CantUseThatYet",	--PC Dialogue
		"Cast Spell Water",	--SFX
		"CastAir",	--SFX
		"CastFire",	--SFX
		"CastVoid",	--SFX
		"CastWater",	--SFX
		"CatacombsHubAmbience",	--Ambience
		"CatacombsLevel1Ambience",	--Ambience
		"CatacombsLevel2Ambience",	--Ambience
		"CerberusAttack",	--SFX
		"CerberusDeath",	--SFX
		"CerberusPain",	--SFX
		"Charge",	--SFX
		"ChargeFemale",	--SFX
		"ChestClose",	--SFX
		"ChestOpen",	--SFX
		"Cloak",	--SFX
		"ClubAmbience",	--Music
		"CoinsDrop",	--SFX
		"CoinsPickup",	--SFX
		"CosmicGate",	--Music
		"CraftingHammerHit",	--SFX
		"CrateClose",	--SFX
		"CrateOpen",	--SFX
		"Crystal_Magic_04",	--SFX
		"DaggerDraw",	--Combat
		"DaggerDrop",	--SFX
		"DaggerImpactLeather",	--Combat
		"DaggerImpactPlate",	--Combat
		"DaggerMiss",	--Combat
		"DaggerParry",	--SFX
		"DaggerPickup",	--SFX
		"DaggerSheath",	--Combat
		"DeathAttack",	--SFX
		"DeathCannon",	--Ambience
		"DeathDeath",	--SFX
		"DeathPain",	--SFX
		"DeathSkullAttack",	--SFX
		"DeathSkullDeath",	--SFX
		"DeathSkullPain",	--SFX
		"DeathSkullWing",	--SFX
		"DeathVoice1",	--NPC Dialogue
		"DeathVoice2",	--NPC Dialogue
		"DeathVoice3",	--NPC Dialogue
		"DeathVoice4",	--NPC Dialogue
		"DeathVoice5",	--NPC Dialogue
		"DeathVoice6",	--NPC Dialogue
		"DeathVoice7",	--NPC Dialogue
		"DeerAttack",	--SFX
		"DeerDeath",	--SFX
		"DeerEnvironment",	--Environment
		"DeerPain",	--SFX
		"DeerRunDirt",	--Footsteps
		"DeerWalkDirt",	--Footsteps
		"DeerWalkGrass",	--Footsteps
		"Defend",	--SFX
		"DemonAttack",	--SFX
		"DemonDeath",	--SFX
		"DemonIdle",	--SFX
		"DemonPain",	--SFX
		"Desert",	--Music
		"DesertAmbience",	--Ambience
		"Digging",	--SFX
		"DimensionalRipLoop",	--EnviromentLarge
		"DogAttack",	--SFX
		"DogBodyfall",	--SFX
		"DogDeath",	--SFX
		"DogEnvironment",	--Environment
		"DogImpact",	--SFX
		"DogPain",	--SFX
		"DoorClose",	--SFX
		"DoorLock",	--SFX
		"DoorLockPick",	--SFX
		"DoorOpen",	--SFX
		"DoorUnlock",	--Mechanical
		"DragonAttack",	--SFX
		"DragonDeath",	--SFX
		"DragonFlameBreath",	--SFX
		"DragonPain",	--SFX
		"Dungeon",
		"ElectricBolt",	--Music
		"EnergySound",	--SFX
		"EntAttack",	--Music
		"EntDeath",	--SFX
		"EntMiss",	--SFX
		"EntPain",	--SFX
		"EntWalkGrass",	--SFX
		"Evasion",	--Footsteps
		"Eviscerate",	--SFX
		"Fabrication",	--SFX
		"FallingMeteor",	--SFX
		"FemaleShout",	--SFX
		"FightMeYouHeathen",	--NPC Dialogue
		"FireAreaAmbience",	--PC Dialogue
		"Fireball",	--Ambience
		"FireballImpact",	--Projectile
		"FireTrap",	--SFX
		"FishCast",	--SFX
		"FishJump",	--SFX
		"FishReel",	--SFX
		"FlameWave",	--SFX
		"FlatbowImpactLeather",	--SFX
		"FlatbowImpactPlate",	--Combat
		"FlatbowLoad",	--Combat
		"FlatbowMiss",	--Combat
		"FlatbowShoot",	--Combat
		"Flurry",	--Combat
		"FlurryEffect",	--SFX
		"FlurryFemale",	--SFX
		"FollowThrough",	--SFX
		"Forage",	--SFX
		"ForgeLoop",	--SFX
		"ForTheKing",	--Environment
		"Fountain",	--PC Dialogue
		"FrostBolt",	--Environment
		"GargoyleAttack",	--SFX
		"GargoyleDeath",	--SFX
		"GargoylePain",	--SFX
		"GateClose",	--SFX
		"GateOpen",	--Mechanical
		"GhostAmbient",	--Mechanical
		"GhostFemaleAttack",	--SFX
		"GhostFemaleDeath",	--SFX
		"GhostFemalePain",	--SFX
		"GhostMaleAttack",	--SFX
		"GhostMaleDeath",	--SFX
		"GhostMalePain",	--SFX
		"GhoulDeath",	--SFX
		"GhoulPain",	--SFX
		"Graveyard",	--SFX
		"GraveyardDayAmbience",	--Music
		"GraveyardNightAmbience",	--Ambience
		"GreaterHeal",	--Ambience
		"GrimAura",	--SFX
		"GuardBusy",	--SFX
		"GuardCultistKilledMessage1",	--NPC Dialogue
		"GuardCultistKilledMessage2",	--NPC Dialogue
		"GuardCultistKilledMessage3",	--NPC Dialogue
		"GuardCultistKilledMessage4",	--NPC Dialogue
		"GuardPlayerKilledMessage1",	--NPC Dialogue
		"GuardPlayerKilledMessage2",	--NPC Dialogue
		"HammerDraw",	--NPC Dialogue
		"HammerImpactLeather",	--Combat
		"HammerImpactPlate",	--Combat
		"HammerMiss",	--Combat
		"HammerSheathe",	--Combat
		"HandImpact",	--Combat
		"HandMiss",	--SFX
		"HandParry",	--SFX
		"HorseAttack",	--SFX
		"HorseDeath",	--SFX
		"HorsePain",	--SFX
		"HorseRunGrass",	--SFX
		"HorseWalkGrass",	--Footsteps
		"HumanFemaleAttack",	--Footsteps
		"HumanFemaleAttack6",	--SFX
		"HumanFemaleDeath",	
		"HumanFemalePain",	--SFX
		"HumanFemaleWalkDirt",	--SFX
		"HumanFemaleWalkGrass",	--Footsteps
		"HumanFemaleWalkLeaves",	--Footsteps
		"HumanFemaleWalkSnow",	--Footsteps
		"HumanFemaleWalkStone",	--Footsteps
		"HumanFemaleWalkWood",	--Footsteps
		"HumanMaleAttack",	--Footsteps
		"HumanMaleDeath",	--SFX
		"HumanMalePain",	--SFX
		"HumanMaleWalkDirt",	--SFX
		"HumanMaleWalkGrass",	--Footsteps
		"HumanMaleWalkLeaves",	--Footsteps
		"HumanMaleWalkSnow",	--Footsteps
		"HumanMaleWalkStone",	--Footsteps
		"HumanMaleWalkWood",	--Footsteps
		"ICant",	--Footsteps
		"ICantAffordThatYet",	--PC Dialogue
		"IceRain",	--PC Dialogue
		"Impale",	--SFX
		"ImpAttack",	--SFX
		"ImpDeath",	--SFX
		"ImpPain",	--SFX
		"IMustFindAKey",	--SFX
		"IMustWait",	--PC Dialogue
		"IncubusAttack",	--PC Dialogue
		"IncubusDeath",	--SFX
		"IncubusPain",	--SFX
		"INeedMoreMoney",	--SFX
		"IronGateClose",	--PC Dialogue
		"IronGateOpen",	--SFX
		"JustWhatINeeded",	--SFX
		"KatanaDraw",	--PC Dialogue
		"KatanaImpactLeather",	--Combat
		"KatanaImpactPlate",	--Combat
		"KatanaMiss",	--Combat
		"KatanaSheathe",	--Combat
		"KeyDrop",	--Combat
		"KeyPickup",	--SFX
		"KillTheSavage",	--SFX
		"KnifeDrop",	--PC Dialogue
		"KnifePickup",	--SFX
		"KnifeUse",	--SFX
		"LargeShieldMetalBlock",	--SFX
		"LargeShieldWoodBlock",	--Combat
		"Leap",	--Combat
		"LeatherArmorDrop",	--SFX
		"LeatherArmorPickup",	--SFX
		"Light Rain",	--SFX
		"Lightning",	--Weather
		"LightningImpact",	--SFX
		"LightningImpact2",	--SFX
		"LimboAmbience",	--SFX
		"LoadScreenLoop",	--Ambience
		"LongbowImpactLeather",	--Environment
		"LongbowImpactPlate",	--Combat
		"LongbowLoad",	--Combat
		"LongbowMiss",	--Combat
		"LongbowShoot",	--Combat
		"Lumberjack",	--Combat
		"Lunge",	--SFX
		"LungeEffect",	--SFX
		"LungeFemale",	--SFX
		"MaleShout",	
		"MarketEnvironment",	--NPC Dialogue
		"MeatCook",	--Environment
		"MeatDrop",	--SFX
		"MeatPickup",	--SFX
		"Meditation",	--SFX
		"MediumShieldMetalBlock",	--PC Dialogue
		"MediumShieldWoodBlock",	--Combat
		"MerchantGreeting",	--Combat
		"MerchantPlayerNoPurchase",	--NPC Dialogue
		"MerchantPlayerPurchase",	--NPC Dialogue
		"MeteorImpact",	--NPC Dialogue
		"Mining",	--SFX
		"MSwordImpactLeather",	--SFX
		"MSwordImpactPlate",	--Combat
		"MSwordMiss",	--Combat
		"MSwordSheathe",	--Combat
		"Ocean",	--Combat
		"OgreAttack",	--Ambience
		"OgreDeath",	--SFX
		"OgreMiss",	--SFX
		"OgrePain",	--SFX
		"OgreWake",	--SFX
		"PaddedArmorDrop",	--SFX
		"PaddedArmorPickup",	--SFX
		"Perforate",	
		"PhantomAttack",	--SFX
		"PhantomDeath",	--SFX
		"PhantomPain",	--SFX
		"PillarofFire",	--SFX
		"Plains",	--SFX
		"PlainsDayAmbience",	--Music
		"PlainsNightAmbience",	--Ambience
		"PlateArmorDrop",	--Ambience
		"PlateArmorPickup",	--SFX
		"Pommel",	--SFX
		"Portal",	--SFX
		"PotionDrop",	--Environment
		"PotionPickup",	--SFX
		"PotionUse",	--SFX
		"Pulverize",	--SFX
		"Puncture",	--SFX
		"PVE",	--SFX
		"PvEIslandAmbience",	--Music
		"PVP",	--Ambience
		"PvPIslandAmbience",	--Music
		"QuestComplete",	--Ambience
		"RabbitDeath",	--SFX
		"Rain",	--SFX
		"RatAttack",	--Weather
		"RatBodyfall",	--SFX
		"RatDeath",	--SFX
		"RatEnvironment",	--SFX
		"RatImpact",	--Environment
		"RatPain",	--SFX
		"ReanimateSkeleton",	--SFX
		"RecurveImpactLeather",	--SFX
		"RecurveImpactPlate",	--Combat
		"RecurveLoad",	--Combat
		"RecurveMiss",	--Combat
		"RecurveShoot",	--Combat
		"RelicDrop",	--Combat
		"RelicPickup",	--SFX
		"Restoration",	--SFX
		"Resurrect",	--SFX
		"RiseAgain",	--SFX
		"SabreDraw",	--Music
		"SabreImpactLeather",	--Combat
		"SabreImpactPlate",	--Combat
		"SabreMiss",	--Combat
		"SabreSheathe",	--Combat
		"ScrollDrop",	--Combat
		"ScrollPickup",	--SFX
		"ScrollUse",	--SFX
		"SewerAmbience",	--SFX
		"ShieldBash",	--Ambience
		"ShieldMetalDrop",	--SFX
		"ShieldMetalPickup",	--SFX
		"ShieldWoodDrop",	--SFX
		"ShieldWoodPickup",	--SFX
		"Shockwave",	--SFX
		"Shout",	--SFX
		"SkeletonAttack",	--SFX
		"SkeletonDeath",	--SFX
		"SkeletonPain",	--SFX
		"SkillGain",	--SFX
		"SmallShieldMetalBlock",	--GUI
		"SmallShieldWoodBlock",	--Combat
		"SmallswordDraw",	--Combat
		"SmallswordImpactLeather",	--Combat
		"SmallswordImpactPlate",	--Combat
		"SmallswordMiss",	--Combat
		"SmallswordSheathe",	--Combat
		"SnakeAttack",	--Combat
		"SnakeBodyfall",	--SFX
		"SnakeDeath",	--SFX
		"SnakeEnvironment",	--SFX
		"SnakeImpact",	--Environment
		"SnakePain",	--SFX
		"SoulDrain",	--SFX
		"SoupCook",	--SFX
		"SouthernHills",	--SFX
		"SouthernHillsAmbience",	--Music
		"SpaceAura",	--Ambience
		"SpawnImp",	--Music
		"Spell Impact 01",	--SFX
		"SpiderAttack",	--SFX
		"SpiderDeath",	--SFX
		"SpiderPain",	--SFX
		"SpikePath",	--SFX
		"SpikeTrapIn",	--SFX
		"SpikeTrapOut",	--SFX
		"Stab",	--SFX
		"SuccubusAttack",	--Combat
		"SuccubusDeath",	--SFX
		"SuccubusPain",	--SFX
		"Sunder",	--SFX
		"SunderEffect",	--SFX
		"SwampDayAmbience",	--SFX
		"SwampNightAmbience",	--Ambience
		"SwordDrop",	--Ambience
		"SwordImpactChainmail",	--SFX
		"SwordParry",	--SFX
		"SwordPickup",	--SFX
		"TavernEnvironment",	--SFX
		"Teleport",	--Environment
		"TeleportToEffect",	--SFX
		"THAxeDraw",	
		"THAxeImpactLeather",	--Combat
		"THAxeImpactPlate",	--Combat
		"THAxeMiss",	--Combat
		"THAxeSheathe",	--Combat
		"TheHeavenlyEnvoy",	--Combat
		"Theme",	--Music
		"TheSpellsNotReady",	--Music
		"THHammerDraw",	--PC Dialogue
		"THHammerImpactLeather",	--Combat
		"THHammerImpactPlate",	--Combat
		"THHammerMiss",	--Combat
		"THHammerSheathe",	--Combat
		"Thunderstorm",	--Combat
		"Torch",	--Weather
		"TownAmbience",	--Environment
		"Trinit",	--EnviromentLarge
		"TurkeyAttack",	--Music
		"UtilityPouchClose",	--SFX
		"UtilityPouchDrop",	--SFX
		"UtilityPouchOpen",	--SFX
		"UtilityPouchPickup",	--SFX
		"Valus",	--SFX
		"VegetableDrop",	--Music
		"VegetablePickup",	--SFX
		"VegetableUse",	--SFX
		"VoidBlast",	--SFX
		"WallofFire",	--SFX
		"WarhammerDraw",	--SFX
		"WarhammerDrop",	--SFX
		"WarhammerImpactLeather",	--SFX
		"WarhammerImpactPlate",	--SFX
		"WarhammerMiss",	--SFX
		"WarhammerParry",	--SFX
		"WarhammerPickup",	--SFX
		"WarhammerSheathe",	--SFX
		"Waterfall",	--SFX
		"WaterLoop",	--EnviromentLarge
		"WaterShore",	--!!! Enter Unique Category Name Here !!!
		"WerebatAttack",	--EnviromentLarge
		"WerebatDeath",	--SFX
		"WerebatPain",	--SFX
		"WerewolfRoar",	--SFX
		"WhereAmI",	--SFX
		"WindLoop",	--PC Dialogue
		"Windmill",	--Ambience
		"WolfAttack",	--Environment
		"WolfBodyfall",	--SFX
		"WolfDeath",	--SFX
		"WolfEnvironment",	--SFX
		"WolfEnvironment2",	--Environment
		"WolfImpact",	
		"WolfPain",	--SFX
		"WolfWalkGrass",	--SFX
		"WoodenDoorClose",	--Footsteps
		"WoodenDoorOpen",	--SFX
		"Woodsmith",	--SFX
		"WormAttack",	--SFX
		"WormDeath",	--SFX
		"WormPain",	--SFX
	},

	VisualEffects = {
		--[[
		"Effect",
		"Restoration",
		]]
		"AfterburnEffect",
		"ArcaneSpitEffect",
		"ArenaFlagBlueEffect",
		"ArenaFlagRedEffect",
		"ArrowEffect",
		"AvoidArrowEffect",
		"BardAbsorbEarthEffect",
		"BardAbsorbThewEffect",
		"BardAbsorbWaterEffect",
		"BardDistortionCyan",
		"BardDistortionGreen",
		"BardDistortionMagenta",
		"BardDistortionOrange",
		"BardDistortionRed",
		"BardDistortionViolet",
		"BardExplosionCyan",
		"BardExplosionRed",
		"BardExplosionViolet",
		"BardProjectileGreen",
		"BardProjectileMagenta",
		"BardProjectileOrange",
		"BardSongEarthEffect",
		"BardSongThewEffect",
		"BardSongWaterEffect",
		"BashEffect",
		"BeamEffect",
		"BlackForestFogEffect",
		"BlackForestSpores",
		"BlackForestSpores",
		"BlackHoleEffect",
		"BlacksmithSpark",
		"BlacksmithSpark",
		"BlacksmithSpark2",
		"Blood7",
		"BloodBleedEffect",
		"BloodBleedEffect",
		"BloodDropsEffect",
		"BloodEffect_A",
		"BloodSplatter1Effect",
		"BloodSplatter2Effect",
		"BloodSplatter3Effect",
		"BloodSplatter4Effect",
		"BlowingSandEffect",
		"BlueFireball1",
		"BlueFogEffect",
		"BlueParticlesShot",
		"BodyExplosion",
		"bsSmoke",
		"BubbleEffect",
		"BubbleEnergyEffect",
		"BubbleSparksEffect",
		"BuffEffect_A",
		"BuffEffect_B",
		"BuffEffect_C",
		"BuffEffect_D",
		"BuffEffect_E",
		"BuffEffect_F",
		"BuffEffect_G",
		"BuffEffect_H",
		"BuffEffect_I",
		"BuffEffect_J",
		"BuffEffect_K",
		"BuffEffect_L",
		"BuffEffect_M",
		"BuffEffect_O",
		"ButterfliesBlueEffect",
		"ButterfliesRedEffect",
		"CastAir",
		"CastAir2",
		"CastEarth",
		"CastEarth2",
		"CastFire",
		"CastFire2",
		"CastVoid",
		"CastVoid2",
		"CastWater",
		"CastWater2",
		"ChainLightningEffect",
		"ChainLightningExplosionEffect",
		"ChargedWeaponEffect",
		"ChargeEffect",
		"CleaverObject",
		"CloakEffect",
		"ClubDrugEffect",
		"ComeDownFromSkyEffect",
		"ConjurePrimeBlueEffect",
		"ConjurePrimeGreenEffect",
		"ConjurePrimePurpleEffect",
		"ConjurePrimeRedEffect",
		"ConjurePrimeYellowEffect",
		"CriticalHitEffect",
		"CureEffect",
		"CystExplode",
		"DarkEnergySpawnEffect",
		"DeathwaveEffect",
		"DesertDustStorm",
		"DesertDustStorm",
		"DesertDustStormLight",
		"DesertHeatDistortionEffect",
		"DesertHeatDistortionEffect",
		"DesertWeatherEffect",
		"DestructionBlueEffect",
		"DestructionEffect",
		"DevilHandEffect",
		"DigDirtEffect",
		"DigDirtParticle",
		"DragonFireEffect",
		"DragonFireBlueEffect",
		"DrainEffect",
		"DrunkenEffect",
		"DustTrailEffect",
		"DustwaveEffect",
		"EarthFlagEffect",
		"EarthquakeDustEffect",
		"EldeirFlagEffect",
		"ElectricBallEffect",
		"ElectricShield",
		"EnergyExplosionEffect",
		"EnergyShieldEffect",
		"ExplosionBallEffect",
		"FallingLeavesEffect",
		"FallingRockDustEffect",
		"FallingRockEffect",
		"FastSlashEffect",
		"FireballEffect",
		"FireballExplosionEffect",
		"FireFlagEffect",
		"FireFliesEffect",
		"FireFliesEffect",
		"FireForceShield",
		"FireHeadEffect",
		"FirePillarEffect",
		"FireShield",
		"FireTornadoEffect",
		"FlameAuraEffect",
		"FlameThrowerTrapEffect",
		"FliesEffect",
		"FloatingOrbEffect",
		"FlurryEffect",
		"ForceField",
		"ForceShield",
		"FrostBallEffect",
		"FrostBoltEffect",
		"FrostShield",
		"FrostShotEffect",
		"FrostWave",
		"FrozenMineEffect",
		"FrozenObjectEffect",
		"GhostEffect",
		"GoldenFogEffect",
		"GrassWindEffect",
		"GrassWindEffect",
		"GravityWaveEffect",
		"GravityWaveEffect",
		"GrayScreenEffect",
		"GreaterHealEffect",
		"GreenFogEffect",
		"GreenParticlesBuffEffect",
		"GreenPortalEffect",
		"GrimAuraDebuffEffect",
		"GrimAuraEffect",
		"GroundBuff2Object",
		"GroundBuff3Object",
		"GroundBuffObject",
		"GroundCircleGreen1",
		"GroundCircleGreen2",
		"GroundCircleRed1",
		"GroundCircleRed2",
		"GroundSpikeEffect",
		"HeadFlareEffect",
		"HealEffect",
		"HelmFlagEffect",
		"HexSpellEffect",
		"HolyEffect",
		"HoneycombShield",
		"IceBall",
		"IceCrystalExplosionEffect",
		"IceCrystalFrozenEffect",
		"IceExplosionEffect",
		"IceLanceEffect",
		"IceRainEffect",
		"IceRaySpellEffect",
		"IceSparkEffect",
		"IgnitedEffect",
		"ImpactWaveEffect",
		"LaughingSkullEffect",
		"LightningBallEffect",
		"LightningBodyEffect",
		"LightningCloudEffect",
		"LightningEffect",
		"LightningExplosionEffect",
		"LungeEffect",
		"MagicPrimeHandBlueEffect",
		"MagicPrimeHandGreenEffect",
		"MagicPrimeHandPurpleEffect",
		"MagicPrimeHandRedEffect",
		"MagicPrimeHandYellowEffect",
		"MagicPrimeTrailBlueEffect",
		"MagicPrimeTrailGreenEffect",
		"MagicPrimeTrailRedEffect",
		"MagicPrimeTrailYellowEffect",
		"MagicRingEffect",
		"MagicRingExplosionEffect",
		"ManaInfuseGive",
		"ManaInfuseReceive",
		"ManaMissileEffect",
		"ManaMissileExplosionEffect",
		"ManaSpell2Effect",
		"ManaSpell2ExplosionEffect",
		"MeatExplosion",
		"MeteorEffect",
		"MouseRing",
		"MusicNoteIntensity1Effect",
		"MusicNoteIntensity2Effect",
		"MusicNoteIntensity3Effect",
		"ObjectGlowEffect",
		"PeriodicDustEffect",
		"PinkFlowersWindEffect",
		"PlainsSporesEffect",
		"PlainsSporesWhiteEffect",
		"PoisonCloudEffect",
		"PoisonCloudEffectSmall",
		"PoisonObjectEffect",
		"PoisonRingEffect",
		"PoisonSpellEffect",
		"PommelEffect",
		"PotionHealEffect",
		"PrimedAir",
		"PrimedAir2",
		"PrimedEarth2",
		"PrimedFire",
		"PrimedFire2",
		"PrimedVoid",
		"PrimedVoid2",
		"PrimedWater",
		"PrimedWater2",
		"PurplePortalEffect",
		"PyrosFlagEffect",
		"RadiationAuraEffect",
		"RainEffect",
		"RainLightEffect",
		"RainStormEffect",
		"ReaperCrystalRay",
		"ReaperForceShield",
		"RedCoreImpactWaveEffect",
		"RedShadowFogEffect",
		"RegenEffect",
		"ResurrectEffect",
		"RipplesEffect",
		"RiverSplashEffect",
		"RockDropEffect",
		"RockLaunchEffect",
		"RootedSpellEffect",
		"SacredCactusEffect",
		"SandDustLightEffect",
		"ScreenShakeEffect",
		"ShadowFogEffect",
		"ShieldBashEffect",
		"ShockwaveEffect",
		"ShoutEffect",
		"SlimeTrailEffect",
		"SlimeTrailEffect",
		"SlowEffect",
		"SoulDrainEffect",
		"SpitAcidEffect",
		"SprintTrailEffect",
		"StarfallEffect",
		"StarsEffect",
		"StatusEffectBlood",
		"StatusEffectHex",
		"StatusEffectPoison",
		"StatusEffectSlow",
		"StatusEffectStunned",
		"StrangerEffect",
		"StunnedEffectObject",
		"SunderEffect",
		"SunRayEffect",
		"TargetArrowEffect",
		"Teleport1Effect",
		"TeleportFromEffect",
		"TeleportToEffect",
		"ThrownClientObjectEffect",
		"TreeEventFinishEffect",
		"TreeEventHolyWaterEffect",
		"TumbleweedEffect",
		"TurbulentWindEffect",
		"VanguardEffect",
		"VoidAuraEffect",
		"VoidBlastEffect",
		"VoidExplosionEffect",
		"VoidPillar",
		"VoidTeleportToEffect",
		"WarriorAoETaunt",
		"WarriorBeacon",
		"WarriorBloodlustDefensive",
		"WarriorBloodlustedDefensive",
		"WarriorBloodlustedOffensive",
		"WarriorBloodlustOffensive",
		"WarriorCorpseBloodlusted",
		"WarriorDemoShout",
		"WarriorDemoStatus",
		"WarriorEnrage",
		"WarriorExecute",
		"WarriorExecuteAct",
		"WarriorNemesis",
		"WarriorShield",
		"WarriorSiphon",
		"WarriorTargetTaunt",
		"WarriorTaunted",
		"WaterFlagEffect",
		"WaterShield",
		"WaterSplashEffect",
		"WaterSplashEffect",
		"WaterSurfaceWalkEffect",
		"WindEffect",
		"WispBeamEffect",
		"WispEffect",
		"WispExplosionEffect",
		"WispLaunchEffect",
		"WispShieldEffect",
		"WispSummonEffect",
		"YellowPortalEffect",
		"TelegraphShapeEffect",
		"SnowEffect",
		"SnowPathEffect",
		"ImpactWaveSmallEffect",
	},

	Buttons = {
		"BackpackButton",
		"BookList", --"faded"--(string) button state (optional, valid options are default,pressed,disabled)
		"BookPageDown",
		"BookPageUp",
		"CategoryButton",
		"CharacterButton",
		"CombatToggle",
		"CloseSquare",
		"Default",
		"DownButtonSquare",
		"EvocationTab",
		"FemaleButton",
		"HelpButton",
		"Invisible",
		"Keys",
		"LeftArrow",
		"List",
		"MaleButton",
		"ManifestationTab",
		"Minus",
		"Next",
		-- "NextArrow",
		"NextEnd",
		"OrnateLeft",
		"OrnateRight",
		"PageCircle",
		"Plus",
		"Previous",
		"PreviousEnd",
		"PvPButton",
		"Radio",
		"Release",
		"RightArrow",
		"ScrollTitleText",
		"SectionButton",
		"Selection",
		"Selection2",
		-- "SettingsButton",
		"SkillButton",
		"SkillSectionButton",
		"SkillTab",
		"Star",
		"StealthButton",
		-- "TabBar",
		"TabButton",
		"ThinFrameHover",
		"Title",
		"Track",
		"UpButtonSquare",
		"UpDownLock",
	},
	
	Images = {
		"AbilitiesTab_Off",
		"AbilityDock_AbilitySlot",
		"AbilityDock_BG",
		"AbilityDock_Center_Background",
		"Accept_Knife",
		"ArrowFrameBackground1",
		"BasicWindow_Panel",
		"Blank",
		"Blank_Slot",
		"CategoryTitleFrameDivider",
		"Checkbox32px_Off",
		"Checkbox32px_On",
		"Checkmark",
		"CombatTarget_Icon",
		"ConversationWindow_BG",
		"CraftingItemsFrame",
		"CraftingNoItemsFrame",
		"CraftingTabDivider",
		"CraftingWindowSectionBackground",
		"CraftingWindowSectionHeader",
		"CraftingWindowWeaponTypeHighlight",
		"DiamondButtons_Active_Default_House",
		"DiamondButtons_DBackground",
		"DiamondButtons_Icon_Active_AggroStance",
		"DiamondButtons_Icon_Active_BalancedStance",
		"DiamondButtons_Icon_Active_Character",
		"DiamondButtons_Icon_Active_DefenseStance",
		"DiamondButtons_Icon_Active_GodPowers",
		"DiamondButtons_Icon_Active_PVPFlag",
		"DiamondButtons_Icon_Active_Quest",
		"DiamondButtons_Icon_Active_Run",
		"DiamondButtons_Icon_Active_Skills",
		"DiamondButtons_Icon_Default_AggroStance",
		"DiamondButtons_Icon_Default_AutoTarget",
		"DiamondButtons_Icon_Default_BalancedStance",
		"DiamondButtons_Icon_Default_Character",
		"DiamondButtons_Icon_Default_House",
		"DiamondButtons_Icon_Default_PVPFlag",
		"DiamondButtons_Icon_Default_Quest",
		"DiamondButtons_Icon_Default_Run",
		"DiamondButtons_Icon_Default_Settings",
		"DiamondButtons_Icon_Default_Walk",
		"DiamondButtons_Icon_Hover_AggroStance",
		"DiamondButtons_Icon_Hover_BalancedStance",
		"DiamondButtons_Icon_Hover_Character",
		"DiamondButtons_Icon_Hover_DefensiveStance",
		"DiamondButtons_Icon_Hover_GodPowers",
		"DiamondButtons_Icon_Hover_House",
		"DiamondButtons_Icon_Hover_PVPFlag",
		"DiamondButtons_Icon_Hover_Quest",
		"DiamondButtons_Icon_Hover_Run",
		"DiamondButtons_Icon_Hover_Settings",
		"DiamondButtons_Icon_Hover_Walk",
		"Divider",
		"DropHeaderBackground",
		"DynamicBar",
		"EmptySlotIcon",
		"EquipSlotBG",
		"ExpandBackgroundGradient",
		"Familiar_Slot",
		"FireFactionSymbolLarge",
		"FireFactionSymbolSmall",
		"FullBody_Slot",
		"FullBorder",
		"GreyOutImage",
		"Head_Slot",
		"HeaderRow_Bar",
		"healthbar_compactframe",
		"healthbar_frame",
		"HealthStatusBarBackground",
		"HealthStatusBarFrame",
		"HorizontalWindowDivider",
		"HouseDecorationWindow_EastButtonDefault","HouseDecorationWindow_EastButtonHover","HouseDecorationWindow_EastButtonClick",
		"HouseDecorationWindow_NorthButtonDefault","HouseDecorationWindow_NorthButtonHover","HouseDecorationWindow_NorthButtonClick",
		"HouseDecorationWindow_SouthButtonDefault","HouseDecorationWindow_SouthButtonHover","HouseDecorationWindow_SouthButtonClick",
		"HouseDecorationWindow_WestButtonDefault","HouseDecorationWindow_WestButtonHover","HouseDecorationWindow_WestButtonClick",
		"HouseDecorationWindow_RotateCW_Default","HouseDecorationWindow_RotateCW_Hover","HouseDecorationWindow_RotateCW_Click",
		"HouseDecorationWindow_RotateCCW_Default","HouseDecorationWindow_RotateCCW_Hover","HouseDecorationWindow_RotateCCW_Click",
		"HouseDecorationWindow_Button_Default","HouseDecorationWindow_Button_Hover","HouseDecorationWindow_Button_Click",
		"InventoryDivider",
		"InventoryWindow_NameplateDesign",
		"KeysButton_Active",
		"KeysButton_Default",
		"KeysButton_Hover",
		"LargeTextFieldUnsliced",
		"LeftHand_Slot",
		"Legs_Slot",
		"LighterSlotIcon",
		"location_arrow",
		"lock",
		"LockButton_default",
		"LockButton_hover",
		"manabar_compactframe",
		"manabar_frame",
		"ManaStatusBarBackground",
		"ManaStatusBarFrame",
		"ObjectPictureFrame",
		"PageButtonBackground",
		"PageButtonDefault",
		"PageButtonHover",
		"PageHighlightFrame",
		"PartialBorder",
		"Prestige_Divider",
		"Prestige_StaticIconFrame",
		"Prestige_TitleHeader",
		"progress_bar_empty",
		"progress_bar_full",
		"ProgressBarFrame",
		"ProgressBarFull",
		"QuestCompletionRibbonDesign",
		"RightHand_Slot",
		"Scroll_Divider",
		"ScrollParchmentUIWindow",
		"SearchBarFrame",
		"SectionBackground",
		"SectionDividerBar",
		"SectionDividerBar_Half",
		"SectionDividerFrames_nobackground",
		"SelfTarget_Icon",
		"Skill_Channeling",
		"Skill_Endurance",
		"Skill_Manifestation",
		"Skill_Melee",
		"Skill_Necromancy",
		"Skill_Regeneration",
		"Skill_Rogue",
		"SkillIconsFrame",
		"SkillLevelBarDivider",
		"SkillLevelBarFrame",
		"SkillsCategoryTitleFrame",
		"SkillSelectionFrame_Default",
		"SkillsSelectionFrame_Default",
		"SkillsSelectionFrame_Hover",
		"SkillsTab_Off",
		"SkillTracker_BG",
		"SkillTrackerFrame",
		"SpellAbilInvalid",
		"Spellbook",
		"SpellIndexInfo_Divider",
		"StaminaBar_Frame",
		"StatBarFrame",
		"StatusIcon_Backpack",
		"StatusIcon_Backpack_Active",
		"StatusIcon_Backpack_Default",
		"StatusIcon_Backpack_Hover",
		"StatusIcon_Combat",
		"StatusIcon_Passive",
		"StatusIcon_Passive_Hover",
		"Tab_Bar",
		"TabFrameBackground",
		"TabSelectionBackground",
		"Target_background",
		"Textbox_Major_Elements",
		"TextBoxBG",
		"TextFieldChatUnsliced",
		"ThinFrameBackgroundExpand",
		"ThinFrameBackgroundInfo",
		"TitleBackground",
		"TitleBar",
		"Torso_Slot",
		"TradeWindow_AcceptedCheckBox",
		"TradeWindow_Arrow_MyOffer",
		"TradeWindow_Arrow_TheirOffer",
		"TradeWindow_BG",
		"TradeWindow_Background",
		"unlock",
		"UtilityBar_Frame",
		"UtilityBar_StatusFrame",
		"VerticalWindowDivider",
		"WaterFactionSymbolLarge",
		"WaterFactionSymbolSmall",
		"WindowBackgroundFancy",
	},
}