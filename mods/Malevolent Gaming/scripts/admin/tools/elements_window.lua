require 'admin.tools.elements_data'

local squareSize = 100
local tabCount, rowCount = 5, 3
local mWidth, mHeight = tabCount*(squareSize+25)+25, rowCount*squareSize+146

local SECTIONS = {
    "AnimationEffects", -- 1
    "SoundEffects", -- 2
    "VisualEffects", -- 3
    "Buttons", -- 4
    "Images", -- 5
}
local sectIndex = 1
local imageColor = "FFFFFF"
local imageHue = 0
local customFieldContent = ""
local targPerformer = this
local playDuration = 12

local spriteTypes, spriteInd, buttonTypes = {"Simple","Sliced"}, 1, {"List","TabButton"}
local lastImage

-- Capture button press
RegisterEventHandler(EventType.DynamicWindowResponse,"ElementsWindow",
        function(user,buttonId,fieldData)
        EleWinButtonResponse(user,buttonId,fieldData)
    end
)

RegisterEventHandler(EventType.ClientTargetGameObjResponse,"TargetPlayTarget",function(target)
    if target then targPerformer = target end
    EleWindow()
end)

-- Main dynamic window
-- SCAN ADDED (HOPE MOD)
function LoremasterPermission()

    local loremasterPlayer = {
        -- Add character permissions here
        "SCAN",
        "LORD SCAN",
        "The Dark Lord Hellicus",
    }
    
    local loremasterRole = false
		for index, name in ipairs(loremasterPlayer) do
			if name == this:GetName() then
				loremasterRole = true
			end
		end

		if( loremasterRole ) then
            EleWindow()
        else
            this:SystemMessage("You do not have the admin-granted, Loremaster Permission. " ..this:GetName())
            this:SystemMessage("You do not have the admin-granted, Loremaster Permission. " ..this:GetName(), "info")
        end			
end

-- Main dynamic window
function EleWindow()
    -- Create main window body
    local mainWindow = DynamicWindow("ElementsWindow",string.gsub(SECTIONS[sectIndex],"Effects"," Effects"),mWidth,118,1,1,"TransparentDraggable")
    mainWindow:AddImage(0,0,"Blank",mWidth,118,"Sliced","332211")
    mainWindow:AddImage(0,0,"ProgressBarFrame",(mWidth>1000 and 1000 or mWidth),118,"Sliced","ff9988") -- Indicates dragging zone (maximum possible is 1000)
    mainWindow:AddImage(0,118,"TextFieldChatUnsliced",mWidth,mHeight-35,"Sliced")
    mainWindow:AddImage(0,118,"TextFieldChatUnsliced",mWidth,mHeight-57,"Sliced")
    -- Scroll area
    mainWindow:AddScrollWindow(formScrollArea(SECTIONS[sectIndex]))
    -- Navigation
    for i=1, #(SECTIONS) do -- Section buttons
        sTitle = string.gsub(SECTIONS[i],"Effects","") --Subtract "Effects" from name
        mainWindow:AddButton((i-1)*mWidth/#(SECTIONS)*0.95+10,6,tostring(-i),sTitle,mWidth/#(SECTIONS)*0.95,28,sTitle,nil,false,"Release",(i == sectIndex and "pressed" or "default"))
    end
    mainWindow:AddButton(mWidth-64,40,"leftA","[000000]< [-]",15,15,"Previous section",nil,false,"PageCircle")
    mainWindow:AddButton(mWidth-46,40,"rightA","[000000]> [-]",15,15,"Next section",nil,false,"PageCircle")
    mainWindow:AddButton(mWidth-17,1,"closeButton","",16,16,"Close window",nil,true,"CloseSquare")
    mainWindow:AddButton(1,mHeight+65,"closeButton","",16,16,"Close window",nil,true,"CloseSquare")
    -- Window resize controls
    mainWindow:AddButton(0,-16,"-Col","",20,20,"Decrease columns",nil,false,"Minus")
    mainWindow:AddButton(18,-16,"+Col","",20,20,"Increase columns",nil,false,"Plus")
    mainWindow:AddButton(-20,-8,"-Row","",20,20,"Decrease rows",nil,false,"Minus")
    mainWindow:AddButton(-20,10,"+Row","",20,20,"Increase rows",nil,false,"Plus")
    -- Fields
    if SECTIONS[sectIndex] == "Images" then
        mainWindow:AddButton(mWidth-380,mHeight+57,"imageColorButton","Color",50,20,"Set image color\nDefault: FFFFFF",nil,false)
        mainWindow:AddTextField(mWidth-330,mHeight+59,75,12,"imageColorField",imageColor)
        mainWindow:AddButton(mWidth-240,mHeight+57,"imageHueButton","Hue",50,20,"Set image Hue number",nil,false)
        mainWindow:AddTextField(mWidth-190,mHeight+59,75,12,"imageHueField",tostring(imageHue))
        mainWindow:AddButton(mWidth-100,mHeight+60,"0",spriteTypes[spriteInd],70,20,"Toggle Simple/Sliced/Object",nil,false,buttonTypes[spriteInd])
    end
    -- Custom effects button
    mainWindow:AddTextField(10,92,254,23,"customPlayField",customFieldContent)
    mainWindow:AddButton(265,92,"customPlayButton","",28,20,"Play",nil,false,"Next")
    mainWindow:AddButton(293,90,"customStopButton","",23,23,"Stop",nil,false,"Default")
    -- Effects duration
    mainWindow:AddLabel(18,72,"Duration:",70,0,16)
    mainWindow:AddTextField(80,70,35,18,"durationField",tostring(playDuration))
    -- Target performing the effects (default is self)
    mainWindow:AddLabel(44,48,tostring(targPerformer:GetName()),70,0,16)
    mainWindow:AddImage(20,46,"SkillIconsFrame",19,19,nil,"FF4444")
    mainWindow:AddButton(20,46,"targetButton","",19,19,"[ccff99]Select effects emitter.\nCurrent[-]:\n"..tostring(targPerformer:GetName()).."   ( [ffcc00]"..tostring(targPerformer).."[-] )",nil,true,"Radio")


    -- Show the window
    this:OpenDynamicWindow(mainWindow)
end

-- Create the scroll area
function formScrollArea(section)
    local activeSection = AllElements[section]
    local scrollHeight = (section == "Buttons" or section == "Images") and (squareSize+18) or (squareSize*0.4366) --Smaller scroll sizes for effects 
    local scrollWindow = ScrollWindow(12,100,mWidth-20,mHeight*0.95,scrollHeight)
    -- DebugMessage("tabCount: "..tabCount) --debug
    for i=1, #(activeSection), tabCount do  -- for i=1 until i=elements amount increase by tabCount each time --forms tabCount elements at a time in each row(item)
        scrollEle = ScrollElement()
        scrollEle:AddButton(0,25,"scrollBackground",nil,mWidth*0.94,(scrollHeight),nil,nil,false,"Invisible","pressed") --Allows proper mousewheel scrolling
        if section == "AnimationEffects" or section == "SoundEffects" or section == "VisualEffects" then
            for j=0, tabCount-1, 1 do
                if activeSection[i+j] then
                    scrollEle:AddButton(j*(squareSize+25),20,tostring(j+i),"",squareSize*1.2,squareSize*0.4,"[FFFF00]"..activeSection[i+j].."[-]",nil,false,"List")
                    scrollEle:AddLabel(j*(squareSize+25)+5,25,activeSection[i+j],squareSize*1.1,squareSize*1.08,18,"left",false,true)
                end
            end
        elseif section == "Buttons" then
            for j=0, tabCount-1, 1 do
                if activeSection[i+j] then
                    scrollEle:AddButton(j*(squareSize+25)-5,squareSize+15,tostring(j+i),"",24,24,"Load [FFFF00]"..activeSection[i+j].."[-] to field",nil,false,"Radio",(customFieldContent == activeSection[i+j]) and "pressed" or nil)
                    scrollEle:AddButton(10+j*(squareSize+25),40,"button"..j+i,customFieldContent,squareSize*0.8,squareSize*0.8,activeSection[i+j],nil,false,activeSection[i+j])
                    scrollEle:AddLabel(55+j*(squareSize+25),squareSize+30,activeSection[i+j],squareSize,squareSize+8,18,"center",false,true)
                end
            end
        elseif section == "Images" then
            for j=0, tabCount-1, 1 do
                if activeSection[i+j] then
                    scrollEle:AddImage(j*(squareSize+25)+5,30,activeSection[i+j],squareSize,squareSize,spriteTypes[spriteInd],imageColor and imageColor or nil,imageHue)
                    scrollEle:AddButton(j*(squareSize+25)+5,30,tostring(j+i),"",squareSize,squareSize,"[FFFF00]"..activeSection[i+j].."[-]",nil,false,"Title")
                end
            end
            
        end
        scrollWindow:Add(scrollEle)
    end
    return scrollWindow
end

-- Button definitions
function EleWinButtonResponse(user,buttonId,fieldData)
    sectName = SECTIONS[sectIndex]
    playDuration = fieldData.durationField
    customFieldContent = fieldData.customPlayField -- Maintains field content
    -- user:SystemMessage("\n[AAEE55]sectIndex: "..sectIndex.."\nbuttonId: "..tostring(buttonId).."\nsectName: "..sectName) --debug
    if buttonId == "leftA" then
        sectIndex = (sectIndex - 2) % #SECTIONS + 1
    elseif buttonId == "rightA" then
        sectIndex = sectIndex % #SECTIONS + 1
    elseif buttonId == "-Col" then
        if tabCount > 1 then
            tabCount = tabCount-1
            mWidth, mHeight = tabCount*(squareSize+25)+25, rowCount*squareSize+140
        end
    elseif buttonId == "+Col" then
        tabCount = tabCount+1
        mWidth, mHeight = tabCount*(squareSize+25)+25, rowCount*squareSize+140
    elseif buttonId == "-Row" then
        if rowCount > 1 then
            rowCount = rowCount-1
            mWidth, mHeight = tabCount*(squareSize+25)+25, rowCount*squareSize+140
        end
    elseif buttonId == "+Row" then
        rowCount = rowCount+1
        mWidth, mHeight = tabCount*(squareSize+25)+25, rowCount*squareSize+140
    elseif (buttonId == "customPlayButton") and fieldData.customPlayField and fieldData.customPlayField ~= "" then
        if sectIndex == 1 then
            if not targPerformer:PlayAnimation(customFieldContent) then user:SystemMessage("[DD0022]Inconceivable![-]") end
        elseif sectIndex == 2 then
            if not PlayObjectSoundLim(targPerformer,customFieldContent,false,playDuration) then user:SystemMessage("[DD0022]Inconceivable![-]") end
        elseif sectIndex == 3 then
            if not PlayEffectLim(targPerformer,customFieldContent,playDuration) then user:SystemMessage("[DD0022]Inconceivable![-]") end
        elseif sectIndex == 4 then
            -- user:SystemMessage("[88FF44]This only functions with the effects sections.[-]")
        elseif sectIndex == 5 then
        end
    elseif (buttonId == "customStopButton") and fieldData.customPlayField and fieldData.customPlayField ~= "" then
        if sectIndex == 1 then
            -- user:SystemMessage("[DD7744]Only visual and sound effects are stopped this way.[-]")
        elseif sectIndex == 2 then
            if not targPerformer:StopObjectSound(customFieldContent) then user:SystemMessage("[DD7744]Inconceivable![-]") end
        elseif sectIndex == 3 then 
            if not targPerformer:StopEffect(customFieldContent) then user:SystemMessage("[DD7744]Inconceivable![-]") end
        elseif sectIndex == 4 then
            customFieldContent = ""
        else
            user:SystemMessage("[88FF44]This only functions with the effects sections.[-]")
        end
    elseif  tonumber(buttonId) then
        if tonumber(buttonId) < 0 then
            sectIndex = -tonumber(buttonId)
        else
            local effName = AllElements[sectName][tonumber(buttonId)]
            -- print("Element:",effName) -- debug
            if sectIndex == 1 then 
                if not targPerformer:PlayAnimation(effName) then user:SystemMessage("[DD0022]Inconceivable![-]") end
            elseif sectIndex == 2 then
                if not PlayObjectSoundLim(targPerformer,effName,false,playDuration) then user:SystemMessage("[DD0022]Inconceivable![-]") end
            elseif sectIndex == 3 then
                if not PlayEffectLim(targPerformer,effName,playDuration) then user:SystemMessage("[DD0022]Inconceivable![-]") end
            -- elseif sectIndex == 4 then
            elseif sectIndex == 5 then
                if buttonId == "0" then
                    spriteInd = (spriteInd % #spriteTypes) + 1
                    if lastImage then ShowImageWindow(effName) end
                else
                    ShowImageWindow(effName)
                end
            end
            if effName then customFieldContent = effName end
        end
    elseif buttonId == "imageHueButton" then
        imageHue = tonumber(fieldData.imageHueField) or 0
        ShowImageWindow()
    elseif buttonId == "imageColorButton" then
        if fieldData.imageColorField ~= fieldData.imageColorField:match("%x%x%x%x%x%x") then -- Validate color format.
            user:SystemMessage("[BBBBBB]Invalid color input.[-]\nRequired format: RRGGBB")
        else
            imageColor = fieldData.imageColorField:match("%x%x%x%x%x%x") -- Enforcing pattern, just in case.
            user:SystemMessage("Image color set to ["..(imageColor).."]"..imageColor.."[-]")
            ShowImageWindow()
        end
    elseif buttonId == "targetButton" then
        this:RequestClientTargetGameObj(this,"TargetPlayTarget")
        return
    else
        return
    end
    EleWindow()
end
--------------------------------
--------------------------------
--------------------------------
local imgW, imgH = 255, 255
RegisterEventHandler(EventType.DynamicWindowResponse,"ImageWindow",
    function(user,buttonId,fieldData)
        if not buttonId or buttonId == "" then
            lastImage = nil
            return
        else
            if buttonId == "deflateButton" then
                if (imgW > 25 and imgH > 10) then imgW, imgH = imgW-25, imgH-25 end
            elseif buttonId == "inflateButton" then
                if (imgW < 1505 and imgH < 1505) then imgW, imgH = imgW+25, imgH+25 end
            elseif buttonId == "-w" then
                if imgW > 25 then imgW = imgW-25 end
            elseif buttonId == "+w" then
                if imgW < 1505 then imgW = imgW+25 end
            elseif buttonId == "-h" then
                if imgH > 25 then imgH = imgH-25 end
            elseif buttonId == "+h" then
                if imgH < 1505 then imgH = imgH+25 end
            end
            ShowImageWindow()
        end
    end
)

function ShowImageWindow(imageName)
    lastImage = imageName or lastImage
    if not lastImage then return end
    local imgWin = DynamicWindow("ImageWindow",tostring(lastImage).." - "..spriteTypes[spriteInd],imgW,imgH+100,200,500,"TransparentDraggable")
    -- Top bar
    imgWin:AddImage(0,0,"CraftingTabDivider",imgW,50,"Sliced","775533")
    imgWin:AddLabel(75,15,lastImage,0,0,22,"Left",false,true)
    imgWin:AddButton(5,5,"deflateButton","[000000]-[-]",30,30,"Deflate\n"..imgW.."/"..imgH,nil,false,"PageCircle") --Increase size
    imgWin:AddButton(35,5,"inflateButton","[000000]+[-]",30,30,"Inflate\n"..imgW.."/"..imgH,nil,false,"PageCircle") --Decrease size
    imgWin:AddButton(0,-16,"-w","",20,20,"w-25\n"..imgW..","..imgH,nil,false,"Minus")
    imgWin:AddButton(18,-16,"+w","",20,20,"w+25\n"..imgW..","..imgH,nil,false,"Plus")
    imgWin:AddButton(-20,-8,"-h","",20,20,"h-25\n"..imgW..","..imgH,nil,false,"Minus")
    imgWin:AddButton(-20,10,"+h","",20,20,"h+25\n"..imgW..","..imgH,nil,false,"Plus")
    imgWin:AddButton(imgW-19,1,"","",18,18,"Close image",nil,true,"CloseSquare") --Close button
    -- Image area
    imgWin:AddImage(-10,40,"CraftingItemsFrame",imgW+20,imgH+20,"Sliced") --Frame
    imgWin:AddImage(0,50,lastImage,imgW,imgH,spriteTypes[spriteInd],imageColor,imageHue)
    
    this:OpenDynamicWindow(imgWin)
end
--------------------------------
--------------------------------
--------------------------------

-- Create the /command entry





    ---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
-- PlayObjectSound with a duration limit (as the native duration argument doesn't actually work) [JBir]
-- @param Object user
-- @param String sound (name of sound effect), 
-- @param Boolean generalizer (usually false),
-- @param Number duration
-- @return Boolean True if PlayObjectSound returns True.
function PlayObjectSoundLim(user,sound,generalizer,duration)
	local playSuccess = user:PlayObjectSound(sound,generalizer)
	CallFunctionDelayed(TimeSpan.FromSeconds(duration), function()
		user:StopObjectSound(sound,false)
	end)
	return playSuccess
end
---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
-- PlayEffect with a duration limit (as the native duration argument doesn't actually work) [JBir]
-- @param Object user
-- @param String veffect (name of visual effect), 
-- @param Number duration
-- @return Boolean True if PlayEffect returns True.
function PlayEffectLim(user,veffect,duration)
	local playSuccess = user:PlayEffect(veffect)
	CallFunctionDelayed(TimeSpan.FromSeconds(duration), function()
		user:StopEffect(veffect)
	end)
	return playSuccess
end
