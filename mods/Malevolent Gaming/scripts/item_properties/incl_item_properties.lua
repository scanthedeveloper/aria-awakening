-- this table holds all the item property system tables
ItemProperties = {}
-- this table contains the item property string color choices
ItemProperties.TextColors =
{
	PropertyColor = "[CCCC66]",
	PropertyValueColor ="[6699CC]",
	PropertyValueColorNegative="[CC3333]",
}
-- this is a blank template to stamp on a item it has all the current variables needed.
ItemProperties.PropertyTable = {
	AccuracyPlus 		= 0,
	AccuracyTimes		= 0,
	AgilityPlus 		= 0,
	AgilityTimes		= 0,
	AttackPlus 			= 0,
	AttackTimes			= 0,	
	PowerPlus			= 0,
	PowerTimes			= 0,	
	ForcePlus			= 0,
	ForceTimes			= 0,	
	ConstitutionPlus	= 0,
	ConstitutionTimes 	= 0,		
	DefensePlus			= 0,
	DefenseTimes		= 0,	
	EvasionPlus			= 0,
	EvasionTimes		= 0,	
	IntelligencePlus	= 0,
	IntelligenceTimes	= 0,	
	StrengthPlus		= 0,
	StrengthTimes		= 0,	
	WillPlus			= 0,
	WillTimes			= 0,	
	WisdomPlus			= 0,
	WisdomTimes			= 0,	
	MaxHealthPlus		= 0,
	MaxHealthTimes		= 0,	
	MaxManaPlus			= 0,
	MaxManaTimes        = 0,	
	MaxStaminaPlus		= 0,
	MaxStaminaTimes		= 0,	
	MaxVitalityPlus		= 0,
	MaxVitalityTimes	= 0,	
	HealthRegenPlus		= 0,
	HealthRegenTimes	= 0,	
	ManaRegenPlus		= 0,
	ManaRegenTimes		= 0,	
	StaminaRegenPlus	= 0,
	StaminaRegenTimes	= 0,	
	VitalityRegenPlus	= 0,
	VitalityRegenTimes	= 0,	
	MoveSpeedPlus		= 0,
	MoveSpeedTimes 		= 0,		
}
-- this is the string name value of the property that is displayed in the moust over properties
ItemProperties.PropertyBonusStrings = {
	AccuracyPlus 		= "Accuracy",
	AccuracyTimes	 	= "Accuracy",
	AgilityPlus 		= "Agility",
	AgilityTimes		= "Agility",
	AttackPlus 			= "Attack",
	AttackTimes			= "Attack",	
	PowerPlus			= "Power",
	PowerTimes			= "Power",	
	ForcePlus			= "Force",	
	ForceTimes			= "Force",
	ConstitutionPlus	= "Constitution",
	ConstitutionTimes	= "Constitution",		
	DefensePlus			= "Defense",
	DefenseTimes		= "Defense",	
	EvasionPlus			= "Evasion",
	EvasionTimes		= "Evasion",	
	IntelligencePlus	= "Intelligence",
	IntelligenceTimes	= "Intelligence",	
	StrengthPlus		= "Strength",
	StrengthTimes		= "Strength",	
	WillPlus			= "Will",
	WillTimes			= "Will",	
	WisdomPlus			= "Wisdom",
	WisdomTimes			= "Wisdom",	
	MaxHealthPlus		= "Health",
	MaxHealthTimes		= "Health",	
	MaxManaPlus			= "Mana",
	MaxManaTimes		= "Mana",	
	MaxStaminaPlus		= "Stamina",
	MaxStaminaTimes		= "Stamina",	
	MaxVitalityPlus		= "Vitality",
	MaxVitalityTimes	= "Vitality",	
	HealthRegenPlus		= "Health Regen",
	HealthRegenTimes	= "Health Regen",	
	ManaRegenPlus		= "Mana Regen",	
	ManaRegenTimes		= "Mana Regen",
	StaminaRegenPlus	= "Stamina Regen",
	StaminaRegenTimes	= "Stamina Regen",	
	VitalityRegenPlus	= "Vitality Regen",	
	VitalityRegenTimes	= "Vitality Regen",
	MoveSpeedPlus		= "Movement Speed",
	MoveSpeedTimes		= "Movement Speed",		
}

--- this updates the tooltip variable for the current object
-- @param object - in game object to setup basic item properties
ItemProperties.UpdateItemTooltip = function(object)
	local propertyValues = object:GetObjVar("MagicProperties")
	if(propertyValues == nil) then return end

	local MOVE_SPEED_DIVISOR = 100
	
	if(propertyValues["AccuracyPlus"] ~= 0) then
		SetTooltipEntry(object,"AccuracyPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["AccuracyPlus"].. "[-]" .. getIf(propertyValues["AccuracyPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["AccuracyPlus"]>0," +", " ")  ..  propertyValues["AccuracyPlus"].."[-]",210)
	end
	if(propertyValues["AccuracyTimes"] ~= 0) then
		SetTooltipEntry(object,"AccuracyTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["AccuracyTimes"].. "[-]" .. getIf(propertyValues["AccuracyTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["AccuracyTimes"]>0," x", " x") ..  propertyValues["AccuracyTimes"].."[-]",209)
	end
	if(propertyValues["AgilityPlus"] ~= 0) then
		SetTooltipEntry(object,"AgilityPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["AgilityPlus"].. "[-]" .. getIf(propertyValues["AgilityPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["AgilityPlus"]>0," +", " ") .. propertyValues["AgilityPlus"].."[-]",200)
	end	
	if(propertyValues["AgilityTimes"] ~= 0) then
		local bonus = propertyValues["AgilityTimes"] * 100
		local bonus_string = bonus.."%"		
		SetTooltipEntry(object,"AgilityTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["AgilityTimes"].. "[-]" .. getIf(propertyValues["AgilityTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["AgilityTimes"]>0," x", " x").. bonus_string.."[-]",199)
	end
	if(propertyValues["AttackPlus"] ~= 0) then
		SetTooltipEntry(object,"AttackPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["AttackPlus"].. "[-]" .. getIf(propertyValues["AttackPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["AttackPlus"]>0," +", " ")  ..  propertyValues["AttackPlus"].."[-]",190)
	end
	if(propertyValues["AttackTimes"] ~= 0) then
		SetTooltipEntry(object,"AttackTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["AttackTimes"].. "[-]" .. getIf(propertyValues["AttackTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["AttackTimes"]>0," x", " x") ..  propertyValues["AttackTimes"].."[-]",189)
	end	
	if(propertyValues["PowerPlus"] ~= 0) then
		SetTooltipEntry(object,"PowerPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["PowerPlus"].. "[-]" .. getIf(propertyValues["PowerPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["PowerPlus"]>0," +", " ")  ..  propertyValues["PowerPlus"].."[-]",180)
	end
	if(propertyValues["PowerTimes"] ~= 0) then
		SetTooltipEntry(object,"PowerTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["PowerTimes"].. "[-]" .. getIf(propertyValues["PowerTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["PowerTimes"]>0," x", " x") ..  propertyValues["PowerTimes"].."[-]",179)
	end	
	if(propertyValues["ForcePlus"] ~= 0) then
		SetTooltipEntry(object,"ForcePlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["ForcePlus"].. "[-]" .. getIf(propertyValues["ForcePlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["ForcePlus"]>0," +", " ")  ..  propertyValues["ForcePlus"].."[-]",170)
	end
	if(propertyValues["ForceTimes"] ~= 0) then
		SetTooltipEntry(object,"ForceTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["ForceTimes"].. "[-]" .. getIf(propertyValues["ForceTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["ForceTimes"]>0," x", " x") ..  propertyValues["ForceTimes"].."[-]",169)
	end	
	if(propertyValues["ConstitutionPlus"] ~= 0) then
		SetTooltipEntry(object,"ConstitutionPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["ConstitutionPlus"].. "[-]" .. getIf(propertyValues["ConstitutionPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["ConstitutionPlus"]>0," +", " ")  ..  propertyValues["ConstitutionPlus"].."[-]",160)
	end
	if(propertyValues["ConstitutionTimes"] ~= 0) then
		SetTooltipEntry(object,"ConstitutionTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["ConstitutionTimes"].. "[-]" .. getIf(propertyValues["ConstitutionTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["ConstitutionTimes"]>0," x", " x") ..  propertyValues["ConstitutionTimes"].."[-]",159)
	end	
	if(propertyValues["DefensePlus"] ~= 0) then
		SetTooltipEntry(object,"DefensePlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["DefensePlus"].. "[-]" .. getIf(propertyValues["DefensePlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["DefensePlus"]>0," +", " ")  ..  propertyValues["DefensePlus"].."[-]",140)
	end
	if(propertyValues["DefenseTimes"] ~= 0) then
		SetTooltipEntry(object,"DefenseTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["DefenseTimes"].. "[-]" .. getIf(propertyValues["DefenseTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["DefenseTimes"]>0," x", " x") ..  propertyValues["DefenseTimes"].."[-]",139)
	end	
	if(propertyValues["EvasionPlus"] ~= 0) then
		SetTooltipEntry(object,"EvasionPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["EvasionPlus"].. "[-]" .. getIf(propertyValues["EvasionPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["EvasionPlus"]>0," +", " ")  ..  propertyValues["EvasionPlus"].."[-]",130)
	end
	if(propertyValues["EvasionTimes"] ~= 0) then
		SetTooltipEntry(object,"EvasionTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["EvasionTimes"].. "[-]" .. getIf(propertyValues["EvasionTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["EvasionTimes"]>0," x", " x") ..  propertyValues["EvasionTimes"].."[-]",129)
	end	
	if(propertyValues["IntelligencePlus"] ~= 0) then
		SetTooltipEntry(object,"IntelligencePlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["IntelligencePlus"].. "[-]" .. getIf(propertyValues["IntelligencePlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["IntelligencePlus"]>0," +", " ")  ..  propertyValues["IntelligencePlus"].."[-]",120)
	end
	if(propertyValues["IntelligenceTimes"] ~= 0) then
		local bonus = propertyValues["IntelligenceTimes"] * 100
		local bonus_string = bonus.."%"			
		SetTooltipEntry(object,"IntelligenceTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["IntelligenceTimes"].. "[-]" .. getIf(propertyValues["IntelligenceTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["IntelligenceTimes"]>0," x", " x").. bonus_string.."[-]",119)
	end	
	if(propertyValues["StrengthPlus"] ~= 0) then
		SetTooltipEntry(object,"StrengthPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["StrengthPlus"].. "[-]" .. getIf(propertyValues["StrengthPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["StrengthPlus"]>0," +", " ")  ..  propertyValues["StrengthPlus"].."[-]",110)
	end
	if(propertyValues["StrengthTimes"] ~= 0) then
		local bonus = propertyValues["StrengthTimes"] * 100
		local bonus_string = bonus.."%" 
		SetTooltipEntry(object,"StrengthTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["StrengthTimes"].. "[-]" .. getIf(propertyValues["StrengthTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["StrengthTimes"]>0," x", " x").. bonus_string.."[-]",109)
	end	
	if(propertyValues["WillPlus"] ~= 0) then
		SetTooltipEntry(object,"WillPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["WillPlus"].. "[-]" .. getIf(propertyValues["WillPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["WillPlus"]>0," +", " ")  ..  propertyValues["WillPlus"].."[-]",100)
	end
	if(propertyValues["WillTimes"] ~= 0) then
		SetTooltipEntry(object,"WillTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["WillTimes"].. "[-]" .. getIf(propertyValues["WillTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["WillTimes"]>0," x", " x") ..  propertyValues["WillTimes"].."[-]",99)
	end	
	if(propertyValues["WisdomPlus"] ~= 0) then
		SetTooltipEntry(object,"WisdomPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["WisdomPlus"].. "[-]" .. getIf(propertyValues["WisdomPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["WisdomPlus"]>0," +", " ")  ..  propertyValues["WisdomPlus"].."[-]",90)
	end
	if(propertyValues["WisdomTimes"] ~= 0) then
		SetTooltipEntry(object,"WisdomTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["WisdomTimes"].. "[-]" .. getIf(propertyValues["WisdomTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["WisdomTimes"]>0," x", " x") ..  propertyValues["WisdomTimes"].."[-]",89)
	end	
	if(propertyValues["MaxHealthPlus"] ~= 0) then
		SetTooltipEntry(object,"MaxHealthPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["MaxHealthPlus"].. "[-]" .. getIf(propertyValues["MaxHealthPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["MaxHealthPlus"]>0," +", " ")  ..  propertyValues["MaxHealthPlus"].."[-]",80)
	end
	if(propertyValues["MaxHealthTimes"] ~= 0) then
		SetTooltipEntry(object,"MaxHealthTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["MaxHealthTimes"].. "[-]" .. getIf(propertyValues["MaxHealthTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["MaxHealthTimes"]>0," x", " x") ..  propertyValues["MaxHealthTimes"].."[-]",79)
	end	
	if(propertyValues["MaxManaPlus"] ~= 0) then
		SetTooltipEntry(object,"MaxManaPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["MaxManaPlus"].. "[-]" .. getIf(propertyValues["MaxManaPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["MaxManaPlus"]>0," +", " ")  ..  propertyValues["MaxManaPlus"].."[-]",70)
	end
	if(propertyValues["MaxManaTimes"] ~= 0) then
		SetTooltipEntry(object,"MaxManaTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["MaxManaTimes"].. "[-]" .. getIf(propertyValues["MaxManaTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["MaxManaTimes"]>0," x", " x") ..  propertyValues["MaxManaTimes"].."[-]",69)
	end	
	if(propertyValues["MaxStaminaPlus"] ~= 0) then
		SetTooltipEntry(object,"MaxStaminaPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["MaxStaminaPlus"].. "[-]" .. getIf(propertyValues["MaxStaminaPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["MaxStaminaPlus"]>0," +", " ")  ..  propertyValues["MaxStaminaPlus"].."[-]",60)
	end
	if(propertyValues["MaxStaminaTimes"] ~= 0) then
		SetTooltipEntry(object,"MaxStaminaTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["MaxStaminaTimes"].. "[-]" .. getIf(propertyValues["MaxStaminaTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["MaxStaminaTimes"]>0," x", " x") ..  propertyValues["MaxStaminaTimes"].."[-]",59)
	end	
	if(propertyValues["MaxVitalityPlus"] ~= 0) then
		SetTooltipEntry(object,"MaxVitalityPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["MaxVitalityPlus"].. "[-]" .. getIf(propertyValues["MaxVitalityPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["MaxVitalityPlus"]>0," +", " ")  ..  propertyValues["MaxVitalityPlus"].."[-]",50)
	end
	if(propertyValues["MaxVitalityTimes"] ~= 0) then
		SetTooltipEntry(object,"MaxVitalityTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["MaxVitalityTimes"].. "[-]" .. getIf(propertyValues["MaxVitalityTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["MaxVitalityTimes"]>0," x", " x") ..  propertyValues["MaxVitalityTimes"].."[-]",49)
	end	
	if(propertyValues["HealthRegenPlus"] ~= 0) then
		SetTooltipEntry(object,"HealthRegenPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["HealthRegenPlus"].. "[-]" .. getIf(propertyValues["HealthRegenPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["HealthRegenPlus"]>0," +", " ")  ..  propertyValues["HealthRegenPlus"].."[-]",40)
	end
	if(propertyValues["HealthRegenTimes"] ~= 0) then
		SetTooltipEntry(object,"HealthRegenTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["HealthRegenTimes"].. "[-]" .. getIf(propertyValues["HealthRegenTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["HealthRegenTimes"]>0," x", " x") ..  propertyValues["HealthRegenTimes"].."[-]",39)
	end	
	if(propertyValues["ManaRegenPlus"] ~= 0) then
		SetTooltipEntry(object,"ManaRegenPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["ManaRegenPlus"].. "[-]" .. getIf(propertyValues["ManaRegenPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["ManaRegenPlus"]>0," +", " ")  ..  propertyValues["ManaRegenPlus"].."[-]",30)
	end
	if(propertyValues["ManaRegenTimes"] ~= 0) then
		SetTooltipEntry(object,"ManaRegenTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["ManaRegenTimes"].. "[-]" .. getIf(propertyValues["ManaRegenTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["ManaRegenTimes"]>0," x", " x") ..  propertyValues["ManaRegenTimes"].."[-]",29)
	end
	if(propertyValues["StaminaRegenPlus"] ~= 0) then
		SetTooltipEntry(object,"StaminaRegenPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["StaminaRegenPlus"].. "[-]" .. getIf(propertyValues["StaminaRegenPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["StaminaRegenPlus"]>0," +", " ")  ..  propertyValues["StaminaRegenPlus"].."[-]",28)
	end
	if(propertyValues["StaminaRegenTimes"] ~= 0) then
		SetTooltipEntry(object,"StaminaRegenTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["StaminaRegenTimes"].. "[-]" .. getIf(propertyValues["StaminaRegenTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["StaminaRegenTimes"]>0," x", " x") ..  propertyValues["StaminaRegenTimes"].."[-]",27)
	end		
	if(propertyValues["VitalityRegenPlus"] ~= 0) then
		SetTooltipEntry(object,"VitalityRegenPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["VitalityRegenPlus"].. "[-]" .. getIf(propertyValues["VitalityRegenPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["VitalityRegenPlus"]>0," +", " ")  ..  propertyValues["VitalityRegenPlus"].."[-]",25)
	end
	if(propertyValues["VitalityRegenTimes"] ~= 0) then
		SetTooltipEntry(object,"VitalityRegenTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["VitalityRegenTimes"].. "[-]" .. getIf(propertyValues["VitalityRegenTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["VitalityRegenTimes"]>0," x", " x") ..  propertyValues["VitalityRegenTimes"].."[-]",24)
	end	
	if(propertyValues["MoveSpeedPlus"] ~= 0) then
		SetTooltipEntry(object,"MoveSpeedPlus",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["MoveSpeedPlus"].. "[-]" .. getIf(propertyValues["MoveSpeedPlus"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["MoveSpeedPlus"]>0," +", " ") ..  propertyValues["MoveSpeedPlus"] * MOVE_SPEED_DIVISOR .. "%".."[-]",20)
	end
	if(propertyValues["MoveSpeedTimes"] ~= 0) then
		SetTooltipEntry(object,"MoveSpeedTimes",ItemProperties.TextColors.PropertyColor..ItemProperties.PropertyBonusStrings["MoveSpeedTimes"].. "[-]" .. getIf(propertyValues["MoveSpeedTimes"]>0,ItemProperties.TextColors.PropertyValueColor,ItemProperties.TextColors.PropertyValueColorNegative) .. getIf(propertyValues["MoveSpeedTimes"]>0," x", " x")..  propertyValues["MoveSpeedTimes"] * MOVE_SPEED_DIVISOR .. "%".."[-]",19)
	end	
	if(object:HasObjVar("LoreString")) then
		SetTooltipEntry(object,"ItemLoreSpace","",-1001)
		SetTooltipEntry(object,"ItemLore",object:GetObjVar("LoreString").. "[-]",-1000)
	end
end

--- a simple condiontal help function
-- @param mBool - true or false
-- @param v1 - true value to return
-- @param v2 - false value to return
function getIf(mBool, v1, v2)
    if mBool then 
        return v1
    else
        return v2
    end
end

