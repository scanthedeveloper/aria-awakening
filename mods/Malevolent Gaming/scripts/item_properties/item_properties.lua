--- handles the loading of the item properties when saved with the /properties command editor
-- @param item - the item thats currently loading, update
function HandleInitializer(item)
	local properties = item:GetObjVar("MagicProperties")
	--if we dont have a properties table then create one
	if(properties == nil) then
		properties = ItemProperties.PropertyTable
	end
	if(initializer ~= nil) then
		for k,v in pairs(initializer) do
			if(initializer[k] ~= nil) then
				properties[k] = initializer[k]
			end		
		end
		ItemProperties.UpdateItemTooltip(this)	
	end
	item:SetObjVar("MagicProperties",properties)
end

----------------------------------------------------------
--EVENTTYPE - ModuleAttached 
----------------------------------------------------------
RegisterEventHandler(EventType.ModuleAttached,GetCurrentModule(),
function()
	if not(this:IsPlayer()) then
		if not(this:HasObjVar("MagicProperties")) then
			this:SetObjVar("MagicProperties",ItemProperties.PropertyTable)
			HandleInitializer(this)
			ItemProperties.UpdateItemTooltip(this)
		end
	end
end)







