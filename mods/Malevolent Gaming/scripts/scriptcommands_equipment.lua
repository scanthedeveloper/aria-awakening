require 'default:scriptcommands'
require 'default:scriptcommands_mortal'
require 'default:scriptcommands_immortal'
require 'default:scriptcommands_demigod'
require 'default:scriptcommands_immortal'
require 'default:scriptcommands_god'


require 'editors.equipment_editor'
--- this opens up the equipment editor
function EditItem()
	this:RequestClientTargetGameObj(this, "requestTargetObject")
end

RegisterEventHandler(EventType.ClientTargetGameObjResponse,"requestTargetObject",function(targetObj)
    if(targetObj == nil) then return end
    -- is this a valid piece of equipment such as armor, shields or weapons then display the editor
    if(targetObj:HasObjVar("ArmorType") or targetObj:HasObjVar("ShieldType") or targetObj:HasObjVar("WeaponType")) then
    	if not (targetObj:HasModule("editors.equipment_editor")) then
    		targetObj:AddModule("editors.equipment_editor")
    	end
    	targetObj:SendMessage("OpenForUser",this)
    	this:CloseDynamicWindow("ColorEditorDynamicResponse")
    	this:CloseDynamicWindow("EssenceEditorDynamicResponse")
	else
		this:SystemMessage("[FF7700]This is not a valid item to edit, try targeting a piece of equipment")
		EditItem()
	end
end)

RegisterCommand{ Command="edititem", AccessLevel = AccessLevel.Mortal, Func=EditItem, Usage="", Desc="opens up a item editor for that type of item" }
