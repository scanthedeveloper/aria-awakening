--- required script files
require 'default:scriptcommands'
require 'editors.dialog_editor'

--- this opens up the dialog editor
function CreateDialog()
    this:RequestClientTargetGameObj(this, "requestTargetNpc")
end

--- handles the targeting of the object, and applys the dialog editor script to it
RegisterEventHandler(EventType.ClientTargetGameObjResponse,"requestTargetNpc",function(targetObj)
    if(targetObj == nil) then return end
    -- is this a valid piece of equipment such as armor, shields,jewelry or weapons then display the editor
    if(targetObj:GetCreationTemplateId() == "dialog_test_guy") then
        if not (targetObj:HasModule("editors.dialog_editor")) then
            targetObj:AddModule("editors.dialog_editor")
        end
        --ShowDialogEditor(user,"Greeting",false)
        -- if this is a new object, make sure we remove the pickers for the old object
    else
        this:SystemMessage("[FF7700]create a dialog_test_guy to work with")
        EditItem()
    end
end)

--- the command to create dialogs 
RegisterCommand{ Command="createdialog", AccessLevel = AccessLevel.God, Func=CreateDialog, Usage="", Desc="opens up a dialog editor" }


