--- fires when the player equips and item and then loads mods to the player
-- @param item - the item your checking for item properties
function OnItemEquip(item)
	local propertyValues = item:GetObjVar("MagicProperties")
	if(propertyValues == nil) then return end
	--fix health/mana/stamina values when equipping new items.
	for k,v in pairs(propertyValues) do
		SetMobileMod(this,k,item.Id..k,v)
		if(k=="ConstitutionPlus" or "ConstitutionTimes" or "MaxHealthPlus" or "MaxHealthTimes") then
			CallFunctionDelayed(TimeSpan.FromMilliseconds(50),function() SetCurHealth(this,GetCurHealth(this)+v) end)
		end
		if(k=="IntelligencePlus" or "IntelligenceTimes" or "MaxManaPlus" or "MaxManaTimes") then
			CallFunctionDelayed(TimeSpan.FromMilliseconds(50),function() SetCurMana(this,GetCurMana(this)+v) end)
		end
		if(k=="AgilityPlus" or "AgilityTimes" or "MaxStaminaPlus" or "MaxStaminaTimes") then
			CallFunctionDelayed(TimeSpan.FromMilliseconds(50),function() SetCurStamina(this,GetCurStamina(this)+v) end)
		end
	end
end

--- fires when the player unequips an item and removes mods from the player
-- @param item - the item your checking for item properties
function OnItemUnEquip(item)
	local propertyValues = item:GetObjVar("MagicProperties")
	if(propertyValues == nil) then return end
	for k,v in pairs(propertyValues) do
		SetMobileMod(this,k,item.Id..k,nil)
	end
end

--validates the current magic items worn, fires in the onload event.
function ValidateEquipmentMagicalProperties()
	local slots = {"Head","Chest","Legs","LeftHand","RightHand","Ring","Necklace"}
	for k,v in pairs(slots) do
		if(this:GetEquippedObject(v) ~= nil) then 
			local equippedObject = this:GetEquippedObject(v)
			if(equippedObject:HasObjVar("MagicProperties")) then
				OnItemEquip(equippedObject) 
			end
		end
	end
end

----------------------------------------------------------
--EVENTTYPE - ItemEquipped 
----------------------------------------------------------
RegisterEventHandler(EventType.ItemEquipped, "", 
function(item)
	if ( item ~= nil ) then
		if(item:HasObjVar("MagicProperties")) then
			OnItemEquip(item)
		end
	end
end)

----------------------------------------------------------
--EVENTTYPE - ItemUnequipped 
----------------------------------------------------------
RegisterEventHandler(EventType.ItemUnequipped, "", 
function(item)
	if ( item ~= nil ) then
		if(item:HasObjVar("MagicProperties")) then
			OnItemUnEquip(item)
		end
	end
end)

----------------------------------------------------------
--EVENTTYPE - LoadFromBackup 
----------------------------------------------------------
RegisterEventHandler(EventType.LoadedFromBackup,"",
function()
	--For ItemProperty Login Validation
	ValidateEquipmentMagicalProperties()
end)
