--this is color of the merchant title
MerchantTitleColor = "[00FF00]"
--this is the color of the merchants name
MerchantNameColor = "[FFFF55]"
--how far away players can be to interact with the npc
--also how far away the player can be, if to fare it closes the dynamic window
InteractionDistance = 5
--this is the View pulse time in seconds
ViewPulseTime = 1
--this is the dynamic window handler string
--in case you want to change it to something else
DynamicWindowResponseHandler = "MerchantDynamicWindowHandler"