StoreTypes.DUNEcurrency_exchange_store = 
{
	MerchantTitle = "-DUNE Currency Exchange Merchant-",
	CurrencyType = "CrystalizedWater",
	CurrencyName = "Crystalized Water",
	Stock =
	{
		DUNE_Gold1 =
		{
			DisplayName="1. DUNE Currency Exchange (1)",
			Template="guild_coin",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 1 Crystalized Water into 1 Guild Note.",
		},
		DUNE_Gold5 =
		{
			DisplayName="2. DUNE Currency Exchange (5)",
			Template="guild_coin_box_5",
			SellPrice=5,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 5 Crystalized Water into 5 Guild Notes.",
		},
		DUNE_Gold10 =
		{
			DisplayName="3. DUNE Currency Exchange (10)",
			Template="guild_coin_box_10",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 10 Crystalized Water into 10 Guild Notes.",
		},
		DUNE_Gold20 =
		{
			DisplayName="4. DUNE Currency Exchange (20)",
			Template="guild_coin_box_20",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 20 Crystalized Water into 20 Guild Notes.",
		},
		DUNE_Gold50 =
		{
			DisplayName="5. DUNE Currency Exchange (50)",
			Template="guild_coin_box_50",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 50 Crystalized Water into 50 Guild Notes.",
		},
		DUNE_Gold100 =
		{
			DisplayName="6. DUNE Currency Exchange (100)",
			Template="guild_coin_box_100",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 100 Crystalized Water into 100 Guild Notes.",
		},
	},
}