StoreTypes.NightbornDuke = 
{
	MerchantTitle = "-Royal Heirophant-",
	CurrencyType = "DemonBlood",
	CurrencyName = "Demon Blood",
	Stock =
	{
		NightbornGems25 =
		{
			DisplayName="Quest Reward - Demonblood",
			Template="nightborn_gem_box_25",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (25) Nightborn gems for (1) sample of Demon Blood.",
		},
	},
}