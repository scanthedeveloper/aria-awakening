StoreTypes.WarlockEssenceTrader = 
{
	MerchantTitle = "-Quest NPC-",
	CurrencyType = "NetherEssence",
	CurrencyName = "Nether Essence",
	Stock =
	{
		EssenceCrate =
		{
			DisplayName="Quest - Nether Essence",
			Template="furniture_box_rebel",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards a loot box containing a random assortment of mini artifacts, resources, cosmetics, and decorations.",
		},
	},
}