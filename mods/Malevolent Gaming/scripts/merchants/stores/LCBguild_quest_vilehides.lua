StoreTypes.LCBGuildStoreVileHides = 
{
	MerchantTitle = "-Hides Trader-",
	CurrencyType = "VileLeatherHide",
	CurrencyName = "Vile Hides",
	Stock =
	{
		LCB_Gold10 =
		{
			DisplayName="1. LCB Scrips (10)",
			Template="lcb_guild_gold_box_10",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (10) Scrips for (50) Vile Hides.",
		},
		LCB_Gold50 =
		{
			DisplayName="2. LCB Scrips (50)",
			Template="lcb_guild_gold_box_50",
			SellPrice=250,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (50) Scrips for (250) Vile Hides.",
		},
		LCB_Gold100 =
		{
			DisplayName="3. LCB Scrips (100)",
			Template="lcb_guild_gold_box_100",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (100) Scrips for (500) Vile Hides.",
		},
	},
}