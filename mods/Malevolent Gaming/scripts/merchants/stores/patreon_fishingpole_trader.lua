StoreTypes.PatreonFishingPoleTrader = 
{
	MerchantTitle = "-Patreon Fishing Pole Trader-",
	CurrencyType = "BrokenGodFishingpole",
	CurrencyName = "Patreon God Fishing Poles",
	Stock =
	{
        GodFishingPole =
		{
			DisplayName="Tool - God Fishingpole",
			Template="SCAN_harvest_GOD_fishingpole2",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Tool-\n"..ToolTipTextColor.."A pre-enchanted fishing pole that provides, strength, speed, and weight bonuses.",
        },
	},
}