StoreTypes.Tailor = 
{
	MerchantTitle = "-Clothing Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Coins",
	Stock =
	{	
		ShortSleeveShirt =
		{
			DisplayName="Short Sleeve Shirt",
			Template="clothing_short_sleeve_shirt_chest",
			SellPrice=36,
			BuyPrice=12,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Short Sleeve Shirt-\n"..ToolTipTextColor.."A common piece of clothing.",
		},
		LongSleeveShirt =
		{
			DisplayName="Long Sleeve Shirt",
			Template="clothing_long_sleeve_shirt_chest",
			SellPrice=36,
			BuyPrice=12,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Short Sleeve Shirt-\n"..ToolTipTextColor.."A common piece of clothing.",
		},
		Shorts =
		{
			DisplayName="Shorts",
			Template="clothing_shorts_legs",
			SellPrice=30,
			BuyPrice=10,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Short Sleeve Shirt-\n"..ToolTipTextColor.."A common piece of clothing.",
		},
	},	
}