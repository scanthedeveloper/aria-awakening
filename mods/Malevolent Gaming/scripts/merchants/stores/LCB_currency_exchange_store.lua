StoreTypes.LCBcurrency_exchange_store = 
{
	MerchantTitle = "-LCB Currency Exchange Merchant-",
	CurrencyType = "Venom",
	CurrencyName = "LCB Scrib",
	Stock =
	{
		LCB_Gold1 =
		{
			DisplayName="1. LCB Currency Exchange (1)",
			Template="guild_coin",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 1 Scribs into 1 Guild Note.",
		},
		LCB_Gold5 =
		{
			DisplayName="2. LCB Currency Exchange (5)",
			Template="guild_coin_box_5",
			SellPrice=5,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 5 Scribs into 5 Guild Notes.",
		},
		LCB_Gold10 =
		{
			DisplayName="3. LCB Currency Exchange (10)",
			Template="guild_coin_box_10",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 10 Scribs into 10 Guild Notes.",
		},
		LCB_Gold20 =
		{
			DisplayName="4. LCB Currency Exchange (20)",
			Template="guild_coin_box_20",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 20 Scribs into 20 Guild Notes.",
		},
		LCB_Gold50 =
		{
			DisplayName="5. LCB Currency Exchange (50)",
			Template="guild_coin_box_50",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 50 Scribs into 50 Guild Notes.",
		},
		LCB_Gold100 =
		{
			DisplayName="6. LCB Currency Exchange (100)",
			Template="guild_coin_box_100",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 100 Scribs into 100 Guild Notes.",
		},
	},
}