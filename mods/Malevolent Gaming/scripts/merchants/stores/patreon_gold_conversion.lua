StoreTypes.PatreonGemCurrencyExchange = 
{
	MerchantTitle = "-Patreon Currency Exchange-",
	CurrencyType = "PatronGem",
	CurrencyName = "Patron Gems",
	Stock =
	{
		--[[Dagger =
		{
			DisplayName="Dagger",
			Template="weapon_dagger",
			SellPrice=36,
			BuyPrice=0,
			MaxStock=20,
			CanSell = false,
			CanBuy = false,
			Tooltip = ToolTipHeaderColor.."-Dagger-\n"..ToolTipTextColor.."A sharp quick weapon that requires the peircing skill to be effective.",
			Conditions = 
			{
				{Type="ObjVarEqual",Value="TestDaggerReq|test"},
				{Type="ObjVarGreaterThan",Value="BiggerThan|25"},
				{Type="ObjVarLessThan",Value="LessThan|25"},
				{Type="SkillGreaterThan",Value="Fabrication|50"},
				{Type="SkillLessThan",Value="Healing|50"},
			},
		},]]--
        Patron_Gold1 =
		{
			DisplayName="1. Patron Gem Exchange (1)",
			Template="coin_box_1",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 1 Patron Gems into 250 Gold.",
		},
		Patron_Gold5 =
		{
			DisplayName="2. Patron Gem Exchange (5)",
			Template="coin_box_5",
			SellPrice=5,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 5 Patron Gems into 1250 Gold.",
		},
		Patron_Gold10 =
		{
			DisplayName="3. Patron Gem Exchange (10)",
			Template="coin_box_10",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 10 Patron Gems into 2500 Gold.",
		},
		Patron_Gold50 =
		{
			DisplayName="4. Patron Gem Exchange (50)",
			Template="coin_box_50",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 20 Patron Gems into 1 Platinum 2500 Gold.",
		},
		Patron_Gold100 =
		{
			DisplayName="5. Patron Gem Exchange (100)",
			Template="coin_box_100",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 50 Patron Gems into 2 Platinum 5000 Gold.",
		},
		Patron_Gold500 =
		{
			DisplayName="6. Patron Gem Exchange (500)",
			Template="coin_box_500",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 100 Patron Gems into 12 Platinum 5000 Gold.",
		},
        Patron_Gold1000 =
		{
			DisplayName="7. Patron Gem Exchange (1000)",
			Template="coin_box_1000",
			SellPrice=1000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 100 Patron Gems into 25 Platinum.",
		},
        Patron_Gold2000 =
		{
			DisplayName="8. Patron Gem Exchange (2000)",
			Template="coin_box_2000",
			SellPrice=2000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 100 Patron Gems into 50 Platinum.",
		},
        Patron_Gold5000 =
		{
			DisplayName="9. Patron Gem Exchange (5000)",
			Template="coin_box_5000",
			SellPrice=5000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 100 Patron Gems into 125 Platinum.",
		},
        Patron_Gold10000 =
		{
			DisplayName="99. Patron Gem Exchange (10000)",
			Template="coin_box_10000",
			SellPrice=10000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 100 Patron Gems into 250 Platinum.",
		},
	},
}