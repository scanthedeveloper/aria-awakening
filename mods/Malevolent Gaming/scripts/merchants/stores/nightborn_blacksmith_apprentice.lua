StoreTypes.NightbornBlacksmithApprentice = 
{
	MerchantTitle = "-Blacksmithing Apprentice-",
	CurrencyType = "WerewolfQuestBook",
	CurrencyName = "Book of the Damned",
	Stock =
	{
		NightbornGems25 =
		{
			DisplayName="Quest Reward - Book of the Damned",
			Template="nightborn_gem_box_100",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (100) Nightborn gems for (1) Book of the Damned.",
		},
	},
}