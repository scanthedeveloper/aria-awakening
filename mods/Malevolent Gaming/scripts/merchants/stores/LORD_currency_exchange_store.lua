StoreTypes.LORDcurrency_exchange_store = 
{
	MerchantTitle = "-LORD Currency Exchange Merchant-",
	CurrencyType = "DraconicTablet",
	CurrencyName = "Draconic Tablet",
	Stock =
	{
		lord_Gold1 =
		{
			DisplayName="1. LORD Currency Exchange (1)",
			Template="guild_coin",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 1 Draconic Tablet into 1 Guild Note.",
		},
		lord_Gold5 =
		{
			DisplayName="2. LORD Currency Exchange (5)",
			Template="guild_coin_box_5",
			SellPrice=5,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 5 Draconic Tablet into 5 Guild Notes.",
		},
		lord_Gold10 =
		{
			DisplayName="3. LORD Currency Exchange (10)",
			Template="guild_coin_box_10",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 10 Draconic Tablet into 10 Guild Notes.",
		},
		lord_Gold20 =
		{
			DisplayName="4. LORD Currency Exchange (20)",
			Template="guild_coin_box_20",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 20 Draconic Tablet into 20 Guild Notes.",
		},
		lord_Gold50 =
		{
			DisplayName="5. LORD Currency Exchange (50)",
			Template="guild_coin_box_50",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 50 Draconic Tablet into 50 Guild Notes.",
		},
		lord_Gold100 =
		{
			DisplayName="6. LORD Currency Exchange (100)",
			Template="guild_coin_box_100",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 100 Draconic Tablet into 100 Guild Notes.",
		},
	},
}