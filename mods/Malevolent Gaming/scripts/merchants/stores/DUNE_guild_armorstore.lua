StoreTypes.DUNEGuildArmorStore = 
{
	MerchantTitle = "-Guild Gear Merchant-",
	CurrencyType = "GuildNote",
	CurrencyName = "Guild Note",
	Stock =
	{
        --CLOTH ITEMS
		DUNE_Stone_Villa =
		{
			DisplayName="Blueprint - DUNE Villa",
			Template="blueprint_hope_stone_villa",
			SellPrice=400,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Housing Blueprint-\n"..ToolTipTextColor.."Custom DUNE Stone Villa with +100 Lockdowns and +5 Storage Bonus.",
        },
		DUNE_Stone_Estate =
		{
			DisplayName="Blueprint - DUNE Estate",
			Template="blueprint_hope_stone_estate",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Housing Blueprint-\n"..ToolTipTextColor.."Custom DUNE Stone Estate with +100 Lockdowns and +5 Storage Bonus.",
        },
		Dune_Cloth_Helm =
		{
			DisplayName="Cloth-Alkuth'Aban Mage Hat",
			Template="dune_cloth_helm",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Mana, and 1000 Durability.  Enchant it yourself!",
        },
        Dune_Cloth_Tunic =
		{
			DisplayName="Cloth-Alkuth'Aban Mage Tunic",
			Template="dune_cloth_tunic",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Mana, and 1000 Durability.  Enchant it yourself!",
        },
		Dune_Cloth_Robe =
		{
			DisplayName="Cloth-Alkuth'Aban Mage Robe",
			Template="dune_cloth_robe",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Mana, and 1000 Durability.  Enchant it yourself!",
        },
        Dune_Cloth_Leggings =
		{
			DisplayName="Cloth-Alkuth'Aban Mage Leggings",
			Template="dune_cloth_leggings",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Mana, and 1000 Durability.  Enchant it yourself!",
        },
		--LIGHT ITEMS
		Dune_Light_Helm =
		{
			DisplayName="Light-Alkuth'Aban Leather Hood",
			Template="dune_light_helm",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Stamina, and 1000 Durability.  Enchant it yourself!",
        },
        Dune_Light_Tunic =
		{
			DisplayName="Light-Alkuth'Aban Leather Tunic",
			Template="dune_light_tunic",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Stamina, and 1000 Durability.  Enchant it yourself!",
        },
        Dune_Light_Leggings =
		{
			DisplayName="Light-Alkuth'Aban Leather Leggings",
			Template="dune_light_leggings",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Stamina, and 1000 Durability.  Enchant it yourself!",
        },
        --PLATE ITEMS
		Dune_Heavy_Helm =
		{
			DisplayName="Heavy-Alkuth'Aban Plate Helm",
			Template="dune_plate_helm",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Plate armor piece with +5 Defense, +Hit Points, and 1000 Durability.  Enchant it yourself!",
        },
        Dune_Heavy_Tunic =
		{
			DisplayName="Heavy-Alkuth'Aban Plate Tunic",
			Template="dune_plate_tunic",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Plate armor piece with +5 Defense, +Hit Points, and 1000 Durability.  Enchant it yourself!",
        },
        Dune_Heavy_Leggings =
		{
			DisplayName="Heavy-Alkuth'Aban Plate Leggings",
			Template="dune_plate_leggings",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Plate armor piece with +5 Defense, +Hit Points, and 1000 Durability.  Enchant it yourself!",
        },
        --OTHER ITEMS
        Dune_Cloak =
		{
			DisplayName="Cloak - Guild Cloak",
			Template="dune_cloak",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +Hitpoints and Health Regen V.",
		},
        Dune_Blue_Dye =
		{
			DisplayName="Dye - Alkuth'Aban Dye",
			Template="dune_blue_dye",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Dye #907-\n"..ToolTipTextColor.."Official Guild Blue Dye.",
        },
        Dune_Sandstone_Dye =
		{
			DisplayName="Dye - Sandstone Blue Dye",
			Template="dune_sandstone_blue_dye",
			SellPrice=15,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Dye #825-\n"..ToolTipTextColor.."Wastewater Fanatic's Blue Dye.",
        },
        Dune_Horse =
		{
			DisplayName="Mount - Alkuth'Aban Stallion",
			Template="item_statue_mount_dune_horse",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Horse Mount-\n"..ToolTipTextColor.."The magical stallion of Alkuth'Aban City.",
        },
    },
}