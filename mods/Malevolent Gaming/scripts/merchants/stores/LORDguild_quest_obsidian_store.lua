StoreTypes.LORDGuildStoreObsidian = 
{
	MerchantTitle = "-Obsidian Merchant-",
	CurrencyType = "Obsidian",
	CurrencyName = "Obsidian Ingots",
	Stock =
	{
		lord_Gold10 =
		{
			DisplayName="1. Legion Draconic Tablets (10)",
			Template="lord_guild_gold_box_10",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (10) Draconic Tablets for (50) Obsidian Ingots.",
		},
		lord_Gold50 =
		{
			DisplayName="2. Legion Draconic Tablets (50)",
			Template="lord_guild_gold_box_50",
			SellPrice=250,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (50) Draconic Tablets for (250) Obsidian Ingots.",
		},
		lord_Gold100 =
		{
			DisplayName="3. Legion Draconic Tablets (100)",
			Template="lord_guild_gold_box_100",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (100) Draconic Tablets for (500) Obsidian Ingots.",
		},
	},
}