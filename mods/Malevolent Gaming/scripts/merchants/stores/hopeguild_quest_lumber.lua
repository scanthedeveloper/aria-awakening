StoreTypes.HopeGuildStoreAshBoards = 
{
	MerchantTitle = "-Lumber Trader-",
	CurrencyType = "AshBoards",
	CurrencyName = "Ash Boards",
	Stock =
	{
		HOPE_Gold =
		{
			DisplayName="Quest Reward - HOPE Gold (10)",
			Template="hope_guild_gold_box_10",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (10) HOPE Guild Gold for (100) Ash Boards.",
		},

		Wood_Boards =
		{
			DisplayName="Exchange - Wood Boards (10)",
			Template="hope_guild_wood_box_10",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Exchange 20 Ashboards for (10) Wood Boards.",
		},

		Blightwood_Boards =
		{
			DisplayName="Exchange - Blightwood Boards (10)",
			Template="hope_guild_blightwood_box_10",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Exchange 20 Ashboards for (10) Blightwood Boards.",
		},
	},
}