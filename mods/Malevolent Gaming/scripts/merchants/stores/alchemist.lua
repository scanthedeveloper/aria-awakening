StoreTypes.Alchemist = 
{
	MerchantTitle = "-Alchemy Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Coins",
	Stock =
	{
		Ginseng =
		{
			DisplayName="Ginseng",
			Template="ingredient_ginsengroot",
			SellPrice=4,
			BuyPrice=1,
			MaxStock=50,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Ginseng-\n"..ToolTipTextColor.."A root found out in the wild, very useful for spell casting and alchemy.",
		},	
		LemonGrass =
		{
			DisplayName="Lemon Grass",
			Template="ingredient_lemongrass",
			SellPrice=4,
			BuyPrice=1,
			MaxStock=50,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Lemon Grass-\n"..ToolTipTextColor.."A root found out in the wild, very useful for spell casting and alchemy.",
		},
		Moss =
		{
			DisplayName="Moss",
			Template="ingredient_moss",
			SellPrice=4,
			BuyPrice=1,
			MaxStock=50,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Moss-\n"..ToolTipTextColor.."A root found out in the wild, very useful for spell casting and alchemy.",
		},
		Mushroom =
		{
			DisplayName="Mushroom",
			Template="ingredient_mushroom",
			SellPrice=4,
			BuyPrice=1,
			MaxStock=50,
			CanSell = false,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Mushroom-\n"..ToolTipTextColor.."A root found out in the wild, very useful for spell casting and alchemy.",
		},
	},
}