StoreTypes.BlackpathScrapper = 
{
	MerchantTitle = "-Blackpath Salvager-",
	CurrencyType = "MetalScraps",
	CurrencyName = "Blackpath Metal Scraps",
	Stock =
	{
		NightbornGems25 =
		{
			DisplayName="Quest Reward - Blackpath Metal Scraps",
			Template="blackpath_gem_box_50",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (50) Blackpath Blood Runes for (1) Blackpath Metal Scrap.",
		},
	},
}