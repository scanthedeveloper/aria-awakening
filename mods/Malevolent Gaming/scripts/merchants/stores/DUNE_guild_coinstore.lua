StoreTypes.DUNEGuildCoinStore = 
{
	MerchantTitle = "-Guild Quest Merchant-",
	CurrencyType = "GuildNote",
	CurrencyName = "Guild Note",
	Stock =
	{
        --CLOTH ITEMS
		 Dune_Quest_Reward =
		{
			DisplayName="Box of Coins",
			Template="furniture_box_dune",
			SellPrice=5,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."A box of 2500 coins.",
        },
    },
}