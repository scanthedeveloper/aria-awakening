StoreTypes.HOPEGuildCoinStore = 
{
	MerchantTitle = "-Guild Quest Merchant-",
	CurrencyType = "HopeGem",
	CurrencyName = "HOPE Gold",
	Stock =
	{
        --CLOTH ITEMS
		 Dune_Quest_Reward =
		{
			DisplayName="Box of Coins",
			Template="furniture_box_hope_coins",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."A box of 2500 coins.",
        },
    },
}