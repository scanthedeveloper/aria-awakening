StoreTypes = {}

ToolTipHeaderColor = "[FF7700]"
ToolTipTextColor = "[FFFFAA]"

require 'merchants.stores.weaponsmith'
require 'merchants.stores.tailor'
require 'merchants.stores.armorsmith'
require 'merchants.stores.leatherworker'
require 'merchants.stores.alchemist'

require 'merchants.stores.newplayer_reward_merchant'