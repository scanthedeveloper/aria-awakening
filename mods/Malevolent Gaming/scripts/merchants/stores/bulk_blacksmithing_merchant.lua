StoreTypes.BulkBlacksmithingMerchant = 
{
	MerchantTitle = "-Bulk Blacksmithing Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Platinum",
	Stock =
	{
		--[[Dagger =
		{
			DisplayName="Dagger",
			Template="weapon_dagger",
			SellPrice=36,
			BuyPrice=0,
			MaxStock=20,
			CanSell = false,
			CanBuy = false,
			Tooltip = ToolTipHeaderColor.."-Dagger-\n"..ToolTipTextColor.."A sharp quick weapon that requires the peircing skill to be effective.",
			Conditions = 
			{
				{Type="ObjVarEqual",Value="TestDaggerReq|test"},
				{Type="ObjVarGreaterThan",Value="BiggerThan|25"},
				{Type="ObjVarLessThan",Value="LessThan|25"},
				{Type="SkillGreaterThan",Value="Fabrication|50"},
				{Type="SkillLessThan",Value="Healing|50"},
			},
		},]]--

        --INGOTS
		BulkIronRecipe =
		{
			DisplayName="1. Bulk Iron Recipe",
			Template="recipe_bulk_iron",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine iron (100).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Metalsmith|59"},
			},
		},
        BulkCopperRecipe =
		{
			DisplayName="2. Bulk Copper Recipe",
			Template="recipe_bulk_copper",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine copper (100).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Metalsmith|59"},
			},
		},
        BulkGoldRecipe =
		{
			DisplayName="3. Bulk Gold Recipe",
			Template="recipe_bulk_gold",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine gold (100).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Metalsmith|79"},
			},
		},
        BulkCobaltRecipe =
		{
			DisplayName="4. Bulk Cobalt Recipe",
			Template="recipe_bulk_cobalt",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine cobalt (100).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Metalsmith|79"},
			},
		},
        BulkObsidianRecipe =
		{
			DisplayName="5. Bulk Obsidian Recipe",
			Template="recipe_bulk_obsidian",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine obsidian (100).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Metalsmith|99"},
			},
		},
	},
}