StoreTypes.NightbornTailor = 
{
	MerchantTitle = "-Nightborn Tailor-",
	CurrencyType = "NightbornGem",
	CurrencyName = "Nightborn Gems",
	Stock =
	{
		NightBoneLoom =
		{
			DisplayName="Nightborn Bone Loom",
			Template="nightborn_bone_loom_packed",
			SellPrice=1000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A purple, Nightborn themed bone loom for Fabrication.",
		},
		DreamweaveHelm =
		{
			DisplayName="Dreamweave Helm",
			Template="nightborn_dreamweave_helm",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Armor-\n"..ToolTipTextColor.."A cursed cloth helmet that gives an increase to all stats.",
		},
		DreamweaveTunic =
		{
			DisplayName="Dreamweave Tunic",
			Template="nightborn_dreamweave_tunic",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Armor-\n"..ToolTipTextColor.."A cursed cloth tunic that gives an increase to all stats.",
		},
		DreamweaveLeggings =
		{
			DisplayName="Dreamweave Leggings",
			Template="nightborn_dreamweave_leggings",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Armor-\n"..ToolTipTextColor.."Cursed cloth leggings that gives an increase to all stats.",
		},
		DreamweavePouch =
		{
			DisplayName="Dreamweave Pouch",
			Template="nightborn_dreamweave_pouch",
			SellPrice=250,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Container-\n"..ToolTipTextColor.."Nightborn themed pouch that holds 200 items.",
		},
	},
}