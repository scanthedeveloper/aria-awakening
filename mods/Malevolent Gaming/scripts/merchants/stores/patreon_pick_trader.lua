StoreTypes.PatreonPickTrader = 
{
	MerchantTitle = "-Patreon God Pick Trader-",
	CurrencyType = "BrokenGodPick",
	CurrencyName = "Patreon God Picks",
	Stock =
	{
        GodPick =
		{
			DisplayName="Tool - God Pick",
			Template="SCAN_harvest_GOD_pick2",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Tool-\n"..ToolTipTextColor.."A pre-enchanted pick that provides, strength, speed, and weight bonuses.",
        },
	},
}