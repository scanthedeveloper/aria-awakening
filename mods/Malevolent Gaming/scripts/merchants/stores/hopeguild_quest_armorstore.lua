StoreTypes.HopeGuildArmorStore = 
{
	MerchantTitle = "-Armor Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Platinum",
	Stock =
	{
		Enforcer_Helm =
		{
			DisplayName="Heavy - Enforcer's Helm",
			Template="hope_enforcer_helm",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Chain Armor-\n"..ToolTipTextColor.."Heavy armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Enforcer_Tunic =
		{
			DisplayName="Heavy - Enforcer's Tunic",
			Template="hope_enforcer_tunic",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Chain Armor-\n"..ToolTipTextColor.."Heavy armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Enforcer_Leggings =
		{
			DisplayName="Heavy - Enforcer's Leggings",
			Template="hope_enforcer_leggings",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Chain Armor-\n"..ToolTipTextColor.."Heavy armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Guardian_Helm =
		{
			DisplayName="Heavy - Guardian's Helm",
			Template="hope_guardian_helm",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Heavy armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Guardian_Tunic =
		{
			DisplayName="Heavy - Guardian's Tunic",
			Template="hope_guardian_tunic",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Heavy armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Guardian_Leggings =
		{
			DisplayName="Heavy - Guardian's Leggings",
			Template="hope_guardian_leggings",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Heavy armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Soldiers_Helm =
		{
			DisplayName="Light - Soldier's Helm",
			Template="hope_soldier_helm",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Soldiers_Tunic =
		{
			DisplayName="Light - Soldier's Tunic",
			Template="hope_soldier_tunic",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Soldiers_Leggings =
		{
			DisplayName="Light - Soldier's Leggings",
			Template="hope_soldier_leggings",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Saint_Helm =
		{
			DisplayName="Cloth - Saint's Helm",
			Template="hope_saint_helm",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Saint_Tunic =
		{
			DisplayName="Cloth - Saint's Robe",
			Template="hope_saint_tunic",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Saint_Leggings =
		{
			DisplayName="Cloth - Saint's Leggings",
			Template="hope_saint_leggings",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Neuromancer_Helm =
		{
			DisplayName="Cloth - Neuromancer's Helm",
			Template="hope_neuromancer_helm",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Neuromancer_Tunic =
		{
			DisplayName="Cloth - Neuromancer's Tunic",
			Template="hope_neuromancer_tunic",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
        },
        Neuromancer_Leggings =
		{
			DisplayName="Cloth - Neuromancer's Leggings",
			Template="hope_neuromancer_leggings",
			SellPrice=50000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +4 Defense and 400 Durability.  Enchant it yourself!",
		},
	},
}