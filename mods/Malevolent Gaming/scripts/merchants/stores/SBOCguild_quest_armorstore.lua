StoreTypes.SBOCGuildArmorStore = 
{
	MerchantTitle = "-Guild Merchant-",
	CurrencyType = "GuildNote",
	CurrencyName = "Guild Note",
	Stock =
	{
		Anarchic_Helm =
		{
			DisplayName="Heavy - Anarchic Helm",
			Template="sboc_plate_helm",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Heavy armor piece with +5 Defense and 500 Durability.  Enchant it yourself!",
        },
        Anarchic_Tunic =
		{
			DisplayName="Heavy - Anarchic Tunic",
			Template="sboc_plate_tunic",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Heavy armor piece with +5 Defense and 500 Durability.  Enchant it yourself!",
        },
        Anarchic_Leggings =
		{
			DisplayName="Heavy - Anarchic Leggings",
			Template="sboc_plate_leggings",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Heavy armor piece with +5 Defense and 500 Durability.  Enchant it yourself!",
        },
        Wyrmskin_Helm =
		{
			DisplayName="Light - Sacred Skin Helm",
			Template="sboc_light_helm",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense and 400 Durability.  Enchant it yourself!",
        },
        Wyrmskin_Tunic =
		{
			DisplayName="Light - Sacred Skin Tunic",
			Template="sboc_light_tunic",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense and 400 Durability.  Enchant it yourself!",
        },
        Wyrmskin_Leggings =
		{
			DisplayName="Light - Sacred Skin Leggings",
			Template="sboc_light_leggings",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense and 400 Durability.  Enchant it yourself!",
        },
        Ivysilk_Helm =
		{
			DisplayName="Cloth - Magnate Finery Helm",
			Template="sboc_cloth_helm",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +5 Defense and 300 Durability.  Enchant it yourself!",
        },
        Ivysilk_Tunic =
		{
			DisplayName="Cloth - Magnate Finery Robe",
			Template="sboc_cloth_tunic",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +5 Defense and 300 Durability.  Enchant it yourself!",
        },
        Ivysilk_Leggings =
		{
			DisplayName="Cloth - Magnate Finery Leggings",
			Template="sboc_cloth_leggings",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +5 Defense and 300 Durability.  Enchant it yourself!",
        },
	},
}