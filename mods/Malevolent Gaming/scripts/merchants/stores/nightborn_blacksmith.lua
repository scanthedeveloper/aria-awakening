StoreTypes.NightbornBlacksmith = 
{
	MerchantTitle = "-Nightborn Blacksmith-",
	CurrencyType = "NightbornGem",
	CurrencyName = "Nightborn Gems",
	Stock =
	{
		NightbornRing =
		{
			DisplayName="Nightborn Forge",
			Template="nightborn_forge_packed",
			SellPrice=1000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Workstation-\n"..ToolTipTextColor.."A Nightborn Themed forge that allows you to perform blacksmithing skills.",
		},
		NightbornMiningCart =
		{
			DisplayName="Nightborn Mining Cart",
			Template="dreamwood_mining_cart_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A Nightborn Themed mining cart.",
        },
        FaeSteelHelm =
		{
			DisplayName="Fae Steel Helm",
			Template="nightborn_faesteel_helm",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Armor-\n"..ToolTipTextColor.."A cursed Nightborn Themed heavy plate helm with +ALL Stats, +Sunder Chance, +Spellblocking< & +Defense.",
        },
        FaeSteelLeggings =
		{
			DisplayName="Fae Steel Leggings",
			Template="nightborn_faesteel_leggings",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Armor-\n"..ToolTipTextColor.."A cursed Nightborn Themed heavy plate leggings with +ALL Stats, +Sunder Chance, +Spellblocking< & +Defense.",
        },
        FaeSteelTunic =
		{
			DisplayName="Fae Steel Tunic",
			Template="nightborn_faesteel_tunic",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Armor-\n"..ToolTipTextColor.."A cursed Nightborn Themed heavy plate tunic with +ALL Stats, +Sunder Chance, +Spellblocking< & +Defense.",
        },
	},
}