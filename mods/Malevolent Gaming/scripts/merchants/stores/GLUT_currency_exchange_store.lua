StoreTypes.GLUTcurrency_exchange_store = 
{
	MerchantTitle = "-GLUT Currency Exchange Merchant-",
	CurrencyType = "EmberCrystal",
	CurrencyName = "Ember Crystal",
	Stock =
	{
		GLUT_Gold1 =
		{
			DisplayName="1. GLUT Currency Exchange (1)",
			Template="guild_coin",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 1 Ember Crystals into 1 Guild Note.",
		},
		GLUT_Gold5 =
		{
			DisplayName="2. GLUT Currency Exchange (5)",
			Template="guild_coin_box_5",
			SellPrice=5,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 5 Ember Crystals into 5 Guild Notes.",
		},
		GLUT_Gold10 =
		{
			DisplayName="3. GLUT Currency Exchange (10)",
			Template="guild_coin_box_10",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 10 Ember Crystals into 10 Guild Notes.",
		},
		GLUT_Gold20 =
		{
			DisplayName="4. GLUT Currency Exchange (20)",
			Template="guild_coin_box_20",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 20 Ember Crystals into 20 Guild Notes.",
		},
		GLUT_Gold50 =
		{
			DisplayName="5. GLUT Currency Exchange (50)",
			Template="guild_coin_box_50",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 50 Ember Crystals into 50 Guild Notes.",
		},
		GLUT_Gold100 =
		{
			DisplayName="6. GLUT Currency Exchange (100)",
			Template="guild_coin_box_100",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Exchange-\n"..ToolTipTextColor.."Convert 100 Ember Crystals into 100 Guild Notes.",
		},
	},
}