StoreTypes.FrozenCrystalConversionStore = 
{
	MerchantTitle = "-Master Druid-",
	CurrencyType = "FrozenCrystalFragment",
	CurrencyName = "Frozen Crystal Fragments",
	Stock =
	{
		FrozenGlowingOrb =
		{
			DisplayName="Quest Reward - Frozen Orb",
			Template="resource_frozenorb",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Provides the player with a Strangely Cold Orb which is sought after by the Druids in the Northwest Region of Frozen Tundra..",
		},
	},
}