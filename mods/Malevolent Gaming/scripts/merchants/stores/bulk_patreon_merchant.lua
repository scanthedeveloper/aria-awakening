StoreTypes.BulkPatreonMerchant = 
{
	MerchantTitle = "-Bulk Patron Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Platinum",
	Stock =
	{
		--[[Dagger =
		{
			DisplayName="Dagger",
			Template="weapon_dagger",
			SellPrice=36,
			BuyPrice=0,
			MaxStock=20,
			CanSell = false,
			CanBuy = false,
			Tooltip = ToolTipHeaderColor.."-Dagger-\n"..ToolTipTextColor.."A sharp quick weapon that requires the peircing skill to be effective.",
			Conditions = 
			{
				{Type="ObjVarEqual",Value="TestDaggerReq|test"},
				{Type="ObjVarGreaterThan",Value="BiggerThan|25"},
				{Type="ObjVarLessThan",Value="LessThan|25"},
				{Type="SkillGreaterThan",Value="Fabrication|50"},
				{Type="SkillLessThan",Value="Healing|50"},
			},
		},]]--

        --CARPENTRY
		BulkBrickRecipe =
		{
			DisplayName="C - 1. Bulk Brick Recipe",
			Template="recipe_bulk_brick",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine brick (100).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Woodsmith|59"},
			},
		},
        BulkWoodRecipe =
		{
			DisplayName="C - 2. Bulk Wood Recipe",
			Template="recipe_bulk_boards",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine wood boards (100).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Woodsmith|59"},
			},
		},
        BulkAshRecipe =
		{
			DisplayName="C - 3. Bulk Ash Recipe",
			Template="recipe_bulk_ashboards",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine ash boards (100).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Woodsmith|79"},
			},
		},
        BulkBlightwoodRecipe =
		{
			DisplayName="C - 4. Bulk Blightwood Recipe",
			Template="recipe_bulk_blightwoodboards",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine blightwood boards (100).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Woodsmith|79"},
			},
		},
        BulkDreamwoodRecipe =
		{
			DisplayName="C - 5. Bulk Dreamwood Recipe",
			Template="recipe_bulk_dreamwood",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine dreamwood boards (100).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Woodsmith|99"},
			},
		},
        BulkIronRecipe =
		{
			DisplayName="B - 1. Bulk Iron Recipe",
			Template="recipe_bulk_iron",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine iron (100).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Metalsmith|59"},
			},
		},
        BulkCopperRecipe =
		{
			DisplayName="B - 2. Bulk Copper Recipe",
			Template="recipe_bulk_copper",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine copper (100).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Metalsmith|59"},
			},
		},
        BulkGoldRecipe =
		{
			DisplayName="B - 3. Bulk Gold Recipe",
			Template="recipe_bulk_gold",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine gold (100).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Metalsmith|79"},
			},
		},
        BulkCobaltRecipe =
		{
			DisplayName="B - 4. Bulk Cobalt Recipe",
			Template="recipe_bulk_cobalt",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine cobalt (100).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Metalsmith|79"},
			},
		},
        BulkObsidianRecipe =
		{
			DisplayName="B - 5. Bulk Obsidian Recipe",
			Template="recipe_bulk_obsidian",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine obsidian (100).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Metalsmith|99"},
			},
		},
        BulkClothRecipe =
		{
			DisplayName="F - 1. Bulk Cloth Recipe",
			Template="recipe_bulk_cloth",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine cloth (500).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Fabrication|59"},
			},
		},
		BulkQuiltedRecipe =
		{
			DisplayName="F - 2. Bulk Quilted Recipe",
			Template="recipe_bulk_quilted",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine quilted cloth (500).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Fabrication|79"},
			},
		},
		BulkSilkRecipe =
		{
			DisplayName="F - 3. Bulk Silk Recipe",
			Template="recipe_bulk_silk",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine silk cloth (500).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Fabrication|99"},
			},
		},

        --LEATHER
        BulkLeatherhRecipe =
		{
			DisplayName="F - 4. Bulk Leather Recipe",
			Template="recipe_bulk_leather",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine leather (300).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Fabrication|59"},
			},
		},
		BulkBeastLeatherRecipe =
		{
			DisplayName="F - 5. Bulk Beast Leather Recipe",
			Template="recipe_bulk_beastleather",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine beast leather (300).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Fabrication|79"},
			},
		},
		BulkVileLeatherRecipe =
		{
			DisplayName="F - 6. Bulk Vile Leather Recipe",
			Template="recipe_bulk_vileleather",
			SellPrice=250000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine vile leather (300).",
			Conditions = 
			{
				--{Type="SkillGreaterThan",Value="Fabrication|99"},
			},
		},
	},
}