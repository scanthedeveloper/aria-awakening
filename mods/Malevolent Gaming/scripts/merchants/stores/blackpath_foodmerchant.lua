StoreTypes.Blackpath_Foodmerchant = 
{
	MerchantTitle = "-Food Merchant-",
	CurrencyType = "BlackpathGem",
	CurrencyName = "Blood Runes",
	Stock =
	{
		BattleMageFood =
		{
			DisplayName="Food Battlemage Food",
			Template="item_food_blackpath_battlemage_food",
			SellPrice=25,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Food-\n"..ToolTipTextColor.."Food that increases your strength, intelligence, and defense by +10.",
        },
        AgilityFood =
		{
			DisplayName="Food Agility Turkey Leg",
			Template="item_food_blackpath_agility_food",
			SellPrice=25,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Food-\n"..ToolTipTextColor.."Food that increases your health by +100 and agility by +10.",
        },
        BloodlustFood =
		{
			DisplayName="Food Bloodlust Meat Slices",
			Template="item_food_blackpath_bloodlust_food",
			SellPrice=25,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Food-\n"..ToolTipTextColor.."Food that increases your bloodlust by +50, and strength by +10.",
        },
        MageFood =
		{
			DisplayName="Food Mage Food",
			Template="item_food_blackpath_mage_food",
			SellPrice=25,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Food-\n"..ToolTipTextColor.."Food that increases your intelligence by +10, wisdom by +10, and mana by +50",
        },
        SpeedFood =
		{
			DisplayName="Food Speed Loaf",
			Template="item_food_blackpath_speed_food",
			SellPrice=25,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Food-\n"..ToolTipTextColor.."Food that increases your speed by 20%.",
        },
        SpellBlockingFood =
		{
			DisplayName="Food Spellblocking Meat",
			Template="item_food_blackpath_spellblocking_food",
			SellPrice=25,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Food-\n"..ToolTipTextColor.."Food that increases your spellblocking success chance by +20% and defense by +10.",
        },
        StrengthFood =
		{
			DisplayName="Food Strength Turkey Leg",
			Template="item_food_blackpath_strength_food",
			SellPrice=25,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Food-\n"..ToolTipTextColor.."Food that increases your health by +100 and strength by +10.",
        },
        WeightFood =
		{
			DisplayName="Food Weight Banquet",
			Template="item_food_blackpath_weight_food",
			SellPrice=25,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Food-\n"..ToolTipTextColor.."Food that increases your your weight capacity by +200.",
        },
	},
}