StoreTypes.NightbornStableMaster = 
{
	MerchantTitle = "-Nightborn Stable Master-",
	CurrencyType = "NightbornGem",
	CurrencyName = "Nightborn Gems",
	Stock =
	{
		NightbornWarg =
		{
			DisplayName="Nightborn Warg Mount",
			Template="item_statue_mount_warg_nightborn",
			SellPrice=1500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Mount-\n"..ToolTipTextColor.."A Blessed, Saddled Nightborn Purple Warg.",
		},
		NightbornStag =
		{
			DisplayName="Nightborn Stag Mount",
			Template="item_statue_mount_nightborn_stag",
			SellPrice=1500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Mount-\n"..ToolTipTextColor.."A Blessed, Saddled Nightborn Purple Stag.",
		},
	},
}