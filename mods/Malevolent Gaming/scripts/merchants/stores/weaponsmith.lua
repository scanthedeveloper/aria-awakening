StoreTypes.Weaponsmith = 
{
	MerchantTitle = "-Weaponsmith Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Coins",
	Stock =
	{
		Dagger =
		{
			DisplayName="Dagger",
			Template="weapon_dagger",
			SellPrice=36,
			BuyPrice=12,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Dagger-\n"..ToolTipTextColor.."A sharp quick weapon that requires the peircing skill to be effective.",
			Conditions = 
			{
				{Type="ObjVarEqual",Value="TestDaggerReq|test"},
				{Type="ObjVarGreaterThan",Value="BiggerThan|25"},
				{Type="ObjVarLessThan",Value="LessThan|25"},
				{Type="SkillGreaterThan",Value="Fabrication|50"},
				{Type="SkillLessThan",Value="Healing|50"},
			},
		},
		Longsword =
		{
			DisplayName="Longsword",
			Template="weapon_longsword",
			SellPrice=72,
			BuyPrice=24,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Longsword-\n"..ToolTipTextColor.."A sharp weapon that requires the slashing skill to be effective.",
		},
		Mace =
		{
			DisplayName="Mace",
			Template="weapon_mace",
			SellPrice=72,
			BuyPrice=24,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Mace-\n"..ToolTipTextColor.."A slow but hard hitting weapon that requires the bashing skill to be effective.",
		},
		QuarterStaff =
		{
			DisplayName="Quarter Staff",
			Template="weapon_quarterstaff",
			SellPrice=108,
			BuyPrice=36,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quarter Staff-\n"..ToolTipTextColor.."A long wooden weapon that requires the bashing skill to be effective.",
		},
		Warfork =
		{
			DisplayName="Warfork",
			Template="weapon_warfork",
			SellPrice=108,
			BuyPrice=36,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Warfork-\n"..ToolTipTextColor.."A mutli pointed tip staff that requires the lancing skill to be effective.",
		},
	},
}