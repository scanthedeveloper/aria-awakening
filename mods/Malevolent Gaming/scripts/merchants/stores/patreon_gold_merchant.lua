StoreTypes.PatreonRecipeMerchant = 
{
	MerchantTitle = "-Patreon Recipe Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Platinum",
	Stock =
	{
		--[[Dagger =
		{
			DisplayName="Dagger",
			Template="weapon_dagger",
			SellPrice=36,
			BuyPrice=0,
			MaxStock=20,
			CanSell = false,
			CanBuy = false,
			Tooltip = ToolTipHeaderColor.."-Dagger-\n"..ToolTipTextColor.."A sharp quick weapon that requires the peircing skill to be effective.",
			Conditions = 
			{
				{Type="ObjVarEqual",Value="TestDaggerReq|test"},
				{Type="ObjVarGreaterThan",Value="BiggerThan|25"},
				{Type="ObjVarLessThan",Value="LessThan|25"},
				{Type="SkillGreaterThan",Value="Fabrication|50"},
				{Type="SkillLessThan",Value="Healing|50"},
			},
		},]]--
        --SLASHING
		PatronNinjatoRecipe =
		{
			DisplayName="Recipe - Slashing: Ninjato",
			Template="recipe_patron_ninjato",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Slashing Weapon-\n"..ToolTipTextColor.."A very fast attacking weapon with Stab & Bleed weapon abilities.",
		},
		
		PatronFalchionRecipe =
		{
			DisplayName="Recipe - Slashing: Falchion",
			Template="recipe_patron_falchion",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Slashing Weapon-\n"..ToolTipTextColor.."A thick utility sword with Follow-Through & Eviscerate weapon abilities.",
		},
		--[[
        PatronKnightsSword =
		{
			DisplayName="Recipe - Patron Knight's Sword",
			Template="recipe_patron_knightssword",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Slashing Weapon-\n"..ToolTipTextColor.."A conventional tanking sword with Cleave & Stun weapon abilities.",
		},
        PatronHandAxeRecipe =
		{
			DisplayName="Recipe - Patron Hand Axe",
			Template="recipe_patron_handaxe",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Slashing Weapon-\n"..ToolTipTextColor.."A hard hitting one-handed axe with Bleed & Howl weapon abilities..",
		},
        PatronClaymore =
		{
			DisplayName="Recipe - Patron Claymore",
			Template="recipe_patron_claymore",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Slashing Weapon-\n"..ToolTipTextColor.."A hard hitting 2-handed weapon with Cleave & Bleed.",
		},
        PatronBeserkersAxe =
		{
			DisplayName="Recipe - Patron Beserkers Axe",
			Template="recipe_patron_beserkers_axe",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Slashing Weapon-\n"..ToolTipTextColor.."2h Axe with Cleave & Enrage.",
		},
        PatronKingsAxe =
		{
			DisplayName="Recipe - Patron King's Axe",
			Template="recipe_patron_kings_axe",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Slashing Weapon-\n"..ToolTipTextColor.."1h Slashing weapon with Followthrough & Stun.",
		},
        ]]
        --ARCHERY
		PatronDoubleShotBowRecipe =
		{
			DisplayName="Recipe - Archery: Double Shot Bow",
			Template="recipe_patron_doubleshot_bow",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Longbow-\n"..ToolTipTextColor.."A long bow that can shoot a target twice after (.5) seconds..",
		},
        PatronDazeBow =
		{
			DisplayName="Recipe - Archery: Daze Bow",
			Template="recipe_patron_daze_bow",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Archery Weapon-\n"..ToolTipTextColor.."Short bow archetype that can proc Daze on it's shot.",
		},
        --[[
		PatronBleedingBow =
		{
			DisplayName="Recipe - Patron Bleeding Bow",
			Template="recipe_patron_bleed_bow",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Longbow-\n"..ToolTipTextColor.."A bow that allows you to apply bleed to enemy ranged targets.",
        },
        PatronStunBow =
		{
			DisplayName="Recipe - Patron Stun Bow",
			Template="recipe_patron_stunbow_fix",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Warfork-\n"..ToolTipTextColor.."Warbow that allows you to stun your enemy using a ranged attack.",
		},
        PatronAutomaticBowRecipe =
		{
			DisplayName="Recipe - Patron Automatic Bow",
			Template="recipe_patron_automatic_bow",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Shortbow-\n"..ToolTipTextColor.."A short bow that can auto-shoot your target and is pre-enchanted with +Accuracy(1-30).",
        },
        ]]
		--BASHING
        PatronBraidedWarhammer =
		{
			DisplayName="Recipe - Bashing: Braided Warhammer",
			Template="recipe_patron_braidedwarhammer",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Bashing Weapon-\n"..ToolTipTextColor.."2h Bashing weapon with Followthrough & Silence.",
		},
        PatronOrnateGreathammer =
		{
			DisplayName="Recipe - Bashing: Ornate Greathammer",
			Template="recipe_patron_ornate_greathammer",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Bashing Weapon-\n"..ToolTipTextColor.."1h Bashing weapon with Overpower & Stun.",
		},
        --[[
        PatronBeserkersMaul =
		{
			DisplayName="Recipe - Patron Beserkers Maul",
			Template="recipe_patron_beserkers_maul",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Bashing Weapon-\n"..ToolTipTextColor.."2h Maul with Bleed & Enrage.",
		},
        ]]
        --LANCING
		PatronBeserkersNaginata =
		{
			DisplayName="Recipe - Lancing: Beserkers Naginata",
			Template="recipe_patron_beserkers_naginata",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Lancing Weapon-\n"..ToolTipTextColor.."2h Naginata with Cleave & Enrage.",
		},
        PatronPike =
		{
			DisplayName="Recipe - Lancing: Pike",
			Template="recipe_patron_pike",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Lancing Weapon-\n"..ToolTipTextColor.."A massive, 1h lancing weapon with stab and bleed.",
		},
        --[[
        PatronPoleAxe =
		{
			DisplayName="Recipe - Patron Pole Axe",
			Template="recipe_patron_pole_axe",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Lancing Weapon-\n"..ToolTipTextColor.."2h Lancing weapon with Cleave & Bleed.",
		},
        ]]
        --PIERCING
        PatronDemonDagger =
		{
			DisplayName="Recipe - Piercing: Demon Dagger",
			Template="recipe_patron_demon_dagger",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Piercing Weapon-\n"..ToolTipTextColor.."Piercing weapon with Silence & Poison.",
		},
        PatronDualbladeDagger =
		{
			DisplayName="Recipe - Piercing: Beserker's Dagger",
			Template="recipe_patron_dualblade_dagger",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Piercing Weapon-\n"..ToolTipTextColor.."Piercing weapon with Bleed & Enrage.",
		},
        --[[
            PatronSacrificialDagger =
		{
			DisplayName="Recipe - Patron Sacrificial Dagger",
			Template="recipe_patron_sacrificial_dagger",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Piercing Weapon-\n"..ToolTipTextColor.."Piercing weapon with Silence & Bleed.",
		},
        ]]
		--SHIELD
		PatronCrossShield =
		{
			DisplayName="Recipe - Shield: Cross Shield",
			Template="recipe_patron_cross_shield",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Shield-\n"..ToolTipTextColor.."Wooden Shield with increased stamina regeneration.",
		},
		PatronOrnateShield =
		{
			DisplayName="Recipe - Shield: Ornate Shield",
			Template="recipe_patron_ornate_shield",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Shield-\n"..ToolTipTextColor.."A large, wooden, decorative shield with a high armor rating and health regeneration.",
		},
        --[[
        PatronHeaterShield =
		{
			DisplayName="Recipe - Patron Heater Shield",
			Template="recipe_patron_heater_shield",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Shield-\n"..ToolTipTextColor.."A large shield with a high armor rating and bloodlust regeneration.",
		},
        PatronSavageShield =
		{
			DisplayName="Recipe - Patron Savage Shield",
			Template="recipe_patron_savage_shield",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Shield-\n"..ToolTipTextColor.."A large wooden shield with a high armor rating and added strength.",
		},
        ]]
	},
}