StoreTypes.HopeGuildStoreCobalt = 
{
	MerchantTitle = "-Metal Trader-",
	CurrencyType = "Cobalt",
	CurrencyName = "Cobalt Ingots",
	Stock =
	{
		HOPE_Gold =
		{
			DisplayName="Quest Reward - HOPE Gold (10)",
			Template="hope_guild_gold_box_10",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (10) HOPE Guild Gold for (100) Cobalt Ingots.",
		},

		Iron_Ingots =
		{
			DisplayName="Exchange - Ingots Iron (10)",
			Template="hope_guild_iron_box_10",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Exchange 20 Cobalt Ingots for (10) Iron Ingots.",
		},

		Copper_Ingots =
		{
			DisplayName="Exchange - Ingots Copper (10)",
			Template="hope_guild_copper_box_10",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Exchange 20 Cobalt Ingots for (10) Copper Ingots.",
        },
        
        Gold_Ingots =
		{
			DisplayName="Exchange - Ingots Gold (10)",
			Template="hope_guild_gold_ingot_box_10",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Exchange 20 Cobalt Ingots for (10) Gold Ingots.",
        },
        
        Obsidian_Ingots =
		{
			DisplayName="Exchange - Ingots Obsidian (10)",
			Template="hope_guild_obsidian_box_10",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Exchange 20 Cobalt Ingots for (10) Obsidian Ingots.",
		},
	},
}