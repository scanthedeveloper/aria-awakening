StoreTypes.NightbornArchitect = 
{
	MerchantTitle = "-Nightborn Architect-",
	CurrencyType = "NightbornGem",
	CurrencyName = "Nightborn Gems",
	Stock =
	{
		NightbornVilla =
		{
			DisplayName="Blueprint - Nightborn Villa",
			Template="blueprint_nightborn_villa",
			SellPrice=5000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Housing Blueprint-\n"..ToolTipTextColor.."A Nightborn Themed Wood Mayor's house with +5 Bonus Storage Capacity.",
		},
		NightbornTavern =
		{
			DisplayName="Blueprint - Nightborn Tavern",
			Template="blueprint_nightborn_tavern",
			SellPrice=7500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Housing Blueprint-\n"..ToolTipTextColor.."A Nightborn Themed Wood Tavern house with +5 Bonus Storage Capacity.",
        },
        DreamwoodBoards =
		{
			DisplayName="Recipe - Dreamwood Boards",
			Template="recipe_dreamwood_boards",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A resource recipe that teaches you how to make the legendary Dreamwood Boards.",
        },
        DreamwoodFlute =
		{
			DisplayName="Recipe - Dreamwood Flute",
			Template="recipe_nightborn_flute",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A weapon recipe that teaches you how to make the legendary Dreamwood Boards.",
        },
        WandOfPower =
		{
			DisplayName="Recipe - Dreamwood Orb",
			Template="recipe_nightborn_wand",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A weapon recipe that teaches you how to make the legendary Dreamwood Boards.",
		},
		DreamwoodArrows =
		{
			DisplayName="Recipe - Dreamwood Arrows",
			Template="recipe_dreamwood_arrow",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A weapon recipe that teaches you how to make the legendary Dreamwood Arrows.",
		},
		DreamwoodCrook =
		{
			DisplayName="Recipe - Dreamwood Crook",
			Template="recipe_dreamwood_crook",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A tool recipe that teaches you how to make the legendary Dreamwood Crook +Taming %.",
		},
		DreamwoodCrown =
		{
			DisplayName="Recipe - Dreamwood Crown",
			Template="recipe_dreamwood_crown",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A light armor recipe that teaches you how to make the legendary Dreamwood Crown.",
		},
		DreamwoodLeggings =
		{
			DisplayName="Recipe - Dreamwood Leggings",
			Template="recipe_dreamwood_leggings",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A light armor recipe that teaches you how to make the legendary Dreamwood Leggings.",
		},
		DreamwoodTunic =
		{
			DisplayName="Recipe - Dreamwood Tunic",
			Template="recipe_dreamwood_tunic",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A light armor recipe that teaches you how to make the legendary Dreamwood Leggings.",
		},
		DreamwoodShield2 =
		{
			DisplayName="Recipe - Dreamwood Spiked Shield",
			Template="recipe_dreamwood_shield",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A shield recipe that teaches you how to make the legendary Dreamwood Spiked Shield.",
		},
		DreamwoodSpikedClub =
		{
			DisplayName="Recipe - Dreamwood Spiked Club",
			Template="recipe_dreamwood_spikedclub",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A bashing, weapon recipe that teaches you how to make the legendary Dreamwood Spiked Club.",
		},
		DreamwoodStarfallBow =
		{
			DisplayName="Recipe - Dreamwood Starfall Bow",
			Template="recipe_dreamwood_starfall_bow",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."A weapon recipe that teaches you how to make the legendary Dreamwood Starfall Bow.",
		},
		NightboreDye_Purple =
		{
			DisplayName="Recipe - Puple Dye",
			Template="recipe_dye_nightborn_purple",
			SellPrice=250,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Nightborn Themed Dye",
		},
		NightboreDye_Pink =
		{
			DisplayName="Recipe - Pink Dye",
			Template="recipe_dye_nightborn_pink",
			SellPrice=250,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Nightborn Themed Dye",
		},
		NightboreDye_NightbornDye =
		{
			DisplayName="Recipe - Nightborn Dye",
			Template="recipe_dye_nightborn_nightbornpurple",
			SellPrice=250,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Nightborn Themed Dye",
		},
		NightboreDye_Fuchsia =
		{
			DisplayName="Recipe - Fuchsia Dye",
			Template="recipe_dye_nightborn_fuchsia",
			SellPrice=250,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Nightborn Themed Dye",
		},
		NightboreDye_Electrum =
		{
			DisplayName="Recipe - Electrum Green Dye",
			Template="recipe_dye_nightborn_electrumgreen",
			SellPrice=250,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Nightborn Themed Dye",
        },
        DreamwoodWoodBox =
		{
			DisplayName="Resource - Dreamwood",
			Template="dreamwood_wood_box",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Resources-\n"..ToolTipTextColor.."A box of 100 Dreamwood.",
		},
	},
}