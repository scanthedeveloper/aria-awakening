StoreTypes.LeatherWorker = 
{
	MerchantTitle = "-Weaponsmith Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Coins",
	Stock =
	{
		HuntingKnife =
		{
			DisplayName="Hunting Knife",
			Template="tool_hunting_knife",
			SellPrice=36,
			BuyPrice=12,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Hunting Knife-\n"..ToolTipTextColor.."A useful tool for skinning and cutting up things.",
		},	
		LeatherHelm =
		{
			DisplayName="Leather Helm",
			Template="armor_leather_helm",
			SellPrice=72,
			BuyPrice=24,
			MaxStock=20,
			CanSell = false,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Helm-\n"..ToolTipTextColor.."A light armor made of leather.",
		},
		LeatherLeggings =
		{
			DisplayName="Leather Leggings",
			Template="armor_leather_leggings",
			SellPrice=108,
			BuyPrice=36,
			MaxStock=20,
			CanSell = false,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Leggings-\n"..ToolTipTextColor.."A light armor made of leather.",
		},
		LeatherTunic =
		{
			DisplayName="Leather Chest",
			Template="armor_leather_chest",
			SellPrice=72,
			BuyPrice=24,
			MaxStock=20,
			CanSell = false,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Chest-\n"..ToolTipTextColor.."A light armor made of leather.",
		},
	},
}