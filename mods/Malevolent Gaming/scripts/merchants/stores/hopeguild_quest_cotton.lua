StoreTypes.HopeGuildStoreCotton = 
{
	MerchantTitle = "-Cotton Trader-",
	CurrencyType = "CottonFluffy",
	CurrencyName = "Fluffy Cotton",
	Stock =
	{
		HOPE_Gold =
		{
			DisplayName="Quest Reward - HOPE Gold (10)",
			Template="hope_guild_gold_box_10",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (10) HOPE Guild Gold for (100) Fluffy Cotton.",
		},

		Cotton_Exchange =
		{
			DisplayName="Exchange - Cotton (10)",
			Template="hope_guild_cotton_box_10",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Exchange 20 Fluffy Cotton for (10) Cotton.",
		},

		Silk_Exchange =
		{
			DisplayName="Exchange - Spider's Silk (10)",
			Template="hope_guild_silk_box_10",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Exchange 20 Fluffy Cotton for (10) Spider's Silk.",
        },
	},
}