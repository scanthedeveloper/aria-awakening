StoreTypes.NightbornApprentice = 
{
	MerchantTitle = "-Royal Apprentice-",
	CurrencyType = "ToxicBones",
	CurrencyName = "Toxic Bones",
	Stock =
	{
		NightbornGems25 =
		{
			DisplayName="Quest Reward - Toxic Bones",
			Template="nightborn_gem_box_10",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (10) Nightborn gems for (1) sample of Toxic Bones.",
		},
	},
}