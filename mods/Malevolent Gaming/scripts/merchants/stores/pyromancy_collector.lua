StoreTypes.PyromancyCollector = 
{
	MerchantTitle = "-Resin Collector-",
	CurrencyType = "Resin",
	CurrencyName = "Hot Resin",
	Stock =
	{
		NightbornGems25 =
		{
			DisplayName="Quest Reward - Pyromancy Loot Box",
			Template="furniture_box_resin",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Contains a random chance at gold, resources, rare dyes, and pyromancy items.",
		},
	},
}