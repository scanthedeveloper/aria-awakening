StoreTypes.NewPlayerRewardMerchant = 
{
	MerchantTitle = "-New Player Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Coins",	
	Stock =
	{
		Token501 =
		{
			DisplayName="1. Powerhour Potion",
			Template="newplayer_powerhour_crate",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Newplayer Reward-\n"..ToolTipTextColor.."Rewards a new player with a Power-hour potion.",
		},
		Token502 =
		{
			DisplayName="2. Jewelry Crate",
			Template="newplayer_jewelry_crate",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Newplayer Reward-\n"..ToolTipTextColor.."Rewards a new player with a ring & necklace.",
		},
		Token503 =
		{
			DisplayName="3. Cloak Crate",
			Template="newplayer_cloak_crate",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Newplayer Reward-\n"..ToolTipTextColor.."Rewards a new player with a pure black cloak.",
		},
		Token504 =
		{
			DisplayName="4. Mount Crate",
			Template="newplayer_mount_crate",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Newplayer Reward-\n"..ToolTipTextColor.."Rewards a new player with a Pyros horse mount.",
		},
	},
}