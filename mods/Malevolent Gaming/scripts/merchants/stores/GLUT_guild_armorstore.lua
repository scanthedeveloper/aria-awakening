StoreTypes.GLUTGuildArmorStore = 
{
	MerchantTitle = "-Guild Gear Merchant-",
	CurrencyType = "GuildNote",
	CurrencyName = "Guild Note",
	Stock =
	{
        --CLOTH ITEMS
		GLUT_Stone_Villa =
		{
			DisplayName="Blueprint - GLUT Villa",
			Template="blueprint_glut_stone_villa",
			SellPrice=400,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Housing Blueprint-\n"..ToolTipTextColor.."Custom GLUT Stone Villa with +100 Lockdowns and +5 Storage Bonus.",
        },
		GLUT_Stone_Estate =
		{
			DisplayName="Blueprint - GLUT Estate",
			Template="blueprint_glut_stone_estate",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Housing Blueprint-\n"..ToolTipTextColor.."Custom GLUT Stone Estate with +100 Lockdowns and +5 Storage Bonus.",
        },
		glut_Cloth_Helm =
		{
			DisplayName="Cloth-Gluthaven Mage Halo",
			Template="glut_cloth_helm",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Mana, and 1000 Durability.  Enchant it yourself!",
        },
        glut_Cloth_Tunic =
		{
			DisplayName="Cloth-Gluthaven Mage Tunic",
			Template="glut_cloth_tunic",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Mana, and 1000 Durability.  Enchant it yourself!",
        },
		glut_Cloth_Robe =
		{
			DisplayName="Cloth-Gluthaven Fancy Dress",
			Template="glut_cloth_dress",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Mana, and 1000 Durability.  Enchant it yourself!",
        },
        glut_Cloth_Leggings =
		{
			DisplayName="Cloth-Gluthaven Mage Leggings",
			Template="glut_cloth_leggings",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Mana, and 1000 Durability.  Enchant it yourself!",
        },
		--LIGHT ITEMS
		glut_Light_Helm =
		{
			DisplayName="Light-Gluthaven Leather Halo",
			Template="glut_light_helm",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Stamina, and 1000 Durability.  Enchant it yourself!",
        },
        glut_Light_Tunic =
		{
			DisplayName="Light-Gluthaven Leather Tunic",
			Template="glut_light_tunic",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Stamina, and 1000 Durability.  Enchant it yourself!",
        },
        glut_Light_Leggings =
		{
			DisplayName="Light-Gluthaven Leather Leggings",
			Template="glut_light_leggings",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Leather Armor-\n"..ToolTipTextColor.."Light armor piece with +5 Defense, +Stamina, and 1000 Durability.  Enchant it yourself!",
        },
        --PLATE ITEMS
		glut_Heavy_Helm =
		{
			DisplayName="Heavy-Gluthaven Plate Halo",
			Template="glut_plate_helm",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Plate armor piece with +5 Defense, +Hit Points, and 1000 Durability.  Enchant it yourself!",
        },
        glut_Heavy_Tunic =
		{
			DisplayName="Heavy-Gluthaven Plate Tunic",
			Template="glut_plate_tunic",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Plate armor piece with +5 Defense, +Hit Points, and 1000 Durability.  Enchant it yourself!",
        },
        glut_Heavy_Leggings =
		{
			DisplayName="Heavy-Gluthaven Plate Leggings",
			Template="glut_plate_leggings",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Plate Armor-\n"..ToolTipTextColor.."Plate armor piece with +5 Defense, +Hit Points, and 1000 Durability.  Enchant it yourself!",
        },
        --OTHER ITEMS
        glut_Cloak =
		{
			DisplayName="Cloak - Gluthaven Cloak",
			Template="glut_cloak",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Cloth Armor-\n"..ToolTipTextColor.."Linen armor piece with +Hitpoints and Health Regen V.",
		},
        glut_Shield =
		{
			DisplayName="Shield - Gluthaven Shield",
			Template="glut_shield",
			SellPrice=25,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Shield-\n"..ToolTipTextColor.."Sturdy shield with +HP and %Chance to be healed when struck.",
		},
        glut_gold_Dye =
		{
			DisplayName="Dye - Gluthaven Gold Dye",
			Template="glut_gold_dye",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Dye #827-\n"..ToolTipTextColor.."Official Guild Gold Dye (Used with Pyromancy Themes).",
        },
        glut_orange_Dye =
		{
			DisplayName="Dye - Gluthaven Orange Dye",
			Template="glut_orange_dye",
			SellPrice=15,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Dye #933-\n"..ToolTipTextColor.."Official Guild Orange Dye (Color of GLUT stone-houses).",
        },
        glut_red_Dye =
		{
			DisplayName="Dye - Gluthaven Red Dye",
			Template="glut_red_dye",
			SellPrice=15,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Dye #833-\n"..ToolTipTextColor.."Official Guild Red Dye (Unique color theme only for GLUT).",
        },
        glut_Horse =
		{
			DisplayName="Mount - Gluthaven Warg",
			Template="glut_mount_warg",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Warg Mount-\n"..ToolTipTextColor.."The vicious Warg mount of Gluthaven.",
        },
    },
}