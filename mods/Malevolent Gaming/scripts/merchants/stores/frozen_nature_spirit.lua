StoreTypes.FrozenNatureSpiritStore = 
{
	MerchantTitle = "-Nature Spirit-",
	CurrencyType = "FrozenQuestOrb",
	CurrencyName = "Frozen Orb",
	Stock =
	{
		FrozenGlowingOrb =
		{
			DisplayName="Quest Reward - Frozen Loot Box",
			Template="frozen_mage_lootbox",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards the player with a frozen lootbox.",
		},
	},
}