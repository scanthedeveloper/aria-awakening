StoreTypes.NightbornRogue = 
{
	MerchantTitle = "-Nightborn Rogue-",
	CurrencyType = "NightbornGem",
	CurrencyName = "Nightborn Gems",
	Stock =
	{
		NightbornRing =
		{
			DisplayName="Nightborn Curiosity Ring",
			Template="nightborn_curiosity_ring",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Jewelry-\n"..ToolTipTextColor.."A Nightborn Themed ring that gives +Carry, +Agility, +Lockpicking & +Movement Speed.",
		},
		NightbornCloak =
		{
			DisplayName="Nightborn Curiosity Cloak",
			Template="nightborn_curiosity_cloak",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Housing Blueprint-\n"..ToolTipTextColor.."A Nightborn Themed cloak that gives +Carry, +Constitution, +Lockpicking & +Movement Speed.",
        },
	},
}