StoreTypes.ArmorSmith = 
{
	MerchantTitle = "-Armorsmith Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Coins",	
	Stock =
	{	
		ChainHelm =
		{
			DisplayName="Chain Helm",
			Template="armor_chain_helm",
			SellPrice=36,
			BuyPrice=12,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Chain Helm-\n"..ToolTipTextColor.."A heavy armor made of chain.",
		},
		ChainLeggings =
		{
			DisplayName="Chain Leggings",
			Template="armor_chain_leggings",
			SellPrice=108,
			BuyPrice=36,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Chain Leggings-\n"..ToolTipTextColor.."A heavy armor made of chain.",
		},
		ChainTunic =
		{
			DisplayName="Chain Tunic",
			Template="armor_chain_tunic",
			SellPrice=108,
			BuyPrice=36,
			MaxStock=20,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Chain Tunic-\n"..ToolTipTextColor.."A heavy armor made of chain.",
		},
	},
}