StoreTypes.BulkTailoringMerchant = 
{
	MerchantTitle = "-Bulk Tailoring Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Platinum",
	Stock =
	{
		--[[Dagger =
		{
			DisplayName="Dagger",
			Template="weapon_dagger",
			SellPrice=36,
			BuyPrice=0,
			MaxStock=20,
			CanSell = false,
			CanBuy = false,
			Tooltip = ToolTipHeaderColor.."-Dagger-\n"..ToolTipTextColor.."A sharp quick weapon that requires the peircing skill to be effective.",
			Conditions = 
			{
				{Type="ObjVarEqual",Value="TestDaggerReq|test"},
				{Type="ObjVarGreaterThan",Value="BiggerThan|25"},
				{Type="ObjVarLessThan",Value="LessThan|25"},
				{Type="SkillGreaterThan",Value="Fabrication|50"},
				{Type="SkillLessThan",Value="Healing|50"},
			},
		},]]--

        --CLOTH
		BulkClothRecipe =
		{
			DisplayName="1. Bulk Cloth Recipe",
			Template="recipe_bulk_cloth",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine cloth (500).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Fabrication|59"},
			},
		},
		BulkQuiltedRecipe =
		{
			DisplayName="3. Bulk Quilted Recipe",
			Template="recipe_bulk_quilted",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine quilted cloth (500).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Fabrication|79"},
			},
		},
		BulkSilkRecipe =
		{
			DisplayName="5. Bulk Silk Recipe",
			Template="recipe_bulk_silk",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine silk cloth (500).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Fabrication|99"},
			},
		},

        --LEATHER
        BulkLeatherhRecipe =
		{
			DisplayName="2. Bulk Leather Recipe",
			Template="recipe_bulk_leather",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine leather (300).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Fabrication|59"},
			},
		},
		BulkBeastLeatherRecipe =
		{
			DisplayName="4. Bulk Beast Leather Recipe",
			Template="recipe_bulk_beastleather",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine beast leather (300).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Fabrication|79"},
			},
		},
		BulkVileLeatherRecipe =
		{
			DisplayName="6. Bulk Vile Leather Recipe",
			Template="recipe_bulk_vileleather",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine vile leather (300).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Fabrication|99"},
			},
		},
	},
}