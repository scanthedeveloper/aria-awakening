StoreTypes.BulkCarpentryMerchant = 
{
	MerchantTitle = "-Bulk Carpentry Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Platinum",
	Stock =
	{
		--[[Dagger =
		{
			DisplayName="Dagger",
			Template="weapon_dagger",
			SellPrice=36,
			BuyPrice=0,
			MaxStock=20,
			CanSell = false,
			CanBuy = false,
			Tooltip = ToolTipHeaderColor.."-Dagger-\n"..ToolTipTextColor.."A sharp quick weapon that requires the peircing skill to be effective.",
			Conditions = 
			{
				{Type="ObjVarEqual",Value="TestDaggerReq|test"},
				{Type="ObjVarGreaterThan",Value="BiggerThan|25"},
				{Type="ObjVarLessThan",Value="LessThan|25"},
				{Type="SkillGreaterThan",Value="Fabrication|50"},
				{Type="SkillLessThan",Value="Healing|50"},
			},
		},]]--

        --CARPENTRY
		BulkBrickRecipe =
		{
			DisplayName="1. Bulk Brick Recipe",
			Template="recipe_bulk_brick",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine brick (100).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Woodsmith|59"},
			},
		},
        BulkWoodRecipe =
		{
			DisplayName="2. Bulk Wood Recipe",
			Template="recipe_bulk_boards",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine wood boards (100).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Woodsmith|59"},
			},
		},
        BulkAshRecipe =
		{
			DisplayName="3. Bulk Ash Recipe",
			Template="recipe_bulk_ashboards",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine ash boards (100).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Woodsmith|79"},
			},
		},
        BulkBlightwoodRecipe =
		{
			DisplayName="4. Bulk Blightwood Recipe",
			Template="recipe_bulk_blightwoodboards",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine blightwood boards (100).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Woodsmith|79"},
			},
		},
        BulkDreamwoodRecipe =
		{
			DisplayName="5. Bulk Dreamwood Recipe",
			Template="recipe_bulk_dreamwood",
			SellPrice=100000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Recipe-\n"..ToolTipTextColor.."Recipe that allows a user to bulk craft & refine dreamwood boards (100).",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="Woodsmith|99"},
			},
		},
	},
}