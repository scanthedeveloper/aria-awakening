StoreTypes.HopeGuildEnchantingStore = 
{
	MerchantTitle = "-Guild Merchant-",
	CurrencyType = "HopeGem",
	CurrencyName = "HOPE Gold",
	Stock =
	{
		Essence_Con =
		{
			DisplayName="Essence of Health (1-10)",
			Template="essence_constitution_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchant an armor item with a 10-50 chance of gaining hitpoints.",
		},
		Essence_Str =
		{
			DisplayName="Essence of Strength (1-10)",
			Template="essence_strength_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchant an armor item with a 1-10 chance of gaining Strength.",
		},
		Essence_Agil =
		{
			DisplayName="Essence of Agility (1-10)",
			Template="essence_agility_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchant an armor item with a 1-10 chance of gaining Agility.",
		},
		Essence_Int =
		{
			DisplayName="Essence of Intelligence (1-10)",
			Template="essence_intelligence_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchant an armor item with a 1-10 chance of gaining Intelligence.",
		},
		Essence_Wis =
		{
			DisplayName="Essence of Wisdom (1-10)",
			Template="essence_wisdom_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchant an armor item with a 1-10 chance of gaining Wisdom.",
		},
		Essence_Will =
		{
			DisplayName="Essence of Will (1-10)",
			Template="essence_will_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants an armor item with a 1-10 chance of gaining Wisdom.",
		},
		Essence_Poison =
		{
			DisplayName="Essence of Poison (1-25)",
			Template="essence_poison_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a weapon item with a 1-25% chance of gaining Poison.",
		},
		Essence_BloodlustRegen =
		{
			DisplayName="Essence of Bloodlust Regen (.5-1)",
			Template="essence_bloodlustregen_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a bonus regenerates bloodlust passively, only usable on a shield.",
		},
		Essence_Defense=
		{
			DisplayName="Essence of Defense (1-5)",
			Template="essence_defense_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a (1-5) defense bonus, only usable on armor.",
		},
		Essence_HealingPower=
		{
			DisplayName="Essence of Healing Power (2-10)",
			Template="essence_healingpower_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a (5-25) healing power bonus, only usable on weapons.",
		},
		Essence_Mana=
		{
			DisplayName="Essence of Mana (5-10)",
			Template="essence_mana_greater",
			SellPrice=5,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a (5-10) mana total bonus, only usable on armor.",
		},
		Essence_Movement=
		{
			DisplayName="Essence of Movement (10-20)",
			Template="essence_movement_greater",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a (10-20%) run speed bonus, only usable on a weapon.",
		},
		Essence_Silencing=
		{
			DisplayName="Essence of Silence (5-15)",
			Template="essence_silencing_greater",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a (5-15%) chance to silence an enemy, only usable on a weapon.",
		},
		Essence_Stunning=
		{
			DisplayName="Essence of Stunning (5-15)",
			Template="essence_stunning_greater",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a (5-15%) chance to stun an enemy, only usable on a weapon.",
		},
		Essence_Sundering=
		{
			DisplayName="Essence of Sundering (5-15)",
			Template="essence_sunder_greater",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a (5-15%) chance to sunder an enemy, only usable on a weapon.",
		},
		Essence_Carrying=
		{
			DisplayName="Essence of Weight (10-50)",
			Template="essence_carrying_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a (10-50) bonus to total weight, only usable on a armor.",
		},
		Essence_Stamina=
		{
			DisplayName="Essence of Stamina (5-20)",
			Template="essence_stamina_greater",
			SellPrice=10,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Enchanting Essence-\n"..ToolTipTextColor.."Enchants a (5-20) bonus to total stamina, only usable on a armor.",
		},
	},
}