StoreTypes.BlackpathFarmer = 
{
	MerchantTitle = "-Blackpath Farmer-",
	CurrencyType = "Wheat",
	CurrencyName = "Dark Seed",
	Stock =
	{
		NightbornGems25 =
		{
			DisplayName="Quest Reward - Dark Seed",
			Template="blackpath_gem_box_25",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (25) Blackpath Blood Runes for (1) Blackpath Dark Seed.",
		},
	},
}