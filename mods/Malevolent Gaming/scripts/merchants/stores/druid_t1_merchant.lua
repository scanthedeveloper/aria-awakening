StoreTypes.DruidTier1Merchant = 
{
	MerchantTitle = "-Druid Armor Merchant-",
	CurrencyType = "coins",
	CurrencyName = "Platinum",
	Stock =
	{
		--[[Dagger =
		{
			DisplayName="Dagger",
			Template="weapon_dagger",
			SellPrice=36,
			BuyPrice=0,
			MaxStock=20,
			CanSell = false,
			CanBuy = false,
			Tooltip = ToolTipHeaderColor.."-Dagger-\n"..ToolTipTextColor.."A sharp quick weapon that requires the peircing skill to be effective.",
			Conditions = 
			{
				{Type="ObjVarEqual",Value="TestDaggerReq|test"},
				{Type="ObjVarGreaterThan",Value="BiggerThan|25"},
				{Type="ObjVarLessThan",Value="LessThan|25"},
				{Type="SkillGreaterThan",Value="Fabrication|50"},
				{Type="SkillLessThan",Value="Healing|50"},
			},
		},]]--
		PatronNinjatoRecipe =
		{
			DisplayName="Druid Summoner's Set",
			Template="demonborn_lootcrate_set_druid",
			SellPrice=500000,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Armor Kit-\n"..ToolTipTextColor.."Contains a mask, tunic, leggings, horns, and staff.",
			Conditions = 
			{
				{Type="SkillGreaterThan",Value="NatureMagic|99"},
			},
		},
	},
}