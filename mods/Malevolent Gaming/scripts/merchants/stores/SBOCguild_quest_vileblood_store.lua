StoreTypes.SBOCGuildStoreVileBlood = 
{
	MerchantTitle = "-Blood Merchant-",
	CurrencyType = "VileBlood",
	CurrencyName = "Vile Blood",
	Stock =
	{
		SBOC_Gold10 =
		{
			DisplayName="1. SBoC Sacred Blades (10)",
			Template="sboc_guild_gold_box_10",
			SellPrice=50,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (10) Sacred Blades for (50) Vile Blood.",
		},
		SBOC_Gold50 =
		{
			DisplayName="2. SBoC Sacred Blades (50)",
			Template="sboc_guild_gold_box_50",
			SellPrice=250,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (50) Sacred Blades for (250) Vile Blood.",
		},
		SBOC_Gold100 =
		{
			DisplayName="3. SBoC Sacred Blades (100)",
			Template="sboc_guild_gold_box_100",
			SellPrice=500,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards (100) Sacred Blades for (500) Vile Blood.",
		},
	},
}