StoreTypes.RebelFarmerQuest = 
{
	MerchantTitle = "-Pyro's Farmer-",
	CurrencyType = "RebelQuestHead",
	CurrencyName = "Rebel Heads",
	Stock =
	{
		NightbornGems25 =
		{
			DisplayName="Quest - Rebel Heads",
			Template="furniture_box_rebel",
			SellPrice=20,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Quest Reward-\n"..ToolTipTextColor.."Rewards a loot box containing a random assortment of mini artifacts, resources, and decorations.",
		},
	},
}