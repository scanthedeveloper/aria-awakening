StoreTypes.PatreonHatchetTrader = 
{
	MerchantTitle = "-Patreon Hatchet Trader-",
	CurrencyType = "BrokenGodHatchet",
	CurrencyName = "Patreon God Hatchets",
	Stock =
	{
		GodHatchet =
		{
			DisplayName="Tool - God Hatchet",
			Template="SCAN_harvest_GOD_hatchet2",
			SellPrice=1,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Tool-\n"..ToolTipTextColor.."A pre-enchanted hatchet that provides, strength, speed, and weight bonuses.",
        },
	},
}