StoreTypes.NightbornArtist = 
{
	MerchantTitle = "-Nightborn Artist-",
	CurrencyType = "NightbornGem",
	CurrencyName = "Nightborn Gems",
	Stock =
	{
		NightHedge =
		{
			DisplayName="Night Hedge",
			Template="night_hedge_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A purple, Nightborn themed hedge.",
		},
		FuryHedge =
		{
			DisplayName="Fury Hedge",
			Template="nightborn_fury_hedge_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A orange, Nightborn themed hedge.",
		},
		FuryLeaf =
		{
			DisplayName="Fury Leaf",
			Template="nightborn_fury_leaf_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A orange, Nightborn themed leaf plant.",
		},
		FuryRose =
		{
			DisplayName="Fury Rose",
			Template="nightborn_fury_rose_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A orange, Nightborn themed rose plant.",
		},
		NightLeaf =
		{
			DisplayName="Night Leaf",
			Template="night_leaf_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A purple, Nightborn themed leaf plant.",
		},
		DreamLeaf =
		{
			DisplayName="Dream Leaf",
			Template="nightborn_dream_leaf_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A green, Nightborn themed leaf plant.",
		},
		DreamTree =
		{
			DisplayName="Dream Tree",
			Template="nightborn_dream_tree_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A green, Nightborn themed potted tree.",
		},
		FallingGlowingMushroom =
		{
			DisplayName="Glowing Mushroom - Falling",
			Template="nightborn_falling_glowing_mushroom_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A large glowing mushroom that is falling over.",
		},
		LeftGlowingMushroom =
		{
			DisplayName="Glowing Mushroom - Left",
			Template="nightborn_left_glowing_mushroom_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A large glowing mushroom that is leaning left.",
		},
		RightGlowingMushroom =
		{
			DisplayName="Glowing Mushroom - Right",
			Template="nightborn_right_glowing_mushroom_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A large glowing mushroom that is leaning right.",
		},
		NightbornBlackForestMap =
		{
			DisplayName="Nightborn Black Forest Map",
			Template="nightborn_blackforest_map_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A Nightborn themed map of Celador with focus on the Black Forest.",
		},
		NightbornExistencePainting =
		{
			DisplayName="Nightborn Existence Painting",
			Template="nightborn_existenence_painting_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A Nightborn themed map of Celador with focus on the Black Forest.",
		},
		NightbornChevronRug =
		{
			DisplayName="Nightborn Chevron Rug",
			Template="night_sewn_cheveron_carpet_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A Nightborn themed chevron patterned rug.",
		},
		NightbornOrnateRug =
		{
			DisplayName="Nightborn Ornate Rug",
			Template="night_sewn_ornate_rug_packed",
			SellPrice=100,
			BuyPrice=0,
			MaxStock=9999,
			CanSell = true,
			CanBuy = true,
			Tooltip = ToolTipHeaderColor.."-Decoration-\n"..ToolTipTextColor.."A Nightborn themed decorative rug.",
		},
	},
}