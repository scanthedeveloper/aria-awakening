Styles = {}

Styles.Dark =
{
    Header = 
    {
        Colors = 
        {
            BackGround = "353535",
            BackGroundFrame = "757575",
        },
    },
    Body =
    {
        Colors = 
        {
            BackGround = "050505",
            BackGroundFrame = "757575",
        },
    },
    Portrait =
    {
        Colors = 
        {
            BackGround = "121212",
            BackGroundFrame = "757575",
        },    
    },
    SellToPlayerWindow =
    {
        Colors = 
        {
            Text = 
            {
                ItemName = "[FFFFBB]",
                Quantity = "[FFFFBB]",
                Cost = "[FFFFBB]",
            },
        },
    },
    MerchantDialogText =
    {
    	Colors =
    	{
    		Merchant = "[CCCCCC]",
    		Player = "[FFFF44]",
    	},
    }
}