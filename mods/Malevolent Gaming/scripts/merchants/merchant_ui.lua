--#########################################################
--				 DYNAMIC WINDOW FUNCTIONS
--#########################################################
--- this will draw the close button on a dynamic window
-- @param x - this is the x location to draw the close button at
-- @param y - this is the y location to draw the close button at
-- @param dynamicWindow - this is the dynamic window to attach the close button too
function DrawCloseButton(x,y,dynamicWindow)
    --border background color
    dynamicWindow:AddImage(x,y,"HueSwatch",18,18,"Sliced","555555")
    --background color
    dynamicWindow:AddImage(x+2,y+2,"HueSwatch",14,14,"Sliced","252525")
    --invisible button
    dynamicWindow:AddButton(x,y,"CloseButton","",18,18,LocalizationTable.CloseButton,"",true,"Invisible")
    --draw the "X" string
    dynamicWindow:AddLabel(x+5,y+3,"[959555]X",18,18,18,"left")
end

--- This will draw the dialog window from the dialog data tables
-- @param x - the x position
-- @param y - the y posttion
-- @param dynamicWindow - the dynamicWindow to draw the dialog on
-- @param dialogNode - the current node to display
-- @param user - the user of the dialog
function DrawDialog(x,y,dynamicWindow,dialognode,user)
    --MerchantDialog
    local dialog = this:GetObjVar("Data.Dialog")
    dynamicWindow:AddLabel(x,y,Styles.Dark.MerchantDialogText.Colors.Merchant..Dialog[dialog][dialognode].NpcText,300,96,16,"left")
    local y_pos = y+66
    for k,v in pairs(Dialog[dialog][dialognode].Responses) do
        if (MeetsDialogRequirements(user,v.Req)) then
           local responseInfo = StringSplit(v.Text,"|")
           y_pos = y_pos + 16
           dynamicWindow:AddLabel(x,y_pos,Styles.Dark.MerchantDialogText.Colors.Player..responseInfo[1],300,16,16,"left")
           dynamicWindow:AddButton(x,y_pos,"DialogAction|"..v.Text,"",300,16,"","",true,"Invisible")
        end
    end
end

--- Returns true/false based on if the player meets the conditions to purchase.
-- @param user - the user opening up the merchant sell window
-- @param storeItemTable - the item table to check the conditons of.
-- Currently Supports the following checks
-- Objvar = value
-- ObjVar > value
-- ObjVar < value
-- Skill > value
-- Skill < value
function MeetsItemBuyRequirements(user, storeItemTable)
    if(#storeItemTable.Conditions <=0) then return true end
    local retunVal = true
    if(storeItemTable.Conditions) then
        for index,condition in pairs(storeItemTable.Conditions) do
            if(type(condition)=="table") then
                local objVarData = StringSplit(condition.Value,"|")
                if(condition.Type == "ObjVarEqual") then
                    if(user:HasObjVar(objVarData[1])) then
                        local objVar = user:GetObjVar(objVarData[1])
                        if(objVar == objVarData[2]) then 
                            returnVal = true
                        else
                            returnVal = false
                        end
                    end 
                end
                if(condition.Type == "ObjVarGreaterThan") then
                    if(user:HasObjVar(objVarData[1])) then
                        if(tonumber(user:GetObjVar(objVarData[1])) > tonumber(objVarData[2])) then
                            returnVal = true
                        else
                            returnVal = false
                        end
                    else
                        returnVal = false
                    end
                end                
                if(condition.Type == "ObjVarLessThan") then
                    if(user:HasObjVar(objVarData[1])) then
                        if(tonumber(user:GetObjVar(objVarData[1])) < tonumber(objVarData[2])) then
                            returnVal = true
                        else
                            returnVal = false
                        end
                    else
                        returnVal = false
                    end
                end
                if(condition.Type == "SkillGreaterThan") then
                    local skillData = StringSplit(condition.Value,"|")
                    local skillDictionary = user:GetObjVar("SkillDictionary") or {}
                    if(skillDictionary[skillData[1].."Skill"]) then
                        if(skillDictionary[skillData[1].."Skill"].SkillLevel > tonumber(skillData[2])) then
                            returnVal = true
                        else
                            returnVal = false
                        end
                    else
                        returnVal = false
                    end
                end
                if(condition.Type == "SkillLessThan") then
                    local skillData = StringSplit(condition.Value,"|")
                    local skillDictionary = user:GetObjVar("SkillDictionary") or {}
                    if(skillDictionary[skillData[1].."Skill"]) then
                        if(skillDictionary[skillData[1].."Skill"].SkillLevel < tonumber(skillData[2])) then
                            returnVal = true
                        else
                            returnVal = false
                        end
                    else
                        returnVal = false
                    end
                end                     
            end                          
        end
    end
    return returnVal
end

--- This opens the main dynamic window user interface
-- @param user - player object to open the window for
-- @param merchant - object to handle the events
function OpenMerchantDialog(user,merchant,dialognode)
	if(dialognode == nil) then dialognode = "StartNode" end
    local style = merchant:GetObjVar("Data.UIStyle") or "Dark"
	--register event handler for dynamic window
	RegisterEventHandler(EventType.DynamicWindowResponse,DynamicWindowResponseHandler..merchant.Id, HandleDynamicWindowResponse)
	--create the dynamic window
    local dynamicWindow = DynamicWindow(DynamicWindowResponseHandler..merchant.Id,"",500,180,-200,-420,"TransparentDraggable","Bottom")
    --Create Background
    dynamicWindow:AddImage(1,1,"HueSwatch",500,180,"Simple",Styles[style].Body.Colors.BackGround)
    --Create Header Background
    dynamicWindow:AddImage(1,1,"HueSwatch",500,34,"Simple",Styles[style].Header.Colors.BackGround)
    DrawCloseButton(471,9,dynamicWindow,"MerchantDialog")
    --Create Header Panel
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",506,40,"Sliced",Styles[style].Header.Colors.BackGroundFrame)
    --Merchants Name
    dynamicWindow:AddLabel(85,10,merchant:GetName(),170,20,20,"center")
    dynamicWindow:AddLabel(320,10,merchant:GetSharedObjectProperty("Title"),300,20,20,"center")

    --Create Background of Portrait
    dynamicWindow:AddImage(1,32,"HueSwatch",178,150,"Simple",Styles[style].Portrait.Colors.BackGround)
        
    --Create Portrait Panel
    dynamicWindow:AddImage(-2,30,"CraftingItemsFrame",180,154,"Sliced",Styles[style].Portrait.Colors.BackGroundFrame)
    --Add portrait of Merchant
    dynamicWindow:AddPortrait(2,5,170,170,merchant,"head",true)
        
    --Add body panel                                   233
    dynamicWindow:AddImage(171,30,"CraftingItemsFrame",333,154,"Sliced",Styles[style].Body.Colors.BackGroundFrame)
    
    DrawDialog(190,40,dynamicWindow,dialognode,user)
    user:OpenDynamicWindow(dynamicWindow,merchant)
end

--- This function opens up the vendors inventory for players to purchase from
-- @param user - player object to open the window for
-- @param merchant - object to handle the events-
function OpenPlayerBuyFromVendorDialog(user,merchant)
    local style = merchant:GetObjVar("Data.UIStyle") or "Dark"
    --register event handler for dynamic window
    RegisterEventHandler(EventType.DynamicWindowResponse,DynamicWindowResponseHandler..merchant.Id, HandleDynamicWindowResponse)
    --create the dynamic window
    local dynamicWindow = DynamicWindow(DynamicWindowResponseHandler..merchant.Id,"",500,382,-250,-420,"TransparentDraggable","Bottom")
    --Create Background
    dynamicWindow:AddImage(1,1,"HueSwatch",500,382,"Simple",Styles[style].Body.Colors.BackGround)
    --Create Header Background
    dynamicWindow:AddImage(1,1,"HueSwatch",500,34,"Simple",Styles[style].Header.Colors.BackGround)
    DrawCloseButton(471,9,dynamicWindow,"PlayerBuyFromVendor")
    --Create Header Panel
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",506,40,"Sliced",Styles[style].Header.Colors.BackGroundFrame)

    local currencyType = StoreTypes[this:GetObjVar("Data.StoreType")].CurrencyName
    dynamicWindow:AddLabel(250,10,"[959555]What would you like to purchase with your ".. currencyType .."?",500,20,20,"center")

    local sortedTable = {}
    local scrollWindow = ScrollWindow(11,39,468,340,40)
    for i,j in pairs(merchant:GetObjVar("Data.StoreInfo")) do table.insert(sortedTable,j) end
    table.sort(sortedTable,function (a,b)
        if (a.DisplayName < b.DisplayName) then
            return true
        else
            return false
        end
    end)    
    for k,v in pairs(sortedTable) do
        if(v.CanBuy == true and v.CurrentStock > 0) then
            if(MeetsItemBuyRequirements(user,v)) then
                local scrollElement = ScrollElement()
                scrollElement:AddLabel(1,10,Styles[style].SellToPlayerWindow.Colors.Text.ItemName..v.DisplayName,465,24,24,"left")
                --scrollElement:AddLabel(255,10,Styles[style].SellToPlayerWindow.Colors.Text.Quantity..v.CurrentStock.."/"..v.MaxStock)
                if(v.SellPrice < 100) then
                    if(merchant:GetObjVar("Data.CurrencyType") == "coins") then
                        scrollElement:AddLabel(300,10,Styles[style].SellToPlayerWindow.Colors.Text.ItemName,200,24,24,"left")
                        scrollElement:AddLabel(340,10,"[959555]"..v.SellPrice.." [FF7700]g",80,24,24,"right")
                    else
                        scrollElement:AddLabel(300,10,Styles[style].SellToPlayerWindow.Colors.Text.ItemName,200,24,24,"left")
                        scrollElement:AddLabel(340,10,"[959555]"..v.SellPrice,80,24,24,"right")
                    end
                else
                    if(merchant:GetObjVar("Data.CurrencyType") == "coins") then
                        scrollElement:AddLabel(300,10,Styles[style].SellToPlayerWindow.Colors.Text.ItemName,200,24,24,"left")
                        scrollElement:AddLabel(340,10,"[959555]"..GetMoneyString(v.SellPrice),80,24,24,"right")
                    else
                        scrollElement:AddLabel(300,10,Styles[style].SellToPlayerWindow.Colors.Text.ItemName,200,24,24,"left")
                        scrollElement:AddLabel(340,10,"[959555]"..v.SellPrice,80,24,24,"right")
                    end
                end
                scrollElement:AddButton(1,10,"","",200,24,v.Tooltip,"",false,"Invisible")

                scrollElement:AddImage(-10,-5,"CraftingItemsFrame",390,48,"Sliced",Styles[style].Body.Colors.BackGroundFrame)
                --create button
                scrollElement:AddImage(380,1,"HueSwatch",70,39,"Simple","333333")
                scrollElement:AddImage(373,-5,"CraftingItemsFrame",82,48,"Sliced",Styles[style].Body.Colors.BackGroundFrame)
                scrollElement:AddLabel(414,10,"[959555]Buy",82,24,24,"center")
                scrollElement:AddButton(373,1,"BuyShopItem|"..v.Template.."~"..v.SellPrice.."~"..v.DisplayName.."~"..v.Tooltip,"",82,38,"[FF7700]Purchase "..v.DisplayName.."\n[FFFFAA]Click Buy to purchase this "..v.DisplayName,"",false,"Invisible")
                scrollWindow:Add(scrollElement)
            end
        end
    end      
   
    dynamicWindow:AddScrollWindow(scrollWindow)
    --Add body panel                                  
    dynamicWindow:AddImage(-2,30,"CraftingItemsFrame",506,356,"Sliced",Styles[style].Body.Colors.BackGroundFrame)
    --Open the dynamicWindow for the player and allowing the test tool
    --to handle all the events and clicks from the window.
    
    user:OpenDynamicWindow(dynamicWindow,merchant)
end

--- This will open up the confirmation on the purchase of a inventory item
-- @param user - player object to open the window for
-- @param merchant - object to handle the events-
-- @param template - the item template to purchase
-- @param price - the price of the item in coppper
-- @param displayName - the display name of the item your purchasing
function OpenPurchaseConfirmationDialog(user,merchant,template,price,displayName)
    if(style == nil)then style = "Dark" end
    --register event handler for dynamic window
    RegisterEventHandler(EventType.DynamicWindowResponse,DynamicWindowResponseHandler..merchant.Id, HandleDynamicWindowResponse)
    --create the dynamic window
    local dynamicWindow = DynamicWindow(DynamicWindowResponseHandler..merchant.Id,"",500,84,-250,-620,"TransparentDraggable","Bottom")
    --Create Background
    dynamicWindow:AddImage(1,1,"HueSwatch",500,84,"Simple",Styles[style].Body.Colors.BackGround)
    --Create Header Background
    dynamicWindow:AddImage(1,1,"HueSwatch",500,34,"Simple",Styles[style].Header.Colors.BackGround)
    DrawCloseButton(471,9,dynamicWindow,"PurchaseConfirm")
    --Create Header Panel
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",506,40,"Sliced",Styles[style].Header.Colors.BackGroundFrame)
    --Merchants Name
    dynamicWindow:AddLabel(250,10,"[959555]Confirmation of item and price?",500,20,20,"center")
    --Add body panel                                  
    dynamicWindow:AddImage(-2,30,"CraftingItemsFrame",506,58,"Sliced",Styles[style].Body.Colors.BackGroundFrame)
    --Open the dynamicWindow for the player and allowing the test tool
    --to handle all the events and clicks from the window.
    dynamicWindow:AddImage(348,41,"HueSwatch",70,39,"Simple","333333")
    dynamicWindow:AddImage(341,35,"CraftingItemsFrame",82,48,"Sliced",Styles[style].Body.Colors.BackGroundFrame)
    dynamicWindow:AddLabel(382,50,"[959555]Yes",82,24,24,"center")
    dynamicWindow:AddButton(341,41,"ConfirmPurchaseYes|"..template..","..price,"",82,38,"Click Yes to purchase this "..displayName,"",true,"Invisible")    

    dynamicWindow:AddImage(423,41,"HueSwatch",70,39,"Simple","333333")
    dynamicWindow:AddImage(416,35,"CraftingItemsFrame",82,48,"Sliced",Styles[style].Body.Colors.BackGroundFrame)
    dynamicWindow:AddLabel(457,50,"[959555]No",82,24,24,"center")
    dynamicWindow:AddButton(416,41,"ConfirmPurchaseNo|"..template..","..price,"",82,38,"Click No to decline purchasing this "..displayName,"",true,"Invisible")    
    if(price < 100) then
        if(merchant:GetObjVar("Data.CurrencyType") == "coins") then
            dynamicWindow:AddLabel(12,51,"[959555]Purchase a "..displayName,320,24,24,"left")
            dynamicWindow:AddLabel(340,51,"[959555]"..price.." [FF7700]g",80,24,24,"right")
        else
            dynamicWindow:AddLabel(12,51,"[959555]Purchase a "..displayName,320,24,24,"left")
            dynamicWindow:AddLabel(340,51,"[959555]"..price,80,24,24,"right")
        end
    else
        if(merchant:GetObjVar("Data.CurrencyType") == "coins") then
            dynamicWindow:AddLabel(12,51,"[959555]Purchase a "..displayName,320,24,24,"left")
            dynamicWindow:AddLabel(340,51,GetMoneyString(price),80,24,24,"right")
        else
            dynamicWindow:AddLabel(12,51,"[959555]Purchase a "..displayName,320,24,24,"left")
            dynamicWindow:AddLabel(340,51,"[959555]"..price,80,24,24,"right")            
        end 
    end
    user:OpenDynamicWindow(dynamicWindow,merchant)    
end

---This just takes an amount of coins and splits it into a string showing the money value
-- @param coins - the amount of copper you want to convert to a string value
function GetMoneyString(coins)
    local plat = 0
    local gold = 0
    local silver = 0
    local copper = 0
    if(coins > 999999) then --platinum
        while (coins > 999999) do
            plat = plat + 1
            coins = coins - 1000000
        end
    end
    if(coins > 9999) then --gold
        while (coins > 9999) do
            gold = gold + 1
            coins = coins - 10000
        end
    end
    if(coins > 99) then --silver
        while (coins > 99) do
            silver = silver + 1
            coins = coins - 100
        end
    end
    local returnString = "[959555]"
    if(plat > 0) then returnString = returnString .. plat .. "[BBBBBB]p[959555]" end
    if(gold > 0) then returnString = returnString .. gold .. "[BBBB00]p[959555]" end
    if(silver > 0) then returnString = returnString .. silver .. "[939393]g[959555]" end
    if(coins > 0) then returnString = returnString .. coins .. "[FF7700]g[959555]" end
    return returnString
end

--- This handles the check to see if the player has enough currency to buy the item
-- @param - user - the user of the dynamic window
-- @param - resourceType - the resource used to purchase the item
-- @price - the price in copper of the item they player is trying to buy
function HandleConsumeResource(user,resourceType,price)
    local success = false

    local backpackObj = user:GetEquippedObject("Backpack")
    if( backpackObj ~= nil and CountResourcesInContainer(backpackObj,resourceType) >= price ) then
        local resourceObjs = GetResourcesInContainer(backpackObj,resourceType)
        -- sort stackable objects from smallest to largest
        table.sort(resourceObjs,function(a,b) return GetStackCount(a)<GetStackCount(b) end)
        local remainingAmount = price
        for index, resourceObj in pairs(resourceObjs) do
            local resourceCount = GetStackCount(resourceObj)
            if( resourceCount > remainingAmount ) then              
                RequestSetStackCount(resourceObj,resourceCount - remainingAmount)
                remainingAmount = 0
            else
                remainingAmount = remainingAmount - resourceCount
                resourceObj:Destroy()
            end

            if( remainingAmount == 0 ) then
                break
            end
        end

        success = true
    end
    return success
end


--- this handles the dynamic window response
-- @param user - player object to open the window for
-- @param buttonId - the dynamic window button id clicked/used
-- @param fieldData - holds the the textfield data on a dynamic window
function HandleDynamicWindowResponse(user,buttonId,fieldData)
    local responseData = StringSplit(buttonId,"|")
    local button = responseData[1]
    local args = responseData[2]
    local dataTable = this:GetObjVar("Data.StoreInfo")
    if(button=="Buy") then
        OpenPlayerBuyFromVendorDialog(user,this)
    end
    if(button=="BuyShopItem") then
        local splitArgs = StringSplit(args,"~")
        local template = splitArgs[1]
        local price = tonumber(splitArgs[2])
        local displayName = splitArgs[3]
        local tooltip = splitArgs[4]
        OpenPurchaseConfirmationDialog(user,this,template,price,displayName,tooltip)
    end
    if(button=="DialogAction") then
    	if(Dialog.Responses[responseData[3]]) then
    		Dialog.Responses[responseData[3]](user,this,buttonId)
    	end
    end
    if(button=="ConfirmPurchaseYes") then
    	local splitArgs = StringSplit(args,",")
    	local template = splitArgs[1]
    	local price = tonumber(splitArgs[2])
    	if(HandlePurchase(user,template,price) == true) then
            local store = this:GetObjVar("Data.StoreInfo")
            for index,storeItem in pairs(store) do
                for storeDataType, storeDataValue in pairs(storeItem) do
                    if(storeDataType=="Template" and storeDataValue==template) then
                        store[index]["CurrentStock"] = store[index]["CurrentStock"] - 1
                        this:SetObjVar("Data.StoreInfo",store)
                        return 
                    end
                end
            end
        end
    end
    if(button=="SellShopItem") then
        local splitArgs = StringSplit(args,"~")
        local currencyCount = tonumber(splitArgs[2])
        local currencyType = this:GetObjVar("Data.CurrencyType")
        if(currencyCount > 0) then
            local backpack = user:GetEquippedObject("Backpack")
            if(currencyType == "coins") then
                CreateObjInContainer("coin_purse", backpack, GetRandomDropPosition(backpack), "PayPlayer", currencyCount)
                GameObj(tonumber(splitArgs[1])):Destroy()
            else
                local template = ResourceData.ResourceInfo[currencyType].Template
                if(template == nil) then
                    Print("Error with NPC:"..this.Id..", bad CurrencyType object variable")
                    CallFunctionDelayed(TimeSpan.FromMilliseconds(20),function() OpenPlayerSellToVendorDialog(user,this) end)
                    return
                end
                CreateObjInContainer(template, backpack, GetRandomDropPosition(backpack), "PayPlayer", currencyCount)
            end
        end
        CallFunctionDelayed(TimeSpan.FromMilliseconds(20),function() OpenPlayerSellToVendorDialog(user,this) end)
    end
end

RegisterEventHandler(EventType.CreatedObject, "PayPlayer", function(success,objRef,quantity)
    if(success == true) then
        if( quantity ~= nil and quantity > 0 ) then
            RequestSetStackCount(objRef,quantity)
        end
    end
end)

--- This handles the purchasing of item from the vendor store
-- @param user - the user of the dyanamic window
-- @param template - the template the player is purchasing
-- @param price - the price in copper of the item
function HandlePurchase(user,template,price)
    local currencyType = this:GetObjVar("Data.CurrencyType") or "coins"
    if(HandleConsumeResource(user,currencyType,price)) then
	   local backpack = user:GetEquippedObject("Backpack")
	   CreateObjInContainer(template, backpack, GetRandomDropPosition(backpack), "sellItemToPlayer", 1)	
	   OpenMerchantDialog(user,this)
       return true
    else
		user:SystemMessage("you can not afforde this...")
        return false
	end
end

--- This function checks to see if the player meets the requirements for the response node
-- @param user - player object to open the window for
-- @param requirement - requirement table
function MeetsDialogRequirements(user,requirement)
    if(requirement) then
        if(requirement.ObjVar) then
            if(user:GetObjVar(requirement.ObjVar) ~= requirement.Value) then return false end
        end
        --add custom checks to handle 
        --example code
        --if(a:Value > b:Value) then return false end
    end
    return true
end




function GetBuyableItems(user)
    local returnList = {}
    for k,v in pairs(StoreTypes[this:GetObjVar("Data.StoreType")].Stock) do
        if(v.CanBuy == true) then
            table.insert(returnList,{Template =v.Template,Price=v.BuyPrice,DisplayName=v.DisplayName,})
        end
    end
    return returnList
end

function GeneratePurchasableItemsFromPlayer(user)
    local backpackObj = user:GetEquippedObject("Backpack")
    local purchaseList = {}
    local pouchItem = FindItemInContainerRecursive(backpackObj,
        function(item)            
            CheckIsOnBuyList(item,purchaseList)
        end)
    return purchaseList
end

function CheckIsOnBuyList(item,list)
    local purchasebleItems = GetBuyableItems(user)
    for index, purchaseableItemTable in pairs(purchasebleItems) do
        if(item:GetCreationTemplateId()==purchaseableItemTable.Template) then
            table.insert(list,{Item=item.Id,DisplayName=purchaseableItemTable.DisplayName,Price=purchaseableItemTable.Price,})
        end
    end
end

--- This function opens up the vendors inventory for players to purchase from
-- @param user - player object to open the window for
-- @param merchant - object to handle the events-
function OpenPlayerSellToVendorDialog(user,merchant)
    local style = merchant:GetObjVar("Data.UIStyle") or "Dark"
    --get list of matching items the player has the vendor will purchase.
    local sellList = GeneratePurchasableItemsFromPlayer(user)
    --register event handler for dynamic window
    RegisterEventHandler(EventType.DynamicWindowResponse,DynamicWindowResponseHandler..merchant.Id, HandleDynamicWindowResponse)
    --create the dynamic window
    local dynamicWindow = DynamicWindow(DynamicWindowResponseHandler..merchant.Id,"",500,382,-250,-420,"TransparentDraggable","Bottom")
    --Create Background
    dynamicWindow:AddImage(1,1,"HueSwatch",500,382,"Simple",Styles[style].Body.Colors.BackGround)
    --Create Header Background
    dynamicWindow:AddImage(1,1,"HueSwatch",500,34,"Simple",Styles[style].Header.Colors.BackGround)
    DrawCloseButton(471,9,dynamicWindow,"PlayerSellToVendor")
    --Create Header Panel
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",506,40,"Sliced",Styles[style].Header.Colors.BackGroundFrame)

    local currencyType = StoreTypes[this:GetObjVar("Data.StoreType")].CurrencyName
    dynamicWindow:AddLabel(250,10,"[959555]What would you like to sell for some ".. currencyType .."?",500,20,20,"center")

    local sortedTable = {}
    local scrollWindow = ScrollWindow(11,39,468,340,40)
    for i,j in pairs(sellList) do table.insert(sortedTable,j) end
    table.sort(sortedTable,function (a,b)
        if (a.DisplayName < b.DisplayName) then
            return true
        else
            return false
        end
    end)
    for k,v in pairs(sortedTable) do
        local scrollElement = ScrollElement()
        scrollElement:AddLabel(1,10,Styles[style].SellToPlayerWindow.Colors.Text.ItemName..v.DisplayName,465,24,24,"left")
        if(merchant:GetObjVar("Data.CurrencyType") == "coins") then
            scrollElement:AddLabel(300,10,Styles[style].SellToPlayerWindow.Colors.Text.ItemName,200,24,24,"left")
            scrollElement:AddLabel(340,10,"[959555]"..v.Price.." [FF7700]c",80,24,24,"right")
        else
            scrollElement:AddLabel(300,10,Styles[style].SellToPlayerWindow.Colors.Text.ItemName,200,24,24,"left")
            scrollElement:AddLabel(340,10,"[959555]"..v.Price,80,24,24,"right")
        end

        scrollElement:AddImage(-10,-5,"CraftingItemsFrame",390,48,"Sliced",Styles[style].Body.Colors.BackGroundFrame)
        --create button
        scrollElement:AddImage(380,1,"HueSwatch",70,39,"Simple","333333")
        scrollElement:AddImage(373,-5,"CraftingItemsFrame",82,48,"Sliced",Styles[style].Body.Colors.BackGroundFrame)
        scrollElement:AddLabel(414,10,"[959555]Sell",82,24,24,"center")
        scrollElement:AddButton(373,1,"SellShopItem|"..v.Item.."~"..v.Price.."~"..v.DisplayName,"",82,38,"[FF7700]Sell "..v.DisplayName.."\n[FFFFAA]Click Sell to sell this "..v.DisplayName .. " to the merchant","",false,"Invisible")
        
        scrollWindow:Add(scrollElement)
    end    

    dynamicWindow:AddScrollWindow(scrollWindow)
    --Add body panel                                  
    dynamicWindow:AddImage(-2,30,"CraftingItemsFrame",506,356,"Sliced",Styles[style].Body.Colors.BackGroundFrame)
    --Open the dynamicWindow for the player and allowing the test tool
    --to handle all the events and clicks from the window.
    
    user:OpenDynamicWindow(dynamicWindow,merchant)
end