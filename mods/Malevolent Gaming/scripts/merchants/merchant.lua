require 'merchants.merchant_settings'
require 'merchants.stores.stores'
require 'merchants.merchant_localization'
require 'merchants.styles.styles'
require 'merchants.features.features'
require 'merchants.outfits.outfits'
require 'merchants.names.names'
require 'dialog.dialog'
require 'merchants.merchant_ui'
--- Add a store item to the Data.StoreInfo or merchants inventory
-- @param storeitem - a table consisting of the following variables
--  {
--      DisplayName=(string),
--      Template=(string),
--      SellPrice=(number),
--      BuyPrice=(number),
--      MaxStock=(number),
--      CurrentStock=(number),
--      CanSell=(bool),
--      CanBuy=(bool),
--      Conditions=(table),
--      Tooltip=(string),
--  }
function AddStoreItem(storeitem)
    if(storeitem ~= nil) then
        local storeInfo = this:GetObjVar("Data.StoreInfo")
        table.insert(storeInfo,storeitem)
        this:SetObjVar("Data.StoreInfo",storeInfo)
    else
         DebugMessage("Object #"..this.Id.." - storeitem variable is nil, check your spelling or capital letters")
    end
end

--- Clear the store table
function ClearStore()
    this:SetObjVar("Data.StoreInfo",{})
end

--- Create a store item table to add to the merchants inventory
-- @param displayname - the display name string to display on the ui
-- @param template - the item template to sell
-- @param sellprice - this is the price the merchant will sell the item for to a player
-- @param buyprice - this is the price the merchant will pay for the given item from a player
-- @param maxstock - this is the maximum amount the merchant carries of the item
--			       - this also sets the current stock = max stock
function CreateStoreItem(displayname,template,sellprice,buyprice,maxstock,cansell,canbuy,tooltip,condition)
	--double check everything is okay
    if(cansell == nil) then cansell = true end
	if(canbuy == nil) then canbuy = true end
    if(tooltip == nil) then tooltip ="" end
    if(condition == nil) then condition = {None=true} end
	--create a basic store item Table entry
    local storeItemTable = 
        {
            DisplayName=displayname,
            Template=template,
            SellPrice=sellprice,
            BuyPrice=buyprice,
            MaxStock=maxstock,
            CurrentStock=maxstock,
            CanSell=cansell,
            CanBuy=canbuy,
            Tooltip=tooltip,
            Conditions=condition
        }
	return storeItemTable
end

--- Load the store table info from the StoreTypes table in merchant_datatables.lua
-- @param storetype - the key name in the StoreTypes table
function LoadStore(storetype)
    if(storetype ~= nil) then
    	for k,v in pairs(StoreTypes[storetype].Stock) do
    		AddStoreItem(CreateStoreItem(v.DisplayName,v.Template,v.SellPrice,v.BuyPrice,v.MaxStock,v.CanSell,v.CanBuy,v.Tooltip,v.Conditions))
    	end
    else
        DebugMessage("Object #"..this.Id.." - storetype variable is nil, check your spelling or capital letters")
    end
end	

---this handles when this script file is attached to this object
RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()
    --Set up the context menu (right clicking the object)
    AddUseCase(this,"[FFFF00]Talk",true)
    --Create the view to help with auto closing players user interfaces.
    AddView("NearbyPlayers", SearchPlayerInRange(InteractionDistance),ViewPulseTime)
	--Clear the Current Store
    ClearStore()
    --Load the table with the key string stored in Data.StoreType
    --this file is merchant.stores.stores
    LoadStore(this:GetObjVar("Data.StoreType"))
    --Use random npc generation located in merchants.features.features
    --[[
    if(not IsFemale(this)) then
        local name = MerchantNameColor..GetName("Human","Male")
        --this:SetName(name)
        this:SetSharedObjectProperty("DisplayName",name)
        Features[this:GetObjVar("Data.Features")]["Male"].Random(this)
    else
        local name = MerchantNameColor..GetName("Human","Female")
       --this:SetName(name)
        this:SetSharedObjectProperty("DisplayName",name)        
        Features[this:GetObjVar("Data.Features")]["Female"].Random(this)
    end
    ]]
    --Equip the merchant with some random clothing generated from the key string
    --in the Data.Outfits located in merchants.outfits.outfits
    EquipMerchant(this,this:GetObjVar("Data.Outfit"))
    --Set the merchants store title
    this:SetSharedObjectProperty("Title",MerchantTitleColor..StoreTypes[this:GetObjVar("Data.StoreType")].MerchantTitle)
    --Set the currencty type for the merchant. This can be any Resource in the game, like lemongrass
    --this variable is stored in the [Data.StoreType].store info table in
    --merchants.store.store
    this:SetObjVar("Data.CurrencyType",StoreTypes[this:GetObjVar("Data.StoreType")].CurrencyType)    
end)

---this handles when the user clicks the object this is attached too
RegisterEventHandler(EventType.Message, "UseObject", function(user, usedType) 
    if(user:DistanceFrom(this) < InteractionDistance + 1) then
	    this:SetFacing(this:GetLoc():YAngleTo(user:GetLoc()))
	    if(usedType=="[FFFF00]Talk") then
	        OpenMerchantDialog(user,this)   
	        return
	    end
	end
end)

---this handles the merchant equipment generation and hue/color data
RegisterEventHandler(EventType.CreatedObject, "createMerchantEquipment", function(success,obj,args)
    if(success) then
        obj:SetHue(args.Hue)
        obj:SetColor(args.Color)
    end
end)
--this handles the merchants body hue/color data
RegisterEventHandler(EventType.CreatedObject, "createFaceObj", function(success,obj,args)
    if(success) then
        obj:SetHue(args.Hue)
        obj:SetColor(args.Color)
    end
end)
---this handles the merchants hair hue/color data
RegisterEventHandler(EventType.CreatedObject, "createHairObj", function(success,obj,args)
    if(success) then
        obj:SetHue(args.Hue)
        obj:SetColor(args.Color)
    end
end)        
---this event is called but does nothing
RegisterEventHandler(EventType.EnterView, "NearbyPlayers", function (enteringObject)
	if(enteringObject:IsPlayer()) then
	end
end)
---this handles the view when a player leaves it, to auto close any dialog window
RegisterEventHandler(EventType.LeaveView, "NearbyPlayers", function (exitingObject)
	if(exitingObject:IsPlayer()) then
		exitingObject:CloseDynamicWindow(DynamicWindowResponseHandler..this.Id)
	end
end)

--this makes sure when the npc loads it fires the view back up
RegisterSingleEventHandler(EventType.LoadedFromBackup,"", function ( ... )
	AddView("NearbyPlayers", SearchPlayerInRange(InteractionDistance),ViewPulseTime)
end)

RegisterEventHandler(EventType.CreatedObject, "sellItemToPlayer", function(success, objref,quantity)
	if(type(quantity) == "number") then
		if(success and quantity > 1) then
			RequestSetStack(objref,quantity)
		else
			SetItemTooltip(objref)
		end
	else
		SetItemTooltip(objref)			
	end
end)
