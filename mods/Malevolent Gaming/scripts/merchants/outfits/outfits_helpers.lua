function EquipMerchant(mobile,outfit)
	for k,v in pairs(Outfit[outfit].Equipment) do
		CreateEquippedObj(v.Template,mobile,"createMerchantEquipment",{Hue=EquipmentHues[v.EquipmentHue][math.random(1,#EquipmentHues[v.EquipmentHue])],Color=EquipmentColors[v.EquipmentColor][math.random(1,#EquipmentColors[v.EquipmentColor])]})
	end
end