FaceType = {}

FaceType.Human =
{
	Male =
	{
		"head_male02",
		"head_male03",
		"head_male04",
	},
	Female =
	{
		"head_female01",
		"head_female02",
		"head_female03",
		"head_female04",
		"head_female05",
	},
}