HairType = {}

HairType.Human =
{
	Male=
	{
		"hair_male",
		"hair_male_bangs",
		"hair_male_buzzcut",
		"hair_male_messy",
		"hair_male_nobleman",
		"hair_male_rougish",
		"hair_male_sideundercut",
		"hair_male_undercut",
		"hair_male_windswept",
		"hair_male_long",
	},
	Female=
	{
		"hair_female",
		"hair_female_bob",
		"hair_female_bun",
		"hair_female_buzzcut",
		"hair_female_pigtails",
		"hair_female_ponytail",
		"hair_female_shaggy",
	},
}