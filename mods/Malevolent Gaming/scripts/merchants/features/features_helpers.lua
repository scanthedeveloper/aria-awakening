function GetRandomFaceType(race,gender)
	local face = FaceType[race][gender][math.random(1,#FaceType[race][gender])]
	return face
end

function GetRandomHairType(race,gender)
	local hair = HairType[race][gender][math.random(1,#HairType[race][gender])]
	return hair
end

function GetRandomHairColor(race,gender)
	local color = HairColor[race][gender].Color[math.random(1,#HairColor[race][gender].Color)]
	return color
end

function GetRandomHairHue(race,gender)
	local hue = HairColor[race][gender].Hue[math.random(1,#HairColor[race][gender].Hue)]
	return hue
end

function GetRandomSkinColor(race,gender)
	local color = SkinColor[race][gender].Color[math.random(1,#SkinColor[race][gender].Color)]
	return color
end

function GetRandomSkinHue(race,gender)
	local hue = SkinColor[race][gender].Hue[math.random(1,#SkinColor[race][gender].Hue)]
	return hue
end



