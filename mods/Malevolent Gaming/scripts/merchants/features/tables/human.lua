Features.Human = 
{
	Male =
	{
		Random = function(mobile)
			local skinHue = GetRandomSkinHue("Human","Male")
			local skinColor = GetRandomSkinColor("Human","Male")
			local hairHue = GetRandomHairHue("Human","Male")
			local hairColor = GetRandomHairColor("Human","Male")
			local faceType = GetRandomFaceType("Human","Male")
			local hairType = GetRandomHairType("Human","Male")
			mobile:SetHue(skinHue)
			mobile:SetColor(skinColor)
			
			CreateEquippedObj(faceType,mobile,"createFaceObj",{Hue=skinHue,Color=skinColor})
			CreateEquippedObj(hairType,mobile,"createHairObj",{Hue=hairHue,Color=hairColor})
		end,
	},
	Female =
	{
		Random = function(mobile)
			local skinHue = GetRandomSkinHue("Human","Female")
			local skinColor = GetRandomSkinColor("Human","Female")
			local hairHue = GetRandomHairHue("Human","Female")
			local hairColor = GetRandomHairColor("Human","Female")
			local faceType = GetRandomFaceType("Human","Female")
			local hairType = GetRandomHairType("Human","Female")
			--mobile:SetAppearanceFromTemplate("merchant_female")
			mobile:SetHue(skinHue)
			mobile:SetColor(skinColor)
		
			CreateEquippedObj(faceType,mobile,"createFaceObj",{Hue=skinHue,Color=skinColor})
			CreateEquippedObj(hairType,mobile,"createHairObj",{Hue=hairHue,Color=hairColor})
		end,
	},
}