-- table to hold the merchant features
Features = {}
-- tables for the merchant features
require 'merchants.features.facetypes.facetypes'				
require 'merchants.features.haircolors.haircolors'				
require 'merchants.features.hairtypes.hairtypes'				
require 'merchants.features.skincolors.skincolors'				
require 'merchants.features.features_helpers'					
require 'merchants.features.tables.human'						

