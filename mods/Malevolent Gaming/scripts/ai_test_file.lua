-- grab the Dialog code from the base_ai_npc file
require 'base_ai_npc'

-- set some general npc flags
AI.Settings.EnableTrain = false
AI.Settings.MerchantEnabled = false
AI.Settings.SkipIntroDialog = true

---------------------------------------------------------------------------
-- Part1 Node
---------------------------------------------------------------------------
--- this opens the Part1 part of the dialog
--@ param user - the user object to display the dialog node too
function Dialog.OpenPart1Dialog(user)
	-- the npc text dialog to display to the player
	local text = "Part 1 - Test example"
	-- the table to hold the player response info
	local response = {}

	response[1]={}
	response[1].text = "Close window"
	response[1].handle =""

	-- load up the new npc interaction dynamic window for the user
	NPCInteraction(text,this,user,"Responses",response)

	-- set the attention of the npc to the user who is interacting with it
	GetAttention(user)
end

---------------------------------------------------------------------------
-- Greeting Node
---------------------------------------------------------------------------
--- this opens the Greeting part of the dialog
--@ param user - the user object to display the dialog node too
function Dialog.OpenGreetingDialog(user)
	-- the npc text dialog to display to the player
	local text = "Welcome!  This is a custom dummy I made for the scouraged adventure with Til!"
	-- the table to hold the player response info
	local response = {}

	response[1]={}
	response[1].text = "Want to learn more?"
	response[1].handle ="Part1"

	-- load up the new npc interaction dynamic window for the user
	NPCInteraction(text,this,user,"Responses",response)

	-- set the attention of the npc to the user who is interacting with it
	GetAttention(user)
end
