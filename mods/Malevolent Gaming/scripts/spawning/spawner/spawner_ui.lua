--- in the event somehow magically a dynamic window handler
--- has this handle value, you can change it here.
DynamicWindowResponseHandler = "SpawnerDynamicWindowHandler"

--- This opens the main dynamic window user interface
-- @param user - player object to open the window for
-- @param controlobject - object to handle the events
function OpenSpawnerPropertyWindow(user,controlobject)
	--register event handler for dynamic window
	local height = 220
	RegisterEventHandler(EventType.DynamicWindowResponse,DynamicWindowResponseHandler..controlobject.Id, HandleDynamicWindowResponse)
	--create the dynamic window
    local dynamicWindow = DynamicWindow(DynamicWindowResponseHandler..controlobject.Id,"",300,height,-150,-110,"TransparentDraggable","Center")
    --Create Background
    dynamicWindow:AddImage(1,1,"HueSwatch",300,height,"Simple","151515")
    --Create Header Background
    dynamicWindow:AddImage(1,1,"HueSwatch",300,34,"Simple","353535")
    DrawCloseButton(271,9,dynamicWindow)
    DrawActiveButton(15,9,dynamicWindow)   
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",306,40,"Sliced","757555")
    dynamicWindow:AddLabel(150,10,"[959555]Spawner Properties - "..this.Id,300,20,20,"center")
    --Create Body Background
    dynamicWindow:AddImage(-2,30,"CraftingItemsFrame",306,height-26,"Sliced","757555")
    --Draw the spawner data stored in the Data ObjVar
    DrawSpawnerDataTable(3,1, dynamicWindow)
    --Open the dynamicWindow for the player and allowing the test tool
    --to handle all the events and clicks from the window.

    user:OpenDynamicWindow(dynamicWindow,controlobject)
end

--- this opens the number selector dynamic window
-- @param user - player object to open the window for
-- @param controlobject - object to handle the events
-- @param tableName - this is the table to edit
function OpenNumericWindow(user,controlobject,tableName)
    buttonCount = 0
	--register event handler for dynamic window
	RegisterEventHandler(EventType.DynamicWindowResponse,DynamicWindowResponseHandler.."_number"..controlobject.Id, HandleDynamicWindowResponse)
    --create the dynamic window
    local dynamicWindow = DynamicWindow(DynamicWindowResponseHandler.."_number"..controlobject.Id,"",200,250,150,-110,"TransparentDraggable","Center")
    --Create Background
    dynamicWindow:AddImage(1,1,"HueSwatch",200,240,"Simple","151515")
    --Create Header Background
    dynamicWindow:AddImage(1,1,"HueSwatch",200,34,"Simple","353535")
    DrawCloseButton(171,9,dynamicWindow)
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",206,40,"Sliced","757555")
    dynamicWindow:AddLabel(100,10,"[959555]"..DefaultSpawnerTable[tableName].Label,200,20,20,"center")
    --Create Body Background
    dynamicWindow:AddImage(-2,30,"CraftingItemsFrame",206,214,"Sliced","757555")
    --draw number buttons
    for y=1,160,40 do
    	for x=1,180,60 do
    		buttonCount = buttonCount + 1
    		dynamicWindow:AddImage(11+x,70+y,"HueSwatch",55,35,"Simple","555555")
    		dynamicWindow:AddImage(13+x,72+y,"HueSwatch",51,31,"Simple","252525")
    		if(buttonCount == 10) then 
    			dynamicWindow:AddLabel(39+x,77+y,"[959515].",55,30,30,"center")
    			dynamicWindow:AddButton(11+x,70+y,"SetNumberFromWindow|"..buttonCount.."|"..tableName,"",55,35,"","",false,"Invisible")
    		elseif(buttonCount == 11) then
    			dynamicWindow:AddLabel(39+x,77+y,"[959515]0",55,30,30,"center")
    			dynamicWindow:AddButton(11+x,70+y,"SetNumberFromWindow|"..buttonCount.."|"..tableName,"",55,35,"","",false,"Invisible")
    		elseif(buttonCount == 12) then 
    			dynamicWindow:AddLabel(39+x,77+y,"[959515]Set",55,30,30,"center")
    			dynamicWindow:AddButton(11+x,70+y,"SetNumberFromWindow|"..buttonCount.."|"..tableName,"",55,35,"","",true,"Invisible")
    		else
    			dynamicWindow:AddLabel(39+x,77+y,"[959515]"..buttonCount,55,30,30,"center")
    			dynamicWindow:AddButton(11+x,70+y,"SetNumberFromWindow|"..buttonCount.."|"..tableName,"",55,35,"","",false,"Invisible")
    		end
    	end
    end
    --draw the label to show your current number value
    dynamicWindow:AddImage(11,41,"HueSwatch",177,25,"Simple","555555")
    dynamicWindow:AddImage(12,42,"HueSwatch",175,23,"Simple","252525")
    dynamicWindow:AddLabel(177,45,this:GetObjVar("Data.NumberPickerValue"),170,24,24,"right")
    user:OpenDynamicWindow(dynamicWindow,controlobject)
end

--- this opens the object picker to select a spawn object template
-- @param user - player object to open the window for
-- @param controlobject - object to handle the events
-- @param category - this is category of mosters to list
function OpenSpawnableSelectionList(user,controlobject,category)
	--register event handler for dynamic window
	RegisterEventHandler(EventType.DynamicWindowResponse,DynamicWindowResponseHandler.."_objectpicker"..controlobject.Id, HandleDynamicWindowResponse)
	--create the dynamic window
    local dynamicWindow = DynamicWindow(DynamicWindowResponseHandler.."_objectpicker"..controlobject.Id,"",200,500,150,-110,"TransparentDraggable","Center")
    --Create Background
    dynamicWindow:AddImage(1,1,"HueSwatch",200,280,"Simple","151515")
    --Create Header Background
    dynamicWindow:AddImage(1,1,"HueSwatch",200,34,"Simple","353535")
    DrawCloseButton(171,9,dynamicWindow)
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",206,40,"Sliced","757555")
    dynamicWindow:AddLabel(100,10,"[959555]Object Picker",200,20,20,"center")
    --Create Body Background
    dynamicWindow:AddImage(-2,30,"CraftingItemsFrame",206,254,"Sliced","757555")
 	DrawSpawnableDataTable(10,45,dynamicWindow,category)
 	--if category is not nil, then draw the back to category button
    if(category ~= nil) then
    	dynamicWindow:AddImage(10,7,"HouseDecorationWindow_RotateCCW_Default",20,20,"Sliced","757555")
    	dynamicWindow:AddButton(10,7,"Edit|SpawnTemplate","",20,20,"[4488CC]Back To Category List\n[959555]This will go back to the category list selection menu","",false,"Invisible")
    end
    user:OpenDynamicWindow(dynamicWindow,controlobject)
end

--- This is a helper function to display the spawnable data table
-- @param x - the x location to start drawing this table listing
-- @param y - the y location to start drawing this table listing
-- @param dynamicWindow - the dynamic window to attach this list too
-- @param category - the category to display
function DrawSpawnableDataTable(x,y,dynamicWindow,category)
	--load category selection on first opening
	local scrollWindow = ScrollWindow(x,y,175,225,25)
	local scrollElement = nil
	local indexKey = {}
	if(category == nil) then
    	for n in pairs(SpawnableObjectsTable) do table.insert(indexKey, n) end
    	table.sort(indexKey)		
    	for k,v in pairs(indexKey) do
			scrollElement = ScrollElement()
    		scrollElement:AddImage(1,1,"HueSwatch",160,25,"Simple","555555")
    		scrollElement:AddImage(2,2,"HueSwatch",158,23,"Simple","252525")
    		scrollElement:AddLabel(6,6,"[959515]"..v,160,18,18,"left")
    		scrollElement:AddButton(1,1,"SetSpawnCategory|"..v,"",160,25,"","",false,"Invisible")
    		scrollWindow:Add(scrollElement)    					
		end
	else
		for i,j in pairs(SpawnableObjectsTable[category]) do table.insert(indexKey,j) end
		table.sort(indexKey,function (a,b)
			if (a.Display < b.Display) then
				return true
			else
				return false
			end
		end)
		for k,v in pairs(indexKey) do
			scrollElement = ScrollElement()
    		scrollElement:AddImage(1,1,"HueSwatch",160,25,"Simple","555555")
    		scrollElement:AddImage(2,2,"HueSwatch",158,23,"Simple","252525")
    		scrollElement:AddLabel(6,6,"[959515]"..v.Display,160,18,18,"left")
    		scrollElement:AddButton(1+x,1,"SetSpawnObjectType|"..v.Template,"",160,25,"","",true,"Invisible")
    		scrollWindow:Add(scrollElement)    					
		end
	end
	dynamicWindow:AddScrollWindow(scrollWindow)
end

--- this will draw the close button on a dynamic window
-- @param x - this is the x location to draw the close button at
-- @param y - this is the y location to draw the close button at
-- @param dynamicWindow - this is the dynamic window to attach the close button too
function DrawActiveButton(x,y,dynamicWindow)
	if(this:GetObjVar("Disable")==true) then
		dynamicWindow:AddImage(x,y,"Blank",18,18,"Sliced","880000")
		--background color
		dynamicWindow:AddImage(x+2,y+2,"Blank",14,14,"Sliced","440000")
		--invisible button
		dynamicWindow:AddButton(x,y,"Activate|activate","",18,18,LocalizationTable.Activate,"",false,"Invisible")		
	else
		dynamicWindow:AddImage(x,y,"Blank",18,18,"Sliced","008800")
		--background color
		dynamicWindow:AddImage(x+2,y+2,"Blank",14,14,"Sliced","004400")
		--invisible button
		dynamicWindow:AddButton(x,y,"Deactivate|deactivate","",18,18,LocalizationTable.Deactivate,"",false,"Invisible")
	end
end

--- this will draw the close button on a dynamic window
-- @param x - this is the x location to draw the close button at
-- @param y - this is the y location to draw the close button at
-- @param dynamicWindow - this is the dynamic window to attach the close button too
function DrawCloseButton(x,y,dynamicWindow)
	--border background color
	dynamicWindow:AddImage(x,y,"Blank",18,18,"Sliced","555555")
	--background color
	dynamicWindow:AddImage(x+2,y+2,"Blank",14,14,"Sliced","252525")
	--invisible button
	dynamicWindow:AddButton(x,y,"CloseButton|close","",18,18,LocalizationTable.CloseButton,"",true,"Invisible")
	--draw the "X" string
	dynamicWindow:AddLabel(x+5,y+3,"[959555]X",18,18,18,"left")
end

--- this will draw the spawnable objects data table on a dynamic window
-- @param x - this is the x location to draw the data table at
-- @param y - this is the y location to draw the data table at
-- @param dynamicWindow - this is the dynamic window to attach the data table too
function DrawSpawnerDataTable(x,y, dynamicWindow)
	local dataTable = this:GetObjVar("Data.SpawnerTable")
	local indexCount = 0
	if(dataTable) then
		--sort the table by key name
		local indexKey = {}
    	for n in pairs(dataTable) do table.insert(indexKey, n) end
    	table.sort(indexKey)		
    	for k,v in pairs(indexKey) do
		--loop through the indexKey
			indexCount = indexCount + 1
			--draw the entry background color
			dynamicWindow:AddImage(x+8,y*(indexCount-1)*24+44,"HueSwatch",125,20,"Simple","353535")
			--draw the entry label
			dynamicWindow:AddLabel(x+13,y*(indexCount-1)*24+47,"[959555]"..dataTable[v].Label,125,20,18,"left")
			--draw help button
			dynamicWindow:AddImage(x+115,y*(indexCount-1)*24+45,"HelpButton_Glow01",18,18,"Simple","FFFFFF")
			--draw help button invisible button
			dynamicWindow:AddButton(x+115,y*(indexCount-1)*24+45,"","",18,18,LocalizationTable.HelpMouseOver[v],"",false,"Invisible")
			--draw the edit button image border color
			dynamicWindow:AddImage(x+263,y*(indexCount-1)*24+44,"HueSwatch",25,20,"Simple","454545")
			--draw the edit button image background color
			dynamicWindow:AddImage(x+264,y*(indexCount-1)*24+45,"HueSwatch",23,18,"Simple","303030")			
			--draw the edit settings icon
			dynamicWindow:AddImage(x+268,y*(indexCount-1)*24+46,"SettingsButton_Default",16,16,"Simple","FFFFFF")
			--draw the edit settings invisible button
			dynamicWindow:AddButton(x+268,y*(indexCount-1)*24+46,"Edit|"..v,"",18,18,LocalizationTable.SettingsMouseOver[v],"",false,"Invisible")

			--process special table data into UI
			if(v=="SpawnTemplate") then
				if(this:HasObjVar("spawnTable")) then
					local spawnTable = this:GetObjVar("spawnTable")
					if(#spawnTable < 1) then
						dynamicWindow:AddImage(x+138,y*(indexCount-1)*24+44,"HueSwatch",120,20,"Simple","250000")
						--display that this is required in redish colors
						dynamicWindow:AddLabel(x+198,y*(indexCount-1)*24+47,"[751515]0 Entries",120,20,18,"center")
					else
						dynamicWindow:AddImage(x+138,y*(indexCount-1)*24+44,"HueSwatch",120,20,"Simple","252525")
						--display the spawn template in normal colors
						dynamicWindow:AddLabel(x+198,y*(indexCount-1)*24+47,"[959555]"..#spawnTable.." Entries",120,20,18,"center")
					end
				else
					if(dataTable[v].Value ~= "") then
						dynamicWindow:AddImage(x+138,y*(indexCount-1)*24+44,"HueSwatch",120,20,"Simple","252525")
						--display the spawn template in normal colors
						dynamicWindow:AddLabel(x+198,y*(indexCount-1)*24+47,"[959555]"..dataTable[v].Value,120,20,18,"center")
					else
						dynamicWindow:AddImage(x+138,y*(indexCount-1)*24+44,"HueSwatch",120,20,"Simple","250000")
						--display that this is required in redish colors
						dynamicWindow:AddLabel(x+198,y*(indexCount-1)*24+47,"[751515] *required*",120,20,18,"center")
					end
				end				
			end
			if(v=="TimeOfDay") then
				dynamicWindow:AddImage(x+138,y*(indexCount-1)*24+44,"HueSwatch",120,20,"Simple","252525")
				--Display the time of day to spawn the objects
				dynamicWindow:AddLabel(x+198,y*(indexCount-1)*24+47,"[959555]"..dataTable[v].Value,120,20,18,"center")
			end
			if(v=="SpawnChance") then
				dynamicWindow:AddImage(x+138,y*(indexCount-1)*24+44,"HueSwatch",120,20,"Simple","252525")
				local chance = this:GetObjVar("spawnChance") or 1
				--Since the SpawnChance is between 0-1, lets display this as a percentage
				dynamicWindow:AddLabel(x+198,y*(indexCount-1)*24+47,"[959555]"..dataTable[v].Value,120,20,18,"center")
			end
			if(v=="SpawnMax") then
				dynamicWindow:AddImage(x+138,y*(indexCount-1)*24+44,"HueSwatch",120,20,"Simple","252525")
				--This is the maximum amount of objects this spawner can have out at once
				dynamicWindow:AddLabel(x+198,y*(indexCount-1)*24+47,"[959555]"..dataTable[v].Value,120,20,18,"center")
			end
			if(v=="SpawnDelay") then
				dynamicWindow:AddImage(x+138,y*(indexCount-1)*24+44,"HueSwatch",120,20,"Simple","252525")
				--This is the delay in seconds in which the spawner will produce another object
				dynamicWindow:AddLabel(x+198,y*(indexCount-1)*24+47,"[959555]"..dataTable[v].Value,120,20,18,"center")
			end
			if(v=="SpawnPulse") then
				dynamicWindow:AddImage(x+138,y*(indexCount-1)*24+44,"HueSwatch",120,20,"Simple","252525")
				--This is the heartbeat of the spawner, it checks for validation of objects every x seconds
				dynamicWindow:AddLabel(x+198,y*(indexCount-1)*24+47,"[959555]"..dataTable[v].Value,120,20,18,"center")
			end	
			if(v=="SpawnRadius") then
				dynamicWindow:AddImage(x+138,y*(indexCount-1)*24+44,"HueSwatch",120,20,"Simple","252525")
				local radius = this:GetObjVar("spawnRadius") or dataTable[v].Value
				--This is the radius in which objects can spawn away from the spawner
				dynamicWindow:AddLabel(x+198,y*(indexCount-1)*24+47,"[959555]"..radius,120,20,18,"center")
			end																													
		end 
	end
end

--- this will reset the spawner to the default spawning conditions
function ResetSpawnerToDefault()
	--reset the data table to default
	this:SetObjVar("Data.SpawnerTable",DefaultSpawnerTable)
	--the below code was taken from the simple_mob_spawner
	--it just deletes everything attached to the spawner.
	local spawnData = this:GetObjVar("spawnData") or {}
    for i=1,#spawnData do
        if (spawnData[i].ObjRef ~= nil and spawnData[i].ObjRef:IsValid()) then
            spawnData[i].ObjRef:Destroy()
            spawnData[i].GoneTime = DateTime.UtcNow
        end
    end
end

--- this is called each time a user clicks a dynamic window button
--- it will display the properties of the table data
function UpdateTooltipInformation()
	if(this:GetObjVar("Disable")==true) then
		SetTooltipEntry(this,"ActiveStatus","[880000]*Disabled*",100)
	else
		SetTooltipEntry(this,"ActiveStatus","[008800]*Enabled*",100)
	end		
end

--- this handles the dynamic window response
-- @param user - player object to open the window for
-- @param buttonId - the dynamic window button id clicked/used
-- @param fieldData - holds the the textfield data on a dynamic window
function HandleDynamicWindowResponse(user, buttonId,fieldData)
    local responseData = StringSplit(buttonId,"|")
    local button = responseData[1]
    local args = responseData[2]
    local dataTable = this:GetObjVar("Data.SpawnerTable")
    if(button == nil) then return false end
    --when the player clicks the edit button for the Time Of Day
    --it will toggle between Any, Day and Night
    if(button == "Edit") then
        if(args == "TimeOfDay") then
            if(dataTable.TimeOfDay.Value=="Any") then
                dataTable.TimeOfDay.Value="Day"
                this:SetObjVar("DaySpawn",true)
                this:DelObjVar("NightSpawn")
            elseif(dataTable.TimeOfDay.Value=="Day") then
                dataTable.TimeOfDay.Value="Night"
                this:SetObjVar("NightSpawn",true)
                this:DelObjVar("DaySpawn")
            elseif(dataTable.TimeOfDay.Value=="Night") then
                dataTable.TimeOfDay.Value="Any"
                this:DelObjVar("NightSpawn")
                this:DelObjVar("DaySpawn")
            end
        end
        if(args=="SpawnChance" or args == "SpawnDelay" or args == "SpawnMax" or args == "SpawnPulse" or args == "SpawnRadius") then
            OpenNumericWindow(user,this,args)
        end
        if(args=="SpawnTemplate") then
            --open default selection with no category
            OpenSpawnableSelectionList(user,this)
        end 
        this:SetObjVar("Data.SpawnerTable",dataTable)
        OpenSpawnerPropertyWindow(user,this)
    end
    --handle number picker
    if(button=="SetNumberFromWindow") then
        local value = this:GetObjVar("Data.NumberPickerValue") or ""
        local tableName = responseData[3]
        local keyCount = tonumber(args)
        if(keyCount<10) then
            value = value .. args
            this:SetObjVar("Data.NumberPickerValue",value)
            OpenNumericWindow(user,this,tableName)
        elseif(keyCount == 10) then
            value = value .. "."
            this:SetObjVar("Data.NumberPickerValue",value)
            OpenNumericWindow(user,this,tableName)
        elseif(keyCount == 11) then
            value = value .. "0"
            this:SetObjVar("Data.NumberPickerValue",value)
            OpenNumericWindow(user,this,tableName)
        else
            dataTable[tableName].Value = tonumber(value)
            this:SetObjVar("Data.SpawnerTable",dataTable)
            OpenSpawnerPropertyWindow(user,this)
            this:SetObjVar("Data.NumberPickerValue","")
            if(tableName=="SpawnPulse") then
                this:SetObjVar("spawnPulseSecs",tonumber(value)) 
            elseif(tableName=="SpawnDelay") then
                this:SetObjVar("spawnDelay",tonumber(value)) 
            elseif(tableName=="SpawnMax") then
                this:SetObjVar("spawnCount",tonumber(value)) 
            elseif(tableName=="SpawnChance") then
                this:SetObjVar("spawnChance",tonumber(value)) 
            elseif(tableName=="SpawnRadius") then
                this:SetObjVar("spawnRadius",tonumber(value)) 
            end
        end
    end
    if(button=="SetSpawnCategory") then
        OpenSpawnableSelectionList(user,this,args)
    end
    if(button=="SetSpawnObjectType") then
        this:SetObjVar("spawnTemplate",args)
        dataTable.SpawnTemplate.Value = args
        this:SetObjVar("Data.SpawnerTable",dataTable)
        OpenSpawnerPropertyWindow(user,this)
    end
    if(button=="Activate") then
    	this:SetObjVar("Disable",false)
    	OpenSpawnerPropertyWindow(user,this)
    end    
    if(button=="Deactivate") then
    	this:SetObjVar("Disable",true)
    	OpenSpawnerPropertyWindow(user,this)
    	CleanObjects()
    end
    UpdateTooltipInformation()
end

--- this function fires when the player shuts the spawner off it clears the spawn data table
function CleanObjects()
	local spawnData = this:GetObjVar("spawnData")
	if(spawnData ~= nil) then 
		for index,spawnedObject in pairs(spawnData) do
			spawnedObject.ObjRef:Destroy()
		end
	end
	this:SetObjVar("spawnData",{})
end


