--- this table contains all the text in english but allows
--  anybody to customize the values here to their language
LocalizationTable =
{
	--the tooltip text for the close button
	CloseButton = "[4488CC]Close Button\n[959555]clicking this will close the spawn property window.",
	Activate = "[4488CC]Activate Spawner\n[959555]clicking this will activate the spawner.",
	Deactivate = "[4488CC]Deactivate Spawner\n[959555]clicking this will deactivate the spawner.",
	--the tooltip text for the help mouse over buttons
	HelpMouseOver = 
	{
		SpawnChance="[4488CC]Chance To Spawn\n[959555]This is a chance to spawn every time the Spawner Pulse, this value must be between 0-1.[959555]\n\n*Tip: 0.1 is 10%, 0.25 is 25%*",
		SpawnDelay="[4488CC]Spawning Delay\n[959555]This is the delay in seconds in which the spawner will allow a respawn of an object.",
		SpawnMax="[4488CC]Max Amount To Spawn\n[959555]This is the amount of objects the spawner can have out in the world at once.",
		SpawnPulse="[4488CC]Spawner Pulse\n[959555]This is a general heartbeat of the spawner, every pulse it checks for spawned object validation and handles respawning.",
		SpawnRadius="[4488CC]Radius To Spawn\n[959555]This is the distance in units in which the objects can spawn in a 360 degree radius.",
		SpawnTemplate="[4488CC]Template\n[959555]This is the spawner template of the object you wish to spawn in the world. Entry Tables automatically override this template.",
		TimeOfDay="[4488CC]Time Of Day\n[959555]This can be Day, Night or Any. Click the edit button to toggle between them."
	},
	--the tooltip text for the settings mouse over buttons
	SettingsMouseOver =
	{
		SpawnChance = "[4488CC]Edit Spawn Chance\n[959555]This will open the Spawn Chance Dialog Editor",
		SpawnDelay = "[4488CC]Edit Spawn Delay\n[959555]This will open the Spawn Delay Dialog Editor",
		SpawnMax = "[4488CC]Edit Max Amount\n[959555]This will open the Maximum Spawn Dialog Editor",
		SpawnPulse = "[4488CC]Edit Spawn Pulse\n[959555]This will open the Spawn Pulse Dialog Editor",
		SpawnRadius = "[4488CC]Edit Radius\n[959555]This will open the Radius Dialog Editor",
		SpawnTemplate = "[4488CC]Edit Template\n[959555]This will open the Template Picker",
		TimeOfDay = "[4488CC]Edit Time Of Day\n[959555]This will Toggle between [Any,Day,Night]",
	},
	--this is the values displayed in the property window of the simple_mob_spawner object.
	TooltipSettings =
	{
		SpawnChance = {Str="[4488CC]Chance: [959555]%s[-]",Priority="106"},
		SpawnDelay = {Str="[4488CC]Delay: [959555]%s[4488CC]s[-]",Priority="105"},
		SpawnMax = {Str="[4488CC]Max: [959555]%s[-]",Priority="104"},
		SpawnPulse = {Str="[4488CC]Pulse: [959555]%s[4488CC]s[-]",Priority="103"},
		SpawnRadius = {Str="[4488CC]Range: [959555]%s[-]",Priority="102"},
		SpawnTemplate = {Str= "[4488CC]Template: [959555]%s[-]",Priority="101"},
		TimeOfDay = {Str="[4488CC]Time: [959555]%s[-]",Priority="100"},
	},
}