require 'spawning.spawner.spawner_datatables'
require 'spawning.spawner.spawner_localization'
require 'spawning.spawner.spawner_ui'
require 'incl_gametime'

isDungeon = IsDungeonMap()

DEFAULT_DELAY_SECS = 180
DEFAULT_SPAWN_COUNT = 1
DEFAULT_SPAWN_CHANCE = 1
DEFAULT_PULSE_FREQ_SECS = 10

--- get the spawners heart beat clock, this checks every 10 seconds 
--  can be modified by changing the public variable DEFAULT_PULSE_FREQ_SECS
function GetSpawnPulse()
    local delaySecs = this:GetObjVar("spawnPulseSecs") or DEFAULT_PULSE_FREQ_SECS
    return TimeSpan.FromSeconds(delaySecs + math.random())
end

--- get the spawn delay in seconds
--  can be modified by changing the public variable DEFAULT_DELAY_SECS
function GetSpawnDelay()
    local delaySecs = this:GetObjVar("spawnDelay") or DEFAULT_DELAY_SECS
    return TimeSpan.FromSeconds(delaySecs + math.random())
end
--- get the spawn location by changing the radius with the user interface of the spawner
function GetSpawnLoc()   
    local spawnRegion = nil
    if(this:HasObjVar("spawnRegions")) then
        -- DAB TODO: Weight based on size of region
        local spawnRegions = this:GetObjVar("spawnRegions")
        spawnRegion = spawnRegions[math.random(1,#spawnRegions)]
    else
        spawnRegion = this:GetObjVar("spawnRegion")
    end
    if(spawnRegion) then
        if(isDungeon) then
            return GetRandomDungeonSpawnLocation(spawnRegion)
        else
            return GetRandomPassableLocation(spawnRegion,true)
        end
    end
    local spawnRadius = this:GetObjVar("spawnRadius")
    if(spawnRadius) then
        return GetRandomPassableLocationInRadius(this:GetLoc(),spawnRadius,true)
    end    
    return this:GetLoc()
end

--- check to see if the spawner should spawn a new object
-- @param spawnData - the current spawn data being processed
-- @param spawnIndex - the index of the spawn data table
function ShouldSpawn(spawnData, spawnIndex)
    local isValid = spawnData[spawnIndex].ObjRef and spawnData[spawnIndex].ObjRef:IsValid()
    local isDead = isValid and spawnData[spawnIndex].ObjRef and spawnData[spawnIndex].ObjRef:IsMobile() and IsDead(spawnData[spawnIndex].ObjRef)
    if( isValid and not(isDead) and not(IsPet(spawnData[spawnIndex].ObjRef)) ) then
        return false
    end
    local spawnDelay = GetSpawnDelay(spawnInfo)
    if(spawnDelay.TotalSeconds ~= 0) then
        local goneTime = spawnData[spawnIndex].GoneTime
        if( spawnData[spawnIndex].ObjRef and not(goneTime) ) then  
            DebugMessageB(this,"MOB DEAD OR DESPAWNED")   
            spawnData[spawnIndex].GoneTime = DateTime.UtcNow
            return false
        end
        if(goneTime and DateTime.UtcNow < (goneTime + spawnDelay)) then  
            return false
        end
    end
    local spawnChance = this:GetObjVar("spawnChance") or DEFAULT_SPAWN_CHANCE
    if(spawnChance < 1 and math.random() > spawnChance) then   
        spawnData[spawnIndex].GoneTime = DateTime.UtcNow
        return false
    end
    return true
end

--- check to see if its time to spawn
function CheckSpawn()
    this:ScheduleTimerDelay(GetSpawnPulse(), "spawnTimer")
    if (this:GetObjVar("Disable") == true) then return end
    local spawnData = this:GetObjVar("spawnData")        
    local spawnCount = this:GetObjVar("spawnCount") or DEFAULT_SPAWN_COUNT
    local templateId = nil
    local spawnTable = this:GetObjVar("spawnTable")
    if(spawnTable) then
        local totalWeight = 0
        for i,spawnEntry in pairs(spawnTable) do
            totalWeight = totalWeight + (spawnEntry.Weight or 1)
        end
        local weightRoll = math.random() * totalWeight
        totalWeight = 0
        for i,spawnEntry in pairs(spawnTable) do
            if( weightRoll <= totalWeight + spawnEntry.Weight ) then
                templateId = spawnEntry.Template
                break
            end
            totalWeight = totalWeight + (spawnEntry.Weight or 1)
        end        
    end
    if not(templateId) then
        templateId = this:GetObjVar("spawnTemplate")
    end
    if( templateId == nil ) then return end
    if (spawnData ~= nil) then
        if (this:HasObjVar("NightSpawn")) then
            if (IsDayTime()) then
                local playerAround = not (#FindObjects(SearchPlayerInRange(20)) == 0)
                if not playerAround then
                    for i=1,#spawnData do
                        if (spawnData[i].ObjRef ~= nil and spawnData[i].ObjRef:IsValid()) then
                            spawnData[i].ObjRef:Destroy()
                            spawnData[i].GoneTime = DateTime.UtcNow
                        end
                    end
                end
                return
            end
        elseif (this:HasObjVar("DaySpawn")) then
            if (IsNightTime()) then 
                local playerAround = not (#FindObjects(SearchPlayerInRange(20)) == 0)
                if not playerAround then
                    for i=1,#spawnData do
                        if (spawnData[i].ObjRef ~= nil and spawnData[i].ObjRef:IsValid()) then
                            spawnData[i].ObjRef:Destroy()
                            spawnData[i].GoneTime = DateTime.UtcNow
                        end
                    end
                end
                return
            end
        end
    end
    if spawnData == nil then
        spawnData = {}
    elseif #spawnData > spawnCount then
    	for i=spawnCount+1,#spawnData do
            table.remove(spawnData,i)
        end
    end        

    for i=1, spawnCount do
        if(spawnData[i] == nil) then spawnData[i] = {} end
        if( ShouldSpawn(spawnData, i) ) then        
            local spawnLoc = GetSpawnLoc()
            if(spawnLoc) then
         	   CreateObj(templateId, spawnLoc, "mobSpawned", i)
        	   break
            end
        end
    end
    this:SetObjVar("spawnData", spawnData)
end

--- this fires when the duplicate spawner context menu option is selected
-- @param success - true/false if the spawner was created
-- @param objref - a reference to the object being created
function DuplicateSpawner(success, objref)
    if(success) then
        CallFunctionDelayed(TimeSpan.FromMilliseconds(300),function()
            objref:SetObjVar("Data.SpawnerTable",this:GetObjVar("Data.SpawnerTable"))
            objref:SetObjVar("spawnPulseSecs",this:GetObjVar("spawnPulseSecs")) 
            objref:SetObjVar("spawnDelay",this:GetObjVar("spawnDelay")) 
            objref:SetObjVar("spawnCount",this:GetObjVar("spawnCount")) 
            objref:SetObjVar("spawnChance",this:GetObjVar("spawnChance")) 
            objref:SetObjVar("spawnRadius",this:GetObjVar("spawnRadius"))
            
            local template = this:GetObjVar("spawnTemplate") or ""
            objref:SetObjVar("spawnTemplate",template)
        end)

    end
end

--- this fires when the object spawns from the spawner
-- @param success - true/false if the spawner was created
-- @param objref - a reference to the object being created
-- @param index - the index of the spawner data
function MobSpawned(success, objref, index)
    if( success) then
        local spawnData = this:GetObjVar("spawnData") 
        if(spawnData[index]) then
            spawnData[index].ObjRef = objref
            spawnData[index].GoneTime = nil
        else
            spawnData[index] = { ObjRef = objref }
        end            
        this:SetObjVar("spawnData",spawnData)
        objref:SetObjVar("Spawner",this)
        if(objref:IsMobile()) then
            if(this:HasObjVar("spawnRadius")) then
                objref:SetFacing(math.random() * 360)
            else
                objref:SetFacing(this:GetFacing())
            end
        end

        local spawnObjVars = this:GetObjVar("spawnObjVars")
        if(spawnObjVars) then
            for varName,varData in pairs(spawnObjVars) do
                objref:SetObjVar(varName,varData)
            end
        end
        UpdateTooltipInformation()
    end
end

----------------------------------------------------------
--EVENTTYPE - Client Target Loc Response 
----------------------------------------------------------
RegisterEventHandler(EventType.ClientTargetLocResponse,"requestMoveSpawner",function(success, location, targetObj)
    if(success) then
        if(location ~= nil) then
            this:SetWorldPosition(location)
        end
    end
end)

----------------------------------------------------------
--EVENTTYPE - Created Object 
----------------------------------------------------------
RegisterEventHandler(EventType.CreatedObject, "mobSpawned", function(...) MobSpawned(...) end)


----------------------------------------------------------
--EVENTTYPE - Created Object 
----------------------------------------------------------
RegisterEventHandler(EventType.CreatedObject, "duplicateSpawner", function(...) DuplicateSpawner(...) end)

----------------------------------------------------------
--EVENTTYPE - Message 
----------------------------------------------------------
RegisterEventHandler(EventType.Message,"Activate",function ( ... )
    this:DelObjVar("Disable")
end)

----------------------------------------------------------
--EVENTTYPE - Message 
----------------------------------------------------------
RegisterEventHandler(EventType.Message,"Deactivate",function ( ... )
    this:SetObjVar("Disable",true)
end)

----------------------------------------------------------
--EVENTTYPE - Message 
----------------------------------------------------------
RegisterEventHandler(EventType.Message,"RemoveSpawnedObject",function (targetObj)
    local spawnData = this:GetObjVar("spawnData")

    for i,spawnInfo in pairs(spawnData) do
        if(spawnInfo.ObjRef == targetObj) then
            spawnInfo.ObjRef = nil
            spawnInfo.GoneTime = DateTime.UtcNow
        end
    end
    this:SetObjVar("spawnData",spawnData)
end)

----------------------------------------------------------
--EVENTTYPE - Message 
----------------------------------------------------------
RegisterEventHandler(EventType.Message, "UseObject", function(user, usedType) 
    if(usedType=="[FFFF00]Properties") then
        OpenSpawnerPropertyWindow(user,this)
        return
    end
    if(usedType=="[FFFF00]Move Spawner") then
        user:RequestClientTargetLoc(this,"requestMoveSpawner")
    end
    if(usedType=="[FFFF00]Duplicate") then
        CreateObj("spawner", user:GetLoc(), "duplicateSpawner")
    end    
end)

----------------------------------------------------------
--EVENTTYPE - Module Attached 
----------------------------------------------------------
RegisterSingleEventHandler(EventType.ModuleAttached, GetCurrentModule(), function()
    --Set up the context menu (right clicking the object)
    AddUseCase(this,"[FFFF00]Properties",true)
    AddUseCase(this,"[FFFF00]Move Spawner",false)
    AddUseCase(this,"[FFFF00]Duplicate",false)
    --this creates a data table to store our ui data in
    this:SetObjVar("Data.SpawnerTable",DefaultSpawnerTable)
    --set base values for the simple_spawner object
    this:SetObjVar("spawnPulseSecs",10) 
    this:SetObjVar("spawnDelay",300) 
    this:SetObjVar("spawnCount",1) 
    this:SetObjVar("spawnChance",1) 
    this:SetObjVar("spawnRadius",5)
    this:SetObjVar("Disable",false) 
    --update the tooltip information
    UpdateTooltipInformation()
	CheckSpawn()
end)

----------------------------------------------------------
--EVENTTYPE - Timer 
----------------------------------------------------------
RegisterEventHandler(EventType.Timer, "spawnTimer", 
	function()
		CheckSpawn()
	end)

--soon as script attaches and fires, kick off the timer
this:ScheduleTimerDelay(GetSpawnPulse(), "spawnTimer")