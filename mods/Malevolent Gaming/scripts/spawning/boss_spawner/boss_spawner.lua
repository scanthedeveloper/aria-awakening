--- incluce the bossspawner tables
require 'spawning.boss_spawner.boss_spawner_tables'

--- speed in seconds to check spawner logic while awake
TIMER_PULSE_AWAKE = 1
--- speed in seconds to check spawner logic while sleeping
TIMER_PULSE_SLEEPING = 15
--- delay in seconds before it can restart
RESET_DELAY = 3600
--- range from the spawner to broadcast new wave message
BROADCAST_RANGE = 30
--- spawn range in case it is not included in the boss spawner table
SPAWN_RANGE = 5

--- this will broadcast to all players within BROADCAST_RANGE
-- @param bossSpawnerKey - this is the Boss Key in the bossspawner table
-- @param stage - the current boss spawn stage
function BroadCastMessage(bossSpawnerKey,stage)
	if(BossSpawner[bossSpawnerKey].Stages[stage].StartWaveMessage == nil) then return end
	local mobiles = FindObjects(SearchMobileInRange(BROADCAST_RANGE))
	for k,v in pairs(mobiles) do
		if(v:IsPlayer()) then v:SystemMessage(BossSpawner[bossSpawnerKey].Stages[stage].StartWaveMessage,"event") end
	end
end

--- can be modified by changing the public variable TIMER_PULSE
function GetSpawnPulse()
	if(this:GetObjVar("Data.Sleeping") == true) then
		return TimeSpan.FromSeconds(TIMER_PULSE_SLEEPING + math.random())
	else
		return TimeSpan.FromSeconds(TIMER_PULSE_AWAKE + math.random())
	end
end

--- checks to see if all the mobs are dead
function IsSpawnDead()
	local spawnTable = this:GetObjVar("Data.SpawnTable")
	local tableCount = #this:GetObjVar("Data.SpawnTable")
	local deadCount = 0
	for k,v in pairs(spawnTable) do
		if((v:IsMobile() and IsDead(v)) or not v:IsValid()) then deadCount = deadCount + 1 end
	end
	return (deadCount == tableCount)
end

--- fires when a monster is spawned
-- @param success - if the object is correctly created and valid
-- @param objref - the object reference that is spawned
function HandleCreatedObject(success, objref)
	local spawnTable = this:GetObjVar("Data.SpawnTable")
	if(success) then
		table.insert(spawnTable,objref)
	end
	this:SetObjVar("Data.SpawnTable",spawnTable)
end

--- fires when this module is attached to the boss spawner object
function HandleModuleAttached()
	this:SetObjVar("Data.BossKey","Mogus")
	this:SetObjVar("Data.SpawnTable",{})
	this:SetObjVar("Data.DelayInSeconds", RESET_DELAY)
	this:SetObjVar("Data.CurrentWave",0)
	--this:SetObjVar("Data.SpawnRange",5)
	this:SetObjVar("Data.RestartTime", os.time() + this:GetObjVar("Data.DelayInSeconds"))
	this:SetObjVar("Data.Sleeping",false)
end

--- check the spawn conditions
function HandleSpawnTimer()
	if(this:GetObjVar("Data.Sleeping") == true) then
		local restartTime = this:GetObjVar("Data.RestartTime")
		if(restartTime < os.time()) then ResetBossSpawner() end	 
		this:ScheduleTimerDelay(GetSpawnPulse(), "checkSpawner")
		return 
	end
	if(IsSpawnDead()) then
		local currentWave = this:GetObjVar("Data.CurrentWave") or 1
		local key = this:GetObjVar("Data.BossKey") or "Mogus"
		local stageKey = this:GetObjVar("Data.CurrentStage") or BossSpawner[key].StartingStage
		--- if starting up for first run, broadcast the start wave message
		if(currentWave == 0 and stageKey == BossSpawner[key].StartingStage) then
			BroadCastMessage(this:GetObjVar("Data.BossKey"),BossSpawner[this:GetObjVar("Data.BossKey")].StartingStage)
		end
		--- updage wave counter and save it
		currentWave = currentWave + 1
		this:SetObjVar("Data.CurrentWave",currentWave)
		--- if we done all stages, update stage info
		if(currentWave > BossSpawner[key].Stages[stageKey].TotalWaves) then
			--- reset the wave counter
			this:SetObjVar("Data.CurrentWave",1)
			--- set the new current stage
			this:SetObjVar("Data.CurrentStage",BossSpawner[key].Stages[stageKey].NextStage)
			stageKey = BossSpawner[key].Stages[stageKey].NextStage
			--only broadcast if there is a next wave
			if(BossSpawner[key].Stages[stageKey]) then 
				BroadCastMessage(key,stageKey) 
			end
		end
		--- if there is a stageKey (NextStage) then spawn the wave
		--- else this must be the end of the wave. check reset time.
		if (stageKey ~= nil and stageKey ~= "EndStage") then 
			SpawnWave(key,stageKey) 
		end
		--- lastly if its the end stage, lets kill everything and put the spawner to sleep
		if (stageKey == "EndStage") then 
			this:SetColor("752525")
			this:SetObjVar("Data.Sleeping",true) 
			this:SetObjVar("Data.RestartTime", os.time() + this:GetObjVar("Data.DelayInSeconds"))
		end
	end
	--- start the timer up
	this:ScheduleTimerDelay(GetSpawnPulse(), "checkSpawner")
end

--- reset the boss spawner for the next wave
function ResetBossSpawner()
	this:SetObjVar("Data.SpawnTable",{})
	this:SetObjVar("Data.CurrentWave",0)
	this:SetObjVar("Data.RestartTime",DateTime.UtcNow + TimeSpan.FromMinutes(this:GetObjVar("Data.DelayInSeconds")))
	this:SetObjVar("Data.Sleeping",false)
	this:SetObjVar("Data.CurrentStage",BossSpawner[this:GetObjVar("Data.BossKey")].StartingStage)
	this:SetColor("257525")
	BroadCastMessage(this:GetObjVar("Data.BossKey"),BossSpawner[this:GetObjVar("Data.BossKey")].StartingStage)
end

--- this will broadcast to all players within BROADCAST_RANGE
-- @param bossSpawnerKey - this is the Boss Key in the bossspawner table
-- @param stage - the current boss spawn stage
function SpawnWave(bossSpawnerKey,stage)
	--calculate spawn amount, make sure at least 1 spawns
	local spawnAmount = BossSpawner[bossSpawnerKey].Stages[stage].MonstersPerWave.Base + BossSpawner[bossSpawnerKey].Stages[stage].MonstersPerWave.Bonus * this:GetObjVar("Data.CurrentWave") or 1
	for i=1, spawnAmount do
		local template = BossSpawner[bossSpawnerKey].Stages[stage].MonsterList[math.random(#BossSpawner[bossSpawnerKey].Stages[stage].MonsterList)]
		local range = SPAWN_RANGE
		if(BossSpawner[bossSpawnerKey].Stages[stage].Range ~= nil) then
			range = math.random(BossSpawner[bossSpawnerKey].Stages[stage].Range.Min,BossSpawner[bossSpawnerKey].Stages[stage].Range.Max)
		end
		local location = GetRandomPassableLocationInRadius(this:GetLoc(),range,true)	
		CreateObj(template, location, "mobSpawned")
	end
end

----------------------------------------------------------
--EVENTTYPE - Created Object 
----------------------------------------------------------
RegisterEventHandler(EventType.CreatedObject, "mobSpawned", function(...) HandleCreatedObject(...) end)

----------------------------------------------------------
--EVENTTYPE - ModuleAttached 
----------------------------------------------------------
RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), function(...) HandleModuleAttached(...) end)

----------------------------------------------------------
--EVENTTYPE - Timer 
----------------------------------------------------------
RegisterEventHandler(EventType.Timer, "checkSpawner", function(...) HandleSpawnTimer(...) end)

--soon as script attaches and fires, kick off the timer
this:ScheduleTimerDelay(GetSpawnPulse(), "checkSpawner")

