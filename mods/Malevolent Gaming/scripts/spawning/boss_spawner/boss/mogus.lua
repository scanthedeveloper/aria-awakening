BossSpawner.Mogus =
{
	-- the starting stage of the boss spawner
	StartingStage = "Stage1",
	-- table containing the stages of the spawn
	Stages =
	{
		-- a stage entry which contains information needed by the boss spawner
		-- this is to allow you to tweak settings.
		Stage1 =
		{
			-- a list of template strings to randomly pick from and spawn during this stages waves
			MonsterList = {"mogus_skeleton_warrior","mogus_skeleton_mage","mogus_skeleton_archer",},
			-- a numeric value that controls how many waves are in this stage
			TotalWaves = 1,
			-- a table to control the amount of monsters to spawn every wave
			-- Base is the minimum amount to spawn each round
			-- Bonus is amount to add per wave.
			MonstersPerWave = {Base = 5, Bonus=1},
			-- min and max range to spawn objects
			Range = {Min = 1, Max = 7},
			-- system message to aleart players the waves have begun
			StartWaveMessage = "Mogus's minions grow stronger.",
			-- when this stage is done, what is the next stage to move too.
			NextStage = "Stage2",
		},
		Stage2 =
		{
			MonsterList = {"mogus_skeleton_warrior","mogus_skeleton_mage","mogus_skeleton_archer",},
			TotalWaves = 1,
			MonstersPerWave = {Base = 1, Bonus=1},
			Range = {Min = 1, Max = 7},
			StartWaveMessage = "Mogus's minions grow stronger.",
			NextStage = "Stage3",
		},		
		Stage3 =
		{
			MonsterList = {"mogus_skeleton_warrior","mogus_skeleton_mage","mogus_skeleton_archer",},
			TotalWaves = 1,
			MonstersPerWave = {Base = 1, Bonus=1},
			Range = {Min = 1, Max = 7},
			StartWaveMessage = "Mogus's minions grow stronger.",
			NextStage = "Stage4",
		},		
		Stage4 =
		{
			MonsterList = {"mogus_skeleton_warrior","mogus_skeleton_mage","mogus_skeleton_archer",},
			TotalWaves = 1,
			MonstersPerWave = {Base = 1, Bonus=1},
			Range = {Min = 1, Max = 7},
			StartWaveMessage = "Mogus's minions grow stronger.",
			NextStage = "Stage5",
		},		
		Stage5 =
		{
			MonsterList = {"mogus_boss",},
			TotalWaves = 1,
			MonstersPerWave = {Base = 1, Bonus=0},
			Range = {Min = 1, Max = 1},
			StartWaveMessage = "[FF7700]Mogus:[-] Death to all who challange me.",
			NextStage = "EndStage"
		},		
	}
}