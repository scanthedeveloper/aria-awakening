require 'editors.color_table'
-- get the item property data
require 'item_properties.incl_item_properties'

--- display the color editor
-- @param user - the user to open this dynamic window for
-- @parm object - the object currently being edited. Can be nil
function DisplayColorEditor(user,object)
	-- create local objects and variables
	local dynamicWindow = DynamicWindow("ColorEditorDynamicResponse","",100,230,-150,-120,"TransparentDraggable","Center")
	local scrollWindow = ScrollWindow(10,45,175,175,25)
	local scrollElement = nil    
	local indexKey = {}

    -- create the main background window
    dynamicWindow:AddImage(1,1,"HueSwatch",200,230,"Simple","333333")
    dynamicWindow:AddImage(3,3,"HueSwatch",196,224,"Simple","000000")
    
    -- create the header background and text
    dynamicWindow:AddImage(1,1,"HueSwatch",200,34,"Simple","333333")
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",206,40,"Sliced","000000")
    dynamicWindow:AddLabel(90,10,"[959555]Color Picker",120,20,20,"center")	

	-- register event handler for dynamic window
	RegisterEventHandler(EventType.DynamicWindowResponse,"ColorEditorDynamicResponse", HandleColorEditorDynamicWindowResponse)
	
	-- sort the color table alphabetically by name
	for i,j in pairs(ColorTable) do table.insert(indexKey,j) end
	table.sort(indexKey,function (a,b)
		if (a.Name < b.Name) then
			return true
		else
			return false
		end
	end)
	
	-- create the scroll elements from the ColorTable table
	for index,value in pairs(indexKey) do
		scrollElement = ScrollElement()
		-- create scroll element background and text
		scrollElement:AddImage(1,1,"HueSwatch",160,25,"Simple","333333")
		scrollElement:AddImage(2,2,"HueSwatch",158,23,"Simple","000000")
		scrollElement:AddLabel(6,6,"[959555]"..value.Name,160,18,18,"left")

		-- supplly the button with the hue and color to send to the event handler
		scrollElement:AddButton(1,1,"SetColor|"..value.Hue..":"..value.Color,"",160,25,"[CCCC33]"..value.Name.."\n[FFFFFF]Hue: ".. value.Hue .. "\nColor: " .. value.Color,"",false,"Invisible")

		-- attach the scroll elemental to the scroll window
		scrollWindow:Add(scrollElement) 		
	end

	--attach the scrollWindow object to the dynamic window
	dynamicWindow:AddScrollWindow(scrollWindow)

	-- draw the close button on the dynamic window
	DrawCloseButton(171,9,dynamicWindow)

	-- open up the finalized dynamic window for the user
	user:OpenDynamicWindow(dynamicWindow,object)

	UpdateTooltip()
	
end

--- this handles the dynamic window response
-- @param user - player object to open the window for
-- @param buttonId - the dynamic window button id clicked/used
-- @param fieldData - holds the the textfield data on a dynamic window
function HandleColorEditorDynamicWindowResponse(user, buttonId, fieldData)
	    -- create local objects and variables
	    local responseData = StringSplit(buttonId,"|")
    	local button = responseData[1]
    	local args = responseData[2]

    	--handle button responses
    	if(button == "SetColor") then
    		local colorData = StringSplit(args,":")
    		this:SetHue(tonumber(colorData[1]))
			this:SetColor(colorData[2])
    		--if this is a piece of equipment, reopen the equipment editor to reflect the changes
    		if(this:HasObjVar("ArmorType") or this:HasObjVar("ShieldType") or this:HasObjVar("WeaponType")) then
    			DisplayEquipmentEditor(user,this)
    		end
		end
end