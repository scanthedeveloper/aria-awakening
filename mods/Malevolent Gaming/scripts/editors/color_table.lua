--- table contains table entrys that supply the hue and color to select from
--- add new color entries here.
ColorTable =
{
	--Base Color 0 hue FFFFF color
	{ Name="1. None",Hue=0, Color="FFFFFF"},
	--Guilds
	{ Name="2. Guild - [HOPE]",Hue=866,Color="FFFFFF"},
	{ Name="2. Guild - [ELF]",Hue=538,Color="FFFFFF"},
	{ Name="2. Guild - [ORDR]",Hue=196,Color="BBBBBB"},
	--Resources
	{ Name="3.1 Resource - Iron",Hue=22,Color="FFFFFF"},
	{ Name="3.2 Resource - Copper",Hue=667,Color="FFFFFF"},
	{ Name="3.3 Resource - Gold",Hue=787,Color="FFFFFF"},
	{ Name="3.4 Resource - Cobalt",Hue=836,Color="FFFFFF"},
	{ Name="3.5 Resource - Obsidian",Hue=893,Color="FFFFFF"},
	{ Name="4.1 Resource - Boards",Hue=775,Color="FFFFFF"},
	{ Name="4.2 Resource - Ash",Hue=866,Color="BBBBBB"},
	{ Name="4.3 Resource - Blightwood",Hue=865,Color="FFFFFF"},
	{ Name="5.1 Resource - Cloth",Hue=139,Color="FFFFFF"},
	{ Name="5.2 Resource - Quilted",Hue=934,Color="FFFFFF"},
	{ Name="5.3 Resource - Silk",Hue=872,Color="FFFFFF"},
	--Red
	{ Name="8. Red",Hue=862,Color="FFFFFF"},
	{ Name="8. Red II",Hue=862,Color="BBBBBB"},
	{ Name="8. Red III",Hue=862,Color="777777"},
	--Pink
	{ Name="8. Pink",Hue=920,Color="FFFFFF"},
	{ Name="8. Pink II",Hue=920,Color="BBBBBB"},
	{ Name="8. Pink III",Hue=920,Color="777777"},
	--Aqua
	{ Name="8. Blue",Hue=913,Color="FFFFFF"},
	{ Name="8. Blue II",Hue=913,Color="BBBBBB"},
	{ Name="8. Blue III",Hue=913,Color="777777"},
	--Green
	{ Name="8. Green",Hue=550,Color="FFFFFF"},
	{ Name="8. Green II",Hue=550,Color="BBBBBB"},
	{ Name="8. Green III",Hue=550,Color="777777"},
	--Yellow
	{ Name="8. Yellow",Hue=830,Color="FFFFFF"},
	{ Name="8. Yellow II",Hue=830,Color="BBBBBB"},
	{ Name="8. Yellow III",Hue=830,Color="777777"},
	--Orange
	{ Name="8. Orange",Hue=930,Color="FFFFFF"},
	{ Name="8. Orange II",Hue=930,Color="BBBBBB"},
	{ Name="8. Orange III",Hue=930,Color="777777"},
	--Brown
	{ Name="8. Brown",Hue=815,Color="FFFFFF"},
	{ Name="8. Brown II",Hue=815,Color="BBBBBB"},
	{ Name="8. Brown III",Hue=815,Color="777777"},
	--White
	{ Name="6. White",Hue=819,Color="FFFFFF"},
	{ Name="6. White II",Hue=819,Color="BBBBBB"},
	{ Name="6. White III",Hue=819,Color="777777"},
	--Black
	{ Name="7. Black",Hue=1,Color="FFFFFF"},
	{ Name="7. Black II",Hue=1,Color="BBBBBB"},
	{ Name="7. Black III",Hue=1,Color="777777"},
	--Purple
	{ Name="8. Purple",Hue=313,Color="FFFFFF"},
	{ Name="8. Purple II",Hue=313,Color="BBBBBB"},
	{ Name="8. Purple III",Hue=313,Color="777777"},
	--Ice
	--{ Name="Ice",Hue=820,Color="FFFFFF"},
	--{ Name="Ice II",Hue=820,Color="BBBBBB"},
	--{ Name="Ice III",Hue=820,Color="777777"},
	--Sky
	--{ Name="Sky",Hue=821,Color="FFFFFF"},
	--{ Name="Sky II",Hue=821,Color="BBBBBB"},
	--{ Name="Sky III",Hue=821,Color="777777"},
	--Blaze
	--{ Name="Blaze",Hue=826,Color="FFFFFF"},
	--{ Name="Blaze II",Hue=826,Color="BBBBBB"},
	--{ Name="Blaze III",Hue=826,Color="777777"},
	--Volcanic
	--{ Name="Volcanic",Hue=832,Color="FFFFFF"},
	--{ Name="Volcanic II",Hue=832,Color="BBBBBB"},
	--{ Name="Volcanic III",Hue=832,Color="777777"},
	--Blood
	--{ Name="Blood",Hue=862,Color="FF0000"},
}