require 'editors.color_picker'
require 'editors.essence_picker'

--- display the equipment editor
-- @param user - the user to open this dynamic window for
-- @parm object - the object currently being edited. Can be nil
function DisplayEquipmentEditor(user,object)
	-- if object is nil, show nothing
	if(object == nil) then 
		user:SystemMessage("[FF7700]Something bad happened, the object your trying to edit is no longer their")
		return 
	end
	
	-- create local objects and variables
	local dynamicWindow = DynamicWindow("EquipmentEditorDynamicResponse","",300,430,-150,-115,"TransparentDraggable","Center")
	local scrollWindow = ScrollWindow(10,185,275,200,25)
	local scrollElement = nil
	local enchants = this:GetObjVar("Enchants") or nil
    -- create the main background window
    dynamicWindow:AddImage(1,1,"HueSwatch",300,430,"Simple","252535")
    dynamicWindow:AddImage(3,3,"HueSwatch",296,426,"Simple","151525")
    
    -- create the header background and text
    dynamicWindow:AddImage(1,1,"HueSwatch",300,34,"Simple","353545")
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",306,40,"Sliced","557575")
    dynamicWindow:AddLabel(150,10,"[959555]Equipment Editor",300,20,20,"center")	
	
	-- register event handler for dynamic window
	RegisterEventHandler(EventType.DynamicWindowResponse,"EquipmentEditorDynamicResponse", HandleDynamicWindowResponse)

	-- draw the close button on the dynamic window
	DrawCloseButton(271,9,dynamicWindow)
	
	-- populate the dynamic window with editing features
	-- Name
	dynamicWindow:AddLabel(10,45,"[959555]Name",80,18,18,"left")
	dynamicWindow:AddTextField(80,43,192,20,"ItemName",object:GetName())
	-- Durability
	dynamicWindow:AddLabel(10,70,"[959555]Durability",80,18,18,"left")
	dynamicWindow:AddTextField(80,68,192,20,"ItemDurability",tostring(object:GetObjVar("MaxDurability")))
	-- Hue
	dynamicWindow:AddLabel(10,95,"[959555]Hue",80,18,18,"left")
	dynamicWindow:AddTextField(80,93,192,20,"ItemHue",tostring(object:GetHue()))
	-- Color
	dynamicWindow:AddLabel(10,120,"[959555]Color",80,18,18,"left")
	dynamicWindow:AddTextField(80,118,192,20,"ItemColor",object:GetColor())

	-- create the edit buttons for each element
	dynamicWindow:AddImage(276,107,"SettingsButton_Default",18,18,"Simple","9595A5")
	dynamicWindow:AddButton(276,107,"OpenColorPicker|","",18,18,"[CCCC33]Opens the Color Picker\n[FFFFFF]this will open up the color picker.","",false,"Invisible")
	
    -- create the enchant header background and text
    dynamicWindow:AddImage(1,145,"HueSwatch",300,34,"Simple","353545")
    dynamicWindow:AddImage(-2,143,"CraftingItemsFrame",306,40,"Sliced","557575")
    dynamicWindow:AddLabel(150,155,"[959555]Enchants",300,20,20,"center")

    if(enchants ~= nil) then
		-- create the scroll elements from the items enchants objvar
		for key,value in pairs(enchants) do
			-- create local objects and variables
			local effectMin = EssenceData[value.essenceName].EffectRanges[1][1] or 1
			local effectMax = EssenceData[value.essenceName].EffectRanges[3][2] or 1			

			-- create instance of the scroll element
			scrollElement = ScrollElement()
			
			-- create scroll element background
			scrollElement:AddImage(1,1,"HueSwatch",264,25,"Simple","353545")
			scrollElement:AddImage(2,2,"HueSwatch",262,23,"Simple","252535")

			-- show a tooltip when you hover over displaying information about the enchant itself
			scrollElement:AddButton(1,1,"NoButton|","",210,25,"[CCCC33]Description\n[FFFFFF]".. EssenceData[value.essenceName].EffectName .. "\n\n[CCCC00]MobileMod\n[FFFFFF]" ..EssenceData[value.essenceName].MobileMod.."\n\n[CCCC00]Suggested Values\n[FFFFFF]Between "..effectMin.." - ".. effectMax,"",false,"Invisible")

			-- add the essence textfield for editing the value
			-- the identifier in the event handler is the essenceName key entry
			scrollElement:AddTextField(220,3,40,20,value.essenceName,tostring(value.essenceModifier))
			-- display the essence effect name
			scrollElement:AddLabel(26,6,"[959555]"..EssenceData[value.essenceName].Name,200,18,18,"left")

			-- draw the delete enchant button
			--border background color
			scrollElement:AddImage(3,3,"Blank",18,18,"Sliced","550000")
			--background color
			scrollElement:AddImage(5,5,"Blank",14,14,"Sliced","250000")
			--invisible button
			scrollElement:AddButton(3,3,"DeleteEnchant|"..value.essenceName,"",18,18,"[CCCC33]Delete Enchant\n[FFFFFF]deletes the " .. EssenceData[value.essenceName].Name .. " enchant.","",false,"Invisible")
			--draw the "X" string
			scrollElement:AddLabel(8,5,"[959555]X",18,18,18,"left")			
			-- supplly the button with the hue and color to send to the event handler
			--scrollElement:AddButton(1,1,"SetColor|","",160,25,"[4488CC]ToolTip","",true,"Invisible")

			-- attach the scroll elemental to the scroll window
			scrollWindow:Add(scrollElement) 		
		end
	end

	--create the script button on the bottom left
	dynamicWindow:AddImage(11,396,"HueSwatch",90,25,"Simple","353545")
	dynamicWindow:AddImage(12,397,"HueSwatch",88,23,"Simple","252535")
	dynamicWindow:AddLabel(55,400,"[959555]Script It",90,22,22,"center")
	dynamicWindow:AddButton(11,396,"ScriptIt|","",90,25,"[CCCC33]Script It\n[FFFFFF]create a xml file of this item","",false,"Invisible")

	-- create the Add Enchant button in the bottom middle
	dynamicWindow:AddImage(106,396,"HueSwatch",90,25,"Simple","353545")
	dynamicWindow:AddImage(107,397,"HueSwatch",88,23,"Simple","252535")
	dynamicWindow:AddLabel(150,400,"[959555]Enchant",90,22,22,"center")
	dynamicWindow:AddButton(106,396,"OpenEnchantPicker|","",90,25,"[CCCC33]Enchant Item\n[FFFFFF]opens up the enchant picker","",false,"Invisible")

	-- create the update enchant button on the bottom right
	dynamicWindow:AddImage(201,396,"HueSwatch",90,25,"Simple","353545")
	dynamicWindow:AddImage(202,397,"HueSwatch",88,23,"Simple","252535")
	dynamicWindow:AddLabel(245,400,"[959555]Update",90,22,22,"center")
	dynamicWindow:AddButton(201,396,"UpdateChanges|","",90,25,"[CCCC33]Update Changes\n[FFFFFF]updates changed text fields","",false,"Invisible")
	
	--attach the scrollWindow object to the dynamic window
	dynamicWindow:AddScrollWindow(scrollWindow)
	--open up the finalized dynamic window for the user
	user:OpenDynamicWindow(dynamicWindow,object)
end

--- this will draw the close button on a dynamic window
-- @param x - this is the x location to draw the close button at
-- @param y - this is the y location to draw the close button at
-- @param dynamicWindow - this is the dynamic window to attach the close button too
function DrawCloseButton(x,y,dynamicWindow)
	--border background color
	dynamicWindow:AddImage(x,y,"Blank",18,18,"Sliced","555555")
	--background color
	dynamicWindow:AddImage(x+2,y+2,"Blank",14,14,"Sliced","252525")
	--invisible button
	dynamicWindow:AddButton(x,y,"CloseButton|close","",18,18,"[CCCC33]Close Button\n[FFFFFF]clicking this will close the window.","",true,"Invisible")
	--draw the "X" string
	dynamicWindow:AddLabel(x+5,y+3,"[959555]X",18,18,18,"left")
end

--- this handles the dynamic window response
-- @param user - player object to open the window for
-- @param buttonId - the dynamic window button id clicked/used
-- @param fieldData - holds the the textfield data on a dynamic window
function HandleDynamicWindowResponse(user, buttonId, fieldData)
    local responseData = StringSplit(buttonId,"|")
    local button = responseData[1]
    local args = responseData[2]

    if(button == "OpenColorPicker") then
    	DisplayColorEditor(user,this)
    end

    if(button == "OpenEnchantPicker")then
    	DisplayEssenceEditor(user,this)
   	end	

   	if(button == "UpdateChanges") then
   		HandleUpdateChanges(user,fieldData)
   	end

   	if(button == "DeleteEnchant") then
		HandleDeleteOfEnchant(user, args)
   	end
   	SetItemTooltip(this)
end

-- handles the delete button response
function HandleDeleteOfEnchant(user, enchantId)
	local enchants = this:GetObjVar("Enchants")
	if(enchants ~= nil) then
		for key,value in pairs(enchants) do
			-- is this the enchant we want to delete?
			if(value.essenceName == enchantId) then
				-- delete the enchant, update the enchants table and reopoen the main window
				table.remove(enchants,key)
				this:SetObjVar("Enchants",enchants)
				this:SendMessage("OpenForUser",user)

				-- finally let the user know it was deleted
				user:SystemMessage(enchantId .. " has been removed","event")
				return
			end
		end
	end
end

-- handles the saving of textfield data changed by the editor
function HandleUpdateChanges(user, fieldData)
	-- handle top textfield entries
	this:SetName(fieldData.ItemName)
	this:SetHue(tonumber(fieldData.ItemHue))
	this:SetColor(fieldData.ItemColor)
	if(this:HasObjVar("MaxDurability")) then this:SetObjVar("MaxDurability",tonumber(fieldData.ItemDurability)) end

	-- update the enchants
	local enchants = this:GetObjVar("Enchants")
	if(enchants ~= nil) then
		for key,value in pairs(enchants) do
			value.essenceModifier = tonumber(fieldData[value.essenceName])
		end
		this:SetObjVar("Enchants",enchants)
	end
	-- refresh equipment editor window
	this:SendMessage("OpenForUser",user)

	-- finally let the user know things are updated
	user:SystemMessage(this:GetName() .. " properties updated","event")
end

--- this handles the module attach event
function HandleModuleAttached()
	-- incase we close the window, add a Edit to the context menu
	AddUseCase(this,"Edit",false,"IsGod")
end

--- this handles the event message for UsedObject
function HandleMessageUseObject(user,usedType)
	if(IsGod(user)) then
		DisplayEquipmentEditor(user,this)
	end
end

function HandleOpenForUser(user)
	DisplayEquipmentEditor(user,this)
end

RegisterEventHandler(EventType.Message, "OpenForUser", HandleOpenForUser)

RegisterEventHandler(EventType.Message, "UseObject", HandleMessageUseObject)

--- register the event handler for when this module gets attached to the object
RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), HandleModuleAttached)