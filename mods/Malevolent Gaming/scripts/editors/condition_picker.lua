--- Show the condition picker
-- @param user -
-- @param object -
-- @param node -
-- @param index -
function ShowConditionPicker(user,object,node,index,actionName)
	-- create local objects and variables
	local name = "Conditions"
	local textColor = "FFFF00"
	
	-- default to HasObjVar for actionName
	if(actionName == nil) then actionName = "HasObjVar" end
	
	-- the table for the DialogTree is located in the editors/dialog_editior.lua file
	local nodeObject = object:GetObjVar("DialogTree")
	local scrollElement = ScrollElement()
	
	-- current conditions window
	local currentConditionsScrollWindow = ScrollWindow(10,30,200,200,20)
	
	-- available conditions window
	local availableConditionsScrollWindow = ScrollWindow(230,30,240,120,20)
	if(node ~= nil and index ~= nil) then
		name = node .. ":"..index
	end
	
	-- create the dynamic window
	local dynamicWindow = DynamicWindow("ConditionsEditorMain",name,500,290,648,-280,"Parchment","Bottom")
	
	-- condition window text titles
	dynamicWindow:AddLabel(100,0,"[880000]Current Conditions",200,24,24,"center",false,false,"PermianSlabSerif_Dynamic_Bold","center")
	dynamicWindow:AddLabel(350,0,"[880000]Available Conditions",250,24,24,"center",false,false,"PermianSlabSerif_Dynamic_Bold","center")
	
	--create the available action scroll element entries
	textColor = GetTextColor("HasObjVar",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]HasObjVar",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectCondition|HasObjVar|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the HasObjVar properties to edit and insert.","",false,"Invisible")
	availableConditionsScrollWindow:Add(scrollElement)

	textColor = GetTextColor("HasObjVarGreaterThan",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]HasObjVarGreaterThan",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectCondition|HasObjVarGreaterThan|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the HasObjVarGreaterThan properties to edit and insert.","",false,"Invisible")
	availableConditionsScrollWindow:Add(scrollElement)

	textColor = GetTextColor("HasObjVarLessThan",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]HasObjVarLessThan",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectCondition|HasObjVarLessThan|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the HasObjVarLessThan properties to edit and insert.","",false,"Invisible")
	availableConditionsScrollWindow:Add(scrollElement)
	
	textColor = GetTextColor("NotHasObjVar",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]NotHasObjVar",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectCondition|NotHasObjVar|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the NotHasObjVar properties to edit and insert.","",false,"Invisible")
	availableConditionsScrollWindow:Add(scrollElement)	

	-- set up the current actions already applied to this player response node	
	for key,value in pairs(nodeObject[node].Player.Responses[index].Requirements) do
		scrollElement = ScrollElement()
		scrollElement:AddLabel(25,2,"[000000]" .. value.Type,190,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
		scrollElement:AddImage(0,0,"BasicWindow_CloseButton_Default",20,20,"Simple","FFFFFF")
		scrollElement:AddButton(0,2,"RemoveCondition|"..node.."|".. index .. "|" .. key .. "|" .. actionName,"",20,20,"[FF7700]Remove Action\n[FFFFFF]click to delete this action\n[CCCC44]Arguments\n[FFFFFF]" ..value.Values,"",false,"Invisible")
		currentConditionsScrollWindow:Add(scrollElement)
	end

	-- add the action windows to the dynamic window object
	dynamicWindow:AddScrollWindow(currentConditionsScrollWindow)
	dynamicWindow:AddScrollWindow(availableConditionsScrollWindow)
	
	if(actionName == "HasObjVar") then
		dynamicWindow:AddLabel(230,160,"[880000]Enter the objvar in the text field",250,40,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")		
		dynamicWindow:AddImage(230,180,"HueSwatch",250,20,"Simple","252525")
		dynamicWindow:AddTextField(230,180,250,20,"ObjVarName","")
		dynamicWindow:AddButton(330,210,"InsertCondition|"..node.."|" .. index .. "|HasObjVar","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end

	if(actionName == "HasObjVarGreaterThan") then
		dynamicWindow:AddLabel(230,160,"[880000]Enter the objvar and value",200,40,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")		
		dynamicWindow:AddImage(230,180,"HueSwatch",200,20,"Simple","252525")
		dynamicWindow:AddImage(435,180,"HueSwatch",40,20,"Simple","252525")
		dynamicWindow:AddTextField(230,180,200,20,"ObjVarName","")
		dynamicWindow:AddTextField(435,180,40,20,"ObjVarValue","")
		dynamicWindow:AddButton(330,210,"InsertCondition|"..node.."|" .. index .. "|HasObjVarGreaterThan","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end

	if(actionName == "HasObjVarLessThan") then
		dynamicWindow:AddLabel(230,160,"[880000]Enter the objvar and value",200,40,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")		
		dynamicWindow:AddImage(230,180,"HueSwatch",200,20,"Simple","252525")
		dynamicWindow:AddImage(435,180,"HueSwatch",40,20,"Simple","252525")
		dynamicWindow:AddTextField(230,180,200,20,"ObjVarName","")
		dynamicWindow:AddTextField(435,180,40,20,"ObjVarValue","")
		dynamicWindow:AddButton(330,210,"InsertCondition|"..node.."|" .. index .. "|HasObjVarLessThan","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end

	if(actionName == "NotHasObjVar") then
		dynamicWindow:AddLabel(230,160,"[880000]Enter the objvar in the text field",250,40,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")		
		dynamicWindow:AddImage(230,180,"HueSwatch",250,20,"Simple","252525")
		dynamicWindow:AddTextField(230,180,250,20,"ObjVarName","")
		dynamicWindow:AddButton(330,210,"InsertCondition|"..node.."|" .. index .. "|NotHasObjVar","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end
	-- finally display all this fantasticness to the user	
	user:OpenDynamicWindow(dynamicWindow,object)
end

--- handle the dynamic window response for the picker
-- @param elementName -
-- @param actionName -
function GetTextColor(elementName,actionName)
	if(elementName == actionName) then return "009900" end
	return "000000"
end

--- handle the dynamic window response for the picker
-- @param user -
-- @param buttonId -
-- @param fieldData - 
function HandleDynamicWindowResponseConditionPicker(user, buttonId, fieldData)
    -- responseData array size may vary based on button selection
    local responseData = StringSplit(buttonId,"|")
    local button = responseData[1]

    if(button=="SelectCondition") then
    	ShowConditionPicker(user,this,responseData[3],tonumber(responseData[4]),responseData[2])
    end

    if(button=="InsertCondition") then
    	local dialogTree = this:GetObjVar("DialogTree")

    	if(responseData[4] == "HasObjVar") then
			if(fieldData.ObjVarName ~= "") then 
    			table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Requirements, {Type = responseData[4], Values = fieldData.ObjVarName})
    		else
    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]No objvar in text field\n[CCCC33]Fix: [FFFFFF]Type the objvar needed in the text field")
    		end    
    	end
    	if(responseData[4] == "HasObjVarGreaterThan") then
			if(fieldData.ObjVarName ~= "" and fieldData.ObjVarValue ~= "") then 
    			table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Requirements, {Type = responseData[4], Values = fieldData.ObjVarName .. "|" .. fieldData.ObjVarValue})
    		else
    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]Missing ObjVar or Value\n[CCCC33]Fix: [FFFFFF]You need a ObjVar and Value")
    		end 
    	end
    	if(responseData[4] == "HasObjVarLessThan") then
			if(fieldData.ObjVarName ~= "" and fieldData.ObjVarValue ~= "") then 
    			table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Requirements, {Type = responseData[4], Values = fieldData.ObjVarName .. "|" .. fieldData.ObjVarValue})
    		else
    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]Missing ObjVar or Value\n[CCCC33]Fix: [FFFFFF]You need a ObjVar and Value")
    		end 
    	end  
    	if(responseData[4] == "NotHasObjVar") then
			if(fieldData.ObjVarName ~= "") then 
    			table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Requirements, {Type = responseData[4], Values = fieldData.ObjVarName})
    		else
    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]No objvar in text field\n[CCCC33]Fix: [FFFFFF]Type the objvar needed in the text field")
    		end    
    	end    	  	
    	-- save the action information to the dialogTree object
    	this:SetObjVar("DialogTree",dialogTree)
    	
    	-- update changes to the action picker
    	ShowConditionPicker(user,this,responseData[2],tonumber(responseData[3]),responseData[4])
    end

    if(button == "RemoveCondition") then
    	local dialogTree = this:GetObjVar("DialogTree")

    	-- remove the condition entry
    	table.remove(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Requirements,tonumber(responseData[4]))
    	
    	-- save the condition information to the dialogTree object
    	this:SetObjVar("DialogTree",dialogTree)
    	
    	-- update changes to the action picker
    	ShowConditionPicker(user,this,responseData[2],tonumber(responseData[3]),responseData[5])    	
    end
end

--- checks to see if a given action is already present in the event you only want to allow one
--- such as closeing the dialog or opening a bank
-- @param dialogTree -
-- @param responseData -
-- @param actionName - 
function HasConditionType(dialogTree,responseData,actionName)
	for key,value in pairs(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Requirements) do
		if(value.Type == actionName) then
	 		return true
		end
	end	
	return false
end

-- 
RegisterEventHandler(EventType.DynamicWindowResponse,"ConditionsEditorMain",HandleDynamicWindowResponseConditionPicker)
