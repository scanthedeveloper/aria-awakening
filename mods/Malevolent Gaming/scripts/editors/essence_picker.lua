--- display the essence editor
-- @param user - the user to open this dynamic window for
-- @parm object - the object currently being edited. Can be nil
function DisplayEssenceEditor(user,object)
	-- create local objects and variables
	local dynamicWindow = DynamicWindow("EssenceEditorDynamicResponse","",100,230,-150,-110,"TransparentDraggable","Center")
	local scrollWindow = ScrollWindow(10,45,175,175,25)
	local scrollElement = nil    
	local indexKey = {}
	local essenceKey = ""
    local essenceTable = GetEssenceInfo()
    -- create the main background window
    dynamicWindow:AddImage(1,1,"HueSwatch",200,230,"Simple","252535")
    dynamicWindow:AddImage(3,3,"HueSwatch",194,224,"Simple","151525")
    -- create the header background and text
    dynamicWindow:AddImage(1,1,"HueSwatch",200,34,"Simple","353545")
    dynamicWindow:AddImage(-2,-2,"CraftingItemsFrame",206,40,"Sliced","557575")
    dynamicWindow:AddLabel(90,10,"[959555]Enchant Picker",120,20,20,"center")	

	-- register event handler for dynamic window
	RegisterEventHandler(EventType.DynamicWindowResponse,"EssenceEditorDynamicResponse", HandleEssenceEditorDynamicWindowResponse)
	
	-- sort the essence table alphabetically by name
	table.sort(essenceTable,function (a,b)
		if (a.EssenceName < b.EssenceName) then
			return true
		else
			return false
		end
	end)

	-- create the scroll elements from the ColorTable table
	for index,value in pairs(essenceTable) do
		scrollElement = ScrollElement()
		
		-- create scroll element background and text
		scrollElement:AddImage(1,1,"HueSwatch",160,25,"Simple","353545")
		scrollElement:AddImage(2,2,"HueSwatch",158,23,"Simple","252535")
		scrollElement:AddLabel(6,6,"[959555]"..value.EssenceName,160,18,18,"left")
		
		-- get the essence key we want to attach to the button event handler
		scrollElement:AddButton(1,1,"SetEssence|"..value.EssenceKey,"",160,25,"[CCCC33]" .. value.EssenceName .. "\n[959555]Applys the mobile mod [FFFFFF]\n"..value.MobileMod,"",false,"Invisible")
		
		-- attach the scroll elemental to the scroll window
		scrollWindow:Add(scrollElement) 		
	end
	--attach the scrollWindow object to the dynamic window
	dynamicWindow:AddScrollWindow(scrollWindow)

	-- draw the close button on the dynamic window
	DrawCloseButton(171,9,dynamicWindow)

	-- open up the finalized dynamic window for the user
	user:OpenDynamicWindow(dynamicWindow,object)
end

--- this gets the essence info from the EssenceData global table
function GetEssenceInfo()
	local essenceData = {}
	-- loop through the EssenceData table and find all
	-- values that have a table entry, this is a essence table
	for key,value in pairs(EssenceData) do
		if(type(value)=="table") then
			if(value.Name ~= nil) then
				table.insert(essenceData,{EssenceKey=key,EssenceName=value.Name,MobileMod=value.MobileMod})
				--table.insert(essenceData,value)
			end
		end
	end
	-- finally return t he prepaird table of essences information
	return essenceData
end

--- this handles the dynamic window response
-- @param user - player object to open the window for
-- @param buttonId - the dynamic window button id clicked/used
-- @param fieldData - holds the the textfield data on a dynamic window
function HandleEssenceEditorDynamicWindowResponse(user, buttonId, fieldData)
    -- create local objects and variables
    local responseData = StringSplit(buttonId,"|")
	local button = responseData[1]
	local args = responseData[2]

	--handle button responses
	if(button == "SetEssence") then
		local essenceKey = args
		local itemEnchants = this:GetObjVar("Enchants")
		-- grab max modifier value from essence entry
		local modiferValue = EssenceData[essenceKey].EffectRanges[3][2] or 0
		-- does the item have a enchant table?
		if(itemEnchants ~= nil) then
			-- verify the item does not have this enchant already
			for key,value in pairs(itemEnchants) do
				if(essenceKey == value.essenceName) then
					user:SystemMessage("[CCCC33]This item already has the essence key [959555]"..essenceKey,"event")
					return
				end
			end
			-- insert the new essence mod into the enchant table
			-- note that effect ranges have 3 entries so if you make
			-- custom essences and have an error this might be why
			table.insert(itemEnchants,{essenceName=essenceKey,essenceModifier=modiferValue})
			-- save the changes to the enchant table
			this:SetObjVar("Enchants",itemEnchants)
			-- reset the tooltip info on the item to reflect the changes
			SetItemTooltip(this)
			this:SendMessage("OpenForUser",user)
			user:SystemMessage("added the enchant " .. essenceKey,"event")
		else
			itemEnchants = {}
			-- insert the new essence mod into the enchant table
			-- note that effect ranges have 3 entries so if you make
			-- custom essences and have an error this might be why
			table.insert(itemEnchants,{essenceName=essenceKey,essenceModifier=modiferValue})
			-- save the changes to the enchant table
			this:SetObjVar("Enchants",itemEnchants)
			-- reset the tooltip info on the item to reflect the changes
			SetItemTooltip(this) 
			this:SendMessage("OpenForUser",user)
			user:SystemMessage("added the enchant " .. essenceKey,"event")   			
		end
	end
end