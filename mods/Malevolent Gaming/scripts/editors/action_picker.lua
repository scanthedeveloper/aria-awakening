--- Show the action picker
-- @param user -
-- @param object -
-- @param node -
-- @param index -
function ShowActionPicker(user,object,node,index,actionName)
	-- create local objects and variables
	local name = "Actions"
	local textColor = "FFFF00"
	
	-- default to CloseDialog for actionName
	if(actionName == nil) then actionName = "CloseDialog" end
	
	-- the table for the DialogTree is located in the editors/dialog_editior.lua file
	local nodeObject = object:GetObjVar("DialogTree")
	local scrollElement = ScrollElement()
	
	-- current actions window
	local currentActionsScrollWindow = ScrollWindow(10,30,200,200,20)
	
	-- available actions window
	local availableActionsScrollWindow = ScrollWindow(230,30,240,120,20)
	if(node ~= nil and index ~= nil) then
		name = node .. ":"..index
	end
	
	-- create the dynamic window
	local dynamicWindow = DynamicWindow("ActionEditorMain",name,500,290,648,-280,"Parchment","Bottom")
	
	-- action window text titles
	dynamicWindow:AddLabel(100,0,"[880000]Current Actions",200,24,24,"center",false,false,"PermianSlabSerif_Dynamic_Bold","center")
	dynamicWindow:AddLabel(350,0,"[880000]Available Actions",250,24,24,"center",false,false,"PermianSlabSerif_Dynamic_Bold","center")
	
	--create the available action scroll element entries
	textColor = GetTextColor("CloseDialog",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]CloseDialog",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectAction|CloseDialog|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the CloseDialog properties to edit and insert.","",false,"Invisible")
	availableActionsScrollWindow:Add(scrollElement)
	
	textColor = GetTextColor("GotoNode",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]GotoNode",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectAction|GotoNode|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the GotoNode properties to edit and insert.","",false,"Invisible")
	availableActionsScrollWindow:Add(scrollElement)

	textColor = GetTextColor("OpenBank",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]OpenBank",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectAction|OpenBank|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the OpenBank properties to edit and insert.","",false,"Invisible")
	availableActionsScrollWindow:Add(scrollElement)

	textColor = GetTextColor("StablePet",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]StablePet",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectAction|StablePet|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the StablePet properties to edit and insert.","",false,"Invisible")
	availableActionsScrollWindow:Add(scrollElement)
	
	textColor = GetTextColor("ShowPets",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]ShowPets",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectAction|ShowPets|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the ShowPets properties to edit and insert.","",false,"Invisible")
	availableActionsScrollWindow:Add(scrollElement)

	textColor = GetTextColor("SetObjVar",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]SetObjVar",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectAction|SetObjVar|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the SetObjVar properties to edit and insert.","",false,"Invisible")
	availableActionsScrollWindow:Add(scrollElement)

	textColor = GetTextColor("DelObjVar",actionName)
	scrollElement = ScrollElement()
	scrollElement:AddLabel(0,2,"[" .. textColor .. "]DelObjVar",240,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
	scrollElement:AddButton(0,2,"SelectAction|DelObjVar|"..node.."|"..index,"",240,20,"[FF7700]Select\n[FFFFFF]Click to select and bring up the DelObjVar properties to edit and insert.","",false,"Invisible")
	availableActionsScrollWindow:Add(scrollElement)

	-- set up the current actions already applied to this player response node	
	for key,value in pairs(nodeObject[node].Player.Responses[index].Actions) do
		scrollElement = ScrollElement()
		scrollElement:AddLabel(25,2,"[000000]" .. value.Type,190,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
		scrollElement:AddImage(0,0,"BasicWindow_CloseButton_Default",20,20,"Simple","FFFFFF")
		scrollElement:AddButton(0,2,"RemoveAction|"..node.."|".. index .. "|" .. key .. "|" .. actionName,"",20,20,"[FF7700]Remove Action\n[FFFFFF]click to delete this action\n[CCCC44]Arguments\n[FFFFFF]" ..value.Values,"",false,"Invisible")
		currentActionsScrollWindow:Add(scrollElement)
	end

	-- add the action windows to the dynamic window object
	dynamicWindow:AddScrollWindow(currentActionsScrollWindow)
	dynamicWindow:AddScrollWindow(availableActionsScrollWindow)
	
	-- create the editor property elements needed for each action type
	if(actionName == "GotoNode") then
		dynamicWindow:AddLabel(230,160,"[880000]Enter a node in the text field",250,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
		dynamicWindow:AddImage(230,180,"HueSwatch",250,20,"Simple","252525")
		dynamicWindow:AddTextField(230,180,250,20,"Arguments","")
		dynamicWindow:AddButton(330,210,"InsertAction|"..node.."|" .. index .. "|GotoNode","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end

	if(actionName == "CloseDialog") then
		dynamicWindow:AddLabel(230,160,"[880000]Click insert to add the close action",250,40,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")		
		dynamicWindow:AddButton(330,210,"InsertAction|"..node.."|" .. index .. "|CloseDialog","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end

	if(actionName == "OpenBank") then
		dynamicWindow:AddLabel(230,160,"[880000]Click insert to add the bank action",250,40,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")		
		dynamicWindow:AddButton(330,210,"InsertAction|"..node.."|" .. index .. "|OpenBank","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end

	if(actionName == "StablePet") then
		dynamicWindow:AddLabel(230,160,"[880000]Click insert to add the stable pet action",250,40,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")		
		dynamicWindow:AddButton(330,210,"InsertAction|"..node.."|" .. index .. "|StablePet","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end

	if(actionName == "ShowPets") then
		dynamicWindow:AddLabel(230,160,"[880000]Click insert to add the show pet action",250,40,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")		
		dynamicWindow:AddButton(330,210,"InsertAction|"..node.."|" .. index .. "|ShowPets","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end

	if(actionName == "SetObjVar") then
		dynamicWindow:AddLabel(230,160,"[880000]Enter the objvar and value",200,40,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")		
		dynamicWindow:AddImage(230,180,"HueSwatch",200,20,"Simple","252525")
		dynamicWindow:AddImage(435,180,"HueSwatch",40,20,"Simple","252525")
		dynamicWindow:AddTextField(230,180,200,20,"ObjVarName","")
		dynamicWindow:AddTextField(435,180,40,20,"ObjVarValue","")
		dynamicWindow:AddButton(330,210,"InsertAction|"..node.."|" .. index .. "|SetObjVar","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end

	if(actionName == "DelObjVar") then
		dynamicWindow:AddLabel(230,160,"[880000]Enter a node in the text field",250,20,20,"left",false,false,"PermianSlabSerif_Dynamic_Bold")
		dynamicWindow:AddImage(230,180,"HueSwatch",250,20,"Simple","252525")
		dynamicWindow:AddTextField(230,180,250,20,"DelObjVarName","")
		dynamicWindow:AddButton(330,210,"InsertAction|"..node.."|" .. index .. "|DelObjVar","[FF0000]Insert",150,28,"[FF7700]Insert Action\n[FFFFFF]Insert the " .. actionName .. " into the current actions list.","",false,"ParchmentButton")
	end

	-- finally display all this fantasticness to the user	
	user:OpenDynamicWindow(dynamicWindow,object)
end

--- handle the dynamic window response for the picker
-- @param elementName -
-- @param actionName -
function GetTextColor(elementName,actionName)
	if(elementName == actionName) then return "009900" end
	return "000000"
end

--- handle the dynamic window response for the picker
-- @param user -
-- @param buttonId -
-- @param fieldData - 
function HandleDynamicWindowResponseActionPicker(user, buttonId, fieldData)
    -- responseData array size may vary based on button selection
    local responseData = StringSplit(buttonId,"|")
    local button = responseData[1]

    --@responseData 1 (button) SelectAction - the button being clicked
    --@responseData 2 (string) node - the node of the dialogTree objvar
    --@responseData 3 (number) index - the index of the player response table
    --@responseData 4 (string) actionName - the actionName
    if(button=="SelectAction") then
    	ShowActionPicker(user,this,responseData[3],tonumber(responseData[4]),responseData[2])
    end

    if(button=="InsertAction") then
    	local dialogTree = this:GetObjVar("DialogTree")

    	if(responseData[4] == "GotoNode") then
    		if(HasActionType(dialogTree,responseData,"GotoNode")) then
	    		user:SystemMessage("\n[FF9922]Error: [FFFFFF]More then one goto node\n[CCCC33]Fix: [FFFFFF]Can only have one goto node action")
	    	else
	    		if(fieldData.Arguments ~= "") then 
	    			table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions, {Type = responseData[4], Values = fieldData.Arguments})
	    		else
	    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]No node entered in text field\n[CCCC33]Fix: [FFFFFF]Type the dialog node to anchor too in the text field")
	    		end
    		end
    	end

    	if(responseData[4] == "CloseDialog") then
    		if (HasActionType(dialogTree,responseData,"CloseDialog") == true) then
    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]More then one close dialog\n[CCCC33]Fix: [FFFFFF]Can only have one close dialog action")
    		else
    			table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions, {Type = responseData[4], Values = "None"})
    		end
    	end

    	if(responseData[4] == "OpenBank") then 
    		if (HasActionType(dialogTree,responseData,"OpenBank") == true) then
    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]More then one open bank\n[CCCC33]Fix: [FFFFFF]Can only have one open bank action")
    		else
	    		table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions, {Type = responseData[4], Values = "None"})
    		end    		
    	end

    	if(responseData[4] == "StablePet") then
    		if (HasActionType(dialogTree,responseData,"StablePet") == true) then
    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]More then one stable pet\n[CCCC33]Fix: [FFFFFF]Can only have one stable pet action")
    		else
    			table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions, {Type = responseData[4], Values = "None"})
    			this:SetObjVar("CanStable",true)
    		end
    	end

    	if(responseData[4] == "ShowPets") then
    		if (HasActionType(dialogTree,responseData,"ShowPets") == true) then
    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]More then one show pets\n[CCCC33]Fix: [FFFFFF]Can only have one show pets action")
    		else
    			table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions, {Type = responseData[4], Values = "None"})
    			this:SetObjVar("CanStable",true)
    		end
    	end

    	if(responseData[4] == "SetObjVar") then
			if(fieldData.ObjVarName ~= "") then 	
    			table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions, {Type = responseData[4], Values = fieldData.ObjVarName .. "|" .. fieldData.ObjVarValue})
    		else
    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]Missing ObjVar or Value\n[CCCC33]Fix: [FFFFFF]You need a ObjVar and Value")
    		end 
    	end

    	if(responseData[4] == "DelObjVar") then
			if(fieldData.DelObjVarName ~= "") then 	
    			table.insert(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions, {Type = responseData[4], Values = fieldData.DelObjVarName })
    		else
    			user:SystemMessage("\n[FF9922]Error: [FFFFFF]Missing DelObjVarName in textfield\n[CCCC33]Fix: [FFFFFF]You need a ObjVar name in the text field to delete")
    		end 
    	end
    	-- save the action information to the dialogTree object
    	this:SetObjVar("DialogTree",dialogTree)
    	
    	-- update changes to the action picker
    	ShowActionPicker(user,this,responseData[2],tonumber(responseData[3]),responseData[4])
    end

    if(button == "RemoveAction") then
    	local dialogTree = this:GetObjVar("DialogTree")
    	-- stable fix
    	if(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions[tonumber(responseData[4])].Type=="StablePet" or dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions[tonumber(responseData[4])].Type=="ShowPets") then
    		this:RemoveObjVar("CanStable")
    	end
    	
    	-- remove the action entry
    	table.remove(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions,tonumber(responseData[4]))
    	
    	-- save the action information to the dialogTree object
    	this:SetObjVar("DialogTree",dialogTree)
    	
    	-- update changes to the action picker
    	ShowActionPicker(user,this,responseData[2],tonumber(responseData[3]),responseData[5])    	
    end
end

--- checks to see if a given action is already present in the event you only want to allow one
--- such as closeing the dialog or opening a bank
-- @param dialogTree -
-- @param responseData -
-- @param actionName - 
function HasActionType(dialogTree,responseData,actionName)
	for key,value in pairs(dialogTree[responseData[2]].Player.Responses[tonumber(responseData[3])].Actions) do
		if(value.Type == actionName) then
	 		return true
		end
	end	
	return false
end

-- 
RegisterEventHandler(EventType.DynamicWindowResponse,"ActionEditorMain",HandleDynamicWindowResponseActionPicker)
