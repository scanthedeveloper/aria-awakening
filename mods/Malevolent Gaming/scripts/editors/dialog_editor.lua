--- require the action picker dynamic window
require 'editors.action_picker'
require 'editors.condition_picker'
require 'xml.dialog_exporter'

--- shows the dialog editor
-- @param user - the player to send the dynamic window too
-- @param parentobject - this is the object you wish to handle the event calls 
function ShowDialogEditor(user,node,showEditor)
	-- create local objects and variables needed for the dynamic window object 
	local windowName = "Dialog Editor"
	local nodeObject = this:GetObjVar("DialogTree")
	local curY = 0
		
	-- add node currently being looked at to the title of the dynamic window
	if(node ~= nil) then
		windowName = windowName .. " - " .. node
	else
		-- when player deletes a node, when it loads with nil, find the first node to reload
		for k,v in pairs(nodeObject) do
			node = v.NodeName
			break;
		end
	end
	
	-- create the dynamic window object
	local dynamicWindow = DynamicWindow("DialogEditorMain",windowName,1077,215,-538,-280,"Parchment","Bottom")
	
	------------------------------------------------------------------------------
	-- remove this section once you correct for the nil value of no dialog entries
	if(node == nil) then
		return
	end
	------------------------------------------------------------------------------
	
	--is the node valid?
    if(node ~= nil) then
		-- create the player text responses and buttons
		for key,value in pairs(nodeObject[node].Player.Responses) do
			local actionString = "[FF7700]Actions"
			local conditionString = "[FF7700]Conditions"
			if (nodeObject[node].Player.Responses[key] ~= nil) then				
				if not(showEditor) then
					dynamicWindow:AddButton(466,curY+10,"DialogResponse:"..key,value.Text,245,20,"","",true,"ScrollSelection")
				else
					-- delete response 
					dynamicWindow:AddImage(456,curY+10,"BasicWindow_CloseButton_Default",20,20,"Simple","FFFFFF")
					dynamicWindow:AddButton(456,curY+10,"RemovePlayerChoice|"..node.."|"..key,"",20,20,"[FF7700]Remove Entry\n[FFFFFF]click to delete this entry","",false,"Invisible")
					-- edit the response text
					dynamicWindow:AddImage(486,curY+10,"HueSwatch",330,20,"Simple","252525")
					dynamicWindow:AddTextField(486,curY+10,330,20,"fieldKey_"..key,value.Text)

					--TODO: If you see this I didnt fix it yet
					--show the values of all the actions attached to this player response node
					for k,v in pairs(value.Actions) do
						actionString = actionString .. "\n[CCCC44]Type: " .. v.Type .. "\n"
						actionString = actionString .. "[FFFFFF]Values: " .. v.Values
					end
					--show the values of all the condtions attached to this player response node
					for k,v in pairs(value.Requirements) do
						conditionString = conditionString .. "\n[CCCC44]Type: " .. v.Type .. "\n"
						conditionString = conditionString .. "[FFFFFF]Values: " .. v.Values
					end
					--create the action picker button
					dynamicWindow:AddButton(820,curY+10,"ShowActionPicker|"..node.."|"..key,"[FF7700]A",18,18,actionString,"",false,"ParchmentButton")

					--create the condition picker button
					dynamicWindow:AddButton(845,curY+10,"ShowConditionPicker|"..node.."|"..key,"[FF7700]C",18,18,conditionString,"",false,"ParchmentButton")
				end	
			end			
			curY = curY + 24
		end

		-- if editing show the add player response button
		-- currently only supporting 5 at this moment
		if(showEditor) then
			if(#nodeObject[node].Player.Responses < 5) then
				dynamicWindow:AddButton(482,curY+8,"AddPlayerResponse|"..node,"(+)Add",245,20,"","",false,"ScrollSelection")
			end
		end

		-- create the npc dialog text based on if the player is editing or not
		if not(showEditor) then
			-- get most likely ai_filename
			local filename = ""
			for key,value in pairs(this:GetAllModules()) do
				if(string.sub(value,1,3)=="ai_") then
					filename = string.gsub(value,"ai_","")
					break
				end
			end

			dynamicWindow:AddLabel(10,10,"[555555]"..nodeObject[node].NpcDialog,440,120,20,"left",true,false,"PermianSlabSerif_Dynamic_Bold")
			ShowNodePicker(dynamicWindow,nodeObject,node,showEditor)
			dynamicWindow:AddButton(0,-20,"EditNode|"..node,"[FF0000]Edit",100,28,"[FF7700]Edit Node\n [FFFFFF]edit the " .. node .. " node.","",false,"ParchmentButton")
			dynamicWindow:AddButton(0, 134,"Export|"..node,"[FF0000]Export",100,28,"[FF7700]Export To Lua\n [FFFFFF]this will save the dialog to lua.","reload ai_"..filename,false,"ParchmentButton")
			dynamicWindow:AddImage(183,137,"HueSwatch",274,24,"Simple","4B3715")
			dynamicWindow:AddImage(185,139,"HueSwatch",270,20,"Simple","252525")			
			
			dynamicWindow:AddTextField(185,139,270,20,"fieldKeyExportFileName",filename)			
			dynamicWindow:AddLabel(102,142,"[332508]File Name",130,120,20,"left",true,false,"PermianSlabSerif_Dynamic_Bold")
		else
			dynamicWindow:AddImage(8,8,"HueSwatch",444,124,"Simple","4B3715")
			dynamicWindow:AddImage(10,10,"HueSwatch",440,120,"Simple","252525")
			dynamicWindow:AddTextField(10,10,440,120,"fieldKeyNpcText",nodeObject[node].NpcDialog)
			dynamicWindow:AddButton(0,-20,"SaveNode|"..node,"[FF0000]Save",100,28,"[FF7700]Save Node\n [FFFFFF]save the " .. node .. " node.","",false,"ParchmentButton")
			dynamicWindow:AddButton(870,134,"RemoveNode|"..node,"[FF0000]Remove",100,28,"[FF7700]Remove Node\n [FFFFFF]removes the " .. node .. " node.","",false,"ParchmentButton")
		end
	end
	-- display the dynamic window to the user
	user:OpenDynamicWindow(dynamicWindow,this)
end	

--- show the node picker
-- @param dynamicWindow - the dynamic window to append too
-- @param nodeObject - the dialog tree object
-- @param node - the node in the dialog tree
function ShowNodePicker(dynamicWindow,nodeObject,node)
	local scrollWindow = ScrollWindow(825,40,140,100,20)
	
	-- add a new node button
	dynamicWindow:AddButton(875,-20,"NewNode|","[FF0000]Add",80,28,"[FF7700]Add New Dialog Node\n[FFFFFF]Click to add a new dialog node. Make sure you enter a valide unique node name below in the text field","",false,"ParchmentButton")
	
	-- text field for new node name
	dynamicWindow:AddImage(825,10,"HueSwatch",150,20,"Simple","4B3715")
	dynamicWindow:AddImage(827,12,"HueSwatch",146,16,"Simple","252525")
	dynamicWindow:AddTextField(825,10,150,20,"NewNodeName","")
	
	-- show the different nodes in the dialog
	for key,value in pairs(nodeObject) do
		local scrollElement = ScrollElement()
		scrollElement:AddLabel(5,5,"[332508]"..key,130,120,16,"left",true,false,"PermianSlabSerif_Dynamic_Bold")
		scrollElement:AddButton(5,5,"NodePickerResponse|"..key,"",130,120,"[FF7700]Select\n[FFFFFF]Select to edit the "..key.." node","",false,"Invisible")
		scrollWindow:Add(scrollElement)
	end

	-- attach the scroll window to the dynamic window object
	dynamicWindow:AddScrollWindow(scrollWindow)
end

--- save the edited dialog node
-- @param user - the user to open this dynamic window for
-- @param node - the node in the dialog tree
-- @param fieldData - this holds the test field data entered 
function SaveEdit(user,node,fieldData)
	if(fieldData ~= nil and node ~= nil) then
		local dialogTree = this:GetObjVar("DialogTree")
		
		-- modify npc text
		if(dialogTree[node] ~= nil) then
			dialogTree[node].NpcDialog  = fieldData.fieldKeyNpcText or "Error Happened with assigning NPCText"
		end
		
		-- modify the player actions
		for key,value in pairs(dialogTree[node].Player.Responses) do
			dialogTree[node].Player.Responses[key].Text = fieldData["fieldKey_"..key]
		end
		
		-- save the dialogTree changes
		this:SetObjVar("DialogTree",dialogTree)
	end
end

--- handle the dynamic window response
-- @param user - the user to open this dynamic window for
-- @param buttonId - the button id that was pressed from the dynamic window
-- @param fieldData - this holds the test field data entered
function HandleDynamicWindowResponse(user, buttonId, fieldData)
    local responseData = StringSplit(buttonId,"|")
    local button = responseData[1]
    local args = responseData[2]
	local dialogTree = this:GetObjVar("DialogTree")

    if(button == "SaveNode") then
    	SaveEdit(user,args,fieldData)
    	ShowDialogEditor(user,args,false)
    end
    
    if(button == "EditNode") then
		ShowDialogEditor(user,args,true)
    end
    
    if(button == "AddPlayerResponse") then
    	if(#dialogTree[args].Player.Responses > 4) then return end
    	local newResponse = 
    	{
			Text = "edit me.",
			Actions = 
			{
				{
					Type = "CloseDialog", 
					Values = "None", 
				}
			},
			Requirements = {}, -- can be omitted
    	}
    	table.insert(dialogTree[args].Player.Responses,#dialogTree[args].Player.Responses+1,newResponse)
    	this:SetObjVar("DialogTree",dialogTree) 
    	ShowDialogEditor(user,args,true)
    end
    
    if(button=="NodePickerResponse") then
    	ShowDialogEditor(user,args,false)
    end
    
    if(button=="NewNode") then
   		if(fieldData.NewNodeName == "" or fieldData.NewNodeName == nil) then
   			user:SystemMessage("[FF9922]\nError:[FFFFFF] Missing name for node\n[CCCC33]Fix: [FFFFFF]Enter the name below the Add button")		
   		else
   			-- no spaces
   			local nodeNameFix = string.gsub(fieldData.NewNodeName," ", "")
   			-- no ' 
   			nodeNameFix = string.gsub(nodeNameFix,"'","")
   			dialogTree[nodeNameFix] =
			{
				NodeName = nodeNameFix,
				NpcDialog = "edit me.",
				Player =
				{
					Responses =
					{
						{
							Text = "edit me.",
							Actions = 
							{
								{
									Type = "CloseDialog", -- OpenBank, SetObjVar, RemoveObjVar, AdjustSkill,GiveItem,TakeItem,CloseDialog
									Values = "None", -- can be omitted if not needed by other types
								}
							},
							Requirements = {}, -- can be omitted
						},					
					}
				} ,	
			}
    		this:SetObjVar("DialogTree",dialogTree) 
    		ShowDialogEditor(user,nodeNameFix,false)					
   		end
    end

    if (button == "RemovePlayerChoice") then
    	table.remove(dialogTree[args].Player.Responses,tonumber(responseData[3]))
    	this:SetObjVar("DialogTree",dialogTree)
    	ShowDialogEditor(user,args,true)
    end

    if(button == "RemoveNode") then
    	if(args == "Greeting") then
    		user:SystemMessage("\n[FF9922]Error: [FFFFFF]Can't remove the Greeting node\n[CCCC33]Fix: [FFFFFF]Every dialog must have a Greeting node")
    		ShowDialogEditor(user,"Greeting",true)
    		return
    	end
    	local counter = 1
    	for k,v in pairs(dialogTree) do
    		counter = counter + 1
    		if(k == args) then
    			dialogTree[args] = nil
    			this:SetObjVar("DialogTree",dialogTree)
    			ShowDialogEditor(user,nil,false)
    			return
    		end
    	end
    end

    if(button == "ShowActionPicker") then
    	local node = args
    	local index = tonumber(responseData[3])
    	ShowActionPicker(user,this,node,index)
    end

    if(button == "ShowConditionPicker") then
    	local node = args
    	local index = tonumber(responseData[3])
    	ShowConditionPicker(user,this,node,index)
    end

    if(button == "Export") then
    	user:SystemMessage(fieldData.fieldKeyExportFileName)
    	if(fieldData.fieldKeyExportFileName ~= "") then
	    	ExportDialog(user,dialogTree,fieldData.fieldKeyExportFileName)
    		ShowDialogEditor(user,args,false)
    	else
    		user:SystemMessage("\n[FF9922]Error: [FFFFFF]Requires a file name to save too\n[CCCC33]Fix: [FFFFFF]Add a file name for the output file")
    		ShowDialogEditor(user,"Greeting",false)
    	end
    end
end

-- this handles the dialog button and text field responses from the dynamic window
RegisterEventHandler(EventType.DynamicWindowResponse,"DialogEditorMain",HandleDynamicWindowResponse)

-- when the player selects open dialog editor from the context menu
RegisterEventHandler(EventType.Message, "UseObject", function(user, usedType) 
    if(usedType=="Open Dialog Editor") then
    	ShowDialogEditor(user,"Greeting",false)
    end
end)

-- this fires when the module is attached and sets up needed variables
RegisterEventHandler(EventType.ModuleAttached, GetCurrentModule(), function() 
	-- adds the use case to 
	AddUseCase(this,"Open Dialog Editor",true,"IsGod")
	-- create a clean table to start with
	local dialogTree = 
	{
		Greeting = 
		{
			NodeName = "Greeting",
			NpcDialog = "Example dialog entry, feel free to edit this Greeting node. Every dialog must have a Greeting entry.",
			Player =
			{
				Responses =
				{
					{
						Text = "Click to close dialog.",
						Actions = 
						{
							{
								Type = "CloseDialog", -- OpenBank, SetObjVar, RemoveObjVar, AdjustSkill,GiveItem,TakeItem,CloseDialog
								Values = "None", -- can be omitted if not needed by other types
							}
						},
						Requirements = {}, -- can be omitted
					},					
				}
			} ,	
		},		
	}
	-- save the table information to the DialogTree objvar for usage by this set of scripts
	this:SetObjVar("DialogTree",dialogTree)
end)
