
-- example table to use with the merchant system.
Dialog.LORDGuildQuestObsidian =
{
	StartNode =
	{
		NpcText = "Hello!  I'm a member of the new Legion Guild quest system.  Managing LoRD is a big effort, and Guild Officers require we collect as much Obsidian as possible!",
		Responses =
		{
			{
				Text = "Lets trade some Obsidian.. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
