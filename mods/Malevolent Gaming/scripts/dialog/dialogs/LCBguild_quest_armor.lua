
-- example table to use with the merchant system.
Dialog.LCBGuildArmor =
{
	StartNode =
	{
		NpcText = "Hello!  I'm a member of the new LCB Guild quest system.  I am very excited to offer you some of our newest armor items, exclusively obtained here.  Check out my shop!",
		Responses =
		{
			{
				Text = "Lets trade for some armor...! |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
