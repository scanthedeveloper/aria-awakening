
-- example table to use with the merchant system.
Dialog.NightbornVillageTailor =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Back from the hunt and wish to turn those pelts into something useful? What about that satchel full of cotton on your hip? Long have the Nightborn made use of all our lands have to offer!",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Show me your wares!|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "How do you produce these goods?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "Nightborn have learned how to stitch magic itself into every pattern and every jerkin we make. Our goods are made using Dreamweave Cloth and more mundane goods that can be found here in Celador.",
		Responses =
		{
			{
				Text = "How is Dreamweave Cloth made?|GotoNode|TellMeMoreNode"
			},
			{
				Text = "Good bye"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Well that truly is a secret.  Perhaps someday SCAN, The God of Hope will enlighten you on its process.",
		Responses =
		{
			{
				Text ="Until then..."
			},
			{ 
				Text = "How about we talk some trade buisness now|GotoNode|StartNode",
			},
		},			
	},
}
