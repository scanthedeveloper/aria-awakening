
-- example table to use with the merchant system.
Dialog.MageT1Dialog =
{
	StartNode =
	{

		NpcText = "I sell the Summoner's Grandmaster Magery Armor Set here.  You must be a Grandmaster Mage to purchase this armor set.",

		Responses =
		{
			{
				Text = "I've come to trade my coins!|SellToPlayer"
            },
		},
	},
}
