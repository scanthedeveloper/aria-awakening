
-- example table to use with the merchant system.
Dialog.NightbornVillageArchitect =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Greetings!  I hope you have enjoyed your visit to our village!  I designed all of the buildings my self... with the hard work of our carpenters of course!  Marvel at their splendor.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Show me your wares!|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "What's so special about a Nightborn house?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "Oh you don't know!?  The Dreamwood we build the houses with is imbued with magic!  Each of the blueprints we offer provides its owner with additional, magic storage!",
		Responses =
		{
			{
				Text = "How much magical storage?|GotoNode|TellMeMoreNode"
			},
			{
				Text = "Good bye"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Well both our Tavern and our Villa models provide the owner with +5 storage capacity!  If you're ready to get started, you can start purchasing Dreamwood from me with some of those magical Nightborn Gems!",
		Responses =
		{
			{
				Text ="Thank you for the information!"
			},
			{ 
				Text = "How about we talk some trade buisness now|GotoNode|StartNode",
			},
		},			
	},
}
