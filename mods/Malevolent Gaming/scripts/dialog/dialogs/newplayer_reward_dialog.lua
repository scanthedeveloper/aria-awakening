
-- example table to use with the merchant system.
Dialog.NewPlayerRewardDialog =
{
	StartNode =
	{
		NpcText = "Hello!  Welcome to the Malevolent Multiverse; LOA edition!  I'm here to help you get a new player reward.  I got some unique items for you to view...",
		Responses =
		{
			{
				Text = "Lets trade some Tokens.. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
