
-- example table to use with the merchant system.
Dialog.BlackpathTurncoat =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Yeah you there havin a look about. Come over here Chum! Now see, unlike me fellows in there I knows which way the cookie crumbles. I knows it's bettah to be open to all deals.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Show me your wares.|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "What are you collecting?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "I dont want no ordinary coin tho. Wont do me any good, just bits of gold...? nah. I needs the Runes ma boys be aftah. Blood Runes, look like da color of blood they do.",
		Responses =
		{
			{
				Text = "What are these Runes used for?|GotoNode|TellMeMoreNode"
			},
			{
				Text = "I'll be back later."
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Its rumored dey restore lost powah, at the cost of a blood sacrifice!  Ya bring me those Runes and Ill let ya have a run of some of our supplies I will! Dem boys wont even notice stuffs missin..",
		Responses =
		{
			{ 
				Text = "What are you pandering?|GotoNode|StartNode",
			},
		},			
	},
}
