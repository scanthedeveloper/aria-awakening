
-- example table to use with the merchant system.
Dialog.PatreonCurrencyExchangeDialog =
{
	StartNode =
	{
		NpcText = "As of 08/01/2021 HOPE Society is no longer accepting Patrons.  All items previously gated behind Patron Gems have been converted into Gold and are now available to everyone. ",
		Responses =
		{
			{
				Text = "Let me trade my Patron Gems for Gold.. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
