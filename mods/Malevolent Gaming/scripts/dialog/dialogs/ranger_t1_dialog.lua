
-- example table to use with the merchant system.
Dialog.RangerT1Dialog =
{
	StartNode =
	{

		NpcText = "I sell the Summoner's Grandmaster Ranger Armor Set here.  You must be a Grandmaster Archer to purchase this armor set.",

		Responses =
		{
			{
				Text = "I've come to trade my coins!|SellToPlayer"
            },
		},
	},
}
