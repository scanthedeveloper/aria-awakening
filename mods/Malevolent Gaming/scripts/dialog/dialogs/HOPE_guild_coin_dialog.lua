
-- example table to use with the merchant system.
Dialog.HOPEGuildCoin =
{
	StartNode =
	{
		NpcText = "Cheers Mate! HOPE Society Guild has finally collected all the resources we need.  If you have any [FF9900]HOPE Gold[-] and I will trade you for coin.",
		Responses =
		{
			{
				Text = "Turn in (10) [FF9900]HOPE Gold[-]. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
