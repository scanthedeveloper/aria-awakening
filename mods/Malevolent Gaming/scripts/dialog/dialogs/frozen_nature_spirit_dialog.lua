
-- example table to use with the merchant system.
Dialog.FrozenNatureSpiritDialog =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Greetings mortal.   It wasn't always this way up here. I mean it has been this way since all could remember, but there was a time when things were different. When we once held to the power of the World Tree, and lived under its bounty, grace and majesty.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Turn in quest item...|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "Hear my story...|GotoNode|AboutMyselfNode"
			},
		},
	},

	AboutMyselfNode =
	{
		NpcText = "Long ago, there were no kings or queens, noble born or empires, only the land and growth. My tale was to protect this land, learn and grow, and spark life. But alas I was bound, imprisoned, and separated from life...",
		Responses =
		{
			{
				Text = "How have you survived?|GotoNode|TellMeMoreNode"
			},
		},
	},

	TellMeMoreNode =
	{
		NpcText = "My friend and companion, Vah'Toori protects my spirit.  Long ago, a monster known as the Wendigo corrupted this land.  He feasts upon others, removing all life and joy.  He is truly dangerous...",
		Responses =
		{
			{
				Text = "Sounds like a vicious opponent...|GotoNode|TellMeMoreNode3"
			},			
		},			
	},

	TellMeMoreNode3 =
	{
		NpcText = "The Wendigo is cursed and cannot be killed.  We've spent a thousand years learning his secrets and by doing so we learned The Wendigo is in possession of Blue, Frozen orbs; where his soul has been fragmented by magic and stored within...",
		Responses =
		{
			{
				Text = "That is truly dark sorcery!|GotoNode|TellMeMoreNode4"
			},
		},
	},

	TellMeMoreNode4 =
	{
		NpcText = "Destroy the Wendigo and by chance you may find one of his Frozen Orbs!  Bring them to me and I shall help you break his curse and restore peace to this land.",
		Responses =
		{
			{
				Text = "I will do as you ask.|GotoNode|StartNode"
			},
		},
	},
}
