
-- example table to use with the merchant system.
Dialog.NightbornApprentice =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Hello!  I am the apprentice to Javan, the Great Duke of the Autumn Court and Royal Heirophant.  As his understudy, I have been tasked is observing how the corruption takes form in the marrow of these Toxic Bones.  Bring some to me and I shall reward you.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "I brought the bones!|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "The Mayor sent message to the Autumn Court with notice of a loss of life and potential dark magiks surfacing throughout the Black Forest.  We have come to investigate and report our findings.",
		Responses =
		{
			{
				Text = "Do you think we can remedy this?|GotoNode|TellMeMoreNode"
			},
			{
				Text = "I've heard enough thanks."
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "I am saddened to say that I don't know.  We need more evidence!  Bone Marrow, & Blood will be a good start for us to begin understanding what is happening to our people!  Please help us.",
		Responses =
		{
			{
				Text ="I shall investigate immidiately!"
			},
			{ 
				Text = "Lets talk about that reward again.|GotoNode|StartNode",
			},
		},			
	},
}
