
-- example table to use with the merchant system.
Dialog.DUNEGuildCoin =
{
	StartNode =
	{
		NpcText = "Hello!  I'm a member of the new DUNE Guild quest system.  These Waterthief Fanatics are a plague on these great lands! Steal back our [FF9900]Crystalized Water[-] and I will reward you.",
		Responses =
		{
			{
				Text = "Turn in (5) [FF9900]Crystalized Water[-]. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
