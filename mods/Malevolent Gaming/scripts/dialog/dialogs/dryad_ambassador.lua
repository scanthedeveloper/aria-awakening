
-- example table to use with the merchant system.
Dialog.DryadAmbassador =
{
	StartNode =
	{
		NpcText = "Hey you!  Yes you!  Please help!  Our village has recently been over-run and taken over!  [00FF00]I'm a Dryad from Dreamshire Cavern.[-]  Our little village has recently been taken over by a huge Ghost who calls himself, [FF5555]Frank the Evil Rabbit![-]  Please help us!",
		Responses =
		{
			{
			    Text = "I will help you!|GotoNode|AboutMyselfNode"
			},
			{
			    Text = "I'm not interested."
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "Our village is actually tucked away inside a huge cave in the Spiritwood.  All of our townsfolk have scattered and are hiding out in dangerous Black Forest.  Gather your friends, because Frank the Evil Rabbit is terribly vicious and you will need help!",
		Responses =
		{
			{
				Text = "Thanks, i'm on my way..."
			},
		},
	},
	--[[TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
