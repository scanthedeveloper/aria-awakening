
-- example table to use with the merchant system.
Dialog.NightbornVillageRogue =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Hey there friend! You look like the more discerning type. The type of fellow that might be curious about a lot of things. A lot of things others might just let slip by.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Show me your wares!|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "Perhaps I am?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "While we Nightborn never encourage outright theft in human terms, we do have a very healthy curiosity. A curiosity that sometimes just has to be given freedom to seek answers.",
		Responses =
		{
			{
				Text = "I agree.  What is it that you have?|GotoNode|TellMeMoreNode"
			},
			{
				Text = "Good bye"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "What I am offering my friend is a way to get a few of those answers a little easier. Oh yes, I just might have the right tool to get a lot of questions answered!",
		Responses =
		{
			{
				Text ="Hmmmm..."
			},
			{ 
				Text = "Lets take a look.|GotoNode|StartNode",
			},
		},			
	},
}
