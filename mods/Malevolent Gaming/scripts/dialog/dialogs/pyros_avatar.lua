
-- example table to use with the merchant system.
Dialog.PyrosAvatar =
{
	StartNode =
	{

		NpcText = "My armies!  What have you done!?  You will pay for your intrusion you insufferable mortals.",

		Responses =
		{

			--[[{
				Text = "I've come to trade Nether Essence!|SellToPlayer"
            },
            ]]
			{
				Text = "Who are you!?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "I am the Avatar of Pyros!  I'm here to exact my revenge against SCAN!  Now clear out of here insect before you incur my wrath!  Pyromancy was not his gift to give and I shall end his mortal existence once and for all!!!",
		Responses =
		{
			{
				Text = "It will not be his end but yours!|GotoNode|TellMeMoreNode"
            },
            {
				Text = "How can I help?|GotoNode|TellMeMoreNode2"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "I have no time for this mortal!  Be gone!  I will deal with you later...",
		Responses =
		{
			{ 
				Text = "Pyros be damned!",
			},
		},			
    },
    TellMeMoreNode2 =
	{
		NpcText = "Ah ha!  I've heard of mortals such as your self rejecting their creator...  None of my creations wouldn't dare defy me!  You lack enough vigor to support my cause puny mortal...",
		Responses =
		{
			{ 
				Text = "Bah!  Damn the Gods!",
			},
		},			
	},
}
