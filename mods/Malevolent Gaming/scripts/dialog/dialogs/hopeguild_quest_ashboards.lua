
-- example table to use with the merchant system.
Dialog.HopeGuildQuestAshBoards =
{
	StartNode =
	{
		NpcText = "Hello!  I'm a member of the new HOPE Guild quest system.  Managing HOPE is a big effort, we need to collect as many Ash Boards as possible!",
		Responses =
		{
			{
				Text = "Lets trade some Ash Boards.. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
