
-- example table to use with the merchant system.
Dialog.HopeGuildQuestCobaltIngots =
{
	StartNode =
	{
		NpcText = "Hello!  I'm a member of the new HOPE Guild quest system.  Managing HOPE City is a big effort, we need to collect as many Cobalt Ingots as possible!",
		Responses =
		{
			{
				Text = "Lets trade some Cobalt Ingots.. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
