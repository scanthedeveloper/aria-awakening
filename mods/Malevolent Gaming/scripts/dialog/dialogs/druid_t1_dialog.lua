
-- example table to use with the merchant system.
Dialog.DruidT1Dialog =
{
	StartNode =
	{

		NpcText = "I sell the Summoner's Grandmaster Druid Armor Set here.  You must be a Grandmaster Nature Mage to purchase this armor set.",

		Responses =
		{
			{
				Text = "I've come to trade my coins!|SellToPlayer"
            },
		},
	},
}
