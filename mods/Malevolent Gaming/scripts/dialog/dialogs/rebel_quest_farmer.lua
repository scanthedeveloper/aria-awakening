
-- example table to use with the merchant system.
Dialog.RebelQuestFarmer =
{
	StartNode =
	{

		NpcText = "We were trading supplies at the Bel Haven market and... my farm! Aaaghh! They, they... killed them... killed all of them.   If we had only been here!  No no, we would have been killed as well...  *Cries*...  ",

		Responses =
		{

			{
				Text = "I've exacted your revenge!|SellToPlayer"
			},
			{
				Text = "What happened here!?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "It was those Rebels!  They always come here raiding and looting our supplies.  We always give them what they want, ever since they kidnapped my eldest boy all those years ago... ",
		Responses =
		{
			{
				Text = "How can I help?|GotoNode|TellMeMoreNode"
			},
			{
				Text = "Good luck..."
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "They have taken everything except my youngest boy, Lort.  My farm is destroyed, my friends have been murdered...  I demand revenge! I have some treasure hidden away they didn't find.  Harvest their heads and I shall see you rewarded!",
		Responses =
		{
			{
				Text ="I shall help you exact revenge!!|GotoNode|StartNode"
			},
			{ 
				Text = "I don't have time for this right now...",
			},
		},			
	},
}
