
-- example table to use with the merchant system.
Dialog.PyromancyT1Dialog =
{
	StartNode =
	{

		NpcText = "I sell the Summoner's Grandmaster Pyromancy Armor Set here.  You must be a Grandmaster Pyromancer to purchase this armor set.",

		Responses =
		{
			{
				Text = "I've come to trade my coins!|SellToPlayer"
            },
		},
	},
}
