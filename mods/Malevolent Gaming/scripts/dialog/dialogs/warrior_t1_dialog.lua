
-- example table to use with the merchant system.
Dialog.WarriorT1Dialog =
{
	StartNode =
	{

		NpcText = "I sell the Summoner's Grandmaster Warrior Armor Set here.  You must be a Grandmaster at Martial Prowess to purchase this armor set.",

		Responses =
		{
			{
				Text = "I've come to trade my coins!|SellToPlayer"
            },
		},
	},
}
