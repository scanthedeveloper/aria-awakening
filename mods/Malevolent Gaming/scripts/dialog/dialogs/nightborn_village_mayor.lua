
-- example table to use with the merchant system.
Dialog.NightbornVillageMayor =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Hello and welcome to our village!  We are the Nightborn!  We seek out magical gems that many of the more dangerous wilderness denizens of Celador may carry that are of particular interest to us.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "Tell me more...|GotoNode|AboutMyselfNode"
			},
			{
				Text = "[00FF00]Patient Zero[-] - Limited Time Quest.|GotoNode|PatientZeroNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "These only now have become available due to our effect on increasing the magic of this world upon our entry. These gems are actually, collected magical energy that has now returned to this world with us.",
		Responses =
		{
			{
				Text = "Tell me more...|GotoNode|TellMeMoreNode"
			},
			{
				Text = "Interesting..."
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Each camp in the wilderness has a sort of leader carrying the gems we seek.  Root out these dangerous creatures who wish to do harm to our world and i'm sure any of our merchants would gladly give you a unique gift from their shop.",
		Responses =
		{
			{
				Text = "Thank you..."
			},
		},			
	},
	PatientZeroNode =
	{
		NpcText = "We have an emergency!  Our mining operation due west of here has come under attack!  I have sent for our Royal Heirophant & his apprentice to help with the investigation.  You can find them here in the village at the Alchemist Shop.",
		Responses =
		{
			{
				Text = "What needs to be done?|GotoNode|PatientZero2Node"
			},
		},			
	},
	PatientZero2Node =
	{
		NpcText = "You are ito investigate the source of the attack at the Black Forest Mine, due west.  Bring back any evidence of this crime so that we may seek justice!  You should begin by talking to the blacksmith apprentice who just returned to town.",
		Responses =
		{
			{
				Text = "Where is he?|GotoNode|PatientZero3Node"
			},
		},
	},
	PatientZero3Node =
	{
		NpcText = "He is a Human!  He was sent here from the mining guild to train with our local Nightborn Blacksmith for a few weeks.  I think he can give you some more information about the camp.",
		Responses =
		{
			{
				Text = "Thank you..."
			},
		},			
	},
}
