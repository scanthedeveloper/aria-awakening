
-- example table to use with the merchant system.
Dialog.LCBGuildQuestVileHides =
{
	StartNode =
	{
		NpcText = "Hello!  I'm a member of the new LCB Guild quest system.  Managing LCB is a big effort, and our Guildmaster requires we collect as many Vile Hides as possible!",
		Responses =
		{
			{
				Text = "Lets trade some Vile Hides.. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
