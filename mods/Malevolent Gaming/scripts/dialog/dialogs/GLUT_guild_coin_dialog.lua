
-- example table to use with the merchant system.
Dialog.GLUTGuildCoin =
{
	StartNode =
	{
		NpcText = "Hello!  I'm a member of the new GLUT Guild quest system.  These Blackstone Inferno Cultists are a plague on these great lands! Collect their [FF9900]Guild Notes[-] and I will reward you.",
		Responses =
		{
			{
				Text = "Turn in (5) [FF9900]Guild Notes[-]. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
