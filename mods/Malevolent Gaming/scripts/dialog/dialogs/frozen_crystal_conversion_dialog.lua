
-- example table to use with the merchant system.
Dialog.FrozenCrystalConversionDialog =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Hello Adventurer!   It wasn't always this damn cold here!  Did you know this land was once a bountiful grassland!?  The great nature spirit has shared his wisdom with us and we could use your help!",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Turn in quest item...|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "How can I help?...|GotoNode|AboutMyselfNode"
			},
		},
	},

	AboutMyselfNode =
	{
		NpcText = "There are tales of an ancient, powerful druid who was corrupted by blood magic at the hands of a Dark Stone.  We believe that he froze the life giving magic in this region into small crystal fragments.",
		Responses =
		{
			{
				Text = "What happened next?|GotoNode|TellMeMoreNode"
			},
		},
	},

	TellMeMoreNode =
	{
		NpcText = "The Nature Spirt believes that if we can reclaim the shattered, magic heart of the Tundra we can bring life back to this world.  Help us reclaim the frozen crystal fragments.  I can help you merge them into larger fragments.",
		Responses =
		{
			{
				Text = "I will begin my search...|GotoNode|StartNode"
			},			
		},			
	},
}
