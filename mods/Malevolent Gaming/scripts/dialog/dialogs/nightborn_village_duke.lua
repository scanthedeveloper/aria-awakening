
-- example table to use with the merchant system.
Dialog.NightbornDuke =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Greetings noble adventurer.  It troubles me greatly to hear about the events of our minining operation to the west.  Perhaps you could bring back some blood samples for me to review and we can better understand how to remedy this nightmare? ",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "I brought the samples of blood!|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "What is a Lycan?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "Commonly mistaken as a werewolf, the Lycan is a more advanced species that, unlike the werewolf, can for the most part, control their transformations.  A werewolf once changed has no control over their body and kills until stopped.",
		Responses =
		{
			{
				Text = "Do you think we can restore their humanity?|GotoNode|TellMeMoreNode"
			},
			{
				Text = "I've heard enough thanks."
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "It's difficult to say.  From ancient texts, we are told that a werewolf can only become a Lycan if we discover the source of their corruption; also known as, Patient Zero.  The first werewolf may help us understand how they transformed to begin with.",
		Responses =
		{
			{
				Text ="Hmmmmm interesting..."
			},
			{ 
				Text = "Lets talk about that reward again.|GotoNode|StartNode",
			},
		},			
	},
}
