
-- example table to use with the merchant system.
Dialog.PatreonVillageHatchetTrader =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Welcome to the new Patreon Shop.  Players can trade the old GOD Tools for new ones here.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Hello, I would like browse your store.|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "Tell me about your self.|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "To run our community server we unfortunately occur operational costs, so we have created an optional subscription service using Patreon.  Our operational costs are 100% funded by our Patrons and help maintain the server.",
		
		Responses =
		{
			{
				Text = "That's really great! Tell me more please|GotoNode|TellMeMoreNode"
			},
			{
				Text = "Good bye"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Our Patrons and their support keep this server alive.  As of December, 2019 our Patrons have already funded the upkeep and operational costs of this server for over (2) years!",
		Responses =
		{
			{
				Text ="Thank you for all that information."
			},
			{ 
				Text = "How about we talk some trade buisness now.|GotoNode|StartNode",
			},
		},			
	},
}
