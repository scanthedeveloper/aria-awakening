
-- example table to use with the merchant system.
Dialog.BlackpathFoodMerchant =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Hmph.  You don't look like a Blackpath, but if ye got some Blood Runes for trade, I suppose we can do business.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Let me see what your selling.|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "Who are you?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "I once commanded a whole gang of bandits!  We ruled Denir and struck fear in the hearts of the local residents. Later, I was recruited to the Blackpath along with my gang.",
		Responses =
		{
			{
				Text = "What happened next?|GotoNode|TellMeMoreNode"
			},
			{
				Text = "<Stare at him>..."
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "I finally got wind of what our underlying motives were and I spoke out against this jack-ass named Malek.  Later, I found my self reassigned to be in charge the cooks!  They can all go to hell.",
		Responses =
		{
			{ 
				Text = "There is always someone worse...|GotoNode|StartNode",
			},
		},			
	},
}
