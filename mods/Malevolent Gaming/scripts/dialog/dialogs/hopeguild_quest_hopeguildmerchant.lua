
-- example table to use with the merchant system.
Dialog.HopeGuildEnchanting =
{
	StartNode =
	{
		NpcText = "Hello!  I'm a member of the new HOPE Guild quest system.  I am very excited to offer you some of our newest server items, exclusively obtained here.  Check out my shop!",
		Responses =
		{
			{
				Text = "Lets trade... |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
