
-- example table to use with the merchant system.
Dialog.EssenceTrader =
{
	StartNode =
	{

		NpcText = "Greetings mortal!  Alas we have arrived here in HOPE City! We are ready and willing to teach the citizens of Celador the Dark Arts of Demonology!  Ha ha!  Though first, we need Nether Essence.  ",

		Responses =
		{

			{
				Text = "I've come to trade Nether Essence!|SellToPlayer"
			},
			{
				Text = "What is Nether Essence?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "Nether Essence is the siphoned demonic energy.  As Warlocks, it empowers our spells and spell books.  Over the millennia, we have learned to harness and control this energy by summoning dark minions under our control!",
		Responses =
		{
			{
				Text = "How can I help?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "It is rumored that Pyros the Fire God, is enraged with SCAN the God of HOPE, for stealing the magics of Pyromancy.  He has aligned him self with a new army of demonborn warriors in the Barrenlands. Harvest & retrieve their Nether Essence.",
		Responses =
		{
			{
				Text ="I shall return with the Nether Essence!!|GotoNode|StartNode"
			},
			{ 
				Text = "Thank you for the explanation...",
			},
		},			
	},
}
