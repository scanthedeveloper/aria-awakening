
-- example table to use with the merchant system.
Dialog.NightbornVillageStableMaster =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = " Ahhh, come closer come closer and have no fear. I assure you while they are still fearsome in appearance the Wargs of the Nightborn are more than docile for their masters. See how they stand beside Stags in harmony.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Show me your wares!|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "How are they so tame?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "Why long have the Nightborn sought to have balance with nature rather than trying to be its master. That is what most humans neglect to seek. The harmony to be found within our devoted mounts.",
		Responses =
		{
			{
				Text = "I respect your appreciation for these animals.|GotoNode|TellMeMoreNode"
			},
			{
				Text = "Good bye"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "For just a few gems you too can ride the hills upon the back of a noble Autumn Stag. Or perhaps you seek the protection behind the fearsome visage of the Night Wargs? Either is a grand choice!",
		Responses =
		{
			{
				Text ="Indeed!"
			},
			{ 
				Text = "How about we talk some trade buisness now|GotoNode|StartNode",
			},
		},			
	},
}
