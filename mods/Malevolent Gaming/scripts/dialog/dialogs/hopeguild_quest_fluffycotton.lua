
-- example table to use with the merchant system.
Dialog.HopeGuildQuestFluffyCotton =
{
	StartNode =
	{
		NpcText = "Hello!  I'm a member of the new HOPE Guild quest system.  Managing HOPE is a big effort, and HOPE City Mayor, Marcus requires we collect as much Fluffy Cotton as possible!",
		Responses =
		{
			{
				Text = "Lets trade some Fluffy Cotton.. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
