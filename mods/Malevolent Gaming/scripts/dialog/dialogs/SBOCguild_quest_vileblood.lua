
-- example table to use with the merchant system.
Dialog.SBOCGuildQuestVileBlood =
{
	StartNode =
	{
		NpcText = "Hello!  I'm a member of the new SBoC Guild quest system.  Managing SBoC is a big effort, and Guild Officers require we collect as many Vile Blood as possible!",
		Responses =
		{
			{
				Text = "Lets trade some Vile Blood.. |SellToPlayer"
			},
			--{
				--Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			--},
		},
	},
	--[[AboutMyselfNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Response?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text.?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Add Text.",
		Responses =
		{
			{
				Text = "Add Text."
			},
		},
    },
    ]]--
}
