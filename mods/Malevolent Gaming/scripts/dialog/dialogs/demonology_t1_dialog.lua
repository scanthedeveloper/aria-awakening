
-- example table to use with the merchant system.
Dialog.DemonologyT1Dialog =
{
	StartNode =
	{

		NpcText = "I sell the Summoner's Grandmaster Demonology Armor Set here.  You must be a Grandmaster Demonologist to purchase this armor set.",

		Responses =
		{
			{
				Text = "I've come to trade my coins!|SellToPlayer"
            },
		},
	},
}
