
-- example table to use with the merchant system.
Dialog.NightbornVillageCarpenter =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Ahh, the art of woodcraft is both a delicate and a tough trade. Its labors can produce glorious works however and even more so when guided by the hands of the Nightborn!",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Show me your wares!|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "What is Dreamwood?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "Dreamwood, is something only we Nightborn can make, without our magicks it would simply be Blightwood. How barbaric would that be? <Laughs>.",
		Responses =
		{
			{
				Text = "Wow, tell me more!|GotoNode|TellMeMoreNode"
			},
			{
				Text = "Good bye"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "We do not simply hack and carve our products, they are grown and shaped through techniques found over millennia. I would try to teach you but I am afraid with your mortal lifespan we would only get through the most basic Fae Techniques!",
		Responses =
		{
			{
				Text ="Maybe someday I'll learn to carve Dreamwood..."
			},
			{ 
				Text = "How about we talk some trade buisness now|GotoNode|StartNode",
			},
		},			
	},
}
