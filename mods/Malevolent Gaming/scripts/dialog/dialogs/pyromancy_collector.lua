
-- example table to use with the merchant system.
Dialog.PyromancyResinCollector =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "What!?  Why do you interrupt me?",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Lets trade...|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "What are you doing in here?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "I am here studying the earth and fire elementals.  I am collecting hot resin to determine if it has any magical use.",
		Responses =
		{
			{
				Text = "Where does Hot Resin come from?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "It appears to be a by-product of these fire elementals within the cave.  Not all of them produce it, but if you have some, i'll reward you for my research. ",
		Responses =
		{
			{
				Text = "What kind of reward?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "Lord SCAN smite you!  You are a greedy bugger!  I have pyromancy items, scrolls, gold, minerals, rare dyes, and a few prized cosmetic items.  Trade me to find out.",
		Responses =
		{
			{
				Text = "I'll go investigate..."
			},
		},
	},
}
