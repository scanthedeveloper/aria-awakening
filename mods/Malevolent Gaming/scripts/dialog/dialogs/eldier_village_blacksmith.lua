
-- example table to use with the merchant system.
Dialog.EldierVillageBlacksmith =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Hello and welcome to my shop. I am attempting to add more conversation to see how long this goes. Hopefully this does not look to bad. Lets try to keep this a little longer if we can. last sentence for sure I swear.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Hello, I would like to purchase an Item|SellToPlayer"
			},
			{
				--this will call the BuyFromPlayer function in dialog_responses.lua
				Text = "I was thinking of selling some stuff|BuyFromPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "Tell me about yourself if you don't mind|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "Where to begin... I was born outside of Helm near Lake Denir. My paw was a miner while my mother tended to the gardens and sold our excess vegitables and fruits. I have always been facinated by blacksmithing... look at me just jibbering away.",
		Responses =
		{
			{
				Text = "That's a nice storey, tell me more please|GotoNode|TellMeMoreNode"
			},
			{
				Text = "Good bye"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "I setteled here in Eldier Village. Oh lets see, some where around 15 or 16 years ago. When the previous blacksmith retired, I took over as a weapon smith. I am also a collector and a buyer of fine weapons. I would like nothing more then to chat all day.",
		Responses =
		{
			{
				Text ="Oh golly ge willikers mister, your a teller..."
			},
			{ 
				Text = "How about we talk some trade buisness now|GotoNode|StartNode",
			},
		},			
	},
}
