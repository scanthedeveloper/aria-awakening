
-- example table to use with the merchant system.
Dialog.BulkCarpentryDialog =
{
	StartNode =
	{

		NpcText = "Welcome to our shop!  If you're a seasoned carpenter you can buy bulk-crafting recipes from me!  Recipes become available at 60, 80, & 100 skill-levels.",

		Responses =
		{
			{
				Text = "I've come to trade my coins!|SellToPlayer"
            },
		},
	},
}
