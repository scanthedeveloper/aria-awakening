
-- example table to use with the merchant system.
Dialog.NightbornVillageBlacksmith =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "I do not have a lot of time to make idle conversation. The forge fire waits for no one and requires my constant vigilance. So make your requests or leave it matters not to me.",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "Show me your wares!|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "This place is amazing!|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "Nightborn smiths do not simply pound ore like cruder beings. We see the possibilities contained within it and bring them out. We know what the metal wishes to be. <Chuckles> To think Dwarves claim themselves to be the master artisans of metals.",
		Responses =
		{
			{
				Text = "What kind of wares do you make?|GotoNode|TellMeMoreNode"
			},
			{
				Text = "Good bye"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "<sniffs the air> what is that I smell upon you? Could it be Nightborn Gems? Well hand it over! I am sure I have something you will desire!",
		Responses =
		{
			{ 
				Text = "Lets take a look.|GotoNode|StartNode",
			},
		},			
	},
}
