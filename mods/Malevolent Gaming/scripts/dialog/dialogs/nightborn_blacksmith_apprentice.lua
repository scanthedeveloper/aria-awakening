
-- example table to use with the merchant system.
Dialog.NightbornBlacksmithApprentice =
{
	-- every dialog table entry needs a StartNode
	-- after that you can name them whatever you want like in this example the other
	-- two are AboutMyselfNode and TellMeMoreNode
	StartNode =
	{
		-- every node needs to have a NpcText string variable.
		-- this is the npc dialog for the current node.
		NpcText = "Those bastards!  They killed all my friends...  Help me find justice!  I am searching for evidence of who attacked our camp at the Black Forest Mine.  Can you help me?",
		-- every node needsd a Responses table
		-- this holds tables that hold the player response strings and requirements
		Responses =
		{
			-- every table in this area needs to have a Text value that is a string
			-- (optional)can contain a Req table with testing if Objvar = (string)value
			-- thats it, simple fast effective tables
			{
				-- right now this system only supports a string = string test
				-- it looks at a objvar by ObjVar="value"
				-- and tests if its equal to Value="test"
				-- Req = {ObjVar="TestObjVar",Value="test"},
				-- the Text string does 2 things, gives the players text response
				-- it also has a | symbol which allows you to run functions
				-- that pass the player,merchant and arguments
				-- in the example below, this fires the SellToPlayer function in the dialog_responses.lua
				-- end result player sees the hello, i would like to purchase. when the click the string
				-- on the interface it calls the function SellToPlayer(player,merchant,args)
				Text = "I found something unusual...|SellToPlayer"
			},
			{
				--this will call the GotoNode function, and send the player to the AboutMyselfNode
				Text = "Where did you come from?|GotoNode|AboutMyselfNode"
			},
		},
	},
	AboutMyselfNode =
	{
		NpcText = "I was sent here to the Nightborn Village to train with the local blacksmith.  We have been uncovering hordes of Nightborn Gems at the Cave and it was my repsonsibility to bring them to town to trade.",
		Responses =
		{
			{
				Text = "Where is the camp?|GotoNode|TellMeMoreNode"
			},
		},
	},
	TellMeMoreNode =
	{
		NpcText = "The camp is located to the far west part of the Black Forest, just south of the Ork Raider camp.  At night you can hear screaming & howling.  By the Gods!  Its unnatural!  There is some type of beast or beasts out there hunting, like which I have never before seen!",
		Responses =
		{
			{
				Text = "What happened next?|GotoNode|MoreQuestInfofNode"
			},
		},			
    },
    MoreQuestInfofNode =
	{
		NpcText = "A few nights before I left, one of the guards went missing.  I'm not sure if he ever returned. There was a type of green-light that pierced the sky that evening followed by a deep fog.  If you find out the nature of these mysterious spells I will reward you!",
		Responses =
		{
			{
				Text = "I'll go investigate..."
			},
		},
	},
}
