-- table to hold the dialog response functions
Dialog.Responses = {}

-- this is what makes the magic happen
-- @param player - player interacting with the merchant
-- @param merchant - current merchant calling this function
-- @param args - arguments sent by the Handler function
Dialog.Responses.Handler = function(player,merchant,args)
	if(player == nil or merchant == nil or args == nil) then return end
	if(Dialog.Responses[event.Function]) then
		Dialog.Responses[event.Function](player,merchant,args)
	end
end

-- this will open the merchant dialog window for players to purchase items
-- @param player - player interacting with the merchant
-- @param merchant - current merchant calling this function
-- @param args - arguments sent by the Handler function
Dialog.Responses.SellToPlayer = function(player,merchant,args)
	OpenPlayerBuyFromVendorDialog(player,merchant)
end

-- this will open the merchants dialog window for the players to sell to
-- @param player - player interacting with the merchant
-- @param merchant - current merchant calling this function
-- @param args - arguments sent by the Handler function
Dialog.Responses.BuyFromPlayer = function(player,merchant,args)
	OpenPlayerSellToVendorDialog(player,merchant)
end

-- this will push the player to the next node selection for extending dialog chains.
-- @param player - player interacting with the merchant
-- @param merchant - current merchant calling this function
-- @param args - arguments sent by the Handler function
Dialog.Responses.GotoNode = function(player,merchant,args)
	local responseInfo = StringSplit(args,"|")
	OpenMerchantDialog(player,merchant,responseInfo[4])
end
