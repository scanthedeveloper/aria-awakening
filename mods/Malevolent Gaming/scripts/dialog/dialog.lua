-- the main creation of the Dialog table
Dialog = {}
-- require the dialog response functions
require 'dialog.dialog_response'

-- require any custom merchant dialog below this will
-- be the location of your new dialog scripts
require 'dialog.dialogs.eldier_village_blacksmith'

require 'dialog.dialogs.nightborn_village_blacksmith'
require 'dialog.dialogs.nightborn_village_carpenter'
require 'dialog.dialogs.nightborn_village_artist'
require 'dialog.dialogs.nightborn_village_tailor'
require 'dialog.dialogs.nightborn_village_stablemaster'
require 'dialog.dialogs.nightborn_village_architect'
require 'dialog.dialogs.nightborn_village_rogue'
require 'dialog.dialogs.nightborn_village_mayor'
require 'dialog.dialogs.nightborn_village_duke'
require 'dialog.dialogs.nightborn_village_apprentice'
require 'dialog.dialogs.nightborn_blacksmith_apprentice'

require 'dialog.dialogs.patreon_village_merchant'
require 'dialog.dialogs.patreon_village_hatchet_trader'
require 'dialog.dialogs.patreon_village_pick_trader'
require 'dialog.dialogs.patreon_village_fishingpole_trader'
require 'dialog.dialogs.patreon_currency_exchangedialog'

require 'dialog.dialogs.blackpath_food_trader'
require 'dialog.dialogs.blackpath_turncoat'
require 'dialog.dialogs.blackpath_seedfarmer'
require 'dialog.dialogs.blackpath_metalscrapper'

require 'dialog.dialogs.hopeguild_quest_ashboards'
require 'dialog.dialogs.hopeguild_quest_cobaltingots'
require 'dialog.dialogs.hopeguild_quest_fluffycotton'
require 'dialog.dialogs.hopeguild_quest_hopeguildenchanting'
require 'dialog.dialogs.hopeguild_quest_armor'
require 'dialog.dialogs.HOPE_guild_coin_dialog'
require 'dialog.dialogs.LCBguild_quest_armor'
require 'dialog.dialogs.LCBguild_quest_vilehides'
require 'dialog.dialogs.SBOCguild_quest_armor'
require 'dialog.dialogs.SBOCguild_quest_vileblood'
require 'dialog.dialogs.LORDguild_quest_obsidian'
require 'dialog.dialogs.LORDguild_quest_armor'
require 'dialog.dialogs.GUILD_currency_exchange_dialog'
require 'dialog.dialogs.DUNE_guild_quest_dialog'
require 'dialog.dialogs.DUNE_guild_coin_dialog'
require 'dialog.dialogs.GLUT_guild_coin_dialog'
require 'dialog.dialogs.GLUT_guild_quest_dialog'

require 'dialog.dialogs.pyromancy_collector'

require 'dialog.dialogs.rebel_quest_farmer'
require 'dialog.dialogs.essence_trader'

require 'dialog.dialogs.frozen_nature_spirit_dialog'
require 'dialog.dialogs.frozen_crystal_conversion_dialog'

require 'dialog.dialogs.druid_t1_dialog'
require 'dialog.dialogs.mage_t1_dialog'
require 'dialog.dialogs.pyromancy_t1_dialog'
require 'dialog.dialogs.demonology_t1_dialog'
require 'dialog.dialogs.ranger_t1_dialog'
require 'dialog.dialogs.warrior_t1_dialog'

require 'dialog.dialogs.bulk_tailoring_dialog'
require 'dialog.dialogs.bulk_blacksmithing_dialog'
require 'dialog.dialogs.bulk_carpentry_dialog'

require 'dialog.dialogs.newplayer_reward_dialog'

